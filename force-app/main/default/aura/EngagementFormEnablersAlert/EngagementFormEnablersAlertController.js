({
    doInit : function(component, event, helper) {
        var action = component.get("c.evaluateEnablers");
        action.setParams({ formId : component.get("v.recordId") });
 
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var serverResponse = response.getReturnValue();
                component.set("v.showCmp", serverResponse.showCmp);
                component.set("v.message", serverResponse.message);
            }
        }); 
        $A.enqueueAction(action);
    }
})