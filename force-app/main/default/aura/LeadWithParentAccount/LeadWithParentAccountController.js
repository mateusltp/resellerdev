({
    doInit : function(component, event, helper) {

        component.set('v.columns', [
            {label: 'Account Name', initialWidth: 150, fieldName: 'linkName', type: 'url',
            typeAttributes: {label: { fieldName: 'Name' },value:{fieldName: 'linkName'}, target: '_blank'}
            },
            {label: 'Parent Name', initialWidth: 150, fieldName: 'pLinkName', type: 'url',
            typeAttributes: {label: { fieldName: 'pName' },value:{fieldName: 'pLinkName' }, target: '_blank'}
            },
            {label: 'Street', fieldName: 'ShippingStreet', type: 'text'},
            {label: 'City', fieldName: 'ShippingCity', type: 'text'},
            {label: 'State', fieldName: 'ShippingState', type: 'text'}
            
        ]);

        helper.searchSimiliarAccount(component, event, helper);
    },

    onChangeFilter: function(component, event, helper){
        helper.onChangeFilter(component, event, helper);
    },
})