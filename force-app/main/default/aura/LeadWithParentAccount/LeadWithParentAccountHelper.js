({
    searchSimiliarAccount : function(component, event, helper)  {
        var company = component.find('companyName').get('v.value');
        var website = component.find('webSite').get('v.value');
        var street = component.find('street').get('v.value');
        console.log('street ' + street);
        var action = component.get("c.findPotentialParentAccount");
        
        action.setParams({
            "companyName": company,
            "street" : street,
            "website" : website
        });

        action.setCallback(this, function(response){
            var state = response.getState();  
            var data = response.getReturnValue();  
            if (state === "SUCCESS") {
                data.forEach(function(data){
                    data.linkName = '/'+data.Id;     
                    if(data.ParentId != null){
                        data.pLinkName = '/'+data.ParentId;
                        data.pName = data.Parent.Name;
                    }               
                });   
                component.set("v.accounts", data);
                component.set("v.filteredAccounts", data);
            } else {
                console.log("ERROR retrieving data");
            }
        });
        $A.enqueueAction(action); 

    },

    onChangeFilter: function(component, event, helper) {
        let data =  component.get("v.accounts"),
                    term = component.get("v.filter"),
                    results = data, regex;
            try {
                regex = new RegExp(term, "i");
                results = data.filter(row=>regex.test(row.Name) || regex.test(row.BillingCity) || regex.test(row.BillingState));
            } catch(e) {
                
            }
            component.set("v.filteredAccounts", results);
    }      
    
})