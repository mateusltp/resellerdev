({
    callChangeStageEvent : function(component, nextIndex) {                
		var cmpEvent = $A.get("e.c:fastTrackStageDoneEvent");
        cmpEvent.setParams({
            "opportunityId": component.get("v.oppId"),
            "nextIndex": 10
        });
        console.log(cmpEvent);
        cmpEvent.fire();
    },

    isFormValid: function (component, event, helper) {
        var requiredFields = []; 
        requiredFields = component.find('required');
        var isValid = true;
        requiredFields.forEach(e => {
            if (!e.get('v.value') || !e.get('v.value').trim().length) {
                isValid = false;
            }
        });
    
        return isValid;
    },

    submitAllFormsHelper: function(component, event, helper){
        component.set("v.spinnerOn", true);
        if(this.isFormValid(component, event, helper)){
            component.find("rEditForm").submit();
            component.find("rEditFormQ").submit();
        } else {
            this.showToast('error', 'Ops!', 'Please enter mandatory fields.');
            component.set("v.spinnerOn", false);
        }        
        this.showToast('warning', 'Warning!', 'Remember to send to Pipefy');
    },
    
    showToast : function(type,title, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "type": type,
            "message":message
        });
        toastEvent.fire();
    }
    
})