({
    doInit : function(component, event, helper) {
        var clientSuccessStatus = component.find('clientSuccessStatus').get('v.value');
        var enablersStatus = component.find('enablersStatus').get('v.value');
        var msg = 'Pendind Approval: ';

        console.log('ClientSuccessStatus ' + clientSuccessStatus);
        console.log('enablersStatus ' + enablersStatus);

        if(!clientSuccessStatus || !enablersStatus.includes('Approved')){
            if(!clientSuccessStatus){
                msg += 'Client Success has not approved yet. '
            }            
            if(!enablersStatus.includes('Approved')){
                msg += 'Launch Scorecard (Enablers) has not been approved yet.'
            }
            component.set('v.showCmp', true);
            component.set('v.message', msg);
        } else {
            component.set('v.showCmp', false);
        }
    }
})