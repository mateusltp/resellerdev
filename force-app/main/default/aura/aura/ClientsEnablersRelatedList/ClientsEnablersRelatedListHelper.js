({
    getEnablers : function(component, event, helper) {
        var action = component.get("c.getEnablers");
        action.setParams({  "recordId" : component.get("v.recordId")  });
 
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                component.set("v.enablers", response.getReturnValue());
            }
        });
 
        $A.enqueueAction(action);
    },
    updateEnablerHpr : function(component, event, helper) {
        var action = component.get("c.updateEnablerApx");

        action.setParams({  
            "recordId" : event.getSource().get('v.id'),
            "achieved" : event.getSource().get('v.checked')
        });
        component.set("v.spinnerOn", true);
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            console.log(response.getState());
            if (state === "SUCCESS") {
                var responseValue = response.getReturnValue();
                if (responseValue.status == 'SUCCESS') {
                    helper.showToast('success', responseValue.status, responseValue.message);
                    document.location.reload(true);;
                } else {
                    helper.showToast('error', responseValue.status, responseValue.message);
                }
                component.set("v.spinnerOn", false);
            }
        });
 
        $A.enqueueAction(action); 
    },
    showToast: function(type, title, message) {      
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type": type
        });
        toastEvent.fire();
	}
})