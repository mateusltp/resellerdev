({
    callChangeStageEvent : function(component, event) {
		var cmpEvent = $A.get("e.c:fastTrackStageDoneEvent");
        var nextIndex = component.get("v.myStageIndex");
        nextIndex += 1;
        cmpEvent.setParams({
            "opportunityId": component.get("v.recordId"),
            "nextIndex": nextIndex
        });
        cmpEvent.fire();
    },
    showToast : function(type, title, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type": type
        });
        toastEvent.fire();
    }
})