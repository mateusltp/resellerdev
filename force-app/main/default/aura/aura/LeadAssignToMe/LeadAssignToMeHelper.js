({
    assignLeadToMe : function( component ) {
        var action = component.get("c.assingToMe");

        action.setParams({ aLeadId : component.get("v.recordId") });
 
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log("From server: " + response.getReturnValue());
                this.showToast("Success", "You own this record!");
            } else if (state === "ERROR") {
                var errors = response.getError();
                var errorMessage;
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        errorMessage = errors[0].message;
                    }
                } else {
                    errorMessage = "Unknown error";
                }   
                this.showToast("Error", errorMessage);
            }
            $A.get("e.force:closeQuickAction").fire();
            $A.get('e.force:refreshView').fire();
        });
        $A.enqueueAction(action);
    },
    showToast : function(title , message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title + "!",
            "message": message,
            "type": title
        });
        toastEvent.fire();
    }
})