({

    callStage : function(component, event) {  
        component.set('v.saveAndCallStage',true);          
    },

    backStage : function(component, event) {  
        var cmpEvent = $A.get("e.c:fastTrackStageDoneEvent");
        var nextIndex = component.get("v.myStageIndex"); 
        var eventSourceId = event.getSource().getLocalId();
        nextIndex +=  -1;
        cmpEvent.setParams({
            "opportunityId": component.get("v.proposalCreationTO.opportunityId"),
            "nextIndex": nextIndex
        });
        cmpEvent.fire();          
    },

    justSave : function(component, event) {  
        component.set('v.saveAndCallStage',false);          
    },
    handleError : function(component, event){
        component.set("v.spinnerOn", false);
    },
    handleSuccess : function(component, event) {  
        if ( component.get("v.yesCounter")>=2 || !component.get('v.saveAndCallStage') ){
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Success",
                "message": "Saved",
                "type": "success"
            });
            toastEvent.fire(); 
        }             
        component.set("v.spinnerOn", false);
        
        if ( !component.get('v.saveAndCallStage') || component.get("v.yesCounter")<2 )
            return;
        var cmpEvt = $A.get("e.c:RefreshMainFastTrackCmpEvent");
        cmpEvt.fire();
        
        var cmpEvent = $A.get("e.c:fastTrackStageDoneEvent");
        var nextIndex = component.get("v.myStageIndex"); 
        var eventSourceId = event.getSource().getLocalId();
        nextIndex += eventSourceId === "btnBack" ? -1 : 1;
        cmpEvent.setParams({
            "opportunityId": component.get("v.proposalCreationTO.opportunityId"),
            "nextIndex": nextIndex
        });
        cmpEvent.fire();
    },

    handleSubmit : function(component, event){
        let yesCounter=0;
        Object.keys( event.getParam("fields")).forEach(function(item){            
            if(  (  item=='CH_Challenges_Opp_Val__c' || 
                    item=='A_Authority_Opp_Val__c' ||
                    item=='M_Money_Opp_Val__c' ||
                    item=='P_Priority_Opp_Val__c' )
                && event.getParam("fields")[item]=='Yes')
                yesCounter++;
            }
        );
        component.set("v.yesCounter", yesCounter);
        if ( yesCounter<2 && component.get('v.saveAndCallStage')  ){
             
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Invalid data",
                "message": "You need to achieve two Yes attributes for Champ",
                "type": "error"
            });
            toastEvent.fire(); 
            component.set("v.spinnerOn", false);
            return;
        }
        component.set("v.spinnerOn", true);
    },


    doInit : function(component, event, helper){   
        var scrollOptions = { left: 0, top: 0, behavior: 'smooth'};
        window.scrollTo(scrollOptions);     
        component.set("v.validatedStepPercent", 33); 
        component.set("v.activeSectionStepControl","validated");
        component.set("v.activeSection","validated");
    },

    handleSectionToggle: function (cmp, event) {
    
        cmp.set("v.activeSectionStepControl", event.getParam("openSections")[
          event.getParam("openSections").length-1
          ]
        );
        
      },

      updateNumberOfEmployees : function(component, event, helper){      
        var cmpEvent = $A.get("e.c:saveAndRefreshOfferEvent");
            cmpEvent.setParams({
                "changedProposalCreationTO": component.get("v.proposalCreationTO")
        });        
        cmpEvent.fire();
    }

})