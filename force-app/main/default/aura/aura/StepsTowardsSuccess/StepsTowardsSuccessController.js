({
    doInit: function (component, event, helper) {
        component.set('v.showSpinner', true);
        //var action = component.get("c.getStepsTowardsSuccess");
        var action = component.get("c.getResponse");

        action.setParams({
            recordId: component.get("v.recordId")
        });

        action.setCallback(this, function (response) {
            var state = response.getState();

            if (state === "SUCCESS") {
                var returnValue = response.getReturnValue();

                if (returnValue.stepsTowardsSuccessWrapper.length == 12) {
                    component.set('v.stepType', '12Steps');
                }

                console.log('returnValue.stepsTowardsSuccessWrapper: ' + JSON.stringify(returnValue.stepsTowardsSuccessWrapper));
                component.set('v.stepsTowardsSuccess', returnValue.stepsTowardsSuccessWrapper);
                component.set('v.showSpinner', false);
            } else {
                console.log(JSON.stringify(response));
                component.set('v.showSpinner', false);
                helper.showToast('Error', 'Whoops!', 'We couldn\'t find the related Steps Towards Success. Please try it again later!', '');
            }
        });

        $A.enqueueAction(action);

        component.set('v.showSpinner', false);
    },
})