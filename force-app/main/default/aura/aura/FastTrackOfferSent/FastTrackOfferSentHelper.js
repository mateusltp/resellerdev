({
	callChangeStageEvent : function(component, event) {
		var cmpEvent = $A.get("e.c:fastTrackStageDoneEvent");
        var nextIndex = component.get("v.myStageIndex"); 
        var eventSourceId = event.getSource().getLocalId();
        nextIndex += eventSourceId === "btnBack" ? -1 : 1;
        cmpEvent.setParams({
            "opportunityId": component.get("v.recordId"),
            "nextIndex": nextIndex
        });
        cmpEvent.fire();
	}
})