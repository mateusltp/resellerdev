({
    getAvailableContracts : function(component)
    {
        var action = component.get("c.getAvailableContracts");
        
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS")
            {
                var availableContracts = response.getReturnValue();
                component.set('v.contractOptions', availableContracts);
            }
 
        });
        $A.enqueueAction(action);
    },
    
    validateContractGeneration : function(component)
    {
        var action = component.get("c.validateContractGeneration");
        
        var recordId = component.get('v.recordId');
        
        action.setParams({
            recordId : recordId});
        
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS")
            {
                if (response.getReturnValue() == true) {
                    component.set('v.validated', false);
                } else {
                    alert('It seems like this record has B2B Menu Exceptions to be approved. Please check it before generating the Contract.');
                    $A.get("e.force:closeQuickAction").fire();
                }
            }
 
        });
        $A.enqueueAction(action);
    },
})