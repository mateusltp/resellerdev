({  
   callChangeStageEvent : function(component, event, index) {
		var cmpEvent = $A.get("e.c:fastTrackStageDoneEvent");
         var nextIndex = component.get("v.myStageIndex");
         nextIndex += (index);
         cmpEvent.setParams({
            "opportunityId": component.get("v.recordId"),
            "nextIndex": nextIndex
         });
         cmpEvent.fire();
	},

	lookForSignedContract: function(component){
      return new Promise(function(resolve, reject){
         var action = component.get('c.isContractSignedForOpportunity');
         action.setParams({ "opportunityId" : component.get("v.recordId")});
         action.setCallback(this, function(response){
            if(response.getState() === 'SUCCESS'){
               resolve(response.getReturnValue());
            } else {
               reject();
            }
         });
         $A.enqueueAction(action);
      });
   }
    
})