({
	doInit : function(component, event, helper) {
		helper.loadData(component, event, helper);		
	},
	handleSelect : function(component, event, helper) {
		var selectedMenuItemValue = event.getParam("value");
		
		if (selectedMenuItemValue == "ElibilityDetails") {
			helper.openAddEligibility(component, helper);																			
		}
	},
	elibilityDetails : function(component, event, helper) {
		helper.openEditEligibility(component, helper);																			
	},

	addEligibility : function(component, event, helper) {
		helper.openAddEligibility(component, helper);
	},

	deleteEligibility : function(component, event, helper) {
		helper.deleteEligibility(component, event, helper);
	}
})