({
    setChildAccounts : function(component, event, helper) {        
        var action = component.get("c.getSetUpDealHierarchy");
        var currentOppId = component.get("v.recordId");
        action.setParams({
            "oppId": currentOppId
        });     
        action.setCallback(this, function(response){
            var state = response.getState();            
            var data = response.getReturnValue();   
            var hasHierarchy = false;        
            var accRelatedToOpp = data["accRelatedToOpp"];          
            var accAvailable = data["accAvailable"];
            var totalNumberOfEmployees = data["numberOfEmployees"];
            if (state === "SUCCESS") {              
                var accountsInOpp = [];
                accRelatedToOpp.forEach(function(accRelatedToOpp){
                    accRelatedToOpp.linkName = '/'+accRelatedToOpp.Id;
                    accountsInOpp.push(accRelatedToOpp.Id);
                }); 
                accAvailable.forEach(function(accAvailable){
                    accAvailable.linkName = '/'+accAvailable.Id;
                });               
                component.set("v.accLst", accAvailable);
                component.set("v.filteredAccLst", accAvailable);
                component.set("v.aorLst", accRelatedToOpp);
                component.set("v.filteredAorLst", accRelatedToOpp);              
                if(accRelatedToOpp.length > 0 || accAvailable.length > 0){
                    hasHierarchy = true;
                } else {
                    component.set("v.noHierarchyLabel",  $A.get("$Label.c.FastTrackNoDealHierarchy"));
                }
                this.fireEvent(component, totalNumberOfEmployees, accountsInOpp, hasHierarchy);
            } else {
                console.log("ERROR retrieving data");
            }
        });
        $A.enqueueAction(action); 
    }, 
    
    handleSubmit : function(component, event, helper) {        
        var objSelected = component.find("accountTable").getSelectedRows();               
        if(objSelected.length == 0){
            this.showToast("warning", "Warning!", "Please select an account!");
            return;
        }
        component.set("v.load", true);
        component.find("btnSubmit").set("v.disabled", true);
        var objUncovered = JSON.parse(JSON.stringify(objSelected));
        var currentOppId = component.get("v.recordId");
        var action = component.get("c.saveAccountWithOpportunity");
       
        action.setParams({
            "oppId": currentOppId,
            "acctLst": objUncovered
                    
        });
        action.setCallback(this, function(response){
            var state = response.getState();  
            var data = response.getReturnValue();                          
            if (state === "SUCCESS") {      
                $A.get('e.force:refreshView').fire();
                component.set("v.load", false);
                component.find("btnSubmit").set("v.disabled", false);
                this.showToast("success", "Success!", "Account(s) included successfully.");
            }
        });
        $A.enqueueAction(action); 
    },

    onChangeFilter: function(component, event, helper) {
        var nameSource = event.getSource().get("v.name");
        if(nameSource == "accFilter"){
            let data =  component.get("v.accLst"),
                        term = component.get("v.filter"),
                        results = data, regex;
            try {
                regex = new RegExp(term, "i");
                results = data.filter(row=>regex.test(row.Name) || regex.test(row.ShippingCity) || regex.test(row.ShippingState) || regex.test(row.BillingCity) || regex.test(row.BillingState));
            } catch(e) {
                
            }
            component.set("v.filteredAccLst", results);
        } else {
            let data =  component.get("v.aorLst"),
                        term = component.get("v.filterAor"),
                        results = data, regex;
            try {
                regex = new RegExp(term, "i");
                results = data.filter(row=>regex.test(row.Name) || regex.test(row.ShippingCity) || regex.test(row.ShippingState) || regex.test(row.BillingCity) || regex.test(row.BillingState));
            } catch(e) {
                
            }
            component.set("v.filteredAorLst", results);
        }       
       
    },

    handleRemove : function(component, event, helper) {   
         
        var objSelected = component.find("accountTableAor").getSelectedRows(); 
        if(objSelected.length == 0){
            this.showToast("warning", "Warning!", "Please select an account!");
            return;
        }
        component.set("v.loadRemove", true);
        component.find("btnRemove").set("v.disabled", true);
        var proposalCreationTO = component.get("v.proposalCreationTO");
        var objUncovered = JSON.parse(JSON.stringify(objSelected));
        var currentOppId = component.get("v.recordId");  
        var accForDelete = [];
        var validateAccount = false;
        
        objUncovered.forEach(function(feeType) {
            if(proposalCreationTO.accountId == feeType.Account__c){
                validateAccount = true;
            }else{
                accForDelete.push(feeType);
            }
        });
        
        if(accForDelete.length > 0){
            var action = component.get("c.removeAccountRelationshipFromOpportunity");
            action.setParams({            
                "oppId": currentOppId,
                "acctLst": accForDelete
            });

            action.setCallback(this, function(response){
                var state = response.getState();  
                var data = response.getReturnValue();                          
                if (state === "SUCCESS") {       
                    $A.get('e.force:refreshView').fire();
                    component.set("v.loadRemove", false);
                    component.find("btnRemove").set("v.disabled", false);
                    this.showToast("success", "Success!", "Account(s) removed successfully.");

                    if(validateAccount){
                        this.showToast("warning", "Warning!", "You can't remove Opportunity Account!");
                    }
                }else {
                    console.log("error removing locations!");
                }
            });
            $A.enqueueAction(action);
        }else{
            this.showToast("warning", "Warning!", "You can't remove Opportunity Account!");
            component.find("btnRemove").set("v.disabled", false);
            component.set("v.loadRemove", false);
        }       
    },

    fireEvent : function(component, totalNumberOfEmployees, accountsInOpp, hasHierarchy)  {
        console.log("FIRE EVENT");
        var cmpEvent = component.getEvent("multipleEntityClientEvent");
        cmpEvent.setParams({
            "totalOfEmployees": totalNumberOfEmployees,
            "accountsInOpp" : accountsInOpp,
            "hasHierarchy" : hasHierarchy
        });
        cmpEvent.fire();
    },

    showToast : function(type, title, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "type": type,
            "title": title,
            "message": message
        });
        toastEvent.fire();
    },

    getDealSplitBillingColumns : function(component, event, helper) {
        var proposalCreationTO = component.get("v.proposalCreationTO");
        var columns = [
            { label: 'Account Name', initialWidth: 200, fieldName: 'linkName', type: 'url',
                typeAttributes: { label: { fieldName: 'Name' }, value: { fieldName: 'linkName' }, target: '_blank'}
            },
            { label: 'City', fieldName: 'BillingCity', type: 'text' },
            { label: 'State', fieldName: 'BillingState', type: 'text' },
            { label: 'Country', fieldName: 'BillingCountry', type: 'text' },
            { label: 'Legal Name', fieldName: 'Razao_Social__c', type: 'text', editable: true },
            { label: 'Legal Number', fieldName: 'Id_Company__c', type: 'Number', editable: true },
            { label: 'Enterprise Subscription', fieldName: 'Billing_Percentage__c', type: 'number' , editable: true,
                typeAttributes: { maximumFractionDigits: 3, minimumFractionDigits: 1 } }
        ];
        
        console.log('proposalCreationTO.customIntegrationsRequired : ' + proposalCreationTO.customIntegrationsRequired);
        if (proposalCreationTO.customIntegrationsRequired== "Yes") {
            columns.push(
                {
                    label: 'Prof. Serv. Maintenance', fieldName: 'Maintenance_Fee_Billing_Percentage__c', type: 'number' , editable: true,
                    typeAttributes: { maximumFractionDigits: 3, minimumFractionDigits: 1 } 
                }
            );
        }
        
        if (proposalCreationTO.oppRecTypeDevName == 'Client_Sales_New_Business' || proposalCreationTO.oppRecTypeDevName == 'SMB_New_Business' || proposalCreationTO.oppRecTypeDevName == 'Indirect_Channel_New_Business') {
            if (proposalCreationTO.setupFeeRequired != 'No') {
                columns.push(
                    { 
                        label: 'Setup Fee', fieldName: 'Setup_Fee_Billing_Percentage__c', type: 'number' , editable: true,
                        typeAttributes: { maximumFractionDigits: 3, minimumFractionDigits: 1 } 
                    }
                );
            }
        
            if (proposalCreationTO.customIntegrationsRequired == "Yes") {
                columns.push(
                    { 
                        label: 'Prof. Serv. Setup Fee', fieldName: 'Prof_Serv_Setup_Fee_Billing_Percentage__c', type: 'number' , editable: true,
                        typeAttributes: { maximumFractionDigits: 3, minimumFractionDigits: 1 } 
                    }
                );
            }
            
        }
     
        return columns;
    },

    isValidPercentages : function(component, helper, draftRecords/*, feeTypes*/) {
        component.set("v.spinnerOn", true);

        var countPercentage = 0.0;
        var recordPercentage;
        var data = [...component.get("v.data")];
        var dataToUpdate = [];
        var positionRecordEdited;
        var recordsPositionsToIgnore = [];
        var recordsMap = new Map();
		
        var countPercentageMap = new Map();
        
        draftRecords.forEach(function(record) {
            var keys = [];
            
            Object.keys(record).forEach(function(key){
            	if (key != "id") keys.push(key);
            });
            
            keys.forEach(function(feeType) {
            	recordPercentage = record[feeType];
                recordPercentage = recordPercentage && recordPercentage > 0 ? parseFloat(recordPercentage) : 0;
                
                positionRecordEdited = parseInt(record['id'].replace('row-',''));
    
                recordsPositionsToIgnore.push( positionRecordEdited );
                
                if (recordsMap.has(data[positionRecordEdited].Id)) {
               		var recordObj = recordsMap.get(data[positionRecordEdited].Id);
                    recordObj[feeType] = recordPercentage;
                } else {
                    var recordObj = {
                        Id : data[positionRecordEdited].Id,
                    	[feeType] : recordPercentage
                    };
                    recordsMap.set(data[positionRecordEdited].Id, recordObj);
                }
            });
        });
        
        for (var value of recordsMap.values()) {
  			dataToUpdate.push(value);
		}
        
        var feesCountPercentages = this.verifyFeesCountPercentages(data, recordsMap);
     	var feeTypesWithIncorrectTotal = [];
        
        Object.keys(feesCountPercentages).forEach(function(key) {
       		if (key == "Billing_Percentage__c" 						&& feesCountPercentages[key] != 100) feeTypesWithIncorrectTotal.push("Enterprise Subscription");
            if (key == "Maintenance_Fee_Billing_Percentage__c" 		&& feesCountPercentages[key] != 100) feeTypesWithIncorrectTotal.push("Prof. Serv. Maintenance");
            if (key == "Setup_Fee_Billing_Percentage__c" 			&& feesCountPercentages[key] != 100) feeTypesWithIncorrectTotal.push("Setup Fee");
            if (key == "Prof_Serv_Setup_Fee_Billing_Percentage__c" 	&& feesCountPercentages[key] != 100) feeTypesWithIncorrectTotal.push("Prof. Serv. Setup Fee");
        });

        if(feeTypesWithIncorrectTotal.length == 0){ 
            component.set("v.errors", null);
            component.set("v.dataToUpdate", dataToUpdate);
            return true; 
        }
        
        component.set("v.errors", {
            table: {
                title: 'Your entry cannot be saved. Fix the errors and try again.',
                messages: [
                    'The percentage total must be 100%.'
                ]
            }
        });

        helper.showToast("Error!",'error', "Please fix deal billing percentages of " + feeTypesWithIncorrectTotal.toString().replaceAll(',', ', ') + ". The total must be 100%.");

        component.set("v.spinnerOn", false);
        return false;
    },

    isValidLegalFields : function(component, event, draftRecords){
        var positionRecordEdited;
        var flagValidate = true;
        var showToast = false;
        var ltValidate = [];
        var ltAllValidate = [];
        var accDataToUpdate = [];        
        var accRecordsMap = new Map();
        var data = [...component.get("v.data")];
        
        draftRecords.forEach(function(record) {
            var keys = [];
            positionRecordEdited = parseInt(record['id'].replace('row-',''));
            ltAllValidate.push(positionRecordEdited);
            
            Object.keys(record).forEach(function(key){
            	if (key != "id") keys.push(key);
            });
            
            keys.forEach(function(feeType) {
                if((feeType == 'Razao_Social__c' || feeType == 'Id_Company__c')){
                    flagValidate = false;

                    if(record[feeType] == ''){
                        showToast = true;
                    }else{
                        if (accRecordsMap.has(data[positionRecordEdited].Account__c)) {
                            var recordObj = accRecordsMap.get(data[positionRecordEdited].Account__c);
                            recordObj[feeType] = record[feeType];
                        } else {
                            var recordObj = {
                                Id : data[positionRecordEdited].Account__c,
                                [feeType] : record[feeType]
                            };
                            accRecordsMap.set(data[positionRecordEdited].Account__c, recordObj);
                        }
                    }
                }
            });

            if(flagValidate){
                ltValidate.push(positionRecordEdited);
            }            
        });

        ltValidate.forEach(function(position) {
            if(data[position].Account__r.Razao_Social__c == '' || data[position].Account__r.Razao_Social__c == undefined || 
                 data[position].Account__r.Id_Company__c == '' || data[position].Account__r.Id_Company__c == undefined){

                    showToast = true;
            }
        });
        
        data.forEach(function(rowTable, index) {    
            if(ltAllValidate[index] != undefined){
                if(index != ltAllValidate[index]){
                    if(rowTable.Account__r.Razao_Social__c == '' || rowTable.Account__r.Razao_Social__c == undefined || 
                         rowTable.Account__r.Id_Company__c == '' || rowTable.Account__r.Id_Company__c == undefined){
    
                        showToast = true;
                    }
                }
            }            
        });        
        
        if(!showToast){
            for (var value of accRecordsMap.values()) {
                accDataToUpdate.push(value);
            }  
            
            component.set("v.errors", null);
            component.set("v.accDataToUpdate", accDataToUpdate);   
            
            return true;             
        }else {
            component.set("v.errors", {
                table: {
                    title: 'Your entry cannot be saved. Fix the errors and try again.',
                    messages: [
                        'The Legal Name or Legal Number fields cannot be empty.'
                    ]
                }
            });
    
            this.showToast("Error!",'error', "The Legal Name or Legal Number fields cannot be empty.");
    
            component.set("v.spinnerOn", false);
            return false;
        }     
    },

    saveAccOppRel : function(component, helper) {
        component.set("v.spinnerOn", true);
        var action = component.get("c.saveAccOppRelationships");
        var ltAccTemp = JSON.stringify(component.get("v.accDataToUpdate"));

        action.setParams({ 
            "accOppRelationships" : component.get("v.dataToUpdate"),
            "ltAcc"               : ltAccTemp
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.showTable", false);
                $A.get('e.force:refreshView').fire();

                helper.showToast("Success!", 'success', "Split Billing saved successfully!");
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        helper.showToast("Error!", 'error', ("Error loading Deal Hierarchy. Please reach your administrator.\n Error Message:" + errors[0].message) );
                    }
                } else {
                    helper.showToast("Error!", 'error', "Error loading Deal Hierarchy. Please reach your administrator.\n Error Message: Unknown error");
                }
            }
            component.set("v.spinnerOn", false);
            component.set("v.dataToUpdate", []);
            component.set("v.accDataToUpdate", []);
        });
        $A.enqueueAction(action);
    },

    verifyFeesCountPercentages : function(data, recordsMap) {
        var feesCountPercentages = {};
        var feeTypeToIdList = new Map();
        var decimals = 3;
        
        for (var value of recordsMap.values()) {
            Object.keys(value).forEach(function(key) {
            	if (key != "Id") feesCountPercentages[key] = 0;
            });
		}
        
        var idArray = [ ...recordsMap.keys() ];
        
        idArray.forEach(function(recordId) {
        	var record = recordsMap.get(recordId);
            
            Object.keys(record).forEach(function(objectKey) {
                if (objectKey != "Id") {
                	if (!feeTypeToIdList.has(objectKey)) {
               			feeTypeToIdList.set(objectKey, new Set([recordId]));
                        
                        var value = feesCountPercentages[objectKey] + record[objectKey];
                        feesCountPercentages[objectKey] = Number(Math.round(value+'e'+decimals)+'e-'+decimals);
                    } else {
                        feeTypeToIdList.get(objectKey).add(recordId);
                        
                        var value = feesCountPercentages[objectKey] + record[objectKey];
                        feesCountPercentages[objectKey] = Number(Math.round(value+'e'+decimals)+'e-'+decimals);
                    }
                }
            });
        });
        
        var feeTypeArray = [ ...feeTypeToIdList.keys() ];
        
        feeTypeArray.forEach(function(feeType) {
        	var idSet = feeTypeToIdList.get(feeType);
            
            data.forEach(function(record) {
                if (!idSet.has(record.Id)) {
                    feesCountPercentages[feeType] = feesCountPercentages[feeType] + (record[feeType] ? record[feeType] : 0);
                }
            });
        });

		return feesCountPercentages;        
    }
})