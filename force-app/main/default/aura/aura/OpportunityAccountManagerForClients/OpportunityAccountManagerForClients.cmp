<!--
  @File Name          : OpportunityAccountManagerForClients.cmp
  @Description        : 
  @Author             : Samuel Silva - GFT (slml@gft.com)
  @Group              : 
  @Last Modified By   : vads@gft.com
  @Last Modified On   : 02-23-2022
  @Modification Log   : 
  Ver       Date            Author      		                                Modification
  1.0    25/03/2020   Samuel Silva - GFT (slml@gft.com)                   Initial Version
  2.0    07-04-2021   Gilberto Souza - GFT (gilberto.souza@gft.com)       Change Deal Hierarchy Split Billing
-->
<aura:component
  controller="FastTrackProposalCreationController"
  implements="lightning:actionOverride,lightning:hasPageReference,flexipage:availableForAllPageTypes,flexipage:availableForRecordHome,force:lightningQuickActionWithoutHeader,force:hasRecordId,lightning:availableForFlowScreens"
  access="global"
>
  <!-- attributes -->
  <aura:attribute name="proposalCreationTO" type="FastTrackProposalCreationTO"/>   
  <aura:attribute name="recordId" type="String" />
  <aura:attribute name="load" type="Boolean" default="false" />
  <aura:attribute name="loadRemove" type="Boolean" default="false" />
  <aura:attribute name="isOpen" type="Boolean" default="false" />
  <aura:attribute name="accLst" type="Account[]" />
  <aura:attribute name="columns" type="List" />
  <aura:attribute name="filteredAccLst" type="Account[]" />
  <aura:attribute name="filter" type="String" />
  <aura:attribute name="aorLst" type="Account[]" />
  <aura:attribute name="filteredAorLst" type="Account[]" />
  <aura:attribute name="columnsAor" type="List" />
  <aura:attribute name="filterAor" type="String" />
  <aura:attribute name="noHierarchyLabel" type="String" />
  <aura:attribute name="data" type="Object"/>
  <aura:attribute name="errors" type="Object"/>
  <aura:attribute name="dataToUpdate" type="Account_Opportunity_Relationship__c[]"/>
  <aura:attribute name="accDataToUpdate" type="Account[]"/>
  <aura:attribute name="activeSection2" type="List" default="['dealHierarchy', 'splitBilling']" />
  <aura:attribute name="resellerAccount" type="Object" default="['deal, split']"/>
  <aura:attribute name="paymentMethod" type="String"/>
  <aura:registerEvent
    name="multipleEntityClientEvent"
    type="c:multipleEntityClientEvent"
  />

  <aura:handler name="init" value="{!this }" action="{!c.doInit }" />
  <aura:handler event="force:refreshView" action="{!c.doInit}" />

  <aura:if isTrue="{!v.proposalCreationTO.oppRecTypeDevName == 'Indirect_Channel_New_Business'}">

    <lightning:accordion 
    aura:id="AccordionSplit"
    activeSectionName="{!v.activeSection2}"
    allowMultipleSectionsOpen="true"
    class="slds-border_top">
    
    
      <lightning:accordionSection  name="dealHierarchy" label="Deal hierarchy">

      <span>Add and remove affiliate</span>
    
        <div class="slds-grid slds-wrap">
          <div class="slds-col slds-size_1-of-4 customMargin">
            <lightning:input
              type="text"
              name="accFilter"
              onchange="{!c.onChangefilter}"
              value="{!v.filter}"
              label="Filter by Name, City or State:"
            />
          </div>
        </div>

        <div class="slds-grid slds-wrap slds-m-top_medium">
          <div class="slds-col slds-size_3-of-3 slds-scrollable customDataTable">
            <aura:if isTrue="{! and(v.paymentMethod == 'Credit Card', 
                              v.proposalCreationTO.businessModel != 'Intermediation')}">
              <lightning:datatable
                columns="{! v.columns}"
                data=""
                keyField="id"
                aura:id="accountTable"
                showRowNumberColumn="true"
              />
              <aura:set attribute="else">
                <lightning:datatable
                  columns="{! v.columns}"
                  data="{! v.filteredAccLst}"
                  keyField="id"
                  aura:id="accountTable"
                  showRowNumberColumn="true"
                />
              </aura:set>
            </aura:if>  

          </div>
        </div>
        <div
          style="text-align: right;"
          class="slds-m-top_medium slds-m-bottom_medium"
        >
          <lightning:button
            variant="Neutral"
            label="Add affiliate"
            aura:id="btnSubmit"
            disabled="false"
            title="submit"
            onclick="{! c.handleSubmit }"
          />
          <aura:if isTrue="{! v.load }">
            <lightning:spinner
              alternativeText="Loading"
              variant="brand"
              size="large"
              title="Loading..."
            />
          </aura:if>

          <lightning:button
            variant="Neutral"
            label="Remove affiliate"
            aura:id="btnRemove"
            disabled="false"
            title="Remove"
            onclick="{!c.handleRemove}"
          />
          <aura:if isTrue="{! v.loadRemove }">
            <lightning:spinner
              alternativeText="Loading"
              variant="brand"
              size="large"
              title="Loading..."
            />
          </aura:if>
        </div>

      </lightning:accordionSection> 
    
      <lightning:accordionSection name="splitBilling" label="Split billing">
        <span>Fill out all the fee fields with the percentage each Account will pay for Gympass:</span>
        
        <div class="slds-grid slds-wrap">
          <div class="slds-col slds-size_3-of-3 slds-m-top_medium slds-scrollable customDataTable">
            <lightning:datatable
              columns="{! v.columnsAor}"
              data="{! v.data}"
              keyField="id"
              aura:id="accountTableAor"
              isLoading="{! !v.data }"
              errors="{! v.errors }"
              onsave="{! c.save }"
            />
          </div>
        </div>
      </lightning:accordionSection>
    </lightning:accordion>
  </aura:if>


  <aura:if isTrue="{!v.proposalCreationTO.oppRecTypeDevName != 'Indirect_Channel_New_Business'}">
    <span>Adjust fee fields/percentages:</span>
   
    <div class="slds-grid slds-wrap">
      <div class="slds-col slds-size_1-of-4 customMargin">
        <lightning:input
          type="text"
          name="accFilter"
          onchange="{!c.onChangefilter}"
          value="{!v.filter}"
          label="Filter by Name, City or State:"
        />
      </div>
    </div>
    <div class="slds-grid slds-wrap slds-m-top_medium">
      <div class="slds-col slds-size_3-of-3 slds-scrollable customDataTable">
        <lightning:datatable
          columns="{! v.columns}"
          data="{! v.filteredAccLst}"
          keyField="id"
          aura:id="accountTable"
          showRowNumberColumn="true"
        />
      </div>
    </div>
    <div
      style="text-align: right;"
      class="slds-m-top_medium slds-m-bottom_medium"
    >
      <lightning:button
        variant="brand"
        label="Add Account"
        aura:id="btnSubmit"
        disabled="false"
        title="submit"
        onclick="{! c.handleSubmit }"
      />
      <aura:if isTrue="{! v.load }">
        <lightning:spinner
          alternativeText="Loading"
          variant="brand"
          size="large"
          title="Loading..."
        />
      </aura:if>
    </div>
    <div class="slds-grid slds-wrap">
      <div
        class="slds-col slds-size_3-of-3 slds-m-top_medium slds-scrollable customDataTable"
      >
        <lightning:datatable
          columns="{! v.columnsAor}"
          data="{! v.data}"
          keyField="id"
          aura:id="accountTableAor"
          isLoading="{! !v.data }"
          errors="{! v.errors }"
          onsave="{! c.save }"
        />
      </div>
    </div>
    <div
      style="text-align: right;"
      class="slds-m-top_medium slds-m-bottom_medium"
    >
      <lightning:button
        variant="destructive"
        label="Remove Account"
        aura:id="btnRemove"
        disabled="false"
        title="Remove"
        onclick="{!c.handleRemove}"
      />
      <aura:if isTrue="{! v.loadRemove }">
        <lightning:spinner
          alternativeText="Loading"
          variant="brand"
          size="large"
          title="Loading..."
        />
      </aura:if>
    </div>
  </aura:if>
</aura:component>