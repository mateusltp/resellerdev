/**
 * @description       : 
 * @author            : Mateus Augusto - GFT (moes@gft.com)
 * @group             : 
 * @last modified on  : 04-22-2022
 * @last modified by  : Mateus Augusto - GFT (moes@gft.com)
**/
({
    init : function(component, event, helper) {
        helper.getExecutiveSuccessClient(component, event, helper);                  
    },
    
    refreshView : function(component, event, helper) {
        component.set("v.spinnerOn", false);
        helper.showToast('success', 'Success!', 'Form saved successfully.');
    },
    
    changeStage : function(component, event, helper) {
        //console.log('console isFormValid ',helper.isFormValid(component, event, helper));
        if(helper.isFormValid(component, event, helper)){
            //helper.submitAllFormsHelper(component, event, helper);
            helper.callChangeStageEvent(component, event);
        } else {            
            helper.showToast('error', 'Ops!', 'Please enter mandatory fields.');
        }
    },
    
    handleSuccess: function(component, event, helper){ 
        $A.get('e.force:refreshView').fire();
        helper.callChangeStageEvent(component, event)
    },
    
    submitAllForms: function(component, event, helper){
        helper.getExecutiveSuccessClient(component, event, helper);
        helper.submitAllFormsHelper(component, event, helper);
    },

    validateID: function(component, event, helper){
        var idValue = event.getParam("value");
        var isCompanyOrNationalId = idValue.includes('mail') ? false : true;
        component.set("v.isCorporateOrNational", isCompanyOrNationalId);
    },

    handleCustomField: function(component, event, helper){
        var fieldValue = event.getSource().get("v.value");
        var name = event.getSource();
        var localId = name.getLocalId();

        var myCurrentDate=new Date();
        var datefieldValue=new Date(fieldValue);
        var date15DaysAfter=new Date(myCurrentDate);
        date15DaysAfter.setDate(date15DaysAfter.getDate() + 15);
 
         if(datefieldValue > date15DaysAfter){
             helper.showToastOptionNotAvalible(component,event,helper);
             component.set("v.StartBillingDate", datefieldValue);
             component.set("v.StartBillingDateFlag", true)
         }else{
             component.set("v.StartBillingDateFlag", false)
         }
        
        
     }
    
    

})