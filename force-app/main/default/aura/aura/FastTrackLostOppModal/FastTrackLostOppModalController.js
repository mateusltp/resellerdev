({
    handleCancel: function(component, event, helper) {
        component.find('overlayLib').notifyClose();
	},
    handleSuccess: function(component, event, helper) {
        helper.showToast('success', 'Success', 'The changes have been saved');
        helper.fireRefreshMainFastTrackCmpEvent();
        component.find('overlayLib').notifyClose();
    },
    handleError: function(component, event, helper) {
        component.set("v.spinnerOn", false);
    },
    handleSubmit: function(component, event, helper) {
        component.set("v.spinnerOn", true);
    } 
})