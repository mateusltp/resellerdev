({
    doInit: function(component){

        var action = component.get("c.getRecordTypeIdByDeveloperName");

        action.setParams({
            'devName': 'Fixed_Date'
        });        

        action.setCallback(this, function(response){
            var state = response.getState();  
            var data = response.getReturnValue();                       
            if (state === "SUCCESS") {      
               component.set('v.rtId', data);
            }else{
                var errors = response.getError();
                var msg = "Error ";
                if (errors) {
                    if (errors[0] && errors[0].message) {	
                        	msg += 	errors[0].message;				
                    }
                } 
               helper.showToast("Error",msg, "error");
            }
          });
        $A.enqueueAction(action);

    },

	handleSuccess: function(component, event, helper) {
		helper.showToast('success', 'Success', 'The changes have been saved');
        // helper.fireFastTrackEvaluationRequestEvent(component, helper);
        //helper.fireRefreshMainFastTrackCmpEvent();
		component.find('overlayLib').notifyClose();
        location.reload();
    },
    handleError: function(component, event, helper) {
        helper.showToast('error', 'Error', 'The changes have been saved');
    },
    handleCancel: function(component, event, helper) {
        component.find('overlayLib').notifyClose();
	}
})