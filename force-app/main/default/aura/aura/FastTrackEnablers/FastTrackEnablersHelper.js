({
	loadEnablers : function(component, helper) {
		var to = component.get("v.proposalCreationTO");

		var action = component.get("c.getEnablers");
		action.setParams({ oppId : to.opportunityId });
		
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
				var enablers = response.getReturnValue();
				helper.setAttributes(component, helper, enablers);
            }
            else {
				alert("Enablers could not be retrieved");
			}
        });
		$A.enqueueAction(action);
		// debugger;
		// var enablers = component.get("v.proposalCreationTO.enablers");
		// helper.setAttributes(component, helper, enablers);
	},

	setAttributes: function(component, helper, enablers) {
		console.log(enablers);
		component.set("v.totalEnablersNumber", enablers.length);
		helper.setAchievedEnablersNumber(component, enablers);
	},

	setAchievedEnablersNumber: function(component, enablers) {
		var achievedEnablersNumber = 0;
		var missingEnablersList = [];
		var achievedEnablersList = [];

		for (var i=0; i<enablers.length; i++) {
			if (enablers[i].Achieved__c == 'Yes') {
				achievedEnablersList.push(enablers[i]);
				achievedEnablersNumber++;
			}

			if (enablers[i].Achieved__c == 'No') {
				missingEnablersList.push(enablers[i]);
			}
		}

		component.set("v.achievedEnablersNumber", achievedEnablersNumber);
		component.set("v.missingEnablers", missingEnablersList);
		component.set("v.achievedEnablers", achievedEnablersList);

		var enablersPopoverHeader = component.find("enablersPopoverHeader");

		if (achievedEnablersList.length <= 6 ) {
			component.set("v.iconFlagVariant", "error");
			$A.util.addClass(enablersPopoverHeader,'slds-popover_error');
		} else if (achievedEnablersList.length <= 9) {
			component.set("v.iconFlagVariant", "warning");
			$A.util.addClass(enablersPopoverHeader,'slds-popover_warning');
		} else {
			component.set("v.iconFlagVariant", "success");
			$A.util.addClass(enablersPopoverHeader,'slds-popover_success');
		}
	}
})