({
    changeStage : function(component, event, helper) {
		helper.callChangeStageEvent(component, event);
	}	,
	showSuccessToast : function(component, event, helper) {
        helper.showToast('success', 'SUCCESS!', 'Opportunity update sucessfully!');
    },
    handleSuccess : function(component, event) {       
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success",
            "message": "Saved",
            "type": "success"
        });
        toastEvent.fire(); 
        component.set("v.spinnerOn", false);
    },

    handleSubmit : function(component, event){
        component.set("v.spinnerOn", true);
    },
    callStage : function(component, event) {        
        var cmpEvent = $A.get("e.c:fastTrackStageDoneEvent");
        var nextIndex = component.get("v.myStageIndex"); 
        var eventSourceId = event.getSource().getLocalId();
        nextIndex += eventSourceId === "btnBack" ? -1 : 1;
        cmpEvent.setParams({
            "opportunityId": component.get("v.proposalCreationTO.opportunityId"),
            "nextIndex": nextIndex
        });
        cmpEvent.fire();
    }
})