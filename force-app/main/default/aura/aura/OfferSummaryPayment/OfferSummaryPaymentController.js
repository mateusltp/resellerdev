({
	doInit : function(component, event, helper) {
		debugger;
		var payment = component.get("v.payment");
		helper.loadData(component, helper, payment);																		
	},
	handleSelect : function(component, event, helper) {
		var selectedMenuItemValue = event.getParam("value");
		//var product = component.get("v.quoteItem");
		
		if (selectedMenuItemValue == "PaymentDetails") {
			//helper.openAddPayment(component, helper, product);
			helper.openAddPayment(component, helper);																			
		} else if (selectedMenuItemValue == "AddWaiver") {
			//helper.openAddWaiver(component, helper, product);
			helper.openAddWaiver(component, helper);
		} else if (selectedMenuItemValue == "AddEligibility") {
			//helper.openAddEligibility(component, helper, product);
			helper.openAddEligibility(component, helper);
		}
	},
	paymentDetails : function(component, event, helper) {
		//var product = component.get("v.quoteItem");
		//helper.openAddPayment(component, helper, product);		
		helper.openAddPayment(component, helper);																
	},
	addEligibility : function(component, event, helper) {
		//var product = component.get("v.quoteItem");
		//helper.openAddEligibility(component, helper, product);
		helper.openAddEligibility(component, helper);
	},
	addWaiver : function(component, event, helper) {
		//var product = component.get("v.quoteItem");
		//helper.openAddWaiver(component, helper, product);
		helper.openAddWaiver(component, helper);
	}
})