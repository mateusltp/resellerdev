({
    refreshView : function(component, event, helper) {
        component.set("v.spinnerOn", false);
        helper.showToast('success', 'Success!', 'Form saved successfully.');
    },
    
    changeStage : function(component, event, helper) {
        if(helper.isFormValid(component, event, helper)){
            helper.submitAllFormsHelper(component, event, helper);
            helper.callChangeStageEvent(component, event);
        } else {            
            helper.showToast('error', 'Ops!', 'Please enter mandatory fields.');
        }
    },
    
    handleSuccess: function(component, event){     
        $A.get('e.force:refreshView').fire();
    },
    
    submitAllForms: function(component, event, helper){
        helper.submitAllFormsHelper(component, event, helper);
    },

    validateID: function(component, event, helper){
        var idValue = event.getParam("value");
        var isCompanyOrNationalId = idValue.includes('mail') ? false : true;
        component.set("v.isCorporateOrNational", isCompanyOrNationalId);
    },
    
    

})