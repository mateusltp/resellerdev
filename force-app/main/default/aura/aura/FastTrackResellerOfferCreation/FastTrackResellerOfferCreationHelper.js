({
    loadGympassEntities: function(component, callback, recordId) {
        let gympassEntitiesService = component.get("c.findGympassEntities");
        gympassEntitiesService.setCallback(this, callback);        
        $A.enqueueAction(gympassEntitiesService);
    },

    loadCompetitors: function(component, callback) {
        let competitorsService = component.get("c.findCompetitors");
        competitorsService.setCallback(this, callback);        
        $A.enqueueAction(competitorsService);
    },
    fireSaveOffer : function(component)  {
        var cmpEvent = $A.get("e.c:saveOffer");
        cmpEvent.setParams({
            "changedProposalCreationTO": component.get("v.proposalCreationTO")
        });
        cmpEvent.fire();
    },
    fireSaveAndRefreshOffer : function(component)  {
        var cmpEvent = $A.get("e.c:saveAndRefreshOfferEvent");
        console.log('SAVE : ' + JSON.stringify(component.get("v.proposalCreationTO")));
        cmpEvent.setParams({
            "changedProposalCreationTO": component.get("v.proposalCreationTO")
        });        
        cmpEvent.fire();

        
    },

    onChangeTechSalesControllingField : function(component, event, helper){
        if ( event.getParam("value")!='Yes' ){  
          component.set('v.proposalCreationTO.communicationEmployee', '');
          component.set('v.proposalCreationTO.systemConnectivity', '');
          component.set('v.proposalCreationTO.payrollDeduction', '');
          component.set('v.proposalCreationTO.employeeRegistration', '');
          component.set('v.proposalCreationTO.eligibilityExchange', '');
        }
        
      },
   
    isResponseSuccessful : function(response) {
        return (response.getState() === "SUCCESS");
    },

    fireRefreshMainFastTrackCmpEvent : function(component, helper) {
        var cmpEvt = $A.get("e.c:RefreshMainFastTrackCmpEvent");
		cmpEvt.fire();
    },

    showToast : function(type, title, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type": type
        });
        toastEvent.fire();
    },

    saveAndRefresh: function(component, event, helper){   
        
        if ( isNaN(component.get('v.proposalCreationTO.quantity')) ){
          helper.showToast('warning', 'Invalid value for Quantity', 'Must be numeric value');
          return; 
        } 
        if (  component.get('v.proposalCreationTO.adjustedProbability')<0 || component.get('v.proposalCreationTO.adjustedProbability')>100 )   {
          helper.showToast('warning', 'Invalid value for Adjust Probability', 'Cannot exceed 100%');
          return;
         }
    
         if(component.get('v.proposalCreationTO.AccessFee.sObj')!=undefined && component.get('v.proposalCreationTO.AccessFee.sObj.Sales_Total_Price__c')==''){
          helper.showToast('warning', 'Invalid Fee Price!', 'Please enter Enterprise Subscription Price. All Fees must have Sales Total Price');
          return;
         }
         console.log('saveAndRefresh ');
          this.fireSaveAndRefreshOffer(component);  
          component.set('v.showDealHierarchy',true); 
       },

       promise : function(action){
        return new Promise(function(resolve,reject)
        {
            action.setCallback(this,function(response)
            {
                if(response.getState() === 'SUCCESS')resolve(response.getReturnValue());
                else reject(response.getError());
            });
            $A.enqueueAction(action);
        });
    },
})