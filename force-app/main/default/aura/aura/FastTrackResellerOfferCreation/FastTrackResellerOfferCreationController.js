({
    doInit: function (component, event, helper) {
      // $A.enqueueAction(component.get("c.setOfferProgressPercent"));        
      $A.enqueueAction(component.get("c.setGoToOfferReview"));     
      $A.enqueueAction(component.get("c.setDisableEnablers"));
      $A.enqueueAction(component.get("c.setTwelveEnablers"));      
      if ( component.get("v.proposalCreationTO.proposal.sObj.Id") == undefined){
        //$A.enqueueAction(component.get("c.saveAndRefresh"));
      }
      
      var endDate = component.find("orderEndDateInput").get("v.value");
      if ( endDate!= null ){
        component.set("v.showSectionOfferAndEnterprise", true);
      }
    },

    setTwelveEnablers: function(component, event, helper){
      var contactPermission = component.find("ContactPermission").get("v.value");
      if ( contactPermission == null || contactPermission == ''){


        component.set("v.proposalCreationTO.proposal.sObj.Expansion_100_of_the_eligible__c", true);
        component.set("v.proposalCreationTO.proposal.sObj.New_Hires__c", true);
        component.set("v.proposalCreationTO.proposal.sObj.HR_Communication__c", true);
        component.set("v.proposalCreationTO.proposal.sObj.Exclusivity_clause__c", true);
        component.set("v.proposalCreationTO.proposal.sObj.Employee_National_ID_if_applicable__c", true);
        component.set("v.proposalCreationTO.proposal.sObj.Employee_Corporate_Email__c", true);
        component.set("v.proposalCreationTO.proposal.sObj.PR_in_contract__c", true);
        component.set("v.proposalCreationTO.proposal.sObj.What_does_success_looks_like_Joint_BP__c", true);

        component.set("v.proposalCreationTO.proposal.sObj.Contact_Permission__c", "Allowlist")

        component.set("v.proposalCreationTO.isThereACompetitor", "No");
        component.set("v.proposalCreationTO.setupFeeRequired", "No");
        component.set("v.proposalCreationTO.areDependentsIncluded", "No");
        component.set("v.proposalCreationTO.willCompanyHaveFreePlan", "No");
        component.set("v.proposalCreationTO.autonomousMarketPlace", "Yes");
        component.set("v.proposalCreationTO.doesItHaveUniqueTechRequest", "No");
        component.set("v.proposalCreationTO.customIntegrationsRequired", "No");

        component.set("v.proposalCreationTO.proposal.sObj.Will_the_MF_be_subsidized__c", No);
      }
    },
  
    refreshView: function(cmp, evt, helper){
      console.log('refreshView new : ' + evt.getParam("value"));
      console.log('refreshView OLD : ' + evt.getParam("oldValue"));
      var employessOffer = cmp.find("EmployeesOnThisOffer").get("v.value");
      if ( employessOffer!=null ){
        cmp.set("v.showSectionOfferAndEnterprise", true);
      }  
      if ( evt.getParam("oldValue")!=undefined && evt.getParam("value")!="" && evt.getParam("oldValue")!=evt.getParam("value")){
        $A.enqueueAction(cmp.get("c.saveAndRefresh"));
      }
    },

  
    handleSectionToggle: function (cmp, event) {
      cmp.set("v.proposalCreationTO.fastTrackCurrentActiveStep", event.getParam("openSections")[
        event.getParam("openSections").length-1
        ]
      );
      let gympassEntities = cmp.get("v.gympassEntities");
     
      if (gympassEntities==undefined || gympassEntities.length==0 )
        $A.enqueueAction(cmp.get("c.loadGympassEntities"));
     
    },
  /*
    
    setOfferProgressPercent: function (cmp) {
      let offerProgressPercent = 0;
      let proposalCreationTO = cmp.get("v.proposalCreationTO");
      
      if ( proposalCreationTO.fastTrackStage=='Offer Creation' && proposalCreationTO.quoteId==null )
        offerProgressPercent = 45;
      
      if ( proposalCreationTO.fastTrackStage=='Offer Creation' && proposalCreationTO.quoteId!=null )
        offerProgressPercent = 70;
  
      if ( cmp.get("v.proposalCreationTO.fastTrackCurrentActiveStep")==undefined ||
           cmp.get("v.proposalCreationTO.fastTrackCurrentActiveStep")==""   ){
        cmp.set("v.activeSection", "stepProposalInformation");
        cmp.set("v.proposalCreationTO.fastTrackCurrentActiveStep", "stepProposalInformation");
      } else{
        cmp.set("v.activeSection", cmp.get("v.proposalCreationTO.fastTrackCurrentActiveStep"));
      }     
  
      cmp.set("v.offerProgressPercent", offerProgressPercent);  
  
  
      cmp.find('requiredFields').forEach(
        function( field ){
          field.showHelpMessageIfInvalid();
        });
            
      cmp.find("billingStreetInput").showHelpMessageIfInvalid();
      cmp.find("billingCityInput").showHelpMessageIfInvalid();
      cmp.find("billingStateInput").showHelpMessageIfInvalid();
      cmp.find("billingCountryInput").showHelpMessageIfInvalid();
      cmp.find("billingPostalCodeInput").showHelpMessageIfInvalid();
  
      var setupFeeRequiredSelector = cmp.find("setupFeeRequiredSelector");
      if( setupFeeRequiredSelector ){ setupFeeRequiredSelector.showHelpMessageIfInvalid(); }
    },*/
  
    loadGympassEntities: function (component, event, helper) {
      let callback = function (response) {
        console.log("load gympassentities response...");
        if (helper.isResponseSuccessful(response)) {
          component.set("v.gympassEntities", response.getReturnValue());
        }
      };
      helper.loadGympassEntities(
        component,
        callback,
        component.get("v.recordId")
      );
    },
  
    loadCompetitors: function (component, event, helper) {
      let callback = function (response) {
        console.log("load competitors response...");
        if (helper.isResponseSuccessful(response)) {
          component.set("v.competitors", response.getReturnValue());
        }
      };
      helper.loadCompetitors(component, callback);
    },
    handleMultipleEntityClientEvent: function (component, event, helper) {
      let proposalCreationTO = component.get("v.proposalCreationTO");
      let oldTotalOfEmployees = proposalCreationTO.dealHierarchy.totalOfEmployees;
      if (proposalCreationTO.dealHierarchy) {
        proposalCreationTO.dealHierarchy.accountsInOpp = event.getParam(
          "accountsInOpp"
        );
        proposalCreationTO.dealHierarchy.totalOfEmployees = event.getParam(
          "totalOfEmployees"
        );
       
      }
    },
    saveAndRefresh: function(component, event, helper){   
     /* if ( component.get('v.proposalCreationTO.proposal.sObj.Access_Fee_Adjustment_Date__c')!=undefined &&
          component.get('v.proposalCreationTO.proposal.sObj.Access_Fee_Adjustment_Date__c').length!=undefined &&
          (component.get('v.proposalCreationTO.proposal.sObj.Access_Fee_Adjustment_Date__c').length>2  ||
          (component.get('v.proposalCreationTO.proposal.sObj.Access_Fee_Adjustment_Date__c')<1 &&
          component.get('v.proposalCreationTO.proposal.sObj.Access_Fee_Adjustment_Date__c')>31) )){
        helper.showToast('warning', 'Invalid value for Access Fee Adjustment Date', 'Inform a valid number from 1 to 31 - Deadline to submit Access Fee Adjustment Date updated.');
        return; 
      }
      if ( component.get('v.proposalCreationTO.proposal.sObj.Eligible_Employee_Database_deadline__c')!=undefined &&
          component.get('v.proposalCreationTO.proposal.sObj.Eligible_Employee_Database_deadline__c').length!=undefined  &&
          (component.get('v.proposalCreationTO.proposal.sObj.Eligible_Employee_Database_deadline__c').length>2  ||
          (component.get('v.proposalCreationTO.proposal.sObj.Eligible_Employee_Database_deadline__c')<1 &&
          component.get('v.proposalCreationTO.proposal.sObj.Eligible_Employee_Database_deadline__c')>31) )){
        helper.showToast('warning', 'Invalid value for Eligible Employee Database deadline', 'Inform a valid number from 1 to 31 - Deadline to submit Eligible Employee Database updated.');
        return; 
      }
      if ( component.get('v.proposalCreationTO.proposal.sObj.Will_the_MF_be_subsidized__c')=='Yes' && 
          ( component.get('v.proposalCreationTO.proposal.sObj.Subsidy_basis_eligible_or_enrolled__c')==undefined || 
            component.get('v.proposalCreationTO.proposal.sObj.Lenght_of_the_subsidy_period_months__c')==undefined || 
            component.get('v.proposalCreationTO.proposal.sObj.Total_amount_subsidized__c')==undefined || 
            component.get('v.proposalCreationTO.proposal.sObj.Why_should_we_accept_this_subsidy__c')==undefined ||
            component.get('v.proposalCreationTO.proposal.sObj.Subsidy_basis_eligible_or_enrolled__c')=='' || 
            component.get('v.proposalCreationTO.proposal.sObj.Lenght_of_the_subsidy_period_months__c')=='' || 
            component.get('v.proposalCreationTO.proposal.sObj.Total_amount_subsidized__c')=='' || 
            component.get('v.proposalCreationTO.proposal.sObj.Why_should_we_accept_this_subsidy__c')=='' ) ){
          helper.showToast('warning', 'Invalid Membership Fee Subsidized form', 'For MembershipFee subsidized, please fill all dependents fields');
          return; 
      }
  
      if ( (component.get('v.proposalCreationTO.doesItHaveUniqueTechRequest')==undefined || component.get('v.proposalCreationTO.doesItHaveUniqueTechRequest')=='') ||
            ( component.get('v.proposalCreationTO.doesItHaveUniqueTechRequest')=='Yes' &&
              ((component.get('v.proposalCreationTO.systemConnectivity')==null || component.get('v.proposalCreationTO.systemConnectivity')=='' ) ||
              (component.get('v.proposalCreationTO.payrollDeduction')==null || component.get('v.proposalCreationTO.payrollDeduction')=='' ) ||
              (component.get('v.proposalCreationTO.employeeRegistration')==null || component.get('v.proposalCreationTO.employeeRegistration')=='' ) ||
              (component.get('v.proposalCreationTO.communicationEmployee')==null || component.get('v.proposalCreationTO.communicationEmployee')=='') ||
              (component.get('v.proposalCreationTO.eligibilityExchange')==null || component.get('v.proposalCreationTO.eligibilityExchange')=='' )) ) ){
          helper.showToast('warning', 'Invalid Tech Sales form', 'For a Tech Request Offer, please fill all dependents fields');
          return; 
      }*/
    
      /*if ( isNaN(component.get('v.proposalCreationTO.adjustedProbability')) && !component.get('v.proposalCreationTO.oppRecTypeDevName').includes('SMB')){
        helper.showToast('warning', 'Invalid value for Adjust Probability', 'Must be numeric value');
        return; 
      } */
      
      if ( isNaN(component.get('v.proposalCreationTO.quantity')) ){
        helper.showToast('warning', 'Invalid value for Quantity', 'Must be numeric value');
        return; 
      } 
      if (  component.get('v.proposalCreationTO.adjustedProbability')<0 || component.get('v.proposalCreationTO.adjustedProbability')>100 ) {
        helper.showToast('warning', 'Invalid value for Adjust Probability', 'Cannot exceed 100%');
        return;
      }
      if(component.get('v.proposalCreationTO.AccessFee.sObj')!=undefined && component.get('v.proposalCreationTO.AccessFee.sObj.Sales_Total_Price__c')==''){
        helper.showToast('warning', 'Invalid Fee Price!', 'Please enter Enterprise Subscription Price. All Fees must have Sales Total Price');
        return;
      }

      //-- [RESELLER-252] - validating the fields on Plans and Commercial conditions section before saving - START
      if(!component.get('v.proposalCreationTO.gympassEntity')) {
        helper.showToast('warning', 'Gympass Entity has no value', 'Please, there are required fields on the Plans and Commercial conditions that must be filled.');
        return; 
      }
      if(!component.get('v.proposalCreationTO.orderStartDate')) {
        helper.showToast('warning', 'Contract Start Date has no value', 'Please, there are required fields on the Plans and Commercial conditions that must be filled.');
        return; 
      }
      if(!component.get('v.proposalCreationTO.orderEndDate')) {
        helper.showToast('warning', 'Contract End Date has no value', 'Please, there are required fields on the Plans and Commercial conditions that must be filled.');
        return; 
      }
      if(!component.get('v.proposalCreationTO.proposal.sObj.Employee_Registration_Method__c')) {
        helper.showToast('warning', 'Employee Registration Method has no value', 'Please, there are required fields on the Plans and Commercial conditions that must be filled.');
        return; 
      }
      //-- [RESELLER-252] -  END

      console.log('saveAndRefresh ');
      helper.fireSaveAndRefreshOffer(component);  
      component.set('v.showDealHierarchy',true); 

        
  
        
       // component.set("v.showSectionOfferAndEnterprise", true); 

       // var customIntegrationRequired = component.find("customIntegrationRequired").get("v.value");
      //  if(customIntegrationRequired == 'Yes' && customIntegrationRequired!= ''){

           // component.set("v.showSectionProfessionalServices", true); 
       // } else{
          //  component.set("v.showSectionProfessionalServices", false);  
        //}

        /* Values off payment
        var quoteItem = component.get("v.quoteItem");
        helper.setAttributes(component, helper, quoteItem);
        helper.setPayments(component, helper, quoteItem);*/
    },
    
    handleOfferChanges: function(component, event, helper){
      if (  event.getParam("oldValue")==undefined )
        return;
        
      if ( component.get('v.goToOfferReview') &&   ( event.getParam("oldValue") != event.getParam("value")) ){
        helper.showToast('warning', 'Proposal changes detected', 'You must save and validate your offer again');
        component.set('v.goToOfferReview',false);
        component.set('v.showDealHierarchy',false);
      }   
  
    },
  
    handleOfferChangesorderExpirationDays:function(component, evt, helper){
      if ( evt.getParam("oldValue")!=undefined && evt.getParam("value")!="" && evt.getParam("oldValue")!=evt.getParam("value") &&  component.get('v.goToOfferReview') ){
        helper.showToast('warning', 'Proposal changes detected', 'You must save and validate your offer again');
        component.set('v.goToOfferReview',false);
      }
    },
    
    handleOfferChangesCustomIntegrationsRequired: function(component, evt, helper){
      if ( evt.getParam("oldValue")!=undefined && evt.getParam("value")!="" && evt.getParam("oldValue")!=evt.getParam("value") &&  component.get('v.goToOfferReview') ){
        helper.showToast('warning', 'Proposal changes detected', 'You must save and validate your offer again');
       
      }
    },
    handleOfferChangesSetupFeeRequired: function(component, evt, helper){
      if ( evt.getParam("oldValue")!=undefined && evt.getParam("value")!="" && evt.getParam("oldValue")!=evt.getParam("value") &&  component.get('v.goToOfferReview') ){
        helper.showToast('warning', 'Proposal changes detected', 'You must save and validate your offer again');
  
      }
    },
    save: function (component, event, helper){
      helper.fireSaveOffer(component);
    },
    
    refreshEnablers: function(component, event, helper){
       helper.fireRefreshEnablers(component);   
    },
    
    callStage : function(component, event, helper) { 
      var cmpEvt = $A.get("e.c:RefreshMainFastTrackCmpEvent");
      cmpEvt.fire();      
      var eventSourceId = event.getSource().getLocalId(); 
      var cmpEvent = $A.get("e.c:fastTrackStageDoneEvent");
      var nextIndex = component.get("v.myStageIndex");     
      nextIndex += eventSourceId === "btnBack" ? -1 : 1;
         
      cmpEvent.setParams({
          "opportunityId": component.get("v.proposalCreationTO.opportunityId"),
          "nextIndex": nextIndex
      });
      cmpEvent.fire();
    },
    setGoToOfferReview : function(component) {
      var to = component.get("v.proposalCreationTO");
  
      var allValid = 
        component.find('requiredFields').reduce(function (validSoFar, inputCmp) {
          if( inputCmp.get('v.disabled') == true ){ return validSoFar; }
  
          return validSoFar && 
            ( ( inputCmp.get('v.validity') && !inputCmp.get('v.validity').valueMissing ) || inputCmp.get('v.value') );
        }, true);
  
      if( allValid && 
          ( to.customIntegrationsRequired && to.areDependentsIncluded ) &&
          ( ( to.doesItHaveUniqueTechRequest != 'Yes' ) ||
            ( to.doesItHaveUniqueTechRequest == 'Yes' &&
              to.systemConnectivity &&
              to.payrollDeduction &&
              to.employeeRegistration &&
              to.communicationEmployee &&
              to.eligibilityExchange ) ) ){
        component.set("v.goToOfferReview", true);
      } else {
        component.set("v.goToOfferReview", false);
      }
    },
  
    onChangeTechSalesControllingField : function(component, event, helper){
      if ( event.getParam("value")!='Yes' ){  
        component.set('v.proposalCreationTO.communicationEmployee', '');
        component.set('v.proposalCreationTO.systemConnectivity', '');
        component.set('v.proposalCreationTO.payrollDeduction', '');
        component.set('v.proposalCreationTO.employeeRegistration', '');
        component.set('v.proposalCreationTO.eligibilityExchange', '');
      }
      
    },
  
    setDisableEnablers : function(cmp) {
      let oppRecTypedevName = cmp.get("v.proposalCreationTO.oppRecTypeDevName");
      
      if (oppRecTypedevName == 'SMB_New_Business' || oppRecTypedevName == 'SMB_Success_Renegotiation' ) {
        cmp.set('v.disableEnablers', true);
      }
    }
  });