({
    doInit: function(component, event, helper) {
        component.set("v.myStageIndex", (component.get("v.fastTrackStage") == 'Contract' ? 7 : 8) );
    },
    
    changeStage: function(component, event, helper){
        var actualStage =   component.get('v.fastTrackStage'); 
        helper.lookForSignedContract(component)
        .then($A.getCallback(function(retorno){
            if ( !retorno && actualStage == 'Contract' ){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error",
                    "message": "Please, get the Contract Sign before move your Opportunity."
                });
                toastEvent.fire();
                
                return;
            }	
            helper.callChangeStageEvent(component, event, 1);
        }))
        .catch($A.getCallback(function(){
            
        }));
    },
    
    onCheck: function(cmp, evt) {
        var checkCmp = cmp.find("checkbox");
        cmp.set("v.signedContract", ""+checkCmp.get("v.signedContract"));
        
    },
    
    backStage : function(component, event, helper) {
        helper.callChangeStageEvent(component, event, -1);         
    },
})