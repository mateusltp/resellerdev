/**
 * @description       : 
 * @author            : Jorge Stevaux - JZRX@gft.com
 * @group             : 
 * @last modified on  : 05-17-2022
 * @last modified by  : Mateus Augusto - GFT (moes@gft.com)
 * Modifications Log 
 * Ver   Date         Author                         Modification
 * 1.0   02-04-2021   Jorge Stevaux - JZRX@gft.com   Initial Version
**/
({
	doInit : function(component, event, helper) {
        var scrollOptions = { left: 0, top: 0, behavior: 'smooth'};
        var dealSplitBillingColumns = helper.getDealSplitBillingColumns(component);       
        let clientManagers = component.get("v.clientManagers");
        if (clientManagers==undefined || clientManagers.length==0 )
            $A.enqueueAction(component.get("c.loadClientManagers"));         
        window.scrollTo(scrollOptions);   
		component.set("v.noHierarchyLabel",  $A.get("$Label.c.FastTrackNoDealHierarchy"));
		component.set('v.columns', dealSplitBillingColumns);
        helper.getPaymentMethod(component, helper);
        helper.getDealDeskOperationalCase(component);
        helper.getExecutiveSuccessClient(component, event, helper);
        $A.enqueueAction(component.get("c.setIsMainContactsSavedAndRefreshed"));   
	},

	nextStage : function(component, event, helper) { 
        console.log('console executivoSucessoClienteFlag ',component.get("v.executivoSucessoClienteFlag"));
        helper.getExecutiveSuccessClient(component, event, helper);
        console.log('console executivoSucessoClienteFlag ',helper.getExecutiveSuccessClient(component, event, helper));
        console.log('console executivoSucessoClienteFlag ',component.get("v.executivoSucessoClienteFlag"));
        component.set('v.saveAndCallStage', true);
        if(!component.get("v.executivoSucessoClienteFlag")){
            helper.showToast('Ops!', 'Error', 'Please, fill out the Client success executive field on Commercial Details section.');
            return;
        }    
        if( helper.canGoNextStage(component) ){
            if(!helper.validateMainContactFields(component))return;               
           	if(!helper.validateEligFields(component))return;
            
            if (component.find("editOppForm") != undefined)
                component.find("editOppForm").submit(); 
        	
            if (component.find("editForm") != undefined) {
                component.find("editForm").forEach( form =>{
                    if (form != undefined)
                        form.submit();
                })
            }
             
            helper.callChangeStageEvent(component, 1);
        }         
    },
    setHasVendorPortal: function(component,event, helper){
        
        var selectValue= component.find('hasVendorPortal').get('v.value');
        console.log("event " + selectValue);
        if(selectValue == 'Yes'){
            component.set("v.hasVendorPortal", true);
        }
        else{
            component.set("v.hasVendorPortal", false);
        }
    },
    backStage : function(component, event, helper) {
        helper.callChangeStageEvent(component, -1);         
    },

    justSave : function(component, event, helper) {   
        if(!helper.validateMainContactFields(component))return;    
        component.set("v.spinnerOn", true);
        component.set('v.saveAndCallStage', false);        
        helper.saveForms(component, event, helper);
    },

    handleSuccessOppForm : function(component, event, helper) {       
        helper.showToast("Success!", "success", "Opportunity and Proposal saved successfully!");
        component.set("v.spinnerOn", false);
    },

    handleSuccessBillingSetting : function(component, event, helper){
        helper.showToast("Success!", "success", "Billing Settings saved successfully!");
        component.set("v.spinnerOn", false);
    },
    
    handleError : function(component, event, helper){
        helper.showToast("Error!", "error", "Complete required firelds");
        component.set("v.spinnerOn", false);
    },

    handleSubmit : function(component, event){
        component.set("v.spinnerOn", false);
    },

	save : function(component, event, helper) {
        var feeType = ''; 
        var drafValues = event.getParam('draftValues');
		var feeTypes = [];
        
        if (drafValues.length > 0) {
            if (drafValues[0].hasOwnProperty('Billing_Percentage__c'))                 		feeTypes.push('Billing_Percentage__c'); 
            if (drafValues[0].hasOwnProperty('Maintenance_Fee_Billing_Percentage__c')) 		feeTypes.push('Maintenance_Fee_Billing_Percentage__c');
            if (drafValues[0].hasOwnProperty('Setup_Fee_Billing_Percentage__c')) 			feeTypes.push('Setup_Fee_Billing_Percentage__c');
            if (drafValues[0].hasOwnProperty('Prof_Serv_Setup_Fee_Billing_Percentage__c')) 	feeTypes.push('Prof_Serv_Setup_Fee_Billing_Percentage__c');
        }        
		
        if (feeTypes.length > 0) {
            if( helper.isValidPercentages( component, helper, drafValues/*, feeTypes */) ){
                helper.saveAccOppRelationships( component , helper );
            }
        }        
    },

    createDealDeskOperationalCase : function(component, event, helper) {
        var actionAPI = component.find("quickActionAPI");
        var fields = helper.getDealDeskOperationalCaseFields(component);
        var args = {
            actionName: "Opportunity.New_Deal_Desk_Operational_Case_Custom_Reseller", 
            targetFields: fields 
        };
        
        actionAPI.setActionFieldValues(args).then(function(){})
            .catch(function(e){
                console.log("error selecting action");
                console.log(JSON.stringify(e.targetFieldErrors));
                console.error(e.errors);
            });
    },

    loadClientManagers: function (component, event, helper) {
        let callback = function (response) {
          console.log("load client managers response...");
          if (helper.isResponseSuccessful(response)) {
            component.set("v.clientManagers", response.getReturnValue());
          }
        };
        helper.loadClientManagers(component, callback, component.get("v.proposalCreationTO.opportunityId"));
      },

     saveForms: function(component, event, helper){         
        component.set("v.spinnerOn", true);
         if(!helper.validateEligFields(component)){
	         component.set("v.spinnerOn", false);
             return;
         }
        component.find("editForm").forEach( form =>{
            form.submit()
        }) 
     },

     setIsMainContactsSavedAndRefreshed : function(component) {
        var to = component.get("v.proposalCreationTO");
        if (to.oppRecTypeDevName == 'SMB_New_Business' || to.oppRecTypeDevName == 'SMB_Success_Renegotiation') {
            component.set("v.IsMainContactsSavedAndRefreshed", true);
        }
        else if ((to.proposal.sObj.Payroll_Debite_Memo_Contact__c!=null && to.proposal.sObj.Payroll_Debite_Memo_Contact__c!='') &&
            (to.proposal.sObj.Operations__c!=null && to.proposal.sObj.Operations__c != '') &&
            (to.proposal.sObj.Finance__c!= null && to.proposal.sObj.ContactId != '') &&
            !(to.oppRecTypeDevName == 'SMB_New_Business' || to.oppRecTypeDevName == 'SMB_Success_Renegotiation')) {
              component.set("v.IsMainContactsSavedAndRefreshed", true);
        } else {
          component.set("v.IsMainContactsSavedAndRefreshed", false);
        }
      },

      saveAndRefresh: function(component, event, helper){        
        if(!helper.validateMainContactFields(component))return;
        component.set("v.spinnerOn", true);
        helper.fireSaveAndRefreshOfferEvent(component);
      },

      saveOpportunityAndProposal: function(component, event, helper){
        if(!helper.validateMainContactFields(component))return;       
        component.set("v.spinnerOn", true);
        
        if (component.find("editOppForm") != undefined)
            component.find("editOppForm").submit(); 
        
        $A.enqueueAction(component.get("c.saveAndRefresh"));  
      },
      

      handleOfferChanges: function(component, event, helper){      
        if (  event.getParam("oldValue")==undefined ){           
          return;           
        }

        var to = component.get("v.proposalCreationTO");

        if (to.oppRecTypeDevName == 'SMB_New_Business' || to.oppRecTypeDevName == 'SMB_Success_Renegotiation') {
            component.set("v.IsMainContactsSavedAndRefreshed", false);
        }
        else if ( component.get('v.IsMainContactsSavedAndRefreshed') &&   ( event.getParam("oldValue") != event.getParam("value")) ){
          helper.showToast('Main Contacts Changed!', 'warning', 'Please, save your offer again.');
          component.set("v.IsMainContactsSavedAndRefreshed", false);
        }   
      }
})