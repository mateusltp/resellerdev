({
    doInit: function(component, event, helper) {
        var product = component.get("v.product");
        var proposalCreationTO = component.get("v.proposalCreationTO");

        if (product.sObj.Fee_Type__c && product.sObj.Fee_Type__c == 'Enterprise Subscription' ) {
            component.set("v.isES", true);
        }

        if (product.sObj.Fee_Type__c == 'Professional Services Setup Fee' || product.sObj.Fee_Type__c == 'Professional Services Maintenance Fee') {
            component.set("v.disableUnitPrice", false);
        }        

        if (proposalCreationTO.hasCopay == 'Yes') {
            component.set("v.hasCopay", true);
        } else {
            component.set("v.hasCopay", false);
        }
    },
    handleSuccess: function(component, event, helper) {
        helper.showToast('success', 'Success', 'The changes have been saved');
        //helper.fireRefreshMainFastTrackCmpEvent();
        component.set("v.spinnerOn", false);
        component.find('overlayLib').notifyClose();
        location.reload();
    },
    handleError: function(component, event, helper) {
        component.set("v.spinnerOn", false);
        helper.showToast('error', 'Error', 'The changes have not been saved');
    },
    handleSubmit: function(component, event, helper) {
        component.set("v.spinnerOn", true);
    },
    handleCancel: function(component, event, helper) {
        component.find('overlayLib').notifyClose();
    }
})