({
    doInit : function(component, event, helper) {
       
        component.set('v.columns', [
            {label: 'Account Name', initialWidth: 300, fieldName: 'linkName', type: 'url',
            typeAttributes: {label: { fieldName: 'Name' },value:{fieldName: 'linkName'}, target: '_blank'}
            },
            {label: 'City', fieldName: 'ShippingCity', type: 'text'},
            {label: 'State', fieldName: 'ShippingState', type: 'text'},
            {label: 'Country', fieldName: 'ShippingCountry', type: 'text'}
        ]);

        component.set('v.columnsAor', [
            {label: 'Account Name', initialWidth: 300, fieldName: 'linkName', type: 'url',
            typeAttributes: {label: { fieldName: 'Name' },value:{fieldName: 'linkName'}, target: '_blank'}
            },
            {label: 'City', fieldName: 'ShippingCity', type: 'text'},
            {label: 'State', fieldName: 'ShippingState', type: 'text'},
            {label: 'Country', fieldName: 'ShippingCountry', type: 'text'}
        ]);

       helper.setChildAccounts(component, event, helper);
       
    },

    onChangefilter: function(component, event, helper){
        helper.onChangeFilter(component, event, helper);
    },

    openPanel: function(component, event, helper){
        let isOpen = component.get("v.isOpen");
        component.set("v.isOpen", !isOpen);
        let filterField = component.find('aorFilterId');
        filterField.set('v.disabled',isOpen);

    },

    handleSubmit : function(component, event, helper) {       
        helper.handleSubmit(component, event, helper);        
    },

    handleRemove : function(component, event, helper) {       
        helper.handleRemove(component, event, helper);        
    }
})