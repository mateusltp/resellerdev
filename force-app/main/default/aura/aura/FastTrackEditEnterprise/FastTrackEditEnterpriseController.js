/**
 * @description       : 
 * @author            : 
 * @group             : 
 * @last modified on  : 04-13-2022
 * @last modified by  : Mateus Augusto - GFT (moes@gft.com)
**/
({
    init: function (component, event, helper) {
        console.log('proposalCreationTO');
        console.log(JSON.parse(JSON.stringify(component.get("v.proposalCreationTO"))));

        var qItem = component.get('v.quoteItem');
        component.set('v.listPriceCalc', (qItem.sObj.List_Price__c))
        helper.findComissionValue(component, event, helper);
        helper.calcTotalComission(component, event, helper);

    },

    openModalFees: function (component, event, helper) {
        component.set("v.showFees", true);
    },

    closeModal: function (component, event, helper) {
        component.set("v.showFees", false);

    },

    handleCancel: function (component, event, helper) {
        component.find('overlayLib').notifyClose();
    },

    totalComission: function (component, event, helper) {

        helper.calcTotalComission(component, helper);

    },

    handleSuccess: function (component, event, helper) {
        console.log('handleSuccess');
        helper.showToast('success', 'Success', 'The changes have been saved');
        // helper.fireFastTrackEvaluationRequestEvent(component, helper);
        //helper.fireRefreshMainFastTrackCmpEvent();
        component.find('overlayLib').notifyClose();
        location.reload();

    },

    handleError: function (component, event, helper) {
        console.log('handleError');
        helper.showToast('error', 'Error', 'The changes have not been saved');
        // helper.fireFastTrackEvaluationRequestEvent(component, helper);
        //helper.fireRefreshMainFastTrackCmpEvent();
        //component.find('overlayLib').notifyClose();

    },

    save: function (component, event, helper) {
        console.log('save');

        component.set('v.loaded', !component.get('v.loaded'));
        //component.set('v.disabledSave', true);

        var businessModel = JSON.parse(JSON.stringify(component.get("v.proposalCreationTO.businessModel")));
        if (component.get('v.valueComissionInitNew') && businessModel == 'Intermediation') {
            return helper.showToast('error', 'Error', 'Required Fields Missing');
        }

        var objQuoteItem = component.get("v.objQuoteItem");
        objQuoteItem.List_Price__c = component.find("listPrice").get("v.value");
        objQuoteItem.UnitPrice = component.get("v.valueSalesPrice");
        objQuoteItem.Fee_Contract_Type__c = component.find("feeContractType").get("v.value");
        //objQuoteItem.Payment_Method__c = component.find("paymentMethod").get("v.value");
        if (businessModel == 'Intermediation') {
            if (component.get('v.percentageAgencyFee') == null) {
                objQuoteItem.Is_there_an_agency_fee__c = component.get('v.percentageAgencyFee');
                console.log('isAgency ', objQuoteItem.Is_there_an_agency_fee__c);

                objQuoteItem.Agency_fee_frequency__c = component.get('v.quoteItem.Agency_fee_frequency__c');
                console.log('AgencyFeeFrequency ', objQuoteItem.Agency_fee_frequency__c);

                objQuoteItem.Percentage_of_agency_fee__c = component.get('v.quoteItem.Percentage_of_agency_fee__c');
                console.log('PercentageOfAgencyFee ', objQuoteItem.Percentage_of_agency_fee__c);


            } else {
                objQuoteItem.Is_there_an_agency_fee__c = component.find("isAgency").get("v.value");
                console.log('isAgency ', objQuoteItem.Is_there_an_agency_fee__c);

                objQuoteItem.Agency_fee_frequency__c = component.find("AgencyFeeFrequency").get("v.value");
                console.log('AgencyFeeFrequency ', objQuoteItem.Agency_fee_frequency__c);

                objQuoteItem.Percentage_of_agency_fee__c = component.find("PercentageOfAgencyFee").get("v.value");
                console.log('PercentageOfAgencyFee ', objQuoteItem.Percentage_of_agency_fee__c);

            }

        }
        console.log('console quoteitem ', JSON.parse(JSON.stringify(component.get("v.objQuoteItem"))));

        var objPayment = component.get("v.objPayment");
        objPayment.Payment_Method__c = component.find("paymentMethod").get("v.value");
        //objPayment.Frequency__c = component.find("frequency").get("v.value");
        console.log('console objPayment ', JSON.parse(JSON.stringify(component.get("v.objPayment"))));


        var objQuote = component.get("v.objQuote");
        objQuote.Comission_Percent__c = component.get("v.valueComission");
        objQuote.Percent_of_comission__c = component.get("v.valueComission");
        objQuote.Discount_Approval_Level__c = 0;
        console.log('console objQuote ', JSON.parse(JSON.stringify(component.get("v.objQuote"))));

        objQuoteItem = JSON.stringify(component.get("v.objQuoteItem"));
        objPayment = JSON.stringify(component.get("v.objPayment"));
        objQuote = JSON.stringify(component.get("v.objQuote"));

        var action = component.get("c.setModal");
        action.setParams({
            "objQuoteItem": objQuoteItem,
            "objPayment": objPayment,
            "objQuote": objQuote

        });

        action.setCallback(this, function (response) {
            var state = response.getState();
            let log;
            console.log('Callback ', state);
            if (state === "SUCCESS") {
                log = response.getReturnValue();
                $A.enqueueAction(component.get('c.handleSuccess'));
            } else {
                log = response.getError();
                console.log(log[0].message);
                console.log(log[0].stackTrace);
                $A.enqueueAction(component.get('c.handleError'));
                //component.set('v.disabledSave', false);
                component.set('v.loaded', true);
            }
        });

        $A.enqueueAction(action);
        //location.reload();
    },

    handleAgency: function (component, event, helper) {
        var agency = component.find("isAgency").get("v.value");
        var objQuoteItem = component.get("v.objQuoteItem");
        if (agency == 'Yes') {
            component.set("v.agencyFee", false);
            component.set("v.percentageAgencyFee", objQuoteItem.Percentage_of_agency_fee__c);
            component.set("v.frequencyAgencyFee", objQuoteItem.Agency_fee_frequency__c);
        } else {
            component.set("v.agencyFee", true);
            //component.find("AgencyFeeFrequency").set("v.value","");
            component.set("v.percentageAgencyFee", "");
            component.set("v.frequencyAgencyFee", "");
        }
    }

})