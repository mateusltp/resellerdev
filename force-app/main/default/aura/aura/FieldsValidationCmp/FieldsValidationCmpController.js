({
	doInit : function(component, event, helper) {
        var action = component.get("c.validateFields");
        
        debugger;
        var recordId = component.get("v.recordId");
        action.setParams({ objectId : recordId });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var validationErrors = response.getReturnValue().validationErrors;
                if (validationErrors.length > 0) {
               		component.set('v.errors', true);
                    component.set('v.validationErrors', validationErrors);
                } else {
                    component.set('v.errors', false);
                }
            }
            else if (state === "INCOMPLETE") {
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);					
	}
})