({
  doInit: function (cmp) {
    $A.enqueueAction(cmp.get("c.setOfferProgressPercent"));        
    $A.enqueueAction(cmp.get("c.setGoToOfferReview"));     
    $A.enqueueAction(cmp.get("c.setDisableEnablers"));     
    if ( cmp.get("v.proposalCreationTO.proposal.sObj.Id")==undefined )
      $A.enqueueAction(cmp.get("c.saveAndRefresh"));
  },

  refreshView: function(cmp, evt){   
    if ( evt.getParam("oldValue")!=undefined && evt.getParam("value")!="" && evt.getParam("oldValue")!=evt.getParam("value")){
      $A.enqueueAction(cmp.get("c.saveAndRefresh"));
    }
  },

  handleSectionToggle: function (cmp, event) {
    cmp.set("v.proposalCreationTO.fastTrackCurrentActiveStep", event.getParam("openSections")[
      event.getParam("openSections").length-1
      ]
    );
    let gympassEntities = cmp.get("v.gympassEntities");
   
    if (gympassEntities==undefined || gympassEntities.length==0 )
      $A.enqueueAction(cmp.get("c.loadGympassEntities"));
   
  },

  setOfferProgressPercent: function (cmp) {
    let offerProgressPercent = 0;
    let proposalCreationTO = cmp.get("v.proposalCreationTO");
    
    if ( proposalCreationTO.fastTrackStage=='Offer Creation' && proposalCreationTO.quoteId==null )
      offerProgressPercent = 45;
    
    if ( proposalCreationTO.fastTrackStage=='Offer Creation' && proposalCreationTO.quoteId!=null )
      offerProgressPercent = 70;

    if ( cmp.get("v.proposalCreationTO.fastTrackCurrentActiveStep")==undefined ||
         cmp.get("v.proposalCreationTO.fastTrackCurrentActiveStep")==""   ){
      cmp.set("v.activeSection", "stepProposalInformation");
      cmp.set("v.proposalCreationTO.fastTrackCurrentActiveStep", "stepProposalInformation");
    } else{
      cmp.set("v.activeSection", cmp.get("v.proposalCreationTO.fastTrackCurrentActiveStep"));
    }     

    cmp.set("v.offerProgressPercent", offerProgressPercent);  

    cmp.find("billingStreetInput").showHelpMessageIfInvalid();
    cmp.find("billingCityInput").showHelpMessageIfInvalid();
    cmp.find("billingStateInput").showHelpMessageIfInvalid();
    cmp.find("billingCountryInput").showHelpMessageIfInvalid();
    cmp.find("billingPostalCodeInput").showHelpMessageIfInvalid();

    cmp.find('requiredFields').forEach(
      function( field ){
        try {
          field.showHelpMessageIfInvalid();
        } catch( e ){}
      });
  },

  loadGympassEntities: function (component, event, helper) {
    let callback = function (response) {
      console.log("load gympassentities response...");
      if (helper.isResponseSuccessful(response)) {
        component.set("v.gympassEntities", response.getReturnValue());
      }
    };
    helper.loadGympassEntities(
      component,
      callback,
      component.get("v.recordId")
    );
  },

  loadCompetitors: function (component, event, helper) {
    let callback = function (response) {
      console.log("load competitors response...");
      if (helper.isResponseSuccessful(response)) {
        component.set("v.competitors", response.getReturnValue());
      }
    };
    helper.loadCompetitors(component, callback);
  },
  handleMultipleEntityClientEvent: function (component, event, helper) {
    let proposalCreationTO = component.get("v.proposalCreationTO");
    let oldTotalOfEmployees = proposalCreationTO.dealHierarchy.totalOfEmployees;
    if (proposalCreationTO.dealHierarchy) {
      proposalCreationTO.dealHierarchy.accountsInOpp = event.getParam(
        "accountsInOpp"
      );
      proposalCreationTO.dealHierarchy.totalOfEmployees = event.getParam(
        "totalOfEmployees"
      );
     
    }
  },
  saveAndRefresh: function(component, event, helper){   
    if ( component.get('v.proposalCreationTO.proposal.sObj.Access_Fee_Adjustment_Date__c')!=undefined &&
        component.get('v.proposalCreationTO.proposal.sObj.Access_Fee_Adjustment_Date__c').length!=undefined &&
        (component.get('v.proposalCreationTO.proposal.sObj.Access_Fee_Adjustment_Date__c').length>2  ||
        (component.get('v.proposalCreationTO.proposal.sObj.Access_Fee_Adjustment_Date__c')<1 &&
        component.get('v.proposalCreationTO.proposal.sObj.Access_Fee_Adjustment_Date__c')>31) )){
      helper.showToast('warning', 'Invalid value for Access Fee Adjustment Date', 'Inform a valid number from 1 to 31 - Deadline to submit Access Fee Adjustment Date updated.');
      return; 
    }
    if ( component.get('v.proposalCreationTO.proposal.sObj.Eligible_Employee_Database_deadline__c')!=undefined &&
        component.get('v.proposalCreationTO.proposal.sObj.Eligible_Employee_Database_deadline__c').length!=undefined  &&
        (component.get('v.proposalCreationTO.proposal.sObj.Eligible_Employee_Database_deadline__c').length>2  ||
        (component.get('v.proposalCreationTO.proposal.sObj.Eligible_Employee_Database_deadline__c')<1 &&
        component.get('v.proposalCreationTO.proposal.sObj.Eligible_Employee_Database_deadline__c')>31) )){
      helper.showToast('warning', 'Invalid value for Eligible Employee Database deadline', 'Inform a valid number from 1 to 31 - Deadline to submit Eligible Employee Database updated.');
      return; 
    }
    if ( component.get('v.proposalCreationTO.proposal.sObj.Will_the_MF_be_subsidized__c')=='Yes' && 
        ( component.get('v.proposalCreationTO.proposal.sObj.Subsidy_basis_eligible_or_enrolled__c')==undefined || 
          component.get('v.proposalCreationTO.proposal.sObj.Lenght_of_the_subsidy_period_months__c')==undefined || 
          component.get('v.proposalCreationTO.proposal.sObj.Total_amount_subsidized__c')==undefined || 
          component.get('v.proposalCreationTO.proposal.sObj.Why_should_we_accept_this_subsidy__c')==undefined ||
          component.get('v.proposalCreationTO.proposal.sObj.Subsidy_basis_eligible_or_enrolled__c')=='' || 
          component.get('v.proposalCreationTO.proposal.sObj.Lenght_of_the_subsidy_period_months__c')=='' || 
          component.get('v.proposalCreationTO.proposal.sObj.Total_amount_subsidized__c')=='' || 
          component.get('v.proposalCreationTO.proposal.sObj.Why_should_we_accept_this_subsidy__c')=='' ) ){
        helper.showToast('warning', 'Invalid Membership Fee Subsidized form', 'For MembershipFee subsidized, please fill all dependents fields');
        return; 
    }

    if ( (component.get('v.proposalCreationTO.doesItHaveUniqueTechRequest')==undefined || component.get('v.proposalCreationTO.doesItHaveUniqueTechRequest')=='') ||
          ( component.get('v.proposalCreationTO.doesItHaveUniqueTechRequest')=='Yes' &&
            ((component.get('v.proposalCreationTO.systemConnectivity')==null || component.get('v.proposalCreationTO.systemConnectivity')=='' ) ||
            (component.get('v.proposalCreationTO.payrollDeduction')==null || component.get('v.proposalCreationTO.payrollDeduction')=='' ) ||
            (component.get('v.proposalCreationTO.employeeRegistration')==null || component.get('v.proposalCreationTO.employeeRegistration')=='' ) ||
            (component.get('v.proposalCreationTO.communicationEmployee')==null || component.get('v.proposalCreationTO.communicationEmployee')=='') ||
            (component.get('v.proposalCreationTO.eligibilityExchange')==null || component.get('v.proposalCreationTO.eligibilityExchange')=='' )) ) ){
        helper.showToast('warning', 'Invalid Tech Sales form', 'For a Tech Request Offer, please fill all dependents fields');
        return; 
    }
  
    if ( isNaN(component.get('v.proposalCreationTO.adjustedProbability')) && !component.get('v.proposalCreationTO.oppRecTypeDevName').includes('SMB')){
      helper.showToast('warning', 'Invalid value for Adjust Probability', 'Must be numeric value');
      return; 
    } 
    if ( isNaN(component.get('v.proposalCreationTO.quantity')) ){
      helper.showToast('warning', 'Invalid value for Quantity', 'Must be numeric value');
      return; 
    } 
    if (  component.get('v.proposalCreationTO.adjustedProbability')<0 || component.get('v.proposalCreationTO.adjustedProbability')>100 )   {
      helper.showToast('warning', 'Invalid value for Adjust Probability', 'Cannot exceed 100%');
      return;
     }

     if(component.get('v.proposalCreationTO.AccessFee.sObj')!=undefined && component.get('v.proposalCreationTO.AccessFee.sObj.Sales_Total_Price__c')==''){
      helper.showToast('warning', 'Invalid Fee Price!', 'Please enter Enterprise Subscription Price. All Fees must have Sales Total Price');
      return;
     }

     var allValid = 
      component.find('requiredFields').reduce(function (validSoFar, inputCmp) {
        if( inputCmp.get('v.disabled') == true ){ return validSoFar; }

        return Boolean(
            validSoFar && ( inputCmp.get('v.value') || !isNaN(inputCmp.get('v.value')) ) );
      }, true);

     if( allValid == false ){
      helper.showToast('warning', 'Validate your proposal!', 'Required fields missing.');
      return;
     }
      helper.fireSaveAndRefreshOffer(component);  
      component.set('v.showDealHierarchy',true);     
  },
  
  handleOfferChanges: function(component, event, helper){
    if (  event.getParam("oldValue")==undefined )
      return;
      
    if ( component.get('v.goToOfferReview') && ( event.getParam("oldValue") != event.getParam("value") ) ){
      helper.showToast('warning', 'Proposal changes detected', 'You must save and validate your offer again');
      component.set('v.goToOfferReview',false);
      component.set('v.showDealHierarchy',false);
    }   

  },

  handleOfferChangesorderExpirationDays:function(component, evt, helper){
    if ( evt.getParam("oldValue")!=undefined && evt.getParam("value")!="" && evt.getParam("oldValue")!=evt.getParam("value") && component.get('v.goToOfferReview') ){
      helper.showToast('warning', 'Proposal changes detected', 'You must save and validate your offer again');
      component.set('v.goToOfferReview',false);
    }
  },
  
  handleOfferChangesCustomIntegrationsRequired: function(component, evt, helper){
    if ( evt.getParam("oldValue")!=undefined && evt.getParam("value")!="" && evt.getParam("oldValue")!=evt.getParam("value") &&  component.get('v.goToOfferReview') ){
      helper.showToast('warning', 'Proposal changes detected', 'You must save and validate your offer again');
     
    }
  },
  handleOfferChangesSetupFeeRequired: function(component, evt, helper){
    if ( evt.getParam("oldValue")!=undefined && evt.getParam("value")!="" && evt.getParam("oldValue")!=evt.getParam("value") &&  component.get('v.goToOfferReview') ){
      helper.showToast('warning', 'Proposal changes detected', 'You must save and validate your offer again');

    }
  },
  save: function (component, event, helper){
    helper.fireSaveOffer(component);
  },
  
  refreshEnablers: function(component, event, helper){
     helper.fireRefreshEnablers(component);   
  },
  
  callStage : function(component, event, helper) { 
    var cmpEvt = $A.get("e.c:RefreshMainFastTrackCmpEvent");
    cmpEvt.fire();      
    var eventSourceId = event.getSource().getLocalId(); 
    var cmpEvent = $A.get("e.c:fastTrackStageDoneEvent");
    var nextIndex = component.get("v.myStageIndex");     
    nextIndex += eventSourceId === "btnBack" ? -1 : 1;
       
    cmpEvent.setParams({
        "opportunityId": component.get("v.proposalCreationTO.opportunityId"),
        "nextIndex": nextIndex
    });
    cmpEvent.fire();
  },
  setGoToOfferReview : function(component, event, helper) {
    var to = component.get("v.proposalCreationTO");

    var allValid = 
      component.find('requiredFields').reduce(function (validSoFar, inputCmp) {
        if( inputCmp.get('v.disabled') == true ){ return validSoFar; }

        return Boolean(
            validSoFar && ( inputCmp.get('v.value') || !isNaN( inputCmp.get('v.value') ) ) );
      }, true);

    if( Boolean( 
        allValid && 
        ( to.customIntegrationsRequired && to.areDependentsIncluded ) &&
        ( ( to.doesItHaveUniqueTechRequest != 'Yes' ) ||
          ( to.doesItHaveUniqueTechRequest == 'Yes' &&
            to.systemConnectivity &&
            to.payrollDeduction &&
            to.employeeRegistration &&
            to.communicationEmployee &&
            to.eligibilityExchange ) ) 
        ) ){
      component.set("v.goToOfferReview", true);
    } else {
      component.set("v.goToOfferReview", false);
    }
  },

  onChangeTechSalesControllingField : function(component, event, helper){
    if ( event.getParam("value")!='Yes' ){  
      component.set('v.proposalCreationTO.communicationEmployee', '');
      component.set('v.proposalCreationTO.systemConnectivity', '');
      component.set('v.proposalCreationTO.payrollDeduction', '');
      component.set('v.proposalCreationTO.employeeRegistration', '');
      component.set('v.proposalCreationTO.eligibilityExchange', '');
    }
    
  },

  setDisableEnablers : function(cmp) {
    let oppRecTypedevName = cmp.get("v.proposalCreationTO.oppRecTypeDevName");
    
    if (oppRecTypedevName == 'SMB_New_Business' || oppRecTypedevName == 'SMB_Success_Renegotiation') {
      cmp.set('v.disableEnablers', true);
    }
  }
});