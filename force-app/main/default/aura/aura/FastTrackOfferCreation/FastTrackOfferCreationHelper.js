({
    loadGympassEntities: function(component, callback, recordId) {
        let gympassEntitiesService = component.get("c.findGympassEntities");
        gympassEntitiesService.setCallback(this, callback);        
        $A.enqueueAction(gympassEntitiesService);
    },

    loadCompetitors: function(component, callback) {
        let competitorsService = component.get("c.findCompetitors");
        competitorsService.setCallback(this, callback);        
        $A.enqueueAction(competitorsService);
    },
    fireSaveOffer : function(component)  {
        var cmpEvent = $A.get("e.c:saveOffer");
        cmpEvent.setParams({
            "changedProposalCreationTO": component.get("v.proposalCreationTO")
        });
        cmpEvent.fire();
    },
    fireSaveAndRefreshOffer : function(component)  {
        var cmpEvent = $A.get("e.c:saveAndRefreshOfferEvent");
        cmpEvent.setParams({
            "changedProposalCreationTO": component.get("v.proposalCreationTO")
        });        
        cmpEvent.fire();
    },
   
    isResponseSuccessful : function(response) {
        return (response.getState() === "SUCCESS");
    },

    fireRefreshMainFastTrackCmpEvent : function(component, helper) {
        var cmpEvt = $A.get("e.c:RefreshMainFastTrackCmpEvent");
		cmpEvt.fire();
    },

    showToast : function(type, title, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type": type
        });
        toastEvent.fire();
    }
})