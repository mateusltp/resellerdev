({
    loadData: function(component, helper) {
        var to = component.get("v.proposalCreationTO");                                       
        helper.loadAccessFee(component, helper, to);
        helper.loadSetupFee(component, helper, to);
        helper.loadProServicesOneFee(component, helper, to);
        helper.loadProServicesMainFee(component, helper, to);
        helper.loadApprovedDealDeskConditions( component, to );
    },
    loadAccessFee: function(component, helper, to) {
        debugger;
        if (to.proposal.accessFee.sObj != undefined && to.proposal.accessFee.sObj.Id != undefined) {
            helper.renderQuoteItemComponent(component, helper, 'accessFee', to.proposal.accessFee);                                           
        }    
    },
    loadSetupFee: function(component, helper, to) {
        if ( (to.proposal.setupFee.sObj != undefined && to.proposal.setupFee.sObj.Id != undefined) 
              && (to.oppRecTypeDevName != 'Indirect_Channel_New_Business' || (to.oppRecTypeDevName == 'Indirect_Channel_New_Business' && to.setupFeeRequired == 'Yes')) ){
            helper.renderQuoteItemComponent(component, helper, 'setupFee', to.proposal.setupFee);                                           
        }
    },
    loadProServicesOneFee: function(component, helper, to) {
        if (to.proposal.proServicesOneFee.sObj != undefined && to.proposal.proServicesOneFee.sObj.Id != undefined) {
            helper.renderQuoteItemComponent(component, helper, 'psOneFee', to.proposal.proServicesOneFee);                                           
        }
    },
    loadProServicesMainFee: function(component, helper, to) {
        if (to.proposal.proServicesMainFee.sObj != undefined && to.proposal.proServicesMainFee.sObj.Id != undefined) {
            helper.renderQuoteItemComponent(component, helper, 'psMainFee', to.proposal.proServicesMainFee);                                           
        }
    },
    showProductDetails: function(row, component) {
        $A.createComponent(
            "c:ProductItemModal", {
                "proposalCreationTO": component.get("v.proposalCreationTO"),
                "product": row
            },
            function(modal) {
                if (component.isValid()) {
                    var targetCmp = component.find('ModalDialogPlaceholder');
                    var body = targetCmp.get("v.body");
                    body.push(modal);
                    targetCmp.set("v.body", body);
                }
            }
        );
    },
    openOfferLetterPreview: function(component) {
        $A.createComponent(
            "c:OfferLetterPreview", {
                "proposalCreationTO": component.get("v.proposalCreationTO")
            },
            function(modal) {
                if (component.isValid()) {
                    component.find('overlayLib').showCustomModal({
						body: modal,
						showCloseButton: true
					})
                }
            }
        );
    },
    renderQuoteItemComponent : function(component, helper, itemDivName, quoteItem) {
        $A.createComponent(
            "c:OfferSummaryQuoteItem", {
                "proposalCreationTO": component.get("v.proposalCreationTO"),
                "quoteItem": quoteItem
            },
            function(childCmp, status, error) {
                if (component.isValid()) {
                    var targetCmp = component.find(itemDivName);
                    var body = targetCmp.get("v.body");
                    body.push(childCmp);
                    targetCmp.set("v.body", body);
                }
            }
        );    
    },

    loadApprovedDealDeskConditions: function(component, to) {
        var approvedConditions = to.proposal.sObj.Deal_Desk_Approved_Conditions__c;
        var approvedConditionsObj;
        
        if( approvedConditions ){
            approvedConditionsObj = JSON.parse( approvedConditions );

            component.set("v.dealDeskApprovedConditions", approvedConditionsObj);
        }    
    }
})