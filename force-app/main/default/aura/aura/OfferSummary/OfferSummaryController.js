({
    doInit: function(component, event, helper) {
        debugger;
        helper.loadData(component, helper);
    },

    handleRowAction: function(component, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');
        switch (action.name) {
            case 'view_details':
                helper.showProductDetails(row, component);
                break;
        }
    },

    closeProductItemModal: function(component, event, helper) {
        debugger;
        component.set("v.showProductItemModal", "false");
    },

    previewOfferLetter: function(component, event, helper) {
        debugger;
        helper.openOfferLetterPreview(component);
    }
})