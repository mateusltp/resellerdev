({
    handleSave: function(component, event, helper) {
        debugger;
        var action = component.get("c.generateOfferLetter");
        var recordId = component.get("v.recordId");
        action.setParams({ oppId: recordId });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                debugger;
                var returnValue = response.getReturnValue();

                if (returnValue.status == 'SUCCESS') {
                    helper.showToast('SUCCESS', returnValue.responseMessage, 'success');
                    $A.get("e.force:closeQuickAction").fire();
                } else {
                    helper.showToast('ERROR', returnValue.responseMessage, 'error');
                }

            } else if (state === "INCOMPLETE") {} else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                            errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });

        $A.enqueueAction(action);
    }
})