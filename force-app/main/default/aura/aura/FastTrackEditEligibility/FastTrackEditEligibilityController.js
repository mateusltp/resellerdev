({
    openModalAdd: function(component, event, helper) {
        component.set("v.showAdd", true);
    },

    openModalEdit: function(component, event, helper) {
        component.set("v.showEdit", true);
    },

    openModalRemove: function(component, event, helper) {
        component.set("v.showRemove", true);
    },

    closeModal: function(component, event, helper) {
        component.set("v.showAdd", false);
        component.set("v.showEdit", false);
        
    },

    save: function(component,event,helper) {
        component.find("editForm").submit();
        component.set("v.showAdd", false);
    },


    handleSuccess: function(component, event, helper) {
		helper.showToast('success', 'Success', 'The changes have been saved');
        helper.fireRefreshMainFastTrackCmpEvent();
		component.find('overlayLib').notifyClose();
    },
    
    handleCancel: function(component, event, helper) {
        component.find('overlayLib').notifyClose();
    }
})