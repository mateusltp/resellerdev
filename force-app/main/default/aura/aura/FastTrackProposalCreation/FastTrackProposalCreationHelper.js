({
    load: function(component, recordId, helper) {
        return new Promise(function(resolve, reject){
            let callback = function (response) {
                if (helper.isResponseSuccessful(response)) {                    
                    component.set("v.proposalCreationTO", response.getReturnValue());
                    if ( response.getReturnValue().proposal.sObj.Id==null ){
                        var cmpEvent = $A.get("e.c:saveOffer");
                        cmpEvent.setParams({
                            "changedProposalCreationTO": component.get("v.proposalCreationTO")
                        });
                        cmpEvent.fire();
                    }
                    console.log("proposalCreationTO " +  response.getReturnValue());
                    console.dir(response.getReturnValue());
                    helper.fireRefreshEnablers(component);
                    resolve();
                } else {
                    reject("Error while loading this component. Contact your administrator");
                }                
                $A.get('e.force:refreshView').fire();
                component.set("v.spinnerOn", false);
            };
            let loadService = component.get("c.find");
            loadService.setParams({
                'recordId': recordId
            });
            loadService.setCallback(this, callback);        
            $A.enqueueAction(loadService);
        });
    },
    
    loadGympassEntities: function(component, callback, recordId) {
        let gympassEntitiesService = component.get("c.findGympassEntities");
        gympassEntitiesService.setCallback(this, callback);        
        $A.enqueueAction(gympassEntitiesService);
    },

    loadClientManagers: function(component, callback, recordId) {
        let clientManagersService = component.get("c.findClientManagers");
        clientManagersService.setParams({
            'recordId': recordId
        });        
        clientManagersService.setCallback(this, callback);        
        $A.enqueueAction(clientManagersService);
    },
    
    renderFastTrackStageCmp: function(component) {
        let cmpInfo = this.getCmpInformation(component);
        let _self = this;
        $A.createComponent(
            cmpInfo.cmpName,
            cmpInfo.params,
            function(newComponent, status, errorMessage){
                if (status === "SUCCESS") {
                    var body = component.get("v.body");
                    body = newComponent;
                    component.set("v.body", body);
                }
                else if (status === "INCOMPLETE") {
                    _self.showToast("error", "Error!", "No response from server or client is offline.");
                }
                else if (status === "ERROR") {
                    _self.showToast("error", "Error!", errorMessage);
                }
            }
        );
        component.set("v.spinnerOn", false);
    },

    renderEnablersCmp: function(component) {
        $A.createComponent(
            "c:FastTrackEnablers", {
                "proposalCreationTO": component.get("v.proposalCreationTO")
            },
            function(childComponent) {
                if (component.isValid()) {
                    debugger;
                    var targetCmp = component.find('enablersCmp');
                    var body = targetCmp.get("v.body");
                    body.push(childComponent);
                    targetCmp.set("v.body", body);
                }
            }
        );
    },

    getStageCmpMappingMetadata: function(component, recordId, helper) {
        return new Promise(function(resolve, reject){
                let callback = function (response) {
                    if (helper.isResponseSuccessful(response)) {
                        component.set("v.fastTrackStageMapping", response.getReturnValue());
                        resolve();
                    } else {
                        reject('Error finding configuration metadata. Contact your administrator!');
                    }
                };

                let loadService = component.get("c.getStageCmpMappingMetadata");
                loadService.setParams({
                    'recordId': recordId
                }); 
                loadService.setCallback(this, callback);        
                $A.enqueueAction(loadService);
            });
    },

    getCmpInformation : function(component) {
        var cmpInfo = {
            cmpName : "lightning:formattedText",
            params : {
                class : "slds-align_absolute-center slds-text-heading_small",
                value : $A.get("$Label.c.FastTrackNoPageFound")
            }
        };
        let proposalCreationTO = component.get("v.proposalCreationTO");
        let lstStageMapping = component.get("v.fastTrackStageMapping");

        if(proposalCreationTO && proposalCreationTO.fastTrackStage && lstStageMapping && lstStageMapping.length) {

            let stageCmpMapped = lstStageMapping.find(element => element.Label == proposalCreationTO.fastTrackStage);

            if( !stageCmpMapped ) { return cmpInfo; }

            cmpInfo = { "cmpName" : "c:" + stageCmpMapped.ChildComponentApiName__c };

            if( !stageCmpMapped.ChildComponentParameters__c ) { return cmpInfo; }

            let createParams = {};
            let cmpParameters = stageCmpMapped.ChildComponentParameters__c.split(',');

            cmpParameters.forEach(
                function(param) {
                    param = param.split(':');
                    if(param[0]) { createParams[param[0]] = component.get("v." + (param.length > 1 ? param[1] : param[0]) ); }
                });
            
            cmpInfo.params = createParams;
        }

        return cmpInfo;
    },

    showToast : function(type, title, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type": type
        });
        toastEvent.fire();
    },

    save: function(component, callback, recordId, proposal) {        
        let saveService = component.get("c.saveProposal");
        var newAccountsInOpp = null;
        
        saveService.setParams({
            'proposalData': JSON.stringify(proposal)
        });  

        saveService.setCallback(this, callback);        
        $A.enqueueAction(saveService);
    },
    
    isResponseSuccessful : function(response) {
        return (response.getState() === "SUCCESS");
    },
    
     fireRefreshEnablers : function(component)  {
        var cmpEvent = $A.get("e.c:reloadEnablersEvent");
        cmpEvent.setParams({
            "proposalCreationTO": component.get("v.proposalCreationTO")
        });
        cmpEvent.fire();
    },

    resetCommercialConditions : function(component, record, helper){       
        let action = component.get("c.resetCommercialConditions");
        action.setParams({
            'to': component.get("v.proposalCreationTO")
        });        
        action.setCallback(this, function(response){
            var state = response.getState();                         
            if (state === "SUCCESS") {      
                this.load(component,record, helper);
            }else{
                component.set("v.spinnerOn", true);
                return;
            }
          });
        component.set("v.spinnerOn", true);
        $A.enqueueAction(action);
       
    }
})