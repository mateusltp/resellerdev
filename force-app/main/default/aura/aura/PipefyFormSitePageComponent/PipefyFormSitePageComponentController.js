({
	doInit : function(component, event, helper) {
        
        var action = component.get("c.getInfo");
        action.setParams({oppId:component.get("v.recordId")});
        action.setCallback(this, function(response){
            component.set("v.opp", response.getReturnValue().opp);
            component.set("v.accountList", response.getReturnValue().accList);
            
        });
        $A.enqueueAction(action);
    },
    
    handleSave: function(component, event, helper){
        var inputList = component.find('inputCrmId');
        var IdToCrmIdList =[];
        if(Array.isArray(inputList)){
            inputList.forEach(function(o){ 
                var mapIdToCrmId ={
                    accId : o.get("v.name"),
                    crmId : o.get("v.value")
                }
                return IdToCrmIdList.push(mapIdToCrmId); 
            });
        }else{
            var mapIdToCrmId ={
                accId : inputList.get("v.name"),
                crmId : inputList.get("v.value")
            }
            IdToCrmIdList.push(mapIdToCrmId);
        }
        
        
        
        var action = component.get("c.saveAccounts");
        action.setParams({ idToCrmIdListStringified : JSON.stringify(IdToCrmIdList)});
        action.setCallback(this, function(response){
            if(response.getState() === 'SUCCESS'){
                alert('Successfully saved.');
            }
            
        });
        $A.enqueueAction(action);
       
    }
})