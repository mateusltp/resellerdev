({
	 handleErrors: function (errors, helper) {
        var message = "Unknown error";
        if (errors && Array.isArray(errors) && errors.length > 0) {
            message = errors[0].message;
        }
        helper.showToast('error', 'Error', message);
    },

    showToast: function (type, title, message) {
        let toastParams = {
            title: title,
            message: message,
            type: type
        };
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams(toastParams);
        toastEvent.fire();
    }
})