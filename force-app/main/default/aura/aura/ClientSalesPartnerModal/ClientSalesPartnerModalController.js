({
    openModel: function(component, event, helper) {
        component.set("v.isModalOpen", true);
        component.set('v.showNewRequest', true);
    },
    
    closeModel: function(component, event, helper) {
        component.set("v.isModalOpen", false);
    },
    
    submitDetails: function(component, event, helper) {
        component.set("v.isModalOpen", false);
    },
    searchAgain : function(component, event, helper) {
        component.set("v.isModalOpen", false);
        component.set("v.isModalOpen", true);
    }
})