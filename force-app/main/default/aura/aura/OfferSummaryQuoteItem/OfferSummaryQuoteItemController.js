/**
 * @description       : 
 * @author            : AlyssonMota
 * @group             : 
 * @last modified on  : 03-22-2022
 * @last modified by  : Mateus Augusto - GFT (moes@gft.com)
**/
({
	doInit : function(component, event, helper) {
		var quoteItem = component.get("v.quoteItem");
		helper.setAttributes(component, helper, quoteItem);
		helper.setPayments(component, helper, quoteItem);
	},
	handleSelect : function(component, event, helper) {
		var selectedMenuItemValue = event.getParam("value");
		var product = component.get("v.quoteItem");
		
		if (selectedMenuItemValue == "ItemDetails") {
			helper.openProductItemModal(component, helper, product);
		} else if (selectedMenuItemValue == "AddPayment") {
			helper.openAddPayment(component, helper, product);																			
		}
	},
	showDetails : function(component, event, helper) {
		var product = component.get("v.quoteItem");
		helper.openProductItemModal(component, helper, product);
	},
	addPayment : function(component, event, helper) {
		var product = component.get("v.quoteItem");
		helper.openAddPayment(component, helper, product);
	},
	addWaiver : function(component, event, helper) {
		var product = component.get("v.quoteItem");
		helper.openAddWaiver(component, helper, product);
	},
	Editfees : function(component, event, helper) {
		var product = component.get("v.quoteItem");
		//var payment = component.get("v.paymentId");
		helper.openEditfees(component, helper, product);
	}
})