<!--
  @description       : 
  @author            : Mateus Augusto - GFT (moes@gft.com)  
  @group             : 
  @last modified on  : 05-17-2022
  @last modified by  : Mateus Augusto - GFT (moes@gft.com)
-->
<aura:component controller="NewDealDeskOperationalCaseController" implements="force:lightningQuickActionWithoutHeader,force:hasRecordId,flexipage:availableForAllPageTypes,flexipage:availableForRecordHome" access="global">
    
    <aura:attribute name="recordId" type="String" />
    <aura:attribute name="spinner" type="boolean"/>
    <aura:attribute name="showLabels" type="boolean" default = "false"/>
    <aura:attribute name="displayError" type="boolean" default = "false"/>
    <aura:attribute name="displayErrorMessage" type="String" />
    <aura:attribute name="disableButton" type="boolean"/>
    <aura:attribute name="title" type="String" default="Create a case"/>
    <aura:attribute name="dealDeskOperationalQueueName" type="String" default="Deal_Desk_Operational"/>
    <aura:attribute name="dealDeskOperationalQueueId" type="String" />
    <aura:attribute name="caseRecordTypeName" type="String" default="Deal Desk Operational"/>
    <aura:attribute name="caseRecordTypeId" type="String" />
    <aura:attribute name="defaultQuoteId" type="String" />
    <aura:attribute name="membershipFeePaymentMethod" type="String" />
    <aura:attribute name="preDefaultValuesByName" type="Map" />
    <aura:attribute name="quoteLineItems" type="List" />
    <aura:attribute name="hasPSConfigurationFee" type="boolean" />
    <aura:attribute name="hasPSMaintenanceFee" type="boolean"/>
    <aura:attribute name="hasSetupFee" type="boolean"/>
    <aura:attribute name="activeSections" type="List" default="['Eligible','Enterprise', 'Setup', 'Professional', 'PS2', 'Subscription', 'Complementary' ]" />
    <aura:attribute name="businessModel" type="String" />
    
    <aura:attribute name="messageWhenLoading" type="String" default="Please wait..."/>

    <aura:handler name="init" value="{!this}" action="{!c.doInit}"/>
    
    <section role="dialog" tabindex="-1" class="slds-modal slds-fade-in-open slds-modal_medium" aria-labelledby="modal-heading-01" aria-modal="true" aria-describedby="modal-content-id-1">
        <div class="slds-modal__container">
            <header class="slds-modal__header">
                <h2 id="modal-heading-01" class="slds-modal__title slds-hyphenate">{!v.title} </h2>
            </header>
            <aura:if isTrue="{!v.spinner}">
                <div class="slds-spinner_container">
                    <div role="status" class="slds-spinner slds-spinner_medium">
                        <span class="slds-assistive-text">Loading</span>
                        <div class="slds-spinner__dot-a"></div>
                        <div class="slds-spinner__dot-b"></div>
                      </div>
                    </div>
            </aura:if>
            <aura:if isTrue="{!v.displayError}">
                <div class="slds-notify slds-notify_alert slds-alert_error" role="alert">
                    <span class="slds-assistive-text">error</span>
                    <h2>{!v.displayErrorMessage}  </h2>
                    <div class="slds-notify__close">
                    <button class="slds-button slds-button_icon slds-button_icon-small slds-button_icon-inverse" title="Close">
                        <span class="slds-assistive-text">Close</span>
                    </button>
                    </div>
                </div>
            </aura:if>

            
            <div class="slds-modal__content slds-p-around_medium slds-is-relative" id="modal-content-id-1" >
                <lightning:recordEditForm 
                    aura:id="caseForm" 
                    recordTypeId="{!v.caseRecordTypeId}" 
                    objectApiName="Case"
                    onerror="{!c.handleError}"
                    onload="{!c.handleLoad}"
                    onsuccess="{!c.handleSuccess}" >
                    <lightning:messages />

                    <lightning:accordion allowMultipleSectionsOpen="true" activeSectionName="{! v.activeSections }" >

                        <lightning:accordionSection name="Eligible" label="Eligible list and file"  class="border">
                            <div class="slds-grid slds-wrap" >
                                <div class="slds-col slds-size_1-of-2 slds-p-right_x-small slds-m-bottom_small">
                                    <lightning:inputField fieldName="Eligible_List_Registration_Method__c" 
                                                            value='{!v.preDefaultValuesByName.eligible_list_registration_method__c }' 
                                                            aura:id='eligible_list_registration_method__c'
                                                            required="true"
                                                            onchange= "{!c.handleChangeEligibleListRegistrationMethod}"/> 
                                </div>
                                <div class="slds-col slds-size_1-of-2 slds-p-left_x-small slds-m-bottom_small">
                                    <lightning:inputField fieldName="Eligible_File_Management__c"   
                                                            value='{!v.preDefaultValuesByName.eligible_file_management__c}' 
                                                            aura:id='eligible_file_management__c'
                                                            required="true" /> 
                                </div>

                                <div class="slds-col slds-size_1-of-2 slds-p-right_x-small slds-m-bottom_large">
                                    <lightning:inputField fieldName="Eligible_File_Update_Frequency__c"    
                                                            value='{!v.preDefaultValuesByName.eligible_file_update_frequency__c}' 
                                                            aura:id='eligible_file_update_frequency__c' 
                                                            required="true" 
                                                            onchange="{!c.handleCustomField}"/> 
                                </div>
                                <div class="slds-col slds-size_1-of-2 slds-p-left_x-small slds-m-bottom_large">
                                    <lightning:inputField fieldName="Deadline_for_sending_Eligible_File__c"  
                                                            value='{!v.preDefaultValuesByName.deadline_for_sending_eligible_file__c}' 
                                                            aura:id='deadline_for_sending_eligible_file__c'
                                                            required="true"   
                                                            onchange="{!c.handleCustomField}"/> 
                                </div>
                            </div>
                        </lightning:accordionSection>

                        <lightning:accordionSection name="Enterprise" label="Enterprise subscription"  class="border">

                            <div class="slds-grid">
                                <div class="slds-col slds-size_1-of-2 slds-p-right_x-small slds-m-bottom_large">
                                    <lightning:inputField fieldName="ES_Billing_Day__c" 
                                                            value='{!v.preDefaultValuesByName.es_billing_day__c}' 
                                                            aura:id='es_billing_day__c'
                                                            required="true"
                                                            onchange="{!c.handleChangeESBillingDay}"  />
                                </div>
                                <div class="slds-col slds-size_1-of-2 slds-p-left_x-small slds-m-bottom_large">
                                    <lightning:inputField fieldName="ES_Payment_Due_Days__c" 
                                                            value='{!v.preDefaultValuesByName.es_payment_due_days__c}' 
                                                            aura:id='es_payment_due_days__c'
                                                            required="true" 
                                                            onchange="{!c.handleChangeESPaymentDueDays}"/>  
                                </div>
                            </div>
                        </lightning:accordionSection>

                        <aura:if isTrue= "{!v.hasSetupFee}">
                            <lightning:accordionSection name="Subscription" label="Setup fee"  class="border">   
                                <div class="slds-grid">
                                    <div class="slds-col slds-size_1-of-2 slds-p-right_x-small slds-m-bottom_large">
                                        <lightning:inputField fieldName="Setup_Fee_Billing_Day__c" 
                                                                value='{!v.preDefaultValuesByName.setup_fee_billing_day__c}' 
                                                                aura:id='Setup_fee_billing_day__c'   
                                                                onchange="{!c.handleCustomField}"/>
                                    </div>

                                    <div class="slds-col slds-size_1-of-2 slds-p-left_x-small slds-m-bottom_large">
                                        <lightning:inputField fieldName="Setup_Fee_Payment_Due_Days__c" 
                                                                value='{!v.preDefaultValuesByName.setup_fee_payment_due_days__c}' 
                                                                aura:id='Setup_fee_payment_due_days__c'    
                                                                onchange="{!c.handleCustomField}"/>
                                    </div>
                                </div>
                            </lightning:accordionSection>
                        </aura:if>

                        <aura:if isTrue= "{!v.hasPSConfigurationFee}">
                            <lightning:accordionSection name="Subscription" label="Professional services" class="border">   
                                <div class="slds-grid">
                                    <div class="slds-col slds-size_1-of-2 slds-p-right_x-small slds-m-bottom_large">

                                        <aura:if isTrue="{!v.showLabels}">
                                            <label for="prof_services_one_fee_billing_day__c" class="slds-form-element__label slds-no-flex">*PS Configuration Fee Billing Day</label>
                                        </aura:if>
                                        <lightning:inputField fieldName="Prof_Services_One_Fee_Billing_Day__c"  
                                                                value='{!v.preDefaultValuesByName.prof_services_one_fee_billing_day__c}' 
                                                                aura:id='prof_services_one_fee_billing_day__c'   
                                                                variant="label-hidden"
                                                                onchange="{!c.handleCustomField}"/>
                                    </div>

                                    <div class="slds-col slds-size_1-of-2 slds-p-left_x-small slds-m-bottom_large">
                                        <aura:if isTrue="{!v.showLabels}">
                                            <label for="prof_services_one_fee_payment_due_days__c" class="slds-form-element__label slds-no-flex">*PS Configuration Fee Due Days</label>
                                        </aura:if>
                                        <lightning:inputField fieldName="Prof_Services_One_Fee_Payment_Due_Days__c"
                                                                value='{!v.preDefaultValuesByName.prof_services_one_fee_payment_due_days__c}' 
                                                                aura:id='prof_services_one_fee_payment_due_days__c'  
                                                                variant="label-hidden"  
                                                                onchange="{!c.handleCustomField}"/>
                                    </div>
                                </div>
                            </lightning:accordionSection>
                        </aura:if>
                        
                        <aura:if isTrue= "{!v.hasPSMaintenanceFee}">
                            <lightning:accordionSection name="Enterprise" label="Professional services maintenance" class="border">
                                
                                <div class="slds-grid">
                                    <div class="slds-col slds-size_1-of-2 slds-p-right_x-small slds-m-bottom_large">
                                        <aura:if isTrue = "{!v.hasPSMaintenanceFee}">
                                            <lightning:helptext content="Same value from ES." />
                                        </aura:if>
                                        <lightning:inputField fieldName="Prof_Services_Main_Fee_Billing_Day__c"
                                                                value='{!v.preDefaultValuesByName.prof_services_main_fee_billing_day__c}' 
                                                                aura:id='prof_services_main_fee_billing_day__c'   />
                                    </div>

                                    <div class="slds-col slds-size_1-of-2 slds-p-left_x-small slds-m-bottom_large">
                                        <aura:if isTrue = "{!v.hasPSMaintenanceFee}">
                                            <lightning:helptext content="Same value from ES." />
                                        </aura:if>
                                        <lightning:inputField fieldName="Prof_Services_Main_Fee_Payment_Due_Days__c" 
                                                                value='{!v.preDefaultValuesByName.prof_services_main_fee_payment_due_days__c}' 
                                                                aura:id='prof_services_main_fee_payment_due_days__c'   />
                                    </div>
                                </div> 
                            </lightning:accordionSection> 
                        </aura:if>

                        <lightning:accordionSection name="Subscription" label="Subscription fee"  class="border">
                            <div class="slds-grid">
                                <div class="slds-col slds-size_1-of-2 slds-p-right_x-small slds-m-bottom_large">
                                    <lightning:inputField fieldName="MF_Eligibility_ES_Billing_Day__c" 
                                                            value='{!v.preDefaultValuesByName.mf_eligibility_es_billing_day__c}' 
                                                            aura:id='mf_eligibility_es_billing_day__c' 
                                                            required="true" 
                                                            onchange="{!c.handleCustomField}"/>
                                </div>

                                <div class="slds-col slds-size_1-of-2 slds-p-left_x-small slds-m-bottom_large">
                                    <lightning:inputField fieldName="MF_Eligibility_ES_Payment_Due_Days__c" 
                                                            value='{!v.preDefaultValuesByName.mf_eligibility_es_payment_due_days__c}' 
                                                            aura:id='mf_eligibility_es_payment_due_days__c'
                                                            required="true" 
                                                            onchange="{!c.handleCustomField}"/>
                                </div>

                            </div>
                        </lightning:accordionSection>

                        <lightning:accordionSection name="Complementary" label="Complementary information"  class="border">
                            <div class="slds-grid slds-wrap">
                                <div class="slds-col slds-size_1-of-1">
                                    <lightning:inputField fieldName="Justificate_No_Compliant_Topics__c"
                                                            value='{!v.preDefaultValuesByName.justificate_no_compliant_topics__c}' 
                                                            aura:id='justificate_no_compliant_topics__c'   />
                                </div>
                        
                                <div class="slds-col slds-size_1-of-2 slds-p-right_x-small">
                                    <lightning:inputField fieldName="Subject" value = "Deal Desk Operational Approval Request"  />
                                </div>

                                <div class="slds-col slds-size_1-of-2 slds-p-left_x-small">
                                    <lightning:inputField fieldName="OwnerId" aura:id='ownerId' />
                                </div>

                                <div class="slds-col slds-size_1-of-2 slds-p-right_x-small slds-m-bottom_large">
                                    <lightning:inputField fieldName="OpportunityId__c" value="{!v.recordId}" />
                                </div>

                                <div class="slds-col slds-size_1-of-2 slds-p-left_x-small slds-m-bottom_large">
                                    <lightning:inputField fieldName="QuoteId__c" value="{!v.defaultQuoteId}"  />
                                </div>
                            </div>
                        </lightning:accordionSection>

                    </lightning:accordion>

                </lightning:recordEditForm>
            </div>

            <footer class="slds-modal__footer">
                <lightning:button label="Cancel" title="Cancel" onclick="{!c.handleCancel}" />
                <lightning:button variant="brand" label="Save" title="Save" onclick="{!c.handleSave}" disabled="{!v.disableButton}"/>
            </footer>
        </div>
    </section>
</aura:component>