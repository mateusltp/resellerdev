({
    doInit: function (cmp, event, helper) {
        //var thisAccountProspectingId = cmp.get("v.recordId");
        var thisAccountProspectingId = cmp.get("v.accPropectingId");
        //var spinner = cmp.find("mySpinner");
        cmp.set('v.loaded', 'false');
        var accountsToReqHeader = cmp.find("accountsToReqHeader");
        
        debugger;
        var columns = [
            {
                type: 'text',
                fieldName: 'name',
                label: $A.get("$Label.c.List_header_name")
            },
            {
                type: 'text',
                fieldName: 'legalName',
                label: $A.get("$Label.c.List_header_legal_number")
            },
            {
                type: 'text',
                fieldName: 'legalNumber',
                label: $A.get("$Label.c.List_header_legal_name")
            }
        ];
        cmp.set('v.gridColumns', columns);

        var getAccountProspecting = cmp.get("c.getAccountRequestInfo");
        getAccountProspecting.setParams({ accountProspectingId: thisAccountProspectingId });

        getAccountProspecting.setCallback(this, function (getAccountProspectingResponse) {
            debugger;
            var getAccountProspectingState = getAccountProspectingResponse.getState();
            if (getAccountProspectingState === "SUCCESS") {
                var accountProspectingValue = getAccountProspectingResponse.getReturnValue();

                if (accountProspectingValue.accountProspecting.Status__c == 'New') {
                    var searchAccounts = cmp.get("c.searchAccounts");
                    searchAccounts.setParams({ accountProspectingId: thisAccountProspectingId });

                    searchAccounts.setCallback(this, function (searchAccountsResponse) {
                        if (searchAccountsResponse.getState() == 'SUCCESS') {
                            var accounts = searchAccountsResponse.getReturnValue();
                            if (accounts.length > 0) {
                                debugger;
                                cmp.set('v.gridData', accounts);
                                cmp.set('v.thereAreAccounts', 'true');
                                cmp.set('v.loaded', 'true');
                            } else {
                                debugger;
                                helper.getMetadata(cmp);
                                
                                var website = accountProspectingValue.accountProspecting.Website__c;
                                
                                if (website != '' && website != null) {
                                    cmp.set('v.website', website);
                                	cmp.set('v.websiteInputReadOnly', 'true');
                                }
                                
                            }
                        } else {
                            console.log("Search Accounts ERROR");
                            cmp.set('v.loaded', 'true');
                        }
                    });
                    $A.enqueueAction(searchAccounts);
                } else if (accountProspectingValue.accountProspecting.Status__c == 'Go') {
                    cmp.set('v.isAccountProspectStatusNew', 'false');
                    cmp.set('v.isGo', 'true');
                    cmp.set('v.prospectStatusMessage', accountProspecting.prospectStatusMessage);
                    if (accountProspecting.opportunityId != '') {
                        cmp.set('v.showLinkOpp', 'true');
                        cmp.set('v.oppUrl', '/partner/s/detail/'+accountProspecting.opportunityId);
                    }
                } else if (accountProspectingValue.accountProspecting.Status__c == 'No Go') {
                    cmp.set('v.isAccountProspectStatusNew', 'false');
                    cmp.set('v.isNoGo', 'true');
                    cmp.set('v.prospectStatusMessage', accountProspecting.prospectStatusMessage);
                } else if (accountProspectingValue.accountProspecting.Status__c == 'Needs Gympass Approval') {
                    cmp.set('v.isAccountProspectStatusNew', 'false');
                    cmp.set('v.prospectStatusMessage', accountProspecting.prospectStatusMessage);
                }
            }
            else {
                console.log("Get Account Prospecting ERROR");
                cmp.set('v.loaded', 'true');
            }
        });

        $A.enqueueAction(getAccountProspecting);
    },

    requestSelectedAccount: function (cmp, event, helper) {
        debugger;
        //var thisAccountProspectingId = cmp.get("v.recordId");
        var thisAccountProspectingId = cmp.get("v.accPropectingId");
        var treeGrid = cmp.find('mytree');
        var selectedRows = treeGrid.getSelectedRows();

        if (selectedRows.length == 0) {
            helper.showToast($A.get("$Label.c.Error_Title"), $A.get("$Label.c.Select_an_account_error"), 'error');
        }
        else if (selectedRows.length == 1) {
            var accountId = selectedRows[0].accountId;
            var prospectAccount = cmp.get("c.prospectAccount");
            var language = helper.getUrlParameter('language');
            
            prospectAccount.setParams({accountProspectingId: thisAccountProspectingId,  accountId: accountId, language: language});

            prospectAccount.setCallback(this, function (prospectAccountResponse) {
                var prospectAccountState = prospectAccountResponse.getState();
                if (prospectAccountState === "SUCCESS") {
                    var response = prospectAccountResponse.getReturnValue();
                    
                    if (response.status == 'SUCCESS') {
                        if (response.accountProspectingStatus == 'Go') {
                            debugger;
                            cmp.set('v.isAccountProspectStatusNew', 'false');
                            cmp.set('v.isGo', 'true');
                            cmp.set('v.sellerName', response.sellerName);
                        	cmp.set('v.accountName', response.accountName);
                        	cmp.set('v.partnerModel', response.partnerModel);
                        	cmp.set('v.gympassContactEmail', response.gympassContactEmail);
                        	cmp.set('v.prospectResultMessage', response.successMessage);
                            cmp.set('v.oppUrl', '/partner/s/detail/'+response.opportunityId);
                        	cmp.set('v.loaded', 'true');
                        } else if (response.accountProspectingStatus == 'No Go') {
                            cmp.set('v.isAccountProspectStatusNew', 'false');
                            cmp.set('v.isNoGo', 'true');
                            cmp.set('v.sellerName', response.sellerName);
                        	cmp.set('v.accountName', response.accountName);
                        	cmp.set('v.partnerModel',response.partnerModel);
                        	cmp.set('v.gympassContactEmail', response.gympassContactEmail);
                        	cmp.set('v.prospectResultMessage', response.successMessage);
                            
                            if (response.oppValidateDays != undefined) {
                           		cmp.set('v.oppValidateDays', response.oppValidateDays);
                                cmp.set('v.hasValidateDays', 'true');
                            }
                            
                        } else if (response.accountProspectingStatus == 'Needs Gympass Approval') {
                            cmp.set('v.isAccountProspectStatusNew', 'false');
                            cmp.set('v.prospectStatusMessage', response.successMessage);
                        }
                    }
                    if (response.status == 'ERROR') {
                        helper.showToast($A.get("$Label.c.Exception_Title"), response.errorMessage, 'error');
                    }
                }
                else {
                    console.log("Prospect Account ERROR");
                }
            });

            $A.enqueueAction(prospectAccount);
        }
        else if (selectedRows.length > 1) {
            helper.showToast($A.get("$Label.c.Error_Title"), $A.get("$Label.c.Select_only_one_account_error"), 'error');
        }
    },
    createAccount : function(cmp, event, helper) {
        cmp.set('v.loaded', 'false');
        var params =  helper.getCreateAccountParams(cmp, helper);
        var createNewAccount = cmp.get("c.createNewAccount");

        var fieldValidation = helper.validateAccountFields(cmp, helper);
        
        if (fieldValidation.status == 'ERROR') {
            helper.showToast('Error', fieldValidation.errorMessage, 'error');
            cmp.set('v.loaded', 'true');
            return;
        }

        createNewAccount.setParams({ params : params});

        createNewAccount.setCallback(this, function (response) {
            var responseState = response.getState();
            if (responseState === "SUCCESS") {
                debugger;
                var value = response.getReturnValue();
                if (value.status == 'SUCCESS') {
                    if (value.opportunityId != '') {
                        /*
                        cmp.set('v.isAccountProspectStatusNew', 'false');
                        cmp.set('v.isGo', 'true');
                        cmp.set('v.showLinkOpp', 'true');
                        cmp.set('v.oppUrl', '/partner/s/detail/'+value.opportunityId);
                        */
                        cmp.set('v.isAccountProspectStatusNew', 'false');
                        cmp.set('v.isGo', 'true');
                        cmp.set('v.sellerName', value.sellerName);
                        cmp.set('v.accountName', value.accountName);
                        cmp.set('v.partnerModel',value.partnerModel);
                        cmp.set('v.gympassContactEmail', value.gympassContactEmail);
                        cmp.set('v.prospectResultMessage', value.successMessage);
                        cmp.set('v.oppUrl', '/partner/s/detail/'+value.opportunityId);
                        cmp.set('v.loaded', 'true');
                    }
                }
                if (value.status == 'ERROR') {
                    debugger;
                    //helper.showToast($A.get("$Label.c.Exception_Title"), value.errorMessage, 'error');
                    cmp.set('v.isAccountProspectStatusNew', 'false');
                    cmp.set('v.isNoGo', 'true');
                    cmp.set('v.prospectResultMessage', 'Something went wrong, please contact the salesforce admin');
                    cmp.set('v.loaded', 'true');
                }
            }
            else {
                console.log("Create Account ERROR");
                cmp.set('v.loaded', 'true');
            }
        });

        $A.enqueueAction(createNewAccount);
    },
    getStateList : function(cmp, event, helper) {
        var country = cmp.find("country-select").get("v.value");
        var states = cmp.get("v.countryToStatesMap")[country];
        
        if (states != undefined && states.length > 0) {
            cmp.set("v.states", states);
        } else {
            cmp.set("v.states", []);
        }
        debugger;    
    },
    requestAnotherAccount : function(cmp, event, helper) {
        cmp.set('v.loaded', 'false');
    	cmp.set('v.thereAreAccounts', 'false');
        helper.getMetadata(cmp);
    },
})