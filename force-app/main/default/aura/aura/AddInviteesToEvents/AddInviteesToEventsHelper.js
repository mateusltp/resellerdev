({
	showToast : function(type, title, message, mode)
    {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "type": type,
            "title": title,
            "message": message,
            "mode": mode
        });
        toastEvent.fire();
	},
    
	addInvitee : function(component, object ,selectedLookUpRecords, recordId, assigneeId)
    {
        var action = component.get("c.addInvitee");
        
        action.setParams({ 
            objectName : object, 
            users : selectedLookUpRecords,
            recordId : recordId,
            assigneeId : assigneeId
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS")
            {
                var retorno = response.getReturnValue();
                
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type": 'success',
                    "title": 'Success!',
                    "message": retorno
                });
                toastEvent.fire();
                
                component.set('v.selectedLookUpRecords','[]');
                $A.get("e.force:closeQuickAction").fire();
            } else {
                
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type": 'error',
                    "title": 'Error!',
                    "message": 'Error while trying to add the invitees. Please try it again later!'
                });
                toastEvent.fire();
                $A.get("e.force:closeQuickAction").fire();
            }
        });
        $A.enqueueAction(action);
	}
})