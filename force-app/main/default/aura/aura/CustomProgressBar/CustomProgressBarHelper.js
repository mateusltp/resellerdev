({
    checkFieldsInObj : function(component, event, helper) { 

        var recordId = component.get("v.recordId");
        var action = component.get("c.checkFieldsInObject");

        action.setParams({
            "recordId": recordId
        });
        
        action.setCallback(this, function(response){
            var state = response.getState();  
            var data = response.getReturnValue();                  
            if (state === "SUCCESS") { 
                component.set("v.progress", data); 
                this.customizeProgressBar(component, data);   
            }
        });
        $A.enqueueAction(action);  
    },

    customizeProgressBar: function(component, data){

        var prgBar = component.find("prgBar");

        $A.util.removeClass(prgBar,'slds-is-critical');        
        $A.util.removeClass(prgBar,'slds-is-low');        
        $A.util.removeClass(prgBar,'slds-is-good');        
        $A.util.removeClass(prgBar,'slds-is-great');

        if(data >= 0 && data < 25){
            $A.util.addClass(prgBar,'slds-is-critical');

        } else if(data >= 25 && data < 50){
            $A.util.addClass(prgBar,'slds-is-low');

        } else if(data >= 50 && data < 75){
            $A.util.addClass(prgBar,'slds-is-good');

        } else {
            $A.util.addClass(prgBar,'slds-is-great');
        }
    }

})