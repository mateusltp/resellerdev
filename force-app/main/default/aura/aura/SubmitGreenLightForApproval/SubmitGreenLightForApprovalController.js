({
	submitForApproval : function(component, event, helper) {
        var formId = component.get("v.recordId");
        var comments = component.find("comments").get("v.value");
        
        var submitFormForApproval = component.get("c.submitFormForApproval");

        submitFormForApproval.setParams({ formId : formId, comments : comments });
        
        submitFormForApproval.setCallback(this, function(response){
            var state = response.getState();
            var data = response.getReturnValue(); 
            if (state === "SUCCESS") {               
                helper.showToast('success', 'Success', 'Enablers submitted to approval.');
                $A.get("e.force:closeQuickAction").fire();        
                $A.get('e.force:refreshView').fire();        
            } else {
                var errors = response.getError();
                var msg = 'Something went wrong. ';
                if (errors) {
                    if (errors[0] && errors[0].message) {					
                        msg = errors[0].message.includes('ALREADY_IN_PROCESS') ? 'Launch Scorecard is already in approval process.' : errors[0].message;
                        msg = errors[0].message.includes('NO_APPLICABLE_PROCESS') ? 'Launch Scorecard does not need to be approved.' : errors[0].message;
                    }
                } 
                helper.showToast('error', 'Whoops!', msg);
            }
        });

        $A.enqueueAction(submitFormForApproval);
	}
})