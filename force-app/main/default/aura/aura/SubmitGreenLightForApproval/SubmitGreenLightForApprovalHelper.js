({
	showToast : function(type, title, message) {
		var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "mode": "sticky",
            "type": type
        });
        toastEvent.fire();
	}
})