({
    doInit: function(component, event, helper) {
        helper.findAccountObj(component, event, helper);
    },

    handleAction: function(component, event, helper) {  
        
        let pvOwner = component.get("v.corporateValue");
        let frOwner = component.get("v.franchiseValue");  
        let currentAccount = component.get("v.currentAccount");  
        var source=event.getSource();

        if(currentAccount.Types_of_ownership__c == frOwner && source.get("v.label")=="Proceed") {
          
            helper.handleFranchise(component, event, helper);
        } else if(currentAccount.Types_of_ownership__c == pvOwner && source.get("v.label")=="Proceed") {
            helper.handleCorporate(component, event, helper);
            source.set("v.label", "Submit");                  
        } else {
            component.find("editForm").submit();
        }
    },

    closeModel: function(component, event, helper) {      
        helper.closeModel(component, event, helper);
    },

    handleSuccess: function(component, event, helper) {  
            
        helper.handleSuccess(component, event, helper);   
    }
 })