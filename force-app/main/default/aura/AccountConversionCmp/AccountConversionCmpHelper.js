({
    findAccountObj : function(component, event, helper) {
        var recordId = component.get("v.recordId");
        var action = component.get("c.getAccountObject");        
        action.setParams({
            "accountId": recordId
        });
        
        action.setCallback(this, function(response){
            var state = response.getState();  
            var data = response.getReturnValue();                  
            if (state === "SUCCESS") {              
                component.set("v.currentAccount", data);                           
            } else {
                console.log("Error finding account");
            }
        });
        $A.enqueueAction(action);  
    },

    handleFranchise: function(component, event, helper){    
        this.handleConversion(component, event, helper);   
          
               
    }, 

    handleCorporate: function(component, event, helper){       
        this.openModel(component, event, helper); 
    }, 

    
    handleSuccess: function(component, event, helper){ 
  
        this.handleConversion(component, event, helper);
        
    },

    handleConversion: function(component, event, helper){    
        component.set("v.isLoad", true);
        component.find("btnSubmit").set("v.disabled", true); 
        var currentAccount = component.get("v.currentAccount");  
        var action = component.get("c.convertAccount");

        action.setParams({
            "currentAccount": currentAccount
        });
        action.setCallback(this, function(response){
            var state = response.getState();             
            var data = response.getReturnValue();                                  
            if (state === "SUCCESS") {               
                this.showToast(component, event, helper);
                window.location.reload();
            } else {
                console.log("An error ocurred during conversion");
            }
        });        
        $A.enqueueAction(action);
        
    },

    closeModel: function(component, event, helper) {      
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
     },

     openModel: function(component, event, helper) {      
        component.set("v.isModalOpen", true);
     },

     showToast : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "type": "success",
            "title": "Success!",
            "message": "Account Hierarchy has been updated successfully."
        });
        toastEvent.fire();
    }
   
})