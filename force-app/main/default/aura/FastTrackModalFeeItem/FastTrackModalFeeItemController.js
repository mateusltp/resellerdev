({
	toChangeOpenModelFeeItem : function(component, event, helper) {
        var openModelFeeItem = component.get('v.openModelFeeItem');
        
        openModelFeeItem = !openModelFeeItem;
        
        component.set('v.openModelFeeItem', openModelFeeItem);
	},
})