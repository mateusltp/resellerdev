({
    doInit : function(component, event, helper) {
        helper.getRT(component, event, helper);
    },

    handleSuccess : function (component, event, helper){      
        helper.showToast("success", "Success!","Green Card saved successfully!");
        component.set("v.spinnerOn", false);
    },

    handleSubmit : function(component, event){
        component.set("v.spinnerOn", true);
    }
})