({
    getRT : function(component, event, helper) {       
        var action = component.get("c.getRecordTypeNameByRecordId");
        action.setParams({
            "recordId": component.get("v.recordId")           
        });

        action.setCallback(this, function(response){
            var state = response.getState();  
            var data = response.getReturnValue();  
            if (state === "SUCCESS") {
               if(data.includes('M1')){                
                   component.set("v.mRt", "M1");
               }
            } else {
                this.showToast("error", "Error!", "No record type - Please contact admin.");
            }
        });
        $A.enqueueAction(action); 
    },

    showToast: function(type, title, message) {      
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type": type
        });
        toastEvent.fire();
	}
    
})