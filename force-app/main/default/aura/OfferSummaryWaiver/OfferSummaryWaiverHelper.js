({
	loadData : function(component, event, helper) {
		debugger;
		var waiver = component.get("v.waiver");
		helper.setAttributes(component, helper, waiver);													
	},
	setAttributes: function(component, helper, waiver) {
		component.set("v.waiverId", waiver.sObj.Id);
	},
	openAddWaiver : function(component, helper) {
		$A.createComponent(
            "c:OfferSummaryAddWaiver", {
				"waiver" : component.get("v.waiver"),
				"isUpdate" : true
            },
            function(modal, status, error) {
                if (component.isValid()) {
					component.find('overlayLib').showCustomModal({
						header: "Waiver or Temporary Discount",
						body: modal,
						showCloseButton: true
					})
                }
            }
        );
	},
	deleteWaiver : function(component, event, helper) {
		$A.createComponent(
            "c:OfferSummaryDelete", {
				"objId" : component.get("v.waiverId")
            },
            function(modal, status, error) {
                if (component.isValid()) {
					component.find('overlayLib').showCustomModal({
						header: "Delete Record",
						body: modal,
						showCloseButton: true
					})
                }
            }
        );
	}
})