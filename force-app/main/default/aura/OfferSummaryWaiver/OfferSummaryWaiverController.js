({
	doInit : function(component, event, helper) {
		helper.loadData(component, event, helper);		
	},
	handleSelect : function(component, event, helper) {
		var selectedMenuItemValue = event.getParam("value");
		//var product = component.get("v.quoteItem");
		
		if (selectedMenuItemValue == "WaiverDetails") {
			helper.openAddWaiver(component, helper);																			
		}		
	},
	waiverDetails : function(component, event, helper) {
		//var product = component.get("v.quoteItem");
		helper.openAddWaiver(component, helper);																			
	},
	deleteWaiver : function(component, event, helper) {
		helper.deleteWaiver(component, event, helper);
	}
})