({
	changeStage : function(component, event, helper) {
		helper.callChangeStageEvent(component, event);
	},

	callStage : function(component, event) {  
        component.set('v.saveAndCallStage',true);          
    },
    

    backStage : function(component, event) {  
        var cmpEvent = $A.get("e.c:fastTrackStageDoneEvent");
        var nextIndex = component.get("v.myStageIndex"); 
        var eventSourceId = event.getSource().getLocalId();
        nextIndex +=  -1;
        cmpEvent.setParams({
            "opportunityId": component.get("v.recordId"),
            "nextIndex": nextIndex
        });
        cmpEvent.fire();          
    },
    justSave : function(component, event) {  
        component.set('v.saveAndCallStage',false);          
    },

    handleSuccess : function(component, event) {       
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success",
            "message": "Saved",
            "type": "success"
        });
        toastEvent.fire(); 
        component.set("v.spinnerOn", false);
        
        if ( !component.get('v.saveAndCallStage') )
            return;
            
        var cmpEvent = $A.get("e.c:fastTrackStageDoneEvent");
        var nextIndex = component.get("v.myStageIndex"); 
        var eventSourceId = event.getSource().getLocalId();
        nextIndex += eventSourceId === "btnBack" ? -1 : 1;
        cmpEvent.setParams({
            "opportunityId": component.get("v.recordId"),
            "nextIndex": nextIndex
        });
        cmpEvent.fire();
    },

    handleSubmit : function(component, event){
        component.set("v.spinnerOn", true);
    },

    doInit : function(component, event, helper){   
        var scrollOptions = { left: 0, top: 0, behavior: 'smooth'};
        window.scrollTo(scrollOptions);   
    }
})