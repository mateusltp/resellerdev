({
	loadEnablers : function(component, helper) {
		
		var action = component.get("c.getEnablers");
		action.setParams({ oppId : component.get("v.recordId") });
		
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
				var enablers = response.getReturnValue();

				component.set("v.enablers", enablers);
				helper.setAchievedEnablersNumber(component, enablers);
            }
            else {
				alert("Enablers could not be retrieved");
			}
        });
		$A.enqueueAction(action);
	},



	setAchievedEnablersNumber: function(component, enablers) {
		var achievedEnablersListRequire = [];
		var achievedEnablersListDontRequire = [];
		var achievedEnablersNumber= 0;
		var optionsRequire = component.get("v.optionsRequire");
		var optionsDont = component.get("v.optionsDont");
		
		

		for (var i=0; i<optionsRequire.length; i++) {
			for (var j=0; j<enablers.length; j++) {
				if(enablers[j].Name == optionsRequire[i].value){
					enablers[j].Name = optionsRequire[i].label;
					achievedEnablersListRequire.push(enablers[j]);
					break;
				}
			}
		}

		for (var i=0; i<optionsDont.length; i++) {
			for (var j=0; j<enablers.length; j++) {
				if(enablers[j].Name == optionsDont[i].value){
					enablers[j].Name = optionsDont[i].label;
					achievedEnablersListDontRequire.push(enablers[j]);
					break;
				}	
			}
		}

		for (var i=0; i<enablers.length; i++) {
			if (enablers[i].Achieved__c == 'Yes') {
				achievedEnablersNumber++;
			}
		}

		

		component.set("v.totalEnablersNumber", enablers.length);
		component.set("v.achievedEnablersNumber", achievedEnablersNumber);
		component.set("v.achievedEnablersListRequire", achievedEnablersListRequire);
		component.set("v.achievedEnablersListDontRequire", achievedEnablersListDontRequire);

	}
})