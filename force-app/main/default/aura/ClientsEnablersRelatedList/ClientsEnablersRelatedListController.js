({
    doInit : function(component, event, helper) {
        helper.getEnablers(component, event, helper);
    },
    updateEnablerCtrl : function(component, event, helper) {
        helper.updateEnablerHpr(component, event, helper);
    }
})