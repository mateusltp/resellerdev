/**
 * @description       : 
 * @author            : tiago.ribeiro@gympass.com
 * @group             : 
 * @last modified on  : 05-19-2022
 * @last modified by  : tiago.ribeiro@gympass.com
**/
({
    init: function (cmp, event, helper){
        helper.startFlow(cmp);
    },
})