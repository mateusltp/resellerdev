/**
 * @description       : 
 * @author            : tiago.ribeiro@gympass.com
 * @group             : 
 * @last modified on  : 05-19-2022
 * @last modified by  : tiago.ribeiro@gympass.com
**/
({
    startFlow: function(cmp) {
        var flow = cmp.find("flowData");
        var inputVariables = [
          {
              name : 'OppId',
              type : 'String',
              value : cmp.get('v.recordId')
          },
          {
              name : 'FastTrackStages_ToCalculateRevenueFields',
              type : 'String',
              value : cmp.get('v.fastTrackStages')
          }
          ];
        flow.startFlow("FastTrack_New_Business_Offer_Summary", inputVariables);
    }
})