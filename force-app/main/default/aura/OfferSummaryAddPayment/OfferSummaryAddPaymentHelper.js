({
	showToast: function(type, title, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type": type
        });
        toastEvent.fire();
	},
	fireFastTrackEvaluationRequestEvent : function(component, helper) {
        var cmpEvt = $A.get("e.c:FastTrackEvaluationRequestEvent");	
		cmpEvt.setParams({"isCommercialConditions" : true });
		cmpEvt.fire();
    },
    fireRefreshMainFastTrackCmpEvent : function(component, helper) {
        var cmpEvt = $A.get("e.c:RefreshMainFastTrackCmpEvent");
		cmpEvt.fire();
    }
})