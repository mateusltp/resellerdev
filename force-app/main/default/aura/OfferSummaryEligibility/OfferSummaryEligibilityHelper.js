({
	loadData : function(component, event, helper) {
		debugger;
		var eligibility = component.get("v.eligibility");
		helper.setAttributes(component, helper, eligibility);																	
	},
	setAttributes: function(component, helper, eligibility) {
		component.set("v.eligibilityId", eligibility.sObj.Id);
	},
	/*openAddEligibility : function(component, helper) {
		$A.createComponent(
            "c:OfferSummaryAddEligibility", {
				"eligibility" : component.get("v.eligibility"),
				"isUpdate" : true
            },
            function(modal, status, error) {
                if (component.isValid()) {
					component.find('overlayLib').showCustomModal({
						header: "Eligibility",
						body: modal,
						showCloseButton: true
					})
                }
            }
        );
	},*/
	deleteEligibility : function(component, event, helper) {
		$A.createComponent(
            "c:OfferSummaryDelete", {
				"payment" : component.get("v.payment"),
				"objType" : "Eligibility",
				"objId" : component.get("v.eligibilityId")
            },
            function(modal, status, error) {
                if (component.isValid()) {
					component.find('overlayLib').showCustomModal({
						header: "Delete Record",
						body: modal,
						showCloseButton: true
					})
                }
            }
        );
	},
	openAddEligibility : function(component, helper) {
		$A.createComponent(
            "c:OfferSummaryAddEligibility", {
				"proposalCreationTO" : component.get("v.proposalCreationTO"),
				"payment" : component.get("v.payment"),
				"isUpdate" : false
            },
            function(modal, status, error) {
                if (component.isValid()) {
					component.find('overlayLib').showCustomModal({
						header: "Add elegibility and subscription fee setup",
						body: modal,
						showCloseButton: true
					})
                }
            }
        );
	},

	openEditEligibility : function(component, helper) {
		var eligibility = component.get("v.eligibility");
		$A.createComponent(
            "c:OfferSummaryAddEligibility", {
				"proposalCreationTO" : component.get("v.proposalCreationTO"),
				"eligibility": eligibility,
				"payment" : component.get("v.payment"),
				"isUpdate" : true
            },
            function(modal, status, error) {
                if (component.isValid()) {
					component.find('overlayLib').showCustomModal({
						header: "Edit elegibility and subscription fee setup",
						body: modal,
						showCloseButton: true
					})
                }
            }
        );
	}
})