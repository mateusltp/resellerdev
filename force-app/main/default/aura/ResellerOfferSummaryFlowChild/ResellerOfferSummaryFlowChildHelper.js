/** cloned from OfferSummaryFlowChild.cmp */
({
    startFlow: function(cmp) {
        var flow = cmp.find("flowData");
        var inputVariables = [
          {
              name : 'OppId',
              type : 'String',
              value : cmp.get('v.recordId')
          },
          {
              name : 'FastTrackStages_ToCalculateRevenueFields',
              type : 'String',
              value : cmp.get('v.fastTrackStages')
          }
          ];
        flow.startFlow("RESELLER_Fast_Track_New_Business_Offer_Summary", inputVariables);
    }
})