({
    handleNewPartnerProductEvt : function(component, event, helper) {
        component.set("v.openNewPartnerProductModal", true);
    },

    handleCloseModal: function(component, event) {
        component.set("v.openNewPartnerProductModal", false);
    }
})