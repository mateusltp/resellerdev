/**
 * @description       : 
 * @author            : 
 * @group             : 
 * @last modified on  : 03-21-2022
 * @last modified by  : Mateus Augusto - GFT (moes@gft.com)
**/
({
    showToast: function (type, title, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type": type
        });
        toastEvent.fire();
    },
    fireFastTrackEvaluationRequestEvent: function (component, helper) {
        var cmpEvt = $A.get("e.c:FastTrackEvaluationRequestEvent");
        cmpEvt.setParams({ "isCommercialConditions": true });
        cmpEvt.fire();
    },
    fireRefreshMainFastTrackCmpEvent: function (component, helper) {
        var cmpEvt = $A.get("e.c:RefreshMainFastTrackCmpEvent");
        cmpEvt.fire();
    },

    calcTotalComission: function (component, helper) {
        console.log('Helper calcTotalComission');
        if (component.get('v.totalComissionCalcInit')) {

            let totalNumberOfEmployees = component.get("v.proposalCreationTO.proposal.sObj.Total_Number_of_Employees__c");
            let salesPrice = component.find("salesPrice").get("v.value");
            let percentageComission;
            let listPrice = component.find("listPrice").get("v.value");

            if (component.get('v.proposalCreationTO.proposal.sObj.Comission_Percent__c') == null) {
                percentageComission = 30;
            } else {
                percentageComission = component.get('v.proposalCreationTO.proposal.sObj.Comission_Percent__c');
            }

            let totalComission = (totalNumberOfEmployees * salesPrice) * Number(percentageComission) / 100;
            component.set('v.totalComissionCalc', totalComission);
            component.set('v.flagTotalComission', true);
            component.set('v.valueSalesPrice', salesPrice);
        }
        else {
            try {

                let totalNumberOfEmployees = component.get("v.proposalCreationTO.proposal.sObj.Total_Number_of_Employees__c");
                let salesPrice = component.find("salesPrice").get("v.value");
                let percentageComission = component.find("percentageComission");
                var newListPrice = component.get("v.newPrices");
                var newPrice;
                let listPrice = component.find("listPrice").get("v.value");

                if (percentageComission != undefined) {
                    percentageComission = component.find("percentageComission").get("v.value");
                }

                newListPrice.forEach(function (item) {
                    if (percentageComission == item.label) {
                        newPrice = item.value;
                    }
                });

                component.set('v.listPriceCalc', newPrice);
                component.set('v.quoteItem.sObj.UnitPrice', newPrice);
                component.set('v.valueComissionInitNew', false);

                if (listPrice == salesPrice) {
                    let totalComission = (totalNumberOfEmployees * newPrice) * Number(percentageComission) / 100;
                    component.set('v.totalComissionCalc', totalComission);
                    component.set('v.valueSalesPrice', newPrice);
                } else {
                    if (component.get("v.flagTotalComission")) {
                        let totalComission = (totalNumberOfEmployees * component.find("salesPrice").get("v.value")) * Number(percentageComission) / 100;
                        component.set('v.totalComissionCalc', totalComission);
                        component.set('v.valueSalesPrice', component.find("salesPrice").get("v.value"));
                        component.set('v.flagTotalComission', false);
                    } else {
                        let totalComission = (totalNumberOfEmployees * salesPrice) * Number(percentageComission) / 100;
                        component.set('v.totalComissionCalc', totalComission);
                        component.set('v.valueSalesPrice', salesPrice);
                    }
                }

            } catch (e) {
                component.set('v.totalComissionCalc', '');
                //console.log(e);           
            }

        }
        console.log('valor salesprice ',component.get("v.valueSalesPrice"));
        component.set('v.totalComissionCalcInit', false);

    },

    findComissionValue: function (component, event, helper) {
        console.log('Helper findComissionValue');

        var productId = component.get("v.quoteItem.sObj.Product2Id");
        var currencyIsoCode = component.get("v.proposalCreationTO.opportunityCurrencyIsoCode");

        var action = component.get("c.findComissionValue");
        action.setParams({
            "productId": productId,
            "currencyIsoCode": currencyIsoCode

        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            let result;
            var items = [];
            var newListPrice = [];
            if (state === "SUCCESS") {
                result = response.getReturnValue();
                console.log(result);
                result.forEach(function (percent) {

                    var item = {
                        "label": percent.Comission_Percent__c.toString() + "%",
                        "value": percent.Comission_Percent__c.toString()
                    };

                    var newPrice = {
                        "label": percent.Comission_Percent__c.toString(),
                        "value": percent.Price__c.toString()
                    };
                    if (item["value"] == '30') {
                        if (component.get('v.proposalCreationTO.proposal.sObj.Comission_Percent__c') == null) {
                            component.set('v.valueComission', item["value"])
                        } else {
                            component.set('v.valueComission', component.get('v.proposalCreationTO.proposal.sObj.Comission_Percent__c').toString())

                        }
                    }

                    items.push(item);
                    newListPrice.push(newPrice);

                });

                component.set("v.options", items);
                component.set("v.newPrices", newListPrice);
            } else {
                let errors = response.getError();
                //console.log(errors);
            }
        });
        $A.enqueueAction(action);

    }
})