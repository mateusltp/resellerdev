({
    doInit : function(component, event, helper) {
        component.set('v.showSpinner', true);
        var language = helper.getUrlParameter('language');
        component.set('v.language', language);
        component.set('v.showSpinner', false);
    },
    
    handleOnSuccess: function(component, event) {
   		var updatedRecord = JSON.parse(JSON.stringify(event.getParams()));
        console.log('onsuccess: ', updatedRecord.id);
        component.set('v.accPropectingId', updatedRecord.response.id);
        component.set('v.showNewRequest', 'false');
   	},
    
    toNextStep : function(component, event, helper) {
        event.preventDefault();
    	var fields = event.getParam("fields");
        debugger;
    	component.find("form").submit(fields);
    },
    
})