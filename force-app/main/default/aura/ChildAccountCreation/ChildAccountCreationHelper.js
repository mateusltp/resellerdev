/**
 * @description       : 
 * @author            : Jorge Stevaux - JZRX@gft.com
 * @group             : 
 * @last modified on  : 11-12-2020
 * @last modified by  : Jorge Stevaux - JZRX@gft.com
 * Modifications Log 
 * Ver   Date         Author                         Modification
 * 1.0   11-12-2020   Jorge Stevaux - JZRX@gft.com   Initial Version
**/
({
    handleSubmit: function(component, event, helper) {      

        event.preventDefault(); 

        var fields = event.getParam('fields');  
        
        if(!this.validateAddress(fields))
            return;

        component.find("btnSubmit").set("v.disabled", true);

        var selectedOption1 = component.find('hierarchyOption1').get("v.checked");
        var selectedOption2 = component.find('hierarchyOption2').get("v.checked"); 
        var selectedOption3 = component.find('hierarchyOption3').get("v.checked");         
        var creationReason = component.find("creationReason").get("v.value");

        var accFormData = {
            parentId: component.get("v.recordId"), 
            name: fields.Name, 
            isMultiBrand: selectedOption1,     
            isHybridModel: selectedOption2, 
            stateCode: fields.ShippingStateCode,            
            countryCode: fields.ShippingCountryCode,             
            street: fields.ShippingStreet,
            city: fields.ShippingCity,
            postalCode: fields.ShippingPostalCode,
            creationReasonDetails: creationReason,
            isSetup: selectedOption3
        }; 

        var action = component.get('c.cloneAccount');

        action.setParams({
           "formData" : accFormData
        });
        
        action.setCallback(this, function(response){
            var state = response.getState();  
            var data = response.getReturnValue(); 
            if (state === "SUCCESS") {           
                if(data.ERROR != null) {
                    this.showToast('error', 'Error!', data.ERROR);               
                    component.find("btnSubmit").set("v.disabled", false);     
                } else {
                    component.set("v.load", true);
                    this.showToast('success', 'Success!', 'New location created successfully.');                
                    //window.location.reload(); 
                    this.openNewAccount(data.SUCCESS);
                }

            } else if(state === 'ERROR'){                   
                this.showToast('error', 'ERROR', 'UNKNOWN ERROR');  
                component.find("btnSubmit").set("v.disabled", false);       
            }
        });
        $A.enqueueAction(action); 
    },

    validateAddress: function(fields){
        if(fields.ShippingCountryCode == null) {
            this.showToast("error","Required Field" ,"Country can not be left empty")
            return false;
        }
        return true;
    },

    showToast : function(type, tittle, msg) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "type": type,
            "title": tittle,
            "message": msg
        });
        toastEvent.fire();
    },

    openNewAccount : function(accountId) {
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": accountId,
            "slideDevName": "detail"
        });
        navEvt.fire();
    }
})