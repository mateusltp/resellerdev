({
    checkParentStatus: function(component, event, helper){
        var statusParent = component.find("parentStatus").get("v.value");

        if(statusParent == 'Cancelled' || statusParent == 'Reject'){
            component.set("v.isValidForm", false);
            component.set("v.errorMsg", "Account Cancelled/Rejected! New location(s) not allowed."); 
            return;
        }

    },
    
    handleSubmit: function(component, event, helper){
               
        helper.handleSubmit(component, event, helper);
    }
})