({
    openModalAdd: function(component, event, helper) {
        component.set("v.showAdd", true);
    },

    openModalEdit: function(component, event, helper) {
        component.set("v.showEdit", true);
    },

    openModalRemove: function(component, event, helper) {
        component.set("v.showRemove", true);
    }
})