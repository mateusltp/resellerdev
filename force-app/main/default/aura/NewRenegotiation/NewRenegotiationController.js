({
    doInit : function(component, event, helper) {
        component.set( "v.spinnerOn" , true );
        helper.loadRenegotiationRts( component , helper );
    },
    selectRt : function(component, event, helper) {
        var lstIndex = parseInt(event.target.value);
        component.set( "v.selectedRt" , component.get("v.lstOppRenegotiationRt")[ lstIndex ] );
    },
    save : function(component, event, helper) {
        if( !component.get( "v.isRtSelected" ) ){
            component.set( "v.spinnerOn" , true );
            component.set( "v.isRtSelected" , true );
            helper.loadOppSubtype( component , helper );
        } else {
            helper.createRenegotiationOpp( component , helper );
        }
    },
    close : function(component, event, helper) {
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
    }
})