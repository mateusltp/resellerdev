({
    loadRenegotiationRts : function(component, helper) {
        var action = component.get("c.getRenegotiationRecordTypes");

        action.setCallback(this, function(response) {
            var state = response.getState();
            if ( state === "SUCCESS" ) {

                component.set( "v.lstOppRenegotiationRt" , response.getReturnValue() );  
                component.set( "v.selectedRt" , response.getReturnValue()[0] );

            } else if ( state === "ERROR" ) {
                var errors = response.getError();
                if ( errors && errors[0] && errors[0].message ) {
                    helper.showToast( "Error!" , "Error retrieving renegotiation record type. " + errors[0].message , "error" );
                } else {
                    helper.showToast( "Error!" , "Unknown error. Please, contact your administrator." , "error" );
                }
            }
            component.set( "v.spinnerOn" , false );
        });

        $A.enqueueAction(action);
    },
    loadOppSubtype : function(component, helper) {
        var action = component.get("c.getPicklistOptions");

        action.setCallback(this, function(response) {
            var state = response.getState();
            if ( state === "SUCCESS" ) {
                var lstPicklistOptions = JSON.parse(response.getReturnValue());
                
                component.set( "v.lstOppSubtypes" , lstPicklistOptions );  
                component.set( "v.selectedSubtype" , lstPicklistOptions[0].value );

            } else if ( state === "ERROR" ) {
                var errors = response.getError();
                if ( errors && errors[0] && errors[0].message ) {
                    helper.showToast( "Error!" , "Error loading opportunity sub-types. Please, contact your administrator." , "error" );
                } else {
                    console.log("Unknown error");
                    helper.showToast( "Error!" , "Unknown error. Please, contact your administrator." , "error" );
                }
            }
            component.set( "v.spinnerOn" , false );
        });

        $A.enqueueAction(action);
    },
    createRenegotiationOpp : function(component, helper) {
        component.set( "v.spinnerOn" , true );
        var action = component.get("c.createNewRenegotiationOpp");

        action.setParams({ 
            aAccId : component.get( "v.recordId" ),
            aRecordTypeDevName : component.get( "v.selectedRt.DeveloperName" ),
            aSubTypeValue : component.get( "v.selectedSubtype" )
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if ( state === "SUCCESS" ) {
                var clonedOppId = response.getReturnValue();

                helper.showToast( "Success!" , "Renegotiation Opportunity created." , "success" );
                helper.redirectNewOpp( clonedOppId );

            } else if ( state === "ERROR" ) {
                var errors = response.getError();
                if ( errors && errors[0] && errors[0].message ) {

                    helper.showToast( "Error!" , errors[0].message , "error" );

                } else {
                    console.log("Unknown error");
                    helper.showToast( "Error!" , "Unknown error. Please, contact your administrator." , "error" );
                }
                component.set( "v.spinnerOn" , false );
            }
        });

        $A.enqueueAction(action);
    },
    redirectNewOpp: function( clonedOppId ){
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": clonedOppId
        });
        navEvt.fire();
    },
    showToast: function( title , message , type ){
        message = this.modifyToastMessage(message);
        
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: title,
            message: message,
            type: type
        });
        toastEvent.fire();
    },
    modifyToastMessage: function( message ){
        if (message.includes('FIELD_CUSTOM_VALIDATION_EXCEPTION, ')) {
            var messageArray = message.split('FIELD_CUSTOM_VALIDATION_EXCEPTION, ');

            if (messageArray[1] != undefined) {
                message = messageArray[1];                
            }
        }

        return message;
    }
})