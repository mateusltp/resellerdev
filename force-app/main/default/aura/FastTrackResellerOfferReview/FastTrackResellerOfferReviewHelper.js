({
    disableSendToApproval: function(component, proposalTO) {
		if( !proposalTO ){ return; }
		let disable = this.approvalNeeded(component, proposalTO);
		component.set('v.disableSendToApproval', disable);
	},

    disableSendOffer: function(component, proposalTO) {
		if( !proposalTO ){ return; }
		let disable = !this.approvalNeeded(component, proposalTO);
		component.set('v.disableSendOffer', disable);
	},

    approvalNeeded : function(component, proposalTO) {
		let approvalNeeded = (
			((!proposalTO.proposal.sObj.Setup_Discount_Approval_Needed__c  || ( proposalTO.proposal.sObj.Setup_Discount_Approval_Needed__c  && proposalTO.proposal.sObj.Setup_Discount_Approved__c ) )  &&
			( !proposalTO.proposal.sObj.Enterprise_Discount_Approval_Needed__c  || ( proposalTO.proposal.sObj.Enterprise_Discount_Approval_Needed__c && proposalTO.proposal.sObj.Enterprise_Subscription_Approved__c ) ) &&
			( !proposalTO.proposal.accessFee.waiversApprovalNeeded ) ) &&
			( !proposalTO.proposal.sObj.Deal_Desk_Approval_Needed__c || (proposalTO.proposal.sObj.Deal_Desk_Approval_Needed__c && proposalTO.proposal.sObj.Deal_Desk_Approved__c) )
		);
		if (proposalTO.oppRecTypeDevName == 'Indirect_Channel_New_Business') {
			approvalNeeded = approvalNeeded && (!proposalTO.proposal.sObj.Enablers_Approval_Needed__c || (proposalTO.proposal.sObj.Enablers_Approval_Needed__c && proposalTO.proposal.sObj.Enablers_Approved__c) );
		}
		return approvalNeeded;
	},

    checkApprovalStatus: function(component, proposalTO){
		if( !proposalTO ){ return; }	
		
		if(	proposalTO.proposal.sObj.Deal_Desk_Approval_Needed__c){ 			
			if(!proposalTO.proposal.sObj.Deal_Desk_Approved__c) {
				component.set("v.iconNameDealDesk", "utility:clear");
				component.set("v.variantDealDesk", "error");
				component.set("v.iconNameDealDeskReady", "utility:clock");
				component.set("v.variantDealDeskReady", "Informational");	
				component.set("v.colorBox1", "colorbox_error");
			} else {
				component.set("v.iconNameDealDesk", "utility:success");
				component.set("v.variantDealDesk", "success");
				component.set("v.iconNameDealDeskReady", "utility:success");
				component.set("v.variantDealDeskReady", "success");
				component.set("v.colorBox1", "colorbox_ok");
			}	
		} else {
				component.set("v.iconNameDealDesk", "utility:success");
				component.set("v.variantDealDesk", "success");
				component.set("v.iconNameDealDeskReady", "utility:success");
				component.set("v.variantDealDeskReady", "success");
				component.set("v.colorBox1", "colorbox_ok");
		}

		console.log('proposalTO.proposal.accessFee.waiversApprovalNeeded ',proposalTO.proposal.accessFee.waiversApprovalNeeded)
		
		if(proposalTO.proposal.accessFee.waiversApprovalNeeded){				
			component.set("v.iconNameWaiver", "utility:clear");
			component.set("v.variantWaiver", "error");
			component.set("v.colorBox2", "colorbox_error");
		} else {
			component.set("v.iconNameWaiver", "utility:success");
			component.set("v.variantWaiver", "success");
			component.set("v.colorBox2", "colorbox_ok");
		}

        if(proposalTO.proposal.sObj.Enterprise_Discount_Approval_Needed__c){ 
			if(proposalTO.proposal.sObj.Enterprise_Subscription_Approved__c){ 
				component.set("v.iconNameEnterprise", "utility:success");
				component.set("v.variantEnterprise", "success");
			} else {
				component.set("v.iconNameEnterprise", "utility:clear");
				component.set("v.variantEnterprise", "error");
				component.set("v.colorBox3", "colorbox_error");
			}			
		}  else {
				component.set("v.iconNameEnterprise", "utility:success");
				component.set("v.variantEnterprise", "success");
		}
      
		if(proposalTO.proposal.sObj.Setup_Discount_Approval_Needed__c){ 
			if(proposalTO.proposal.sObj.Setup_Discount_Approved__c){
				component.set("v.iconNameSetup", "utility:success");
				component.set("v.variantSetup", "success");
			} else {
				component.set("v.iconNameSetup", "utility:clear");
				component.set("v.variantSetup", "error");
				component.set("v.colorBox3", "colorbox_error");
			}			

		}  else {
				component.set("v.iconNameSetup", "utility:success");
				component.set("v.variantSetup", "success");
		}
		
		if(proposalTO.proposal.sObj.Enablers_Approval_Needed__c){ 
			if(proposalTO.proposal.sObj.Enablers_Approved__c){ 
				component.set("v.iconNameEnablers", "utility:success");
				component.set("v.variantEnablers", "success");
			} else {
				component.set("v.iconNameEnablers", "utility:clear");
				component.set("v.variantEnablers", "error");
				component.set("v.colorBox3", "colorbox_error");
			}			
		}  else {
				component.set("v.iconNameEnablers", "utility:success");
				component.set("v.variantEnablers", "success");
		}
	},

    getQuoteApprovalHistoryLink : function(component){
        var proposal = component.get("v.proposalCreationTO.proposal");
        var quote;
        quote = proposal.sObj;
        var obj = {
            name : quote.Name
        };
        component.set("v.quote", obj);
    },

	submitForApproval: function(component, proposalTO){
		var action = component.get("c.submitForApproval");
		component.set("v.spinnerOn", true);  
		console.log('SUBMIT DEAL DESK >> ' + proposalTO);
		console.dir(proposalTO);
		console.log(proposalTO.dealDeskApprovalNeeded);
		
        action.setParams({
            "jsonTO": JSON.stringify(proposalTO)
		});  
		
        action.setCallback(this, function(response){
                var state = response.getState();  
                var data = response.getReturnValue();
                component.set("v.spinnerOn", false);                          
                if (state === "SUCCESS") {  					
					this.showToast("success", "Request sent", "Great! Your approval resquest was sent. You’ll receive feedback soon by email.");
                    $A.get('e.c:RefreshMainFastTrackCmpEvent').fire();
                }else {
					var errors = response.getError();
					var msg = "Error ";
					if (errors) {
						if (errors[0] && errors[0].message) {							
							msg = errors[0].message.includes('ALREADY_IN_PROCESS') ? 'Offer already in approval process - Contact Admin for help.' :  errors[0].message;
						}
					} 
				   this.showToast("error", "Error!", msg);
                }
              });
        $A.enqueueAction(action);
	},
	showToast : function(type,title, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "type": type,
            "message":message
        });
        toastEvent.fire();
    }
})