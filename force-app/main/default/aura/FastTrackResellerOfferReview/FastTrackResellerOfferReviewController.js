({
    doInit: function(component, event, helper) {
        var proposalTO = component.get("v.proposalCreationTO");
        console.log(proposalTO);
        helper.disableSendToApproval(component, proposalTO);
        helper.disableSendOffer(component, proposalTO);
        helper.checkApprovalStatus(component, proposalTO);
    },

    callStage : function(component, event) {        
        var cmpEvent = $A.get("e.c:fastTrackStageDoneEvent");
        var nextIndex = component.get("v.myStageIndex"); 
        var eventSourceId = event.getSource().getLocalId();
        nextIndex += eventSourceId === "btnBack" ? -1 : 1;
        cmpEvent.setParams({
            "opportunityId": component.get("v.proposalCreationTO.opportunityId"),
            "nextIndex": nextIndex
        });
        cmpEvent.fire();
    },

    handleSubmitForApproval: function(component, event, helper){
        var tempFields = component.find('requiredField');
        var requiredFields = [];
        if($A.util.isUndefined(tempFields.length)){
            requiredFields.push(tempFields);
        }  else {
            requiredFields.push.apply(requiredFields,tempFields);       
        }
        var allValid = requiredFields.reduce(function (validSoFar, inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && !inputCmp.get('v.validity').valueMissing;
        }, true);
        if (!allValid) {
            helper.showToast('warning', 'Justify', 'Please justify your conditions. Your offer will be analyzed and you’ll receive feedback by email.');
            return;
        }

        var proposalTO = component.get("v.proposalCreationTO");
        var dealDeskJustification = proposalTO.proposal.dealDeskDescription;
        var commercialJustification;
        var waiverJustification;
        
        requiredFields.forEach(function(field){
            if(field.get('v.name').includes('dealDesk')){
                dealDeskJustification.push('\nRationale: ' + field.get('v.value') );
            } else if(field.get('v.name').includes('commercial')){
                commercialJustification = (field.get('v.value'));
            } else {
                waiverJustification = (field.get('v.value'));
            }
        });

        proposalTO.proposal.dealDeskDescription = dealDeskJustification;
        proposalTO.proposal.commercialJustification = commercialJustification;
        proposalTO.proposal.accessFee.waiversJustification = waiverJustification;

        console.log("TO no controller review >>> " + JSON.stringify(proposalTO));
        helper.submitForApproval(component, proposalTO);
        
    }
})