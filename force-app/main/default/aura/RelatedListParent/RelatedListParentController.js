/**
 * @description       : 
 * @author            : tiago.ribeiro@gympass.com
 * @group             : 
 * @last modified on  : 05-04-2022
 * @last modified by  : tiago.ribeiro@gympass.com
**/
({
    handleAction: function(component, event, helper){
        component.set("v.flowIsRunning", true);
        let flow = component.find("flow");
        let flowDetails = event.getParam('flowDetails');
        let inputVariables = [
            {
                name: 'recordId',
                type: 'String',
                value: flowDetails.recordId ? flowDetails.recordId : component.get('v.recordId')
            }
        ];

        flow.startFlow(flowDetails.flowName, inputVariables);

    },

    closeFlowModal : function(component, event, helper) {
        component.set("v.flowIsRunning", false);
    },

    handleFlowStatusChange : function(component, event, helper) {
        if(event.getParam('status') === "STARTED") {
            var cmpTarget = component.find('flowDiv');
            $A.util.removeClass(cmpTarget, 'hideFlow');
        }
        if(event.getParam('status') === "FINISHED") {

            component.find('relatedlist').handleRefreshData();

            component.set("v.flowIsRunning", false);
            var cmpTarget = component.find('flowDiv');
            $A.util.addClass(cmpTarget, 'hideFlow');
 
        }
    }
})