({
    doInit : function(component, event, helper) {
        var recordId = component.get("v.recordId");
        var action = component.get("c.getApprovalItemsCase");
        action.setParams({
            "caseId": recordId,
        });  
        action.setCallback(this, function(response){
            var state = response.getState();  
            var data = response.getReturnValue();
            if (state === "SUCCESS") {  
                component.set("v.lstApprovalItems", data);  
              
            } 
          });
    $A.enqueueAction(action);
    },

    checkStatus : function(component, event, helper) {
        var closedStatus = "RejectedApproved"
        var status = component.find("statusField").get("v.value");
        component.set("v.isOpenToApprove", !closedStatus.includes(status) ? true : false);
    },

    handleClickApproval : function(component, event, helper){
        var reason = component.get("v.comment");
        if(reason == null || reason.isEmpty ) { 
            helper.showToast("error","Required Field" ,"Please add your considerations.");
        }else{
            component.set("v.isModalOpen", false);
            var status = component.get("v.approvalRequest") == "Reject" ? false : true; 
            var recordId = component.get("v.recordId");
            var action = component.get("c.approveOrRejectDealDesk");
            var oppId = component.find("oppField").get("v.value");    
            component.set("v.spinnerOn", true);  
            action.setParams({
                "oppId": oppId,
                "recordId": recordId,
                "isApproved": status,
                "reason": reason
            });  

            action.setCallback(this, function(response){
                    var state = response.getState();  
                    var data = response.getReturnValue();
                    component.set("v.spinnerOn", false);                          
                    if (state === "SUCCESS") {  
                        console.log('entrou no success')
                        if(status === true){
                            helper.showToast("success", "Success!", "Approval Items Approved!");
                        } else {                        
                            helper.showToast("warning", "Rejected!", "Approval Items Rejected!");
                        }
                        $A.get('e.force:refreshView').fire();
                    } else {                 
                        let errors = response.getError();        
                        let message = 'Something wrong! ';  
                        if (errors && Array.isArray(errors) && errors.length > 0) {
                            if(errors[0].message.includes('FIELD_CUSTOM_VALIDATION_EXCEPTION')){
                                message += 'Please enter Deal Desk Evaluation.';
                            } else if(errors[0].message.includes('ENTITY_IS_LOCKED')) {
                                message += 'The Regional Managers needs to approval the offer first. Please try again later.';
                            } else {
                                message += errors[0].message;
                            }
                        }      
                        helper.showToast("error", "Error!", message);       
                    }
                });
            $A.enqueueAction(action);
        } 
    },

    showModalToFillJustifications: function(component, event, helper) {
        component.set("v.isModalOpen", true);
        var request = event.getSource().get("v.name").includes("reject") ? "Reject":"Approve";
        component.set("v.approvalRequest", request);                          

    },
    closeModal: function(component, event, helper) {
        component.set("v.isModalOpen", false);
                            
    }

})