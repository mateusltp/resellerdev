({
    getDealDeskOperationalCase : function(component) {
        var action = component.get("c.getDealDeskOperationalCase");
        
        action.setParams({ oppId : component.get("v.recordId") });

        action.setCallback(this, function(response) {
            var state = response.getState();

            if( state === "SUCCESS" ) {

                component.set( "v.dealDeskOperationalCase" , response.getReturnValue() );

            }else if( state === "ERROR" ) {
                var errors = response.getError();
                console.log("Couldn't find Deal desk operational case");
                console.dir(errors);
            }
        });
        $A.enqueueAction(action);
    },
})