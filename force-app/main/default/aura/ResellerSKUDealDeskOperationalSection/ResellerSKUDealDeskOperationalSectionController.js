({
    doInit : function(component, event, helper) {
        helper.getDealDeskOperationalCase(component);
    },
    
    createDealDeskOperationalCase : function(component, event, helper) {
        var actionAPI = component.find("quickActionAPI");

        var args = {
            actionName: "Opportunity.New_Deal_Desk_Operational_Case_Custom_Reseller"
        };

        actionAPI.selectAction(args).then(function(){})
            .catch(function(e){
                console.log("error selecting action");
                console.log(JSON.stringify(e.targetFieldErrors));
                console.error(e.errors);
            });
    }
})