({
    doInit: function(component, event, helper) {
        var scrollOptions = {
            left: 0,
            top: 0,
            behavior: 'smooth'
        }
        window.scrollTo(scrollOptions);
        var proposalTO = component.get("v.proposalCreationTO");
        console.log(JSON.stringify(proposalTO));
        
        var listConditions = [];
        var ddConditions = proposalTO.proposal.dealDeskConditions;
        for(var key in ddConditions){
            listConditions.push({value:ddConditions[key], key:key});
        }
        component.set("v.dealDeskConditions", listConditions);

        helper.disableSendToApproval(component, proposalTO);
        helper.disableSendOffer(component, proposalTO);
        //helper.requestOfferEvaluation(component);
        helper.checkApprovalStatus(component, proposalTO);
    },
    hideShowOfferSummaryBody: function(component, event, helper) {
        component.set("v.showOfferSummaryBody", !component.get("v.showOfferSummaryBody"));
    },

    previewOfferLetter: function(component, event, helper) {
        debugger;
        $A.createComponent(
            "c:OfferLetterPreview", {
                "proposalCreationTO": component.get("v.proposalCreationTO")
            },
            function(modal) {
                if (component.isValid()) {
                    var targetCmp = component.find('ModalDialogPlaceholder');
                    var body = targetCmp.get("v.body");
                    body.push(modal);
                    targetCmp.set("v.body", body);
                }
            }
        );
    },

    callStage : function(component, event) {        
        var cmpEvent = $A.get("e.c:fastTrackStageDoneEvent");
        var nextIndex = component.get("v.myStageIndex"); 
        var eventSourceId = event.getSource().getLocalId();
        nextIndex += eventSourceId === "btnBack" ? -1 : 1;
        cmpEvent.setParams({
            "opportunityId": component.get("v.proposalCreationTO.opportunityId"),
            "nextIndex": nextIndex
        });
        cmpEvent.fire();
        //helper.requestEvaluation(component, event);
    },

    handleSubmitForApproval: function(component, event, helper){
        var tempFields = component.find('requiredField');
        var requiredFields = [];
        if($A.util.isUndefined(tempFields.length)){
            requiredFields.push(tempFields);
        }  else {
            requiredFields.push.apply(requiredFields,tempFields);       
        }
        var allValid = requiredFields.reduce(function (validSoFar, inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && !inputCmp.get('v.validity').valueMissing;
        }, true);
        if (!allValid) {
            helper.showToast('error', 'Ops!', 'Please enter mandatory fields before submitting for approval');
            return;
        }

        var proposalTO = component.get("v.proposalCreationTO");
        var dealDeskJustification = proposalTO.proposal.dealDeskDescription;
        var commercialJustification;
        var waiverJustification;
        
        requiredFields.forEach(function(field){
            if(field.get('v.name').includes('dealDesk')){
                dealDeskJustification.push('\nRationale: ' + field.get('v.value') );
            } else if(field.get('v.name').includes('commercial')){
                commercialJustification = (field.get('v.value'));
            } else {
                waiverJustification = (field.get('v.value'));
            }
        });

        proposalTO.proposal.dealDeskDescription = dealDeskJustification;
        proposalTO.proposal.commercialJustification = commercialJustification;
        proposalTO.proposal.accessFee.waiversJustification = waiverJustification;

        // alert('proposalTO.proposal.dealDeskDescription ' + proposalTO.proposal.dealDeskDescription +
        //        '\n\nproposalTO.proposal.commercialJustification ' + proposalTO.proposal.commercialJustification +
        //        '\n\nproposalTO.proposal.accessFee.waiverJustification ' + proposalTO.proposal.accessFee.waiverJustification);
        
        console.log("TO no controller review >>> " + JSON.stringify(proposalTO));
        //if( !proposalTO ){ return; }
        helper.submitForApproval(component, proposalTO);
        
    }
})