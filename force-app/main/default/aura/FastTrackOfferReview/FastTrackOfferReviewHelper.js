({
	
	disableSendToApproval: function(component, proposalTO) {
		if( !proposalTO ){ return; }

		let disable = this.approvalNeeded(component, proposalTO);

		component.set('v.disableSendToApproval', disable);
	},
	
    disableSendOffer: function(component, proposalTO) {
		if( !proposalTO ){ return; }
		
		let disable = !this.approvalNeeded(component, proposalTO);
		component.set('v.disableSendOffer', disable);
	},
	
	approvalNeeded : function(component, proposalTO) {
		let approvalNeeded = (
			((!proposalTO.proposal.sObj.Setup_Discount_Approval_Needed__c  || ( proposalTO.proposal.sObj.Setup_Discount_Approval_Needed__c  && proposalTO.proposal.sObj.Setup_Discount_Approved__c ) )  &&
			( !proposalTO.proposal.sObj.Enterprise_Discount_Approval_Needed__c  || ( proposalTO.proposal.sObj.Enterprise_Discount_Approval_Needed__c && proposalTO.proposal.sObj.Enterprise_Subscription_Approved__c ) ) &&
			( !proposalTO.proposal.accessFee.waiversApprovalNeeded ) ) &&
			( !proposalTO.proposal.sObj.Deal_Desk_Approval_Needed__c || (proposalTO.proposal.sObj.Deal_Desk_Approval_Needed__c && proposalTO.proposal.sObj.Deal_Desk_Approved__c) )
		);

		if (proposalTO.oppRecTypeDevName == 'SMB_New_Business') {
			approvalNeeded = approvalNeeded && (!proposalTO.proposal.sObj.Enablers_Approval_Needed__c || (proposalTO.proposal.sObj.Enablers_Approval_Needed__c && proposalTO.proposal.sObj.Enablers_Approved__c) );
		}

		return approvalNeeded;
	},

	approvalNeededComConditions : function(component, proposalTO) {
		let approvalNeeded = (
			( !proposalTO.proposal.sObj.Setup_Discount_Approval_Needed__c  || ( proposalTO.proposal.sObj.Setup_Discount_Approval_Needed__c  && proposalTO.proposal.sObj.Setup_Discount_Approved__c ) )  &&
			( !proposalTO.proposal.sObj.Enterprise_Discount_Approval_Needed__c  || ( proposalTO.proposal.sObj.Enterprise_Discount_Approval_Needed__c && proposalTO.proposal.sObj.Enterprise_Subscription_Approved__c ) ) &&
			( !proposalTO.proposal.accessFee.waiversApprovalNeeded )
		);

		if (proposalTO.oppRecTypeDevName == 'SMB_New_Business') {
			approvalNeeded = approvalNeeded && (!proposalTO.proposal.sObj.Enablers_Approval_Needed__c || (proposalTO.proposal.sObj.Enablers_Approval_Needed__c && proposalTO.proposal.sObj.Enablers_Approved__c) );
		}

		return approvalNeeded;
	},

	requestOfferEvaluation : function(component) {
		console.log("requestOfferEvaluation");
		var cmpEvt = $A.get("e.c:FastTrackEvaluationRequestEvent");	
		cmpEvt.setParams({"isDealDesk" : true, "isCommercialConditions" : true });
		cmpEvt.fire();

    },
	
	checkApprovalStatus: function(component, proposalTO){
		if( !proposalTO ){ return; }	
		
		if(	proposalTO.proposal.sObj.Deal_Desk_Approval_Needed__c){ 			
			if(!proposalTO.proposal.sObj.Deal_Desk_Approved__c) {
				component.set("v.iconNameDealDesk", "utility:close");
				component.set("v.variantDealDesk", "error");
				component.set("v.iconNameDealDeskReady", "utility:clock");
				component.set("v.variantDealDeskReady", "Informational");		
			} else {
				component.set("v.iconNameDealDesk", "utility:check");
				component.set("v.variantDealDesk", "success");
				component.set("v.iconNameDealDeskReady", "utility:success");
				component.set("v.variantDealDeskReady", "success");
			}	
		} else {
				component.set("v.iconNameDealDesk", "utility:check");
				component.set("v.variantDealDesk", "success");
				component.set("v.iconNameDealDeskReady", "utility:success");
				component.set("v.variantDealDeskReady", "success");
		}	

		if(proposalTO.proposal.sObj.Enterprise_Discount_Approval_Needed__c){ 
			if(proposalTO.proposal.sObj.Enterprise_Subscription_Approved__c){ 
				component.set("v.iconNameEnterprise", "utility:check");
				component.set("v.variantEnterprise", "success");
			} else {
				component.set("v.iconNameEnterprise", "utility:close");
				component.set("v.variantEnterprise", "error");
			}			
		}  else {
				component.set("v.iconNameEnterprise", "utility:check");
				component.set("v.variantEnterprise", "success");
		}
      
		if(proposalTO.proposal.sObj.Setup_Discount_Approval_Needed__c){ 
			if(proposalTO.proposal.sObj.Setup_Discount_Approved__c){
				component.set("v.iconNameSetup", "utility:check");
				component.set("v.variantSetup", "success");
			} else {
				component.set("v.iconNameSetup", "utility:close");
				component.set("v.variantSetup", "error");
			}			

		}  else {
				component.set("v.iconNameSetup", "utility:check");
				component.set("v.variantSetup", "success");
		}
		
		if(proposalTO.proposal.accessFee.waiversApprovalNeeded){				
			component.set("v.iconNameWaiver", "utility:close");
			component.set("v.variantWaiver", "error");
		} else {
			component.set("v.iconNameWaiver", "utility:check");
			component.set("v.variantWaiver", "success");
		}

		if(proposalTO.proposal.sObj.Enablers_Approval_Needed__c){ 
			if(proposalTO.proposal.sObj.Enablers_Approved__c){ 
				component.set("v.iconNameEnablers", "utility:check");
				component.set("v.variantEnablers", "success");
			} else {
				component.set("v.iconNameEnablers", "utility:close");
				component.set("v.variantEnablers", "error");
			}			
		}  else {
				component.set("v.iconNameEnablers", "utility:check");
				component.set("v.variantEnablers", "success");
		}
		
		if(this.approvalNeededComConditions(component, proposalTO)) {
			component.set("v.iconNameCommercial", "utility:success");
			component.set("v.variantCommercial", "success");
		} else {
			component.set("v.iconNameCommercial", "utility:clock");
			component.set("v.variantCommercial", "Informational");
		}

	},

	submitForApproval: function(component, proposalTO){
		var action = component.get("c.submitForApproval");
		component.set("v.spinnerOn", true);  
		console.log('SUBMIT DEAL DESK >> ' + proposalTO);
		console.dir(proposalTO);
		console.log(proposalTO.dealDeskApprovalNeeded);
		
        action.setParams({
            "jsonTO": JSON.stringify(proposalTO)
		});  
		
        action.setCallback(this, function(response){
                var state = response.getState();  
                var data = response.getReturnValue();
                component.set("v.spinnerOn", false);                          
                if (state === "SUCCESS") {  					
					this.showToast("success", "Success!", "Your offer is pending approval");
                    $A.get('e.c:RefreshMainFastTrackCmpEvent').fire();
                }else {
					var errors = response.getError();
					var msg = "Error ";
					if (errors) {
						if (errors[0] && errors[0].message) {							
							msg = errors[0].message.includes('ALREADY_IN_PROCESS') ? 'Offer already in approval process - Contact Admin for help.' :  errors[0].message;
						}
					} 
				   this.showToast("error", "Error!", msg);
                }
              });
        $A.enqueueAction(action);
	},
	showToast : function(type,title, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "type": type,
            "message":message
        });
        toastEvent.fire();
    }

})