({
	doInit : function(component, event, helper) {
		var action = component.get("c.createSalesOrder");
        var recordId = component.get("v.recordId");
        debugger;
        action.setParams({ recordId : recordId });
        action.setCallback(this, function(response) {
            var state = response.getState();
            debugger;
            if (state === "SUCCESS") {
                var returnValue = response.getReturnValue();
                
                if (returnValue.status == "SUCCESS") {
                	var navEvt = $A.get("e.force:navigateToSObject");
    				navEvt.setParams({
      					"recordId": returnValue.salesOrderId
    				});
    				navEvt.fire();
                } else {
                    component.set("v.errorMessage", returnValue.errorMessage);
                }
            }
            else if (state === "INCOMPLETE") {
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);	
	}
})