({
    loadOppSubtype : function(component, helper) {
        var action = component.get("c.getFieldOptions");
        action.setParams({
            "objName": "Opportunity",
            "fieldApiName": "Sub_Type__c",
            "RecordTypeDevName" : component.get("v.selectedRT"),
            "isDependentPickLst" : false
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if ( state === "SUCCESS" ) {
                var lstPicklistOptions = JSON.parse(response.getReturnValue());                            
                component.set( "v.lstOppSubtypes" , lstPicklistOptions);  
                component.set( "v.selectedSubtype" ,lstPicklistOptions[0].value);
                this.loadOpportunitySource(component, helper);
            } else if ( state === "ERROR" ) {
                var errors = response.getError();
                if ( errors && errors[0] && errors[0].message ) {
                    helper.showToast("Error!", errors[0].message, "error");
                    helper.showToast( "Error!" , "Error loading opportunity sub-types. Please, contact your administrator." , "error" );
                } else {
                    console.log("Unknown error");
                    helper.showToast( "Error!" , "Unknown error. Please, contact your administrator." , "error" );
                }                
                component.set( "v.spinnerOn" , false );
            }
        });

        $A.enqueueAction(action);
    },   

    loadOpportunitySource : function(component, helper) {
        var action = component.get("c.getFieldOptions");
        action.setParams({
            "objName": "Opportunity",
            "fieldApiName": "Source__c",
            "RecordTypeDevName" : component.get("v.selectedRT"),
            "isDependentPickLst" : false
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if ( state === "SUCCESS" ) {
                var lstPicklistOptions = JSON.parse(response.getReturnValue());                                                            
                component.set( "v.listOppSource" , lstPicklistOptions);  
                component.set( "v.selectedOppSource" ,lstPicklistOptions[0].value);
            } else if ( state === "ERROR" ) {
                var errors = response.getError();
                if ( errors && errors[0] && errors[0].message ) {
                    helper.showToast("Error!", errors[0].message, "error");
                    helper.showToast( "Error!" , "Error loading opportunity Source. Please, contact your administrator." , "error" );
                } else {
                    console.log("Unknown error");
                    helper.showToast( "Error!" , "Unknown error. Please, contact your administrator." , "error" );
                }
            }
            component.set( "v.spinnerOn" , false );
        });

        $A.enqueueAction(action);
    },   

    fetchPicklistValues: function( component, helper) {       
        // call the server side function  
        var action = component.get("c.getDependentPickList");
        // pass paramerters [object definition , contrller field name , record type name] -
        // to server side function 

        //String objName, String fieldApiName, String RecordTypeDevName, Boolean isDependentPickLst
        action.setParams({
            "objName" : 'Opportunity',
            "fieldApiName": 'Cancellation_Reason_subcategory__c',
            "RecordTypeDevName": component.get("v.selectedRT")
        });
        
        //set callback   
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                
                // Returned a map
                let responseObj = response.getReturnValue();
			             
                // Set the attribute with returned response
                component.set( "v.depnedentFieldMap", responseObj );
                
                // Prepare a list of controlling values
                let controllerFieldValues = []; 
                
                for (var cValue in responseObj ) {
                    controllerFieldValues.push( cValue );
                }
                
                if ( controllerFieldValues.length > 0) {
                    controllerFieldValues.unshift(['']);
                }
                
                // set the controllerFieldValues variable values to continent(controller picklist field)
                component.set("v.listControllingValues", controllerFieldValues);
                component.set( "v.spinnerOn" , false ); 
            }
            else{
                component.set( "v.spinnerOn" , false ); 
                alert('Something went wrong..');
            }
        });
        
        $A.enqueueAction(action);
    },
    
    fetchDepValues: function( component, dependentFieldOptions ) {

        let dependentFields = [];
        //dependentFields.push('--- None ---');
        
        for ( let i = 0; i < dependentFieldOptions.length; i++ ) {
            dependentFields.push( dependentFieldOptions[i] );
        }               
        //dependentFields[0].selected = true;        
        component.set("v.selectedCancelSubCateg", dependentFieldOptions[0]);
        component.set( "v.listDependingValues", dependentFields );        
    },

    showToast: function( title , message , type ){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: title,
            message: message,
            type: type
        });
        toastEvent.fire();
    },
    
    redirectNewOpp: function( clonedOppId ){
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": clonedOppId
        });
        navEvt.fire();
    }
})