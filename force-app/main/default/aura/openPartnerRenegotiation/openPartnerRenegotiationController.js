({
    doInit : function(component, event, helper) {
        component.set( "v.spinnerOn" , true );         
    }, 

    setAttributes : function(component, event, helper) {
        component.set("v.selectedRT", component.get("v.accountRecord").Wishlist__c ? 'Partner_Wishlist_Renegotiation' : 'Partner_Small_and_Medium_Renegotiation');
        component.set("v.businessType", component.get("v.accountRecord").Wishlist__c ? 'Wishlist' : 'Longtail');
        helper.loadOppSubtype( component , helper );       
    }, 

    handleSubTypeChange: function (component, event, helper) {           
        var selectedOption = event.getParam("value");
        component.set("v.selectedSubtype", selectedOption);
        component.set("v.selectedCancelReason", null);       
        component.set("v.selectedCancelSubCateg", null);      
        var listControllingValues = component.get("v.listControllingValues");    
        var listDependingValues = component.get("v.listDependingValues");     
        if(selectedOption == 'Retention' && (listControllingValues.length == 0 || listDependingValues.length == 0  )){
            component.set( "v.spinnerOn" , true ); 
            helper.fetchPicklistValues( component , helper);   
        }
    },

    handleOppSourceChange: function (component, event, helper) {           
        var selectedOption = event.getParam("value");
        component.set("v.selectedOppSource", selectedOption);
    },
  
    close : function(component, event, helper) {
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
    },

    save : function(component, event, helper) {   
       if(component.get("v.selectedSubtype") == 'Retention' && (component.get("v.selectedCancelReason") == null || component.get("v.selectedCancelReason").length < 1)){
            component.find("cancelReasonId").showHelpMessageIfInvalid();
            return;
        }
        var action = component.get("c.createNewPartnerRenegotiationOpp"); 
        component.set( "v.spinnerOn" , true );   
        action.setParams({
            "aAccId": component.get("v.recordId"),
            "aRecordTypeDevName":  component.get( "v.selectedRT"),
            "aSubTypeValue" : component.get("v.selectedSubtype"),
            "aCancellationReason" : component.get("v.selectedCancelReason"),
            "aCancellationSubCateg" : component.get("v.selectedCancelSubCateg"),
            "aOppSource" : component.get("v.selectedOppSource")
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if ( state === "SUCCESS" ) {
                var clonedOppId = response.getReturnValue();
                helper.showToast( "Success!" , "Renegotiation Opportunity created." , "success" );
                helper.redirectNewOpp( clonedOppId );
            } else if ( state === "ERROR" ) {
                var errors = response.getError();
                if ( errors && errors[0] && errors[0].message ) {
                    helper.showToast("Error!", errors[0].message, "error");
                    helper.showToast( "Error!" , "Error creating opportunity. Please, contact your administrator." , "error" );
                } else {
                    console.log("Unknown error");
                    helper.showToast( "Error!" , "Unknown error. Please, contact your administrator." , "error" );
                }
            }
            component.set( "v.spinnerOn" , false );
        });
        $A.enqueueAction(action);      
    },

    onControllerFieldChange: function(component, event, helper) {  
        component.set("v.selectedCancelReason", null);       
        component.set("v.selectedCancelSubCateg", null);           
        var selectedControllerValue = event.getSource().get("v.value"); 
        component.set("v.selectedCancelReason", selectedControllerValue);   
        var depnedentFieldMap = component.get("v.depnedentFieldMap"); 

        if ( selectedControllerValue != null && selectedControllerValue != ['']) {            
            let dependentFieldOptions = depnedentFieldMap[selectedControllerValue];
            
            if( dependentFieldOptions.length > 0 ) {                
                component.set("v.bDisabledDependentFld" , false);                  
                helper.fetchDepValues( component, dependentFieldOptions );                  
            }
            else{                
                component.set("v.bDisabledDependentFld" , true); 
                component.set("v.listDependingValues", ['']);              
            }              
        } 
        else {            
            component.set("v.listDependingValues", ['']);
            component.set("v.bDisabledDependentFld" , true);
        }
    },

    onDependentFieldChange: function(component, event, helper){
        var selectedDependentValue = event.getSource().get("v.value"); 
        component.set("v.selectedCancelSubCateg", selectedDependentValue);

    }
})