({
	loadData : function(component, helper, payment) {
		helper.setParams(component, helper, payment);
		if (component.get("v.isEnterpriseSubscription")) {
			helper.loadEligibilities(component, helper, payment);
			helper.loadWaivers(component, helper, payment);
		} 																												
	},
	setParams : function(component, helper, payment) {
		component.set("v.paymentId", payment.sObj.Id);
	},
	loadWaivers : function(component, helper, payment) {
		var waivers = payment.waivers;
		for (var i=0; i<waivers.length; i++) {
			helper.loadWaiverCmp(component, waivers[i]);
		}
	},
	loadEligibilities : function(component, helper, payment) {
		var eligibilities = payment.eligibilities;
		for (var i=0; i<eligibilities.length; i++) {
			helper.loadEligibilityCmp(component, eligibilities[i]);
		}
	},
	loadWaiverCmp : function(component, waiver) {
		$A.createComponent(
            "c:OfferSummaryWaiver", {
				"proposalCreationTO" : component.get("v.proposalCreationTO"),
                "waiver": waiver
            },
            function(childCmp, status, error) {
                if (component.isValid()) {
                    var targetCmp = component.find('waiverCmp');
                    var body = targetCmp.get("v.body");
					body.push(childCmp);
                    targetCmp.set("v.body", body);
                }
            }
        );																			
	},
	loadEligibilityCmp : function(component, eligibility) {
		$A.createComponent(
            "c:OfferSummaryEligibility", {
				"proposalCreationTO" : component.get("v.proposalCreationTO"),
				"eligibility": eligibility,
				"payment" : component.get("v.payment"),
				"isEnterpriseSubscription": component.get("v.isEnterpriseSubscription")
            },
            function(childCmp, status, error) {
                if (component.isValid()) {
                    var targetCmp = component.find('eligibilityCmp');
                    var body = targetCmp.get("v.body");
                    body.push(childCmp);
                    body.push("<div aura:id=\"eligibilityCmp\"></div>");
                    targetCmp.set("v.body", body);
                }
            }
        );																			
	},
	openAddPayment : function(component, helper) {
		$A.createComponent(
            "c:OfferSummaryAddPayment", {
				"proposalCreationTO" : component.get("v.proposalCreationTO"),
				"isUpdate" : true,
				"payment" : component.get("v.payment"),
				"paymentId" : component.get("v.paymentId"),
				"isEnterpriseSubscription" : component.get("v.isEnterpriseSubscription"),
				"isSetupFee" : component.get("v.isSetupFee"),
				"isProfessionalServicesSetupFee" : component.get("v.isProfessionalServicesSetupFee"),
				"isProfessionalServicesMaintenanceFee" : component.get("v.isProfessionalServicesMaintenanceFee")
            },
            function(modal, status, error) {
                if (component.isValid()) {
					component.find('overlayLib').showCustomModal({
						header: "Payment",
						body: modal,
						showCloseButton: true
					})
                }
            }
        );
	},
	//openAddWaiver : function(component, helper, product) {
	openAddWaiver : function(component, helper) {
		$A.createComponent(
            "c:OfferSummaryAddWaiver", {
				"proposalCreationTO" : component.get("v.proposalCreationTO"),
				"payment" : component.get("v.payment")
            },
            function(modal, status, error) {
                if (component.isValid()) {
					component.find('overlayLib').showCustomModal({
						header: "Waiver or Temporary Discount",
						body: modal,
						showCloseButton: true
					})
                }
            }
        );
	},
	//openAddEligibility : function(component, helper, product) {
	openAddEligibility : function(component, helper) {
		$A.createComponent(
            "c:OfferSummaryAddEligibility", {
				"proposalCreationTO" : component.get("v.proposalCreationTO"),
				"payment" : component.get("v.payment"),
				"isUpdate" : false
            },
            function(modal, status, error) {
                if (component.isValid()) {
					component.find('overlayLib').showCustomModal({
						header: "Eligibility",
						body: modal,
						showCloseButton: true
					})
                }
            }
        );
	}
})