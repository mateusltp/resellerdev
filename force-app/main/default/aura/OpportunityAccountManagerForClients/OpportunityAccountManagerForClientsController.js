({
    doInit : function(component, event, helper) {      
        component.set('v.columns', [
            {label: 'Account Name', initialWidth: 200, fieldName: 'linkName', type: 'url',
                typeAttributes: {label: { fieldName: 'Name' },value:{fieldName: 'linkName'}, target: '_blank'}
            },
            {label: 'City', fieldName: 'BillingCity', type: 'text'},
            {label: 'State', fieldName: 'BillingState', type: 'text'},
            {label: 'Country', fieldName: 'BillingCountry', type: 'text'}
        ]);
       
       component.set('v.recordId', component.get("v.proposalCreationTO.opportunityId"));
       component.set('v.columnsAor', helper.getDealSplitBillingColumns(component, event, helper));   
       helper.setChildAccounts(component, event, helper);       
    },

    onChangefilter: function(component, event, helper){
        helper.onChangeFilter(component, event, helper);
    },

    openPanel: function(component, event, helper){
       // let isOpen = component.get("v.isOpen");
       // component.set("v.isOpen", !isOpen);
        let filterField = component.find('aorFilterId');
        filterField.set('v.disabled',false);

    },

    handleSubmit : function(component, event, helper) {       
        
        helper.handleSubmit(component, event, helper);        
    },

    handleRemove : function(component, event, helper) {       
        helper.handleRemove(component, event, helper);        
    },

    save : function(component, event, helper) {
        var drafValues = event.getParam('draftValues');
		var feeTypes = [];
        
        if (drafValues.length > 0) {
            if (drafValues[0].hasOwnProperty('Billing_Percentage__c'))                 		feeTypes.push('Billing_Percentage__c'); 
            if (drafValues[0].hasOwnProperty('Maintenance_Fee_Billing_Percentage__c')) 		feeTypes.push('Maintenance_Fee_Billing_Percentage__c');
            if (drafValues[0].hasOwnProperty('Setup_Fee_Billing_Percentage__c')) 			feeTypes.push('Setup_Fee_Billing_Percentage__c');
            if (drafValues[0].hasOwnProperty('Prof_Serv_Setup_Fee_Billing_Percentage__c')) 	feeTypes.push('Prof_Serv_Setup_Fee_Billing_Percentage__c');
            if (drafValues[0].hasOwnProperty('Razao_Social__c')) 	                        feeTypes.push('Razao_Social__c');
            if (drafValues[0].hasOwnProperty('Id_Company__c')) 	                            feeTypes.push('Id_Company__c');
        }
		
        if (feeTypes.length > 0) {
            if( helper.isValidLegalFields( component, helper, drafValues) ){
                if( helper.isValidPercentages( component, helper, drafValues/*, feeTypes */) ){
                    helper.saveAccOppRel( component , helper );
                }
            }         
        }       
    }
})