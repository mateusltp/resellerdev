({
    handleSubmit : function(component, event, helper) {
        component.set('v.spinnerOn', true);
    },

    handleError : function(component, event, helper) {
        component.set('v.spinnerOn', false);
    },

    handleSuccess : function(component, event, helper) {
        component.set('v.spinnerOn', false);
        helper.showToast('success', 'Success', 'Changes applied');
    }
})