/** cloned from OfferSummaryFlow.cmp */
({
    init: function(cmp, event, helper){
        helper.createFlowComponent(cmp);
    },

    handleMessage: function(cmp, message, helper){
        helper.refreshFlow(cmp);
    }
})