/** cloned from OfferSummaryFlow.cmp */
({
    createFlowComponent: function(cmp) {
        $A.createComponent(
            "c:ResellerOfferSummaryFlowChild",
            {
                "aura:id": "flowAura",
                "recordId": cmp.get('v.recordId'),
                "fastTrackStages": cmp.get('v.fastTrackStages')
            },
            function(offerSummaryFlow, status, errorMessage){
                if (status === "SUCCESS") {
                    var body = cmp.get("v.body");
                    body.push(offerSummaryFlow);
                    cmp.set("v.body", body);
                }
                else if (status === "INCOMPLETE") {
                    console.log("No response from server or client is offline.")
                    // Show offline error
                }
                else if (status === "ERROR") {
                    console.log("Error: " + errorMessage);
                    // Show error message
                }
            }
        );
    },

    refreshFlow: function(cmp) {
        cmp.find('flowAura').destroy();
        this.createFlowComponent(cmp);
    }
})