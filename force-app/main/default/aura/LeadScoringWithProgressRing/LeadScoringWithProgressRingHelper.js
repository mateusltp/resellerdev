({
    doInit : function(component, event, helper) {
        helper.calculateScoreProgress(component, event, helper)
    },

    calculateScoreProgress:function(component, event, helper) { 
        var score = component.get("v.leadRecord.Referral_Score__c");
        score = score/100.0;
        var totalscore = score * 100;
        
     
        var themeTemp = 'red';
        var totalDeg = score * 360;
        
        if(score <= 0.25 ){
            themeTemp = 'red';
        }else if (score > 0.25 && score <= 0.5 ) {
            themeTemp = 'orange';
        }else if (score > 0.5 && score <= 0.75 ) {
            themeTemp = 'yellow';  
        } else {
            themeTemp = 'green';
       }
    
       component.set('v.score', totalscore.toFixed(1));
       component.set('v.theme', themeTemp);
       component.set('v.cirDeg', totalDeg);

    }


})