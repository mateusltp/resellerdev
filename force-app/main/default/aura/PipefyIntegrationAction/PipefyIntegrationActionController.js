({
    doInit: function(component, event, helper){
        
    },
    
    handleContinue : function(component, event, helper) {
        component.set("v.spinner", true);
		helper.pipefyIntegration(component, event, helper);
    },
    
    handleCancel: function(component, event, helper){
        $A.get("e.force:closeQuickAction").fire();
    }
})