({
	pipefyIntegration : function(component, event, helper) {
		console.log(component.get("v.recordId"));
        console.log(component.get("v.sObjectName"));
        var action = component.get("c.auraHandler");
        action.setParams({ recordId:component.get("v.recordId"),
                          sObjectName:component.get("v.sObjectName")});
        action.setCallback(this, function(response){
            if(response.getState() === 'SUCCESS'){
                if(response.getReturnValue() === 200 || response.getReturnValue() === 201){
                    component.set("v.spinner", false);
                    helper.showToast('success', 'Success!', 'Successfully created card on Pipefy.');
                    $A.get("e.force:closeQuickAction").fire();
                }else{
                    component.set("v.spinner", false);
                    helper.showToast('error', 'Error!', 'Failed to create card on Pipefy. Contact system administrator.');
                    $A.get("e.force:closeQuickAction").fire();
                }
            }else if (response.getState() === 'ERROR') {
                component.set("v.spinner", false);
                helper.handleErrors(response.getError(), helper);
                $A.get("e.force:closeQuickAction").fire();
            }
        });
        $A.enqueueAction(action);
	},
    
    handleErrors: function(errors, helper) {
        var message = "Unkown error. Contact system administrator.";
        if (errors && Array.isArray(errors) && errors.length > 0) {
            message = errors[0].message;
        }
        helper.showToast('error', 'Error', message);
    },
    
    showToast: function(type, title, message) {
        let toastParams = {
            title: title,
            message: message,
            type: type
        };
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams(toastParams);
        toastEvent.fire();
    }
})