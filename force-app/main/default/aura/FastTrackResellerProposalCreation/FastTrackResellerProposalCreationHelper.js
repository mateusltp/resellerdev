// ({

//     /**
//      * @author Rafael Carvalho
//      * @description check if the opp has the quote object.
//      */
//     init : function(cmp, evt, hpr){
//         let action = cmp.get('c.hasProposalInOpp');
//         action.setParams({
//             'oppId' : cmp.get('v.recordId')
//         });
//         this.promise(action)
//         .then(
//             $A.getCallback( result =>{
//                 console.log('hasQuote : ' + result);
//                 cmp.set('v.hasQuote', result);
//                 if(result == true)
//                     hpr.initLoad(cmp, evt, hpr);
//             }),
//             $A.getCallback( error =>{
//                 console.log('error init : ' + JSON.stringify(error));
//             })
//         )
//         .catch( error =>{
//             console.log('catch error init : ' + JSON.stringify(error));
//         });

//     },

//     initLoad : function(cmp, evt, hpr){
//         cmp.set("v.spinnerOn", true);
//         Promise.all([
//             hpr.load(cmp, cmp.get("v.recordId"), hpr),
//             hpr.getStageCmpMappingMetadata(cmp, cmp.get("v.recordId"), hpr)
//           ]).then(
//               function(response) {
//                 hpr.renderFastTrackStageCmp(cmp);
//                 $A.get('e.force:refreshView').fire();
//                 cmp.set("v.spinnerOn", false);
//               }
//           ).catch(
//               function(error) {
//                 hpr.showToast("error", "Error!", error);
//                 cmp.set("v.spinnerOn", false);
//               }
//           );
//     },

//     load: function(component, recordId, helper) {
//         return new Promise(function(resolve, reject){
//             let callback = function (response) {
//                 if (helper.isResponseSuccessful(response)) {                    
//                     component.set("v.proposalCreationTO", response.getReturnValue());
//                     if ( response.getReturnValue().proposal.sObj.Id == null ){
//                         var cmpEvent = $A.get("e.c:saveOffer");
//                         cmpEvent.setParams({
//                             "changedProposalCreationTO": component.get("v.proposalCreationTO")
//                         });
//                         cmpEvent.fire();
//                     }
//                     console.log("proposalCreationTO " +  response.getReturnValue());
//                     console.dir(response.getReturnValue());
//                     helper.fireRefreshEnablers(component);
//                     resolve();
//                 } else {
//                     reject("Error while loading this component. Contact your administrator");
//                 }                
//                 $A.get('e.force:refreshView').fire();
//                 component.set("v.spinnerOn", false);
//             };
//             let loadService = component.get("c.find");
//             loadService.setParams({
//                 'recordId': recordId
//             });
//             loadService.setCallback(this, callback);        
//             $A.enqueueAction(loadService);
//         });
//     },
    
//     loadGympassEntities: function(component, callback, recordId) {
//         let gympassEntitiesService = component.get("c.findGympassEntities");
//         gympassEntitiesService.setCallback(this, callback);        
//         $A.enqueueAction(gympassEntitiesService);
//     },

//     loadClientManagers: function(component, callback, recordId) {
//         let clientManagersService = component.get("c.findClientManagers");
//         clientManagersService.setParams({
//             'recordId': recordId
//         });        
//         clientManagersService.setCallback(this, callback);        
//         $A.enqueueAction(clientManagersService);
//     },
    
//     renderFastTrackStageCmp: function(component) {
//         let cmpInfo = this.getCmpInformation(component);
//         let _self = this;
//         $A.createComponent(
//             cmpInfo.cmpName,
//             cmpInfo.params,
//             function(newComponent, status, errorMessage){
//                 if (status === "SUCCESS") {
//                     var body = component.get("v.body");
//                     body = newComponent;
//                     component.set("v.body", body);
//                 }
//                 else if (status === "INCOMPLETE") {
//                     _self.showToast("error", "Error!", "No response from server or client is offline.");
//                 }
//                 else if (status === "ERROR") {
//                     _self.showToast("error", "Error!", errorMessage);
//                 }
//             }
//         );
//         component.set("v.spinnerOn", false);
//     },

//     renderEnablersCmp: function(component) {
//         $A.createComponent(
//             "c:FastTrackEnablers", {
//                 "proposalCreationTO": component.get("v.proposalCreationTO")
//             },
//             function(childComponent) {
//                 if (component.isValid()) {
//                     debugger;
//                     var targetCmp = component.find('enablersCmp');
//                     var body = targetCmp.get("v.body");
//                     body.push(childComponent);
//                     targetCmp.set("v.body", body);
//                 }
//             }
//         );
//     },

//     getStageCmpMappingMetadata: function(component, recordId, helper) {
//         return new Promise(function(resolve, reject){
//             let callback = function (response) {
//                 if (helper.isResponseSuccessful(response)) {
//                     component.set("v.fastTrackStageMapping", response.getReturnValue());
//                     resolve();
//                 } else {
//                     reject('Error finding configuration metadata. Contact your administrator!');
//                 }
//             };

//             let loadService = component.get("c.getStageCmpMappingMetadata");
//             loadService.setParams({
//                 'recordId': recordId
//             }); 
//             loadService.setCallback(this, callback);        
//             $A.enqueueAction(loadService);
//         });
//     },

//     createProposal : function(cmp, evt, hpr){
//         cmp.set("v.spinnerOn", true);
//         let saveService = cmp.get("c.resellerCreateProposal");
//         console.log('cmp.get("v.recordId") : ' + cmp.get("v.recordId"));
//         saveService.setParams({
//             'oppId': cmp.get("v.recordId")
//         });
//         this.promise(saveService)
//         .then(
//             result =>{
//                 cmp.set("v.spinnerOn", false);
//                 console.log('SUCCESSO : ' + result);
//                 this.showToast('success', 'Concluído', 'Proposta Criada com Sucesso!');
//                 this.init(cmp, evt, hpr);
//             },
//             $A.getCallback( error =>{
//                 cmp.set("v.spinnerOn", false);
//                 let js = JSON.stringify(error);
//                 console.log('Error : ' + JSON.stringify(error));
//                 this.showToast('error', 'Erro' , error[0] ? error[0].message : 'Unknown error. Please contact support.');
                
//             })
//         )
//         .catch( error =>{
//             component.set("v.spinnerOn", false);
//             console.log('CATCH ERROR : ' + JSON.stringify(error));
//         })
//     },

//     getCmpInformation : function(component) {
//         var cmpInfo = {
//             cmpName : "lightning:formattedText",
//             params : {
//                 class : "slds-align_absolute-center slds-text-heading_small",
//                 value : $A.get("$Label.c.FastTrackNoPageFound")
//             }
//         };
//         let proposalCreationTO = component.get("v.proposalCreationTO");
//         let lstStageMapping = component.get("v.fastTrackStageMapping");

//         if(proposalCreationTO && proposalCreationTO.fastTrackStage && lstStageMapping && lstStageMapping.length) {

//             let stageCmpMapped = lstStageMapping.find(element => element.Label == proposalCreationTO.fastTrackStage);

//             if( !stageCmpMapped ) { return cmpInfo; }

//             cmpInfo = { "cmpName" : "c:" + stageCmpMapped.ChildComponentApiName__c };

//             if( !stageCmpMapped.ChildComponentParameters__c ) { return cmpInfo; }

//             let createParams = {};
//             let cmpParameters = stageCmpMapped.ChildComponentParameters__c.split(',');

//             cmpParameters.forEach(
//                 function(param) {
//                     param = param.split(':');
//                     if(param[0]) { createParams[param[0]] = component.get("v." + (param.length > 1 ? param[1] : param[0]) ); }
//                 });
            
//             cmpInfo.params = createParams;
//         }

//         console.log('CMP - INFO' + JSON.stringify(cmpInfo));

//         return cmpInfo;
//     },

//     showToast : function(type, title, message) {
//         var toastEvent = $A.get("e.force:showToast");
//         toastEvent.setParams({
//             "title": title,
//             "message": message,
//             "type": type
//         });
//         toastEvent.fire();
//     },

//     // save: function(component, callback, recordId, proposal) {        
//     //     let saveService = component.get("c.saveProposal");
//     //     var newAccountsInOpp = null;
        
//     //     saveService.setParams({
//     //         'proposalData': JSON.stringify(proposal)
//     //     });  

//     //     saveService.setCallback(this, callback);        
//     //     $A.enqueueAction(saveService);
//     // },

//     save : function(component, callback, recordId, proposal, hpr){
//         let saveService = component.get("c.saveProposal");
//         saveService.setParams({
//             'proposalData': JSON.stringify(proposal)
//         });
//         this.promise(saveService)
//         .then(
//              result =>{
//                 component.set("v.proposalCreationTO", result);
//                 component.set("v.spinnerOn", false);
//                 console.log('SAVE FINAL');
//                 this.showToast('success', 'Concluído', 'Salvo com sucesso');
//                 $A.get('e.force:refreshView').fire();
//             }
//             // $A.getCallback( error =>{
//             //     let js = JSON.stringify(error);
//             //     console.log('Error : ' + JSON.stringify(error));
//             //     component.set("v.spinnerOn", false);
                
//             //         this.showToast('error', 'Erro' , error[0] ? error[0].message : 'Unknown error. Please contact support.');
//             // })
//         )
//         .catch( error =>{
//             component.set("v.spinnerOn", false);
//             console.log('CATCH ERROR : ' + JSON.stringify(error));
//         })
//     },

//     saveAndRefresh: function (component, event, helper){   
//         console.log('saveAndRefresh'); 
//         component.set("v.spinnerOn", true); 
//         let callback = function (response) { 
//           if (helper.isResponseSuccessful(response)) {     
             
//             //helper.load(component, component.get("v.recordId"), helper)
//             $A.enqueueAction(component.get("c.doRefreshView"));
//           }else{
//             this.showToast('error', 'Erro' , response.getError()[0] ? response.getError()[0].message : 'Unknown error. Please contact support.');
//             component.set("v.spinnerOn", false); 
//           }       
          
          
//         };    
//         this.save(component, callback, component.get("v.recordId"), component.get("v.proposalCreationTO"), helper);
//       },

//       doRefreshView: function(component, event, helper){
//         console.log('doRefreshView');
//         this.resetCommercialConditions(component, component.get("v.recordId"), helper);   
//       },

//     promise : function(action)
//     {
//         return new Promise(function(resolve,reject)
//         {
//             action.setCallback(this,function(response)
//             {
//                 if(response.getState() === 'SUCCESS')resolve(response.getReturnValue());
//                 else reject(response.getError());
//             });
//             $A.enqueueAction(action);
//         });
//     },
    
//     isResponseSuccessful : function(response) {
//         return (response.getState() === "SUCCESS");
//     },
    
//      fireRefreshEnablers : function(component)  {
//         var cmpEvent = $A.get("e.c:reloadEnablersEvent");
//         cmpEvent.setParams({
//             "proposalCreationTO": component.get("v.proposalCreationTO")
//         });
//         cmpEvent.fire();
//     },

//     resetCommercialConditions : function(component, record, helper){       
//         let action = component.get("c.resetCommercialConditions");
//         action.setParams({
//             'to': component.get("v.proposalCreationTO")
//         });        
//         action.setCallback(this, function(response){
//             var state = response.getState();                         
//             if (state === "SUCCESS") {      
//                 this.load(component,record, helper);
//             }else{
//                 component.set("v.spinnerOn", true);
//                 return;
//             }
//           });
//         component.set("v.spinnerOn", true);
//         $A.enqueueAction(action);
       
//     }
// })


({
    load: function(component, recordId, helper) {
        return new Promise(function(resolve, reject){
            let callback = function (response) {
                if (helper.isResponseSuccessful(response)) {                    
                    component.set("v.proposalCreationTO", response.getReturnValue());
                    if ( response.getReturnValue().proposal.sObj.Id!=null ){component.set('v.hasQuote', true);}
                    if ( response.getReturnValue().proposal.sObj.Id==null ){
                        var cmpEvent = $A.get("e.c:saveOffer");
                        cmpEvent.setParams({
                            "changedProposalCreationTO": component.get("v.proposalCreationTO")
                        });
                        //cmpEvent.fire();
                    }
                    console.log("proposalCreationTO " +  response.getReturnValue());
                    console.dir(response.getReturnValue());
                    helper.fireRefreshEnablers(component);
                    resolve();
                } else {
                    reject("Error while loading this component. Contact your administrator");
                }                
                $A.get('e.force:refreshView').fire();
                component.set("v.spinnerOn", false);
            };
            let loadService = component.get("c.find");
            loadService.setParams({
                'recordId': recordId
            });
            loadService.setCallback(this, callback);        
            $A.enqueueAction(loadService);
        });
    },

    createProposal : function(cmp, evt, hpr){
        cmp.set("v.spinnerOn", true);
        let saveService = cmp.get("c.resellerCreateProposal");
        console.log('cmp.get("v.recordId") : ' + cmp.get("v.recordId"));
        saveService.setParams({
            'oppId': cmp.get("v.recordId")
        });
        this.promise(saveService)
        .then(
            result =>{
                cmp.set("v.spinnerOn", false);
                cmp.set('v.hasQuote', true);
                console.log('SUCCESSO : ' + result);
                this.showToast('success', 'Success', 'Proposal created succesfully.');
                $A.get('e.force:refreshView').fire();
                
            },
            error =>{
                $A.get('e.force:refreshView').fire();
                cmp.set("v.spinnerOn", false);
                let js = JSON.stringify(error);
                console.log('Error : ' + JSON.stringify(error));
                this.showToast('error', 'Erro' , error[0] ? error[0].message : 'Unknown error. Please contact support.');
                
            }
        )
        .catch( error =>{
            component.set("v.spinnerOn", false);
            console.log('CATCH ERROR : ' + JSON.stringify(error));
        })
    },

    promise : function(action)
    {
        return new Promise(function(resolve,reject)
        {
            action.setCallback(this,function(response)
            {
                if(response.getState() === 'SUCCESS')resolve(response.getReturnValue());
                else reject(response.getError());
            });
            $A.enqueueAction(action);
        });
    },
    
    loadGympassEntities: function(component, callback, recordId) {
        let gympassEntitiesService = component.get("c.findGympassEntities");
        gympassEntitiesService.setCallback(this, callback);        
        $A.enqueueAction(gympassEntitiesService);
    },

    loadClientManagers: function(component, callback, recordId) {
        let clientManagersService = component.get("c.findClientManagers");
        clientManagersService.setParams({
            'recordId': recordId
        });        
        clientManagersService.setCallback(this, callback);        
        $A.enqueueAction(clientManagersService);
    },
    
    renderFastTrackStageCmp: function(component) {
        let cmpInfo = this.getCmpInformation(component);
        let _self = this;
        $A.createComponent(
            cmpInfo.cmpName,
            cmpInfo.params,
            function(newComponent, status, errorMessage){
                if (status === "SUCCESS") {
                    var body = component.get("v.body");
                    body = newComponent;
                    component.set("v.body", body);
                }
                else if (status === "INCOMPLETE") {
                    _self.showToast("error", "Error!", "No response from server or client is offline.");
                }
                else if (status === "ERROR") {
                    _self.showToast("error", "Error!", errorMessage);
                }
            }
        );
        component.set("v.spinnerOn", false);
    },

    renderEnablersCmp: function(component) {
        $A.createComponent(
            "c:FastTrackEnablers", {
                "proposalCreationTO": component.get("v.proposalCreationTO")
            },
            function(childComponent) {
                if (component.isValid()) {
                    debugger;
                    var targetCmp = component.find('enablersCmp');
                    var body = targetCmp.get("v.body");
                    body.push(childComponent);
                    targetCmp.set("v.body", body);
                }
            }
        );
    },

    getStageCmpMappingMetadata: function(component, recordId, helper) {
        return new Promise(function(resolve, reject){
                let callback = function (response) {
                    if (helper.isResponseSuccessful(response)) {
                        component.set("v.fastTrackStageMapping", response.getReturnValue());
                        resolve();
                    } else {
                        reject('Error finding configuration metadata. Contact your administrator!');
                    }
                };

                let loadService = component.get("c.getStageCmpMappingMetadata");
                loadService.setParams({
                    'recordId': recordId
                }); 
                loadService.setCallback(this, callback);        
                $A.enqueueAction(loadService);
            });
    },

    getCmpInformation : function(component) {
        var cmpInfo = {
            cmpName : "lightning:formattedText",
            params : {
                class : "slds-align_absolute-center slds-text-heading_small",
                value : $A.get("$Label.c.FastTrackNoPageFound")
            }
        };
        let proposalCreationTO = component.get("v.proposalCreationTO");
        let lstStageMapping = component.get("v.fastTrackStageMapping");

        if(proposalCreationTO && proposalCreationTO.fastTrackStage && lstStageMapping && lstStageMapping.length) {

            let stageCmpMapped = lstStageMapping.find(element => element.Label == proposalCreationTO.fastTrackStage);

            if( !stageCmpMapped ) { return cmpInfo; }

            cmpInfo = { "cmpName" : "c:" + stageCmpMapped.ChildComponentApiName__c };

            if( !stageCmpMapped.ChildComponentParameters__c ) { return cmpInfo; }

            let createParams = {};
            let cmpParameters = stageCmpMapped.ChildComponentParameters__c.split(',');

            cmpParameters.forEach(
                function(param) {
                    param = param.split(':');
                    if(param[0]) { createParams[param[0]] = component.get("v." + (param.length > 1 ? param[1] : param[0]) ); }
                });
            
            cmpInfo.params = createParams;
        }

        return cmpInfo;
    },

    showToast : function(type, title, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type": type
        });
        toastEvent.fire();
    },

    save: function(component, callback, recordId, proposal) {        
        let saveService = component.get("c.saveProposal");
        var newAccountsInOpp = null;
        
        saveService.setParams({
            'proposalData': JSON.stringify(proposal)
        });  

        saveService.setCallback(this, callback);        
        $A.enqueueAction(saveService);
    },
    
    isResponseSuccessful : function(response) {
        return (response.getState() === "SUCCESS");
    },
    
     fireRefreshEnablers : function(component)  {
        var cmpEvent = $A.get("e.c:reloadEnablersEvent");
        cmpEvent.setParams({
            "proposalCreationTO": component.get("v.proposalCreationTO")
        });
        cmpEvent.fire();
    },

    resetCommercialConditions : function(component, record, helper){       
        let action = component.get("c.resetCommercialConditions");
        action.setParams({
            'to': component.get("v.proposalCreationTO")
        });        
        action.setCallback(this, function(response){
            var state = response.getState();                         
            if (state === "SUCCESS") {      
                this.load(component,record, helper);
            }else{
                component.set("v.spinnerOn", true);
                return;
            }
          });
        component.set("v.spinnerOn", true);
        $A.enqueueAction(action);
       
    }
})