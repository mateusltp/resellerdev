// ({
//   doInit: function (cmp, evt, hpr) {

//     hpr.init(cmp, evt, hpr);
    
//   },

//   handleCreateProposal : function(cmp, evt, hpr){
//     hpr.createProposal(cmp, evt, hpr)
//   },

//   handleSaveOffer: function(component, event, helper){
//     let changedProposalCreationTO = event.getParam("changedProposalCreationTO");
    
//     if (!changedProposalCreationTO.approvalPending) {
//       component.set("v.proposalCreationTO", changedProposalCreationTO);    
//       $A.enqueueAction(component.get("c.save"));    
//     }
//   },

//   handleSaveAndRefreshOffer: function(component, event, helper){
//     let changedProposalCreationTO = event.getParam("changedProposalCreationTO");
    
//     if ( !changedProposalCreationTO )
//       changedProposalCreationTO = component.get("v.proposalCreationTO");

//     if (!changedProposalCreationTO.approvalPending) {
//       component.set("v.proposalCreationTO",changedProposalCreationTO);    
//       $A.enqueueAction(component.get("c.saveAndRefresh"));
      
//     }    
//   },

//   save: function (component, event, helper){    
//     console.log('SAVE ?');
//     let callback = function (response) { 
//       if (helper.isResponseSuccessful(response)) {      
          
//         component.set("v.proposalCreationTO", response.getReturnValue());
//         helper.showToast('success', 'Concluído', 'Salvo com sucesso');
        
//       }else{
//         if ( response.getError()[0].message!=undefined && response.getError()[0].message.indexOf('[Quantity]')>-1 )
//           response.getError()[0].message = 'Please check and fill Total Number of Employees in the Account, and try load the Opportunity again.';
        
//         helper.showToast('error', 'Erro' , response.getError()[0] ? response.getError()[0].message : 'Unknown error. Please contact support.');
//       }
//     };    
//     helper.save(component, callback, component.get("v.recordId"), component.get("v.proposalCreationTO"), helper);
//   },
//   callLostStage : function(component, event) {        
    
//     //component.set("v.calledLostStage",true);

//     $A.createComponent(
//       "c:FastTrackLostOppModal", {
// 		    "proposalCreationTO" : component.get("v.proposalCreationTO"),
//       },
//       function(modal, status, error) {
//         if (component.isValid()) {
// 					component.find('overlayLib').showCustomModal({
// 		        header: "Mark Opportunity as Lost",
// 				    body: modal,
// 				    showCloseButton: true
// 				  })
//         }
//       }
//     );
//   },
//   cancelLost : function(component, event) {            
//     component.set("v.calledLostStage",false);    
//   },
  
//   saveAndRefresh: function (component, event, helper){   
//     console.log('saveAndRefresh'); 
//     component.set("v.spinnerOn", true); 
//     let callback = function (response) { 
//       if (helper.isResponseSuccessful(response)) {     
         
//         //helper.load(component, component.get("v.recordId"), helper)
//         $A.enqueueAction(component.get("c.doRefreshView"));
//       }else{
//         helper.showToast('error', 'Erro' , response.getError()[0] ? response.getError()[0].message : 'Unknown error. Please contact support.');
//         component.set("v.spinnerOn", false); 
//       }       
      
      
//     };    
//     helper.save(component, callback, component.get("v.recordId"), component.get("v.proposalCreationTO"));
//   },

//   handlefastTrackStageDoneEvent: function(component, event, helper){    
//     component.set("v.spinnerOn", true);
//     let action = component.get("c.setStage");
//     var newTo = JSON.stringify(component.get("v.proposalCreationTO"));
//     action.setParams({
//       "newTo": newTo,
//       "index": event.getParam("nextIndex")
//     });
    
//     action.setCallback(this, function(response){
//       var state = response.getState();  
//       var data = response.getReturnValue(); 

//       if (state === "SUCCESS") {       
//         component.set("v.proposalCreationTO", data);
//         helper.renderFastTrackStageCmp(component);
//         $A.get('e.force:refreshView').fire();
//         component.set("v.spinnerOn", false);
//       }else {    
// 		    var errors = response.getError();
//         var msg;
//         if (errors) {
//           if (errors[0] && errors[0].message) {			
//             if(errors[0].message.includes('Deal Desk')){
//               msg = 'Deal Desk Operational Approval is mandatory before proceeding.';
//             } else if(errors[0].message.includes('M0 and M1')){
//               msg = 'You cannot proceed without completing M0 and also M1 from Engagement Journey.';
//             } else 
//               msg = errors[0].message;
//            }
//         } 
//         component.set("v.spinnerOn", false); 
//         helper.showToast("error", "Error!", msg);
//       }
//     });
//     $A.enqueueAction(action);
//   },

//   reloadChildCmp: function (component, event, helper) {
    
//     component.set("v.spinnerOn", true);
//     console.log('PAI reloadChildCmp');
//     helper.renderFastTrackStageCmp(component);
//   },
    
//   doRefreshView: function(component, event, helper){
//     console.log('doRefreshView');
//     helper.resetCommercialConditions(component, component.get("v.recordId"), helper);   
//   },
  
//   handleOfferEvaluation: function(component, event, helper){
    
//     component.set("v.spinnerOn", true);    
//     let action = component.get("c.proposalEvaluation");
    
//     action.setParams({
//       "oppId": component.get("v.recordId")
//     });
    
//     action.setCallback(this, function(response){    
//       var state = response.getState();  
//       var data = response.getReturnValue();                          
    
//       if (state === "SUCCESS") {       
    
//         component.set("v.proposalCreationTO", data);
//         component.set("v.spinnerOn", false);
//       }else {
    
//         console.log("error when chaging stages!");
//       }
//     }); 

//     $A.enqueueAction(action);
//   }


// });


({
  doInit: function (component, event, helper) {
    component.set("v.spinnerOn", true);
    Promise.all([
        helper.load(component, component.get("v.recordId"), helper),
        helper.getStageCmpMappingMetadata(component, component.get("v.recordId"), helper)
      ]).then(
          function(response) {
            helper.renderFastTrackStageCmp(component);
            $A.get('e.force:refreshView').fire();
            component.set("v.spinnerOn", false);
          }
      ).catch(
          function(error) {
            helper.showToast("error", "Error!", error);
            component.set("v.spinnerOn", false);
          }
      );
    
  },
  
  // doInit: function (cmp, evt, hpr) {
  
  //     hpr.init(cmp, evt, hpr);
      
  // },

  handleCreateProposal : function(cmp, evt, hpr){
    hpr.createProposal(cmp, evt, hpr)
  },

  handleSaveOffer: function(component, event, helper){
    let changedProposalCreationTO = event.getParam("changedProposalCreationTO");
    
    if (!changedProposalCreationTO.approvalPending) {
      component.set("v.proposalCreationTO", changedProposalCreationTO);    
      $A.enqueueAction(component.get("c.save"));    
    }
  },

  handleSaveAndRefreshOffer: function(component, event, helper){
    let changedProposalCreationTO = event.getParam("changedProposalCreationTO");
    
    if ( !changedProposalCreationTO )
      changedProposalCreationTO = component.get("v.proposalCreationTO");

    if (!changedProposalCreationTO.approvalPending) {
      component.set("v.proposalCreationTO",changedProposalCreationTO);    
      $A.enqueueAction(component.get("c.saveAndRefresh"));
      
    }    
  },

  save: function (component, event, helper){    
    
    let callback = function (response) { 
      if (helper.isResponseSuccessful(response)) {      
          
        component.set("v.proposalCreationTO", response.getReturnValue());
        helper.showToast('success', 'Concluído', 'Salvo com sucesso');
        
      }else{
        if ( response.getError()[0].message!=undefined && response.getError()[0].message.indexOf('[Quantity]')>-1 )
          response.getError()[0].message = 'Please check and fill Total Number of Employees in the Account, and try load the Opportunity again.';
        
        helper.showToast('error', 'Erro' , response.getError()[0] ? response.getError()[0].message : 'Unknown error. Please contact support.');
      }
    };    
    helper.save(component, callback, component.get("v.recordId"), component.get("v.proposalCreationTO"));
  },
  callLostStage : function(component, event) {        
    
    //component.set("v.calledLostStage",true);

    $A.createComponent(
      "c:FastTrackLostOppModal", {
		    "proposalCreationTO" : component.get("v.proposalCreationTO"),
      },
      function(modal, status, error) {
        if (component.isValid()) {
					component.find('overlayLib').showCustomModal({
		        header: "Mark Opportunity as Lost",
				    body: modal,
				    showCloseButton: true
				  })
        }
      }
    );
  },
  cancelLost : function(component, event) {            
    component.set("v.calledLostStage",false);    
  },
  
  saveAndRefresh: function (component, event, helper){    
    component.set("v.spinnerOn", true); 
    let callback = function (response) { 
      if (helper.isResponseSuccessful(response)) {     
         
        //helper.load(component, component.get("v.recordId"), helper)
        $A.enqueueAction(component.get("c.doRefreshView"));
      }else{
        helper.showToast('error', 'Erro' , response.getError()[0] ? response.getError()[0].message : 'Unknown error. Please contact support.');
        component.set("v.spinnerOn", false); 
      }       
      
      
    };    
    helper.save(component, callback, component.get("v.recordId"), component.get("v.proposalCreationTO"));
  },

  handlefastTrackStageDoneEvent: function(component, event, helper){    
    component.set("v.spinnerOn", true);
    let action = component.get("c.setStage");
    var newTo = JSON.stringify(component.get("v.proposalCreationTO"));
    action.setParams({
      "newTo": newTo,
      "index": event.getParam("nextIndex")
    });
    
    action.setCallback(this, function(response){
      var state = response.getState();  
      var data = response.getReturnValue(); 

      if (state === "SUCCESS") {       
      
        component.set("v.proposalCreationTO", data);
        helper.renderFastTrackStageCmp(component);
        $A.get('e.force:refreshView').fire();
        component.set("v.spinnerOn", false);
      }else {    
		    var errors = response.getError();
        var msg;
        if (errors) {
          if (errors[0] && errors[0].message) {			
            if(errors[0].message.includes('Deal Desk')){
              msg = 'Deal Desk Operational Approval is mandatory before proceeding.';
            } else if(errors[0].message.includes('M0 and M1')){
              msg = 'You cannot proceed without completing M0 and also M1 from Engagement Journey.';
            } else 
              msg = errors[0].message;
           }
        } 
        component.set("v.spinnerOn", false); 
        helper.showToast("error", "Error!", msg);
      }
    });
    $A.enqueueAction(action);
  },

  reloadChildCmp: function (component, event, helper) {
    
    component.set("v.spinnerOn", true);
    helper.renderFastTrackStageCmp(component);
  },
    
  doRefreshView: function(component, event, helper){
    helper.resetCommercialConditions(component, component.get("v.recordId"), helper);   
  },
  
  handleOfferEvaluation: function(component, event, helper){
    
    component.set("v.spinnerOn", true);    
    let action = component.get("c.proposalEvaluation");
    
    action.setParams({
      "oppId": component.get("v.recordId")
    });
    
    action.setCallback(this, function(response){    
      var state = response.getState();  
      var data = response.getReturnValue();                          
    
      if (state === "SUCCESS") {       
    
        component.set("v.proposalCreationTO", data);
        component.set("v.spinnerOn", false);
      }else {
    
        console.log("error when chaging stages!");
      }
    }); 

    $A.enqueueAction(action);
  }


});