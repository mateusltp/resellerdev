({

    doInit: function(component, event, helper){
        helper.setOperationalCaseRecordTypeId(component, event, helper);
        helper.setPreviousDealDeskOperationalCaseValues(component, event, helper);
        helper.getOperationalQueueId(component, event, helper);
        helper.setDefaultQuoteId(component,event,helper);
        helper.setQuoteLineItems(component,event,helper);
    },
    
    handleSuccess : function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
        $A.get('e.force:showToast').setParams({
            "title": "Success",
            "message": "Case has been created!",
            "type": "success",
        }).fire();
        $A.get('e.force:refreshView').fire();
    },
    
    handleSave: function(component, event, helper){
        component.set("v.displayError", false);
       if( helper.requiredFieldsPopulated(component,event,helper)){
            helper.setCaseOwnerToOperationalQueue(component,event,helper);
            component.find("caseForm").submit();
            component.set("v.disableButton", true);  
            component.set("v.spinner", true);

       }else{
            component.set("v.displayErrorMessage", "Please fill out all the required fields.");
            component.set("v.displayError", true);
       }
    },    

    handleLoad: function(component, event, helper){
        helper.defineRequiredEnabledBasedPSConfigurationFee(component,event,helper);
        helper.defineRequiredEnabledBasedMembershipFeePaymentMethod(component,event,helper);
        helper.defineRequiredEnabledEligibleListRegistrationMethod(component,event,helper);
        helper.defineRequiredEnabledBasedPSMaintenanceFee(component,event,helper);
        component.set("v.showLabels", true);
    },

    handleChangeEligibleListRegistrationMethod: function(component, event, helper){ 
        helper.defineRequiredEnabledEligibleListRegistrationMethod(component,event,helper);
    },

    handleChangeESPaymentDueDays: function(component, event, helper){
        helper.copyValueToPSMaintenanceFeePaymentDueDays(component,event,helper);
    },
    handleChangeCustomESPaymentDueDays: function(component, event, helper){
        helper.copyValueToCustomPSMaintenanceFeePaymentDueDays(component,event,helper);
    },
    
    handleChangeESBillingDay: function(component, event, helper){
        helper.copyValueToPSMaintenanceFeeBillingDay(component,event,helper);
    },

    handleChangeCustomESBillingDay: function(component, event, helper){
        helper.copyValueToCustomPSMaintenanceFeeBillingDay(component,event,helper);
    },

    handleCancel: function(component, event, helper){
        $A.get("e.force:closeQuickAction").fire();
    },

   handleError: function(component, event, helper){
        component.set("v.disableButton", false);
        component.set("v.spinner", false);
    }

      
})