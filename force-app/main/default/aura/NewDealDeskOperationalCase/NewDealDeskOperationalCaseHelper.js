({
    setOperationalCaseRecordTypeId : function(component, event, helper) {
        var action = component.get("c.getCaseRecordTypeId");
        action.setParams({ caseRecordTypeName:component.get("v.caseRecordTypeName")});
        action.setCallback(this, function(response){
            if(response.getState() === 'SUCCESS'){
                var data = response.getReturnValue();   
                component.set("v.caseRecordTypeId", data);
            }else if (response.getState() === 'ERROR') {
                helper.handleErrors(response.getError(), helper);
            }
        });
        $A.enqueueAction(action);
	},

    setPreviousDealDeskOperationalCaseValues: function(component, event, helper) {
        var action = component.get("c.getPreviousDealDeskOperationalCase");
        action.setParams({ recordId:component.get("v.recordId")});
        action.setCallback(this, function(response){
            if(response.getState() === 'SUCCESS'){
                component.set("v.spinner", false);
                var valuesByFieldName = response.getReturnValue();                
                component.set("v.preDefaultValuesByName", valuesByFieldName);
            }else if (response.getState() === 'ERROR') {
                component.set("v.spinner", false);
                helper.handleErrors(response.getError(), helper);
            }
        });
        $A.enqueueAction(action);
    },


    getOperationalQueueId : function(component, event, helper) {
        var action = component.get("c.getQueueId");
        action.setParams({ queueDeveloperName:component.get("v.dealDeskOperationalQueueName")});

        action.setCallback(this, function(response){
            if(response.getState() === 'SUCCESS'){
                component.set("v.spinner", false);
                var data = response.getReturnValue();   
                component.set("v.dealDeskOperationalQueueId", data);
            }else if (response.getState() === 'ERROR') {
                component.set("v.spinner", false);
                helper.handleErrors(response.getError(), helper);
            }
        });
        $A.enqueueAction(action);
	},


    setQuoteLineItems : function(component, event, helper) {
        var action = component.get("c.getQuoteLineItems");
        var hasPSConfigurationFeeValue;
        var hasPSMaintenanceFeeValue;
        action.setParams({ opportunityId:component.get("v.recordId")});
        action.setCallback(this, function(response){
            if(response.getState() === 'SUCCESS'){
                var data = response.getReturnValue();   
                component.set("v.quoteLineItems", data);

                for (let i = 0; i < data.length; i++) {
                   if(data[i].Fee_Type__c == 'Professional Services Setup Fee'){
                        hasPSConfigurationFeeValue = true;
                    } else if(data[i].Fee_Type__c == 'Professional Services Maintenance Fee'){
                        hasPSMaintenanceFeeValue = true;
                    }
                }

                if(hasPSConfigurationFeeValue){
                    component.set("v.hasPSConfigurationFee", true);
                }else{
                    component.set("v.hasPSConfigurationFee", false);
                }

                if(hasPSMaintenanceFeeValue){
                    component.set("v.hasPSMaintenanceFee", true);
                }else{
                    component.set("v.hasPSMaintenanceFee", false);
                }

            }else if (response.getState() === 'ERROR') {
                helper.handleErrors(response.getError(), helper);
            }
        });
        $A.enqueueAction(action);
    },



    requiredFieldsPopulated: function(component, event, helper){
        var hasPSConfigurationFee = component.get("v.hasPSConfigurationFee");
        var membershipFeePaymentMethod = component.get("v.membershipFeePaymentMethod");

        var eligibleListMethod = component.find("eligible_list_registration_method__c").get("v.value");    
        var esBilllingDay = component.find("es_billing_day__c").get("v.value");    
        var setupFeeBilling = component.find("setup_fee_billing_day__c").get("v.value");    
        var esPaymentDueDays = component.find("es_payment_due_days__c").get("v.value");    
      
        if(eligibleListMethod == "HR Portal" || eligibleListMethod == "SFTP & HR Portal"){
            var eligibleFileManagement = component.find("eligible_file_management__c").get("v.value");    
            var eligibleUpdateFrequency = component.find("eligible_file_update_frequency__c").get("v.value");    
            var deadlineSendingEligibleFile = component.find("deadline_for_sending_eligible_file__c").get("v.value");                
            if(!eligibleFileManagement || !eligibleUpdateFrequency || !deadlineSendingEligibleFile)  {                
                return false;
             }
        }else if (!eligibleListMethod){
                return false;
        }

        if(hasPSConfigurationFee){
            var psOneFeeBillingDay = component.find("prof_services_one_fee_billing_day__c").get("v.value");    
            var psOneFeePaymentDueDays = component.find("prof_services_one_fee_payment_due_days__c").get("v.value");    
            if(!psOneFeeBillingDay && !psOneFeePaymentDueDays ){
                return false;
            }
        }
        

        if((membershipFeePaymentMethod == 'Payroll' || membershipFeePaymentMethod == 'Payroll + Credit Card') ){
            var mfEligibilityESBillingDay = component.find("mf_eligibility_es_billing_day__c").get("v.value");    
            var mfEligibilityESPaymentDueDays = component.find("mf_eligibility_es_payment_due_days__c").get("v.value");  
              
            if( !mfEligibilityESBillingDay || !mfEligibilityESPaymentDueDays ){
                return false;
            }   
        }
        

        if (!esBilllingDay || !setupFeeBilling ||!esPaymentDueDays) {
                return false;
        } else {
                return true;
        }
      
    },
    setCaseOwnerToOperationalQueue: function(component, event, helper){
        var operationalQueueId = component.get("v.dealDeskOperationalQueueId");
        component.find("ownerId").set("v.value",operationalQueueId);    
    },

    defineRequiredEnabledBasedMembershipFeePaymentMethod: function(component, event, helper){
        var action = component.get("c.getMembershipFeePaymentMethod");
        action.setParams({ opportunityId:component.get("v.recordId")});
        action.setCallback(this, function(response){
            if(response.getState() === 'SUCCESS'){
                var data = response.getReturnValue();   
                component.set("v.membershipFeePaymentMethod", data);
                if(data == 'Credit Card'){
                    component.find("mf_eligibility_es_billing_day__c").set("v.disabled", true);
                    component.find("mf_eligibility_es_billing_day__c").set("v.value", "");

                    component.find("mf_eligibility_es_custom_billing_day__c").set("v.disabled", true);
                    component.find("mf_eligibility_es_custom_billing_day__c").set("v.value", "");

                    component.find("mf_eligibility_es_payment_due_days__c").set("v.disabled", true);
                    component.find("mf_eligibility_es_payment_due_days__c").set("v.value", "");

                    component.find("mf_eligibility_es_custom_payment_due_day__c").set("v.disabled", true);
                    component.find("mf_eligibility_es_custom_payment_due_day__c").set("v.value", "");

                }else if(data == 'Payroll' || data == 'Payroll + Credit Card'){
                    component.find("mf_eligibility_es_billing_day__c").set("v.disabled", false);
                    component.find("mf_eligibility_es_billing_day__c").set("v.required", true);
                    component.find("mf_eligibility_es_custom_billing_day__c").set("v.disabled", false);
                    component.find("mf_eligibility_es_payment_due_days__c").set("v.disabled", false);
                    component.find("mf_eligibility_es_payment_due_days__c").set("v.required", true);
                    component.find("mf_eligibility_es_custom_payment_due_day__c").set("v.disabled", false);

                }
            }else if (response.getState() === 'ERROR') {
                helper.handleErrors(response.getError(), helper);
            }
        });
        $A.enqueueAction(action);
         
    },

    defineRequiredEnabledBasedPSConfigurationFee: function(component, event, helper){

       var hasPSConfigurationFee = component.get("v.hasPSConfigurationFee");
        if(hasPSConfigurationFee){
            component.find("prof_services_one_fee_payment_due_days__c").set("v.disabled", false);
            component.find("prof_services_one_fee_payment_due_days__c").set("v.required", true);
            component.find("custom_prof_services_one_fee_payment__c").set("v.disabled", false);
            component.find("prof_services_one_fee_billing_day__c").set("v.disabled", false);
            component.find("prof_services_one_fee_billing_day__c").set("v.required", true);
            component.find("custom_prof_service_one_fee_billing_day__c").set("v.disabled", false);
        }
        else if(hasPSConfigurationFee == false) {
            component.find("prof_services_one_fee_payment_due_days__c").set("v.disabled", true);
            component.find("prof_services_one_fee_payment_due_days__c").set("v.value", "");
            component.find("prof_services_one_fee_payment_due_days__c").set("v.required", false);
            component.find("custom_prof_services_one_fee_payment__c").set("v.disabled", true);
            component.find("custom_prof_services_one_fee_payment__c").set("v.value", "");
            component.find("prof_services_one_fee_billing_day__c").set("v.disabled", true);
            component.find("prof_services_one_fee_billing_day__c").set("v.value", "");
            component.find("prof_services_one_fee_billing_day__c").set("v.required", false);
            component.find("custom_prof_service_one_fee_billing_day__c").set("v.disabled", true);    
            component.find("custom_prof_service_one_fee_billing_day__c").set("v.value", "");            
        }
         
    },

    defineRequiredEnabledBasedPSMaintenanceFee: function(component, event, helper){

        var hasPSMaintenanceFee = component.get("v.hasPSMaintenanceFee");
         if(hasPSMaintenanceFee){
            component.find("prof_services_main_fee_billing_day__c").set("v.disabled", true);
            helper.copyValueToPSMaintenanceFeeBillingDay(component,event, helper);

            component.find("custom_prof_service_main_fee_billing_day__c").set("v.disabled", true);
            helper.copyValueToCustomPSMaintenanceFeeBillingDay(component, event, helper);

            component.find("custom_prof_services_main_fee_payment__c").set("v.disabled", true);
            helper.copyValueToCustomPSMaintenanceFeePaymentDueDays(component,event,helper);
            
            component.find("prof_services_main_fee_payment_due_days__c").set("v.disabled", true);
            helper.copyValueToPSMaintenanceFeePaymentDueDays(component, event, helper);

        }
     },


    setDefaultQuoteId : function(component, event, helper) {
        var action = component.get("c.getDefaultQuoteId");
        action.setParams({ opportunityId:component.get("v.recordId")});
        action.setCallback(this, function(response){
            if(response.getState() === 'SUCCESS'){
                var data = response.getReturnValue();   
                component.set("v.defaultQuoteId", data);
            }else if (response.getState() === 'ERROR') {
                helper.handleErrors(response.getError(), helper);
            }
        });
        $A.enqueueAction(action);
	},

    defineRequiredEnabledEligibleListRegistrationMethod: function(component, event, helper){
        let newValue =  event.getSource().get("v.value"); 

        if(newValue == null || newValue == 'undefined') { 
            newValue = component.find("eligible_list_registration_method__c").get("v.value");
        }

        if(newValue == "HR Portal" || newValue == "SFTP & HR Portal" )  { 
            component.find("eligible_file_management__c").set("v.disabled", false);
            component.find("eligible_file_update_frequency__c").set("v.disabled", false);
            component.find("deadline_for_sending_eligible_file__c").set("v.disabled", false);
            component.find("if_custom_input_file_update_frequency__c").set("v.disabled", false);
            component.find("if_custom_deadline_for_sending_file__c").set("v.disabled", false);
   
            component.find("eligible_file_management__c").set("v.required", true);
            component.find("eligible_file_update_frequency__c").set("v.required", true);
            component.find("deadline_for_sending_eligible_file__c").set("v.required", true);
        
        }else { 
            component.find("eligible_file_management__c").set("v.value", "");
            component.find("eligible_file_update_frequency__c").set("v.value", "");
            component.find("if_custom_input_file_update_frequency__c").set("v.value", "");
            component.find("deadline_for_sending_eligible_file__c").set("v.value", "");
            component.find("if_custom_deadline_for_sending_file__c").set("v.value", "");
            
            component.find("eligible_file_management__c").set("v.disabled", true);
            component.find("eligible_file_update_frequency__c").set("v.disabled", true);
            component.find("if_custom_input_file_update_frequency__c").set("v.disabled", true);
            component.find("if_custom_deadline_for_sending_file__c").set("v.disabled", true);
            component.find("deadline_for_sending_eligible_file__c").set("v.disabled", true);

            component.find("eligible_file_management__c").set("v.required", false);
            component.find("eligible_file_update_frequency__c").set("v.required", false);
            component.find("deadline_for_sending_eligible_file__c").set("v.required", false);
        }
    
    },
    copyValueToPSMaintenanceFeePaymentDueDays: function(component, event, helper){
        var hasPSMaintenanceFee = component.get("v.hasPSMaintenanceFee");
        if(hasPSMaintenanceFee){
            var esPaymentDueDays = component.find("es_payment_due_days__c").get("v.value");    
            component.find("prof_services_main_fee_payment_due_days__c").set("v.value", esPaymentDueDays);
        }
    },

    copyValueToCustomPSMaintenanceFeePaymentDueDays: function(component, event, helper){
         var hasPSMaintenanceFee = component.get("v.hasPSMaintenanceFee");
         if(hasPSMaintenanceFee){      
            var customESPaymentDueDays = component.find("custom_es_payment_due_days__c").get("v.value");          
            component.find("custom_prof_services_main_fee_payment__c").set("v.value", customESPaymentDueDays);
        }
    },


    copyValueToPSMaintenanceFeeBillingDay: function(component, event, helper){
         var hasPSMaintenanceFee = component.get("v.hasPSMaintenanceFee");
        if(hasPSMaintenanceFee){
            var esBilllingDay = component.find("es_billing_day__c").get("v.value");    
            component.find("prof_services_main_fee_billing_day__c").set("v.value", esBilllingDay);
        }
    },

    copyValueToCustomPSMaintenanceFeeBillingDay: function(component, event, helper){
        var hasPSMaintenanceFee = component.get("v.hasPSMaintenanceFee");
        if(hasPSMaintenanceFee){
            var customESBilllingDay = component.find("custom_es_billing_day__c").get("v.value");    
            component.find("custom_prof_service_main_fee_billing_day__c").set("v.value", customESBilllingDay);
        }
    },

    handleErrors: function(errors, helper) {
        var message = "Unkown error. Contact system administrator.";
        if (errors && Array.isArray(errors) && errors.length > 0) {
            message = errors[0].message;
        }
        helper.showToast('error', 'Error', message);
    },

    showToast: function(type, title, message) {
        let toastParams = {
            title: title,
            message: message,
            type: type
        };
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams(toastParams);
        toastEvent.fire();
    }
})