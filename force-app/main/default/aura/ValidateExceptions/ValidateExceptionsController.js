({
	doInit : function(component, event, helper) {
        component.set('v.showSpinner', true);
        helper.callServerSideController(component, 'c.deleteOldExceptions');
        helper.callServerSideController(component, 'c.setValidateExceptions');
        helper.getExceptions(component);
	},
    
    toSubmitForApproval : function (component, event, helper) {
        var submitForApproval = true;
        var exceptions = component.get('v.exceptions');
        
        for (var i=0; i<exceptions.length; i++) {
            if (exceptions[i].Rationale__c == null || exceptions[i].What_the_Company_Wants__c == null)
                submitForApproval = false;
            
            if (exceptions[i].Rationale__c == null)
                helper.showToast({
                    'title' : 'Type: ' + exceptions[i].Type__c + ' | Subtype: ' + exceptions[i].Subtype__c,
                    'type' : 'error',
                    'message' : 'Error: \'Rationale\' is empty',
                    'mode' : 'sticky'
                });
                
            if (exceptions[i].What_the_Company_Wants__c == null)
                helper.showToast({
                    'title' : 'Type: ' + exceptions[i].Type__c + ' | Subtype: ' + exceptions[i].Subtype__c,
                    'type' : 'error',
                    'message' : 'Error: \'What the Company Wants\' is empty',
                    'mode' : 'sticky'
                });
        }
        
        if (submitForApproval == true) {
            component.set('v.showSpinner', true);
            helper.submitForApproval(component);
        }
    }
})