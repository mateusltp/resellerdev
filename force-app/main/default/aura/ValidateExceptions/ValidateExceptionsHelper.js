({
	callServerSideController : function(component, methodName) {
        var action = component.get(methodName);
        
        action.setParams({
            recordId : component.get("v.recordId")
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {

            } else {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });

        $A.enqueueAction(action);
	},
	
     getExceptions: function(component) {
        var action = component.get('c.getExceptions');
        
        action.setParams({
            recordId : component.get('v.recordId')
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var exceptions = response.getReturnValue();
                
                var notApprovedExceptions = false;
                
                for (var i=0; i<exceptions.length; i++) {
                    if (exceptions[i].Can_be_modified__c)
                        notApprovedExceptions = true;
                }
                
                if (notApprovedExceptions)
                    component.set('v.allowSubmitForApproval', true);
                
                component.set('v.exceptions', exceptions);
                component.set('v.showSpinner', false);
            } else {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });

        $A.enqueueAction(action);
	},

    showToast : function(params) {
        var toastEvent = $A.get("e.force:showToast");
        if(toastEvent){
            toastEvent.setParams(params);
            toastEvent.fire();
        } else{
            alert(params.message);
        }
    },
    
    submitForApproval : function(component) {
        var action = component.get('c.submitForApproval');
        var exceptions = component.get('v.exceptions');
        console.log('exceptions helper: ' + JSON.stringify(exceptions));
        
        action.setParams({
            recordId : component.get('v.recordId'),
            jsonListExceptions : JSON.stringify(exceptions)
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (state === "SUCCESS") {
                $A.get("e.force:closeQuickAction").fire();
                component.set('v.showSpinner', false);
            } else {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });

        $A.enqueueAction(action);
    }
})