({
    invoke : function(component, event, helper) {
        // Fire the event to navigate to the opportunity record
        var sObjectEvent = $A.get("e.force:navigateToSObject");
        sObjectEvent.setParams({
            "recordId": component.get("v.opportunityId")
        })
        sObjectEvent.fire();
    }
})