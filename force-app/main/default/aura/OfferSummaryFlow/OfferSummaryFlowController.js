/**
 * @description       : 
 * @author            : tiago.ribeiro@gympass.com
 * @group             : 
 * @last modified on  : 06-21-2022
 * @last modified by  : tiago.ribeiro@gympass.com
**/
({
    init: function(cmp, event, helper){
        helper.createFlowComponent(cmp);
        helper.subscribePlatformEvent(cmp);
    },

    handleMessage: function(cmp, message, helper){
        helper.refreshFlow(cmp);
    }
})