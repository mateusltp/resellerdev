/**
 * @description       : 
 * @author            : tiago.ribeiro@gympass.com
 * @group             : 
 * @last modified on  : 06-21-2022
 * @last modified by  : tiago.ribeiro@gympass.com
**/
({
    createFlowComponent: function(cmp){
        $A.createComponent(
            "c:OfferSummaryFlowChild",
            {
                "aura:id": "flowAura",
                "recordId": cmp.get('v.recordId'),
                "fastTrackStages": cmp.get('v.fastTrackStages')
            },
            function(offerSummaryFlow, status, errorMessage){
                if (status === "SUCCESS") {
                    var body = cmp.get("v.body");
                    body.push(offerSummaryFlow);
                    cmp.set("v.body", body);
                }
                else if (status === "INCOMPLETE") {
                    console.log("No response from server or client is offline.")
                    // Show offline error
                }
                else if (status === "ERROR") {
                    console.log("Error: " + errorMessage);
                    // Show error message
                }
            }
        );
    },

    subscribePlatformEvent: function(cmp){
        const empApi = cmp.find('empApi');
        // Replay option to get new events
        const replayId = -1;
        empApi.subscribe('/event/FastTrack_Component_Event__e', replayId, $A.getCallback(eventReceived => {
            // Process event (this is called each time we receive an event)
            let message = eventReceived;
            let recordId = cmp.get('v.recordId');
            if(this.platformEventMatchesId(message, recordId)){
                this.refreshFlow(cmp);        
            }
        }))
        .then(subscription => {
            // Subscription response received.
            console.log('Subscription request sent to: ', subscription.channel);
        });
    },

    platformEventMatchesId: function(message, recordId){
        return message.hasOwnProperty('data') && message.data.hasOwnProperty('payload') && 
                message.data.payload.hasOwnProperty('Record_Id__c') && recordId == message.data.payload.Record_Id__c;
    },

    refreshFlow: function(cmp){
        cmp.find('flowAura').destroy();
        this.createFlowComponent(cmp);
    }
})