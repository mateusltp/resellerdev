({

    init: function (component, event, helper) {
        component.set('v.recordId', component.get("v.pageReference").state.c__recordId);
        var recordId = component.get("v.recordId");

        helper.getCaseBusinessUnit(component);
        helper.getDocumentDeviation(component, event, recordId);
        helper.getAnyOtherDetail(component, event, recordId);        
    },

    save: function (component, event, helper) {
        var anyOtherDetail = component.find("anyOtherDetail").get("v.value");
        helper.saveForm(component, event, component.get("v.recordId"), anyOtherDetail);
    },

    close: function (component, event, helper) {
        helper.goBackToCase(component.get("v.recordId"));
    },

    onchangeDisplayOtherField: function (component, event, helper) {
        helper.onchangeDisplayOtherFieldWhenOtherDeviationSelected(component, event);
    },

    onloadDisplayOtherField : function ( component, event, helper ) {
        helper.onloadDisplayOtherFieldIfOtherDeviationSelected(component, event);
    }   
        
})