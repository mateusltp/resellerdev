({
	getCaseBusinessUnit : function( component ) {
		var action = component.get("c.getBusinessUnit");
		action.setParams({
			recordId : component.get("v.recordId")
		});

		action.setCallback(this, function( response ){
			var state = response.getState();

			if( state === 'SUCCESS' ){

				var caseBusinessUnit = response.getReturnValue();				
				component.set("v.caseBusinessUnit", caseBusinessUnit);

			}else if ( state === 'ERROR' ){
				console.log("errors: " + response.getError());
				this.showToast("Error!", "error", "Unable to display Deviation form! Please contact your Salesforce Administrator.");
			}
		});
		$A.enqueueAction(action);
	},	

	saveForm : function( component, event, recordId, anyOtherDetail ){		
		var action = component.get("c.saveDocumentDeviationForm");	
		var docDeviation = component.get("v.documentDeviation");		
		
		
		for (let i=0; i<docDeviation.length; i++){
			if( !docDeviation[i].value.includes('Deviation') ){
				docDeviation[i].otherDeviationValue = '';
			}else if(!docDeviation[i].otherDeviationValue){
				this.showToast("Error!", "error", "If you selected 'other deviation' you must fill in the text field of the deviation.");
				return;
			}
		} 

		action.setParams({
			documentDeviationFormJSON : JSON.stringify(docDeviation),
			recordId : recordId,
			anyOtherDetail : anyOtherDetail			
		});
		
		action.setCallback(this, function( response ){			
			var state = response.getState();
			
			if( state === 'SUCCESS' ){								
				
				this.showToast("Success!", "success", "Deviation form saved successfully!");						
				this.goBackToCase(recordId);					
				
			}else if ( state === 'ERROR'){
				console.log("errors: " + response.getError());
				this.showToast("Error!", "error", "Unable to save Deviation form! Please contact your Salesforce Administrator.");				
			}
		});
		$A.enqueueAction(action);
	},	

	getDocumentDeviation : function( component, event, recordId ) {
        var action = component.get("c.getDocumentDeviation");

		action.setParams({
			recordId : recordId
		});
        
        action.setCallback(this, function( response ){
            
            var state = response.getState();
            
            if( state === 'SUCCESS' ) {            

                var documentDeviation = response.getReturnValue();
				console.log(documentDeviation);
                component.set("v.documentDeviation",documentDeviation);

            }
            else if ( state == 'ERROR' ){            
                console.log("errors: " + response.getError());
            }
        });
        $A.enqueueAction(action);
    },

	getAnyOtherDetail : function ( component, event, recordId ) {
		var action = component.get("c.getAnyOtherDetailField");
		action.setParams({
			recordId : recordId
		});

		action.setCallback(this, function( response ){
			var state = response.getState();
            
            if( state === 'SUCCESS' ) {            

                var anyOtherDetail = response.getReturnValue();				
                component.set("v.anyOtherDetail",anyOtherDetail);

            }
            else if ( state == 'ERROR' ){            
                console.log("errors: " + response.getError());
            }
		});
		$A.enqueueAction(action);
	},

	onchangeDisplayOtherFieldWhenOtherDeviationSelected : function( component, event ){		
        var indexVar = event.getSource().get("v.id");		
        var value = event.getSource().get("v.value");
		
        var indexVarFormatted = indexVar.replace("radio", "txtArea");
        var textArea = document.getElementById(`${indexVarFormatted}`);
		
        if(value.includes('Deviation')){
            textArea.classList.remove('slds-hide');
        }else{
            textArea.classList.add('slds-hide');
        }
	},

	showToast : function( title, type, message ) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type": type
        });
        toastEvent.fire();
    },
   
	goBackToCase : function( recordId ){
		var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": recordId,
            "slideDevName": "related"
        });
        navEvt.fire();
		$A.get('e.force:refreshView').fire();
    },

	onloadDisplayOtherFieldIfOtherDeviationSelected : function ( component, event ){
		var radios = component.find("radioSelect");
        
		if (radios != undefined) {
        for (let i=0; i<radios.length; i++ ){
				var radioId = radios[i].get("v.id");
				var indexVarFormatted = radioId.replace("radio", "txtArea");

				var textArea = document.querySelector(`#${indexVarFormatted}`);
            if(radios[i].get("v.value").includes('Deviation')){
                textArea.classList.remove('slds-hide');
            }
        }
		}
	}
	
})