({
    handleClick : function(component, event, helper) {
        debugger;

        var url = $A.get("$Label.c.Org_Domain") + '/sfc/servlet.shepherd/document/download/' + $A.get("$Label.c.Toolkit_Content_Version_Id") + '?operationContext=S1';

        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire();
    }
})