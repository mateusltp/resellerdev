({
	doInit : function(component, event, helper)
    {        
        var action = component.get("c.getInvitees");
        action.setParams({ 
            recordId : component.get("v.recordId") 
        });

        action.setCallback(this, function(response){
            var state = response.getState();
            
            if (state === "SUCCESS") 
            {
                component.set('v.associations',response.getReturnValue());
        		component.set('v.showSpinner', false);
        		component.set('v.showContent', true);
            }
            else 
            {
        		component.set('v.showSpinner', false);
                helper.showToast('Error', 'Whoops!', 'We couldn\'t find the related invitees. Please try it again later!', '');
            	$A.get("e.force:closeQuickAction").fire();
            }
        });

        $A.enqueueAction(action);
	},
    
	toAddInvitees : function(component, event, helper)
    {
		//component.set('v.showData', false);
        
        var selectedLookUpRecords = component.get('v.selectedLookUpRecords');

        if (!selectedLookUpRecords.length > 0)
        {
            helper.showToast('error', 'Whoops!', 'It is mandatory to select one user at least!', '');
        	$A.get("e.force:closeQuickAction").fire();
        } else {
            var object = 'Compromisso';
            var recordId = component.get('v.recordId');
            
            var action = component.get("c.getOwnerId");
            
            action.setParams({
                recordId : recordId
            });
    
            action.setCallback(this, function(response) 
            {
                var state = response.getState();
                if (state === "SUCCESS")
                {
                    var assigneeId = response.getReturnValue();
                    
           			helper.addInvitee(component, object ,selectedLookUpRecords, recordId, assigneeId);

                    var goToInit = component.get('c.doInit');
                    $A.enqueueAction(goToInit);
                } else {
                    
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "type": 'error',
                        "title": 'Error!',
                        "message": 'It was not possible to get the Assignee Id, please check if the "Assigned To" is filled and then try it again!'
                    });
                    toastEvent.fire();
                    $A.get("e.force:closeQuickAction").fire();
                }
            });
            $A.enqueueAction(action);
        }
	},
  
	doInit : function(component, event, helper)
    {        
        var action = component.get("c.getInvitees");
        action.setParams({ 
            recordId : component.get("v.recordId") 
        });

        action.setCallback(this, function(response){
            var state = response.getState();
            
            if (state === "SUCCESS") 
            {
                component.set('v.associations',response.getReturnValue());
            }
            else 
            {
                helper.showToast('Error', 'Whoops!', 'We couldn\'t find the related invitees. Please try it again later!', '');
            	$A.get("e.force:closeQuickAction").fire();
            }
        });

        $A.enqueueAction(action);
	},
    
	toChangeExpandValue : function(component, event, helper)
    {
        var ariaExpanded = component.get('v.ariaExpanded');
        var ariaHidden = component.get('v.ariaHidden');
        var divClass = component.get('v.divClass');
        
        if (ariaExpanded == true)
        {
            component.set('v.ariaExpanded', false);
            component.set('v.ariaHidden', true);
        } else {
            component.set('v.ariaExpanded', true);
            component.set('v.ariaHidden', false);
        }
        
        if (divClass == 'slds-section slds-is-open')
            component.set('v.divClass','slds-section');
        else
            component.set('v.divClass','slds-section slds-is-open');
	},
    
	toDelete : function(component, event, helper)
    {
        var recordId = event.target.id;
        
        if (recordId == 'Assigned')
        {
            helper.showToast('Error', 'Whoops!', 'It is not possible to manually delete an Assigned user.', '');
        } else {
            var action = component.get("c.deleteEventAssociation");
            action.setParams({ 
                recordId : recordId 
            });
    
            action.setCallback(this, function(response){
                var state = response.getState();
                
                if (state === "SUCCESS") 
                {
                    helper.showToast('Success', 'Success!', response.getReturnValue(), '');
                    
                    var goToInit = component.get('c.doInit');
                    $A.enqueueAction(goToInit);
                }
                else 
                {
                    helper.showToast('Error', 'Whoops!', 'We couldn\'t find the related invitees. Please try it again later!', '');
                }
            });
    
            $A.enqueueAction(action);
        }      
	}
})