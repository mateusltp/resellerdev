({
    init: function (component, event, helper) {
        component.set('v.recordId' , component.get("v.pageReference").state.c__recordId);

        helper.getClauses(component);
        helper.loadSwapped(component, event, helper);
    },
    
    swapClause: function (component, event, helper) {
        helper.swapButton(component, helper);
    },

    swapStandard: function (component, event, helper) {
        helper.swapToStandard(component, event, helper);
    },

    save: function (component, event, helper) {
        helper.saveClauses(component, event, helper);
    },

    selectedRow: function (component, event) {        
        var selectedRows = event.getParam('selectedRows');
        component.set('v.selectedRows', selectedRows);        
        component.set('v.standardDisable', selectedRows.length > 0 ? false : true);
    },
    
    handleSectionToggle: function (component, event) {
        var openSections = event.getParam('openSections');

        if (openSections.length === 0) {
            component.set('v.activeSectionsMessage', "All sections are closed");
        } else {
            component.set('v.activeSectionsMessage', "Open sections: " + openSections.join(', '));
        }
    },
    
    confirmBack: function(component, event, helper) {
        helper.backConfirm(component, event, helper);        
    },
  
    submitDetails: function(component, event, helper) {
      component.set("v.isModalOpen", false);
    },

    getSwap: function(component, event, helper) {
        helper.getSwapValues(component, event);
    },

    getSwapText: function(component, event, helper) {
        helper.getSwapTxt(component, event);
    },

    backContract: function(component, event, helper) {
        helper.backContract(component, event);
    },

    noCancel: function(component, event, helper) {
        component.set('v.isUnsavedChanges', false);		
    },
});