({
	getClauses : function(component) {		
        var action = component.get("c.getSwapClauses");

        action.setParams({ 
			contractAgreementId : component.get("v.recordId") 
		});

        action.setCallback(this, function(response) {
            var state = response.getState();

            if (state === "SUCCESS") {
				var responseData = JSON.parse(response.getReturnValue());

				this.getStandardClauses(component, responseData);
				component.set('v.fullClauses', responseData);

            } else if (state === "ERROR") {
                console.log("errors: " + response.getError());
				helper.showToast("error", "Error!", "Please contact your Salesforce Administrator.");
            }
        });

		$A.enqueueAction(action);
    },	

	getStandardClauses : function(component, response) {
		var clausesStandard = [];

		response.forEach((element) => {
			var clause = {
				'label' : element.Name,
				'value' : element.Id,
			};

			clausesStandard.push(clause);
		});

		component.set('v.standardValues', clausesStandard);
	},

	getSwapValues : function(component, event) {
		var clauseId = event.getSource().get("v.value");
		var fullClauses = component.get('v.fullClauses');		
		var swapClauses = [];
		var swapTexts = [];
		var txtClause;

		fullClauses.forEach((element) => {
			if(element.Id == clauseId){
				txtClause = element.ClauseText;	

				element.SwapOptions.forEach((elementSwp) => {
					var swpClause = {
						'label' : elementSwp.Name,
						'value' : elementSwp.Id,
					};					
							
					swapClauses.push(swpClause);
				});	
				
				component.set('v.standarObject' , element);
			}
		});
		
		component.set('v.swapTextValue', '');
		component.set('v.clauseTextValue', txtClause);
		component.set('v.swapValues', swapClauses);
		component.set('v.swapTexts', swapTexts);
		component.set('v.standarValue', clauseId);
	},

	getSwapTxt : function(component, event) {
		var swapClauseId = event.getSource().get("v.value");		
		var clauseObj = component.get('v.standarObject');
		var txtClause;

		clauseObj.SwapOptions.forEach((element) => {
			if(element.Id == swapClauseId){
				txtClause = element.ClauseText;
				component.set('v.swapObject' , element);
			}
		});			
		
		component.set('v.swapTextValue', txtClause);
		component.set('v.newValue', swapClauseId);
		component.set('v.swapDisable', false);
	},

	swapButton : function(component, helper) {
		var clauseObj = component.get('v.standarObject');
		var swapObj = component.get('v.swapObject');
		var data = component.get('v.data');
		var contractId = component.get('v.recordId');

		var swap = {
			'ObjectId' : null,
			'StandardId': clauseObj.Id,
			'StandardClause': clauseObj.Name,
			'SwapId' : swapObj.Id,
			'NewClause' : swapObj.Name,
			'CreatedDateTime' : Date.now(),
			'Status' : 'Alternative clause applied',
			'Type' : clauseObj.Type,
			'ContractAgreementId' : contractId
		};		

		if (!data.find(element => element.StandardId == clauseObj.Id)){
			data.push(swap);
			helper.showToast("success", "Success!", "Clause was successfully swapped!");
		}else{
			helper.showToast("info", "You already swapped this clause", "Before changing to another, please switch back to the standard clause in the swapped clauses summary.");
		};

		component.set('v.data' , data);

		component.set('v.standarValue', '');
		component.set('v.clauseTextValue', '');
		component.set('v.newValue', '');
		component.set('v.swapTextValue', '');		
		
		component.set('v.swapDisable', true);
		component.set('v.saveDisable', false);		
	},

	swapToStandard : function(component, event, helper) {
		var selectedRows = component.get('v.selectedRows');
		var data = component.get('v.data');
		var willDeleted = [];

		selectedRows.forEach((selected) => {
			data.forEach((element, index) => {

				if(element.StandardId == selected.StandardId){
					data.splice(index, 1);
				}	
			});
			
			if(selected.ObjectId != null){
				willDeleted.push(selected);
			}
		});

		if(willDeleted.length > 0){
			helper.deleteSwapped(component, helper, willDeleted, data);
		}else{
			component.set('v.data' , data);
			helper.showToast("success", "Swapped to standard", "The clauses were successfully swapped to the standard.");
		}		
	},

	saveClauses : function(component, event, helper) {
		var clauses = component.get('v.data');
        var action = component.get("c.saveSwapClauses");

		helper.showSpinner(component, event, helper);

		clauses.forEach((element) => {
			element.CreatedDateTime = null;			
		});

        action.setParams({ 
			swappedClausesToSave : JSON.stringify(clauses)
		});

        action.setCallback(this, function(response) {
            var state = response.getState();

            if (state === "SUCCESS") {
				var responseData = response.getReturnValue();

				if(responseData){
					helper.showToast("success", "Contract saved", "Please go to Generate document to download it.");
					$A.get("e.force:refreshView").fire();
					helper.backContract(component, event, helper);
				}

            } else if (state === "ERROR") {
                console.log("errors: " + response.getError());
				helper.showToast("error", "Error!", "Please contact your Salesforce Administrator.");
            }

			helper.hideSpinner(component, event, helper);
        });

		$A.enqueueAction(action);		
		
    },	

	showToast : function(type,title, message) {
        var toastEvent = $A.get("e.force:showToast");

        toastEvent.setParams({
            "title": title,
            "type": type,
            "message":message
        });

        toastEvent.fire();
    },

	loadSwapped : function(component, event, helper) {
		var contractId = component.get("v.recordId");
        var action = component.get("c.getSwappedClauses");
		
        action.setParams({ 
			contractAgreementId : contractId
		});

        action.setCallback(this, function(response) {
            var state = response.getState();

            if (state === "SUCCESS") {
				var responseData = response.getReturnValue();
				component.set('v.data' , JSON.parse(responseData));
				
            } else if (state === "ERROR") {
                console.log("errors: " + response.getError());
				helper.showToast("error", "Error!", "Please contact your Salesforce Administrator.");				
            }
        });

		$A.enqueueAction(action);
    },
	
	backConfirm : function(component, event, helper) {
		var data = component.get('v.data');
			
		if (data.find(element => element.ObjectId == null)){
			component.set('v.isUnsavedChanges', true);			
		}else{
			helper.backContract(component, event, helper);
		};        
    },

	backContract : function(component, event, helper) {
		var contractId = component.get('v.recordId');
		var navEvt = $A.get("e.force:navigateToSObject");

		navEvt.setParams({
			"recordId": contractId,
			"slideDevName": "related"
		});

		$A.get("e.force:refreshView").fire();	
		navEvt.fire();
    },

	deleteSwapped : function(component, helper, willDeleted, data) {
        var action = component.get("c.deleteSwapClauses");
		
        action.setParams({ 
			swappedClausesToDelete : JSON.stringify(willDeleted)
		});		

        action.setCallback(this, function(response) {
            var state = response.getState();

            if (state === "SUCCESS") {
				if(response.getReturnValue()){
					component.set('v.data' , data);
					helper.showToast("success", "Swapped to standard", "The clauses were successfully swapped to the standard.");
				}
				
            } else if (state === "ERROR") {
                console.log("errors: " + response.getError());
				helper.showToast("error", "Error!", "Please contact your Salesforce Administrator.");
            }
        });

		$A.enqueueAction(action);
    },

    showSpinner: function(component, event, helper) {
        component.set("v.spinner", true); 
    },

    hideSpinner : function(component,event,helper){ 
        component.set("v.spinner", false);
    }
})