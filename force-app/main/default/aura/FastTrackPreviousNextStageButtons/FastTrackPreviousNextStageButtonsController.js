({
    fireEvent : function(component, event, helper) {
        let source = event.getSource().get("v.name");       
        let i = source === "save" ? 1 : -1;
        let cmpEvent = $A.get("e.c:fastTrackStageDoneEvent");
        let nextIndex = component.get("v.parentIndex");
        nextIndex = nextIndex + i;
        cmpEvent.setParams({
            "opportunityId": component.get("v.recordId"),
            "nextIndex": nextIndex
        });

        cmpEvent.fire();
    }
})