/**
 * @description       : 
 * @author            : AlyssonMota
 * @group             : 
 * @last modified on  : 03-22-2022
 * @last modified by  : Mateus Augusto - GFT (moes@gft.com)
**/
({
	setAttributes : function(component, helper, quoteItem) {
		debugger;
		component.set("v.productFamily", quoteItem.sObj.Fee_Type__c);
		component.set("v.itemId", quoteItem.sObj.Id);
	},
	setPayments : function(component, helper, quoteItem) {
		var payments = quoteItem.payments;
		
		for (var i=0; i<payments.length;  i++) {
			if (payments[i].sObj.Id != undefined) 
				helper.loadPaymentComponent(component, helper, payments[i], quoteItem);
		}
	},
	loadPaymentComponent : function(component, helper, payment, quoteItem) {
		$A.createComponent(
            "c:OfferSummaryPayment", {
				"payment": payment,
				"proposalCreationTO" : component.get("v.proposalCreationTO"),
                "isEnterpriseSubscription": (quoteItem.sObj.Fee_Type__c == 'Enterprise Subscription' ? true : false),
				"isSetupFee": (quoteItem.sObj.Fee_Type__c == 'Setup Fee' ? true : false),
				"isProfessionalServicesSetupFee": (quoteItem.sObj.Fee_Type__c == 'Professional Services Setup Fee' ? true : false),
				"isProfessionalServicesMaintenanceFee": (quoteItem.sObj.Fee_Type__c == 'Professional Services Maintenance Fee' ? true : false)
            },
            function(childCmp, status, error) {
                if (component.isValid()) {
                    var targetCmp = component.find('paymentItem');
                    var body = targetCmp.get("v.body");
                    body.push(childCmp);
                    targetCmp.set("v.body", body);
                }
            }
        );
	},
	openProductItemModal : function(component, helper, product) {
		var feeType;

		if (product.sObj.Fee_Type__c == 'Access Fee') {
			feeType = "Enterprise Subscription";
		} else {
			feeType = product.sObj.Fee_Type__c;
		}

        $A.createComponent(
            "c:ProductItemModal", {
				"proposalCreationTO" : component.get("v.proposalCreationTO"),
                "product": product
            },
            function(modal, status, error) {
                if (component.isValid()) {
					component.find('overlayLib').showCustomModal({
						header: feeType,
						body: modal,
						showCloseButton: true
					})
                }
            }
        );
	},
	openAddPayment : function(component, helper, product) {
        var payment = product.payments;
        
        component.set("v.paymentId", payment[0].sObj.Id);
        $A.createComponent(
            "c:OfferSummaryAddPayment", {
                "proposalCreationTO" : component.get("v.proposalCreationTO"),
                "product" : product,
                "isUpdate" : true,
                "payment" : component.get("v.payment"),
                "paymentId" : component.get("v.paymentId"),
                "isProfessionalServicesMaintenanceFee": (product.sObj.Fee_Type__c == 'Professional Services Maintenance Fee' ? true : false),
                "isSetupFee": (product.sObj.Fee_Type__c == 'Setup Fee' ? true : false)
            },
            function(modal, status, error) {
                if (component.isValid()) {
                    component.find('overlayLib').showCustomModal({
                        header: "Payment",
                        body: modal,
                        showCloseButton: true
                    })
                }
            }
        );
    },
    
    openAddWaiver : function(component, helper, product) {
        var payment = product.payments;
        
        $A.createComponent(
            "c:OfferSummaryAddWaiver", {
                "proposalCreationTO" : component.get("v.proposalCreationTO"),
                "payment" : payment[0]
            },
            function(modal, status, error) {
                if (component.isValid()) {
                    component.find('overlayLib').showCustomModal({
                        header: "Add waiver or temporary discount",
                        body: modal,
                        showCloseButton: true
                    })
                }
            }
        );
    },
    openEditfees : function(component, helper, product) {
        var quoteItem = product;
        var payment = product.payments;
        component.set("v.paymentId", payment[0].sObj.Id);
        
        $A.createComponent(
            "c:FastTrackEditEnterprise", {
                "proposalCreationTO" : component.get("v.proposalCreationTO"),
                "quoteItem" : product,
                "paymentId" : component.get("v.paymentId")
                

            },
            function(modal, status, error) {
                if (component.isValid()) {
                    component.find('overlayLib').showCustomModal({
                        header: "Edit enterprise subscription fee",
                        body: modal,
                        showCloseButton: true
                    })
                }
            }
        );
    }
})