({
	doInit : function(component, event, helper) 
    {
        helper.getAvailableContracts(component);
        helper.validateContractGeneration(component);
	},
    
    handleFormatChange: function (component, event)
    {
        component.set('v.fileFormat',event.getParam('value'));
    },
    
    onContractSelectChange: function (component, event, helper)
    {
        var selectedComponent = component.find('contractOptions').get('v.value');
        
        var recordId = component.get('v.recordId');
        
        var baseURL = '/apex/'+selectedComponent+'?id='+recordId;
        component.set('v.baseURL',baseURL);
        
        component.set('v.contractNotSelected', false);
        component.set('v.selectedContract', selectedComponent);
    }, 
    
    handleDownload: function (component, event, helper)
    {
        var baseURL = component.get('v.baseURL');
        var fileFormat = component.get('v.fileFormat');
        
        baseURL += '&fileFormat=' + fileFormat;
        
        window.open(baseURL);
    },
})