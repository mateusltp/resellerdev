/**
 * @description       : 
 * @author            : Mateus Augusto - GFT (moes@gft.com)
 * @group             : 
 * @last modified on  : 04-22-2022
 * @last modified by  : Mateus Augusto - GFT (moes@gft.com)
**/
({
    callChangeStageEvent : function(component, nextIndex) {                
        var cmpEvent = $A.get("e.c:fastTrackStageDoneEvent");
        cmpEvent.setParams({
            "opportunityId": component.get("v.oppId"),
            "proposalCreationTO": component.get("v.proposalCreationTO"),
            "nextIndex": 10
        });
        console.log(cmpEvent);
        cmpEvent.fire();
    },
    
    isFormValid: function (component, event, helper) {
        var requiredFields = []; 
        requiredFields = component.find('required');
        var isValid = true;
        
        if(component.get("v.StartBillingDateFlag")){
            console.log('Entramos no if date')
            component.set("v.StartBillingDate", null)
        }
        requiredFields.forEach(e => {
            if (!e.get('v.value') || !e.get('v.value').trim().length) {
            console.log(e.get('v.value'))
            console.log(e.get('v.fieldName'))
            isValid = false;
        }
                               });
        
        return isValid;
    },
    
    submitAllFormsHelper: function(component, event, helper){
        component.set("v.spinnerOn", true);
        if(this.isFormValid(component, event, helper) && component.get("v.executivoSucessoClienteFlag")){
            component.find("rEditForm").submit();
            component.find("rEditFormQ").submit();
            //this.callChangeStageEvent(component, event);        
        } else {
            if(!this.isFormValid(component, event, helper)){
                this.showToast('error', 'Ops!', 'Please, enter mandatory fields.');
                console.log('console isFormValid ',this.isFormValid(component, event, helper));
                console.log('console executivoSucessoCliente ',component.get("v.executivoSucessoClienteFlag"));                
            }else{
                console.log('console isFormValid ',this.isFormValid(component, event, helper));
                console.log('console executivoSucessoCliente ',component.get("v.executivoSucessoClienteFlag"));
                this.showToast('error', 'Ops!', 'Please, fill out the Client success executive field on Commercial Details section.');                             
            }
            component.set("v.spinnerOn", false);
        }   
        //console.log('console isFormValid ',this.isFormValid(component, event, helper));
        this.showToast('warning', 'Warning!', 'Remember to send to Pipefy');
        //location.reload();
    },
    
    showToast : function(type,title, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "type": type,
            "message":message
        });
        toastEvent.fire();
    },
    
    showToastOptionNotAvalible : function(component,event,helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": 'Choose other',
            "message": 'This date must be scheduled until the 15th day from today.',
            "type": 'warning'
        });
        toastEvent.fire();
    },
    
    getExecutiveSuccessClient: function(component, event, helper) {        
        console.log('Helper getExecutiveSuccessClient');
        
        var oppId = component.get("v.proposalCreationTO.opportunityId");
        
        var action = component.get("c.getExecutiveSuccessClient");        
        action.setParams({
            "oppId": oppId
            
        });        
        action.setCallback(this, function(response){
            var state = response.getState();
            let result;           
            if(state === "SUCCESS"){
                result = response.getReturnValue();
                console.log(result);
                component.set('v.executivoSucessoClienteFlag', result)

            }else {                 
                let errors = response.getError();
                //console.log(errors);
            }
        });
        $A.enqueueAction(action);
        
    }    
    
})