({
	doInit : function(component, event, helper) {
		var action = component.get("c.createIntegrationRequest");
        var recordId = component.get("v.recordId");
        debugger;
        action.setParams({ recordId : recordId });
        action.setCallback(this, function(response) {
            var state = response.getState();
            debugger;
            if (state === "SUCCESS") {
                var returnValue = response.getReturnValue();
               	var navEvt = $A.get("e.force:navigateToSObject");
    			navEvt.setParams({
      				"recordId": returnValue
    			});
    			navEvt.fire();
          	}
            else if (state === "INCOMPLETE") {
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);	
	}
})