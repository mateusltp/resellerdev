/**
 * @description       : 
 * @author            : 
 * @group             : 
 * @last modified on  : 05-17-2022
 * @last modified by  : Mateus Augusto - GFT (moes@gft.com)
**/
({
    getPaymentMethod : function(component, helper) {

        var actionPay = component.get("c.getPayment");
        actionPay.setParams({ oppId : component.get("v.recordId")});
        actionPay.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.paymentMethod", response.getReturnValue());
                console.log('123 payment method: ' + component.get("v.paymentMethod"));
                helper.getAccOppRelationship(component, helper);

            }else{
                console.log('payment method not found');
            }
        });
        $A.enqueueAction(actionPay);
    },
    
    getAccOppRelationship : function(component, helper) {
        let proposalCreationTO = component.get("v.proposalCreationTO");

        var action = component.get("c.getAccOppSubsidyWithReseller");
    /*    if (proposalCreationTO.businessModel != 'Intermediation' && component.get("v.paymentMethod") == 'Credit Card') {
            action = component.get("c.getAccOppSubsidy");
            console.log('query subsidy credit');
        }else if (proposalCreationTO.businessModel != 'Intermediation' && component.get("v.paymentMethod") != 'Credit Card') {
            action = component.get("c.getAccOppSubsidyWithReseller");
            console.log('query subsidy whitout credit');
        }else {
            action = component.get("c.getAccOppRelationships");
            console.log('query inter');
        } */

        action.setParams({ oppId : component.get("v.recordId") });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var responseData = response.getReturnValue();
                var tableColumns = ['Name', 'BillingCity', 'BillingState', 'BillingCountry', 'Razao_Social__c', 'Id_Company__c'];
                console.log(JSON.parse(JSON.stringify(responseData)));                
                responseData.forEach(accOppRelation => {
                    accOppRelation.linkName = '/'+accOppRelation.Account__c;
                    
                    tableColumns.forEach( column => {
                        accOppRelation[column] = accOppRelation['Account__r'][column];
                    });
                }); 

                component.set("v.data", responseData);
                component.set("v.showTable", (responseData.length > 0));
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        helper.showToast("Error!",'error', "Error loading Deal Hierarchy. Please reach your administrator.\n Error Message:" + errors[0].message);
                    }
                } else {
                    helper.showToast("Error!","error", "Error loading Deal Hierarchy. Please reach your administrator.\n Error Message: Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    getDealDeskOperationalCase : function(component) {
        var action = component.get("c.getDealDeskOperationalCase");
        action.setParams({ oppId : component.get("v.recordId") });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.dealDeskOperationalCase" , response.getReturnValue() );
            } else if (state === "ERROR") {
                var errors = response.getError();
                console.log("Couldn't find specialist case id");
                console.dir(errors);
            }
        });
        $A.enqueueAction(action);
    },
    saveAccOppRelationships : function(component, helper) {
        component.set("v.spinnerOn", true);
        var action = component.get("c.saveAccOppRelationships");
        action.setParams({ 
            accOppRelationships : component.get("v.dataToUpdate")
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.showTable", false);
                helper.getAccOppRelationship(component);

                helper.showToast("Success!", 'success', "Billing percentages saved successfully!");
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        helper.showToast("Error!", 'error', ("Error loading Deal Hierarchy. Please reach your administrator.\n Error Message:" + errors[0].message) );
                    }
                } else {
                    helper.showToast("Error!", 'error', "Error loading Deal Hierarchy. Please reach your administrator.\n Error Message: Unknown error");
                }
            }
            component.set("v.spinnerOn", false);
            component.set("v.dataToUpdate", []);
        });
        $A.enqueueAction(action);
    },
    isValidPercentages : function(component, helper, draftRecords/*, feeTypes*/) {
        component.set("v.spinnerOn", true);

        var countPercentage = 0.0;
        var recordPercentage;
        var data = [...component.get("v.data")];
        var dataToUpdate = [];
        var positionRecordEdited;
        var recordsPositionsToIgnore = [];
        var recordsMap = new Map();
		
        var countPercentageMap = new Map();
        
        draftRecords.forEach(function(record) {
            var keys = [];
            
            Object.keys(record).forEach(function(key){
            	if (key != "id") keys.push(key);
            });
            
            keys.forEach(function(feeType) {
            	recordPercentage = record[feeType];
                recordPercentage = recordPercentage && recordPercentage > 0 ? parseFloat(recordPercentage) : 0;
                
                positionRecordEdited = parseInt(record['id'].replace('row-',''));
    
                recordsPositionsToIgnore.push( positionRecordEdited );
                
                if (recordsMap.has(data[positionRecordEdited].Id)) {
               		var recordObj = recordsMap.get(data[positionRecordEdited].Id);
                    recordObj[feeType] = recordPercentage;
                } else {
                    var recordObj = {
                        Id : data[positionRecordEdited].Id,
                    	[feeType] : recordPercentage
                    };
                    recordsMap.set(data[positionRecordEdited].Id, recordObj);
                }
            });
        });
        
        for (var value of recordsMap.values()) {
  			dataToUpdate.push(value);
		}
        
        var feesCountPercentages = this.verifyFeesCountPercentages(data, recordsMap);
     	var feeTypesWithIncorrectTotal = [];
        
        Object.keys(feesCountPercentages).forEach(function(key) {
       		if (key == "Billing_Percentage__c" 						&& feesCountPercentages[key] != 100) feeTypesWithIncorrectTotal.push("Enterprise Subscription");
            if (key == "Maintenance_Fee_Billing_Percentage__c" 		&& feesCountPercentages[key] != 100) feeTypesWithIncorrectTotal.push("Prof. Serv. Maintenance");
            if (key == "Setup_Fee_Billing_Percentage__c" 			&& feesCountPercentages[key] != 100) feeTypesWithIncorrectTotal.push("Setup Fee");
            if (key == "Prof_Serv_Setup_Fee_Billing_Percentage__c" 	&& feesCountPercentages[key] != 100) feeTypesWithIncorrectTotal.push("Prof. Serv. Setup Fee");
        });

        if(feeTypesWithIncorrectTotal.length == 0){ 
            component.set("v.errors", null);
            component.set("v.dataToUpdate", dataToUpdate);
            return true; 
        }
        
        component.set("v.errors", {
            table: {
                title: 'Your entry cannot be saved. Fix the errors and try again.',
                messages: [
                    'The percentage total must be 100%.'
                ]
            }
        });

        helper.showToast("Error!",'error', "Please fix deal billing percentages of " + feeTypesWithIncorrectTotal.toString().replaceAll(',', ', ') + ". The total must be 100%.");

        component.set("v.spinnerOn", false);
        return false;
    },
    canGoNextStage : function(component) {
        let saveAndCallStage = component.get('v.saveAndCallStage');

        return ( saveAndCallStage && !component.get('v.showTable') ) ||
            ( saveAndCallStage && component.get('v.showTable') && this.validationSplitBilling(component) );
    },
    validationSplitBilling: function(component) {
        var records = component.get("v.data");
        var proposalCreationTO = component.get("v.proposalCreationTO");
		var decimals = 3;
        
        if( !records || !records.length ) { return true; }

        var countPercentageES 			= 0;
        var countPercentageMainFee 		= 0;
        var countPercentageSF 			= 0;
        var countPercentageProfServSF 	= 0;

        records.forEach(function(record) {
            if( record.Billing_Percentage__c ){
                countPercentageES += record.Billing_Percentage__c;
            }

            if (record.Maintenance_Fee_Billing_Percentage__c) {
                countPercentageMainFee += record.Maintenance_Fee_Billing_Percentage__c;
            }
            
            if (proposalCreationTO.oppRecTypeDevName == 'Client_Sales_New_Business' || proposalCreationTO.oppRecTypeDevName == 'SMB_New_Business') {
            	if (record.Setup_Fee_Billing_Percentage__c) {
                    countPercentageSF += record.Setup_Fee_Billing_Percentage__c;
                }
                
                if (record.Prof_Serv_Setup_Fee_Billing_Percentage__c) {
                    countPercentageProfServSF += record.Prof_Serv_Setup_Fee_Billing_Percentage__c;
                }    
            }
            
        });
        
        countPercentageES 			= Number(Math.round(countPercentageES+'e'+decimals)+'e-'+decimals);
        countPercentageMainFee 		= Number(Math.round(countPercentageMainFee+'e'+decimals)+'e-'+decimals);
        countPercentageSF 			= Number(Math.round(countPercentageSF+'e'+decimals)+'e-'+decimals);
        countPercentageProfServSF 	= Number(Math.round(countPercentageProfServSF+'e'+decimals)+'e-'+decimals);

        if( countPercentageES != 100.0 ){
            this.setAndShowBillingPercentageErrors(component, "Enterprise Subscription");
            return false;
        }
        
        
        if (proposalCreationTO.customIntegrationsRequired == "Yes") {
        	if( countPercentageMainFee != 100.0 ){
                this.setAndShowBillingPercentageErrors(component, "Prof. Service (Maintenance)");
                return false;
            }    
        }
         
        
        if (proposalCreationTO.oppRecTypeDevName == 'Client_Sales_New_Business' || proposalCreationTO.oppRecTypeDevName == 'SMB_New_Business') {
            if( proposalCreationTO.oppRecTypeDevName == 'Client_Sales_New_Business' && countPercentageSF != 100.0 ){
                this.setAndShowBillingPercentageErrors(component, "Setup Fee");
                return false;
            }

            if ( proposalCreationTO.oppRecTypeDevName == 'SMB_New_Business' && proposalCreationTO.setupFeeRequired == 'Yes' && countPercentageSF != 100.0 ) {
                this.setAndShowBillingPercentageErrors(component, "Setup Fee");
                return false;
            }
            
            if (proposalCreationTO.customIntegrationsRequired == "Yes") {
            	if( countPercentageProfServSF != 100.0 ){
                    this.setAndShowBillingPercentageErrors(component, "Prof. Service (Setup Fee)");
                    return false;
                }    
            }
            
        }

        component.set("v.errors", null);
        return true;
    },
    setAndShowBillingPercentageErrors : function(component, feeType) {
        component.set("v.errors", {
            table: {
                title: 'Your entry cannot be saved. Fix the errors and try again.',
                messages: [
                    'Please select the billing percentages for each account. Remember, percentage total must be 100%.'
                ]
            }
        });
        this.showToast("Error!",'error', "Can't go to next stage. Split billing percentages for " + feeType + " are invalid. Remember, percentage total must be 100%");
    },
    callChangeStageEvent : function(component, paramToChangeIndex) {
		var cmpEvent = $A.get("e.c:fastTrackStageDoneEvent");
        var nextIndex = component.get("v.myStageIndex");
        nextIndex += paramToChangeIndex;
        cmpEvent.setParams({
            "opportunityId": component.get("v.recordId"),
            "nextIndex": nextIndex
        });
        cmpEvent.fire();
    },
    getDealDeskOperationalCaseFields : function(component) {
        var operationalCase = component.get("v.dealDeskOperationalCase");
        var proposalCreationTO = component.get("v.proposalCreationTO");
        var fields = {
            Subject : { value : operationalCase.Subject },
            OwnerId : { value : operationalCase.OwnerId },
            QuoteId__c : {value : proposalCreationTO.proposal.sObj.Id}
        };

        return fields;
    },
    showToast : function(title, type, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type": type
        });
        toastEvent.fire();
    },
   	getDealSplitBillingColumns : function(component) {
   		var proposalCreationTO = component.get("v.proposalCreationTO");
        var columns = [
			{ label: 'Account Name', initialWidth: 200, fieldName: 'linkName', type: 'url',
				typeAttributes: { label: { fieldName: 'Name' }, value: { fieldName: 'linkName' }, target: '_blank'}
            },
			{ label: 'City', fieldName: 'BillingCity', type: 'text' },
			{ label: 'State', fieldName: 'BillingState', type: 'text' },
			{ label: 'Country', fieldName: 'BillingCountry', type: 'text' },
            { label: 'Enterprise Subscription', fieldName: 'Billing_Percentage__c', type: 'number' , editable: true,
                typeAttributes: { maximumFractionDigits: 3, minimumFractionDigits: 1 }}
		];
        
        if (proposalCreationTO.customIntegrationsRequired == "Yes") {
            columns.push(
            	{
                    label: 'Prof. Serv. Maintenance', fieldName: 'Maintenance_Fee_Billing_Percentage__c', type: 'number' , editable: true,
                	typeAttributes: { maximumFractionDigits: 3, minimumFractionDigits: 1 } 
                }
            );
        }
        
        if (proposalCreationTO.oppRecTypeDevName == 'Client_Sales_New_Business' || proposalCreationTO.oppRecTypeDevName == 'SMB_New_Business') {
            if (proposalCreationTO.setupFeeRequired != 'No') {
                columns.push(
                    { 
                        label: 'Setup Fee', fieldName: 'Setup_Fee_Billing_Percentage__c', type: 'number' , editable: true,
                        typeAttributes: { maximumFractionDigits: 3, minimumFractionDigits: 1 } 
                    }
                );
            }
            
            if (proposalCreationTO.customIntegrationsRequired == "Yes") {
                columns.push(
                    { 
                        label: 'Prof. Serv. Setup Fee', fieldName: 'Prof_Serv_Setup_Fee_Billing_Percentage__c', type: 'number' , editable: true,
                        typeAttributes: { maximumFractionDigits: 3, minimumFractionDigits: 1 } 
                    }
                );
            }
            
        }
        
        return columns;
   	},
	verifyFeesCountPercentages : function(data, recordsMap) {
        var feesCountPercentages = {};
        var feeTypeToIdList = new Map();
        var decimals = 3;
        
        for (var value of recordsMap.values()) {
            Object.keys(value).forEach(function(key) {
            	if (key != "Id") feesCountPercentages[key] = 0;
            });
		}
        
        var idArray = [ ...recordsMap.keys() ];
        
        idArray.forEach(function(recordId) {
        	var record = recordsMap.get(recordId);
            
            Object.keys(record).forEach(function(objectKey) {
                if (objectKey != "Id") {
                	if (!feeTypeToIdList.has(objectKey)) {
               			feeTypeToIdList.set(objectKey, new Set([recordId]));
                        
                        var value = feesCountPercentages[objectKey] + record[objectKey];
                        feesCountPercentages[objectKey] = Number(Math.round(value+'e'+decimals)+'e-'+decimals);
                    } else {
                        feeTypeToIdList.get(objectKey).add(recordId);
                        
                        var value = feesCountPercentages[objectKey] + record[objectKey];
                        feesCountPercentages[objectKey] = Number(Math.round(value+'e'+decimals)+'e-'+decimals);
                    }
                }
            });
        });
        
        var feeTypeArray = [ ...feeTypeToIdList.keys() ];
        
        feeTypeArray.forEach(function(feeType) {
        	var idSet = feeTypeToIdList.get(feeType);
            
            data.forEach(function(record) {
                if (!idSet.has(record.Id)) {
                    feesCountPercentages[feeType] = feesCountPercentages[feeType] + (record[feeType] ? record[feeType] : 0);
                }
            });
        });

		return feesCountPercentages;        
    },

    loadClientManagers: function(component, callback, recordId) {
        let clientManagersService = component.get("c.findClientManagers");
        clientManagersService.setParams({
            'recordId': recordId
        });        
        clientManagersService.setCallback(this, callback);        
        $A.enqueueAction(clientManagersService);
    },
    isResponseSuccessful : function(response) {
        return (response.getState() === "SUCCESS");
    },
    fireSaveAndRefreshOfferEvent : function(component)  {
        var cmpEvent = $A.get("e.c:saveAndRefreshOfferEvent");
        cmpEvent.setParams({
            "changedProposalCreationTO": component.get("v.proposalCreationTO")
        });        
        cmpEvent.fire();
    },

    validateMainContactFields: function (component) {
        var mainContactField = component.find('maincontactfield');
        if( !mainContactField ){ return true; }
        var allValid = mainContactField.reduce(function(validSoFar, inputCmp){
            inputCmp.showHelpMessageIfInvalid();     
            return validSoFar && inputCmp.get('v.validity').valid;
        }, true);
        if(!allValid){
            this.showToast('Required Fields Missing', 'warning', 'Please enter required fields.');
        }
        return allValid;
    },

    validateEligFields: function (component) {
        var to = component.get("v.proposalCreationTO");
        if (to.oppRecTypeDevName == 'SMB_New_Business' || to.oppRecTypeDevName == 'SMB_Success_Renegotiation' || to.oppRecTypeDevName == 'Indirect_Channel_New_Business') return true;
		
        var fieldEleg = component.find('mainEligField');
        var fieldMemb = component.find('mainMembershipField');
        var showValidationError = false;
        var vaildationFailReason = '';

        if($A.util.isEmpty(fieldEleg.get("v.value"))){
            showValidationError = true;
            vaildationFailReason = "Eligibility fields cannot be empty!";
        }else if(fieldEleg.get("v.value") == 'Yes'){
            if($A.util.isEmpty(fieldMemb.get("v.value"))){
                showValidationError = true;
                vaildationFailReason = "Eligibility fields cannot be empty!";
            }
        }
        
        if(showValidationError){
            this.showToast('Required Fields Missing', 'warning', vaildationFailReason);
            return false;
        }
            
        return true;
    },
                    
   getExecutiveSuccessClient: function(component, event, helper) {        
        console.log('Helper getExecutiveSuccessClient');
        
        var oppId = component.get("v.proposalCreationTO.opportunityId");
        
        var action = component.get("c.getExecutiveSuccessClient");        
        action.setParams({
            "oppId": oppId
            
        });        
        action.setCallback(this, function(response){
            var state = response.getState();
            let result;           
            if(state === "SUCCESS"){
                result = response.getReturnValue();
                console.log(result);
                component.set('v.executivoSucessoClienteFlag', result)

            }else {                 
                let errors = response.getError();
                //console.log(errors);
            }
        });
        $A.enqueueAction(action);
        
    }                  
                    
})