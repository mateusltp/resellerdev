<!--
  @description       : 
  @author            : roei@gft.com
  @group             : 
  @last modified on  : 05-17-2022
  @last modified by  : Mateus Augusto - GFT (moes@gft.com)
  Modifications Log 
  Ver   Date         Author         Modification
  1.0   09-28-2020   roei@gft.com   Initial Version
-->
<aura:component access="global" controller="FastTrackProposalCreationController">
  
    <aura:attribute name="proposalCreationTO" type="FastTrackProposalCreationTO"/>   
    <aura:attribute name="recordId" type="String"/>
    <aura:attribute name="myStageIndex" type="Integer" default="6" />
    <aura:attribute name="data" type="Object"/>
    <aura:attribute name="saveAndCallStage" type="Boolean" default="false" />
    <aura:attribute name="dataToUpdate" type="Account_Opportunity_Relationship__c[]"/>
    <aura:attribute name="columns" type="List"/>
    <aura:attribute name="draftRecords" type="List" default="[]"/>
    <aura:attribute name="showTable" type="Boolean" default="true" />
    <aura:attribute name="noHierarchyLabel" type="String" />
    <aura:attribute name="errors" type="Object"/>
    <aura:attribute name="dealDeskOperationalCase" type="Case"/>
    <aura:attribute name="hasVendorPortal" type="boolean" default="true" />
    <aura:attribute name="IsMainContactsSavedAndRefreshed" type="boolean" default="false" />
    <aura:attribute name="clientManagers" type="Contact[]" />
    <aura:attribute name="spinnerOn" type="Boolean" />
    <aura:attribute name="activeSection2" type="List" default="['stepMainContacts', 'operationalCase']" />
    <aura:attribute name="paymentMethod" type="String"/>
	<aura:attribute name="executivoSucessoClienteFlag" type="Boolean" default= "false"/>    
	

    <aura:handler name="init" value="{!this}" action="{! c.doInit }"/>
    <aura:registerEvent name="saveAndRefreshOfferEvent" type="c:saveAndRefreshOfferEvent"/>

    <aura:handler name="change"  value="{!v.proposalCreationTO.proposal.sObj.Payroll_Debite_Memo_Contact__c}"   action="{!c.handleOfferChanges}"/>
    <aura:handler name="change"  value="{!v.proposalCreationTO.proposal.sObj.Operations__c}"   action="{!c.handleOfferChanges}"/>
    <aura:handler name="change"  value="{!v.proposalCreationTO.proposal.sObj.Finance__c}"   action="{!c.handleOfferChanges}"/>
    <aura:handler name="change"  value="{!v.proposalCreationTO.proposal.sObj.ContactId}"   action="{!c.handleOfferChanges}"/>

   
    
     <lightning:accordion 
     aura:id="hierarchyAccordion"
     activeSectionName="{!v.activeSection2}"
     allowMultipleSectionsOpen="true">
    <!-- start main contact section-->  
              
            <lightning:accordionSection
                    name="stepMainContacts"
                    label="Main contacts"
                    class="slds-size_12-of-12 slds-m-vertical_large">

                <div class="slds-grid slds-gutters">
                <!--Client Manager Contact-->
                <div class="slds-col slds-size_2-of-8">
                    <div class="slds-form-element__control field-padding-top">
                        <lightning:select   aura:id="maincontactfield" name="clientmanagerSelector" 
                                            label="Administrator contact" 
                                            required="true"
                                            messageWhenValueMissing="Please, select a Client Manager Contact."
                                            value="{!v.proposalCreationTO.proposal.sObj.ContactId}">
                            <option value="">-- None --</option>
                            <aura:iteration items="{!v.clientManagers}" var="ctManager">
                                <option 
                                    selected="{!v.proposalCreationTO.proposal.sObj.ContactId == ctManager.Id}" value="{!ctManager.Id}" text="{!ctManager.Name}">
                                </option> 
                            </aura:iteration>
                        </lightning:select>                
                    </div>
                </div>            
                <!--Client Finance Contact-->
                <div class="slds-col slds-size_2-of-8">
                    <div class="slds-form-element__control field-padding-top">
                        <lightning:select   aura:id="maincontactfield" name="clientfinanceSelector" 
                                            label="Finance contact" 
                                            required="true"
                                            messageWhenValueMissing="Please, select a Client Finance Contact"
                                            value="{!v.proposalCreationTO.proposal.sObj.Finance__c}">    
                                                        
                            <option value="">-- None --</option>
                            <aura:iteration items="{!v.clientManagers}" var="ctManager">                            
                                <option 
                                    selected="{!v.proposalCreationTO.proposal.sObj.Finance__c == ctManager.Id}" value="{!ctManager.Id}" text="{!ctManager.Name}">
                                </option>                   
                            </aura:iteration>
                        </lightning:select>                
                    </div>
                </div>
                <!--Client Operations Contact-->
                <div class="slds-col slds-size_2-of-8">
                    <div class="slds-form-element__control field-padding-top">
                        <lightning:select   aura:id="maincontactfield" name="clientOperationSelector" 
                                            label="Operations contact" 
                                            required="true"
                                            messageWhenValueMissing="Please, select a Client Operations Contact"
                                            value="{!v.proposalCreationTO.proposal.sObj.Operations__c}">
                            <option value="">-- None --</option>
                            <aura:iteration items="{!v.clientManagers}" var="ctManager">                            
                                <option 
                                    selected="{!v.proposalCreationTO.proposal.sObj.Operations__c == ctManager.Id}" value="{!ctManager.Id}" text="{!ctManager.Name}">
                                </option>                   
                            </aura:iteration>
                        </lightning:select>                
                    </div>
                </div>
                <!--Payroll Debite Memo Contact-->
                <div class="slds-col slds-size_2-of-8">
                    <div class="slds-form-element__control field-padding-top">
                        <lightning:select   aura:id="maincontactfield" name="payrollDebiteMemoContactSelector" 
                                            label="Payroll debite memo contact" 
                                            required="true"
                                            messageWhenValueMissing="Please, select a Client Memo Contact"
                                            value="{!v.proposalCreationTO.proposal.sObj.Payroll_Debite_Memo_Contact__c}">
                            <option value="">-- None --</option>
                            <aura:iteration items="{!v.clientManagers}" var="ctManager">
                                <option 
                                    selected="{!v.proposalCreationTO.proposal.sObj.Payroll_Debite_Memo_Contact__c == ctManager.Id}" value="{!ctManager.Id}" text="{!ctManager.Name}">
                                </option> 
                            </aura:iteration>
                        </lightning:select>                
                    </div>
                </div>
            </div>
       

        </lightning:accordionSection> 
   
        
         
        <!--End Marcus-->        
       
            <aura:if isTrue="{! v.showTable }">
                <c:OpportunityAccountManagerForClients
                        proposalCreationTO="{!v.proposalCreationTO}"
                        data="{!v.data}"
                        paymentMethod="{!v.paymentMethod}"
                    >
                </c:OpportunityAccountManagerForClients>
            </aura:if>
              
    </lightning:accordion>
    
    <hr/>

        
    <lightning:quickActionAPI aura:id="quickActionAPI" />

    <lightning:accordion aura:id="dealDeskOperationalCase">
        <lightning:accordionSection name="operationalCase" label="Deal Desk operational case">

        
            <aura:if isTrue="{! v.dealDeskOperationalCase.Id }">

                <lightning:recordEditForm recordId="{! v.dealDeskOperationalCase.Id }" objectApiName="Case">
                    <div class="slds-grid slds-wrap">
                        <div class="slds-col slds-size_1-of-2">
                            <lightning:outputField fieldName="Status" />
                        </div>
                        <div class="slds-col slds-size_1-of-2">
                            <lightning:outputField fieldName="OwnerId" />
                        </div>
                    </div>    
                </lightning:recordEditForm>

                <aura:if isTrue="{! v.dealDeskOperationalCase.IsClosed }">
                    <lightning:button class="slds-align_absolute-center" name="createCase" label="Create a new Case" onclick="{! c.createDealDeskOperationalCase }" />
                </aura:if>

                <aura:set attribute="else">
                    <span>Your offer might need approval - please check the status of the approval steps.</span>

                    <div class="slds-float_right">
                    <lightning:button variant="brand" name="createCase" label="Create a case" onclick="{! c.createDealDeskOperationalCase }" />
                    </div>
                </aura:set>
            </aura:if>
        </lightning:accordionSection>
    </lightning:accordion>

    <hr/>

    <lightning:buttonGroup class="slds-m-bottom_medium slds-float_right">
        <lightning:button aura:id="savebtn" label="Save" type="submit" class="save-button-in-section slds-m-horizontal_x-small"  variant="destructive" name="save"  onclick="{!c.saveOpportunityAndProposal}"/>
        <lightning:button label="Continue" variant="brand" type="submit" name="continue"  onclick="{!c.nextStage}"/>
      </lightning:buttonGroup>
      
      <div class="slds-clearfix">
          <lightning:button label="Back" class="slds-m-top_small slds-float_left" variant="neutral"  name="back" aura:id="btnBack"  onclick="{!c.backStage}"/>
      </div>
    
    
    <aura:if isTrue="{!v.spinnerOn}">
        <lightning:spinner alternativeText="Loading" size="medium" />
    </aura:if>
</aura:component>