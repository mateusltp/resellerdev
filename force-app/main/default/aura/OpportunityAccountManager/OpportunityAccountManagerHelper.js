({
    setChildAccounts : function(component, event, helper) {        
        var action = component.get("c.getRelatedAccounts");
        var currentOppId = component.get("v.recordId");
        action.setParams({
            "oppId": currentOppId
        });
      

        action.setCallback(this, function(response){
            var state = response.getState();  
            console.log(state);
            var data = response.getReturnValue();  
            console.log(data);
            var accWithOpp = data["AccWithOpp"];
            console.log(accWithOpp);
            var accNoOpp = data["AccWithNoOpp"];
            console.log(accNoOpp);
            
            if (state === "SUCCESS") {
                accWithOpp.forEach(function(accWithOpp){
                    accWithOpp.linkName = '/'+accWithOpp.Id;
                });   
                accNoOpp.forEach(function(accNoOpp){
                    accNoOpp.linkName = '/'+accNoOpp.Id;
                }); 

                component.set("v.accLst", accNoOpp);
                component.set("v.filteredAccLst", accNoOpp);
                component.set("v.aorLst", accWithOpp);
                component.set("v.filteredAorLst", accWithOpp);

            } else {
                console.log("ERROR retrieving data");
            }
        });
        $A.enqueueAction(action); 
    }, 
    
    handleSubmit : function(component, event, helper) {        
        var objSelected = component.find("accountTable").getSelectedRows();               
        if(objSelected.length == 0){
            this.showToast(component, event, helper);
            return;
        }
        component.set("v.load", true);
        component.find("btnSubmit").set("v.disabled", true);
        var objUncovered = JSON.parse(JSON.stringify(objSelected));
        var currentOppId = component.get("v.recordId");
        var action = component.get("c.setRelationshipAccountWithOpp");
       
        action.setParams({
            "accLst": objUncovered,
            "oppId": currentOppId            
        });
        action.setCallback(this, function(response){
            var state = response.getState();  
            var data = response.getReturnValue();                          
            if (state === "SUCCESS") {       
                window.location.reload();

            }
        });
        $A.enqueueAction(action); 
    },

    onChangeFilter: function(component, event, helper) {
        var nameSource = event.getSource().get("v.name");
        if(nameSource == "accFilter"){
            let data =  component.get("v.accLst"),
                        term = component.get("v.filter"),
                        results = data, regex;
            try {
                regex = new RegExp(term, "i");
                results = data.filter(row=>regex.test(row.Name) || regex.test(row.ShippingCity) || regex.test(row.ShippingState));
            } catch(e) {
                
            }
            component.set("v.filteredAccLst", results);
        } else {
            let data =  component.get("v.aorLst"),
                        term = component.get("v.filterAor"),
                        results = data, regex;
            try {
                regex = new RegExp(term, "i");
                results = data.filter(row=>regex.test(row.Name) || regex.test(row.ShippingCity) || regex.test(row.ShippingState));
            } catch(e) {
                
            }
            component.set("v.filteredAorLst", results);
        }       
       
    },

    handleRemove : function(component, event, helper) {   
         
        var objSelected = component.find("accountTableAor").getSelectedRows(); 
        if(objSelected.length == 0){
            this.showToast(component, event, helper);
            return;
        }
        component.set("v.loadRemove", true);
        component.find("btnRemove").set("v.disabled", true);
        var objUncovered = JSON.parse(JSON.stringify(objSelected));
        var currentOppId = component.get("v.recordId");     
        var action = component.get("c.breakRelationshipAccountWithOpp");
        action.setParams({
            "accLst" : objUncovered,
            "oppId" : currentOppId
        });

        action.setCallback(this, function(response){
            var state = response.getState();  
            var data = response.getReturnValue();                          
            if (state === "SUCCESS") {       
                window.location.reload();
            }else {
                console.log("error removing locations!");
            }
        });

        $A.enqueueAction(action); 
        
    },

    showToast : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "type": "warning",
            "title": "Warning!",
            "message": "Please select a location!"
        });
        toastEvent.fire();
    }
})