/**
 * @description       : 
 * @author            : ext.gft.marcus.silva@gympass.com
 * @group             : 
 * @last modified on  : 03-03-2022
 * @last modified by  : ext.gft.marcus.silva@gympass.com
**/
({
    getContracts: function (component) {
        component.set("v.columnsContract", [
            { label: 'Contract', fieldName: 'nameUrl', type: 'url', typeAttributes: { label: { fieldName: 'Name' } } },
            { label: 'Status', fieldName: 'APXT_Redlining__Status__c' },
            { label: 'Signed Method', fieldName: 'SignedMethod__c' },
            //{ label: 'T&C', fieldName: 'Include_full_T_C_s__c', type: 'boolean' },
            { label: 'Renegotiation', fieldName: 'APXT_Renegotiation__c', type: 'boolean' },
            { label: 'Created By', fieldName: 'createdByUrl', type: 'url', typeAttributes: { label: { fieldName: 'createdByName' } } },
            {
                label: 'Date/Time Opened', fieldName: 'CreatedDate', type: 'date',
                typeAttributes: {
                    year: "numeric",
                    month: "long",
                    day: "2-digit",
                    hour: "2-digit",
                    minute: "2-digit"
                }
            }
        ]);

        var action = component.get("c.getContractAgreement");
        action.setParams({
            recordId: component.get("v.opportunityId")
        });

        action.setCallback(this, function (response) {
            var state = response.getState();

            if (state === 'SUCCESS') {
                var contracts = response.getReturnValue();

                contracts.forEach(function(row){
                    row.nameUrl = `/${row.Id}`;
                    row.createdByUrl = `/${row.CreatedBy.Id}`;
                    row.createdByName = row.CreatedBy.Name;
                });
                
                component.set("v.contracts", contracts);
                
            } else if (state === 'ERROR') {
                console.log("errors: " + response.getError());                
            }
        });
        $A.enqueueAction(action);
    }, 

    getCases: function (component) {
        component.set("v.columnsCase", [
            { label: 'Case', fieldName: 'nameUrl', type: 'url', typeAttributes: { label: { fieldName: 'CaseNumber' } } },
            { label: 'Owner', fieldName: 'OwnerUrl', type: 'url', typeAttributes: { label: { fieldName: 'OwnerName' } } },
            { label: 'Contract Agreement', fieldName: 'ContractAgreementUrl', type: 'url', typeAttributes: { label: { fieldName: 'ContractAgreementName' } } },
            { label: 'Status', fieldName: 'Status' },
            { label: 'Created By', fieldName: 'createdByUrl', type: 'url', typeAttributes: { label: { fieldName: 'createdByName' } } },
            {
                label: 'Date/Time Opened', fieldName: 'CreatedDate', type: 'date',
                typeAttributes: {
                    year: "numeric",
                    month: "long",
                    day: "2-digit",
                    hour: "2-digit",
                    minute: "2-digit"
                }
            }]);        

        var action = component.get("c.getCase");
        action.setParams({
            recordId: component.get("v.opportunityId")
        });

        action.setCallback(this, function (response) {
            var state = response.getState();

            if (state === 'SUCCESS') {                
                var cases = response.getReturnValue();
                cases.forEach(function(row){
                    row.nameUrl = `/${row.Id}`;
                    row.createdByUrl = `/${row.CreatedBy.Id}`;
                    row.createdByName = row.CreatedBy.Name;
                    row.OwnerUrl = `/${row.Owner.Id}`;
                    row.OwnerName = row.Owner.Name;
                    row.ContractAgreementUrl = `/${row.ContractAgreement__r.Id}`;
                    row.ContractAgreementName = row.ContractAgreement__r.Name;
                });                
                
                component.set("v.cases", cases);
                
            } else if (state === 'ERROR') {
                console.log("errors: " + response.getError());                
            }
        });
        $A.enqueueAction(action);
    },   
   
    getProposalId: function (component, recordId) {
        var action = component.get("c.getProposalId");
        action.setParams({
            recordId: component.get("v.opportunityId")
        });

        action.setCallback(this, function (response) {
            var state = response.getState();

            if (state === 'SUCCESS') {
                var proposalId = response.getReturnValue();
                component.set("v.proposalId", proposalId);
            } else if (state === 'ERROR') {
                console.log("errors: " + response.getError());
            }
        });
        $A.enqueueAction(action);
    }
})