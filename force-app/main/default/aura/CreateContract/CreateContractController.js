/**
 * @description       : 
 * @author            : ext.gft.marcus.silva@gympass.com
 * @group             : 
 * @last modified on  : 05-09-2022
 * @last modified by  : tiago.ribeiro@gympass.com
**/
({
    init : function(component, event, helper) {
        var recordId = component.get("v.opportunityId");
        helper.getProposalId(component, recordId);
        helper.getContracts(component);
        helper.getCases(component);
    },

    handleClick : function(component){
        component.set("v.isModalOpen", true);
        var flow = component.find("flowData");
        var inputVariables = [
            {
                name : "recordId",
                type : "String",
                value : component.get("v.proposalId")
            }
        ];
        flow.startFlow("Create_a_Contract_Final", inputVariables );                
    },
    closeModal : function(component)    {
        component.set("v.isModalOpen", false);
    }
    
})