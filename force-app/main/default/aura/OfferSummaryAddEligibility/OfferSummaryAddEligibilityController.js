({
	handleSuccess: function(component, event, helper) {
		helper.showToast('success', 'Success', 'The changes have been saved');
        // helper.fireFastTrackEvaluationRequestEvent(component, helper);
        //helper.fireRefreshMainFastTrackCmpEvent();
		component.find('overlayLib').notifyClose();
        location.reload();
    },
    handleError: function(component, event, helper) {
        helper.showToast('error', 'Error', 'The changes have been saved');
    },
    handleCancel: function(component, event, helper) {
        component.find('overlayLib').notifyClose();
    }
})