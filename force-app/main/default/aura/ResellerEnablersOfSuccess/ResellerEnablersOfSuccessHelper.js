({
	loadEnablers : function(component, helper) {
		var to = component.get("v.proposalCreationTO");

		var action = component.get("c.getEnablers");
		action.setParams({ oppId : to.opportunityId });
		
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
				var enablers = response.getReturnValue();
				console.log('enablers : ' + JSON.stringify(enablers))
				console.log('enablers : ' + enablers)
				helper.setAttributes(component, helper, enablers);
            }
            else {
				alert("Enablers could not be retrieved");
			}
        });
		$A.enqueueAction(action);
	},

	setAttributes: function(component, helper, enablers) {
		console.log(enablers);
		component.set("v.totalEnablersNumber", enablers.length);
		helper.setAchievedEnablersNumber(component, enablers);
	},

	setAchievedEnablersNumber: function(component, enablers) {
		var achievedEnablersNumber = 0;
		var missingEnablersList = [];
		var achievedEnablersList = [];

		for (var i=0; i<enablers.length; i++) {
			if (enablers[i].Achieved__c == 'Yes') {
				achievedEnablersList.push(enablers[i].Name);
				achievedEnablersNumber++;
			}

			if (enablers[i].Achieved__c == 'No') {
				missingEnablersList.push(enablers[i]);
			}
		}

		component.set("v.achievedEnablersNumber", achievedEnablersNumber);
		component.set("v.missingEnablers", missingEnablersList);
		component.set("v.achievedEnablers", achievedEnablersList);
		this.setCheckbox(component, achievedEnablersList);
	},

	setCheckbox: function(component, achievedEnablersList) {
		component.set("v.valueRequire", achievedEnablersList);
		component.set("v.valueDont", achievedEnablersList);
	}
})