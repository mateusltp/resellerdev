({
    showToast: function (title, message, type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type": type,
            "mode" : 'sticky'
        });
        toastEvent.fire(); 
    },

    getMetadata : function (cmp) {
        var getMetadataAction = cmp.get("c.getMetadata");
        var spinner = cmp.find("mySpinner");
        
        getMetadataAction.setCallback(this, function (response) {
            var getMetadataResponseState = response.getState();
            if (getMetadataResponseState === "SUCCESS") {
                var returnValue = response.getReturnValue();
                cmp.set("v.options", returnValue.sectorOptions);
                cmp.set("v.countries", returnValue.countries);
                cmp.set("v.countryToStatesMap", returnValue.countriesToStatesList);
                debugger;
                cmp.set('v.loaded', 'true');
            }   
            else {
                console.log("Get Metadata ERROR");
                cmp.set('v.loaded', 'true');
            }
        });

        $A.enqueueAction(getMetadataAction);
    },
    getCreateAccountParams : function (cmp, helper) {
        return {
            accountProspectingId: cmp.get("v.accPropectingId"),
            website: cmp.find("website-input").get("v.value"),  
            sector: cmp.find("sector-select").get("v.value"),
            country: cmp.find("country-select").get("v.value"),
            state: cmp.find("state-select").get("v.value"),
            city: cmp.find("city-input").get("v.value"),
            street: cmp.find("street-textarea").get("v.value"),
            language: helper.getUrlParameter('language')
        }
    },
    validateAccountFields : function(cmp, helper) {
        var website = cmp.find("website-input").get("v.value"); 
        var sector = cmp.find("sector-select").get("v.value");
        var country = cmp.find("country-select").get("v.value");
        var state = cmp.find("state-select").get("v.value");
        var states = cmp.get("v.countryToStatesMap")[country];


        var fieldsWithError = [];
        var status;
        var errorMessage = '';
        var errorMessageAux = '';
        
        if (website == '') {
            fieldsWithError.push('Website');
        }
        if (sector == '') {
            fieldsWithError.push('Sector');
        }
        if (country == '') {
            fieldsWithError.push('Billing Country');
        }
        if (states != undefined && states.length > 0 && (state == null || state == undefined)) {
            fieldsWithError.push('Billing State/Province');
        }

        if (fieldsWithError.length > 0) {
            status = 'ERROR';
            errorMessage = $A.get("$Label.c.Fields_Validation_Error") + ' '; 
            errorMessageAux = errorMessage;
            for (var i = 0; i < fieldsWithError.length; i++) {
                if (errorMessage == errorMessageAux) {
                    errorMessage += fieldsWithError[i];
                } else {
                    errorMessage += ', ' + fieldsWithError[i];
                }
            }
        } else {
            status = 'SUCCESS';
        }

        return {
            status : status,
            errorMessage : errorMessage 
        };
    },
    getUrlParameter : function(sParam) {
    	var sPageURL = decodeURIComponent(window.location.search.substring(1));
        var sURLVariables = sPageURL.split('&');
        var sParameterName;

        for (var i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');
    
            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
	}
})