/**
 * @description       : 
 * @author            : gepi@gft.com
 * @group             : 
 * @last modified on  : 04-14-2022
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/
({
    load : function(component, event, helper) { 

        var recordId = component.get("v.recordId");

        var action = component.get('c.findRecordType');

        action.setParams({
            "recordId": recordId
        });
        
        action.setCallback(this, function(response){
            var state = response.getState();  
            var data = response.getReturnValue();      
            console.log('data' + data)            
            if (state === "SUCCESS") { 
                component.set("v.rtOPP", data); 
                if(component.get("v.rtOPP") == 'Partner_Flow_Child_Opportunity'){

                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "mode":"dismissible",
                        "type": "warning",
                        "title": "Warning!",
                        "message": "This is a child product. You are not allowed to edit it."
                    });
                    toastEvent.fire();


                    var navService = component.find("navService");
                    // Sets the route to /lightning/o/Account/home
                    var pageReference = {
                        type: 'standard__recordPage',
                        attributes: {
                            recordId: recordId,
                            objectApiName: 'Product_Item__c',
                            actionName: 'view'
                        }
                    
                    };
                    component.set("v.pageReference", pageReference);
                    navService.navigate(pageReference);
                }
            }
        }); 
             
        $A.enqueueAction(action);  
        // $A.get('e.force:refreshView').fire();

        
        // Set the URL on the link or use the default if there's an error
        // var defaultUrl = "#";
        // navService.generateUrl(pageReference)
        //     .then($A.getCallback(function(url) {
        //         component.set("v.url", url ? url : defaultUrl);
        //     }), $A.getCallback(function(error) {
        //         component.set("v.url", defaultUrl);
        //     }));
       
    },

    back : function(component, event, helper) { 
        Window.history.previous() 
    }

})