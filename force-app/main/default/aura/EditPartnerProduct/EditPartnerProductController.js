/**
 * @description       : 
 * @author            : gepi@gft.com
 * @group             : 
 * @last modified on  : 04-06-2022
 * @last modified by  : gepi@gft.com
**/

({
    reInit : function(component, event, helper) {
        helper.load(component, event, helper);
    }
});