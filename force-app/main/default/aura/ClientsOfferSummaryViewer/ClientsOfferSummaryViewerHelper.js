({
    load: function(component, recordId, helper) {
        return new Promise(function(resolve, reject){
            let callback = function (response) {
                if (helper.isResponseSuccessful(response)) {                    
                    component.set("v.proposalCreationTO", response.getReturnValue());  
                    component.set("v.proposalCreationTO.approvalPending",true);                 
                    resolve();
                } else {
                    reject("Error while loading this component. Contact your administrator");
                }                
                $A.get('e.force:refreshView').fire();
                component.set("v.spinnerOn", false);
            };
            let loadService = component.get("c.find");
            loadService.setParams({
                'recordId': recordId
            });
            loadService.setCallback(this, callback);        
            $A.enqueueAction(loadService);
        });
    },
    showToast : function(type, title, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type": type
        });
        toastEvent.fire();
    },
    isResponseSuccessful : function(response) {
        return (response.getState() === "SUCCESS");
    },
})