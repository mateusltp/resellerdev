({
    doInit: function (component, event, helper) {
        component.set("v.spinnerOn", true);
        Promise.all([
            helper.load(component, component.get("v.recordId"), helper),
          
          ]).then(
              function(response) {                
                $A.get('e.force:refreshView').fire();
                component.set("v.spinnerOn", false);
              }
          ).catch(
              function(error) {
                helper.showToast("error", "Error!", error);
                component.set("v.spinnerOn", false);
              }
          );
        
      },
})