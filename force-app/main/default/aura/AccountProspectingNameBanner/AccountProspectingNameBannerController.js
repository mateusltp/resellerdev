({
    doInit : function(cmp, event, helper) {
        debugger;
        var thisAccountProspectingId = cmp.get("v.recordId");
        var getAccProspectInfoAction = cmp.get("c.getAccProspectInfo");
        getAccProspectInfoAction.setParams({ accountProspectingId: thisAccountProspectingId });

        getAccProspectInfoAction.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var returnValue = response.getReturnValue();
                cmp.set('v.accProspectingObjLabel', returnValue.accProspectingObjLabel);
                cmp.set('v.accProspectingName', returnValue.accProspectingName);
            }
        });

        $A.enqueueAction(getAccProspectInfoAction);        
    }
})