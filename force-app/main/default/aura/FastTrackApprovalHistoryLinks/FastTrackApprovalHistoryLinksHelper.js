({
    getWaiverApprovalHistoryLinks : function(component){
        var navService = component.find("navService");
        var lstWaiver = this.getWaivers(component);
        var lstPageReference = [];
        var lstGenerateUrl = [];
        var lstWaiverNames = [];

        lstWaiver.forEach(waiver => {
            lstPageReference.push({
                "type": "standard__recordRelationshipPage",
                "attributes": {
                    "recordId": waiver.Id,
                    "relationshipApiName": "ProcessSteps",
                    "actionName": "view"
                }
            });
            lstWaiverNames.push(waiver.Name);
        });

        lstPageReference.forEach(pageReference => {
            lstGenerateUrl.push(navService.generateUrl(pageReference));
        });

        Promise.all(lstGenerateUrl)
            .then(function(result){
                var objs = [];

                for(var i = 0; i < result.length ; i++){
                    objs.push({
                        name : lstWaiverNames[i],
                        link : result[i]
                    });
                }

                component.set("v.waivers", objs);
            });
    },
    getQuoteApprovalHistoryLink : function(component, lstQuote){
        var navService = component.find("navService");
        var lstPageReference = [];
        var lstGenerateUrl = [];
        var proposal = component.get("v.proposalCreationTO.proposal");
        var isSmb = component.get("v.proposalCreationTO.oppRecTypeDevName").includes("SMB");
        var quote;

        if( !proposal.sObj || ( !proposal.enterpriseDiscountApprovalNeeded && !proposal.setupDiscountApprovalNeeded && !isSmb ) ||
            ( !proposal.sObj.Enablers_Approval_Needed__c && isSmb && !proposal.enterpriseDiscountApprovalNeeded && !proposal.setupDiscountApprovalNeeded )  ){
            return;
        }
        quote = proposal.sObj;

        lstPageReference.push({
            "type": "standard__recordRelationshipPage",
            "attributes": {
                "recordId": quote.Id,
                "relationshipApiName": "ProcessSteps",
                "actionName": "view"
            }
        });

        lstPageReference.forEach(pageReference => {
            lstGenerateUrl.push(navService.generateUrl(pageReference));
        });

        Promise.all(lstGenerateUrl)
            .then(function(result){
                var obj = {
                    name : quote.Name
                };

                for(var i = 0; i < result.length ; i++){
                    obj.link = result[i];
                }

                component.set("v.quote", obj);
            });
    },
    getWaivers : function(component){
        var payments = component.get("v.proposalCreationTO.proposal.accessFee.payments");
        var waiversToApprove = [];
        var waivers = [];

        if( !payments ){ return waiversToApprove; }

        payments.forEach((payment) => {
            waivers.push.apply(waivers, payment.waivers); 
        });

        waivers.forEach((waiver) => {
            if(waiver.sObj && waiver.sObj.Approval_Status_Formula__c != "No Need for Approval"){
                waiversToApprove.push(waiver.sObj);
            }
        });

        return waiversToApprove;
    }
})