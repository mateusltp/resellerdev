({
    doInit : function(component, event, helper) {
        helper.getWaiverApprovalHistoryLinks(component);
        helper.getQuoteApprovalHistoryLink(component);
    }
})