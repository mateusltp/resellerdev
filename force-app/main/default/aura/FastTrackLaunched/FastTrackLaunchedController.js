({
    changeStage : function(component, event, helper) {
		helper.callChangeStageEvent(component, event);
	}	,
	showSuccessToast : function(component, event, helper) {
        helper.showToast('success', 'SUCCESS!', 'Opportunity update sucessfully!');
    },
    handleSuccess : function(component, event) {       
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success",
            "message": "Saved",
            "type": "success"
        });
        toastEvent.fire(); 
        component.set("v.spinnerOn", false);
    },

    handleSubmit : function(component, event){
        component.set("v.spinnerOn", true);
    },
})