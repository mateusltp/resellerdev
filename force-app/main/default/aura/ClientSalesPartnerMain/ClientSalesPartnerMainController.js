({
    doInit : function(component, event, helper) {
        var url = $A.get('$Resource.Community_Background');
        component.set('v.backgroundImageURL', url);
    },
    
    openModel: function(component, event, helper) {
      component.set("v.isModalOpen", true);
   },
  
   closeModel: function(component, event, helper) {
      component.set("v.isModalOpen", false);
   },
  
   submitDetails: function(component, event, helper) {
      component.set("v.isModalOpen", false);
   },
})