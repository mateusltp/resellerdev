({
    callStage : function(component, event) {  
        component.set('v.saveAndCallStage',true);          
    },

    justSave : function(component, event) {  
        component.set('v.saveAndCallStage',false);          
    },

    handleError : function(component, event){
        component.set("v.spinnerOn", false);
    },

    handleSuccess : function(component, event) {  
        if ( component.get("v.yesCounter")>=2 || !component.get('v.saveAndCallStage') ){
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Success",
                "message": "Saved",
                "type": "success"
            });
            toastEvent.fire(); 
        }             
        component.set("v.spinnerOn", false);
        
        if ( !component.get('v.saveAndCallStage') || component.get("v.yesCounter")<2 )
            return;        

        var cmpEvent = $A.get("e.c:fastTrackStageDoneEvent");
        var nextIndex = component.get("v.myStageIndex"); 
        var eventSourceId = event.getSource().getLocalId();
        nextIndex += eventSourceId === "btnBack" ? -1 : 1;
        cmpEvent.setParams({
            "opportunityId": component.get("v.proposalCreationTO.opportunityId"),
            "nextIndex": nextIndex
        });
        cmpEvent.fire();
    },

    handleSubmit : function(component, event){
        let yesCounter=0;
        Object.keys( event.getParam("fields")).forEach(function(item){            
            if(  (  item=='CH_Challenges__c' || 
                    item=='A_Authority__c' ||
                    item=='M_Money__c' ||
                    item=='P_Priority__c' )
                  && event.getParam("fields")[item]=='Yes')
                yesCounter++;
            }
        );
        component.set("v.yesCounter", yesCounter);
        if ( yesCounter<2 && component.get('v.saveAndCallStage')  ){
             
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Invalid data",
                "message": "You need to achieve two Yes attributes for Champ",
                "type": "error"
            });
            toastEvent.fire(); 
            component.set("v.spinnerOn", false);
            return;
        }
        component.set("v.spinnerOn", true);
    },

    doInit : function(component, event, helper){  
        component.set("v.qualificationStepPercent", 10); 
        component.set("v.activeSectionStepControl","qualification");
        component.set("v.activeSection","qualification");
    },

    handleSectionToggle: function (cmp, event) {
    
        cmp.set("v.activeSectionStepControl", event.getParam("openSections")[
          event.getParam("openSections").length-1
          ]
        );
        
      }
})