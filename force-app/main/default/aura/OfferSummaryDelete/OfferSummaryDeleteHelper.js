({
    deleteRecord : function(component, event, helper) {
        var action = component.get("c.deleteRecord");
        action.setParams({ recordId : component.get("v.objId") });
 
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var responseResult = response.getReturnValue();
                if (responseResult.status == "SUCCESS") {
                    helper.showToast('Success', responseResult.message, 'success');
                    component.find('overlayLib').notifyClose();
                    helper.fireRefreshMainFastTrackCmpEvent();
                } else {
                    helper.showToast('Something went wrong', responseResult.message, 'error');
                }
            }
            else {
                helper.showToast('Something went wrong',  response.getError()[0].message, 'error');
            }
        });
        $A.enqueueAction(action);                 
    },
    showToast : function(title, message, type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type": type
        });
        toastEvent.fire();
    },
    fireRefreshMainFastTrackCmpEvent : function(component, helper) {
        var cmpEvt = $A.get("e.c:RefreshMainFastTrackCmpEvent");
		cmpEvt.fire();
    }
})