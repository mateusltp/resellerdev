@isTest
public class ResellerCreateOpportunityGOTest {
    
    @testSetup static void setup(){
        User thisUser = [SELECT Id FROM User WHERE Id = : UserInfo.getUserId()];
        System.runAs(thisUser){
            Account account = new Account();  
            account.Name = 'Test Account';
            account.Legal_Document_Type__c = 'CNPJ';
            account.Id_Company__c = '56947401000155';
            account.Razao_Social__c = 'TESTE';
            account.Website = 'teste.com';
            account.Industry = 'Airlines';             
            account.BillingCountry = 'Brazil';
            account.Type = 'Prospect';
            account.Industry = 'Government or Public Management';
            account.Indirect_Channel_Pricebook__c = 'Standard';
            account.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Reseller').getRecordTypeId();
            insert account;
            
            account.IsPartner = true;
            update account;
            
            Contact contact = new Contact();
            contact.FirstName = 'testeMateus';
            contact.LastName = 'testeNovo';
            contact.AccountId = account.Id;
            contact.Email = 'testeMateus@bemarius.example';
            contact.Role__c = 'FINANCE';
            insert contact;
            
            
            Account_Request__c request = new Account_Request__c();     
            request.Name = 'testCreateOpp';
            request.Unique_Identifier_Type__c = 'CNPJ';
            request.Unique_Identifier__c = '56947401000155';
            request.Website__c = 'testerequeste.com';
            request.Billing_Country__c = 'Brazil';
            request.AccountId__c = account.Id;
            request.Partner_Model__c = 'Subsidy';
            request.ContactId__c = contact.Id;
            request.Use_My_Own_Data__c = true;
            insert request;
            
           
            List<Profile> profiles = [Select Id From Profile Where Name='Reseller Community'];
            
            UserRole role = new UserRole();
            role.OpportunityAccessForAccountOwner = 'Edit';
            role.PortalType = 'Partner';
            role.PortalAccountId = account.Id;
            insert role;

            Pricebook2 pb = new Pricebook2();
            pb.Name = 'Reseller 5k';
            pb.Business_Model__c = 'Subsidy 5k';
            pb.IsActive = true;
            insert pb;
        }
    }
    
    @isTest
    public static void createOpportunityWithPricebookTest() {
        Test.startTest();
        Account_Request__c request = [SELECT Id,Website__c,Engine_Status__c FROM Account_Request__c WHERE Name = 'testCreateOpp' limit 1];
        request.Engine_Status__c = 'GO';
        request.Website__c = 'www.testeNovo.com';
        update request;
        Test.stopTest();
    }
    
    @isTest
    public static void setJointEffortTest() {
        Account account = new Account();  
        account.RecordTypeId = '01241000000xyotAAA';
        account.Name = 'teste account7';
        account.Legal_Document_Type__c = 'CNPJ';
        account.Id_Company__c = '56947401000117';
        account.Razao_Social__c = 'TESTE222';
        account.Website = 'teste222.com';        
        account.BillingCountry = 'Brazil';
        account.Industry = 'Government or Public Management';
        account.ABM_Prospect__c = true;
        account.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Reseller').getRecordTypeId();
        insert account;
        
        Contact contact = new Contact();
        contact.FirstName = 'teste contact7';
        contact.LastName = 'testeNovo';
        contact.Email = 'testecontact@bemarius.example';
        contact.Role__c = 'FINANCE';
        insert contact;
        
        Account_Request__c request7 = new Account_Request__c();  
        request7.Name = 'teste7';
        request7.Unique_Identifier_Type__c = 'CNPJ';
        request7.Unique_Identifier__c = '56947421000106';
        request7.Website__c = 'testerequeste7.com';
        request7.Billing_Country__c = 'Brazil';
        request7.AccountId__c = account.Id;
        request7.ContactId__c = contact.Id;
        request7.Engine_Status__c = 'Analyze';
        request7.Use_My_Own_Data__c = true;
        insert request7;

        Test.startTest();
        ResellerEngineGoNoGO resellerEngine = new ResellerEngineGoNoGO(new Set<Id> {request7.Id});
        resellerEngine.runSearchAccounts(true);
        resellerEngine.noGO();
        
        ResellerCreateOpportunityGO createOpportunityGO = new ResellerCreateOpportunityGO();
        //createOpportunityGO.getContact(request7);
        createOpportunityGO.getDirectExecutive(account.Id);
        
        request7.Engine_Status__c = 'GO';
        request7.Website__c = 'www.testeNovo2.com';
        request7.Partner_Model__c = 'Subsidy';
        update request7;
        Test.stopTest(); 
        
       System.assertEquals(request7.AccountId__r.OwnerId, request7.OpportunityId__r.OwnerId);

    }
    
}