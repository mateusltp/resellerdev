/**
 * Created by bruno on 14/01/2022.
 */

@IsTest
private class AccountOpportunitiesDataProviderTest
{
    @TestSetup
    static void createData(){

        Account lAcc = PartnerDataFactory.newAccount();
        lAcc.Name = 'Parent Acct';
        Database.insert( lAcc );



    }

    @IsTest
    static void SDG_Test_negative(){

        List<Account> acctLst = [SELECT Id, Types_of_ownership__c,
                Partner_Level__c, ShippingState,
                ShippingCountry, ShippingCountryCode
        FROM Account LIMIT 1];

        String testRecordId = acctLst.get(0).id;

        // instantiate class
        AccountOpportunitiesDataProvider aodDataProvider = new AccountOpportunitiesDataProvider();
        sortablegrid.SDG coreSDG = aodDataProvider.LoadSDG('', testRecordId);

        // create the user request
        sortablegrid.SDGRequest request = new sortablegrid.SDGRequest();
        request.ParentRecordID = testRecordId;
        request.PageID = 1;
        request.PageSize = 10;
        request.FieldSetName=null;
        request.RelationshipName=null;
        request.SDGTag='Apex:AccountOpportunitiesDataProvider';



        sortablegrid.SDGResult result = aodDataProvider.getData(coreSDG, request);
        system.assertEquals(0, result.data.size());
    }

    @IsTest
    static void SDG_Test_positive_with_opportunity_no_opp_member(){
                List<Account> acctLst = [SELECT Id, Types_of_ownership__c,
                Partner_Level__c, ShippingState,
                ShippingCountry, ShippingCountryCode
        FROM Account LIMIT 1];

        String testRecordId = acctLst.get(0).id;

        Opportunity lOpp = PartnerDataFactory.newOpportunity( testRecordId, 'Partner_Flow_Opportunity' );
        Database.insert( lOpp );

        Test.startTest();
        // instantiate class
        AccountOpportunitiesDataProvider aodDataProvider = new AccountOpportunitiesDataProvider();
        sortablegrid.SDG coreSDG = aodDataProvider.LoadSDG('', testRecordId);

        // create the user request
        sortablegrid.SDGRequest request = new sortablegrid.SDGRequest();
        request.ParentRecordID = testRecordId;
        request.PageID = 1;
        request.PageSize = 10;
        request.FieldSetName=null;
        request.RelationshipName=null;
        request.SDGTag='Apex:AccountOpportunitiesDataProvider';


        // get data
        sortablegrid.SDGResult result = aodDataProvider.getData(coreSDG, request);
        Test.stopTest();
        system.assertEquals(1, result.data.size());
    }

    @IsTest
    static void SDG_Test_positive_with_opportunity_and_same_opp_member() {
        List<Account> acctLst = [SELECT Id, Types_of_ownership__c,
                Partner_Level__c, ShippingState,
                ShippingCountry, ShippingCountryCode
        FROM Account LIMIT 1];

        String testRecordId = acctLst.get(0).id;

        Opportunity lOpp = PartnerDataFactory.newOpportunity( testRecordId, 'Partner_Flow_Opportunity' );
        Database.insert( lOpp );

        Account_Opportunity_Relationship__c lAcctOppRel = PartnerDataFactory.newAcctOppRel(testRecordId, lOpp.Id);
        Database.insert(lAcctOppRel);

        Product_Item__c lProd = PartnerDataFactory.newProductFlow();
        Database.insert(lProd);

        Commercial_Condition__c lCommercialCond = PartnerDataFactory.newCommercialCondition();
        lCommercialCond.RecordTypeId = Schema.SObjectType.Commercial_Condition__c.getRecordTypeInfosByDeveloperName().get('CAP').getRecordTypeId();
        Database.insert(lCommercialCond);

        Product_Assignment__c lProductAssignment = PartnerDataFactory.newProductAssignment(lProd.id, lCommercialCond.id);
        Database.insert(lProductAssignment);

        Product_Opportunity_Assignment__c lProductOppAssignment = PartnerDataFactory.newProductOppAssignment(lAcctOppRel.id, lProductAssignment.id);
        Database.insert(lProductOppAssignment);

        Test.startTest();
        // instantiate class
        AccountOpportunitiesDataProvider aodDataProvider = new AccountOpportunitiesDataProvider();
        sortablegrid.SDG coreSDG = aodDataProvider.LoadSDG('', testRecordId);

        // create the user request
        sortablegrid.SDGRequest request = new sortablegrid.SDGRequest();
        request.ParentRecordID = testRecordId;
        request.PageID = 1;
        request.PageSize = 10;
        request.FieldSetName=null;
        request.RelationshipName=null;
        request.SDGTag='Apex:AccountOpportunitiesDataProvider';


        // get data
        sortablegrid.SDGResult result = aodDataProvider.getData(coreSDG, request);
        Test.stopTest();
        system.assertEquals(1, result.data.size());
    }

    @IsTest
    static void SDG_Test_positive_with_opportunity_and_diff_opp_member() {
        List<Account> acctLst = [SELECT Id, Types_of_ownership__c,
                Partner_Level__c, ShippingState,
                ShippingCountry, ShippingCountryCode
        FROM Account LIMIT 1];

        String testRecordId = acctLst.get(0).id;

        Account lAcc = PartnerDataFactory.newAccount();
        lAcc.Name = 'Parent Acct2';
        lAcc.Id_Company__c = '01.000.408/0001-80';
        lAcc.BillingStreet = 'Rua João Gouveia';
        lAcc.ShippingStreet = 'Rua João Gouveia';
        Database.insert( lAcc );

        Opportunity relatedOpp = PartnerDataFactory.newOpportunity( testRecordId, 'Partner_Flow_Opportunity' );
        Database.insert( relatedOpp );

        Opportunity notRelatedOpp = PartnerDataFactory.newOpportunity( lAcc.Id, 'Partner_Flow_Opportunity' );
        Database.insert( notRelatedOpp );

        Account_Opportunity_Relationship__c lAcctOppRel = PartnerDataFactory.newAcctOppRel(testRecordId, notRelatedOpp.Id);
        Database.insert(lAcctOppRel);

        Product_Item__c lProd = PartnerDataFactory.newProductFlow();
        Database.insert(lProd);

        Commercial_Condition__c lCommercialCond = PartnerDataFactory.newCommercialCondition();
        lCommercialCond.RecordTypeId = Schema.SObjectType.Commercial_Condition__c.getRecordTypeInfosByDeveloperName().get('CAP').getRecordTypeId();
        Database.insert(lCommercialCond);

        Product_Assignment__c lProductAssignment = PartnerDataFactory.newProductAssignment(lProd.id, lCommercialCond.id);
        Database.insert(lProductAssignment);

        Product_Opportunity_Assignment__c lProductOppAssignment = PartnerDataFactory.newProductOppAssignment(lAcctOppRel.id, lProductAssignment.id);
        Database.insert(lProductOppAssignment);

        Test.startTest();
        // instantiate class
        AccountOpportunitiesDataProvider aodDataProvider = new AccountOpportunitiesDataProvider();
        sortablegrid.SDG coreSDG = aodDataProvider.LoadSDG('', testRecordId);

        // create the user request
        sortablegrid.SDGRequest request = new sortablegrid.SDGRequest();
        request.ParentRecordID = testRecordId;
        request.PageID = 1;
        request.PageSize = 10;
        request.FieldSetName=null;
        request.RelationshipName=null;
        request.SDGTag='Apex:AccountOpportunitiesDataProvider';


        // get data
        sortablegrid.SDGResult result = aodDataProvider.getData(coreSDG, request);
        Test.stopTest();
        system.assertEquals(2, result.data.size());
    }

    @IsTest
    static void SDG_Test_positive_with_opp_member_no_opportunity() {
        List<Account> acctLst = [SELECT Id, Types_of_ownership__c,
                Partner_Level__c, ShippingState,
                ShippingCountry, ShippingCountryCode
        FROM Account LIMIT 1];

        String testRecordId = acctLst.get(0).id;

        Account lAcc = PartnerDataFactory.newAccount();
        lAcc.Name = 'Parent Acct2';
        lAcc.Id_Company__c = '01.000.408/0001-80';
        lAcc.BillingStreet = 'Rua João Gouveia';
        lAcc.ShippingStreet = 'Rua João Gouveia';
        Database.insert( lAcc );

        Opportunity notRelatedOpp = PartnerDataFactory.newOpportunity( lAcc.Id, 'Partner_Flow_Opportunity' );
        Database.insert( notRelatedOpp );

        Account_Opportunity_Relationship__c lAcctOppRel = PartnerDataFactory.newAcctOppRel(testRecordId, notRelatedOpp.Id);
        Database.insert(lAcctOppRel);

        Product_Item__c lProd = PartnerDataFactory.newProductFlow();
        Database.insert(lProd);

        Commercial_Condition__c lCommercialCond = PartnerDataFactory.newCommercialCondition();
        lCommercialCond.RecordTypeId = Schema.SObjectType.Commercial_Condition__c.getRecordTypeInfosByDeveloperName().get('CAP').getRecordTypeId();
        Database.insert(lCommercialCond);

        Product_Assignment__c lProductAssignment = PartnerDataFactory.newProductAssignment(lProd.id, lCommercialCond.id);
        Database.insert(lProductAssignment);

        Product_Opportunity_Assignment__c lProductOppAssignment = PartnerDataFactory.newProductOppAssignment(lAcctOppRel.id, lProductAssignment.id);
        Database.insert(lProductOppAssignment);

        Test.startTest();
        // instantiate class
        AccountOpportunitiesDataProvider aodDataProvider = new AccountOpportunitiesDataProvider();
        sortablegrid.SDG coreSDG = aodDataProvider.LoadSDG('', testRecordId);

        // create the user request
        sortablegrid.SDGRequest request = new sortablegrid.SDGRequest();
        request.ParentRecordID = testRecordId;
        request.PageID = 1;
        request.PageSize = 10;
        request.FieldSetName=null;
        request.RelationshipName=null;
        request.SDGTag='Apex:AccountOpportunitiesDataProvider';


        // get data
        sortablegrid.SDGResult result = aodDataProvider.getData(coreSDG, request);
        Test.stopTest();
        system.assertEquals(1, result.data.size());
    }

    @IsTest
    static void isUserSelectableTest(){

        Boolean isUserSelectable = true;
        System.assertEquals(isUserSelectable, CurrentValidProductsDataProvider.isUserSelectable());
    }

    @IsTest
    static void createLog_Test() {
        Test.startTest();
        new AccountOpportunitiesDataProvider().createLog('test message', 'test stack trace', 'testmethod', null, null);
        Test.stopTest();
        List<DebugLog__c> logs = [SELECT Id FROM DebugLog__c];
        System.assertEquals(1, logs.size());
    }
}