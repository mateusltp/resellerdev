/**
 * @File Name          : OpportunityTriggerValOppRelHelperTest.cls
 * @Description        : 
 * @Author             : GEPI@GFT.com
 * @Group              : 
 * @Last Modified By   : GEPI@GFT.com
 * @Last Modified On   : 08-05-2020
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    17/04/2020   MLNC@GFT.com     Initial Version
**/
@Istest
public class LeadTriggerAllocationHelperTest {

    @TestSetup
    static void Setup(){   
        Id recordTypeIdAccount =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gym_Partner').getRecordTypeId();
        Id rtIdOwnerLead = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Partner_Owner_Referral_Lead').getRecordTypeId();
        String query = Label.Allocation_User_Role;
     	Id profile = [SELECT Id FROM Profile WHERE Name = 'Gyms'].Id;
        List<User> userlist = new List<User>();
        User us1 = new User(
            ProfileId = profile,
            LastName = 'last', Email = 'elizabeth.tuzzolo@gmail.com',Username = 'elizabeth.tuzzolo@gympass.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',Title = 'title', Alias = 'alias',TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US');
        userlist.add(us1);
		User us2 = new User(
            ProfileId = profile,
            LastName = 'McKee', Email = 'Alex.tuzzolo@gmail.com',Username = 'alex.mckee@gympass.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',Title = 'title', Alias = 'alias',TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US' );
        userlist.add(us2);
        insert userlist;
        
        /*ACCOUNT */
        Account acc = new Account(Name='AccTest', Website='www.test.com',  RecordTypeId = recordTypeIdAccount, 
            ShippingCountry = 'Brazil', ShippingState = 'São Paulo',
            ShippingStreet = 'Bermudas', ShippingCity = 'Sorocaba',                       
            Types_of_ownership__c = Label.franchisePicklist,
            Subscription_Type__c = 'Value per class',
            Subscription_Period__c = 'Monthy value',
            Subscription_Type_Estimated_Price__c    = 100,
            Has_market_cannibalization__c = 'No',
            Exclusivity__c = 'Yes',
            Exclusivity_End_Date__c = Date.today().addYears(1),
            Exclusivity_Partnership__c = 'Full Exclusivity',
            Exclusivity_Restrictions__c= 'No', 
            Gym_Email__c = 'gymemail@apex.com',
            Phone = '3222123123' ); 
        INSERT acc; 
        
        /*SCORE*/
        Score_Data__c standard = new Score_Data__c();
        standard.Country_Score_Data__c = 'Standard';
        standard.State_Score_Data__c = 'Standard';
        standard.City_Score_Data__c = 'Standard';
        standard.Number_Of_Active_Users__c = 0;
        standard.Number_Of_Sign_Ups__c = 0;
        INSERT standard;
        System.Debug('Score Data ----- >  Standard ------------  >>> ' + standard);

        Score_Data__c sdt = new Score_Data__c();
            sdt.Country_Score_Data__c = 'Brazil';
            sdt.State_Score_Data__c = 'São Paulo';
            sdt.City_Score_Data__c = 'Sorocaba';
            sdt.Number_Of_Active_Users__c = 0;
            sdt.Number_Of_Sign_Ups__c = 0;
        INSERT sdt;
        System.Debug('Score Data ----- >  Sorocaba ------------  >>> ' + sdt);

        Score_Data__c sdtUp = new Score_Data__c();
            sdtUp.Country_Score_Data__c = 'Brazil';
            sdtUp.State_Score_Data__c = 'São Paulo';
            sdtUp.City_Score_Data__c = 'São Paulo';
            sdtUp.Number_Of_Active_Users__c = 29;
            sdtUp.Number_Of_Sign_Ups__c = 3001;
        INSERT sdtUp;
        System.Debug('Score Data ----- >  São Paulo ------------  >>> ' + sdtUp);

        Score_Data__c sdtUpAct = new Score_Data__c();
            sdtUpAct.Country_Score_Data__c = 'Brazil';
            sdtUpAct.State_Score_Data__c = 'São Paulo';
            sdtUpAct.City_Score_Data__c = 'Campinas';
            sdtUpAct.Number_Of_Active_Users__c = 1001;
            sdtUpAct.Number_Of_Sign_Ups__c = 3001;
        INSERT sdtUpAct;
        System.Debug('Score Data ----- >  Campinas ------------  >>> ' + sdtUpAct);

        Score_Data__c sdtARGSignUp = new Score_Data__c();
            sdtARGSignUp.Country_Score_Data__c = 'Argentina';
            sdtARGSignUp.State_Score_Data__c = 'Buenos Aires';
            sdtARGSignUp.City_Score_Data__c = 'Almirante Brown';
            sdtARGSignUp.Number_Of_Active_Users__c = 49;
            sdtARGSignUp.Number_Of_Sign_Ups__c = 51;
        INSERT sdtARGSignUp ;
        System.Debug('Score Data ----- >  Argentina - Almirante Brown  ------------  >>> ' + sdtARGSignUp );

        Score_Data__c sdtARGActive = new Score_Data__c();
            sdtARGActive.Country_Score_Data__c = 'Argentina';
            sdtARGActive.State_Score_Data__c = 'Buenos Aires';
            sdtARGActive.City_Score_Data__c = 'Arrecifes';
            sdtARGActive.Number_Of_Active_Users__c = 51;
            sdtARGActive.Number_Of_Sign_Ups__c = 51;
        INSERT sdtARGActive ;
        System.Debug('Score Data ----- >  Argentina - Arrecifes ------------  >>> ' + sdtARGActive );

        Score_Data__c sdtARGAAzul = new Score_Data__c();
            sdtARGAAzul.Country_Score_Data__c = 'Argentina';
            sdtARGAAzul.State_Score_Data__c = 'Buenos Aires';
            sdtARGAAzul.City_Score_Data__c = 'Azul';
            sdtARGAAzul.Number_Of_Active_Users__c = 101;
            sdtARGAAzul.Number_Of_Sign_Ups__c = 101;
        INSERT sdtARGAAzul ;
        System.Debug('Score Data ----- >  Argentina - Azul ------------  >>> ' + sdtARGAAzul );
        
        /*LEAD */
        List<Lead> leadlst = new List<Lead>();
        Lead ld = new Lead(
            LastName='LeadTest', RecordTypeId = rtIdOwnerLead, 
            Country = 'Brazil', CountryCode = 'BR', GP_Referral_Name__c = 'DonoTest', State = 'São Paulo', StateCode = 'SP',
            Street = 'test Lead Base', City = 'Tocantins',                       
            Phone = '(011)2345-2345',
            Email = 'testlead@abdc.com',
            Company = 'Company Test',
            Status = 'em aberto',
            PostalCode = '18071901',
            Type_of_Allocation__c = 'Automatic'); 
        leadlst.add(ld);
        Lead ld2 = new Lead(
            LastName='Mobi', RecordTypeId = rtIdOwnerLead, 
            Country = 'Brazil', CountryCode = 'BR', GP_Referral_Name__c = 'DonoTest', State = 'São Paulo', StateCode = 'SP',
            Street = 'Bermudas BRA Base', City = 'Tocantins',                       
            Phone = '(011)2345-2566',
            Email = 'leasd@abc.com',
            Company = 'camp 2',
            PostalCode = '18071923',
            Type_of_Allocation__c = 'Automatic'); 
        leadlst.add(ld2);        
        INSERT leadlst;
        

        
        
        List<Allocation_Base__c> allocationbasemdl = new List<Allocation_Base__c>();
        Allocation_Base__c cs1 = new Allocation_Base__c(
            Name='US001',FirstName__c='Elizabeth',LastName__c='Tuzzolo', Id_User__c = us1.Id,
            CountryCode__c = 'US', StateCode__c = 'TX', Status__c = true ); 
        allocationbasemdl.add(cs1);
      
       Allocation_Base__c cus2 = new Allocation_Base__c(
            Name='US002',FirstName__c='Elizabeth',LastName__c='Tuzzolo', Id_User__c = us1.id ,
            CountryCode__c = 'US', StateCode__c = 'MN', Status__c = true ); 
        allocationbasemdl.add(cus2);
        
       Allocation_Base__c usCA = new Allocation_Base__c(
            Name='US0CA',FirstName__c='Elizabeth',LastName__c='Tuzzolo', Id_User__c = us1.id ,
            CountryCode__c = 'US', StateCode__c = 'CA', Status__c = true, City__c = 'San Francisco' ); 
        allocationbasemdl.add(usCA);
        
        Allocation_Base__c cs2 = new Allocation_Base__c(
            Name='BR001',FirstName__c='Alex',LastName__c='McKee',  Id_User__c = us2.id  ,
            CountryCode__c = 'BR', StateCode__c = 'SC', Status__c = true );           
        allocationbasemdl.add(cs2);
        
        Allocation_Base__c SP = new Allocation_Base__c(
            Name='BRASP',FirstName__c='Elizabeth',LastName__c='Tuzzolo', Id_User__c = us1.id ,
            CountryCode__c = 'BR', StateCode__c = 'SP', City__c = 'Tocantins',Status__c = true ); 
        allocationbasemdl.add(SP);
        
        Allocation_Base__c csBR1 = new Allocation_Base__c(
            Name='BR002',FirstName__c='Alex',LastName__c='McKee', Id_User__c = us2.id ,
            CountryCode__c = 'BR', StateCode__c = 'AM', Status__c = true );           
        allocationbasemdl.add(csBR1);
        
        Allocation_Base__c csBR2 = new Allocation_Base__c(
            Name='BR003',FirstName__c='Alex',LastName__c='McKee', Id_User__c = us2.id ,
            CountryCode__c = 'BR', StateCode__c = 'MG', Status__c = true );           
        allocationbasemdl.add(csBR2);
       
        Allocation_Base__c cs3 = new Allocation_Base__c(
            Name='AR001',FirstName__c='Elizabeth',LastName__c='Tuzzolo', Id_User__c = us1.id ,
            CountryCode__c = 'AR', StateCode__c = 'BA', Status__c = true,City__c='Ciudad Autónoma de Buenos Aires' ); 
        allocationbasemdl.add(cs3);
        
        Allocation_Base__c cs4 = new Allocation_Base__c(
            Name='MX001',FirstName__c='Alex',LastName__c='McKee', Id_User__c = us2.id ,
            CountryCode__c = 'MX', StateCode__c = 'ME', Status__c = true ); 
        allocationbasemdl.add(cs4);
        
        Allocation_Base__c cMX4 = new Allocation_Base__c(
            Name='MX002',FirstName__c='Alex',LastName__c='McKee', Id_User__c = us2.id ,
            CountryCode__c = 'MX', StateCode__c = 'TB', Status__c = true ); 
        allocationbasemdl.add(cMX4);
        
        Allocation_Base__c cs5 = new Allocation_Base__c(
            Name='CL001',FirstName__c='Alex',LastName__c='McKee',Id_User__c = us2.id , 
            CountryCode__c = 'CL', Status__c = true ); 
        allocationbasemdl.add(cs5);
        
        INSERT allocationbasemdl;
    }
    
    @isTest 
    static void valiteBRA() {
       List<Lead> leadcountry = new List<Lead>();
       Id rtIdOwnerLead = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Partner_Owner_Referral_Lead').getRecordTypeId(); 
       Id userld = [SELECT Id, Email FROM User where LastName = 'last'].Id; 
       
        Test.startTest();         
        	Lead RT1BRA = new Lead(
            LastName='LeadTest03',GP_Referral_Name__c = 'DonoTest', RecordTypeId = rtIdOwnerLead, Country = 'Brazil', CountryCode = 'BR', State = 'Santa Catarina', StateCode = 'SC', 
            Street = 'Nobel 23345 REV', City = 'Tocantins', Phone = '(011)2345-2345', Company = 'Company TEST BRAZIL',Type_of_Allocation__c = 'Automatic' );        
        	leadcountry.add(RT1BRA);
        	
        	Lead RT2BRA = new Lead(
            LastName='LeadTest03',GP_Referral_Name__c = 'DonoTest', RecordTypeId = rtIdOwnerLead, Country = 'Brazil', CountryCode = 'BR', State = 'São Paulo',  StateCode = 'SP',
            Street = 'Arfed 23345', City = 'Tocantins', Phone = '(011)2345-2345', Company = 'Company BRAS 01',Type_of_Allocation__c = 'Automatic' );        
        	leadcountry.add(RT2BRA);        
            INSERT RT2BRA;
        
            RT2BRA.Country = 'United States';
            RT2BRA.CountryCode = 'US';
            RT2BRA.StateCode ='MN';
            RT2BRA.State = 'Minnesota'; 
        	update RT2BRA;
        
        	
       		Id idLead = [SELECT id, OwnerId FROM Lead WHERE Id =: RT2BRA.Id].OwnerId;
            system.assertequals(idlead , userld);
        
	   Test.stopTest(); 
    }
    
    @isTest 
    static void valiteBRA2() {
       List<Allocation_Base__c> baseLst = new List<Allocation_Base__c> ();
       List<Lead> leadcountry = new List<Lead>();
       Id rtIdOwnerLead = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Partner_Owner_Referral_Lead').getRecordTypeId(); 
       
        Test.startTest();         
        	Lead RTDEBRA = new Lead(
            LastName='LeadTest0324BRA ',GP_Referral_Name__c = 'DonoTest', RecordTypeId = rtIdOwnerLead, Country = 'Brazil', CountryCode='BR', State = 'Minas Gerais', StateCode = 'MG', MobilePhone = '(011)92345-2345',
            Street = 'Minas Brasedero 23', City = 'br', Company = 'Company Test BRAZIL MINAS',Type_of_Allocation__c = 'Automatic', Phone = '(011)2345-2345', Email = 'testlead@abdc.com' );        
        	leadcountry.add(RTDEBRA);
        	
        	Lead RTDEFBRA = new Lead(
            LastName='BRA MINAS Test',GP_Referral_Name__c = 'DonoTest', RecordTypeId = rtIdOwnerLead, Country = 'Brazil',CountryCode='BR', State = 'Minas Gerais', StateCode = 'MG', MobilePhone = '(011)92345-2345',
            Street = 'Barracas 23', City = 'br', Company = 'Company BRA MIN',Type_of_Allocation__c = 'Automatic', Phone = '(011)2345-2345', Email = 'testlead@abdc.com' );        
        	leadcountry.add(RTDEFBRA);        
            INSERT leadcountry;
        
            RTDEFBRA.Country='Brazil';
            RTDEFBRA.CountryCode='BR';
            RTDEFBRA.State='Amazonas'; 
            RTDEFBRA.StateCode = 'AM';
            update RTDEFBRA;
	   Test.stopTest(); 
    }
    
    @isTest 
    static void validateUS() {
       List<Allocation_Base__c> baseLst = new List<Allocation_Base__c> ();
       List<Lead> leadcountry = new List<Lead>();
       Id rtIdOwnerLead = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Partner_Owner_Referral_Lead').getRecordTypeId(); 
       
        Test.startTest();         
            Lead RT1US = new Lead(
            LastName='LeadTest02', GP_Referral_Name__c = 'DonoTest', RecordTypeId = rtIdOwnerLead, Country = 'United States',CountryCode='US', State = 'Texas', StateCode = 'TX',
            Street = 'US Bermudas 23344', City = 'terre', Company = 'Company Test USA ', Type_of_Allocation__c = 'Automatic', Phone = '(011)2345-2345', Email = 'testlead@abdc.com' );          
        	leadcountry.add(RT1US);         
            Lead RT2US = new Lead(
            LastName='LeadTest0223',GP_Referral_Name__c = 'DonoTest', RecordTypeId = rtIdOwnerLead, Country = 'United States', CountryCode='US', State = 'California', StateCode = 'CA',
            Street = 'San Fran Bermudas 23', City = 'San Francisco', Company = 'Company US', Type_of_Allocation__c = 'Automatic', Phone = '(011)2345-2345', Email = 'testlead@abdc.com' );          
        	leadcountry.add(RT2US);         
        	INSERT leadcountry;        
            RT2US.Country='Mexico';
            RT2US.CountryCode='MX';
            RT2US.State='Estado de México'; 
            RT2US.StateCode = 'ME';
            update RT2US;
	   Test.stopTest(); 
    }
    
    @isTest 
    static void validateAR() {
       List<Allocation_Base__c> baseLst = new List<Allocation_Base__c> ();
       List<Lead> leadcountry = new List<Lead>();
       Id rtIdOwnerLead = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Partner_Owner_Referral_Lead').getRecordTypeId(); 

        Test.startTest();   
             
			Lead RT1AR = new Lead(
            LastName='LeadTest04',GP_Referral_Name__c = 'DonoTest', RecordTypeId = rtIdOwnerLead, Country = 'Argentina', CountryCode='AR', State = 'Buenos Aires', StateCode = 'BA',
            Street = 'Buenos Aires argentina de 23', City = 'Ciudad Autónoma de Buenos Aires', Company = 'Company ARG ', Type_of_Allocation__c = 'Automatic',Phone = '(011)2345-2345', Email = 'testlead@abdc.com' ); 
            leadcountry.add(RT1AR);          
            Lead RT2AR = new Lead(
            LastName='LeadTest0222',GP_Referral_Name__c = 'DonoTest', RecordTypeId = rtIdOwnerLead, Country = 'Argentina', CountryCode='AR', State = 'Buenos Aires', StateCode ='BA',
            Street = 'argentina de 23', City = 'Men', Company = 'Company Test ARGENTINA', Type_of_Allocation__c = 'Automatic',Phone = '(011)2345-2345', Email = 'testlead@abdc.com' );           
        	leadcountry.add(RT2AR);         
        	INSERT leadcountry;        
            RT2AR.Country='Brazil';
            RT2AR.CountryCode='BR';
            RT2AR.State='São Paulo'; 
            RT2AR.StateCode = 'SP';
        	RT2AR.City='Águas de São Pedro';
            update RT2AR;
	   Test.stopTest(); 
    }
    
    @isTest 
    static void validateCH() {
       List<Allocation_Base__c> baseLst = new List<Allocation_Base__c> ();
       List<Lead> leadcountry = new List<Lead>();
       Id rtIdOwnerLead = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Partner_Owner_Referral_Lead').getRecordTypeId();
     
        Test.startTest();         
			Lead RT1CH = new Lead(
            LastName='LeadTest05',GP_Referral_Name__c = 'DonoTest', RecordTypeId = rtIdOwnerLead, Country = 'Chile',CountryCode='CL', 
            Street = 'chile 23', City = 'ch', Company = 'Company CHILE' , Type_of_Allocation__c = 'Automatic',Phone = '(011)2345-2345', Email = 'testlead@abdc.com' ); 
            leadcountry.add(RT1CH);  
            Lead RT2CH = new Lead(
            LastName='LeadTest0222e',GP_Referral_Name__c = 'DonoTest', RecordTypeId = rtIdOwnerLead, Country = 'Chile',  CountryCode='CL',
            Street = 'chile 235434', City = 'ch', Company = 'Company Test CHI' , Type_of_Allocation__c = 'Automatic',Phone = '(011)2345-2345', Email = 'testlead@abdc.com' );          
        	leadcountry.add(RT2CH);         
        	INSERT leadcountry;        
            RT2CH.Country='United States';
            RT2CH.CountryCode='US'; 
            RT2CH.State='North Dakota'; 
            RT2CH.StateCode = 'ND';          
            update RT2CH;
	   Test.stopTest(); 
    }
    
    @isTest 
    static void validateMX() {
       List<Allocation_Base__c> baseLst = new List<Allocation_Base__c> ();
       List<Lead> leadcountry = new List<Lead>();
       Id rtIdOwnerLead = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Partner_Owner_Referral_Lead').getRecordTypeId();
   
        Test.startTest();         
        
            Lead RT2MX = new Lead(
            LastName='LeadTest022',GP_Referral_Name__c = 'DonoTest', RecordTypeId = rtIdOwnerLead, Country = 'Mexico',CountryCode='MX',State = 'Baja California', StateCode = 'BC',
            Street = 'MEX VER2 Bermudas 2323', City = 'Tijuana', Company = 'Company MEX', Type_of_Allocation__c = 'Automatic',Phone = '(011)2345-2345', Email = 'testlead@abdc.com' );          
        	leadcountry.add(RT2MX);         
        	INSERT leadcountry;
        
            RT2MX.Country='United States';
            RT2MX.CountryCode = 'US'; 
            RT2MX.State='Virginia';
            RT2MX.StateCode = 'VA';
            update RT2MX;
	   Test.stopTest(); 
    }


}