/**
 * @File Name          : OpportunityTriggerAccOppRelHelper.cls
 * @Description        : 
 * @Author             : GEPI@GFT.com
 * @Group              : 
 * @Last Modified By   : Mateus Augusto - GFT (moes@gft.com)
 * @Last Modified On   : 04-27-2022
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    15/04/2020   GEPI@GFT.com     Initial Version
**/
public class OpportunityTriggerAccOppRelHelper {
    
    Id recordTypeIdSmall = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Gyms_Small_Medium_Partner').getRecordTypeId();
    Id recordTypeIdWishList = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Gyms_Wishlist_Partner').getRecordTypeId();
    Id recordTypeIdClientNewBusiness = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
    Id recordTypeIdClientResellerNewBusiness = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Indirect_Channel_New_Business').getRecordTypeId();
    Id recordTypeIdAorClientSales = Schema.SObjectType.Account_Opportunity_Relationship__c.getRecordTypeInfosByDeveloperName().get('Client_Sales').getRecordTypeId();
    
    public void deleteOppRelatedToAccounts(){
        List<Account_Opportunity_Relationship__c> aorLst = new List<Account_Opportunity_Relationship__c>();
        Set<Id> oppIdSet = new Set<Id>();
        List<Opportunity> lstOld = trigger.old;
        for(Opportunity opp : lstOld){  
            if(opp.RecordTypeId == recordTypeIdSmall || opp.RecordTypeId == recordTypeIdWishList){         
                oppIdSet.add(opp.Id);
            }
        }
        aorLst = [SELECT Id FROM Account_Opportunity_Relationship__c WHERE Opportunity__c IN: oppIdSet];
        if(aorLst.size() > 0){           
            Database.delete(aorLst);
        }

    }

    public void createAccountOpportunityRelationship() {
        AccountOpportunityRepository aorRepo = new AccountOpportunityRepository();
        List<Account_Opportunity_Relationship__c> aorLst = new List<Account_Opportunity_Relationship__c>();
        List<Opportunity> newOppList = Trigger.new;

        for (Opportunity newOpp : newOppList) {
            if (newOpp.RecordTypeId == recordTypeIdClientNewBusiness ||
             (newOpp.RecordTypeId == recordTypeIdClientResellerNewBusiness && newOpp.B_M__c =='Intermediation')) {
                Account_Opportunity_Relationship__c aor = new Account_Opportunity_Relationship__c();
                aor.Opportunity__c = newOpp.Id;
                aor.Account__c = newOpp.AccountId;
                aor.RecordTypeId = recordTypeIdAorClientSales;
                aor.Main_Order__c = true;
                aorLst.add(aor);
            }          
            if(newOpp.RecordTypeId == recordTypeIdClientResellerNewBusiness && newOpp.B_M__c !='Intermediation'){
                Account_Opportunity_Relationship__c aor = new Account_Opportunity_Relationship__c();
                aor.Opportunity__c = newOpp.Id;
                aor.Account__c = newOpp.Reseller_del__c;
                aor.RecordTypeId = recordTypeIdAorClientSales;
                aor.Main_Order__c = true;
                aorLst.add(aor);
            }    
        }

        aorRepo.create(aorLst);
    }
}