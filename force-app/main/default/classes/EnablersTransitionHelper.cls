/**
 * @description       : Make the trasition of Opp enablers to account enablers
 * @author            : AlyssonMota
 * @group             : 
 * @last modified on  : 06-15-2021
 * @last modified by  : Alysson Mota
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   11-26-2020   Alysson Mota   Initial Version
**/
public without sharing class EnablersTransitionHelper {
   
    public static void passEnablersToAccountFromOpps(List<Id> oppIdList) {
        System.debug('passEnablersToAccountFromOpps');
        OpportunityRepository oppRepo = new OpportunityRepository();
        EnablersRepository enablersRepo = new EnablersRepository();
        QuoteRepository quoteRepo = new QuoteRepository();
        FormRepository formRepo = new FormRepository();

        List<Id> accountIdList = new List<Id>();
        Map<Id, Step_Towards_Success1__c> stepsToUpdate = new Map<Id, Step_Towards_Success1__c>();
        
        Map<Id, List<Step_Towards_Success1__c>> oppIdToStepList = new Map<Id, List<Step_Towards_Success1__c>>();   
        Map<Id, List<Step_Towards_Success1__c>> accIdToStepList = new Map<Id, List<Step_Towards_Success1__c>>();

        Map<Id, Opportunity> idToOppAux = oppRepo.getIdToOppByIds(oppIdList);
        Map<Id, Opportunity> idToOpp = new Map<Id, Opportunity>();
        Map<Id, Quote> oppIdToQuote = quoteRepo.getLastForOpportunitiesAsOppIdToQuoteMap(oppIdList);
        Map<Id, Form__c> oppIdToM0 = formRepo.getOppIdToM0Map(oppIdList);

        Set< String > lSetOppRT = new Set< String >{
            'Client_Sales_New_Business', 'Client_Success_Renegotiation',
            'SMB_New_Business', 'SMB_Success_Renegotiation',
            'Indirect_Channel','Client_Sales_SKU_New_Business',
            'Client_Success_SKU_Renegotiation','SMB_SKU_New_Business','SMB_Success_SKU_Renegotiation'
        };

        for (Opportunity iOpp : idToOppAux.values()) {
            System.debug('Stage: ' + iOpp.StageName); 
            if (iOpp.StageName == 'Lançado/Ganho' && 
                lSetOppRT.contains( iOpp.RecordType.DeveloperName ) ) {
                idToOpp.put(iOpp.Id, iOpp);
            }
        }

        for (Opportunity opp : idToOpp.values()) {
            accountIdList.add(opp.AccountId);
        }

        List<Step_Towards_Success1__c> oppSteps = enablersRepo.getOpportunityEnablersByOppId(new Set<Id>(oppIdList));
        List<Step_Towards_Success1__c> accSteps = enablersRepo.getAccountEnablersByAccId(new Set<Id>(accountIdList));

        for (Step_Towards_Success1__c step : oppSteps) {
            if (oppIdToStepList.containsKey(step.Related_Opportunity__c)) {
                oppIdToStepList.get(step.Related_Opportunity__c).add(step);
            } else {
                oppIdToStepList.put(step.Related_Opportunity__c, new List<Step_Towards_Success1__c>{step});
            }
        }

        for (Step_Towards_Success1__c step : accSteps) {
            if (accIdToStepList.containsKey(step.Related_Account__c)) {
                accIdToStepList.get(step.Related_Account__c).add(step);
            } else {
                accIdToStepList.put(step.Related_Account__c, new List<Step_Towards_Success1__c>{step});
            }
        }

        for (Id iOppId : idToOpp.keySet()) {
            Opportunity iOpp = idToOpp.get(iOppId);
            List<Step_Towards_Success1__c> iOppSteps = oppIdToStepList.get(iOppId);
            List<Step_Towards_Success1__c> iAccSteps = accIdToStepList.get(iOpp.AccountId);
            Quote iQuote = oppIdToQuote.get(iOppId);

            if (iOppSteps != null && iAccSteps != null) {
                Map<Integer, Step_Towards_Success1__c> oppStepNumberToStep = getMapFromSteps(iOppSteps);
                Map<Integer, Step_Towards_Success1__c> accStepNumberToStep = getMapFromSteps(iAccSteps);

                stepsToUpdate.putAll(trasitStepsFromOppToAcc(oppStepNumberToStep, accStepNumberToStep, iQuote, oppIdToM0));
            }
        }
        
        update stepsToUpdate.values();
    }

    private static Map<Integer, Step_Towards_Success1__c> getMapFromSteps(List<Step_Towards_Success1__c> steps) {
        Map<Integer, Step_Towards_Success1__c> stepNumberToStep = new Map<Integer, Step_Towards_Success1__c>();

        for (Step_Towards_Success1__c step : steps) {
            stepNumberToStep.put((Integer)step.Step_Number__c, step);
        }

        return stepNumberToStep;
    }

    private static Map<Id, Step_Towards_Success1__c> trasitStepsFromOppToAcc(Map<Integer, Step_Towards_Success1__c> oppStepNumberToStep, Map<Integer, Step_Towards_Success1__c> accStepNumberToStep, Quote iQuote, Map<Id, Form__c> oppIdToM0) {
        Map<Id, Step_Towards_Success1__c> stepsToUpdate = new Map<Id, Step_Towards_Success1__c>();
        
        for (Integer iStepNumber : oppStepNumberToStep.keySet()) {
            Step_Towards_Success1__c iOppStep = oppStepNumberToStep.get(iStepNumber);
            Step_Towards_Success1__c iAccStep = accStepNumberToStep.get(iStepNumber);
            
            if (iAccStep != null && iQuote != null) {

                if (iQuote.Opportunity.RecordType.DeveloperName == 'SMB_New_Business' || 
                    iQuote.Opportunity.RecordType.DeveloperName == 'SMB_SKU_New_Business' || 
                    iQuote.Opportunity.RecordType.DeveloperName == 'SMB_Success_Renegotiation'|| 
                    iQuote.Opportunity.RecordType.DeveloperName == 'SMB_Success_SKU_Renegotiation')  {
                    iAccStep.Achieved__c = iOppStep.Achieved__c;
                }
                
                if (iQuote.Opportunity.RecordType.DeveloperName == 'Client_Sales_New_Business' ||
                    iQuote.Opportunity.RecordType.DeveloperName == 'Client_Sales_SKU_New_Business'  )  {
                    Form__c m0 = oppIdToM0.get(iQuote.OpportunityId);
                    //System.debug(m0.SMB_CS_success_executive__c);
                    if (m0 != null && m0.SMB_CS_success_executive__c) {
                        transitForSMBCS(iAccStep, iOppStep, iStepNumber, iQuote);
                    } else {
                        transitForNonSMBCS(iAccStep, iOppStep, iStepNumber, iQuote);
                    }
                }
                
                if ((iQuote.Opportunity.RecordType.DeveloperName == 'Client_Success_Renegotiation'  ||
                iQuote.Opportunity.RecordType.DeveloperName == 'Client_Success_SKU_Renegotiation' ) && iStepNumber != 4 && iStepNumber != 8 && iStepNumber != 9 && iStepNumber != 11)  {
                    iAccStep.Achieved__c = iOppStep.Achieved__c;
                }
                
                if (iQuote.Opportunity.RecordType.DeveloperName == 'Indirect_Channel')  {
                    iAccStep.Achieved__c = iOppStep.Achieved__c;
                    
                    if (iAccStep.Achieved__c == 'Yes' && (iStepNumber == 4 || iStepNumber == 9)) {                      
                    	iAccStep.Expiration_Date__c = iQuote.Proposal_Start_Date__c.addDays(90);
                  	}
                  	
                    if (iAccStep.Achieved__c == 'Yes' && (iStepNumber == 8 || iStepNumber == 11)) {
                    	iAccStep.Expiration_Date__c = iQuote.Proposal_Start_Date__c.addDays(365);
                    }
                }
                
                stepsToUpdate.put(iAccStep.Id, iAccStep);
            }
        }

        return stepsToUpdate;
    }

    public static List<Step_Towards_Success1__c> createStepsForRenegociationOpp(Opportunity opp, Map<Id, List<Step_Towards_Success1__c>> accIdToStepsList) {
        EnablersFactory enablersFactory = new EnablersFactory();
        EnablersRepository enablersRepo = new EnablersRepository();
        
        List<Step_Towards_Success1__c> accSteps = accIdToStepsList.get(opp.AccountId);
        List<Step_Towards_Success1__c> stepsToInsert = new List<Step_Towards_Success1__c>();

        Map<Integer, Step_Towards_Success1__c> oppStepNumberToStep = null;
        Map<Integer, Step_Towards_Success1__c> accStepNumberToStep = null;

        if (accSteps != null) {
            List<Step_Towards_Success1__c> oppSteps = enablersFactory.createEnablersForOpportunityClients(opp); 
            oppStepNumberToStep = getMapFromSteps(oppSteps);
            accStepNumberToStep = getMapFromSteps(accSteps);   
        }

        if (oppStepNumberToStep != null && accStepNumberToStep != null) {
            passValuesFromAccStepsToOppSteps(oppStepNumberToStep, accStepNumberToStep);
            stepsToInsert.addAll(oppStepNumberToStep.values());
        }

        return stepsToInsert;
    }

    private static void passValuesFromAccStepsToOppSteps(Map<Integer, Step_Towards_Success1__c> oppStepNumberToStep, Map<Integer, Step_Towards_Success1__c> accStepNumberToStep) {
        for (Integer iStepNumber : accStepNumberToStep.keySet()) {
            Step_Towards_Success1__c iOppStep = oppStepNumberToStep.get(iStepNumber);
            Step_Towards_Success1__c iAccStep = accStepNumberToStep.get(iStepNumber);

            if (iOppStep != null && iAccStep != null) {
                iOppStep.Achieved__c = iAccStep.Achieved__c;
            }
        }
    }


    private static void transitForNonSMBCS(Step_Towards_Success1__c iAccStep, Step_Towards_Success1__c iOppStep, Integer iStepNumber, Quote iQuote) {

        iAccStep.Achieved__c = iOppStep.Achieved__c;
                    
        if (iAccStep.Achieved__c == 'Yes' && (iStepNumber == 4 || iStepNumber == 9)) {                      
            iAccStep.Expiration_Date__c = iQuote.Start_Date__c.addDays(90);
        }
        
        if (iAccStep.Achieved__c == 'Yes' && (iStepNumber == 8 || iStepNumber == 11)) {
            iAccStep.Expiration_Date__c = iQuote.Start_Date__c.addDays(365);
        }
    }

    private static void transitForSMBCS(Step_Towards_Success1__c iAccStep, Step_Towards_Success1__c iOppStep, Integer iStepNumber, Quote iQuote) {
        System.debug('transitForSMBCS');
        iAccStep.Expiration_Date__c = null;

        if (iStepNumber == 4 || iStepNumber == 8 || iStepNumber == 9 || iStepNumber == 11 || iStepNumber == 12)
            iAccStep.Achieved__c = 'Yes';
        else
            iAccStep.Achieved__c = iOppStep.Achieved__c;
    }
}