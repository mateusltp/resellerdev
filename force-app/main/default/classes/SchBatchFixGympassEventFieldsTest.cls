/*
* @Author: Bruno Pinho
* @Date: March/2019
* @Description: Test class for SchBatchFixGympassEventFields
*/
@isTest(SeeAllData=true)
private class SchBatchFixGympassEventFieldsTest
{    
    static void SetUp()
    {
        test.StartTest();
    }
    
    static void TearDown()
    {
        test.StopTest();
    }
    
    public static testmethod void Test_execute()
    {
        SetUp();
        Datetime sysTime = System.now().addHours(1);
        String chron_exp = '' + sysTime.second() + ' ' + sysTime.minute() + ' ' + sysTime.hour() + ' ' + sysTime.day() + ' ' + sysTime.month() + ' ? ' + sysTime.year();
        String strName = 'HourlyBatchFixGympassEventFields ' + sysTime.day() + '/' + sysTime.month() + '/' + sysTime.year() + ' - '+ sysTime.hour() + ':' + sysTime.minute() + ':' + sysTime.second();
        System.schedule(strName, chron_exp, new SchBatchFixGympassEventFields());
        TearDown();
    }
}