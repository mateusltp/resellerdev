public abstract without sharing class FeeItemDTO {
  protected String id;
  protected String salesforce_id;
  protected String currency_id;
  protected Integer payment_due_days;
  protected List<PaymentDTO> payments;

  public String getId() {
    return this.id;
  }

  public String getSalesforceId() {
    return this.salesforce_id;
  }

  public List<PaymentDTO> getPayments() {
    return this.payments;
  }

  public void setPayments(List<Payment__c> sPayments) {
    this.payments = new List<PaymentDTO>();

    for(Payment__c sPayment : sPayments) {
      this.payments.add(new PaymentDTO(sPayment));
    }
  }

  public abstract QuotelineItem parseToQuoteLineItem();

  public static List<OrderItem> getOrdersItemsWithFieldsToParse(List<Order> orders) {
    return [
      SELECT Id,
      CurrencyIsoCode,
      OrderId,
      Quantity,
      TotalPrice,
      UnitPrice,
      Billing_Date__c,
      Billing_Day__c,
      Contract_Type__c,
      Cutoff_Day__c,
      Discount__c,
      Discount_Price_Ceiling__c,
      Discount_Price_Floor__c,
      Discount_Price_Unit_Decrease__c,
      Flat_Baseline_Adjustment__c,
      Flat_Baseline_Quantity__c,
      Flat_Unit_Price__c,
      Inflation_Adjustment_Index__c,
      Inflation_Adjustment_Period__c,
      Payment_Due_Days__c,
      Recurring_Billing_Period__c,
      Type__c,
      UUID__c,
      Variable_Price_Ceiling__c,
      Variable_Price_Floor__c,
      Variable_Price_Unit_Increase__c
      FROM OrderItem
      WHERE OrderId IN :orders
      WITH SECURITY_ENFORCED
    ];
  }
}