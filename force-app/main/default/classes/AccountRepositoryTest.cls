/**
* @author vinicius.ferraz
* @description AccountRepository unit test
*/
@isTest(seeAllData=false)
public class AccountRepositoryTest {
    
    public static final String ACCOUNT_NAME_TEST_1 = 'Test1';
    
    @TestSetup
    static void createData(){        
        Account acc = generateAccount('1');
        insert acc;
    }
    
    @isTest
    static void testById(){
        Account account = [select Id,Name from Account limit 1];
        AccountRepository accs = new AccountRepository();
        Account acc = null;
        try{
            acc = accs.byId(account.Id);
        }catch(System.QueryException e){
            System.assert(false);
        }
        System.assertEquals(acc.Id,Account.Id);
    }
    
    @isTest
    static void testGympassEntities(){
        Account account = [select Id,Name from Account limit 1];
        AccountRepository accs = new AccountRepository();
        List<Account> gympassEntities = null;
        try{
            gympassEntities = accs.gympassEntities();
        }catch(System.QueryException e){
            System.assert(false);
        }
        System.assertEquals(gympassEntities.size(),1);
    }
    
    @isTest
    static void testAdd(){
        AccountRepository accs = new AccountRepository();
        Account acc = generateAccount('2');        
        System.assertEquals(acc.Id,null);
        try{
            accs.add(acc);
        }catch(System.DmlException e){
            System.assert(false);
        }
        System.assertNotEquals(acc.Id,null);
    }
    
    @isTest
    static void testEdit(){
        AccountRepository accs = new AccountRepository();
        Account acc = generateAccount('3');        
        System.assertEquals(acc.Id,null);
        
        try{
            accs.edit(acc);
        }catch(System.DmlException e){
            System.assert(true);
        }
        System.assertEquals(acc.Id,null);
        
        accs.add(acc);
        
        try{
            accs.edit(acc);
        }catch(System.DmlException e){
            System.assert(false);
        }
        System.assertNotEquals(acc.Id,null);
    }
    
    @isTest
    static void testAddOrEdit(){
        AccountRepository accs = new AccountRepository();
        Account acc = generateAccount('4');        
        System.assertEquals(acc.Id,null);
        
        try{
            accs.addOrEdit(acc);
        }catch(System.DmlException e){
            System.assert(false);
        }
        System.assertNotEquals(acc.Id,null);
        
        try{
            accs.addOrEdit(acc);
        }catch(System.DmlException e){
            System.assert(false);
        }
        System.assertNotEquals(acc.Id,null);
    }
    
    private static Account generateAccount(String name){
        Id rtId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gympass_Entity').getRecordTypeId();
        Account acc = new Account();
        acc.name='Test ' + name;
        acc.RecordTypeId = rtId;
        acc.GP_Status__c = 'Active';
        acc.billingState = 'Minas Gerais';
        acc.BillingCity = 'AccountOpportunityRepositoryTest'+name;
        acc.billingStreet = 'Rua academiabrasilpai';
        acc.billingCountry = 'Brazil';        
        acc.NumberOfEmployees = 520;
        return acc;
    }
    
    @isTest
    static void editTest(){
        AccountRepository accs = new AccountRepository();
        List<Account> editAccount = [select Id,Name from Account limit 10];
        try{
            editAccount = accs.edit(editAccount);
        }catch(System.QueryException e){
            System.assert(false);
        }
        System.assertEquals(editAccount.size(),1);
    }
    
    @isTest
    static void getMapByIdsTest(){
        AccountRepository accs = new AccountRepository();
        List<Account> editAccount = [select Id,Name from Account limit 10];
        Set<Id> resultIds = (new Map<Id,Account>(editAccount)).keySet().clone();
        Map<Id, Account> idToAccount = new Map<Id, Account>();
        try{
            idToAccount = accs.getMapByIds(resultIds);
        }catch(System.QueryException e){
            System.assert(false);
        }
        System.assertEquals(editAccount.size(),1);
    }
    
    @isTest
    static void getAccountsTest(){
        AccountRepository accs = new AccountRepository();
        List<Account> editAccount = [select Id,Name from Account limit 10];
        Set<Id> resultIds = (new Map<Id,Account>(editAccount)).keySet().clone();
        Map<Id, Account> idToAccount = new Map<Id, Account>();
        try{
            editAccount = accs.getAccounts(resultIds);
        }catch(System.QueryException e){
            System.assert(false);
        }
        System.assertEquals(editAccount.size(),1);
    }
    
    @isTest
    static void findChildAccountsTest(){
        AccountRepository accs = new AccountRepository();
        List<Account> listAccount = [select Id,Name from Account limit 1];
        Set<Id> resultIds = new Set<Id>();
        Map<Id, Account> idToAccount = new Map<Id, Account>();
        try{
            resultIds = accs.findChildAccounts(listAccount[0].id);
        }catch(System.QueryException e){
            System.assert(false);
        }
        System.assertEquals(listAccount.size(),1);
    }
    
    @isTest
    static void gympassEntitiesByExternalIdTest(){
        AccountRepository accs = new AccountRepository();
        List<Account> listAccount = [select Id,Name from Account limit 1];
        Map<String, Id> resultIds = new Map<String, Id>();
        try{
            resultIds = accs.gympassEntitiesByExternalId();
        }catch(System.QueryException e){
            System.assert(false);
        }
        System.assertEquals(resultIds.size(),1);
    }   
}