/**
 * @description       : 
 * @author            : AlyssonMota
 * @group             : 
 * @last modified on  : 07-15-2021
 * @last modified by  : Alysson Mota
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   07-15-2021   Alysson Mota   Initial Version
**/
global with sharing class ScheduleCreateSmbAccTeam implements Schedulable {
    global void execute(SchedulableContext SC) {
        Database.executeBatch(new CreateSmbAccTeamBatch());
     }
}