public without sharing class SelfCheckoutRequest {
  private AccountDTO account;
  private OpportunityDTO opportunity;

  public AccountDTO getAccountDTO() {
    return this.account;
  }

  public OpportunityDTO getOpportunityDTO() {
    return this.opportunity;
  }
}