/**
 * @description       : 
 * @author            : roei@gft.com
 * @group             : 
 * @last modified on  : 05-26-2022
 * @last modified by  : roei@gft.com
**/
public without sharing class QuoteDiscountApprovalHandler {    

    public QuoteDiscountApprovalHandler(){}

    @InvocableMethod
    public static void checkNeedApproval( List< Quote > aLstQuote ){
        Set< String > gSetRecordtypeId = new Set< String >{
            Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Client_Sales_SKU_New_Business').getRecordTypeId(),
            Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Client_Success_SKU_Renegotiation').getRecordTypeId(),
            Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('SMB_SKU_New_Business').getRecordTypeId(),
            Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('SMB_Success_SKU_Renegotiation').getRecordTypeId()
        };

        List< Quote > lLstQuoteCheckApproved = new List< Quote >();

        for( Quote iQuote: aLstQuote ){
            if( gSetRecordtypeId.contains( iQuote.RecordTypeId ) ){
                lLstQuoteCheckApproved.add( iQuote );
            }
        }
        
        checkApprovedDiscount( aLstQuote );
    }

    public static void checkApprovedDiscount( List< Quote > aLstQuoteCheckApproved ){
        if( aLstQuoteCheckApproved.isEmpty() ){ return; }
        
        Map< String , Decimal > lMapQuoteIdApprovedDiscount = getOppApprovedDiscount( aLstQuoteCheckApproved );

        Map< String , Integer > lMapQuoteIdApprovalLevel = getMapQuoteApprovalLevel( aLstQuoteCheckApproved );

        for( Quote iQuote : aLstQuoteCheckApproved ){   
            Decimal lApprovedDiscount = lMapQuoteIdApprovedDiscount.get( iQuote.Id );
            if( lApprovedDiscount == null ){ lApprovedDiscount = 0; }

            iQuote.Total_Discount_approved__c = iQuote.Discount <= lApprovedDiscount;
            iQuote.Discount_Approval_Level__c = lMapQuoteIdApprovalLevel.get( iQuote.Id );
        }

        Database.update( aLstQuoteCheckApproved );
    }

    public static Map< String , Decimal > getOppApprovedDiscount( List< Quote > aLstQuoteCheckApproved ){
        Map< String , Decimal > lMapQuoteIdApprovedDiscount = new Map< String , Decimal >();
        Map< String , String > lMapOppIdQuoteId = new Map< String , String >();

        for( Quote iQuote: aLstQuoteCheckApproved ){ lMapOppIdQuoteId.put( iQuote.OpportunityId , iQuote.Id ); }

        for( Assert_Data__c iAssertData : [ SELECT Opportunity__c , Old_Value__c  FROM Assert_Data__c WHERE Opportunity__c =: lMapOppIdQuoteId.keySet() AND Object_Name__c = 'quote' AND Field_Name__c = 'discount' ] ){
            Decimal lApprovedDiscount = String.isBlank( iAssertData.Old_Value__c ) ? 0 : Decimal.valueOf( iAssertData.Old_Value__c );
            lMapQuoteIdApprovedDiscount.put( lMapOppIdQuoteId.get( iAssertData.Opportunity__c ) , lApprovedDiscount );
        }

        return lMapQuoteIdApprovedDiscount;
    }

    public static Map< String , Integer > getMapQuoteApprovalLevel( List< Quote > aLstQuoteCheckApproved ){
        Map< String , Discount_and_Waiver_approval_parameters__mdt > lMapOptionApprovalMetadata = getApprovalParametersMdt( aLstQuoteCheckApproved );

        Map< String , Integer > lMapQuoteIdApprovalLevel = new Map< String , Integer >();

        for( Quote iQuote: aLstQuoteCheckApproved ){
            Integer lApprovalLevel = getDiscountApprovalLevel( lMapOptionApprovalMetadata , 'Annual payment + payroll' , iQuote.Discount );

            lMapQuoteIdApprovalLevel.put( iQuote.Id , lApprovalLevel );
        }
        return lMapQuoteIdApprovalLevel;
    }

    private static Map< String , Discount_and_Waiver_approval_parameters__mdt > getApprovalParametersMdt( List< Quote > aLstQuote ){
        Map< String , Discount_and_Waiver_approval_parameters__mdt > lMapOptionApprovalMetadata 
            = new Map< String , Discount_and_Waiver_approval_parameters__mdt >();

        for( Discount_and_Waiver_approval_parameters__mdt iApprovalMeta : 
            [ SELECT Id, Waiver_CCO__c, Waiver_RM__c, CCO__c, CF__c, Initial_Approver_Email__c, Option__c,
                    RM__c, RM_1__c, RM_2__c, Sellers__c, Waiver_RM_1__c, Initial_Approver_Queue_Name__c, 
                    Manager__c, VP__c, Waiver_VP__c
            FROM Discount_and_Waiver_approval_parameters__mdt WHERE Country__c = 'United States'
            AND Label LIKE :'Clients' + '%'
            AND Minimum_Number_of_Employees__c < : ( aLstQuote[0].Total_Number_of_Employees__c == null ? 1 : aLstQuote[0].Total_Number_of_Employees__c )
            AND Maximum_Number_of_Employees__c >= : ( aLstQuote[0].Total_Number_of_Employees__c == null ? 1 : aLstQuote[0].Total_Number_of_Employees__c ) ] ){
            lMapOptionApprovalMetadata.put( iApprovalMeta.Option__c , iApprovalMeta );
        }

        return lMapOptionApprovalMetadata;
    }

    private static Integer getDiscountApprovalLevel( Map< String , Discount_and_Waiver_approval_parameters__mdt > aMapOptionApprovalMetadata , String aMtdOption, Decimal aDiscount ){
        Discount_and_Waiver_approval_parameters__mdt lApprovalMtd = aMapOptionApprovalMetadata.get( aMtdOption );
   
        if( aDiscount == null || lApprovalMtd == null ){ return 0; }

        Integer lApprovalLevel = 0;

        if( aDiscount > lApprovalMtd.CCO__c || aDiscount > lApprovalMtd.CF__c ){
            lApprovalLevel = 4;    
        } else if( aDiscount > lApprovalMtd.RM_1__c && aDiscount <= lApprovalMtd.RM__c ){
            lApprovalLevel = 3;             
        } else if( aDiscount > lApprovalMtd.RM_2__c && aDiscount <= lApprovalMtd.RM_1__c ){
            lApprovalLevel = 2;
        } else if( aDiscount > lApprovalMtd.Sellers__c && aDiscount <= lApprovalMtd.RM_2__c ){
            lApprovalLevel = 1;
        }

        return lApprovalLevel;
    }
}