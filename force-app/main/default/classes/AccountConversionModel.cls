/**
 * @File Name          : AccountConversionModel.cls
 * @Description        : 
 * @Author             : Samuel Silva - GFT (slml@gft.com)
 * @Group              : 
 * @Last Modified By   : Samuel Silva - GFT (slml@gft.com)
 * @Last Modified On   : 07-28-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    12/03/2020   Samuel Silva - GFT (slml@gft.com)     Initial Version
**/



public with sharing class AccountConversionModel {
  
    static String corporateValue = Label.privateOwnerPicklist;
    static String franchiseValue = Label.franchisePicklist;   
   
    public static SObject getAccountObject(String accountId){
        Account tempAcc = null;
        try{
            tempAcc = [SELECT Id, Name,Types_of_ownership__c FROM Account Where Id =: accountId];           
        }catch(Exception e){
            System.debug('Error retrieving account ' + e);            
        }
        return tempAcc;
    }
    
   
    public static SObject convertAccount(SObject accountObj){
        List<Account> accountsToUpdate = new list<Account>();
        Account currentAccount = (Account)accountObj; 
        System.debug('currentAccount ' + currentAccount.Types_of_ownership__c);
        System.debug('privateOwnerPicklist ' + Label.privateOwnerPicklist);
        System.debug('franchisePicklist ' + Label.franchisePicklist);
        if(currentAccount.Types_of_ownership__c == franchiseValue)  {
            currentAccount.ParentId = null; 
            currentAccount.Types_of_ownership__c = corporateValue;
        }  else if (currentAccount.Types_of_ownership__c == corporateValue){
            currentAccount.Types_of_ownership__c = franchiseValue; 
        }        
        UPDATE currentAccount;
        updateAccountWithChilds(currentAccount);      
        return currentAccount;
      }
      
    
    private static void updateAccountWithChilds(Account currentAccount){
        AccountHierarchy accManager = new AccountHierarchy();
        Set<Id> accountsInHierarchy = new Set<Id>();          
        accountsInHierarchy.add(currentAccount.Id);
        accManager.findlAllChildAccounts(accountsInHierarchy);
        accountsInHierarchy.addAll(accManager.getTotalAccounts());
        accountsInHierarchy.remove(currentAccount.Id);
        if(!accountsInHierarchy.isEmpty()){
            List<Account> childAccounts = [SELECT Id, ParentId, Types_of_Ownership__c FROM Account WHERE ID IN: accountsInHierarchy];
            for(Account c : childAccounts){                
                c.Types_of_ownership__c = currentAccount.Types_of_ownership__c;                 
            }   
            UPDATE childAccounts;                                           
        }        
    }
}