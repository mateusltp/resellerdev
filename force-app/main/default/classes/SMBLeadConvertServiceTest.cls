@isTest
public class SMBLeadConvertServiceTest {
    
    @isTest
    private static void executeManualLeadConversionTest() {
        
        Lead lead = LeadMock.getSMBLead();
        lead.FirstName = 'Test Lead';
        lead.Status = 'Ready to Convert';
        lead.Business_Unit__c = 'SMB';
        lead.CNPJ__c = 'testCNPJ';
        lead.Sales_Flow__c = 'new';
        lead.Client_UUID__c = 'Client UUID';
        lead.Razao_social__c = 'Test';
        lead.NumberOfEmployees = 10;
        lead.Language_Code__c = 'br';
        lead.Contact_UUID__c = 'Contact UUID';
        
        insert lead;
        
        Test.startTest();
        
        SMBLeadConvertService service = new SMBLeadConvertService(lead.Id);
        service.executeManualLeadConversion();
        
        Test.stopTest();
        
        Opportunity opportunity = service.getOpportunity();
        
        lead = [SELECT Id, isConverted FROM Lead WHERE Id = :lead.Id];
        
        System.assert(lead.IsConverted, 'The Lead is not converted');
        
        List<Account> accounts = [SELECT Id FROM Account LIMIT 1];
        
        System.assert(!accounts.isEmpty(), 'No account has been created.');
        
        List<Contact> contacts = [SELECT Id FROM Contact LIMIT 1];
        
        System.assert(!contacts.isEmpty(), 'No contact has been created.');
        
        System.assert(opportunity.Id != null, 'No opportunity has been returned'); 
        
    }
    
    
    @IsTest
    private static void executeLeadConversionTest(){
        
        Lead lead = LeadMock.getSMBLead();
        lead.FirstName = 'Test Lead';
        lead.Status = 'Ready to Convert';
        lead.Business_Unit__c = 'SMB';
        lead.CNPJ__c = 'testCNPJ';
        lead.Sales_Flow__c = 'new';
        lead.Client_UUID__c = 'Client UUID';
        lead.Razao_social__c = 'Test';
        lead.NumberOfEmployees = 10;
        lead.Language_Code__c = 'br';
        lead.Contact_UUID__c = 'Contact UUID';
        lead.CountryCode = 'BR';
        lead.Country = 'Brazil';
        lead.State = 'Bahia';
        
        insert lead;
        
        Contact billingContact = new Contact(
            UUID__c = 'UUID Test',
            FirstName = 'Test FN', 
            LastName = 'Test LN',
            Email = 'test@test.com',
            Finance_Contact__c = true
        );
        
        User integrationSMBJamorUser = [
            SELECT Id
            FROM User
            WHERE Name = 'Integration SMB Jamor'
            LIMIT 1
        ];
        
        
        
        System.runAs(integrationSMBJamorUser) {
            
            Test.startTest();
            
            SMBLeadConvertService service = new SMBLeadConvertService(lead, billingContact);
            service.executeLeadConversion();  
            
            Test.stopTest();
                        
            lead = [SELECT Id, IsConverted, RecordTypeId FROM Lead LIMIT 1];
            
            System.assertEquals(
                true,
                lead.IsConverted,
                'The lead is not converted'
            );
            
            Id recordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('SMB Direct Channel').getRecordTypeId();
            System.assertEquals(
                recordTypeId,
                lead.RecordTypeId,
                'The lead\'s record type is wrong'
            );
            
            Account account = service.getAccount();
            
            System.assertEquals(
                'test company',
                account.Name,
                'The account is not correct'
            );
            
            Contact contact = [SELECT Id, Name, AccountId FROM contact LIMIT 1];
            
            System.assertEquals(
                account.Id,
                contact.AccountId,
                'The AccountId of Contact is not correct'
            );
            
        }
         
    }
    
}