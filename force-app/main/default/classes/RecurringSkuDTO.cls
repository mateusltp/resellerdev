public without sharing class RecurringSkuDTO {
  private List<PaymentDTO> payments;
  private List<Sku> skus;

  public RecurringSkuDTO() {
    this.payments = new List<PaymentDTO>();
    this.skus = new List<Sku>();
  }

  public void addPayment(Payment__c payment) {
    this.payments.add(new PaymentDTO(payment));
  }

  public void addSku(OrderItem orderItem) {
    this.skus.add(new Sku(orderItem));
  }

  public void addSkuFm(OrderItem orderItem) {
    Sku core = new Sku(orderItem);
    core.unit_price = ((orderItem.UnitPrice * 100) / 115);
    core.flat_unit_price = ((orderItem.UnitPrice * 100) / 115);
    core.flat_total_price = ((orderItem.UnitPrice * 100) / 115);
    core.sku_type = SkuType.GYMPASS_CORE;

    Sku fm = new Sku(orderItem);
    fm.unit_price = ((orderItem.UnitPrice * 15) / 115);
    fm.flat_unit_price = ((orderItem.UnitPrice * 15) / 115);
    fm.flat_total_price = ((orderItem.UnitPrice * 15) / 115);
    fm.id = new Uuid().getValue();

    this.skus.add(core);
    this.skus.add(fm);
  }

  private class Sku {
    private String id;
    private String salesforce_id;
    private Datetime begin_date;
    private Datetime end_date;
    private SkuType sku_type;
    private PrycingType pricing_type;

    private Integer flat_baseline_quantity;
    private Decimal unit_price;
    private Decimal flat_unit_price;
    private Decimal flat_total_price;
    private Decimal flat_discount_percentage;
    private Boolean flat_baseline_adjustment;

    private Decimal variable_price_floor;
    private Decimal variable_price_ceiling;
    private Decimal variable_price_unit_increase;

    private Decimal discount_price_floor;
    private Decimal discount_price_ceiling;
    private Decimal discount_price_unit_decrease;

    private String inflation_adjustment_period;
    private String inflation_adjustment_type;

    private List<WaiverDTO> waivers;

    private Sku(OrderItem orderItem) {
      this.id = orderItem.UUID__c != null
        ? orderItem.UUID__c
        : new Uuid().getValue();
      this.salesforce_id = orderItem.Id;
      this.sku_type = getSkuType(orderItem);
      this.pricing_type = getPrycingType(orderItem);
      this.flat_baseline_adjustment = orderItem.Flat_Baseline_Adjustment__c;
      this.unit_price = orderItem.UnitPrice;
      this.flat_unit_price = orderItem.UnitPrice;
      this.flat_total_price = orderItem.TotalPrice;
      this.flat_discount_percentage = orderItem.Discount__c;
      this.flat_baseline_quantity = (Integer) orderItem.Quantity;
      this.variable_price_floor = orderItem.Variable_Price_Floor__c;
      this.variable_price_ceiling = orderItem.Variable_Price_Ceiling__c;
      this.variable_price_unit_increase = orderItem.Variable_Price_Unit_Increase__c;
      this.discount_price_floor = orderItem.Discount_Price_Floor__c;
      this.discount_price_ceiling = orderItem.Discount_Price_Ceiling__c;
      this.discount_price_unit_decrease = orderItem.Discount_Price_Unit_Decrease__c;
      this.inflation_adjustment_type = orderItem.Inflation_Adjustment_Index__c;
      this.inflation_adjustment_period = orderItem.Inflation_Adjustment_Period__c;
      this.begin_date = orderItem.Start_Date__c;
      this.end_date = orderItem.End_Date__c;

      this.waivers = new List<WaiverDTO>();

      for (Waiver__c waiver : orderItem.Waiver__r) {
        this.waivers.add(new WaiverDTO(waiver));
      }
    }

    private SkuType getSkuType(OrderItem orderItem) {
      if (String.isBlank(orderItem.Type__c)) {
        return null;
      }

      if (orderItem.PlanCode__c.contains('with_Family_Members')) {
        return SkuType.FAMILY_MEMBER;
      }
        
      if (orderItem.PlanCode__c.equals('SKUFamilyMembers')) {
        return SkuType.FAMILY_MEMBER;
      }

      if (orderItem.Type__c.equals('Enterprise Subscription')) {
        return SkuType.GYMPASS_CORE;
      }

      if (orderItem.Type__c.contains('Professional Service')) {
        return SkuType.PROFESSIONAL_SERVICES;
      }

      return null;
    }

    private PrycingType getPrycingType(OrderItem orderItem) {
      if (String.isBlank(orderItem.Contract_Type__c)) {
        return null;
      }

      switch on orderItem.Contract_Type__c {
        when 'Copay 2' {
          return PrycingType.VARIABLE_PRICE_ENROLLED;
        }
        when 'Inverted Copay 2' {
          return PrycingType.DISCOUNT_PRICE_ENROLLED;
        }
        when 'Flat Fee' {
          return PrycingType.FLAT_PRICE_ELIGIBLE;
        }
        when else {
          return null;
        }
      }
    }
  }

  private enum SkuType {
    GYMPASS_CORE,
    FAMILY_MEMBER,
    PROFESSIONAL_SERVICES
  }

  private enum PrycingType {
    FLAT_PRICE_ELIGIBLE,
    VARIABLE_PRICE_ELIGIBLE,
    VARIABLE_PRICE_ENROLLED,
    DISCOUNT_PRICE_ENROLLED
  }
}