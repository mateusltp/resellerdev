@isTest//(SeeAllData=true)
private class ValidateExceptionsCtrlTest
{
    static void SetUp() {
		test.StartTest();
	}
    
	static void TearDown() {
		test.StopTest();
	}
    
	public static testmethod void Test_submitForApproval() {
		SetUp();
        Account account = new Account();
        account.Name = 'Contract Test Account';
        account.BillingCountry = 'Brazil';
        account.BillingState = 'São Paulo';
        account.Id_Company__c = '00.000.000/1000-00';
        account.Type = 'Partner';
        INSERT account;
                
        Opportunity opportunity1 = new Opportunity();
        opportunity1.AccountId = account.Id;
        opportunity1.Name = 'op test';
        opportunity1.StageName = 'Offer Sent';
        opportunity1.CloseDate = Date.today();
        opportunity1.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Partner - Small and Medium').getRecordTypeId();
                
        INSERT opportunity1;
                
        opportunity1.StageName = 'Opportunity Validated';        
        UPDATE opportunity1;
        
        Copay_Plan__c copayPlan = new Copay_Plan__c();
        copayPlan.isCustom__c = 'Yes';
        INSERT copayPlan;
                
        Quote quote = new Quote();
        quote.Name = 'Test';
        quote.OpportunityId = opportunity1.Id;
        quote.Gym__c = copayPlan.Id;
        quote.License_Fee_Waiver__c = 'No';
        quote.Volume_Discount_Type__c = 'By single user volume';
        quote.Threshold_Value_1__c= 1;
		quote.Threshold_Value_2__c= 1;
		quote.Threshold_Value_3__c= 1;
		quote.First_Discount_Range__c= 1;
		quote.Second_Discount_Range__c= 1;
		quote.Third_Discount_Range__c= 1;
		quote.Fourth_Discount_Range__c= 1;
		quote.Threshold_Value_4__c= 1;
        quote.RecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByName().get('Gyms - Quote Partner').getRecordTypeId();
        INSERT quote;
        
        
        List<Exception__c> listExceptions = new List<Exception__c>();
        Exception__c exceptionToInsert = new Exception__c();
        exceptionToInsert.Approver__c = UserInfo.getUserId();
        exceptionToInsert.Rationale__c = 'As an internal policy, company pays only on specific days of the month: 1st, 11th and 21st and they discount the employees on the 5th. They also ""close"" their payroll around the 28th which means they need to receive the memorandum at least 8/10 days before. So, customer wants to pay us after discounting the emplyees and they have fixed days to do the payment. What we have agreed with the customer is: - 20th of the month: cut-off date,  - 21st of the month: issuing of memorandum and sending to the customer,  - 11th of the month: payment by customer (which is the following payment date after they discount the employees) Therefore, I request 22 due dates.';
        exceptionToInsert.Status__c = 'Pending_Approval';
        exceptionToInsert.Subtype__c = 'Due Dates';
        exceptionToInsert.Type__c = 'Membership Fee';
        exceptionToInsert.What_the_Company_Wants__c = '22 days';
        exceptionToInsert.Proposal__c = quote.Id;
        INSERT exceptionToInsert;
        listExceptions.add(exceptionToInsert);
        
		Id recordid = exceptionToInsert.Id;
		string jsonlistexceptions = JSON.serialize(listExceptions);
        ValidateExceptionsCtrl.submitForApproval(recordid, jsonlistexceptions);
		TearDown();
	}

    public static testmethod void Test_setValidateExceptions() {
		SetUp();
        Account account = new Account();
        account.Name = 'Contract Test Account';
        account.BillingCountry = 'Brazil';
        account.BillingState = 'São Paulo';
        account.Id_Company__c = '00.000.000/1000-00';
        account.Type = 'Partner';
        INSERT account;
                
        Opportunity opportunity1 = new Opportunity();
        opportunity1.AccountId = account.Id;
        opportunity1.Name = 'op test';
        opportunity1.StageName = 'Offer Sent';
        opportunity1.CloseDate = Date.today();
        opportunity1.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Partner - Small and Medium').getRecordTypeId();
                
        INSERT opportunity1;
                
        opportunity1.StageName = 'Opportunity Validated';        
        UPDATE opportunity1;
        
        Copay_Plan__c copayPlan = new Copay_Plan__c();
        copayPlan.isCustom__c = 'Yes';
        INSERT copayPlan;
                
        Quote quote = new Quote();
        quote.Name = 'Test';
        quote.OpportunityId = opportunity1.Id;
        quote.Gym__c = copayPlan.Id;
        quote.License_Fee_Waiver__c = 'No';
        quote.Volume_Discount_Type__c = 'By single user volume';
        quote.Threshold_Value_1__c= 1;
		quote.Threshold_Value_2__c= 1;
		quote.Threshold_Value_3__c= 1;
		quote.First_Discount_Range__c= 1;
		quote.Second_Discount_Range__c= 1;
		quote.Third_Discount_Range__c= 1;
		quote.Fourth_Discount_Range__c= 1;
		quote.Threshold_Value_4__c= 1;
        quote.RecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByName().get('Gyms - Quote Partner').getRecordTypeId();
        INSERT quote;
        
		Id recordid = quote.Id;
		ValidateExceptionsCtrl.setValidateExceptions(recordid);
		TearDown();
	}

	public static testmethod void Test_deleteOldExceptions() {
		SetUp();
        Account account = new Account();
        account.Name = 'Contract Test Account';
        account.BillingCountry = 'Brazil';
        account.BillingState = 'São Paulo';
        account.Id_Company__c = '00.000.000/0000-00';
        INSERT account;
		Id recordid = account.Id;
		ValidateExceptionsCtrl.deleteOldExceptions(recordid);
		TearDown();
	}

	public static testmethod void Test_getExceptions() {
		SetUp();
        Account account = new Account();
        account.Name = 'Contract Test Account';
        account.BillingCountry = 'Brazil';
        account.BillingState = 'São Paulo';
        account.Id_Company__c = '00.000.000/0000-00';
        INSERT account;
		Id recordid = account.Id;
        ValidateExceptionsCtrl.getExceptions(recordid);
		TearDown();
	}

    public static testmethod void Test_updateExceptions() {
		SetUp();
		List<Exception__c> editedexceptionlist = new List<Exception__c>();
        ValidateExceptionsCtrl.updateExceptions(editedexceptionlist);
		TearDown();
	}
}