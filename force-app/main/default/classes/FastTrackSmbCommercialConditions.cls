/**
 * @description       : 
 * @author            : roei@gft.com
 * @group             : 
 * @last modified on  : 11-18-2021
 * @last modified by  : roei@gft.com
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   04-07-2021   roei@gft.com   Initial Version
**/
public with sharing class FastTrackSmbCommercialConditions extends FastTrackCommercialConditionsHandler {
    public FastTrackSmbCommercialConditions(){
        gWaiverApprovalProcessDevName = 'SMB_Waiver_approval_2';
        gMdtLabelPrefix = 'SMB';
    }

    protected override Integer getWaiverApproverLevelNeeded( Decimal aWaiverMonths ){
        Discount_and_Waiver_approval_parameters__mdt lApprovalParamMdt = mapOptionApprovalMetadata.get( 'Waiver' );

        if( aWaiverMonths == null || aWaiverMonths == 0 || lApprovalParamMdt == null ){ return 0; }
        
        Integer lApprovalLevel = 0;

        if( aWaiverMonths > lApprovalParamMdt.Waiver_RM_1__c && aWaiverMonths <= lApprovalParamMdt.Waiver_RM__c ){
            lApprovalLevel = 1;
        } else if( aWaiverMonths > lApprovalParamMdt.Waiver_RM__c ){
            lApprovalLevel = 2;
        } 

        return lApprovalLevel;
    }

    protected override Boolean commercialConditionsApprovalNeeded(){
        return ( quote.Enterprise_Discount_Approval_Needed__c && !quote.Enterprise_Subscription_Approved__c ) || 
               ( quote.Setup_Discount_Approval_Needed__c && !quote.Setup_Discount_Approved__c ) ||
               ( quote.Enablers_Approval_Needed__c && !quote.Enablers_Approved__c ) ||
               this.currentDiscountLevel == 0;
    }
}