/**
 * @description       : 
 * @author            : alysson.mota@gympass.com
 * @group             : 
 * @last modified on  : 02-25-2022
 * @last modified by  : alysson.mota@gympass.com
**/
public with sharing class ProductAssignmentService {
    Id CapCommConditionRecType = Schema.SObjectType.Commercial_Condition__c.getRecordTypeInfosByDeveloperName().get('CAP').getRecordTypeId();
    String className = ProductAssignmentService.class.getName();
    PS_Constants constants = PS_Constants.getInstance();

    /**
    * Runs on after Product Assignment insert trigger event and calls all the actions that should run on it 
    * @author alysson.mota@gympass.com | 02-21-2022 
    * @param idToNewProdAssignemnt 
    **/
    public void createAndSyncPAsAndRelatedRecords(Map<Id, Product_Assignment__c> idToNewProdAssignemnt) {
        fflib_ISObjectUnitOfWork uow = Application.UnitOfWork.newInstance();

        try {
            createRecordsForAccountsWithChildProducts(uow, idToNewProdAssignemnt);
            syncCapPAsWithNonCapPAs(uow, idToNewProdAssignemnt);
            syncCommConditionsFromSameProduct(uow, idToNewProdAssignemnt);    
            
            // Commits all records after actions performed
            uow.commitWork(); 
        } catch (Exception e) {
            createLog(e, 'createAndSyncPAsAndRelatedRecords');
        }  
    }

    /**
    * Runs on after Product Assignment update trigger event and calls all the actions that should run on it 
    * @author alysson.mota@gympass.com | 02-21-2022 
    * @param idToNewProdAssignemnt 
    * @param idToOldProdAssignemnt 
    **/
    public void evaluateAndSyncPAsAndRelatedRecords(Map<Id, Product_Assignment__c> idToNewProdAssignemnt, Map<Id, Product_Assignment__c> idToOldProdAssignemnt) {
        fflib_ISObjectUnitOfWork uow = Application.UnitOfWork.newInstance();
        
        try {
            checkPAsAndSyncWithChildPAs(uow, idToNewProdAssignemnt, idToOldProdAssignemnt);

            // Commits all records after actions performed
            uow.commitWork();
        } catch(Exception e) {
            createLog(e, 'evaluateAndSyncPAsAndRelatedRecords');
        }
        
    }

    /**
    * Runs on before Product Assignment delete trigger event and calls all the actions that should run on it 
    * @author alysson.mota@gympass.com | 02-21-2022 
    * @param Map<Id, Product_Assignment__c>
    **/
    public void deleteRelatedPaRecords(Map<Id, Product_Assignment__c> idToOldProdAssignemnt) {
        fflib_ISObjectUnitOfWork uow = Application.UnitOfWork.newInstance();
        
        try {
            deleteRelatedRecordsOnCapPaDelete(uow, idToOldProdAssignemnt);

            uow.commitWork();
        } catch(Exception e) {
            createLog(e, 'deleteRelatedPaRecords');
        }
    }


    /**
    * Delete related records on Cap PA delete 
    * @author alysson.mota@gympass.com | 02-21-2022 
    * @param fflib_ISObjectUnitOfWork 
    * @param idToOldProdAssignemnt : Map<Id, Product_Assignment__c>
    **/
    private void deleteRelatedRecordsOnCapPaDelete(fflib_ISObjectUnitOfWork uow, Map<Id, Product_Assignment__c> idToOldProdAssignemnt) {
        Set<Id> prodIdSet               = new Set<Id>();
        Set<Id> productAssignmentIdSet  = new Set<Id>();
        Set<Id> commCondIdSet           = new Set<Id>();
        Set<Id> capProdAssignmentIdSet  = new Set<Id>();

        List<Product_Assignment__c> deletedProdAssigment = new List<Product_Assignment__c>();
        
        Map<Id, List<Product_Opportunity_Assignment__c>> parentProductIdToPoa   = new Map<Id, List<Product_Opportunity_Assignment__c>>();
        Map<Id, Product_Assignment__c> deletedProdAssigmentMap                  = new Map<Id, Product_Assignment__c>();

        ProductAssignmentSelector paSelector                = new ProductAssignmentSelector();
        ProductOpportunityAssignmentSelector poaSelector    = new ProductOpportunityAssignmentSelector();
        CommercialConditionSelector commConditionSelector   = new CommercialConditionSelector();
        
        for (Product_Assignment__c pa : idToOldProdAssignemnt.values()) {
            capProdAssignmentIdSet.add(pa.Id);
        }

        if (capProdAssignmentIdSet.size() > 0) {
            deletedProdAssigment = paSelector.selectProductAssignmentByID(capProdAssignmentIdSet);
        }

        deletedProdAssigmentMap = new Map<Id, Product_Assignment__c>(deletedProdAssigment);

        for (Product_Assignment__c pa : idToOldProdAssignemnt.values()) {
            prodIdSet.add(pa.ProductId__c);
        }

        List<Product_Opportunity_Assignment__c> poaLst = poaSelector.selectByParentProductIdSet(prodIdSet);

        for (Product_Opportunity_Assignment__c poa : poaLst) {
            productAssignmentIdSet.add(poa.ProductAssignmentId__c);
            commCondIdSet.add(poa.ProductAssignmentId__r.CommercialConditionId__c);

            if (parentProductIdToPoa.containsKey(poa.ProductAssignmentId__r.ProductId__r.Parent_Product__c)) {
                parentProductIdToPoa.get(poa.ProductAssignmentId__r.ProductId__r.Parent_Product__c).add(poa);
            } else {
                parentProductIdToPoa.put(poa.ProductAssignmentId__r.ProductId__r.Parent_Product__c, new List<Product_Opportunity_Assignment__c>{poa});
            }
        }

        Map<Id, Product_Assignment__c> paMap = new Map<Id,Product_Assignment__c>(
            paSelector.selectProductAssignmentByID(productAssignmentIdSet)
        );

        Map<Id, Commercial_Condition__c> commCondMap = new Map<Id, Commercial_Condition__c>(
            commConditionSelector.selectCommConditionByID(commCondIdSet)
        );

        for (Product_Assignment__c deletedPa : deletedProdAssigmentMap.values()) {
            List<Product_Opportunity_Assignment__c> childPoaLst = parentProductIdToPoa.get(deletedPa.ProductId__c);

            if (childPoaLst != null) {
                for (Product_Opportunity_Assignment__c poa : childPoaLst) {
                    if (poa.ProductAssignmentId__r.CommercialConditionId__r.RecordTypeId == deletedPa.CommercialConditionId__r.RecordTypeId) {
                        Product_Assignment__c prodAssignment = paMap.get(poa.ProductAssignmentId__c);
                        Commercial_Condition__c commCond = commCondMap.get(poa.ProductAssignmentId__r.CommercialConditionId__c);

                        uow.registerDeleted(poa);
                        if (prodAssignment != null && commCond != null) {
                            uow.registerDeleted(prodAssignment);
                            uow.registerDeleted(commCond);
                        }
                        
                    }
                }
            }  
        }
    }

    /**
    * Check current updated PAs and Sync with childs PAs
    * @author alysson.mota@gympass.com | 02-21-2022 
    * @param fflib_ISObjectUnitOfWork 
    * @param idToNewProdAssignemnt : Map<Id, Product_Assignment__c> 
    * @param idToOldProdAssignemnt : Map<Id, Product_Assignment__c> 
    **/
    private void checkPAsAndSyncWithChildPAs(fflib_ISObjectUnitOfWork uow, Map<Id, Product_Assignment__c> idToNewProdAssignemnt, Map<Id, Product_Assignment__c> idToOldProdAssignemnt) {
        ProductOpportunityAssignmentSelector poaSelector = new ProductOpportunityAssignmentSelector();
        
        Map<Id, Product_Assignment__c> auxIdToNewProdAssignemnt = new Map<Id, Product_Assignment__c>();
        
        for (Product_Assignment__c newProdAssignemnt : idToNewProdAssignemnt.values()) {
            Product_Assignment__c oldProdAssignemnt = idToOldProdAssignemnt.get(newProdAssignemnt.Id);

            if (newProdAssignemnt.Commercial_Condition_Type_Dev_Name__c == 'CAP' && 
                newProdAssignemnt.Market_price__c != oldProdAssignemnt.Market_price__c) {
                auxIdToNewProdAssignemnt.put(newProdAssignemnt.Id, newProdAssignemnt);
            }
        }

        if (auxIdToNewProdAssignemnt.size() > 0) {
            syncCapPAsWithNonCapPAs(uow, auxIdToNewProdAssignemnt);
        }
    }

    /**
    * Creates Product Assignments, Product Opp Assignments, Late Cancel and No show
    * Commercial Condition records for accounts with child products and CAP Commercial Conditions 
    * @author alysson.mota@gympass.com | 02-21-2022 
    * @param fflib_ISObjectUnitOfWork
    * @param Map<Id, Product_Assignment__c>
    **/
    public void createRecordsForAccountsWithChildProducts(fflib_ISObjectUnitOfWork uow, Map<Id, Product_Assignment__c> idToNewProdAssignemnt) {
        Set<Id> prodIdSet       = new Set<Id>();
        Set<Id> commCondIdSet   = new Set<Id>();
        Set<Id> accIdSet        = new Set<Id>();
        
        List<Product_Opportunity_Assignment__c> poaLst  = new List<Product_Opportunity_Assignment__c>();
        List<ObjectsStruct> objStructLst                = new List<ObjectsStruct>();
        
        Map<Id, List<Product_Assignment__c>> prodIdToProdAssignemntLst      = new Map<Id, List<Product_Assignment__c>>();
        Map<Id, Product_Opportunity_Assignment__c> accIdToCapPoaMap         = new Map<Id, Product_Opportunity_Assignment__c>();
        Map<Id, Product_Opportunity_Assignment__c> accIdToLateCancelPoaMap  = new Map<Id, Product_Opportunity_Assignment__c>();
        Map<Id, Product_Opportunity_Assignment__c> accIdToNoShowPoaMap      = new Map<Id, Product_Opportunity_Assignment__c>();
        Map<Id, Commercial_Condition__c> idToCommCond                       = new Map<Id, Commercial_Condition__c>();

        ProductOpportunityAssignmentSelector poaSelector    = new ProductOpportunityAssignmentSelector();
        CommercialConditionSelector commConditionSelector   = new CommercialConditionSelector();
		
        Set<String> commCondsRecTypeDevNameSet = new Set<String>{'Late_Cancellation', 'No_Show'};

        for (Product_Assignment__c pa : idToNewProdAssignemnt.values()) {
            
            if (commCondsRecTypeDevNameSet.contains(pa.Commercial_Condition_Type_Dev_Name__c)) {
                prodIdSet.add(pa.ProductId__c);
                commCondIdSet.add(pa.CommercialConditionId__c);

                if (prodIdToProdAssignemntLst.containsKey(pa.ProductId__c)) {
                    prodIdToProdAssignemntLst.get(pa.ProductId__c).add(pa);
                } else {
                    prodIdToProdAssignemntLst.put(pa.ProductId__c, new List<Product_Assignment__c>{pa});
                }
            }
        }
        
        if (prodIdSet.size() > 0) {
            poaLst = poaSelector.selectByParentProductIdSet(prodIdSet);
        }

        for (Product_Opportunity_Assignment__c poa : poaLst) {
            if (poa.ProductAssignmentId__r.CommercialConditionId__r.RecordType.DeveloperName == 'CAP') {
                accIdToCapPoaMap.put(poa.OpportunityMemberId__r.Account__c, poa);
                accIdSet.add(poa.OpportunityMemberId__r.Account__c);
            }

            if (poa.ProductAssignmentId__r.CommercialConditionId__r.RecordType.DeveloperName == 'No_Show') {
                accIdToNoShowPoaMap.put(poa.OpportunityMemberId__r.Account__c, poa);
            }

            if (poa.ProductAssignmentId__r.CommercialConditionId__r.RecordType.DeveloperName == 'Late_Cancellation') {
                accIdToLateCancelPoaMap.put(poa.OpportunityMemberId__r.Account__c, poa);
            }
        }

        for (Id accId : accIdSet) {
            Product_Opportunity_Assignment__c capPoa         = accIdToCapPoaMap.get(accId);
            Product_Opportunity_Assignment__c noShowPoa      = accIdToNoShowPoaMap.get(accId);
            Product_Opportunity_Assignment__c lateCancelPoa  = accIdToLateCancelPoaMap.get(accId);

            List<Product_Assignment__c> paLst = new List<Product_Assignment__c>();

            if (prodIdToProdAssignemntLst.containsKey(capPoa.ProductAssignmentId__r.ProductId__r.Parent_Product__c)) {
                paLst = prodIdToProdAssignemntLst.get(capPoa.ProductAssignmentId__r.ProductId__r.Parent_Product__c);
            }

            for (Product_Assignment__c pa : paLst) {
                if (pa.Commercial_Condition_Type_Dev_Name__c == 'No_Show'  && noShowPoa == null) {
                    ObjectsStruct objStruct = getObjStruct(capPoa, pa);
                    objStructLst.add(objStruct);
                }
                
                if (pa.Commercial_Condition_Type_Dev_Name__c == 'Late_Cancellation'  && lateCancelPoa == null) {
                    ObjectsStruct objStruct = getObjStruct(capPoa, pa);
                    objStructLst.add(objStruct);
                }
            }
        }

        if (objStructLst.size() > 0) {
            Set<Id> auxCommCondIdSet = new Set<Id>();
            for (ObjectsStruct struct : objStructLst) {
                commCondIdSet.add(struct.insertedParentPa.CommercialConditionId__c);
            }

            List<Commercial_Condition__c> lst = commConditionSelector.selectCommConditionByID(commCondIdSet);
            idToCommCond = new Map<Id, Commercial_Condition__c>(lst);

            for (ObjectsStruct struct : objStructLst) {
                Commercial_Condition__c commCond = idToCommCond.get(struct.insertedParentPa.CommercialConditionId__c);
                struct.insertedCommCond = commCond;
            }
        }

        for (ObjectsStruct struct : objStructLst) {
            if (struct.insertedCommCond != null) {
                Commercial_Condition__c commCondClone       = getCommConditionClone(struct.insertedCommCond);
                Product_Opportunity_Assignment__c poaClone  = getProdOppAssignmentClone(struct.capPoa);
                Product_Assignment__c prodAssignClone       = getProductAssignmentClone(struct.insertedParentPa);
                prodAssignClone.ProductId__c = poaClone.ProductAssignmentId__r.ProductId__c;

                uow.registerNew(commCondClone);
                    
                uow.registerNew(prodAssignClone);
                uow.registerRelationship(prodAssignClone, Product_Assignment__c.CommercialConditionId__c, commCondClone);
                
                uow.registerNew(poaClone);
                uow.registerRelationship(poaClone, Product_Opportunity_Assignment__c.ProductAssignmentId__c, prodAssignClone);
            }            
        }
    }

    /**
    * Creates a clone of the given Commercial_Condition__c
    * @author alysson.mota@gympass.com | 02-21-2022 
    * @param Commercial_Condition__c 
    * @return Commercial_Condition__c 
    **/
    private static Commercial_Condition__c getCommConditionClone(Commercial_Condition__c commCondition) {
        return commCondition.clone(false, true, false, false);
    }

    /**
    * Creates a clone of the given Product_Assignment__c
    * @author alysson.mota@gympass.com | 02-21-2022 
    * @param Product_Assignment__c 
    * @return Product_Assignment__c 
    **/
    private static Product_Assignment__c getProductAssignmentClone(Product_Assignment__c prodAssignment) {
        return prodAssignment.clone(false, true, false, false);
    }

    /**
    * Creates a clone of the given Product_Opportunity_Assignment__c
    * @author alysson.mota@gympass.com | 02-21-2022 
    * @param Product_Opportunity_Assignment__c 
    * @return Product_Opportunity_Assignment__c 
    **/
    private static Product_Opportunity_Assignment__c getProdOppAssignmentClone(Product_Opportunity_Assignment__c poa) {
        Product_Opportunity_Assignment__c dupPoa = poa.clone(false, true, false, false);
        return dupPoa;
    }

    /**
    * Get ObjectsStruct instance
    * @author alysson.mota@gympass.com | 02-21-2022 
    * @param Product_Opportunity_Assignment__c 
    * @param Product_Assignment__c 
    * @return ObjectsStruct 
    **/
    private ObjectsStruct getObjStruct(Product_Opportunity_Assignment__c capPoa, Product_Assignment__c insertedParentPa) {
        ObjectsStruct objStruct = new ObjectsStruct();
        objStruct.capPoa = capPoa;
        objStruct.insertedParentPa = insertedParentPa;

        return objStruct;
    }
    
    /**
    * Copy values of the CAP Product Assignments with Late Cancel and No Show Product Assignments
    * with parent product
    * @author alysson.mota@gympass.com | 02-21-2022 
    * @param fflib_ISObjectUnitOfWork 
    * @param Map<Id, Product_Assignment__c> 
    **/
    private void syncCapPAsWithNonCapPAs(fflib_ISObjectUnitOfWork uow, Map<Id, Product_Assignment__c> idToNewProdAssignemnt) {
        Set<Id> prodIdSet = new Set<Id>();
        
        Map<Id, Product_Assignment__c> prodIdToCapPa            = new Map<Id, Product_Assignment__c>();
        Map<Id, List<Product_Assignment__c>> prodIdToNonCapPa   = new Map<Id, List<Product_Assignment__c>>();
        
        ProductAssignmentSelector paSelector = new ProductAssignmentSelector();

        for (Product_Assignment__c pa : idToNewProdAssignemnt.values()) {
            prodIdSet.add(pa.ProductId__c);
        }

        List<Product_Assignment__c> paLst = paSelector.selectCommercialConditionByProductId(prodIdSet).values();

        for (Product_Assignment__c pa : paLst) {
            if (pa.CommercialConditionId__c == null) {
                continue;
            }            

            if (pa.CommercialConditionId__r.RecordTypeId == CapCommConditionRecType) {
                prodIdToCapPa.put(pa.ProductId__c, pa);
            } else {
                if (prodIdToNonCapPa.containsKey(pa.ProductId__c)) {
                    prodIdToNonCapPa.get(pa.ProductId__c).add(pa);
                } else {
                    prodIdToNonCapPa.put(pa.ProductId__c, new List<Product_Assignment__c>{pa});
                }
            }
        }

        for (Id prodId : prodIdSet) {
            Product_Assignment__c capPA = prodIdToCapPa.get(prodId);
            List<Product_Assignment__c> nonCapPaLst = prodIdToNonCapPa.get(prodId);

            if (capPA != null && nonCapPaLst != null) {
                for (Product_Assignment__c nonCapPa : nonCapPaLst) {
                    nonCapPa.Market_Price__c = capPA.Market_Price__c;
                    uow.registerDirty(nonCapPa);
                }
            } 
        }
    }

    /**
    * Copy values from from Product Assignments' CAP Commercial Condition to the  Product Assignments' 
    * Non Cap Commercial Conditions with the same product
    * @author alysson.mota@gympass.com | 02-21-2022 
    * @param fflib_ISObjectUnitOfWork 
    * @param Map<Id, Product_Assignment__c> 
    **/
    private void syncCommConditionsFromSameProduct(fflib_ISObjectUnitOfWork uow, Map<Id, Product_Assignment__c> idToNewProdAssignemnt) {
        CommercialConditionSelector commConditionSelector   = new CommercialConditionSelector();
        ProductAssignmentSelector paSelector                = new ProductAssignmentSelector();

        Map<Id, Product_Assignment__c> capPaMap             = new Map<Id, Product_Assignment__c>();
        Map<Id, List<Product_Assignment__c>> nonCapPaMap    = new Map<Id, List<Product_Assignment__c>>();
        
        Set<Id> currentCommCondSet = new Set<Id>();
        Set<Id> prodIdSet = new Set<Id>();

        for (Product_Assignment__c pa : idToNewProdAssignemnt.values()) {
            prodIdSet.add(pa.ProductId__c);
        }
        
        List<Product_Assignment__c> paLst = paSelector.selectCommercialConditionByProductId(prodIdSet).values();

        for (Product_Assignment__c pa : paLst) {
            currentCommCondSet.add(pa.CommercialConditionId__c);

            if (pa.CommercialConditionId__r.RecordTypeId == CapCommConditionRecType) {
                capPaMap.put(pa.ProductId__c, pa);
            } else {
                if (nonCapPaMap.containsKey(pa.ProductId__c)) {
                    nonCapPaMap.get(pa.ProductId__c).add(pa);
                } else {
                    nonCapPaMap.put(pa.ProductId__c, new List<Product_Assignment__c>{pa});
                }
            }
        }

        List<Commercial_Condition__c> commCondLst = commConditionSelector.selectCommConditionByID(currentCommCondSet);
        Map<Id, Commercial_Condition__c> idToCommCondition = new Map<Id, Commercial_Condition__c>(commCondLst);


        for (Id prodId : prodIdSet) {
            Product_Assignment__c capPa = capPaMap.get(prodId);
            List<Product_Assignment__c> nonCapPaLst = nonCapPaMap.get(prodId);

            if (capPa != null && nonCapPaLst != null) {
                Commercial_Condition__c capCommCond = idToCommCondition.get(capPa.CommercialConditionId__c);
                
                for (Product_Assignment__c pa : nonCapPaLst) {
                    Commercial_Condition__c nonCapCommCond = idToCommCondition.get(pa.CommercialConditionId__c);
                    
                    if (capCommCond != null && nonCapCommCond != null) {
                        nonCapCommCond.Visits_to_CAP__c = capCommCond.Visits_to_CAP__c;
                        nonCapCommCond.CAP_Discount__c = capCommCond.CAP_Discount__c;
                        uow.registerDirty(nonCapCommCond);
                    }
                }
            }
        }
    }

    private void createLog(Exception e, String origin) {
        DebugLog__c log = new DebugLog__c(
            Origin__c = '['+className+']['+origin+']',
            LogType__c = constants.LOGTYPE_ERROR,
            RelatedToSquad__c = constants.LOGTYPE_SQUAD_PARTNERS,
            ExceptionMessage__c = e.getMessage(),
            StackTrace__c = e.getStackTraceString()
        );
        Logger.createLog(log);
    }

    // Holds data for records creation
    private class ObjectsStruct {
        Product_Opportunity_Assignment__c capPoa;
        Product_Assignment__c insertedParentPa;
        Commercial_Condition__c insertedCommCond;
    }
}