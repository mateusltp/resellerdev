/**
 * @description       : 
 * @author            : AlyssonMota
 * @group             : 
 * @last modified on  : 09-29-2020
 * @last modified by  : Alysson Mota
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   09-29-2020   Alysson Mota   Initial Version
**/
@isTest
public with sharing class FastTrackEnablersControllerTest {
    
    @isTest
    public static void testGetEnablers() {
        Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
        Id oppRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();

        Account acc = new Account();
        acc.RecordTypeId = accRecordTypeId;
        acc.Name = 'Test Account';
        acc.BillingCountry = 'Brazil';
        acc.BillingState = 'São Paulo';
        insert acc;

        Opportunity opp = new Opportunity();
        opp.RecordTypeId = oppRecordTypeId;
        opp.AccountId = acc.Id;
        opp.Name = 'Opp Test';
        opp.StageName = 'Qualification';
        opp.CloseDate = System.now().date();
        insert opp;

        Test.startTest();
        FastTrackEnablersController.getEnablers(opp.Id);
        Test.stopTest();
    }
}