/*
* @Author: Bruno Pinho
* @Date: January/2019
* @Description: Test class for AddInviteesToEventsCtrl
*/
@isTest
private class AddInviteesToEventsCtrlTest {
    static void SetUp() {
        test.StartTest();
    }
    
    static void TearDown() {
        test.StopTest();
    }
    
    public static testmethod void Test_triggers() {
        SetUp();

        Event event = new Event(Subject = 'Test Event', EndDateTime = System.now().addDays(2), StartDateTime = System.now().addDays(1));        
        INSERT event;
        
        DELETE event;
        
        TearDown();
    }
    
    public static testmethod void Test_getInvitees() {
        String recordId = '';
        SetUp();
        Event event = new Event(Subject = 'Test Event', EndDateTime = System.now().addDays(2), StartDateTime = System.now().addDays(1));
        INSERT event;
        List<Gympass_Leader_Association__c> listGympassLeaderAssociation = [SELECT Id FROM Gympass_Leader_Association__c WHERE Gympass_Event__r.Event_Id__c =: event.Id];
        
        if (listGympassLeaderAssociation.size() > 0)
            recordId = listGympassLeaderAssociation [0].Id;
            
        System.Assert(AddInviteesToEventsCtrl.getInvitees(recordid) != null);
        TearDown();
    }
    
    public static testmethod void Test_getOwnerId() {
        String recordId = '';
        
        SetUp();
        Event event = new Event(Subject = 'Test Event', EndDateTime = System.now().addDays(2), StartDateTime = System.now().addDays(1));
        INSERT event;
        
        if (event.Id != null)
            recordId = event.Id;
            
        System.Assert(AddInviteesToEventsCtrl.getOwnerId(recordid) == UserInfo.getUserId());
        TearDown();
    }
    
    public static testmethod void Test_deleteEventAssociation() {
        SetUp();
        
        List<Event> listEventsToInsert = new List<Event>();
        List<Gympass_Event__c> listGympassEventsToInsert = new List<Gympass_Event__c>();
        List<Gympass_Leader_Association__c> listGympassLeaderAssociationsToInsert = new List<Gympass_Leader_Association__c>();
        
        for (Integer i=0; i<10; i++) {
            Event event = new Event(Subject = 'Test Event', EndDateTime = System.now().addDays(2), StartDateTime = System.now().addDays(1));
            listEventsToInsert.add(event);
        }
        
        INSERT listEventsToInsert;
        
        for (Event e: listEventsToInsert) {
            Gympass_Event__c gpe = new Gympass_Event__c();
            gpe.Name = e.Subject;
            gpe.Start__c = e.StartDateTime;
            gpe.End__c = e.EndDateTime;
            gpe.Event_Id__c = e.Id;
            listGympassEventsToInsert.add(gpe);
        }
        
        INSERT listGympassEventsToInsert;
        
        for (Gympass_Event__c gpe: listGympassEventsToInsert) {
            Gympass_Leader_Association__c gpla = new Gympass_Leader_Association__c();
            gpla.Gympass_Event__c = gpe.Id;
            gpla.Gympass_Leader__c = UserInfo.getUserId();
            listGympassLeaderAssociationsToInsert.add(gpla);
        }
        
        INSERT listGympassLeaderAssociationsToInsert;
        AddInviteesToEventsCtrl.deleteEventAssociation(listGympassLeaderAssociationsToInsert[0].Id);
        TearDown();
    }
    
    public static testmethod void Test_deleteEventAssociationException() {
        SetUp();
        AddInviteesToEventsCtrl.deleteEventAssociation(null);
        TearDown();
    }
    
    public static testmethod void Test_addInvitee() {
        SetUp();
        Event event = new Event(Subject = 'Test Event', EndDateTime = System.now().addDays(2), StartDateTime = System.now().addDays(1));
        
        INSERT event;
        
        String recordId = event.Id;
        string objectname = '';
        
        List<SObject> users = new List<SObject>();
        List<User> listUsersObject = [SELECT
                                      	Id
                                      FROM
                                      	User
                                      WHERE
                                      	IsActive = TRUE
                                      LIMIT 
                                      	10];
        
        users.addAll(listUsersObject);
        
        string assigneeid = UserInfo.getUserId();
        String assert = AddInviteesToEventsCtrl.addInvitee(objectname, users, recordid, assigneeid);
        TearDown();
    }
    
    public static testmethod void Test_addInviteeException() {
        SetUp();
        
        String assert = AddInviteesToEventsCtrl.addInvitee(null, null, null, null);
        
        TearDown();
    }
    
    public static testmethod void Test_addInviteeWithoutGympassEvent() {
        SetUp();
        Event event = new Event(Subject = 'Test Event', EndDateTime = System.now().addDays(2), StartDateTime = System.now().addDays(1));
        INSERT event;
        String recordId = event.Id;
        List<Gympass_Event__c> listGympassEvent = [SELECT Id FROM Gympass_Event__c WHERE Event_Id__c =: recordId];
        
        if (listGympassEvent.size() > 0)
            DELETE listGympassEvent;
            
        string objectname = '';
        List<SObject> users = new List<SObject>();
        List<User> listUsersObject = [SELECT Id FROM User WHERE IsActive = TRUE LIMIT 10];
        users.addAll(listUsersObject);
        string assigneeid = UserInfo.getUserId();
        AddInviteesToEventsCtrl.addInvitee(objectname, users, recordid, assigneeid);
        TearDown();
    }
    
    public static testmethod void Test_generateGympassEvent() {
        SetUp();
        Event event = new Event();
        string recordid = '';
        System.Assert(AddInviteesToEventsCtrl.generateGympassEvent(event, recordid) != null);
        TearDown();
    }
    
    public static testmethod void Test_generateGympassLeaderAssociation() {
        SetUp();
        List<SObject> listusers = new List<SObject>();
        List<User> listUsersObject = [SELECT Id FROM User WHERE IsActive = TRUE LIMIT 10];
        listusers.addAll(listUsersObject);
        string assigneeid = UserInfo.getUserId();
        System.Assert(AddInviteesToEventsCtrl.generateGympassLeaderAssociation(listusers, assigneeid) != null);
        TearDown();
    }
    
    public static testmethod void Test_updateGympassLeaderAssociation() {
        SetUp();
        List<Gympass_Leader_Association__c> listgympassleaderassociationtoinsert = new List<Gympass_Leader_Association__c>();
        string masterid = '';
        System.Assert(AddInviteesToEventsCtrl.updateGympassLeaderAssociation(listgympassleaderassociationtoinsert, masterid) != null);
        TearDown();
    }
    
    public static testmethod void Test_handleGympassLeaderAssociation() {
        SetUp();
        List<Gympass_Leader_Association__c> listgympassleaderassociation = new List<Gympass_Leader_Association__c>();
        List<sObject> listusers = new List<sObject>();
        List<User> listUsersObject = [SELECT Id FROM User WHERE IsActive = TRUE LIMIT 10];
        listusers.addAll(listUsersObject);
        string assigneeid = UserInfo.getUserId();
        string mastergympasseventid = '';
        System.Assert(AddInviteesToEventsCtrl.handleGympassLeaderAssociation(listgympassleaderassociation, listusers, assigneeid, mastergympasseventid) != null);
        TearDown();
    }
}