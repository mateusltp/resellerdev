public class SubManagedClausesTriggerHandler extends TriggerHandler {
    public override void beforeInsert() {
        new SubManagedClausesTriggerHelper().setLengthOfRichTextField(Trigger.new);  
    }
    public override void beforeUpdate(){ 
        new SubManagedClausesTriggerHelper().setLengthOfRichTextField(Trigger.new);  
    }
}