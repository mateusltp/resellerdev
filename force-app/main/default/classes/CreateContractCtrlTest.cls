/**
 * @description       : 
 * @author            : ext.gft.marcus.silva@gympass.com
 * @group             : 
 * @last modified on  : 03-09-2022
 * @last modified by  : ext.gft.marcus.silva@gympass.com
**/
@isTest
public with sharing class CreateContractCtrlTest {
    @TestSetup    
    static void setup() {
        Pricebook2 lStandardPricebook = DataFactory.newPricebook();
        Database.update( lStandardPricebook );
        
        Account lAcc = DataFactory.newAccount();
        Database.insert(lAcc);
        
        Opportunity oppNewBusiness = DataFactory.newOpportunity( lAcc.Id, lStandardPricebook, 'Client_Sales_New_Business',true );  
        
        oppNewBusiness.fasttrackstage__c='Qualification';                
        Database.insert(oppNewBusiness);       
        
        Quote oldQuote = DataFactory.newQuote(oppNewBusiness, '1', 'Gympass_Plus');
        oldQuote.Employee_Corporate_Email__c =false;
        oldQuote.end_date__c = Date.today();
        oldQuote.contact_permission__c = 'Sign-up only';
        oldQuote.ExpirationDate = date.valueOf('2021-08-16');
        
        oldQuote.autonomous_marketplace_contract__c = 'Yes';
        oldQuote.Discount_Approval_Level__c = 4;
        oldQuote.Free_Trial_Days__c = 7;                
        Database.insert(oldQuote);
        
        oppNewBusiness.Name = 'New Business';
        oppNewBusiness.SyncedQuoteId = oldQuote.Id;
        oppNewBusiness.StageName = 'Lançado/Ganho';
        oppNewBusiness.FastTrackStage__c = 'Offer Approved';                
        Database.update(oppNewBusiness);                                
    }

    @isTest
    public static void createRecords(){
        Test.startTest();
        Opportunity opp = [SELECT Id FROM Opportunity];
        String qt = [SELECT Id FROM Quote].Id;

        Case legalCase = new Case();
        legalCase.OpportunityId__c = opp.Id;
        legalCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('RTContractReview').getRecordTypeId();
        insert legalCase;

        APXT_Redlining__Contract_Agreement__c contract = new APXT_Redlining__Contract_Agreement__c();
        contract.Opportunity__c = opp.Id;
        contract.RecordTypeId = Schema.SObjectType.APXT_Redlining__Contract_Agreement__c.getRecordTypeInfosByDeveloperName().get('B2B_Contract').getRecordTypeId();
        insert contract;

        List<APXT_Redlining__Contract_Agreement__c> lstContract = CreateContractController.getContractAgreement(opp.Id);
        List<Case> lstCase = CreateContractController.getCase(opp.Id);
        String proposalId = CreateContractController.getProposalId(opp.Id);
        Test.stopTest();

        System.assertEquals( 1, lstCase.size() );
        System.assertEquals( 1, lstContract.size());
        System.assertEquals( qt, proposalId );
    }
}