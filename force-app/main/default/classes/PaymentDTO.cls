public without sharing class PaymentDTO {
  private String id;
  private String salesforce_id;
  private String client_id;
  private Decimal split_bill_percentage;
  private List<WaiverDTO> waivers;
  private List<EligibilityDTO> eligibles;

  public PaymentDTO(Payment__c payment) {
    this.id = payment.UUID__c != null ? payment.UUID__c : new Uuid().getValue();
    this.salesforce_id = payment.Id;
    this.split_bill_percentage = payment.Percentage__c == null
      ? 100
      : payment.Percentage__c;
    this.client_id = payment.Account__r.UUID__c;

    this.waivers = new List<WaiverDTO>();

    for (Waiver__c waiver : payment.Waivers__r) {
      this.waivers.add(new WaiverDTO(waiver));
    }

    this.eligibles = new List<EligibilityDTO>();

    for (Eligibility__c eligibility : payment.Eligibility__r) {
      this.eligibles.add(new EligibilityDTO(eligibility));
    }
  }

  public String getId() {
    return this.id;
  }

  public String getSalesforceId() {
    return this.salesforce_id;
  }

  public List<WaiverDTO> getWaivers() {
    if (this.waivers == null) {
      return new List<WaiverDTO>();
    }
    return this.waivers;
  }

  public List<EligibilityDTO> getEligibilities() {
    if (this.eligibles == null) {
      return new List<EligibilityDTO>();
    }
    return this.eligibles;
  }

  public Payment__c parseToSPayment() {
    Account account = new Account(UUID__c = this.client_id);

    return new Payment__c(
      UUID__c = this.id,
      Percentage__c = this.split_bill_percentage,
      Account__r = account
    );
  }
}