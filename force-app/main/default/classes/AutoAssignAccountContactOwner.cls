public without sharing class AutoAssignAccountContactOwner {
	
    @InvocableMethod
    public static void assignAccountContactOwner(List<Id> listAccountIds) {
        System.debug('assignAccountContactOwner');
   		Map<Id, List<Contact>> accountIdToContactList = new Map<Id, List<Contact>>();
        List<Contact> contactsToUpdate = new List<Contact>();
        
        Map<Id, Account> idToAccountMap = new Map<Id, Account>([
        	SELECT Id, Name, BDR__c, OwnerId
            FROM Account
            WHERE Id IN :listAccountIds
        ]);
        
        List<Contact> contactList = [
            SELECT Id, AccountId, OwnerId
            FROM Contact
            WHERE AccountId IN :listAccountIds
            AND Email != NULL
        ];
        
        for (Contact c : contactList) {
            if (accountIdToContactList.containsKey(c.AccountId)) {
            	accountIdToContactList.get(c.AccountId).add(c);
            } else {
                accountIdToContactList.put(c.AccountId, new List<Contact>{c});
            }
        }
        
        for (Id accountId : idToAccountMap.keySet()) {
            Account a = idToAccountMap.get(accountId);
            List<Contact> contacts = accountIdToContactList.get(accountId);
            
            if (contacts != null) {
                for (Contact c : contacts) {
                    if (a.BDR__c != null) {
                        c.OwnerId = a.BDR__c;
                    } else {
                        c.OwnerId = a.OwnerId;
                    }
                    
                    contactsToUpdate.add(c);
                }
            }
        }
        
        List<Database.SaveResult> saveResultList = Database.update(contactsToUpdate, false);
        
        for (Database.SaveResult sr : saveResultList) {
            if (!sr.isSuccess()) {
                for(Database.Error err : sr.getErrors()) {
            		System.debug('The following error has occurred for o sObject with id ' + sr.getId() + ':');                    
            		System.debug(err.getStatusCode() + ': ' + err.getMessage());
        		}
            }
        }
   	}
}