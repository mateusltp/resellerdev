@isTest
public class SmbSkuWaiversControllerTest {
    
    @TestSetup
    static void makeData(){
        
        Account gympassEntity = AccountMock.getGympassEntity();
        gympassEntity.CurrencyIsoCode = 'BRL';
        insert gympassEntity;
        
        Account account = AccountMock.getStandard('Empresas');
        account.CurrencyIsoCode = 'BRL';
        account.UUID__c = SelfCheckoutRequestMock.ACCOUNT_UUID;
        
        insert account;
        
        Contact contact = ContactMock.getStandard(account);
        
        insert contact;
        
        account.Attention__c = contact.Id;
        
        update account;
        
        Pricebook2 pricebook = new Pricebook2(
            CurrencyIsoCode = 'BRL',
            Id = Test.getStandardPricebookId(),
            IsActive = true
        );
        
        update pricebook;
        
        List<Product2> products = new List<Product2>();
        
        Product2 setupFee = ProductMock.getSetupFee();
        setupFee.Minimum_Number_of_Employees__c = 0;
        setupFee.Maximum_Number_of_Employees__c = 10000;
        setupFee.IsActive = true;
        setupFee.Family = 'Enterprise Subscription';
        setupFee.Copay2__c = false;
        setupFee.Family_Member_Included__c = false;
        products.add(setupFee);
        
        Product2 accessFee = ProductMock.getAccessFee();
        accessFee.Minimum_Number_of_Employees__c = 0;
        accessFee.Maximum_Number_of_Employees__c = 10000;
        accessFee.IsActive = true;
        accessFee.Family = 'Enterprise Subscription';
        accessFee.Copay2__c = false;
        accessFee.Family_Member_Included__c = false;
        products.add(accessFee);
        
        insert products;
        
        
        List<PricebookEntry> pricebookEntries = new List<PricebookEntry>();
        
        PricebookEntry setupFeePricebookEntry = PricebookMock.getPricebookEntry(
            pricebook,
            setupFee
        );
        setupFeePricebookEntry.UnitPrice = 10;
        pricebookEntries.add(setupFeePricebookEntry);
        
        PricebookEntry accessFeePricebookEntry = PricebookMock.getPricebookEntry(
            pricebook,
            accessFee
        );
        accessFeePricebookEntry.UnitPrice = 10;
        pricebookEntries.add(accessFeePricebookEntry);
        
        insert pricebookEntries;
        
        Opportunity opp = OpportunityMock.getNewBusiness(account, pricebook);
        opp.CurrencyIsoCode = 'BRL';
        opp.Current_Billing_Day__c = 15;
        opp.Current_Order_End_Date__c = Date.today().addMonths(3);
        opp.Quantity_Offer_Number_of_Employees__c = 50;
        
        insert opp;
        
        Quote qt = QuoteMock.getStandard(opp);
        insert qt;
        
        opp.SyncedQuoteId = qt.Id;
        update opp;
        
        QuoteLineItem qtItemSetup = QuoteLineItemMock.getSetupFee(qt,setupFeePricebookEntry);
        QuoteLineItem qtItemAccess = QuoteLineItemMock.getSetupFee(qt,accessFeePricebookEntry);
        insert new List<QuoteLineItem> { qtItemSetup, qtItemAccess };
            
        RecordType rt = [SELECT Id, Name FROM RecordType WHERE SobjectType='Waiver__c' AND Name = 'Months after Launch' LIMIT 1];
        Date today = Date.today();
        List<Waiver__c> waivers = new List<Waiver__c>();
        Waiver__c waiver1 = WaiverMock.getStandard(qtItemAccess);
        waiver1.RecordTypeId = rt.Id;
        waiver1.position__c = 1;
        waiver1.End_Date__c = Date.newInstance(today.year(), today.month(), 15);
        Waiver__c waiver2 = WaiverMock.getStandard(qtItemAccess);
        waiver2.RecordTypeId = rt.Id;
        waiver2.position__c = 2;
        waiver2.End_Date__c = waiver1.End_Date__c.addMonths(1);
        waivers.add(waiver1);
        waivers.add(waiver2);
        insert waivers;      
            
  	}

    @isTest
    static void getDataTest() {
        Test.startTest();        
        QuoteLineItem qtItem = [SELECT Id FROM QuoteLineItem WHERE Product2.Name = 'Enterprise Subscription' LIMIT 1];
        SmbSkuWaiversController.DataWrapper data = SmbSkuWaiversController.getData(qtItem.Id);
        System.assertEquals(2, data.waivers.size(), 'The number of waivers retrieved is not correct.');
        Test.stopTest();
    }
    
    @isTest
    static void getDataExceptionTest() {        
        try {
            Test.startTest();
            SmbSkuWaiversController.getData(null);
            System.assert(false, 'This assert must never be executed. An exception must be thrown before this assert.');
            Test.stopTest();
        }
        catch(Exception e) {
            System.assertEquals('System.AuraHandledException', e.getTypeName(), 'The exception thrown is not the expected one');
        }
    }
    
}