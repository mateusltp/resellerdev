/**
 * @description       : 
 * @author            : gft.jorge.stevaux@ext.gympass.com
 * @group             : 
 * @last modified on  : 08-11-2021
 * @last modified by  : gft.jorge.stevaux@ext.gympass.com
**/
@isTest(seeAllData=false)
private class OrgWideEmailAddressSelectorTest {

     
    @isTest
    static void getSObjectFieldList_Test(){

        List<Schema.SObjectField> schSObjField = new List<Schema.SObjectField> {
			OrgWideEmailAddress.Id
		};

    Test.startTest();
    System.assertEquals(schSObjField, new OrgWideEmailAddressSelector().getSObjectFieldList());
    Test.stopTest();

    }

    @isTest
    static void getSObjectType_Test(){

    Test.startTest();
    System.assertEquals(OrgWideEmailAddress.sObjectType, new OrgWideEmailAddressSelector().getSObjectType());
    Test.stopTest();

    }

    @isTest
    static void selectById_Test(){

        List<OrgWideEmailAddress> orgWideLst = [SELECT Id FROM OrgWideEmailAddress LIMIT 1];
        Set<Id> orgWideIds = (new Map<Id, OrgWideEmailAddress>(orgWideLst)).keySet();

    Test.startTest();
    System.assertEquals(orgWideLst.get(0).id, new OrgWideEmailAddressSelector().selectById(orgWideIds).get(0).id);
    Test.stopTest();

    }

    @isTest
    static void selectGlobalB2BOrgWideEmailAddress_Test(){

    OrgWideEmailAddress orgWideLst = [SELECT Id FROM OrgWideEmailAddress WHERE Address = 'global.b2b@gympass.com' LIMIT 1];

    Test.startTest();
    System.assertEquals(orgWideLst, new OrgWideEmailAddressSelector().selectGlobalB2BOrgWideEmailAddress());
    Test.stopTest();

    }
    
}