/**
 * @description       : 
 * @author            : roei@gft.com
 * @group             : 
 * @last modified on  : 01-29-2021
 * @last modified by  : roei@gft.com
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   01-28-2021   roei@gft.com   Initial Version
**/
public without sharing class AssociateCampaignNewOpp {
    private Id gSalesNewBusinessRt = 
        Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
    private Id gContDirectChannelRt = 
        Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Direct_Channel').getRecordTypeId();

    public void assignOppToCampaign(){
        List< Opportunity > lLstSalesNewBusinessOpp = new List< Opportunity >();

        for( Opportunity iOpp : (List< Opportunity >) Trigger.new ){
            if( iOpp.RecordTypeId == gSalesNewBusinessRt ){
                lLstSalesNewBusinessOpp.add( iOpp );
            }   
        }

        assignOppNewBusinessToCampaign( lLstSalesNewBusinessOpp );
    }

    private void assignOppNewBusinessToCampaign( List< Opportunity > aLstSalesNewBusinessOpp ){
        if( aLstSalesNewBusinessOpp.isEmpty() ){ return; }
        
        List< Id > lLstAccountId = new List< Id >();

        for( Opportunity iOpp : aLstSalesNewBusinessOpp ){
            lLstAccountId.add( iOpp.AccountId );
        }

        Map< Id , Id > lLstContactIdAccId = new Map< Id , Id >();

        for( Account iAcc : [ SELECT Id, 
                            ( SELECT Id FROM Contacts WHERE RecordTypeId =: gContDirectChannelRt ORDER BY CreatedDate DESC LIMIT 1 ) 
                              FROM Account WHERE Id =: lLstAccountId ] ){
            if( iAcc.Contacts.isEmpty() ){ continue; }
            
            lLstContactIdAccId.put( iAcc.Contacts[0].Id , iAcc.Id );
        }

        if( lLstContactIdAccId.isEmpty() ){ return; }

        Map< Id , Id > lLstAccIdCampaignId = new Map< Id , Id >();

        for( CampaignMember iCampMem : [ SELECT CampaignId, ContactId FROM CampaignMember WHERE ContactId =: lLstContactIdAccId.keySet() ] ){
            lLstAccIdCampaignId.put( lLstContactIdAccId.get( iCampMem.ContactId ) , iCampMem.CampaignId );
        }

        if( lLstAccIdCampaignId.isEmpty() ){ return; }
        Id lCampaignId;

        for( Opportunity iOpp : aLstSalesNewBusinessOpp ){
            lCampaignId = lLstAccIdCampaignId.get( iOpp.AccountId );

            if( lCampaignId == null ){ continue; }

            iOpp.CampaignId = lCampaignId;
        }
    }
}