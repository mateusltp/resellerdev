public without sharing class ResellerGetValuesFromOppController {
    
    @AuraEnabled
    public static ResellerPriceResponse getPriceValues(Id oppId){
        ResellerPriceResponse response = new ResellerPriceResponse();
        try{
            response = ResellerPriceService.getPriceValues(oppId);
        }catch(Exception e){
            System.debug(e.getStackTraceString());
        }
        return response;
    }
    
}