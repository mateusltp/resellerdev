/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 05-13-2022
 * @last modified by  : gft.samuel.silva@ext.gympass.com
 * Modifications Log 
 * Ver   Date         Author                             Modification
 * 1.0   07-02-2021   gft.samuel.silva@ext.gympass.com   Initial Version
**/

public with sharing class OpportunityService {

  private static final PS_Constants constants = PS_Constants.getInstance();
  private static final String className = OpportunityService.class.getName();
    
  public static List<Opportunity> openRenegotiation (Map<Id, Opportunity> mAccountOpp, String recordTypeDevName){

    try{
      return service(recordTypeDevName).openRenegotiation(mAccountOpp);
    }catch (Exception e){
      if(e.getMessage().contains(constants.ERROR_NO_IMPLEMENTATION_FOR_RT)){
        System.debug(constants.ERROR_NO_IMPLEMENTATION_FOR_RT + ': ' + recordTypeDevName);
        return new List<Opportunity>();
      } else {
        createLog(e, 'openRenegotiation', JSON.serialize(mAccountOpp), null);
        throw new OpportunityServiceException(e.getMessage() + ' - ' + e.getLineNumber());
      }
    }
    
  }

  public static List<Opportunity> updateStageToLost(List<Opportunity> oppScopeLst){
    try{
      String recordTypeDevName = Schema.SObjectType.Opportunity.getRecordTypeInfosById().get(oppScopeLst.get(0).recordTypeId).getDeveloperName();
      return service(recordTypeDevName).updateStageToLost(oppScopeLst);
    }catch (Exception e){
      if(e.getMessage().contains(constants.ERROR_NO_IMPLEMENTATION_FOR_RT)){
        System.debug(constants.ERROR_NO_IMPLEMENTATION_FOR_RT);
        return new List<Opportunity>();
      } else {
        createLog(e, 'updateStageToLost', JSON.serialize(oppScopeLst), null);
        throw new OpportunityServiceException(e.getMessage() + ' - ' + e.getLineNumber());
      }
    }
  }

  public static List<Opportunity> NotifyOpportunityCancellation(List<Opportunity> aOppList){
    String recordTypeDevName = Schema.SObjectType.Opportunity.getRecordTypeInfosById().get(aOppList.get(0).recordTypeId).getDeveloperName();
    try{
      return service(recordTypeDevName).NotifyOpportunityCancellation(aOppList);
    }catch (Exception e){
      System.debug('::e.getStackTraceString()');
      System.debug(e.getStackTraceString());
      if(e.getMessage().contains(constants.ERROR_NO_IMPLEMENTATION_FOR_RT)){
        System.debug(constants.ERROR_NO_IMPLEMENTATION_FOR_RT + ': ' + recordTypeDevName);
        return new List<Opportunity>();
      } else {
        createLog(e, 'NotifyOpportunityCancellation', JSON.serialize(aOppList), null);
        throw new OpportunityServiceException(e.getMessage() + ' - ' + e.getLineNumber());
      }
    }
  }

  /*public static void validateOpportunityForTagus(List<Opportunity> newOppLst, Map<Id, Opportunity> oldOppMap){
    List<Opportunity> oppLst = new List<Opportunity>(); 
    String recordTypeDevName = Schema.SObjectType.Opportunity.getRecordTypeInfosById().get(newOppLst.get(0).recordTypeId).getDeveloperName();
   
    for( Opportunity iOpp : newOppLst ){
      if(iOpp.StageName.equals(constants.OPPORTUNITY_STAGE_LANCADO_GANHO) && !oldOppMap.get(iOpp.Id).StageName.equals(constants.OPPORTUNITY_STAGE_LANCADO_GANHO) ){
        oppLst.add(iOpp);
      }
    }
    if(oppLst.isEmpty()) return;
    try{     
      service(recordTypeDevName).validateOpportunityForTagus(oppLst);
    }catch (Exception e){
      DebugLog__c log = new DebugLog__c(
              Origin__c = '['+className+'][validateOpportunityForTagus]',
              LogType__c = constants.LOGTYPE_ERROR,
              RelatedToSquad__c = constants.LOGTYPE_SQUAD_PARTNERS,
              ExceptionMessage__c = e.getMessage(),
              StackTrace__c = e.getStackTraceString(),
              DataScope__c = JSON.serialize(newOppLst)
      );
      Logger.createLog(log);
      System.debug('e.getStackTraceString() ' + e.getStackTraceString());
      if(e.getMessage().contains(constants.ERROR_NO_IMPLEMENTATION_FOR_RT)){
        System.debug(constants.ERROR_NO_IMPLEMENTATION_FOR_RT + ': ' + recordTypeDevName);
      } else {
          throw new OpportunityServiceException(e.getMessage());  
      }
    }
  }*/

 /* public static void setAccountTypePartnerOnOppWon(List<Opportunity> newOppLst, Map<Id, Opportunity> oldOppMap)
  {
    List<Opportunity> oppLst = new List<Opportunity>();
    String recordTypeDevName = Schema.SObjectType.Opportunity.getRecordTypeInfosById().get(newOppLst.get(0).recordTypeId).getDeveloperName();
    Set<String> scopeRecordTypeNames = new Set<String>{constants.OPPORTUNITY_RT_GYMS_SMP, constants.OPPORTUNITY_RT_GYMS_WISHLIST};
    for (Opportunity iOpp : newOppLst)
    {
      if (iOpp.StageName.equals(constants.OPPORTUNITY_STAGE_LANCADO_GANHO) && !oldOppMap.get(iOpp.Id).StageName.equals(constants.OPPORTUNITY_STAGE_LANCADO_GANHO) && scopeRecordTypeNames.contains(recordTypeDevName))
      {
        oppLst.add(iOpp);
      }
    }
    if (oppLst.isEmpty()) return;
    try{
      service(recordTypeDevName).setAccountTypePartnerOnOppWon(oppLst);
    }catch (Exception e){
      DebugLog__c log = new DebugLog__c(
              Origin__c = '['+className+'][setAccountTypePartnerOnOppWon]',
              LogType__c = constants.LOGTYPE_ERROR,
              RelatedToSquad__c = constants.LOGTYPE_SQUAD_PARTNERS,
              ExceptionMessage__c = e.getMessage(),
              StackTrace__c = e.getStackTraceString(),
              DataScope__c = JSON.serialize(oppLst)
      );
      Logger.createLog(log);
      if(e.getMessage().contains(constants.ERROR_NO_IMPLEMENTATION_FOR_RT)){
        System.debug(constants.ERROR_NO_IMPLEMENTATION_FOR_RT + ': ' + recordTypeDevName);
      } else {
        throw new OpportunityServiceException(e.getMessage());
      }
    }
  }
 /*
/**
 * @param recordTypeDevName should be RecordTypeApiName + sObjectApiName . Example: Partner_Flow_Account (Partner_Flow = RecordTypeApiName)
 */
  private static IOpportunityService service(String recordTypeDevName) {	
    try{
      return (IOpportunityService) Application.ServiceByRecordType.newInstanceByRecordType(getRecordTypeAndSObjectName(recordTypeDevName));
    }catch (Exception e){
        throw new OpportunityServiceException(e.getMessage() + ' - ' + e.getLineNumber());
    }
   
	}

  
  public static List<Opportunity> validateW9Form(List<Opportunity> aOppList, Map< Id , Opportunity > lMapOld){
    String recordTypeDevName = Schema.SObjectType.Opportunity.getRecordTypeInfosById().get(aOppList.get(0).recordTypeId).getDeveloperName();
    try{
      return service(recordTypeDevName).validateW9Form(aOppList,lMapOld);
    }catch (Exception e){
      if(e.getMessage().contains(constants.ERROR_NO_IMPLEMENTATION_FOR_RT)){
        System.debug(constants.ERROR_NO_IMPLEMENTATION_FOR_RT + ': ' + recordTypeDevName);
        return new List<Opportunity>();
      } else {
        createLog(e, 'validateW9Form', JSON.serialize(aOppList), null);
        throw new OpportunityServiceException(e.getMessage() + ' - ' + e.getLineNumber());
      }
    }
  }

  public static List<Opportunity> validate5Photos(List<Opportunity> aOppList, Map< Id , Opportunity > lMapOld){
    String recordTypeDevName = Schema.SObjectType.Opportunity.getRecordTypeInfosById().get(aOppList.get(0).recordTypeId).getDeveloperName();
    try{
      return service(recordTypeDevName).validate5Photos(aOppList,lMapOld);
    }catch (Exception e){
      if(e.getMessage().contains(constants.ERROR_NO_IMPLEMENTATION_FOR_RT)){
        System.debug(constants.ERROR_NO_IMPLEMENTATION_FOR_RT + ': ' + recordTypeDevName);
        return new List<Opportunity>();
      } else {
        createLog(e, 'validate5Photos', JSON.serialize(aOppList), null);
        throw new OpportunityServiceException(e.getMessage() + ' - ' + e.getLineNumber());
      }
    }
  }

  public static Opportunity createChildOpportunity (String childOppName, Date closeDate, String ownerId, String accountId, String masterOpportunityId, List<Id> accountInOppIds, String recordTypeDevName){
    try{
      return service(recordTypeDevName).createChildOpportunity(childOppName, closeDate, ownerId, accountId, masterOpportunityId, accountInOppIds );
    }catch (Exception e){
      if(e.getMessage().contains(constants.ERROR_NO_IMPLEMENTATION_FOR_RT)){
        System.debug(constants.ERROR_NO_IMPLEMENTATION_FOR_RT + ': ' + recordTypeDevName);
        return new Opportunity();
      } else {
        createLog(e, 'createChildOpportunity', JSON.serialize(childOppName), null);
        throw new OpportunityServiceException(e.getMessage() + ' - ' + e.getLineNumber());
      }
    }
  }

  public static void validateOpportunityStage(Map<Id, Opportunity> opportunitiesByIds, Map<Id, Opportunity> oppOldMap) {
    String recordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Opportunity').getRecordTypeId();
    String recordTypeChildId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Child_Opportunity').getRecordTypeId();
    Map<Id, Opportunity> scopeOpps = new Map<Id, Opportunity>();
    for (Opportunity opportunity : opportunitiesByIds.values()) {
      if (opportunity.StageName != oppOldMap.get(opportunity.Id).StageName && (recordTypeId == opportunity.RecordTypeId || recordTypeChildId == opportunity.RecordTypeId )) {
        scopeOpps.put(opportunity.Id, opportunity);
      }
    }
    if(!scopeOpps.isEmpty()) {
      String recordTypeDevName = Schema.SObjectType.Opportunity.getRecordTypeInfosById().get(opportunitiesByIds.values().get(0).recordTypeId).getDeveloperName();
      try{
        service(recordTypeDevName).validateOpportunityStage(scopeOpps);
      }catch (Exception e){
        if(e.getMessage().contains(constants.ERROR_NO_IMPLEMENTATION_FOR_RT)){
          System.debug(constants.ERROR_NO_IMPLEMENTATION_FOR_RT + ': ' + recordTypeDevName);
        } else {
          createLog(e, 'validateOpportunityStage', JSON.serialize(scopeOpps), null);
          throw new OpportunityServiceException(e.getMessage() + ' - ' + e.getLineNumber());
        }
      }
    }
  }

  private static String getRecordTypeAndSObjectName (String recordTypeDevName){
      return constants.OPPORTUNITY_OBJECT+'.'+recordTypeDevName;
  }
  
  @TestVisible
  private static void createLog(Exception e, String methodName, String dataScope, Id objectId) {
      DebugLog__c log = new DebugLog__c(
          Origin__c = '['+className+']['+methodName+']',
          LogType__c = constants.LOGTYPE_ERROR,
          RelatedToSquad__c = constants.LOGTYPE_SQUAD_PARTNERS,
          ExceptionMessage__c = e.getMessage(),
          StackTrace__c = e.getStackTraceString(),
          DataScope__c = dataScope, // test to verify how the uow would look like at this point
          ObjectId__c = objectId
      );
      Logger.createLog(log);
  }

	public class OpportunityServiceException extends Exception {} 	

}