/**
 * @File Name          : AccountHierarchy.cls
 * @Description        : 
 * @Author             : Samuel Silva - GFT (slml@gft.com)
 * @Group              : 
 * @Last Modified By   : Samuel Silva - GFT (slml@gft.com)
 * @Last Modified On   : 22/04/2020 18:08:04
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    13/04/2020   Samuel Silva - GFT (slml@gft.com)     Initial Version
**/
public without sharing class AccountHierarchy {
    
    private Set<Id> totalAccounts = new Set<Id>();    
    
    public void findlAllChildAccounts(Set<ID> accountToVerify){
        Set<Id> newIterationIds = new Set<ID>(new Map<Id,Account>([SELECT Id FROM Account WHERE ParentId IN : accountToVerify]).keySet());
        if(newIterationIds.size()>0) {
            totalAccounts.addAll(newIterationIds);
            findlAllChildAccounts(newIterationIds);
        }    
    }

    public Set<Id> getTotalAccounts(){
        return this.totalAccounts;
    }

    public void updateNumberOfLocations(Account acc, Integer numberOfLocations){
        acc.Number_of_Locations__c = numberOfLocations;
        UPDATE acc;
    }

}