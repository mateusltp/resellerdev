public without sharing class AccountDTO {
  private String id;
  private String trade_name;
  private String phone;
  private String parent_id;
  private String legal_name;
  private String legal_document_type;
  private String legal_document;
  private String currency_id;
  private List<ContactDTO> contacts;
  private String business_unit;
  private String billing_email;
  private Boolean is_government_company;
  private AccountAddressesDTO addresses;

  public List<ContactDTO> getContacts() {
    return this.contacts;
  }

  public List<String> getMissingRequiredFields() {
    List<String> missingRequiredFields = new List<String>();

    if (String.isBlank(this.id)) {
      missingRequiredFields.add('id');
    }

    if (String.isBlank(this.trade_name)) {
      missingRequiredFields.add('trade_name');
    }

    if (String.isBlank(this.currency_id)) {
      missingRequiredFields.add('currency_id');
    }

    if (this.contacts == null || this.contacts.isEmpty()) {
      missingRequiredFields.add('contacts');
    }

    if (this.addresses == null) {
      missingRequiredFields.add('addresses');
    }

    return missingRequiredFields;
  }

  public Account parseToSAccount() {
    Contact attention = String.isBlank(this?.addresses?.billing.attention)
      ? null
      : new Contact(UUID__c = this.addresses.billing.attention);

    return new Account(
      UUID__c = this.id,
      Name = this.trade_name,
      Phone = this.phone,
      ParentId = this.parent_id,
      Razao_Social__c = this.legal_name,
      Legal_Document_Type__c = this.legal_document_type,
      Id_Company__c = this.legal_document,
      CurrencyIsoCode = this.currency_id,
      Business_Unit__c = this.business_unit,
      Email__c = this.billing_email,
      BillingStreet = this?.addresses?.billing.street,
      BillingCity = this?.addresses?.billing.city,
      BillingCountryCode = this?.addresses?.billing.country_id,
      BillingPostalCode = this?.addresses?.billing.postal_code,
      BillingState = this?.addresses?.billing.state,
      Attention__r = attention,
      Send_To_Tagus__c = true
    );
  }

  public class ContactDTO {
    private String id;
    private List<String> portal_roles;
    private String phone;
    private String first_name;
    private String last_name;
    private String email;
    private AddressDTO address;

    public List<String> getMissingRequiredFields() {
      List<String> missingRequiredFields = new List<String>();

      if (String.isBlank(this.id)) {
        missingRequiredFields.add('id');
      }

      if (String.isBlank(this.first_name)) {
        missingRequiredFields.add('first_name');
      }

      if (String.isBlank(this.last_name)) {
        missingRequiredFields.add('last_name');
      }

      if (String.isBlank(this.email)) {
        missingRequiredFields.add('email');
      }

      return missingRequiredFields;
    }

    public Contact parseToSContact() {
      return new Contact(
        UUID__c = this.id,
        Phone = this.phone,
        Role__c = parseContactRoles(),
        FirstName = this.first_name,
        LastName = this.last_name,
        Email = this.email,
        MailingStreet = this?.address?.street,
        MailingCity = this?.address?.city,
        MailingCountryCode = this?.address?.country_id,
        MailingPostalCode = this?.address?.postal_code,
        MailingState = this?.address?.state
      );
    }

    private String parseContactRoles() {
      String contactRoles = '';

      if (this.portal_roles == null) {
        return null;
      }

      for(String role : this.portal_roles) {
          contactRoles += role + ';';
      }

      return contactRoles;
    }
  }

  private class AccountAddressesDTO {
    private AccountAddressDTO billing;
    private AccountAddressDTO mailing;
  }

  private class AccountAddressDTO extends AddressDTO {
    private String attention;
  }

  private virtual class AddressDTO {
    private String street;
    private String city;
    private String country_id;
    private String postal_code;
    private String state;
  }
}