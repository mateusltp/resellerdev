public with sharing class FastTrackOfferCreationController {
    public FastTrackOfferCreationController() {

    }

    @AuraEnabled
    public static List<Account> findGympassEntities(){
        AccountRepository accounts = new AccountRepository();
        return accounts.gympassEntities();
    }

    @AuraEnabled
    public static List<Contact> findClientManagers(String recordId){
        OpportunityRepository opportunities = new OpportunityRepository();
        ContactRepository contacts = new ContactRepository();
        SObject opportunity =  opportunities.byId(recordId);
        return contacts.fromAccount( (String) opportunity.get('AccountId') );
    }

    @AuraEnabled
    public static List<String> findCompetitors() {    
        List<String> allOpts = new List<String>();        
        Schema.sObjectType objType = Opportunity.getSObjectType();
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        Map<String,Schema.SObjectField> fieldMap = objDescribe.fields.getMap();        
        list < Schema.PicklistEntry > values = fieldMap.get('Competitor__c').getDescribe().getPickListValues();        
        for (Schema.PicklistEntry a: values) 
            allOpts.add(a.getValue());        
        allOpts.sort();
        return allOpts;
    }
}