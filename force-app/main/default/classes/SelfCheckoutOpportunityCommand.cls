public without sharing class SelfCheckoutOpportunityCommand extends AbstractCommand {

    override
    public void execute(){

        SelfCheckoutOpportunityRequest selfCheckoutOpportunityRequest = (SelfCheckoutOpportunityRequest)
                                                     event.getPayloadFromJson(SelfCheckoutOpportunityRequest.class);
        
        new SelfCheckoutOpportunityService(selfCheckoutOpportunityRequest).execute();
    }
    
}