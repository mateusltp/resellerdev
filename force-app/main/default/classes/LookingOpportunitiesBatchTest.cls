@isTest
public class LookingOpportunitiesBatchTest {

    
    @isTest
    public static void LookingOpportunitiesBatchTest() {
        
         Massive_Account_Request__c massReq = new Massive_Account_Request__c(
            name='test_loadMassiveAccountRequest',
            Status__c=ResellerEnumRepository.MassiveAccountRequestStatus.LookingForOpportunities.name()   
        );
  
        insert massReq;
        
        Account account = new Account();
        account.Name = 'Account';
        account.BillingCountry = 'Brazil';
        account.BillingState = 'São Paulo';
        account.Id_Company__c = '82.898.430/0001-37';
        insert account;
        
        Account_Request__c request = new Account_Request__c();  
        request.Name = 'teste7';
        request.Unique_Identifier_Type__c = 'CNPJ';
        request.Unique_Identifier__c = '73054510000115';
        request.Billing_Country__c = 'Brazil';
        request.Massive_Account_Request__c = massReq.Id;
        request.AccountId__c = account.Id;
        request.Bulk_Operation__c = true;
        request.Massive_Status__c = ResellerEnumRepository.MassiveAccountRequestStatus.LookingForOpportunities.name();
        insert request;
        
        
        Test.startTest();
        	 LookingOpportunitiesBatch bt = new LookingOpportunitiesBatch();
             bt.massiveId = massReq.Id;
             ID idOpp = DataBase.executeBatch(bt);
        Test.stopTest();
        
        System.assertEquals(massReq.Status__c, 'LookingForOpportunities');
        
        //Database.executeBatch(new LookingOpportunitiesBatch());
        //Account accountAssert = [ SELECT Id, Name FROM Account WHERE BillingCountry = 'Brazil' limit 1];
        //System.assertEquals(accountAssert.id, 'Encaminhado');
        //System.assertEquals(accountAssert.name, 'AccountUpdated From Batch');
        
    }
    
}