/**
 * @description       : 
 * @author            : Samuel Silva - GFT (slml@gft.com)
 * @group             : 
 * @last modified on  : 07-02-2021
 * @last modified by  : Alysson Mota
 * Modifications Log 
 * Ver   Date         Author                              Modification
 * 1.0   11-27-2020   Samuel Silva - GFT (slml@gft.com)   Initial Version
**/
@isTest(seeAllData=false)
public with sharing class OpportunityTriggerContrCancelTest {
  
    @TestSetup
    static void createData(){        
        Account acc = generateClientsAccount();              
        insert acc;
    
        Pricebook2 standardPricebook = new Pricebook2(
            Id = Test.getStandardPricebookId(),
            IsActive = true
        );
        update standardPricebook;

        Pricebook2 pb = standardPricebook;                      
        Product2 accessFee = generateProduct('Enterprise Subscription');
        insert accessFee;
        Product2 setupFee = generateProduct('Setup Fee');
        insert setupFee;        
        Opportunity opp = new Opportunity(CurrencyIsoCode='BRL');
        opp = generateRenegotiationOpp(acc.Id,pb);
        insert opp;
       // CreateM0FormHelper.createM0ForOppsWithIds(new List<Id>{opp.id});
    }

    @isTest
    public static void testLostOpp() {    
        Opportunity myOpp = [SELECT Id from Opportunity WHERE Name Like '%Opp Test%' Limit 1];   
        Test.startTest();
            myOpp.stageName = 'Perdido';
            myOpp.Loss_Reason__c = 'Price';
            update myOpp;
        Test.stopTest();
        
        List<Case> lCase = [SELECT Id, Subject FROM Case WHERE RecordType.DeveloperName = 'Contract_Cancellation' AND OpportunityId__c =: myOpp.id];
        System.assertEquals(1, lCase.size());
    }


    private static Account generateClientsAccount(){
        ID accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
        Account acc = new Account();
        acc.RecordTypeId = accRecordTypeId;
        acc.Name = 'Test Account';
        acc.BillingCountry = 'Brazil';
        acc.BillingState = 'São Paulo';
        acc.NumberOfEmployees = 2000;
        return acc;
    }

    
    private static Opportunity generateRenegotiationOpp(Id accId, Pricebook2 pb){
        Id oppRtId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Success_Renegotiation').getRecordTypeId();
        Opportunity opp = new Opportunity();
        opp.recordTypeId = oppRtId;
        opp.StageName='Qualification';
        opp.Sub_Type__c = 'Retention';
        opp.cancellation_date__c = Date.today();
        opp.AccountId = accId;
        opp.CloseDate = Date.today();
        opp.Engagement_Journey_Completed__c = true;
        opp.Name='Opp Test'; 
        opp.Pricebook2Id = pb.Id;
        return opp;
    }

    private static Product2 generateProduct(String ProductName){
        Product2 product = new Product2();
        product.Name = ProductName;
        product.Minimum_Number_of_Employees__c = 0;
        product.Maximum_Number_of_Employees__c = 900;
        product.Family = ProductName;
        product.IsActive = true;
        return product;
    }

}