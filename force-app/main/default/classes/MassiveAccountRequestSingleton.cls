public with sharing class MassiveAccountRequestSingleton {
    private static MassiveAccountRequestSingleton instance;
    private Massive_Account_Request__c massiveAccountRequest = new Massive_Account_Request__c();

    private MassiveAccountRequestSingleton(Id massiveId){
        this.massiveAccountRequest = [ SELECT Id, OwnerId, Status__c, Companies__c FROM Massive_Account_Request__c WHERE Id =: massiveId LIMIT 1 ];
    }

    public static MassiveAccountRequestSingleton getInstance(Id massiveId){
        if(instance == null)
            instance = new MassiveAccountRequestSingleton(massiveId);
        return instance;
    }

    public void dataUpdate(){
        update massiveAccountRequest;
    }

    public String getOwnerId(){
        return this.massiveAccountRequest.OwnerId;
    }

    public String getStatus(){
        return this.massiveAccountRequest.Status__c;
    }

    public void setStatus(String status){
        this.massiveAccountRequest.Status__c = status;
    }

    public String getCompanies(){
        return this.massiveAccountRequest.Companies__c;
    }

    public void setCompanies(String companies){
        this.massiveAccountRequest.Companies__c = Companies;
    }

    public void setMassiveLogError(String massiveLogError){
        this.massiveAccountRequest.MassiveLogError__c = massiveLogError;
    }
}