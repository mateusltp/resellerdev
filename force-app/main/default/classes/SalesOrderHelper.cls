public class SalesOrderHelper {
    private static String administratorPhoneCountryCode;
    private static String financePhoneCountryCode;
    private static String operationsPhoneCountryCode;
    
    public static Id putSalesOrderForSMB(String accountId, String opportunityId, String quoteId) {
        List<Tagus_Integration_Parameter__mdt> listTagusIntegrationParameters = [SELECT
                                                                                 Id, MasterLabel, DeveloperName, Field_API_Name__c, Object__c, Tagus_Entity__c
                                                                                 FROM
                                                                                 Tagus_Integration_Parameter__mdt
                                                                                 ORDER BY
                                                                                 //Tagus_Entity__c ASC];
                                                                                 Label ASC];
        
        Map<String, Map<String, List<Tagus_Integration_Parameter__mdt>>> mapEntityObjectParameters = new Map<String, Map<String, List<Tagus_Integration_Parameter__mdt>>>();
        
        Map<String, List<String>> mapObjectListFields = new Map<String, List<String>>();
        
        for (Tagus_Integration_Parameter__mdt tagusIntegrationParameter : listTagusIntegrationParameters) {
            String tagusEntity = tagusIntegrationParameter.Tagus_Entity__c;
            String salesforceObject = tagusIntegrationParameter.Object__c;
            String fieldAPIName = tagusIntegrationParameter.Field_API_Name__c;
            String masterLabel = tagusIntegrationParameter.MasterLabel;
            
            
            if (mapEntityObjectParameters.get(tagusEntity) == null) {
                List<Tagus_Integration_Parameter__mdt> listTagusIntegrationParametersToInstantiate = getNewListWithTagusIntegrationParameter(tagusIntegrationParameter);
                
                Map<String, List<Tagus_Integration_Parameter__mdt>> mapObjectTagusIntegrationParameters = getNewMapObjectTagusIntegrationParameters(salesforceObject, listTagusIntegrationParametersToInstantiate);
                
                mapEntityObjectParameters.put(tagusEntity, mapObjectTagusIntegrationParameters);
            } else {
                Map<String, List<Tagus_Integration_Parameter__mdt>> mapObjectTagusIntegrationParameters = mapEntityObjectParameters.get(tagusEntity);
                
                if (mapObjectTagusIntegrationParameters.get(salesforceObject) == null) {
                    List<Tagus_Integration_Parameter__mdt> listTagusIntegrationParametersToInstantiate = getNewListWithTagusIntegrationParameter(tagusIntegrationParameter);
                    
                    mapObjectTagusIntegrationParameters.put(salesforceObject, listTagusIntegrationParametersToInstantiate);
                } else {
                    mapObjectTagusIntegrationParameters.get(salesforceObject).add(tagusIntegrationParameter);
                }
            }
            
            if (mapObjectListFields.get(salesforceObject) == null) {
                List<String> listFields = new List<String>();
                listFields.add(fieldAPIName);
                
                mapObjectListFields.put(salesforceObject, listFields);
            } else {
                mapObjectListFields.get(salesforceObject).add(fieldAPIName);
            }
        }
        
        Map<String, String> mapObjectQuery = new Map<String, String>();
        
        for (String salesforceObject : mapObjectListFields.keySet()) {
            String recordId = '';
            String query = 'SELECT ';
            
            List<String> listFields = mapObjectListFields.get(salesforceObject);
            
            for (String field : listFields) {
                query += field + ',';
            }
            
            if (query.right(1) == ',')
                query = query.substringBeforeLast(',');
            
            query += ' FROM ' + salesforceObject;
            
            if (salesforceObject == 'Account')
                query += ' WHERE Id = \'' + accountId + '\'';
            else if (salesforceObject == 'Opportunity')
                query += ' WHERE Id = \'' + opportunityId + '\'';
            else if (salesforceObject == 'Quote')
                query += ' WHERE Id = \'' + quoteId + '\'';
            else if (salesforceObject == 'QuoteLineItem')
                query += ' WHERE QuoteId = \'' + quoteId + '\'';
            
            System.debug('query: ' + query);
            mapObjectQuery.put(salesforceObject, query);
        }
        
        Map<String, List<SObject>> mapObjectListRecords = new Map<String, List<SObject>>();
        
        for (String salesforceObject : mapObjectQuery.keySet()) {
            String query = mapObjectQuery.get(salesforceObject);
            
            List<SObject> listSObjects = Database.query(query);
            
            mapObjectListRecords.put(salesforceObject, listSObjects);
        }
        
        Id salesOrderId = null;
        
        System.debug('mapObjectListRecords: ' + mapObjectListRecords);
        
        for (String entity : mapEntityObjectParameters.keySet()) {
            Map<String, List<Tagus_Integration_Parameter__mdt>> mapObjectTagusIntegrationParameter = mapEntityObjectParameters.get(entity);
            
            if (entity == 'Sales Order') {
                if (salesOrderId == null) {
                    salesOrderId = putSalesOrder(accountId, opportunityId);
                }
                
                putSalesOrderParameters(salesOrderId, mapObjectTagusIntegrationParameter, mapObjectListRecords);
            } else if (entity == 'Sales Order Item') {
                List<QuoteLineItem> listQuoteLineItems = mapObjectListRecords.get('QuoteLineItem');
                
                for (QuoteLineItem quoteLineItem : listQuoteLineItems) {
                    Id salesOrderItemId = putSalesOrderItem(salesOrderId, quoteLineItem.Product2.Name);
                    
                    putSalesOrderItemParameters(salesOrderItemId, mapObjectTagusIntegrationParameter, mapObjectListRecords, quoteLineItem);
                }
            }
        }
        
        if (salesOrderId != null) {
            List<Id> salesOrderIdList = new List<Id>{salesOrderId};
            createIntegrationRequest(salesOrderIdList);
        }
        

        return salesOrderId;
    }
    
    private static Id putSalesOrderItem(String salesOrderId, String salesOrderItemName) {
        String uuidSalesOrderItem = getUUID();
        
        Sales_Order_Item__c salesOrderItem = new Sales_Order_Item__c();
        salesOrderItem.Sales_Order__c = salesOrderId;
        salesOrderItem.UUID__c = uuidSalesOrderItem;
        salesOrderItem.Name = salesOrderItemName;
        INSERT salesOrderItem;
        
        return salesOrderItem.Id;
    }
    
    private static void putSalesOrderItemParameters(Id salesOrderItemId, Map<String, List<Tagus_Integration_Parameter__mdt>> mapObjectTagusIntegrationParameter, Map<String, List<SObject>> mapObjectListRecords, QuoteLineItem quoteLineItem) {
        List<Sales_Order_Item_Parameter__c> listSalesOrderItemParametersToInsert = new List<Sales_Order_Item_Parameter__c>();
        
        for (String salesforceObject : mapObjectTagusIntegrationParameter.keySet()) {
            if (salesforceObject == 'QuoteLineItem') {
                SObject sObjectIteration = quoteLineItem;
                
                List<Tagus_Integration_Parameter__mdt> listTagusIntegrationParameters = mapObjectTagusIntegrationParameter.get(salesforceObject);
                
                for (Tagus_Integration_Parameter__mdt tagusIntegrationParameter : listTagusIntegrationParameters) {
                    System.debug('Field: ' + tagusIntegrationParameter.Field_API_Name__c);
                    if (tagusIntegrationParameter.Field_API_Name__c.contains('.')) {
                        List<String> relationshipField = tagusIntegrationParameter.Field_API_Name__c.split('\\.');
                        
                        if (relationshipField.size() == 2) {
                            String sObjectIterationObject = relationshipField[0];
                            String sObjectIterationField = relationshipField[1];
                            
                            if (sObjectIteration.getSObject(sObjectIterationObject) != null) {
                                //String value = String.valueOf(sObjectIteration.getSObject(sObjectIterationObject).get(sObjectIterationField));
                                
                                Object obj = sObjectIteration.getSObject(sObjectIterationObject).get(sObjectIterationField);
                       			String value = formatValue(obj);
                                value = formatValueAccessFeeFrequency(value, tagusIntegrationParameter.MasterLabel);
                                
                                Sales_Order_Item_Parameter__c salesOrderItemParameter = new Sales_Order_Item_Parameter__c();
                                salesOrderItemParameter.Sales_Order_Item__c = salesOrderItemId;
                                salesOrderItemParameter.Name = tagusIntegrationParameter.MasterLabel;
                                salesOrderItemParameter.Value__c = value;
                                
                                listSalesOrderItemParametersToInsert.add(salesOrderItemParameter);
                            }
                        }
                    } else {
                        //String value = String.valueOf(sObjectIteration.get(tagusIntegrationParameter.Field_API_Name__c));
                        
                        Object obj = sObjectIteration.get(tagusIntegrationParameter.Field_API_Name__c);
                       	String value = formatValue(obj);
                        value = formatValueAccessFeeFrequency(value, tagusIntegrationParameter.MasterLabel);
                        
                        Sales_Order_Item_Parameter__c salesOrderItemParameter = new Sales_Order_Item_Parameter__c();
                        salesOrderItemParameter.Sales_Order_Item__c = salesOrderItemId;
                        salesOrderItemParameter.Name = tagusIntegrationParameter.MasterLabel;
                        salesOrderItemParameter.Value__c = value;
                        
                        listSalesOrderItemParametersToInsert.add(salesOrderItemParameter);
                    }
                }
            } else {
                List<SObject> listRecords = mapObjectListRecords.get(salesforceObject);
                List<Tagus_Integration_Parameter__mdt> listTagusIntegrationParameters = mapObjectTagusIntegrationParameter.get(salesforceObject);
                
                for (SObject sObjectIteration : listRecords) {
                    for (Tagus_Integration_Parameter__mdt tagusIntegrationParameter : listTagusIntegrationParameters) {
                        System.debug('Field: ' + tagusIntegrationParameter.Field_API_Name__c);
                        if (tagusIntegrationParameter.Field_API_Name__c.contains('.')) {
                            List<String> relationshipField = tagusIntegrationParameter.Field_API_Name__c.split('\\.');
                            
                            if (relationshipField.size() == 2) {
                                String sObjectIterationObject = relationshipField[0];
                                String sObjectIterationField = relationshipField[1];
                                
                                if (sObjectIteration.getSObject(sObjectIterationObject) != null) {
                                    //String value = String.valueOf(sObjectIteration.getSObject(sObjectIterationObject).get(sObjectIterationField));
                                    
                                    Object obj = sObjectIteration.getSObject(sObjectIterationObject).get(sObjectIterationField);
                                	String value = formatValue(obj);
                                    value = formatValueAccessFeeFrequency(value, tagusIntegrationParameter.MasterLabel);
                                    
                                    Sales_Order_Item_Parameter__c salesOrderItemParameter = new Sales_Order_Item_Parameter__c();
                                    salesOrderItemParameter.Sales_Order_Item__c = salesOrderItemId;
                                    salesOrderItemParameter.Name = tagusIntegrationParameter.MasterLabel;
                                    salesOrderItemParameter.Value__c = value;
                                    
                                    listSalesOrderItemParametersToInsert.add(salesOrderItemParameter);
                                }
                            }
                        } else {
                            //String value = String.valueOf(sObjectIteration.get(tagusIntegrationParameter.Field_API_Name__c));
                            
                            Object obj = sObjectIteration.get(tagusIntegrationParameter.Field_API_Name__c);
                           	String value = formatValue(obj);
                            value = formatValueAccessFeeFrequency(value, tagusIntegrationParameter.MasterLabel);
                            
                            Sales_Order_Item_Parameter__c salesOrderItemParameter = new Sales_Order_Item_Parameter__c();
                            salesOrderItemParameter.Sales_Order_Item__c = salesOrderItemId;
                            salesOrderItemParameter.Name = tagusIntegrationParameter.MasterLabel;
                            salesOrderItemParameter.Value__c = value;
                            
                            listSalesOrderItemParametersToInsert.add(salesOrderItemParameter);
                        }
                    }
                }
            }
        }
        
        INSERT listSalesOrderItemParametersToInsert;
    }
    
    private static void putSalesOrderParameters(Id salesOrderId, Map<String, List<Tagus_Integration_Parameter__mdt>> mapObjectTagusIntegrationParameter, Map<String, List<SObject>> mapObjectListRecords) {
        List<Sales_Order_Parameter__c> listSalesOrderParametersToInsert = new List<Sales_Order_Parameter__c>();
        
        for (String salesforceObject : mapObjectTagusIntegrationParameter.keySet()) {
            List<SObject> listRecords = mapObjectListRecords.get(salesforceObject);
            List<Tagus_Integration_Parameter__mdt> listTagusIntegrationParameters = mapObjectTagusIntegrationParameter.get(salesforceObject);
            
            listTagusIntegrationParameters.sort();
            
            for (SObject sObjectIteration : listRecords) {
                for (Tagus_Integration_Parameter__mdt tagusIntegrationParameter : listTagusIntegrationParameters) {
                    if (tagusIntegrationParameter.Field_API_Name__c.contains('.')) {
                        List<String> relationshipField = tagusIntegrationParameter.Field_API_Name__c.split('\\.');
                        
                        if (relationshipField.size() == 2) {
                            String sObjectIterationObject = relationshipField[0];
                            String sObjectIterationField = relationshipField[1];
                            
                            if (sObjectIteration.getSObject(sObjectIterationObject) != null) {
                                //String value = String.valueOf(sObjectIteration.getSObject(sObjectIterationObject).get(sObjectIterationField));
                                
                                Object obj = sObjectIteration.getSObject(sObjectIterationObject).get(sObjectIterationField);
                                String value = formatValue(obj);
                                value = getFormatValueForPhoneField(tagusIntegrationParameter.MasterLabel, value, sObjectIteration, sObjectIterationObject, sObjectIterationField);
                                value = formatUniqueIdentifierValue(value, tagusIntegrationParameter.MasterLabel);
                                value = formatDocumentNumbers(value, tagusIntegrationParameter.MasterLabel);
                                
                                Sales_Order_Parameter__c salesOrderParameter = new Sales_Order_Parameter__c();
                                salesOrderParameter.Sales_Order__c = salesOrderId;
                                salesOrderParameter.Name = tagusIntegrationParameter.MasterLabel;
                                salesOrderParameter.Value__c = value;
                                
                                System.debug('tagusIntegrationParameter.MasterLabel: ' + tagusIntegrationParameter.MasterLabel);
                                
                                assignCountryCodeVariables(tagusIntegrationParameter.MasterLabel, value);
                                
                                listSalesOrderParametersToInsert.add(salesOrderParameter);
                            }
                        }
                    } else {
                        //String value = String.valueOf(sObjectIteration.get(tagusIntegrationParameter.Field_API_Name__c));
                        
                        Object obj = sObjectIteration.get(tagusIntegrationParameter.Field_API_Name__c);
                       	String value = formatValue(obj);
                        value = getFormatValueForPhoneField(tagusIntegrationParameter.MasterLabel, value, sObjectIteration, null, tagusIntegrationParameter.Field_API_Name__c);
                        value = formatUniqueIdentifierValue(value, tagusIntegrationParameter.MasterLabel);
                        
                        Sales_Order_Parameter__c salesOrderParameter = new Sales_Order_Parameter__c();
                        salesOrderParameter.Sales_Order__c = salesOrderId;
                        salesOrderParameter.Name = tagusIntegrationParameter.MasterLabel;
                        salesOrderParameter.Value__c = value;
                        
                        assignCountryCodeVariables(tagusIntegrationParameter.MasterLabel, value);
                        
                        listSalesOrderParametersToInsert.add(salesOrderParameter);
                    }
                }
            }
        }
        
        INSERT listSalesOrderParametersToInsert;
    }
    
    private static Id putSalesOrder(String accountId, String opportunityId) {
        String uuidSalesOrder = getUUID();
        
        Sales_Order__c salesOrder = new Sales_Order__c();
        salesOrder.Account__c = accountId;
        salesOrder.Opportunity__c = opportunityId;
        salesOrder.UUID__c = uuidSalesOrder;
        INSERT salesOrder;
        
        return salesOrder.Id;
    }
    
    private static String getUUID() {
        Uuid uuid = new Uuid();
        String uuidValue = uuid.getValue();
        
        return uuidValue;
    }
    
    private static List<Tagus_Integration_Parameter__mdt> getNewListWithTagusIntegrationParameter(Tagus_Integration_Parameter__mdt tagusIntegrationParameter) {
        List<Tagus_Integration_Parameter__mdt> listTagusIntegrationParametersToInstantiate = new List<Tagus_Integration_Parameter__mdt>();
        listTagusIntegrationParametersToInstantiate.add(tagusIntegrationParameter);
        
        return listTagusIntegrationParametersToInstantiate;
    }
    
    private static Map<String, List<Tagus_Integration_Parameter__mdt>> getNewMapObjectTagusIntegrationParameters(String salesforceObject, List<Tagus_Integration_Parameter__mdt> listTagusIntegrationParametersToInstantiate) {
        Map<String, List<Tagus_Integration_Parameter__mdt>> mapObjectTagusIntegrationParameters = new Map<String, List<Tagus_Integration_Parameter__mdt>>();
        mapObjectTagusIntegrationParameters.put(salesforceObject, listTagusIntegrationParametersToInstantiate);
        
        return mapObjectTagusIntegrationParameters;
    }
    
    private static String formatValue(Object obj) {
        String value;
        
        if (obj instanceof Date) {
            Date dt = (Date)obj;
            Time t = Time.newInstance(3, 0, 0, 0);
            DateTime dTime = DateTime.newInstance(dt, t);
            value = String.valueOf(dTime.format('yyyy-MM-dd\'T\'HH:mm:ss.SSSSSS\'Z\''));
        } else if (obj instanceof DateTime) {
            DateTime dTime = (DateTime)obj;
            value = String.valueOf(dTime.format('yyyy-MM-dd\'T\'HH:mm:ss.SSSSSS\'Z\''));
        } else {
            value = String.valueOf(obj);
        }       
        return value;
    }
    
    private static String getFormatValueForPhoneField(String masterLabel, String value, sObject sObjectIteration, String sObjectIterationObject, String sObjectIterationField) {
        DisplayType fieldType;
        
        if (sObjectIterationObject != null) {
            sObject sobj = sObjectIteration.getSObject(sObjectIterationObject);
			Schema.SObjectType soType = sobj.getSObjectType();
			DescribeSObjectResult result = soType.getDescribe();

		 	fieldType = result.fields.getMap().get(sObjectIterationField).getDescribe().getType();
        } else {
			Schema.SObjectType soType = sObjectIteration.getSObjectType();
			DescribeSObjectResult result = soType.getDescribe();

		 	fieldType = result.fields.getMap().get(sObjectIterationField).getDescribe().getType();
        }
        
        if (fieldType == DisplayType.Phone) {
        	return formatPhoneValue(masterLabel, value);
        } else {
        	return value;    
        }
    }
    
    private static String formatPhoneValue(String masterLabel, String value) {
        if (value == '' || value == null) {
            return value;
        }
        
        System.debug('masterLabel');
        System.debug(masterLabel);
        
        if (masterLabel == 'administratorPhone' && administratorPhoneCountryCode != null) {
            System.debug('Passou aqui administratorPhone');
       		value = value.replaceAll('[()\\- ]', '');
            if (!value.startsWith(administratorPhoneCountryCode)) {
                value = administratorPhoneCountryCode + value;
            }
            System.debug('value: ' + value);
        }
        
        if (masterLabel == 'financePhone' && financePhoneCountryCode != null) {
            System.debug('Passou aqui financePhone');
            value = value.replaceAll('[()\\- ]', '');
            if (!value.startsWith(financePhoneCountryCode)) {
            	value = financePhoneCountryCode + value;    
            }
            System.debug('value: ' + value);
        }
        
        if (masterLabel == 'operationsPhone' && operationsPhoneCountryCode != null) {
            System.debug('Passou aqui operationsPhone');
            value = value.replaceAll('[()\\- ]', '');
            if (!value.startsWith(operationsPhoneCountryCode)) {
            	value = operationsPhoneCountryCode + value;    
            }
            System.debug('value: ' + value);
        }
        
        return value;
    }
    
    private static void assignCountryCodeVariables(String masterLabel, String value) {
        List<Phone_Country_Code__mdt> phoneCountryCode = [
        	SELECT Label, MasterLabel, Code_Number__c
            FROM Phone_Country_Code__mdt
            WHERE MasterLabel = :value
        ];
        
        if (phoneCountryCode.size() > 0) {
        	if (masterLabel == 'administratorMailingCountryCode') {
                System.debug('SKON 1');
       			administratorPhoneCountryCode = phoneCountryCode.get(0).Code_Number__c;
                System.debug('administratorPhoneCountryCode: ' + administratorPhoneCountryCode);
        	} else if (masterLabel == 'financeMailingCountryCode') {
                System.debug('SKON 2');
            	financePhoneCountryCode = phoneCountryCode.get(0).Code_Number__c;
                System.debug('financePhoneCountryCode: ' + financePhoneCountryCode);
            } else if (masterLabel == 'operationsMailingCountryCode') {
            	System.debug('SKON 3');
            	operationsPhoneCountryCode = phoneCountryCode.get(0).Code_Number__c;
                System.debug('operationsPhoneCountryCode: ' + operationsPhoneCountryCode);
            }   
        }
    }
    
    private static String formatUniqueIdentifierValue(String value, String masterLabel) {
        if (masterLabel == 'uniqueIdentifier') {
            if (value != null) {
            	value = value.toLowerCase();
                value = value.replace('-', '');
                value = value.replace(' ', '_');
            }
        }
        
        return value;
    }
    
    private static String formatValueAccessFeeFrequency(String value, String masterLabel) {
        if (masterLabel == 'accessFeeFrequency') {
            Map<String, String> accessFeeFrenquencyDictionary = new Map<String, String>{
				'Monthly' 		=> 'month',
        		'Quarterly' 	=> 'quarter',
                'Annual' 		=> 'year',
                'Daily' 		=> 'day',
               	'Weekly' 		=> 'week',
               	'Semiannual' 	=> 'semiannual'
            };
            
            String frequency = accessFeeFrenquencyDictionary.get(value);
           	if (frequency != null) {
            	return frequency;
            } else {
                return value;
            }
        }
        
        return value;
    }

    private static String formatDocumentNumbers(String value, String masterLabel) {
        if (value != null && (masterLabel == 'administratorDocumentNumber' || masterLabel == 'financeDocumentNumber')) {
            value = value.replaceAll('[\\.-]', '');
        }

        return value;
    }

    @future
    private static void createIntegrationRequest(List<Id> sObjectIdList) {
        TagusIntegrationController.startIntegrationRequestCreation(sObjectIdList, 'Create Sales Order Button');
    }
}