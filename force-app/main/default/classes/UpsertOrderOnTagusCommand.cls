public without sharing class UpsertOrderOnTagusCommand extends AbstractUpdateableOutboundCommand {
  private IntegrationRequest request;
  private Object response;

  override public Object transformToSend() {
    TagusTransaction.TransactionType transactionType = event.getEventName()
        .contains('CREATE')
      ? TagusTransaction.TransactionType.CREATED
      : TagusTransaction.TransactionType.UPDATED;

    this.request = new IntegrationRequest();

    request.transaction_info = new TagusTransaction(event, transactionType);

    request.transaction_data = new TagusTransactionData(
      TagusTransactionData.SourceEventType.CRUD
    );

    List<OrderTagusDTO> orders = (List<OrderTagusDTO>) event.getPayloadFromJson(
      List<OrderTagusDTO>.class
    );

    request.transaction_data.setData(new IntegrationRequestData(orders));

    return request;
  }

  override public void processResult(Object response) {
    this.response = response;
  }

  override public void postUpdateExecute() {
    IntegrationRequestData requestData = (IntegrationRequestData) this.request.transaction_data.getdata();

    List<Order> orders = new List<Order>();

    for (OrderTagusDTO orderDTO : requestData.orders) {
      if (!orderDTO.getIsOrderOnTagus()) {
        orders.add(
          new Order(
            Id = orderDTO.getSalesforceId(),
            Is_Order_On_Tagus__c = true
          )
        );
      }
    }

    if (!orders.isEmpty()) {
      Database.update(orders);
    }
  }

  private class IntegrationRequest {
    private TagusTransaction transaction_info;
    private TagusTransactionData transaction_data;
  }

  private class IntegrationRequestData {
    private List<OrderTagusDTO> orders;

    private IntegrationRequestData(List<OrderTagusDTO> orders) {
      this.orders = orders;
    }
  }
}