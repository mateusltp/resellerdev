/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 08-11-2021
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/
public with sharing class TagusPartnerTransaction {
   
    private String name;       
    private PartnerTransactionType type;
    private Datetime timestamp;
    private String source;
    private String salesforce_id;
    private String id;   
    private CreatedBy created_by_user;

    public TagusPartnerTransaction(EventQueue event, PartnerTransactionType type) {
        this.name = event.getEventName();
        this.type = type;
        this.timestamp = Datetime.now();
        this.source = event.get().sender__c;
        this.salesforce_id = event.getEventId();        
        this.id = new Uuid().getValue(); 
        this.created_by_user = new CreatedBy();
    }


    private class CreatedBy {
        private String username;
        private String salesforce_id;    
        private CreatedBy() {
          this.salesforce_id = UserInfo.getUserId();
          this.username = UserInfo.getUserName();
        }
    }

    public enum PartnerTransactionType {
        CREATED,
        UPDATED,
        DELETED
    }
}