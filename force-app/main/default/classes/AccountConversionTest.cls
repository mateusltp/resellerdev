/**
 * @File Name          : AccountConversionTest.cls
 * @Description        : 
 * @Author             : GEPI@GFT.com
 * @Group              : 
 * @Last Modified By   : GEPI@GFT.com
 * @Last Modified On   : 07-24-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    12/03/2020   Samuel Silva - GFT (slml@gft.com)     Initial Version
**/
@isTest 
private class AccountConversionTest {


    @TestSetup
    static void generateGenericAccounts(){
        
        Id rtId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gym_Partner').getRecordTypeId();
       
        Account acc = new Account();

        acc.name='AcademiaBrasilCompanyPai';
        acc.RecordTypeId = rtId;
        acc.Types_of_ownership__c = Label.privateOwnerPicklist;
        acc.GP_Status__c = 'Pending Approval';
        acc.ShippingStreet = 'RUAURA4 aac';
        acc.ShippingState = 'Minas Gerais';
        acc.ShippingCity ='MI CYTIX';
        acc.ShippingCountry = 'Brazil';
        acc.Gym_Type__c = 'Studios';
        acc.Subscription_Type__c = 'Value per class';
        acc.Subscription_Period__c = 'Monthy value';
        acc.Subscription_Type_Estimated_Price__c	= 100;
        acc.Has_market_cannibalization__c = 'No';
        acc.Exclusivity__c = 'Yes';
        acc.Exclusivity_End_Date__c = Date.today().addYears(1);
        acc.Exclusivity_Partnership__c = 'Full Exclusivity';
        acc.Exclusivity_Restrictions__c= 'No';
        acc.Website = 'testing@tesapex.com';
        acc.Gym_Email__c = 'gymemail@apex.com';
        acc.Phone = '3222123123';
        INSERT acc;
      
        Account child = new Account();
        child.name='academiaBrasilCompanyChild1'; 
        child.ParentId = acc.Id;
        child.Types_of_ownership__c = Label.privateOwnerPicklist;
        child.GP_Status__c = 'Pending Approval';
        child.website='www.ChildOne.com';
        child.ShippingCity = 'KKKKK';
        child.RecordTypeId = rtId; 
        child.Gym_Type__c ='Studios';
        child.ShippingCountry = 'Brazil'; 
        child.ShippingState = 'Roraima';
        child.ShippingStreet = 'RAXXX';
        child.Subscription_Type__c = 'Value per class';
        child.Subscription_Period__c = 'Monthy value';
        child.Subscription_Type_Estimated_Price__c = 100;
        child.Exclusivity__c = 'Yes';
        child.Exclusivity_End_Date__c = Date.today().addYears(1);
        child.Exclusivity_Partnership__c = 'Full Exclusivity';
        child.Exclusivity_Restrictions__c= 'No';
        child.Gym_Email__c = 'gymemail@apex.com';
        child.Phone = '3222123123';
        
        INSERT child; 


        Account child2 = new Account();
        child2.name='academia2'; 
        child2.ParentId = acc.Id;
        child2.Types_of_ownership__c = Label.privateOwnerPicklist;
        child2.GP_Status__c = 'Pending Approval';
        child2.website='www.ChildOne.com';
        child2.RecordTypeId = rtId; 
        child2.Gym_Type__c ='Studios';
        child2.ShippingStreet = 'RUA test';
        child2.ShippingCountry = 'Argentina'; 
        child2.ShippingCity = 'AR';
        child2.ShippingState = 'Buenos Aires';
        child2.Subscription_Type__c = 'Value per class';
        child2.Subscription_Period__c = 'Monthy value';
        child2.Subscription_Type_Estimated_Price__c = 100;
        child2.Exclusivity__c = 'Yes';
        child2.Exclusivity_End_Date__c = Date.today().addYears(1);
        child2.Exclusivity_Partnership__c = 'Full Exclusivity';
        child2.Exclusivity_Restrictions__c= 'No';
        child2.Gym_Email__c = 'gymemail@apex.com';
        child2.Phone = '1111111';
        
        INSERT child2; 


        Account acc2 = new Account();

        acc2.name='acc2';
        acc2.RecordTypeId = rtId;
        acc2.GP_Status__c = 'Pending Approval';
        acc2.ShippingState = 'Mato Grosso';
        acc2.ShippingStreet = 'RUAURA2 Test';
        acc2.ShippingCountry = 'Brazil';
        acc2.ShippingCity = 'AAAASDDD';
        acc2.Gym_Type__c = 'Studios';
        acc2.Types_of_ownership__c = Label.franchisePicklist;
        acc2.Subscription_Type__c = 'Value per class';
        acc2.Subscription_Period__c = 'Monthy value';
        acc2.Subscription_Type_Estimated_Price__c	= 100;
        acc2.Has_market_cannibalization__c = 'No';
        acc2.Exclusivity__c = 'Yes';
        acc2.Exclusivity_End_Date__c = Date.today().addYears(1);
        acc2.Exclusivity_Partnership__c = 'Full Exclusivity';
        acc2.Exclusivity_Restrictions__c= 'No';
        acc2.Website = 'testing@tesapex.com';
        acc2.Gym_Email__c = 'gymemail@apex.com';
        acc2.Phone = '3222123123';
        INSERT acc2;
    
    }

    
    @isTest static void getInvalidAccountObjectTest() {
        String badAccountId = 'badIdRequest';            
        Test.startTest();
            Account accObj = (Account)AccountConversionController.getAccountObject(badAccountId);
            System.assertEquals(null, accObj);
        Test.stopTest();               
    }

    @isTest static void getValidAccountObjectTest() {
        Account acct = [SELECT Id, Name FROM Account WHERE Name='AcademiaBrasilCompanyPai'];           
        Test.startTest();   
            Account accObj = (Account)AccountConversionController.getAccountObject(acct.Id);
            System.assertEquals(acct.Name, accObj.Name);
        Test.stopTest();
    }

    @isTest static void convertAccountWithChild(){
        Account acct = [SELECT Id, Name, ParentId, Types_of_ownership__c FROM Account WHERE Name='AcademiaBrasilCompanyPai'];  
        Test.startTest();   
            Account accObj = (Account)AccountConversionController.convertAccount(acct);
            System.assertEquals(acct.Name, accObj.Name);
        Test.stopTest();          
    }

    
    @isTest static void convertToCorporate(){
        Account acct = [SELECT Id, Name, ParentId, Types_of_ownership__c FROM Account WHERE Name='acc2'];  
        Test.startTest();   
            Account accObj = (Account)AccountConversionController.convertAccount(acct);
            System.assertEquals('Private owner', accObj.types_of_ownership__c);
        Test.stopTest();          
    }

    @isTest static void convertToFranchise(){
        Account acct = [SELECT Id, Name, ParentId, Types_of_ownership__c FROM Account WHERE Name='AcademiaBrasilCompanyPai'];  
        Test.startTest();   
            Account accObj = (Account)AccountConversionController.convertAccount(acct);
            System.assertEquals('Franchise', accObj.types_of_ownership__c);
        Test.stopTest();          
    }

   
}