/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 03-14-2022
 * @last modified by  : GEPI@GFT.com
 * Modifications Log 
 * Ver   Date         Author                             Modification
 * 1.0   07-22-2021   gft.samuel.silva@ext.gympass.com   Initial Version
 * 1.1   02-15-2022   bruno.mendes@gympass.com           IT-2306 - Populate Legal Representative on Account
**/
public with sharing class ContactPartnerServiceImpl implements IContactService {
    PS_Constants constants = PS_Constants.getInstance();

    public Id getRecordTypeId(String recordTypeDevName){
       return Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get(recordTypeDevName).getRecordTypeId();
    }

    
    public void updateEmailAddress(List<Contact> aContactList ){

        fflib_ISObjectUnitOfWork uow = Application.UnitOfWork.newInstance();
        for(Contact iContact : aContactList){
            iContact.email = 'balalsla@gmailt.com';          
           uow.registerDirty(iContact);
        }
        
        uow.commitWork();
    }

    /**
    * @author bruno.mendes@gympass.com
    * @description IT-2306 - populate legal representative on Account - runs on Contact Trigger after insert context
    * @param List<Contact> contacts - Trigger.New or any Contact list anywhere else in case of need
    */
    public void updateAccountLegalRepresentative(List<Contact> contacts) {
        System.debug('## entered contacts trigger on insert - update account legal rep: '+contacts);
        fflib_ISObjectUnitOfWork unitOfWork = Application.UnitOfWork.newInstance();
        List<Account> accountsToUpdate = new List<Account>();

        for (Contact c : contacts) {
            system.debug('## contact name: '+c.LastName);
            system.debug('## contact type: '+c.Type_of_contact__c);
            system.debug('## contact accountid: '+c.AccountId);
            if ( c.Type_of_contact__c != null && c.Type_of_contact__c.contains(constants.CONTACT_TYPE_LEGAL_REPRESENTATIVE)) {
                accountsToUpdate.add(new Account(
                        Id = c.AccountId,
                        Legal_Representative__c = c.Id
                ));
            }
        }


        unitOfWork.registerDirty(accountsToUpdate);
        unitOfWork.commitWork();
    }

    /**
    * @author bruno.mendes@gympass.com
    * @description IT-2306 - populate legal representative on Account - runs on Contact Trigger after update context
    * @param List<Contact> contacts - Trigger.New
    * @param Map<Id, Contact> oldContactsByIds - Trigger.oldMap
    */
    public void updateAccountLegalRepresentative(List<Contact> contacts, Map<Id, Contact> oldContactsByIds) {
        fflib_ISObjectUnitOfWork unitOfWork = Application.UnitOfWork.newInstance();
        List<Account> accountsToUpdate = new List<Account>();

        for (Contact c : contacts) {
            // Contact wasn't the legal representative and now is
            if ( c.Type_of_contact__c != null && c.Type_of_contact__c.contains(constants.CONTACT_TYPE_LEGAL_REPRESENTATIVE) && (String.isBlank(oldContactsByIds.get(c.Id).Type_of_contact__c) || (String.isNotBlank(oldContactsByIds.get(c.Id).Type_of_contact__c) && !oldContactsByIds.get(c.Id).Type_of_contact__c.contains(constants.CONTACT_TYPE_LEGAL_REPRESENTATIVE)))) {
                accountsToUpdate.add(new Account(
                        Id = c.AccountId,
                        Legal_Representative__c = c.Id
                ));
            }
            // Contact was the legal representative but now is not
            else if (c.Type_of_contact__c != null && !c.Type_of_contact__c.contains(constants.CONTACT_TYPE_LEGAL_REPRESENTATIVE) && String.isNotBlank(oldContactsByIds.get(c.Id).Type_of_contact__c) && oldContactsByIds.get(c.Id).Type_of_contact__c.contains(constants.CONTACT_TYPE_LEGAL_REPRESENTATIVE)) {
                accountsToUpdate.add(new Account(
                        Id = c.AccountId,
                        Legal_Representative__c = null
                ));
            }
        }

        unitOfWork.registerDirty(accountsToUpdate);
        unitOfWork.commitWork();
    }

    /**
    * @author bruno.mendes@gympass.com
    * @description IT-XXXX - update the Roles field on the Account Contact Relation record - runs on Contact Trigger after insert context
    * @param List<Contact> contacts - Trigger.New or any Contact list anywhere else in case of need
    */
    public void updateRelationshipRole(List<Contact> contacts) {
        Map<Id, Contact> filteredContactsByIds = new Map<Id, Contact>();
        for(Contact c : contacts) {
            if (String.isNotBlank(c.Type_of_Contact__c)) {
                filteredContactsByIds.put(c.Id, c);
            }
        }
        updateRole(filteredContactsByIds);
    }

    /**
    * @author bruno.mendes@gympass.com
    * @description IT-XXXX - update the Roles field on the Account Contact Relation record - runs on Contact Trigger after update context
    * @param List<Contact> contacts - Trigger.New or any Contact list anywhere else in case of need
    */
    public void updateRelationshipRole(List<Contact> contacts, Map<Id, Contact> oldContactsByIds) {
        Map<Id, Contact> filteredContactsByIds = new Map<Id, Contact>();
        for (Contact c : contacts) {
            if(c.Type_of_Contact__c != oldContactsByIds.get(c.Id).Type_of_Contact__c) {
                filteredContactsByIds.put(c.Id, c);
            }
        }
        updateRole(filteredContactsByIds);
    }

    /**
        * @author bruno.mendes@gympass.com
        * @description IT-XXXX - edit all accounts with selected existing contact as legal representative
        * @param List<Id> accountIds, Id selectedLegalRepContactId
        */
    public void updateAccountsWithExistingLegalRepresentative(List<Id> accountIds, Id selectedLegalRepContactId) {
        fflib_ISObjectUnitOfWork uow = Application.UnitOfWork.newInstance();
        AccountContactRelationSelector relationSelector = new AccountContactRelationSelector();
        ContactSelector contactSelector = new ContactSelector();

        Set<Id> accountIdSet = new Set<Id>();
        accountIdSet.addAll(accountIds);

        List<AccountContactRelation> accountContactRelationsSameLegalRep = new List<AccountContactRelation>();
        List<AccountContactRelation> accountContactRelationsDiffLegalRep = new List<AccountContactRelation>();
        // AccountContactRelation selector by accountIds
        for (AccountContactRelation relation : relationSelector.selectContactRelationById(accountIdSet)) {
            if (relation.ContactId == selectedLegalRepContactId) {
                accountContactRelationsSameLegalRep.add(relation);
            } else {
                accountContactRelationsDiffLegalRep.add(relation);
            }
        }

        List<Contact> contactsDiffLegalRep = new List<Contact>();
        // Contact selector by accountIds
        for (Contact contact : contactSelector.selectContactsByID(accountIdSet)) {
            if (contact.Id != selectedLegalRepContactId) {
                contactsDiffLegalRep.add(contact);
            }
        }

        List<AccountContactRelation> relationsToUpdate = removeCurrentLegalRepsFromRelations(accountContactRelationsDiffLegalRep);
        List<Contact> contactsToUpdate = removeCurrentLegalRepsFromContacts(contactsDiffLegalRep);

        List<Account> accountsToUpdate = setLegalRepresentative(accountIds, selectedLegalRepContactId);

        for (AccountContactRelation relation : accountContactRelationsSameLegalRep) {
            if (relation.AccountId == relation.Contact.AccountId) {
                Contact contact = new Contact(
                        Id = relation.ContactId,
                        Type_of_contact__c = relation.Contact.Type_of_contact__c
                );
                contactsToUpdate.add(updateContactRole(contact));
            } else {
                relationsToUpdate.add(updateContactRelationRole(relation));
            }
            accountIds.remove(accountIds.indexOf(relation.AccountId));
        }

        // se não existir relação, criar uma nova relação
        List<AccountContactRelation> relationsToCreate = new List<AccountContactRelation>();
        for (Id accountId : accountIds) {
            relationsToCreate.add(createContactRelation(accountId, selectedLegalRepContactId));
        }

        uow.registerDirty(relationsToUpdate);
        uow.registerDirty(contactsToUpdate);
        uow.registerDirty(accountsToUpdate);
        uow.registerNew(relationsToCreate);

        uow.commitWork();
    }

    /**
        * @author bruno.mendes@gympass.com
        * @description IT-XXXX - edit all accounts with selected existing contact as legal representative
        * @param List<Id> accountIds, String newContactSerialized
        */
    public void updateAccountsWithNewLegalRepresentative(List<Id> accountIds, String newContactSerialized) {
        Contact newContact = (Contact)JSON.deserialize(newContactSerialized, Contact.class);

        fflib_ISObjectUnitOfWork uowForOldData = Application.UnitOfWork.newInstance();
        fflib_ISObjectUnitOfWork uowForNewData = Application.UnitOfWork.newInstance();
        AccountContactRelationSelector relationSelector = new AccountContactRelationSelector();
        ContactSelector contactSelector = new ContactSelector();

        Set<Id> accountIdSet = new Set<Id>();
        accountIdSet.addAll(accountIds);

        List<AccountContactRelation> accountContactRelations = relationSelector.selectContactRelationById(accountIdSet);
        system.debug('## accountContactRelations: '+accountContactRelations);
        List<Contact> contacts = contactSelector.selectContactsByID(accountIdSet);


        List<AccountContactRelation> relationsToUpdate = removeCurrentLegalRepsFromRelations(accountContactRelations);
        uowForOldData.registerDirty(relationsToUpdate);
        List<Contact> contactsToUpdate = removeCurrentLegalRepsFromContacts(contacts);
        uowForOldData.registerDirty(contactsToUpdate);
        uowForOldData.commitWork();

        newContact = updateContactRole(newContact);
        uowForNewData.registerNew(newContact);

        List<AccountContactRelation> relationsToCreate = new List<AccountContactRelation>();

        for (Id accountId : accountIds) {
            if (accountId != newContact.AccountId) {

               AccountContactRelation accountContactRel = createContactRelation(accountId);

               relationsToCreate.add(accountContactRel);
                uowForNewData.registerRelationship( accountContactRel, AccountContactRelation.ContactId, newContact );

                Account aAaccount = new Account(
                    Id = accountId
                );

                uowForNewData.registerDirty(aAaccount, Account.Legal_Representative__c, newContact );
            }
        }

        uowForNewData.registerNew(relationsToCreate);

        uowForNewData.commitWork();
    }

    private void updateRole(Map<Id, Contact> contactsByIds) {
        AccountContactRelationSelector relationSelector = new AccountContactRelationSelector();
        List<AccountContactRelation> accountContactRelations = relationSelector.selectByContactId(contactsByIds.keySet());
        for (AccountContactRelation acr : accountContactRelations) {
            for (Id contactId : contactsByIds.keySet()) {
                if (acr.AccountId == contactsByIds.get(contactId).AccountId && acr.ContactId == contactId) {
                    acr.Type_of_Contact__c = contactsByIds.get(contactId).Type_of_Contact__c;
                }
            }
        }
        update accountContactRelations;
    }



    private static Contact updateContactRole(Contact contact) {
        if(String.isBlank(contact.Type_of_contact__c)) {
            contact.Type_of_contact__c = 'Legal Representative';
        }
        else if(!contact.Type_of_contact__c.contains('Legal Representative')) {
            contact.Type_of_contact__c += ';Legal Representative';
        }
        return contact;
    }

    private static AccountContactRelation updateContactRelationRole(AccountContactRelation relation) {
        if(String.isBlank(relation.Type_of_Contact__c))  {
            relation.Type_of_Contact__c = 'Legal Representative';
        }
        else if(!relation.Type_of_Contact__c.contains('Legal Representative')) {
            relation.Type_of_Contact__c += ';Legal Representative';
        }
        return relation;
    }

    private static AccountContactRelation createContactRelation(Id accountId, Id contactId ) {
        AccountContactRelation relation = new AccountContactRelation(
                AccountId = accountId,
                ContactId = contactId,
                Type_of_Contact__c = 'Legal Representative'
        );
        return relation;
    }

    private static AccountContactRelation createContactRelation(Id accountId ) {
        AccountContactRelation relation = new AccountContactRelation(
                AccountId = accountId,
                Type_of_Contact__c = 'Legal Representative'
        );
        return relation;
    }

    private static List<AccountContactRelation> removeCurrentLegalRepsFromRelations(List<AccountContactRelation> accountContactRelations) {
        List<AccountContactRelation> relationsToUpdate = new List<AccountContactRelation>();

        // remove current legal rep from relationships
        List<String> roles;
        for (AccountContactRelation accountContactRelation : accountContactRelations) {
            if(!String.isBlank(accountContactRelation.Type_of_Contact__c)) {
                roles = accountContactRelation.Type_of_Contact__c.split(';');
                if (roles.contains('Legal Representative')) {
                    roles.remove(roles.indexOf('Legal Representative'));
                    accountContactRelation.Type_of_Contact__c = String.join(roles, ';');
                    relationsToUpdate.add(accountContactRelation);
                }
            }
        }
        return relationsToUpdate;
    }

    private static List<Contact> removeCurrentLegalRepsFromContacts(List<Contact> contacts) {
        List<Contact> contactsToUpdate = new List<Contact>();

        // remove current legal rep from contacts
        List<String> roles;
        for (Contact contact : contacts) {
            if(!String.isBlank(contact.Type_of_contact__c)) {
                roles = contact.Type_of_contact__c.split(';');
                if (roles.contains('Legal Representative')) {
                    roles.remove(roles.indexOf('Legal Representative'));
                    contact.Type_of_contact__c = String.join(roles, ';');
                    contactsToUpdate.add(contact);
                }
            }
        }
        return contactsToUpdate;
    }

    private static List<Account> setLegalRepresentative(List<Id> accountIds, Id contactId) {
        List<Account> accountsToUpdate = new List<Account>();

        for (Id accountId : accountIds) {
            Account account = new Account(
                    Id = accountId,
                    Legal_Representative__c = contactId
            );
            accountsToUpdate.add(account);
        }
        return accountsToUpdate;
    }

}