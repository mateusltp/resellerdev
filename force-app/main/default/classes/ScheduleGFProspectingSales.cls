/**
* @File Name          : ScheduleGFProspectingSales.cls
* @Description        : Class Test -> ScheduleGetFeedbackTest
* @Author             : JRDL@GFT.com
* @Group              :
* @Last Modified By   : 
* @Last Modified On   : 
* @Modification Log   :
* Ver       Date            Author      		    Modification
* 1.0    10/06/2021   JRDL@GFT.com     Initial Version
**/
global class ScheduleGFProspectingSales implements Schedulable {	
  	global void execute(SchedulableContext SC) {
  		Database.executeBatch(new BatchGFProspectingSales());
   	}
}