/*
* @author Bruno Pinho
* @date January-2019
* @description This class implements the ITriggerHandler to provide abstract/virtual methods for the interface methods
*               and so that the trigger handlers need to implement only the method that they have to. The only exception
*               is the mainEntry, which is mandatory for the trigger handlers to implement.
*/
public abstract class TriggerHandlerBase implements ITriggerHandler
{
    protected Map<Id, SObject> sObjectsToUpdate = new Map<Id, SObject>();
    
    public abstract void mainEntry(TriggerParameters tp);
    
    public virtual void inProgressEntry(TriggerParameters tp){}
    
    public virtual void updateObjects()
    {
        if(sObjectsToUpdate.size() > 0)
            update sObjectsToUpdate.values();
    }
}