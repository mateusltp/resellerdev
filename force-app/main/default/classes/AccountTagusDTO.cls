public without sharing class AccountTagusDTO {
  private String id;
  private String parent_id;
  private String legal_document_type;
  private String legal_document;
  private String legal_name;
  private String trade_name;
  private String country_id;
  private String salesforce_id;
  private String business_unit;
  private String billing_email;
  private String currency_id;
  private String phone;
  private String account_type;
  private Boolean government_company;
  private PaymentMethod payment_method;
  private List<ContactDTO> contacts;
  private AccountAddressesDTO addresses;
  private AdditionalDocuments additional_documents;

  public AccountTagusDTO(Account account) {
    this.id = account.UUID__c != null ? account.UUID__c : new Uuid().getValue();
    this.parent_id = account?.Parent?.UUID__c;
    this.legal_document_type = account.Legal_Document_Type__c;
    this.legal_document = account.Id_Company__c;
    this.legal_name = account.Razao_Social__c;
    this.trade_name = account.Name;
    this.country_id = account.BillingCountryCode;
    this.salesforce_id = account.Id;
    this.business_unit = account.Business_Unit__c;
    this.phone = account.Phone;
    this.account_type = account.RecordType.DeveloperName;
    this.billing_email = account.Email__c;
    this.currency_id = account.CurrencyIsoCode;
    this.government_company = account.Is_Government_Company__c;
    this.payment_method = getPaymentMethod(account);

    this.contacts = new List<ContactDTO>();

    for (Contact contact : account.Contacts) {
      ContactDTO contactDTO = new ContactDTO(contact);

      this.contacts.add(contactDTO);
    }

    AccountAddressDTO billingAddressDTO = new AccountAddressDTO();
    billingAddressDTO.addressee = account.Razao_Social__c;
    billingAddressDTO.attention = account.Attention__r.Name;
    billingAddressDTO.street = account.BillingStreet;
    billingAddressDTO.city = account.BillingCity;
    billingAddressDTO.country_id = account.BillingCountryCode;
    billingAddressDTO.postal_code = account.BillingPostalCode;
    billingAddressDTO.state = account.BillingState;
    billingAddressDTO.district = account.District__c;
    billingAddressDTO.complements = account.Complements__c;

    this.addresses = new AccountAddressesDTO();
    this.addresses.billing = billingAddressDTO;

    this.additional_documents = new AdditionalDocuments();
    this.additional_documents.br_state_inscription = account.BR_State_Inscription__c;
    this.additional_documents.br_municipal_inscription = account.BR_Municipal_Inscription__c;
    this.additional_documents.ar_responsible_type = getAR_ResponsibleType(
      account
    );
    this.additional_documents.cl_economic_activity = account.CL_Economic_Activity__c;
    this.additional_documents.mx_foreign_company = account.MX_Foreign_Company__c;
    this.additional_documents.usa_eu_taxable = account.USA_EU_IsTaxable__c;
    this.additional_documents.usa_eu_exemption_certificate = account.USA_EU_Exemption_Cetificate__c;
  }

  public PaymentMethod getPaymentMethod(Account account) {
    switch on account.Payment_Method__c {
      when 'Boleto' {
        return PaymentMethod.BOLETO;
      }
      when 'Wire Transfer' {
        return PaymentMethod.WIRE_TRANSFER;
      }
      when 'TED' {
        return PaymentMethod.TED;
      }
      when 'Direct Debit' {
        return PaymentMethod.DIRECT_DEBIT;
      }
      when 'Credit Card' {
        return PaymentMethod.CREDIT_CARD;
      }
      when 'ACH' {
        return PaymentMethod.ACH;
      }
      when else {
        return PaymentMethod.CREDIT_CARD;
      }
    }
  }

  @SuppressWarnings('PMD.MethodNamingConventions')
  public AR_ResponsibleType getAR_ResponsibleType(Account account) {
    switch on account.AR_Responsible_Type__c {
      when 'Cliente del Exterior' {
        return AR_ResponsibleType.CLIENTE_DEL_EXTERIOR;
      }
      when 'Consumidor Final' {
        return AR_ResponsibleType.CONSUMIDOR_FINAL;
      }
      when 'IVA Liberado – Ley Nº 19.640' {
        return AR_ResponsibleType.IVA_LIBERADO;
      }
      when 'IVA no Responsable' {
        return AR_ResponsibleType.IVA_NO_RESPONSABLE;
      }
      when 'IVA Responsable Inscripto' {
        return AR_ResponsibleType.IVA_RESPONSABLE_INSCRIPTO;
      }
      when 'IVA Responsable Inscripto – Agente de Percepción' {
        return AR_ResponsibleType.IVA_RESPONSABLE_INSCRIPTO_AGENTE_DE_PERCEPCION;
      }
      when 'IVA Responsable no Inscripto' {
        return AR_ResponsibleType.IVA_RESPONSABLE_NO_INSCRIPTO;
      }
      when 'IVA Sujeto Exento' {
        return AR_ResponsibleType.IVA_SUJETO_EXENTO;
      }
      when 'Monotributista Social' {
        return AR_ResponsibleType.MONOTRIBUTISTA_SOCIAL;
      }
      when 'Pequeño Contribuyente Eventual' {
        return AR_ResponsibleType.PEQUENO_CONTRIBUYENTE_EVENTUAL;
      }
      when 'Pequeño Contribuyente Eventual Social' {
        return AR_ResponsibleType.PEQUENO_CONTRIBUYENTE_EVENTUAL_SOCIAL;
      }
      when 'Proveedor del Exterior' {
        return AR_ResponsibleType.PROVEEDOR_DEL_EXTERIOR;
      }
      when 'Responsable Monotributo' {
        return AR_ResponsibleType.RESPONSABLE_MONOTRIBUTO;
      }
      when 'Sujeto no Categorizado' {
        return AR_ResponsibleType.SUJETO_NO_CATEGORIZADO;
      }
      when else {
        return null;
      }
    }
  }

  public String getId() {
    return this.id;
  }

  public Id getSalesforceId() {
    return this.salesforce_id;
  }

  public List<ContactDTO> getContacts() {
    return this.contacts;
  }

  public String getParentId() {
    return this.parent_id;
  }

  public void setParentId(String parentId) {
    this.parent_id = parentId;
  }

  public static Map<Id, Account> getAccountMapWithFieldsToParse(
    List<Account> accounts
  ) {
    return new Map<Id, Account>(
      [
        SELECT
          Id,
          Name,
          BillingStreet,
          BillingCity,
          BillingState,
          BillingPostalCode,
          BillingCountryCode,
          CurrencyIsoCode,
          ParentId,
          Parent.UUID__c,
          Payment_Method__c,
          Phone,
          RecordType.DeveloperName,
          Attention__r.Name,
          Email__c,
          Id_Company__c,
          Is_Government_Company__c,
          Razao_Social__c,
          Business_Unit__c,
          Legal_Document_Type__c,
          UUID__c,
          BR_State_Inscription__c,
          BR_Municipal_Inscription__c,
          AR_Responsible_Type__c,
          CL_Economic_Activity__c,
          MX_Foreign_Company__c,
          USA_EU_IsTaxable__c,
          USA_EU_Exemption_Cetificate__c,
          District__c,
          Complements__c,
          (
            SELECT
              Id,
              FirstName,
              LastName,
              MailingStreet,
              MailingCity,
              MailingState,
              MailingPostalCode,
              MailingCountryCode,
              Email,
              Phone,
              Role__c,
              Job_Title__c,
              Latam_Is_Send_Email__c,
              Complements__c,
              District__c,
              UUID__c
            FROM Contacts
            WHERE Status_do_contato__c = 'Ativo'
          )
        FROM Account
        WHERE Id IN :accounts
        WITH SECURITY_ENFORCED
      ]
    );
  }

  public class ContactDTO {
    private String id;
    private String salesforce_id;
    private String email;
    private String phone;
    private String first_name;
    private String last_name;
    private String job_title;
    private Boolean latam_send_email;
    private List<String> roles;
    private AddressDTO address;

    private ContactDTO(Contact contact) {
      this.id = contact.UUID__c != null
        ? contact.UUID__c
        : new Uuid().getValue();
      this.salesforce_id = contact.Id;
      this.email = contact.Email;
      this.first_name = contact.FirstName;
      this.last_name = contact.LastName;
      this.phone = contact.Phone;
      this.job_title = contact.Job_Title__c;
      this.latam_send_email = contact.Latam_Is_Send_Email__c;

      this.roles = new List<String>();

      for (String role : contact.Role__c?.split(';')) {
        roles.add('clients:' + role.toLowerCase());
      }

      this.address = new AddressDTO();
      this.address.street = contact.MailingStreet;
      this.address.city = contact.MailingCity;
      this.address.country_id = contact.MailingCountryCode;
      this.address.postal_code = contact.MailingPostalCode;
      this.address.state = contact.MailingState;
      this.address.district = contact.District__c;
      this.address.complements = contact.Complements__c;
    }

    public String getId() {
      return this.id;
    }

    public Id getSalesforceId() {
      return this.salesforce_id;
    }
  }

  private class AccountAddressesDTO {
    private AccountAddressDTO billing;
    private AccountAddressDTO mailing;
  }

  private class AccountAddressDTO extends AddressDTO {
    private String addressee;
    private String attention;
  }

  private virtual class AddressDTO {
    private String street;
    private String city;
    private String country_id;
    private String postal_code;
    private String district;
    private String complements;
    private String state;
  }

  private class AdditionalDocuments {
    private String br_state_inscription;
    private String br_municipal_inscription;
    private AR_ResponsibleType ar_responsible_type;
    private String cl_economic_activity;
    private Boolean mx_foreign_company;
    private Boolean usa_eu_taxable;
    private String usa_eu_exemption_certificate;
  }

  private enum PaymentMethod {
    BOLETO,
    WIRE_TRANSFER,
    TED,
    DIRECT_DEBIT,
    CREDIT_CARD,
    ACH
  }

  private enum AR_ResponsibleType {
    CLIENTE_DEL_EXTERIOR,
    CONSUMIDOR_FINAL,
    IVA_LIBERADO,
    IVA_NO_RESPONSABLE,
    IVA_RESPONSABLE_INSCRIPTO,
    IVA_RESPONSABLE_INSCRIPTO_AGENTE_DE_PERCEPCION,
    IVA_RESPONSABLE_NO_INSCRIPTO,
    IVA_SUJETO_EXENTO,
    MONOTRIBUTISTA_SOCIAL,
    PEQUENO_CONTRIBUYENTE_EVENTUAL,
    PEQUENO_CONTRIBUYENTE_EVENTUAL_SOCIAL,
    PROVEEDOR_DEL_EXTERIOR,
    RESPONSABLE_MONOTRIBUTO,
    SUJETO_NO_CATEGORIZADO
  }
}