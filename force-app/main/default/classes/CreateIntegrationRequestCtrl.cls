public class CreateIntegrationRequestCtrl {
	
    @AuraEnabled
    public static Id createIntegrationRequest(String recordId) {
        Id sObjectId = (Id)recordId; 
        
        TagusIntegrationController.TagusIntegrationCtlrResponse tagusIntegrationResponse = null;
        
        if (sObjectId.getSobjectType() == Schema.Sales_Order__c.SObjectType)  {
            List<Id> recordIdList = new List<Id>{sObjectId};
            
            tagusIntegrationResponse = TagusIntegrationController.startIntegrationRequestCreation(recordIdList, 'Lightning Component');
        }
        
        if (tagusIntegrationResponse.integrationRequests.size() == 1) {
			return tagusIntegrationResponse.integrationRequests.get(0).Id;									
        } else {
            return null;
        }
    }
    
    public class Response {
		@AuraEnabled public String status;
 		@AuraEnabled public String errorMessage;
        @AuraEnabled public String integrationRequestId;
    }
}