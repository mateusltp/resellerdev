public with sharing class UpsertClientOrderService {
  private List<Opportunity> opportunities;

  public UpsertClientOrderService(List<Opportunity> opportunities) {
    this.opportunities = opportunities;
  }

  public void execute() {
    if (
      opportunities.isEmpty() ||
      UserInfo.getName().equals('Integration RH Portal')
    ) {
      return;
    }

    List<ClientDealBO> clientDeals = ClientDealDAO.getClientDealsByOpportunities(
      opportunities
    );

    List<Order> orders = new List<Order>();
    List<OrderItem> ordersItems = new List<OrderItem>();
    List<SObject> comercialSettings = new List<SObject>();

    for (ClientDealBO clientDeal : clientDeals) {
      Opportunity opportunity = clientDeal.getOpportunity();
      Quote quote = clientDeal.getQuote();

      Order order = new Order(
        Opportunity = opportunity,
        UUID__c = getOrderUUID(clientDeal),
        Version__c = getOrderVersion(clientDeal)
      );

      if (clientDeal.getOrder() == null) {
        order.Pricebook2Id = opportunity.Pricebook2Id;
      }

      if (getOrderStatus(opportunity).equals('Canceled')) {
        order.Cancelation_Date__c = opportunity.cancellation_date__c;
        order.Cancelation_Reason__c = opportunity.Loss_Reason__c;
        order.Cancelation_Description__c = opportunity.Cancelation_Description__c;
      } else {
        order.AccountId = opportunity.AccountId;
        order.ContractId = quote.ContractId;
        order.CurrencyIsoCode = opportunity.CurrencyIsoCode;
        order.EffectiveDate = quote.Start_Date__c;
        order.EndDate = quote.End_Date__c;
        order.OpportunityId = opportunity.Id;
        order.Opportunity = opportunity;
        order.QuoteId = quote.Id;
        order.Status = 'Draft';
        order.Type = 'Clients';
        order.Contact_Permission__c = quote.Contact_Permission__c;
        order.Employee_Registration_Method__c = quote.Employee_Registration_Method__c;
        order.Gympass_Entity__c = opportunity.Gympass_Entity__c;
        order.Unique_Identifier__c = quote.Unique_Identifier__c;
        order.Payment_Due_Days__c = opportunity.Payment_Due_Days__c;
        order.Billing_Period__c = opportunity.Billing_Period__c;
        order.Billing_Day__c = opportunity.Billing_Day__c;
        order.Cutoff_Day__c = opportunity.Cutoff_Day__c;
      }

      orders.add(order);

      if (getOrderStatus(opportunity).equals('Canceled')) {
        continue;
      }

      for (ClientDealBO.SalesItem salesItem : clientDeal.getSalesItems()) {
        QuoteLineItem quoteLineItem = salesItem.getQuoteLineItem();

        OrderItem orderItem = new OrderItem(
          Order = new Order(UUID__c = order.UUID__c),
          PricebookEntryId = quoteLineItem.PricebookEntryId,
          Product2Id = quoteLineItem.Product2Id,
          Quantity = quoteLineItem.Quantity,
          QuoteLineItemId = quoteLineItem.Id,
          UnitPrice = quoteLineItem.UnitPrice,
          Contract_Type__c = quoteLineItem.Fee_Contract_Type__c,
          Discount__c = quoteLineItem.Discount,
          Discount_Price_Ceiling__c = quoteLineItem.Discount_Price_Ceiling__c,
          Discount_Price_Floor__c = quoteLineItem.Discount_Price_Floor__c,
          Discount_Price_Unit_Decrease__c = quoteLineItem.Discount_Price_Unit_Decrease__c,
          Flat_Baseline_Adjustment__c = quoteLineItem.Flat_Baseline_Adjustment__c,
          Inflation_Adjustment_Index__c = quoteLineItem.Inflation_Adjustment_Index__c,
          Inflation_Adjustment_Period__c = quoteLineItem.Inflation_Adjustment_Period__c,
          Recurring_Billing_Period__c = quoteLineItem.Recurring_Billing_Period__c,
          Variable_Price_Ceiling__c = quoteLineItem.Variable_Price_Ceiling__c,
          Variable_Price_Floor__c = quoteLineItem.Variable_Price_Floor__c,
          Variable_Price_Unit_Increase__c = quoteLineItem.Variable_Price_Unit_Increase__c,
          Type__c = quoteLineItem.Fee_Type__c,
          Payment_Due_Days__c = quoteLineItem.Payment_Due_Days__c,
          Cutoff_Day__c = quoteLineItem.Cutoff_Day__c,
          Billing_Day__c = quoteLineItem.Billing_Day__c,
          Billing_Date__c = quoteLineItem.Start_Date__c,
          Start_Date__c = quoteLineItem.Start_Date__c,
          End_Date__c = quoteLineItem.End_Date__c,
          Order_Version__c = order.Version__c,
          UUID__c = quoteLineItem.UUID__c != null
            ? quoteLineItem.UUID__c
            : new Uuid().getValue()
        );

        ordersItems.add(orderItem);

        for (Waiver__c waiver : salesItem.getWaivers()) {
          if (waiver.Order_Item__c == null) {
            waiver.Order_Item__r = new OrderItem(UUID__c = orderItem.UUID__c);
          }

          if (String.isBlank(waiver.UUID__c)) {
            waiver.UUID__c = new Uuid().getValue();
          }

          comercialSettings.add(waiver);
        }
      }

      for (
        ClientDealBO.PaymentItem paymentItem : clientDeal.getPaymentItems()
      ) {
        Payment__c payment = paymentItem.getPayment();

        if (payment.Order__c == null) {
          payment.Order__r = new Order(UUID__c = order.UUID__c);
        }

        payment.Order_Version__c = order.Version__c;

        if (String.isBlank(payment.UUID__c)) {
          payment.UUID__c = new Uuid().getValue();
        }

        comercialSettings.add(payment);

        for (Eligibility__c eligibility : payment.Eligibility__r) {
          if (String.isBlank(eligibility.UUID__c)) {
            eligibility.UUID__c = new Uuid().getValue();

            comercialSettings.add(eligibility);
          }
        }
      }
    }

    Database.upsert(orders, Schema.Order.UUID__c);

    if (!ordersItems.isEmpty()) {
      Database.upsert(ordersItems, Schema.OrderItem.UUID__c);
    }

    if (!comercialSettings.isEmpty()) {
      Database.upsert(comercialSettings);
    }

    for (Order order : orders) {
      order.Status = getOrderStatus(order.Opportunity);

      if (!isToCancelOrder(order)) {
        order.Status = 'Activated';
      }

      if (UserInfo.getName().equals('Integration SMB Jamor')) {
        order.Is_Order_On_Tagus__c = true;
      }
    }

    Database.update(orders);
  }

  private Double getOrderVersion(ClientDealBO clientDeal) {
    Order order = clientDeal.getOrder();
    Opportunity opportunity = clientDeal.getOpportunity();

    if (order != null && order.Version__c != null) {
      if (getOrderStatus(opportunity).equals('Canceled')) {
        return order.Version__c;
      }

      return order.Version__c + 1;
    }

    return 0;
  }

  private String getOrderUUID(ClientDealBO clientDeal) {
    Opportunity opportunity = clientDeal.getOpportunity();

    if (String.isNotBlank(opportunity.UUID__c)) {
      return opportunity.UUID__c;
    }

    Order order = clientDeal.getOrder();

    if (order != null && String.isNotBlank(order.UUID__c)) {
      return order.UUID__c;
    }

    return new Uuid().getValue();
  }

  private String getOrderStatus(Opportunity opportunity) {
    switch on opportunity.StageName {
      when ('Lançado/Ganho') {
        return 'Activated';
      }
      when ('Perdido') {
        return 'Canceled';
      }
      when else {
        return 'Draft';
      }
    }
  }

  private Boolean isToCancelOrder(Order order) {
    return order.Status == 'Canceled' &&
      order.Cancelation_Date__c != null &&
      order.Cancelation_Date__c <= Date.today();
  }
}