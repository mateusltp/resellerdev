/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 08-13-2021
 * @last modified by  : gft.jorge.stevaux@ext.gympass.com
 * Modifications Log 
 * Ver   Date         Author                             Modification
 * 1.0   05-26-2021   gft.samuel.silva@ext.gympass.com   Initial Version
**/
@isTest(seeAllData=false)
public with sharing class PartnerAccountStatusTest {
    private static Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gym_Partner').getRecordTypeId();
    private static Id lOppRt = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Partner_Small_and_Medium_Renegotiation').getRecordTypeId();
    private static Id ctcRtId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Gyms_Partner').getRecordTypeId();
    private static Id quoteRtId = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Gyms_Quote_Partner').getRecordTypeId();
    private static Id recordTypePartnerForm = Schema.SObjectType.Ops_Setup_Validation_Form__c.getRecordTypeInfosByDeveloperName().get('Partner_Ops_Setup_Validation').getRecordTypeId();
    private static Id recordTypeIdBank = Schema.SObjectType.Bank_Account__c.getRecordTypeInfosByDeveloperName().get('Gyms_Bank_Account').getRecordTypeId();
    private static Id recordTypeIdAccBank = Schema.SObjectType.Acount_Bank_Account_Relationship__c.getRecordTypeInfosByDeveloperName().get('Gyms_Account_Bank_Account_Partner').getRecordTypeId();    
    private static Id recordTypeIdProductItem = Schema.SObjectType.Product_Item__c.getRecordTypeInfosByDeveloperName().get('Gyms_Product_Item_Partner').getRecordTypeId();
    

    @TestSetup
    static void setupData(){
       
        Account acc = generatePartnerAccount();
        insert acc;        
        Contact partnerContact = generatePartnerContact(acc);
        insert partnerContact;     

        Pricebook2 standardPricebook = new Pricebook2(
            Id = Test.getStandardPricebookId(),
            IsActive = true
        );
        update standardPricebook;
        Pricebook2 pb = standardPricebook;  

        Opportunity opp = generateOpportunity(acc,pb);
        insert opp;	
        
        Account_Opportunity_Relationship__c lAccOppRelationship = generateAccOppRel(opp, acc);
        insert lAccOppRelationship;

        Quote proposal = generateProposal(opp);
        insert proposal;
                      
        Product_Item__c prod = generateProductItem(opp);
        insert prod;

        Ops_Setup_Validation_Form__c accOppForm = generateOpsValidationForm(opp);
        insert accOppForm;        
        
        /**Bank Account*/
        Bank_Account__c bankAccount = generateBankAccount();
        insert bankAccount;
        
        Acount_Bank_Account_Relationship__c abr = generateAccBankRel(bankAccount, acc);
        insert abr;
    }
    
	@isTest
    private static void cancelAccounts(){
        Opportunity opp = [SELECT Id, AccountId,Account.Cancellation_Reason__c,Account.GP_Status__c,Account.GP_Blacklist__c, StageName,Loss_Reason__c, Cancellation_Reason__c FROM Opportunity LIMIT 1];        
        Test.startTest();
            opp.StageName = 'Perdido';
            opp.Loss_Reason__c = 'Fear of cannibalization';
            update opp;
            Account acc = [SELECT Id, Cancellation_Reason__c,GP_Status__c,GP_Blacklist__c FROM Account LIMIT 1];        
            System.assertEquals('Cancelled', acc.GP_Status__c);
            //System.assertEquals(true, acc.GP_Blacklist__c);
            System.assertEquals(opp.Cancellation_Reason__c, acc.Cancellation_Reason__c);            
        Test.stopTest();             
    }    

    @isTest
    private static void cancelAccountsWithIntegrityError(){
        Opportunity opp = [SELECT Id, AccountId,Account.Cancellation_Reason__c,Account.GP_Status__c,Account.GP_Blacklist__c, StageName,Loss_Reason__c, Cancellation_Reason__c FROM Opportunity LIMIT 1];        
        Test.startTest();
            try {
                opp.StageName = 'Perdido';
                opp.Loss_Reason__c = 'Fear of cannibalization';
                opp.Cancellation_Reason__c = '';
                update opp;
            } catch (Exception e){
                System.assert(e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'));       
            }
        Test.stopTest();             
    }    

    private static Account generatePartnerAccount(){
       Account lAcc = new Account();
        lAcc.name='AcademiaBrasilCompanyPai';
        lAcc.RecordTypeId = rtId;
        lAcc.GP_Status__c = 'Active';
        lAcc.billingState = 'Minas Gerais';
        lAcc.CAP_Value__c = 120;
        lAcc.BillingCity = 'CityAcademiaBrasil';
        lAcc.billingStreet = 'Rua academiabrasilpai';
        lAcc.billingCountry = 'Brazil';
        lAcc.ShippingCountry ='Brazil';
        lAcc.ShippingStreet ='Dona Maria';
        lAcc.ShippingCity ='Uberlandia';
        lAcc.Gym_Type__c = 'Studios';
        lAcc.Gym_Classes__c = 'Cardio';
        lAcc.Legal_Title__c = '11111';
        lAcc.Id_Company__c = '11111';
        lAcc.Types_of_ownership__c = Label.franchisePicklist;
        lAcc.Subscription_Type__c = 'Value per class';
        lAcc.Subscription_Period__c = 'Monthy value';
        lAcc.Subscription_Type_Estimated_Price__c    = 100;
        lAcc.Has_market_cannibalization__c = 'No';
        lAcc.Exclusivity__c = 'Yes';
        lAcc.Exclusivity_End_Date__c = Date.today().addYears(1);
        lAcc.Exclusivity_Partnership__c = 'Full Exclusivity';
        lAcc.Exclusivity_Restrictions__c= 'No';
        lAcc.Website = 'testing@tesapex.com';
        lAcc.Gym_Email__c = 'gymemail@apex.com';
        lAcc.Phone = '3222123123';
        lAcc.Can_use_logo__c = 'Yes';
        lAcc.Legal_Registration__c = 12123;
        lAcc.Legal_Title__c = 'Title LTDA';
        lAcc.Gyms_Identification_Document__c = 'CNPJ'; 
        return lAcc;
    }
    
    private static Opportunity generateOpportunity(Account lAcc, Pricebook2 lPricebook){
       Opportunity lAccOpp = new Opportunity();
        lAccOpp.CurrencyIsoCode='BRL';
        lAccOpp.recordTypeId = lOppRt;
        lAccOpp.AccountId = lAcc.id;
        lAccOpp.Sub_Type__c = 'Retention';
        lAccOpp.Name = lAcc.Id; 
        lAccOpp.CMS_Used__c = 'Yes';     
        lAccOpp.Gym_agreed_to_an_API_integration__c  = 'Yes';
        lAccOpp.Club_Management_System__c = 'Companhia Athletica';
        lAccOpp.Integration_Fee_Deduction__c = 'No';
        lAccOpp.CloseDate = Date.today();
        lAccOpp.Success_Look_Like__c = 'Yes';
        lAccOpp.Success_Look_Like_Description__c = 'Money money';
        lAccOpp.Cancellation_Reason__c = 'Revenue Dissatisfaction';
        lAccOpp.Cancellation_Reason_subcategory__c = 'Payment Cap';
        lAccOpp.StageName = 'Proposta Enviada';
        lAccOpp.Type = 'Expansion';  
        lAccOpp.Country_Manager_Approval__c = true;
        lAccOpp.Payment_approved__c = true;   
        lAccOpp.CurrencyIsoCode = 'BRL';
        lAccOpp.Gympass_Plus__c = 'Yes';
        lAccOpp.Standard_Payment__c = 'Yes';
        lAccOpp.Request_for_self_checkin__c = 'Yes';  
        lAccOpp.Pricebook2Id = lPricebook.Id;
        lAccOpp.Training_Comments__c = 'teste';
        lAccOpp.Should_OPS_team_send_the_training__c = 'Standard Training';
        return lAccOpp;
    }
    
    private static Pricebook2 generatePricebook(){        
        Pricebook2 pb = new Pricebook2();
        pb.Name = 'Br';
        pb.Country__c = 'Brazil';
        pb.IsActive = true;
        return pb;
    }
    
    private static PricebookEntry generatePricebookEntry(Pricebook2 pb, Product2 product, Opportunity opp){        
        PricebookEntry pbEntry = new PricebookEntry();
        pbEntry.Product2Id = product.Id;
        pbEntry.Pricebook2Id = Test.getStandardPricebookId();
        pbEntry.UnitPrice = 100;
        pbEntry.IsActive = true;
        pbEntry.CurrencyIsoCode = opp.CurrencyIsoCode;
        return pbEntry;
    }
    
    private static Product2 generateProduct(String ProductName){
        Product2 product = new Product2();
        product.Name = ProductName;
        product.Minimum_Number_of_Employees__c = 0;
        product.Maximum_Number_of_Employees__c = 900;
        product.Family = ProductName;
        product.IsActive = true;
        return product;
    }

    private static Contact generatePartnerContact(Account acc) {
        return new Contact(
            FirstName = 'Fernando',
            LastName = 'Souza',
            Email = 'fernandoTestSouze@mail.com',
            Phone = '111111111',
            recordTypeId = ctcRtId,
            MailingCountry = 'Brazil',
            Type_of_contact__c = 'Point of Contact;Legal Representative',
            Cargo__c = 'CEO',            
            accountId = acc.id
        );       

    }

    private static Account_Opportunity_Relationship__c generateAccOppRel(Opportunity opp, Account acc){
        Account_Opportunity_Relationship__c lAccOppRelationship = new Account_Opportunity_Relationship__c();
        lAccOppRelationship.Opportunity__c = opp.Id;
        lAccOppRelationship.Account__c = acc.Id;
        return lAccOppRelationship;
    }

    private static Quote generateProposal(Opportunity opp){
        Quote proposal = new Quote();
            proposal.RecordTypeId = quoteRtId;
            proposal.Name = 'academiaBrasilCompanyQuote';
            proposal.Status = 'Ganho';
            proposal.Signed__c = Date.today();
            proposal.Start_Date__c  = Date.today();
            proposal.Final_Date__c =  Date.today().addYears(4);
            proposal.Priority_Tier__c = 'Tier 1';
            proposal.PR_Activity__c  = 'Yes';
            proposal.discounts_usage_volume_range__c = 'Yes';
            proposal.Threshold_Value_1__c = 12;
            proposal.Threshold_Value_2__c = 32;                         
            proposal.Threshold_Value_3__c = 42;                
            proposal.Threshold_Value_4__c = 48;                               
            proposal.First_Discount_Range__c = 20;
            proposal.Second_Discount_Range__c = 24; 
            proposal.Third_Discount_Range__c = 56;
            proposal.Fourth_Discount_Range__c = 70;
            proposal.Volume_Discount_Type__c = 'By single user volume';
            proposal.OpportunityId = opp.Id;       
        return proposal;
    }

    private static Product_Item__c generateProductItem(Opportunity opp){
        Product_Item__c prodItem = new Product_Item__c();
        prodItem.recordTypeId = recordTypeIdProductItem;
        prodItem.Opportunity__c = opp.id;
        prodItem.Product_Type__c = 'In person';
        prodItem.Is_Network_CAP__c = 'No';
        prodItem.CAP_Value__c = 100;
        prodItem.CurrencyIsoCode = 'BRL';
        prodItem.Do_You_Have_A_No_Show_Fee__c = 'No';
        prodItem.Has_Late_Cancellation_Fee__c = 'No';
        prodItem.Late_Cancellation_Percent__c = '0';
        prodItem.Maximum_Live_Class_Per_Month__c = 0;
        prodItem.No_Show_Fee_Percent__c = '0';
        prodItem.Gym_Activity_Concat__c = 'Dance';
        prodItem.Product_Definition__c  ='Dança';
        prodItem.Product_Restriction__c = 'None';
        prodItem.Name = 'Dança'; 
        prodItem.Package_Type__c = 'Value per class';      
        prodItem.Max_Monthly_Visit_Per_User__c = 12;
        prodItem.Reference_Price_Value_Unlimited__c = 1;
        prodItem.Net_Transfer_Price__c  = 11;
        prodItem.Price_Visits_Month_Package_Selected__c = 400;
        prodItem.Selected_Plan__c = 'Gold';
        prodItem.Justification_Plan__c = 'Fit';
        return prodItem;        
    }
    
    private static Ops_Setup_Validation_Form__c generateOpsValidationForm(Opportunity opp) {
        return new Ops_Setup_Validation_Form__c (
        recordTypeId = recordTypePartnerForm,
        Name = opp.Name + ' - OPS Form',
        Status__c = 'Approved',
        Did_you_send_the_training_email__c = 'Yes',
        Opportunity__c = opp.Id ); 
    }

    private static Bank_Account__c generateBankAccount() {
        Bank_Account__c bankAccount = new Bank_Account__c();
        bankAccount.recordTypeId = recordTypeIdBank;
        bankAccount.Bank_Account_Number_or_IBAN__c = 'abc';
        bankAccount.Bank_Account_Ownership__c = 'Contact text';
        bankAccount.Bank_Name__c = 'ItauFake';
        bankAccount.BIC_or_Routing_Number_or_Sort_Code__c = 'abc';
        bankAccount.Routing_Number__c = 1234;
        bankAccount.VAT_Number_or_UTR_number__c = 'tttt1231';
        return bankAccount;
    }

    private static Acount_Bank_Account_Relationship__c generateAccBankRel(Bank_Account__c bankAccount, Account acc){
        Acount_Bank_Account_Relationship__c abr = new Acount_Bank_Account_Relationship__c();
        abr.Account__c = acc.Id;
        abr.Bank_Account__c = bankAccount.id;
        abr.RecordTypeId = recordTypeIdAccBank;
        return abr;
    }
}