public class ManagedSwapClauseTriggerHelper {
   
    public void createManagedClauses(Map<id, sObject> ManagedSwapClauseMap){

        Set<Id> ContractAgreementSetId = new Set<Id>();
        for (ManagedSwapClause__c managedSwapClause : (List<ManagedSwapClause__c>) ManagedSwapClauseMap.values()){
            ContractAgreementSetId.add(managedSwapClause.ContractAgreement__c);
        }

        Database.DeleteResult[] deleteManagedSwapClausesList = Database.delete([SELECT Id FROM ManagedClause__c WHERE ContractAgreement__c IN : ContractAgreementSetId], true);

        Database.DeleteResult[] deleteManagedSwapClausesList2 = Database.delete([SELECT Id FROM SubManagedClauses__c WHERE ManagedClause__c IN  
                                                                                                    (SELECT Id 
                                                                                                    FROM ManagedClause__c 
                                                                                                    WHERE ContractAgreement__c 
                                                                                                    IN : ContractAgreementSetId) ], true);

        List<APXT_Redlining__Contract_Agreement__c> contractAgreementList = [SELECT Id, 
                                                                                    Template_Group__c, 
                                                                                    Template_Selection__c ,
                                                                                    Include_full_T_C_s__c 
                                                                                    FROM 
                                                                                    APXT_Redlining__Contract_Agreement__c 
                                                                                    WHERE Include_full_T_C_s__c = true 
                                                                                    AND Id 
                                                                                    IN : ContractAgreementSetId] ;
        new ManagedClauseHelper(contractAgreementList);


    }
}