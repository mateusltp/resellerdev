/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 04-14-2022
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/
global with sharing class PartnerProductDataProvider implements sortablegrid.sdgIDataProvider {

    private static final PS_Constants constants = PS_Constants.getInstance();
    static Id opportunityRT = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(constants.OPPORTUNITY_RT_PARTNER_FLOW_OPP).getRecordTypeId();
    static Id opportunityChildRT = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(constants.OPPORTUNITY_RT_PARTNER_FLOW_CHILD_OPP).getRecordTypeId();

    global static Boolean isUserSelectable()
    {
        System.debug('$$$$$');
        return true;
    }

    global static sortablegrid.SDGResult getData(sortablegrid.SDG coreSDG,  sortablegrid.SDGRequest request)
    {
        sortablegrid.SDGResult result = new sortablegrid.SDGResult();
        
        Map<Id, Product_Opportunity_Assignment__c> prodAssignMap = new Map<Id, Product_Opportunity_Assignment__c>();

        AccountOpportunitySelector accOppSelector = (AccountOpportunitySelector)Application.Selector.newInstance(Account_Opportunity_Relationship__c.sobjectType);
        List<Product_Opportunity_Assignment__c> paLst = accOppSelector.selectProductAssignmentToOpportunitySDGGrid(new Set<Id>{request.ParentRecordID}).get(request.ParentRecordID);

        OpportunitySelector oppSelector = (OpportunitySelector)Application.Selector.newInstance(Opportunity.SObjectType);
        Opportunity opp = oppSelector.selectOpportunityForPartnerProductFlow(new Set<Id>{request.ParentRecordID}).get(0);
        if (paLst != null) {
            for(Product_Opportunity_Assignment__c prod : paLst){
                if (prod.OpportunityMemberId__r.Account__c == opp.AccountId && prod.ProductAssignmentId__r.ProductId__c != null ){ 
                    prodAssignMap.put(prod.ProductAssignmentId__r.ProductId__c, prod);
                } 
            }
        }

        result.data = new List<Sobject>();

        result.data.addAll((List<sObject>)prodAssignMap.values());
        result.FullQueryCount = result.data.size();
        result.pagecount = 1;
        result.isError = false;
        result.ErrorMessage = '';
        return result;
    }

    global sortablegrid.SDG LoadSDG(String SDGTag, String ParentRecordId)
    {   
        OpportunitySelector oppSelector = (OpportunitySelector)Application.Selector.newInstance(Opportunity.SObjectType);
        List<Opportunity> opps = oppSelector.selectOpportunityForPartnerProductFlow(new Set<Id>{ParentRecordId});
        Opportunity opp = oppSelector.selectOpportunityForPartnerProductFlow(new Set<Id>{ParentRecordId}).get(0);
        ID oppRT = opp.RecordTypeId;

        sortablegrid.SDG CoreSDG = new sortablegrid.SDG( 'teste' );

        CoreSDG.SDGActions = new List<sortablegrid.SDGAction>();
        CoreSDG.SDGFields = GetFields(oppRT);
        CoreSDG.SDGActions = GetActions(oppRT);
        return CoreSDG;
    }

   
    private List<sortablegrid.SDGField> GetFields(Id oppRT)
    {
        List<sortablegrid.SDGField> fields = new List<sortablegrid.SDGField>();

        fields.add( new sortablegrid.SDGField('1', 'Name', 'ProductAssignmentId__r.ProductId__r.Name', 'STRING', '', false, false, null, 1));    
        return fields;
    }

    global List<sortablegrid.SDGAction> GetActions(Id oppRT)
   {

       List<sortablegrid.SDGAction> actions = new List<sortablegrid.SDGAction>();
       if(oppRT == opportunityRT){
            actions.add(new sortablegrid.SDGAction('1', 'New', 'e.c:NewPartnerProductEvt', 'List Button', '{}', '', 1));
       }
       
       return actions;
   }

}