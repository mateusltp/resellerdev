/**
 * @File Name          : LeadTriggerScoringController.cls
 * @Description        : 
 * @Author             : David Mantovani - DDMA@gft.com
 * @Group              : 
 * @Last Modified By   : David Mantovani - DDMA@gft.com
 * @Last Modified On   : 08-05-2020 
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    29/06/2020   Samuel Silva - GFT (slml@gft.com)     Initial Version
**/
public without sharing class LeadTriggerScoringController {
    private Id rtIdLeadOwner = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Partner_Owner_Referral_Lead').getRecordTypeId();
    private Id rtIdScoreData = Schema.SObjectType.Score_Data__c.getRecordTypeInfosByDeveloperName().get('Partners').getRecordTypeId();
    private List<Lead> leadsTriggerLst = trigger.new;
    private List<Lead> leadsToScore = new List<Lead>();
    private List<Lead> leadRemovedAccLst = new List<Lead>();
    private Set<Id> accIdToUpdate = new Set<Id>();
    private Set<String> City = new Set<String>();
    private Set<String> Country = new Set<String>();
    private Set<Id> priorLeadsDup = new Set<Id>();

    /* 1) Separar em After insert e Update
       2) After insert -> roda score normal
       3) After update -> roda o score apenas pra leads que tiverem company name ou endereço diferente (old value x new value)
       4) Atualizar process builder lead para que não execute em leads not new;
    */

    public void executeJob(){       

        for(Lead l : leadsTriggerLst) {
            if(l.recordTypeId == rtIdLeadOwner){
                if(Trigger.isInsert){
                    leadsToScore.add(l);
                    City.add(l.City);
                    Country.add(l.Country);
                }else if(Trigger.isUpdate){     
                    Lead oldLead = (Lead)Trigger.oldMap.get(l.id);                   
                    if( l.Street != oldLead.Street || l.City != oldLead.City || l.State != oldLead.State || 
                       l.Country != oldLead.Country || l.PostalCode != oldLead.PostalCode || l.Company != oldLead.Company){
                           
                        if(oldLead.Referral_Account__c != l.Referral_Account__c){                     
                        	accIdToUpdate.add(oldLead.Referral_Account__c);
                        	leadRemovedAccLst.add(oldLead);
                    	}
                        Set<Id> tempPriorDup = DuplicateHandler.foundLeadDuplicateLeads(oldLead);
                        if(!tempPriorDup.isEmpty()){                      
                            priorLeadsDup.add(new List<ID>(tempPriorDup).get(0));
                        }
                        leadsToScore.add(l);
                        City.add(l.City);
                        Country.add(l.Country);
                    }
                }
            }
        }    

        if(!accIdToUpdate.isEmpty()){
            recalculateScoreForAccounts(leadRemovedAccLst);
        }


        if(!leadsToScore.isEmpty()){
            if(!priorLeadsDup.isEmpty()){
                leadsToScore.addAll([SELECT ID FROM LEAD WHERE ID IN: priorLeadsDup]);
            }
            
            try {            
                LeadTriggerScoringHelper leadScore = new LeadTriggerScoringHelper(getScoreMap(), getScoreData(), leadsToScore);           
                if( Limits.getQueueableJobs() != Limits.getLimitQueueableJobs() && !System.isQueueable()) {
                    System.enqueueJob(leadScore);
                } else {
                    CalloutException y = new CalloutException('QUEUEABLE JOBS IS ' + Limits.getQueueableJobs() + ' WHERE MAX IS ' + Limits.getLimitQueueableJobs());
                    System.debug('CalloutException ' + y);
                }
            } catch(Exception e) {
                System.debug( e.getMessage());
            }
        }
              
    }
    public Map<String,Score_Data__c> getScoreData(){
        Map<String,Score_Data__c> scoreDataMap = new Map<String,Score_Data__c>();

        List<Score_Data__c> scoreDataLst = [ SELECT Id, Country_Score_Data__c, State_Score_Data__c, 
                                        City_Score_Data__c, Number_Of_Active_Users__c, 
                                        Number_Of_Sign_Ups__c 
                                        FROM Score_Data__c WHERE City_Score_Data__c IN : City OR City_Score_Data__c = 'Standard'];
                                                          
        for(Score_Data__c scoreDt : scoreDataLst ){
            scoreDataMap.put(scoreDt.City_Score_Data__c + scoreDt.Country_Score_Data__c  , scoreDt);
        }
        System.debug('scoreDataLst ' + scoreDataLst.size());
        System.debug('scoreDataMap ' + scoreDataMap);
        
        return scoreDataMap;
                                
    }

    public Map<String, Lead_Score_by_Country__mdt> getScoreMap() {     
        Map<String, Lead_Score_by_Country__mdt> scoreMap = new Map<String,Lead_Score_by_Country__mdt>();
         
        List<Lead_Score_by_Country__mdt> scoreByCountryLst = [SELECT MasterLabel,  
                                    X1st_Range_of_Referral_Qty_Percentage__c, X1st_Range_of_Referral_Quantity__c,
                                    X2nd_Range_of_Referral_Qty_Percentage__c, X2nd_Range_of_Referral_Quantity__c,
                                    X3rd_Range_of_Referral_Qty_Percentage__c, X3rd_Range_of_Referral_Quantity__c,
                                    X4th_Range_of_Referral_Qty_Percentage__c, X4th_Range_of_Referral_Quantity__c,
                                    X5th_Range_of_Referral_Qty_Percentage__c, X5th_Range_of_Referral_Quantity__c,
                                    X6th_Range_of_Referral_Qty_Percentage__c, X6th_Range_of_Referral_Quantity__c,
                                    SignUp_Score_1__c, SignUp_Score_2__c, SignUp_Score_3__c, SignUp_Score_4__c, 
                                    SignUP_Value_1__c, SignUP_Value_2__c, SignUP_Value_3__c, SignUP_Value_4__c,
                                    Active_Score_1__c, Active_Score_2__c, Active_Score_3__c, Active_Score_4__c,
                                    Active_Value_1__c, Active_Value_2__c, Active_Value_3__c, Active_Value_4__c
                                    FROM Lead_Score_by_Country__mdt WHERE MasterLabel IN : Country 
                                    OR MasterLabel = 'Standard'
                            ];
        for(Lead_Score_by_Country__mdt score : scoreByCountryLst){
            scoreMap.put(score.MasterLabel, score);
        } 
        return scoreMap;
    }

    private void recalculateScoreForAccounts(List<Lead> leadRemovedAccLst) {
        List<Account> accLst = [SELECT Id, Referral_Score__c FROM Account WHERE ID IN: accIdToUpdate];
        for(Account acc : accLst){
            for(Lead l : leadRemovedAccLst){
                if(acc.id == l.Referral_Account__c){
                    acc.Referral_Score__c = l.Number_of_Referrals__c-1;
                    System.debug('acc.Referral_Score__c ' + acc.Referral_Score__c);
                }
            }
        }
        UPDATE accLst;
    }

}