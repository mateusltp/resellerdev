public without sharing class OrderTagusDTO {
  private String client_order_id;
  private String salesforce_id;
  private OrderStatus status;
  private String account_id;
  private String gympass_entity_id;
  private Datetime begin_date;
  private Datetime end_date;
  private Datetime cancelation_date;
  private String cancelation_reason;
  private String cancelation_description;
  private Integer payment_due_days;
  private BillingPeriod billing_period;
  private Integer billing_day;
  private Integer cutoff_day;
  private String currency_id;
  private OrderItemDTO order_items;
  private Boolean is_order_on_tagus;

  public OrderTagusDTO(Order order) {
    this.client_order_id = order.UUID__c != null
      ? order.UUID__c
      : new Uuid().getValue();
    this.salesforce_id = order.Id;
    this.status = getOrderStatus(order);
    this.account_id = order.Account_External_Id__c;
    this.gympass_entity_id = order.Gympass_Entity_External_Id__c;
    this.begin_date = (Datetime) order.EffectiveDate;
    this.end_date = (Datetime) order.EndDate;
    this.cancelation_date = order.Cancelation_Date__c;
    this.cancelation_reason = order.Cancelation_Reason__c != null
      ? order.Cancelation_Reason__c.toUpperCase().replaceAll(' ', '_')
      : null;
    this.cancelation_description = order.Cancelation_Description__c;
    this.payment_due_days = (Integer) order.Payment_Due_Days__c;
    this.billing_period = getBillingPeriod(order);
    this.billing_day = (Integer) order.Billing_Day__c;
    this.cutoff_day = (Integer) order.Cutoff_Day__c;
    this.currency_id = order.CurrencyIsoCode;
    this.order_items = new OrderItemDTO();
    this.is_order_on_tagus = order.Is_Order_On_Tagus__c;
  }

  private OrderStatus getOrderStatus(Order order) {
    switch on order.Status {
      when 'Activated' {
        return OrderStatus.ACTIVE;
      }
      when 'Inactivated' {
        return OrderStatus.INACTIVE;
      }
      when 'Cancel Requested' {
        return OrderStatus.CANCEL_REQUESTED;
      }
      when 'Canceled' {
        return OrderStatus.CANCELED;
      }
      when else {
        return null;
      }
    }
  }

  public BillingPeriod getBillingPeriod(Order order) {
    switch on order.Billing_Period__c {
      when 'Monthly' {
        return BillingPeriod.MONTH;
      }
      when 'Yearly' {
        return BillingPeriod.YEAR;
      }
      when 'Quarterly' {
        return BillingPeriod.QUARTER;
      }
      when else {
        return null;
      }
    }
  }

  public Boolean getIsOrderOnTagus() {
    return this.is_order_on_tagus;
  }

  public String getSalesforceId() {
    return this.salesforce_id;
  }

  public void setOrderItems(List<OrderItem> orderItems) {
    for (OrderItem orderItem : orderItems) {
      this.order_items.addSku(orderItem);
    }
  }

  public void setPayments(List<Payment__c> payments) {
    for (Payment__c payment : payments) {
      this.order_items.addPayment(payment);
    }
  }

  private class OrderItemDTO {
    private NonRecurringSkuDTO non_recurring;
    private RecurringSkuDTO recurring;

    private void addSku(OrderItem orderItem) {
      if (String.isBlank(orderItem.Type__c)) {
        return;
      }

      if (orderItem.Type__c.contains('Setup Fee')) {
        if (this.non_recurring == null) {
          this.non_recurring = new NonRecurringSkuDTO();
        }

        this.non_recurring.addSku(orderItem);
      }

      if (orderItem.Type__c.contains('Enterprise Subscription')) {
        if (this.recurring == null) {
          this.recurring = new RecurringSkuDTO();
        }

        if (orderItem.PlanCode__c.contains('with_Family'))
          this.recurring.addSkuFm(orderItem);
        else
          this.recurring.addSku(orderItem);
      }
    }

    private void addPayment(Payment__c payment) {
      if (String.isBlank(payment.RecordTypeId)) {
        return;
      }

      if (payment.RecordType.DeveloperName.equals('Non_Recurring')) {
        if (this.non_recurring == null) {
          this.non_recurring = new NonRecurringSkuDTO();
        }

        this.non_recurring.addPayment(payment);
      }

      if (payment.RecordType.DeveloperName.equals('Recurring')) {
        if (this.recurring == null) {
          this.recurring = new RecurringSkuDTO();
        }

        this.recurring.addPayment(payment);
      }
    }
  }

  private enum OrderStatus {
    ACTIVE,
    INACTIVE,
    CANCEL_REQUESTED,
    CANCELED
  }

  private enum BillingPeriod {
    MONTH,
    YEAR,
    DAY,
    WEEK,
    QUARTER,
    SEMIANNUAL
  }
}