public with sharing class LegalSendEmailNewCaseFile {

    public static void sendNotification( Set<Id> contractAgreement ) {              
        List<String> status = new List<String>{'In legal review', 'In legal approval', 'In finance approval', 'In business approval'};
        
        Id emailTemplateId = getEmailTemplate();
        Id orgWideEmailAddressId = getOrgWideEmailAddress();
        

        for( Id contractId : contractAgreement ){
            for( Case legalCase : getLegalCase ( contractAgreement ) ){
                if( legalCase.ContractAgreement__c == contractId && status.contains(legalCase.Status) ){
                    
                    Messaging.SingleEmailMessage email = Messaging.renderStoredEmailTemplate( emailTemplateId, legalCase.createdById, legalCase.Id );
                    
                    String emailSubject = email.getSubject();
                    String emailTextBody = email.getPlainTextBody();

                    email.setTargetObjectId(legalCase.createdById);
                    email.setSubject(emailSubject);
                    email.setPlainTextBody(emailTextBody);
                    email.saveAsActivity = false;                
                    //email.setOrgWideEmailAddressId(orgWideEmailAddressId);
                    email.setReplyTo('no-reply@gympass.com');
                    email.setsenderdisplayName('no-reply@gympass.com');
                    if( Test.isRunningTest() ){
                        email.toAddresses = new String[] {'test@test.com'};
                    }else{
                        email.toAddresses = new String[] {legalCase.Owner.Email};
                    }
                    
                    Messaging.SendEmailResult[] results = Messaging.sendEmail(new List<Messaging.Email>{email});

                    if (results[0].success) {
                        System.debug('The email was sent successfully.');
                    }
                    else {
                        System.debug('The email failed to send: ' + results[0].errors[0].message);
                    }
                }
            }
        }
    }

    public static List<Case> getLegalCase (Set<Id> contractAgreement){
        return [SELECT ContractAgreement__c, CreatedById, Owner.Email, Status FROM CASE WHERE ContractAgreement__c IN : contractAgreement];
    }

    public static Id getEmailTemplate(){
        return [SELECT Id FROM EmailTemplate WHERE DeveloperName = 'ETCaseNewFile'].Id;
    }

    public static Id getOrgWideEmailAddress(){
        return [SELECT Id, Address, DisplayName FROM OrgWideEmailAddress WHERE DisplayName = 'Gympass-Feedback'].Id;
    }


}