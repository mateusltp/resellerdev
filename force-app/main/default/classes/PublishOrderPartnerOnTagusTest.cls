/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 09-14-2021
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/
@isTest(seeAllData=false)
public class PublishOrderPartnerOnTagusTest {

    @TestSetup
    static void setupData(){
        String recordTypePartnerFlow = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Account').getRecordTypeId();
        Account partnerAcc = PartnerDataFactory.newAccount();
        partnerAcc.RecordTypeId = recordTypePartnerFlow;
        partnerAcc.CRM_ID__c = '1231';
        partnerAcc.UUID__c = new Uuid().getValue();
        INSERT partnerAcc;
             
        String recordTypePartnerContactFlow = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Contact').getRecordTypeId();
        Contact partnerContact = PartnerDataFactory.newContact(partnerAcc.Id);
        partnerContact.RecordTypeId = recordTypePartnerContactFlow;        
        partnerContact.Type_of_Contact__c = 'Admin';
        partnerContact.UUID__c = new Uuid().getValue();
        INSERT partnerContact;      

        Opportunity aOpp = PartnerDataFactory.newOpportunity( partnerAcc.Id, 'Partner_Flow_Opportunity'); 
        INSERT aOpp;

        Quote proposal = PartnerDataFactory.newQuote( aOpp );
        proposal.RecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Proposal').getRecordTypeId();
        proposal.Signed__c = Date.Today() - 100;
        proposal.End_Date__c = Date.Today();
        INSERT proposal;

        Account_Opportunity_Relationship__c oppMember = PartnerDataFactory.newAcctOppRel(partnerAcc.Id, aOpp.Id);
        oppMember.RecordTypeId = Schema.SObjectType.Account_Opportunity_Relationship__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Opportunity_Member').getRecordTypeId();
        INSERT oppMember;

        Product_Item__c aProd = PartnerDataFactory.newProduct( aOpp );
        aProd.RecordTypeId = Schema.SObjectType.Product_Item__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product').getRecordTypeId();
        aProd.Opportunity__c = null;       
        INSERT aProd;

        Commercial_Condition__c aComm = new Commercial_Condition__c();
        aComm.RecordTypeId = Schema.SObjectType.Commercial_Condition__c.getRecordTypeInfosByDeveloperName().get('CAP').getRecordTypeId();
        //aComm.Name = 'CAP';
        INSERT aComm;

        Product_Assignment__c prodAssign = new Product_Assignment__c();
        prodAssign.RecordTypeId = Schema.SObjectType.Product_Assignment__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product_Assignment').getRecordTypeId();
        prodAssign.ProductId__c = aProd.Id;
        prodAssign.CommercialConditionId__c = aComm.Id;
        INSERT prodAssign;

        Product_Opportunity_Assignment__c prodOppAssign = new Product_Opportunity_Assignment__c();
        prodOppAssign.RecordTypeId = Schema.SObjectType.Product_Opportunity_Assignment__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product_Opportunity_Assignment').getRecordTypeId();
        prodOppAssign.OpportunityMemberId__c = oppMember.Id;
        prodOppAssign.ProductAssignmentId__c = prodAssign.Id;
        INSERT prodOppAssign;
    }

    @isTest 
    static void execute_Test() {
        Opportunity aOpp = [ SELECT ID FROM Opportunity Limit 1];
        Account aAcc = [ SELECT ID FROM Account Limit 1];
        Quote aProposal = [ SELECT ID FROM Quote Limit 1];  
        Order aOrder =  new Order ();
        aOrder.AccountId = aAcc.Id;
        aOrder.QuoteId = aProposal.Id;
        aOrder.CurrencyIsoCode = 'BRL';
        aOrder.EffectiveDate = Date.TODAY()-100;
        aOrder.EndDate = Date.TODAY()+460;
        aOrder.OpportunityId = aOpp.Id;
        aOrder.Status = 'Inactivated';
        aOrder.Type = 'Partner';
        aOrder.UUID__c = new Uuid().getValue();
        Test.startTest();                     
            INSERT aOrder;                
        Test.StopTest();
    }

    
    @isTest 
    static void execute_New_Test() {
        Opportunity aOpp = [ SELECT ID FROM Opportunity Limit 1];
        Account aAcc = [ SELECT ID, UUID__c FROM Account Limit 1];
        
        aAcc.UUID__c = null;
        UPDATE aAcc;

        Quote aProposal = [ SELECT ID FROM Quote Limit 1];  
        Order aOrder =  new Order ();
        aOrder.AccountId = aAcc.Id;
        aOrder.QuoteId = aProposal.Id;
        aOrder.CurrencyIsoCode = 'BRL';
        aOrder.EffectiveDate = Date.TODAY()-100;
        aOrder.EndDate = Date.TODAY()+460;
        aOrder.OpportunityId = aOpp.Id;
        aOrder.Status = 'Inactivated';
        aOrder.Type = 'Partner';
        Test.startTest();                     
            INSERT aOrder;                
        Test.StopTest();
    }
}