/**
 * @author vncferraz
 * 
 * Facade abstraction class to provide Offer_Queue__c processing logic layer for consumer
 */
public abstract class OfferQueueProcessor {
    
    protected List<Offer_Queue__c> offers;
    protected BaseOfferWrapperBuilder builder;

    public OfferQueueProcessor(List<Offer_Queue__c> offers){
        this.offers = offers;
        init();
    }
    
    public List<Offer_Queue__c> run(){

        for (Offer_Queue__c offer : offers){
           
            BaseOfferWrapper wrapper = buildWrapper(offer); 
            
            BaseOfferWrapper.ProcessResult result =  wrapper.finish();

            offer.Status__c = result.status;
            offer.Status_Detail__c = result.statusDetail;
        }

        return this.offers;
    }

    protected abstract void init();
    
    protected virtual BaseOfferWrapper buildWrapper(Offer_Queue__c offer){
        return builder.build( offer );        
    }

}