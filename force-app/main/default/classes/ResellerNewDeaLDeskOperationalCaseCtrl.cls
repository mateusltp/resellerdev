public with sharing class ResellerNewDeaLDeskOperationalCaseCtrl {
    

    @AuraEnabled
    public static DealDeskOperationalCaseInitialData retrieveInitData(String opportunityId) {

        DealDeskOperationalCaseInitialData initData = new DealDeskOperationalCaseInitialData();
        initData.recordTypeId = getCaseRecordTypeId();
        initData.queueId = getQueueId();
        initData.previousDealDeskOperationalCase = getPreviousDealDeskOperationalCase(opportunityId);
        initData.defaultQuoteId = getDefaultQuoteId(opportunityId);
        initData.businessModel = getBusinessModel(opportunityId);
        initData.quoteLineItems = getQuoteLineItems(opportunityId);
        initData.membershipFeePaymentMethod = getMembershipFeePaymentMethod(opportunityId);
        
        System.debug('### INTO THE retrieveInitData ### initData >>> ');
         System.debug('testee'+initData.previousDealDeskOperationalCase);
        return initData;
    }

    private static String getCaseRecordTypeId() {
        return Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Deal_Desk_Operational').getRecordTypeId();
    }

    private static Map<String, String> getPreviousDealDeskOperationalCase(String opportunityId) {
        List<String> caseFieldsAPINameToPrepopulate = new List<String>();
        List<Schema.FieldSetMember> caseFieldSetFields = SObjectType.Case.FieldSets.New_Deal_Desk_Operational_Case.getFields();
        for(Schema.FieldSetMember f : caseFieldSetFields) {
            caseFieldsAPINameToPrepopulate.add(f.getFieldPath());
        }
        
        List<Assert_Data__c> changesOnPreviousCase = 
            [SELECT Id ,Old_Value__c, Field_Name__c, CreatedDate 
             FROM Assert_Data__c WHERE Opportunity__c = :opportunityId 
             AND Object_Name__c = 'Case'
             AND Field_Name__c IN :caseFieldsAPINameToPrepopulate
             ORDER BY createdDate DESC];

        Map<String, String> valueOfLastChangesOfPreviousCaseByFieldName = New Map<String,String>();
        for(Assert_Data__c oneChange :changesOnPreviousCase){
            String fieldNameOneChange = oneChange.Field_Name__c;
            valueOfLastChangesOfPreviousCaseByFieldName.put(fieldNameOneChange, oneChange.Old_Value__c);
            // System.debug(oneChange.Field_Name__c + ' *** ' + oneChange.Old_Value__c);
        }
        return valueOfLastChangesOfPreviousCaseByFieldName;
    }

    private static String getDefaultQuoteId(String opportunityId) {
        List<Quote> defaultQuoteLst = [SELECT Id FROM Quote WHERE OpportunityId = :opportunityId LIMIT 1];
        return (!defaultQuoteLst.isEmpty()) ? defaultQuoteLst.get(0).Id : null;
    }

    private static String getQueueId() {
        List<Group> queueDetails = [SELECT Id FROM Group WHERE Type = 'Queue' AND DeveloperName = 'Deal_Desk_Operational' LIMIT 1];
        return (!queueDetails.isEmpty()) ? queueDetails.get(0).Id : '';
    }

    private static String getBusinessModel(String opportunityId) {
        List<Opportunity> businessModelLst = [SELECT B_M__c FROM Opportunity WHERE Id = :opportunityId LIMIT 1];
        return (!businessModelLst.isEmpty()) ? businessModelLst.get(0).B_M__c : '';
    }

    private static List<QuoteLineItem> getQuoteLineItems(String opportunityId) {
        QuoteLineItemRepository repository = new QuoteLineItemRepository();
        List<QuoteLineItem> quoteLineItems = repository.allForOpportunity(opportunityId);
        return quoteLineItems;
    }

    private static String getMembershipFeePaymentMethod(String opportunityId) {
        EligibilityRepository repository = new EligibilityRepository();
        List<Eligibility__c> eligibilities = repository.getEligibilitiesForOpportunity(opportunityId);

        for(Eligibility__c eligibility : eligibilities) {
            if(eligibility.Payment__r.Quote_Line_Item__r.Fee_Type__c == 'Enterprise Subscription') {
                return eligibility.Payment_Method__c;
            }
        }
        return null;
    }

    //-- support data object
    public class DealDeskOperationalCaseInitialData {
        @AuraEnabled
        public string recordTypeId;

        @AuraEnabled
        public string queueId;

        @AuraEnabled
        public string defaultQuoteId;

        @AuraEnabled
        public string businessModel;

        @AuraEnabled
        public string membershipFeePaymentMethod;

        @AuraEnabled
        public List<QuoteLineItem> quoteLineItems;

        @AuraEnabled
        public Map<String, String> previousDealDeskOperationalCase;
    }
}