/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 06-08-2021
 * @last modified by  : gft.samuel.silva@ext.gympass.com
 * Modifications Log 
 * Ver   Date         Author                             Modification
 * 1.0   06-01-2021   gft.samuel.silva@ext.gympass.com   Initial Version
**/
@isTest(seeAllData=false)
public with sharing class PartnerOpportunityStatusBatchTest {
    private static final Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gym_Partner').getRecordTypeId();
    private static final Id gRtLongTailOpp =  Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Partner_Small_and_Medium_Renegotiation').getRecordTypeId();
    private static final Id gRtWishLstOpp = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Partner_Wishlist_Renegotiation').getRecordTypeId();

    @TestSetup
    static void setupData(){
        List<Account> accLst = new List<Account>();       
        Account accLongTail = generatePartnerAccount();
        accLst.add(accLongTail);      

        Account accWishList = generatePartnerAccount();       
        accWishList.BillingCity = 'Sorocaba';
        accWishList.billingStreet = 'Rua sorocaba2';
        accWishList.billingCountry = 'Brazil';
        accWishList.ShippingCountry ='Brazil';
        accWishList.ShippingStreet ='Dona Maria';
        accWishList.ShippingCity ='Pedra';
        accLst.add(accWishList);        

        insert accLst;

        Pricebook2 standardPricebook = new Pricebook2(
            Id = Test.getStandardPricebookId(),
            IsActive = true
        );
        update standardPricebook;
        Pricebook2 pb = standardPricebook;  

        Opportunity oppLongTail = generateLongTailOpportunity(accLongTail,pb);
        insert oppLongTail;	
        Opportunity oppWishLst = generateWishLstOpportunity(accWishList,pb);
        insert oppWishLst;	

    }


    @isTest
    static void test() {   
        Test.startTest();    
            String testTime = '0 0 0 * * ?'; 
            PartnerOpportunityStatusBatch.start(testTime);   
            CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime 
                                FROM CronTrigger 
                                WHERE CronJobDetail.Name = 'Partner Opportunity Status Test'];            
            System.assertEquals(testTime, ct.CronExpression);
            System.assertEquals(0, ct.TimesTriggered);
            String timeNow = String.valueOf(Date.today()+1) + ' 00:00:00';
            System.assertEquals(timeNow, String.valueOf(ct.NextFireTime));
        Test.stopTest();
    }

    private static Account generatePartnerAccount(){
        Account lAcc = new Account();
         lAcc.name=generateRandomString(10);
         lAcc.RecordTypeId = rtId;
         lAcc.GP_Status__c = 'Active';
         lAcc.billingState = 'Minas Gerais';
         lAcc.CAP_Value__c = 120;
         lAcc.BillingCity = 'CityAcademiaBrasil';
         lAcc.billingStreet = 'Rua academiabrasilpai';
         lAcc.billingCountry = 'Brazil';
         lAcc.ShippingCountry ='Brazil';
         lAcc.ShippingStreet ='Dona Maria';
         lAcc.ShippingCity ='Uberlandia';
         lAcc.Gym_Type__c = 'Studios';
         lAcc.Gym_Classes__c = 'Cardio';
         lAcc.Legal_Title__c = '11111';
         lAcc.Id_Company__c = '11111';
         lAcc.Types_of_ownership__c = Label.franchisePicklist;
         lAcc.Subscription_Type__c = 'Value per class';
         lAcc.Subscription_Period__c = 'Monthy value';
         lAcc.Subscription_Type_Estimated_Price__c    = 100;
         lAcc.Has_market_cannibalization__c = 'No';
         lAcc.Exclusivity__c = 'Yes';
         lAcc.Exclusivity_End_Date__c = Date.today().addYears(1);
         lAcc.Exclusivity_Partnership__c = 'Full Exclusivity';
         lAcc.Exclusivity_Restrictions__c= 'No';
         lAcc.Website = 'testing@tesapex.com';
         lAcc.Gym_Email__c = 'gymemail@apex.com';
         lAcc.Phone = '3222123123';
         lAcc.Can_use_logo__c = 'Yes';
         lAcc.Legal_Registration__c = 12123;
         lAcc.Legal_Title__c = 'Title LTDA';
         lAcc.Gyms_Identification_Document__c = 'CNPJ'; 
         return lAcc;
     }
     
     private static Opportunity generateLongTailOpportunity(Account lAcc, Pricebook2 lPricebook){
        Opportunity lAccOpp = new Opportunity();
         lAccOpp.CurrencyIsoCode='BRL';
         lAccOpp.recordTypeId = gRtLongTailOpp;
         lAccOpp.AccountId = lAcc.id;
         lAccOpp.Sub_Type__c = 'Retention';
         lAccOpp.Name = lAcc.Id; 
         lAccOpp.Cancellation_Date__c = Date.today();
         lAccOpp.CMS_Used__c = 'Yes';     
         lAccOpp.Gym_agreed_to_an_API_integration__c  = 'Yes';
         lAccOpp.Club_Management_System__c = 'Companhia Athletica';
         lAccOpp.Integration_Fee_Deduction__c = 'No';
         lAccOpp.CloseDate = Date.today();
         lAccOpp.Success_Look_Like__c = 'Yes';
         lAccOpp.Success_Look_Like_Description__c = 'Money money';
         lAccOpp.Cancellation_Reason__c = 'Fraud';
         lAccOpp.Cancellation_Reason_subcategory__c = 'Visit manipulation';
         lAccOpp.StageName = 'Waiting Cancellation';
         lAccOpp.Type = 'Expansion';  
         lAccOpp.Country_Manager_Approval__c = true;
         lAccOpp.Payment_approved__c = true;   
         lAccOpp.CurrencyIsoCode = 'BRL';
         lAccOpp.Gympass_Plus__c = 'Yes';
         lAccOpp.Standard_Payment__c = 'Yes';
         lAccOpp.Request_for_self_checkin__c = 'Yes';  
         lAccOpp.Pricebook2Id = lPricebook.Id;
         lAccOpp.Training_Comments__c = 'teste';
         lAccOpp.Should_OPS_team_send_the_training__c = 'Standard Training';
         return lAccOpp;
     }

     private static Opportunity generateWishLstOpportunity(Account lAcc, Pricebook2 lPricebook){
        Opportunity lAccOpp = new Opportunity();
         lAccOpp.CurrencyIsoCode='BRL';
         lAccOpp.recordTypeId = gRtWishLstOpp;
         lAccOpp.AccountId = lAcc.id;
         lAccOpp.Sub_Type__c = 'Retention';
         lAccOpp.Name = lAcc.Id; 
         lAccOpp.CMS_Used__c = 'Yes';    
         lAccOpp.Cancellation_Date__c = Date.today(); 
         lAccOpp.Gym_agreed_to_an_API_integration__c  = 'Yes';
         lAccOpp.Club_Management_System__c = 'Companhia Athletica';
         lAccOpp.Integration_Fee_Deduction__c = 'No';
         lAccOpp.CloseDate = Date.today();
         lAccOpp.Success_Look_Like__c = 'Yes';
         lAccOpp.Success_Look_Like_Description__c = 'Money money';
         lAccOpp.Cancellation_Reason__c = 'Fraud';
         lAccOpp.Cancellation_Reason_subcategory__c = 'Visit manipulation';
         lAccOpp.StageName = 'Waiting Cancellation';
         lAccOpp.Type = 'Expansion';  
         lAccOpp.Country_Manager_Approval__c = true;
         lAccOpp.Payment_approved__c = true;   
         lAccOpp.CurrencyIsoCode = 'BRL';
         lAccOpp.Gympass_Plus__c = 'Yes';
         lAccOpp.Standard_Payment__c = 'Yes';
         lAccOpp.Request_for_self_checkin__c = 'Yes';  
         lAccOpp.Pricebook2Id = lPricebook.Id;
         lAccOpp.Training_Comments__c = 'teste';
         lAccOpp.Should_OPS_team_send_the_training__c = 'Standard Training';
         return lAccOpp;
     }
    
    public static String generateRandomString(Integer len) {
    	final String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
    	String randStr = '';
    	while (randStr.length() < len) {
       		Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
       		randStr += chars.substring(idx, idx+1);
    	}
    	return randStr;
	}
     
}