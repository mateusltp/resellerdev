/*
* @Author: Bruno Pinho
* @Date: January/2019
* @Description: Wildcard Multi Select Lookup
*/
public class WildcardMultiSelectLookupCtrl {    
    @AuraEnabled
    public static List <sObject> fetchLookUpValues(String searchKeyWord, String ObjectName, List<sObject> ExcludeitemsList)
    {
        String searchKey = '%' + searchKeyWord + '%';
        String sQuery = '';
        
        List<sObject> returnList = new List<sObject>();
        List<String> lstExcludeitems = new List<String>();
        
        for(sObject item : ExcludeitemsList)
            lstExcludeitems.add(item.id);
        
        if (ObjectName == 'User')
            sQuery = 'SELECT Id, Name FROM ' + ObjectName + ' WHERE Name LIKE: searchKey AND Id NOT IN: lstExcludeitems AND IsActive = TRUE ORDER BY CreatedDate DESC LIMIT 5';
        else
            sQuery = 'SELECT Id, Name FROM ' + ObjectName + ' WHERE Name LIKE: searchKey AND Id NOT IN: lstExcludeitems ORDER BY CreatedDate DESC LIMIT 5';
        
        System.debug('Query: '+sQuery);
        
        List <sObject> lstOfRecords = Database.query(sQuery);
        
        for (sObject obj: lstOfRecords)
            returnList.add(obj);
        
        return returnList;
    }
}