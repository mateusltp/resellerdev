/**
 * @description       : 
 * @author            : gepi@gft.com
 * @group             : 
 * @last modified on  : 05-13-2022
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/
@isTest
public with sharing class ProductAssignmentListControllerTest {
    
    @testSetup
    static void makeData(){
        Account accParent = getAccountInstance('Parent Account', null);
        insert accParent;
        
        Account accChild = getAccountInstance('Child Account', accParent.Id);
        accChild.Partner_Level__c = 'Location';
        insert accChild;        

        Opportunity opp = getOpportunityInstance(accParent.Id);
        insert opp;

        Account_Opportunity_Relationship__c aor = getAorInstance(accParent.Id, opp.Id);
        insert aor;

        Account_Opportunity_Relationship__c aor2 = getAorInstance(accChild.Id, opp.Id);
        insert aor2;
        
        Opportunity childOpp = getOpportunityInstance(accParent.Id);
        childOpp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Child_Opportunity').getRecordTypeId();
        childOpp.MasterOpportunity__c = opp.Id;
        insert childOpp;      

        Account_Opportunity_Relationship__c childAor = getAorInstance(accParent.Id, childOpp.Id);
        insert childAor;

        Account_Opportunity_Relationship__c childAor2 = getAorInstance(accChild.Id, childOpp.Id);
        insert childAor2;

        Commercial_Condition__c capCommCondition = getCapCommConditionInstance();
        insert capCommCondition;

        Product_Item__c product = getProductInstance();
        insert product;

        Commercial_Condition__c childCapCommCondition = getCapCommConditionInstance();
        insert childCapCommCondition;

        Product_Item__c childProduct = getProductInstance();
        childProduct.RecordTypeId = Schema.SObjectType.Product_Item__c.getRecordTypeInfosByDeveloperName().get('Partner_Child_Product').getRecordTypeId();
        childProduct.Parent_Product__c = product.Id;
        insert childProduct;

        Product_Assignment__c pa = getProdAssignmentInstance(capCommCondition.Id, product.Id);
        insert pa;

        Product_Assignment__c childPa = getProdAssignmentInstance(childCapCommCondition.Id, childProduct.Id);
        insert childPa;

        Product_Opportunity_Assignment__c poa = getPoaInstance(pa.Id, aor.Id);
        insert poa;

        Product_Opportunity_Assignment__c poa2 = getPoaInstance(pa.Id, aor2.Id);
        insert poa2;

        Product_Opportunity_Assignment__c childPoa = getPoaInstance(childPa.Id, childAor.Id);
        insert childPoa;

        Product_Opportunity_Assignment__c childPoa2 = getPoaInstance(childPa.Id, childAor2.Id);
        insert childPoa2;

        List<Profile> profiles = [Select Id From Profile Where Name='Relationship'];

        User user = new User();
        user.Username = 'r.user@test.com'; 
        user.LastName = 'user'; 
        user.Email = 'r.user@test.com'; 
        user.Alias = 'tuser'; 
        user.TimeZoneSidKey = 'America/Los_Angeles'; 
        user.LocaleSidKey = 'en_US'; 
        user.EmailEncodingKey = 'UTF-8';
        user.ProfileId = profiles.get(0).id;
        user.LanguageLocaleKey = 'en_US';
        insert user;

    }
    
    @isTest
    public static void buildNetworkTest() {
        Product_Item__c product = [SELECT Id FROM Product_Item__c LIMIT 1];
        
        SelectPartnersAccountsController.buildNetwork(product.Id, 'Partner_Flow_Account');
    }

    @isTest
    public static void buildNetworkForOppTest() {
        Account acc = [SELECT Id FROM Account WHERE Name = 'Parent Account'];
        
        SelectPartnersAccountsController.buildNetwork(acc.Id, 'Partner_Flow_Account');
    }

    @isTest
    public static void buildNetworkExceptionTest() {
        Product_Item__c product = [SELECT Id FROM Product_Item__c LIMIT 1];
        
        try {
            SelectPartnersAccountsController.buildNetwork(product.Id, 'Partner_Flow_Account_Exception');
        } catch(Exception e) {

        }
    }

    @isTest
    public static void buildNetworkNoPaExceptionTest() {
        Product_Assignment__c pa = [SELECT Id FROM Product_Assignment__c LIMIT 1];
        delete pa;

        Product_Item__c product = [SELECT Id FROM Product_Item__c LIMIT 1];
        
        try {
            SelectPartnersAccountsController.buildNetwork(product.Id, 'Partner_Flow_Account_Exception');
        } catch(Exception e) {
            System.assertEquals('This product does not have an standard Product Assignment', e.getMessage());
        }
    }

    @isTest
    public static void getProdAssignmentsTest() {
        Product_Item__c product = [SELECT Id FROM Product_Item__c LIMIT 1];
        
        ProductAssignmentListController.getProdAssignments(product.Id);
    }

    @isTest
    public static void getAccountDataTest() {
        Product_Item__c product = [SELECT Id FROM Product_Item__c LIMIT 1];
        
        ProductAssignmentListController.getAccountData(product.Id);
    }

    @isTest
    public static void updateProdAssignmentsTest() {
        Product_Assignment__c pa = [SELECT Id, Market_price__c FROM Product_Assignment__c LIMIT 1];
        List<ProductAssignmentListController.Wrapper> wrapperLst = new List<ProductAssignmentListController.Wrapper>();

        ProductAssignmentListController.Wrapper wrapper = new ProductAssignmentListController.Wrapper();
        wrapper.Id = pa.Id;
        wrapper.marketPrice = 90;

        wrapperLst.add(wrapper);
        
        Test.startTest();
        ProductAssignmentListController.updateProdAssignments(
            JSON.deserializeUntyped(JSON.serialize(wrapperLst))
        );
        Test.stopTest();

        pa = [SELECT Id, Market_price__c FROM Product_Assignment__c LIMIT 1];

        System.assertEquals(90, pa.Market_price__c);
    }

    @isTest
    public static void deleteProdAssignmentTest() {
        Product_Assignment__c pa = [SELECT Id FROM Product_Assignment__c LIMIT 1];

        ProductAssignmentListController.deleteProdAssignment(pa.Id);
    }

    @isTest
    public static void getHelperDataTest() {
        Product_Item__c product = [SELECT Id FROM Product_Item__c LIMIT 1];
        Account parentAccount   = [ SELECT Id FROM Account WHERE ParentId = null LIMIT 1 ];
        
        Account newChildAcc  = getAccountInstance('New Child Acc', parentAccount.Id);
        insert newChildAcc;

        Test.startTest();
            ProductAssignmentListController.getHelperData(product.Id);
        Test.stopTest();
    }

    @isTest
    public static void createProdAssignmentsTest() {
        Product_Item__c product = [SELECT Id FROM Product_Item__c LIMIT 1];
        Account parentAccount   = [ SELECT Id FROM Account WHERE ParentId = null LIMIT 1 ];
        
        Account newChildAcc  = getAccountInstance('New Child Acc', parentAccount.Id);
        insert newChildAcc;

        Test.startTest();
        ProductAssignmentListController.createProdAssignments(product.Id, new List<Id>{newChildAcc.Id});
        Test.stopTest();
    }

    @isTest
    public static void createProdAssignmentsForChildProdTest() {
        Id childRt = Schema.SObjectType.Product_Item__c.getRecordTypeInfosByDeveloperName().get('Partner_Child_Product').getRecordTypeId();
        Product_Item__c childProduct = [SELECT Id, RecordTypeId FROM Product_Item__c WHERE RecordTypeID =: childRt LIMIT 1];
       
        Test.startTest();
        ProductAssignmentListController.createProdAssignmentsForChildProd(childProduct.Id);
        Test.stopTest();
    }


    private static Account getAccountInstance(String name, Id parentId) {
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Account').getRecordTypeId();
        
        Account acc = new Account();
        acc.Name = name;
        acc.RecordTypeId = recordTypeId;
        acc.ParentId = parentId;
        acc.BillingCountry = 'Brazil';
        acc.BillingState = 'São Paulo';
        acc.BillingCity = 'São Paulo';
        acc.BillingStreet = 'Casa do Ator';
        acc.NumberOfEmployees = 100;
        acc.Business_Unit__c = 'SMB';
        acc.Id_Company__c = (name.contains('Parent') ? '57.454.841/0001-96' : '29.315.529/0001-86');
        acc.Legal_Document_Type__c = 'CNPJ';
        acc.Razao_Social__c = 'Test Acc';
        
        return acc;
    }

    private static Opportunity getOpportunityInstance(Id accountId) {
        Id recordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Opportunity').getRecordTypeId();
        
        Opportunity opp = new Opportunity();
        opp.RecordTypeId = recordTypeId;
        opp.Name = 'My Opp';
        opp.CloseDate = System.now().date();
        opp.AccountId = accountId;
        opp.StageName = 'Qualificação';

        return opp;
    }

    private static Account_Opportunity_Relationship__c getAorInstance(Id accountId, Id oppId) {
        Id recordTypeId = Schema.SObjectType.Account_Opportunity_Relationship__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Opportunity_Member').getRecordTypeId();

        Account_Opportunity_Relationship__c aor = new Account_Opportunity_Relationship__c();
        aor.Account__c = accountId;
        aor.Opportunity__c = oppId;
        aor.RecordTypeId = recordTypeId;

        return aor;
    }

    private static Commercial_Condition__c getCapCommConditionInstance() {
        Id recordTypeId = Schema.SObjectType.Commercial_Condition__c.getRecordTypeInfosByDeveloperName().get('CAP').getRecordTypeId();

        Commercial_Condition__c capCommCondition = new Commercial_Condition__c();
        capCommCondition.RecordTypeId = recordTypeId;

        return capCommCondition;
    }

    private static Product_Item__c getProductInstance() {
        Id recordTypeId = Schema.SObjectType.Product_Item__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product').getRecordTypeId();

        Product_Item__c product = new Product_Item__c();
        product.Name = 'Boxing';
        product.RecordTypeId = recordTypeId;

        return product;
    }

    private static Product_Assignment__c getProdAssignmentInstance(Id capCommConditionId, Id productId) {
        Id recordTypeId = Schema.SObjectType.Product_Assignment__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product_Assignment').getRecordTypeId();
        
        Product_Assignment__c prodAssignment = new Product_Assignment__c();
        prodAssignment.RecordTypeId = recordTypeId;
        prodAssignment.CommercialConditionId__c = capCommConditionId;
        prodAssignment.ProductId__c = productId;

        return prodAssignment;
    }

    private static Product_Opportunity_Assignment__c getPoaInstance(Id paId, Id aorId) {
        Id recordTypeId = Schema.SObjectType.Product_Opportunity_Assignment__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product_Opportunity_Assignment').getRecordTypeId();
        
        Product_Opportunity_Assignment__c poa = new Product_Opportunity_Assignment__c();
        poa.RecordTypeId = recordTypeId;
        poa.ProductAssignmentId__c = paId;
        poa.OpportunityMemberId__c = aorId;

        return poa;
    }
}