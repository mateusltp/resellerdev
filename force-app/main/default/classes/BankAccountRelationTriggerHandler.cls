/**
 * @File Name          : BankAccountRelationTriggerHandler.cls
 * @Description        : 
 * @Author             : GEPI@GFT.com
 * @Group              : 
 * @Last Modified By   : GEPI@GFT.com
 * @Last Modified On   : 20/05/2020 17:41:06
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    20/05/2020   GEPI@GFT.com     Initial Version
**/
public class BankAccountRelationTriggerHandler extends triggerhandler{
    List<Acount_Bank_Account_Relationship__c> lstNew = trigger.new;

    public override void beforeInsert() {    
        System.debug('entro no trigger');
        new BankAccountRelTrgCreationBlockHelper().blockOnBeforeInsert(lstNew);
    }

}