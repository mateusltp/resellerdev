/**
 * @description       : 
 * @author            : roei@gft.com
 * @group             : 
 * @last modified on  : 07-15-2022
 * @last modified by  : tiago.ribeiro@gympass.com
**/
@isTest(seeAllData=false)
public with sharing class SkuShoppingCartHelperTest {
    @TestSetup
    private static void createData(){  
        Pricebook2 lStandardPricebook = DataFactory.newPricebook();
        Database.update( lStandardPricebook );

        Account lAcc = DataFactory.newAccount();
        Database.insert( lAcc );
        
        Product2 lEsProduct = DataFactory.newProduct( 'Enterprise Subscription' , false , 'BRL' );
        lEsProduct.Payment_Type__c = 'Recurring fee';
        Database.insert( lEsProduct );

        Opportunity lSKUNewOpp = DataFactory.newOpportunity( lAcc.Id , lStandardPricebook , 'Client_Sales_SKU_New_Business' );
        lSKUNewOpp.Name = 'OppTestSKU';
        lSKUNewOpp.Billing_Period__c = 'Monthly';
        lSKUNewOpp.TotalOpportunityQuantity = 1000;
        lSKUNewOpp.Quantity_Offer_Number_of_Employees__c = 1000;
        lSKUNewOpp.Purchase_Order_number_is_needed__c = 'Yes';
        lSKUNewOpp.Membership_Fee_PO_Duration__c = 'Annual';
        
        PricebookEntry lEnterpriseSubscriptionEntry = DataFactory.newPricebookEntry( lStandardPricebook , lEsProduct , lSKUNewOpp );
        Database.insert( lEnterpriseSubscriptionEntry );

        SKU_Price__c lSkuPrice = DataFactory.newSKUPrice( lEsProduct.Id , lSKUNewOpp , lAcc , 1 , 50 );
        Database.insert( lSkuPrice );

        Database.insert( lSKUNewOpp );

        Quote lQuote = DataFactory.newQuote( lSKUNewOpp , 'QuoteTest' , 'Client_Sales_SKU_New_Business' );
        Database.insert( lQuote );
        
        lSKUNewOpp.SyncedQuoteId = lQuote.Id;
        Database.update( lSKUNewOpp );

        QuoteLineItem lEnterpriseSubscriptionItem = DataFactory.newQuoteLineItem( lQuote , lEnterpriseSubscriptionEntry );
        lEnterpriseSubscriptionItem.Discount = 50;
        Database.insert( lEnterpriseSubscriptionItem );

        Product2 lTestProduct = DataFactory.newProduct( 'Product Test' , false , 'BRL' );
        lTestProduct.Payment_Type__c = 'Recurring fee';
        lTestProduct.Professional_services_types__c = 'EM;Copay';
        lTestProduct.Professional_Services_Type_Selection__c = 'Radio';
        Database.insert( lTestProduct );

        SKU_Price__c lSkuPriceTest = DataFactory.newSKUPrice( lTestProduct.Id , lSKUNewOpp , lAcc , 1 , 1000 );
        Database.insert( lSkuPriceTest );
    }

    @isTest
    private static void shouldGetQuoteLineItemBasedOnSkuPrice(){
        Opportunity lOpp = [ SELECT Id, SyncedQuoteId, TotalOpportunityQuantity, CurrencyIsoCode FROM Opportunity WHERE Name LIKE '%OppTestSKU' ];
        Quote lQuote = [ SELECT Id, OpportunityId, Total_Number_of_Employees__c FROM Quote WHERE Id =: lOpp.SyncedQuoteId ];

        SkuShoppingCartHelper.OppRecord lClsOppRec = new SkuShoppingCartHelper.OppRecord();
        lClsOppRec.recordId = lOpp.Id;
        lClsOppRec.currencyCode = lOpp.CurrencyIsoCode;
        lClsOppRec.syncedQuote = lQuote.Id;
        lClsOppRec.quantity = Integer.valueOf( lOpp.TotalOpportunityQuantity );
        lClsOppRec.pricebookId = DataFactory.newPricebook().Id;
        
        Test.startTest();
        
        List< QuoteLineItem > lLstQuoteLineItem = SkuShoppingCartHelper.getSkuProducts( new SkuShoppingCartHelper.OppRecord[]{ lClsOppRec } )[0];

        Test.stopTest();

        System.assert( lLstQuoteLineItem.size() == 1 );
    }
}