/**
 * Created by gympasser on 01/04/2022.
 */

public with sharing class ContentVersionSelector extends ApplicationSelector{

    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
                ContentVersion.Id,
                ContentVersion.Type_Files_fileupload__c
        };
    }

    public Schema.SObjectType getSObjectType() {
        return ContentVersion.sObjectType;
    }

    
    public List<ContentVersion> selectByFirstPublishLocations(Set<Id> recordIds) {
        return (List<ContentVersion>) Database.query(
                newQueryFactory().
                        selectField('FirstPublishLocationId').
                        setCondition('FirstPublishLocationId IN :recordIds' ).
                        toSOQL());
    }

    public List<ContentVersion> selectById(Set<Id> recordIds) {
        return (List<ContentVersion>) Database.query(
                newQueryFactory().
                        selectField(ContentVersion.Id).
                        selectField(ContentVersion.ContentDocumentId).
                        selectField(ContentVersion.ContentSize).
                        selectField(ContentVersion.Type_Files_fileupload__c).
                        setCondition('Id IN :recordIds' ).
                        toSOQL());
    }

    public Map<Id, ContentVersion> selectConventVersionById(Set<Id> recordIds) {
        return new Map<Id, ContentVersion>(this.selectById(recordIds));
    }

    public List<ContentVersion> selectByContentDocumentId(Set<Id> contentDocumentIds) {
        return (List<ContentVersion>) Database.query(
                newQueryFactory().
                        selectField(ContentVersion.Id).
                        selectField(ContentVersion.ContentDocumentId).
                        selectField(ContentVersion.ContentSize).
                        selectField(ContentVersion.Type_Files_fileupload__c).
                        setCondition('ContentDocumentId IN :contentDocumentIds' ).
                        toSOQL());
    }
}