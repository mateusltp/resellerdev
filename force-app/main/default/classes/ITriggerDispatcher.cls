/*
* @author Bruno Pinho
* @date January/2019
* @description Defines the interface for the trigger dispatching architecture.
*/
public interface ITriggerDispatcher
{
    void bulkBefore();
    
    void bulkAfter();
    
    void andFinally();
    
    void beforeInsert(TriggerParameters tp);
    
    void beforeUpdate(TriggerParameters tp);
    
    void beforeDelete(TriggerParameters tp);
    
    void afterInsert(TriggerParameters tp);
    
    void afterUpdate(TriggerParameters tp);
    
    void afterDelete(TriggerParameters tp);
    
    void afterUnDelete(TriggerParameters tp);
}