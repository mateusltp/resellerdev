/**
 * @description       : 
 * @author            : Samuel Silva - GFT (slml@gft.com)
 * @group             : 
 * @last modified on  : 05-24-2022
 * @last modified by  : Mateus Augusto - GFT (moes@gft.com)
 * Modifications Log 
 * Ver   Date         Author                              Modification
 * 1.0   09-14-2020   Samuel Silva - GFT (slml@gft.com)   Initial Version
**/
public with sharing virtual class FastTrackCommercialConditionsHandler {
    
    @TestVisible private FastTrackProposalCreationTO to;
    private Opportunity opp;
    @TestVisible protected Quote quote;
    @TestVisible protected Integer currentDiscountLevel;
    private Boolean setupApprovalNeeded;
    private Boolean accessFeeApprovalNeeded;
    @TestVisible protected Boolean gInCurrentContract;
    @TestVisible private List<Waiver__c> waiversToUpdtLvApproval;
    protected String gQuoteApprovalProcessDevName = 'Quote_Discount_Approval';
    protected String gWaiverApprovalProcessDevName = 'Waiver_approval_2';
    protected String gMdtLabelPrefix = 'Clients';
    private EnablersComercialConditionsHandler enablersComHandler;
    @TestVisible private Map< String , Map< String , Assert_Data__c > > gMapFeeTypeMapSobjNOldFieldValue;
    Id IndirectRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Indirect_Channel_New_Business').getRecordTypeId();

    @TestVisible
    protected Map<String , Discount_and_Waiver_approval_parameters__mdt> mapOptionApprovalMetadata;
    
    public FastTrackCommercialConditionsHandler(){}

    public FastTrackCommercialConditionsHandler( FastTrackProposalCreationTO to ) {
        init( to );
    }
	
    @TestVisible
    private void init( FastTrackProposalCreationTO to ){
        this.to = to;
        if( to.proposal.sObj.id == null ){ return; }   
        this.opp = new OpportunityRepository().byId( to.opportunityId );
        this.quote = new QuoteRepository().lastForOpportunity( to.opportunityId );
        this.currentDiscountLevel = 0;
        to.commercialConditionsApprovalNeeded = false;
        this.waiversToUpdtLvApproval = new List<Waiver__c>();
        this.enablersComHandler = new EnablersComercialConditionsHandler();
        this.gMapFeeTypeMapSobjNOldFieldValue = new Map< String , Map< String , Assert_Data__c > >();

        Account lAccount = [SELECT Proposal_End_Date__c FROM Account WHERE Id = :to.AccountId];  
        gInCurrentContract = ( lAccount.Proposal_End_Date__c != null && to.orderStartDate != null && to.orderStartDate <= lAccount.Proposal_End_Date__c );

        mapOptionApprovalMetadata = getApprovalParametersMdt();
    }

    public void submitForApproval(){
		system.debug('this.opp.RecordTypeId '+this.opp.RecordTypeId);
        system.debug('this.IndirectRecordTypeId '+this.opp.RecordTypeId);	
        if(this.opp.RecordTypeId != this.IndirectRecordTypeId){
            submitQuoteForApproval();
        } else {
            if(quote.Enablers_Approval_Needed__c){
                quote.Approval_Level__c = 1;
                if(quote.Enterprise_Discount_Approval_Needed__c){
                    quote.Approval_Level__c = quote.Approval_Level__c + quote.User_Discount_Approval_Level__c;
                }
            }else if(quote.Enterprise_Discount_Approval_Needed__c){
                quote.Approval_Level__c = 3 + quote.User_Discount_Approval_Level__c;
            }
            submitIndirectCommercialApproval();
        }

        if( this.to.proposal.accessFee?.waiversApprovalNeeded == true ){
            submitItemWaiversToApproval( getItemWaiversToApprove(this.to.proposal.accessFee.payments) );
        }
    }

    public void commercialConditionsEvaluation(){
        if( to.proposal.sObj.id == null ){ return; }   
        if(this.opp.RecordTypeId != this.IndirectRecordTypeId){
            quote.Enterprise_Discount_Approval_Needed__c = false;
            quote.Setup_Discount_Approval_Needed__c = false;
            quote.Enablers_Approval_Needed__c = false;
        }

        getApprovedSobjFieldValues();

        setWaiverApproverNeeded();
        
        List<FastTrackProposalCreationTO.Payment> paymentObjLst = new List<FastTrackProposalCreationTO.Payment>();

        Map<Id, QuoteLineItem> mapIdFeeObj = new Map<Id, QuoteLineItem>();

        if(!to.proposal.accessFee.payments.isEmpty()){
            paymentObjLst.add(to.proposal.accessFee.payments.get(0));
            mapIdFeeObj.put(to.proposal.accessFee.sObj.id,to.proposal.accessFee.sObj);
        }
        
        //using first payment__c in payment list - TBD
        if(!to.proposal.setupFee.payments.isEmpty()){
            paymentObjLst.add(to.proposal.setupFee.payments.get(0));
            mapIdFeeObj.put(to.proposal.setupFee.sObj.id, to.proposal.setupFee.sObj);
        }

        if(!paymentObjLst.isEmpty()){            
            FastTrackProposalCreationTO.Payment iPay = paymentObjLst[0];
            FastTrackProposalCreationTO.Payment iPaySetupFee = null;
           	
            if (paymentObjLst.size() > 1) {
            	iPaySetupFee = paymentObjLst[1];    
            }
            
            String Payment_Method = '',Frequency = iPay.sObj.Frequency__c;
            String Payment_Method_Setup = '',Frequency_Setup = iPay.sObj.Frequency__c;
            if(!iPay.eligibilities.isEmpty()){
                for(FastTrackProposalCreationTO.Eligibility  iEligibility : iPay.eligibilities){
                    switch on iEligibility.sObj.Payment_Method__c {
                        when 'Credit Card' {	
                            Payment_Method = 'Credit Card';	
                            break;
                        }
                        when 'Payroll + Credit Card' {
                            Payment_Method = 'Credit Card';	
                            break;
                        }
                        when 'Payroll' {	
                            Payment_Method = 'Payroll';
                        }
                    }
                }
               
                Payment_Method_Setup = Payment_Method;
               
                QuoteLineItem feeItem = mapIdFeeObj.get(iPay.sObj.Quote_Line_Item__c);
                switch on Payment_Method {                        
                    when 'Credit Card' {		
                        if(Frequency.contains('Yearly')){
                            evaluateDiscount('Annual payment + credit card', feeItem);
                        } else if(Frequency.contains('Monthly')){
                            evaluateDiscount('Monthly payment + credit card', feeItem);
                        }
                    }	
                    when 'Payroll' {	
                        if(Frequency.contains('Yearly')){
                            evaluateDiscount('Annual payment + payroll', feeItem);
                        } else if(Frequency.contains('Monthly')){                            
                            evaluateDiscount('Monthly payment + payroll', feeItem);
                        }
                    }
                    when else {}
                }
                
                if (iPaySetupFee != null) {
                	QuoteLineItem setupItem = mapIdFeeObj.get(iPaySetupFee.sObj.Quote_Line_Item__c);
                    switch on Payment_Method_Setup {                        
                        when 'Credit Card' {		
                            if(Frequency_Setup.contains('Yearly')){
                                evaluateDiscount('Annual payment + credit card', setupItem);
                            } else if(Frequency_Setup.contains('Monthly')){
                                evaluateDiscount('Monthly payment + credit card', setupItem);
                            }
                        }	
                        when 'Payroll' {	
                            if(Frequency_Setup.contains('Yearly')){
                                evaluateDiscount('Annual payment + payroll', setupItem);
                            } else if(Frequency_Setup.contains('Monthly')){                            
                                evaluateDiscount('Monthly payment + payroll', setupItem);
                            }
                        }
                        when else {}
                    }    
                }
            }
        }

        evaluateCopay(mapIdFeeObj);
        enablersComHandler.evaluateEnablers(quote, opp);

        if(commercialConditionsApprovalNeeded()){
            if(this.opp.RecordTypeId != this.IndirectRecordTypeId){
                quote.Discount_Approval_Level__c = this.currentDiscountLevel;
            }
            new QuoteRepository().edit( quote );
        }

        if( !waiversToUpdtLvApproval.isEmpty() ){
            Database.update( waiversToUpdtLvApproval );
        }
    }
	
    @testVisible
    protected virtual Boolean commercialConditionsApprovalNeeded(){
		if(this.opp.RecordTypeId == this.IndirectRecordTypeId){
            return !quote.Enterprise_Subscription_Approved__c || !quote.Enablers_Approved__c || !quote.Setup_Discount_Approved__c;
        }
        return ( quote.Enterprise_Discount_Approval_Needed__c && !quote.Enterprise_Subscription_Approved__c ) || 
               ( quote.Setup_Discount_Approval_Needed__c && !quote.Setup_Discount_Approved__c ) ||
               this.currentDiscountLevel == 0;
    }

    private void submitQuoteForApproval(){
        try{
            quote.Rationale__c = to.proposal.commercialJustification;
            new QuoteRepository().edit( quote ); 

            Approval.ProcessSubmitRequest quoteReq = new Approval.ProcessSubmitRequest();
            quoteReq.setComments('Submitting request for approval. ' + to.proposal.commercialJustification);
            quoteReq.setObjectId( quote.Id );
            quoteReq.setSubmitterId( UserInfo.getUserId() );
            quoteReq.setProcessDefinitionNameOrId( gQuoteApprovalProcessDevName );

            Approval.ProcessResult result = Approval.process( quoteReq );
        } catch( Exception aException ){
            if( !aException.getMessage().contains( 'NO_APPLICABLE_PROCESS' ) ){
                throw new CustomException( aException.getMessage() );
            }
        }
    }
    
    //trocar nome do metodo e do processo (Indirect_Approval)
    @testVisible
    private void submitIndirectCommercialApproval(){
        try{
            quote.Rationale__c = to.proposal.commercialJustification;
            quote.Deal_Desk_Comments__c = to.proposal.sObj.Deal_Desk_Comments__c;
            quote.Waiver_Comments__c = to.proposal.sObj.Waiver_Comments__c;
            
            new QuoteRepository().edit(quote); 

            Approval.ProcessSubmitRequest quoteReq = new Approval.ProcessSubmitRequest();
            quoteReq.setComments('Submitting request for approval. ' + to.proposal.commercialJustification);
            quoteReq.setObjectId(quote.Id);
            quoteReq.setSubmitterId(UserInfo.getUserId());
            quoteReq.setProcessDefinitionNameOrId('Indirect_Approval_v9');
            Approval.ProcessResult result = Approval.process( quoteReq );
        } catch( Exception aException ){
            if( !aException.getMessage().contains( 'NO_APPLICABLE_PROCESS' ) ){
                throw new CustomException( aException.getMessage() + 'Indirect_Approval' );
            }
        }
    }
    
    private Id getWaiverInitialApproverId(){
        User lCurrentUser;

        if(this.opp.RecordTypeId == this.IndirectRecordTypeId){
            lCurrentUser = [ SELECT ManagerId FROM User WHERE Id =: UserInfo.getUserId() ];
            return lCurrentUser.ManagerId;
        }else{
            lCurrentUser = [ SELECT Manager.ManagerId FROM User WHERE Id =: UserInfo.getUserId() ];
        }

        return lCurrentUser.Manager?.ManagerId;
    }

    @testVisible
    private void submitItemWaiversToApproval(List<Waiver__c> waivers){
        if( waivers.isEmpty() ){ return; }

        List<Approval.ProcessSubmitRequest> approvalRequests = new List<Approval.ProcessSubmitRequest>();

        for( Waiver__c iWaiver : waivers ){
            Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
            req1.setComments('Submitting request for approval. ' + to.proposal.accessFee.waiversJustification);
            req1.setObjectId( iWaiver.Id );
            req1.setSubmitterId( UserInfo.getUserId() );
            req1.setNextApproverIds(new Id[] { getWaiverInitialApproverId() });
            req1.setProcessDefinitionNameOrId( this.opp.RecordTypeId == this.IndirectRecordTypeId ? 'Waiver_Approval_Indirect' : gWaiverApprovalProcessDevName );

            approvalRequests.add( req1 );
            
            iWaiver.Rationale__c = to.proposal.accessFee.waiversJustification;
        }

        new WaiverRepository().edit(waivers);

        try{
            Approval.ProcessResult[] results = Approval.process(approvalRequests);
        } catch (Exception e){
            if(!e.getMessage().contains( 'NO_APPLICABLE_PROCESS' )){
                throw new CustomException(e.getMessage());
            }
        }
    }
    
    @testVisible
    private List<Waiver__c> getItemWaiversToApprove(List<FastTrackProposalCreationTO.Payment> payments){        
        List<Waiver__c> waiversSobj = new List<Waiver__c>();
        List<Id> waiversId = new List<Id>();

        if( payments == null || payments.isEmpty() ){ return waiversSobj; }

        List<FastTrackProposalCreationTO.Waiver> waivers = new List<FastTrackProposalCreationTO.Waiver>();

        for( FastTrackProposalCreationTO.Payment payment : payments ){
            if( payment.waivers == null || payment.waivers.isEmpty() ){ continue; }

            waivers.addAll(payment.waivers);
        }

        for( FastTrackProposalCreationTO.Waiver waiver : waivers ){
            waiversId.add(waiver.sObj.Id);
        }

        for( Waiver__c waiver : [ SELECT Id, Approval_level__c, Approval_Status__c,Approval_Status_Formula__c FROM Waiver__c WHERE Id =: waiversId AND 
            Approval_Status__c IN ('New', 'Pending Approval', 'Rejected') ]){
            waiversSobj.add(waiver);
        }

        return waiversSobj;
    }

    private Map<String , Discount_and_Waiver_approval_parameters__mdt> getApprovalParametersMdt(){
        Map<String , Discount_and_Waiver_approval_parameters__mdt> lMapOptionApprovalMetadata 
            = new Map<String , Discount_and_Waiver_approval_parameters__mdt>();

        for( Discount_and_Waiver_approval_parameters__mdt iApprovalMeta : 
            [ SELECT Id, Waiver_CCO__c, Waiver_RM__c, CCO__c, CF__c, Initial_Approver_Email__c, Option__c,
                RM__c, RM_1__c, RM_2__c, Sellers__c, Waiver_RM_1__c, Initial_Approver_Queue_Name__c, 
                Manager__c, VP__c, Waiver_VP__c, Maximum_Number_of_Employees__c, Minimum_Number_of_Employees__c
              FROM Discount_and_Waiver_approval_parameters__mdt WHERE Country__c = :this.opp.Account.BillingCountry
              AND Label LIKE :gMdtLabelPrefix + '%' 
              AND Minimum_Number_of_Employees__c < :this.opp.Quantity_Offer_Number_of_Employees__c
              AND Maximum_Number_of_Employees__c >= :this.opp.Quantity_Offer_Number_of_Employees__c ] ){
            lMapOptionApprovalMetadata.put( iApprovalMeta.Option__c , iApprovalMeta );
        }

        if( lMapOptionApprovalMetadata.isEmpty() ){
            for( Discount_and_Waiver_approval_parameters__mdt iApprovalMeta : 
                [ SELECT Id, Waiver_CCO__c, Waiver_RM__c, CCO__c, CF__c, Initial_Approver_Email__c, Option__c,
                        RM__c, RM_1__c, RM_2__c, Sellers__c, Waiver_RM_1__c, Initial_Approver_Queue_Name__c, 
                        Manager__c, VP__c, Waiver_VP__c
                FROM Discount_and_Waiver_approval_parameters__mdt WHERE Country__c = 'United States'
                AND Label LIKE :gMdtLabelPrefix + '%' 
                AND Minimum_Number_of_Employees__c < :this.opp.Quantity_Offer_Number_of_Employees__c
                AND Maximum_Number_of_Employees__c >= :this.opp.Quantity_Offer_Number_of_Employees__c ] ){
                lMapOptionApprovalMetadata.put( iApprovalMeta.Option__c , iApprovalMeta );
            }
        }

        return lMapOptionApprovalMetadata;
    }

    @testVisible
    private void evaluateDiscount(String metadataOption, QuoteLineItem q){        
        Integer tempLevel = getDiscountApprovalLevel(metadataOption, q.Discount__c);
        Boolean lEvaluateAssertData;
        Boolean lApproved;

        if(tempLevel > 0){
            if( q.Fee_Type__c.contains('Enterprise') ){
                quote.Enterprise_Discount_Approval_Needed__c = true;               
                Assert_Data__c lESDiscount = gMapFeeTypeMapSobjNOldFieldValue.get( 'Enterprise Subscription' )?.get( 'quotelineitem.discount__c' );
                Assert_Data__c lQuoteCopay = gMapFeeTypeMapSobjNOldFieldValue.get( 'Enterprise Subscription' )?.get( 'quote.copay__c' );

                if( lESDiscount != null && !( lESDiscount?.Pre_Approved_Condition__c == true && !gInCurrentContract ) ){                    
                    lApproved = true;
                    
                    if( !String.isBlank( lQuoteCopay?.Old_Value__c ) && lQuoteCopay.Old_Value__c != this.to.hasCopay ){
                        lApproved = false;
                    }

                    if( lESDiscount.Old_Value__c == null ||
                        ( this.to.proposal.accessFee.sObj.Discount__c != null && 
                          this.to.proposal.accessFee.sObj.Discount__c > Decimal.valueOf( lESDiscount.Old_Value__c ) ) ){
                        lApproved = false;
                    }
                    
                    quote.Enterprise_Subscription_Approved__c = lApproved;
                } else {
                    quote.Enterprise_Subscription_Approved__c = false;
                }  

            } else if(q.Fee_Type__c.contains('Setup') && !( q.Quote.Opportunity.RecordType.DeveloperName.contains('SMB') && quote.Are_Setup_Fee_Included__c == 'No' ) ){    
                quote.Setup_Discount_Approval_Needed__c = true;

                Assert_Data__c lSetupFeeDiscount = gMapFeeTypeMapSobjNOldFieldValue.get( 'Setup Fee' )?.get( 'quotelineitem.discount__c' );

                if( lSetupFeeDiscount != null && !( lSetupFeeDiscount?.Pre_Approved_Condition__c == true && !gInCurrentContract ) ){  
                    lApproved = true;

                    if( lSetupFeeDiscount.Old_Value__c == null ||
                        ( this.to.proposal.setupFee.sObj.Discount__c != null && 
                          this.to.proposal.setupFee.sObj.Discount__c > Decimal.valueOf( lSetupFeeDiscount.Old_Value__c ) )){                                                                
                        lApproved = false;
                    }  
                    
                    quote.Setup_Discount_Approved__c = lApproved;
                } else {
                    quote.Setup_Discount_Approved__c = false;
                }                
            } 

            if( lApproved == null || !lApproved ){ 
                this.currentDiscountLevel = currentDiscountLevel < tempLevel ? tempLevel : currentDiscountLevel; 
            }     
        }
    }
    
    protected virtual Integer getDiscountApprovalLevel(String metadataOption, Decimal discount){
        Discount_and_Waiver_approval_parameters__mdt approvalMeta = mapOptionApprovalMetadata.get(metadataOption);
   
        if( discount == null || approvalMeta == null ){ return 0; }

        Integer approvalLevel = 0;

        if( discount > approvalMeta.CCO__c || discount > approvalMeta.CF__c ){
            approvalLevel = 4;    
        } else if( discount > approvalMeta.RM_1__c && discount <= approvalMeta.RM__c ){
            approvalLevel = 3;             
        } else if( discount > approvalMeta.RM_2__c && discount <= approvalMeta.RM_1__c ){
            approvalLevel = 2;
        } else if( discount > approvalMeta.Sellers__c && discount <= approvalMeta.RM_2__c ){
            approvalLevel = 1;
        }

        return approvalLevel;
    }

    @testVisible
    private void setWaiverApproverNeeded(){
        to.commercialConditionsApprovalNeeded |= isApprovalNeededForPropItem(to.proposal.accessFee);
    }

    private Boolean isApprovalNeededForPropItem(FastTrackProposalCreationTO.ProposalItem item){
        Boolean waiverApprovalNeeded = false;

        if( item?.payments == null || item.payments.isEmpty() ){ return waiverApprovalNeeded; }

        List<Id> waiversIdToApprove = new List<Id>();
        
        for( FastTrackProposalCreationTO.Payment payment : item.payments ){
            if( payment.waivers != null ){
                for( FastTrackProposalCreationTO.Waiver waiver : payment.waivers ){
                    if( waiver.sObj?.Id == null ){ continue; }
                    waiversIdToApprove.add( waiver.sObj.Id );
                }
            }
        }

        List< Waiver__c > lLstAllWaivers = getAllWaivers( waiversIdToApprove );

        Boolean lIsWaiverPreApproved = checkIfWaiverIsPreApproved( lLstAllWaivers );

        if( lIsWaiverPreApproved ){ 

            approveWaivers( lLstAllWaivers );

        } else {
            for( Waiver__c iWaiver : lLstAllWaivers ){    
                waiverApprovalNeeded |= isApprovalNeededForWaiver( iWaiver );
            }
        }

        item.waiversApprovalNeeded = waiverApprovalNeeded;

        return waiverApprovalNeeded;
    }

    @TestVisible
    private Boolean isApprovalNeededForWaiver(Waiver__c waiverSobj){
        if( waiverSobj.Approval_Status__c == 'Pending Approval' ){ return true; }
        
        Integer approvalLevel = getWaiverApproverLevelNeeded( waiverSobj.Number_of_Months_after_Launch__c );
        Boolean isApprovalNeeded = false;

        waiverSobj.Approval_level__c = approvalLevel;
        if(this.opp.RecordTypeId == this.IndirectRecordTypeId){
            if(waiverSobj.Approval_Status__c != 'Approved' && waiverSobj.Approval_Status__c != 'Rejected'){
                waiverSobj.Approval_level__c = 1;
                waiverSobj.Approval_Status__c =  'New';
                isApprovalNeeded = true;
                waiversToUpdtLvApproval.add( waiverSobj );
                return isApprovalNeeded;         
            }
            return isApprovalNeeded;
        }
        if( approvalLevel == 0 ){
            waiverSobj.Approval_Status__c =  'No Need for Approval';
            waiverSobj.Rationale__c = 'Standard Waiver';
        } else {
            waiverSobj.Approval_Status__c =  'New';
            isApprovalNeeded = true;
        }

        waiversToUpdtLvApproval.add( waiverSobj );

        return isApprovalNeeded;
    }
    
    private virtual void approveWaivers( List< Waiver__c > aLstWaiver ){
        Set< String > lSetStatusNotChangeToApproved = new Set< String >{
            'Approved',
            'Pending Approval',
            'No Need for Approval'
        };
        for( Waiver__c iWaiver : aLstWaiver ){
            if( lSetStatusNotChangeToApproved.contains( iWaiver.Approval_Status__c ) ){ continue; }

            iWaiver.Approval_Status__c = 'Approved';
            waiversToUpdtLvApproval.add( iWaiver );
        }
    }

    protected virtual Integer getWaiverApproverLevelNeeded(Decimal waiverMonths){
        Discount_and_Waiver_approval_parameters__mdt approvalMeta = mapOptionApprovalMetadata.get('Waiver');

        if( waiverMonths == null || waiverMonths == 0 || approvalMeta == null ){ return 0; }
        
        Integer approvalLevel = 0;

        if( waiverMonths >= approvalMeta.Waiver_RM_1__c && waiverMonths < approvalMeta.Waiver_RM__c ){
            approvalLevel = 1;
        } else if( waiverMonths >= approvalMeta.Waiver_RM__c  && waiverMonths < approvalMeta.Waiver_CCO__c ){
            approvalLevel = 2;
        } else if( waiverMonths >= approvalMeta.Waiver_CCO__c ){
            approvalLevel = 3;
        }

        return approvalLevel;
    }

    private void evaluateCopay(Map<Id, QuoteLineItem> mapIdFeeObj) {
        for (Id lineItemId : mapIdFeeObj.keySet()) {
            QuoteLineItem lineItem = mapIdFeeObj.get(lineItemId);
            Integer approvalLevel = 0;
            
            if (lineItem.Fee_Type__c == 'Enterprise Subscription' && quote.Copay__c == 'Yes') {

                if (lineItem.minInvestment__c       < lineItem.Variable_Price_Floor__c          || 
                    lineItem.monthlyCap__c          < lineItem.Variable_Price_Ceiling__c        ||
                    lineItem.priceperEnrolled__c    < lineItem.Variable_Price_Unit_Increase__c  ) {
                    approvalLevel = 3;
                } else {
                    approvalLevel = 2;
                }
                
                this.currentDiscountLevel = currentDiscountLevel < approvalLevel ? approvalLevel : currentDiscountLevel;
                quote.Enterprise_Discount_Approval_Needed__c = true; 
            }
        }        
    }

    private void getApprovedSobjFieldValues(){
        Map< String , Assert_Data__c > lMapEsFeeFieldOldValue = new Map< String , Assert_Data__c >();
        Map< String , Assert_Data__c > lMapSetupFeeFieldOldValue = new Map< String , Assert_Data__c >();

        for( Assert_Data__c iAssertData : 
                [ SELECT Object_Name__c, Old_Value__c, Field_Name__c, Fee_Type__c, New_Value__c, Pre_Approved_Condition__c 
                  FROM Assert_Data__c WHERE Opportunity__c = :to.opportunityId AND
                  Fee_Type__c IN ( 'Enterprise Subscription' , 'Setup Fee' ) ] ){
            if( iAssertData.Fee_Type__c.contains( 'Enterprise' ) ){
                lMapEsFeeFieldOldValue.put( iAssertData.Object_Name__c + '.' + iAssertData.Field_Name__c , iAssertData );
            } else {
                lMapSetupFeeFieldOldValue.put( iAssertData.Object_Name__c + '.' + iAssertData.Field_Name__c , iAssertData );
            }
        }  

        gMapFeeTypeMapSobjNOldFieldValue.put( 'Enterprise Subscription' , lMapEsFeeFieldOldValue );
        gMapFeeTypeMapSobjNOldFieldValue.put( 'Setup Fee' , lMapSetupFeeFieldOldValue );
    }

    private Boolean checkIfWaiverIsPreApproved( List< Waiver__c > aLstWaiver ){
        Assert_Data__c lWaiverMonthsAssertData = gMapFeeTypeMapSobjNOldFieldValue.get('Enterprise Subscription')?.get('waiver__c.number_of_months_after_launch__c');

        if( lWaiverMonthsAssertData?.Pre_Approved_Condition__c == true && !gInCurrentContract ){
            return false;
        }

        Decimal lTotalWaiverMonths = 0;

        for( Waiver__c iWaiver : aLstWaiver ){
            lTotalWaiverMonths += ( iWaiver.Number_of_Months_after_Launch__c == null ? 0 : 
                iWaiver.Number_of_Months_after_Launch__c );
        }

        String lApprovedWaiverMonths = lWaiverMonthsAssertData?.Old_Value__c;
        
        Integer lTotalApprovedWaiverMonths = String.isBlank( lApprovedWaiverMonths ) ? 0 : Integer.valueOf( lApprovedWaiverMonths );

        return lTotalWaiverMonths <= lTotalApprovedWaiverMonths;
    }

    private List< Waiver__c > getAllWaivers( List< Id > aLstWaiverId ){
        return [ SELECT Id, Approval_level__c,Number_of_Months_after_Launch__c, Start_Date__c,End_Date__c,Approval_Status__c,Approval_Status_Formula__c  
                 FROM Waiver__c WHERE Id =: aLstWaiverId ];
    }

    public class CustomException extends Exception {}
    
    public static FastTrackCommercialConditionsHandler getInstanceByOppId( String aOppId , FastTrackProposalCreationTO aProposalTO ){
        /* Opportunity flow as written in class name */
        Map< String , String > gMapRTDevNameClassName = new Map< String , String >{
            'Client_Sales_New_Business' => 'FastTrackCommercialConditionsHandler',
            'Client_Success_Renegotiation' => 'FastTrackCommercialConditionsHandler',
            'SMB_New_Business' => 'FastTrackSmbCommercialConditions',
            'SMB_Success_Renegotiation' => 'FastTrackSmbCommercialConditions',
            'Indirect_Channel_New_Business' => 'FastTrackCommercialConditionsHandler'
        };
        String lClassToInstanciate;
        FastTrackCommercialConditionsHandler lCommercialConditionsHandler;

        try{
            Opportunity lOpp = [ SELECT RecordType.DeveloperName FROM Opportunity WHERE Id = :aOppId ];
            lClassToInstanciate = gMapRTDevNameClassName.get( lOpp.RecordType.DeveloperName );

            lCommercialConditionsHandler = (FastTrackCommercialConditionsHandler) Type.forName( lClassToInstanciate ).newInstance();
            
            lCommercialConditionsHandler.init( aProposalTO );
        } catch( Exception aException ){
            lCommercialConditionsHandler = new FastTrackCommercialConditionsHandler( aProposalTO );            
        }

        return lCommercialConditionsHandler;
    }
}