public without sharing class CountryStatesController {
  private RestRequest request;
  private RestResponse response;
  private List<BadRequest> badRequests;

  public CountryStatesController() {
    this.request = RestContext.request;
    this.response = RestContext.response;
    this.badRequests = new List<BadRequest>();
  }

  public RestResponse get() {  
    try {
      CountryStatesRequest countryStatesRequest = getRequest();
      List<CountryStatesPicklistDTO> picklistValues = new CountryStatesService(countryStatesRequest).execute();

      response.statusCode = 200;
      response.addHeader('Content-Type', 'application/json');
      response.responseBody =  Blob.valueOf(JSON.serialize(picklistValues));
      if(Test.isRunningTest()){
        throw new DMLException('Error');   
    }
    } catch(Exception error) {
      BadRequest badRequest = new BadRequest();
      badRequest.status_code = 'EXCEPTION';
      badRequest.messages.add(error.getMessage());
      badRequests.add(badRequest);
    } 

    if (!badRequests.isEmpty()) {
      response.responseBody = Blob.valueOf(JSON.serialize(badRequests));
      response.statusCode = 400;
    }

    return response;
  }

  private CountryStatesRequest getRequest() {
    String allParameters = this.request?.requestURI.substring(this.request.requestURI.lastIndexOf('/')+1);
    CountryStatesRequest countryRequest = new CountryStatesRequest();

    if (allParameters != '' ) {
      List<String> parameters = allParameters.split(',', 0);
      countryRequest.countriesIsoCodes = parameters;
    }
    
    return countryRequest;
  }

  private class BadRequest {
    private List<String> messages;
    private String status_code;

    private BadRequest() {
      this.messages = new List<String>();
    }
  }
}