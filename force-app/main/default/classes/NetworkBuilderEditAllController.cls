/**
 * Created by gympasser on 11/03/2022.
 */

public with sharing class NetworkBuilderEditAllController {
    private final static Integer MAX_RESULTS = 20;
    private static final Id gymsPartnerRT = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Gyms_Partner').getRecordTypeId();
    private static final PS_Constants constants = PS_Constants.getInstance(); 
    private static final String className = PartnerSetupController.class.getName();

    @AuraEnabled(cacheable=true)
    public static Id getRecordTypeId(String recordTypeDevName){
        return ContactService.getRecordTypeId(recordTypeDevName);
    }

    @AuraEnabled(Cacheable=true)
    public static List<AccountWrapper> getAccountData(List<Id> accountIds) {
        List<AccountWrapper> wrappers = new List<AccountWrapper>();

        AccountSelector selector = (AccountSelector)Application.Selector.newInstance(Account.SObjectType);
        Set<Id> accountIdsSet = new Set<Id>(accountIds);
        for (Account account : selector.selectWithExclusivityApprovalStatusById(accountIdsSet)) {
            AccountWrapper accountWrapper = new AccountWrapper(account.Id,
                                                               exclusivityApprovalNeeded(account.BillingCountryCode),
                                                               isExclusivityApproved(account.Approval_Status_Exclusivity__c));
            wrappers.add(accountWrapper);
        }

        return wrappers;
    }

    @AuraEnabled
    public static void saveDataExistingContact(List<Id> accountIds, Id selectedLegalRepContactId) {
        ContactService.updateAccountsWithExistingLegalRepresentative(accountIds, selectedLegalRepContactId, 'Gyms_Partner');
    }

    @AuraEnabled
    public static void saveDataNewContact(List<Id> accountIds, String newContactSerialized) {
        ContactService.updateAccountsWithNewLegalRepresentative(accountIds, newContactSerialized, 'Gyms_Partner');
    }

    @AuraEnabled(Cacheable=true)
    public static List<LookupSearchResult> search(String searchTerm, List<String> selectedIds) {
        // Prepare query paramters
        searchTerm += '*';

        // Execute search query
        List<List<SObject>> searchResults = [
                FIND :searchTerm
                IN ALL FIELDS
                        RETURNING
                        Contact(Id, Name, Account.Name WHERE Id NOT IN :selectedIds AND RecordTypeId = :gymsPartnerRT)
                LIMIT :MAX_RESULTS
        ];
        system.debug('## searchResults: '+searchResults);
        // Prepare results
        List<LookupSearchResult> results = new List<LookupSearchResult>();

        // Extract Accounts & convert them into LookupSearchResult
        String contactIcon = 'standard:contact';
        Contact[] contacts = (List<Contact>) searchResults[0];
        for (Contact contact : contacts) {
            String subtitle = 'Contact • ' + contact.Account.Name;
            results.add(new LookupSearchResult(contact.Id, 'Contact', contactIcon, contact.Name, subtitle));
        }

        // Optionnaly sort all results on title
        results.sort();

        return results;
    }

    @AuraEnabled(Cacheable=true)
    public static List<LookupSearchResult> getRecentlyViewed() {
        List<LookupSearchResult> results = new List<LookupSearchResult>();
        // Get recently viewed records of type Account or Opportunity
        List<RecentlyViewed> recentRecords = [
                SELECT Id, Name
                FROM RecentlyViewed
                WHERE Type = 'Contact' AND RecordTypeId = :gymsPartnerRT
                ORDER BY LastViewedDate DESC
                LIMIT 5
        ];
        // Convert recent records into LookupSearchResult
        for (RecentlyViewed recentRecord : recentRecords) {
            results.add(
                    new LookupSearchResult(
                            recentRecord.Id,
                            'Contact',
                            'standard:contact',
                            recentRecord.Name,
                            'Contact • ' + recentRecord.Name
                    )
            );
        }
        return results;
    }

    @AuraEnabled(cacheable=false)
    public static void updateAccountLst(String partnersToUpdate, List<Id> selectedRecords) {
        fflib_ISObjectUnitOfWork uow = Application.UnitOfWork.newInstance(); 
        List<Account> accountsToUpdate = new List<Account>();

        try {
            for (Id accountId : selectedRecords) {
                Account aPartner = (Account) JSON.deserialize(partnersToUpdate, Account.class);
                aPartner.id = accountId;
                accountsToUpdate.add(aPartner);            
            }
            uow.registerDirty(accountsToUpdate);  
            uow.commitWork();
            
        } catch(Exception e){
            createLog(e.getMessage(), e.getStackTraceString(), 'updateAccountLst', JSON.serialize(accountsToUpdate),null);
            throw new NetworkBuilderEditAllControllerException( e.getMessage() );
        }

       
    }

    @AuraEnabled(cacheable=true)
    public static Id getAccountRecordTypeId(String recordTypeDevName){
        Id rtIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(recordTypeDevName).getRecordTypeId();
        return rtIdAccount;
    }

    private static Boolean exclusivityApprovalNeeded(String accountBillingCountryCode) {
        // currently only for Brazil, custom metadata needed for future releases
        Boolean approvalNeeded;
        String countryCodesRequiringApproval = getCountryCodesRequiringApproval();
        if (!String.isBlank(countryCodesRequiringApproval) && countryCodesRequiringApproval.contains(accountBillingCountryCode)) {
            approvalNeeded = true;
        } else {
            approvalNeeded = false;
        }
        return approvalNeeded;
    }

    private static Boolean isExclusivityApproved(String exclusivityApprovalStatus) {
        Boolean exclusivityApproved;
        if (exclusivityApprovalStatus == 'Approved') {
            exclusivityApproved = true;
        } else {
            exclusivityApproved = false;
        }
        return exclusivityApproved;
    }

    private static String getCountryCodesRequiringApproval() {
        List<Partner__mdt> approvalProcessCustomMetadata = [SELECT Value__c FROM Partner__mdt WHERE DeveloperName = :constants.EXCLUSIVITY_APPROVAL_COUNTRIES];
        return !approvalProcessCustomMetadata.isEmpty() ? approvalProcessCustomMetadata.get(0).Value__c : '';
    }

    @TestVisible
    private static void createLog(String exceptionMsg, String exceptionStackTrace, String methodName, String dataScope, Id objectId) {
        DebugLog__c log = new DebugLog__c(
                Origin__c = '['+className+']['+methodName+']',
                LogType__c = constants.LOGTYPE_ERROR,
                RelatedToSquad__c = constants.LOGTYPE_SQUAD_PARTNERS,
                ExceptionMessage__c = exceptionMsg,
                StackTrace__c = exceptionStackTrace,
                DataScope__c = dataScope,
                ObjectId__c = objectId
        );
        Logger.createLog(log);
    }

    public class AccountWrapper {
        @AuraEnabled
        public String accountId;
        @AuraEnabled
        public Boolean countryRequiresExclusivityApproval;
        @AuraEnabled
        public Boolean isExclusivityApproved;

        public AccountWrapper(Id accountId, Boolean countryRequiresExclusivityApproval, Boolean isExclusivityApproved) {
            this.accountId = accountId;
            this.countryRequiresExclusivityApproval = countryRequiresExclusivityApproval;
            this.isExclusivityApproved = isExclusivityApproved;
        }
    }



    private class NetworkBuilderEditAllControllerException extends Exception {}

}