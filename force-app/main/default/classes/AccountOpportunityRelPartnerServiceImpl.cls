/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 07-30-2021
 * @last modified by  : gft.samuel.silva@ext.gympass.com
 * Modifications Log 
 * Ver   Date         Author                             Modification
 * 1.0   07-19-2021   gft.samuel.silva@ext.gympass.com   Initial Version
**/
public with sharing class AccountOpportunityRelPartnerServiceImpl implements IAccountOpportunityRelationshipService{

    private static final PS_Constants constants = PS_Constants.getInstance();
    private static final String className = AccountOpportunityRelPartnerServiceImpl.class.getName();

    private final ID gPARTNER_RT = Schema.SObjectType.Account_Opportunity_Relationship__c.getRecordTypeInfosByDeveloperName().get(constants.ACC_OPP_REL_RT_PARTNERS_AOR).getRecordTypeId();

    public void createAccountOpportunityRelationship(Map<Id, Id> aAccountByOppIdMap){       

        fflib_ISObjectUnitOfWork uow = Application.UnitOfWork.newInstance();

        for(Id iAccId : aAccountByOppIdMap.keySet()){
            Account_Opportunity_Relationship__c accOppRel = new Account_Opportunity_Relationship__c();
            accOppRel.recordTypeId = gPARTNER_RT;
            accOppRel.Opportunity__c = aAccountByOppIdMap.get(iAccId);
            accOppRel.Account__c = iAccId;
            uow.registerNew(accOppRel);
        }     
        try{          
            uow.commitWork();
        }   catch (Exception e){
            DebugLog__c log = new DebugLog__c(
                    Origin__c = '['+className+'][createAccountOpportunityRelationship]',
                    LogType__c = constants.LOGTYPE_ERROR,
                    RelatedToSquad__c = constants.LOGTYPE_SQUAD_PARTNERS,
                    ExceptionMessage__c = e.getMessage(),
                    StackTrace__c = e.getStackTraceString(),
                    DataScope__c = JSON.serialize(aAccountByOppIdMap)
            );
            Logger.createLog(log);
            throw new AccountOpportunityRelPartnerServiceImplException(e.getMessage());
        }    
    
    }

    public Map<SObjectType, Type>  test(){

        return new Map<SObjectType, Type> {
            Account.SObjectType => AccountSelector.class,
            Opportunity.SObjectType => OpportunitySelector.class,
            EmailTemplate.sObjectType => EmailTemplateSelector.class,
            CustomNotificationType.SObjectType => CustomNotificationTypeSelector.class
        }; 

    }

    public class AccountOpportunityRelPartnerServiceImplException extends Exception {}

}