/*
 * @author Bruno Pinho
 * @date March/2019
 * @description Schedulable class to run BatchFixGympassEventFields
 */
global class SchBatchFixGympassEventFields Implements Schedulable
    {
        global void execute(SchedulableContext sc)
        {
            scheduleBatchFixGympassEventFields();
        }

        public void scheduleBatchFixGympassEventFields()
        {
            Database.executeBatch(new BatchFixGympassEventFields());
        }
    }