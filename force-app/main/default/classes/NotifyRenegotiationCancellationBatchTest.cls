/**
 * @description       : 
 * @author            : roei@gft.com
 * @group             : 
 * @last modified on  : 01-06-2021
 * @last modified by  : roei@gft.com
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   01-06-2021   roei@gft.com   Initial Version
**/
@isTest(seeAllData=false)
public with sharing class NotifyRenegotiationCancellationBatchTest {
    
	@TestSetup
    public static void createData(){        
        Account lAcc = generateClientsAccount();              
        Database.insert( lAcc );
    
        Pricebook2 lStandardPriceBook = new Pricebook2(
            Id = Test.getStandardPricebookId(),
            IsActive = true
        );
        Database.update( lStandardPriceBook );

        Pricebook2 lPb = lStandardPriceBook;  
        
        Opportunity lOpp = new Opportunity(CurrencyIsoCode='BRL');
        lOpp = generateRenegotiationOpp( lAcc.Id, lPb );
        Database.insert( lOpp );
    }
    
    @isTest
    public static void shouldSendEmailToOppOwner() {   
    	Test.startTest();
        	Id lBatchJobId = Database.executeBatch( new NotifyRenegotiationCancellationBatch() , 200 );
        Test.stopTest();
        
        System.assert( lBatchJobId != null );
    }
    
    @isTest
    public static void shouldScheduleBatch() {
		Test.startTest();
            String lSchString = '0 0 23 * * ?';
            System.schedule( 'Test Notify Cancellation batch' , lSchString , new NotifyRenegotiationCancellationBatch() ); 
        Test.stopTest();
    }
    
    private static Opportunity generateRenegotiationOpp( Id aAccId , Pricebook2 aPb ){
        Id lOppRtId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Success_Renegotiation').getRecordTypeId();
        Opportunity lOpp = new Opportunity();
        lOpp.recordTypeId = lOppRtId;
        lOpp.StageName = 'Qualification';
        lOpp.Sub_Type__c = 'Retention';
        lOpp.cancellation_date__c = Date.today() + 5;
        lOpp.AccountId = aAccId;
        lOpp.CloseDate = Date.today();
        lOpp.Engagement_Journey_Completed__c = true;
        lOpp.Name='Opp Test'; 
        lOpp.Pricebook2Id = aPb.Id;
        return lOpp;
    }
    
    private static Account generateClientsAccount(){
        ID lAccRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
        Account lAcc = new Account();
        lAcc.RecordTypeId = lAccRecordTypeId;
        lAcc.Name = 'Test Account';
        lAcc.BillingCountry = 'Brazil';
        lAcc.BillingState = 'São Paulo';
        lAcc.NumberOfEmployees = 2000;
        return lAcc;
    }
}