/**
 * @description       : 
 * @author            : roei@gft.com
 * @group             : 
 * @last modified on  : 09-14-2020
 * @last modified by  : roei@gft.com
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   09-14-2020   roei@gft.com   Initial Version
**/
@isTest
public class OpportunityTriggerCurrencyTest {

    @TestSetup
    private static void oppData(){
        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();

        Account acc = new Account();
        acc.Name = 'Test Acc';
        acc.RecordTypeId = rtId;
        acc.BillingCountry = 'Brazil';
        acc.BillingState = 'São Paulo';
        acc.NumberOfEmployees = 100;

        Database.insert( acc );

        /**Opportunity*/                
        List<Opportunity> oppToInsert = new List<Opportunity>();        
        Id oppRtId =   Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        
        Opportunity accOpp = new Opportunity();
        accOpp.recordTypeId = oppRtId;
        accOpp.AccountId = acc.id;
        accOpp.Name = 'academiaBrasilCompanyOpp';      
        accOpp.Gym_agreed_to_an_API_integration__c	= 'Yes';
        accOpp.Integration_Fee_Deduction__c = 'No';
        accOpp.CloseDate = Date.today();
        accOpp.Success_Look_Like__c = 'Yes';
        accOpp.Success_Look_Like_Description__c	= 'Money money';
        accOpp.StageName = 'Qualification';
        accOpp.Type	= 'Expansion';     
        accOpp.CurrencyIsoCode = 'BRL';
        accOpp.Gympass_Plus__c = 'Yes';

        Database.insert( accOpp );
    }

    @isTest 
    private static void shouldThrowErrorOnUpdateCountryAcc() {
        Map<String , String> mapCurrencyByCountry = new Map<String , String>();

        for( CurrencyByCountry__mdt iCurrencyByContry : [ SELECT Country__c, Currency__c, Code__c FROM CurrencyByCountry__mdt ] ){
            mapCurrencyByCountry.put(iCurrencyByContry.Country__c, iCurrencyByContry.Currency__c);
        }
        
        Account acc = [SELECT id, Name, billingCountry FROM Account WHERE Name = 'Test Acc'];
             
        Id oppRtId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        
        Opportunity accOpp = new Opportunity();
        accOpp.recordTypeId = oppRtId;
        accOpp.AccountId = acc.Id;
        accOpp.Name = 'academiaBrasilCompanyOpp';      
        accOpp.Gym_agreed_to_an_API_integration__c	= 'Yes';
        accOpp.Integration_Fee_Deduction__c = 'No';
        accOpp.CloseDate = Date.today();
        accOpp.Success_Look_Like__c = 'Yes';
        accOpp.Success_Look_Like_Description__c	= 'Money money';
        accOpp.StageName = 'Qualification';
        accOpp.Type	= 'Expansion';     
        accOpp.CurrencyIsoCode = 'BRL';
        accOpp.Gympass_Plus__c = 'Yes';

        Test.startTest();
        
        Database.insert( accOpp );
        
        Test.stopTest();
        
        accOpp = [ SELECT CurrencyIsoCode FROM Opportunity WHERE Id = :accOpp.Id ];
        
        System.assertEquals(accOpp.CurrencyIsoCode, mapCurrencyByCountry.get(acc.billingCountry),
            'Opportunity currency not set correctly');
    }
}