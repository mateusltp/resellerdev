/**
 * @File Name          : OpportunityTriggerAccProdHelperTest.cls
 * @Description        : 
 * @Author             : GEPI@GFT.com
 * @Group              : 
 * @Last Modified By   : GEPI@GFT.com
 * @Last Modified On   : 07-24-2020
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    18/06/2020   Samuel Silva - GFT (slml@gft.com)     Initial Version
**/
@Istest
public with sharing class OpportunityTriggerAccProdHelperTest {
    
    @TestSetup
    static void setupData(){
        Id rtId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gym_Partner').getRecordTypeId();
        Account acc = new Account();
        acc.name='AcademiaBrasilCompanyPai';
        acc.RecordTypeId = rtId;
        acc.GP_Status__c = 'Active';
        acc.Legal_Title__c = 'title';
        acc.Id_Company__c = '11111';
        acc.ShippingState = 'Minas Gerais';
        acc.CAP_Value__c = 120;
        acc.ShippingCity = 'CityAcademiaBrasil';
        acc.ShippingStreet = 'Rua academiabrasilpai';
        acc.ShippingCountry = 'Brazil';
        acc.Gym_Type__c = 'Studios';
        acc.Gym_Classes__c = 'Cardio';
        acc.Types_of_ownership__c = Label.franchisePicklist;
        acc.Subscription_Type__c = 'Value per class';
        acc.Subscription_Period__c = 'Monthy value';
        acc.Subscription_Type_Estimated_Price__c    = 100;
        acc.Has_market_cannibalization__c = 'No';
        acc.Exclusivity__c = 'Yes';
        acc.Exclusivity_End_Date__c = Date.today().addYears(1);
        acc.Exclusivity_Partnership__c = 'Full Exclusivity';
        acc.Exclusivity_Restrictions__c= 'No';
        acc.Website = 'testing@tesapex.com';
        acc.Gym_Email__c = 'gymemail@apex.com';
        acc.Phone = '3222123123';
        acc.Can_use_logo__c = 'Yes';
        acc.Legal_Registration__c = 12123;
        acc.Legal_Title__c = 'Title LTDA';
        acc.Gyms_Identification_Document__c = 'CNPJ'; 
        INSERT acc;
        
        Id oppRtId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Gyms_Small_Medium_Partner').getRecordTypeId();
        Opportunity accOpp = new Opportunity();
        accOpp.recordTypeId = oppRtId;
        accOpp.AccountId = acc.id;
        accOpp.Name = 'academiaBrasilCompanyOpp'; 
        accOpp.CMS_Used__c = 'Yes';     
        accOpp.Should_OPS_team_send_the_training__c = 'Standard Training';
        accOpp.Training_Comments__c = 'Standard';
        accOpp.Gym_agreed_to_an_API_integration__c  = 'Yes';
        accOpp.Club_Management_System__c = 'Companhia Athletica';
        accOpp.Integration_Fee_Deduction__c = 'No';
        accOpp.CloseDate = Date.today();
        accOpp.Success_Look_Like__c = 'Yes';
        accOpp.Success_Look_Like_Description__c = 'Money money';
        accOpp.StageName = 'Qualificação';
        accOpp.Type = 'Expansion';  
        accOpp.Country_Manager_Approval__c = true;
        accOpp.Payment_approved__c = true;   
        accOpp.CurrencyIsoCode = 'BRL';
        accOpp.Gympass_Plus__c = 'Yes';
        accOpp.Standard_Payment__c = 'Yes';
        accOpp.Request_for_self_checkin__c = 'Yes';        
        INSERT accOpp;
        
        Id ctcRtId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Gyms_Partner').getRecordTypeId();
        Contact contactAcc = new Contact(
            FirstName = 'Fernando',
            LastName = 'Souza',
            Email = 'fernandoTestSouze@mail.com',
            recordTypeId = ctcRtId,
            MailingCountry = 'Brazil',
            Type_of_contact__c = 'Point of Contact;Legal Representative',
            Cargo__c = 'CEO',           
            accountId = acc.id
        );       
        INSERT contactAcc; 
        
        Id bkaccRtId = Schema.SObjectType.Bank_Account__c.getRecordTypeInfosByDeveloperName().get('Gyms_Bank_Account').getRecordTypeId();
        Bank_Account__c bankAccount = new Bank_Account__c();
        bankAccount.recordTypeId = bkaccRtId;
        bankAccount.Bank_Account_Number_or_IBAN__c = 'abc';
        bankAccount.Bank_Account_Ownership__c  = contactAcc.Name;
        bankAccount.Bank_Name__c = 'ItauFake';
        bankAccount.BIC_or_Routing_Number_or_Sort_Code__c = 'abc';
        bankAccount.Routing_Number__c = 1234;
        bankAccount.VAT_Number_or_UTR_number__c = 'tttt1231';
        INSERT bankAccount;

        Id abrRtId = Schema.SObjectType.Acount_Bank_Account_Relationship__c.getRecordTypeInfosByDeveloperName().get('Gyms_Account_Bank_Account_Partner').getRecordTypeId();
        Acount_Bank_Account_Relationship__c abr = new Acount_Bank_Account_Relationship__c();
        abr.RecordTypeId = abrRtId;
        abr.Account__c = acc.Id;
        abr.Bank_Account__c = bankAccount.id;
        INSERT abr;

        Id propRtId = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Gyms_Quote_Partner').getRecordTypeId();
        Quote proposal = new Quote();
        proposal.recordTypeId= propRtId;
        proposal.Name = 'academiaBrasilCompanyQuote';
        proposal.Signed__c = Date.today();

        proposal.Start_Date__c  = Date.today();
        proposal.Final_Date__c =  Date.today().addYears(4);
        proposal.Priority_Tier__c = 'Tier 1';
        proposal.PR_Activity__c  = 'Yes';
        proposal.discounts_usage_volume_range__c = 'Yes';
        proposal.Volume_Discount_Type__c = 'By single user volume';
        proposal.Threshold_Value_1__c = 12;
        proposal.Threshold_Value_2__c = 32;                         
        proposal.Threshold_Value_3__c = 42;                
        proposal.Threshold_Value_4__c = 48;                               
        proposal.First_Discount_Range__c = 20;
        proposal.Second_Discount_Range__c = 24; 
        proposal.Third_Discount_Range__c = 56;
        proposal.Fourth_Discount_Range__c = 70;
        proposal.OpportunityId = accOpp.id;    
        INSERT proposal;

        Id prodRtId = Schema.SObjectType.Product_Item__c.getRecordTypeInfosByDeveloperName().get('Gyms_Product_Item_Partner').getRecordTypeId();
        Product_Item__c prod = new Product_Item__c();    
        prod.recordTypeId = prodRtId;
        prod.Product_Type__c = 'In person';
        prod.Is_Network_CAP__c = 'No';
        prod.CAP_Value__c = 100;
        prod.CurrencyIsoCode = 'BRL';
        prod.Do_You_Have_A_No_Show_Fee__c = 'No';
        prod.Has_Late_Cancellation_Fee__c = 'No';
        prod.Late_Cancellation_Percent__c = '0';
        prod.Maximum_Live_Class_Per_Month__c = 0;
        prod.No_Show_Fee_Percent__c = '0';
        prod.Product_Definition__c  ='Dança';
        prod.Product_Restriction__c = 'None';
        prod.Name = 'DançaAcademiaBrasilCompanyPai';
        prod.Gym_Classes__c = 'Dance;Cardio';
        prod.Opportunity__c = accOpp.Id;       
        prod.Max_Monthly_Visit_Per_User__c = 12;
        prod.Reference_Price_Value_Unlimited__c = 12;
        prod.Net_Transfer_Price__c  = 11;
        prod.Price_Visits_Month_Package_Selected__c = 400;
        INSERT prod;    

        Id recordTypePartnerForm = Schema.SObjectType.Ops_Setup_Validation_Form__c.getRecordTypeInfosByDeveloperName().get('Partner_Ops_Setup_Validation').getRecordTypeId();
        Ops_Setup_Validation_Form__c accOppForm = new Ops_Setup_Validation_Form__c (
            recordTypeId = recordTypePartnerForm,
            Name = accOpp.Name + ' - OPS Form',
            Status__c = 'Approved',
            Did_you_send_the_training_email__c = 'Yes',
            Opportunity__c = accOpp.Id ); 
        
        INSERT accOppForm;  
    
    }
    

    @IsTest
    public static void updateGymWithProductClassesTest(){
        Opportunity opp = [SELECT ID, StageName FROM Opportunity WHERE Name = 'academiaBrasilCompanyOpp' ];
        Product_Item__c prod = [SELECT Gym_Classes__c FROM Product_Item__c WHERE Name = 'DançaAcademiaBrasilCompanyPai'];
        Test.startTest();
            opp.StageName = 'Lançado/Ganho';
            UPDATE opp;
            Account acc = [SELECT Gym_Classes__c FROM Account WHERE Name = 'AcademiaBrasilCompanyPai'];
            System.assertEquals(prod.Gym_Classes__c, acc.Gym_Classes__c, 'Product Update in Account failed');
        Test.stopTest();
        
    }

}