/**
 * @description       : 
 * @author            : roei@gft.com
 * @group             : 
 * @last modified on  : 12-16-2021
 * @last modified by  : roei@gft.com
**/
@IsTest
public class OrderTagusEventRequestTest {
  /*
  @IsTest
  public static void execute() {
    Account account = AccountMock.getGympassEntity();
    account.Send_To_Tagus__c = true;
    account.UUID__c = new Uuid().getValue();
    insert account;

    Contact contact = ContactMock.getStandard(account);
    insert contact;

    account.Attention__c = contact.Id;
    update account;

    Pricebook2 pricebook = new Pricebook2(
      CurrencyIsoCode='BRL',
      Id = Test.getStandardPricebookId(),
      IsActive = true
    );

    List<Product2> products = new List<Product2>();

    Product2 setupFee = ProductMock.getSetupFee();
    products.add(setupFee);

    Product2 accessFee = ProductMock.getAccessFee();
    products.add(accessFee);

    insert products;

    List<PricebookEntry> pricebookEntries = new List<PricebookEntry>();

    PricebookEntry setupFeePricebookEntry = PricebookMock.getPricebookEntry(pricebook, setupFee);
    pricebookEntries.add(setupFeePricebookEntry);

    PricebookEntry accessFeePricebookEntry = PricebookMock.getPricebookEntry(pricebook, accessFee);
    pricebookEntries.add(accessFeePricebookEntry);

    insert pricebookEntries;

    Opportunity opportunity = OpportunityMock.getNewBusiness(account, pricebook);
    opportunity.Gympass_Entity__c = account.Id;
    insert opportunity;

    Account_Opportunity_Relationship__c splitBilling = new Account_Opportunity_Relationship__c(
      Account__c = account.Id,
      Billing_Percentage__c = 100,
      Maintenance_Fee_Billing_Percentage__c = 100,
      Opportunity__c = opportunity.Id,
      Prof_Serv_Setup_Fee_Billing_Percentage__c = 100,
      Setup_Fee_Billing_Percentage__c = 100
    );
    insert splitBilling;

    Quote quote = QuoteMock.getStandard(opportunity);
    insert quote;

    List<QuoteLineItem> quoteLineItems = new List<QuoteLineItem>();

    QuoteLineItem setupFeeLine = QuoteLineItemMock.getSetupFee(
      quote,
      setupFeePricebookEntry
    );
    quoteLineItems.add(setupFeeLine);

    QuoteLineItem accessFeeLine = QuoteLineItemMock.getEnterpriseSubscription(
      quote,
      accessFeePricebookEntry
    );
    quoteLineItems.add(accessFeeLine);

    insert quoteLineItems;

    List<Payment__c> payments = new List<Payment__c>();

    Payment__c setupFeePayment = PaymentMock.getStandard(setupFeeLine);
    payments.add(setupFeePayment);

    Payment__c accessFeePayment = PaymentMock.getStandard(accessFeeLine);
    payments.add(accessFeePayment);

    insert payments;

    List<Eligibility__c> eligibles = new List<Eligibility__c>();

    Eligibility__c setupFeeEligibility = EligibilityMock.getStandard(setupFeePayment);
    eligibles.add(setupFeeEligibility);

    Eligibility__c accessFeeEligibility = EligibilityMock.getStandard(accessFeePayment);
    eligibles.add(accessFeeEligibility);

    insert eligibles;

    List<Waiver__c> waivers = new List<Waiver__c>();

    Waiver__c setupFeeWaiver = WaiverMock.getStandard(setupFeePayment);
    waivers.add(setupFeeWaiver);

    Waiver__c accessFeeWaiver = WaiverMock.getStandard(accessFeePayment);
    waivers.add(accessFeeWaiver);

    insert waivers;

    Test.startTest();

    opportunity.StageName = 'Lançado/Ganho';
    update opportunity;

    Order order = [SELECT Id FROM Order LIMIT 1];

    OrderItem orderItem = [
      SELECT Id,
      (
        SELECT Id
        FROM Payments__r
      )
      FROM OrderItem
      WHERE OrderId = :order.Id
      LIMIT 1
    ];

    OrderTagusEventRequest orderTagusEventRequest = new OrderTagusEventRequest(order);

    System.assert(orderTagusEventRequest.getOrder() != null, 'Order not set in request');

    orderTagusEventRequest.addItem(orderItem, orderItem.Payments__r);

    List<OrderTagusEventRequest.ItemRequest> itemsRequest = orderTagusEventRequest.getItems();

    System.assert(!itemsRequest.isEmpty(), 'Items not set in request');

    for(OrderTagusEventRequest.ItemRequest itemRequest : itemsRequest) {
      System.assert(itemRequest.getItem() != null, 'Order item not set in request');

      System.assert(!itemRequest.getPayments().isEmpty(), 'Payments not set in request');
    }

    Test.stopTest();
  }
  */
}