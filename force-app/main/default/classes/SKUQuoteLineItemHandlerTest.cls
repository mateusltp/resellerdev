/**
 * @description       : 
 * @author            : tiago.ribeiro@gympass.com
 * @group             : 
 * @last modified on  : 06-08-2022
 * @last modified by  : tiago.ribeiro@gympass.com
**/
@isTest
public with sharing class SKUQuoteLineItemHandlerTest {
    @TestSetup
    static void makeData(){
        Account acc = DataFactory.newAccount();   
        insert acc;
        
        Pricebook2 standardPricebook = new Pricebook2(
            Id = Test.getStandardPricebookId(),
            IsActive = true
        );
        update standardPricebook;
        Pricebook2 pb = standardPricebook;

        Product2 lAcessFee = DataFactory.newProduct( 'Enterprise Subscription' , false , 'BRL' );
        lAcessFee.Payment_Type__c = 'Recurring fee';
        Database.insert( lAcessFee );

        Opportunity opp = DataFactory.newOpportunity( acc.Id, standardPricebook, 'Client_Sales_SKU_New_Business' );
        opp.Billing_Period__c = 'Monthly';
        opp.TotalOpportunityQuantity = 1000;

        PricebookEntry lAcessFeeESEntry = DataFactory.newPricebookEntry( standardPricebook , lAcessFee, opp);
        insert lAcessFeeESEntry;

        SKU_Price__c skuPrice = DataFactory.newSKUPrice(lAcessFee.Id, opp, acc, 1, 50);
        insert skuPrice;

        insert opp;
    }

    @isTest
    static void testPreventDefaultQuoteItemRemoval(){
        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
        List<QuoteLineItem> quoteLineItems = [SELECT Id, Is_Default__c FROM QuoteLineItem WHERE Quote.OpportunityId = :opp.Id LIMIT 1];

        String errorMessage = '';
        try{
            delete quoteLineItems;
        }catch(Exception e){
            errorMessage = e.getMessage();
        }

        System.assertEquals(quoteLineItems[0].Is_Default__c, true, 'Not default Quote Line Item');
        System.assertEquals(errorMessage.contains(label.errorDeleteSKUMessage), true, 'Not default Quote Line Item error');
    }

    @isTest
    static void testRemovalNonDefaultQuoteItem(){
        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
        List<QuoteLineItem> quoteLineItems = [SELECT Id, Is_Default__c FROM QuoteLineItem WHERE Quote.OpportunityId = :opp.Id LIMIT 1];
        quoteLineItems[0].Is_Default__c = false;
        update quoteLineItems;

        String errorMessage = '';
        try{
            delete quoteLineItems;
        }catch(Exception e){
            errorMessage = e.getMessage();
        }

        System.assertEquals(errorMessage, '', 'Default Quote Line Item error');
    }
}