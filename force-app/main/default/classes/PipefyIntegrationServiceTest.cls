@isTest
public with sharing class PipefyIntegrationServiceTest {
    

    @TestSetup
    static void makeData(){
        
        Account acc = DataFactory.newAccount();
        insert acc;

        Pricebook2 defaultPricebook = DataFactory.newPricebook();
        upsert defaultPricebook;

        Opportunity opp = DataFactory.newOpportunity(acc.Id, defaultPricebook, 'Indirect_Channel_New_Business', true);
        opp.Comments__c = 'Teste';
        opp.FastTrackStage__c = 'Launched/Won';
        insert opp;

        Product2 enterpriseSubscription = DataFactory.newProduct('Enterprise Subscription', false, 'BRL');
        insert enterpriseSubscription;

        Product2 setupFee = DataFactory.newProduct('Setup Fee', false, 'BRL');
        insert setupFee;

        PricebookEntry enterpriseSubscriptionEntry = DataFactory.newPricebookEntry(defaultPricebook, enterpriseSubscription, opp);
        insert enterpriseSubscriptionEntry;

        PricebookEntry setupFeeEntry = DataFactory.newPricebookEntry(defaultPricebook, setupFee, opp);
        insert setupFeeEntry;

        Quote oppQuote = DataFactory.newQuote(opp, 'IndirectQuote', 'Indirect_Channel');
        insert oppQuote;

        QuoteLineItem setupFeeLineItem = DataFactory.newQuoteLineItem(oppQuote, setupFeeEntry);
        insert setupFeeLineItem;

        QuoteLineItem enterpriseSubscriptionEntryLineItem = DataFactory.newQuoteLineItem(oppQuote, enterpriseSubscriptionEntry);
        insert enterpriseSubscriptionEntryLineItem;

        Payment__c acessFeePayment = DataFactory.newPayment(enterpriseSubscriptionEntryLineItem);
        acessFeePayment.Payment_Due_Days__c = 'Custom';
        acessFeePayment.Custom_Payment_Due_Days__c = 30;

        insert acessFeePayment;

        Eligibility__c eli = DataFactory.newEligibility(acessFeePayment);
        eli.Payment__c = acessFeePayment.Id;
        insert eli;

        Waiver__c wai = DataFactory.newWaiver(acessFeePayment);
        wai.Percentage__c = 100;
        insert wai;
        
        Waiver__c wai2 = DataFactory.newWaiver(acessFeePayment);
        wai2.Start_Date__c = system.today().adddays(11);
        wai2.End_Date__c = system.today().addDays(20);
        insert wai2;

        //setup fee
        
        Payment__c payForSetupFee = DataFactory.newPayment(setupFeeLineItem);
        insert payForSetupFee;


         //pro services one fee

        Product2 pProfServicesOneFee = DataFactory.newProduct('Professional Services', false, 'BRL');
        pProfServicesOneFee.Name = 'Professional Services Setup Fee';
        insert pProfServicesOneFee;
        
        PricebookEntry profServicesOneFeeEntry = DataFactory.newPricebookEntry(defaultPricebook, pProfServicesOneFee, opp);
        insert profServicesOneFeeEntry;

        QuoteLineItem proServicesOneFee = DataFactory.newQuoteLineItem(oppQuote, profServicesOneFeeEntry);
        insert proServicesOneFee;
         
        Payment__c payForProOneFee = DataFactory.newPayment(proServicesOneFee);
        insert payForProOneFee;

        Form__c m0 = new  Form__c();
        m0.Opportunity__c = opp.Id;
        m0.Client_Success_Executive__c = [ SELECT Id FROM User WHERE Profile.Name = 'Relationship' AND Email != null and IsActive = true LIMIT 1].Id;
        m0.Client_Success_Approval__c = true;
        m0.Approval_Status__c = 'Approved By Regional CEO';
        m0.Form_Status__c = 'Closed';
        m0.RecordTypeId = Schema.SObjectType.Form__c.getRecordTypeInfosByDeveloperName().get('M0_12_Steps').getRecordTypeId();
        User us = [select id from user where alias = 'sadmin' and isactive = true limit 1];
        m0.OwnerId = us.id;
        insert m0;

    }


    @isTest
    public static void testCreatedData01(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new PipefyIntegrationMock());
        
        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1]; 
        PipefyIntegrationController.auraHandler(opp.Id, 'Opportunity');
       
        Test.stopTest();
    }

    @isTest
    public static void testCreatedData02(){
        String recordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Success_Renegotiation').getRecordTypeId();
        
        Opportunity opp = [SELECT Id, RecordTypeId FROM Opportunity LIMIT 1]; 
        opp.RecordTypeId = recordType;
        opp.FastTrackStage__c = 'Lost';
        opp.Sub_Type__c = 'Retention';
        update opp;
        Case cs = new Case();
        cs.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Contract_Cancellation').getRecordTypeId();
        cs.OpportunityId__c = opp.Id;
        cs.Status = 'Confirmed';
        cs.I_have_deactivated_the_current_contract__c = true;
        insert cs;
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new PipefyIntegrationMock());
        
        
        PipefyIntegrationController.auraHandler(cs.Id, 'Case');
       
        Test.stopTest();
    }

    @isTest
    public static void testCreatedData03(){
        String recordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        
        Opportunity opp = [SELECT Id, RecordTypeId FROM Opportunity LIMIT 1]; 
        opp.RecordTypeId = recordType;
        update opp;
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new PipefyIntegrationMock());
        
        
        PipefyIntegrationController.auraHandler(opp.Id, 'Opportunity');
       
        Test.stopTest();
    }

    @isTest 
    public static void testAlone(){

        PipefyIntegrationService.changesToContractModel model = new PipefyIntegrationService.changesToContractModel();
        String newFamilyMemberCompany = model.newFamilyMemberCompany;
        String addmoreCountriesInDeals = model.addmoreCountriesInDeals;
        String changeOnLegalNameOrTaxId = model.changeOnLegalNameOrTaxId;
        String addLegalNameOrTaxId = model.addLegalNameOrTaxId;
        String companyPlansChangeToEmployee = model.companyPlansChangeToEmployee;
        String companyPlansChangeToFamilyMembers = model.companyPlansChangeToFamilyMembers;
        String accessFeeValueChange = model.accessFeeValueChange;
        String changeCutOffDateMembershipFee = model.changeCutOffDateMembershipFee;
        String changePaymentDueDateMembershipFee = model.changePaymentDueDateMembershipFee;
        String changePaymentDueDateAccessFee = model.changePaymentDueDateAccessFee;
        String changePaymentMethod = model.changePaymentMethod;
        String changeOperationalModel = model.changeOperationalModel;
        String changePermissionToContact = model.changePermissionToContact;
        String changeDeductionTerm = model.changeDeductionTerm;
        String changeSelfRegistrationType = model.changeSelfRegistrationType;
        String enableCustomPlans = model.enableCustomPlans;
        String extensionContractsEndDate = model.extensionContractsEndDate;
        String waiver = model.waiver;
        String configurationFee = model.configurationFee;
        String otherChanges = model.otherChanges;
        String changeFreePlan = model.changeFreePlan;

    }


}