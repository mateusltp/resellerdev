/**
 * Created by bruno on 31/01/2022.
 */

/**
 * @description       :
 * @author            : bruno.mendes@gympass.com
 * @group             :
 * @last modified on  : 01-31-2022
 * @last modified by  : bruno.mendes@gympass.com
**/
public with sharing class ThresholdSelector extends ApplicationSelector {

    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
                Threshold__c.Id
        };
    }

    public Schema.SObjectType getSObjectType() {
        return Threshold__c.sObjectType;
    }

    public List<Threshold__c> selectById(Set<Id> ids) {
        return (List<Threshold__c>) super.selectSObjectsById(ids);
    }
}