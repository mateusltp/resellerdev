public with sharing class ProdOppAssignmentService {
    public void insertCurrencyProdOppAssignment(List<Product_Opportunity_Assignment__c> prodOppAssignmentLst) {
        try{
            String recordTypeDevName = Schema.SObjectType.Product_Opportunity_Assignment__c.getRecordTypeInfosById().get(prodOppAssignmentLst.get(0).recordTypeId).getDeveloperName();
            service(recordTypeDevName).insertCurrencyProdOppAssignment(prodOppAssignmentLst);
        }catch (Exception e){
            system.debug('## msg no metodo: '+e.getMessage());
             throw new ProdOppAssignmentServiceException(e.getMessage());
        }  
    }

    public void updateCurrencyRelatedPartnerData(Map<Id, Product_Opportunity_Assignment__c> prodOppAssignmentMap) {
        try{
            String recordTypeDevName = Schema.SObjectType.Product_Opportunity_Assignment__c.getRecordTypeInfosById().get(prodOppAssignmentMap.values().get(0).recordTypeId).getDeveloperName();
            service(recordTypeDevName).updateCurrencyRelatedPartnerData(prodOppAssignmentMap);
        }catch (Exception e){
             throw new ProdOppAssignmentServiceException(e.getMessage());
        }
    }

    public void updateCurrencyRelatedPartnerData(Map<Id, Product_Opportunity_Assignment__c> prodOppAssignmentNewMap, Map<Id, Product_Opportunity_Assignment__c> prodOppAssignmentOldMap) {
        try{
            String recordTypeDevName = Schema.SObjectType.Product_Opportunity_Assignment__c.getRecordTypeInfosById().get(prodOppAssignmentNewMap.values().get(0).recordTypeId).getDeveloperName();
            service(recordTypeDevName).updateCurrencyRelatedPartnerData(prodOppAssignmentNewMap, prodOppAssignmentOldMap);
        }catch (Exception e){
             throw new ProdOppAssignmentServiceException(e.getMessage());
        }
    }

    private static IProdOppAssignmentService service(String recordTypeDevName) {
        return (IProdOppAssignmentService) Application.ServiceByRecordType.newInstanceByRecordType(getRecordTypeAndSObjectName(recordTypeDevName));
    }

    private static String getRecordTypeAndSObjectName(String recordTypeDevName) {
        return 'Product_Opportunity_Assignment__c.' + recordTypeDevName;
    }

    public class ProdOppAssignmentServiceException extends Exception {
        
    }
}