/**
 * @description       : 
 * @author            : roei@gft.com
 * @group             : 
 * @last modified on  : 02-07-2022
 * @last modified by  : roei@gft.com
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.1   02-07-2021   Tiago Ribeiro  Added tests to DealDeskOperationalApprovalItems
 * 1.0   12-11-2020   roei@gft.com   Initial Version
**/
@isTest(seeAllData=false)
public class DealDeskOperationalAutoApproveCaseTest {
    
	@TestSetup
    static void createData(){
        Pricebook2 lStandardPricebook = DataFactory.newPricebook();
        Database.update( lStandardPricebook );
        
        Account lAcc = DataFactory.newAccount();              
        Database.insert( lAcc );
                     
        Product2 lAccessFee = DataFactory.newProduct('Enterprise Subscription', false, 'BRL');
        Database.insert( lAccessFee );
        
        Product2 lSetupFee = DataFactory.newProduct('Setup Fee', false, 'BRL');
        Database.insert( lSetupFee );

        Product2 lPSMaintenanceFee = DataFactory.newProduct('Professional Services Maintenance Fee', false, 'BRL');
        Database.insert( lPSMaintenanceFee );
        
        Product2 lPSSetupFee = DataFactory.newProduct('Professional Services Setup Fee', false, 'BRL');
        Database.insert( lPSSetupFee );
        
        Opportunity lNewBusinessOpp = DataFactory.newOpportunity( lAcc.Id, lStandardPricebook, 'Client_Sales_New_Business' );     
        Database.insert( lNewBusinessOpp );
        
        Quote lQuote = DataFactory.newQuote(lNewBusinessOpp, '1', 'Gympass_Plus');
        Database.insert(lQuote);
        
        lNewBusinessOpp.SyncedQuoteId = lQuote.Id;
        lNewBusinessOpp.StageName = 'Proposta Aprovada';
        lNewBusinessOpp.FastTrackStage__c = 'Offer Approved';
        Database.update( lNewBusinessOpp );
        
        PricebookEntry accessFeeEntry = DataFactory.newPricebookEntry(lStandardPricebook, lAccessFee, lNewBusinessOpp);
        Database.insert(accessFeeEntry);
        
        QuoteLineItem accessFee = DataFactory.newQuoteLineItem(lQuote, accessFeeEntry);
        Database.insert(accessFee);        
        
        Payment__c payForAccessFee = DataFactory.newPayment(accessFee);
        Database.insert(payForAccessFee);
        
        PricebookEntry setupFeeEntry = DataFactory.newPricebookEntry(lStandardPricebook, lSetupFee, lNewBusinessOpp);
        Database.insert(setupFeeEntry);
                
        QuoteLineItem setupFee = DataFactory.newQuoteLineItem(lQuote, setupFeeEntry);
        Database.insert(setupFee); 
        
        Payment__c payForSetupFee = DataFactory.newPayment(setupFee);
        Database.insert(payForSetupFee);

        PricebookEntry lPSMaintenanceFeeEntry = DataFactory.newPricebookEntry(lStandardPricebook, lPSMaintenanceFee, lNewBusinessOpp);
        Database.insert(lPSMaintenanceFeeEntry);
                
        QuoteLineItem PSMaintenanceFeeQL = DataFactory.newQuoteLineItem(lQuote, lPSMaintenanceFeeEntry);
        Database.insert(PSMaintenanceFeeQL); 
        
        Payment__c payForPSMaintenanceFeeQL = DataFactory.newPayment(PSMaintenanceFeeQL);
        Database.insert(payForPSMaintenanceFeeQL);

        PricebookEntry lPSsetupFeeEntry = DataFactory.newPricebookEntry(lStandardPricebook, lPSSetupFee, lNewBusinessOpp);
        Database.insert(lPSsetupFeeEntry);
                
        QuoteLineItem lPSsetupFeeQL = DataFactory.newQuoteLineItem(lQuote, lPSsetupFeeEntry);
        Database.insert(lPSsetupFeeQL); 
        
        Payment__c payForlPSsetupQL = DataFactory.newPayment(lPSsetupFeeQL);
        Database.insert(payForlPSsetupQL);
    }
    
    @isTest
    public static void shouldAutoApproveOperationalCase() {
        Test.startTest();
            Opportunity lOpp = [ SELECT Id, CurrencyIsoCode FROM Opportunity WHERE Name = 'Renegotiation' ];
            Quote qt = [SELECT Id FROM Quote Limit 1];            
            Set<Id> setQtId = new Set<Id>{qt.Id};            
            PaymentRepository paymentRepository = new PaymentRepository();
            Payment__c payToUpdt = new Payment__c();

                
            Case lOperationalCase = DataFactory.newCase(lOpp, qt, 'Deal_Desk_Operational');
			lOperationalCase.Justificate_No_Compliant_Topics__c = 'Justification Test';
        	lOperationalCase.Subject = 'Deal Desk Operational Approval Request';
            lOperationalCase.Description = '';

            Map<Id, List<Payment__c>> mpPayment = paymentRepository.getPaymentsForQuotes(setQtId);

            for(Payment__c pay : mpPayment.get(qt.Id)){
                if(pay.Payment_Code__c.contains('Enterprise_Subscription')){
                    pay.frequency__c = 'Yearly';
                    payToUpdt = pay;
                    break;
                }
            }
            Database.update(payToUpdt);
        
        	Database.insert( lOperationalCase );        
            
            lOpp = [ SELECT Id, Specialist_Approved__c FROM Opportunity WHERE Id =: lOpp.Id ];
            lOperationalCase = [ SELECT Id, Auto_Approved_Case__c FROM Case WHERE Id =: lOperationalCase.Id ];
            
            System.assertEquals([SELECT Id FROM Approval_Item__c WHERE Case__c = :lOperationalCase.Id].size(), 0, 'Case not auto approved');
            System.assertEquals( true , lOpp.Specialist_Approved__c );
            System.assertEquals( true , lOperationalCase.Auto_Approved_Case__c );            
        Test.stopTest();
    }
    
    @isTest
    public static void shouldNotAutoApproveOperationalCaseWithAssertData(){
        Test.startTest();
            Opportunity lOpp = [ SELECT Id, CurrencyIsoCode, Specialist_Approved__c FROM Opportunity WHERE Name = 'Renegotiation' ];
            Quote qt = [SELECT Id FROM Quote Limit 1];
            Set<Id> setQtId = new Set<Id>{qt.Id};            
            PaymentRepository paymentRepository = new PaymentRepository();
            Payment__c payToUpdt = new Payment__c();
                
            Case lOperationalCase = DataFactory.newCase(lOpp, qt, 'Deal_Desk_Operational');
			lOperationalCase.Eligible_List_Registration_Method__c = 'HR Portal';
            lOperationalCase.Eligible_File_Management__c = 'By the company';
            lOperationalCase.Eligible_File_Update_Frequency__c = 'Once a month';
            lOperationalCase.Deadline_for_sending_Eligible_File__c = '5 business days before the cut date';
            lOperationalCase.ES_Billing_Day__c = '01';
            lOperationalCase.ES_Payment_Due_Days__c = '10 days';
            lOperationalCase.Setup_Fee_Billing_Day__c = '01';
            lOperationalCase.Setup_Fee_Payment_Due_Days__c = '10 days';
            lOperationalCase.Prof_Services_One_Fee_Billing_Day__c = '01';
            lOperationalCase.Prof_Services_One_Fee_Payment_Due_Days__c = '10 days';
            lOperationalCase.MF_Eligibility_ES_Billing_Day__c = '01';
            lOperationalCase.MF_Eligibility_ES_Payment_Due_Days__c = '10 days';

            Map<Id, List<Payment__c>> mpPayment = paymentRepository.getPaymentsForQuotes(setQtId);

            for(Payment__c pay : mpPayment.get(qt.Id)){
                if(pay.Payment_Code__c.contains('Enterprise_Subscription')){
                    pay.frequency__c = 'Yearly';
                    payToUpdt = pay;
                    break;
                }
            }
            Database.update(payToUpdt);
        
        	Database.insert( lOperationalCase );

            lOpp = [ SELECT Id, Specialist_Approved__c FROM Opportunity WHERE Id =: lOpp.Id ];
            lOperationalCase = [ SELECT Id, Auto_Approved_Case__c FROM Case WHERE Id =: lOperationalCase.Id ];

            System.assertEquals( true , lOpp.Specialist_Approved__c );
            System.assertEquals( true , lOperationalCase.Auto_Approved_Case__c );

            Case newCase = DataFactory.newCase(lOpp, qt, 'Deal_Desk_Operational');
            newCase.Eligible_List_Registration_Method__c = 'HR Portal';
            newCase.Eligible_File_Management__c = 'By the company';
            newCase.Eligible_File_Update_Frequency__c = 'Once a month';
            newCase.Deadline_for_sending_Eligible_File__c = '5 business days before the cut date';
            newCase.ES_Billing_Day__c = 'Custom';
            newCase.Custom_ES_Billing_Day__c = 13;
            newCase.ES_Payment_Due_Days__c = '10 days';
            newCase.Setup_Fee_Billing_Day__c = '01';
            newCase.Setup_Fee_Payment_Due_Days__c = '10 days';
            newCase.Prof_Services_One_Fee_Billing_Day__c = '01';
            newCase.Prof_Services_One_Fee_Payment_Due_Days__c = '10 days';
            newCase.MF_Eligibility_ES_Billing_Day__c = '01';
            newCase.MF_Eligibility_ES_Payment_Due_Days__c = '10 days';
            newCase.OwnerId = '00G1L000005y8CuUAI';

            Database.insert( newCase );

            lOpp = [ SELECT Id, Specialist_Approved__c FROM Opportunity WHERE Id =: lOpp.Id ];
            newCase = [ SELECT Id, Auto_Approved_Case__c FROM Case WHERE Id =: newCase.Id ];
            
            System.assertEquals( false ,  newCase.Auto_Approved_Case__c );

        Test.stopTest();
    }

    @isTest
    public static void shouldNotAutoApproveOperationalCaseFieldsCustom(){
        Test.startTest();
            Opportunity lOpp = [ SELECT Id, CurrencyIsoCode, Specialist_Approved__c FROM Opportunity WHERE Name = 'Renegotiation' ];
            Quote qt = [SELECT Id FROM Quote Limit 1];            

            Case newCase = DataFactory.newCase(lOpp, qt, 'Deal_Desk_Operational');
			newCase.Description = 'isTest2';
            newCase.Eligible_List_Registration_Method__c = 'HR Portal';
            newCase.Eligible_File_Management__c = 'By the company';
            newCase.Eligible_File_Update_Frequency__c = 'Once a month';
            newCase.Deadline_for_sending_Eligible_File__c = '5 business days before the cut date';
            newCase.ES_Billing_Day__c = 'Custom';
            newCase.Custom_ES_Billing_Day__c = 13;
            newCase.ES_Payment_Due_Days__c = '10 days';
            newCase.Setup_Fee_Billing_Day__c = '01';
            newCase.Setup_Fee_Payment_Due_Days__c = '10 days';
            newCase.Prof_Services_One_Fee_Billing_Day__c = '01';
            newCase.Prof_Services_One_Fee_Payment_Due_Days__c = '10 days';
            newCase.MF_Eligibility_ES_Billing_Day__c = '01';
            newCase.MF_Eligibility_ES_Payment_Due_Days__c = '10 days';
            newCase.OwnerId = '00G1L000005y8CuUAI';

            Database.insert( newCase );

            lOpp = [ SELECT Id, Specialist_Approved__c FROM Opportunity WHERE Id =: lOpp.Id ];
            newCase = [ SELECT Id, Auto_Approved_Case__c FROM Case WHERE Id =: newCase.Id ];
            List<Assert_Data__c> assertData = [SELECT Id FROM Assert_Data__c WHERE Opportunity__c =: lOpp.Id AND Object_Name__c = 'case'];
            
            System.assertEquals( false ,  newCase.Auto_Approved_Case__c );
            System.assert(assertData.size() == 0);
        Test.stopTest();
    }

    @isTest
    public static void dealDeskOperationalCaseApprovedByDealDesk(){
        Test.startTest();
            Opportunity lOpp = [ SELECT Id, CurrencyIsoCode, Specialist_Approved__c FROM Opportunity WHERE Name = 'Renegotiation' ];
            Quote qt = [SELECT Id FROM Quote Limit 1];            

            Case newCase = DataFactory.newCase(lOpp, qt, 'Deal_Desk_Operational');
			newCase.Description = 'isTest2';
            newCase.Eligible_List_Registration_Method__c = 'HR Portal';
            newCase.Eligible_File_Management__c = 'By the company';
            newCase.Eligible_File_Update_Frequency__c = 'Once a month';
            newCase.Deadline_for_sending_Eligible_File__c = '5 business days before the cut date';
            newCase.ES_Billing_Day__c = 'Custom';
            newCase.Custom_ES_Billing_Day__c = 13;
            newCase.ES_Payment_Due_Days__c = '10 days';
            newCase.Setup_Fee_Billing_Day__c = '01';
            newCase.Setup_Fee_Payment_Due_Days__c = '10 days';
            newCase.Prof_Services_One_Fee_Billing_Day__c = '01';
            newCase.Prof_Services_One_Fee_Payment_Due_Days__c = '10 days';
            newCase.MF_Eligibility_ES_Billing_Day__c = '01';
            newCase.MF_Eligibility_ES_Payment_Due_Days__c = '10 days';
            newCase.OwnerId = '00G1L000005y8CuUAI';
            newCase.Deal_Desk_Evaluation__c = 'ok';

            Database.insert( newCase );

            FastTrackProposalCreationController.approveOrRejectDealDesk(lOpp.Id, newCase.Id, true, 'TestReason');

            lOpp = [ SELECT Id, Specialist_Approved__c FROM Opportunity WHERE Id =: lOpp.Id ];
            newCase = [ SELECT Id, Auto_Approved_Case__c FROM Case WHERE Id =: newCase.Id ];
            List<Assert_Data__c> assertData = [SELECT Id FROM Assert_Data__c WHERE Opportunity__c =: lOpp.Id AND Object_Name__c = 'case'];
            
            System.assertEquals( true , lOpp.Specialist_Approved__c );
            System.assertEquals( false ,  newCase.Auto_Approved_Case__c );
            System.assert(assertData.size() > 0);
        Test.stopTest();
    }

    @isTest
    public static void shouldNotAutoApproveOperationalCaseFrequencyChanged(){
        Test.startTest();
            Opportunity lOpp = [ SELECT Id, CurrencyIsoCode, Specialist_Approved__c FROM Opportunity WHERE Name = 'Renegotiation' ];
            Quote qt = [SELECT Id FROM Quote LIMIT 1];
            Set<Id> setQtId = new Set<Id>{qt.Id};            
            PaymentRepository paymentRepository = new PaymentRepository();
            Payment__c payToUpdt = new Payment__c();

            Map<Id, List<Payment__c>> mpPayment = paymentRepository.getPaymentsForQuotes(setQtId);

            Case lOperationalCase = DataFactory.newCase(lOpp, qt, 'Deal_Desk_Operational');
			lOperationalCase.Eligible_List_Registration_Method__c = 'HR Portal';
            lOperationalCase.Eligible_File_Management__c = 'By the company';
            lOperationalCase.Eligible_File_Update_Frequency__c = 'Once a month';
            lOperationalCase.Deadline_for_sending_Eligible_File__c = '5 business days before the cut date';
            lOperationalCase.ES_Billing_Day__c = '01';
            lOperationalCase.ES_Payment_Due_Days__c = '10 days';
            lOperationalCase.Setup_Fee_Billing_Day__c = '01';
            lOperationalCase.Setup_Fee_Payment_Due_Days__c = '10 days';
            lOperationalCase.Prof_Services_One_Fee_Billing_Day__c = '01';
            lOperationalCase.Prof_Services_One_Fee_Payment_Due_Days__c = '10 days';
            lOperationalCase.MF_Eligibility_ES_Billing_Day__c = '01';
            lOperationalCase.MF_Eligibility_ES_Payment_Due_Days__c = '10 days';
        
        	Database.insert( lOperationalCase );
            
            for(Payment__c pay : mpPayment.get(qt.Id)){
                if(pay.Payment_Code__c.contains('Enterprise_Subscription')){
                    pay.frequency__c = 'Yearly';
                    payToUpdt = pay;
                    break;
                }
            }            

            Database.update(payToUpdt);            

            lOpp = [ SELECT Id, CurrencyIsoCode, Specialist_Approved__c FROM Opportunity WHERE Name = 'Renegotiation' ];

            System.assertEquals( false , lOpp.Specialist_Approved__c );

            Quote qtAtt = [SELECT Id FROM Quote LIMIT 1];

            Case lOperationalCase2 = DataFactory.newCase(lOpp, qtAtt, 'Deal_Desk_Operational');
			lOperationalCase2.Eligible_List_Registration_Method__c = 'HR Portal';
            lOperationalCase2.Eligible_File_Management__c = 'By the company';
            lOperationalCase2.Eligible_File_Update_Frequency__c = 'Once a month';
            lOperationalCase2.Deadline_for_sending_Eligible_File__c = '5 business days before the cut date';
            lOperationalCase2.ES_Billing_Day__c = '01';
            lOperationalCase2.ES_Payment_Due_Days__c = 'Custom';
            lOperationalCase2.Custom_ES_Payment_Due_Days__c = 70;
            lOperationalCase2.Setup_Fee_Billing_Day__c = '01';
            lOperationalCase2.Setup_Fee_Payment_Due_Days__c = '10 days';
            lOperationalCase2.Prof_Services_One_Fee_Billing_Day__c = '01';
            lOperationalCase2.Prof_Services_One_Fee_Payment_Due_Days__c = '10 days';
            lOperationalCase2.MF_Eligibility_ES_Billing_Day__c = '01';
            lOperationalCase2.MF_Eligibility_ES_Payment_Due_Days__c = '10 days';            
            lOperationalCase2.OwnerId = '00G1L000005y8CuUAI';
        
        	Database.insert( lOperationalCase2 );            
            
            Case newCase = [ SELECT Id, Auto_Approved_Case__c FROM Case WHERE Id =: lOperationalCase2.Id ];
            List<Assert_Data__c> assertData = [SELECT Id FROM Assert_Data__c WHERE Opportunity__c =: lOpp.Id AND Object_Name__c = 'case'];            
            List<Approval_Item__c> listApprovalItems = [SELECT Id, Name, Value__c FROM Approval_Item__c WHERE Case__c = :newCase.Id];
            System.assertEquals( false ,  newCase.Auto_Approved_Case__c );
            System.assert(listApprovalItems.size() > 0);
            System.assert(assertData.size() > 0);
        Test.stopTest();
    }

    @isTest
    public static void shouldAutoApproveOperationalCaseFrequencyChanged(){
        Test.startTest();
            Opportunity lOpp = [ SELECT Id, CurrencyIsoCode, Specialist_Approved__c FROM Opportunity WHERE Name = 'Renegotiation' ];
            Quote qt = [SELECT Id FROM Quote LIMIT 1];
            Set<Id> setQtId = new Set<Id>{qt.Id};            
            PaymentRepository paymentRepository = new PaymentRepository();
            Payment__c payToUpdt = new Payment__c();

            Map<Id, List<Payment__c>> mpPayment = paymentRepository.getPaymentsForQuotes(setQtId);

            Case lOperationalCase = DataFactory.newCase(lOpp, qt, 'Deal_Desk_Operational');
			lOperationalCase.Eligible_List_Registration_Method__c = 'HR Portal';
            lOperationalCase.Eligible_File_Management__c = 'By the company';
            lOperationalCase.Eligible_File_Update_Frequency__c = 'Once a month';
            lOperationalCase.Deadline_for_sending_Eligible_File__c = '5 business days before the cut date';
            lOperationalCase.ES_Billing_Day__c = '01';
            lOperationalCase.ES_Payment_Due_Days__c = '10 days';
            lOperationalCase.Setup_Fee_Billing_Day__c = '01';
            lOperationalCase.Setup_Fee_Payment_Due_Days__c = '10 days';
            lOperationalCase.Prof_Services_One_Fee_Billing_Day__c = '01';
            lOperationalCase.Prof_Services_One_Fee_Payment_Due_Days__c = '10 days';
            lOperationalCase.MF_Eligibility_ES_Billing_Day__c = '01';
            lOperationalCase.MF_Eligibility_ES_Payment_Due_Days__c = '10 days';
        
        	Database.insert( lOperationalCase );
            
            for(Payment__c pay : mpPayment.get(qt.Id)){
                if(pay.Payment_Code__c.contains('Enterprise_Subscription')){
                    pay.frequency__c = 'Yearly';
                    payToUpdt = pay;
                    break;
                }
            }            

            Database.update(payToUpdt);            

            lOpp = [ SELECT Id, CurrencyIsoCode, Specialist_Approved__c FROM Opportunity WHERE Name = 'Renegotiation' ];

            System.assertEquals( false , lOpp.Specialist_Approved__c );

            Quote qtAtt = [SELECT Id FROM Quote LIMIT 1];

            Case lOperationalCase2 = DataFactory.newCase(lOpp, qtAtt, 'Deal_Desk_Operational');
			lOperationalCase2.Eligible_List_Registration_Method__c = 'HR Portal';
            lOperationalCase2.Eligible_File_Management__c = 'By the company';
            lOperationalCase2.Eligible_File_Update_Frequency__c = 'Once a month';
            lOperationalCase2.Deadline_for_sending_Eligible_File__c = '5 business days before the cut date';
            lOperationalCase2.ES_Billing_Day__c = '01';
            lOperationalCase2.ES_Payment_Due_Days__c = '10 days';            
            lOperationalCase2.Setup_Fee_Billing_Day__c = '01';
            lOperationalCase2.Setup_Fee_Payment_Due_Days__c = '10 days';
            lOperationalCase2.Prof_Services_One_Fee_Billing_Day__c = '01';
            lOperationalCase2.Prof_Services_One_Fee_Payment_Due_Days__c = '10 days';
            lOperationalCase2.MF_Eligibility_ES_Billing_Day__c = '01';
            lOperationalCase2.MF_Eligibility_ES_Payment_Due_Days__c = '10 days';            
            lOperationalCase2.OwnerId = '00G1L000005y8CuUAI';
        
        	Database.insert( lOperationalCase2 );            
            
            Case newCase = [ SELECT Id, Auto_Approved_Case__c FROM Case WHERE Id =: lOperationalCase2.Id ];
            List<Assert_Data__c> assertData = [SELECT Id FROM Assert_Data__c WHERE Opportunity__c =: lOpp.Id AND Object_Name__c = 'case'];            
            
            System.assertEquals( true ,  newCase.Auto_Approved_Case__c );
            System.assert(assertData.size() > 0);
        Test.stopTest();
    }

    @isTest
    public static void shouldNotAutoApproveOperationalCasePSSetupChanged(){
        Test.startTest();
            Opportunity lOpp = [ SELECT Id, CurrencyIsoCode, Specialist_Approved__c FROM Opportunity WHERE Name = 'Renegotiation' ];
            Quote qt = [SELECT Id FROM Quote LIMIT 1];
            Set<Id> setQtId = new Set<Id>{qt.Id};            
            PaymentRepository paymentRepository = new PaymentRepository();
            Payment__c payToUpdt = new Payment__c();

            Map<Id, List<Payment__c>> mpPayment = paymentRepository.getPaymentsForQuotes(setQtId);

            Case lOperationalCase = DataFactory.newCase(lOpp, qt, 'Deal_Desk_Operational');
            lOperationalCase.Description = '';
			lOperationalCase.Eligible_List_Registration_Method__c = 'HR Portal';
            lOperationalCase.Eligible_File_Management__c = 'By the company';
            lOperationalCase.Eligible_File_Update_Frequency__c = 'Once a month';
            lOperationalCase.Deadline_for_sending_Eligible_File__c = '5 business days before the cut date';
            lOperationalCase.ES_Billing_Day__c = '01';
            lOperationalCase.ES_Payment_Due_Days__c = '10 days';
            lOperationalCase.Setup_Fee_Billing_Day__c = '01';
            lOperationalCase.Setup_Fee_Payment_Due_Days__c = '10 days';
            lOperationalCase.Prof_Services_One_Fee_Billing_Day__c = '01';
            lOperationalCase.Prof_Services_One_Fee_Payment_Due_Days__c = '10 days';
            lOperationalCase.Prof_Services_Main_Fee_Payment_Due_Days__c = '10 days';
            lOperationalCase.MF_Eligibility_ES_Billing_Day__c = '01';
            lOperationalCase.MF_Eligibility_ES_Payment_Due_Days__c = '10 days';
        
        	Database.insert( lOperationalCase );

            System.assertEquals([SELECT Id FROM Approval_Item__c].size(), 0, 'Case not approved');
            
            for(Payment__c pay : mpPayment.get(qt.Id)){
                if(pay.Payment_Code__c.contains('Professional_Services_Maintenance_Fee')){
                    pay.Frequency__c = 'Yearly';
                    payToUpdt = pay;
                    break;
                }
            }            

            Database.update(payToUpdt);            

            lOpp = [ SELECT Id, CurrencyIsoCode, Specialist_Approved__c FROM Opportunity WHERE Name = 'Renegotiation' ];

            Quote qtAtt = [SELECT Id FROM Quote LIMIT 1];

            Case lOperationalCase2 = DataFactory.newCase(lOpp, qtAtt, 'Deal_Desk_Operational');
			lOperationalCase2.Eligible_List_Registration_Method__c = 'HR Portal';
            lOperationalCase2.Eligible_File_Management__c = 'By the company';
            lOperationalCase2.Eligible_File_Update_Frequency__c = 'Once a month';
            lOperationalCase2.Deadline_for_sending_Eligible_File__c = '5 business days before the cut date';
            lOperationalCase2.ES_Billing_Day__c = '01';
            lOperationalCase2.ES_Payment_Due_Days__c = 'Custom';
            lOperationalCase2.Custom_ES_Payment_Due_Days__c = 70;
            lOperationalCase2.Setup_Fee_Billing_Day__c = '01';
            lOperationalCase2.Setup_Fee_Payment_Due_Days__c = '10 days';
            lOperationalCase2.Prof_Services_One_Fee_Billing_Day__c = '01';
            lOperationalCase2.Prof_Services_One_Fee_Payment_Due_Days__c = 'Custom';
            lOperationalCase2.Custom_Prof_Services_One_Fee_Payment__c = 62;
            lOperationalCase.Custom_Prof_Services_Main_Fee_Payment__c = 61;
            lOperationalCase.Prof_Services_Main_Fee_Payment_Due_Days__c = 'Custom';
            lOperationalCase2.MF_Eligibility_ES_Billing_Day__c = '01';
            lOperationalCase2.MF_Eligibility_ES_Payment_Due_Days__c = '10 days';            
            lOperationalCase2.OwnerId = '00G1L000005y8CuUAI';
        
        	Database.insert( lOperationalCase2 );            
            
            Case newCase = [ SELECT Id, Auto_Approved_Case__c FROM Case WHERE Id =: lOperationalCase2.Id ];
            List<Assert_Data__c> assertData = [SELECT Id FROM Assert_Data__c WHERE Opportunity__c =: lOpp.Id AND Object_Name__c = 'case'];            
            List<Approval_Item__c> listApprovalItems = [SELECT Id, Name, Value__c FROM Approval_Item__c WHERE Case__c = :newCase.Id];
            System.debug(listApprovalItems);
            System.assertEquals( false ,  newCase.Auto_Approved_Case__c );
            System.assertEquals(listApprovalItems.size(), 2, 'Case Approved');
            System.assert(assertData.size() > 0);
        Test.stopTest();
    }
}