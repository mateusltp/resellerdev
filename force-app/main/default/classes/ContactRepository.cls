/**
* @author vinicius.ferraz
* @description Provide mockable repository data layer for Contact domain
*/
public with sharing class ContactRepository {

    public ContactRepository(){

    }

    /**
     * throwable System.QueryException
     */
    public virtual Contact byId(String recordId){
        return [select Id from Contact
                  where Id = :recordId];
    }

      /**
     * throwable System.DMLException
     */
    public virtual Contact add(Contact record){
        Database.insert(record);
        return record;
    }

    /**
     * throwable System.DMLException
     */
    public virtual Contact edit(Contact record){
        Database.update(record);
        return record;
    }

    /**
     * throwable System.DMLException
     */
    public virtual Contact addOrEdit(Contact record){
        Database.upsert(record, Contact.Id, true);
        return record;
    }

    public List<Contact> fromAccount(String accountId){
        return [select Id,Name,Primary_HR_Contact__c from Contact where AccountId = :accountId];
    }
}