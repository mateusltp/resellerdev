public without sharing class UpdateIntegrationInfoHelper {
    @invocableMethod
    public static void updateIntegrationInfo(List<Id> recordIdList) { 
        List<Integration_Request__c> integrationRequestList = [
       		SELECT Id, SObject_ID__c, SObject_type__c, Integration_Request_Status__c
            FROM Integration_Request__c
            WHERE Id IN :recordIdList
        ];
        
    	Map<String, List<Id>> objectTypeNameToIdList = new Map<String, List<Id>>();
        
        for (Integration_Request__c ir : integrationRequestList) {
            if (objectTypeNameToIdList.containsKey(ir.SObject_type__c)) {
           	    objectTypeNameToIdList.get(ir.SObject_type__c).add(ir.SObject_ID__c);
            } else {
                objectTypeNameToIdList.put(ir.SObject_type__c, new List<Id>{ir.SObject_ID__c});
            }
        }
        
        if (objectTypeNameToIdList.containsKey('Sales_Order__c')) {
       		List<Id> soList = objectTypeNameToIdList.get('Sales_Order__c');
            
            Map<Id, Sales_Order__c> idToSalesOrder = new Map<Id, Sales_Order__c>([
           		SELECT Id, Integration_Status__c, Integration_Request__c
                FROM Sales_Order__c
                WHERE Id IN :soList
            ]);
            
            List<Sales_Order__c> soListToUpdate = new List<Sales_Order__c>();
            
            for (Integration_Request__c ir : integrationRequestList) {
                Id soId = (Id)ir.SObject_ID__c;
				Sales_Order__c so = idToSalesOrder.get(soId);
                
                so.Integration_Request__c = ir.Id;
                
                if (ir.Integration_Request_Status__c == 'success') {
               		so.Integration_Status__c = 'Integrated';
                } else if (ir.Integration_Request_Status__c == 'error') {
                	so.Integration_Status__c = 'Error';
                }
                
                soListToUpdate.add(so);
            }
            
            update soListToUpdate;
        }
    }
}