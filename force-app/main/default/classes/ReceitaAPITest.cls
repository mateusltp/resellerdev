@IsTest
public class ReceitaAPITest {
	   
	static testMethod void testReceita() {
        
		String json = '{\"atividade_principal\":[{\"text\":\"Atividades de intermediação e agenciamento de serviços e negócios em geral, exceto imobiliários\",\"code\":\"74.90-1-04\"}],\"data_situacao\":\"29/05/2012\",\"complemento\":\"ANDAR 10\",\"tipo\":\"MATRIZ\",\"nome\":\"GPBR PARTICIPACOES LTDA.\",\"uf\":\"SP\",\"telefone\":\"(11) 3522-7499\",\"email\":\"recepsp@gympass.com\",\"atividades_secundarias\":[{\"text\":\"Holdings de instituições não-financeiras\",\"code\":\"64.62-0-00\"},{\"text\":\"Desenvolvimento e licenciamento de programas de computador não-customizáveis\",\"code\":\"62.03-1-00\"}],\"qsa\":[{\"qual\":\"37-Sócio Pessoa Jurídica Domiciliado no Exterior\",\"pais_origem\":\"PAÍSES BAIXOS (HOLANDA)\",\"nome_rep_legal\":\"PEDRO PAULO COSTA GOMES\",\"qual_rep_legal\":\"17-Procurador\",\"nome\":\"GPNL INTERNATIONAL HOLDING B.V.\"},{\"qual\":\"05-Administrador\",\"nome\":\"MARIA LEONOR GAYOTTO DE BORBA BACHIR\"},{\"qual\":\"05-Administrador\",\"nome\":\"LEANDRO FARAH CALDEIRA\"}],\"situacao\":\"ATIVA\",\"bairro\":\"CIDADE MONCOES\",\"logradouro\":\"AV ENGENHEIRO LUIZ CARLOS BERRINI 716\",\"numero\":\"716\",\"cep\":\"04.571-926\",\"municipio\":\"SAO PAULO\",\"porte\":\"DEMAIS\",\"abertura\":\"29/05/2012\",\"natureza_juridica\":\"206-2 - Sociedade Empresária Limitada\",\"fantasia\":\"GYMPASS\",\"cnpj\":\"15.664.649/0001-84\",\"ultima_atualizacao\":\"2021-07-27T19:38:56.438Z\",\"status\":\"OK\",\"efr\":\"\",\"motivo_situacao\":\"\",\"situacao_especial\":\"\",\"data_situacao_especial\":\"\",\"capital_social\":\"49201521.00\",\"extra\":{},\"billing\":{\"free\":true,\"database\":true}}';
		String CNPJ = '56947401000108';
        
        TOReceitaAPI obj = TOReceitaAPI.parse(json);
        
		System.assert(obj != null);

        Test.startTest();
      
            Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
    
            ReceitaAPI.callReceitaAPI(CNPJ);
            
     
            Test.setMock(HttpCalloutMock.class, new UnauthorizedEndpointResponse());
    
            ReceitaAPI.callReceitaAPI(CNPJ);
        
        Test.stopTest(); 
	}

    public class MockHttpResponseGenerator implements HttpCalloutMock { 
            
        public HTTPResponse respond(HTTPRequest req) {
            
            String endpoint = 'https://www.receitaws.com.br/v1/cnpj/'; 
    
            String json = '{\"atividade_principal\":[{\"text\":\"Atividades de intermediação e agenciamento de serviços e negócios em geral, exceto imobiliários\",\"code\":\"74.90-1-04\"}],\"data_situacao\":\"29/05/2012\",\"complemento\":\"ANDAR 10\",\"tipo\":\"MATRIZ\",\"nome\":\"GPBR PARTICIPACOES LTDA.\",\"uf\":\"SP\",\"telefone\":\"(11) 3522-7499\",\"email\":\"recepsp@gympass.com\",\"atividades_secundarias\":[{\"text\":\"Holdings de instituições não-financeiras\",\"code\":\"64.62-0-00\"},{\"text\":\"Desenvolvimento e licenciamento de programas de computador não-customizáveis\",\"code\":\"62.03-1-00\"}],\"qsa\":[{\"qual\":\"37-Sócio Pessoa Jurídica Domiciliado no Exterior\",\"pais_origem\":\"PAÍSES BAIXOS (HOLANDA)\",\"nome_rep_legal\":\"PEDRO PAULO COSTA GOMES\",\"qual_rep_legal\":\"17-Procurador\",\"nome\":\"GPNL INTERNATIONAL HOLDING B.V.\"},{\"qual\":\"05-Administrador\",\"nome\":\"MARIA LEONOR GAYOTTO DE BORBA BACHIR\"},{\"qual\":\"05-Administrador\",\"nome\":\"LEANDRO FARAH CALDEIRA\"}],\"situacao\":\"ATIVA\",\"bairro\":\"CIDADE MONCOES\",\"logradouro\":\"AV ENGENHEIRO LUIZ CARLOS BERRINI 716\",\"numero\":\"716\",\"cep\":\"04.571-926\",\"municipio\":\"SAO PAULO\",\"porte\":\"DEMAIS\",\"abertura\":\"29/05/2012\",\"natureza_juridica\":\"206-2 - Sociedade Empresária Limitada\",\"fantasia\":\"GYMPASS\",\"cnpj\":\"15.664.649/0001-84\",\"ultima_atualizacao\":\"2021-07-27T19:38:56.438Z\",\"status\":\"OK\",\"efr\":\"\",\"motivo_situacao\":\"\",\"situacao_especial\":\"\",\"data_situacao_especial\":\"\",\"capital_social\":\"49201521.00\",\"extra\":{},\"billing\":{\"free\":true,\"database\":true}}';
 
            HttpResponse response = new HttpResponse(); 
       
            response.setBody(json);
            response.setStatusCode(200); 
            response.setStatusCode(429);
    
            return response; 
        } 
    }
    
    public class UnauthorizedEndpointResponse implements HttpCalloutMock { 
        
        public HTTPResponse respond(HTTPRequest req) {
            
            CalloutException e = (CalloutException)CalloutException.class.newInstance();
            e.setMessage('Unauthorized endpoint');
            throw e;
        }     
	}   
}