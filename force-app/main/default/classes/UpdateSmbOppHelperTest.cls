@isTest
public class UpdateSmbOppHelperTest {
	@testSetup
    public static void createData() {
        Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
        Id contactRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Direct_Channel').getRecordTypeId();
        Id oppRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SMB').getRecordTypeId();
        
        Account acc = new Account();
        acc.Name = 'Test Account';
        acc.RecordTypeId = accRecordTypeId;
        acc.BillingCountry = 'Brazil';
        acc.BillingState = 'São Paulo';
        insert acc;
        
        Contact c = new Contact();
        c.LastName = 'Test';
        c.FirstName = 'Contact';
        c.Email = 'testcontact@contact.com';
        c.AccountId = acc.Id;
        c.Business_Model_Indirect__c = 'Intermediation';
        c.Cofee_Comission_Rate__c = 20.99;
        insert c;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Test Opp';
        opp.AccountId = acc.Id;
        opp.StageName = 'Qualificação';
        opp.CloseDate = System.today();
        opp.RecordTypeId = oppRecordTypeId;
        insert opp;
    }
    
    @isTest
    public static void testUpdateSmbOpp() {
        Contact c = [
            SELECT Id, AccountId, Business_Model_Indirect__c, Cofee_Comission_Rate__c
            FROM Contact LIMIT 1
        ];
        
        UpdateSmbOppHelper.updateStepsTowardsSuccess(new List<Id>{c.Id});
    }
}