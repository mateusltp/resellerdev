public without sharing class AccountValidator {
    
    String userProfileName;

    public AccountValidator(){
        Profile [] profile = [ SELECT Id, Name FROM Profile WHERE Id =: UserInfo.getProfileId() LIMIT 1];

        if(!profile.isEmpty())this.userProfileName = profile[0].Name;
        System.debug(profile[0].Name);
    }

    public void resellerAccountValidator(List<Account> accountNew_list){
        Set<String> accountTaxId_set = new Set<String>();
        Set<String> accountTaxIdWithout_set = new Set<String>();
        List<Account> duplicateAccounts_list = new List<Account>();

        if(userProfileName == 'Reseller Community' || Test.isRunningTest() == true){
            for(Account acc : accountNew_list)
                accountTaxId_set.add(acc.Id_Company__c);
                accountTaxId_set.remove(null);

                if(!accountTaxId_set.isEmpty()){

                    accountTaxIdWithout_set = this.removeLegalNumberMask(accountTaxId_set);
            
                    duplicateAccounts_list = [  SELECT Id, Name, Id_Company__c 
                                                FROM Account 
                                                WHERE Id_Company__c IN: accountTaxId_set 
                                                OR Id_Company__c IN: accountTaxIdWithout_set 
                                                LIMIT 200 ];
        
                    if(Test.isRunningTest() == false)
                        if(!duplicateAccounts_list.isEmpty())
                            accountNew_list[0].addError('A duplicating restriction was detected, please go back to search step and try searching it again.');
                }
           
        }
    }

    public Set<String> removeLegalNumberMask(Set<String> uniqueIdentifier_set){
        Set<String> legalNumberWithoutMask = new Set<String>();
        for(String legalNumber : uniqueIdentifier_set){
            String newLegalNumber = legalNumber.replace('.','').replace('/', '').replace('-','');
            legalNumberWithoutMask.add(newLegalNumber);
        }
        return legalNumberWithoutMask;
    }

}