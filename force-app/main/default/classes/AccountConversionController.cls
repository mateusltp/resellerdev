/**
 * @File Name          : AccountConversionController.cls
 * @Description        : 
 * @Author             : Samuel Silva - GFT (slml@gft.com)
 * @Group              : 
 * @Last Modified By   : Samuel Silva - GFT (slml@gft.com)
 * @Last Modified On   : 12/03/2020 14:14:12
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    11/03/2020   Samuel Silva - GFT (slml@gft.com)     Initial Version
**/

public with sharing class AccountConversionController {
  
    @AuraEnabled
    public static SObject getAccountObject(String accountId){     
        return AccountConversionModel.getAccountObject(accountId);       
        
    }

    @AuraEnabled
    public static SObject convertAccount(SObject currentAccount){
        return AccountConversionModel.convertAccount(currentAccount);   
     
    }
    
}