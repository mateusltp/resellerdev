/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 09-21-2021
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/
public with sharing class UpsertOrderPartnerOnTagusCommand extends AbstractUpdateableOutboundCommand {
    private Request request;
    private Object response;
  
    override public Object transformToSend() {   
        System.debug('Entrou no transformToSend em UpsertOrderPartnerOnTagusCommand');
        TagusPartnerTransaction.PartnerTransactionType transactionType = event.getEventName()
            .contains('CREATE')
          ? TagusPartnerTransaction.PartnerTransactionType.CREATED
          : TagusPartnerTransaction.PartnerTransactionType.UPDATED;
    

      List<OrderPartnerTagusDTO> orderRequests = (List<OrderPartnerTagusDTO>) event.getPayloadFromJson(
        List<OrderPartnerTagusDTO>.class
      );
  
      this.request = new Request();
      this.request.transaction_request = new RequestData(new TagusPartnerTransaction(event, transactionType), orderRequests);
      
      return request;
    }


    
  override public void processResult(Object response) {
    this.response = response;
  }

  
    override public void postUpdateExecute() {
        System.debug('Entrou no postUpdateExecute em UpsertOrderPartnerOnTagusCommand');
        fflib_ISObjectUnitOfWork uow = Application.UnitOfWork.newInstance();
        RequestData requestData = (RequestData) this.request.transaction_request;
        List<Quote> proposals = new List<Quote>(); // partner_Order
        List<Opportunity> opportunities = new List<Opportunity>(); //partner_order_details
        List<Order> orders = new List<Order>(); //partner_order_details
        List<Product_Item__c> productItems = new List<Product_Item__c>(); //partner_order_details
        
    
        for(OrderPartnerTagusDTO orderPartnerDTO : (List<OrderPartnerTagusDTO>)requestData.data.getOrders()) {

          for(OrderPartnerTagusDTO.Partner_order_items iPartnerOrderItem : orderPartnerDTO.getPartnerOrderItem()){
            proposals.add(new Quote(     Id =  orderPartnerDTO.getSalesforceTransactionId(),  UUID__c = orderPartnerDTO.getUuId(), Order_Item_UUID__c = iPartnerOrderItem.getUuId()   ));          
            for(OrderPartnerTagusDTO.Partner_products iPartnerOrderProduct : iPartnerOrderItem.getPartnerProducts()){
              productItems.add(new Product_Item__c(     Id =  iPartnerOrderProduct.getSalesforceId() ,  UUID__c = iPartnerOrderProduct.getUuId()   ));
            }
          }         
          OrderPartnerTagusDTO.Partner_order_detail iPartnerOrderDetail = orderPartnerDTO.getPartnerOrderDetail();
          opportunities.add(new Opportunity(     Id = iPartnerOrderDetail.getSalesforceId(),  UUID__c = iPartnerOrderDetail.getUuId()   ));
       
        }

        uow.registerDirty(proposals);
        uow.registerDirty(opportunities);
        uow.registerDirty(productItems);
        uow.commitWork();
    }
  
    private class Request {
      private RequestData transaction_request;
    }
  
    private class RequestData {
      
      private TagusPartnerTransaction info;      
      private OrderData data;
      
      public RequestData(TagusPartnerTransaction info, List<OrderPartnerTagusDTO> orders){
        setInfo(info);
        this.data = new OrderData(orders);
      }

      private void setInfo(TagusPartnerTransaction info){
        this.info = info;
      }
      
    }

    private class OrderData {
      
      private List<OrderPartnerTagusDTO> partner_orders; 

      public OrderData(List<OrderPartnerTagusDTO> orders){
        this.partner_orders = orders;
      }

      public List<OrderPartnerTagusDTO> getOrders(){
        return this.partner_orders;
      }
    }
}