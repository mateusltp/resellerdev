public with sharing class ResellerMultipleDetailController {
    
    public List<Account_Request__c> accountRequestList{get;set;}
    public List<Massive_Account_Request__c> massiveAccountRequest{get;set;}
    public List<User> user{get;set;}
    public String userReseller {get; set;}
    public String reseller {get; set;}
    public String dateMassiveRequest {get; set;}
    public Id massiveId {get;set;}
    public String userContryCode {get;set;}
    

    public ResellerMultipleDetailController(Apexpages.StandardController controller) {
        massiveId = ApexPages.CurrentPage().getparameters().get('id');

        massiveAccountRequest = [SELECT createdby.name, Reseller__r.Name, CreatedDate 
                                FROM Massive_Account_Request__c 
                                WHERE Id =: massiveId];
        this.userReseller = massiveAccountRequest[0].createdby.name;
        this.reseller = massiveAccountRequest[0].Reseller__r.Name;
        this.dateMassiveRequest = massiveAccountRequest[0].CreatedDate.format('dd/MM/YYYY');
        

        accountRequestList = [SELECT Name, Website__c, Unique_Identifier_Type__c, Unique_Identifier__c, Engine_Status__c, Error_message__c 
                            FROM Account_Request__c 
                            WHERE Massive_Account_Request__c =: massiveId
                            ORDER BY Engine_Status__c ASC];
                 
        user = [SELECT Id, CountryCode FROM User WHERE Id = : Userinfo.getUserId() LIMIT 1];
        userContryCode = user[0].CountryCode;
        

    }
}