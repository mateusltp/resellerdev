@isTest
public without sharing class SmbOfferReviewControllerTest {
    
    @isTest
    public static void sendToApproval(){
        Account lAcc = DataFactory.newAccount();
        lAcc.NumberOfEmployees = 500;
        Database.insert( lAcc );

        Pricebook2 lStandardPb = DataFactory.newPricebook();
        Database.update( lStandardPb );
        
        Opportunity lOpp = new Opportunity(CurrencyIsoCode='BRL');

        lOpp = DataFactory.newOpportunity( lAcc.Id , lStandardPb , 'Client_Sales_New_Business' );
        lOpp.Quantity_Offer_Number_of_Employees__c = 500;
        Database.insert( lOpp );

        Product2 lSetupFee = DataFactory.newProduct( 'Setup Fee' , false , 'BRL' );
        Database.insert( lSetupFee );

        PricebookEntry lSetupFeeEntry = DataFactory.newPricebookEntry( lStandardPb , lSetupFee , lOpp );
        Database.insert( lSetupFeeEntry );

        Quote lProposal = DataFactory.newQuote( lOpp , 'Quote Test', 'Client_Sales_New_Business' );
        Database.insert( lProposal );

        QuoteLineItem lQuoteLineItem = DataFactory.newQuoteLineItem( lProposal , lSetupFeeEntry );
        lQuoteLineItem.Discount__c = 49.0;
        Database.insert( lQuoteLineItem );

        Payment__c lPayForLineItem = DataFactory.newPayment( lQuoteLineItem );
        Database.insert( lPayForLineItem );

        Waiver__c lWaiver = DataFactory.newWaiver( lPayForLineItem );
        lWaiver.Quote_Line_Item__c = lQuoteLineItem.Id;
        Database.insert( lWaiver );
        
        Waiver__c lWaiver2 = DataFactory.newWaiver( lPayForLineItem );
        lWaiver2.Quote_Line_Item__c = lQuoteLineItem.Id;
        Database.insert( lWaiver2 );
        
        Test.startTest();
        SmbOfferReviewController sorc = new SmbOfferReviewController();
        SmbOfferReviewController.sendToApproval(lOpp.Id, 'Quote Rationale', 49.0, lProposal.Id, 'Dealdesk Rationale', True, False);
        System.assertEquals( false , lProposal.Total_Discount_approved__c );
        Test.stopTest();
        
        
    }

}