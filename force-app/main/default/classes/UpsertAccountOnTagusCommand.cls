public without sharing class UpsertAccountOnTagusCommand extends AbstractUpdateableOutboundCommand {
  private Request request;
  private Object response;

  override public Object transformToSend() {
    if (
      !Schema.SObjectType.Account.isUpdateable() ||
      !Schema.SObjectType.Contact.isUpdateable()
    ) {
      throw new CustomException(
        'You should have update permission in contact and account sobjects to execute this event'
      );
    }

    TagusTransaction.TransactionType transactionType = event.getEventName()
        .contains('CREATE')
      ? TagusTransaction.TransactionType.CREATED
      : TagusTransaction.TransactionType.UPDATED;

    this.request = new Request();

    request.transaction_info = new TagusTransaction(event, transactionType);

    request.transaction_data = new TagusTransactionData(
      TagusTransactionData.SourceEventType.CRUD
    );

    Map<Id, Account> accountMap = (Map<Id, Account>) event.getPayloadFromJson(
      Map<Id, Account>.class
    );

    request.transaction_data.setData(new RequestData(accountMap));

    return request;
  }

  override public void processResult(Object response) {
    this.response = response;
  }

  override public void postUpdateExecute() {
    RequestData requestData = (RequestData) request.transaction_data.getdata();

    List<Account> accounts = new List<Account>();
    List<Contact> contacts = new List<Contact>();

    for (AccountTagusDTO accountDTO : requestData.accounts) {
      Account account = new Account(
        Id = accountDTO.getSalesforceId(),
        UUID__c = accountDTO.getId(),
        Is_Account_On_Tagus__c = true
      );

      accounts.add(account);

      for (AccountTagusDTO.ContactDTO contactDTO : accountDTO.getContacts()) {
        Contact contact = new Contact(
          Id = contactDTO.getSalesforceId(),
          UUID__c = contactDTO.getId()
        );

        contacts.add(contact);
      }
    }

    Database.update(accounts);

    Database.update(contacts);
  }

  private class Request {
    private TagusTransaction transaction_info;
    private TagusTransactionData transaction_data;
  }

  private class RequestData {
    private List<AccountTagusDTO> accounts;

    private RequestData(Map<Id, Account> accountMap) {
      setAccounts(accountMap);
    }

    private void setAccounts(Map<Id, Account> accountMap) {
      this.accounts = new List<AccountTagusDTO>();

      Set<Id> parentAccountIds = new Set<Id>();

      for (Account account : accountMap.values()) {
        if (String.isNotBlank(account.ParentId)) {
          parentAccountIds.add(account.ParentId);
        }
      }

      for (Account account : accountMap.values()) {
        if (!parentAccountIds.contains(account.Id)) {
          AccountTagusDTO accountDTO = new AccountTagusDTO(account);

          if (String.isNotBlank(account.ParentId)) {
            Account parentAccount = accountMap.get(account.ParentId);

            AccountTagusDTO parentAccountDTO = new AccountTagusDTO(
              parentAccount
            );
            this.accounts.add(parentAccountDTO);

            accountDTO.setParentId(parentAccountDTO.getId());
          }

          this.accounts.add(accountDTO);
        }
      }
    }
  }
}