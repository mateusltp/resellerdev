/* @description       : 
* author            : Laerte Kimura - leka@gft.com
* @group             : 
* @last modified on  : 19-05-2021
* @last modified by  : Laerte Kimura - leka@gft.com
* Modifications Log 
* Ver   Date         Author                         Modification
* 1.0   19-05-2021   Laerte Kimura - leka@gft.com   Created 
**/

@istest
public class ContractDataCreatorTest {
 @TestSetup
    static void createData(){
        Pricebook2 lStandardPricebook = DataFactory.newPricebook();
        Database.update( lStandardPricebook );
        
        Account lAcc = DataFactory.newAccount();              
        Database.insert( lAcc );
        
        //Create the opportunities
        Opportunity lNewBusinessOpp = DataFactory.newOpportunity( lAcc.Id, lStandardPricebook, 'Client_Sales_New_Business',true );  
        Opportunity lRenegociationOpp = DataFactory.newOpportunity( lAcc.Id, lStandardPricebook, 'Client_Sales_New_Business',true );  
        
        lNewBusinessOpp.fasttrackstage__c='Qualification';
        Database.insert(new List<Opportunity>{lNewBusinessOpp,lRenegociationOpp} );
        
        //Create the Quotes
        Quote lQuote1 = DataFactory.newQuote(lNewBusinessOpp, '1', 'Gympass_Plus');
        lQuote1.Employee_Corporate_Email__c =false;
        lQuote1.end_date__c = Date.today();
        lQuote1.contact_permission__c = 'Sign-up only';
        lQuote1.Will_this_company_have_the_Free_Product__c = 'No';
        Quote lQuote2 = DataFactory.newQuote(lRenegociationOpp, '2', 'Gympass_Plus');
        lQuote2.Employee_Corporate_Email__c =true;
        lQuote2.end_date__c = Date.today().adddays(7);
        lQuote2.contact_permission__c = 'Allowlist';
        lQuote2.Will_this_company_have_the_Free_Product__c = 'Yes';
        Database.insert(new List<Quote>{lQuote1, lQuote2});
        
        //Update the opportunities 
        lNewBusinessOpp.Name = 'New Business';
        lNewBusinessOpp.SyncedQuoteId = lQuote1.Id;
        lNewBusinessOpp.StageName = 'Proposta Aprovada';
        lNewBusinessOpp.FastTrackStage__c = 'Offer Approved';
        
        lRenegociationOpp.Name = 'Renegociation';
        lRenegociationOpp.SyncedQuoteId = lQuote2.Id;
        lRenegociationOpp.StageName = 'Proposta Aprovada';
        lRenegociationOpp.FastTrackStage__c = 'Offer Approved';
        lRenegociationOpp.Quantity_Offer_Number_of_Employees__c = 600;
        Database.update( new List<Opportunity>{lNewBusinessOpp,lRenegociationOpp} );
        
        
        Product2 accessFeeProduct = DataFactory.newProduct('Enterprise Subscription', false, 'BRL');
        Database.insert(accessFeeProduct);
        
        PricebookEntry accessFeeEntry = DataFactory.newPricebookEntry(lStandardPricebook, accessFeeProduct, lNewBusinessOpp);
        Database.insert(accessFeeEntry);
        
        //Create Enterprise Subscription
        QuoteLineItem accessFee1 = DataFactory.newQuoteLineItem(lQuote1, accessFeeEntry);
        QuoteLineItem accessFee2 = DataFactory.newQuoteLineItem(lQuote2, accessFeeEntry, 500);
        accessFee1.Description = 'Enterprise Subscription';
        accessFee2.Description = 'Enterprise Subscription';
        Database.insert(new List<QuoteLineItem>{accessFee1,accessFee2} );        
        
        //Payment
        Payment__c payForAccessFee1 = DataFactory.newPayment(accessFee1);
        Payment__c payForAccessFee2 = DataFactory.newPayment(accessFee2,'Boleto');
        payForAccessFee2.Frequency__c ='Yearly';
        Database.insert(new List<Payment__c>{payForAccessFee1,payForAccessFee2});  
        
        //Waiver
        Waiver__c accessFeeWaiver1 = DataFactory.newWaiver(payForAccessFee1);
        accessFeeWaiver1.Quote_Line_Item__c = accessFee1.id;
        Waiver__c accessFeeWaiver2 = DataFactory.newWaiver(payForAccessFee2);
        accessFeeWaiver2.Quote_Line_Item__c = accessFee2.id;
        Database.insert(new List<Waiver__c>{accessFeeWaiver1,accessFeeWaiver2});
        
        //Eligibility 
        Eligibility__c eli1 = DataFactory.newEligibility(payForAccessFee1, true);
        eli1.Payment_Method__c = 'Credit Card';
        Eligibility__c eli2 = DataFactory.newEligibility(payForAccessFee2, true);
        eli2.Payment_Method__c = 'Payroll';
        Database.insert(new List<Eligibility__c>{eli1,eli2 });
        
    }
    
    @isTest
    public static void createOpportunity() {
        
        List<Opportunity> lOpp = [ SELECT Id, Name, AccountId, SyncedQuoteId  FROM Opportunity  ];
        Set<id> setIdOpportunity =new Set<id>();
        Set<id> setAccountId =  new Set<id>();
        Set<id> setIdQuotes =new Set<id>();
        
        for(opportunity opp: lOpp ) {
            setIdOpportunity.add(opp.id);
            setAccountId.add(opp.AccountId);
            setIdQuotes.add(opp.SyncedQuoteId);
        }	
        
        Test.startTest();
        
        
        List<Opportunity> listOppToUpdate = new List<Opportunity>();
        List<Opportunity> listOppToRenegociation = new List<Opportunity>();
        Id quoteToCreateCase;
        Id quoteToCreateContract;
        for (opportunity opp:  lOpp){
            if (opp.Name == 'New Business'){
                opp.StageName = 'Lançado/Ganho';
                listOppToUpdate.add(opp);
                quoteToCreateCase = opp.SyncedQuoteId;
            }
            if (opp.Name == 'Renegociation'){
                listOppToRenegociation.add(opp);
                quoteToCreateContract = opp.SyncedQuoteId; 
            }
        }
        
        
        /*Add the First Contract of New Business*/
        APXT_Redlining__Contract_Agreement__c contractAgreementNewBusiness = DataFactory.newContractAgreement(listOppToUpdate[0].id, quoteToCreateCase, listOppToUpdate[0].AccountId); 
        Database.insert(new List<APXT_Redlining__Contract_Agreement__c>{contractAgreementNewBusiness});
        
        /*Create operational case to launch the New business */        
        Case lOperationalCase = DataFactory.newCase(listOppToUpdate[0].id, quoteToCreateCase, 'Deal_Desk_Operational');
        lOperationalCase.OwnerId = [ SELECT Id FROM Group WHERE Name = 'Deal Desk Operational' ].Id;
        Database.insert( lOperationalCase ); 
        
        Database.update(listOppToUpdate);
        
        /* Update one of the opportunity to renegociation */
        for(Opportunity oppRen : listOppToRenegociation){
            oppRen.recordtypeid=Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Success_Renegotiation').getRecordTypeId(); 
        }
        Database.update(listOppToRenegociation);
        
        
        /*Add a record to update*/
        Assert_Data__c cDataOpp = new Assert_Data__c( 	Opportunity__c = listOppToRenegociation[0].id,
                                                         Object_Name__c = 'opportunity', 
                                                         Field_Name__c = 'quantity_offer_number_of_employees__c' ,
                                                         Old_Value__c  = '999',
                                                         New_Value__c = '1999',
                                                         Fee_type__c = '');
        
        Assert_Data__c cDataQuoteLine = new Assert_Data__c( 	Opportunity__c = listOppToRenegociation[0].id,
                                                               Object_Name__c = 'quotelineitem', 
                                                               Field_Name__c = 'totalprice' ,
                                                               Old_Value__c  = '0',
                                                               New_Value__c = '80000.00',
                                                               Fee_type__c = 'Enterprise Subscription');
        
        Assert_Data__c cDataQuoteLineNotEnterprise = new Assert_Data__c( 	Opportunity__c = listOppToRenegociation[0].id,
                                                                            Object_Name__c = 'quotelineitem', 
                                                                            Field_Name__c = 'totalprice' ,
                                                                            Old_Value__c  = '80000.01',
                                                                            New_Value__c = '80000.00',
                                                                            Fee_type__c = 'Setup Fee');
        
        
        Database.insert(new List<Assert_Data__c>{cDataOpp, cDataQuoteLine,cDataQuoteLineNotEnterprise});
        
        
        /* create the contract to renegociation */
        APXT_Redlining__Contract_Agreement__c contractAgreement = DataFactory.newContractAgreement(listOppToRenegociation[0].id, quoteToCreateContract, lOpp[0].AccountId);
        contractAgreement.APXT_Renegotiation__c = true;
        contractAgreement.APXT_Redlining__Contract_Agreement_Family_Parent__c = contractAgreementNewBusiness.id;
        Database.insert(new List<APXT_Redlining__Contract_Agreement__c>{contractAgreement});
        
        
        Test.stopTest();   
        
        
        /*Start asserting the generated data*/
        List<Assert_Data__c> listContractData = [SELECT Id, Opportunity__c , Object_Name__c , Field_Name__c , Old_Value__c , New_Value__c, Fee_type__c
                                                   FROM Assert_Data__c WHERE Opportunity__c =: listOppToRenegociation[0].id  ];
       
        integer countExpectedRecordsFound = 10; /*Set the expected record count to be found */
        integer countAllrecordsVerified = 0;
        
         /*Quantidade de registros*/
        System.assert(listContractData.size() == countExpectedRecordsFound);
        
        for( Assert_Data__c contractData : listContractData){
            //system.debug('============== ' +contractData.Object_Name__c);
            switch on contractData.Object_Name__c{
                when 'opportunity' {
                    if (contractData.Field_Name__c == 'quantity_offer_number_of_employees__c'){
                        System.assertEquals(contractData.Old_Value__c, '500');
                        System.assertEquals(contractData.New_Value__c, '600');
                        countAllrecordsVerified ++;
                        system.debug('++++ ' + contractData.Field_Name__c);
                    }
                }
                when 'quote' {
                    if (contractData.Field_Name__c == 'contact_Permission__c'){
                        System.assertEquals(contractData.Old_Value__c, string.valueof('Sign-up only'));
                        System.assertEquals(contractData.New_Value__c, string.valueof('Allowlist'));
                        countAllrecordsVerified ++;
                        system.debug('++++ ' + contractData.Field_Name__c);
                    }
                    if (contractData.Field_Name__c == 'employee_corporate_email__c'){
                        System.assertEquals(contractData.Old_Value__c, string.valueof(false));
                        System.assertEquals(contractData.New_Value__c, string.valueof(true));
                        countAllrecordsVerified ++;
                        system.debug('++++ ' + contractData.Field_Name__c);
                    }
                    if (contractData.Field_Name__c == 'end_date__c'){
                        System.assertEquals(Date.valueof(contractData.Old_Value__c), Date.today());
                        System.assertEquals(Date.valueof(contractData.New_Value__c),  Date.today().addDays(7));
                        countAllrecordsVerified ++;
                        system.debug('++++ ' + contractData.Field_Name__c);
                    }
                    if (contractData.Field_Name__c == 'will_this_company_have_the_free_product__c'){
                        System.assertEquals(contractData.Old_Value__c, 'No');
                        System.assertEquals(contractData.New_Value__c, 'Yes');
                        countAllrecordsVerified ++;
                        system.debug('++++ ' + contractData.Field_Name__c);
                    }
                }
                when 'quotelineitem' {
                    if (contractData.Field_Name__c == 'subtotal__c'){
                        System.assertEquals(contractData.Old_Value__c, string.valueof(20000.00));
                        System.assertEquals(contractData.New_Value__c, string.valueof(18000.00));
                        countAllrecordsVerified ++;
                        system.debug('++++ ' + contractData.Field_Name__c);
                    }
                    if (contractData.Field_Name__c == 'totalprice'){
                        if (contractData.Fee_type__c == 'Enterprise Subscription'){
                            System.assertEquals(contractData.Old_Value__c, string.valueof(20000.00));
                            System.assertEquals(contractData.New_Value__c, string.valueof(45000.00));
                            countAllrecordsVerified ++;
                            system.debug('++++ ' + contractData.Field_Name__c);
                        }else{
                            
                            System.assertEquals(contractData.Old_Value__c, string.valueof(80000.01));
                            System.assertEquals(contractData.New_Value__c, string.valueof(80000.00));
                            System.assertEquals(contractData.Fee_type__c, string.valueof('Setup Fee'));
                            countAllrecordsVerified ++;
                            system.debug('++++ ' + contractData.Field_Name__c);
                        }
                    }
                }
                when 'payment__c' {
                    if (contractData.Field_Name__c == 'billing_day__c'){
                        System.assertEquals(contractData.New_Value__c, string.valueof(15));
                        countAllrecordsVerified ++;
                        system.debug('++++ ' + contractData.Field_Name__c);
                    }
                }
                when 'eligibility__c' {
                    if (contractData.Field_Name__c == 'payment_method__c'){
                        System.assertEquals(contractData.Old_Value__c, 'Credit Card');
                        System.assertEquals(contractData.New_Value__c, 'Payroll');
                        countAllrecordsVerified ++;
                        system.debug('++++ ' + contractData.Field_Name__c);
                    }
                }
                /*when 'waiver__c' {
                    if (contractData.Field_Name__c == 'percentage__c'){
                        System.assertEquals(contractData.Old_Value__c, 'Log All');
                        System.assertEquals(contractData.New_Value__c, '10.00');
                        countAllrecordsVerified ++;
                        system.debug('++++ ' + contractData.Field_Name__c);
                    }
                }*/
                
            }
            
        }
        
        /*Check if all fields were validated */
        System.assertequals(countExpectedRecordsFound, countAllrecordsVerified);
    } 
    
    
}