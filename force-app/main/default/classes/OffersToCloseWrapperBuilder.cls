/**
 * @author vncferraz
 * 
 * Provide  builder specialization for Type of Request "Close"
 */
public virtual class OffersToCloseWrapperBuilder extends BaseOfferWrapperBuilder {
    
    protected Opportunity oppToClose;
    protected OpportunityRepository opportunities;

    override
    public virtual void init(){
        super.init();
        this.wrapper = new OfferToCloseWrapper();
        this.opportunities = new OpportunityRepository();
    }

    override
    protected virtual void preLoad(Offer_Queue__c offer){
        try{
            this.oppToClose = this.opportunities.byId(offer.Opportunity_ID__c);        
        }catch(System.QueryException e){
            addError('Opportunity not found for ID '+offer.Opportunity_ID__c);
            return;
        }
    }

    override
    public virtual BaseOfferWrapper build( Offer_Queue__c offer){
        
        preLoad(offer);

        if ( !this.wrapper.hasError() )
            postLoad(offer);
        
        return this.wrapper;
    }
    
    override
    public BaseOfferWrapper postLoad(Offer_Queue__c offer){ 
        if (!wrapper.hasError())        
            wrapper.setOpportunity( toClosedOpportunity(offer) );

        return wrapper;
    }

    protected Opportunity toClosedOpportunity(Offer_Queue__c offer){
        String stageApiValue = null;
        if ( offer.Stage__c == 'Lost' )
            stageApiValue = 'Perdido';
        else if (offer.Stage__c== 'Launched/Won')
            stageApiValue = 'Lançado/Ganho';

        return new Opportunity(
            Id = offer.Opportunity_ID__c,
            StageName = stageApiValue,
            Loss_Reason__c = (stageApiValue=='Perdido'? offer.Loss_Reason__c : null ) 
        );
    }
}