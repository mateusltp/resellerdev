/**
 * @description       : 
 * @author            : alysson.mota@gympass.com
 * @group             : 
 * @last modified on  : 04-08-2022
 * @last modified by  : alysson.mota@gympass.com
**/
public interface IPartnerStepsTowardsSuccessService {
    Map<Id, List<Step_Towards_Success_Partner__c>> getOppNewFlowPartnerSteps(Set<Id> oppIdSet);
    //Map<Id, List<Step_Towards_Success_Partner__c>> getAccNewFlowPartnerSteps(Set<Id> accIdSet);
}