public without sharing class IntegrationRequestFactory {
    private Tagus_Integration_Settings__mdt integrationSettings;
    private Sales_Order_Integration_Settings__mdt soIntegrationSettings;
    private String sourceEvent;
    private String sObjectType;
    private String transactionName;
    private String transactionSource;
    private String transactionType;
    
    public IntegrationRequestFactory(
        String sourceEvent, 
        String sObjectType, 
        String transactionName,
        String transactionSource,
        String transactionType
    ) {
    	this.sourceEvent = sourceEvent;
        this.sObjectType = sObjectType; 
        this.transactionName = transactionName;
        this.transactionSource = transactionSource;
        this.transactionType = transactionType;
        
        this.integrationSettings = [
            SELECT Integration_Skeleton__c, Objects_To_Integrate__c, Data_Json_Skeleton__c
            FROM Tagus_Integration_Settings__mdt 
            LIMIT 1
        ];
        
        this.soIntegrationSettings = [
          	SELECT Sales_Order_Items_Params_Json_Struct__c, Sales_Order_Params_Json_Struct__c,
            	   Payment_Items_Params_Json_Struct__c, Eligibility_Items_Params_Json_Struct__c
            FROM Sales_Order_Integration_Settings__mdt
            LIMIT 1
        ];
    }
    
    public List<Integration_Request__c> getIntegrationRequests(List<sObject> sObjectList, String sObjectType) {
        List<Integration_Request__c> createdIntegrationRequests = new List<Integration_Request__c>();
        
        if (integrationSettings.Objects_To_Integrate__c.contains(sObjectType)) {
        	createdIntegrationRequests = getIntegrationRequest(sObjectList, sObjectType);    
        }
        
        return createdIntegrationRequests;
    }
    
    private List<Integration_Request__c> getIntegrationRequest(List<sObject> sObjectList, String sObjectType) {
        List<Integration_Request__c> createdIntegrationRequests = new List<Integration_Request__c>();
        Map<String, User> sObjectIdToUserMap = new Map<String, User>();        

        sObjectIdToUserMap = getUsersForSObjects(sObjectList, sObjectType);

        for (sObject obj : sObjectList) {
            String objId = String.valueOf(obj.get('Id'));

			Integration_Request__c integrationRequest = new Integration_Request__c();
            integrationRequest.Endpoint__c = 'callout:Tagus_Dev';
            integrationRequest.HTTP_Request_Method__c = 'POST';
            integrationRequest.Integration_Request_Status__c = '';
            integrationRequest.SObject_type__c = sObjectType;
            integrationRequest.SObject_ID__c = objId;
            
            // Assing user
            User user = sObjectIdToUserMap.get(objId);
            if (user != null) {
                integrationRequest.User__c = user.Id;
            }

            // Assign payload
            String jsonPayload = getJsonPayloadForsObjectType(obj);
            jsonPayload = jsonPayload.replace('\r\n', '');
            jsonPayload = jsonPayload.replace('\n', '');
            integrationRequest.Payload__c = jsonPayload;
            
            createdIntegrationRequests.add(integrationRequest);
        }
        
        return createdIntegrationRequests;
    }

    private Map<String, User> getUsersForSObjects(List<sObject> sObjectList, String sObjectType) {
        Map<String, User> sObjectIdToUserMap = new Map<String, User>();
        Set<String> sObjectIdSet = new Set<String>(); 
        Set<Id> userIdSet = new Set<Id>();
        Map<Id, User> users;

        for (sObject sObj : sObjectList) {
            String sObjId = String.valueOf(sObj.get('Id'));
            sObjectIdSet.add(sObjId);
        }

        if (sObjectType == 'Sales_Order__c') {
            List<Sales_Order__c> salesOrderList = [
                SELECT Id, Name, Account__r.Owner.Id
                FROM Sales_Order__c
                WHERE Id IN :sObjectIdSet
            ];

            for (Sales_Order__c so : salesOrderList) {
                userIdSet.add(so.Account__r.Owner.Id);
            }
            
            users = new Map<Id, User>([
                SELECT Id
                FROM User
                WHERE Id IN :userIdSet
            ]);            

            for (Sales_Order__c so : salesOrderList) {
                User u = users.get(so.Account__r.Owner.Id);
                if (u != null) {
                    sObjectIdToUserMap.put(String.valueOf(so.Id), u);
                }
            }
        }

        return sObjectIdToUserMap;
    }
    
    private String getJsonPayloadForsObjectType(sObject obj) {
        String finalJson = integrationSettings.Integration_Skeleton__c;
        
        finalJson = replaceTransactionData(finalJson);
        finalJson = replaceEntityData(finalJson, obj);
        
        List<Tagus_Integration_Data__mdt> integrationMtd = getIntegrationMetadataForSObject();
       	String objectData = buildObjectDataJsonList(obj, integrationMtd);
       	finalJson = finalJson.replace('@object_data', objectData);
        
        String objectRelatedData = buildRelatedObjectDataJsonList(obj, integrationMtd);
        finalJson = finalJson.replace('@relatedData', objectRelatedData);
        
        return finalJson;
   	}
    
    private List<Tagus_Integration_Data__mdt> getIntegrationMetadataForSObject() {
        List<Tagus_Integration_Data__mdt> integrationMtd = [
            SELECT Field_API_Name_Or_Related_API_Path__c, Name__c, Is_Relationship__c, Object_API_Name__c, Put_field_in_payload__c 
            FROM Tagus_Integration_Data__mdt
            WHERE Object_API_Name__c = :sObjectType
        ];
        
        return integrationMtd;
    }
    
    private String replaceTransactionData(String json) {
    	json = json.replace('@transaction_name', transactionName);
        json = json.replace('@transaction_type', transactionType);
        json = json.replace('@transaction_source', transactionSource);
        json = json.replace('@source_event_type', sourceEvent);
        json = json.replace('@actor_username', UserInfo.getUserName());
        json = json.replace('@actor_id', UserInfo.getUserId());
        json = json.replace('@actor_timestamp', String.valueOf(System.now()));
        json = json.replace('@object_name', sObjectType);
        
        return json;
    }
    
    private String replaceEntityData(String json, sObject obj) {
        String entityId = '';
        String entityUUId = '';
        
        if (sObjectType == 'Account') {
      		entityId = String.valueOf(obj.get('Id'));
            
            String s = String.valueOf(obj.get('UUID__c'));
            
           	entityUUId = s == null ? '' : s;
        } else if (sObjectType == 'Sales_Order__c') {
       		entityId = getAccountIdFromSalesOrder(obj);
            entityUUId = getAccountUUIdFromSalesOrder(obj);
        }
        
		json = json.replace('@entityId', entityId);
        json = json.replace('@entityUUId', entityUUId);
        
    	return json;
    }
    
    private String getAccountIdFromSalesOrder(sObject obj) {
   		String accountId = String.valueOf(obj.getSobject('Account__r').get('Id'));
        return accountId;
    }
    
    private String getAccountUUIdFromSalesOrder(sObject obj) {
        String accountUUId = String.valueOf(obj.getSobject('Account__r').get('UUID__c'));
        return (accountUUId == null ? '' : accountUUId);
    }
    
    private String buildObjectDataJsonList(sObject obj, List<Tagus_Integration_Data__mdt> integrationMtd) {
        String objectData = '';
        
        for (Integer i=0; i<integrationMtd.size(); i++) {
            if (integrationMtd.get(i).Put_field_in_payload__c) {
                if (i == (integrationMtd.size() - 1)) {
                    objectData += replaceObjectData(obj, integrationMtd.get(i));
                } else {
                    objectData += replaceObjectData(obj, integrationMtd.get(i)) + ',';
                }
            }
        }
        
        return objectData;
    }
    
    private String replaceObjectData(sObject obj, Tagus_Integration_Data__mdt integrationMtd) {
        String objectDataSkeleton = integrationSettings.Data_Json_Skeleton__c;
        
       	objectDataSkeleton = objectDataSkeleton.replace('@name', integrationMtd.Name__c);
      	objectDataSkeleton = objectDataSkeleton.replace('@value', getObjectFieldValue(obj, integrationMtd.Field_API_Name_Or_Related_API_Path__c));
        
        return objectDataSkeleton;
    }
    
    private String getObjectFieldValue(sObject sObj, String field) {
        String value = '';
        List<String> listPath = field.split('\\.');
        
        value = getValueFromFieldOrRelatedPath(sObj, listPath);
		
        return value;
    }
    
    private String getValueFromFieldOrRelatedPath(sObject sObj, List<String> listPath) {
   		String value = '';
        sObject obj = sObj;
        
        for (Integer i=0; i<listPath.size(); i++) {
            if (i < listPath.size() - 1) {
            	obj = getParentSObject(obj, listPath.get(i));
            } else {
                value = getValueFromSObjectAndField(obj, listPath.get(i));
            }
        }
        
        return value;
    }
    
    private String getValueFromSObjectAndField(sObject obj, String field) {
        String value = '';
        
        try {
        	value = String.valueOf(obj.get(field));
        
            if (value == null) {
                value = '';
            }    
        } catch(Exception e) {
            System.debug(e.getMessage());
        }
		
        return value;
    }
    
    private sObject getParentSObject(sObject obj, String field) {
    	sObject parentObj = obj.getSObject(field); 
        return parentObj;
    }
    
    private String buildRelatedObjectDataJsonList(sObject obj, List<Tagus_Integration_Data__mdt> integrationMtd) {
        if (sObjectType == 'Account') {
        	return '';
        } else if (sObjectType == 'Sales_Order__c') {
			return buildRelatedListForSalesOrder(obj);
        } else {
			return '';
        }
    }
    
    private String buildRelatedListForSalesOrder(sObject obj) {
        String id = String.valueOf(obj.get('Id'));
        List<Id> salesOrderItemListId = new List<Id>();
        Map<Id, List<Sales_Order_Item_Parameter__c>> salesOrderItemIdToSalesOrderItemParameter = 
            	new Map<Id, List<Sales_Order_Item_Parameter__c>>();
            
		List<Sales_Order_Parameter__c> soParameterList = [
        	SELECT Id, Name, Value__c
            FROM Sales_Order_Parameter__c
            WHERE Sales_Order__c =:id
        ];
        
       	Map<Id, Sales_Order_Item__c> salesOrderItemMap = new Map<Id, Sales_Order_Item__c> ([
        	SELECT Id, Name, UUID__c
            FROM Sales_Order_Item__c
            WHERE Sales_Order__c =:id
        ]);
        
        List<Sales_Order_Item_Parameter__c> salesOrderItemParameterList = [
       		SELECT Id, Name, Value__c, Sales_Order_Item__c, Sales_Order_Item__r.Name
            FROM Sales_Order_Item_Parameter__c
            WHERE Sales_Order_Item__c IN :salesOrderItemMap.keySet()
        ];
        
     	String relatedJsonStr = soIntegrationSettings.Sales_Order_Params_Json_Struct__c;
        relatedJsonStr = replaceSOParametersList(relatedJsonStr, soParameterList);
        relatedJsonStr = replaceSOItemsList(relatedJsonStr, salesOrderItemParameterList, salesOrderItemMap);
        
        return relatedJsonStr;
    }
    
    private String replaceSOParametersList(String skeleton, List<Sales_Order_Parameter__c> soParameterList) {
        String paramsList = '';
        for (Integer i=0; i<soParameterList.size(); i++) {
            if (i == (soParameterList.size() - 1)) {
                paramsList += replaceSalesOrderParamData(soParameterList.get(i)); 
           	} else {
            	paramsList += replaceSalesOrderParamData(soParameterList.get(i)) + ',';
           	}
        }
        
        skeleton =  skeleton.replace('@parametersList', paramsList);
        return skeleton;
    }
    
    private String replaceSalesOrderParamData(Sales_Order_Parameter__c sop) {
		String data = integrationSettings.Data_Json_Skeleton__c;
        data = data.replace('@name', sop.Name);
        data = data.replace('@value', (sop.Value__c == null ? '' : String.valueOf(sop.Value__c)));
        return data;
    }
    
    // SOItem
    private String replaceSOItemsList(String json, List<Sales_Order_Item_Parameter__c> salesOrderItemParameterList, Map<Id, Sales_Order_Item__c> salesOrderItemMap) {
        Map<Id, List<Sales_Order_Item_Parameter__c>> paramMap = new Map<Id, List<Sales_Order_Item_Parameter__c>>();
        String salesOrderItemsParamsJson = soIntegrationSettings.Sales_Order_Items_Params_Json_Struct__c;
        String salesOrderItemsParamsListJson = '';
        
        for (Sales_Order_Item_Parameter__c param : salesOrderItemParameterList) {
            if (paramMap.containsKey(param.Sales_Order_Item__c)) {
           		paramMap.get(param.Sales_Order_Item__c).add(param);
            } else {
                paramMap.put(param.Sales_Order_Item__c, new List<Sales_Order_Item_Parameter__c>{param});
            }
        }
        
        List<Id> idList = new List<Id>();
        idList.addAll(paramMap.keySet());
        
        for (Integer i=0; i<idList.size(); i++) {
            if (i == idList.size()-1) {
            	salesOrderItemsParamsListJson += salesOrderItemsParamsJson;    
            } else {
                salesOrderItemsParamsListJson += salesOrderItemsParamsJson + ',';
            }
			             
            Id salesOrderItemId = idList.get(i);
            System.debug('ID');
            System.debug(salesOrderItemId);
            List<Sales_Order_Item_Parameter__c> paramsList = paramMap.get(salesOrderItemId);
            
            Sales_Order_Item__c soItem = salesOrderItemMap.get(salesOrderItemId);
            
            salesOrderItemsParamsListJson = salesOrderItemsParamsListJson.replace('@typeName', 'ACCESS_FEE');
            salesOrderItemsParamsListJson = salesOrderItemsParamsListJson.replace('@itemUUid', soItem.UUID__c);
            salesOrderItemsParamsListJson = replaceSaleOrderItemsParamsList(salesOrderItemsParamsListJson, paramsList);
        }
        
        json = json.replace('@salesOrderItemList', salesOrderItemsParamsListJson);
        return json;
    }
    
    private String replaceSaleOrderItemsParamsList(String json, List<Sales_Order_Item_Parameter__c> paramsList) {
        String itemParamsList = '';
        
        for (Integer i=0; i<paramsList.size(); i++) {
            if (i == (paramsList.size() - 1)) {
                itemParamsList += replaceSalesOrderParamData(paramsList.get(i)); 
           	} else {
            	itemParamsList += replaceSalesOrderParamData(paramsList.get(i)) + ',';
           	}
        }
        
        json = json.replace('@itemsList', itemParamsList);
        return json;
    }
    
     private String replaceSalesOrderParamData(Sales_Order_Item_Parameter__c soip) {
		String data = integrationSettings.Data_Json_Skeleton__c;
        data = data.replace('@name', soip.Name);
        data = data.replace('@value', (soip.Value__c == null ? '' : String.valueOf(soip.Value__c)));
        return data;
    }
}