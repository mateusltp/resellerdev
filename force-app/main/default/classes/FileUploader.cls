public class FileUploader 
{
    public String IdEvent{get;set;}
    list<String> listCNPJ{get;set;}
    list<String> listEmail{get;set;}
    public string nameFile{get;set;}
    public String fullFileURL{get;set;}
    public Blob contentFile{get;set;}
    List<Account> accstoupload;
    String[] filelines = new String[]{};
        
    public FileUploader()
    {
        ContentDocument doc = new ContentDocument();
        
        IdEvent = ApexPages.currentPage().getParameters().get('Id');
        
        List<ContentDocument> listDoc = [SELECT Id 
                               			 FROM ContentDocument 
                               			 WHERE Title = 'Modelo'];
        
        if (listDoc.size() > 0)
        {
            doc = listDoc[0];
        	fullFileURL = URL.getSalesforceBaseUrl().toExternalForm() + System.Label.LinkDownloadBulkUpload + doc.id;
        }
    }
    
    
    public Pagereference ReadFile()
    {
        List<Id> idList =  new List<Id>();
        Map<String,string> map1 = new Map<string,string>();
        
        system.debug('contentFile: ' + contentFile);
        
        listCNPJ = new list<String>();
        listEmail = new list<String>();
        nameFile = contentFile.toString();
        
        system.debug('nameFile: ' + nameFile);
        
        List<List<String>> parsedFields = parseCSV(nameFile,true);
        
        
        map<String,Account> mapAccount = new map<String,Account>();
        map<String,Contact> mapContact = new map<String,Contact>();
        
        list<Account> listNewAccount = new list<Account>();
        list<Contact> listNewContacts = new list<Contact>();
        list<EventRelation> listNewEventRelation = new list<EventRelation>();
        
        try {
            list<Account> accQuery = [SELECT Id,Id_Company__c
                                      FROM Account
                                      WHERE Id_Company__c IN :listCNPJ];
            
            list<Contact> contQuery = [SELECT Id, Email
                                       FROM Contact
                                       WHERE Email IN :listEmail];
            
            for(Account acc : accQuery)
                mapAccount.put(acc.Id_Company__c, acc);
            
            for(Contact cont : contQuery)
                mapContact.put(cont.Email, cont);
            
            System.debug('PARSEFIELDS: ' + parsedFields);
            
            for (list<String> psF : parsedFields)
            {
                if (!mapAccount.containsKey(psF[8]))
                {
                    account acc = new Account();
                    acc.Name = psF[2];
                    acc.BillingCountry = psF[9];
                    acc.BillingState = psF[10];
                    acc.Industry = psF[6];
                    acc.NumberOfEmployees = Integer.valueOf(psF[7]);
                    acc.Id_Company__c = psF[8];
                    listNewAccount.add(acc);
                    
                    System.debug('listNewAccount: ' + listNewAccount);
                }
            }
            
            insert listNewAccount;
            
            for (Account acc : listNewAccount)
            {
                mapAccount.put(acc.Id_Company__c,acc);
                System.debug('mapAccount: ' + mapAccount);
            }
            
            for (list<String> psF : parsedFields)
            {
                if (mapAccount.containsKey(psF[8]))
                {
                    account acc = mapAccount.get(psF[8]);
                    if (!mapContact.containsKey(psF[5])){
                        Contact contact = new Contact();
                        contact.LastName = psF[1];
                        contact.Cargo__c = psF[3];
                        contact.Area_Setor__c = psF[4];
                        contact.Email = psF[5];
                        contact.AccountId = acc.Id;
                        listNewContacts.add(contact);
                    }
                }
                else{
                    //mensagem de erro
                }
            }
            insert listNewContacts;
            
            for (Contact cont : listNewContacts)
            {
                mapContact.put(cont.Email,cont);
                System.debug('mapContact: ' + mapContact);
            }
            
            
            
            
            
            for (list<String> psF : parsedFields)
            {
                Contact cont = mapContact.get(psF[5]);
                
                EventRelation Evt = new EventRelation();
                Evt.RelationId = cont.Id;
                Evt.EventId = psF[0];
                Evt.IsParent = true;
                Evt.IsInvitee = false;
                listNewEventRelation.add(Evt);
            }
            
            insert listNewEventRelation;            
        } catch (System.ListException e) {
            ApexPages.Message myMsg3 = new ApexPages.Message(ApexPages.Severity.INFO,'Create Records Error');
            ApexPages.addMessage(myMsg3);
        }
        
        ApexPages.Message myMsg2 = new ApexPages.Message(ApexPages.Severity.INFO,'File Uploaded Successfully');
        ApexPages.addMessage(myMsg2);    
        return null;
    }  
    
    public List<List<String>> parseCSV(String contents,Boolean skipHeaders)
    {
        List<List<String>> allFields = new List<List<String>>();
        List<String> lines = new List<String>();
        List<String> campos = new list<String>();
        
        contents = contents.replaceAll(',"""',',"DBLQT').replaceall('""",','DBLQT",');
        contents = contents.replaceAll('""','DBLQT');
        
        try {
            lines = contents.split('\n');
        } catch (System.ListException e) {
            System.debug('Limits exceeded?' + e.getMessage());
        }
        
        system.debug('Line num is: '+ lines.size());
        
        for (Integer l = 0; l < lines.size(); l++)
        {
            system.debug('>> Line: ' + lines[l]);
            if (l == 0)
            {
                campos = lines[l].split(';');
                system.debug('>> Campos.size: ' + campos);
            } else {
                list<String> dados = lines[l].split(';');
                System.debug('DADOS:  POSICAO : ' + dados);
                listCNPJ.add(dados[8]);
                
                allFields.add(dados);
            }
        }
        
        System.debug('listCNPJ: ' + listCNPJ);
        
        return allFields;       
    } 
}