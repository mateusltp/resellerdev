/**
 * @description       : 
 * @author            : pohr@gft.com
 * @group             : 
 * @last modified on  : 05-31-2021
 * @last modified by  : gft.samuel.silva@ext.gympass.com
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   12-05-2021   pohr@gft.com   Initial Version
**/

public without sharing class PartnerSuccessAutoRenewalFlow {
    public class CustomException extends Exception {}
    private static String gOppPartnersRt; 
    private static List< Id > gLstNoOppToCloneAccId = new List< Id >();
    private static String gSubType;
    private static String gCancelReason;  
    private static String gCancelSubCateg;  
    private static String gOppSource;
    
    public static List< Opportunity > cloneLastClosedOpp( List< Id > aLstAccId, String aRtDevName, String aSubType, String aCancelReason, String aCancelSubCateg, String aOppSource){
        gOppPartnersRt = aRtDevName;
        gSubType = aSubType;
        gCancelReason = aCancelReason;
        gCancelSubCateg = aCancelSubCateg;
        gOppSource = aOppSource;
        List< Opportunity > lLstLastClosedOpp = getLastClosedOppByAcc( aLstAccId );
        System.debug('lLstLastClosedOpp ' + lLstLastClosedOpp.size());
        List< Opportunity > lLstPartnersOpp = new List< Opportunity >();

        if( !gLstNoOppToCloneAccId.isEmpty() ) { lLstPartnersOpp = createNewPartnersOpp(); }

        if( lLstLastClosedOpp.isEmpty() ){ return lLstPartnersOpp; }

        Map< Id , Opportunity > lMapOppIdClonedOpp = cloneOfferSobjects( lLstLastClosedOpp );
        
        return !lLstPartnersOpp.isEmpty() ? lLstPartnersOpp : lMapOppIdClonedOpp.values();
    }

    private static List< Opportunity > createNewPartnersOpp(){
        List< Opportunity > lLstPartnersOpp = new List< Opportunity >();

        for( Id iAccId : gLstNoOppToCloneAccId ){
            lLstPartnersOpp.add(
                setDefaultValuesForRenegotiationOpp( new Opportunity( AccountId = iAccId , No_Won_Opportunity_in_SF__c = true ) )
            );
        }

        try{
            Database.insert( lLstPartnersOpp );
        } catch( Exception lExcep ){
            System.debug('Error Message: ' + lExcep.getMessage());
            System.debug('Error Stack Trace: ' + lExcep.getStackTraceString());
            throw new CustomException('Error creating new opportunity. Error message: ' + lExcep.getMessage());
        }

        return lLstPartnersOpp;
    }


    private static List< Opportunity > getLastClosedOppByAcc( List< Id > aLstAccId ){
        Set< String > lSetRTClosedOpp;
        if(gOppPartnersRt == 'Partner_Small_and_Medium_Renegotiation') {
            lSetRTClosedOpp = new Set< String >{'Gyms_Small_Medium_Partner','Partner_Small_and_Medium_Renegotiation'};
        } else if(gOppPartnersRt == 'Partner_Wishlist_Renegotiation'){
            lSetRTClosedOpp = new Set< String >{'Gyms_Wishlist_Partner','Partner_Wishlist_Renegotiation'};
        }

        List< Opportunity > lLstLastClosedOpp = new List< Opportunity >();
        System.debug('lSetRTClosedOpp ' + lSetRTClosedOpp);
        List<Account> accLst =  Database.query( getAccLastClosedOppQuery() ) ;
        System.debug('accLst '+accLst);
        for( Account iAcc : Database.query( getAccLastClosedOppQuery() ) ){
            System.debug('Entrou no for');            
            if( iAcc.Opportunities.isEmpty() ) {
                gLstNoOppToCloneAccId.add( iAcc.Id );
                System.debug('Entrou no if');
                continue;
            }
            
            Opportunity lOpp = setDefaultValuesForRenegotiationOpp( iAcc.Opportunities[0] );
            System.debug('lOpp ' + lOpp);
            lLstLastClosedOpp.add( lOpp );
        }

        return lLstLastClosedOpp;
    }

    private static Map< Id , Opportunity > cloneOfferSobjects( List< Opportunity > aLstOppToClone ){
        Map< Id , Opportunity > lMapOppIdClonedOpp = cloneOpportunity( aLstOppToClone );

        cloneAccOppRelationship( lMapOppIdClonedOpp );

        Map< Id , Quote > lMapQuoteIdClonedQuote = cloneQuote( lMapOppIdClonedOpp );

        //Product_Item__c 
        Map< Id , Product_Item__c> lMapOppIdProductItems = cloneProdutItems( lMapOppIdClonedOpp );

        //Gym_Activity__c 
        Map< Id , Gym_Activity__c > lMapProdIdGymActivity = cloneGymActivities( lMapOppIdProductItems );

        //Map< Id , QuoteLineItem > lMapIdQuoteLineItemsCloned = cloneQuoteLineItems( lMapQuoteIdClonedQuote );
       
        // Map< Id , Payment__c > lMapIdBillingSettingCloned = cloneQuoteBillingSettings( lMapIdQuoteLineItemsCloned );

        // cloneEligibilities( lMapIdBillingSettingCloned );
        
        return lMapOppIdClonedOpp;
    }

    private static Map< Id , Opportunity > cloneOpportunity( List< Opportunity > aLstOppToClone ){
        Map< Id , Opportunity > lMapOppIdClonedOpp = new Map< Id , Opportunity >();
        Map< Id , Form__c > lMapOppIdM1Form = new Map< Id , Form__c >();               
        Id lOppId;

        for( Opportunity iOpp : aLstOppToClone ){
            lOppId = iOpp.Id;
            iOpp.Id = null;
            lMapOppIdClonedOpp.put( lOppId , iOpp.clone(false, true, false, false) );
        }

        try{
            Database.insert( lMapOppIdClonedOpp.values() );
        } catch( Exception lExcep ){
            System.debug('Error Message: ' + lExcep.getMessage());
            System.debug('Error Stack Trace: ' + lExcep.getStackTraceString());
            throw new CustomException('Error while cloning Opportunity. Error message: ' + lExcep.getMessage());
        }

        return lMapOppIdClonedOpp;
    }

    private static void cloneAccOppRelationship( Map< Id , Opportunity > aMapOppIdClonedOpp ){
        Set< Id > aSetOppId = aMapOppIdClonedOpp.keySet();
        List< Account_Opportunity_Relationship__c > lLstAccOppRelationshipCloned = new List< Account_Opportunity_Relationship__c >();

        for( Account_Opportunity_Relationship__c iAccOppRelationship : 
             Database.query( getSobjectQuery( 'Account_Opportunity_Relationship__c' , 'WHERE Opportunity__c =: aSetOppId' ) ) ){ 
            iAccOppRelationship.Id = null;
            iAccOppRelationship.Opportunity__c = aMapOppIdClonedOpp.get( iAccOppRelationship.Opportunity__c ).Id;

            lLstAccOppRelationshipCloned.add( iAccOppRelationship.clone(false, true, false, false) );
        }

        try{
            Database.insert( lLstAccOppRelationshipCloned );
        } catch( Exception lExcep ){
            System.debug('Error Message: ' + lExcep.getMessage());
            System.debug('Error Stack Trace: ' + lExcep.getStackTraceString());
            throw new CustomException('Error while cloning Account Opportunity Relationship. Error message: ' + lExcep.getMessage());
        }
    }

    private static Map< Id , Quote > cloneQuote( Map< Id , Opportunity > aMapOppIdClonedOpp ){
        Map< Id , Quote > lMapQuoteIdClonedQuote = new Map< Id , Quote >();
        Set<Id> lLstOppId = aMapOppIdClonedOpp.keySet();
        Id lQuoteId;

        for( Quote iQuote : Database.query( getSobjectQuery( 'Quote' , 'WHERE OpportunityId =: lLstOppId' ) ) ){
            lQuoteId = iQuote.Id;
            
            iQuote.Id = null;
            iQuote.Start_Date__c = null;
            iQuote.End_Date__c = null;
            iQuote.Payment_Terms_for_License_Fee__c = null;
            iQuote.OpportunityId = aMapOppIdClonedOpp.get( iQuote.OpportunityId ).Id;
            lMapQuoteIdClonedQuote.put( lQuoteId , iQuote.clone(false, true, false, false) );
        }
        system.debug('>>>>>>>>> quote' + lMapQuoteIdClonedQuote);
        try{
            Database.insert( lMapQuoteIdClonedQuote.values() );
        } catch( Exception lExcep ){
            System.debug('Error Message: ' + lExcep.getMessage());
            System.debug('Error Stack Trace: ' + lExcep.getStackTraceString());
            throw new CustomException('Error while cloning Proposal. Error message: ' + lExcep.getMessage());
        }

        return lMapQuoteIdClonedQuote;
    }

    private static Map<Id, Product_Item__c> cloneProdutItems(Map< Id , Opportunity > aMapOppIdClonedOpp ){
        Map< Id , Product_Item__c > lMapProdIdClonedProductItems = new Map< Id , Product_Item__c >();
        Set<Id> lLstOppId = aMapOppIdClonedOpp.keySet();
        Id lProdId;

        for( Product_Item__c iProductItem : Database.query( getSobjectQuery( 'Product_Item__c' , 'WHERE Opportunity__c =: lLstOppId' ) ) ){
            lProdId = iProductItem.id;
            iProductItem.Id = null;
            iProductItem.Opportunity__c = aMapOppIdClonedOpp.get( iProductItem.Opportunity__c ).Id;   
            lMapProdIdClonedProductItems.put( lProdId , iProductItem.clone(false, true, false, false) );
        }
        system.debug('>>>>>>>>> product items' + lMapProdIdClonedProductItems);
        try{
            Database.insert( lMapProdIdClonedProductItems.values() );
        } catch( Exception lExcep ){
            System.debug('Error Message: ' + lExcep.getMessage());
            System.debug('Error Stack Trace: ' + lExcep.getStackTraceString());
            throw new CustomException('Error while cloning Proposal. Error message: ' + lExcep.getMessage());
        }

        return lMapProdIdClonedProductItems;

    }

    private static Map< Id , Gym_Activity__c > cloneGymActivities ( Map< Id , Product_Item__c > lMapProdIdProductItems ){
        Map< Id , Gym_Activity__c > lMapIdGymActivityCloned = new Map< Id , Gym_Activity__c >();
        Set<Id> lLstProdId = lMapProdIdProductItems.keySet();      
        Id lProdId;

        List<Gym_Activity__c> activityes =  Database.query( getSobjectQuery( 'Gym_Activity__c' , 'WHERE Product_Item__c =: lLstProdId '));
        System.debug('activityes ' + activityes.size() + ' lLstProdId ' + lLstProdId);
        for( Gym_Activity__c iGymActivity : Database.query( getSobjectQuery( 'Gym_Activity__c' , 'WHERE Product_Item__c =: lLstProdId '))){
            lProdId = lMapProdIdProductItems.get( iGymActivity.Product_Item__c ).Id;
            iGymActivity.Id = null;
            iGymActivity.Product_Item__c = lProdId;
            lMapIdGymActivityCloned.put( lProdId , iGymActivity.clone(false, true, false, false) );
        }
     
        try{
            Database.insert( lMapIdGymActivityCloned.values() );
        } catch( Exception lExcep ){
            System.debug('Error Message: ' + lExcep.getMessage());
            System.debug('Error Stack Trace: ' + lExcep.getStackTraceString());
            throw new CustomException('Error while cloning Gym Activities. Error message: ' + lExcep.getMessage());
        }

        return lMapIdGymActivityCloned;
   }

    private static Opportunity setDefaultValuesForRenegotiationOpp( Opportunity aOpp ){
        aOpp.Name = gSubType;        
        aOpp.Sub_Type__c = gSubType;
        aOpp.Cancellation_Reason__c = gCancelReason;
        aOpp.Cancellation_Reason_subcategory__c = gCancelSubCateg;
        aOpp.StageName = 'Qualificação';
        aOpp.Source__c = gOppSource;
        aOpp.CloseDate = System.today();
        aOpp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(gOppPartnersRt).getRecordTypeId();
        aOpp.OwnerId = UserInfo.getUserId();
        aOpp.Submission_Date__c = null;
        aOpp.Data_do_Lancamento__c = null;
        aOpp.Type = 'Renegotiation';
        return aOpp;
    }

    private static String getSobjectQuery( String sObjectName , String whereClause ){
        return ' SELECT ' + Utils.getSobjectFieldsDevNameForQuery( sObjectName ) + 
               ' FROM ' + sObjectName + ' ' +
               whereClause;
    }

    private static String getAccLastClosedOppQuery(){
        return 'SELECT Id,' + 
               ' ( SELECT ' + Utils.getSobjectFieldsDevNameForQuery('Opportunity') + 
               '   FROM Opportunities WHERE isClosed = true AND StageName = \'Lançado/Ganho\' AND Recordtype.DeveloperName IN: lSetRTClosedOpp ORDER BY CreatedDate DESC LIMIT 1 ) ' +
               'FROM Account ' +
               'WHERE Id =: aLstAccId';
    }
}