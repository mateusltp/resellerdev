/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 03-28-2022
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/
public with sharing class SearchCustomLookupController {

    private final static Integer MAX_RESULTS = 20;
    
    @AuraEnabled(Cacheable=true)
    public static List<LookupSearchResult> userSearch(String searchTerm, List<String> selectedIds) {
        // Prepare query paramters
        searchTerm += '*';


        // Execute search query
        List<List<SObject>> searchResults = [
                FIND :searchTerm
                IN ALL FIELDS
                        RETURNING
                        User(Id, Name, Email WHERE id NOT IN :selectedIds)
                LIMIT :MAX_RESULTS
        ];
   
        // Prepare results
        List<LookupSearchResult> results = new List<LookupSearchResult>();

        // Extract Users & convert them into LookupSearchResult
        String userIcon = 'standard:user';
        User[] users = (List<User>) searchResults[0];
        for (User iUser : users) {
            String subtitle = iUser.Email == null ? 'Email' : 'Email • ' + iUser.Email;
            results.add(new LookupSearchResult(iUser.Id, 'User', userIcon, iUser.Name, subtitle));
        }

        // Optionnaly sort all results on title
        results.sort();

        return results;
    }

    @AuraEnabled(Cacheable=true)
    public static List<LookupSearchResult> getUserRecentlyViewed() {
        List<LookupSearchResult> results = new List<LookupSearchResult>();
        // Get recently viewed records of type Users or Opportunity
        List<RecentlyViewed> recentRecords = [
                SELECT Id, Name
                FROM RecentlyViewed
                WHERE Type = 'User'
                ORDER BY LastViewedDate DESC
                LIMIT 5
        ];
        // Convert recent records into LookupSearchResult
        for (RecentlyViewed recentRecord : recentRecords) {
            results.add(
                    new LookupSearchResult(
                            recentRecord.Id,
                            'User',
                            'standard:user',
                            recentRecord.Name,
                            'User • ' + recentRecord.Name
                    )
            );
        }
        return results;
    }

    @AuraEnabled(Cacheable=true)
    public static List<LookupSearchResult> searchAccount(String searchTerm, List<String> selectedIds) {
        // Prepare query paramters
        searchTerm += '*';


        // Execute search query
        List<List<SObject>> searchResults = [
                FIND :searchTerm
                IN ALL FIELDS
                        RETURNING
                        Account(Id, Name, Owner.Name WHERE id NOT IN :selectedIds)
                LIMIT :MAX_RESULTS
        ];
   
        // Prepare results
        List<LookupSearchResult> results = new List<LookupSearchResult>();

        // Extract Users & convert them into LookupSearchResult
        String accountIcon = 'standard:account';
        Account[] accounts = (List<Account>) searchResults[0];
        for (Account iAccount : accounts) {
            String subtitle = iAccount.Owner.Name == null ? 'Owner:' : 'Owner • ' + iAccount.Owner.Name;
            results.add(new LookupSearchResult(iAccount.Id, 'Account', accountIcon, iAccount.Name, subtitle));
        }

        // Optionnaly sort all results on title
        results.sort();

        return results;
    }

    @AuraEnabled(Cacheable=true)
    public static List<LookupSearchResult> searchContact(String searchTerm, List<String> selectedIds) {
        // Prepare query paramters
        searchTerm += '*';
        // Execute search query
        List<List<SObject>> searchResults = [
                FIND :searchTerm
                IN ALL FIELDS
                        RETURNING
                        Contact(Id, Name, Email WHERE id NOT IN :selectedIds)
                LIMIT :MAX_RESULTS
        ];
   
        // Prepare results
        List<LookupSearchResult> results = new List<LookupSearchResult>();

        // Extract Users & convert them into LookupSearchResult
        String contactIcon = 'standard:contact';
        Contact[] contacts = (List<Contact>) searchResults[0];
        for (Contact iContact : contacts) {
            String subtitle = iContact.Email == null ? 'Email:' : 'Email • ' + iContact.Email;
            results.add(new LookupSearchResult(iContact.Id, 'Contact', contactIcon, iContact.Name, subtitle));
        }

        // Optionnaly sort all results on title
        results.sort();

        return results;
    }


    @AuraEnabled(Cacheable=true)
    public static List<LookupSearchResult> bankAccountSearch(String searchTerm, List<String> selectedIds) {
        // Prepare query paramters
        searchTerm += '*';


        // Execute search query
        List<List<SObject>> searchResults = [
                FIND :searchTerm
                IN ALL FIELDS
                        RETURNING
                        Bank_Account__c(Id, Bank_Name__c, Bank_Account_Ownership__c WHERE id NOT IN :selectedIds)
                LIMIT :MAX_RESULTS
        ];
   
        // Prepare results
        List<LookupSearchResult> results = new List<LookupSearchResult>();

        // Extract Users & convert them into LookupSearchResult
        String bankAccountIcon = 'standard:case';
        Bank_Account__c[] bankAccounts = (List<Bank_Account__c>) searchResults[0];
        for (Bank_Account__c iBankAcc : bankAccounts) {
            String subtitle = iBankAcc.Bank_Account_Ownership__c == null ? 'Owner' : 'Owner • ' + iBankAcc.Bank_Account_Ownership__c;
            results.add(new LookupSearchResult(iBankAcc.Id, 'Bank Account', bankAccountIcon, iBankAcc.Bank_Name__c, subtitle));
        }

        // Optionnaly sort all results on title
        results.sort();

        return results;
    }
}