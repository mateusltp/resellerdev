/**
 * @description       : 
 * @author            : AlyssonMota
 * @group             : 
 * @last modified on  : 12-14-2020
 * @last modified by  : Alysson Mota
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   12-14-2020   Alysson Mota   Initial Version
**/
public class ProposalExceptionsRejected {
    
    List<Quote> quoteList = Trigger.New;	
    
    public void validarExceptionsRejected() {
        Id quoteRecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Indirect_Channel').getRecordTypeId();

        Map<Id, Exception__c> mapException = new Map<Id, Exception__c>();
        Set<Id> setId = new Set<Id>();
        
        for (Quote forQuote : quoteList) {
            if (forQuote.RecordTypeId == quoteRecordTypeId) 
                setId.add(forQuote.Id);
        }
        
        List<Exception__c> exceptionQuery = new List<Exception__c>();
        
        if (setId.size() > 0) {
            exceptionQuery = [
                SELECT Id, name, status__c, Proposal__c 
                FROM Exception__c 
                WHERE Proposal__c IN : setId
            ];
        }

        System.System.debug('Exception List' + exceptionQuery);
        
        for(Exception__c excepObj : exceptionQuery) {
            if(excepObj.status__c != 'Approved') {
                mapException.put(excepObj.Proposal__c, excepObj);
            }
        } 
        
        for(Quote forQuote : quoteList) {
            if(mapException != null) {
                
                Exception__c exceptionObj = mapException.get(forQuote.Id);
                
                if(exceptionObj != null) {
                    forQuote.Exception__c = True;
                } 
                else {
                    forQuote.Exception__c = False;                 
                }
            }
        }
    }
}