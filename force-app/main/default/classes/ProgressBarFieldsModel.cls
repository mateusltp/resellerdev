/**
 * @File Name          : ProgressBarFieldsModel.cls
 * @Description        : 
 * @Author             : Samuel Silva - GFT (slml@gft.com)
 * @Group              : 
 * @Last Modified By   : gft.samuel.silva@ext.gympass.com
 * @Last Modified On   : 05-27-2021
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    30/04/2020   Samuel Silva - GFT (slml@gft.com)     Initial Version
**/
public without sharing class ProgressBarFieldsModel {
    
    private Double totalFields = 0;
    
    public Double checkFieldsInObject(String recordId){  
        SObject obj = findFieldsForGymPartnerAccount(recordId);   
        Double progress = (obj.getPopulatedFieldsAsMap().keySet().size() / totalFields) * 100;
        return Math.roundToLong(progress); 
    }
    
    private SObject findFieldsForGymPartnerAccount(String recordId){
         
        String argument = '';        
        List<Metadata.Metadata> layouts = null;
        
        String sObjectType = ((Id)recordId).getSobjectType().getDescribe().getName();
       
        if (sObjectType == 'Opportunity') { 
            Opportunity opp = [SELECT RecordType.Name FROM Opportunity where Id =: recordId];

            switch on opp.RecordType.Name {
           		when 'Partner - Small and Medium'{                                                           
                    layouts = Metadata.Operations.retrieve(Metadata.MetadataType.Layout, new List<String> {'Opportunity-Gyms - Small/Medium Partners'}); 
                }
                when 'Partner - Wishlist'{ 
                    layouts = Metadata.Operations.retrieve(Metadata.MetadataType.Layout, new List<String> {'Opportunity-Gyms - Wishlist Partners'}); 
                }
                when 'Partner - Small and Medium Renegotiation'{                                                           
                    layouts = Metadata.Operations.retrieve(Metadata.MetadataType.Layout, new List<String> {'Opportunity-Gyms - Small/Medium Partners'}); 
                }
                when 'Partner - Wishlist Renegotiation'{ 
                    layouts = Metadata.Operations.retrieve(Metadata.MetadataType.Layout, new List<String> {'Opportunity-Gyms - Wishlist Partners'}); 
                }
                when else {
                    layouts = Metadata.Operations.retrieve(Metadata.MetadataType.Layout, new List<String> {'Opportunity-'+opp.RecordType.Name});
                }
            }          
        }
        else {         
            Account acc = [SELECT RecordType.Name FROM Account where Id =: recordId];  
            if(acc.RecordType.Name == 'Partner'){
                layouts = Metadata.Operations.retrieve(Metadata.MetadataType.Layout, new List<String> {'Account-Gym Partner'});
            }
        }

        Metadata.Layout layoutMd = (Metadata.Layout) layouts.get(0);
        
        for (Metadata.LayoutSection section : layoutMd.layoutSections) {
            for (Metadata.LayoutColumn column : section.layoutColumns) { 
                if (column.layoutItems != null) {
                    for (Integer i=0; i<column.layoutItems.size(); i++) {               
                        if(column.layoutItems[i].field != null && 
                            column.layoutItems[i].field != 'IsMultipleBrand__c' && 
                            column.layoutItems[i].field != 'HybridModel__c' && 
                            column.layoutItems[i].field != 'Cancellation_Details__c'&& 
                            column.layoutItems[i].field != 'Cancellation_Reason__c' && 
                            column.layoutItems[i].field != 'Loss_Reason__c' &&
                            column.layoutItems[i].field != 'Description' ) {
                                argument += column.layoutItems[i].field +  ',';
                                totalFields ++;                                    
                        }
                       
                    } 
                }
            }
        }
        totalFields +=1; //id
        argument = argument.substring(0, argument.length() - 1);       
        String dQuery = 'SELECT '+ argument + ' FROM ' + sObjectType +' WHERE ID =:  recordId';
        return Database.query(dQuery);
    }

    
}