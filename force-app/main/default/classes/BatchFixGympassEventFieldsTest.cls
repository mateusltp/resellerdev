/*
 * @author Bruno Pinho
 * @date January/2019
 * @description Test To BatchFixGympassEventFields
 */
@isTest(SeeAllData=true)
private class BatchFixGympassEventFieldsTest
{
    static void SetUp()
    {
        test.StartTest();
    }
    
    static void TearDown()
    {
        test.StopTest();
    }
    
    public static testmethod void test()
    {
        SetUp();
        System.assert(Database.executeBatch(new BatchFixGympassEventFields()) != null);
        TearDown();
    }
}