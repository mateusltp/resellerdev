public without sharing class SelfTerminationPublisher {
    private final String CANCEL_EVENT = 'CANCEL_SELF_TERMINATION_INBOUND';
    private final String UNDO_EVENT = 'UNDO_SELF_TERMINATION_INBOUND';
    private final String CANCEL_REQUESTED= 'Cancel Requested';
    private final String ACTIVATED = 'Activated';
    private Order orderRequested;
    private Account accountRequested;
    private SelfTerminationRequest terminationRequest;
    private String errorMessages;
  
    public SelfTerminationPublisher(SelfTerminationRequest terminationRequest) {
      this.terminationRequest = terminationRequest;
      setOrder();
    }
  
    public SelfTerminationPublisher(SelfTerminationRequest terminationRequest, String errorMessages) {
      this.terminationRequest = terminationRequest;
      this.errorMessages = errorMessages;
    }
  
    public EventQueue runCancel() {
      EventQueue event = publishEvent(CANCEL_EVENT);
      if( this.errorMessages == null){
        updateOrderOnTagus(event, CANCEL_EVENT);
      }
      event.save();
      return event;
    }
  
    public EventQueue runUndo() {
      EventQueue event = publishEvent(UNDO_EVENT);
      if( this.errorMessages == null){
        updateOrderOnTagus(event, UNDO_EVENT);
      }
      event.save();
      return event;
    }
  
    private EventQueue publishEvent(String eventName) {
      setAccount();

      EventQueue event = new EventBuilder()
        .createEventFor(eventName)
        .withReceiver('TAGUS')
        .withSender('SALESFORCE')
        .withBusinessDocumentNumber(this.accountRequested?.Name)
        .withBusinessDocumentCorrelatedNumber(String.valueOf(terminationRequest.getDueDate()))
        .buildEvent();
      if (orderRequested != null) {
        event.addPayload(eventName, JSON.serialize(orderRequested));
        event.setObjectId(orderRequested.Id);
        event.addPayload(
          'REQUEST_PAYLOAD_' + System.now(),
          JSON.serialize(terminationRequest)
        );    
    }
  
      if (errorMessages != null) {
        event.addPayload('REQUEST_PAYLOAD_' + System.now(), JSON.serialize(terminationRequest));
        event.addPayload(
          'RESPONSE_PAYLOAD_' + System.now(),
          JSON.serialize(errorMessages)
        );
        event.setStatus('ERROR');
      }
      return event;
    }
  
    private void setOrder() {
      List<Order> orders = [
        SELECT
          Id,
          Status,
          Cancelation_Date__c,
          Cancelation_Reason__c,
          Cancelation_Description__c,
          OpportunityId,
          AccountId,
          UUID__c,
          Account_External_Id__c,
          Gympass_Entity_External_Id__c,
          EffectiveDate,
          Payment_Due_Days__c,
          EndDate,
          Billing_Period__c,
          Billing_Day__c,
          Cutoff_Day__c,    
          CurrencyIsoCode,
          Is_Order_On_Tagus__c,
          Version__c
        FROM Order
        WHERE
          UUID__c = :this.terminationRequest.getClientOrderId()
          AND Account.UUID__c = :this.terminationRequest.getClientId()
        WITH SECURITY_ENFORCED
        LIMIT 1
      ];
      if (orders.size() > 0) {
        this.orderRequested = orders.get(0);
      }
    }

    private void setAccount() {
      List<Account> accounts = [
        SELECT
          Id, Name
        FROM Account
        WHERE
          UUID__c = :this.terminationRequest.getClientId()
        LIMIT 1
      ];

      if (accounts.size() > 0) {
        this.accountRequested = accounts.get(0);
      }
    }
  
    public void updateOrderOnTagus(EventQueue event, String eventName){
      if( eventName.contains(CANCEL_EVENT)){
          this.orderRequested.Status = CANCEL_REQUESTED;
          this.orderRequested.Cancelation_Date__c = this.terminationRequest.getDueDate();
          this.orderRequested.Cancelation_Reason__c = this.terminationRequest.getReason();
          this.orderRequested.Cancelation_Description__c = this.terminationRequest.getReasonDetails();
  
      }else if( eventName.contains(UNDO_EVENT) ){
          this.orderRequested.Status = ACTIVATED;
          this.orderRequested.Cancelation_Date__c = null;
          this.orderRequested.Cancelation_Reason__c = null;
          this.orderRequested.Cancelation_Description__c = '';
      }
  
      SelfTerminationPublisherHelper helper = new SelfTerminationPublisherHelper(event,this.orderRequested);
      helper.updateOrderOnTagus();
    }
  }