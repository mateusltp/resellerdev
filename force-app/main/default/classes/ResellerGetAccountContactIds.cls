public with sharing class ResellerGetAccountContactIds {
    
    @InvocableMethod(label='[RESELLER] Get Account-related Contacts')
    public static List<String> getAccountContacts(List<String> accountIds) {

        if(accountIds.isEmpty()) return new List<String>();

        List<Contact> contactsLst = [SELECT Id FROM Contact WHERE AccountId = :accountIds[0]];
        
        Set<String> contactIdsSet = new Set<String>();
        for(Contact c : contactsLst) {
            contactIdsSet.add(c.Id);
        }
        
        return new List<String>(contactIdsSet);
    }
}