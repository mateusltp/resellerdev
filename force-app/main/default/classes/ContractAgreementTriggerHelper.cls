/**
 * @description       : 
 * @author            : marcus.silva@gft.com
 * @group             : 
 * @last modified on  : 04-26-2022
 * @last modified by  : gft.samuel.silva@ext.gympass.com
 * Modifications Log 
 * Ver   Date         Author         			Modification
 * 1.0   07-05-2021   marcus.silva@gft.com   Initial Version
 * 2.0   24-05-2021   laerte.kimura@gft.com   Added createContractData method
 * 3.0   17-06-2021   marcus.silva@gft.com    Added setParentOpportunityAndProposal and init method
 * 4.0   16-07-2021   laerte.kimura@gft.com   Added setContractEffectiveDate method
**/

public with sharing class ContractAgreementTriggerHelper {   
    
    private static Id rtId = Schema.SObjectType.APXT_Redlining__Contract_Agreement__c.getRecordTypeInfosByDeveloperName().get('B2B_Contract').getRecordTypeId();

    public static void createContractData(List<APXT_Redlining__Contract_Agreement__c> lstContract){
        ContractDataCreator contractDataCreator = new ContractDataCreator(lstContract);
    }

    public static  void init(List<APXT_Redlining__Contract_Agreement__c> lstContract){
        setNumberOfRenegotiation(lstContract);
        setParentContractAgreement(lstContract);
        setParentOpportunityAndProposal(lstContract);
        setContractEffectiveDate(lstContract);
    }
    
    public static void setContractEffectiveDate(List<APXT_Redlining__Contract_Agreement__c> lstContract){
        /*Setting the effective date of creation of Contract Agreement to Today - Used by Legal */
        for (APXT_Redlining__Contract_Agreement__c contract : lstContract){
            contract.ContractEffectiveDate__c = Date.today(); /*removed for test about Conga sync issues -- rollback 24/09/2021 */ /* Added again with different name 01/12/2021  */
        } 
    }
    
    public static void setNumberOfRenegotiation(List<APXT_Redlining__Contract_Agreement__c> lstContract){
        Map<Id, Integer> mapAmendmentVersionByAccount;
        List<Id> lstAccount = new List <Id>();
        
        for (APXT_Redlining__Contract_Agreement__c contract : lstContract){
            if( contract.recordTypeId == rtId ){
                lstAccount.add(contract.APXT_Redlining__Account__c);
            }
        } 

        if(!lstAccount.isEmpty()){            
            mapAmendmentVersionByAccount = ContractAgreementTriggerHelper.getNumberOfRenegotiation(lstAccount);
            for (APXT_Redlining__Contract_Agreement__c contract : lstContract){
                for(Id accountId : mapAmendmentVersionByAccount.keySet()){
                    if (contract.APXT_Redlining__Account__c == accountId){
                        contract.APXT_Amendment_Version__c = (mapAmendmentVersionByAccount.get(accountId)) + 1;
                    }
                } 
            } 
        }

    }

    public static void setParentContractAgreement(List<APXT_Redlining__Contract_Agreement__c> lstContract){       
        List<APXT_Redlining__Contract_Agreement__c> proposalContract = getParentContractAgreement(lstContract);        

        if(!(proposalContract.isEmpty())){            
            for (APXT_Redlining__Contract_Agreement__c contract : lstContract){
                for(APXT_Redlining__Contract_Agreement__c contractPreviousProposal : proposalContract){
                    if (contract.APXT_Redlining__Account__c == contractPreviousProposal.APXT_Redlining__Account__c){
                        contract.APXT_Redlining__Contract_Agreement_Family_Parent__c = contractPreviousProposal.Id;
                    }
                }
            }
    	}
    }

    public static void setParentOpportunityAndProposal(List<APXT_Redlining__Contract_Agreement__c> lstContract){
        List<APXT_Redlining__Contract_Agreement__c> proposalContract = getParentContractAgreement(lstContract);        
        if(!(proposalContract.isEmpty())){
            for (APXT_Redlining__Contract_Agreement__c contract : lstContract){
                for(APXT_Redlining__Contract_Agreement__c contractPreviousProposal : proposalContract){
                    if (contract.APXT_Redlining__Account__c == contractPreviousProposal.APXT_Redlining__Account__c){
                    contract.Parent_Proposal__c = contractPreviousProposal.Proposal__c;
                    contract.Parent_Opportunity__c = contractPreviousProposal.Opportunity__c;
                    }
                }
            }
        }
    }

	public static String getAccLastClosedOppQuery(){
        return 'SELECT Id,' + 
            ' ( SELECT ' + Utils.getSobjectFieldsDevNameForQuery('Opportunity') + 
            '   FROM Opportunities WHERE isClosed = true AND StageName = \'Lançado/Ganho\' AND ' + 
            ' (Recordtype.DeveloperName = \'Client_Sales_New_Business\' OR Recordtype.DeveloperName = \'Client_Success_Renegotiation\' OR Recordtype.DeveloperName = \'SMB_New_Business\' OR Recordtype.DeveloperName = \'SMB_Success_Renegotiation\') ORDER BY CreatedDate DESC LIMIT 1 ) ' +
            'FROM Account ' +
            'WHERE Id =: aSetAccId';
    }
    
    public static String getNumberOfRenegotiationQuery(){
        return 'SELECT Id, ' +
         ' (SELECT Id FROM Opportunities WHERE isClosed = true AND StageName = \'Lançado/Ganho\' AND Recordtype.DeveloperName = \'Client_Success_Renegotiation\')' +
         ' FROM Account' +
         ' WHERE Id =: aSetAccId';
    }
    
    public static List< Opportunity > getLastClosedOppByAcc( List<Id> aLstAccId ){       
        List< Opportunity > lLstLastClosedOpp = new List< Opportunity >();        
        String straLstAccId = String.join(aLstAccId,',');
        straLstAccId = String.escapeSingleQuotes(straLstAccId);
        List<String> aSetAccId = new List<String>(straLstAccId.split(','));
        for( Account iAcc : Database.query( getAccLastClosedOppQuery() ) ){
            if( !(iAcc.Opportunities.isEmpty()) ) {                
                lLstLastClosedOpp.add(iAcc.Opportunities);                
            }                        
        }        
        return lLstLastClosedOpp;
    }

    public static Map<Id, Integer> getNumberOfRenegotiation(List<Id> aLstAccId){    
        Map <Id, Integer> mapAccountAmendmentVersion = new Map <Id, Integer>();        
        String straLstAccId = String.join(aLstAccId,',');
        straLstAccId = String.escapeSingleQuotes(straLstAccId);
        List<String> aSetAccId = new List<String>(straLstAccId.split(','));
         for( Account iAcc : Database.query( getNumberOfRenegotiationQuery() ) ){
            if( !(iAcc.Opportunities.isEmpty()) ) {                
                mapAccountAmendmentVersion.put(iAcc.Id, iAcc.Opportunities.size());
            }else{
                mapAccountAmendmentVersion.put(iAcc.Id, 0);
            }
        } 
        return mapAccountAmendmentVersion;        
    }

    public static List<APXT_Redlining__Contract_Agreement__c> getParentContractAgreement(List<APXT_Redlining__Contract_Agreement__c> lstContract){
        List<Id> lstAccount = new List <Id>();
        List<Opportunity> lastClosedOppWon = new List<Opportunity>();
        List<Id> proposal = new List<Id>();
        List<APXT_Redlining__Contract_Agreement__c> parentContractAgreement = new List<APXT_Redlining__Contract_Agreement__c>();                
        
        for (APXT_Redlining__Contract_Agreement__c contract : lstContract){
            if( contract.recordTypeId == rtId ){
                lstAccount.add(contract.APXT_Redlining__Account__c);
            }
           
        }
        if(!lstAccount.isEmpty()){
            lastClosedOppWon = getLastClosedOppByAcc(lstAccount);    

            for (Opportunity opp : lastClosedOppWon) {
                proposal.add(opp.SyncedQuoteId);
            }        
            parentContractAgreement = [SELECT Id, proposal__c, opportunity__c, APXT_Redlining__Account__c FROM APXT_Redlining__Contract_Agreement__c WHERE APXT_Redlining__Status__c = 'Active' AND Proposal__c =: proposal];        
          
        }

        return parentContractAgreement;
       
    }
}