/**
 * @description       : 
 * @author            : Samuel Silva - GFT (slml@gft.com)
 * @group             : 
 * @last modified on  : 11-20-2020
 * @last modified by  : Samuel Silva - GFT (slml@gft.com)
 * Modifications Log 
 * Ver   Date         Author                              Modification
 * 1.0   11-16-2020   Samuel Silva - GFT (slml@gft.com)   Initial Version
**/
public class SubmitGreenLightForApprovalController {
	@auraEnabled
    public static void submitFormForApproval(String formId, String comments) {
        List<Id> userIds = new List<Id>();
        FormRepository formRepo = new FormRepository();
        Form__c ejForm = formRepo.byId(formId);
        userIds.add(ejForm.Country_Manager__c);
        try{
            if(ejForm.Approval_Status__c != 'Auto Approved'){
                validateEnablersFields(ejForm);
            }          
            Approval.ProcessSubmitRequest ejFormApproval = new Approval.ProcessSubmitRequest();
            ejFormApproval.setObjectId(ejForm.Id);
            ejFormApproval.setNextApproverIds(userIds);
            ejFormApproval.setComments(comments);
            ejFormApproval.setSubmitterId(UserInfo.getUserId());
            ejFormApproval.setProcessDefinitionNameOrId('EJ_M0_and_M1');
            Approval.ProcessResult result = Approval.process(ejFormApproval);
        } catch (Exception e){
            throw new AuraException(e.getMessage());
        }
    }
    
    private static void validateEnablersFields(Form__c ejForm){
        //1
        if(ejForm.Standard_Membership_Fee_GL__c == 'No' &&
            String.isBlank(ejForm.Standard_Membership_Fee_GL_justify__c)){
                throw new CustomException('Please enter (1) Standard Membership Fee justification before submitting to approval. All fields with "No" must have a justification.');
        }
        //2
        if(ejForm.Expansion_100_of_the_eligible_GL__c == 'No' && 
            String.isBlank(ejForm.Expansion_100_of_the_eligible_GL_justify__c)) {
                throw new CustomException('Please enter (2) Full launch|Expansion justification before submitting to approval. All fields with "No" must have a justification.');
        }

        //3
        if(ejForm.Minimum_Access_Fee_GL__c == 'No' && 
            String.isBlank(ejForm.Minimum_Access_Fee_GL_justify__c)){
                throw new CustomException('Please enter (3) Minimum Access Fee justification before submitting to approval. All fields with "No" must have a justification.');
        }

        //4
        if(ejForm.Engagement_with_C_Level_GL__c == 'No' &&  
            String.isBlank(ejForm.Engagement_with_C_Level_GL_justify__c)){
                throw new CustomException('Please enter (4) Engagement with C-Level justification before submitting to approval. All fields with "No" must have a justification.');
        }

        //5
        if(ejForm.Payroll_GL__c == 'No' && 
            String.isBlank(ejForm.Payroll_GL_justify__c)) {
                throw new CustomException('Please enter (5) Payroll justification before submitting to approval. All fields with "No" must have a justification.');
        }

        //6
        if(ejForm.Database_Email_GL__c == 'No' && 
            String.isBlank(ejForm.Database_Email_GL_justify__c)) {
                throw new CustomException('Please enter (6) Database|Email justification before submitting to approval. All fields with "No" must have a justification.');
        }

        //7
        if(ejForm.Whitelist_GL__c == 'No' && 
            String.isBlank(ejForm.Whitelist_GL_justify__c)) {
                throw new CustomException('Please enter (7) Allowlist justification before submitting to approval. All fields with "No" must have a justification.');
        }

        //8
        if(ejForm.New_Hires_GL__c == 'No' && 
            String.isBlank(ejForm.New_Hires_GL_justify__c)) {
                throw new CustomException('Please enter (8) New Hires justification before submitting to approval. All fields with "No" must have a justification.');
        }

        //9
        if(ejForm.HR_Communication_GL__c == 'No' && 
            String.isBlank(ejForm.HR_Communication_GL_justify__c)){
                throw new CustomException('Please enter (9) HR Communication justification before submitting to approval. All fields with "No" must have a justification.');
        }

        //10
        if(ejForm.Exclusivity_clause_GL__c == 'No' && 
            String.isBlank(ejForm.Exclusivity_clause_GL_justify__c)){
                throw new CustomException('Please enter (10) Exclusivity clause justification before submitting to approval. All fields with "No" must have a justification.');
        }

        //11
        if(ejForm.PR_in_contract_GL__c == 'No' && 
            String.isBlank(ejForm.PR_in_contract_GL_justify__c)){
                throw new CustomException('Please enter (11) PR in contract justification before submitting to approval. All fields with "No" must have a justification.');
        }

        //12
        if(ejForm.What_does_success_looks_like_Joint_BP_GL__c == 'No' &&
            String.isBlank(ejForm.What_does_success_looks_like_GL_justify__c)){
                throw new CustomException('Please enter (12) Joint Business Plan justification before submitting to approval. All fields with "No" must have a justification.');
        }

    }

    private class CustomException extends Exception {}

}