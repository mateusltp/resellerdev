/**
* @author vncferraz
*
*   Generate records to provide information to business which contacts will receive which survey on getFeedback
*/
global class BatchGFProspectingSalesMonitor implements Database.Batchable<sObject> {
    
    global Integer daysToAdd;

    global BatchGFProspectingSalesMonitor(Integer daysToAdd){
        this.daysToAdd = daysToAdd;
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        Date ed = (Date.today().addDays(this.daysToAdd)).addDays(-1);
        return Database.getQueryLocator([
            SELECT Id, Data_do_Lancamento__c, AccountId, Account.RecordTypeId, RecordTypeId, StageName, Opportunity_Lost_Date__c, CloseDate
            FROM Opportunity
            WHERE Data_do_Lancamento__c =: ed OR Opportunity_Lost_Date__c =: ed OR CloseDate =: ed
        ]);
    }
    
    global void execute(Database.BatchableContext bc, List<Opportunity> scope) {
        
        List<Account> lstAccNew = new List<Account>();
        List<Account> lstAccReneg = new List<Account>();
        List<Account> lstAccLost = new List<Account>();
        List<Get_Feedback_Survey_Schedule__c> schedules = new List<Get_Feedback_Survey_Schedule__c>();
        List<Contact> lstUpdateConNew = new List<Contact>();
        List<Contact> lstUpdateConReneg = new List<Contact>();
        
        Id rtOppClientNew = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        Id rtOppSMBNew = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SMB_New_Business').getRecordTypeId();
        Id rtOppClientReneg = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Success_Renegotiation').getRecordTypeId();
        Id rtOppSMBReneg = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SMB_Success_Renegotiation').getRecordTypeId(); 
        
        Id rtAcc = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
		
		        
        for(Opportunity o: scope){
            if((o.RecordTypeId == rtOppClientNew || o.RecordTypeId == rtOppSMBNew) && o.Data_do_Lancamento__c == Date.today().addDays(-1) && o.StageName == 'Lançado/Ganho' && o.Account.RecordTypeId == rtAcc){                      
                lstAccNew.add(o.Account);
               }
            if((o.RecordTypeId == rtOppClientReneg || o.RecordTypeId == rtOppSMBReneg) && o.CloseDate == Date.today().addDays(-1) && o.StageName == 'Lançado/Ganho' && o.Account.RecordTypeId == rtAcc){                                  
                lstAccReneg.add(o.Account);
               }
            if((o.RecordTypeId == rtOppClientNew || o.RecordTypeId == rtOppSMBNew) && o.Opportunity_Lost_Date__c == Date.today().addDays(-1) && o.StageName == 'Perdido' && o.Account.RecordTypeId == rtAcc){                              
                lstAccLost.add(o.Account);
               }

        }
        if(!lstAccNew.isEmpty()){
            for(Contact con: [Select Id, Status_do_contato__c, GFSendSurvey__c, Email, Primary_HR_Contact__c,DecisionMaker__c, Role__c FROM Contact WHERE AccountID IN :lstAccNew]){
                if(con.Status_do_contato__c == 'Ativo' && con.Email != null && con.Primary_HR_Contact__c == TRUE){
                    schedules.add( 
                        new Get_Feedback_Survey_Schedule__c(Contact__c=con.Id,
                                                             Survey_Name__c= 'Client Sales Won',
                                                             Scheduled_Date__c=Date.today().addDays(this.daysToAdd))
                    ); 
                   
                }
            }
        }
        
        if(!lstAccReneg.isEmpty()){
            for(Contact con: [Select Id, Status_do_contato__c, GFSendSurvey__c, Email, Primary_HR_Contact__c,DecisionMaker__c, Role__c FROM Contact WHERE AccountID IN :lstAccReneg]){
                if(con.Status_do_contato__c == 'Ativo' && con.Email != null && con.Primary_HR_Contact__c == TRUE){
                    schedules.add( 
                        new Get_Feedback_Survey_Schedule__c(Contact__c=con.Id,
                                                             Survey_Name__c= 'Client Sales PostRenewal',
                                                             Scheduled_Date__c=Date.today().addDays(this.daysToAdd))
                    ); 
                   
                }
            }
        }
        
        if(!lstAccLost.isEmpty()){
            for(Contact con: [Select Id, Status_do_contato__c, GFSendSurvey__c, Email, Primary_HR_Contact__c,DecisionMaker__c, Role__c FROM Contact WHERE AccountID IN :lstAccLost]){
                if(con.Status_do_contato__c == 'Ativo' && con.Email != null && con.Primary_HR_Contact__c == TRUE){
                    schedules.add( 
                        new Get_Feedback_Survey_Schedule__c(Contact__c=con.Id,
                                                             Survey_Name__c= 'Client Sales Lost',
                                                             Scheduled_Date__c=Date.today().addDays(this.daysToAdd))
                    );
                    
                }
            }
        }
        
        
        if(!schedules.isEmpty()){
            insert schedules;
        }
    }
    
    global void finish(Database.BatchableContext bc) {
        
    }
}