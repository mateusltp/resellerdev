@IsTest
public class QuoteMock {
  public static Quote getStandard(Opportunity opportunity) {
    return new Quote(
      Name = opportunity.Id,
      OpportunityId = opportunity.Id,
      Contact_Permission__c = 'Allowlist',
      Employee_Registration_Method__c = 'Eligible file',
      End_Date__c = Date.today().addMonths(1),
      License_Fee_Waiver__c = 'No',
      Start_Date__c = Date.today(),
      Unique_Identifier__c = 'Corporate E-mail'
    );
  }
}