/*
* @author Bruno Pinho
* @date December/2018
* @description Event trigger helper to promote code reusability
*/

public with sharing class EventTriggerHelper
{
    public static void deleteRelatedGympassEvents(List<Event> listEvents)
    {
        System.debug('@listEvents: '+listEvents);
        
        List<Gympass_Event__c> listGympassEventsToDelete = new List<Gympass_Event__c>();
        
        if (listEvents.size() > 0)
        {
            for (Event evt : listEvents)
            {
                if (evt.Gympass_Event__c != null)
                    listGympassEventsToDelete.add(new Gympass_Event__c(Id = evt.Gympass_Event__c));
            }
        }
        
        if (listGympassEventsToDelete.size() > 0)
            delete listGympassEventsToDelete;
    }
    
}