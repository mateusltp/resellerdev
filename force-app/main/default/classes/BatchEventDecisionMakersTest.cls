/*
 * @author Bruno Pinho
 * @date January/2019
 * @description Test To BatchEventDecisionMakers
 */
@isTest(SeeAllData=true)
private class BatchEventDecisionMakersTest
{
    static void SetUp()
    {
        test.StartTest();
    }
    
    static void TearDown()
    {
        test.StopTest();
    }
    
    public static testmethod void test()
    {
        SetUp();
        System.assert(Database.executeBatch(new BatchEventDecisionMakers()) != null);
        TearDown();
    }
}