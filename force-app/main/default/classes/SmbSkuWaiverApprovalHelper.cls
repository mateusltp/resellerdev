public without sharing class SmbSkuWaiverApprovalHelper {

    private Map< String , String > gMapOppIdQuoteId = new Map< String , String >();
    private Set< String > gSetRecordtypeName = new Set< String >{
        'SMB_SKU_New_Business',
        'SMB_Success_SKU_Renegotiation'
    };
    @TestVisible
    private Map< String , Discount_and_Waiver_approval_parameters__mdt > gMapOptionApprovalMetadata;
    
    public Boolean checkApprovalNeed( List< Waiver__c > aLstWaiver, List< Waiver__c > waiversToDelete ){

        Map< String , FeeWaiver > lMapQuoteIdFeeWaiver = getMapQuoteMaxWaiverMonths( aLstWaiver, waiversToDelete );
        if( lMapQuoteIdFeeWaiver.isEmpty() ){ return false; }

        Map< String , Integer > lMapQuoteIdWaiverMonthsApproved = getApprovedWaiverMonths();

        getApprovalParametersMdt();

        Boolean allWaiversApproved = true;

        for( String iQuoteId : lMapQuoteIdFeeWaiver.keySet() ){
            FeeWaiver lFeeWaiver = lMapQuoteIdFeeWaiver.get( iQuoteId );
            Integer lApprovedWaiverMonths = lMapQuoteIdWaiverMonthsApproved.get( iQuoteId );

            Integer lWaiverApprovalLevelNeed = getWaiverApproverLevelNeeded( lFeeWaiver.gMaxWaiverMonths );
            Boolean lIsWaiverApproved = ( lWaiverApprovalLevelNeed == 0 ||
                    ( lApprovedWaiverMonths != null && lFeeWaiver.gMaxWaiverMonths <= lApprovedWaiverMonths ) );
            
            allWaiversApproved &= lIsWaiverApproved;
    
        }

        return !allWaiversApproved;

    }

    public Map< String , FeeWaiver > getMapQuoteMaxWaiverMonths( List< Waiver__c > aLstWaiver, List< Waiver__c > waiversToDelete ){
        Map< String , List< Waiver__c > > lMapItemIdLstWaiver = new Map< String , List< Waiver__c > >();
        Map< String , List< QuoteLineItem > > lMapQuoteIdLstLineItem = new Map< String , List< QuoteLineItem > >();
        Map< String , FeeWaiver > lMapQuoteIdFeeWaiver = new Map< String , FeeWaiver >();
        Set< String > lSetQuoteId = new Set< String >();

        for( Waiver__c iWaiver : aLstWaiver ){ 
            String quoteId;
            
            if(iWaiver.QuoteId__c != null && iWaiver.QuoteId__c.length() > 15) {
                quoteId = String.valueOf(iWaiver.QuoteId__c).substring(0, 15);
            }
            else {
                quoteId = iWaiver.QuoteId__c;       
            }
            lSetQuoteId.add( quoteId ); 
            if( lMapItemIdLstWaiver.get( iWaiver.Quote_Line_Item__c ) == null ){
                lMapItemIdLstWaiver.put( iWaiver.Quote_Line_Item__c , new List< Waiver__c >{ iWaiver } );
            } else {
                lMapItemIdLstWaiver.get( iWaiver.Quote_Line_Item__c ).add( iWaiver );
            }
        }

        for( Waiver__c iWaiver : [ SELECT Id, Quote_Line_Item__c, Duration__c 
                                    FROM Waiver__c 
                                    WHERE QuoteId__c =: lSetQuoteId AND Id NOT IN :aLstWaiver AND Id NOT IN :waiversToDelete] ){

            if( lMapItemIdLstWaiver.get( iWaiver.Quote_Line_Item__c ) == null ){
                lMapItemIdLstWaiver.put( iWaiver.Quote_Line_Item__c , new List< Waiver__c >{ iWaiver } );
            } else {
                lMapItemIdLstWaiver.get( iWaiver.Quote_Line_Item__c ).add( iWaiver );
            }
        }        
        
        for( QuoteLineItem iQuoteLineItem : [ SELECT Id, QuoteId, Quote.OpportunityId, Product_Name__c FROM QuoteLineItem 
                WHERE QuoteId =: lSetQuoteId AND Quote.RecordType.DeveloperName =: gSetRecordtypeName ] ){
            if( lMapQuoteIdLstLineItem.get( iQuoteLineItem.QuoteId ) == null ){
                lMapQuoteIdLstLineItem.put( iQuoteLineItem.QuoteId , new List< QuoteLineItem >{ iQuoteLineItem } );
            } else {
                lMapQuoteIdLstLineItem.get( iQuoteLineItem.QuoteId ).add( iQuoteLineItem );
            }
        }

        for( String iQuoteId : lMapQuoteIdLstLineItem.keySet() ){
            Decimal lQuoteMaxWaiverMonths = 0;
            String lOppId;
            for( QuoteLineItem iQuoteLineItem : lMapQuoteIdLstLineItem.get( iQuoteId ) ){
                lOppId = iQuoteLineItem?.Quote?.OpportunityId;

                Decimal lLineItemMaxWaiverMonths = 0;

                List< Waiver__c > lLstQuoteWaivers = lMapItemIdLstWaiver.get( iQuoteLineItem.Id );
                if( lLstQuoteWaivers == null || lLstQuoteWaivers.isEmpty() ){ continue; }

                for( Waiver__c iWaiver : lLstQuoteWaivers ){
                    lLineItemMaxWaiverMonths += ( iWaiver.Duration__c == null ? 0 : iWaiver.Duration__c );
                }

                if( lLineItemMaxWaiverMonths > lQuoteMaxWaiverMonths ){
                    lQuoteMaxWaiverMonths = lLineItemMaxWaiverMonths;
                }
            }

            gMapOppIdQuoteId.put( lOppId , iQuoteId );
            lMapQuoteIdFeeWaiver.put( iQuoteId , new FeeWaiver( lQuoteMaxWaiverMonths , lOppId ) );
        }

        return lMapQuoteIdFeeWaiver;
    }

    public Map< String , Integer > getApprovedWaiverMonths(){
        Map< String , Integer > lMapQuoteIdWaiverMonthsApproved = new Map< String , Integer >();

        for( Assert_Data__c iAssertData : 
            [ SELECT Old_Value__c, Opportunity__c 
                FROM Assert_Data__c
                WHERE Opportunity__c =: gMapOppIdQuoteId.keySet() AND Object_Name__c = 'quote' AND Field_Name__c = 'waiver_max_number_of_months__c' ]){
            lMapQuoteIdWaiverMonthsApproved.put( gMapOppIdQuoteId.get( iAssertData.Opportunity__c ) , ( String.isBlank( iAssertData.Old_Value__c ) ? 0 : Integer.valueOf( iAssertData.Old_Value__c ) ) );
        }

        return lMapQuoteIdWaiverMonthsApproved;
    }

    private Integer getWaiverApproverLevelNeeded( Decimal waiverMonths ){
        Discount_and_Waiver_approval_parameters__mdt approvalMeta = gMapOptionApprovalMetadata.get('Waiver');

        if( waiverMonths == null || waiverMonths == 0 || approvalMeta == null ){ return 0; }
        
        Integer approvalLevel = 0;

        if( waiverMonths >= approvalMeta.Waiver_RM_1__c && waiverMonths < approvalMeta.Waiver_RM__c ){
            approvalLevel = 1;
        } else if( waiverMonths >= approvalMeta.Waiver_RM__c  && waiverMonths < approvalMeta.Waiver_CCO__c ){
            approvalLevel = 2;
        } else if( waiverMonths >= approvalMeta.Waiver_CCO__c ){
            approvalLevel = 3;
        }

        return approvalLevel;
    }

    private void getApprovalParametersMdt(){
        Map<String , Discount_and_Waiver_approval_parameters__mdt> lMapOptionApprovalMetadata 
            = new Map<String , Discount_and_Waiver_approval_parameters__mdt>();

        for( Discount_and_Waiver_approval_parameters__mdt iApprovalMeta : 
            [ SELECT Id, Waiver_CCO__c, Waiver_RM__c, CCO__c, CF__c, Initial_Approver_Email__c, Option__c,
                    RM__c, RM_1__c, RM_2__c, Sellers__c, Waiver_RM_1__c, Initial_Approver_Queue_Name__c, 
                    Manager__c, VP__c, Waiver_VP__c
            FROM Discount_and_Waiver_approval_parameters__mdt WHERE Country__c = 'United States'
            AND Label LIKE :'Clients' + '%' 
            AND Option__c = 'Waiver' ] ){
            lMapOptionApprovalMetadata.put( iApprovalMeta.Option__c , iApprovalMeta );
        }

        if(!Test.isRunningTest()) gMapOptionApprovalMetadata = lMapOptionApprovalMetadata;
    }

    public class FeeWaiver{
        public Decimal gMaxWaiverMonths;
        public String gOppId;

        public FeeWaiver( Decimal aMaxWaiverMonths , String aOppId ){
            this.gMaxWaiverMonths = aMaxWaiverMonths;
            this.gOppId = aOppId;
        }
    }
}