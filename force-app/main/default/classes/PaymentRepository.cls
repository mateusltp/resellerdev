/**
 * @description       : 
 * @author            : AlyssonMota
 * @group             : 
 * @last modified on  : 05-11-2022
 * @last modified by  : tiago.ribeiro@gympass.com
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   09-11-2020   Alysson Mota   Initial Version
**/
public with sharing class PaymentRepository {

    
    public List<Payment__c> getPaymentObjList(List<FastTrackProposalCreationTO.Payment> payments){
        
        List<Payment__c> payList = new List<Payment__c>();
        for(FastTrackProposalCreationTO.Payment iPay : payments){
            payList.add(iPay.sObj);
        }
        return payList;
        
    }

    public List<Payment__c> getPaymentsForQuote(Id quoteId){
        return [SELECT Id,Inflation_Adjustment_Index__c,Percentage__c, Frequency__c,Inflation_Adjustment_Period__c,Non_Recurring_Billing_Date__c,
                        Payment_Due_Days__c,RecordTypeId, Billing_Day__c,Custom_Billing_Day__c,Custom_Payment_Due_Days__c, Quote_Line_Item__c,Has_Relevant_Changes_for_Offer_Approval__c,Has_Relevant_Changes_for_Offer_Deal_Desk__c, Payment_Method__c,Recurring_Cutoff_Day__c,Cutoff_Day__c,Recurring_Billing_Period__c,Billing_Due_Date__c,Account__c
                    FROM Payment__c 
                    WHERE Quote_Line_Item__r.QuoteId = :quoteId];
    }

    public  Map<Id, List<Payment__c>> getPaymentsForQuotes ( Set<Id> lProposalIdLst ){
        Map<Id, List<Payment__c>> lQuotePaymentMap = new Map<Id, List<Payment__c>>();
        for(Payment__c iPayment : [SELECT Id,Payment_Code__c, Quote_Line_Item__r.QuoteId, Inflation_Adjustment_Index__c,Percentage__c, Frequency__c,Inflation_Adjustment_Period__c,Non_Recurring_Billing_Date__c,
                                    Payment_Due_Days__c,RecordTypeId, Billing_Day__c,Custom_Billing_Day__c,Custom_Payment_Due_Days__c, Quote_Line_Item__c,Has_Relevant_Changes_for_Offer_Approval__c,Has_Relevant_Changes_for_Offer_Deal_Desk__c, Payment_Method__c,Recurring_Cutoff_Day__c,Cutoff_Day__c,Recurring_Billing_Period__c,Billing_Due_Date__c,Account__c
                                    FROM Payment__c 
                                    WHERE Quote_Line_Item__r.QuoteId IN :lProposalIdLst]){                                    
                                        
            if (lQuotePaymentMap.containsKey(iPayment.Quote_Line_Item__r.QuoteId)) {
                lQuotePaymentMap.get(iPayment.Quote_Line_Item__r.QuoteId).add(iPayment);
            } else {
                lQuotePaymentMap.put(iPayment.Quote_Line_Item__r.QuoteId, new List<Payment__c>{iPayment});
            }
        }
        return lQuotePaymentMap;
    }
    
    public List<Payment__c> getPaymentsForOpp(Id oppId){
        return [SELECT Id,Inflation_Adjustment_Index__c,Percentage__c, Frequency__c,Inflation_Adjustment_Period__c,Non_Recurring_Billing_Date__c,
                        Payment_Due_Days__c,RecordTypeId, Billing_Day__c,Custom_Billing_Day__c,Custom_Payment_Due_Days__c, Quote_Line_Item__c,Has_Relevant_Changes_for_Offer_Approval__c,Has_Relevant_Changes_for_Offer_Deal_Desk__c, Payment_Method__c,Recurring_Cutoff_Day__c,Cutoff_Day__c,Recurring_Billing_Period__c,Billing_Due_Date__c,Account__c
                    FROM Payment__c 
                    WHERE Opportunity__r.Id = :oppId];
    }
    
    public Map<Id, Payment__c> getAccPaymentsMapForOpp(Id oppId, Id RecordType){
        List<Payment__c> paymentsList = this.getPaymentsForOpp(oppId);
        System.debug('paymentsList ' + paymentsList);   
        Map<Id, Payment__c> paymentsAccMap = new Map<Id, Payment__c>();

        if(paymentsList.size()>0){
            for(Payment__c pay : paymentsList){
                if(RecordType == pay.RecordTypeId && pay.Account__c!=null){
                    paymentsAccMap.put(pay.Account__c, pay);
                 }
            }
        }
        return paymentsAccMap;
    }

    public List<Payment__c> getListPaymentsBySetOpps(Set<Id> setOppIds){
        return [SELECT Id, RecordTypeId, Opportunity__c, Account__c, Payment_Method__c, PO_Required__c FROM Payment__c WHERE Opportunity__c IN :setOppIds];
    }
    
    
    public List<Payment__c>  edit(List<Payment__c> payments ){
        Database.update(payments); 
        return payments;
    }

    
    public ID getRecordTypeIdByDeveloperName (String devName){      
        return Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get(devName).getRecordTypeId();
    }

}