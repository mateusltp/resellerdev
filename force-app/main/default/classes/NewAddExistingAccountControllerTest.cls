/**
 * @description       : 
 * @author            : bruno.mendes@gympass.com
 * @group             : 
 * @last modified on  : 26/04/2022
 * @last modified by  : bruno.mendes@gympass.com
**/

@IsTest
private class NewAddExistingAccountControllerTest {

    @TestSetup
    static void setup() {
        Account lAcc = PartnerDataFactory.newAccount();
        lAcc.Name = 'Test';
        Database.insert(lAcc);
    }

    @IsTest
    static void search_Test() {
        Account account = [SELECT Id FROM Account LIMIT 1];
        Test.setFixedSearchResults(new List<String>{account.Id});
        Test.startTest();
        List<LookupSearchResult> results = NewAddExistingAccountController.search('Test', new List<String>());
        system.assertEquals(1, results.size());
        Test.stopTest();
    }

    @IsTest
    static void getRecentlyViewed_Test() {
        Test.startTest();
        Account account = [SELECT Id FROM Account FOR VIEW ];
        List<LookupSearchResult> results = NewAddExistingAccountController.getRecentlyViewed();
        system.debug('results: '+results);
        Test.stopTest();
        system.assertNotEquals(null, results.size());
    }
}