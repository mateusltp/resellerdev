/**
 * @File Name          : AvoidTwoProductInOpp.cls
 * @Description        :
 * @Author             : POHR@GFT.com
 * @Group              :
 * @Last Modified By   : alysson.mota@gympass.com
 * @Last Modified On   : 05-17-2022
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    08/07/2021   POHR@GFT.com     Initial Version
 **/


public with sharing class ProductItemHelper {

    public void avoidDuplicatedProducts(){
        List<Product_Item__c> lstProd = trigger.new;
        Set<Id> stNewProd = new Set<id>();
        Map<Id, List<String>> mapOppProdName = new Map<Id, List<String>>();
        Id gymsProdRecTypeId = Schema.SObjectType.Product_Item__c.getRecordTypeInfosByDeveloperName().get('Gyms_Product_Item_Partner').getRecordTypeId();

        for(Product_Item__c iProd : lstProd){
            if (iProd.RecordTypeId == gymsProdRecTypeId) {
                if( mapOppProdName.containsKey(iProd.Opportunity__c )){
                    if(mapOppProdName.get(iProd.Opportunity__c).contains(iProd.Name.toLowerCase())){
                        (new DuplicateProductError()).doAction('Duplicated product found. Please create a different product.');
                    } else {
                        mapOppProdName.get(iProd.Opportunity__c ).add(iProd.Name.toLowerCase());
                    }
                } else {
                    mapOppProdName.put(iProd.Opportunity__c, new List<String>{iProd.Name.toLowerCase()});
                }
                stNewProd.add(iProd.Id);
            }
        }

        for(Opportunity opp : [Select Id, (Select Name, RecordTypeId FROM Product__r WHERE Id NOT IN: stNewProd) FROM Opportunity WHERE Id IN: mapOppProdName.keySet()]){
            for (Product_Item__c iProd : opp.Product__r){
                if (iProd.RecordTypeId == gymsProdRecTypeId) {
                    if(mapOppProdName.get(opp.Id).contains(iProd.Name.toLowerCase())){
                        (new DuplicateProductError()).doAction('Duplicated product found. Please create a different product.');
                    }
                }
            }
        }
    }

    public void updateChildProducts() {
        fflib_ISObjectUnitOfWork uow = Application.UnitOfWork.newInstance();
        
        Id partnerFlowRecTypeId = Schema.SObjectType.Product_Item__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product').getRecordTypeId();
        
        Map<Id, Product_Item__c> newMap = (Map<Id, Product_Item__c>)trigger.newMap;       
        Map<Id, Product_Item__c> oldMap = (Map<Id, Product_Item__c>)trigger.oldMap;
        
        List<Id> parentProdIdLst            = new List<Id>();
        List<Product_Item__c> childProds    = new List<Product_Item__c>(); 
        
        ProductItemSelector selector = new ProductItemSelector();

        for (Product_Item__c newProd : newMap.values()) {
            Product_Item__c oldProd = oldMap.get(newProd.Id);

            if (newProd.RecordTypeId == partnerFlowRecTypeId && (
                    newProd.No_Show_Fee_Percent__c          != oldProd.No_Show_Fee_Percent__c       ||
                    newProd.Late_Cancellation_Percent__c    != oldProd.Late_Cancellation_Percent__c ||
                    newProd.Name                            != oldProd.Name
            )){
                parentProdIdLst.add(newProd.Id);
            }
        }

        if (parentProdIdLst.size() > 0) {
            childProds = selector.selectProductByParentID(new Set<Id>(parentProdIdLst));
        }

        for (Product_Item__c childProd : childProds) {
            Product_Item__c parentProduct = newMap.get(childProd.Parent_Product__c);
            childProd.No_Show_Fee_Percent__c        = parentProduct.No_Show_Fee_Percent__c;
            childProd.Late_Cancellation_Percent__c  = parentProduct.Late_Cancellation_Percent__c;
            childProd.Name                          = parentProduct.Name;
        }

        uow.registerDirty(childProds);
        uow.commitWork();
    }

    private class DuplicateProductError{
        public DuplicateProductError() {}
        public void doAction(String msg) {
            // Trigger.new is valid here
            SObject[] sobjects = Trigger.new;
            for (Sobject sobj : sobjects) {
                sobj.addError(msg);
            }
        }
    }
}