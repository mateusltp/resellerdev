public without sharing class WaiverDTO {
  private String id;
  private String salesforce_id;
  private Datetime begin_date;
  private Datetime end_date;
  private Decimal waived_percentage;

  public WaiverDTO(Waiver__c waiver) {
    this.id = waiver.UUID__c != null ? waiver.UUID__c : new Uuid().getValue();
    this.salesforce_id = waiver.Id;
    this.begin_date = (Datetime) waiver.Start_Date__c;
    this.end_date = (Datetime) waiver.End_Date__c;
    this.waived_percentage = waiver.Percentage__c;
  }

  public String getId() {
    return this.id;
  }

  public String getSalesforceId() {
    return this.salesforce_id;
  }

  public Waiver__c parseToSWaiver() {
    return new Waiver__c(
      UUID__c = this.id,
      Start_Date__c = Date.newInstance(
        this.begin_date?.year(),
        this.begin_date?.month(),
        this.begin_date?.day()
      ),
      End_Date__c = Date.newInstance(
        this.end_date?.year(),
        this.end_date?.month(),
        this.end_date?.day()
      ),
      Percentage__c = this.waived_percentage
    );
  }
}