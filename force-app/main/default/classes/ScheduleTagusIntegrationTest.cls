@isTest
public class ScheduleTagusIntegrationTest {
	public static String CRON_EXP = '0 0 13 * * ?';
    
    @isTest
    public static void functionalTest() {
    	Test.startTest();
        String jobId = System.schedule('ScheduledApexTest',
            CRON_EXP, 
            new ScheduleTagusIntegration());         
        Test.stopTest();
    }
}