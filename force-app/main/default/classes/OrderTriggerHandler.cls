/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 10-13-2021
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/
public without sharing class OrderTriggerHandler extends TriggerHandler {
  override public void afterUpdate() {
    new PublishOrderOnTagus().execute();
  }

  override public void afterInsert(){
    new PublishOrderPartnerOnTagus().execute();
  }
  
}