/**
 * Created by bruno on 24/02/2022.
 */

@IsTest
private class OpportunityWithChildOppsDataProviderTest
{
    private static final PS_Constants constants = PS_Constants.getInstance();

    @IsTest
    private static void forceException_UnitTest() {
        OpportunityWithChildOppsDataProvider dataProvider = new OpportunityWithChildOppsDataProvider();
        sortablegrid.SDG coreSDG = dataProvider.LoadSDG('', 'randomstring');

        // create the user request
        sortablegrid.SDGRequest request = new sortablegrid.SDGRequest();
        request.ParentRecordID = 'randomstring';
        request.PageID = 1;
        request.PageSize = 10;
        request.FieldSetName=null;
        request.RelationshipName=null;
        request.SDGTag='Apex:OpportunityWithChildOppsDataProvider';


        try {
            sortablegrid.SDGResult result = dataProvider.getData(coreSDG, request);
        } catch (Exception e) {
            System.assert(e instanceof OpportunityWithChildOppsDataProvider.OpportunityWithChildOppsDataProviderException);
        }

    }

    @IsTest
    private static void SDG_No_Child_UnitTest() {
        Id opportunityPartnerFlowRT = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(constants.OPPORTUNITY_RT_PARTNER_FLOW_OPP).getRecordTypeId();

        fflib_ApexMocks mocks = new fflib_ApexMocks();
        AccountOpportunitySelector mockAccountOpportunitySelector = (AccountOpportunitySelector)mocks.mock(AccountOpportunitySelector.class);
        OpportunitySelector mockOpportunitySelector = (OpportunitySelector)mocks.mock(OpportunitySelector.class);
        Id mockOppId = fflib_IDGenerator.generate(Opportunity.SObjectType);
        Id mockAccountId = fflib_IDGenerator.generate(Account.SObjectType);
        Id mockOppMemberId = fflib_IDGenerator.generate(Account_Opportunity_Relationship__c.SObjectType);

        Account_Opportunity_Relationship__c mockOppMember = new Account_Opportunity_Relationship__c( Id = mockOppMemberId, Opportunity__c = mockOppId, Account__c = mockAccountId);
        List<Account_Opportunity_Relationship__c> mockOppMemberResponse = new List<Account_Opportunity_Relationship__c>{mockOppMember};
        List<Opportunity> mockOpportunitiesResponse = new List<Opportunity>{new Opportunity(Id = mockOppId, Name = 'Test Opp', StageName = 'Waiting Cancellation', RecordTypeId = opportunityPartnerFlowRT)};

        mocks.startStubbing();
            mocks.when(mockAccountOpportunitySelector.sObjectType()).thenReturn(Account_Opportunity_Relationship__c.SObjectType);
            mocks.when(mockAccountOpportunitySelector.selectById(new Set<Id> {mockOppMemberId})).thenReturn(mockOppMemberResponse);

            mocks.when(mockOpportunitySelector.sObjectType()).thenReturn(Opportunity.SObjectType);
            mocks.when(mockOpportunitySelector.selectOpportunityAndChildOppsById(new Set<Id> {mockOppId})).thenReturn(mockOpportunitiesResponse);
        mocks.stopStubbing();

        Application.Selector.setMock(mockAccountOpportunitySelector);
        Application.Selector.setMock(mockOpportunitySelector);

        Test.startTest();
            // instantiate class
            OpportunityWithChildOppsDataProvider dataProvider = new OpportunityWithChildOppsDataProvider();
            sortablegrid.SDG coreSDG = dataProvider.LoadSDG('', mockOppMemberId);

            // create the user request
            sortablegrid.SDGRequest request = new sortablegrid.SDGRequest();
            request.ParentRecordID = mockOppMemberId;
            request.PageID = 1;
            request.PageSize = 10;
            request.FieldSetName=null;
            request.RelationshipName=null;
            request.SDGTag='Apex:OpportunityWithChildOppsDataProvider';



            sortablegrid.SDGResult result = dataProvider.getData(coreSDG, request);
            system.assertEquals(1, result.data.size());
        Test.stopTest();

    }

    @IsTest
    private static void SDG_With_Child_UnitTest() {
        Id opportunityPartnerFlowRT = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(constants.OPPORTUNITY_RT_PARTNER_FLOW_OPP).getRecordTypeId();
        Id opportunityChildPartnerFlowRT = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(constants.OPPORTUNITY_RT_PARTNER_FLOW_CHILD_OPP).getRecordTypeId();


        fflib_ApexMocks mocks = new fflib_ApexMocks();
        AccountOpportunitySelector mockAccountOpportunitySelector = (AccountOpportunitySelector)mocks.mock(AccountOpportunitySelector.class);
        OpportunitySelector mockOpportunitySelector = (OpportunitySelector)mocks.mock(OpportunitySelector.class);
        Id mockOppId = fflib_IDGenerator.generate(Opportunity.SObjectType);
        Id mockAccountId = fflib_IDGenerator.generate(Account.SObjectType);
        Id mockOppMemberId = fflib_IDGenerator.generate(Account_Opportunity_Relationship__c.SObjectType);
        Id mockOppChild = fflib_IDGenerator.generate(Opportunity.SObjectType);

        Account_Opportunity_Relationship__c mockOppMember = new Account_Opportunity_Relationship__c( Id = mockOppMemberId, Opportunity__c = mockOppId, Account__c = mockAccountId);
        List<Account_Opportunity_Relationship__c> mockOppMemberResponse = new List<Account_Opportunity_Relationship__c>{mockOppMember};
        List<Opportunity> mockOpportunitiesResponse = (List<Opportunity>) fflib_ApexMocksUtils.makeRelationship(
                List<Opportunity>.class,
                new List<Opportunity>{new Opportunity(Id = mockOppId, Name = 'Test Opp', StageName = 'Waiting Cancellation', RecordTypeId = opportunityPartnerFlowRT)},
                Opportunity.MasterOpportunity__c,
                new List<List<Opportunity>> {
                        new List<Opportunity>{
                                new Opportunity(Id = mockOppChild, Name = 'Test Opp Child', StageName = 'Waiting Cancellation', RecordTypeId = opportunityChildPartnerFlowRT)
                        }
                }
        );



        mocks.startStubbing();
        mocks.when(mockAccountOpportunitySelector.sObjectType()).thenReturn(Account_Opportunity_Relationship__c.SObjectType);
        mocks.when(mockAccountOpportunitySelector.selectById(new Set<Id> {mockOppMemberId})).thenReturn(mockOppMemberResponse);

        mocks.when(mockOpportunitySelector.sObjectType()).thenReturn(Opportunity.SObjectType);
        mocks.when(mockOpportunitySelector.selectOpportunityAndChildOppsById(new Set<Id> {mockOppId})).thenReturn(mockOpportunitiesResponse);
        mocks.stopStubbing();

        Application.Selector.setMock(mockAccountOpportunitySelector);
        Application.Selector.setMock(mockOpportunitySelector);

        Test.startTest();
        // instantiate class
        OpportunityWithChildOppsDataProvider dataProvider = new OpportunityWithChildOppsDataProvider();
        sortablegrid.SDG coreSDG = dataProvider.LoadSDG('', mockOppMemberId);

        // create the user request
        sortablegrid.SDGRequest request = new sortablegrid.SDGRequest();
        request.ParentRecordID = mockOppMemberId;
        request.PageID = 1;
        request.PageSize = 10;
        request.FieldSetName=null;
        request.RelationshipName=null;
        request.SDGTag='Apex:OpportunityWithChildOppsDataProvider';



        sortablegrid.SDGResult result = dataProvider.getData(coreSDG, request);
        system.assertEquals(2, result.data.size());
        Test.stopTest();

    }

    @IsTest
    static void isUserSelectableTest(){

        Boolean isUserSelectable = true;
        System.assertEquals(isUserSelectable, OpportunityWithChildOppsDataProvider.isUserSelectable());
    }
}