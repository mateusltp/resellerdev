@isTest
public with sharing class UserSingletonTest {
    


    @isTest
    public static void testMain(){

        Test.startTest();
        UserSingleton user = UserSingleton.getInstance(UserInfo.getUserId());

        String id = user.getId();
        String name = user.getName();
        String contact = user.getContactId();
        String email = user.getEmail();
        String phone = user.getPhone();
        String accountId = user.getAccountId();
        Boolean isActive = user.getIsActive();
        String countryCode = user.getCountryCode();

        System.assertNotEquals(null, user);

        Test.stopTest();

    }
}