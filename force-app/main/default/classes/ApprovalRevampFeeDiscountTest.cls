/**
 * @description       : 
 * @author            : roei@gft.com
 * @group             : 
 * @last modified on  : 10-08-2021
 * @last modified by  : roei@gft.com
**/
@isTest(SeeAllData=false)
public with sharing class ApprovalRevampFeeDiscountTest {

    @isTest 
    private static void shouldCreateAssertData(){
        Opportunity lOpp = [ SELECT Id FROM Opportunity LIMIT 1 ];
        Quote lProposal = [ SELECT Id FROM Quote LIMIT 1 ];
        lProposal.Enterprise_Subscription_Approved__c = true;

        Test.startTest();

        Database.update( lProposal );

        Test.stopTest();

        List< Assert_Data__c > lLstAssertData = [ SELECT Id, Old_Value__c FROM Assert_Data__c WHERE Opportunity__c =: lOpp.Id AND 
          Fee_Type__c = 'Enterprise Subscription' AND Field_Name__c = 'discount__c' AND Object_Name__c = 'quotelineitem' ];

        System.assertEquals( false , lLstAssertData.isEmpty() );
        System.assertEquals( 1 , lLstAssertData.size() );
        System.assertEquals( '50.00' , lLstAssertData[0].Old_Value__c );
    }

    @TestSetup
    private static void createData(){    
        Account lParentAcc = DataFactory.newAccount();
        lParentAcc.NumberOfEmployees = 1000;
        Database.insert( lParentAcc );

        Pricebook2 lStandardPb = DataFactory.newPricebook();

        Opportunity lOpp = DataFactory.newOpportunity( lParentAcc.Id , lStandardPb , 'Client_Sales_New_Business' );
        lOpp.Quantity_Offer_Number_of_Employees__c = 500;
        Database.insert( lOpp );

        Quote lProposal = DataFactory.newQuote( lOpp , 'Quote Test', 'Client_Sales_New_Business' );
        Database.insert( lProposal );

        Product2 lAcessFee = DataFactory.newProduct( 'Enterprise Subscription' , false , 'BRL' );
        Database.insert( lAcessFee );

        PricebookEntry lAccessFeeEntry = DataFactory.newPricebookEntry( lStandardPb , lAcessFee , lOpp );
        Database.insert( lAccessFeeEntry );

        QuoteLineItem lQuoteAccessFee = DataFactory.newQuoteLineItem( lProposal , lAccessFeeEntry );
        lQuoteAccessFee.Discount__c = 50;
        Database.insert( lQuoteAccessFee );
    }
}