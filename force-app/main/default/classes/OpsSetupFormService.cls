/**
 * @description       : 
 * @author            : ext.gft.pedro.oliveira@gympass.com
 * @group             : 
 * @last modified on  : 04-08-2022
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/

public with sharing class OpsSetupFormService { 
    
    private static final PS_Constants constants = PS_Constants.getInstance();   
    
    public static void createOpsFormForAccounts(Id aOpportunityId, Set<Id> accountsId, String recordTypeDevName){
      try{          
            service(recordTypeDevName).createOpsFormForAccounts(aOpportunityId,accountsId);
        }catch (Exception e){
            DebugLog__c log = new DebugLog__c(
                    Origin__c = '[opsFormPartnerValidation]',
                    LogType__c = constants.LOGTYPE_ERROR,
                    RelatedToSquad__c = constants.LOGTYPE_SQUAD_PARTNERS,
                    ExceptionMessage__c = e.getMessage(),
                    StackTrace__c = e.getStackTraceString(),
                    DataScope__c = JSON.serialize(aOpportunityId)
            );
            Logger.createLog(log);
            throw new OppsSetupFormServiceException(e.getMessage());
      }
    }

    private static String getRecordTypeAndSObjectName (String recordTypeDevName){
      return constants.OPS_SETUP_FORM_OBJECT+'.'+recordTypeDevName;
  }
  
  private static IOpsSetupFormService service(String recordTypeDevName) {
      return (IOpsSetupFormService) Application.ServiceByRecordType.newInstanceByRecordType(getRecordTypeAndSObjectName(recordTypeDevName));
  }

  public class OppsSetupFormServiceException extends Exception {}
}