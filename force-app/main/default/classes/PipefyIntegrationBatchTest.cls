/**
 * @description       : 
 * @author            : AlyssonMota
 * @group             : 
 * @last modified on  : 12-16-2021
 * @last modified by  : roei@gft.com
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   07-19-2021   Alysson Mota   Initial Version
**/
@isTest
public with sharing class PipefyIntegrationBatchTest {
    @TestSetup
    public static void setupData() {
        Integer numberOfRecords = 5;
        
        Map<Integer, Account> accMap = new Map<Integer, Account>();
        Map<Integer, Opportunity> oppMap = new Map<Integer, Opportunity>();
        Map<Integer, Quote> quoteMap = new Map<Integer, Quote>();
        Map<Integer, Case> ddoCaseMap = new Map<Integer, Case>();
        Map<Integer, QuoteLineItem> accessFeeMap = new Map<Integer, QuoteLineItem>();
        Map<Integer, Payment__c> payForAccessFeeMap = new Map<Integer, Payment__c>();
        Map<Integer, QuoteLineItem> setupFeeMap = new Map<Integer, QuoteLineItem>();
        Map<Integer, Payment__c> payForSetupFeeMap = new Map<Integer, Payment__c>();
        Map<Integer, QuoteLineItem> proServicesOneFeeMap = new Map<Integer, QuoteLineItem>();
        Map<Integer, Payment__c> payForServOneFeeMap = new Map<Integer, Payment__c>();
        Map<Integer, Payment__c> payForServMainFeeMap = new Map<Integer, Payment__c>();
        Map<Integer, QuoteLineItem> proServicesMainFeeMap = new Map<Integer, QuoteLineItem>();
        List<QuoteLineItem> qtLineItens = new List<QuoteLineItem>();
        List<Payment__c> payments = new List<Payment__c>();
        List<Eligibility__c> eligibilities = new List<Eligibility__c>();
        List<Waiver__c> waivers = new List<Waiver__c>();

        User us = getClientSuccessExecutive();
       
        for (Integer i=0; i<numberOfRecords; i++) {
            accMap.put(i, getAccount(i));
        }
        insert accMap.values();
        
        Pricebook2 standardPricebook = new Pricebook2(
            Id = Test.getStandardPricebookId(),
            IsActive = true
        );
        update standardPricebook;
        Pricebook2 pb = standardPricebook;
        
        Product2 accessFeeProduct = getProduct('Enterprise Subscription');
        insert accessFeeProduct;
        
        Product2 setupFeeProduct = getProduct('Setup Fee');
        insert setupFeeProduct; 
        
        PricebookEntry accessFeeEntry = getPricebookEntry(pb, accessFeeProduct);
        insert accessFeeEntry;
        
        PricebookEntry setupFeeEntry = getPricebookEntry(pb, setupFeeProduct);
        insert setupFeeEntry;
        
        Product2 pProfServicesOneFee = getProfServicesOneFeeProduct();
        insert pProfServicesOneFee;
        
        PricebookEntry profServicesOneFeeEntry = getPricebookEntry(pb, pProfServicesOneFee);
        insert profServicesOneFeeEntry;
        
        Product2 pProfServicesMainFee = getProfServicesMainFeeProduct();
        insert pProfServicesMainFee;
        
        PricebookEntry profServicesMainFeeEntry = getPricebookEntry(pb, pProfServicesMainFee);
        insert profServicesMainFeeEntry;
       
        Account acc = null;
        for (Integer i : accMap.keySet()) {
            acc = accMap.get(i);
            oppMap.put(i, getSmbOpp(acc, pb));
        } 
        insert oppMap.values();
        
        Account acc2 = null;
        Opportunity opp = null;
        for (Integer i : accMap.keySet()) {
            acc2 = accMap.get(i);
            opp = oppMap.get(i);
            AccountOpportunityRepository accountsInOpp = new AccountOpportunityRepository(opp.Id);
            accountsInOpp.setRelationshipAccountWithOppForClients(new List<Account>{acc});
        }

        Offer_Queue__c offerQueue1 = new Offer_Queue__c();

        offerQueue1.Account_ID__c = acc.Id;
        offerQueue1.Adjusted_Probability__c = null;
        offerQueue1.Allowlist_Enabler__c = 'Yes';        
        offerQueue1.CurrencyIsoCode = 'BRL';
        offerQueue1.End_Date__c = System.today().addDays(365);
        offerQueue1.ES_Billing_Percentage__c = 100;
        offerQueue1.ES_Payment_Frequency__c = 'Monthly';
        offerQueue1.Family_Member__c = 'Yes';
        offerQueue1.Free_Plan__c = 'No';
        offerQueue1.Number_of_Employees__c = 900;
        offerQueue1.Close_Date__c = System.today();
        offerQueue1.Opportunity_ID__c = opp.Id;
        offerQueue1.Parent_Account_ID__c = null;
        offerQueue1.Price_index__c = 'IPCA';
        offerQueue1.Price_index_description__c = '';
        offerQueue1.Reference_Sales_Price_per_Eligible__c = 4.45;
        offerQueue1.Stage__c = 'Launched/Won';
        offerQueue1.Start_Date__c = System.today();
        offerQueue1.Status__c = 'Success';
        offerQueue1.Status_Detail__c = '';
        offerQueue1.Comments__c = '[expansion]';
        offerQueue1.Type_of_Request__c = 'SMB Sales';
        offerQueue1.Waiver_End_Date__c = System.today().addDays(90);
        offerQueue1.Waiver_Percentage__c = 100;
        offerQueue1.Waiver_Start_Date__c = System.today().addDays(30);
        offerQueue1.Full_Launch__c = 'Yes';
        offerQueue1.Exclusivity_clause__c = 'Yes';
        insert offerQueue1;


        for (Integer i : oppMap.keySet()) {
            Case ddo = getDealDeskOperationalApproval(oppMap.get(i));
            ddoCaseMap.put(i, ddo);
        }
        insert ddoCaseMap.values();
       
        for (Integer i : oppMap.keySet()) {
            Quote qt = getQuote(oppMap.get(i));
            quoteMap.put(i, qt);
        }
        insert quoteMap.values();
        
        // access fee
        for (Integer i : quoteMap.keySet()) {
            QuoteLineItem accessFee = getAccessFee(quoteMap.get(i), accessFeeEntry);
            accessFeeMap.put(i, accessFee);
        }
        qtLineItens.addAll(accessFeeMap.values());

       

       /* insert qtLineItens;

        // Access Fee Payments
        for (Integer i : accMap.keySet()) {
            Account acc3 = accMap.get(i);
            QuoteLineItem accessFee = accessFeeMap.get(i);
            payForAccessFeeMap.put(i, getPaymentForFee(accessFee, acc3));
        }
        payments.addAll(payForAccessFeeMap.values());
      
        // Eligibilities and Waivers for access Fee
        for (Payment__c payment : payForAccessFeeMap.values()) {
            Eligibility__c eli = getEligibilityForPayment(payment);
            eligibilities.add(eli);
            
        }

        insert eligibilities;*/

    }

    /*@isTest
    public static void testBatchIntegration() {
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new Mock());

        try {
            PipefyIntegrationBatch pipefyBatch = new PipefyIntegrationBatch();
            Database.executeBatch(pipefyBatch);

            //String CRON_EXP = '0 0 0 * * ? *';
            
            //SchedulePipefyIntegrationBatch sch = new SchedulePipefyIntegrationBatch();
            //String jobId = System.schedule('ScheduledApexTest', CRON_EXP, sch);         
            //sch.execute(null);

        } catch(Exception e) {
            System.debug('Error');
        }
        Test.stopTest();

        List<Opportunity> opps = [SELECT Id, Pipefy_Card_Id__c FROM Opportunity WHERE StageName = 'Lançado/Ganho'];
        System.debug('opps size');
        System.debug(opps.size());
        
    }*/

    private static Account getAccount(Integer accNumber){
        Account acc = new Account();
        acc.Name = 'Test Name' + accNumber;
        acc.Razao_Social__c = 'Test Legal Name' + accNumber;
        acc.BillingCity = 'Test Billig City';
        acc.BillingCountry = 'Brazil';
        acc.BillingCountryCode = 'BR';
        acc.BillingPostalCode = '00000-000';
        acc.BillingStreet = 'Test Street 999';
        acc.Id_Company__c = '0000000' + accNumber;
        acc.Send_To_Tagus__c = false;
        
        return acc;
    }

    private static Opportunity getSmbOpp(Account acc, Pricebook2 pb){
        Opportunity opp = new Opportunity();
        opp.AccountId = acc.Id;
        opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SMB_New_Business').getRecordTypeId();
        opp.Name = 'Opp Test Name Renew';
        opp.CloseDate = system.today().addDays(30);
        opp.FastTrackStage__c = 'Launched/Won';
        opp.StageName = 'Lançado/Ganho';
        opp.Pricebook2Id = pb.Id;
        opp.Type = 'Expansion';  
        opp.Country_Manager_Approval__c = true;
        opp.Payment_approved__c = true;   
        opp.CurrencyIsoCode = 'BRL';
        opp.Gympass_Plus__c = 'Yes';
        opp.Standard_Payment__c = 'Yes';
        opp.Request_for_self_checkin__c = 'Yes';
        opp.Self_Checkout_New_Business__c = false;
        opp.Comments__c = 'Test';
        return opp;
    }

    private static PricebookEntry getPricebookEntry(Pricebook2 pb, Product2 prd){        
        PricebookEntry pbEntry = new PricebookEntry();
        pbEntry.Product2Id = prd.Id;
        pbEntry.Pricebook2Id = Test.getStandardPricebookId();
        pbEntry.UnitPrice = 200;
        pbEntry.IsActive = true;
        pbEntry.CurrencyIsoCode = 'BRL';
        return pbEntry;
    }

    private static User getClientSuccessExecutive(){
        //return [ SELECT Id, FirstName, LastName, ProfileId, Email FROM User WHERE Profile.Name = 'Client Success Executive' AND Email != null and IsActive = true LIMIT 1];
        Profile profileSuccess = [
            SELECT Id, Name FROM Profile WHERE Name LIKE '%Success%' LIMIT 1
        ];

        User testUser = new User();
        testUser.Username = 'user@gympass.com';
        testUser.LastName = 'Gympass';
        testUser.Email = 'user@gympass.com';
        testUser.Alias = 'user';
        testUser.TimeZoneSidKey = 'America/Sao_Paulo';
        testUser.LocaleSidKey = 'en_US';
        testUser.EmailEncodingKey = 'ISO-8859-1';
        testUser.ProfileId = profileSuccess.Id;
        testUser.LanguageLocaleKey = 'en_US';
        insert testUser;

        return testUser;
    }

    private static Product2 getProduct(String ProductName){
        Product2 product = new Product2();
        product.Name = ProductName;
        product.Minimum_Number_of_Employees__c = 0;
        product.Maximum_Number_of_Employees__c = 800;
        product.Family = ProductName;
        product.IsActive = true;
        return product;
    }
    
    private static PricebookEntry getPricebookEntry(Pricebook2 pb, Product2 prd, Opportunity opp){        
        PricebookEntry pbEntry = new PricebookEntry();
        pbEntry.Product2Id = prd.Id;
        pbEntry.Pricebook2Id = Test.getStandardPricebookId();
        pbEntry.UnitPrice = 200;
        pbEntry.IsActive = true;
        pbEntry.CurrencyIsoCode = opp.CurrencyIsoCode;
        return pbEntry;
    }

    private static Product2 getProfServicesOneFeeProduct(){
        Product2 product = new Product2();
        product.Name = 'Professional Services Setup Fee';
        product.Minimum_Number_of_Employees__c = 0;
        product.Maximum_Number_of_Employees__c = 900;
        product.Family = 'Professional Services';
        product.IsActive = true;
        product.Family_Member_Included__c = true;
        return product;
    }

    private static Product2 getProfServicesMainFeeProduct(){
        Product2 product = new Product2();
        product.Name = 'Professional Services Maintenance Fee';
        product.Minimum_Number_of_Employees__c = 0;
        product.Maximum_Number_of_Employees__c = 900;
        product.Family = 'Professional Services';
        product.IsActive = true;
        product.Family_Member_Included__c = true;
        return product;
    }

    private static Case getDealDeskOperationalApproval(Opportunity opp){
        Case ddo = new Case();
        ddo.OpportunityId__c = opp.Id;
        ddo.Justificate_No_Compliant_Topics__c = 'Justification Test';
        ddo.Subject = 'Deal Desk Operational Approval Request';
        ddo.Auto_Approved_Case__c = true;
        ddo.Status = 'Approved';
        ddo.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Deal_Desk_Operational').getRecordTypeId(); 
        ddo.Deal_Desk_Evaluation__c = 'Approved';
        ddo.ES_Payment_Due_Days__c = '10 days';
        return ddo;
    }

    private static Quote getQuote(Opportunity opp){
        Quote qt = new Quote();
        qt.Name = 'Test';
        qt.OpportunityId = opp.Id;
        qt.License_Fee_Waiver__c = 'Yes';
        qt.Waiver_Termination_Date__c = system.today().addDays(10);
        return qt;
    }

    private static QuoteLineItem getAccessFee(Quote qt, PricebookEntry pbEntry){
        QuoteLineItem accessFee = new QuoteLineItem();
        accessFee.QuoteId = qt.Id;
        accessFee.PricebookEntryId = pbEntry.Id;
        accessFee.Description = '';
        accessFee.Quantity = 100;
        accessFee.UnitPrice = pbEntry.UnitPrice;  
        
        return accessFee;
    }
    
    private static QuoteLineItem getSetupFee(Quote qt, PricebookEntry pbEntry){
        QuoteLineItem SetupFee = new QuoteLineItem();
        SetupFee.QuoteId = qt.Id;
        SetupFee.PricebookEntryId = pbEntry.Id;
        SetupFee.Description = '';
        SetupFee.Quantity = 100;
        SetupFee.UnitPrice = pbEntry.UnitPrice; 
        
        return SetupFee;
    }
    
    private static Payment__c getPaymentForFee(QuoteLineItem qli, Account acc){
        Payment__c afp = new Payment__c();
        afp.RecordTypeId = Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Payment').getRecordTypeId();
        afp.Quote_Line_Item__c = qli.Id;
        afp.Percentage__c = 100;
        afp.Payment_Method__c = 'Wire Transfer';
        afp.Payment_Due_Days__c = '30 Days';
        afp.Billing_Day__c = '01';
        afp.PO_Required__c = 'Yes';
        afp.Frequency__c = 'Monthly';
        afp.Cutoff_Day__c = 1;   
        afp.Account__c =  acc.Id;
        return afp;
    }

    private static Eligibility__c getEligibilityForPayment(Payment__c payment){
        Eligibility__c eli = new Eligibility__c();
        eli.Name = 'Headquarters';
        eli.Communication_Restriction__c = '3. Accept communications with employees after sign up';
        eli.Is_Default__c = true;
        eli.Payment_Due_Days__c = '30 Days';
        eli.Billing_Day__c = '01';
        eli.Group_Name__c = 'Main';
        eli.Launch_Date__c = system.today().addDays(10);
        return eli;
    }

    private static Waiver__c getWaiverForPayment(Payment__c pay){
        Waiver__c w = new Waiver__c();
        w.Start_Date__c = System.today();
        w.End_Date__c = System.today().addDays(10);
        w.Percentage__c = 10;
        w.RecordTypeId = Schema.SObjectType.Waiver__c.getRecordTypeInfosByDeveloperName().get('Fixed_Date').getRecordTypeId();
        w.Payment__c = pay.Id;
        return w;
    }

    public class Mock implements HttpCalloutMock {
    	protected Integer code = 200;
    	protected String status = 'OK';
    	protected String body = '{"data":{"createCard":{"card":{"id":"405722846"},"clientMutationId":null}}}';

        /**
         * @description Returns an HTTP response for the given request.
         * @param req HTTP request for which response should be returned
         * @return mocked response
         */
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse resp = new HttpResponse();
            resp.setStatusCode(code);
            resp.setStatus(status);
            resp.setBody(body);
            return resp;
        }
    }
}