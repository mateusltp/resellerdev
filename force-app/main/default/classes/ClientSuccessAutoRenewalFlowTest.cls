/**
 * @description       : 
 * @author            : AlyssonMota
 * @group             : 
 * @last modified on  : 08-30-2021
 * @last modified by  : roei@gft.com
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   07-02-2021   Alysson Mota   Initial Version
**/
@isTest(seeAllData=false)
public with sharing class ClientSuccessAutoRenewalFlowTest {
    
    @TestSetup
    static void createData(){        
        Account acc = generateGympassEntity();
        insert acc;
        Pricebook2 standardPricebook = new Pricebook2(
            Id = Test.getStandardPricebookId(),
            IsActive = true
        );
        update standardPricebook;
        Pricebook2 pb = standardPricebook;                      
        Product2 accessFee = generateProduct('Enterprise Subscription');
        insert accessFee;
        Product2 setupFee = generateProduct('Setup Fee');
        insert setupFee;        
        Opportunity opp = new Opportunity(CurrencyIsoCode='BRL');
        PricebookEntry accessFeeEntry = generatePricebookEntry(pb, accessFee, opp);
        insert accessFeeEntry;
        Product2 pFamilyMember = generateAccessFeeFamilyMemberIncludedProduct();
        insert pFamilyMember;
        PricebookEntry accessFeeFamilyMember = generatePricebookEntry(pb, pFamilyMember, opp);
        insert accessFeeFamilyMember;
        PricebookEntry setupFeeEntry = generatePricebookEntry(pb, setupFee, opp);
        insert setupFeeEntry;
        Product2 pProfServicesOneFee = generateProfServicesOneFeeProduct();
        insert pProfServicesOneFee;
        PricebookEntry profServicesOneFee = generatePricebookEntry(pb, pProfServicesOneFee, opp);
        insert profServicesOneFee;
        Product2 pProfServicesMainFee = generateProfServicesMainFeeProduct();
        insert pProfServicesMainFee;
        PricebookEntry profServicesMainFee = generatePricebookEntry(pb, pProfServicesMainFee, opp);
        insert profServicesMainFee;
        opp = generateOpportunity('Client_Sales_New_Business',acc,pb);
        insert opp;
        
        Account_Opportunity_Relationship__c lAccOppRelationship = new Account_Opportunity_Relationship__c();
        lAccOppRelationship.Opportunity__c = opp.Id;
        lAccOppRelationship.Account__c = acc.Id;
        insert lAccOppRelationship;
        
        Quote quote = new Quote();
        quote.Name = 'Quote test';
        quote.OpportunityId = opp.Id;
        quote.License_Fee_Waiver__c = 'Yes';
        quote.Waiver_Termination_Date__c = System.today();
        insert quote;
        
        QuoteLineItem lAccessFee = new QuoteLineItem();           
        lAccessFee.Quantity = 1;
        lAccessFee.Discount__c = 10.0; 
        lAccessFee.Description = '';
        lAccessFee.QuoteId = quote.Id;
        lAccessFee.pricebookEntryId = accessFeeEntry.Id; 
        lAccessFee.UnitPrice = 100;
        insert lAccessFee;
        
        QuoteLineItem lSetupFee = new QuoteLineItem();           
        lSetupFee.Quantity = 1;
        lSetupFee.Discount__c = 10.0; 
        lSetupFee.Description = '';
        lSetupFee.QuoteId = quote.Id;
        lSetupFee.pricebookEntryId = setupFeeEntry.Id; 
        lSetupFee.UnitPrice = 100;
        insert lSetupFee;
        
        Payment__c lPayment = new Payment__c();
        lPayment.RecordtypeId = Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Payment').getRecordTypeId();
        lPayment.Quote_Line_Item__c = lAccessFee.Id;
        insert lPayment;
        
        Eligibility__c lEligibility = new Eligibility__c();
        lEligibility.Payment__c = lPayment.Id;
        insert lEligibility;
    }
   
    
	@isTest
    private static void shouldCloneOpportunityAndRelatedObjects(){
        Opportunity lOpp = [ SELECT Id,AccountId FROM Opportunity LIMIT 1 ];
        lOpp.StageName = 'Lançado/Ganho';
        update lOpp;
        
        Account lAcc = [ SELECT Id FROM Account LIMIT 1 ];
        
        Test.startTest();
        ClientSuccessAutoRenewalFlow.cloneLastClosedOpp( new List< Id >{ lAcc.Id } );
        Test.stopTest();
        
        List< Opportunity > lLstOpp = 
            [ SELECT Id,( SELECT Id FROM Quotes ) FROM Opportunity WHERE StageName = 'Validated' AND Recordtype.DeveloperName = 'Client_Success_Renegotiation' ];
        
        System.assert( lLstOpp.size() == 1 );
        System.assert( lLstOpp[0].Quotes.size() == 1 );
    }
    
    
    @isTest
    private static void shouldCreateNewRenegotiationOpportunity(){
    	Opportunity lOpp = [ SELECT Id,AccountId FROM Opportunity LIMIT 1 ];
        lOpp.StageName = 'Perdido';
        lOpp.Loss_Reason__c = 'Lost to Competitor';
        update lOpp;
        
        Account lAcc = [ SELECT Id FROM Account LIMIT 1 ];
        
        Test.startTest();
        ClientSuccessAutoRenewalFlow.cloneLastClosedOpp( new List< Id >{ lAcc.Id } );
        Test.stopTest();
        
        List< Opportunity > lLstOpp = 
            [ SELECT Id, Pricebook2Id,( SELECT Id FROM Quotes ) 
              FROM Opportunity WHERE StageName = 'Validated' AND Recordtype.DeveloperName = 'Client_Success_Renegotiation' ];
        
        System.assert( lLstOpp.size() == 1 );
        System.assert( lLstOpp[0].Quotes.size() == 0 );
    }

    private static Account generateGympassEntity(){
        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gympass_Entity').getRecordTypeId();
        Account lAcc = new Account();
        lAcc.name='AcademiaBrasilCompanyPai';
        lAcc.RecordTypeId = rtId;
        lAcc.GP_Status__c = 'Active';
        lAcc.billingState = 'Minas Gerais';
        lAcc.CAP_Value__c = 120;
        lAcc.BillingCity = 'CityAcademiaBrasil';
        lAcc.billingStreet = 'Rua academiabrasilpai';
        lAcc.billingCountry = 'Brazil';
        lAcc.Gym_Type__c = 'Studios';
        lAcc.Gym_Classes__c = 'Cardio';
        lAcc.Types_of_ownership__c = Label.franchisePicklist;
        lAcc.Subscription_Type__c = 'Value per class';
        lAcc.Subscription_Period__c = 'Monthy value';
        lAcc.Subscription_Type_Estimated_Price__c    = 100;
        lAcc.Has_market_cannibalization__c = 'No';
        lAcc.Exclusivity__c = 'Yes';
        lAcc.Exclusivity_End_Date__c = Date.today().addYears(1);
        lAcc.Exclusivity_Partnership__c = 'Full Exclusivity';
        lAcc.Exclusivity_Restrictions__c= 'No';
        lAcc.Website = 'testing@tesapex.com';
        lAcc.Gym_Email__c = 'gymemail@apex.com';
        lAcc.Phone = '3222123123';
        lAcc.Can_use_logo__c = 'Yes';
        lAcc.Legal_Registration__c = 12123;
        lAcc.Legal_Title__c = 'Title LTDA';
        lAcc.Gyms_Identification_Document__c = 'CNPJ'; 
        return lAcc;
    }
    
    private static Opportunity generateOpportunity(String aRecordType, Account lAcc, Pricebook2 lPricebook){
        Id lOppRt = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(aRecordType).getRecordTypeId();
        Opportunity lAccOpp = new Opportunity();
        lAccOpp.recordTypeId = lOppRt;
        lAccOpp.AccountId = lAcc.id;
        lAccOpp.Name = lAcc.Id; 
        lAccOpp.CMS_Used__c = 'Yes';     
        lAccOpp.Gym_agreed_to_an_API_integration__c  = 'Yes';
        lAccOpp.Club_Management_System__c = 'Companhia Athletica';
        lAccOpp.Integration_Fee_Deduction__c = 'No';
        lAccOpp.CloseDate = Date.today();
        lAccOpp.Success_Look_Like__c = 'Yes';
        lAccOpp.Success_Look_Like_Description__c = 'Money money';
        lAccOpp.StageName = 'Proposta Enviada';
        lAccOpp.Type = 'Expansion';  
        lAccOpp.Country_Manager_Approval__c = true;
        lAccOpp.Payment_approved__c = true;   
        lAccOpp.CurrencyIsoCode = 'BRL';
        lAccOpp.Gympass_Plus__c = 'Yes';
        lAccOpp.Standard_Payment__c = 'Yes';
        lAccOpp.Request_for_self_checkin__c = 'Yes';  
        lAccOpp.Pricebook2Id = lPricebook.Id;
        return lAccOpp;
    }
    
    private static Pricebook2 generatePricebook(){        
        Pricebook2 pb = new Pricebook2();
        pb.Name = 'Br';
        pb.Country__c = 'Brazil';
        pb.IsActive = true;
        return pb;
    }
    
    private static PricebookEntry generatePricebookEntry(Pricebook2 pb, Product2 product, Opportunity opp){        
        PricebookEntry pbEntry = new PricebookEntry();
        pbEntry.Product2Id = product.Id;
        pbEntry.Pricebook2Id = Test.getStandardPricebookId();
        pbEntry.UnitPrice = 100;
        pbEntry.IsActive = true;
        pbEntry.CurrencyIsoCode = opp.CurrencyIsoCode;
        return pbEntry;
    }
    
    private static Product2 generateProduct(String ProductName){
        Product2 product = new Product2();
        product.Name = ProductName;
        product.Minimum_Number_of_Employees__c = 0;
        product.Maximum_Number_of_Employees__c = 900;
        product.Family = ProductName;
        product.IsActive = true;
        return product;
    }
    
    private static Product2 generateProfServicesOneFeeProduct(){
        Product2 product = new Product2();
        product.Name = 'Professional Services Setup Fee';
        product.Minimum_Number_of_Employees__c = 0;
        product.Maximum_Number_of_Employees__c = 900;
        product.Family = 'Professional Services';
        product.IsActive = true;
        product.Family_Member_Included__c = true;
        return product;
    }

    private static Product2 generateProfServicesMainFeeProduct(){
        Product2 product = new Product2();
        product.Name = 'Professional Services Maintenance Fee';
        product.Minimum_Number_of_Employees__c = 0;
        product.Maximum_Number_of_Employees__c = 900;
        product.Family = 'Professional Services';
        product.IsActive = true;
        product.Family_Member_Included__c = true;
        return product;
    }
    
    private static Product2 generateAccessFeeFamilyMemberIncludedProduct(){
        Product2 product = new Product2();
        product.Name = 'Enteprise FM';
        product.Minimum_Number_of_Employees__c = 0;
        product.Maximum_Number_of_Employees__c = 900;
        product.Family_Member_Included__c = true;
        product.Family = 'Enterprise Subscription';
        product.IsActive = true;
        return product;
    }
}