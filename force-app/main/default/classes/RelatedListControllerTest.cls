/**
 * @description       : 
 * @author            : tiago.ribeiro@gympass.com
 * @group             : 
 * @last modified on  : 04-04-2022
 * @last modified by  : tiago.ribeiro@gympass.com
**/
@isTest
public with sharing class RelatedListControllerTest {
    @isTest public static void testInit(){
        Account acc = DataFactory.newAccount();
        insert acc;
        Contact cnt = DataFactory.newContact(acc, 'TestCibtact');
        insert cnt;
                
        Map<String, Object> requestMap = new Map<String, Object>(); 
        requestMap.put(RelatedListController.FIELDS_PARAM, 'Name, Title, Email, Phone');
        requestMap.put(RelatedListController.RELATED_FIELD_API_NAME_PARAM, 'AccountId');
        requestMap.put(RelatedListController.RECORD_ID_PARAM, acc.Id);
        requestMap.put(RelatedListController.NUMBER_OF_RECORDS_PARAM, 1);
        requestMap.put(RelatedListController.SOBJECT_API_NAME_PARAM, 'Contact');
        requestMap.put(RelatedListController.SORTED_BY_PARAM, 'Name');
        requestMap.put(RelatedListController.SORTED_DIRECTION_PARAM, 'ASC');
        requestMap.put(RelatedListController.RELATED_FROM_CHILD_OBJECT, false);
                
        String jsonData = RelatedListController.initData(JSON.serialize(requestMap));
        Map<String, Object> responseMap = (Map<String, Object>)JSON.deserializeUntyped(jsonData);
        List<Object> records = (List<Object>)responseMap.get(RelatedListController.RECORDS_PARAM);
        System.assert(!records.isEmpty());
    }
}