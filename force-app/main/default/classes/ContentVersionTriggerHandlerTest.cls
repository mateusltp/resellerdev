/**
 * @description       : 
 * @author            : gepi@gft.com
 * @group             : 
 * @last modified on  : 05-02-2022
 * @last modified by  : gepi@gft.com
**/
@istest
public class ContentVersionTriggerHandlerTest {
    
    @istest
    public static void CreateContentOnAccount(){

        Test.startTest();
        
        //Create a contract - A clone class will create a Document__c on Insert equals the original Contract Agreement
        APXT_Redlining__Contract_Agreement__c contrato =  new APXT_Redlining__Contract_Agreement__c();
        contrato.RecordTypeId = Schema.SObjectType.APXT_Redlining__Contract_Agreement__c.getRecordTypeInfosByName().get('B2B Contract').getRecordTypeId();
        
        insert contrato;

        
		//Creating a attachment
        Blob bodyBlob=Blob.valueOf('Just a blob to insert into a attachment as body'); 
            
            ContentVersion contentVersion_1 = new ContentVersion(
                Title='Contract Attachment', 
                PathOnClient ='Contract_Attachment.docx',
                VersionData = bodyBlob, 
                origin = 'H'
            );
        
        insert contentVersion_1;

        
		ContentVersion contentVersion_2 = [SELECT Id, Title, ContentDocumentId 
                            FROM ContentVersion WHERE Id = :contentVersion_1.Id LIMIT 1];
        
        //When the content link is created the content version trigger will clone the attachment to the Document__c clone
        ContentDocumentLink contentlink = new ContentDocumentLink();
        contentlink.LinkedEntityId = contrato.id;
        contentlink.contentdocumentid = contentVersion_2.contentdocumentid;
        contentlink.ShareType = 'V';
        insert contentlink;

        //Asserting the the content is ok
        System.assertEquals(contentlink.ContentDocumentId,contentVersion_2.ContentDocumentId );
        
        Document__c docCreated = [SELECT Id, ContractAgreement__c FROM Document__c WHERE ContractAgreement__c = : contrato.Id LIMIT 1];
        
        ContentDocumentLink ContentDocLinkToAssert = [SELECT LinkedEntityId, ContentDocumentId , ContentDocument.Title , ContentDocument.ContentModifiedDate, ContentDocument.ContentSize  
                                                        FROM ContentDocumentLink WHERE LinkedEntityId =:docCreated.Id LIMIT 1];
        
        //Asserting if the attachment was cloned to the document__c 
        System.assertEquals(ContentDocLinkToAssert.LinkedEntityId, docCreated.Id);
        
		Test.stopTest();
    }

    @istest
    public static void CreateContentOnAccountContractReview(){

        Test.startTest();
        
        //Create a contract - A clone class will create a Document__c on Insert equals the original Contract Agreement        

        APXT_Redlining__Contract_Agreement__c contrato =  new APXT_Redlining__Contract_Agreement__c();
        contrato.RecordTypeId = Schema.SObjectType.APXT_Redlining__Contract_Agreement__c.getRecordTypeInfosByName().get('B2B Contract').getRecordTypeId();
        
        insert contrato;
        
		//Creating a attachment
        Blob bodyBlob=Blob.valueOf('Just a blob to insert into a attachment as body'); 
            
            ContentVersion contentVersion_1 = new ContentVersion(
                Title='Contract Attachment', 
                PathOnClient ='Contract_Attachment.docx',
                VersionData = bodyBlob, 
                origin = 'H'
            );        
        insert contentVersion_1;

        Case legalCase = new Case();
            legalCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Contract Review').getRecordTypeId();
            legalCase.status = 'In legal review'; 
            legalCase.ContractAgreement__c = contrato.Id;
            legalCase.OwnerId = '0051L00000Em42OQAR';             
        insert legalCase;        
        
		ContentVersion contentVersion_2 = [SELECT Id, Title, ContentDocumentId 
                            FROM ContentVersion WHERE Id = :contentVersion_1.Id LIMIT 1];
        
        //When the content link is created the content version trigger will clone the attachment to the Document__c clone
        ContentDocumentLink contentlink = new ContentDocumentLink();
        contentlink.LinkedEntityId = contrato.Id;
        contentlink.contentdocumentid = contentVersion_2.contentdocumentid;
        contentlink.ShareType = 'V';
        insert contentlink;        

        //Asserting the the content is ok
        System.assertEquals(contentlink.ContentDocumentId,contentVersion_2.ContentDocumentId );        
        
        // Document__c docCreated = [SELECT Id, ContractAgreement__c FROM Document__c WHERE ContractAgreement__c = : contrato.Id LIMIT 1];        
        
        // ContentDocumentLink ContentDocLinkToAssert = [SELECT LinkedEntityId, ContentDocumentId , ContentDocument.Title , ContentDocument.ContentModifiedDate, ContentDocument.ContentSize  
        //                                                 FROM ContentDocumentLink WHERE LinkedEntityId =:docCreated.Id LIMIT 1];
        
        // //Asserting if the attachment was cloned to the document__c 
        // System.assertEquals(ContentDocLinkToAssert.LinkedEntityId, docCreated.Id);
        
		Test.stopTest();

        
    }

}