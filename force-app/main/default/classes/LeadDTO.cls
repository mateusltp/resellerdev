public class LeadDTO {

    private String client_id;
    private String trade_name;
    private String legal_name;
    private String lead_source;
    private String lead_subsource_sf;
    private String sales_flow;
    private String legal_document_type;
    private String legal_document;
    private String phone;
    private String mobile;
    private String billing_id;
    private String billing_firstname;
    private String billing_lastname;
    private String billing_email;
    private String business_unit;
    private String currency_id;
    private Integer number_of_employees;
    private Integer number_of_employees_rounded;
    private String UTM_url;
    private String UTM_campaign;
    private String UTM_source;
    private String UTM_medium;
    private String UTM_content;
    private String UTM_term;
    private String language;
    private String funnel_stage;
    private String coupon_code;
    private Datetime coupon_expiration;
    private Decimal coupon_amount;
    private Decimal total_value;
    private Decimal total_value_w_discount;
    private String sku_selected;
    private ContactDTO contact;
    private AddressDTO address;

    public ContactDTO getContact() {
        return this.contact;
    }

    public AddressDTO getAddress() {
      return this.address;
    }

    public String getClientName() {
        return this.trade_name;
    }

    public Lead parseToSLead() {
        Lead lead = new Lead();
            lead.LeadSource = this.lead_source == null ? 'Inbound' : this.lead_source;
            lead.Lead_Sub__c = this.lead_subsource_sf == null ? 'Hands Up' : this.lead_subsource_sf;
            lead.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('SMB_Direct_Channel').getRecordTypeId();

            if(!String.isBlank(this.client_id))lead.Client_UUID__c = this.client_id;
            if(!String.isBlank(this.trade_name))lead.Company = this.trade_name;
            if(!String.isBlank(this.legal_name))lead.Razao_social__c = this.legal_name;
            if(!String.isBlank(this.sales_flow))lead.Sales_Flow__c = this.sales_flow;
            if(!String.isBlank(this.legal_document_type))lead.Legal_Document_Type__c = this.legal_document_type;
            if(!String.isBlank(this.legal_document))lead.CNPJ__c = this.legal_document;
            if(!String.isBlank(this.phone))lead.Phone = this.phone;
            if(!String.isBlank(this.billing_id))lead.Finance_Contact_UUID__c = this.billing_id;
            if(!String.isBlank(this.billing_lastname))lead.Finance_Contact_Name__c = this.billing_firstname + ' ' + this.billing_lastname;
            if(!String.isBlank(this.billing_email))lead.Finance_Contact_Email__c = this.billing_email;
            if(!String.isBlank(this.business_unit))lead.Business_Unit__c = this.business_unit;
            if(!String.isBlank(this.currency_id))lead.CurrencyIsoCode = this.currency_id;
            if(!String.isBlank(String.valueOf(this.number_of_employees)))lead.NumberOfEmployees = this.number_of_employees;
            if(!String.isBlank(String.valueOf(this.number_of_employees_rounded)))lead.Number_of_Employees_Rounded__c = this.number_of_employees_rounded;
            if(!String.isBlank(this.language))lead.Language_Code__c  = this.language;
            if(!String.isBlank(this.utm_url)) lead.UTM_URL__c = this.utm_url;
            if(!String.isBlank(this.utm_campaign))lead.UTM_Campaign__c = this.utm_campaign;
            if(!String.isBlank(this.utm_source))lead.UTM_Source__c = this.utm_source;
            if(!String.isBlank(this.utm_medium))lead.UTM_Medium__c = this.utm_medium;
            if(!String.isBlank(this.utm_content))lead.UTM_Content__c = this.utm_content;
            if(!String.isBlank(this.utm_term))lead.UTM_Term__c = this.utm_term;
            if(!String.isBlank(this.funnel_stage))lead.Funnel_Stage__c = this.funnel_stage;
            if(!String.isBlank(this.coupon_code))lead.Coupon_Code__c = this.coupon_code;
            if(!String.isBlank(String.valueOf(this.coupon_expiration)))lead.Coupon_Expiration__c = this.coupon_expiration;
            if(!String.isBlank(String.valueOf(this.coupon_amount)))lead.Coupon_Amount__c = this.coupon_amount;
            if(!String.isBlank(String.valueOf(this.total_value)))lead.Total_Value__c = this.total_value;
            if(!String.isBlank(String.valueOf(this.total_value_w_discount)))lead.Total_Value_W_Discount__c = this.total_value_w_discount;
            if(!String.isBlank(this.sku_selected))lead.SKU_Selected__c = this.sku_selected;
        
        if(this.contact != null) {
            if(!String.isBlank(this.contact.contact_id))lead.Contact_UUID__c = this.contact.contact_id;
            if(!String.isBlank(this.contact.first_name))lead.FirstName = this.contact.first_name;
            if(!String.isBlank(this.contact.last_name))lead.LastName = this.contact.last_name;
            if(!String.isBlank(this.contact.email))lead.Email = this.contact.email;
            if(!String.isBlank(this.contact.phone))lead.MobilePhone = this.contact.phone;
            if(!String.isBlank(this.contact.job_title))lead.Title = this.contact.job_title;
        }
        if(this.address != null) {
            if(!String.isBlank(this.address.street))lead.Street = this.address.street;
            if(!String.isBlank(this.address.state))lead.State = this.address.state;
            if(!String.isBlank(this.address.postal_code))lead.PostalCode = this.address.postal_code;
            if(!String.isBlank(this.address.country_id))lead.CountryCode = this.address.country_id;
            if(!String.isBlank(this.address.city))lead.City = this.address.city;
            if(!String.isBlank(this.address.district))lead.District__c = this.address.district;
            if(!String.isBlank(this.address.complements))lead.Complements__c = this.address.complements;
        }
        return lead;
    }

    public Contact parseToBillingContact() {
        Contact contact = new Contact(
            UUID__c = this.billing_id,
            FirstName = this.billing_firstname, 
            LastName = this.billing_lastname,
            Email = this.billing_email,
            Finance_Contact__c = true     
        );
        if(this.address != null) {
            contact.MailingStreet = this.address.street;
            contact.MailingState = this.address.state;
            contact.MailingPostalCode = this.address.postal_code;
            contact.MailingCountryCode = this.address.country_id;
            contact.MailingCity = this.address.city;
            contact.District__c = this.address.district;
            contact.Complements__c = this.address.complements;
        }
        return contact;
    }

    public List<String> getMissingRequiredFields() {
        List<String> missingRequiredFields = new List<String>();
    
        if (String.isBlank(this.client_id)) {
          missingRequiredFields.add('client_id');
        }
    
        if (String.isBlank(this.trade_name)) {
          missingRequiredFields.add('trade_name');
        }

        if (String.isBlank(this.billing_id)) {
            missingRequiredFields.add('billing_id');
        }

        if (String.isBlank(this.billing_firstname)) {
            missingRequiredFields.add('billing_firstname');
        }

        if (String.isBlank(this.billing_lastname)) {
            missingRequiredFields.add('billing_lastname');
        }

        if (String.isBlank(this.billing_email)) {
            missingRequiredFields.add('billing_email');
        }

        if (String.isBlank(this.business_unit)) {
            missingRequiredFields.add('business_unit');
        }
    
        if (String.isBlank(this.currency_id)) {
          missingRequiredFields.add('currency_id');
        }

        if (this.number_of_employees == null) {
            missingRequiredFields.add('number_of_employees');
        }

        if (String.isBlank(this.language)) {
            missingRequiredFields.add('language');
        }
    
        if (this.contact == null) {
          missingRequiredFields.add('contact');
        }

        if (this.sales_flow == null) {
          missingRequiredFields.add('sales_flow');
        }

        if (this.number_of_employees_rounded == null) {
          missingRequiredFields.add('number_of_employees_rounded');
        }
    
        return missingRequiredFields;
      }
    
    public List<String> getMissingRequiredFieldsOnSalesCompleted() {
        List<String> missingRequiredFields = getMissingRequiredFields();

        if (String.isBlank(this.legal_document)) {
            missingRequiredFields.add('legal_document');
        }

        return missingRequiredFields;
    }

    public class ContactDTO {
        private String contact_id;
        private String first_name;
        private String last_name;
        private String phone;
        private String email;
        private String job_title;

        public List<String> getMissingRequiredFields() {
            List<String> missingRequiredFields = new List<String>();
      
            if (String.isBlank(this.contact_id)) {
              missingRequiredFields.add('contact_id');
            }
      
            if (String.isBlank(this.first_name)) {
              missingRequiredFields.add('first_name');
            }
      
            if (String.isBlank(this.last_name)) {
              missingRequiredFields.add('last_name');
            }
      
            if (String.isBlank(this.email)) {
              missingRequiredFields.add('email');
            }
      
            return missingRequiredFields;
        }

	  }

  public class AddressDTO {
    private String street;
    private String district;
    private String country_id;
    private String complements;
    private String postal_code;
    private String city;
    private String state;

    public List<String> getMissingRequiredFields() {
      List<String> missingRequiredFields = new List<String>();
      
      if (String.isBlank(this.country_id)) {
        missingRequiredFields.add('country_id');
      }
      
      return missingRequiredFields;
    }
  }

}