public without sharing class LegacyPartnerPriceRepository {    
    public LegacyPartnerPriceRepository() {
        
    }
    
    public List<Legacy_Partner_Price__c> isPartnerByAccountId(Id accountId, String currencyIsoCode){
        return [SELECT id, name,Account__c, CurrencyIsoCode, Legacy_Price__c, isActive__c FROM Legacy_Partner_Price__c WHERE Account__c = :accountId AND CurrencyIsoCode = :currencyIsoCode AND Business_Model__c = 'Subsidy' AND isActive__c = true];
    }

    public List<Legacy_Partner_Price__c> getExclusivePrice(Id accountId, String currencyIsoCode){
        return [SELECT Id, Name,Account__c, CurrencyIsoCode, Legacy_Price__c FROM Legacy_Partner_Price__c WHERE Account__c = :accountId AND CurrencyIsoCode = :currencyIsoCode AND Business_Model__c = 'Exclusive' AND isActive__c = true];
    }
    
    public Map<String, Legacy_Partner_Price__c> getLegacyPriceMapByAccountId(Id accountId){
        
        Map<String, Legacy_Partner_Price__c> currencyCodeToLegacyPriceMap = new Map<String, Legacy_Partner_Price__c>();
        for(Legacy_Partner_Price__c price : [SELECT id, name,Account__c, CurrencyIsoCode, Legacy_Price__c, isActive__c FROM Legacy_Partner_Price__c WHERE Account__c = :accountId AND Business_Model__c =: ResellerEnumRepository.AccountRequestPartnerModel.Subsidy.name() AND isActive__c = true])
            currencyCodeToLegacyPriceMap.put(price.CurrencyIsoCode, price);
        
            return currencyCodeToLegacyPriceMap;
    }

    public Map<String, Legacy_Partner_Price__c> getExclusivePriceMapByAccountId(Id accountId){
        Map<String, Legacy_Partner_Price__c> currencyCodeToLegacyPriceMap = new Map<String, Legacy_Partner_Price__c>();
        for(Legacy_Partner_Price__c price : [SELECT Id, Name,Account__c, CurrencyIsoCode, Legacy_Price__c FROM Legacy_Partner_Price__c WHERE Account__c = :accountId AND Business_Model__c =: ResellerEnumRepository.AccountRequestPartnerModel.Exclusive.name() AND isActive__c = true])
            currencyCodeToLegacyPriceMap.put(price.CurrencyIsoCode, price);
        
        return currencyCodeToLegacyPriceMap;
    }

    public List<Legacy_Partner_Price__c> getAllByAccountId(Id accountId){
        return [SELECT id, name,Account__c, CurrencyIsoCode, Legacy_Price__c, isActive__c, Business_Model__c FROM Legacy_Partner_Price__c WHERE Account__c = :accountId AND isActive__c = true];
    }
}