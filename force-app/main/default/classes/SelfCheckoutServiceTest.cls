@IsTest
public class SelfCheckoutServiceTest {
  
  @TestSetup
  public static void makeData() {
    Account gympassEntity = AccountMock.getGympassEntity();
    gympassEntity.CurrencyIsoCode = 'BRL';
    gympassEntity.UUID__c = SelfCheckoutRequestMock.GYMPASS_ENTITY_UUID;
    insert gympassEntity;

    Account account = AccountMock.getStandard('Empresas');
    account.CurrencyIsoCode = 'BRL';
    account.UUID__c = SelfCheckoutRequestMock.ACCOUNT_UUID;

    insert account;

    Contact contact = ContactMock.getStandard(account);

    insert contact;

    account.Attention__c = contact.Id;

    update account;

    Pricebook2 pricebook = new Pricebook2(
      CurrencyIsoCode = 'BRL',
      Id = Test.getStandardPricebookId(),
      IsActive = true
    );

    update pricebook;

    List<Product2> products = new List<Product2>();

    Product2 setupFee = ProductMock.getSetupFee();
    setupFee.Minimum_Number_of_Employees__c = 0;
    setupFee.Maximum_Number_of_Employees__c = 10000;
    setupFee.IsActive = true;
    setupFee.Family = 'Enterprise Subscription';
    setupFee.Copay2__c = false;
    setupFee.Family_Member_Included__c = false;
    products.add(setupFee);

    Product2 accessFee = ProductMock.getAccessFee();
    accessFee.Minimum_Number_of_Employees__c = 0;
    accessFee.Maximum_Number_of_Employees__c = 10000;
    accessFee.IsActive = true;
    accessFee.Family = 'Enterprise Subscription';
    accessFee.Copay2__c = false;
    accessFee.Family_Member_Included__c = false;
    products.add(accessFee);

    insert products;

    List<PricebookEntry> pricebookEntries = new List<PricebookEntry>();

    PricebookEntry setupFeePricebookEntry = PricebookMock.getPricebookEntry(
      pricebook,
      setupFee
    );
    setupFeePricebookEntry.UnitPrice = 10;
    pricebookEntries.add(setupFeePricebookEntry);

    PricebookEntry accessFeePricebookEntry = PricebookMock.getPricebookEntry(
      pricebook,
      accessFee
    );
    accessFeePricebookEntry.UnitPrice = 10;
    pricebookEntries.add(accessFeePricebookEntry);

    insert pricebookEntries;

    Opportunity opp = OpportunityMock.getNewBusiness(account, pricebook);
    opp.CurrencyIsoCode = 'BRL';
    opp.StageName = 'Lançado/Ganho';
    opp.UUID__c = SelfCheckoutRequestMock.OPPORTUNITY_UUID;

    insert opp;

    Quote qt = QuoteMock.getStandard(opp);
    insert qt;

    QuoteLineItem qtItem = QuoteLineItemMock.getSetupFee(qt,setupFeePricebookEntry);
    insert qtItem;

    Payment__c payment = PaymentMock.getStandard(qtItem);
    insert payment;

    Waiver__c waiver = WaiverMock.getStandard(payment);
    insert waiver;
  }

  @IsTest
  public static void execute() {
    User integrationSMBJamorUser = [
      SELECT Id
      FROM User
      WHERE Name = 'Integration SMB Jamor'
    ];

    Test.startTest();

    System.runAs(integrationSMBJamorUser) {
      new SelfCheckoutService(SelfCheckoutRequestMock.getMock()).execute();
    }

    Test.stopTest();

    Opportunity opportunity = [SELECT Id, StageName FROM Opportunity LIMIT 1];

    System.assertEquals(
      'Lançado/Ganho',
      opportunity.StageName,
      'The opportunity is not closed'
    );
  }
}