/**
 * @File Name          : AccountTriggerHandler.cls
 * @Description        :
 * @test classes       : AccountServiceTest
 * @Author             : GEPI@GFT.com
 * @Group              :
 * @Last Modified By   : alysson.mota@gympass.com
 * @Last Modified On   : 05-19-2022
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    07/05/2020   GEPI@GFT.com     Initial Version
 **/
public class AccountTriggerHandler extends TriggerHandler {
  public override void beforeInsert() {
    new AccountDefaultFieldsHelper().assignGympassEntity();
    if(System.isBatch() == false)new AccountValidator().resellerAccountValidator(Trigger.new);
    new PartnerLeadsConversion().updateAccountAddressFromBillingToShipping();
    new AccountTriggerCurrencyHelper()
      .setCurrencyOnBeforeInsert();
      new AccountService()
      .avoidDuplicateAccountName(Trigger.new);
     new AccountService().wishListAndTierDependency(Trigger.new);
     //new AccountService().avoidDuplicatedNetworkIds(Trigger.new);
    new AccountService().setAccountTypeProspect(Trigger.new);
    new AccountService().getAccountWishlistFromParent(Trigger.new);

  }

  public override void beforeUpdate() {
    new AccountTriggerCurrencyHelper()
      .setCurrencyOnBeforeUpdate();
    new AccountService().wishListAndTierDependency(Trigger.new);
    //new AccountService().avoidDuplicatedNetworkIds((Map<Id, Account>)Trigger.newMap, (Map<Id, Account>)Trigger.oldMap);
  }

  public override void afterInsert(){
    
  }

  public override void afterUpdate() {
    new AccountTriggerCurrencyHelper()
      .setCurrencyForOppsOnAfterUpdate();

    //new AccountTriggerLocationsHelper().populateOwnerField();

    new AccountTriggerGetFeedback()
    .accountChangeClient(
        Trigger.new,
        (Map<Id, Account>) Trigger.oldMap
    );


    new PublishAccountOnTagusEvent().execute();
   new PublishAccountPartnerOnTagusEvent().execute();

  }

}