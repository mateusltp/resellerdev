public class CountriesByStatesByCitiesWrapper {
    public String Country;
    public String State;
    public String City;

    public static void CountriesByStatesByCitiesWrapper(){
    }

    public static List<CountriesByStatesByCitiesWrapper> parse(Blob csvCitiesBlob){
        
        List<CountriesByStatesByCitiesWrapper> citiesDTO = new List<CountriesByStatesByCitiesWrapper>();
        String allCitiesString = csvCitiesBlob.toString();
        allCitiesString = allCitiesString.replace('"', '').replace('\r','');
        String[] allCitiesList = allCitiesString.split('\n');
        allCitiesList.remove(0);         //to remove header
        for(String city : allCitiesList){
            CountriesByStatesByCitiesWrapper cityDTO = new CountriesByStatesByCitiesWrapper();
            String[] cityParamenters = city.split(',');
            cityDTO.Country = cityParamenters[0];
            cityDTO.State = cityParamenters[1];
            cityDTO.City = cityParamenters[2];
            citiesDTO.add(cityDTO);
        }
        return citiesDTO;

    }


}