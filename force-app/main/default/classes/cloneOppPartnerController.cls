/**
 * @description       : 
 * @author            : pohr@gft.com
 * @group             : 
 * @last modified on  : 07-23-2021
 * @last modified by  : gft.samuel.silva@ext.gympass.com
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   12-05-2021   pohr@gft.com   Initial Version
**/

public without sharing class cloneOppPartnerController {

    @AuraEnabled
    public static String getFieldOptions(String objName, String fieldApiName, String RecordTypeDevName){ 
        System.debug('cloneOppPartnerController >>>>>> ' + objName + ' ' + fieldApiName + ' ' + RecordTypeDevName);
        List<PicklistOption> lstPickValues = new List<PicklistOption>();
        try {
            Map<String, List<String>> response = Utils.getPicklistValues(objName,fieldApiName,RecordTypeDevName, false);
             System.debug('response >>>>>> ' + response);
      
            for(String option : response.get(fieldApiName)){
                System.debug('option >>>>>> ' + option);
                PicklistOption pickValue = new PicklistOption(option, option);   
                 System.debug('pickValue >>>>>> ' + pickValue);
                lstPickValues.add(pickValue);
            }
            return JSON.serialize(lstPickValues);
        } catch (Exception e){
            throw new AuraHandledException(e.getMessage());
        } 
    }


    @AuraEnabled
    public static Map<String,List<String>> getDependentPickList(String objName, String fieldApiName, String RecordTypeDevName){    
        List<PicklistOption> lstPickValues = new List<PicklistOption>();
        try {
            return Utils.getPicklistValues(objName,fieldApiName, RecordTypeDevName, true);
        } catch (Exception e){
            throw new AuraHandledException(e.getMessage());
        } 
    }

    @AuraEnabled
    public static String createNewPartnerRenegotiationOpp( String aAccId , String aRecordTypeDevName, String aSubTypeValue, String aCancellationReason, String aCancellationSubCateg, String aOppSource ){
        Id rtId =  Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(aRecordTypeDevName).getRecordTypeId();
        Opportunity formOpp = new Opportunity(AccountId = aAccId, recordTypeId = rtId, Sub_Type__c = aSubTypeValue, Cancellation_Reason__c = aCancellationReason, Cancellation_Reason_subcategory__c = aCancellationSubCateg, Source__c = aOppSource);    
        try{            
            List< Opportunity > lLstOpp = 
                OpportunityService.openRenegotiation(new Map<Id, Opportunity>{aAccId => formOpp}, aRecordTypeDevName);              
                return lLstOpp[0].Id;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    public class PicklistOption{
        public PicklistOption( String aLabel , String aValue ){
            label = aLabel;
            value = aValue;
        }
        public String label{get;set;}
        public String value{get;set;}
    }

}