/**
 * @description       : 
 * @author            : roei@gft.com
 * @group             : 
 * @last modified on  : 01-22-2021
 * @last modified by  : roei@gft.com
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   01-22-2021   roei@gft.com   Initial Version
**/
@isTest(seeAllData=false)
public class LeadAssignToMeControllerTest {
	@isTest
    public static void shouldAssignLeadToCurrentUser() {
        Profile lStandardProfile = [ SELECT Id FROM Profile WHERE Name = 'Standard User' ];
        User lTestUser = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',                          
            LocaleSidKey = 'en_US', TimeZoneSidKey = 'America/Los_Angeles',
            ProfileId = lStandardProfile.Id, UserName='standardUserLead@testorg.com');
        
        Database.insert( lTestUser );
        
    	Lead lNewLead = new Lead(
            LastName='Sales Lead',
            RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Client').getRecordTypeId(), 
            Country = 'Brazil',
            State = 'São Paulo',
            Street = 'Av Paulista 283',
            City = 'Sorocaba',                 
            Phone = '(011)3098-1423',
            Email = 'contact123@test.com',
            Company = 'Test Account'
            );
        Database.insert( lNewLead );
        
        Test.startTest();
        	System.runAs( lTestUser ) {
            	LeadAssignToMeController.assingToMe( lNewLead.Id );
            }
        Test.stopTest();
        
        lNewLead = [ SELECT OwnerId FROM Lead WHERE Id =: lNewLead.Id ];
        
        System.assertEquals( lTestUser.Id , lNewLead.OwnerId , 'Couldn\'t assign Lead to User' );
    }
}