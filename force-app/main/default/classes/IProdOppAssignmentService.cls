public interface IProdOppAssignmentService {
    void insertCurrencyProdOppAssignment(List<Product_Opportunity_Assignment__c> prodOppAssignmentLst);
    void updateCurrencyRelatedPartnerData(Map<Id, Product_Opportunity_Assignment__c> prodOppAssignmentMap);
    void updateCurrencyRelatedPartnerData(Map<Id, Product_Opportunity_Assignment__c> prodOppAssignmentNewMap, Map<Id, Product_Opportunity_Assignment__c> prodOppAssignmentOldMap);
}