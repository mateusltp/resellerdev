/**
 * @File Name          : ChildAccountCreationModel.cls
 * @Description        : Handles Clone Account and new fields update
 * @Author             : Samuel Silva - GFT (slml@gft.com)
 * @Group              : 
 * @Last Modified By   : gft.samuel.silva@ext.gympass.com
 * @Last Modified On   : 08-03-2021
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    23/03/2020   Samuel Silva - GFT (slml@gft.com)     Initial Version
**/
public without sharing class ChildAccountCreationModel {
    
    public class CustomFormChildAccountUnwrapper {
        public String parentID;        
        public String name;
        public Boolean isHybridModel;
        public Boolean isMultibrand;
        public Boolean isSetup;
        public String stateCode;
        public String state;
        public String countryCode;
        public String country;
        public String street;
        public String postalCode;
        public String city;
        public String creationReasonDetails;
        
        public CustomFormChildAccountUnwrapper(String newParentID, String newAccountName, Boolean newIsMultiBrand, Boolean newIsHybridModel, String newStateCode, String newCountryCode, String newStreet, String newCity, String newPostalCode, String newCreationReasonDetails, Boolean newIsSetup){
            this.parentID = newParentID;
            this.name = newAccountName;
            this.isMultibrand = newIsMultiBrand;
            this.isHybridModel = newIsHybridModel;           
            this.stateCode = newStateCode;          
            this.countryCode = newCountryCode;            
            this.street = newStreet;
            this.city = newCity;
            this.postalCode = newPostalCode;
            this.creationReasonDetails = newCreationReasonDetails;
            this.isSetup = newIsSetup;
        }
    }
    
    public static Map<String, String>  cloneParentAccount(Object formData){
        Map<String, String> resultMap = new Map<String, String>();
        CustomFormChildAccountUnwrapper accData = (CustomFormChildAccountUnwrapper) JSON.deserialize(JSON.serialize(formData),CustomFormChildAccountUnwrapper.class);
        Id recordId = accData.parentID;
        //ApprovalProcessHandler approver = new ApprovalProcessHandler(); 

        List<String> fields = new List<String>();

        fields.addAll(Utils.getSetWithAllFieldsFromObject('Account'));
        String soql = 'SELECT ' + String.join(fields, ',') + ' FROM Account WHERE Id =: recordId';
        Account parentToClone = Database.query(soql);    
        Account clonedAccount = parentToClone.clone(false, false, false, false);
        cleanAccountAddress(clonedAccount);
        clonedAccount.recordTypeId = parentToClone.RecordTypeId;
        populateAccountWithData(clonedAccount, accData);    
        Database.SaveResult sr = Database.insert(clonedAccount, false);
        String message = '';
        Id cccountId = null;
        if (sr.isSuccess()) {
            System.debug('SUCCESS');
            System.debug('Successfully inserted account. Account ID: ' + sr.getId());
            //approver.submitApproval(clonedAccount);  
            //resultMap.put('SUCCESS', 'Account successfully submitted. Awaiting approval.');
            //resultMap.put('SUCCESS', 'New location created successfully.');
            resultMap.put('SUCCESS', sr.getId());
        } else {             
            for(Database.Error err : sr.getErrors()) {            
                System.debug('The following error has occurred.');                    
                System.debug(err.getStatusCode() + ': ' + err.getMessage()); 
                System.debug('Account fields that affected this error: ' + err.getFields());
                resultMap.put('ERROR', err.getMessage());
            }            
        } 
        
       return resultMap;
        
    }
    
    private static void cleanAccountAddress(Account clonedAccount){
        clonedAccount.Gym_Classes__c = '';
        ClonedAccount.Gym_Type__c = '';               
        clonedAccount.ShippingCountry = '';
        clonedAccount.ShippingCountryCode = '';
        clonedAccount.ShippingStateCode = '';
        clonedAccount.ShippingState = '';
        clonedAccount.ShippingStreet = '';
        clonedAccount.ShippingCity = '';
        clonedAccount.ShippingPostalCode = '';    
        clonedAccount.CRM_ID__c ='';   
    }

    /**
     * This method populates the new account with data from form
     */
    private static void populateAccountWithData(Account clonedAccount, CustomFormChildAccountUnwrapper accData){
        clonedAccount.parentId = accData.parentID;    
        clonedAccount.GP_Status__c = 'Active';
        clonedAccount.Name = accData.name;    
        clonedAccount.IsMultipleBrand__c = accData.isMultibrand;
        clonedAccount.HybridModel__c = accData.isHybridModel;  
        clonedAccount.Setup__c = accData.isSetup; 
        clonedAccount.ShippingCountryCode = accData.countryCode;
        clonedAccount.ShippingStateCode = accData.stateCode;
        clonedAccount.ShippingStreet = accData.street;
        clonedAccount.ShippingCity = accData.city;
        clonedAccount.ShippingPostalCode = accData.postalCode;
        clonedAccount.Creation_Reason_Details__c = accData.creationReasonDetails;
        clonedAccount.Number_of_Locations__c = 0;
        clonedAccount.Partner_Level__c = 'Location';
    }
}