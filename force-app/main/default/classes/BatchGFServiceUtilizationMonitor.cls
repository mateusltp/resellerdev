/**
* @author vncferraz
*
*   Generate records to provide information to business which contacts will receive which survey on getFeedback
**/
global class BatchGFServiceUtilizationMonitor implements Database.Batchable<sObject> {

    global Integer daysToAdd;

    global BatchGFServiceUtilizationMonitor(Integer daysToAdd){
        this.daysToAdd = daysToAdd;
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        
		Id rtAcc = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();        
        Date ed = (Date.today().addDays(this.daysToAdd)).addDays(90);
        Date ed2 = (Date.today().addDays(this.daysToAdd)).addDays(-119);
        
        return Database.getQueryLocator([
            SELECT Id, Data_do_Lancamento__c, AccountId, Account.RecordTypeId, RecordTypeId, StageName, Synced_Quote_End_Date__c,Account.EnvioServiceUtilization__c, Account.RespostaServiceUtilization__c
            FROM Opportunity
            WHERE  Data_do_Lancamento__c < :ed2 AND StageName = 'Lançado/Ganho' AND Account.RecordTypeId = :rtAcc
        ]);
    }
    
    global void execute(Database.BatchableContext bc, List<Opportunity> scope) {
        
        List<Account> lstAcc = new List<Account>();
        
        List<Contact> lstUpdateCon = new List<Contact>();
        List<Get_Feedback_Survey_Schedule__c> schedules = new List<Get_Feedback_Survey_Schedule__c>();

        Id rtOppClientNew = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        Id rtOppSMBNew = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SMB_New_Business').getRecordTypeId();
        Id rtOppClientReneg = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Success_Renegotiation').getRecordTypeId();
        Id rtOppSMBReneg = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SMB_Success_Renegotiation').getRecordTypeId(); 
        
        Id rtAcc = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
        
        for(Opportunity o: scope){
            
            if((o.RecordTypeId == rtOppClientNew || o.RecordTypeId == rtOppSMBNew || o.RecordTypeId == rtOppClientReneg || o.RecordTypeId == rtOppSMBReneg)){
                   if(o.Data_do_Lancamento__c.addDays(120) == Date.today() && o.Account.EnvioServiceUtilization__c == null){
                       o.Account.EnvioServiceUtilization__c = System.today();
                       lstAcc.add(o.Account);
                       schedules.add( 
                        new Get_Feedback_Survey_Schedule__c(Account__c=o.AccountId,
                                                             Survey_Name__c='Client Service Utilization',
                                                             Scheduled_Date__c=Date.today().addDays(this.daysToAdd))
                    ); 
                   }
                   if(o.Data_do_Lancamento__c.addDays(120) < Date.today() && o.Account.EnvioServiceUtilization__c != null){
                      
                       if(o.Account.EnvioServiceUtilization__c.addDays(30) == Date.today() && 
                          (o.Account.EnvioServiceUtilization__c > o.Account.RespostaServiceUtilization__c || o.Account.RespostaServiceUtilization__c == null)){
                              o.Account.EnvioServiceUtilization__c = System.today();
                              lstAcc.add(o.Account);
                              schedules.add( 
                                    new Get_Feedback_Survey_Schedule__c(Account__c=o.AccountId,
                                                                        Survey_Name__c='Client Service Utilization',
                                                                        Scheduled_Date__c=Date.today().addDays(this.daysToAdd))
                                ); 
                          }
                       if(o.Account.EnvioServiceUtilization__c.addDays(90) == Date.today() &&
                          o.Account.EnvioServiceUtilization__c <= o.Account.RespostaServiceUtilization__c){
                              o.Account.EnvioServiceUtilization__c = System.today();
                              lstAcc.add(o.Account);
                              schedules.add( 
                                    new Get_Feedback_Survey_Schedule__c(Account__c=o.AccountId,
                                                                        Survey_Name__c='Client Service Utilization',
                                                                        Scheduled_Date__c=Date.today().addDays(this.daysToAdd))
                                );
                          }
                   }
               }
        }
        system.debug('-------------' + lstAcc);
        
        if(!lstAcc.isEmpty()){
            for(Contact con: [Select Id, Status_do_contato__c, GFSendSurvey__c, Email, Primary_HR_Contact__c,DecisionMaker__c, Role__c FROM Contact WHERE AccountID IN :lstAcc]){
                if(con.Status_do_contato__c == 'Ativo' && con.Email != null && con.Primary_HR_Contact__c == TRUE){
                    schedules.add( 
                        new Get_Feedback_Survey_Schedule__c(Contact__c=con.Id,
                                                             Survey_Name__c='Client Service Utilization',
                                                             Scheduled_Date__c=Date.today().addDays(this.daysToAdd))
                    );                  
                }
            }
        }
       
        if(!schedules.isEmpty()){
            insert schedules;
        }
    }
    
    global void finish(Database.BatchableContext bc) {
        
    }
}