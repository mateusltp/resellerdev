/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 09-03-2021
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/
public with sharing class TagusPartnerTransactionData {

    private List<AccountPartnerTagusDTO> partners;

    public TagusPartnerTransactionData(Map<Id, Account> partnerMap) {
        setPartners(partnerMap);
    }

       
    public List<AccountPartnerTagusDTO> getPartners() {
        return this.partners;
    }


    private void setPartners(Map<Id, Account> partnerMap) {
        this.partners = new List<AccountPartnerTagusDTO>();  
        Map<String,String> mapPartnerTypeTagus = new Map<String, String>();
        for(Tagus_Integration_Partner_Type__mdt iMdt : Tagus_Integration_Partner_Type__mdt.getAll().values()){
            mapPartnerTypeTagus.put(iMdt.label, iMdt.Partner_Type_Tagus__c);
        }

        for (Account iPartner : partnerMap.values()) {      
            AccountPartnerTagusDTO partnerDTO = new AccountPartnerTagusDTO(iPartner);       
            partnerDTO.setPartnerType('FULL_SERVICE');//mapPartnerTypeTagus.get(partnerDTO.getPartnerType()));            
            this.partners.add(partnerDTO);
        }
    }


}