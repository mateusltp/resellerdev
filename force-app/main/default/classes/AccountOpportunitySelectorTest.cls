/**
 * @description       : 
 * @author            : gft.jorge.stevaux@ext.gympass.com
 * @group             : 
 * @last modified on  : 03-23-2022
 * @last modified by  : alysson.mota@gympass.com
**/
@isTest(seeAllData=false)
private class AccountOpportunitySelectorTest {

    @TestSetup
    static void createData(){
            
        Account lAcc = PartnerDataFactory.newAccount();  
        lAcc.Name = 'Parent Acct';            
        Database.insert( lAcc );

        Account lChildAcct = PartnerDataFactory.newChildAccount(lAcc.Id);
        lChildAcct.Name = 'Child Acct';
        Database.insert( lChildAcct );

        Contact lContact = PartnerDataFactory.newContact(lAcc.Id);
        Database.insert(lContact);

        Bank_Account__c lBankAcct = PartnerDataFactory.newBankAcct();
        Database.insert(lBankAcct);

        Acount_Bank_Account_Relationship__c lBankAcctRel = PartnerDataFactory.newBankAcctRel(lAcc.Id, lBankAcct);
        Database.insert(lBankAcctRel);
                                  
        Opportunity lOpp = PartnerDataFactory.newOpportunity( lAcc.Id, 'Partner_Wishlist_Renegotiation' );     
        Database.insert( lOpp );

        Account_Opportunity_Relationship__c lAcctOppRel = PartnerDataFactory.newAcctOppRel(lAcc.Id, lOpp.Id);
        Database.insert(lAcctOppRel);
        
    }


    @isTest 
    static void getSObjectFieldList_Test(){

        List<Schema.SObjectField> schemaLst = new List<Schema.SObjectField> {
			Account_Opportunity_Relationship__c.Id,
			Account_Opportunity_Relationship__c.Name,
            Account_Opportunity_Relationship__c.Opportunity__c,
            Account_Opportunity_Relationship__c.Account__c
		};

        Test.startTest();
        System.assertEquals(schemaLst, new AccountOpportunitySelector().getSObjectFieldList());
        Test.stopTest();
    }

    @isTest 
    static void getSObjectType_Test(){
        
        Test.startTest();
        System.assertEquals(Account_Opportunity_Relationship__c.sObjectType, new AccountOpportunitySelector().getSObjectType());
        Test.stopTest();
    }

    @isTest 
    static void selectByOppIdToClone_Test(){

        List<Opportunity> oppLst = [SELECT Id, Name FROM Opportunity LIMIT 1];
        Set<Id> oppIds = (new Map<Id, Opportunity>(oppLst)).keySet();
        List<Account_Opportunity_Relationship__c> acctOppRel = [SELECT Id, Name, CurrencyIsoCode, OwnerId, IsDeleted, RecordTypeId,CreatedDate, 
                                                                        CreatedById, LastModifiedDate, LastModifiedById, SystemModstamp, LastViewedDate,
                                                                        LastReferencedDate, Account__c, Opportunity__c, Main_Order__c 
                                                                        FROM Account_Opportunity_Relationship__c 
                                                                        WHERE Opportunity__c IN : oppIds ];

        Test.startTest();
        System.assertEquals(acctOppRel[0].id, new AccountOpportunitySelector().selectByOppIdToClone(oppIds)[0].id);
        Test.stopTest();
    }

    @isTest
    static void selectExistingOpportunityMemberByAccountId_Test(){
        List<Opportunity> oppLst = [SELECT Id, AccountId, Name FROM Opportunity LIMIT 1];
        Set<Id> oppIds = (new Map<Id, Opportunity>(oppLst)).keySet();
        List<Account_Opportunity_Relationship__c> acctOppRel = [SELECT Id, Account__c, Opportunity__c
                                                                    FROM Account_Opportunity_Relationship__c 
                                                                    WHERE Opportunity__c IN : oppIds ];        
        Test.startTest();
            System.assertEquals(acctOppRel[0].Id, new AccountOpportunitySelector().selectExistingOpportunityMemberByAccountId(new Set<Id>{oppLst[0].AccountId}).get(acctOppRel[0].Id).Id);
        Test.stopTest();
        
    }

    @IsTest
    static void selectById_Test() {
        List<Opportunity> oppLst = [SELECT Id, Name FROM Opportunity LIMIT 1];
        Set<Id> oppIds = (new Map<Id, Opportunity>(oppLst)).keySet();
        List<Account_Opportunity_Relationship__c> acctOppRel = [SELECT Id
                                                                FROM Account_Opportunity_Relationship__c
                                                                WHERE Opportunity__c IN : oppIds ];
        Set<Id> acctOppRelIds = (new Map<Id, Account_Opportunity_Relationship__c>(acctOppRel)).keySet();

        Test.startTest();
        System.assertEquals(acctOppRel[0].id, new AccountOpportunitySelector().selectById(acctOppRelIds)[0].id);
        Test.stopTest();
    }

    @IsTest
    static void selectProductAssignmentToTagus_Test() {
        List<Opportunity> oppLst = [SELECT Id, Name FROM Opportunity LIMIT 1];
        Set<Id> oppIds = (new Map<Id, Opportunity>(oppLst)).keySet();

        Test.startTest();
        System.assert(new AccountOpportunitySelector().selectProductAssignmentToTagus(oppIds).keySet().contains(oppLst[0].Id));
        Test.stopTest();
    }
}