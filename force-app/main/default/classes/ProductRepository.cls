public with sharing class ProductRepository {
    


    public static List<Product2> getProduct2AccessFeeByEligiblesQuantity(Decimal eligiblesQuantityMax, Decimal eligiblesQuantityMin){

        return [    SELECT Id 
                    FROM Product2 
                    WHERE Family_Member_Included__c = false 
                    AND Minimum_Number_of_Employees__c <=: eligiblesQuantityMin
                    AND Maximum_Number_of_Employees__c >=: eligiblesQuantityMax
                    AND Family = 'Enterprise Subscription' ];

    }


}