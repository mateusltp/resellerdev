/**
 * @description       : 
 * @author            : AlyssonMota
 * @group             : 
 * @last modified on  : 11-26-2020
 * @last modified by  : Alysson Mota
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   11-26-2020   Alysson Mota   Initial Version
**/
@isTest
public with sharing class ClientsEnablersRelatedListControllerTest {
    
    @isTest
    public static void testGetEnablers() {
        Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();

        Account acc = new Account();
        acc.Name = 'Account Test';
        acc.BillingState = 'São Paulo';
        acc.BillingCountry = 'Brazil';
        acc.RecordTypeId = accRecordTypeId;
        insert acc;

        ClientsEnablersRelatedListController.getEnablers((String)acc.Id);
    }

     @isTest
    public static void testUpdateEnablerApx() {
        Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();

        Account acc = new Account();
        acc.Name = 'Account Test';
        acc.BillingState = 'São Paulo';
        acc.BillingCountry = 'Brazil';
        acc.RecordTypeId = accRecordTypeId;
        insert acc;

        EnablersRepository enablersRepo = new EnablersRepository();
        List<Step_Towards_Success1__c> steps = enablersRepo.getAccountEnablers(acc);

        if (steps.size() > 1) {
            ClientsEnablersRelatedListController.updateEnablerApx((String)steps.get(0).Id, true);
            ClientsEnablersRelatedListController.updateEnablerApx((String)steps.get(0).Id, false);
        }
    }
}