/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 07-30-2021
 * @last modified by  : gft.samuel.silva@ext.gympass.com
 * Modifications Log 
 * Ver   Date         Author                             Modification
 * 1.0   07-07-2021   gft.samuel.silva@ext.gympass.com   Initial Version
**/
public interface IAccountOpportunityRelationshipService {
   
    void createAccountOpportunityRelationship(Map<Id, Id> aAccountByOppIdMap );
    
}