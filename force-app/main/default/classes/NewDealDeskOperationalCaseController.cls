public without sharing class NewDealDeskOperationalCaseController {
    





    @AuraEnabled
    public static String getCaseRecordTypeId (String caseRecordTypeName){
   
        String caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(caseRecordTypeName).getRecordTypeId();

        return caseRecordTypeId;
    }

    @AuraEnabled
    public static Map<String, String> getPreviousDealDeskOperationalCase(String recordId){
   
        String caseObjectName = 'Case';
        List<String> caseFieldsAPINameToPrepopulate = new List<String>();

        List<Schema.FieldSetMember> caseFieldSetFields =   SObjectType.Case.FieldSets.New_Deal_Desk_Operational_Case.getFields();
      
        for(Schema.FieldSetMember f : caseFieldSetFields) {
            caseFieldsAPINameToPrepopulate.add(f.getFieldPath());
        }

        List<Assert_Data__c> changesOnPreviousCase = [SELECT Id ,Old_Value__c, Field_Name__c, CreatedDate 
                                                        FROM Assert_Data__c Where Opportunity__c = :recordId 
                                                        AND Object_Name__c = :caseObjectName
                                                        AND Field_Name__c IN : caseFieldsAPINameToPrepopulate
                                                        ORDER BY createdDate DESC];


        Map<String, String> valueOfLastChangesOfPreviousCaseByFieldName = New Map<String,String>();


        for(Assert_Data__c oneChange :changesOnPreviousCase){
            String fieldNameOneChange = oneChange.Field_Name__c;
                valueOfLastChangesOfPreviousCaseByFieldName.put(fieldNameOneChange, oneChange.Old_Value__c);
        }

        return valueOfLastChangesOfPreviousCaseByFieldName;

    }

    @AuraEnabled
    public static String getQueueId (String queueDeveloperName){
        String queueId;
        String queueType = 'Queue';

        List<Group> queueDetails = [SELECT Id FROM Group WHERE TYPE =:queueType AND DeveloperName = :queueDeveloperName];

        if(queueDetails.size() > 0 ){
            queueId = queueDetails.get(0).Id;
        }
        return queueId;
    }


    @AuraEnabled
    public static String getDefaultQuoteId (String opportunityId){
   
        String defaultQuoteId;
        List<Quote> defaultQuote = [SELECT Id FROM Quote WHERE OpportunityId = :opportunityId LIMIT 1];

        if(defaultQuote.size()>0){
            defaultQuoteId = defaultQuote.get(0).Id;
        }
        return defaultQuoteId;
    }


    @AuraEnabled
    public static List<QuoteLineItem> getQuoteLineItems (String opportunityId){
        
        QuoteLineItemRepository repository = new QuoteLineItemRepository();
        List<QuoteLineItem> quoteLineItems = repository.allForOpportunity(opportunityId);

        return quoteLineItems;
    }

    @AuraEnabled
    public static String getMembershipFeePaymentMethod (String opportunityId){
        String membershipFeePaymentMethod;
        EligibilityRepository repository = new EligibilityRepository();
        List<Eligibility__c> eligibilities = repository.getEligibilitiesForOpportunity(opportunityId);

        for(Eligibility__c eligibility : eligibilities){
            if(eligibility.Payment__r.Quote_Line_Item__r.Fee_Type__c == 'Enterprise Subscription'){
                membershipFeePaymentMethod = eligibility.Payment_Method__c ;
                break;
            }
        }
        return membershipFeePaymentMethod;
    }

    @AuraEnabled
    public static String getBusinessModel (String opportunityId){
        String businessModel;
        List<Opportunity> BM = [SELECT B_M__c FROM Opportunity WHERE Id = :opportunityId LIMIT 1];
        businessModel = BM.get(0).B_M__c;
        return businessModel;
    }
}