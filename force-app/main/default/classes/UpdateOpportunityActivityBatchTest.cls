@isTest
private class UpdateOpportunityActivityBatchTest { 
    @testSetup
    static void setup() {
        User thisUser = [SELECT Id FROM User WHERE Id = : UserInfo.getUserId()];
        
        System.runAs(thisUser){    
            Account account = AccountMock.getStandard('Empresas');
            account.CurrencyIsoCode = 'BRL';
            insert account;
            
            Pricebook2 standardPricebook = new Pricebook2(
                Id = Test.getStandardPricebookId(),
                IsActive = true
            );
            update standardPricebook; 

            Product2 prod = new Product2();
            prod.Name = 'Enterprise Subscription';
            prod.Family_Member_Included__c = false;
            prod.Minimum_Number_of_Employees__c = 0;
            prod.Maximum_Number_of_Employees__c = 1000;
            prod.IsActive = true;
            insert prod;
            
            PricebookEntry standardPBE = new PricebookEntry(Pricebook2Id = standardPricebook.Id, Product2Id = prod.Id, UnitPrice = 8, IsActive = true, UseStandardPrice = false, CurrencyIsoCode = 'BRL');
            insert standardPBE;
            
            SKU_Price__c sku = new SKU_Price__c();
            sku.Minimum_Quantity__c = 0;
            sku.Product__c = prod.Id;
            sku.Start_Date__c = Date.Today() - 10;
            sku.CountrySKU__c = 'Brazil';
            sku.Unit_Price__c = 1;
            sku.CurrencyIsoCode = 'BRL';
            insert sku;
            
            Id recordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SMB_SKU_New_Business').getRecordTypeId();
            
            Opportunity opp = new Opportunity(
                Name='Teste Opp', 
                FastTrackStage__c = 'Offer Creation',
                RecordTypeId=recordTypeId, 
                OwnerId = thisUser.Id, 
                StageName = 'Qualification', 
                CloseDate = Date.Today().addDays(90), 
                AccountId = account.Id, 
                Pricebook2Id = standardPricebook.Id,
                TotalOpportunityQuantity = 50,
                CurrencyIsoCode = 'BRL'
            );
            insert opp;
            
            
            Task t = new Task();
            t.ActivityDate = Date.today();
            t.Subject = 'Call';
            t.Status = 'Concluído';
            t.WhatId = opp.Id;
            insert t;
        }
    }
    
    @isTest static void testSchedule() {
        Test.startTest();
        String jobId = System.schedule('testUpdateOpportunityActivityBatch', UpdateOpportunityActivityBatch.CRON_EXP, new UpdateOpportunityActivityBatch());
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        Opportunity opp = [SELECT LastActivityDate, DaysWithoutActivity__c FROM Opportunity LIMIT 1];
        System.assertEquals(UpdateOpportunityActivityBatch.CRON_EXP, ct.CronExpression);
        System.assertNotEquals(opp.LastActivityDate, Null);
        Test.stopTest();
    }
}