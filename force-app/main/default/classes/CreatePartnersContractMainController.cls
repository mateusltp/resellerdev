/**
 * @description       : Controller for contract creation in partners flow
 * @author            : alysson.mota@gympass.com
 * @group             : 
 * @last modified on  : 06-10-2022
 * @last modified by  : alysson.mota@gympass.com
**/
public with sharing class CreatePartnersContractMainController {
    private static Id childPartnerOppRecTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Child_Opportunity').getRecordTypeId();
    private static PS_Constants constants = PS_Constants.getInstance();

    /**
    * Initialize TO and sends first version to front end 
    * @author alysson.mota@gympass.com | 03-09-2022 
    * @param oppId 
    * @return ContractCreationTO 
    **/
    @AuraEnabled(cacheable=true)
    public static ContractCreationTO getContractToInstance(Id oppId) {
       
        ContractCreationTO to = new ContractCreationTO(oppId);

        AccountSelector accSelector = new AccountSelector();
        OpportunitySelector oppSelector = (OpportunitySelector)Application.Selector.newInstance(Opportunity.SObjectType);
        ContractAgreementSelector contractSelector = (ContractAgreementSelector)Application.Selector.newInstance(APXT_Redlining__Contract_Agreement__c.SObjectType);

        try {
            Opportunity opp = oppSelector.byId(new Set<Id>{oppId}).get(0);
            List<Account> accLst = accSelector.selectByIdForPartnersWithoutContactRels(new Set<Id>{opp.AccountId});
            List<APXT_Redlining__Contract_Agreement__c> contractLst = contractSelector.byOpportunityId(new Set<Id>{oppId});

            to.masterOpportunity = opp;
            to.masterAccount = accLst.get(0);
            to.contract.contractSobjectRecord = (contractLst.size() > 0 ? contractLst.get(0) : null);
            
            
            if (to.masterOpportunity.RecordTypeId == childPartnerOppRecTypeId) {
                to.locationsIdInChildOpp = getLocationsIdInChildOpp(to.masterOpportunity);
            }

        } catch(Exception e) {
            throw new AuraHandledException(e.getMessage());
        }

        return to;
    }

    private static Set<Id> getLocationsIdInChildOpp(Opportunity opp) {
        Set<Id> locationsIdInChildOpp = new Set<Id>();
        AccountOpportunitySelector accOppRelSelector = (AccountOpportunitySelector)Application.Selector.newInstance(Account_Opportunity_Relationship__c.sObjectType);

        List<Account_Opportunity_Relationship__c> accOppRelLst = accOppRelSelector.selectOppMemberByOppID(new Set<Id>{opp.Id});

        for (Account_Opportunity_Relationship__c accOppRel : accOppRelLst) {
            if (accOppRel.From_Child_Opp_Creation_Process__c) {
                locationsIdInChildOpp.add(accOppRel.Account__c);
            }
        }

        return locationsIdInChildOpp;
    }

    /**
    * Evaluate TO and returns result to front end before save records
    * @author alysson.mota@gympass.com | 03-09-2022 
    * @param oppId 
    * @return ContractCreationTO 
    **/
    @AuraEnabled(cacheable=true)
    public static ContractCreationTO evaluateTransferObj(Object data) {
        ContractCreationTO to = (ContractCreationTO) JSON.deserialize(JSON.serialize(data),ContractCreationTO.class);
        
        evaluateLegalRepsFromLocations(to);

        return to;
    } 

    /**
    * Search seleceted accounts from TO
    * @author alysson.mota@gympass.com | 03-09-2022 
    * @param to 
    * @return List<Account> 
    **/
    private static ContractCreationTO searchAccounts(ContractCreationTO to) {
        AccountSelector accSelector = new AccountSelector();
        List<Account> accounts = accSelector.selectByIdForPartnersWithoutContactRels(new Set<Id>(to.locationsAccountIdList));
        to.locations = accounts;
        return to;
    }

    /**
    * Eval Legal Reps from locations and returns TO with different legal reps  
    * @author alysson.mota@gympass.com | 03-10-2022 
    * @param to 
    * @return ContractCreationTO 
    **/
    private static ContractCreationTO evaluateLegalRepsFromLocations(ContractCreationTO to) {
        searchAccounts(to);

        to.locationsWithDifLegalRep = new List<Account>();

        // Iterate through locatons added into to in seaarchAccountsMethod 
        for (Account acc : to.locations) {
            if (to.masterOpportunity.RecordType.DeveloperName == 'Partner_Flow_Child_Opportunity') {
                if (acc.Legal_Representative__c != to.masterOpportunity.Legal_representative__c) {
                    to.locationsWithDifLegalRep.add(acc);
                }
            } else {
                if (acc.Legal_Representative__c != to.masterAccount.Legal_Representative__c) {
                    to.locationsWithDifLegalRep.add(acc);
                }
            }
        }

        return to;
    }

    /**
    * Create APXT_Redlining__Contract_Agreement__c, Account_Contract_Agreement_Relationship__c , Partner_Contract_Anexx_Info__c records
    * and update account legal reps if necessary
    * @author alysson.mota@gympass.com | 03-10-2022 
    * @param to 
    * @return ContractCreationTO 
    **/
    @AuraEnabled(cacheable=false)
    public static ContractCreationTO createContractRelatedRecordsAndUpdateLegalReps(Object data) {
        APXT_Redlining__Contract_Agreement__c newContract;
        ContractCreationTO to;
        
        fflib_ISObjectUnitOfWork uow = Application.UnitOfWork.newInstance();

        try {
            to = (ContractCreationTO) JSON.deserialize(JSON.serialize(data),ContractCreationTO.class);

            if (to.contract.type == 'Intention Contract') {
                newContract = getIntentionContractAndRegisterRecords(to, uow);
            } else {
                newContract = getContractAndRegisterRecords(to, uow); 
            }
                
            // Commit uow;
            uow.commitWork();

            // Update TO with new contract information to send back
            to.contract.contractSobjectRecord = newContract;
        } catch (Exception e) {
            System.debug(e.getMessage());
            System.debug(e.getStackTraceString());
            createLog(e, 'createContractRelatedRecordsAndUpdateLegalReps');
            throw new AuraHandledException(e.getMessage());
        }

        return to;
    }

    private static APXT_Redlining__Contract_Agreement__c getIntentionContractAndRegisterRecords(ContractCreationTO to, fflib_ISObjectUnitOfWork uow) {
        // Create APXT_Redlining__Contract_Agreement__c
        APXT_Redlining__Contract_Agreement__c newContract = createContractAgreementRecord(to);

        // Create Partner_Contract_Anexx_Info__c
        List<Partner_Contract_Anexx_Info__c> anexxInfoLst = createAnexxInfoForIntentionContract(to);

        // Register records in uow;
        uow.registerNew(newContract);

        for (Partner_Contract_Anexx_Info__c anexxInfo : anexxInfoLst) {
            uow.registerNew(anexxInfo);
            uow.registerRelationship(anexxInfo, Partner_Contract_Anexx_Info__c.ContractAgreementId__c, newContract);
        }

        return newContract;
    }

    private static APXT_Redlining__Contract_Agreement__c getContractAndRegisterRecords(ContractCreationTO to, fflib_ISObjectUnitOfWork uow) {
        // Create APXT_Redlining__Contract_Agreement__c
        APXT_Redlining__Contract_Agreement__c newContract = createContractAgreementRecord(to); 
            
        // Create Account_Contract_Agreement_Relationship__c 
        List<Account_Contract_Agreement_Relationship__c> accContractRelLst = createAccountContractAgreementRels(to); 

        // Create Partner_Contract_Anexx_Info__c  
        List<Partner_Contract_Anexx_Info__c> anexxInfoLst = createAnexxInfo(to); 
        
        // Update accounts legal reps and create accountContactRels
        List<AccountContactRelation> accContactRels = new List<AccountContactRelation>();
        List<Account> locationsToUpdate             = new List<Account>();

        if (to.locationsWithDifLegalRep.size() > 0) {
            accContactRels = createAccContactRels(to);
            locationsToUpdate = getLocationsWithNewLegalReps(to);
        }

        // Register records in uow;
        uow.registerNew(newContract);

        uow.registerNew(accContactRels);

        uow.registerDirty(locationsToUpdate);

        for (Account_Contract_Agreement_Relationship__c accContractRel : accContractRelLst) {
            uow.registerNew(accContractRel);
            uow.registerRelationship(accContractRel, Account_Contract_Agreement_Relationship__c.ContractAgreementId__c, newContract);
        }

        for (Partner_Contract_Anexx_Info__c anexxInfo : anexxInfoLst) {
            uow.registerNew(anexxInfo);
            uow.registerRelationship(anexxInfo, Partner_Contract_Anexx_Info__c.ContractAgreementId__c, newContract);
        }

        return newContract;
    }

    /**
    * @description 
    * @author alysson.mota@gympass.com | 03-17-2022 
    * @param to 
    * @return List<AccountContactRelation> 
    **/
    private static List<AccountContactRelation> createAccContactRels(ContractCreationTO to) {
        AccountContactRelationSelector accContactRelSelector = new AccountContactRelationSelector();
        List<AccountContactRelation> accContactRels = new List<AccountContactRelation>();
        Map<Id, AccountContactRelation> accIdToAccContactRel = new Map<Id, AccountContactRelation>();
        
        Set<Id> locationsIdsWithDifLegalReps = getLocationsIdsWithDifLegalReps(to);

        List<AccountContactRelation> exitentAccContactRels = 
            accContactRelSelector.selectByAccountIdAndContactId(locationsIdsWithDifLegalReps, new Set<Id>{to.masterAccount.Legal_Representative__c});

        for (AccountContactRelation accContactRel : exitentAccContactRels) {
            accIdToAccContactRel.put(accContactRel.AccountId, accContactRel);
        }  

        for (Account location : to.locationsWithDifLegalRep) {
            AccountContactRelation existingAccContactRel = accIdToAccContactRel.get(location.Id);

            if (existingAccContactRel == null) {
                AccountContactRelation accContactRel = new AccountContactRelation();
                accContactRel.ContactId = to.masterAccount.Legal_Representative__c;
                accContactRel.AccountId = location.Id;
                accContactRel.Type_of_Contact__c = 'Legal Representative';

                accContactRels.add(accContactRel);
            } 
        }

        return accContactRels;
    }

    /**
    * @description 
    * @author alysson.mota@gympass.com | 03-21-2022 
    * @param to 
    * @return Set<Id> 
    **/
    private static Set<Id> getLocationsIdsWithDifLegalReps(ContractCreationTO to) {
        Set<Id> locationsIdsWithDifLegalReps = new Set<Id>();

        for (Account location : to.locationsWithDifLegalRep) {
            locationsIdsWithDifLegalReps.add(location.Id);
        }

        return locationsIdsWithDifLegalReps;
    }

    /**
    * @description 
    * @author alysson.mota@gympass.com | 03-17-2022 
    * @param to 
    * @return List<Account> 
    **/
    private static List<Account> getLocationsWithNewLegalReps(ContractCreationTO to) {
        List<Account> locationsToUpdate = new List<Account>();

        for (Account location : to.locationsWithDifLegalRep) {
            location.Legal_Representative__c = to.masterAccount.Legal_Representative__c;
            locationsToUpdate.add(location);
        }

        return locationsToUpdate;
    }

    /**
    * Create APXT_Redlining__Contract_Agreement__c instance for insert
    * @author alysson.mota@gympass.com | 03-10-2022 
    * @param to 
    * @return APXT_Redlining__Contract_Agreement__c 
    **/
    private static APXT_Redlining__Contract_Agreement__c createContractAgreementRecord(ContractCreationTO to) {
        Id recordTypeId = Schema.SObjectType.APXT_Redlining__Contract_Agreement__c.getRecordTypeInfosByDeveloperName().get('Partner_Contract').getRecordTypeId();

        APXT_Redlining__Contract_Agreement__c contract = new APXT_Redlining__Contract_Agreement__c();
        contract.RecordTypeId = recordTypeId;
        contract.APXT_Redlining__Account__c = to.masterAccount.Id;
        contract.Opportunity__c = to.masterOpportunity.Id;
        contract.Type_of_contract__c = to.contract.type;
        contract.Template_Selection__c = getTemplateSelectionForContract(to);

        return contract;
    }


    /**
    * @description 
    * @author alysson.mota@gympass.com | 03-10-2022 
    * @param to 
    * @return List<Account_Contract_Agreement_Relationship__c> 
    **/
    private static List<Account_Contract_Agreement_Relationship__c> createAccountContractAgreementRels(ContractCreationTO to) {
        List<Account_Contract_Agreement_Relationship__c> accContractRels = new List<Account_Contract_Agreement_Relationship__c>();

        for (Id accId : to.locationsAccountIdList) {
            Account_Contract_Agreement_Relationship__c accContactRel = new Account_Contract_Agreement_Relationship__c();
            accContactRel.AccountId__c = accId;
            accContractRels.add(accContactRel);
        }

        return accContractRels;
    }

    /**
    * @description 
    * @author alysson.mota@gympass.com | 04-26-2022 
    * @param to 
    * @return List<Partner_Contract_Anexx_Info__c> 
    **/
    private static List<Partner_Contract_Anexx_Info__c> createAnexxInfoForIntentionContract(ContractCreationTO to) {
        ProductOpportunityAssignmentSelector poaSelector = new ProductOpportunityAssignmentSelector();
        List<Product_Opportunity_Assignment__c> poaLst = poaSelector.selectByAccountAndOpportunity(to.masterAccount.Id, to.masterOpportunity.Id);
        
        Map<Id, List<Product_Opportunity_Assignment__c>> prodIdToPoaLst = new Map<Id, List<Product_Opportunity_Assignment__c>>();
   
        for (Product_Opportunity_Assignment__c poa : poaLst) {
           
            if (prodIdToPoaLst.containsKey(poa.ProductAssignmentId__r.ProductId__c)) {
                prodIdToPoaLst.get(poa.ProductAssignmentId__r.ProductId__c).add(poa);
            } else {
                prodIdToPoaLst.put(poa.ProductAssignmentId__r.ProductId__c, new List<Product_Opportunity_Assignment__c>{poa});
            }
        }

        Map<Id, List<Product_Activity_Relationship__c>> productIdToProdActivityRelLst = getProdIdToProductAcvtyRelLstFromProdIdSet(prodIdToPoaLst.keySet());
        Map<Id, List<Product_Threshold_Member__c>> productIdToProductThresholdMembLst = getProdIdToProdThrhldMembLstFromProdIdSet(prodIdToPoaLst.keySet());
        
        List<Partner_Contract_Anexx_Info__c> anexxInfoLst = new List<Partner_Contract_Anexx_Info__c>();
        
        for (Id prodId : prodIdToPoaLst.keySet()) {
            List<Product_Opportunity_Assignment__c> auxPoaLst = prodIdToPoaLst.get(prodId);
            Partner_Contract_Anexx_Info__c anexx = new Partner_Contract_Anexx_Info__c();

            anexx.Volume_Discount__c = getVolumeDiscountString(productIdToProductThresholdMembLst.get(prodId), to);
            anexx.Types_of_Service__c = getTypesOfServiceString(productIdToProdActivityRelLst.get(prodId), to);
            anexx.ProductId__c = prodId;

            for (Product_Opportunity_Assignment__c poa : auxPoaLst) {
                if (poa.ProductAssignmentId__r.Commercial_Condition_Type_Dev_Name__c == 'CAP') {
                    anexx.CapProductAssignmentId__c = poa.ProductAssignmentId__c;
                    anexx.OpportunityMemberId__c = poa.OpportunityMemberId__c;
                }

                if (poa.ProductAssignmentId__r.Commercial_Condition_Type_Dev_Name__c == 'Late_Cancellation') {
                    anexx.LateCancelProductAssignmentId__c = poa.ProductAssignmentId__c;
                }
                
                if (poa.ProductAssignmentId__r.Commercial_Condition_Type_Dev_Name__c == 'No_Show') {
                    anexx.NoShowProductAssignmentId__c = poa.ProductAssignmentId__c;
                }
            }

            anexxInfoLst.add(anexx);
        }

        return anexxInfoLst;
    }

    /**
    * Create Partner_Contract_Anexx_Info__c instances to insert
    * @author alysson.mota@gympass.com | 03-10-2022 
    * @param to 
    * @return List<Account_Contract_Agreement_Relationship__c> 
    **/
    private static List<Partner_Contract_Anexx_Info__c> createAnexxInfo(ContractCreationTO to) {
        ProductOpportunityAssignmentSelector poaSelector = new ProductOpportunityAssignmentSelector();

        Map<Id, List<Product_Opportunity_Assignment__c>> accountIdToPoaLst          = new Map<Id, List<Product_Opportunity_Assignment__c>>();
        Map<Id, Set<Id>> prodIdToAccountIdSet                                       = new Map<Id, Set<Id>>();
        Map<Id, Id> prodIdToParentProdId                                            = new Map<Id, Id>();
        Map<String, List<Product_Opportunity_Assignment__c>> prodIdAndAccIdPoaLst    = new Map<String, List<Product_Opportunity_Assignment__c>>();
        
        List<Partner_Contract_Anexx_Info__c> anexxInfoLst = new List<Partner_Contract_Anexx_Info__c>();
        List<Product_Opportunity_Assignment__c> poaLst = poaSelector.selectByOpportunityWithProduct(to.masterOpportunity.Id);

        for (Product_Opportunity_Assignment__c poa : poaLst) {
            if (to.locationsAccountIdList.contains(poa.OpportunityMemberId__r.Account__c)) {
                if (poa.ProductAssignmentId__r.ProductId__r.Parent_Product__c != null) {
                    prodIdToParentProdId.put(poa.ProductAssignmentId__r.ProductId__c, poa.ProductAssignmentId__r.ProductId__r.Parent_Product__c);
                }  else if(to.masterAccount.Partner_Level__c == constants.PARTNER_LEVEL_SINGLE_PARTNER){
                    prodIdToParentProdId.put(poa.ProductAssignmentId__r.ProductId__c, poa.ProductAssignmentId__r.ProductId__c);
                }

                if (prodIdToAccountIdSet.containsKey(poa.ProductAssignmentId__r.ProductId__c)) {
                    prodIdToAccountIdSet.get(poa.ProductAssignmentId__r.ProductId__c).add(poa.OpportunityMemberId__r.Account__c);
                } else {
                    prodIdToAccountIdSet.put(poa.ProductAssignmentId__r.ProductId__c, new Set<Id>{poa.OpportunityMemberId__r.Account__c});
                }
            }
            
        }       

        prodIdAndAccIdPoaLst = getProdIdAndAccIdPoaLst(poaLst);
    
        // SELECT Product_Activity_Relationship__c
        Map<Id, List<Product_Activity_Relationship__c>> productIdToProdActivityRelLst = getProductIdToProdActivityRelLst(prodIdToParentProdId);

        // SELECT Product_Threshold_Member__c
        Map<Id, List<Product_Threshold_Member__c>> productIdToProductThresholdMembLst = getProductIdToProductThresholdMembLst(prodIdToParentProdId);

        for (Id productId : prodIdToAccountIdSet.keySet()) {
            Set<Id> accountIdSet = prodIdToAccountIdSet.get(productId);

            Id parentProdId = prodIdToParentProdId.get(productId);

            String volumeDiscount = null;
            String typesOfService = null;
            
            if (parentProdId != null) {
                volumeDiscount = getVolumeDiscountFromProduct(productIdToProductThresholdMembLst, parentProdId, to);
                typesOfService = getTypesOfServiceFromProduct(productIdToProdActivityRelLst, parentProdId, to);    
            }

            for (Id accountId : accountIdSet) {
                Partner_Contract_Anexx_Info__c anexx = new Partner_Contract_Anexx_Info__c();
                
                anexx.Volume_Discount__c    = volumeDiscount;
                anexx.Types_of_Service__c   = typesOfService;
                anexx.ProductId__c = parentProdId;

                String key = parentProdId + '/' + accountId;
                List<Product_Opportunity_Assignment__c> accountLocationPoaLst = prodIdAndAccIdPoaLst.get(key);
                if( accountLocationPoaLst == null && to.masterAccount.Partner_Level__c == constants.PARTNER_LEVEL_SINGLE_PARTNER ){
                    accountLocationPoaLst = new List<Product_Opportunity_Assignment__c>(poaLst);
                }
                if (accountLocationPoaLst != null) {
                    for (Product_Opportunity_Assignment__c poa : accountLocationPoaLst) {
                        if (poa.ProductAssignmentId__r.Commercial_Condition_Type_Dev_Name__c == 'CAP') {
                            anexx.CapProductAssignmentId__c = poa.ProductAssignmentId__c;
                            anexx.OpportunityMemberId__c = poa.OpportunityMemberId__c;
                        }
        
                        if (poa.ProductAssignmentId__r.Commercial_Condition_Type_Dev_Name__c == 'Late_Cancellation') {
                            anexx.LateCancelProductAssignmentId__c = poa.ProductAssignmentId__c;
                        }
                        
                        if (poa.ProductAssignmentId__r.Commercial_Condition_Type_Dev_Name__c == 'No_Show') {
                            anexx.NoShowProductAssignmentId__c = poa.ProductAssignmentId__c;
                        }
                    }
                }

                anexxInfoLst.add(anexx);
            }
        }

        return anexxInfoLst;
    }

    /**
    * @description 
    * @author alysson.mota@gympass.com | 04-14-2022 
    * @param poaLst 
    * @return Map<String, List<Product_Opportunity_Assignment__c>> 
    **/
    private static Map<String, List<Product_Opportunity_Assignment__c>> getProdIdAndAccIdPoaLst(List<Product_Opportunity_Assignment__c> poaLst) {
        Map<String, List<Product_Opportunity_Assignment__c>> prodIdAndAccIdPoaLst  = new Map<String, List<Product_Opportunity_Assignment__c>>();

        for (Product_Opportunity_Assignment__c poa : poaLst) {
            String key;
            if(poa.ProductAssignmentId__r.ProductId__r.Parent_Product__c == null && poa.OpportunityMemberId__r.Account__r.Partner_Level__c == constants.PARTNER_LEVEL_SINGLE_PARTNER){
                key = poa.ProductAssignmentId__r.ProductId__c + '/' + poa.OpportunityMemberId__r.Account__c;
            } else {
                key = poa.ProductAssignmentId__r.ProductId__r.Parent_Product__c + '/' + poa.OpportunityMemberId__r.Account__c;
            }           
            
            if (prodIdAndAccIdPoaLst.containsKey(key)) {
                prodIdAndAccIdPoaLst.get(key).add(poa);
            } else {
                prodIdAndAccIdPoaLst.put(key, new List<Product_Opportunity_Assignment__c>{poa});
            }
        }

        return prodIdAndAccIdPoaLst;
    }

    /**
    * @description 
    * @author alysson.mota@gympass.com | 04-14-2022 
    * @param prodIdToParentProdId 
    * @return Map<Id, List<Product_Activity_Relationship__c>> 
    **/
    private static Map<Id, List<Product_Activity_Relationship__c>> getProductIdToProdActivityRelLst(Map<Id, Id> prodIdToParentProdId) {
        Map<Id, List<Product_Activity_Relationship__c>> productIdToProdActivityRelLst = getProdIdToProductAcvtyRelLstFromProdIdSet(new Set<Id>(prodIdToParentProdId.values()));
        
        return productIdToProdActivityRelLst;
    }

    /**
    * @description 
    * @author alysson.mota@gympass.com | 04-26-2022 
    * @param prodIdSet 
    * @return Map<Id, List<Product_Activity_Relationship__c>> 
    **/
    private static Map<Id, List<Product_Activity_Relationship__c>> getProdIdToProductAcvtyRelLstFromProdIdSet(Set<Id> prodIdSet) {
        ProductActivityRelationshipSelector productActivityRelSelector = new ProductActivityRelationshipSelector();
        Map<Id, List<Product_Activity_Relationship__c>> productIdToProdActivityRelLst = new Map<Id, List<Product_Activity_Relationship__c>>();
        
        List<Product_Activity_Relationship__c> parLst = productActivityRelSelector.selectActivityByProductId(prodIdSet);

        for (Product_Activity_Relationship__c par : parLst) {
            if (productIdToProdActivityRelLst.containsKey(par.Product__c)) {
                productIdToProdActivityRelLst.get(par.Product__c).add(par);
            } else {
                productIdToProdActivityRelLst.put(par.Product__c, new List<Product_Activity_Relationship__c>{par});
            }
        }

        return productIdToProdActivityRelLst;
    }


    /**
    * @description 
    * @author alysson.mota@gympass.com | 04-14-2022 
    * @param prodIdToParentProdId 
    * @return Map<Id, List<Product_Threshold_Member__c>> 
    **/
    private static Map<Id, List<Product_Threshold_Member__c>> getProductIdToProductThresholdMembLst(Map<Id, Id> prodIdToParentProdId) {
        Map<Id, List<Product_Threshold_Member__c>> productIdToProductThresholdMembLst = getProdIdToProdThrhldMembLstFromProdIdSet(new Set<Id>(prodIdToParentProdId.values()));

        return productIdToProductThresholdMembLst;
    }

    /**
    * @description 
    * @author alysson.mota@gympass.com | 04-26-2022
    * Changed on 06-10-2022 IT-2746
    * @param prodIdSet 
    * @return Map<Id, List<Product_Threshold_Member__c>> 
    **/
    private static Map<Id, List<Product_Threshold_Member__c>> getProdIdToProdThrhldMembLstFromProdIdSet(Set<Id> prodIdSet) {
        ProductThresholdMemberSelector productThresholdMembSelector = new ProductThresholdMemberSelector();
        Map<Id, List<Product_Threshold_Member__c>> productIdToProductThresholdMembLst = new Map<Id, List<Product_Threshold_Member__c>>();
        
        List<Product_Threshold_Member__c> ptmLst = productThresholdMembSelector.selectThresholdByProductId(prodIdSet);

        for (Id prodId : prodIdSet) {
            productIdToProductThresholdMembLst.put(prodId, new List<Product_Threshold_Member__c>());
        }
        
        for (Product_Threshold_Member__c ptm : ptmLst) {
            if (productIdToProductThresholdMembLst.containsKey(ptm.Product__c)) {
                productIdToProductThresholdMembLst.get(ptm.Product__c).add(ptm);
            } else {
                productIdToProductThresholdMembLst.put(ptm.Product__c, new List<Product_Threshold_Member__c>{ptm});
            }
        }

        return productIdToProductThresholdMembLst;
    }

    /**
    * @description 
    * @author alysson.mota@gympass.com | 03-11-2022 
    * @param productIdToProductThresholdMembLst 
    * @param parentProdId 
    * @return String 
    **/
    private static String getVolumeDiscountFromProduct(Map<Id, List<Product_Threshold_Member__c>> productIdToProductThresholdMembLst, Id parentProdId, ContractCreationTO to) {
        List<Product_Threshold_Member__c> ptmLst = new List<Product_Threshold_Member__c>();

        if (productIdToProductThresholdMembLst.containsKey(parentProdId)) {
            ptmLst = productIdToProductThresholdMembLst.get(parentProdId);
        }

        return getVolumeDiscountString(ptmLst, to);
    }

    private static String getVolumeDiscountString(List<Product_Threshold_Member__c> ptmLst, ContractCreationTO to) {
        String volumeDiscount = '';
        Map<String, Traslation_Partner_Contract_Setting__mdt> countryNameToTranslationMdt = new Map<String, Traslation_Partner_Contract_Setting__mdt>();
        List<Traslation_Partner_Contract_Setting__mdt> translationMdtLst = [
            SELECT Country__c, In_Revenue_Label__c, Percentages_Applied_Text__c, Users_Label__c
            FROM Traslation_Partner_Contract_Setting__mdt
        ];

        for (Traslation_Partner_Contract_Setting__mdt mdt : translationMdtLst) {
            countryNameToTranslationMdt.put(mdt.Country__c, mdt);
        }
        Traslation_Partner_Contract_Setting__mdt translationMdt = new Traslation_Partner_Contract_Setting__mdt();

        translationMdt = countryNameToTranslationMdt.get(to.contract.country);
        
        if (translationMdt == null) {
            throw new AuraHandledException('Volume discount translation not found for the selected country.');
        }

        volumeDiscount = getTranslatedVolDiscountString(ptmLst, translationMdt);

        return volumeDiscount;
    }

    /**
    * @description 
    * @author alysson.mota@gympass.com | 05-13-2022 
    * @param ptmLst 
    * @param translationMdt 
    * @return String 
    **/
    private static String getTranslatedVolDiscountString(List<Product_Threshold_Member__c> ptmLst,  Traslation_Partner_Contract_Setting__mdt translationMdt) {
        String volumeDiscount = '';

        for (Integer i=0; i<ptmLst.size(); i++) {
            Product_Threshold_Member__c ptm = ptmLst.get(i);
            
            if (ptm.Threshold__r.Volume_Discount_Type__c == 'By single user volume') {
                if (i != ptmLst.size()-1) {
                    volumeDiscount += ptm.Threshold__r.Threshold_value_start__c + ' ' + translationMdt.Users_Label__c + ': ' + ptm.Threshold__r.Discount__c + '% / ';
                } else {
                    volumeDiscount += ptm.Threshold__r.Threshold_value_start__c + ' ' + translationMdt.Users_Label__c + ': ' + ptm.Threshold__r.Discount__c + '%';
                }
            }
            
            if (ptm.Threshold__r.Volume_Discount_Type__c == 'By volume of money') {
                if (i != ptmLst.size()-1) {
                    volumeDiscount += ptm.Product__r.CurrencyIsoCode + ' ' + ptm.Threshold__r.Threshold_value_start__c + ' ' + translationMdt.In_Revenue_Label__c + ': ' + ptm.Threshold__r.Discount__c + '% / ';
                } else {
                    volumeDiscount += ptm.Product__r.CurrencyIsoCode + ' ' + ptm.Threshold__r.Threshold_value_start__c + ' ' + translationMdt.In_Revenue_Label__c + ': ' + ptm.Threshold__r.Discount__c + '%' + '\n\n' + translationMdt.Percentages_Applied_Text__c;
                }
            }
        }

        return volumeDiscount;
    }
    
    /**
    * @description 
    * @author alysson.mota@gympass.com | 03-11-2022 
    * @param productIdToProdActivityRelLst 
    * @param parentProdId 
    * @return String 
    **/
    private static String getTypesOfServiceFromProduct(Map<Id, List<Product_Activity_Relationship__c>> productIdToProdActivityRelLst, Id parentProdId, ContractCreationTO to) {
        List<Product_Activity_Relationship__c> parLst = new List<Product_Activity_Relationship__c>();

        if (productIdToProdActivityRelLst.containsKey(parentProdId)) {
            parLst = productIdToProdActivityRelLst.get(parentProdId);
        }
       
        return getTypesOfServiceString(parLst, to);
    }

    /**
    * @description 
    * @author alysson.mota@gympass.com | 03-11-2022 
    * @param parLst 
    * @param to 
    * @return String 
    **/
    private static String getTypesOfServiceString(List<Product_Activity_Relationship__c> parLst, ContractCreationTO to) {
        String typesOfService = '';
        
        for (Integer i=0; i<parLst.size(); i++) {
            if (i != parLst.size()-1) {
                typesOfService += getGymActivityRelNameTranslated(parLst.get(i), to) + '; ';
            } else {
                typesOfService += getGymActivityRelNameTranslated(parLst.get(i), to);
            }
        }

        return typesOfService;
    }

    /**
    * @description 
    * @author gft.jorge.stevaux@ext.gympass.com | 05-31-2022 
    * @return String 
    **/
    private static String getGymActivityRelNameTranslated (Product_Activity_Relationship__c parItem, ContractCreationTO to) {
        switch on to.contract.country {
            
            when 'United Kingdom' {	
                return parItem.Gym_Activity_Relationship__r.Name_UK__c;   
            }
            when 'Spain' {		
                return parItem.Gym_Activity_Relationship__r.Name_ES__c;   

            }
            when 'Italy'  {		
                return parItem.Gym_Activity_Relationship__r.Name_IT__c;   

            }
            when 'Mexico' {		
                return parItem.Gym_Activity_Relationship__r.Name_MX__c;   

            }
            when 'Chile' {		
                return parItem.Gym_Activity_Relationship__r.Name_CL__c;   

            }
            when 'Brazil' {		
                return parItem.Gym_Activity_Relationship__r.Name_BR__c;   

            }
            when 'Argentina' {		
                return parItem.Gym_Activity_Relationship__r.Name_AR__c;   

            }
            when 'Germany' {		
                return parItem.Gym_Activity_Relationship__r.Name_DE__c;   

            }
            when 'Portugal' {		
                return parItem.Gym_Activity_Relationship__r.Name_PT__c;   

            }
            when 'Netherlands' {		
                return parItem.Gym_Activity_Relationship__r.Name_NL__c;   

            }
            when 'France' {		
                return parItem.Gym_Activity_Relationship__r.Name_FR__c;   

            }
            when else {	
                return parItem.Gym_Activity_Relationship__r.Name;	  
                
            }
        }

    }

    /**
    * Select template name for the given country 
    * @author alysson.mota@gympass.com | 03-10-2022 
    * @param to 
    * @return String 
    **/
    private static String getTemplateSelectionForContract(ContractCreationTO to) {
        Map<String, String> countryCodeToTemplateSelectionValue = new Map<String, String>{
            'United States'     => 'USA',
            'United Kingdom'    => 'UK',
            'Spain'             => 'Spain',
            'Italy'             => 'Italy',
            'Mexico'            => 'Mexico',
            'Chile'             => 'Chile',
            'Brazil'            => 'Brazil',
            'Argentina'         => 'Argentina',
            'Germany'           => 'Germany'
        };

        String templateSelection = countryCodeToTemplateSelectionValue.get(to.contract.country);
        
        if (templateSelection == null) {
            throw new AuraHandledException('No contract template found for the selected country.');
        }

        return templateSelection;
    }

    private static void createLog(Exception e, String origin) {
        String className = CreatePartnersContractMainController.class.getName();
     
        DebugLog__c log = new DebugLog__c(
            Origin__c = '['+className+']['+origin+']',
            LogType__c = constants.LOGTYPE_ERROR,
            RelatedToSquad__c = constants.LOGTYPE_SQUAD_PARTNERS,
            ExceptionMessage__c = e.getMessage(),
            StackTrace__c = e.getStackTraceString()
        );
        Logger.createLog(log);
    }

    public class ContractCreationTO {
        // Ids
        @AuraEnabled public Id masterOpportunityId {get; set;}
        @AuraEnabled public List<Id> locationsAccountIdList {get; set;}

        // SObjects
        @AuraEnabled public Account masterAccount {get; set;}
        @AuraEnabled public Opportunity masterOpportunity {get; set;}
        @AuraEnabled public List<Account> locationsWithDifLegalRep {get; set;} 
        @AuraEnabled public List<Account> locations {get; set;}
        @AuraEnabled public Set<Id> locationsIdInChildOpp {get; set;}
        
        // Custom structs
        @AuraEnabled public ContractStructure contract {get; set;}

        public ContractCreationTO(Id oppId) {
            this.masterOpportunityId = oppId;
            this.locationsAccountIdList = new List<Id>();

            this.masterAccount = new Account();
            this.masterOpportunity = new Opportunity();
            this.locationsWithDifLegalRep = new List<Account>();
            this.locations = new List<Account>();
            this.locationsIdInChildOpp = new Set<Id>();

            this.contract = new ContractStructure();
        }
    }

    public class ContractStructure {
        @AuraEnabled public String country {get; set;}
        @AuraEnabled public String type {get; set;}
        @AuraEnabled public APXT_Redlining__Contract_Agreement__c contractSobjectRecord {get; set;}
    }
}