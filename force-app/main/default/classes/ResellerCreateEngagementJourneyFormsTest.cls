/**
 * @description		Performs unit tests to the invocables repsonsible for creating the M0 and M1 forms implmented on
 * 					ResellerCreateM0FormHelper and ResellerCreateM1FormHelper.
 * @author			JSilva
 * @date			27-09-22
 */
@isTest
public class ResellerCreateEngagementJourneyFormsTest {

    @testSetup
    static void setup() {
        
        User adminUser = getAdminUser();
        Database.insert(adminUser);
        
        System.runAs(adminUser) {
            //-- create test account
        	Account testAccount = newClientsAccount();
        	Database.insert(testAccount);
        	//System.debug('### TEST ### Account created SUCCESS');
        
        	//-- create test account-related contacts
        	List<Contact> testContactsLst = newContactsLst(testAccount, 10);
        	Database.insert(testContactsLst);
        	//System.debug('### TEST ### Contacts created SUCCESS');
        
        
        	Pricebook2 testStandardPb = new Pricebook2(Id = Test.getStandardPricebookId(), IsActive = true);
       		Database.update(testStandardPb);
        	//System.debug('### TEST ### Pricebook #1 created SUCCESS');

       		Pricebook2 testPriceBook = newPricebook('SKU Price Book','Intermediation');
       		Database.insert( testPriceBook );
        	//System.debug('### TEST ### Pricebook #2 created SUCCESS');
    
      		Product2 testAcessFeeProduct = newProduct('Enterprise Subscription', false ,'USD');
       		Database.insert(testAcessFeeProduct);
        	//System.debug('### TEST ### Access Fee Product created SUCCESS');

      		PricebookEntry testStandardPBE = new PricebookEntry(
            	Pricebook2Id = Test.getStandardPricebookId(),  
            	Product2Id = testAcessFeeProduct.Id, 
            	UnitPrice = 400, 
            	IsActive = true);
      		Database.insert(testStandardPBE);
			//System.debug('### TEST ### Access Fee Pricebook created SUCCESS');
        
       	
        	//-- create test offer approved opportunity
			Opportunity testOpportunity = newOpportunity(testAccount.Id, testStandardPb.Id, 'Offer Sent', 'Intermediation');
        	Database.insert(testOpportunity);
        	//System.debug('### TEST ### Opportunity created SUCCESS');
        
        	//-- create test engagement journey m0 form
        	//Form__c testM0Form = newEngagementJourneyM0Form(testOpportunity.Id);
			//Database.insert(testM0Form);
        	//System.debug('### TEST ### M0 Form created SUCCESS');
        
        	//-- create test engagement journey form contact assignment
        	//Engagement_Journey_Contact_Assignment__c testContactAssignment = newContactAssignment(testContactsLst[0].Id, testM0Form.Id);
        	//Database.insert(testContactAssignment);
        	//System.debug('### TEST ### Contact Assignment created SUCCESS');
        }
        
    }
    
    @isTest
    public static void testCreateM0() {
        Opportunity myOpp = [SELECT Id from Opportunity WHERE Name Like '%TestOpp%' Limit 1];
        //System.debug('### testCreateM0 ###');
        //System.debug('### myOpp ### ' + myOpp);
        
        Test.startTest();                    
            ResellerCreateM0FormHelper.createM0ForOppsIds(new List<Id>{myOpp.id});
            List<Form__c> m0Lst = [SELECT Id, Name 
                  		  		   FROM Form__c 
                  		           WHERE Opportunity__c =: myOpp.id 
                  		           AND RecordType.DeveloperName = 'RESELLER_Engagement_Journey_M0' ];
            System.assertEquals(1, m0Lst.size());
        Test.stopTest();
    }


    @isTest
    public static void testCreateM1() {
        Opportunity myOpp = [SELECT Id from Opportunity WHERE Name Like '%TestOpp%' Limit 1];      
        
        //-- create test engagement journey m0 form
       	Form__c testM0Form = newEngagementJourneyM0Form(myOpp.Id);
		Database.insert(testM0Form);
        //System.debug('### TEST ### M0 Form created SUCCESS');
              
        Test.startTest();
            ResellerCreateM1FormHelper.createM1ForOppsWithIds(new List<Id>{myOpp.id});            
            List<Form__c> m1 = [SELECT Id, Name 
                                FROM Form__c 
                                WHERE Opportunity__c =: myOpp.id 
                                AND RecordType.DeveloperName = 'RESELLER_Engagement_Journey_M1' ];
            System.assertEquals(1, m1.size());
        Test.stopTest();
    }
    
    //-- private helper methods -- START ----------------------------------------------
    private static Account newClientsAccount() {
        Account testAccount = new Account();  
        testAccount.Name = 'Reseller Account';
        testAccount.Legal_Document_Type__c = 'CNPJ';
        testAccount.Id_Company__c = '43712486000101';
        testAccount.Razao_Social__c = 'IndirectAccount';
        testAccount.Website = 'IndirectAccount.com';
        testAccount.Industry = 'Airlines';
        testAccount.BillingCountry = 'Brazil';
        testAccount.NumberOfEmployees = 600;
        testAccount.CurrencyIsoCode = 'USD';
        testAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
        return testAccount;
    }
    
    private static List<Contact> newContactsLst(Account acc, Integer numberOfContacts) {
        List<Contact> contactsLst = new List<Contact>();
        
        for(Integer index = 0; index < numberOfContacts; index++) {
            Contact cont = new Contact();
        	cont.AccountId = acc.Id;
        	cont.LastName = 'test ' + index;
        	cont.Email = 'test' + index +'@tst.com';
            contactsLst.add(cont);
        }
        return contactsLst;
    }
    
    private static Pricebook2 generatePricebook() {        
        Pricebook2 pb = new Pricebook2();
        pb.Name = 'Br';
        pb.Country__c = 'Brazil';
        pb.IsActive = true;
        return pb;
    }
    
    private static Pricebook2 newPricebook(String name, String businessModel) {
        Pricebook2 pricebook = new Pricebook2(
           	Name = name,
            Business_Model__c = businessModel,
            IsActive = true
        );
        return pricebook;
    }
    
    private static Product2 newProduct(String ProductName , Boolean isFamilyMemberIncluded , String currencyIsoCode) {
        Product2 product = new Product2();
        product.Name = ProductName;
        product.Minimum_Number_of_Employees__c = 0;
        product.Maximum_Number_of_Employees__c = 900;
        product.Family = ProductName;
        product.IsActive = true;
        product.Family_Member_Included__c = isFamilyMemberIncluded;
        product.ProductCode = ProductName.replace(' ', '_');
        product.Copay2__c = false;
        product.CurrencyIsoCode = currencyIsoCode;
        return product;
    }  
    
    private static Opportunity newOpportunity(String accountId, String stdPricebookId, String fastTrackStage, String businessModel) {
        Id oppRtId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Indirect_Channel_New_Business').getRecordTypeId();
        
    	Opportunity Opp = new Opportunity();
       	Opp.recordTypeId = oppRtId;
       	Opp.TotalOpportunityQuantity = 1;
       	Opp.AccountId = accountId;
       	Opp.Name = 'TestOpp'; 
       	Opp.CMS_Used__c = 'Yes';     
       	Opp.Gym_agreed_to_an_API_integration__c  = 'Yes';
       	Opp.Club_Management_System__c = 'Companhia Athletica';
       	Opp.Integration_Fee_Deduction__c = 'No';
       	Opp.CloseDate = Date.today();
       	Opp.Success_Look_Like__c = 'Yes';
       	Opp.Success_Look_Like_Description__c = 'Money money';
       	Opp.StageName = 'Validated';
       	Opp.Type = 'Expansion';  
       	Opp.Country_Manager_Approval__c = true;
       	Opp.Payment_approved__c = true;   
       	Opp.CurrencyIsoCode = 'USD';
       	Opp.Gympass_Plus__c = 'Yes';
       	Opp.B_M__c = businessModel;
       	Opp.Standard_Payment__c = 'Yes';
       	Opp.Request_for_self_checkin__c = 'Yes';  
       	Opp.Pricebook2Id = stdPricebookId;
       	
        return Opp;
    }
    
    private static Form__c newEngagementJourneyM0Form(ID oppId) {
        ID rtId = Schema.SObjectType.Form__c.getRecordTypeInfosByDeveloperName().get('RESELLER_Engagement_Journey_M0').getRecordTypeId();
        String currentUserId = UserInfo.getUserId();
        
        Form__c m0 = new Form__c();
        m0.RecordTypeId = rtId;
        m0.Opportunity__c = oppId;
        m0.Head_of_client_success__c = UserInfo.getUserId();
        m0.Client_Success_Executive__c = UserInfo.getUserId();
        m0.Client_Sales_Executive__c = UserInfo.getUserId();
        return m0;
    }
    
    private static Engagement_Journey_Contact_Assignment__c newContactAssignment(String contactId, String formId) {
        Engagement_Journey_Contact_Assignment__c ca = new Engagement_Journey_Contact_Assignment__c();
        ca.Engagement_Journey_Form__c = formId;
        ca.Contact__c = contactId;
        return ca;
    }
    
    private static User getAdminUser() {
        Id adminProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1].Id;
        User adminUser = new User();
        adminUser.ProfileId = adminProfileId;
        adminUser.FirstName = 'Tester';
        adminUser.LastName = 'Tester';
        adminUser.Username = 'testergftgympass@test.com';
        adminUser.Email = 'testergftgympass@test.com';
        adminUser.TimeZoneSidKey = 'GMT';
        adminUser.LanguageLocaleKey = 'en_US';
        adminUser.LocaleSidKey = 'en_US';
        adminUser.EmailEncodingKey = 'ISO-8859-1';
        adminUser.Alias = 'tsts';
        adminUser.Bypass_Automations__c = true;
        
        return adminUser;
    }
    //-- private helper methods --  END  ----------------------------------------------
}