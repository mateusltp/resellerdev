/**
* @File Name          : BatchGFProspectingSales.cls
* @Description        :
* @Author             : JRDL@GFT.com
* @Group              :
* @Last Modified By   : 
* @Last Modified On   : 
* @Modification Log   :
* Ver       Date            Author      		    Modification
* 1.0    25/09/2021   		JRDL@GFT.com     		Initial Version
**/
global class BatchGFProspectingSales implements Database.Batchable<sObject>,   Database.AllowsCallouts {
    global Database.QueryLocator start(Database.BatchableContext bc) {
        Date ed = Date.today().addDays(-1);
        return Database.getQueryLocator([
            SELECT Id, Data_do_Lancamento__c, AccountId, Account.RecordTypeId, RecordTypeId, StageName, Opportunity_Lost_Date__c, CloseDate
            FROM Opportunity
            WHERE Data_do_Lancamento__c =: ed OR Opportunity_Lost_Date__c =: ed OR CloseDate =: ed
        ]);
    }
    
    global void execute(Database.BatchableContext bc, List<Opportunity> scope) {
        
        List<Account> lstAccNew = new List<Account>();
        List<Account> lstAccReneg = new List<Account>();
        List<Account> lstAccLost = new List<Account>();
        
        List<Contact> lstUpdateConNew = new List<Contact>();
        List<Contact> lstUpdateConReneg = new List<Contact>();
        
        Id rtOppClientNew = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        Id rtOppSMBNew = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SMB_New_Business').getRecordTypeId();
        Id rtOppClientReneg = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Success_Renegotiation').getRecordTypeId();
        Id rtOppSMBReneg = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SMB_Success_Renegotiation').getRecordTypeId(); 
        
        Id rtAcc = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
        
        Map<String, Account> mapAcc = new Map<String, Account>();
		        
        for(Opportunity o: scope){
            if((o.RecordTypeId == rtOppClientNew || o.RecordTypeId == rtOppSMBNew) && o.Data_do_Lancamento__c == Date.today().addDays(-1) && o.StageName == 'Lançado/Ganho' && o.Account.RecordTypeId == rtAcc){                      
                lstAccNew.add(o.Account);
               }
            if((o.RecordTypeId == rtOppClientReneg || o.RecordTypeId == rtOppSMBReneg) && o.CloseDate == Date.today().addDays(-1) && o.StageName == 'Lançado/Ganho' && o.Account.RecordTypeId == rtAcc){                                  
                lstAccReneg.add(o.Account);
               }
            if((o.RecordTypeId == rtOppClientNew || o.RecordTypeId == rtOppSMBNew) && o.Opportunity_Lost_Date__c == Date.today().addDays(-1) && o.StageName == 'Perdido' && o.Account.RecordTypeId == rtAcc){                              
                lstAccLost.add(o.Account);
               }

        }
        
        for(Account acc: [Select id,Name,Payment_method__c,GP_NumberOfDependents__c,Business_Unit__c,Tagus_Based__c, Size__c,Owner.Name,ID_Gympass_CRM__c,
                          NumberOfEmployees,BillingCountry,RecordType.DeveloperName,Proposal_End_Date__c,Contract_Signed_At__c,Launch_date__c
                          FROM Account WHERE (id IN: lstAccNew OR id IN: lstAccReneg)]){
            mapAcc.put(acc.id, acc);
        }
        
        Qualtrics__mdt mdtQualtricsNew = Qualtrics__mdt.getInstance('Client_Sales_Won');
        Qualtrics__mdt mdtQualtricsReneg = Qualtrics__mdt.getInstance('Client_Sales_PostRenewal');
        
        if(!lstAccNew.isEmpty()){
            for(Contact con: [Select Id,AccountId,MobilePhone, Status_do_contato__c,FirstName,LastName,Language_Code__c,HasOptedOutOfEmail, GFSendSurvey__c, Email, Primary_HR_Contact__c,DecisionMaker__c, Role__c FROM Contact WHERE AccountID IN :lstAccNew]){
                if(con.Status_do_contato__c == 'Ativo' && con.Email != null && con.Primary_HR_Contact__c == TRUE){
                    con.GFSendSurvey__c = 'Client Sales Won';
                    lstUpdateConNew.add(con);
                }
            }
        }
        if(!lstAccNew.isEmpty() && !lstUpdateConNew.isEmpty() && mdtQualtricsNew.Active__c == true){
            Qualtrics.createMailingList('Client Sales Won',lstUpdateConNew, mapAcc);
        }
        
        if(!lstAccReneg.isEmpty()){
            for(Contact con: [Select Id, Status_do_contato__c, GFSendSurvey__c, Email, Primary_HR_Contact__c,DecisionMaker__c, Role__c FROM Contact WHERE AccountID IN :lstAccReneg]){
                if(con.Status_do_contato__c == 'Ativo' && con.Email != null && con.Primary_HR_Contact__c == TRUE){
                    con.GFSendSurvey__c = 'Client Sales PostRenewal';
                    lstUpdateConNew.add(con);
                }
            }
        }
        
        if(!lstAccReneg.isEmpty() && !lstUpdateConNew.isEmpty() && mdtQualtricsReneg.Active__c == true){
            Qualtrics.createMailingList('Client Sales PostRenewal',lstUpdateConNew, mapAcc);
        }
        
        
        /*if(!lstAccLost.isEmpty()){
            for(Contact con: [Select Id, Status_do_contato__c, GFSendSurvey__c, Email, Primary_HR_Contact__c,DecisionMaker__c, Role__c FROM Contact WHERE AccountID IN :lstAccLost]){
                if(con.Status_do_contato__c == 'Ativo' && con.Email != null && con.Primary_HR_Contact__c == TRUE){
                    con.GFSendSurvey__c = 'Client Sales Lost';
                    lstUpdateConNew.add(con); 
                }
            }
        }*/
        
        
        if(!lstUpdateConNew.isEmpty()){
            update lstUpdateConNew;
        }
    }
    
    global void finish(Database.BatchableContext bc) {
        
    }
}