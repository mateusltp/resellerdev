/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 05-10-2022
 * @last modified by  : alysson.mota@gympass.com
 * Modifications Log 
 * Ver   Date         Author                             Modification
 * 1.0   07-19-2021   gft.samuel.silva@ext.gympass.com   Initial Version
**/
public with sharing class OpportunityPartnerServiceImpl implements IOpportunityService{

    private static final PS_Constants constants = PS_Constants.getInstance();
    private static final String className = OpportunityPartnerServiceImpl.class.getName();

    private static final Set<Id> RtPartner = 
    new Set<Id>{    Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(constants.OPPORTUNITY_RT_PARTNER_SM_RENEG).getRecordTypeId(),
                    Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(constants.OPPORTUNITY_RT_PARTNER_WISHLIST_RENEG).getRecordTypeId(),
                    Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(constants.OPPORTUNITY_RT_GYMS_SMP).getRecordTypeId(),
                    Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(constants.OPPORTUNITY_RT_GYMS_WISHLIST).getRecordTypeId()
                };  
  
    public static List< Opportunity > openRenegotiation(Map<Id, Opportunity> mAccountNewOpp){       

        fflib_ISObjectUnitOfWork uow = Application.UnitOfWork.newInstance();      
        Map<Id, Opportunity> mAccountOppWon = ((OpportunitySelector)Application.Selector.newInstance(Opportunity.SObjectType)).selectLastWonToClone(mAccountNewOpp.keySet());        
        Map<Id, Opportunity> mOppClonedNewOpp = new Map<Id, Opportunity>();  
        Map<Id, Opportunity> mOppOppResponse = new Map<Id, Opportunity>();      
        System.debug('mAccountNewOpp ' + mAccountNewOpp);
        System.debug('mAccountOppWon ' + mAccountOppWon);
        if(!mAccountNewOpp.isEmpty()){
            for(Id iAcc : mAccountNewOpp.keySet()){
                Opportunity newOpp = new Opportunity(); 
                System.debug('iAcc ' + iAcc);   
                System.debug('mAccountOppWon.containsKey(iAcc) ' + mAccountOppWon);             
                if(mAccountOppWon.containsKey(iAcc)){
                    System.debug('mAccountOppWon.containsKey(iAcc) entrou if');    
                    newOpp = setDefaultValuesForRenegotiationOpp(cloneOpportunity(mAccountOppWon.get(iAcc)), mAccountNewOpp.get(iAcc), uow);  
                    mOppClonedNewOpp.put(mAccountOppWon.get(iAcc).Id, newOpp);
                } else {
                    System.debug('mAccountOppWon.containsKey(iAcc) entrou else');    
                    newOpp = setDefaultValuesForRenegotiationOpp(createNewRenegotiationPartnerOpp(iAcc), mAccountNewOpp.get(iAcc), uow);   
                    System.debug('newOpp ' + newOpp);   
                }
                mOppOppResponse.put(iAcc, newOpp);
           
            }

            if(!mOppClonedNewOpp.isEmpty()){
                System.debug('mOppClonedNewOpp.isEmpty entrou');  
                cloneProposal(mOppClonedNewOpp, uow);
                cloneAccountOpportunityRelationship(mOppClonedNewOpp, uow);
                Map<Id, Product_Item__c> mProdByOldProd = cloneProductItems(mAccountOppWon, mOppClonedNewOpp, uow);
                cloneGymActivities(mProdByOldProd, mOppClonedNewOpp, uow);    
            }           
        }

        try {
            System.debug(' trying ');    
            uow.commitWork();
        } catch (Exception e){
            DebugLog__c log = new DebugLog__c(
                    Origin__c = '['+className+'][openRenegotiation]',
                    LogType__c = constants.LOGTYPE_ERROR,
                    RelatedToSquad__c = constants.LOGTYPE_SQUAD_PARTNERS,
                    ExceptionMessage__c = e.getMessage(),
                    StackTrace__c = e.getStackTraceString(),
                    DataScope__c = JSON.serialize(uow) // test to verify how the uow would look like at this point
            );
            Logger.createLog(log);
            System.debug(' Error opening renegotiation ' + e);    
            throw new OpportunityServiceImplException(constants.ERROR_OPP_OPENING_RENEGOTIATION + e.getMessage());
        }        
        return mOppOppResponse.values();        
    }




    private static Opportunity setDefaultValuesForRenegotiationOpp( Opportunity aOpp, Opportunity formOpp, fflib_ISObjectUnitOfWork uow ){        
        System.debug('setDefaultValuesForRenegotiationOpp ');
        aOpp.Name = formOpp.Sub_Type__c;        
        aOpp.Sub_Type__c = formOpp.Sub_Type__c;
        aOpp.Cancellation_Reason__c = formOpp.Cancellation_Reason__c;
        aOpp.Cancellation_Reason_subcategory__c = formOpp.Cancellation_Reason_subcategory__c;
        aOpp.StageName = constants.OPPORTUNITY_STAGE_QUALIFICACAO;
        aOpp.Source__c = formOpp.Source__c;
        aOpp.CloseDate = System.today();
        aOpp.RecordTypeId = formOpp.RecordTypeId == null ? getDefaultRecordTypeId() : formOpp.RecordTypeId;
        aOpp.OwnerId = UserInfo.getUserId();
        aOpp.Submission_Date__c = null;
        aOpp.Data_do_Lancamento__c = null;
        aOpp.Pipefy_Card_ID__c = null;
        aOpp.Type = constants.OPPORTUNITY_TYPE_RENEGOTIATION;
        uow.registerNew(aOpp);
        return aOpp;
    }


    private static void cloneProposal( Map< Id , Opportunity > mOppClonedNewOpp, fflib_ISObjectUnitOfWork uow ){       
        ProposalSelector propSel = (ProposalSelector)Application.Selector.newInstance(Quote.SObjectType);
        for( Quote iQuote :  propSel.selectByOppIdToClone(mOppClonedNewOpp.keySet())){
            Quote aQuote = iQuote.clone(false, true, false, false);
            aQuote.Id = null;
            uow.registerNew(aQuote, Quote.OpportunityId, mOppClonedNewOpp.get( iQuote.OpportunityId ) );
        }
     
    }  


    private static Id getDefaultRecordTypeId(){
        return ((OpportunitySelector)Application.Selector.newInstance(Opportunity.SObjectType)).getDefaultRecordType().getRecordTypeId();
    }


    
    private static void cloneAccountOpportunityRelationship( Map< Id , Opportunity > mOppClonedNewOpp, fflib_ISObjectUnitOfWork uow ){        
        AccountOpportunitySelector accOppSel = ((AccountOpportunitySelector)Application.Selector.newInstance(Account_Opportunity_Relationship__c.SObjectType));
        for( Account_Opportunity_Relationship__c iAccOpp :  accOppSel.selectByOppIdToClone(mOppClonedNewOpp.keySet())){
            Account_Opportunity_Relationship__c aAccOpp = iAccOpp.clone(false, true, false, false);
            aAccOpp.Id = null;
            uow.registerNew(aAccOpp, Account_Opportunity_Relationship__c.Opportunity__c, mOppClonedNewOpp.get( iAccOpp.Opportunity__c ));
        }    
    }



    private static Map<Id, Product_Item__c> cloneProductItems(Map<Id, Opportunity> mAccountOppWon , Map<Id, Opportunity> mOppClonedNewOpp, fflib_ISObjectUnitOfWork uow){
        
        Map<Id, Product_Item__c> mProdByOldProd = new Map<Id, Product_Item__c>();
        ProductItemSelector prodItemSel = ((ProductItemSelector)Application.Selector.newInstance(Product_Item__c.SObjectType));
        Set<Id> oppIds = (new Map<Id,Opportunity>(mAccountOppWon.values())).keySet();

        for( Product_Item__c iProductItem : prodItemSel.selectByOppIdToClone(oppIds) ){
            Product_Item__c aProdItem = iProductItem.clone(false, true, false, false);
            aProdItem.Id = null;
            uow.registerNew( aProdItem, Product_Item__c.Opportunity__c, mOppClonedNewOpp.get( iProductItem.Opportunity__c ) );
            mProdByOldProd.put( iProductItem.Id, aProdItem );
        } 
        
        return mProdByOldProd;
    }

    private static void cloneGymActivities(  Map<Id, Product_Item__c> mProdByOldProd , Map<Id, Opportunity> mOppClonedNewOpp, fflib_ISObjectUnitOfWork uow){

        GymActivitySelector gymActSel = ((GymActivitySelector)Application.Selector.newInstance(Gym_Activity__c.SObjectType));

        for( Gym_Activity__c iGymActivity : gymActSel.selectByProductIdToClone( mProdByOldProd.keySet()) ){
            Gym_Activity__c aGymActivity = iGymActivity.clone( false, true, false, false );
            aGymActivity.Id = null;
            uow.registerNew(aGymActivity, Gym_Activity__c.Product_Item__c, mProdByOldProd.get( iGymActivity.Product_Item__c ));
        } 
        
    }



    private static Opportunity cloneOpportunity( Opportunity aOpp){    
        Opportunity nOpp = aOpp.clone(false, true, false, false);
        return nOpp;
    }

    private static Opportunity createNewRenegotiationPartnerOpp(Id accId){     
        System.debug('createNewRenegotiationPartnerOpp ');
        return new Opportunity( AccountId = accId , No_Won_Opportunity_in_SF__c = true );
    }

     public static List<Opportunity> updateStageToLost(List<Opportunity> oppScopeLst){
        fflib_ISObjectUnitOfWork uow = Application.UnitOfWork.newInstance(); 
        for(Opportunity iOpp : oppScopeLst){    
           iOpp.StageName = constants.OPPORTUNITY_STAGE_PERDIDO;
           iOpp.Loss_Reason__c = constants.OPPORTUNITY_LOSS_REASON_OTHER;
        }
        uow.registerDirty(oppScopeLst, false);

           try {
            uow.commitWork();
        } catch (Exception e){
               DebugLog__c log = new DebugLog__c(
                       Origin__c = '['+className+'][updateStageToLost]',
                       LogType__c = constants.LOGTYPE_ERROR,
                       RelatedToSquad__c = constants.LOGTYPE_SQUAD_PARTNERS,
                       ExceptionMessage__c = e.getMessage(),
                       StackTrace__c = e.getStackTraceString(),
                       DataScope__c = JSON.serialize(uow) // test to verify how the uow would look like at this point
               );
               Logger.createLog(log);
               throw new OpportunityServiceImplException(constants.ERROR_OPP_UPDATING_STAGE_LOST + e.getMessage());
        }   
        return oppScopeLst;
        
     
    }

    public static List <Opportunity> notifyOpportunityCancellation (List <Opportunity> aOppList){

        EmailTemplateSelector etSelector = (EmailTemplateSelector)Application.Selector.newInstance(EmailTemplate.SObjectType);
        OrgWideEmailAddressSelector oweSelector = (OrgWideEmailAddressSelector)Application.Selector.newInstance(OrgWideEmailAddress.SObjectType);

        OrgWideEmailAddress owea = oweSelector.selectGlobalB2BOrgWideEmailAddress();
        EmailTemplate et =  etSelector.selectEmailTemplateByName(constants.ET_WAITING_CANCELATION_NOTIFICATION);
        fflib_ISObjectUnitOfWork uow = Application.UnitOfWork.newInstance(); 
        

        for (Opportunity opp : aOppList) {
            String userId = opp.OwnerId;
            String whatId = opp.Id;

            Messaging.SingleEmailMessage email;
    
            if(!Test.isRunningTest()){
            
                email = Messaging.renderStoredEmailTemplate(et.id, userId, whatId);

            }else{
                email = new Messaging.SingleEmailMessage(); 
                email.setSubject('Single Email message Example');
                email.setPlainTextBody('Hello!!!!!!!!!!This is a test email to test single email message program');
            }
           
    
            String emailSubject = email.getSubject();
            String emailTextBody = email.getPlainTextBody();
            
            email.setTargetObjectId(userId);
            email.setSubject(emailSubject);
    		email.setOrgWideEmailAddressId(owea.Id);
            email.setPlainTextBody(emailTextBody);
            email.saveAsActivity = false;
            if(!Test.isRunningTest()) {
                Messaging.SendEmailResult[] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
            }
            notifyUsers(new Set<String>{opp.OwnerId}, opp.Id);
            
        }

            return aOppList;
    }


    public static void notifyUsers(Set<String> recipientsIds, String recordId) {
        Messaging.CustomNotification notification = new Messaging.CustomNotification();
        try {
            CustomNotificationTypeSelector cntSelector = (CustomNotificationTypeSelector)Application.Selector.newInstance(CustomNotificationType.sObjectType);
            CustomNotificationType notificationType = cntSelector.selectCustomNotificationTypeByDeveloperName(constants.NOTIF_NAME_WAITING_CANCELATION_NOTIFY);
            notification.setTitle(constants.NOTIF_TITLE_WAITING_CANCELATION_NOTIFY);
            notification.setBody(constants.NOTIF_BODY_WAITING_CANCELATION_NOTIFY);
            notification.setNotificationTypeId(notificationType.Id);
            notification.setTargetId(recordId);
            notification.send(recipientsIds);
        }
        catch (Exception e) {
            DebugLog__c log = new DebugLog__c(
                    Origin__c = '['+className+'][notifyUsers]',
                    LogType__c = constants.LOGTYPE_ERROR,
                    RelatedToSquad__c = constants.LOGTYPE_SQUAD_PARTNERS,
                    ExceptionMessage__c = e.getMessage(),
                    StackTrace__c = e.getStackTraceString(),
                    DataScope__c = JSON.serialize(notification) // test to verify how the uow would look like at this point
            );
            Logger.createLog(log);
            System.debug('Problem sending notification: ' + e.getMessage());
            throw new OpportunityServiceImplException(constants.NOTIF_EXCEPTION_MESSAGE + e.getMessage());
        }
    }
    

    // Starting Validation TAGUS 
    /*public static void validateOpportunityForTagus(List<Opportunity> newOppLst){
        if(System.Label.TAGUS_PARTNER != constants.PARTNER) return;
        Id partnerRt = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(constants.OPPORTUNITY_RT_PARTNER_FLOW_OPP).getRecordTypeId();
        
        for(Integer i=0; i<newOppLst.size(); i++){
            if(newOppLst[i].RecordTypeId != partnerRt){
                newOppLst.remove(i);
            }
        }
        Map<Id, Account>  aAccMap = getPartners( newOppLst );  
        System.debug('getPartners ' + aAccMap);
        Map<Id, List<Product_Opportunity_Assignment__c>> aProductMap = getProductsAndCommercialConditions( newOppLst );
        Map<Id, Quote> aProposalMap = getProposals( newOppLst );
        validatePartners(aAccMap,aProposalMap, newOppLst, aProductMap);
    }

    public static void setAccountTypePartnerOnOppWon(List<Opportunity> oppList) {
        system.debug('## entered method on impl class');
        fflib_ISObjectUnitOfWork uow = Application.UnitOfWork.newInstance();
        Set<Id> accountIds = new Set<Id>();
        for (Opportunity opp : oppList) {
            // check if account id is readable
            accountIds.add(opp.AccountId);
        }
        List<Account> accountList = new List<Account>();
        for (Id accountId : accountIds) {
            accountList.add(new Account(
                Id = accountId,
                Type = constants.PARTNER
            ));
        }
        system.debug('## accountList: '+accountList);
        uow.registerDirty(accountList);
        uow.commitWork();
    }

    private static Map<Id, Account> getPartners( List<Opportunity> aOppLst ) {
        List<Account> accLst = new List<Account>();
        for(Opportunity iOpp : aOppLst){
            accLst.add( new Account(Id=iOpp.AccountId));
        }
        AccountSelector accSelector = ((AccountSelector)Application.Selector.newInstance(Account.sobjectType));        
        return accSelector.selectAccountMapWithFieldsToTagusParse(accLst);
    }

    private  static Map<Id, Quote>  getProposals( List<Opportunity> aOppLst ) {
        return ((ProposalSelector)Application.Selector.newInstance(Quote.sobjectType)).selectQuoteByOppId(aOppLst);
    }

    private static Map<Id, List<Product_Opportunity_Assignment__c>> getProductsAndCommercialConditions( List<Opportunity> aOppLst ){
        Set<Id> oppIds = new Set<Id>();
        for(Opportunity iOpp : aOppLst){
            oppIds.add( iOpp.Id);
        }
        return ((AccountOpportunitySelector)Application.Selector.newInstance(Account_Opportunity_Relationship__c.sobjectType)).selectProductAssignmentToTagus(oppIds);
    }

    private static void validatePartners( Map<Id, Account> aAccMap,Map<Id, Quote> proposalMap, List<Opportunity> aOppLst, Map<Id, List<Product_Opportunity_Assignment__c>> aProdMap ){
        for( Opportunity iOpp : aOppLst ){
            System.debug('aAccMap ' + aAccMap);
            Account aAcc = aAccMap.get(iOpp.AccountId);
            if (String.isBlank(aAcc.Legal_Document_Type__c)) {iOpp.addError('The Partner/Gym Legal Document Type is required to close this opportunity.');
            }
            if (String.isBlank(aAcc.Id_Company__c)) {iOpp.addError('The Partner/Gym Legal Number is required to close this opportunity.');
            }
            if (String.isBlank(aAcc.Legal_Title__c)) {iOpp.addError('The Partner/Gym Legal Title is required to close this opportunity.');
            }
            if (String.isBlank(aAcc.BillingCountryCode)) {iOpp.addError('The Partner/Gym Billing Country is required to close this opportunity.');
            }
            if (String.isBlank(aAcc.BillingStreet)) {iOpp.addError('The Partner/Gym Billing Street is required to close this opportunity.');
            }
            if (String.isBlank(aAcc.BillingPostalCode)) {iOpp.addError('The Partner/Gym Billing Postal Code is required to close this opportunity.');
            }
            if (String.isBlank(aAcc.BillingCity)) {iOpp.addError('The Partner/Gym Billing City is required to close this opportunity.');
            }
            if (String.isBlank(aAcc.BillingState)) {iOpp.addError('The Partner/Gym Billing State is required to close this opportunity.');
            }
            validatePartnerContacts( aAcc, iOpp );            
            validateProposal( proposalMap, iOpp );
            validatePartnerProductsAndCommercialConditions( aProdMap.get(iOpp.Id), iOpp );
        }
    }

    private static void validateProposal(Map<Id, Quote> proposalMap, Opportunity aOpp){

        if (String.isBlank(String.valueOf(proposalMap.get(aOpp.Id).Start_Date__c)) ) {aOpp.addError(' The Start Date in Proposal is required to close this opportunity. ');
        }

        if (String.isBlank(String.valueOf(proposalMap.get(aOpp.Id).End_Date__c)) ) {aOpp.addError(' The End Date in Proposal is required to close this opportunity. ');
        }

        if (String.isBlank(String.valueOf(proposalMap.get(aOpp.Id).Signed__c)) ) {aOpp.addError(' The Signed Date in Proposal is required to close this opportunity. ');
        }      
    }

    private static void validatePartnerContacts( Account aAcc, Opportunity aOpp ){     
        for (AccountContactRelation contactRel : aAcc.AccountContactRelations) {    
            if (String.isBlank(contactRel.Contact.FirstName)) {aOpp.addError('First Name for Admin (Contact) is required to close this opportunity.');
            }
            if (String.isBlank(contactRel.Contact.Email)) {aOpp.addError(' The email address for ' + contactRel.Contact.FirstName + ' contact is required to close this opportunity. ');
            }
            if (String.isBlank(contactRel.Contact.Type_of_contact__c)) {aOpp.addError(' The Type of Contact for ' + contactRel.Contact.FirstName + ' contact is required to close this opportunity. ');
            }
            if(!Pattern.matches('^\\+[1-9]\\d{1,14}$', contactRel.Contact.Phone)){aOpp.addError('Contact Phone number format is invalid for ' + contactRel.Contact.FirstName + '. Expected format +5599999999999. ');
            }
        }
    }

    private static void validatePartnerProductsAndCommercialConditions( List<Product_Opportunity_Assignment__c> aProdLst , Opportunity aOpp ){

        for( Product_Opportunity_Assignment__c aProd : aProdLst ){
            if (String.isBlank( aProd.ProductAssignmentId__r.ProductId__r.Name )) {aOpp.addError(' Product Name is required to close this opportunity. ');
            }

            if ( aProd.ProductAssignmentId__r.CommercialConditionId__r.RecordType.DeveloperName.equals('CAP') && String.isBlank( String.valueOf(aProd.ProductAssignmentId__r.CommercialConditionId__r.Amount__c ))) {
                aOpp.addError(' Amount ($) in CAP Product is required to close this opportunity. ');
            }

            if ( String.isBlank( String.valueOf(aProd.ProductAssignmentId__r.ProductId__r.Net_Transfer_Price__c  ))) {aOpp.addError(' Net Transfer Price in Product(s) is required to close this opportunity. ');
            }
            
            if ( String.isBlank( aProd.ProductAssignmentId__r.ProductId__r.Type__c )) {aOpp.addError(' Product Type is required to close this opportunity. ');
            }
        }
    }
    */
    // Ending Validation TAGUS 


    public static List<Opportunity> validateW9Form (List<Opportunity> lstOpp, Map< Id , Opportunity > lMapOld ){  
        AccountSelector accSelector = (AccountSelector)Application.Selector.newInstance(Account.sobjectType); 
        List<FIleObject__c> fileLst = new List<FIleObject__c>(); 
        List<FIleObject__c> fileLstW9 = new List<FIleObject__c>();
        List<FIleObject__c> fileLstPhotos = new List<FIleObject__c>(); 
        List<FIleObject__c> fileLstLogo = new List<FIleObject__c>(); 
        List<Account> lstAccountW9 = new List<Account>();
        List<Opportunity> lLstOpp = lstOpp;
        Set<Id> lSetIdOpp = new Set<Id>();
        Set<Id> lSetIdAcc = new Set<Id>();  

        for( Opportunity iOpp : lstOpp){
            if(iOpp.StageName == constants.OPPORTUNITY_SET_UP_VALIDATION && lMapOld.get(iOpp.Id).StageName == constants.OPPORTUNITY_SIGNED_CONTRACT && RtPartner.contains(iOpp.RecordtypeId)){
                lSetIdOpp.add(iOpp.AccountId);
            }
        }
        lstAccountW9 = accSelector.selectAccForValidateW9(lSetIdOpp);
        for(Account lstAcc : lstAccountW9){lSetIdAcc.add(lstAcc.Id);}
        if(lSetIdAcc.isEmpty()){return lLstOpp;}

        fileLst = [SELECT Id, Name,File_Type__c FROM FIleObject__c WHERE  Account__c != null  AND Account__c =: lSetIdAcc];
        fileLstW9 = [SELECT Id, Name,File_Type__c FROM FIleObject__c WHERE Id IN : fileLst AND File_Type__c = :constants.FILEOBJECT_FILETYPE_W9];
        fileLstPhotos = [SELECT Id, Name,File_Type__c FROM FIleObject__c WHERE  Account__c != null  AND Account__c =: lSetIdAcc AND File_Type__c = :constants.FILEOBJECT_FILETYPE_PHOTO];
        fileLstLogo = [SELECT Id, Name,File_Type__c FROM FIleObject__c WHERE  Account__c != null  AND Account__c =: lSetIdAcc AND File_Type__c = :constants.FILEOBJECT_FILETYPE_LOGO];

             if(fileLstPhotos.size() < 5 && fileLstLogo.size() < 1 && fileLstW9.size() < 1){(new IntegrityError()).doAction('Upload 5 files in the account page and select “Photos” from the file type menu;  Upload a file in the account page and select “W9 Form” from the file type menu; Upload a file in the account page and select “Logo” from the file type menu');}
        else if(fileLstPhotos.size() > 4 && fileLstLogo.size() < 1 && fileLstW9.size() < 1){(new IntegrityError()).doAction('Upload a file in the account page and select “W9 Form” from the file type menu; Upload a file in the account page and select “Logo” from the file type menu');}
        else if(fileLstPhotos.size() < 5 && fileLstLogo.size() > 0 && fileLstW9.size() < 1){(new IntegrityError()).doAction('Upload 5 files in the account page and select “Photos” from the file type menu;  Upload a file in the account page and select “W9 Form” from the file type menu');}
        else if(fileLstPhotos.size() < 5 && fileLstLogo.size() < 1 && fileLstW9.size() > 0){(new IntegrityError()).doAction('Upload 5 files in the account page and select “Photos” from the file type menu;  Upload a file in the account page and select “Logo” from the file type menu');}
        else if(fileLstPhotos.size() > 4 && fileLstLogo.size() > 0 && fileLstW9.size() < 1){(new IntegrityError()).doAction('Upload a file in the account page and select “W9 Form” from the file type menu');}
        else if(fileLstPhotos.size() < 5 && fileLstLogo.size() > 0 && fileLstW9.size() > 0){(new IntegrityError()).doAction('Upload 5 files in the account page and select “Photos” from the file type menu');}
        else if(fileLstPhotos.size() > 4 && fileLstLogo.size() < 1 && fileLstW9.size() > 0){(new IntegrityError()).doAction('Upload a file in the account page and select “Logo” from the file type menu');}

        return lLstOpp;
    }

    public static List<Opportunity> validate5Photos (List<Opportunity> lstOpp, Map< Id , Opportunity > lMapOld ){  
        AccountSelector accSelector = (AccountSelector)Application.Selector.newInstance(Account.sobjectType); 
        List<FIleObject__c> fileLst = new List<FIleObject__c>(); 
        List<FIleObject__c> fileLstLogo = new List<FIleObject__c>(); 
        List<Account> lstAccount5Photos = new List<Account>();
        List<Opportunity> lLstOpp = lstOpp;
        Set<Id> lSetIdOpp = new Set<Id>();
        Set<Id> lSetIdAcc = new Set<Id>();  
        String searchKey = '%' + constants.FILEOBJECT_FILETYPE_PHOTO.toUpperCase() + '%';
        String searchKeyLogo = '%'+ constants.FILEOBJECT_FILETYPE_LOGO.toUpperCase() + '%';
        for( Opportunity iOpp : lstOpp){
            if(iOpp.StageName == constants.OPPORTUNITY_SET_UP_VALIDATION && lMapOld.get(iOpp.Id).StageName == constants.OPPORTUNITY_SIGNED_CONTRACT && RtPartner.contains(iOpp.RecordtypeId)){lSetIdOpp.add(iOpp.AccountId);
            }
        }


        lstAccount5Photos = accSelector.selectAccForValidate5Photos(lSetIdOpp);
        for(Account lstAcc : lstAccount5Photos){lSetIdAcc.add(lstAcc.Id);}
        if(lSetIdAcc.isEmpty()){return lLstOpp;}
        fileLst = [SELECT Id, Name FROM FIleObject__c WHERE  Account__c != null  AND Account__c =: lSetIdAcc AND (Name LIKE: searchKey OR File_Type__c = :constants.FILEOBJECT_FILETYPE_PHOTO)];
        fileLstLogo = [SELECT Id, Name FROM FIleObject__c WHERE  Account__c != null  AND Account__c =: lSetIdAcc AND (Name LIKE: searchKeyLogo OR File_Type__c = :constants.FILEOBJECT_FILETYPE_LOGO)];
        if(fileLst.size() < 5 && fileLstLogo.size() < 1){(new IntegrityError()).doAction('Upload 5 files in the account page and select “Photos” from the file type menu; Upload a file in the account page and select “Logo” from the file type menu');}
        else if(fileLst.size() > 4 && fileLstLogo.size() < 1){(new IntegrityError()).doAction('Upload a file in the account page and select “Logo” from the file type menu');}
        else if(fileLst.size() < 5 && fileLstLogo.size() > 0){(new IntegrityError()).doAction('Upload 5 files in the account page and select “Photos” from the file type menu');}

        return lLstOpp;
    }


    public static Opportunity createChildOpportunity(String childOppName, Date closeDate, String ownerId, String accountId, String masterOpportunityId, List<Id> accountInOppIds ){
        fflib_ISObjectUnitOfWork uow = Application.UnitOfWork.newInstance();
        Map<Id, Opportunity> opportunityToClone = ((OpportunitySelector)Application.Selector.newInstance(Opportunity.SObjectType)).selectOpportunityToCloneChildOpportunity( new Set<Id>{ masterOpportunityId } );
       
        List<Account> childAccountsWithLegalRepresentative = ((AccountSelector)Application.Selector.newInstance(Account.SObjectType)).selectByIdForPartnersWithoutContactRels( new Set<Id>( accountInOppIds ) );
        Id legalRepresentativeId;
        if( childAccountsWithLegalRepresentative != null && !childAccountsWithLegalRepresentative.isEmpty() ){
            for(Account childPartner : childAccountsWithLegalRepresentative){
                if(childPartner.Legal_Representative__c != null){
                    legalRepresentativeId = childPartner.Legal_Representative__c;
                    break;
                }
            }
        }

        accountInOppIds.add(accountId);
        Opportunity childOpportunity;
        if(!opportunityToClone.isEmpty()){
            childOpportunity = cloneOpportunity(opportunityToClone.get(masterOpportunityId));
            populateChildOpportunity(childOppName, closeDate, ownerId, accountId, masterOpportunityId, childOpportunity, legalRepresentativeId);    
            List<Account_Opportunity_Relationship__c> aOppMemberLst = new List<Account_Opportunity_Relationship__c>();
            for( Account_Opportunity_Relationship__c oppMember : createOpportunityMembers(childOpportunity, accountInOppIds, uow)){
                if(oppMember.Account__c == accountId){
                    aOppMemberLst.add(oppMember);
                }
            }
            cloneProductSchema(masterOpportunityId, accountId, aOppMemberLst, uow);
            uow.registerNew(childOpportunity);
            uow.commitWork();
        }
        return childOpportunity;
    }

    public static void validateOpportunityStage(Map<Id, Opportunity> opportunitiesByIds) {
        try {
            OpportunityStageValidator validator = new OpportunityStageValidator(opportunitiesByIds);
            validator.validate();
        } catch (Exception e) {
            SYSTEM.DEBUG(e.getMessage() + ' - ' + e.getLineNumber());
            DebugLog__c log = new DebugLog__c(
                    Origin__c = '[' + className + '][validateOpportunityStage]',
                    LogType__c = constants.LOGTYPE_ERROR,
                    RelatedToSquad__c = constants.LOGTYPE_SQUAD_PARTNERS,
                    ExceptionMessage__c = e.getMessage(),
                    StackTrace__c = e.getStackTraceString(),
                    DataScope__c = JSON.serialize(opportunitiesByIds.keySet())
            );
            Logger.createLog(log);
            System.debug('e.getStackTraceString() ' + e.getStackTraceString());
            throw new OpportunityServiceImplException(e.getMessage());
        }
    }

    private static void cloneProductSchema(Id masterOpportunityId, Id accountId,List<Account_Opportunity_Relationship__c> aOppMemberLst, fflib_ISObjectUnitOfWork uow){
        Map<Id, Set<Id>> prodCommerMapIds = new Map<Id, Set<Id>>();
            Set<Id> commercialIds = new Set<Id>();          
            for(Product_Opportunity_Assignment__c iProdAssign : ((ProductOpportunityAssignmentSelector)Application.Selector.newInstance(Product_Opportunity_Assignment__c.SObjectType)).selectByAccountAndOpportunity(accountId, masterOpportunityId )){
                commercialIds.add(iProdAssign.ProductAssignmentId__r.CommercialConditionId__c);                
                if(prodCommerMapIds.containsKey(iProdAssign.ProductAssignmentId__r.ProductId__c)){
                    prodCommerMapIds.get(iProdAssign.ProductAssignmentId__r.ProductId__c).add(iProdAssign.ProductAssignmentId__r.CommercialConditionId__c);
                } else {
                    prodCommerMapIds.put(iProdAssign.ProductAssignmentId__r.ProductId__c, new Set<id>{iProdAssign.ProductAssignmentId__r.CommercialConditionId__c});
                }
            }

            if(!prodCommerMapIds.isEmpty()){

                Map<Id, Product_Item__c> prodMap = ((ProductItemSelector)Application.Selector.newInstance(Product_Item__c.SObjectType)).selectProductByIdToClone( prodCommerMapIds.keySet() );

                Map<Id, List<Commercial_Condition__c>> commercialByProductIdmap = new Map<Id, List<Commercial_Condition__c>>();

                for(Commercial_Condition__c iComm : ((CommercialConditionSelector)Application.Selector.newInstance(Commercial_Condition__c.SObjectType)).selectByIdToClone( commercialIds) ) {

                    for(Id iProdId : prodCommerMapIds.keySet()){
                        Set<id> commercialSet = prodCommerMapIds.get(iProdId);
                        if(commercialSet.contains(iComm.Id)){
                            if(commercialByProductIdmap.containsKey(iProdId)){
                                commercialByProductIdmap.get(iProdId).add(iComm);
                            } else {
                                commercialByProductIdmap.put(iProdId, new List<Commercial_Condition__c>{iComm}); 
                            } 
                        }
                    }
                }
                if(!commercialByProductIdmap.isEmpty()){
                    createProductSchema(commercialByProductIdmap,prodMap,aOppMemberLst, uow);
                } 
            }
          
    }
    
    private static void populateChildOpportunity(String childOppName, Date closeDate, String ownerId, String accountId, String masterOpportunityId, Opportunity childOpportunity, Id legalRepresentativeId ){
        Id childOppRt = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(constants.OPPORTUNITY_RT_PARTNER_FLOW_CHILD_OPP).getRecordTypeId();
        childOpportunity.recordTypeId = childOppRt;
        childOpportunity.Name = childOppName;
        childOpportunity.StageName = constants.OPPORTUNITY_STAGE_QUALIFICACAO;
        childOpportunity.UUID__c = '';
        childOpportunity.MasterOpportunity__c = masterOpportunityId;
        childOpportunity.AccountId = accountId;
        childOpportunity.CloseDate = closeDate;
        childOpportunity.OwnerId = ownerId;
        childOpportunity.Legal_representative__c = legalRepresentativeId;
    }

    private static List<Account_Opportunity_Relationship__c> createOpportunityMembers(Opportunity childOpportunity, List<Id> accountInOppIds, fflib_ISObjectUnitOfWork uow ){     
        Id oppMemberRt = Schema.SObjectType.Account_Opportunity_Relationship__c.getRecordTypeInfosByDeveloperName().get(constants.OPPORTUNITY_MEMBER_RT_PARTNER_FLOW).getRecordTypeId();
        List<Account_Opportunity_Relationship__c> aOppMemberLst = new List<Account_Opportunity_Relationship__c>();
        for(Id iAccId : accountInOppIds){
            Account_Opportunity_Relationship__c oppMember = new Account_Opportunity_Relationship__c();
            oppMember.RecordTypeId = oppMemberRt;
            oppMember.Account__c = iAccId;
            oppMember.From_Child_Opp_Creation_Process__c = true;
            uow.registerNew( oppMember, Account_Opportunity_Relationship__c.Opportunity__c, childOpportunity );
            aOppMemberLst.add(oppMember);
        }
        return aOppMemberLst;
    }

    private static void createProductSchema( Map<Id, List<Commercial_Condition__c>>  commercialByProductIdmap, Map<Id, Product_Item__c> prodMap, List<Account_Opportunity_Relationship__c> aOppMemberLst, fflib_ISObjectUnitOfWork uow){
        Id prodAssignRt = Schema.SObjectType.Product_Assignment__c.getRecordTypeInfosByDeveloperName().get(constants.PRODUCT_ASSIGNMEN_RT_PARTNER_FLOW).getRecordTypeId();
        Id prodOppAssignRt = Schema.SObjectType.Product_Opportunity_Assignment__c.getRecordTypeInfosByDeveloperName().get(constants.PRODUCT_OPPORTUNITY_ASSIGNMEN_RT_PARTNER_FLOW).getRecordTypeId();
        Id prodActivityRelRt = Schema.SObjectType.Product_Activity_Relationship__c.getRecordTypeInfosByDeveloperName().get(constants.PRODUCT_ACTIVITY_RT_PARTNER_FLOW).getRecordTypeId();
        Id thresholdRt =  Schema.SObjectType.Threshold__c.getRecordTypeInfosByDeveloperName().get(constants.THRESHOLD_RT_PARTNER_FLOW).getRecordTypeId();

        List<Product_Activity_Relationship__c> productActivityRelationships = ((ProductActivityRelationshipSelector)Application.Selector.newInstance(Product_Activity_Relationship__c.SObjectType)).selectActivityByProductId( prodMap.keySet() );
        List<Product_Threshold_Member__c> thresholdMembers = ((ProductThresholdMemberSelector)Application.Selector.newInstance(Product_Threshold_Member__c.SObjectType)).selectThresholdByProductId( prodMap.keySet() );

        for(Id iProdId : prodMap.keySet()){
            Product_Item__c nProduct = prodMap.get(iProdId).clone(false, true, false, false);
            nProduct.UUID__c = '';
            nProduct.Opportunity__c = null;
            nProduct.Parent_Product__c = iProdId;
            nProduct.RecordTypeId = Schema.SObjectType.Product_Item__c.getRecordTypeInfosByDeveloperName().get(constants.PRODUCT_ITEM_CHILD_RT_PARTNER_FLOW).getRecordTypeId(); 
            uow.registerNew( nProduct );

            for(Product_Activity_Relationship__c iActRel : productActivityRelationships){
                Product_Activity_Relationship__c nActRel = new Product_Activity_Relationship__c();
                nActRel.Gym_Activity_Relationship__c = iActRel.Gym_Activity_Relationship__c;
                uow.registerNew( nActRel, Product_Activity_Relationship__c.Product__c, nProduct );
            }

            for(Product_Threshold_Member__c thresholdMember : thresholdMembers ){
                Threshold__c nThreshold = new Threshold__c();
                nThreshold.recordTypeId = thresholdRt;
                nThreshold.Volume_discount_date__c = thresholdMember.Threshold__r.Volume_discount_date__c;
                nThreshold.Discount__c = thresholdMember.Threshold__r.Discount__c;
                nThreshold.Volume_discount_date__c = thresholdMember.Threshold__r.Volume_discount_date__c;
                nThreshold.Threshold_value_start__c = thresholdMember.Threshold__r.Threshold_value_start__c;
                Product_Threshold_Member__c nThresholdMember = new Product_Threshold_Member__c();
                uow.registerNew( nThreshold );
                uow.registerNew( nThresholdMember, Product_Threshold_Member__c.Product__c, nProduct );
                uow.registerRelationship( nThresholdMember, Product_Threshold_Member__c.Threshold__c, nThreshold );
            }

            for( Commercial_Condition__c iCommercial : commercialByProductIdmap.get(iProdId) ){
                Commercial_Condition__c nCommercial = iCommercial.clone(false, true, false, false);
                uow.registerNew( nCommercial );
                Product_Assignment__c nProdAssign = new Product_Assignment__c();
                nProdAssign.RecordTypeId = prodAssignRt;
                uow.registerNew( nProdAssign, Product_Assignment__c.CommercialConditionId__c, nCommercial );
                uow.registerRelationship( nProdAssign, Product_Assignment__c.ProductId__c, nProduct );
                for(Account_Opportunity_Relationship__c iOppMember : aOppMemberLst){
                    Product_Opportunity_Assignment__c nProdOppAssign = new Product_Opportunity_Assignment__c();
                    nProdOppAssign.recordTypeId = prodOppAssignRt;
                    uow.registerNew( nProdOppAssign, Product_Opportunity_Assignment__c.ProductAssignmentId__c, nProdAssign );
                    uow.registerRelationship( nProdOppAssign, Product_Opportunity_Assignment__c.OpportunityMemberId__c, iOppMember );
                }
            }
        }
    }


    public class IntegrityError {
        public IntegrityError() {}
        
        public void doAction(String msg) {
            // Trigger.new is valid here
            SObject[] sobjects = Trigger.new;
            for (Sobject sobj : sobjects) {
                sobj.addError(msg);
            }
        }
    }
    public class OpportunityServiceImplException extends Exception {}
}