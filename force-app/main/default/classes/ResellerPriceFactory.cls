public class ResellerPriceFactory {

    private ResellerPriceResponse response;
    private Decimal value;
    private Decimal quantity;

    public ResellerPriceFactory(Opportunity opp, PricebookEntry pbEntry){
        this.quantity = opp.TotalOpportunityQuantity == null ? 0 : opp.TotalOpportunityQuantity ;
        this.value = pbEntry.UnitPrice == null ? 0 : pbEntry.UnitPrice;
    }
    
    public ResellerPriceFactory(Opportunity opp, Legacy_Partner_Price__c legacy){
        this.quantity = opp.TotalOpportunityQuantity == null ? 0 : opp.TotalOpportunityQuantity ;
        this.value = legacy.Legacy_Price__c == null ? 0 : legacy.Legacy_Price__c;
    }
    
    private ResellerPriceResponse setValues(){
        Decimal pricePerElegible = this.value;
        Decimal quantityOpp = this.quantity;
        Decimal totalPrice = pricePerElegible * quantityOpp;
        return new ResellerPriceResponse(totalPrice, pricePerElegible);
    }

    public ResellerPriceResponse build(){
        this.response = setValues();
        return this.response;
    }
}