/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 12-15-2021
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/
public with sharing class NetworkBuilderCreateNewAccountController {
    
    @AuraEnabled(cacheable=true)
    public static List<AccountContactRelation> getContactsFromParent(Id recordId){
        return AccountService.getContactsFromParent(recordId,'Partner_Flow_Account');
        //return AccountService.getContactsFromParent(recordId,'Gyms_Partner');
    }    

    @AuraEnabled(cacheable=true)
    public static Id getRecordTypeId(String recordTypeDevName){ 
        return ContactService.getRecordTypeId(recordTypeDevName);
    }   
}