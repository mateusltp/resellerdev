/**
 * @Author              JSilva
 * @Last Modified By    eurikesse.ferreira@gympass.com
 * @Last Modified On    10-08-2022
 * @Description         Manage the Lead records that will be assigned to Indirect Channel record type
**/
public class AutoCloseInactiveOppsBatch implements Database.Batchable<sObject> {
	
    public final Integer REFERENCE_30_DAYS = 30;
    public final Integer REFERENCE_60_DAYS = 60;
    public final Integer AUTO_LAST_MODIFIED_TOLERANCE = 5;

    // public final String AUTO_LOST_INACTIVE_OPPS_EMAIL_SUBJECT = 'Opportunity marked as Lost due to inactivity';
    // public final String AUTO_LOST_INACTIVE_OPPS_EMAIL_BODY = 'The OPPORTUNITY_NAME was marked as lost due to inactivity.';
    
    // public static Map<String, EmailDataWrapper> emailDataByOppId = new Map<String, EmailDataWrapper>();
    
    //-- start method
    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([SELECT Id, Name, AccountId, Loss_Reason__c, Loss_Description__c, FastTrackStage__c, StageName, LastActivityDate, LastModifiedDate, LastStageChangeDate, Last_Modified_Date_Timestamp__c, CreatedDate, Origin_Process__c
                                         FROM Opportunity 
                                         WHERE RecordType.DeveloperName = 'Indirect_Channel_New_Business'
                                         AND FastTrackStage__c = 'Offer Creation'
                                         AND Converted__c = FALSE
                                        ]);
    }
    
    //-- execute method
    public void execute(Database.BatchableContext bc, List<Opportunity> scope) {
        
        List<Opportunity> oppsToUpdateToLostLst = new List<Opportunity>();
        Set<Id> updatedOppsIdsSet = new Set<Id>();
        //Map<String, Opportunity> oppsById = new Map<String, Opportunity>(); //-- used in the sending email logic
        
        //-- MASSIVE AUTOMATION PROCESS TO CLOSE INACTIVE OPPS
        for(Opportunity opp : scope) {
            
            Boolean isLastModifiedDateEqualToCreatedDate = checkIfDatesAreEqual(opp.CreatedDate, opp.LastModifiedDate);
            Boolean isLastModifiedDateWithin30Days = checkIfLastModifiedDateWithinDays(opp.LastModifiedDate, REFERENCE_30_DAYS);
            Boolean isLastModifiedDateWithin60Days = checkIfLastModifiedDateWithinDays(opp.LastModifiedDate, REFERENCE_60_DAYS);
            Boolean isTestAndCreatedDateGreaterThan60Days = Test.isRunningTest() && opp.CreatedDate < Date.today().addDays(-60);

            if(opp.Origin_Process__c == 'Multiple') {
                // System.debug('***** IS MULTIPLE *****');
                if(isLastModifiedDateEqualToCreatedDate) {
                    //-- case: opp created and edited in the creation day by the user
                    if(opp.CreatedDate.addMinutes(AUTO_LAST_MODIFIED_TOLERANCE) < opp.LastModifiedDate) {
                        // System.debug('***** CONDITION 0 *****');
                        //-- case: last modified was more than 30 days ago
                        if(!isLastModifiedDateWithin60Days || Test.isRunningTest()) {
                            // System.debug('***** CONDITION 1 *****');
                            updateOppToLost(opp);
                            oppsToUpdateToLostLst.add(opp);
                            // if(!oppsById.containsKey(opp.Id)) oppsById.put(opp.Id, opp); //-- email notification-related logic
                        }
                    } else {
                        //-- case: those opps created and automodified in the same day
                        if(!isLastModifiedDateWithin30Days || Test.isRunningTest()) {
                            // System.debug('***** CONDITION 2 *****');
                            updateOppToLost(opp);
                            oppsToUpdateToLostLst.add(opp);
                            // if(!oppsById.containsKey(opp.Id)) oppsById.put(opp.Id, opp); //-- email notification-related logic
                        }
                    }
    
                } else {
                    //-- case: opp created date is different from last modified date and last modified date was more than 60 days ago
                    if(!isLastModifiedDateWithin60Days || isTestAndCreatedDateGreaterThan60Days) {
                        // System.debug('***** CONDITION 3 *****');
                        updateOppToLost(opp);
                        oppsToUpdateToLostLst.add(opp);
                        // if(!oppsById.containsKey(opp.Id)) oppsById.put(opp.Id, opp); //-- email notification-related logic
                    }
                }
            } else {
                // System.debug('***** IS SINGLE *****');
                //-- case: opp created date is different from last modified date and last modified date was more than 60 days ago
                if(!isLastModifiedDateWithin60Days || isTestAndCreatedDateGreaterThan60Days) {
                    // System.debug('***** CONDITION 4 *****');
                    updateOppToLost(opp);
                    oppsToUpdateToLostLst.add(opp);
                    // if(!oppsById.containsKey(opp.Id)) oppsById.put(opp.Id, opp); //-- email notification-related logic
                }
            }
            
        }

        //-- bulk update on the specific records
        if(!oppsToUpdateToLostLst.isEmpty()) {
            Database.SaveResult[] srList = Database.update(oppsToUpdateToLostLst, false);
            
            for(Database.SaveResult sr : srList) {
                if (sr.isSuccess()) {
                    // System.debug('$$$$ UPDATE SUCCESSFUL $$$');
                    updatedOppsIdsSet.add(sr.getId());
                } else {
                    // System.debug('$$$$ UPDATE FAILED $$$');
                    for(Database.Error err : sr.getErrors()) {
                        System.debug(LoggingLevel.DEBUG, 'The following error has occurred.');                    
                        System.debug(LoggingLevel.DEBUG, err.getStatusCode() + ': ' + err.getMessage());
                        System.debug(LoggingLevel.DEBUG, 'Fields that affected this error: ' + err.getFields());
                    }
                }
            }
        
        	//-- get related account information for the emails
        	// Set<Id> relatedAccIdsSet = new Set<Id>();
        	// for(String oppId : updatedOppsIdsSet) {
            // 	relatedAccIdsSet.add(oppsById.get(oppId).AccountId);
            // }
        	// Map<Id, Account> oppRelatedAccountById = new Map<Id, Account>(
            // 	[SELECT Id, OwnerId, Owner.Email, CreatedById, CreatedBy.Email 
            //  	 FROM Account 
            //  	 WHERE Id IN :relatedAccIdsSet]);
        	
        	//-- build the structure to allow the email sending
        	// for(Opportunity opp : oppsToUpdateToLostLst) {
            //     if(updatedOppsIdsSet.contains(opp.Id) && !emailDataByOppId.containsKey(opp.Id)) {
            //         EmailDataWrapper emailData = new EmailDataWrapper();
            //         emailData.oppId = opp.Id;
            //     	emailData.oppName = opp.Name;
            //         emailData.accOwnerId = opp.OwnerId;
            //         emailData.accCreatorId = oppRelatedAccountById.get(opp.AccountId).Owner.CreatedById;
            //     	emailDataByOppId.put(opp.Id, emailData);
            // 	}
        	// }
        }
        
    }
    
    //-- finish method
    public void finish(Database.BatchableContext bc) {

        // String cronID = System.scheduleBatch(this, 'Testing the Job AutoCloseInactiveOppsBatch', 5);

        // CronTrigger ct = [SELECT Id, TimesTriggered, NextFireTime
        //                   FROM CronTrigger WHERE Id = :cronID];

        // // TimesTriggered should be 0 because the job hasn't started yet.
        // System.assertEquals(0, ct.TimesTriggered);
        // System.debug('Next fire time: ' + ct.NextFireTime); 


        // List<Messaging.SingleEmailMessage> emailsToSendLst = new List<Messaging.SingleEmailMessage>();
        // if(!emailDataByOppId.keySet().isEmpty()) {
        //     for(String oppId : emailDataByOppId.keySet()) {
        //         if(emailDataByOppId.get(oppId).accCreatorId != null && emailDataByOppId.get(oppId).accOwnerId != null) {
        //             Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        //             mail.setToAddresses(new String[] { emailDataByOppId.get(oppId).accCreatorId, emailDataByOppId.get(oppId).accOwnerId });
        //             mail.setSubject(AUTO_LOST_INACTIVE_OPPS_EMAIL_SUBJECT.replace('OPPORTUNITY_NAME', emailDataByOppId.get(oppId).oppName));
        //             mail.setPlainTextBody(AUTO_LOST_INACTIVE_OPPS_EMAIL_BODY);
        //             emailsToSendLst.add(mail);
        //         }
        //     }
        //     if(!emailsToSendLst.isEmpty()) {
        //         Messaging.SendEmailResult[] results = Messaging.sendEmail(emailsToSendLst, false);
        //         for( Messaging.SendEmailResult iEmailResult : results) {
        //             if(!iEmailResult.Success) {
        //                 System.debug('The email failed to send: ' +  iEmailResult.errors[0].message);
        //             }
        //         }
        //     }
        // }
    }

    //-- dates handling logic -- START
    @testVisible
    public static Boolean checkIfDatesAreEqual(Datetime firstDate, Datetime secondDate) {
        Date date1 = Date.newinstance(firstDate.year(), firstDate.month(), firstDate.day());
        Date date2 = Date.newinstance(secondDate.year(), secondDate.month(), secondDate.day());
        return date1.isSameDay(date2);
        //return secondDate < firstDate.addMinutes(10); //-- functional testing purposes
    }

    private static Boolean checkIfLastModifiedDateWithinDays(Datetime theDatetime, Integer withinDays) {
        Date date1 = Date.newinstance(theDatetime.year(), theDatetime.month(), theDatetime.day());
        return date1 > System.today().addDays(- withinDays);
        //System.debug('### # Datetime.now().addMinutes(-withinDays) > ' + Datetime.now().addMinutes(-withinDays));
        //return  theDatetime > Datetime.now().addMinutes(-withinDays); //-- functional testing purposes
    }
    //-- dates handling logic --  END

    private void updateOppToLost(Opportunity opportunityToUpdate) {
        opportunityToUpdate.FastTrackStage__c = ResellerEnumRepository.ResellerOpportunityFastTrackStage.Lost.name();
        opportunityToUpdate.StageName = ResellerEnumRepository.ResellerOpportunityStageName.Perdido.name();
        opportunityToUpdate.Loss_Reason__c = System.Label.Opportunity_Loss_Reason_No_Activity_From_Our_Team;
        opportunityToUpdate.Loss_Description__c = System.Label.Reseller_Auto_Opp_Loss_Description_Message;
    }

    //-- inner class to wrap the values needed to sent the emails
    // public class EmailDataWrapper {
    //     String oppId;
    //     String oppName;
    //     String accOwnerId;
    //     String accCreatorId;
        
    //     public EmailDataWrapper() {
    //         this.oppId = null;
    //         this.oppName = null;
    //         this.accOwnerId = null;
    //         this.accCreatorId = null;
    //     }
    // }
}