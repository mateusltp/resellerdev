/**
 * @description       : 
 * @author            : GEPI@GFT.com
 * @group             : 
 * @last modified on  : 07-12-2022
 * @last modified by  : gft.samuel.silva@ext.gympass.com
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   07-18-2021   GEPI@GFT.com   Initial Version
**/
public inherited sharing class AccountSelector extends ApplicationSelector{
    

	private Set<Id> gChildAccountTree = new Set<Id>();
	private static final Set<Id> accountPartner = 
	new Set<Id>{    Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gym_Partner').getRecordTypeId(),
	Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Account').getRecordTypeId()};

    public List<Schema.SObjectField> getSObjectFieldList() {
		return new List<Schema.SObjectField> {
			Account.Id,
			Account.Name,
            Account.ParentID,
			Account.GP_Status__c,												
			Account.Google_ID__c,
			Account.OwnerId,
			Account.Wishlist__c
		};
	}

	public Schema.SObjectType getSObjectType() {
		return Account.sObjectType;
	}

    public List<Account> selectById(Set<Id> ids) {
		return (List<Account>) super.selectSObjectsById(ids);
	}


	public List<Account> selectByIdForPartners(Set<Id> ids) {	
		
		fflib_QueryFactory accountsQueryFactory = newQueryFactory(false);
		fflib_QueryFactory accountContactRelationFactory = new AccountContactRelationSelector().addQueryFactorySubselect(accountsQueryFactory);
		accountContactRelationFactory.selectField('Contact.Id');
		accountContactRelationFactory.selectField('Contact.Name');
		accountContactRelationFactory.selectField('Contact.Email');
		accountContactRelationFactory.setCondition('Contact.Type_of_contact__c includes (\'Legal Representative	\')');
		return ( List<Account> ) Database.query( 					
												accountsQueryFactory.selectField(Account.Id).
												selectField(Account.ParentId).
												selectField(Account.Name).
												selectField(Account.Partner_Level__c).		
												selectField(Account.Types_of_ownership__c).		
												selectField(Account.ShippingCity).	
												selectField(Account.ShippingStreet).	
												selectField(Account.ShippingState).	
												selectField(Account.ShippingCountryCode).											
												selectField(Account.BillingCountryCode).
												selectField(Account.RecordTypeId).
												selectField(Account.Legal_Representative__c).
												selectField('Legal_Representative__r.Name').
												selectField('Legal_Representative__r.Email').
												selectField(Account.Site).
												selectField('RecordType.DeveloperName').
												selectField(Account.OwnerId).
												setCondition('Id in : ids').toSOQL()
											);
	}

	public List<Account> selectByIdForPartnersWithoutContactRels(Set<Id> ids) {	
		return ( List<Account> ) Database.query( 					
			newQueryFactory().
			selectField(Account.Id).
			selectField(Account.ParentId).
			selectField(Account.Name).
			selectField(Account.Gym_Type__c).
			selectField(Account.Partner_Level__c).		
			selectField(Account.Types_of_ownership__c).		
			selectField(Account.ShippingCity).	
			selectField(Account.ShippingStreet).	
			selectField(Account.ShippingState).	
			selectField(Account.ShippingCountryCode).											
			selectField(Account.BillingCountry).											
			selectField(Account.BillingCountryCode).
			selectField(Account.RecordTypeId).
			selectField(Account.Legal_Representative__c).
			selectField('Legal_Representative__r.Name').
			selectField('Legal_Representative__r.Email').
			selectField(Account.Site).
			selectField(Account.Wishlist__c).
			selectField('RecordType.DeveloperName').		 										
			setCondition('Id in : ids').toSOQL()
		);
	}

	public Map<Id, Account> selectByIdForPartnersAsMap(Set<Id> ids) {
		return new Map<Id, Account>(selectByIdForPartners(ids));
	}

	/** tags: clone, cloning, to clone, won, partners clone 
	 * toClone = all fields from sobject
	*/
    public Map<Id, Account> selectAccountToClone( Set<Id> aAccountList ) {
									
		return new Map<Id, Account>( (List<Account>) Database.query(
																newQueryFactory().
																selectFields( getAllFieldsFromAccount() ).
																setCondition( 'Id in : aAccountList' ).
																toSOQL()
													));
	}


	/**
	 *  return all accounts in tree (hierarchy)
	 */
    public Set<Id> selectChildAccountTree(Set<Id> parentIds){
		
        Set<Id> lChildreenIds = new Set<ID>(selectByParentId(parentIds).keySet());
        if(lChildreenIds.size()>0) {
            gChildAccountTree.addAll(lChildreenIds);
            selectChildAccountTree(lChildreenIds);
        }

        return gChildAccountTree;
    }

	
    public Map<Id, Account> selectByParentId ( Set<Id> parentIds ){
		fflib_QueryFactory accountsQueryFactory = newQueryFactory(false);
		fflib_QueryFactory accountContactRelationFactory = new AccountContactRelationSelector().addQueryFactorySubselect(accountsQueryFactory);
		accountContactRelationFactory.selectField('Contact.Id');
		accountContactRelationFactory.selectField('Contact.Name');
		accountContactRelationFactory.selectField('Contact.Email');
		accountContactRelationFactory.setCondition('Contact.Type_of_contact__c includes (\'Legal Representative	\')');
		return new Map<Id, Account>( ( List<Account> ) Database.query( 					
								accountsQueryFactory.selectField(Account.Id).
								selectField(Account.ParentId).
								selectField(Account.Name).
								selectField(Account.Partner_Level__c).		
								selectField(Account.Types_of_ownership__c).	
								selectField(Account.ShippingState).	
								selectField(Account.ShippingCountry).
								selectField(Account.ShippingCountryCode).											
								selectField(Account.Legal_Representative__c).
								selectField(Account.BillingCountryCode).
								selectField('Legal_Representative__r.Name').
								selectField('Legal_Representative__r.Email').
								selectField('RecordType.DeveloperName').
								selectField(Account.OwnerId).
								setCondition('parentID in :parentIds').toSOQL()
						));
    }

	


	/** tags: TAGUS, Partners
	*/
	public Map<Id, Account> selectAccountMapWithFieldsToTagusParse ( List<Account> aAccountLst ){
		fflib_QueryFactory accountsQueryFactory = newQueryFactory(false);
		fflib_QueryFactory accountContactRelationFactory = new AccountContactRelationSelector().addQueryFactorySubselect(accountsQueryFactory);
		accountContactRelationFactory.selectField('Contact.Email');
		accountContactRelationFactory.selectField('Contact.FirstName');
		accountContactRelationFactory.selectField('Contact.LastName');
		accountContactRelationFactory.selectField('Contact.Id');
		accountContactRelationFactory.selectField('Contact.phone');
		accountContactRelationFactory.selectField('Contact.Type_of_contact__c');
		accountContactRelationFactory.selectField('Contact.UUID__c');
		accountContactRelationFactory.setCondition('Contact.Type_of_contact__c includes (\'Admin\')');
		return new Map<Id, Account> ( ( List<Account> ) Database.query( 					
														accountsQueryFactory.selectField(Account.UUID__c).
														selectField(Account.CurrencyIsoCode).
														selectField(Account.BillingCountryCode).
														selectField(Account.BillingStreet).
														selectField(Account.BillingPostalCode).
														selectField(Account.BillingState).
														selectField(Account.BillingCity).
														selectField(Account.Legal_Document_Type__c).
														selectField(Account.Id_Company__c).
														selectField(Account.Legal_Title__c).
														selectField(Account.CRM_ID__c).
														selectField(Account.Id).
														selectField(Account.Name).														
														selectField(Account.Gym_Type__c).													
														setCondition('Id in : aAccountLst').toSOQL()
												));
	}

	/** tags: TAGUS, Partners
	*/
	public List<Account> selectToTagus ( Set<Id> aAccountIds ){

		return  ( List<Account> ) Database.query( 					
													newQueryFactory(false).
													setEnforceFLS(true).
													selectField(Account.UUID__c).
													selectField(Account.Id).
													setCondition('Id in : aAccountIds AND UUID__c != NULL AND Send_To_Tagus__c = true').
													toSOQL()
												);												  
	}

	public List<Account> selectFieldsToSetUp ( Set<Id> aAccountIds ){

		return  ( List<Account> ) Database.query( 					
													newQueryFactory(false).
													selectField(Account.Id).
													selectField(Account.ShippingCountry).
													setCondition('Id in : aAccountIds').
													toSOQL()
												);												  
	}

	public Map<Id, Account> selectWishlist(Set<Id> accountIds) {

		return new Map<Id, Account>(( List<Account> ) Database.query(
				newQueryFactory(false).
						selectField(Account.Wishlist__c).
						setCondition('Id IN :accountIds').
						toSOQL()
		));
	}


	/** tags: Partners, avoid duplicated name
	*/

	public Map<String,Account> selectAccountNamePartners ( List<Account> aNewAccLst ){

		Set<String> nameSet = new Set<String>();
		Set<Id> idSet = new Set<Id>();

		for (Account iAcc : aNewAccLst){
			idSet.add(iAcc.Id);
			nameSet.add(iAcc.Name);
		}
	
		Map<String,Account> accSelectorsMap = new Map<String,Account> ();
        for(Account iAcc : (List<Account>) Database.Query(
														newQueryFactory(false).
															selectField(Account.Name).
															selectField(Account.RecordTypeId).
															setCondition('Name IN: nameSet AND Id NOT IN: idSet AND RecordTypeId IN: accountPartner').
															toSOQL())){

			accSelectorsMap.put(iAcc.Name,iAcc);
		}	
		return accSelectorsMap;
 
    }
	public List<Account> selectAccForValidateW9 ( Set<Id> aAccountIds ){

		return new List<Account>( (List<Account>)Database.query(
			newQueryFactory().
				selectField(Account.Id).
				selectField(Account.ShippingState).
				selectField(Account.ShippingCountryCode).				 
				setCondition('Id in : aAccountIds AND RecordTypeId IN: accountPartner AND ShippingCountryCode = \'US\'').
				toSOQL()));
}

	public List<Account> selectAccForValidate5Photos ( Set<Id> aAccountIds ){

		return new List<Account>( (List<Account>)Database.query(
			newQueryFactory().
				selectField(Account.Id).			 
				setCondition('Id in : aAccountIds AND RecordTypeId IN: accountPartner').
				toSOQL()));
}



		public Account selectContryAccount ( Id accountId ){

			return ( Account ) Database.query( 					
														newQueryFactory(false).
														selectField(Account.Id).
														selectField(Account.ShippingCountry).
														selectField(Account.ShippingCountryCode).
														selectField(Account.BillingCountry).				 
														selectField(Account.BillingCountryCode).				 				 
														setCondition('Id =: accountId').
														toSOQL());
		}

	private Set<String> getAllFieldsFromAccount(){
		Map<String, Schema.SObjectField> accountFields = Schema.getGlobalDescribe().get('Account').getDescribe().fields.getMap();
		Set<String> apiFieldsNameSet = new Set<String>();
		for (String key : accountFields.keySet()) {
			if (key != 'tier')
				apiFieldsNameSet.add(key);
		}
        return apiFieldsNameSet;
    }

	public List<Account> selectByNetworkId(Set<String> networkIdsSet, Set<String> accountIdsSet) {	
		
		fflib_QueryFactory accountsQueryFactory = newQueryFactory(false);
	
		return ( List<Account> ) Database.query( 					
												accountsQueryFactory.selectField(Account.Id).
												selectField(Account.ParentId).
												selectField(Account.Name).
												selectField(Account.Network_ID__c).
												selectField(Account.Ultimate_Parent__c).
												selectField(Account.RecordTypeId).											
												selectField('RecordType.DeveloperName').		 										
												setCondition('Network_ID__c in : networkIdsSet AND RecordTypeId in : accountPartner AND Id not in : accountIdsSet').toSOQL()
											);
	}

	public Map<Id, Account> selectWithOpportunitiesById(Set<Id> accountIds) {
		fflib_QueryFactory accountsQueryFactory = newQueryFactory(false)
				.setCondition('Id IN :accountIds');

		accountsQueryFactory.subSelectQuery('Opportunities')
				.selectFields(new Set<String> {'Id', 'Name', 'StageName', 'Owner_Name__c'})
				.setOrdering('CreatedDate', fflib_QueryFactory.SortOrder.DESCENDING);

		accountsQueryFactory.subselectQuery('Inherited_Opportunity__r')
				.selectFields(new Set<String> {'Opportunity__c'})
				.setOrdering('CreatedDate', fflib_QueryFactory.SortOrder.DESCENDING);

		return new Map<Id, Account> ((List<Account>)Database.query(accountsQueryFactory.toSOQL()));
	}

	public List<Account> selectWithExclusivityApprovalStatusById(Set<Id> accountIds) {
		fflib_QueryFactory accountsQueryFactory = newQueryFactory()
				.selectField('Approval_Status_Exclusivity__c')
				.selectField('BillingCountryCode')
				.setCondition('Id IN :accountIds');

		return (List<Account>)Database.query(accountsQueryFactory.toSOQL());
	}


	public Map<String, List<Account>> selectByGoogleId(Set<String> googleIds) {
		Map<String, List<Account>> response = new Map<String, List<Account>>();
        for(Account iAcc : (List<Account>) Database.Query(
														newQueryFactory(false).
															selectField(Account.Name).
															selectField(Account.GP_Status__c).												
															selectField(Account.Google_ID__c).
															selectField(Account.OwnerId).
															selectField(Account.Wishlist__c).
															setCondition('Google_ID__c IN: googleIds AND Google_ID__c != null').
															toSOQL())){
			if( response.containsKey( iAcc.Google_ID__c ) ){
				response.get(iAcc.Google_ID__c).add(iAcc);
			} else {
				response.put(iAcc.Google_ID__c, new List<Account>{iAcc});
			}		
		}	
		return response;
	}
}