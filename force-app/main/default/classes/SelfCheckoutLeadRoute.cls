@RestResource(urlMapping='/self-checkout-lead/*')
global with sharing class SelfCheckoutLeadRoute {
    @HttpPost
    global static SelfCheckoutLeadResponse post() {
        RestResponse restResponse = new SelfCheckoutLeadController().post();
    
        return new SelfCheckoutLeadResponse(restResponse);
    }

    global class SelfCheckoutLeadResponse {
        private Integer statusCode;
        private String errorMessage;
    
        private SelfCheckoutLeadResponse(RestResponse restResponse) {
          this.statusCode = restResponse.statusCode;
          this.errorMessage = restResponse?.responseBody?.toString();
        }
    
        public Integer getStatusCode() {
          return this.statusCode;
        }
    
        public String getErrorMessage() {
          return this.errorMessage;
        }
    }

}