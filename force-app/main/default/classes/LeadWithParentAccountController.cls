/**
 * @File Name          : LeadWithParentAccountController.cls
 * @Description        : 
 * @Author             : Samuel Silva - GFT (slml@gft.com)
 * @Group              : 
 * @Last Modified By   : Samuel Silva - GFT (slml@gft.com)
 * @Last Modified On   : 08/06/2020 14:41:20
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    05/06/2020   Samuel Silva - GFT (slml@gft.com)     Initial Version
**/
public with sharing class LeadWithParentAccountController {
    
    @AuraEnabled
    public static List<Account> findPotentialParentAccount(String companyName, String street){
        return LeadWithParentAccountModel.findPotentialParentAccount(companyName, street);
    }
    
    @AuraEnabled
    public static List<Account> findPotentialParentAccount(String companyName, String street, String website){
        return LeadWithParentAccountModel.findPotentialParentAccount(companyName, street, website);
    }
}