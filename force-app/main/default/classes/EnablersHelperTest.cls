/**
 * @description       : 
 * @author            : AlyssonMota
 * @group             : 
 * @last modified on  : 06-11-2021
 * @last modified by  : roei@gft.com
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   09-18-2020   Alysson Mota   Initial Version
**/

@isTest(seeAllData=false)
public class EnablersHelperTest {
    
    @TestSetup
    static void setup(){
        Pricebook2 standardPricebook = new Pricebook2(
	        Id = Test.getStandardPricebookId(),
	        IsActive = true
        );

        update standardPricebook;
    }

    @isTest
    public static void testUpdateStepsForQuote() {
        Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
        Id oppRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        Id quoteRecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        
        List<Opportunity> oppInsertList = new List<Opportunity>();
        List<Opportunity> oppUpdateList = new List<Opportunity>();
        List<Quote> quoteInsertList = new List<Quote>();
        
        Account acc = new Account();
        acc.RecordTypeId = accRecordTypeId;
        acc.Name = 'Test Account';
        acc.BillingCountry = 'Brazil';
        acc.BillingState = 'São Paulo';
        insert acc;

        Contact c = new Contact();
        c.Email = 'c.testaccount@teste.com';
        c.FirstName = 'Test';
        c.LastName = 'Contact';
        c.AccountId = acc.Id;
        c.Cargo__c = 'CEO';
        insert c;

        Product2 plan0to100 = new Product2();
        plan0to100.Name = 'Plan 01 (0 - 100)';
        plan0to100.Family = 'Enterprise Subscription';
        plan0to100.Maximum_Number_of_Employees__c = 100;
        plan0to100.IsActive = true;
        insert plan0to100;
        
        Product2 plan100to500 = new Product2();
        plan100to500.Name = 'Plan 02 (100 - 500)';
        plan100to500.Family = 'Enterprise Subscription';
        plan100to500.Maximum_Number_of_Employees__c = 500;
        plan100to500.IsActive = true;
        insert plan100to500;
        
        insert new PricebookEntry(pricebook2id = Test.getStandardPricebookId(), product2id = plan0to100.id, unitprice=1.0, isActive=true, CurrencyIsoCode='BRL');
        
        Pricebook2 pb = new pricebook2(name='Price List BR', IsActive=true, Country__c = 'Brazil');
        insert pb;
        
        PricebookEntry entry1 = new PricebookEntry(pricebook2id=pb.id, product2id=plan0to100.id, unitprice=1.0, isActive=true, CurrencyIsoCode='BRL');
        insert entry1;
        
        Opportunity opp1 = new Opportunity();
        opp1.Name = 'Test Opp';
        opp1.StageName = 'Qualification';
        opp1.RecordTypeId = oppRecordTypeId;
        opp1.CloseDate = System.now().date();
        opp1.AccountId = acc.Id;

        oppInsertList.add(opp1);
        
        Opportunity opp2 = new Opportunity();
        opp2.Name = 'Test Opp';
        opp2.StageName = 'Qualification';
        opp2.RecordTypeId = oppRecordTypeId;
        opp2.CloseDate = System.now().date();
        opp2.AccountId = acc.Id;

        oppInsertList.add(opp2);
        
        insert oppInsertList;

        opp1.StageName = 'Validated';
        oppUpdateList.add(opp1);

        opp2.StageName = 'Validated';
        oppUpdateList.add(opp2);

        update oppUpdateList;
        
        Quote proposal1 = new Quote();
        proposal1.Name = 'Test Proposal1';
        proposal1.RecordTypeId = quoteRecordTypeId;
        proposal1.OpportunityId = opp1.Id;
        proposal1.License_Fee_Waiver__c = 'No';

        quoteInsertList.add(proposal1);

        Copay_Plan__c membershipFee = new Copay_Plan__c();
        membershipFee.Name = 'Brasil Padrão';
        insert membershipFee;

        Quote proposal2 = new Quote();
        proposal2.Name = 'Test Proposal2';
        proposal2.RecordTypeId = quoteRecordTypeId;
        proposal2.OpportunityId = opp2.Id;
        proposal2.License_Fee_Waiver__c = 'No';
        proposal2.Expansion_100_of_the_eligible__c = true;
        proposal2.Payment_method__c = 'Payroll';
        proposal2.Employee_Corporate_Email__c = true;
        proposal2.Contact_Permission__c = 'Allowlist';
        proposal2.New_Hires__c = true;
        proposal2.HR_Communication__c = true;
        proposal2.Exclusivity_clause__c = true;
        proposal2.PR_in_contract__c = true;
        proposal2.What_does_success_looks_like_Joint_BP__c = true; 
        proposal2.Gym__c = membershipFee.Id;
        proposal2.Pricebook2Id = pb.Id;

        quoteInsertList.add(proposal2);

        insert quoteInsertList;

        QuoteLineItem item = new QuoteLineItem();
        item.QuoteId = proposal2.Id;
        item.UnitPrice = 0;
        item.Product2Id = plan0to100.Id;
        item.PricebookEntryId = entry1.Id;
        item.Quantity = 100;
        insert item; 

        Event e = new Event();
        e.WhatId = opp2.Id;
        e.WhoId = c.Id;
        e.DurationInMinutes = 60;
        e.ActivityDateTime = System.now();
        insert e;

        List<Id> sObjectIdList = new List<Id>();
        sObjectIdList.add(proposal1.Id);
        sObjectIdList.add(proposal2.Id);
        sObjectIdList.add(item.Id);
        sObjectIdList.add(e.Id);
        
        List<Payment__c> paymentsToInsert = new List<Payment__c>();
        
        for (Integer i=0; i<2; i++) {
        	Payment__c payment = new Payment__c();
            payment.Quote_Line_Item__c = item.Id;
            payment.Frequency__c = 'Monthly';
            paymentsToInsert.add(payment);
        }
        
        Test.startTest();
        insert paymentsToInsert;
       
        for (Payment__c payment : paymentsToInsert) {
            sObjectIdList.add(payment.Id);
        }

        List<Eligibility__c> eligibiltitesToInsert = new List<Eligibility__c>();
        
        for (Integer i=0; i<paymentsToInsert.size(); i++) {
            Eligibility__c eligibility = new Eligibility__c();
         	eligibility.Payment__c = paymentsToInsert.get(i).Id;
            if (Math.mod(i, 2) == 0) {
            	eligibility.Payment_Method__c = 'Payroll';    
            } else {
                eligibility.Payment_Method__c = 'Credit Card';    
            }
            	
        	eligibiltitesToInsert.add(eligibility);
        }
        
        insert eligibiltitesToInsert;

        List<Waiver__c> waiversToInsert = new List<Waiver__c>();
        
        for (Integer i=0; i<paymentsToInsert.size(); i++) {
            Waiver__c waiver = new Waiver__c();
             waiver.Payment__c = paymentsToInsert.get(i).Id;
             waiver.Percentage__c = 20;
        	waiversToInsert.add(waiver);
        }
        
        insert waiversToInsert;

        for (Eligibility__c eligibility : eligibiltitesToInsert) {
   			sObjectIdList.add(eligibility.Id);					 		 		 		 				
        }
        
        EnablersHelper.updateStepsTowardsSuccess(sObjectIdList);
        
        delete waiversToInsert;
        Test.stopTest();
    }

    @isTest
    public static void testCreateStepsForAccounts() {
        List<Id> accIdList = new List<Id>();

        Account acc1 = new Account();
        acc1.Name = 'Account 1';
        acc1.BillingCountry = 'Brazil';
        acc1.BillingState = 'São Paulo';
        insert acc1;
        accIdList.add(acc1.Id);

        Account acc2 = new Account();
        acc2.Name = 'Account 2';
        acc2.BillingCountry = 'Brazil';
        acc2.BillingState = 'São Paulo';
        insert acc2;
        accIdList.add(acc2.Id);

        Test.startTest();
        EnablersHelper.updateStepsTowardsSuccess(accIdList);
        Test.stopTest();
    }

    @isTest
    public static void testCreateStepsForRenegociationOpp() {
        Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
        Id oppRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Success_Renegotiation').getRecordTypeId();
        Id quoteRecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();

        List<Id> oppIdList = new List<Id>();
        List<Opportunity> oppsToInsert = new List<Opportunity>();
        List<Quote> quotesToInsert = new List<Quote>();

        Account acc = new Account();
        acc.Name = 'Account Test';
        acc.BillingState = 'São Paulo';
        acc.BillingCountry = 'Brazil';
        acc.RecordTypeId = accRecordTypeId;
        insert acc;

        Opportunity opp = new Opportunity();
        opp.RecordTypeId = oppRecordTypeId;
        opp.AccountId = acc.Id;
        opp.Name = 'Opp Test';
        opp.CloseDate = System.now().date();
        opp.StageName = 'Proposta Enviada';
        opp.Sub_Type__c = 'Renegotiation';
        insert opp;

        Test.startTest();
        EnablersHelper.updateStepsTowardsSuccess(new List<Id>{opp.Id});
        Test.stopTest();
    }

    @isTest
    public static void testUpdateStepsForEventsWithoutExpiration() {
        Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
        Id oppRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Success_Renegotiation').getRecordTypeId();
        Id quoteRecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        Id eventClientCommRecTypeId = Schema.SObjectType.Event.getRecordTypeInfosByDeveloperName().get('Client_Communication').getRecordTypeId();
        Id eventLogMettingRecTypeId = Schema.SObjectType.Event.getRecordTypeInfosByDeveloperName().get('Meeting').getRecordTypeId();

        List<Id> eventIdList = new List<Id>();
        List<Opportunity> oppsToInsert = new List<Opportunity>();
        List<Quote> quotesToInsert = new List<Quote>();

        Account acc = new Account();
        acc.Name = 'Account Test';
        acc.BillingState = 'São Paulo';
        acc.BillingCountry = 'Brazil';
        acc.RecordTypeId = accRecordTypeId;
        insert acc;

        Contact accContact = new Contact();
        accContact.AccountId = acc.Id;
        accContact.FirstName = 'Test';
        accContact.LastName = 'Contact';
        accContact.Email = 'testcontact@test.com';
        accContact.Cargo__c = 'CEO';
        insert accContact;

        Event newHiresEvent = new Event();
        newHiresEvent.RecordTypeId = eventClientCommRecTypeId;
        newHiresEvent.WhatId = acc.Id;
        newHiresEvent.Type = 'New Hires';
        newHiresEvent.StartDateTime = System.now();
        newHiresEvent.DurationInMinutes = 60;
        insert newHiresEvent;

        Event prSocialMediaEvent = new Event();
        prSocialMediaEvent.RecordTypeId = eventClientCommRecTypeId;
        prSocialMediaEvent.WhatId = acc.Id;
        prSocialMediaEvent.Type = 'PR & Social Media';
        prSocialMediaEvent.StartDateTime = System.now();
        prSocialMediaEvent.DurationInMinutes = 60;
        insert prSocialMediaEvent;

        Event clientCommEvent = new Event();
        clientCommEvent.RecordTypeId = eventClientCommRecTypeId;
        clientCommEvent.WhatId = acc.Id;
        clientCommEvent.Type = 'Client Communication';
        clientCommEvent.StartDateTime = System.now();
        clientCommEvent.DurationInMinutes = 60;
        insert clientCommEvent;

        Event logMeetEvent = new Event();
        logMeetEvent.RecordTypeId = eventLogMettingRecTypeId;
        logMeetEvent.WhatId = acc.Id;
        logMeetEvent.WhoId = accContact.Id;
        logMeetEvent.StartDateTime = System.now();
        logMeetEvent.DurationInMinutes = 60;
        insert logMeetEvent;

        eventIdList.add(newHiresEvent.Id);
        eventIdList.add(prSocialMediaEvent.Id);
        eventIdList.add(clientCommEvent.Id);
        eventIdList.add(logMeetEvent.Id);

        Test.startTest();
        EnablersHelper.updateStepsTowardsSuccess(eventIdList);
        Test.stopTest();
    }

    @isTest
    public static void testUpdateStepsForEventsWithExpiration() {
        Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
        Id oppRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Success_Renegotiation').getRecordTypeId();
        Id quoteRecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        Id eventClientCommRecTypeId = Schema.SObjectType.Event.getRecordTypeInfosByDeveloperName().get('Client_Communication').getRecordTypeId();
        Id eventLogMettingRecTypeId = Schema.SObjectType.Event.getRecordTypeInfosByDeveloperName().get('Meeting').getRecordTypeId();

        List<Id> eventIdList = new List<Id>();
        List<Opportunity> oppsToInsert = new List<Opportunity>();
        List<Quote> quotesToInsert = new List<Quote>();

        Account acc = new Account();
        acc.Name = 'Account Test';
        acc.BillingState = 'São Paulo';
        acc.BillingCountry = 'Brazil';
        acc.RecordTypeId = accRecordTypeId;
        insert acc;

        Contact accContact = new Contact();
        accContact.AccountId = acc.Id;
        accContact.FirstName = 'Test';
        accContact.LastName = 'Contact';
        accContact.Email = 'testcontact@test.com';
        accContact.Cargo__c = 'CEO';
        insert accContact;
        
        EnablersRepository enablersRepo = new EnablersRepository();
        List<Step_Towards_Success1__c> steps = enablersRepo.getAccountEnablers(acc);

        for (Step_Towards_Success1__c step : steps) {
            if (step.Step_Number__c == 4 || step.Step_Number__c == 8 || step.Step_Number__c ==  9|| step.Step_Number__c == 11) {
                step.Expiration_Date__c = System.now().date();
            }
        }

        update steps;

        Event newHiresEvent = new Event();
        newHiresEvent.RecordTypeId = eventClientCommRecTypeId;
        newHiresEvent.WhatId = acc.Id;
        newHiresEvent.Type = 'New Hires';
        newHiresEvent.StartDateTime = System.now();
        newHiresEvent.DurationInMinutes = 60;
        insert newHiresEvent;

        Event prSocialMediaEvent = new Event();
        prSocialMediaEvent.RecordTypeId = eventClientCommRecTypeId;
        prSocialMediaEvent.WhatId = acc.Id;
        prSocialMediaEvent.Type = 'PR & Social Media';
        prSocialMediaEvent.StartDateTime = System.now();
        prSocialMediaEvent.DurationInMinutes = 60;
        insert prSocialMediaEvent;

        Event clientCommEvent = new Event();
        clientCommEvent.RecordTypeId = eventClientCommRecTypeId;
        clientCommEvent.WhatId = acc.Id;
        clientCommEvent.Type = 'Client Communication';
        clientCommEvent.StartDateTime = System.now();
        clientCommEvent.DurationInMinutes = 60;
        insert clientCommEvent;

        Event logMeetEvent = new Event();
        logMeetEvent.RecordTypeId = eventLogMettingRecTypeId;
        logMeetEvent.WhatId = acc.Id;
        logMeetEvent.WhoId = accContact.Id;
        logMeetEvent.StartDateTime = System.now();
        logMeetEvent.DurationInMinutes = 60;
        insert logMeetEvent;

        eventIdList.add(newHiresEvent.Id);
        eventIdList.add(prSocialMediaEvent.Id);
        eventIdList.add(clientCommEvent.Id);
        eventIdList.add(logMeetEvent.Id);

        Test.startTest();
        EnablersHelper.updateStepsTowardsSuccess(eventIdList);
        Test.stopTest();
    }

    @isTest
    public static void testPassEnablersToAccountFromOpps() {
        Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
        Id oppRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        Id quoteRecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        Id formM0RecordTypeId = Schema.SObjectType.Form__c.getRecordTypeInfosByDeveloperName().get('M0_12_Steps').getRecordTypeId();
        
        Account acc = new Account();
        acc.RecordTypeId = accRecordTypeId;
        acc.Name = 'Test Account';
        acc.BillingCountry = 'Brazil';
        acc.BillingState = 'São Paulo';
        acc.NumberOfEmployees = 1000;
        insert acc;

        Opportunity opp1 = new Opportunity();
        opp1.Name = 'Test Opp';
        opp1.StageName = 'Lançado/Ganho';
        opp1.RecordTypeId = oppRecordTypeId;
        opp1.CloseDate = System.now().date();
        opp1.AccountId = acc.Id;
        insert opp1;

        Quote proposal1 = new Quote();
        proposal1.Name = 'Test Proposal1';
        proposal1.RecordTypeId = quoteRecordTypeId;
        proposal1.OpportunityId = opp1.Id;
        proposal1.License_Fee_Waiver__c = 'No';
        insert proposal1;

        EnablersHelper.updateStepsTowardsSuccess(new List<Id>{opp1.Id});
    }

    @isTest
    public static void testCreateEnablersToSmbNewBusinessOpps() {
        Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
        Id oppRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SMB_New_Business').getRecordTypeId();
        Id quoteRecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        
        Account acc = new Account();
        acc.RecordTypeId = accRecordTypeId;
        acc.Name = 'Test Account';
        acc.BillingCountry = 'Brazil';
        acc.BillingState = 'São Paulo';
        insert acc;

        Opportunity opp1 = new Opportunity();
        opp1.Name = 'Test Opp';
        opp1.StageName = 'Qualificação';
        opp1.RecordTypeId = oppRecordTypeId;
        opp1.CloseDate = System.now().date();
        opp1.AccountId = acc.Id;
        insert opp1;

        EnablersHelper.updateStepsTowardsSuccess(new List<Id>{opp1.Id});

        opp1.StageName = 'Qualificação';
    }

    @isTest
    public static void testPassEnablersToAccountFromSmbOpps() {
        Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
        Id oppRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SMB_New_Business').getRecordTypeId();
        Id quoteRecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        
        Account acc = new Account();
        acc.RecordTypeId = accRecordTypeId;
        acc.Name = 'Test Account';
        acc.BillingCountry = 'Brazil';
        acc.BillingState = 'São Paulo';
        insert acc;

        Opportunity opp1 = new Opportunity();
        opp1.Name = 'Test Opp';
        opp1.StageName = 'Lançado/Ganho';
        opp1.RecordTypeId = oppRecordTypeId;
        opp1.CloseDate = System.now().date();
        opp1.AccountId = acc.Id;
        insert opp1;

        opp1.StageName = 'Lançado/Ganho';
        update opp1;

        Quote proposal1 = new Quote();
        proposal1.Name = 'Test Proposal1';
        proposal1.RecordTypeId = quoteRecordTypeId;
        proposal1.OpportunityId = opp1.Id;
        proposal1.License_Fee_Waiver__c = 'No';
        insert proposal1;

        EnablersHelper.updateStepsTowardsSuccess(new List<Id>{opp1.Id});
    }

    @IsTest
    public static void testStepsTowardsSucessClientsDefinition() {

        List<Step_Towards_Success_Definition__mdt> toward = EnablersRepository.getMapStepsTowardsSucessClientsDefinition();
        String developerName = toward[0].DeveloperName;

        System.assertEquals(
            'Standard_Membership_Fee',
            developerName,
            'The lead was updated'
        );
    }

}