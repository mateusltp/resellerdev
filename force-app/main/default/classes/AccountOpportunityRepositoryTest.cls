/**
* @description       : 
* @author            : Samuel Silva - GFT (slml@gft.com)
* @group             : 
* @last modified on  : 09-29-2020
* @last modified by  : Alysson Mota
* Modifications Log 
* Ver   Date         Author                              Modification
* 1.0   08-31-2020   Samuel Silva - GFT (slml@gft.com)   Initial Version
**/

@isTest(seeAllData=false)
public class AccountOpportunityRepositoryTest {
    
    public static final String OPPORTUNITY_NAME_TEST_1 = 'OPP Test1';
    
    
    @TestSetup
    static void setup(){
        Pricebook2 standardPricebook = new Pricebook2(
            Id = Test.getStandardPricebookId(),
            IsActive = true
        );
        
        update standardPricebook;
        
        Account acc = generateAccount('Pai');
        INSERT acc;
        
        Opportunity opp = generateOpportunity(acc);
        INSERT opp;
        
    }
    
    @isTest
    static void setRelationshipAccountWithOppTest(){
        Opportunity opp = [SELECT ID FROM Opportunity limit 1];
        AccountOpportunityRepository accOppRep = new AccountOpportunityRepository(opp.Id);
        List<Account> accLst = [SELECT ID FROM Account WHERE Name =: 'Test Pai'];
        try {
            accOppRep.setRelationshipAccountWithOpp(accLst);
        } catch(Exception e){
            System.assert(false);
        }
    }
    
    @isTest
    static void testGetAccountsInOpp() {
        Opportunity opp = [SELECT ID FROM Opportunity limit 1];
        AccountOpportunityRepository accOppRep = new AccountOpportunityRepository(opp.Id);
        try {
            accOppRep.getAccountsInOpp();
        } catch(Exception e){
            System.assert(false);
        }
    }
    
    @isTest
    static void setRelationshipAccountWithOppForClientsTest(){
        Opportunity opp = [SELECT ID FROM Opportunity limit 1];
        AccountOpportunityRepository accOppRep = new AccountOpportunityRepository(opp.Id);
        List<Account> accLst = [SELECT ID FROM Account WHERE Name =: 'Test Pai'];
        try {
            accOppRep.setRelationshipAccountWithOppForClients(accLst);
        } catch(Exception e){
            System.assert(false);
        }
    }
    
    @isTest
    static void breakRelationshipAccountWithOppTest(){
        Opportunity opp = [SELECT ID FROM Opportunity limit 1];
        AccountOpportunityRepository accOppRep = new AccountOpportunityRepository(opp.Id);
        List<Account> accLst = [SELECT ID FROM Account WHERE Name =: 'Test Pai'];
        try {
            accOppRep.breakRelationshipAccountWithOpp(accLst);
        } catch(Exception e){
            System.assert(false);
        }
    }
    
    private static Account generateAccount(String name){
        Id rtId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gympass_Entity').getRecordTypeId();
        Account acc = new Account();
        acc.name='Test ' + name;
        acc.RecordTypeId = rtId;
        acc.GP_Status__c = 'Active';
        acc.billingState = 'Minas Gerais';
        acc.BillingCity = 'AccountOpportunityRepositoryTest'+name;
        acc.billingStreet = 'Rua academiabrasilpai';
        acc.billingCountry = 'Brazil';        
        acc.NumberOfEmployees = 520;
        return acc;
    }
    
    private static Opportunity generateOpportunity(Account acc){
        Id oppRtId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        Opportunity accOpp = new Opportunity();
        accOpp.recordTypeId = oppRtId;
        accOpp.AccountId = acc.id;
        accOpp.Name = OPPORTUNITY_NAME_TEST_1; 
        accOpp.CloseDate = Date.today();
        accOpp.StageName = 'Qualificação';
        accOpp.Type = 'Expansion';  
        return accOpp;
    }
    
    @isTest
    static void getResellerAccountsInOppTest(){
        Opportunity opp = [SELECT ID FROM Opportunity limit 1];
        AccountOpportunityRepository accOppRep = new AccountOpportunityRepository(opp.Id);
        try {
            accOppRep.getResellerAccountsInOpp();
            accOppRep.getAccountsInOppWhithReseller();
        } catch(Exception e){
            System.assert(false);
        }
    }
    
    @isTest
    static void getAccountsInOppWhithResellerTest(){
        Opportunity opp = [SELECT ID FROM Opportunity limit 1];
        AccountOpportunityRepository accOppRep = new AccountOpportunityRepository(opp.Id);
        try {
            accOppRep.getAccountsInOppWhithReseller();
        } catch(Exception e){
            System.assert(false);
        }
    }

}