@IsTest
public class ContactMock {
  public static Contact getStandard(Account account) {
    return new Contact(
      AccountId = account.Id,
      Email = 'emanuelyagocosta-87@bemarius.example',
      FirstName = 'Emanuel',
      LastName = 'Yago Costa',
      MailingCity = 'Campinas',
      MailingCountry = 'Brazil',
      MailingPostalCode = '13050-723',
      MailingState = 'São Paulo',
      MailingStreet = 'Rua Adir Jorge, 421',
      Role__c = 'ADMIN',
      Status_do_contato__c = 'Ativo'
    );
  }
}