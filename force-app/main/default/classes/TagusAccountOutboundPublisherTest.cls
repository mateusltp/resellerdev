@IsTest
public class TagusAccountOutboundPublisherTest {
  @TestSetup
  public static void makeData() {
    List<Account> accounts = new List<Account>();

    Account parentAccount = new Account(
      Name = 'Andrea e Rayssa Fotografias Ltda',
      BillingCountry = 'Brazil',
      BillingState = 'São Paulo',
      Id_Company__c = '07.984.765/0001-52'
    );
    insert parentAccount;

    Account account = new Account(
      Name = 'Samuel e Matheus Transportes ME',
      BillingCountry = 'Brazil',
      BillingState = 'São Paulo',
      ParentId = parentAccount.Id,
      Id_Company__c = '31.168.315/0001-76'
    );
    insert account;
  }

  @IsTest
  public static void runCreate() {
    Test.setMock(HttpCalloutMock.class, new HttpResponseMock());

    List<Account> accounts = [SELECT Id FROM Account];

    new TagusAccountOutboundPublisher(accounts).runCreate();

    System.assert(![SELECT Id FROM Queue__c].isEmpty(), 'Event not published');
  }

  @IsTest
  public static void runUpdate() {
    Test.setMock(HttpCalloutMock.class, new HttpResponseMock());

    List<Account> accounts = [SELECT Id FROM Account];

    new TagusAccountOutboundPublisher(accounts).runUpdate();

    System.assert(![SELECT Id FROM Queue__c].isEmpty(), 'Event not published');
  }

  private class HttpResponseMock implements HttpCalloutMock {
    public HTTPResponse respond(HTTPRequest req) {
      HttpResponse res = new HttpResponse();
      res.setHeader('Content-Type', 'application/json');
      res.setStatusCode(200);
      return res;
    }
  }
}