public without sharing class FieldsValidationCtrl {
	@auraEnabled
    public static Response validateFields(String objectId) {
        Response response = new Response();
        response.isVisible = true;
        
        System.debug(objectId);
        
        Id sObjectId = (Id)objectId;
        System.debug(sObjectId);
        List<ValidationError> validationErrors = null;
        
        if (sObjectId.getSobjectType() == Schema.Opportunity.SObjectType) {
            validationErrors = getValidationsForOpp(sObjectId); 
        }
        
        response.validationErrors = validationErrors;
        
        return response;
    }
    
    private static List<ValidationError> getValidationsForOpp(Id oppId) {
        List<ValidationError> validationErrors = new List<ValidationError>();
       	
		Opportunity opp = getOpportunity(oppId);
        validationErrors.addAll(getValidationsForOpp(opp));
        
        Quote proposal = getProposal(oppId);
        if (proposal != null) {
        	validationErrors.addAll(getValidationsForProposal(proposal));   
        } else {
       		ValidationError validation = getValidationErrorInstance(
            	'No synced Proposal',
                'There is no synced Proposal to this Opportunity'
            );
            validationErrors.add(validation);
        }
            
        QuoteLineItem proposalLineItem = null;
        
        if (proposal != null) {
      		proposalLineItem = getQuoteLineItem(proposal.Id);
        }
        
       	validationErrors.addAll(getValidationsForProposalLineItem(proposalLineItem));
        
        if (validationErrors.size() > 0) {
        	opp.Has_validation_errors__c = true;
            update opp;
        } else {
            opp.Has_validation_errors__c = false;
            update opp;
        }
        
        return validationErrors;
    }
    
    public static Opportunity getOpportunity(Id oppId) {
   		Opportunity opp = [
        	SELECT 
            	Id, 
           	 	Name,
            	CloseDate,
            	Gympass_Entity__c,
            	Gympass_Entity__r.UUID__c, 
            	Data_do_Lancamento__c,
				Owner.Name,
				StageName,
            	Has_validation_errors__c,
            	SyncedQuoteId,
				AccountId,
				Account.Name,
				Account.BillingCity,
				Account.BillingCountry,
				Account.BillingCountryCode,
				Account.BillingPostalCode,
				Account.BillingState,
				Account.BillingStreet,
				Account.Business_Unit__c,
				Account.Id_Company__c,
				Account.Legal_Document_Type__c,
				Account.Razao_Social__c,
				Account.RecordType.Name,
				Account.UUID__c,
            	Account.NumberOfEmployees
            FROM Opportunity
            WHERE Id =: oppId
        ];
        
        return opp;
    }
    
    public static Quote getProposal(Id oppId) {
        List<Quote> proposals = [
            SELECT
            	Id,
                License_Fee_cut_off_date__c,
                License_Fee_Frequency__c,
                Payment_Method_License_Fee__c,
                LF_PO_Number_Required__c,
                License_Fee_Waiver__c,
            	Administrator__c,
                Administrator__r.Email,
                Administrator__r.MailingCity,
                Administrator__r.MailingCountry,
                Administrator__r.MailingCountryCode,
                Administrator__r.MailingPostalCode,
                Administrator__r.MailingState,
                Administrator__r.MailingStreet,
                Administrator__r.Name,
                Administrator__r.Phone,
                Administrator__r.Document_Number__c,
                Are_Family_Members_Included2__c,
                Contact.Email,
                Contact.MailingCity,
                Contact.MailingCountry,
                Contact.MailingPostalCode,
                Contact.MailingState,
                Contact.Name,
                Contact_Permission__c,
                Contact.Phone,
                Contact.Document_Number__c,
                CurrencyIsoCode,
                Employee_Company_ID__c,
                Employee_Corporate_Email__c,
                Employee_Name__c,
                Employee_National_ID_if_applicable__c,
                Employee_National_ID__c,
                Employee_Registration_Method__c,
            	Finance__c,
            	Finance__r.Email,
                Finance__r.MailingCity,
                Finance__r.MailingCountry,
                Finance__r.MailingCountryCode,
                Finance__r.MailingPostalCode,
                Finance__r.MailingState,
                Finance__r.MailingStreet,
                Finance__r.Name,
                Finance__r.Phone,
                Finance__r.Document_Number__c,
                Gym__r.Market_Value_Basic_I__c,
                Gym__r.Market_Value_Basic_II__c,
                Gym__r.Market_Value_Black__c,
                Gym__r.Market_Value_Gold__c,
                Gym__r.Market_Value_Gold_Plus__c,
                Gym__r.Market_Value_Platinum__c,
                Gym__r.Market_Value_Silver__c,
                Gym__r.Market_Value_Starter__c,
                MB_PO_Number_Required__c,
                Gym__r.Name,
                Gym__r.Monthly_Fee_Basic_I__c,
                Gym__r.Monthly_Fee_Basic_II__c,
                Gym__r.Monthly_Fee_Black__c,
                Gym__r.Monthly_Fee_Family_Members_Basic_I__c,
                Gym__r.Monthly_Fee_Family_Members_Basic_II__c,
                Gym__r.Monthly_Fee_Family_Members_Black__c,
                Gym__r.Monthly_Fee_Family_Members_Gold__c,
                Gym__r.Monthly_Fee_Family_Members_Gold_Plus__c,
                Gym__r.Monthly_Fee_Family_Members_Platinum__c,
                Gym__r.Monthly_Fee_Family_Members_Silver__c,
                Gym__r.Monthly_Fee_Family_Members_Starter__c,
                Gym__r.Monthly_Fee_Gold__c,
                Gym__r.Monthly_Fee_Gold_Plus__c,
                Gym__r.Monthly_Fee_Platinum__c,
                Gym__r.Monthly_Fee_Silver__c,
                Gym__r.Monthly_Fee_Starter__c,
            	Operations__r.Email,
                Operations__r.MailingCity,
                Operations__r.MailingCountry,
                Operations__r.MailingCountryCode,
                Operations__r.MailingPostalCode,
                Operations__r.MailingState,
                Operations__r.Name,
                Operations__r.Phone,
                Payment_Terms_for_License_Fee__c,
                Payment_method__c,
                Proposal_End_Date__c,
                Proposal_Start_Date__c,
                Setup_Fee__c,
                Payment_Terms_for_Setup_Fee__c,
                Submission_Date__c,
                Total_Setup_Fee_Value__c,
                Unique_Identifier__c
            
            FROM Quote
        	WHERE OpportunityId =: oppId
            AND IsSyncing = true 
        ];
        
        if (proposals.size() > 0) {
        	return proposals.get(0);
        } else {
            return null;
        }
    }

    public static QuoteLineItem getQuoteLineItem(Id proposalId) {
		List<QuoteLineItem> quoteLineItems = [
       		SELECT 
            	Id,
            	TotalPrice,
                Description,
                Product2.Name,
                Quantity,
                unitPrice
            FROM QuoteLineItem
            WHERE QuoteId =: proposalId
        ];
        
        if (quoteLineItems.size() > 0) {
        	return quoteLineItems.get(0);
        } else {
        	return null;   
        } 
    }
    
    public static List<ValidationError> getValidationsForOpp(Opportunity opp) {
        List<ValidationError> validations = new List<ValidationError>();
        
        // Validate Account Address
        if (opp.Account.BillingCity == null	|| opp.Account.BillingCountry == null || opp.Account.BillingCountryCode == null || opp.Account.BillingPostalCode == null || opp.Account.BillingCountry == null || opp.Account.BillingState == null || opp.Account.BillingStreet == null) {
			ValidationError validation = getValidationErrorInstance(
            	'Account address incomplete',
                'Please complete all the Address informations in the account ' + opp.Account.Name
            );
            validations.add(validation);
        }
        
        // Validate Account Business Unit field
        if (opp.Account.NumberOfEmployees <= 1000 && opp.Account.Business_Unit__c != 'SMB') {
        	System.debug('SMB');
            ValidationError validation = getValidationErrorInstance(
            	'Account Business Unit',
                'For accounts with the field \'Total Nº of Employees\' less or equal than 1000 the field \'Business Unit\' should be filled as \'SMB\''
            );
            validations.add(validation);
        }
        
        // Validate Legal Number, Legal Name, Name  Business Unit field
        if ((opp.Account.Id_Company__c == null && opp.Account.BillingCountry == 'Brazil') || opp.Account.Legal_Document_Type__c == null || opp.Account.Razao_Social__c == null || opp.Account.Name == null || opp.Account.UUID__c == null) {
           	System.debug('CNPJ | Razão Social | Name | UUid');
            ValidationError validation = getValidationErrorInstance(
            	'Some empty fields in account',
                'The fields \'Legal Number\', \'Legal Name\', \'Legal Document Type\', \'Account Name\' and \'UUID\' from opportunity\'s account must not be empty'
            );
            validations.add(validation);
        }
        
        // Validate Field Gympass Entity UUid
        if (opp.Gympass_Entity__c == null) {
            System.debug('Gympass Entity');
       		ValidationError validation = getValidationErrorInstance(
            	'Field \'Gympass Entity\' empty',
                'The \'Gympass Entity\' field in this Opportunity must not be empty'
            );
            validations.add(validation);	 	    
        } else {
            if (opp.Gympass_Entity__r.UUID__c == null) {
                System.debug('Gympass Entity UUid');
                ValidationError validation = getValidationErrorInstance(
            		'Field \'UUID\' in Gympass Entity empty',
                	'The field \'UUid\' in the Gympass Entity must not be empty'
            	);
            	validations.add(validation);
            }
        }
        
        return validations;
    }
    
    public static List<ValidationError> getValidationsForProposal(Quote prop) {
    	List<ValidationError> validations = new List<ValidationError>();
        
        // Validates administrator and finance contacts
        if (prop.Administrator__c == null && prop.Finance__c == null) {
       		ValidationError validation = getValidationErrorInstance(
           		'Fields \'Administrator\' and \'Finance\' empty',
            	'At least one of the fields in the synced proposal must not be empty'
          	);
           	validations.add(validation);
        } 
        
        if (prop.Administrator__c != null) {
            if (prop.Administrator__r.Email == null || prop.Administrator__r.MailingCity == null || prop.Administrator__r.MailingCountry == null || prop.Administrator__r.MailingCountryCode == null || prop.Administrator__r.MailingState == null || prop.Administrator__r.MailingStreet == null || prop.Administrator__r.Name == null || prop.Administrator__r.Phone == null || prop.Administrator__r.Document_Number__c == null) {
                ValidationError validation = getValidationErrorInstance(
                   	'Missing fields in Administrator contact',
            		'Please complete the Email, Phone, Document Number and Address in contact assigned to the proposal\'s \'Admininstrator\' field'
          		);
                validations.add(validation);
            }
            /*
            else {
                if (!validatePhoneFormat(prop.Administrator__r.Phone, prop.Administrator__r.MailingCountryCode)) {
                	ValidationError validation = getValidationErrorInstance(
                   		'Administrator contact phone not in the correct format',
            			'The phone must have the country code and region code. It also must have no parentheses and dashes'
          			);
                    validations.add(validation);
                }
            }
			*/
        }
        
        if (prop.Finance__c != null) {
        	if (prop.Finance__r.Email == null || prop.Finance__r.MailingCity == null || prop.Finance__r.MailingCountry == null || prop.Finance__r.MailingCountryCode == null || prop.Finance__r.MailingState == null || prop.Finance__r.MailingStreet == null || prop.Finance__r.Name == null || prop.Finance__r.Phone == null || prop.Finance__r.Document_Number__c == null) {
               	ValidationError validation = getValidationErrorInstance(
                   	'Missing fields in Finance contact',
            		'Please complete the Email, Phone, Document Number and Address in contact assigned to the proposal\'s \'Finance\' field'
          		);
                validations.add(validation);
            } 
            /*
            else {
                if (!validatePhoneFormat(prop.Finance__r.Phone, prop.Finance__r.MailingCountryCode)) {
                	ValidationError validation = getValidationErrorInstance(
                   		'Finance contact phone not in the correct format',
            			'The phone must have the country code and region code. It also must have no parentheses and dashes'
          			);
                    validations.add(validation);
                } 
            }
			*/
        }
        
        // Validates other fields
        if (prop.License_Fee_cut_off_date__c == null || prop.CurrencyIsoCode == null || prop.Payment_Terms_for_License_Fee__c == null || prop.Proposal_Start_Date__c == null || prop.Unique_Identifier__c == null || prop.Are_Family_Members_Included2__c == null) {
          	ValidationError validation = getValidationErrorInstance(
           		'Fields in synced proposal empty',
           		'The fields \'Access Fee Cut-Off Date\', \'Proposal Currency\', \'Payment Terms for Access Fee\', \'Proposal Start Date\', \'Are Family Members Included?\' and \'Unique Identifier\' must not be empty'
         	);
          	validations.add(validation);
        }
             
        // Validates the Membership Fee in proposal
        if (prop.Gym__r.Market_Value_Basic_I__c == null || prop.Gym__r.Market_Value_Basic_II__c	== null || prop.Gym__r.Market_Value_Black__c == null || prop.Gym__r.Market_Value_Gold__c == null || prop.Gym__r.Market_Value_Gold_Plus__c == null || prop.Gym__r.Market_Value_Platinum__c == null || prop.Gym__r.Market_Value_Silver__c == null){	
           	ValidationError validation = getValidationErrorInstance(
           		'Fields in membership fee empty',
           		'The market value fields in the membership fee must not be empty'
         	);
          	validations.add(validation);
        }
        
        if (prop.Gym__r.Monthly_Fee_Basic_I__c == null || prop.Gym__r.Monthly_Fee_Basic_II__c == null || prop.Gym__r.Monthly_Fee_Black__c == null || prop.Gym__r.Monthly_Fee_Gold__c == null || prop.Gym__r.Monthly_Fee_Gold_Plus__c == null || prop.Gym__r.Monthly_Fee_Platinum__c == null || prop.Gym__r.Monthly_Fee_Silver__c == null) {
        	ValidationError validation = getValidationErrorInstance(
           		'Fields in membership fee empty',
           		'The monthly fee value fields in the membership fee must not be empty'
         	);
          	validations.add(validation);  
     	}
        
        return validations;
    }
    
    private static List<ValidationError> getValidationsForProposalLineItem(QuoteLineItem proposalLineItem) {
        List<ValidationError> validations = new List<ValidationError>();
        
        if (proposalLineItem == null) {
            ValidationError validation = getValidationErrorInstance(
           		'No Proposal Line Item',
           		'Please add a Proposal Line Item in the synced Proposal'
         	);
          	validations.add(validation); 
        }
        
        return validations;
    }
    
    /*
    private static Boolean validatePhoneFormat(String phone, String countryCode) {
        Boolean result;
        Pattern p;
        
        // Validates phone for brasil and the rest of the countries 
        if (countryCode == 'BR') {
            p = Pattern.compile('^\\+55\\d{10,11}$');
        } else {
        	p = Pattern.compile('^\\+[1-9]\\d{1,14}$'); 
        }
        
        Matcher m = p.matcher(phone);
        result = m.matches();
    	return result;
    }
	*/    

    public static ValidationError getValidationErrorInstance(String title, String description) {
   		ValidationError validationError = new ValidationError();
        validationError.title = title;
        validationError.description = description;
        
        return validationError;
    }
    
    public class ValidationError {
        @AuraEnabled public String title {get; set;}
        @AuraEnabled public String description {get; set;}
    }
    
    public class Response {
        @AuraEnabled public List<ValidationError> validationErrors {get; set;}
        @AuraEnabled public Boolean isVisible {get; set;}
    } 
}