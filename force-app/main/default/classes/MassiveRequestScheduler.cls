public without sharing class MassiveRequestScheduler {
    
    
    public static void schedulerExecute(List<Massive_Account_Request__c> massOld, List<Massive_Account_Request__c> massNew){
        //List<Massive_Account_Request__c> massOld_list = massOld != null ? (List<Massive_Account_Request__c>)JSON.deserialize(massOld, List<Massive_Account_Request__c>.class) : new List<Massive_Account_Request__c>();
        //List<Massive_Account_Request__c> massNew_list = massNew != null ? (List<Massive_Account_Request__c>)JSON.deserialize(massNew, List<Massive_Account_Request__c>.class) : new List<Massive_Account_Request__c>();
        MassiveRequestScheduler.execute(massOld,massNew);
        system.debug('massOld '+massOld[0]);
        system.debug('massNew '+massNew[0]);
    }

   private static void execute(List<Massive_Account_Request__c> massOld, List<Massive_Account_Request__c> massNew) {
      //for(Integer i; i < massNew.size(); i++){
         Integer i = 0;
         for(Massive_Account_Request__c acc : massNew){
         system.debug('massOld[i].Status__c '+massOld[i].Status__c);
         system.debug('massNew[i].Status__c '+acc.Status__c);
         if(massOld[i].Status__c == 'Loading' && acc.Status__c == 'Looking for Accounts'){
            ID idAcc = Database.executeBatch(new LookingAccountsBatch());
            system.debug('idAcc '+idAcc);
         }
         // else if(massOld[i].Status__c == 'Looking for Accounts' && massNew[i].Status__c == 'Looking for Opportunities'){
         //    ID idOpp = Database.executeBatch(new LookingOpportunitiesBatch());
         //    system.debug('idOpp '+idOpp);
         // }
         // else if(massOld[i].Status__c == 'Looking for Opportunities' && massNew[i].Status__c == 'Looking for Events'){
         //    ID idEve = Database.executeBatch(new LookingEventsBatch());
         //    system.debug('idEve '+idEve);
         // }
         // else if(massOld[i].Status__c == 'Looking for Events' && massNew[i].Status__c == 'Run Engine'){
         //    ID idEng = Database.executeBatch(new RunEngineBatch());
         //    system.debug('idEng '+idEng);
         // }
         i++;
     }
   }

}