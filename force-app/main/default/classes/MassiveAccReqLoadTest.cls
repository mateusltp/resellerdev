@isTest
public class MassiveAccReqLoadTest {

    @isTest
    public static void massTest() {

        Massive_Account_Request__c massReq = new Massive_Account_Request__c(
            name='test_loadMassiveAccountRequest',
            Status__c='Draft'
            
        );
        insert massReq;
        
        Contact contact = new Contact();
        contact.FirstName = 'teste contact';
        contact.LastName = 'teste contact';
        contact.Email = 'testecontact@bemarius.example';
        contact.Role__c = 'FINANCE';
        insert contact;
        
        account account = new Account();  
        account.Name = 'teste account';
        account.Legal_Document_Type__c = 'CNPJ';
        account.Id_Company__c = '73054510000115';
        account.Razao_Social__c = 'TESTE222';
        account.Website = 'testemassive.com';        
        account.BillingCountry = 'Brazil';
        account.Industry = 'Government or Public Management';
        account.ABM_Prospect__c = true;
        account.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Reseller').getRecordTypeId();
        insert account;
        
        Account_Request__c request = new Account_Request__c();  
        request.Name = 'teste7';
        request.Unique_Identifier_Type__c = 'CNPJ';
        request.Unique_Identifier__c = '73054510000115';
        request.Website__c = 'testerequeste7.com';
        request.Billing_Country__c = 'Brazil';
        request.Massive_Account_Request__c = massReq.Id;
        request.Engine_Status__c = 'Invalid';
        request.ContactId__c = contact.Id;
        request.AccountId__c = account.Id;
        insert request;
        
        
        
        ContentVersion content = new ContentVersion(); 
        content.Title='Test_csv'; 
        content.PathOnClient='/' + content.Title + '.csv'; 
        Blob bodyBlob=Blob.valueOf('Company name;Website;Business model;Employees on this offer;Pais\nTesteMassive1;testemassive1.com.br;Subsidy;10;Spain\nTesteMassive2;testemassive2.com.br;Intermediation;11;Spain\nTesteMassive;testemassive3.com.br;Subsidy;12;Spain'); 
        content.VersionData=bodyBlob; 
        content.origin = 'H';
        insert content;
        
        ContentDocumentLink contentlink=new ContentDocumentLink();
        contentlink.LinkedEntityId=massReq.id;
        contentlink.contentdocumentid=[select contentdocumentid from contentversion where id =: content.id].contentdocumentid;
        contentlink.ShareType = 'I';
        contentlink.Visibility = 'AllUsers'; 
        insert contentlink;
        
        Integer quantity;
        Test.startTest();
        MassiveAccReqLoad.loadMassiveAccountRequest(massReq.Id);
        MassiveAccReqLoad.runBatchesAgain(massReq.Id);
        quantity = MassiveAccReqLoad.quantityMassiveAccountRequest(massReq.Id);
        Test.stopTest(); 
        
        List<Massive_Account_Request__c> massive =[SELECT Id, Companies__c FROM Massive_Account_Request__c LIMIT 1];
        
        System.assertEquals(4, quantity);
        System.assertEquals('4', massive[0].Companies__c);

        
    }
    
}