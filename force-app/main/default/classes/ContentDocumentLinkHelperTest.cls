/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 04-27-2022
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/

@IsTest
public with sharing class ContentDocumentLinkHelperTest {
    
    private static final PS_Constants constants = PS_Constants.getInstance(); 
    private static final String className = PartnerSetupController.class.getName();

    @TestSetup
    static void setupData(){
        
        String recordTypePartnerFlow = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(constants.ACCOUNT_RT_PARTNER_FLOW_ACCOUNT).getRecordTypeId();
        Account partnerAcc = PartnerDataFactory.newAccount();
        partnerAcc.RecordTypeId = recordTypePartnerFlow;
        partnerAcc.CRM_ID__c = '1231';
        partnerAcc.Legal_Document_Type__c = 'CNPJ';
        partnerAcc.Legal_Title__c = 'PartnerDocumentLinkHelper';
        partnerAcc.BillingCountryCode = 'US';
        partnerAcc.BillingCountry = 'United States';
        partnerAcc.BillingState = 'Arizona';
        partnerAcc.BillingPostalCode = '1223';
        partnerAcc.ShippingCountryCode = 'US';
        partnerAcc.ShippingCountry = 'United States';
        partnerAcc.ShippingState = 'Arizona';
        partnerAcc.CurrencyIsoCode = 'USD';
        partnerAcc.Description = 'dasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasda';
        INSERT partnerAcc;
        
        List<ContentVersion> lstContentVersion = new List<ContentVersion>();
        ContentVersion contentVersion_1 = new ContentVersion(
            Title = 'photo 1',
            PathOnClient = '/logoPenguins1.jpg',
            VersionData = Blob.valueOf('Unit Test PartnerSetupController Body 1'),
            origin = 'H',
            Type_Files_fileupload__c = 'Photo'    
        );
        lstContentVersion.add(contentVersion_1);

        ContentVersion contentVersion_2 = new ContentVersion(
            Title = 'photo 2',
            PathOnClient = '/logoPenguins2.jpg',
            VersionData = Blob.valueOf('Unit Test PartnerSetupController Body 2'),
            origin = 'H',
            Type_Files_fileupload__c = 'Photo'    
        );
        lstContentVersion.add(contentVersion_2);

        ContentVersion contentVersion_3 = new ContentVersion(
            Title = 'photo 3',
            PathOnClient = '/logoPenguins3.jpg',
            VersionData = Blob.valueOf('Unit Test PartnerSetupController Body 3'),
            origin = 'H',
            Type_Files_fileupload__c = 'Photo'    
        );
        lstContentVersion.add(contentVersion_3);

        ContentVersion contentVersion_4 = new ContentVersion(
            Title = 'LOGO',
            PathOnClient = '/gymlogo.jpg',
            VersionData = Blob.valueOf('Unit Test PartnerSetupController Logo'),
            origin = 'H',
            Type_Files_fileupload__c = 'Logo'    
        );

        lstContentVersion.add(contentVersion_4);

        ContentVersion contentVersion_5 = new ContentVersion(
            Title = 'W9',
            PathOnClient = '/W9.pdf',
            VersionData = Blob.valueOf('Unit Test PartnerSetupController w9'),
            origin = 'H',
            Type_Files_fileupload__c = 'W9'    
        );

        lstContentVersion.add(contentVersion_5);
        INSERT lstContentVersion;


        Id ContentDocumentId_1 = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: contentVersion_1.Id].ContentDocumentId;
        Id ContentDocumentId_2 = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: contentVersion_2.Id].ContentDocumentId;
        Id ContentDocumentId_3 = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: contentVersion_3.Id].ContentDocumentId;
        Id ContentDocumentId_4 = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: contentVersion_4.Id].ContentDocumentId;
        Id ContentDocumentId_5 = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: contentVersion_5.Id].ContentDocumentId;

        List<ContentDocumentLink> lstContentLink = new List<ContentDocumentLink>();
        ContentDocumentLink ContentDLAcc1 = new ContentDocumentLink();
        ContentDLAcc1.ContentDocumentId = ContentDocumentId_1;
        ContentDLAcc1.LinkedEntityId = partnerAcc.Id;
        lstContentLink.add(ContentDLAcc1);

        ContentDocumentLink ContentDLAcc2 = new ContentDocumentLink();
        ContentDLAcc2.ContentDocumentId = ContentDocumentId_2;
        ContentDLAcc2.LinkedEntityId = partnerAcc.Id;
        lstContentLink.add(ContentDLAcc2);

        ContentDocumentLink ContentDLAcc3 = new ContentDocumentLink();
        ContentDLAcc3.ContentDocumentId = ContentDocumentId_3;
        ContentDLAcc3.LinkedEntityId = partnerAcc.Id;
        lstContentLink.add(ContentDLAcc3);
       
        ContentDocumentLink ContentDLAcc4 = new ContentDocumentLink();
        ContentDLAcc4.ContentDocumentId = ContentDocumentId_4;
        ContentDLAcc4.LinkedEntityId = partnerAcc.Id;
        lstContentLink.add(ContentDLAcc4);

        ContentDocumentLink ContentDLAcc5 = new ContentDocumentLink();
        ContentDLAcc5.ContentDocumentId = ContentDocumentId_5;
        ContentDLAcc5.LinkedEntityId = partnerAcc.Id;
        lstContentLink.add(ContentDLAcc5);

        INSERT lstContentLink;
    }


    @IsTest
    private static void deleteRelatedRecords_Success(){
   
        Test.startTest(); 
            Set<String> contentVersionStrings = new Set<String>();
            Set<Id> contentDocumentIdLst = new Set<Id>();
            List<ContentVersion> contentVersionOldLst = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Type_Files_fileupload__c IN ('Logo', 'Photo', 'W9')];
            List<ContentDocumentLink> contentLinksLst = new List<ContentDocumentLink>();
            Account aPartner = [ SELECT ID, Name,Gym_Email__c  FROM Account WHERE Legal_Title__c LIKE 'PartnerDocumentLinkHelper' LIMIT 1];
            for(ContentVersion ct : contentVersionOldLst) {
                contentVersionStrings.add(String.valueOf(ct.Id));
                contentDocumentIdLst.add(ct.ContentDocumentId);
            }
            contentLinksLst = [SELECT ID FROM ContentDocumentLink WHERE LinkedEntityId =: aPartner.Id ];
            List<FileObject__c> fileObj = [SELECT Id, conversionId__c, Account__c FROM FileObject__c WHERE conversionId__c IN: contentVersionStrings];  
            System.debug('::Test FileObj '  + fileObj);
            System.assert(fileObj.size() > 0);
            delete contentLinksLst;
            List<FileObject__c> fileObjNew = [SELECT Id, conversionId__c, Account__c FROM FileObject__c WHERE conversionId__c IN: contentVersionStrings AND IsDeleted = false];  
            System.assert(fileObjNew.size() == 0);
           
        Test.stopTest();


    }

    
    // @IsTest
    // private static void deleteRelatedRecords_Success(){
   
    //     Test.startTest(); 
    //         try{
    //             String partnerInput = 'INVALID STRING JSON';
    //             PartnerSetupController.saveAccountRecords(partnerInput); 
    //         }catch(Exception e){
    //             System.assert(e instanceof PartnerSetupController.PartnerSetupControllerException);
    //         }          
    //         //String recordString = JSON.serialize(objectToSerialize)(PartnerSetupController.getAccountsFromContract(opp.Id));    
    //     Test.stopTest();
    // }

}