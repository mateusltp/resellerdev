/**
 * @description       : 
 * Test Classes       : ProductItemServiceTest
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 05-17-2022
 * @last modified by  : alysson.mota@gympass.com
**/
public with sharing class ProductItemService {

    private static final PS_Constants constants = PS_Constants.getInstance();
    private static final String className = ProductItemService.class.getName();


    public static Product_Item__c createNewProduct (Id opportunityId, Id accountId, Product_Item__c prodItem, Set<Id> gymActivities, Map<String, List<Commercial_Condition__c>> commercialConditions, List<Threshold__c> thresholds ){

        try{
          String recordTypeDevName = Schema.SObjectType.Product_Item__c.getRecordTypeInfosById().get(prodItem.recordTypeId).getDeveloperName();       
          return service(recordTypeDevName).createNewProduct(opportunityId, accountId, prodItem, gymActivities,  commercialConditions, thresholds);
          
        } catch (Exception e){
          createLog(e, 'createNewProduct', JSON.serialize(prodItem), null);
          throw new ProductItemServiceException(e.getMessage() + ' - ' + e.getStackTraceString());
        }
      }

    public static Map<String, List<Commercial_Condition__c>> deleteProductRelationships (Product_Item__c prodItem, List<Commercial_Condition__c> commercialConditions) {
        try{
            String recordTypeDevName = Schema.SObjectType.Product_Item__c.getRecordTypeInfosById().get(prodItem.recordTypeId).getDeveloperName();
            Map<String, List<Commercial_Condition__c>> commercialConditionsByDML = service(recordTypeDevName).deleteProductRelationships(prodItem.Id, commercialConditions);
            return commercialConditionsByDML;
        } catch (Exception e){
          createLog(e, 'deleteProductRelationships', JSON.serialize(prodItem), null);
            throw new ProductItemServiceException(e.getMessage());
        }
    }

    public void deleteProductsAndRelationships (List<Product_Item__c> prodItemLst) {
        try{
            Id newFlowRecTypeId = Schema.SObjectType.Product_Item__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product').getRecordTypeId();
            List<Product_Item__c> newFlowProdLst = new List<Product_Item__c>();
            
            for (Product_Item__c prodItem : prodItemLst) {
                if (prodItem.RecordTypeId == newFlowRecTypeId) {
                    newFlowProdLst.add(prodItem);
                }
            }

            if (newFlowProdLst.size() > 0) {
                String recordTypeDevName = Schema.SObjectType.Product_Item__c.getRecordTypeInfosById().get(newFlowProdLst.get(0).recordTypeId).getDeveloperName();
                service(recordTypeDevName).deleteProductsAndRelationships(newFlowProdLst);
            }

        } catch (Exception e){
            system.debug('#### '+e.getMessage() + ' - ' + e.getLineNumber()+ ' - ' + e.getStackTraceString());
            createLog(e, 'deleteProductsAndRelationships', JSON.serialize(prodItemLst), null);
            throw new ProductItemServiceException(e.getMessage());
        }
    }

    private static IProductItemService service(String recordTypeDevName) {	
        try{
          return (IProductItemService) Application.ServiceByRecordType.newInstanceByRecordType(getRecordTypeAndSObjectName(recordTypeDevName));
        }catch (Exception e){
           createLog(e, 'service', null, null);
          throw new ProductItemServiceException(e.getMessage());
        }   
    }

    private static String getRecordTypeAndSObjectName (String recordTypeDevName){
      System.debug(' constants.PRODUCT_ITEM_OBJECT+recordTypeDevName' );
        System.debug( constants.PRODUCT_ITEM_OBJECT+'.'+recordTypeDevName );
        return constants.PRODUCT_ITEM_OBJECT+'.'+recordTypeDevName;
    }
    
    @TestVisible
    private static void createLog(Exception e, String methodName, String dataScope, Id objectId) {
        DebugLog__c log = new DebugLog__c(
            Origin__c = '['+className+']['+methodName+']',
            LogType__c = constants.LOGTYPE_ERROR,
            RelatedToSquad__c = constants.LOGTYPE_SQUAD_PARTNERS,
            ExceptionMessage__c = e.getMessage(),
            StackTrace__c = e.getStackTraceString(),
            DataScope__c = dataScope,
            ObjectId__c = objectId
        );
        Logger.createLog(log);
    }

    public class ProductItemServiceException extends Exception {} 	
}