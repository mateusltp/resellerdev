/*
* @Author: Bruno Pinho
* @Date: January/2019
* @Description: Invocable to relate Events to Gympass Events
*/
public class RelateEventToGympassEvent
{
    @InvocableMethod
    public static void EventToGympassEvent(List<Id> gympassEventsId)
    {
        Set<String> setEventIds = new Set<String>();
        List<Event> listEventsToUpdate = new List<Event>();
        
        List<Gympass_Event__c> listGympassEvents = [SELECT Id, Event_Id__c
                                                    FROM Gympass_Event__c
                                                    WHERE Id IN: gympassEventsId];
        
        for (Gympass_Event__c gympassEvent: listGympassEvents)
            setEventIds.add(gympassEvent.Event_Id__c);
        
        List<Event> listEvents = [SELECT Id, Gympass_Event__c
                                  FROM Event
                                  WHERE Id IN: setEventIds];
        
        for (Event event: listEvents)
        {
            for (Gympass_Event__c gympassEvent: listGympassEvents)
            {
                if (event.Id == gympassEvent.Event_Id__c)
                {
                    event.Gympass_Event__c = gympassEvent.Id;
                    listEventsToUpdate.add(event);
                }
            }
        }
        
        if (listEventsToUpdate.size() > 0)
            UPDATE listEventsToUpdate;
   }
}