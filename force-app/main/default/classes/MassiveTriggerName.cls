public without sharing class MassiveTriggerName {
    
    public void massiveAddName() {
        String query = System.label.Community_Query; //'Query';


        List<Massive_Account_Request__c> listMassive = new List<Massive_Account_Request__c>();
        Map<Id, Massive_Account_Request__c> massiveMap = new Map<Id, Massive_Account_Request__c>([
            SELECT Id, Reseller__r.Name 
            FROM Massive_Account_Request__c
            WHERE Id IN : Trigger.new
        ]);

        for(Massive_Account_Request__c massive: (List<Massive_Account_Request__c>)Trigger.New)
        {
            String resellerName = massiveMap.get(massive.Id).Reseller__r.Name;
            Massive_Account_Request__c objMassive = new Massive_Account_Request__c(Id = massive.Id);
            objMassive.Name = query + '/' + resellerName + '-' + massive.Massive_Account_Request_Controller__c;
            //objMassive.Name = Date.today().month() + '/' + Date.today().year() + '/' + resellerName + '/' + massive.Massive_Account_Request_Controller__c;
            listMassive.add(objMassive);
        }
        UPDATE listMassive;
    }
}