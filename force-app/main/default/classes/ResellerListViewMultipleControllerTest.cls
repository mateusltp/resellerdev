@isTest
public class ResellerListViewMultipleControllerTest {

    @isTest
    public static void massTest() {

    Massive_Account_Request__c massReq = new Massive_Account_Request__c(
            name='teste massivo',
            Status__c='Draft'
            
        );
        insert massReq;
        
        Contact contact = new Contact();
        contact.FirstName = 'teste contact';
        contact.LastName = 'teste contact';
        contact.Email = 'testecontact@bemarius.example';
        contact.Role__c = 'FINANCE';
        insert contact;
        
        account account = new Account();  
        account.Name = 'teste account';
        account.Legal_Document_Type__c = 'CNPJ';
        account.Id_Company__c = '73054510000115';
        account.Razao_Social__c = 'TESTE222';
        account.Website = 'testemassive.com';        
        account.BillingCountry = 'Brazil';
        account.Industry = 'Government or Public Management';
        account.ABM_Prospect__c = true;
        account.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Reseller').getRecordTypeId();
        insert account;
        
        Account_Request__c request = new Account_Request__c();  
        request.Name = 'teste7';
        request.Unique_Identifier_Type__c = 'CNPJ';
        request.Unique_Identifier__c = '73054510000115';
        request.Website__c = 'testerequeste7.com';
        request.Billing_Country__c = 'Brazil';
        request.Massive_Account_Request__c = massReq.Id;
        request.Engine_Status__c = 'Invalid';
        request.ContactId__c = contact.Id;
        request.AccountId__c = account.Id;
        request.Partner_Model__c = 'Intermediation';
        insert request;
        
    	List<Account_Request__c> accountRequest = new List<Account_Request__c>();
        Test.startTest();
        ResellerListViewMultipleController.getSearchValue(massReq.Id, request.Name, request.Engine_Status__c);
        ResellerListViewMultipleController.getSearchValue(massReq.Id, '', request.Engine_Status__c);

    	accountRequest = ResellerListViewMultipleController.getAccountRequest(massReq.Id, request.Engine_Status__c);
        ResellerListViewMultipleController.updateAccountRequests(new List<Account_Request__c>{request});
        Test.stopTest(); 
        
        System.assertEquals(false, accountRequest.isEmpty());
    
    }
}