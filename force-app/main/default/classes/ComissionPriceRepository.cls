public without sharing virtual class ComissionPriceRepository {
    public ComissionPriceRepository() {
        
    }
    
    public virtual List<Comission_Price__c> byProductIdAndCurrencyIsoCode(Id productId, String currencyIsoCode){
        return [select id, comission_percent__c, price__c from Comission_Price__c where Product__c = :productId and CurrencyIsoCode__c = :currencyIsoCode order by Comission_Percent__c];
    }
    
}