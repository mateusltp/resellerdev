@IsTest
public class ValidateOpportunityAccountTest {
  @IsTest
  public static void execute() {
    Account account = AccountMock.getStandard('Empresas');
    account.Email__c = null;
    account.Id_Company__c = null;
    account.Legal_Document_Type__c = null;
    account.Attention__c = null;
    account.BillingStreet = null;
    account.BillingCity = null;
    account.Send_To_Tagus__c = true;
    insert account;

    Contact contact = ContactMock.getStandard(account);
    contact.MailingStreet = null;
    contact.MailingCity = null;
    contact.Role__c = null;
    insert contact;

    Pricebook2 pricebook = new Pricebook2(
      CurrencyIsoCode = 'BRL',
      Id = Test.getStandardPricebookId(),
      IsActive = true
    );

    List<Product2> products = new List<Product2>();

    Product2 setupFee = ProductMock.getSetupFee();
    products.add(setupFee);

    Product2 accessFee = ProductMock.getAccessFee();
    products.add(accessFee);

    insert products;

    List<PricebookEntry> pricebookEntries = new List<PricebookEntry>();

    PricebookEntry setupFeePricebookEntry = PricebookMock.getPricebookEntry(
      pricebook,
      setupFee
    );
    pricebookEntries.add(setupFeePricebookEntry);

    PricebookEntry accessFeePricebookEntry = PricebookMock.getPricebookEntry(
      pricebook,
      accessFee
    );
    pricebookEntries.add(accessFeePricebookEntry);

    insert pricebookEntries;

    Opportunity opportunity = OpportunityMock.getNewBusiness(
      account,
      pricebook
    );
    opportunity.Gympass_Entity__c = account.Id;
    insert opportunity;

    Quote quote = QuoteMock.getStandard(opportunity);
    insert quote;

    List<QuoteLineItem> quoteLineItems = new List<QuoteLineItem>();

    QuoteLineItem setupFeeLine = QuoteLineItemMock.getSetupFee(
      quote,
      setupFeePricebookEntry
    );
    quoteLineItems.add(setupFeeLine);

    QuoteLineItem accessFeeLine = QuoteLineItemMock.getEnterpriseSubscription(
      quote,
      accessFeePricebookEntry
    );
    quoteLineItems.add(accessFeeLine);

    insert quoteLineItems;

    List<Payment__c> payments = new List<Payment__c>();

    Payment__c setupFeePayment = PaymentMock.getStandard(setupFeeLine);
    payments.add(setupFeePayment);

    Payment__c accessFeePayment = PaymentMock.getStandard(accessFeeLine);
    payments.add(accessFeePayment);

    insert payments;

    List<Eligibility__c> eligibles = new List<Eligibility__c>();

    Eligibility__c setupFeeEligibility = EligibilityMock.getStandard(
      setupFeePayment
    );
    eligibles.add(setupFeeEligibility);

    Eligibility__c accessFeeEligibility = EligibilityMock.getStandard(
      accessFeePayment
    );
    eligibles.add(accessFeeEligibility);

    insert eligibles;

    Test.startTest();

    opportunity.StageName = 'Lançado/Ganho';
    Database.SaveResult saveResult = Database.update(opportunity, false);

    System.assert(!saveResult.isSuccess(), 'Should not allow update');

    System.assertEquals(
      'The account email is required to close an opportunity',
      saveResult.getErrors()[0].getMessage(),
      'Wrong error message'
    );

    account.Email__c = 'gymemail@apex.com';
    update account;

    saveResult = Database.update(opportunity, false);

    System.assert(!saveResult.isSuccess(), 'Should not allow update');

    System.assertEquals(
      'The account legal number is required to close an opportunity',
      saveResult.getErrors()[0].getMessage(),
      'Wrong error message'
    );

    account.Id_Company__c = '39.450.055/0001-39';
    update account;

    saveResult = Database.update(opportunity, false);

    System.assert(!saveResult.isSuccess(), 'Should not allow update');

    System.assertEquals(
      'The account legal document type is required to close an opportunity',
      saveResult.getErrors()[0].getMessage(),
      'Wrong error message'
    );

    account.Legal_Document_Type__c = 'CNPJ';
    update account;

    saveResult = Database.update(opportunity, false);

    System.assert(!saveResult.isSuccess(), 'Should not allow update');

    System.assertEquals(
      'The account attention is required to close an opportunity',
      saveResult.getErrors()[0].getMessage(),
      'Wrong error message'
    );

    account.Attention__c = contact.Id;
    update account;

    saveResult = Database.update(opportunity, false);

    System.assert(!saveResult.isSuccess(), 'Should not allow update');

    System.assertEquals(
      'The account street is required to close an opportunity',
      saveResult.getErrors()[0].getMessage(),
      'Wrong error message'
    );

    account.BillingStreet = 'Praça Conselho do Povo';
    update account;

    saveResult = Database.update(opportunity, false);

    System.assert(!saveResult.isSuccess(), 'Should not allow update');

    System.assertEquals(
      'The account city is required to close an opportunity',
      saveResult.getErrors()[0].getMessage(),
      'Wrong error message'
    );

    account.BillingCity = 'Itu';
    update account;

    saveResult = Database.update(opportunity, false);

    System.assert(!saveResult.isSuccess(), 'Should not allow update');

    String contactName = contact.FirstName + ' ' + contact.LastName;

    System.assertEquals(
      'The contact ' +
      contactName +
      ' street is required to close an opportunity',
      saveResult.getErrors()[0].getMessage(),
      'Wrong error message'
    );

    contact.MailingStreet = 'Rua Adir Jorge, 421';
    update contact;

    saveResult = Database.update(opportunity, false);

    System.assert(!saveResult.isSuccess(), 'Should not allow update');

    System.assertEquals(
      'The contact ' +
      contactName +
      ' city is required to close an opportunity',
      saveResult.getErrors()[0].getMessage(),
      'Wrong error message'
    );

    contact.MailingCity = 'Campinas';
    update contact;

    saveResult = Database.update(opportunity, false);

    System.assert(!saveResult.isSuccess(), 'Should not allow update');

    System.assertEquals(
      'The contact ' +
      contactName +
      ' role is required to close an opportunity',
      saveResult.getErrors()[0].getMessage(),
      'Wrong error message'
    );

    contact.Role__c = 'ADMIN';
    update contact;

    saveResult = Database.update(opportunity, false);

    String errorMessage = 'Should allow update';

    if (!saveResult.getErrors().isEmpty()) {
      errorMessage = saveResult.getErrors()[0].getMessage();
    }

    System.assert(saveResult.isSuccess(), errorMessage);

    Test.stopTest();
  }
}