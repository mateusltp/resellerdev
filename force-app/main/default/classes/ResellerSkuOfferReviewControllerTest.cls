@isTest(seeAllData=false)
public with sharing class ResellerSkuOfferReviewControllerTest {

    @TestSetup
    private static void createData(){  
        Profile lStandardProfile = [ SELECT Id FROM Profile WHERE Name = 'Standard User' ];

        List< User > lLstUserToCreate = new List< User >{
            new User(
                Alias = 'standt1', Email = 'standarduser1@testorg.com', 
                EmailEncodingKey='UTF-8', LastName = 'Testing1', LanguageLocaleKey = 'en_US', 
                LocaleSidKey = 'en_US', ProfileId = lStandardProfile.Id, Bypass_automations__c = true,
                TimeZoneSidKey = 'America/Los_Angeles', UserName = 'standarduser1@test.gympass.com' ),
            new User(
                Alias = 'standt2', Email = 'standarduser2@testorg.com', 
                EmailEncodingKey='UTF-8', LastName = 'Testing2', LanguageLocaleKey = 'en_US', 
                LocaleSidKey = 'en_US', ProfileId = lStandardProfile.Id, Bypass_automations__c = true,
                TimeZoneSidKey = 'America/Los_Angeles', UserName = 'standarduser2@test.gympass.com' ),
            new User(
                Alias = 'standt3', Email = 'standarduser3@testorg.com', 
                EmailEncodingKey='UTF-8', LastName = 'Testing3', LanguageLocaleKey = 'en_US', 
                LocaleSidKey = 'en_US', ProfileId = lStandardProfile.Id, Bypass_automations__c = true,
                TimeZoneSidKey = 'America/Los_Angeles', UserName = 'standarduser3@test.gympass.com' )
        };

        Database.insert( lLstUserToCreate );

        lLstUserToCreate[0].ManagerId = lLstUserToCreate[1].Id;
        lLstUserToCreate[1].ManagerId = lLstUserToCreate[2].Id;

        Database.update( lLstUserToCreate );

        PermissionSet lClientsPermSet = [ SELECT Id FROM PermissionSet WHERE Name = 'Clients_PermissionSet' ];

        List< PermissionSetAssignment > lLstPermissionSetAssign = new List< PermissionSetAssignment >();

        for( User iUser : lLstUserToCreate ){
            lLstPermissionSetAssign.add(
                new PermissionSetAssignment(
                    AssigneeId = iUser.id , PermissionSetId = lClientsPermSet.Id
                ));
        }

        Database.insert( lLstPermissionSetAssign );

        Profile lAdminProfile = [ SELECT Id FROM Profile WHERE Name = 'System Administrator' ];

        User lUserAdmin = new User(
            Alias = 'admin', Email = 'admin@testorg.com', 
            EmailEncodingKey='UTF-8', LastName = 'adminTesting', LanguageLocaleKey = 'en_US', 
            LocaleSidKey = 'en_US', ProfileId = lAdminProfile.Id, Bypass_automations__c = true,
            TimeZoneSidKey = 'America/Los_Angeles', UserName = 'admin@test.gympass.com' );

        Database.insert( lUserAdmin );

        System.runAs( lUserAdmin ){
            Pricebook2 lStandardPricebook = DataFactory.newPricebook();
            Database.update( lStandardPricebook );

            Account lAcc = DataFactory.newAccount();
            lAcc.OwnerId = lLstUserToCreate[0].Id;
            lAcc.Id_Company__c = '16599799000114';
            lAcc.Legal_Document_Type__c = 'CNPJ';
            lAcc.NumberOfEmployees = 100;
            lAcc.Website = 'testesku1.com';
            Database.insert( lAcc );
            
            Product2 lEsProduct = DataFactory.newProduct( 'Enterprise Subscription' , false , 'BRL' );
            lEsProduct.Payment_Type__c = 'Recurring fee';
            Database.insert( lEsProduct );

            Opportunity lSKUNewOpp = DataFactory.newOpportunity( lAcc.Id , lStandardPricebook , 'Indirect_Channel_New_Business' );
            lSKUNewOpp.Name = 'OppTestSKU';
            lSKUNewOpp.Billing_Period__c = 'Monthly';
            lSKUNewOpp.TotalOpportunityQuantity = 1000;
            lSKUNewOpp.Quantity_Offer_Number_of_Employees__c = 1000;
            lSKUNewOpp.Purchase_Order_number_is_needed__c = 'Yes';
            lSKUNewOpp.Membership_Fee_PO_Duration__c = 'Annual';
            lSKUNewOpp.OwnerId = lLstUserToCreate[0].Id;
            
            Opportunity lSKUNewOpp2 = DataFactory.newOpportunity( lAcc.Id , lStandardPricebook , 'Indirect_Channel_New_Business' );
            lSKUNewOpp2.Name = 'OppTestSKU2';
            lSKUNewOpp2.Billing_Period__c = 'Monthly';
            lSKUNewOpp2.TotalOpportunityQuantity = 1000;
            lSKUNewOpp2.Quantity_Offer_Number_of_Employees__c = 1000;
            lSKUNewOpp2.Purchase_Order_number_is_needed__c = 'Yes';
            lSKUNewOpp2.Membership_Fee_PO_Duration__c = 'Annual';
            lSKUNewOpp2.OwnerId = lLstUserToCreate[0].Id;
            
            PricebookEntry lEnterpriseSubscriptionEntry = DataFactory.newPricebookEntry( lStandardPricebook , lEsProduct , lSKUNewOpp );
            Database.insert( lEnterpriseSubscriptionEntry );

            SKU_Price__c lSkuPrice = DataFactory.newSKUPrice( lEsProduct.Id , lSKUNewOpp , lAcc , 1 , 50 );
            lSkuPrice.OwnerId = lLstUserToCreate[0].Id;
            Database.insert( lSkuPrice );

            Database.insert( lSKUNewOpp );
            Database.insert( lSKUNewOpp2 );

            Quote lQuote = DataFactory.newQuote( lSKUNewOpp , 'QuoteTest' , 'Client_Sales_SKU_New_Business' );
            lQuote.Deal_Desk_Approved_Conditions__c = '[]';
            lQuote.OwnerId = lLstUserToCreate[0].Id;
            Database.insert( lQuote );
            
            Quote lQuote2 = DataFactory.newQuote( lSKUNewOpp2 , 'QuoteTest' , 'Client_Sales_SKU_New_Business' );
            lQuote2.Deal_Desk_Approved_Conditions__c = '[]';
            lQuote2.OwnerId = lLstUserToCreate[0].Id;
            Database.insert( lQuote2 );
            
            lSKUNewOpp.SyncedQuoteId = lQuote.Id;
            Database.update( lSKUNewOpp );

            QuoteLineItem lEnterpriseSubscriptionItem = DataFactory.newQuoteLineItem( lQuote , lEnterpriseSubscriptionEntry );
            lEnterpriseSubscriptionItem.Discount = 50;
            Database.insert( lEnterpriseSubscriptionItem );

            Waiver__c lWaiver = DataFactory.newWaiver( lEnterpriseSubscriptionItem , 10 , 1 );
            lWaiver.OwnerId = lLstUserToCreate[0].Id;
            Database.insert( lWaiver );
            
            Payment__c lPayment = DataFactory.newPayment( lSKUNewOpp );
        	Database.insert( lPayment );
        
        	Eligibility__c lEligibility = DataFactory.newEligibility( lPayment );
        	Database.insert( lEligibility );
        }
    }


    @isTest 
    private static void shouldSendToApprovalAndCreateCase(){
        User lUser = [ SELECT Id, UserName FROM User WHERE UserName = 'standarduser1@test.gympass.com' ];
        Opportunity lOpp = [ SELECT Id, SyncedQuoteId FROM Opportunity WHERE Owner.UserName =: lUser.UserName limit 1];
        Quote lQuote = [ SELECT Id, OpportunityId, Total_Number_of_Employees__c, RecordTypeId, Deal_Desk_Approved_Conditions__c FROM Quote WHERE Id =: lOpp.SyncedQuoteId limit 1];

        Test.startTest();
        
        System.runAs( lUser ) {
            ResellerSkuOfferReviewController.sendToApproval( lOpp.Id , 'sent by Test class' , 50 , lQuote.Id , 'sent by Test class' , true , true, false );
        }
        
        lQuote.Exclusivity_clause__c = true;
        lQuote.Contact_Permission__c = 'Allowlist';
        update lQuote;
        
        System.assertEquals('Allowlist', lQuote.Contact_Permission__c);
        System.assertNotEquals(null, lQuote.Deal_Desk_Approved_Conditions__c);
        
        Test.stopTest();        
    }
    
     @isTest 
    private static void shouldSendToApprovalAndCreateCas2(){
         User lUser = [ SELECT Id, UserName FROM User WHERE UserName = 'standarduser1@test.gympass.com' limit 1 ];
        Opportunity lOpp2 = [ SELECT Id, SyncedQuoteId FROM Opportunity WHERE Owner.UserName =: lUser.UserName limit 1 ];
        Quote lQuote2 = [ SELECT Id, OpportunityId, Total_Number_of_Employees__c, RecordTypeId FROM Quote WHERE Id =: lOpp2.SyncedQuoteId limit 1 ];
        
        Test.startTest();
        
        System.runAs( lUser ) {  
            ResellerSkuOfferReviewController.sendToApproval( lOpp2.Id , 'sent by Test class' , 50 , lQuote2.Id , 'sent by Test class' , false , false, true );
        }
 
        Test.stopTest();        
    }

  
}