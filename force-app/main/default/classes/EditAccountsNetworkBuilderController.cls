/**
 * @description       : 
 * @author            : alysson.mota@gympass.com
 * @group             : 
 * @last modified on  : 03-09-2022
 * @last modified by  : alysson.mota@gympass.com
**/
public with sharing class EditAccountsNetworkBuilderController {
    
    @AuraEnabled(cacheable=true)
    public static List<AccountWrapper> getAccountList(List<Id> accIds){
        AccountSelector accSelector = new AccountSelector();
        List<AccountWrapper> accountLst = new List<AccountWrapper>();

        List<Account> accounts = accSelector.selectByIdForPartners(new Set<Id>(accIds));

        for (Account acc : accounts) {
            AccountWrapper wrapper = new AccountWrapper();
            wrapper.Id = acc.Id;
            wrapper.Name = acc.Name;
            wrapper.PartnerLevel = acc.Partner_Level__c;
            //wrapper.contacts = new List<AccountContactRelation>();
            
            /*
            for (AccountContactRelation c : acc.AccountContactRelations) {
                wrapper.contacts.add(c);
            }
            */
 
            if (acc.Legal_Representative__c != null) {
                wrapper.LegalRepresentative = acc.Legal_Representative__r.Name;
            }

            

            accountLst.add(wrapper);
        }

        return accountLst;
    }

    @AuraEnabled
    public static void updateAccounts(Object data) {
        List<Account> accsToUpdate = new List<Account>();
        fflib_ISObjectUnitOfWork uow = Application.UnitOfWork.newInstance();

        List<AccountWrapper> accountWrapperList = (List<AccountWrapper>) JSON.deserialize(
            JSON.serialize(data),
            List<AccountWrapper>.class
        );

        for (AccountWrapper wrapper : accountWrapperList) {
            Account accToUpdate = new Account();
            accToUpdate.Id = wrapper.Id;
            
            if (wrapper.Name != null)
                accToUpdate.Name = wrapper.Name;
            
            if (wrapper.PartnerLevel != null) 
                accToUpdate.Partner_Level__c =  wrapper.PartnerLevel;
            
            accsToUpdate.add(accToUpdate);
        }
        
        try {
            uow.registerDirty(accsToUpdate);
            uow.commitWork();
        } catch(Exception e) {
            String errorMessage = getErrorMessageFromException(e);

            throw new AuraHandledException(errorMessage);
        }
    }

    public class AccountWrapper {
        @AuraEnabled public String Id {get; set;}
        @AuraEnabled public String Name {get; set;}
        @AuraEnabled public String PartnerLevel {get; set;}
        @AuraEnabled public List<AccountContactRelation> Contacts {get; set;}
        @AuraEnabled public String LegalRepresentative {get; set;}
    }

    private static String getErrorMessageFromException(Exception e) {
        String errorMessage = e.getMessage();

        if (e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')) {
            errorMessage = errorMessage.substringAfter('FIELD_CUSTOM_VALIDATION_EXCEPTION');
        }

        if (e.getMessage().contains('INVALID_OR_NULL_FOR_RESTRICTED_PICKLIST')) {
            errorMessage = errorMessage.substringAfter('INVALID_OR_NULL_FOR_RESTRICTED_PICKLIST');
        }

        if (e.getMessage().contains('REQUIRED_FIELD_MISSING')) {
            errorMessage = errorMessage.substringAfter('REQUIRED_FIELD_MISSING');
        }

        return errorMessage;
    } 
}