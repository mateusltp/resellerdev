@IsTest
public class AccountTagusDTOTest {
  @TestSetup
  public static void makeData() {
    TriggerHandler.bypass('AccountTriggerHandler');
    TriggerHandler.bypass('ContactTriggerHandler');

    List<Account> accounts = new List<Account>();

    Account parentAccount = new Account(
      Name = 'Helena e Nair Gráfica ME',
      BillingCountry = 'Brazil',
      BillingState = 'São Paulo',
      UUID__c = new Uuid().getValue()
    );
    accounts.add(parentAccount);

    Account account = new Account(
      Name = 'Theo e Eliane Alimentos Ltda',
      BillingCountry = 'Brazil',
      BillingState = 'São Paulo',
      UUID__c = new Uuid().getValue()
    );
    accounts.add(account);

    insert accounts;

    List<Contact> contacts = new List<Contact>();

    Contact parentContact = new Contact(
      FirstName = 'Emanuel',
      LastName = 'Yago Costa',
      AccountId = parentAccount.Id,
      Email = 'emanuelyagocosta-87@bemarius.example',
      Role__c = 'FINANCE',
      UUID__c = new Uuid().getValue()
    );
    contacts.add(parentContact);

    Contact contact = new Contact(
      FirstName = 'Emanuel',
      LastName = 'Yago Costa',
      AccountId = account.Id,
      Email = 'emanuelyagocosta-87@bemarius.example',
      Role__c = 'FINANCE',
      UUID__c = new Uuid().getValue()
    );
    contacts.add(contact);

    insert contacts;

    accounts.clear();

    parentAccount.Attention__c = parentContact.Id;
    accounts.add(parentAccount);

    account.Attention__c = contact.Id;
    accounts.add(account);

    update accounts;
  }

  @IsTest
  public static void execute() {
    List<Account> accounts = [
      SELECT Id
      FROM Account
      ORDER BY ParentId NULLS LAST
    ];

    Map<Id, Account> accountMap = AccountTagusDTO.getAccountMapWithFieldsToParse(
      accounts
    );

    Account parentAccount = accountMap.get(accounts[0].Id);

    Account account = accountMap.get(accounts[1].Id);

    AccountTagusDTO parentAccountDTO = new AccountTagusDTO(parentAccount);

    AccountTagusDTO accountDTO = new AccountTagusDTO(account);

    System.assertEquals(
      account.UUID__c,
      accountDTO.getId(),
      'Wrong id assigned'
    );

    System.assertEquals(
      account.Id,
      accountDTO.getSalesforceId(),
      'Wrong Salesforce id assigned'
    );

    accountDTO.setParentId(parentAccountDTO.getId());

    System.assertEquals(
      parentAccountDTO.getId(),
      accountDTO.getParentId(),
      'Parent id not assigned'
    );

    List<AccountTagusDTO.ContactDTO> parentContactsDTO = parentAccountDTO.getContacts();

    System.assert(!parentContactsDTO.isEmpty(), 'No contact parsed');

    AccountTagusDTO.ContactDTO parentContactDTO = parentContactsDTO[0];

    Contact parentContact = [
      SELECT Id, UUID__c
      FROM Contact
      WHERE UUID__c = :parentContactDTO.getId()
      LIMIT 1
    ];

    System.assertEquals(
      parentContact.Id,
      parentContactDTO.getSalesforceId(),
      'Wrong Salesforce id assigned'
    );

    List<AccountTagusDTO.ContactDTO> contactsDTO = accountDTO.getContacts();

    System.assert(!contactsDTO.isEmpty(), 'No contact parsed');

    AccountTagusDTO.ContactDTO contactDTO = contactsDTO[0];

    Contact contact = [
      SELECT Id, UUID__c
      FROM Contact
      WHERE UUID__c = :contactDTO.getId()
      LIMIT 1
    ];

    System.assertEquals(
      contact.Id,
      contactDTO.getSalesforceId(),
      'Wrong Salesforce id assigned'
    );
  }
}