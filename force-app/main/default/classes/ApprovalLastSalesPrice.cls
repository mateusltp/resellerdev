/**
 * @description       : 
 * @author            : Mateus Augusto - GFT (moes@gft.com)
 * @group             : 
 * @last modified on  : 03-12-2022
 * @last modified by  : Mateus Augusto - GFT (moes@gft.com)
**/
public without sharing class ApprovalLastSalesPrice {
    
    List< Id > gLstOppId;
    Map< Id , Quote > gMapIdOldQuote;
    List< Quote > gLstNewQuote;
    
    public ApprovalLastSalesPrice(){}
    
    public void saveLastSalesPriceApproved( Map< Id , Quote > aMapIdOldQuote , List< Quote > aLstNewQuote ){
        gLstNewQuote = aLstNewQuote;
        gMapIdOldQuote = aMapIdOldQuote;
        List< Assert_Data__c > lLstFeesApprovedDiscount = saveLastSalesPriceApproved();
        
        if( !lLstFeesApprovedDiscount.isEmpty() ){
            Database.upsert( lLstFeesApprovedDiscount );
        }
    }
    
    
    private List< Assert_Data__c > saveLastSalesPriceApproved(){
        system.debug('metodo');
        List< Assert_Data__c > lLstFeesApprovedDiscount = new List< Assert_Data__c >();
        system.debug('lista assert '+lLstFeesApprovedDiscount);
        List< Id > lLstQuoteId = getApprovedQuoteId();
        system.debug('lLstQuoteId '+lLstQuoteId);
        if( lLstQuoteId.isEmpty() ){ return lLstFeesApprovedDiscount; } 
        system.debug('depois do return');
        List< QuoteLineItem > lLstQuoteLineItem = getApprovedFees( lLstQuoteId );
        system.debug('lLstQuoteLineItem '+lLstQuoteLineItem);
        Map< String , Decimal > lMapFeeTypeApprovedDiscount = getMapFeeTypeApprovedDiscount( lLstQuoteLineItem );
        system.debug('lMapFeeTypeApprovedDiscount '+lMapFeeTypeApprovedDiscount);
        lLstFeesApprovedDiscount = setAssertDataFeeDiscount( lLstQuoteLineItem , lMapFeeTypeApprovedDiscount );
        
        return lLstFeesApprovedDiscount;
    }
    
    private List< Assert_Data__c > setAssertDataFeeDiscount( List< QuoteLineItem > aLstQuoteLineItem , Map< String , Decimal > aMapFeeTypeApprovedDiscount ){
        List< Assert_Data__c > lLstAssertData = 
            [ SELECT Id, Field_Name__c, Object_Name__c, Fee_Type__c,  Old_Value__c, New_Value__c,Opportunity__c
             FROM Assert_Data__c WHERE Opportunity__c =: gLstOppId AND Fee_Type__c IN( 'Enterprise Subscription' , 'Setup Fee' )
             AND Field_Name__c = 'UnitPrice' AND Object_Name__c = 'quotelineitem' ];
        system.debug('lLstAssertData lista vazia ou nao '+lLstAssertData);
        
        if( lLstAssertData.isEmpty() ){
            for( QuoteLineItem iQuoteLineItem : aLstQuoteLineItem ){
                lLstAssertData.add( new Assert_Data__c(
                    Opportunity__c = gLstOppId[0],
                    Fee_Type__c = iQuoteLineItem.Fee_Type__c,
                    Object_Name__c = 'quotelineitem',
                    Field_Name__c = 'UnitPrice'
                    //Pre_Approved_Condition__c = false,
                    //New_Value__c = String.valueOf(iQuoteLineItem.UnitPrice)
                ) );
            }
        }
        system.debug('lLstAssertData lista antes do for '+lLstAssertData);
        for( Assert_Data__c iAssertData : lLstAssertData ){
            Decimal lNewDiscount = aMapFeeTypeApprovedDiscount.get( iAssertData.Fee_Type__c );
            system.debug('lNewDiscount  '+lNewDiscount);
            if(iAssertData.New_Value__c == null){
                iAssertData.New_Value__c = String.valueOf( lNewDiscount );
            } else{
                if(!String.isBlank( iAssertData.New_Value__c ) && Decimal.valueOf( iAssertData.New_Value__c ) > lNewDiscount){
                    iAssertData.Old_Value__c = iAssertData.New_Value__c;
                    iAssertData.New_Value__c = String.valueOf( lNewDiscount );
                }
                
            }

        }
       /* 
        List<Step_Towards_Success1__c>  listStep = [select id,name,Achieved__c,Step_Number__c from Step_Towards_Success1__c where Related_Opportunity__c =: lLstAssertData[0].Opportunity__c];
        for(Step_Towards_Success1__c step : listStep){
            step.Achieved__c = 'Yes';
        }
        update listStep; */
        
        return lLstAssertData;
    }
    
    private List< Id > getApprovedQuoteId(){
        gLstOppId = new List< Id >();
        List< Id > lLstQuoteId = new List< Id >();
        
        for( Quote iQuote : gLstNewQuote ){
            if( !gMapIdOldQuote.get( iQuote.Id ).Enterprise_Subscription_Approved__c && iQuote.Enterprise_Subscription_Approved__c && iQuote.Opportunity_Record_Type__c == 'Indirect Channel - Fast Track'){
                lLstQuoteId.add( iQuote.Id );
                gLstOppId.add( iQuote.OpportunityId );
            }
        }
        system.debug('depois do return');
        return lLstQuoteId;
    }
    
    private List< QuoteLineItem > getApprovedFees( List< Id > aLstQuoteId ){
        return [ SELECT Id, Fee_Type__c, UnitPrice FROM QuoteLineItem WHERE QuoteId =: aLstQuoteId 
                AND Fee_Type__c IN( 'Enterprise Subscription' , 'Setup Fee' ) ];
    }
    
    private Map< String , Decimal > getMapFeeTypeApprovedDiscount( List< QuoteLineItem > aLstQuoteLineItem ){
        Map< String , Decimal > lMapFeeTypeApprovedDiscount = new Map< String , Decimal >();
        
        for( QuoteLineItem iQuoteLineItem : aLstQuoteLineItem ){
            lMapFeeTypeApprovedDiscount.put( iQuoteLineItem.Fee_Type__c , iQuoteLineItem.UnitPrice );
        }
        
        return lMapFeeTypeApprovedDiscount;
    } 
}