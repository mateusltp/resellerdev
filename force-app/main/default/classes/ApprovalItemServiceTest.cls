/**
 * @description       : ApprovalItemService Test Class
 * @author            : Tiago Ribeiro
 * @group             : Corporate
 * @last modified on  : 02-14-2022
 * @last modified by  : Tiago Ribeiro
 * Modifications Log 
 * Ver   Date         Author                              Modification
 * 1.0   01-31-2022   Tiago Ribeiro                       Initial Version
**/
@isTest
public with sharing class ApprovalItemServiceTest {
    @TestSetup
    static void setupData(){
        Pricebook2 lStandardPricebook = DataFactory.newPricebook();
        Database.update( lStandardPricebook );
        
        Account lAcc = DataFactory.newAccount();              
        Database.insert( lAcc );

        Opportunity lNewBusinessOpp = DataFactory.newOpportunity( lAcc.Id, lStandardPricebook, 'Client_Sales_New_Business' );     
        Database.insert( lNewBusinessOpp );

        Quote lQuote = DataFactory.newQuote(lNewBusinessOpp, '1', 'Gympass_Plus');
        Database.insert(lQuote);
        
        lNewBusinessOpp.SyncedQuoteId = lQuote.Id;
        lNewBusinessOpp.StageName = 'Proposta Aprovada';
        lNewBusinessOpp.FastTrackStage__c = 'Offer Approved';
        Database.update( lNewBusinessOpp );

        Case lOperationalCase = DataFactory.newCase(lNewBusinessOpp, lQuote, 'Deal_Desk_Operational');
        Database.insert(lOperationalCase);
    }

    @isTest
    /**
     * Testing the following methods: 
     * - addApprovalItem
     * - generateApprovalItem
     * - createApprovalItems
     */
    static void testCreateApprovalItems(){
        Map<String, String> mapApprovalItems = new Map<String, String>{
            'ItemName1' => 'TestValue1',
            'ItemName2' => 'TestValue2',
            'ItemName3' => 'TestValue3'
        };

        Map<String, String> mapItemsApproved = new Map<String, String>{
            'ItemName1' => 'false', 
            'ItemName2' => 'false',
            'ItemName3' => 'true'
        };

        List<Case> listCases = [SELECT Id FROM Case LIMIT 1];

        ApprovalItemService approvalItemServ = new ApprovalItemService();
        //Test with List Items and Creation of Approval Items 
        List<Approval_Item__c> listApprovalItems = approvalItemServ.addApprovalItem(listCases[0].Id, mapApprovalItems, mapItemsApproved);
        System.assertEquals(listApprovalItems.size(), 2, 'Two Approval Items to create');
        approvalItemServ.createApprovalItems(listApprovalItems);
        System.assertEquals([SELECT Id FROM Approval_Item__c WHERE Name IN :mapItemsApproved.keySet()].size(), 2, 'Two Approval Items created');

        //Test without List Items
        mapItemsApproved = new Map<String, String>();
        listApprovalItems = approvalItemServ.addApprovalItem(listCases[0].Id, mapApprovalItems, mapItemsApproved);
        System.assert(listApprovalItems.isEmpty(), 'No Approval Items Created when no List Items are sent');
    }

    @isTest
    static void testCreateApprovalItemsWithError(){
        Map<String, String> mapApprovalItems = new Map<String, String>{
            'ItemName1' => 'TestValue1',
            'ItemName2' => 'TestValue2',
            'ItemName3' => 'TestValue3'
        };
        String message = '';
        Map<String, String> mapItemsApproved = new Map<String, String>{
            'ItemName1' => 'false', 
            'ItemName2' => 'false',
            'ItemName3' => 'true'
        };
        ApprovalItemService approvalItemServ = new ApprovalItemService();
        List<Approval_Item__c> listApprovalItems = approvalItemServ.addApprovalItem(null, mapApprovalItems, mapItemsApproved);
        try{
            approvalItemServ.createApprovalItems(listApprovalItems);
        }catch (Exception e){
            message = e.getMessage();
        }

        System.assert(!String.isBlank(message), 'No Case Id on the Approval Items');
    }
}