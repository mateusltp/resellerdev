/**
* @File Name          : BatchGFPartnerExperience.cls
* @Description        :
* @Author             : JRDL@GFT.com
* @Group              :
* @Last Modified By   : 
* @Last Modified On   : 
* @Modification Log   :
* Ver       Date            Author      		    Modification
* 1.0    04/06/2021   JRDL@GFT.com     Initial Version
**/
global class BatchGFPartnerExperience implements Database.Batchable<sObject> {
    global Database.QueryLocator start(Database.BatchableContext bc) {
        Date ed = Date.today().addMonths(-1);
        return Database.getQueryLocator([
            SELECT Id, Data_do_Lancamento__c, AccountId, Account.RecordTypeId, RecordTypeId, StageName
            FROM Opportunity
            WHERE Data_do_Lancamento__c =: ed
        ]);
    }
    
    global void execute(Database.BatchableContext bc, List<Opportunity> scope) {
        
        List<Account> lstAccPartner = new List<Account>();        
        List<Contact> lstUpdateCon = new List<Contact>();
        Map<Id,Contact> lMapContactEvent = new Map<Id,Contact>();
        Id rtPartners = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        Id rtAccPartners = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gym_Partner').getRecordTypeId();
        
        for(Opportunity o: scope){
            if(o.RecordTypeId == rtPartners && o.StageName == 'Lançado/Ganho' && o.Account.RecordTypeId == rtAccPartners){                lstAccPartner.add(o.Account);
            } 
        }
        if(!lstAccPartner.isEmpty()){
            for(Contact con: [Select Id, Status_do_contato__c, GFSendSurvey__c, Email, Primary_HR_Contact__c,DecisionMaker__c FROM Contact WHERE AccountID IN :lstAccPartner]){
                if(con.Status_do_contato__c == 'Ativo' && con.Email != null && con.Primary_HR_Contact__c == TRUE){
                    con.GFSendSurvey__c = 'Partners CSAT - Sales Experience';
                    lstUpdateCon.add(con); 
                }
                else if (con.Status_do_contato__c == 'Ativo' && con.Email != null && con.DecisionMaker__c == 'Yes'){
                    con.GFSendSurvey__c = 'Clients CSAT - Sales Experience';
                    lMapContactEvent.put(con.Id, con);
                }
            }
        }
        
        if(!lMapContactEvent.isEmpty()){
            for(Event ev : [SELECT Id, WhoId, Who.Name,Realizado__c, RecordType.Name FROM Event Where Subject='Meeting' AND WhoId IN: lMapContactEvent.KeySet() AND Realizado__c = 'Yes' AND ActivityDate <= LAST_N_DAYS:90]){
				lstUpdateCon.add(lMapContactEvent.get(ev.whoId));
            }
        }
        
        if(!lstUpdateCon.isEmpty()){
            update lstUpdateCon;
        }
    }
    
    global void finish(Database.BatchableContext bc) {
        
    }
}