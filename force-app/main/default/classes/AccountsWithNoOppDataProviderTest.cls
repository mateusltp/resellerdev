/**
 * Created by bruno on 23/02/2022.
 */

@IsTest
private class AccountsWithNoOppDataProviderTest
{
    @TestSetup
    static void createData(){

        Account lAcc = PartnerDataFactory.newAccount();
        lAcc.Name = 'Parent Acct';
        Database.insert( lAcc );

        Opportunity lOpp = PartnerDataFactory.newOpportunity( lAcc.Id, 'Partner_Flow_Opportunity' );
        Database.insert( lOpp );
    }

    @IsTest
    static void SDG_Test_no_child_no_results(){

        List<Opportunity> oppList = [SELECT Id FROM Opportunity LIMIT 1];

        String testRecordId = oppList.get(0).Id;

        // instantiate class
        AccountsWithNoOppDataProvider dataProvider = new AccountsWithNoOppDataProvider();
        sortablegrid.SDG coreSDG = dataProvider.LoadSDG('', testRecordId);

        // create the user request
        sortablegrid.SDGRequest request = new sortablegrid.SDGRequest();
        request.ParentRecordID = testRecordId;
        request.PageID = 1;
        request.PageSize = 10;
        request.FieldSetName=null;
        request.RelationshipName=null;
        request.SDGTag='Apex:AccountsWithNoOppDataProvider';



        sortablegrid.SDGResult result = dataProvider.getData(coreSDG, request);
        system.assertEquals(0, result.data.size());
    }

    @IsTest
    static void SDG_Test_positive_with_child_with_results(){
        Account parentAccount = [SELECT Id FROM Account LIMIT 1];

        Account childAccount = PartnerDataFactory.newChildAccount(parentAccount.Id);
        childAccount.Partner_Level__c = 'Location';
        insert childAccount;

        List<Opportunity> oppList = [SELECT Id FROM Opportunity LIMIT 1];

        Account_Opportunity_Relationship__c accountOpportunityRelationship = new Account_Opportunity_Relationship__c();
        accountOpportunityRelationship.Account__c = childAccount.Id;
        accountOpportunityRelationship.Opportunity__c = oppList.get(0).Id;
        insert accountOpportunityRelationship;

        String testRecordId = oppList.get(0).Id;

        // instantiate class
        AccountsWithNoOppDataProvider dataProvider = new AccountsWithNoOppDataProvider();
        sortablegrid.SDG coreSDG = dataProvider.LoadSDG('', testRecordId);

        // create the user request
        sortablegrid.SDGRequest request = new sortablegrid.SDGRequest();
        request.ParentRecordID = testRecordId;
        request.PageID = 1;
        request.PageSize = 10;
        request.FieldSetName=null;
        request.RelationshipName=null;
        request.SDGTag='Apex:AccountsWithNoOppDataProvider';



        sortablegrid.SDGResult result = dataProvider.getData(coreSDG, request);
        system.assertEquals(1, result.data.size());
    }

    @IsTest
    static void SDG_Test_positive_with_child_no_results() {
        Account parentAccount = [SELECT Id FROM Account LIMIT 1];

        Account childAccount = PartnerDataFactory.newChildAccount(parentAccount.Id);
        childAccount.Partner_Level__c = 'Location';
        insert childAccount;

        List<Opportunity> oppList = [SELECT Id FROM Opportunity LIMIT 1];

        Account_Opportunity_Relationship__c accountOpportunityRelationship = new Account_Opportunity_Relationship__c();
        accountOpportunityRelationship.Account__c = childAccount.Id;
        accountOpportunityRelationship.Opportunity__c = oppList.get(0).Id;
        insert accountOpportunityRelationship;

        Opportunity childOpp = PartnerDataFactory.newOpportunity( parentAccount.Id, 'Partner_Flow_Child_Opportunity' );
        childOpp.MasterOpportunity__c = oppList.get(0).Id;
        Database.insert( childOpp );

        Account_Opportunity_Relationship__c accountOpportunityRelationshipChild = new Account_Opportunity_Relationship__c();
        accountOpportunityRelationshipChild.Account__c = childAccount.Id;
        accountOpportunityRelationshipChild.Opportunity__c = childOpp.Id;
        insert accountOpportunityRelationshipChild;

        String testRecordId = oppList.get(0).Id;

        // instantiate class
        AccountsWithNoOppDataProvider dataProvider = new AccountsWithNoOppDataProvider();
        sortablegrid.SDG coreSDG = dataProvider.LoadSDG('', testRecordId);

        // create the user request
        sortablegrid.SDGRequest request = new sortablegrid.SDGRequest();
        request.ParentRecordID = testRecordId;
        request.PageID = 1;
        request.PageSize = 10;
        request.FieldSetName=null;
        request.RelationshipName=null;
        request.SDGTag='Apex:AccountsWithNoOppDataProvider';



        sortablegrid.SDGResult result = dataProvider.getData(coreSDG, request);
        system.assertEquals(0, result.data.size());
    }

    @IsTest
    static void isUserSelectableTest(){

        Boolean isUserSelectable = true;
        System.assertEquals(isUserSelectable, AccountsWithNoOppDataProvider.isUserSelectable());
    }
}