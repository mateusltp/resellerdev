/**
 * @description       :
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             :
 * @last modified on  : 06-30-2022
 * @last modified by  : alysson.mota@gympass.com
**/
public without sharing class PublishContactPartnerOnTagusEvent {

    private static final PS_Constants constants = PS_Constants.getInstance();

    public void execute() {
      Set<Id> partnerIds = new Set<Id>();
      if(System.Label.TAGUS_PARTNER !=  constants.PARTNER && !Test.isRunningTest()) return;
      for (Contact newContact : (List<Contact>) Trigger.new) {
        if (isToPublishContact(newContact)) {
            partnerIds.add(newContact.AccountId);
        }
      }
      if(!partnerIds.isEmpty()){
        List<Account> partners = ((AccountSelector)Application.Selector.newInstance(Account.sobjectType)).selectToTagus(partnerIds);
        new TagusAccountPartnerOutboundPublisher(partners).runUpdate();
      }
    }

    private Boolean isToPublishContact(Contact newContact) {
      final id CONTACT_CLIENT_RECORDTYPE = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Contact').getRecordTypeId();
      Map<Id, Contact> oldContactMap = (Map<Id, Contact>) Trigger.oldMap;

      Contact oldContact = oldContactMap?.get(newContact.Id);

      return newContact.Status_do_contato__c.equals('Ativo') && newContact.recordTypeId == CONTACT_CLIENT_RECORDTYPE && (oldContact == null || oldContact.Status_do_contato__c != newContact.Status_do_contato__c || oldContact.LastName != newContact.LastName || oldContact.Email != newContact.Email || oldContact.Role__c != newContact.Role__c || oldContact.FirstName != newContact.FirstName || oldContact.MailingStreet != newContact.MailingStreet || oldContact.MailingCity != newContact.MailingCity || oldContact.MailingCountry != newContact.MailingCountry || oldContact.MailingState != newContact.MailingState || oldContact.MailingPostalCode != newContact.MailingPostalCode || oldContact.Phone != newContact.Phone);
    }
  }