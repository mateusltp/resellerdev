/**
 * @description       : 
 * @author            : AlyssonMota
 * @group             : 
 * @last modified on  : 05-24-2022
 * @last modified by  : Mateus Augusto - GFT (moes@gft.com)
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   11-30-2020   Alysson Mota   Initial Version
**/
public with sharing class EnablersComercialConditionsHandler {
    private Map<String, List<Discount_and_Waiver_approval_parameters__mdt>> mapOptionApprovalMetadata;
    private final Set<String> enablersNamesSmb = new Set<String>{'Allowlist', 'Full launch | Expansion', 'Exclusivity', 'Email Database'};

    private Id indNewRenRtId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Indirect_Channel_New_Business').getRecordTypeId();
    private Id smbNewBusRtId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SMB_New_Business').getRecordTypeId();
    private Id smbNewRenRtId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SMB_Success_Renegotiation').getRecordTypeId();
    private Id cliNewBusRtId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
    private Id cliNewRenRtId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Success_Renegotiation').getRecordTypeId();

    public EnablersComercialConditionsHandler() {
        initMetadata();
    }

    private void initMetadata() {
        mapOptionApprovalMetadata = new Map<String, List<Discount_and_Waiver_approval_parameters__mdt>>();

        List<Discount_and_Waiver_approval_parameters__mdt> params = [
            SELECT  Id, Waiver_CCO__c, Waiver_RM__c, CCO__c, CF__c, Initial_Approver_Email__c, Option__c,
                    RM__c, RM_1__c, RM_2__c, Sellers__c, Waiver_RM_1__c, Initial_Approver_Queue_Name__c, Label, Country__c, VP__c
            FROM Discount_and_Waiver_approval_parameters__mdt
        ];

        for (Discount_and_Waiver_approval_parameters__mdt param : params) {
            if (mapOptionApprovalMetadata.containsKey(param.Country__c)) {
                mapOptionApprovalMetadata.get(param.Country__c).add(param);
            } else {
                mapOptionApprovalMetadata.put(param.Country__c, new List<Discount_and_Waiver_approval_parameters__mdt>{param});
            }
        }
    }

    public Boolean areConditionsOkForES(Opportunity opp, QuoteLIneItem item, Payment__c payment,List<Waiver__c> waivers, List<Eligibility__c> eligibilities) {
        Boolean conditionsOk = true;

        if (waivers != null && waivers.size() > 0) {
            return false;
        }        

        List<Discount_and_Waiver_approval_parameters__mdt> discountParams = mapOptionApprovalMetadata.get(opp.Account.BillingCountry);
        
        // Default to United States
        if (discountParams == null) {
            discountParams = mapOptionApprovalMetadata.get('United States'); 
        }

        // No params return true (OK)
        if (discountParams == null) {
            return true;
        }

        String paymentMethod = getPaymentMethod(eligibilities);
        
        if (paymentMethod != null && payment != null && payment.Frequency__c != null) {
            String paymentFrequency = (payment.Frequency__c == 'Yearly' ? 'Annual' : payment.Frequency__c);

            paymentMethod = paymentMethod.toLowerCase();
            paymentFrequency = paymentFrequency.toLowerCase();

            Map<String, Discount_and_Waiver_approval_parameters__mdt> discountPamramsMap = convertParamsListToMap(discountParams);

            Decimal rmDiscountPercent = getRmDiscount(opp, paymentMethod, paymentFrequency, discountPamramsMap);

            if (rmDiscountPercent != null && item.Discount__c != null) {
                if (item.Discount__c > rmDiscountPercent) {
                    conditionsOk = false;
                } else {
                    conditionsOk = true;
                }
            }
        }
        return conditionsOk;
    }

    private Decimal getRmDiscount(Opportunity opp, String paymentMethod, String paymentFrequency, Map<String, Discount_and_Waiver_approval_parameters__mdt> discountPamramsMap) {
        Decimal discountPercentage = null;
        
        for (String key : discountPamramsMap.keySet()) {
            Discount_and_Waiver_approval_parameters__mdt param = discountPamramsMap.get(key);
            
            if ((opp.RecordTypeId == smbNewBusRtId || opp.RecordTypeId == smbNewRenRtId) && param.Label.contains('SMB')) {
                if (key.containsIgnoreCase(paymentFrequency) && key.containsIgnoreCase(paymentMethod)) {
                    discountPercentage = param.RM__c;
                    break;
                } 
            } 
            
            if (opp.RecordTypeId == cliNewBusRtId || opp.RecordTypeId == cliNewRenRtId || opp.RecordTypeId == indNewRenRtId) {
                if (key.containsIgnoreCase(paymentFrequency) && key.containsIgnoreCase(paymentMethod)) {
                    discountPercentage = param.RM__c;
                    break;
                }
            }
        }
        
        return discountPercentage;
    }

    private Map<String, Discount_and_Waiver_approval_parameters__mdt> convertParamsListToMap(List<Discount_and_Waiver_approval_parameters__mdt> discountParams) {
        Map<String, Discount_and_Waiver_approval_parameters__mdt> discountPamramsMap = new Map<String, Discount_and_Waiver_approval_parameters__mdt>();
        
        for (Discount_and_Waiver_approval_parameters__mdt param : discountParams) {
            discountPamramsMap.put(param.label, param);
        }

        return discountPamramsMap;
    }

    private String getPaymentMethod(List<Eligibility__c> eligibilities) {
        String paymentMethod = null;
        
        if (eligibilities != null) {
            for (Eligibility__c eligibility : eligibilities) {
                if (eligibility.Payment_Method__c == 'Payroll + Credit Card' || eligibility.Payment_Method__c == 'Credit Card') {
                    paymentMethod = 'Credit Card';
                    break;
                } else if (eligibility.Payment_Method__c == 'Payroll') {
                    paymentMethod = 'Payroll';
                }
            }
        }

        return paymentMethod;
    }

    public void evaluateEnablers(Quote quote, Opportunity opp) {
        if (quote == null || opp == null) return; 
        if (opp.RecordType.DeveloperName != 'SMB_New_Business' && opp.RecordType.DeveloperName != 'SMB_Success_Renegotiation' && opp.RecordType.DeveloperName != 'Indirect_Channel_New_Business') return;
        
        String enablersOutOfStandardCondition = '';
        List<Step_Towards_Success1__c> steps = opp.Steps_Towards_Success1__r;
        
        for (Step_Towards_Success1__c step : steps) {
            if (enablersNamesSmb.contains(step.Name) && step.Achieved__c == 'No') {
                if (opp.Account.BillingCountry == 'Brazil' && step.Step_Number__c == 6 && (opp.RecordTypeId == smbNewBusRtId || opp.RecordTypeId == indNewRenRtId)) {
                    enablersOutOfStandardCondition += (enablersOutOfStandardCondition == '' ? step.Name : '; ' + step.name) ;
                } else if (step.Step_Number__c != 6) {
                    enablersOutOfStandardCondition += (enablersOutOfStandardCondition == '' ? step.Name : '; ' + step.name);
                }
            }
        }
        if(opp.RecordTypeId == indNewRenRtId){
            if(quote.Enablers_out_of_standard_condition__c != enablersOutOfStandardCondition){//&& quote.Enablers_Approved__c == true
                quote.Enablers_Approval_Needed__c = true;
                quote.Enablers_Approved__c = false;                
            }
            if(enablersOutOfStandardCondition == ''){
                quote.Enablers_Approval_Needed__c = false;
                quote.Enablers_Approved__c = true;
            }
            quote.Enablers_out_of_standard_condition__c = enablersOutOfStandardCondition;
            update quote;
        }else{            
            quote.Enablers_out_of_standard_condition__c = enablersOutOfStandardCondition;            
            if (quote.Enablers_out_of_standard_condition__c != '') {
                quote.Discount_Approval_Level__c = (quote.Discount_Approval_Level__c < 2 || quote.Discount_Approval_Level__c == null ? 2 : quote.Discount_Approval_Level__c);
                quote.Enablers_Approval_Needed__c = true;
            }
            else{
                quote.Enablers_Approval_Needed__c = false;
            }
            
        }
        
    }
}