global class ScheduleTagusIntegration implements Schedulable, Database.AllowsCallouts {	
  	global void execute(SchedulableContext SC) {
  		Database.executeBatch(new BatchTagusIntegration());
   	}
}