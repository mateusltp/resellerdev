public without sharing class DisableSalesOrder {
  public void execute() {
    Map<Id, Opportunity> newOpportunityMap = (Map<Id, Opportunity>) Trigger.newMap;
    List<Order> orders = queryActiveOrders();

    for(Order order : orders) {
      Opportunity newOpportunity = newOpportunityMap.get(order.OpportunityId);

      if (order.Opportunity.Type.equals(newOpportunity.Type)) {
        order.Status = 'Inactivated';
      }
    }

    if (!orders.isEmpty()) {
      Database.update(orders);
    }
  }

  private List<Order> queryActiveOrders() {
    List<Account> accounts = new List<Account>();

    for(Opportunity opportunity : (List<Opportunity>) Trigger.new) {
      accounts.add(new Account(Id = opportunity.AccountId));
    }

    return [
      SELECT Id,
      OpportunityId,
      Opportunity.StageName,
      Opportunity.Type,
      Status
      FROM Order
      WHERE AccountId IN :accounts
      AND Status = 'Activated'
      WITH SECURITY_ENFORCED
    ];
  }
}