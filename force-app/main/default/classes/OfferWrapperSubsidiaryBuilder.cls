/**
 * @author vncferraz
 * 
 * Provide  builder specialization for Type of Request Subsidiary
 */
public virtual class OfferWrapperSubsidiaryBuilder extends BaseOfferWrapperBuilder{
    
    protected Account parentAccount;
    protected Opportunity relatedOpportunity;
    protected OpportunityRepository opportunities;

    override
    public virtual void init(){
        super.init();
        this.wrapper = new SubsidiaryOfferWrapper();
        this.opportunities = new OpportunityRepository();
    }

    override
    protected void preLoad(Offer_Queue__c offer){
        
        super.preLoad(offer);
        
        try{
            this.parentAccount = accounts.byId( offer.Parent_Account_ID__c );
            this.relatedOpportunity = opportunities.byIdSMBForAccount( this.parentAccount.Id, offer.Opportunity_ID__c );
        }catch(System.QueryException e){
            addError('Please check Account Hierarchy and Opportunity for Account '+offer.Account_ID__c + ' and Parent Account '+ offer.Parent_Account_ID__c);
        }
    }   
    
    override
    public BaseOfferWrapper postLoad(Offer_Queue__c offer){ 
        if (!wrapper.hasError())        
            wrapper.setOpportunity( this.relatedOpportunity );

        if (!wrapper.hasError())        
            wrapper.setAccountInOpportunity( toAccountInOpportunity(offer) );
        
        return wrapper;
    }

    protected Account_Opportunity_Relationship__c toAccountInOpportunity(Offer_Queue__c offer){
        Account_Opportunity_Relationship__c accountInOpportunity = new Account_Opportunity_Relationship__c();
        accountInOpportunity.Account__c = offer.Account_ID__c;
        accountInOpportunity.Billing_Percentage__c = offer.ES_Billing_Percentage__c;
        accountInOpportunity.CurrencyIsoCode = this.relatedOpportunity.CurrencyIsoCode;
        accountInOpportunity.Gympass_Entity__c = this.gympassEntityId;
        accountInOpportunity.Main_Order__c = false;
        accountInOpportunity.Opportunity__c = this.relatedOpportunity.Id;
        accountInOpportunity.OwnerId = this.relatedOpportunity.OwnerId;    

        return accountInOpportunity;
    }
}