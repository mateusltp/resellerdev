/**
 * @description       : IT-2185: Data provider class for CurrentValidProducts Sortable Data Grid
 * @author            : bruno.mendes@gympass.com
 * @group             :
 * @last modified on  : 04-25-2022
 * @last modified by  : gft.samuel.silva@ext.gympass.com
 */

global without sharing class AccountsWithNoOppDataProvider implements sortablegrid.sdgIDataProvider
{
    global String className = AccountsWithNoOppDataProvider.class.getName();
    global PS_Constants constants = PS_Constants.getInstance();

    global static Boolean isUserSelectable()
    {
        return true;
    }

    global sortablegrid.SDGResult getData(sortablegrid.SDG coreSDG,  sortablegrid.SDGRequest request)
    {
        sortablegrid.SDGResult result = new sortablegrid.SDGResult();
        System.debug('result >>>'+result);

        result.data = new List<Sobject>();
        System.debug('result data'+result.data);

        try {
            AccountOpportunitySelector accOppSelector = (AccountOpportunitySelector)Application.Selector.newInstance(Account_Opportunity_Relationship__c.sobjectType);
            // retrieve opportunity members from master opportunity
            List<Account_Opportunity_Relationship__c> accountOpportunityRelationshipsFromMasterOpp = accOppSelector.selectOppMemberByOppID(new Set<Id> {request.ParentRecordID});
            Map<Id, Account> childAccountsById = new Map<Id, Account>();

            // ignore parent account
            for (Account_Opportunity_Relationship__c accountOpportunityRelationship : accountOpportunityRelationshipsFromMasterOpp) {
                if (accountOpportunityRelationship.Account__c != accountOpportunityRelationshipsFromMasterOpp.get(0).Opportunity__r.AccountId) {
                    childAccountsById.put(accountOpportunityRelationship.Account__c, new Account(Id = accountOpportunityRelationship.Account__c, Name = accountOpportunityRelationship.Account__r.Name));
                }
            }
            system.debug('## childAccountsById: '+childAccountsById);
            OpportunitySelector opportunitySelector = (OpportunitySelector)Application.Selector.newInstance(Opportunity.SObjectType);

            // retrieve child opportunities from master opportunity
            Map<Id, Opportunity> opportunitiesById = opportunitySelector.selectChildOpportunitiesByMasterOppId(new Set<Id> { request.ParentRecordID });
            system.debug('## opportunitiesById: '+opportunitiesById);

            List<Account_Opportunity_Relationship__c> accountOpportunityRelationshipsFromChildrenOpps = accOppSelector.selectOppMemberByOppID(opportunitiesById.keySet());
            system.debug('## accountOpportunityRelationshipsFromChildrenOpps: '+accountOpportunityRelationshipsFromChildrenOpps);

            // remove from "Accounts with no opportunity" list the accounts that are part of a child opportunity
            for (Account_Opportunity_Relationship__c accountOpportunityRelationship : accountOpportunityRelationshipsFromChildrenOpps) {
                childAccountsById.remove(accountOpportunityRelationship.Account__c);
            }

            AccountContractAgreementRelSelector accountContractSelector = (AccountContractAgreementRelSelector)Application.Selector.newInstance(Account_Contract_Agreement_Relationship__c.sobjectType); 
            Map<Id, List<Account_Contract_Agreement_Relationship__c>> accsInContract = accountContractSelector.selectByOpportunityId(new Set<Id>{request.ParentRecordID});
            if(!accsInContract.isEmpty()){
                for (Account_Contract_Agreement_Relationship__c iAcc : accsInContract.get( request.ParentRecordID )  ){
                    childAccountsById.remove(iAcc.AccountId__r.Id);
                }
            }

            if(childAccountsById.isEmpty()){
                PartnerChildOpportunityManagerController.setShowButton(false);
            } else {
                PartnerChildOpportunityManagerController.setShowButton(true);
            }
            
            system.debug('## childAccountsById: '+json.serializePretty(childAccountsById));
            result.data.addAll(childAccountsById.values());
            result.FullQueryCount = result.data.size();
            result.pagecount = 1;
            result.isError = false;
            result.ErrorMessage = '';
        }
        catch (Exception e) {
            DebugLog__c log = new DebugLog__c(
                    Origin__c = '['+className+'][getData]',
                    LogType__c = constants.LOGTYPE_ERROR,
                    RelatedToSquad__c = constants.LOGTYPE_SQUAD_PARTNERS,
                    ExceptionMessage__c = e.getMessage(),
                    StackTrace__c = e.getStackTraceString(),
                    ObjectId__c = request.ParentRecordID
            );
            Logger.createLog(log);
        }

        return result;
    }

    global sortablegrid.SDG LoadSDG(String SDGTag, String ParentRecordId)
    {
        sortablegrid.SDG CoreSDG = new sortablegrid.SDG( 'AccountsWithNoOpportunity' );
        CoreSDG.SDGFields = GetFields();
        return CoreSDG;
    }

    private List<sortablegrid.SDGField> GetFields()
    {
        List<sortablegrid.SDGField> fields = new List<sortablegrid.SDGField>();

        fields.add( new sortablegrid.SDGField('1', 'NAME', 'Name', 'STRING', '', false, false, null, 1));

        return fields;
    }


    public class CurrentValidProductsDataProviderException extends Exception {}
}