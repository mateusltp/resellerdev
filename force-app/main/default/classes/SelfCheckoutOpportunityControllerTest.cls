@IsTest
public class SelfCheckoutOpportunityControllerTest {
    
    @IsTest
    public static void postPositiveTest() {
  
      RestRequest request = new RestRequest();
      request.requestURI = '/services/apexrest/self-checkout-opportunity';
      request.httpMethod = 'POST';
      request.requestBody = Blob.valueOf(SelfCheckoutOpportunityRequestMock.getRequestBody());
  
      Test.startTest();
  
      RestContext.response = new RestResponse();
  
      RestContext.request = request;
      RestResponse response = new SelfCheckoutOpportunityController().post();
  
      Test.stopTest();
  
      System.assertEquals(
        204,
        response.statusCode,
        'Error Message: ' + response?.responseBody?.toString()
      );
    }

    @IsTest
    public static void postWithNoBody() {
      RestRequest request = new RestRequest();
      request.requestURI = '/services/apexrest/self-checkout-opportunity';
      request.httpMethod = 'POST';
  
      Test.startTest();
  
      RestContext.response = new RestResponse();
      RestContext.request = request;
  
      RestResponse response = new SelfCheckoutOpportunityController().post();
  
      Test.stopTest();
  
      System.assertEquals(
        400,
        response.statusCode,
        'With no request body the status code must be 400'
      );
    }

    @IsTest
  public static void postWithInvalidBody() {
    RestRequest request = new RestRequest();
    request.requestURI = '/services/apexrest/self-checkout-opportunity';
    request.httpMethod = 'POST';
    request.requestBody = Blob.valueOf('{]');

    Test.startTest();

    RestContext.response = new RestResponse();
    RestContext.request = request;

    RestResponse response = new SelfCheckoutOpportunityController().post();

    Test.stopTest();

    System.assertEquals(
      400,
      response.statusCode,
      'With a invalid request body the status code must be 400'
    );
  }


}