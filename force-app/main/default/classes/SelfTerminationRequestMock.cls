@IsTest
public class SelfTerminationRequestMock {
  public static final String ACCOUNT_UUID = 'tt47c899-7e59-47ad-9ad7-7d0f6310f4ea';
  public static final String ORDER_UUID = 'da26ed65-debb-48b8-bbfb-6c60326761c3';
  public static final String ORDER_UUID_INVALID = 'aaa26ed65-debb-48b8-bbfb-6c60326761c3a';
  public static final String DUE_DATE = '2021-12-02T00:00:00.000Z';
  public static final String OTHER_REASON = 'OTHER';
  public static final String CANCEL = 'CANCEL';
  public static final String REASON_DETAILS_EXAMPLE = 'Other.';

  public static SelfTerminationRequest getMock(String action) {
    String requestJson =
      '{' +
      '"client_id": "' +
      ACCOUNT_UUID +
      '",' +
      '"client_order_id": "' +
      ORDER_UUID +
      '",' +
      '"action": "' +
      action +
      '",' +
      '"due_date": "' +
      DUE_DATE +
      '",' +
      '"reason": "' +
      OTHER_REASON +
      '",' +
      '"reason_details": "' +
      REASON_DETAILS_EXAMPLE +
      '"}';
    return (SelfTerminationRequest) JSON.deserialize(
      requestJson,
      SelfTerminationRequest.class
    );
  }

  public static SelfTerminationRequest getMockMissingParameters() {
    String requestJson = '{' + '"action": "' + 'CANCEL' + '"}';
    return (SelfTerminationRequest) JSON.deserialize(
      requestJson,
      SelfTerminationRequest.class
    );
  }

  public static SelfTerminationRequest getMockOrderDoesntExist() {
    String requestJson =
      '{' +
      '"client_id": "' +
      ACCOUNT_UUID +
      '",' +
      '"client_order_id": "' +
      ORDER_UUID_INVALID +
      '",' +
      '"action": "' +
      CANCEL +
      '",' +
      '"due_date": "' +
      DUE_DATE +
      '",' +
      '"reason": "' +
      OTHER_REASON +
      '",' +
      '"reason_details": "' +
      REASON_DETAILS_EXAMPLE +
      '"}';
    return (SelfTerminationRequest) JSON.deserialize(
      requestJson,
      SelfTerminationRequest.class
    );
  }
}