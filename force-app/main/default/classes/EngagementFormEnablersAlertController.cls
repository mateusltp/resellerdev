/**
 * @description       : 
 * @author            : AlyssonMota
 * @group             : 
 * @last modified on  : 06-11-2021
 * @last modified by  : roei@gft.com
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   11-17-2020   Alysson Mota   Initial Version
**/
public with sharing class EngagementFormEnablersAlertController {
    @AuraEnabled
    public static Response evaluateEnablers(String formId) {
        FormRepository formRepo = new FormRepository();
        Form__c form = formRepo.ById((Id)formId);

        Integer achievedNumber;
        Boolean hasPendingJustification;

        Response response = new Response();
        response.showCmp = false;
        
        if (form.SMB_CS_success_executive__c == false && form.Approval_Status__c != 'Auto Approved' && form.Approval_Status__c != 'Approved By Regional CEO') {
            achievedNumber = getAchievedEnablersNumber(form);
            hasPendingJustification = evalIfHasPendingEnablerJustification(form);

            if (achievedNumber < 6) {
                response.showCmp = true;                        
                response.message = 'There are less than 6 enablers achived. Please, submit the form for enablers approval.\n';
                
                if (hasPendingJustification) {
                    response.message += 'Also be sure that all enablers with \'No\' have been justified in the Launch Scorecard.\n';
                }
            }
        }

        if (form.SMB_CS_success_executive__c == true && (form.Approval_Status__c == 'Needs Approval' || form.Approval_Status__c == 'Rejected By Regional CEO')) {
            response.showCmp = true;                        
            response.message = 'Please, submit the form for enablers approval.\n';
            response.message += 'Also be sure that all enablers with \'No\' have been justified in the Launch Scorecard.\n';
        }

        return response;
    }

    private static Integer getAchievedEnablersNumber(Form__c form) {
        Boolean hasLessThanSixEnablersAchieved = false;
        Integer achievedNumber = 0;

        if (form.Standard_Membership_Fee_GL__c              == 'Yes')   achievedNumber++;
        if (form.Expansion_100_of_the_eligible_GL__c        == 'Yes')   achievedNumber++;
        if (form.Minimum_Access_Fee_GL__c                   == 'Yes')   achievedNumber++;
        if (form.Engagement_with_C_Level_GL__c              == 'Yes')   achievedNumber++;
        if (form.Payroll_GL__c                              == 'Yes')   achievedNumber++;
        if (form.Database_Email_GL__c                       == 'Yes')   achievedNumber++;
        if (form.Whitelist_GL__c                            == 'Yes')   achievedNumber++;
        if (form.New_Hires_GL__c                            == 'Yes')   achievedNumber++;
        if (form.HR_Communication_GL__c                     == 'Yes')   achievedNumber++;
        if (form.Exclusivity_clause_GL__c                   == 'Yes')   achievedNumber++;
        if (form.PR_in_contract_GL__c                       == 'Yes')   achievedNumber++;
        if (form.What_does_success_looks_like_GL_justify__c == 'Yes')   achievedNumber++;

        return achievedNumber;
    }

     private static Boolean evalIfHasPendingEnablerJustification(Form__c form) {
        if (form.Standard_Membership_Fee_GL__c              == 'No' && form.Standard_Membership_Fee_GL_justify__c       == null) return true;
        if (form.Expansion_100_of_the_eligible_GL__c        == 'No' && form.Expansion_100_of_the_eligible_GL_justify__c == null) return true;
        if (form.Minimum_Access_Fee_GL__c                   == 'No' && form.Minimum_Access_Fee_GL_justify__c            == null) return true;
        if (form.Engagement_with_C_Level_GL__c              == 'No' && form.Engagement_with_C_Level_GL_justify__c       == null) return true;
        if (form.Payroll_GL__c                              == 'No' && form.Payroll_GL_justify__c                       == null) return true;
        if (form.Database_Email_GL__c                       == 'No' && form.Database_Email_GL_justify__c                == null) return true;
        if (form.Whitelist_GL__c                            == 'No' && form.Whitelist_GL_justify__c                     == null) return true;
        if (form.New_Hires_GL__c                            == 'No' && form.New_Hires_GL_justify__c                     == null) return true;
        if (form.HR_Communication_GL__c                     == 'No' && form.HR_Communication_GL_justify__c              == null) return true;
        if (form.Exclusivity_clause_GL__c                   == 'No' && form.Exclusivity_clause_GL_justify__c            == null) return true;
        if (form.PR_in_contract_GL__c                       == 'No' && form.PR_in_contract_GL_justify__c                == null) return true;
        if (form.What_does_success_looks_like_GL_justify__c == 'No' && form.What_does_success_looks_like_Joint_BP_GL__c == null) return true;

        return false;
    }

    public class Response {
       @AuraEnabled public Boolean showCmp {get; set;} 
       @AuraEnabled public String message {get; set;}
    }
}