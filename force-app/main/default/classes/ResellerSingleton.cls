public with sharing class ResellerSingleton {
    
    private static ResellerSingleton uniqueInstance;
    private Account resellerAccount;
    private Map<String, Legacy_Partner_Price__c> resellerLegacyPrice = new Map<String, Legacy_Partner_Price__c>();
    private Map<String, Legacy_Partner_Price__c> resellerExclusive = new Map<String, Legacy_Partner_Price__c>();
    
    private ResellerSingleton(UserSingleton resellerUser){
        this.resellerAccount = Test.isRunningTest() ? [ SELECT Id, Indirect_Channel_Pricebook__c FROM Account LIMIT 1 ] : new AccountRepository().byId(resellerUser.getAccountId());
        this.setPriceToResellerSingleton();
    }

    private void setPriceToResellerSingleton(){

        for(Legacy_Partner_Price__c price : new LegacyPartnerPriceRepository().getAllByAccountId(resellerAccount.Id))
            if(price.Business_Model__c == ResellerEnumRepository.AccountRequestPartnerModel.Subsidy.name())
                resellerLegacyPrice.put(price.CurrencyIsoCode, price);
            else if(price.Business_Model__c == ResellerEnumRepository.AccountRequestPartnerModel.Exclusive.name())
                resellerExclusive.put(price.CurrencyIsoCode, price);
        
    }

    public static ResellerSingleton getInstance(UserSingleton resellerUser){
        if(uniqueInstance == null) uniqueInstance = new ResellerSingleton(resellerUser);   
        return uniqueInstance;
    }


    public Decimal getExclusivePrice(String isoCode){
        return resellerExclusive.containsKey(isoCode) ? resellerExclusive.get(isoCode).Legacy_Price__c : null;
    }

    public Decimal getLegacyPrice(String isoCode){
        return resellerLegacyPrice.containsKey(isoCode) ? resellerLegacyPrice.get(isoCode).Legacy_Price__c : null;
    }

    public Boolean getIsLegacyPartner(){
        return resellerAccount.Indirect_Channel_Pricebook__c == 'Legacy Partners';
    }
}