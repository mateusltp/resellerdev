@isTest(isParallel=true)
public class AutoCloseInactiveOppsBatchTest {
    
    @testSetup static void setup() {
        //-- get test accounts
        List<Account> testAccsLst = new List<Account>();
        for(Integer i = 0; i < 15; i++) {
			Account testAccount = DataFactory.newAccount();
            testAccount.Name = 'Test Account - ' + i;
        	testAccount.OwnerId = UserInfo.getUserId();
            testAccsLst.add(testAccount);
        }
		insert testAccsLst;
        
        //-- get a contact
        Contact testContact = new Contact();
        testContact.FirstName = 'teste';
        testContact.LastName = 'testeNovo';
        testContact.AccountId = testAccsLst[0].Id;
        testContact.Email = 'teste@bemarius.example';
        testContact.Role__c = 'FINANCE';
        insert testContact;

     
        //-- get test opportunities
		List<Opportunity> testOppsLst = new List<Opportunity>();
		for(Integer i = 0; i < 15; i++) {
			Opportunity opportunity = new Opportunity();
    		Id oppRtId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Indirect_Channel_New_Business').getRecordTypeId();
    		opportunity.recordTypeId = oppRtId;
    		opportunity.AccountId = testAccsLst[i].id;
    		opportunity.Name = 'teste opp';      
    		opportunity.CloseDate = System.today().addDays(30);
    		opportunity.Sales_Channel__c = 'Direct';
    		opportunity.Type = 'Expansion';     
    		opportunity.Probability = 20;
    		opportunity.FastTrackStage__c = 'Offer Creation';
            opportunity.StageName = 'Qualification'; 
            opportunity.CurrencyIsoCode = 'EUR';
            opportunity.Origin_Process__c = (i < 10) ? 'Multiple' : 'Single';
			testOppsLst.add(opportunity);
            
		}
    
            insert testOppsLst;

        Map<Id, Opportunity> oppsById = new Map<Id, Opportunity>([SELECT Id, CreatedDate, LastModifiedDate, Origin_Process__c FROM Opportunity]);
        //List<Account_Request__c> testAccReqLst = new List<Account_Request__c>();
        Integer index = 0;
        for(Opportunity opp : testOppsLst) {

            /*
            Account_Request__c request = new Account_Request__c();  
        	request.Name = 'teste_' + index;
        	request.Website__c = 'testerequeste.com';
        	request.Billing_Country__c = 'Portugal';
        	request.Existing_In_SalesForce__c = true;
        	request.Is_Gympass_Customer__c = true;
        	request.ContactId__c = testContact.Id;
        	request.None_of_Above__c = false;
        	request.AccountId__c = testAccsLst[index].id;
            request.Bulk_Operation__c = true;
            request.OpportunityId__c = opp.Id;
            testAccReqLst.add(request);
            */
            //-- update the CreatedDate in the opportunity for testing purposes
            if(index > 0 && math.mod(index, 3) == 0) {
        		Test.setCreatedDate(opp.Id, oppsById.get(opp.Id).LastModifiedDate.addMinutes(-6));
            } else if(index > 0 && math.mod(index, 2) == 0) {
                Test.setCreatedDate(opp.Id, Date.today().addDays(-65));
            }
            index++;
        }
		//insert testAccReqLst;
        update testOppsLst;
    }
    
    @isTest static void autoCloseInactiveOppsTest() {
        
        List<Account_Request__c> accReqLst = 
            [SELECT OpportunityId__r.Id, OpportunityId__r.Name, OpportunityId__r.FastTrackStage__c,OpportunityId__r.Converted__c, OpportunityId__r.StageName, OpportunityId__r.LastActivityDate, OpportunityId__r.LastModifiedDate
             FROM Account_Request__c
             WHERE Bulk_Operation__c = true
             AND OpportunityId__r.FastTrackStage__c = 'Offer Creation'
             AND OpportunityId__r.Converted__c = FALSE];
        
        List<Opportunity> oppsLst = [SELECT Id, CreatedDate, LastModifiedDate, RecordType.Name, FastTrackStage__c
                                     FROM Opportunity];
        
        Test.startTest();
        Id batchJobId = Database.executeBatch(new AutoCloseInactiveOppsBatch(), 200);
        Test.stopTest();
        
        oppsLst = [SELECT Id, CreatedDate, LastModifiedDate, RecordType.Name, FastTrackStage__c, StageName, Loss_Reason__c, Loss_Description__c, Origin_Process__c
                   FROM Opportunity];

        List<Opportunity> notUpdatedOppsLst = new List<Opportunity>();
        List<Opportunity> updatedWithin30DaysOppsLst = new List<Opportunity>();
        List<Opportunity> updatedAfter60DaysOppsLst = new List<Opportunity>();
        List<Opportunity> singleUpdatedAfter60DaysOppsLst = new List<Opportunity>();
        for(Opportunity opp : oppsLst) {
            Boolean isSameDay = AutoCloseInactiveOppsBatch.checkIfDatesAreEqual(opp.CreatedDate, opp.LastModifiedDate);
            if(opp.CreatedDate < Date.today().addDays(-60) && opp.Origin_Process__c == 'Multiple'
               && opp.FastTrackStage__c == ResellerEnumRepository.ResellerOpportunityFastTrackStage.Lost.name()) {
                updatedAfter60DaysOppsLst.add(opp);
            } else if(isSameDay && opp.FastTrackStage__c == ResellerEnumRepository.ResellerOpportunityFastTrackStage.Lost.name()) {
                updatedWithin30DaysOppsLst.add(opp);
            } else if(opp.Origin_Process__c == 'Single' && opp.CreatedDate < Date.today().addDays(-60)) {
            	singleUpdatedAfter60DaysOppsLst.add(opp);
            } else {
                notUpdatedOppsLst.add(opp);
            }
        }
        System.assert(notUpdatedOppsLst.size() == 3, 'There should be 3 unchanged records.');
        System.assert(updatedWithin30DaysOppsLst.size() == 7, 'There should be 7 changed records due to inactivity during the first 30 days.');
        System.assert(updatedAfter60DaysOppsLst.size() == 3, 'There should be 3 changed records due to inactivity for more than 60 days.');
        System.assert(singleUpdatedAfter60DaysOppsLst.size() == 2, 'There should be 2 changed records from Single process due to inactivity for more than 60 days.');
    }

}