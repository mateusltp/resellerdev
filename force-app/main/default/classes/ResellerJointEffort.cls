public with sharing class ResellerJointEffort{

    private Account_Request__c accountRequest{get;set;}

    public ResellerJointEffort(Account_Request__c accountRequest) {
        this.accountRequest = accountRequest;
    }

    public Boolean isJointEffort(){

        if(this.isIndirectChannelOpenOpp())return false;
        else if(this.isPublicSector() && !this.isExistingInSalesForce())return true;
        else if(this.isPublicSector() && this.isExistingInSalesForce() && !this.isGympassCustomer())return true;
        else if(this.isPublicSector() && this.isExistingInSalesForce() && this.isGympassCustomer())return false;

        else if(!this.isPublicSector() && !this.isExistingInSalesForce())return false;
        else if(!this.isPublicSector() && this.isExistingInSalesForce() && this.isGympassCustomer())return false;
        else if(!this.isPublicSector() && this.isExistingInSalesForce() && !this.isGympassCustomer() && this.isAbmProspect())return false;
        else if(!this.isPublicSector() && this.isExistingInSalesForce() && !this.isGympassCustomer() && !this.isAbmProspect() && !this.isDirectChannelOpenOpp() && this.lastActivityLessEqual90())return true;
        else if(!this.isPublicSector() && this.isExistingInSalesForce() && !this.isGympassCustomer() && !this.isAbmProspect() && !this.isDirectChannelOpenOpp() && this.lastActivityMore90())return false;

        else if(!this.isPublicSector() && this.isExistingInSalesForce() && !this.isGympassCustomer() && !this.isAbmProspect() && this.isDirectChannelOpenOpp() && this.inOfferApprovedStatus_or_inContractStatus_or_inSignedContractStatus())return false;

        else if(!this.isPublicSector() && this.isExistingInSalesForce() && !this.isGympassCustomer() && !this.isAbmProspect() && this.isDirectChannelOpenOpp() && this.inOfferSentStatus() && this.isBid())return true;
        else if(!this.isPublicSector() && this.isExistingInSalesForce() && !this.isGympassCustomer() && !this.isAbmProspect() && this.isDirectChannelOpenOpp() && this.inOfferSentStatus() && this.lastActivityMore60())return true;
        else if(!this.isPublicSector() && this.isExistingInSalesForce() && !this.isGympassCustomer() && !this.isAbmProspect() && this.isDirectChannelOpenOpp() && this.inOfferSentStatus() && this.lastActivitylessEqual60())return false;

        
        else if(!this.isPublicSector() && this.isExistingInSalesForce() && !this.isGympassCustomer() && !this.isAbmProspect() && this.isDirectChannelOpenOpp() && this.inValidatedStatus() && this.isBid())return true;
        else if(!this.isPublicSector() && this.isExistingInSalesForce() && !this.isGympassCustomer() && !this.isAbmProspect() && this.isDirectChannelOpenOpp() && this.inValidatedStatus() && !this.isBid())return false;
        
        else if(!this.isPublicSector() && this.isExistingInSalesForce() && !this.isGympassCustomer() && !this.isAbmProspect() && this.isDirectChannelOpenOpp() && this.inQualificationStatus() && this.isBid())return true;
        else if(!this.isPublicSector() && this.isExistingInSalesForce() && !this.isGympassCustomer() && !this.isAbmProspect() && this.isDirectChannelOpenOpp() && this.inQualificationStatus() && !this.isBid())return false;

        return false;

    }

    private Boolean isPublicSector(){
        return this.accountRequest.Public_Sector__c == true;
    }

    private Boolean isExistingInSalesForce(){
        return this.accountRequest.Existing_In_SalesForce__c == true;
    }

    private Boolean isGympassCustomer(){
        return this.accountRequest.Is_Gympass_Customer__c == true;
    }

    private Boolean isAbmProspect(){
        return this.accountRequest.ABM_Prospect__c == true;
    }

    private Boolean isDirectChannelOpenOpp(){
        return this.accountRequest.Direct_Channel_Open_Opp__c == true;
    }

    private Boolean isIndirectChannelOpenOpp(){
        return this.accountRequest.Indirect_Channel_Open_Opp__c == true;
    }

    @TestVisible
    private Boolean hasOpenOpp(){
        return this.isDirectChannelOpenOpp() == true && this.isIndirectChannelOpenOpp() == true;
    }

    private Boolean isBid(){
        return this.accountRequest.BID__c == 'Yes';
    }

    @TestVisible
    private Boolean isSubsidy(){
        return this.accountRequest.Partner_Model__c == ResellerEnumRepository.AccountRequestPartnerModel.Subsidy.name();
    }

    @TestVisible
    private Boolean isIntermediation(){
        return this.accountRequest.Partner_Model__c == ResellerEnumRepository.AccountRequestPartnerModel.Intermediation.name();
    }

    @TestVisible
    private Boolean lastActivityLessEqual90(){
        return ResellerActivity.lastActivityLessEqual90(this.accountRequest);
    }

    @TestVisible
    private Boolean lastActivityMore90(){
        return ResellerActivity.lastActivityMore90(this.accountRequest);
    }

    @TestVisible
    private Boolean lastActivityLessEqual60(){
        return ResellerActivity.lastActivityLessEqual60(this.accountRequest);
    }

    @TestVisible
    private Boolean lastActivityMore60(){
        return ResellerActivity.lastActivityMore60(this.accountRequest);
    }

    @TestVisible
    private Boolean lastActivityLessEqual30(){
        return ResellerActivity.lastActivityLessEqual30(this.accountRequest);
    }

    @TestVisible
    private Boolean lastActivityMore30(){
        return ResellerActivity.lastActivityMore30(this.accountRequest);
    }

    private Boolean inOfferApprovedStatus_or_inContractStatus_or_inSignedContractStatus(){
        return this.inOfferApprovedStatus() == true || this.inContractStatus() == true || this.inSignedContractStatus() == true;
    }

    private Boolean inOfferApprovedStatus(){
        return this.accountRequest.Current_Stage_In_Open_Opp__c == 'Proposta Aprovada' || this.accountRequest.Current_Stage_In_Open_Opp__c == 'Offer Approved';
    }

    private Boolean inContractStatus(){
        return this.accountRequest.Current_Stage_In_Open_Opp__c == 'Contract';
    }

    private Boolean inSignedContractStatus(){
        return this.accountRequest.Current_Stage_In_Open_Opp__c == 'Contrato Assinado' || this.accountRequest.Current_Stage_In_Open_Opp__c == 'Signed Contract';
    }

    private Boolean inOfferSentStatus(){
        return this.accountRequest.Current_Stage_In_Open_Opp__c == 'Offer Sent' || this.accountRequest.Current_Stage_In_Open_Opp__c == 'Proposta Enviada';
    }

    private Boolean inValidatedStatus(){
        return this.accountRequest.Current_Stage_In_Open_Opp__c == 'Validated' || this.accountRequest.Current_Stage_In_Open_Opp__c == 'Oportunidade Validada Após 1ª Reunião';
    }

    private Boolean inQualificationStatus(){
        return this.accountRequest.Current_Stage_In_Open_Opp__c == 'Qualificação' || this.accountRequest.Current_Stage_In_Open_Opp__c == 'Qualification';
    }

}