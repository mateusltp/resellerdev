/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 04-29-2022
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/
@IsTest
public with sharing class PartnerSetupControllerTest {


    @TestSetup
    static void setupData(){
        
        PS_Constants constants = PS_Constants.getInstance();  
        String recordTypePartnerFlow = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(constants.ACCOUNT_RT_PARTNER_FLOW_ACCOUNT).getRecordTypeId();
        Account partnerAcc = PartnerDataFactory.newAccount();
        partnerAcc.RecordTypeId = recordTypePartnerFlow;
        partnerAcc.CRM_ID__c = '1231';
        partnerAcc.Legal_Document_Type__c = 'CNPJ';
        partnerAcc.Legal_Title__c = 'PartnerSetup';
        partnerAcc.BillingCountryCode = 'US';
        partnerAcc.BillingCountry = 'United States';
        partnerAcc.BillingState = 'Arizona';
        partnerAcc.BillingPostalCode = '1223';
        partnerAcc.ShippingCountryCode = 'US';
        partnerAcc.ShippingCountry = 'United States';
        partnerAcc.ShippingState = 'Arizona';
        partnerAcc.CurrencyIsoCode = 'USD';
        partnerAcc.Description = 'dasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasda';
        INSERT partnerAcc;

        System.debug('### 1 account insert.');
        System.debug('### queries: '+Limits.getQueries());
        System.debug('### cpu: '+Limits.getCpuTime());
        System.debug('### dml: '+Limits.getDmlStatements());
        System.debug('### ---------------------------------------------------------------------------- ###');

        List<ContentVersion> lstContentVersion = new List<ContentVersion>();
        ContentVersion contentVersion_1 = new ContentVersion(
            Title = 'photo 1',
            PathOnClient = '/logoPenguins1.jpg',
            VersionData = Blob.valueOf('Unit Test PartnerSetupController Body 1'),
            origin = 'H',
            Type_Files_fileupload__c = 'Photo'    
        );
        lstContentVersion.add(contentVersion_1);

        ContentVersion contentVersion_2 = new ContentVersion(
            Title = 'photo 2',
            PathOnClient = '/logoPenguins2.jpg',
            VersionData = Blob.valueOf('Unit Test PartnerSetupController Body 2'),
            origin = 'H',
            Type_Files_fileupload__c = 'Photo'    
        );
        lstContentVersion.add(contentVersion_2);

        ContentVersion contentVersion_3 = new ContentVersion(
            Title = 'photo 3',
            PathOnClient = '/logoPenguins3.jpg',
            VersionData = Blob.valueOf('Unit Test PartnerSetupController Body 3'),
            origin = 'H',
            Type_Files_fileupload__c = 'Photo'    
        );
        lstContentVersion.add(contentVersion_3);

        ContentVersion contentVersion_4 = new ContentVersion(
            Title = 'LOGO',
            PathOnClient = '/gymlogo.jpg',
            VersionData = Blob.valueOf('Unit Test PartnerSetupController Logo'),
            origin = 'H',
            Type_Files_fileupload__c = 'Logo'    
        );

        lstContentVersion.add(contentVersion_4);

        ContentVersion contentVersion_5 = new ContentVersion(
            Title = 'W9',
            PathOnClient = '/W9.pdf',
            VersionData = Blob.valueOf('Unit Test PartnerSetupController w9'),
            origin = 'H',
            Type_Files_fileupload__c = 'W9'    
        );

        lstContentVersion.add(contentVersion_5);
        INSERT lstContentVersion;

        System.debug('### 1 lista 5 contentversions insert.');
        System.debug('### queries: '+Limits.getQueries());
        System.debug('### cpu: '+Limits.getCpuTime());
        System.debug('### dml: '+Limits.getDmlStatements());
        System.debug('### ---------------------------------------------------------------------------- ###');


        Id ContentDocumentId_1 = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: contentVersion_1.Id].ContentDocumentId;
        Id ContentDocumentId_2 = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: contentVersion_2.Id].ContentDocumentId;
        Id ContentDocumentId_3 = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: contentVersion_3.Id].ContentDocumentId;
        Id ContentDocumentId_4 = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: contentVersion_4.Id].ContentDocumentId;
        Id ContentDocumentId_5 = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: contentVersion_5.Id].ContentDocumentId;

        List<ContentDocumentLink> lstContentLink = new List<ContentDocumentLink>();
        ContentDocumentLink ContentDLAcc1 = new ContentDocumentLink();
        ContentDLAcc1.ContentDocumentId = ContentDocumentId_1;
        ContentDLAcc1.LinkedEntityId = partnerAcc.Id;
        lstContentLink.add(ContentDLAcc1);

        /*ContentDocumentLink ContentDLAcc2 = new ContentDocumentLink();
        ContentDLAcc2.ContentDocumentId = ContentDocumentId_2;
        ContentDLAcc2.LinkedEntityId = partnerAcc.Id;
        lstContentLink.add(ContentDLAcc2);

        ContentDocumentLink ContentDLAcc3 = new ContentDocumentLink();
        ContentDLAcc3.ContentDocumentId = ContentDocumentId_3;
        ContentDLAcc3.LinkedEntityId = partnerAcc.Id;
        lstContentLink.add(ContentDLAcc3);
       
        ContentDocumentLink ContentDLAcc4 = new ContentDocumentLink();
        ContentDLAcc4.ContentDocumentId = ContentDocumentId_4;
        ContentDLAcc4.LinkedEntityId = partnerAcc.Id;
        lstContentLink.add(ContentDLAcc4);

        ContentDocumentLink ContentDLAcc5 = new ContentDocumentLink();
        ContentDLAcc5.ContentDocumentId = ContentDocumentId_5;
        ContentDLAcc5.LinkedEntityId = partnerAcc.Id;
        lstContentLink.add(ContentDLAcc5);*/

        Account childAcc = PartnerDataFactory.newChildAccount(partnerAcc.Id);
        childAcc.RecordTypeId = recordTypePartnerFlow;
        childAcc.CRM_ID__c = '4455';
        childAcc.Legal_Document_Type__c = 'CNPJ';
        childAcc.Partner_Level__c = 'Location';
        childAcc.Legal_Title__c = 'PartnerSetup';
        childAcc.BillingCountryCode = 'US';
        childAcc.BillingCountry = 'United States';
        childAcc.BillingState = 'Arizona';
        childAcc.BillingPostalCode = '1223';
        childAcc.ShippingCountryCode = 'US';
        childAcc.ShippingCountry = 'United States';
        childAcc.ShippingState = 'Arizona';
        partnerAcc.CurrencyIsoCode = 'USD';
        childAcc.Description = 'dasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasda';
        INSERT childAcc;

        System.debug('### 1 account insert.');
        System.debug('### queries: '+Limits.getQueries());
        System.debug('### cpu: '+Limits.getCpuTime());
        System.debug('### dml: '+Limits.getDmlStatements());
        System.debug('### ---------------------------------------------------------------------------- ###');


        ContentDocumentLink ContentDLChild1 = new ContentDocumentLink();
        ContentDLChild1.ContentDocumentId = ContentDocumentId_1;
        ContentDLChild1.LinkedEntityId = childAcc.Id;
        lstContentLink.add(ContentDLChild1);

        /*ContentDocumentLink ContentDLChild2 = new ContentDocumentLink();
        ContentDLChild2.ContentDocumentId = ContentDocumentId_2;
        ContentDLChild2.LinkedEntityId = childAcc.Id;
        lstContentLink.add(ContentDLChild2);

        ContentDocumentLink ContentDLChild3 = new ContentDocumentLink();
        ContentDLChild3.ContentDocumentId = ContentDocumentId_3;
        ContentDLChild3.LinkedEntityId = childAcc.Id;
        lstContentLink.add(ContentDLChild3);

        ContentDocumentLink ContentDLChild4 = new ContentDocumentLink();
        ContentDLChild4.ContentDocumentId = ContentDocumentId_4;
        ContentDLChild4.LinkedEntityId = childAcc.Id;
        lstContentLink.add(ContentDLChild4);

        ContentDocumentLink ContentDLChild5 = new ContentDocumentLink();
        ContentDLChild5.ContentDocumentId = ContentDocumentId_5;
        ContentDLChild5.LinkedEntityId = childAcc.Id;
        lstContentLink.add(ContentDLChild5);*/

        INSERT lstContentLink;

        System.debug('### 1 lista 2 contentdoclinks (isto processa trigger 1 a 1) insert.');
        System.debug('### queries: '+Limits.getQueries());
        System.debug('### cpu: '+Limits.getCpuTime());
        System.debug('### dml: '+Limits.getDmlStatements());
        System.debug('### ---------------------------------------------------------------------------- ###');

        String recordTypePartnerContactFlow = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Contact').getRecordTypeId();
        Contact partnerContact = PartnerDataFactory.newContact(partnerAcc.Id);
        partnerContact.RecordTypeId = recordTypePartnerContactFlow;        
        partnerContact.Type_of_Contact__c = 'Legal Representative';
        partnerContact.FirstName = 'Admin';
        partnerContact.UUID__c = new Uuid().getValue();
        INSERT partnerContact;

        System.debug('### 1 contact insert.');
        System.debug('### queries: '+Limits.getQueries());
        System.debug('### cpu: '+Limits.getCpuTime());
        System.debug('### dml: '+Limits.getDmlStatements());
        System.debug('### ---------------------------------------------------------------------------- ###');

        partnerAcc.Legal_Representative__c = partnerContact.Id;
        UPDATE partnerContact;

        System.debug('### 1 contact update.');
        System.debug('### queries: '+Limits.getQueries());
        System.debug('### cpu: '+Limits.getCpuTime());
        System.debug('### dml: '+Limits.getDmlStatements());
        System.debug('### ---------------------------------------------------------------------------- ###');

        Opportunity aOpp = PartnerDataFactory.newOpportunity( partnerAcc.Id, 'Partner_Flow_Opportunity'); 
        aOpp.Name = 'PartnerSetup';
        aOpp.CurrencyIsoCode = 'USD';
        INSERT aOpp;

        System.debug('### 1 opportunity insert.');
        System.debug('### queries: '+Limits.getQueries());
        System.debug('### cpu: '+Limits.getCpuTime());
        System.debug('### dml: '+Limits.getDmlStatements());
        System.debug('### ---------------------------------------------------------------------------- ###');

        APXT_Redlining__Contract_Agreement__c contractAgreementPartner = PartnerDataFactory.newContractAgreement(aOpp,  partnerAcc, 'Hybrid Contract');
        contractAgreementPartner.APXT_Redlining__Status__c = 'Active';
        contractAgreementPartner.Opportunity__c = aOpp.Id;
        contractAgreementPartner.APXT_Amendment_Version__c = 0;
        contractAgreementPartner.Conga_Sign_Status__c = 'DRAFT';
        contractAgreementPartner.Include_full_T_C_s__c = true;
        contractAgreementPartner.Include_signature__c = false;
        contractAgreementPartner.Legal_Contract_Status__c= 'Contract Draft';
        contractAgreementPartner.APXT_Renegotiation__c = false;
        contractAgreementPartner.APXT_Redlining__Status__c = 'In Process';
        contractAgreementPartner.Template_Selection__c = 'USA';
        contractAgreementPartner.RecordTypeId = Schema.SObjectType.APXT_Redlining__Contract_Agreement__c.getRecordTypeInfosByDeveloperName().get('Partner_Contract').getRecordTypeId();
        
        INSERT contractAgreementPartner;

        System.debug('### queries: '+Limits.getQueries());
        System.debug('### cpu: '+Limits.getCpuTime());
        System.debug('### dml: '+Limits.getDmlStatements());
        System.debug('### ---------------------------------------------------------------------------- ###');
        
        Account_Contract_Agreement_Relationship__c accContractAgreemRel = new Account_Contract_Agreement_Relationship__c();
        accContractAgreemRel.AccountId__c = partnerAcc.Id;
        accContractAgreemRel.ContractAgreementId__c = contractAgreementPartner.Id;
   

        Account_Contract_Agreement_Relationship__c accContractAgreemRelChild = new Account_Contract_Agreement_Relationship__c();
        accContractAgreemRelChild.AccountId__c = childAcc.Id;
        accContractAgreemRelChild.ContractAgreementId__c = contractAgreementPartner.Id;
   
        List<Account_Contract_Agreement_Relationship__c> accContractRelLst = new List<Account_Contract_Agreement_Relationship__c>();
        accContractRelLst.add(accContractAgreemRel);
        accContractRelLst.add(accContractAgreemRelChild);
        INSERT accContractRelLst;

        System.debug('### queries: '+Limits.getQueries());
        System.debug('### cpu: '+Limits.getCpuTime());
        System.debug('### dml: '+Limits.getDmlStatements());
        System.debug('### ---------------------------------------------------------------------------- ###');

        Account_Opportunity_Relationship__c oppMember = PartnerDataFactory.newAcctOppRel(partnerAcc.Id, aOpp.Id);
        oppMember.RecordTypeId = Schema.SObjectType.Account_Opportunity_Relationship__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Opportunity_Member').getRecordTypeId();
        INSERT oppMember;

        System.debug('### queries: '+Limits.getQueries());
        System.debug('### cpu: '+Limits.getCpuTime());
        System.debug('### dml: '+Limits.getDmlStatements());
        System.debug('### ---------------------------------------------------------------------------- ###');

        Account_Opportunity_Relationship__c oppMemberChild = PartnerDataFactory.newAcctOppRel(childAcc.Id, aOpp.Id);
        oppMemberChild.RecordTypeId = Schema.SObjectType.Account_Opportunity_Relationship__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Opportunity_Member').getRecordTypeId();
        INSERT oppMemberChild;

        System.debug('### queries: '+Limits.getQueries());
        System.debug('### cpu: '+Limits.getCpuTime());
        System.debug('### dml: '+Limits.getDmlStatements());
        System.debug('### ---------------------------------------------------------------------------- ###');

        Id bankAccountRtId =  Schema.SObjectType.Bank_Account__c.getRecordTypeInfosByDeveloperName().get(constants.BANK_ACCOUNT_PARTNER_RT).getRecordTypeId();
        Id bankAccountRelRtId =  Schema.SObjectType.Acount_Bank_Account_Relationship__c.getRecordTypeInfosByDeveloperName().get(constants.BANK_ACCOUNT_REL_PARTNER_RT).getRecordTypeId();
    
        Bank_Account__c aBankAcc = PartnerDataFactory.newBankAcct();
        aBankAcc.recordTypeId = bankAccountRtId;
        INSERT aBankAcc;

        System.debug('### queries: '+Limits.getQueries());
        System.debug('### cpu: '+Limits.getCpuTime());
        System.debug('### dml: '+Limits.getDmlStatements());
        System.debug('### ---------------------------------------------------------------------------- ###');

        Acount_Bank_Account_Relationship__c aBankAccRel = PartnerDataFactory.newBankAcctRel(partnerAcc.Id, aBankAcc);
        aBankAccRel.recordTypeId = bankAccountRelRtId;
        INSERT aBankAccRel;

        System.debug('### queries: '+Limits.getQueries());
        System.debug('### cpu: '+Limits.getCpuTime());
        System.debug('### dml: '+Limits.getDmlStatements());
        System.debug('### ---------------------------------------------------------------------------- ###');

        List<Opening_Hours__c> openHoursLst = new List<Opening_Hours__c>();
        Opening_Hours__c openHours = PartnerDataFactory.newOpeningHours(partnerAcc.Id);
        System.debug('::openHours ' + openHours);
        openHoursLst.add(openHours);
        Opening_Hours__c openHours2 = PartnerDataFactory.newOpeningHours(childAcc.Id);
        openHoursLst.add(openHours2);
        INSERT openHoursLst;

        System.debug('### queries: '+Limits.getQueries());
        System.debug('### cpu: '+Limits.getCpuTime());
        System.debug('### dml: '+Limits.getDmlStatements());
        System.debug('### ---------------------------------------------------------------------------- ###');

    }

    @IsTest
    private static void createOpsFormForContractAccounts_Success(){
       
        Test.startTest();
            Opportunity opp = [SELECT ID FROM Opportunity WHERE Name LIKE '%PartnerSetup' LIMIT 1];
            PartnerSetupController.createOpsFormForContractAccounts(opp.Id); 
        Test.stopTest();
    }

    @IsTest
    private static void getAccountsFromContractAndSaveRecord_Success(){
   
        Test.startTest();
            Opportunity opp = [SELECT ID FROM Opportunity WHERE Name LIKE '%PartnerSetup' LIMIT 1];
            List<PartnerSetupController.SetupPartner> lstPartner = PartnerSetupController.getAccountsFromContract(opp.Id);   
            String partnerInput = JSON.serialize(lstPartner);
            PartnerSetupController.saveAccountRecords(partnerInput); 
            //String recordString = JSON.serialize(objectToSerialize)(PartnerSetupController.getAccountsFromContract(opp.Id));    
        Test.stopTest();
    }

    @IsTest
    private static void getAccountsFromContractAndSaveRecord_Error(){
   
        Test.startTest(); 
            try{
                String partnerInput = 'INVALID STRING JSON';
                PartnerSetupController.saveAccountRecords(partnerInput); 
            }catch(Exception e){
                System.assert(e instanceof PartnerSetupController.PartnerSetupControllerException);
            }          
            //String recordString = JSON.serialize(objectToSerialize)(PartnerSetupController.getAccountsFromContract(opp.Id));    
        Test.stopTest();
    }

    
    @IsTest
    private static void deleteFiles_Success(){
     
        Test.startTest();
            Opportunity opp = [SELECT ID FROM Opportunity WHERE Name LIKE '%PartnerSetup' LIMIT 1];
            PartnerSetupController.SetupPartner setupPartner = PartnerSetupController.getAccountsFromContract(opp.Id)[0];   
            String recordString = JSON.serialize(setupPartner.partnerPhotos);
            PartnerSetupController.deleteFiles(recordString, true); 
            //String recordString = JSON.serialize(objectToSerialize)(PartnerSetupController.getAccountsFromContract(opp.Id));    
        Test.stopTest();
    }

        
    @IsTest
    private static void deleteFiles_Error(){
     
        Test.startTest();        
            try{
                String recordString = 'INVALID STRING JSON';
                PartnerSetupController.deleteFiles(recordString, true); 
            }catch(Exception e){
                System.assert(e instanceof PartnerSetupController.PartnerSetupControllerException);
            }        
        Test.stopTest();

        
    }

    @IsTest
    private static void updatePartnerFromSetup_Success(){
      
        Test.startTest();   
            Opportunity opp = [SELECT ID FROM Opportunity WHERE Name LIKE '%PartnerSetup' LIMIT 1];
            List<Id> accIds = new List<Id>();
            List<Account> accLst = [ SELECT ID, Name,Gym_Email__c  FROM Account WHERE Legal_Title__c LIKE 'PartnerSetup'];
            for(Account aAcc : accLst ){
                accIds.add(aAcc.Id);
            }
            List<Bank_Account__c> bankAccLst = [SELECT ID, Name,Bank_Account_Ownership__c FROM Bank_Account__c WHERE Bank_Account_Ownership__c='Test BankAcct'];
            List<Opening_Hours__c> openingHoursLst = [SELECT ID, Account_Related__c,Open_1__c,Close_1__c,Days__c  FROM Opening_Hours__c WHERE Account_Related__c IN: accIds ];      
            List<PartnerSetupController.SetupPartner> lstPartner = PartnerSetupController.getAccountsFromContract( opp.Id );
            List<PartnerSetupController.SetupFile> files = new List<PartnerSetupController.SetupFile>();

            ContentVersion newContentVersion = new ContentVersion(
                Title = 'photo new 2',
                PathOnClient = '/newfilephoto2444.jpg',
                VersionData = Blob.valueOf('Unit Test PartnerSetupController newfilephoto2444'),
                origin = 'H',
                Type_Files_fileupload__c = 'Photo'    
            );

            INSERT newContentVersion;
            newContentVersion = [SELECT ID, Title,Type_Files_fileupload__c, ContentDocumentId FROM ContentVersion WHERE Id =: newContentVersion.Id];
            
            PartnerSetupController.SetupFile cFile = new PartnerSetupController.SetupFile();
            cFile.name = newContentVersion.Title;
            cFile.contentVersionId = newContentVersion.Id;
            cFile.fileType = newContentVersion.Type_Files_fileupload__c;
            cFile.documentId = newContentVersion.ContentDocumentId;
            cFile.mimeType = 'JPG';
            cFile.isInsert = false;
            files.add(cFile);

            String recordString = JSON.serialize( getUpdatedPartnerRecord( accLst[0] , bankAccLst[0], openingHoursLst[0], files ));
            PartnerSetupController.updatePartnerFromSetup(recordString, accIds);    
        Test.stopTest();
    }

    @IsTest
    private static void updatePartnerFromSetup_Error(){
      
        Test.startTest();   
            try{
                List<Id> accIds = new List<Id>();
                String recordString = 'INVALID STRING JSON';
                PartnerSetupController.updatePartnerFromSetup(recordString, accIds);  
            }catch(Exception e){
                System.assert(e instanceof PartnerSetupController.PartnerSetupControllerException);
            }     
            
         
        Test.stopTest();
    }


    @IsTest
    private static void updatePartnerFromSetup_SingleAccount_Success(){

        Test.startTest(); 
            Opportunity opp = [SELECT ID FROM Opportunity WHERE Name LIKE '%PartnerSetup' LIMIT 1];
            List<Id> accIds = new List<Id>();
            List<Account> accLst = [ SELECT ID, Name,Gym_Email__c  FROM Account WHERE Legal_Title__c LIKE 'PartnerSetup'];
            for(Account aAcc : accLst ){
                accIds.add(aAcc.Id);
            }
            List<Bank_Account__c> bankAccLst = [SELECT ID, Name,Bank_Account_Ownership__c FROM Bank_Account__c WHERE Bank_Account_Ownership__c='Test BankAcct'];
            List<Opening_Hours__c> openingHoursLst = [SELECT ID, Account_Related__c,Open_1__c,Close_1__c,Days__c  FROM Opening_Hours__c WHERE Account_Related__c IN: accIds ];       
            List<PartnerSetupController.SetupPartner> lstPartner = PartnerSetupController.getAccountsFromContract( opp.Id );
            List<PartnerSetupController.SetupFile> files = new List<PartnerSetupController.SetupFile>();
            ContentVersion newContentVersion = new ContentVersion(
                Title = 'photo new',
                PathOnClient = '/newfilephoto.jpg',
                VersionData = Blob.valueOf('Unit Test PartnerSetupController new photo'),
                origin = 'H',
                Type_Files_fileupload__c = 'Photo'
            );
            INSERT newContentVersion;

            newContentVersion = [SELECT ID, Title,Type_Files_fileupload__c, ContentDocumentId FROM ContentVersion WHERE Id =: newContentVersion.Id];
            for(Id iAcc : accIds){
                PartnerSetupController.SetupFile cFile = new PartnerSetupController.SetupFile();
                cFile.name = newContentVersion.Title;
                cFile.contentVersionId = newContentVersion.Id;
                cFile.fileType = newContentVersion.Type_Files_fileupload__c;
                cFile.documentId = newContentVersion.ContentDocumentId;
                cFile.mimeType = 'JPG';
                cFile.entityId = iAcc;
                cFile.isInsert = true;
                files.add(cFile);
            }
            String recordString = JSON.serialize( getUpdatedPartnerRecord( accLst[0] , bankAccLst[0], openingHoursLst[0], files ));
            PartnerSetupController.updatePartnerFromSetup(recordString,  new List<Id>());    
        Test.stopTest();
    }


    private static PartnerSetupController.UpdatedPartner getUpdatedPartnerRecord(Account acc, Bank_Account__c bankacc, Opening_Hours__c openHours, List<PartnerSetupController.SetupFile> files ) {
        PartnerSetupController.UpdatedPartner updPartner = new PartnerSetupController.UpdatedPartner();
        updPartner.partner = new PartnerSetupController.PartnerDetail();
        updPartner.partner.fields = acc;
        updPartner.partner.apiName = 'Account';
        updPartner.bankAccount = new PartnerSetupController.BankAccountDetail();
        updPartner.bankAccount.fields = bankacc;
        updPartner.openingHours = new PartnerSetupController.OpeningHoursDetail();
        updPartner.openingHours.fields = openHours;
        updPartner.files = new List<PartnerSetupController.SetupFile>();
        updPartner.files = files;
        return updPartner;
    }

}