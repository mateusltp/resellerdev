public with sharing class customLookupController {
    // @AuraEnabled(cacheable=true)
    // public static List<sObject> search(String searchTerm, string myObject, String filter) {
    //     String myQuery = null;
    //     if(filter != null && filter != ''){
    //         myQuery = 'Select Id, Name from '+myObject+' Where Name Like  \'%' + searchTerm + '%\' AND '+filter+' LIMIT  5';
    //     }
    //     else {
    //         if(searchTerm == null || searchTerm == ''){
    //             myQuery = 'Select Id, Name from '+myObject+' Where LastViewedDate != NULL ORDER BY LastViewedDate DESC LIMIT  5';
    //         }
    //         else {
    //             myQuery = 'Select Id, Name from '+myObject+' Where Name Like  \'%' + searchTerm + '%\' LIMIT  5';
    //         }
    //     }
    //     List<sObject> lookUpList = database.query(myQuery);
    //     return lookUpList;
    // }

    @AuraEnabled(cacheable=true)
    public static List<sObject> search(String searchTerm, string myObject, String filter) {
        List<List<SObject>> retorno = [FIND :searchTerm IN NAME FIELDS RETURNING Account(Id,Name)];

        return retorno[0];

    }
}