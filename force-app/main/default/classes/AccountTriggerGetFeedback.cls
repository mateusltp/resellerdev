/**
* @File Name          : AccountTriggerGetFeedback.cls
* @Description        :
* @Author             : JRDL@GFT.com
* @Group              :
* @Last Modified By   : gft.jorge.stevaux@ext.gympass.com
* @Last Modified On   : 09-02-2021
* @Modification Log   :
* Ver       Date            Author      		    Modification
* 1.0    04/06/2021   JRDL@GFT.com     Initial Version
**/
public class AccountTriggerGetFeedback {
    
    Set<Id> accIds = new Set<Id>();
    List<Contact> lstUpdateCon = new List<Contact>();
    Map<Id,Contact> lMapContactEvent = new Map<Id,Contact>();
    
    //Get RecordTypeId for Clients and Partners
    Id rtDirectChannel = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
    Id rtIndirectChannel = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Reseller').getRecordTypeId();
    
    public void accountChangeClient(List<Account> lstNew,Map<Id,Account> mapOld){
        for(Account acc: lstNew){
            if(acc.RecordTypeId == rtDirectChannel || acc.RecordTypeId == rtIndirectChannel){
                if(acc.Type == 'Client' &&
                   mapOld.get(acc.Id).Type == 'Prospect' &&
                   acc.Type != mapOld.get(acc.Id).Type){
                       accIds.add(acc.Id);
                   }
            }
        }
        if(!accIds.isEmpty()){
            for(Contact con: [Select Id, Status_do_contato__c, GFSendSurvey__c, Email, Primary_HR_Contact__c, DecisionMaker__c FROM Contact where AccountID IN :accIds]){
                if(con.Status_do_contato__c == 'Ativo' && con.Email != null && con.Primary_HR_Contact__c == TRUE){
                    con.GFSendSurvey__c = 'Clients CSAT - Sales Experience';
                    lstUpdateCon.add(con); 
                }
                else if (con.Status_do_contato__c == 'Ativo' && con.Email != null && con.DecisionMaker__c == 'Yes'){
                    con.GFSendSurvey__c = 'Clients CSAT - Sales Experience';
                    lMapContactEvent.put(con.Id, con);
                }
            }
        }
        
        if(!lMapContactEvent.isEmpty()){
            for(Event ev : [SELECT Id, WhoId, Who.Name,Realizado__c, RecordType.Name FROM Event Where Subject='Meeting' AND WhoId IN: lMapContactEvent.KeySet() AND Realizado__c = 'Yes' AND ActivityDate <= LAST_N_DAYS:90]){
                lstUpdateCon.add(lMapContactEvent.get(ev.whoId));
            }
        }
        
        if(!lstUpdateCon.isEmpty()){
            Map<Id, Contact> mapConToUpdate = new Map<Id, Contact>();
            mapConToUpdate.putAll(lstUpdateCon);
            if (!mapConToUpdate.isEmpty()) {
                update mapConToUpdate.values();
            }
        }
    }
}