public with sharing class ResellerEJContactLookupController {
    
    private final static Integer MAX_RESULTS = 5;

    @AuraEnabled(cacheable=true)
    public static List<Contact> fetchDefaultValue(String recordId) {
        // System.debug('### fetchDefaultValue ### recordId --> ' + recordId);
        List<Engagement_Journey_Contact_Assignment__c> assignemts = [SELECT Id, Contact__c 
                                                                     FROM Engagement_Journey_Contact_Assignment__c
                                                                     WHERE Engagement_Journey_Form__c = :recordId];

        Set<String> contactsIdsSet = new Set<String>();
        for(Engagement_Journey_Contact_Assignment__c assignment : assignemts) {
            contactsIdsSet.add(assignment.Contact__c);
        }

        String sQuery =  'SELECT Id, Name, Email FROM Contact WHERE (Id IN :contactsIdsSet) ORDER BY Name ASC';
        
        // System.debug('### fetchDefaultValue ### sQuery --> ' + sQuery);
        // System.debug(Database.query(sQuery));
        return Database.query(sQuery);
    }

    @AuraEnabled(cacheable=true)
    public static List<Contact> accountRelatedContactsSearch(
        String searchKeyWord, 
        String aditionalFields, 
        String conditions, 
        List<Id> notInclude,
        String limitStr) {
        // System.debug('### accountRelatedContactsSearch ### conditions --> ' + conditions);
        // System.debug('### accountRelatedContactsSearch ### notInclude --> ' + notInclude);
        
        List <Contact> returnList = new List < sObject > ();

        String searchKey = searchKeyWord + '%';
        notInclude = notInclude != null ? notInclude : new List<Id>();
          
        String sQuery =  'SELECT Id, Name';
        sQuery += (!String.isBlank(aditionalFields) ? ', ' + aditionalFields : '');
        sQuery += ' FROM Contact WHERE (Name LIKE :searchKey OR Email LIKE :searchKey) AND Id NOT IN :notInclude';
        sQuery += (String.isBlank(conditions) ? '' : ' AND (' + conditions + ') ');
        sQuery += ' ORDER BY Name ASC';
        sQuery +=  ' LIMIT ' + limitStr;
        // System.debug('### sQuery --> ' + sQuery);
        
        try {
            returnList = Database.query(sQuery);
        } catch (Exception e) {
            System.debug(e.getMessage());
            throw new AuraHandledException(e.getMessage());
        }
        // System.debug('### returnList --> ' + returnList);
        return returnList;
    }

    @AuraEnabled(cacheable=true)
    public static List<LookupSearchResult> searchContact(String accountId) {
        // System.debug('### APEX params:');
        // System.debug('### searchTerm: ' + searchTerm);
        // System.debug('### selectedIds: ' + selectedIds);
        // System.debug('### accountId: ' + accountId);
        
        // Prepare query parameters
        //searchTerm += '*';
        // Execute search query
        // List<List<SObject>> searchResults = [
        //         FIND :searchTerm
        //         IN ALL FIELDS
        //                 RETURNING
        //                 Contact(Id, Name, Email WHERE AccountId = :accountId AND Id NOT IN :selectedIds)
        //         LIMIT :MAX_RESULTS
        // ];
   
        List<Contact> searchResults = [SELECT Id, Name, Email FROM Contact WHERE AccountId = :accountId];

        // Prepare results
        List<LookupSearchResult> results = new List<LookupSearchResult>();

        // Extract Users & convert them into LookupSearchResult
        String contactIcon = 'standard:contact';
        //Contact[] contacts = (List<Contact>) searchResults[0];
        for (Contact iContact : searchResults) {
            String subtitle = iContact.Email == null ? 'Email:' : 'Email • ' + iContact.Email;
            results.add(new LookupSearchResult(iContact.Id, 'Contact', contactIcon, iContact.Name, subtitle));
        }

        // Optionnaly sort all results on title
        results.sort();

        return results;
    }

    @AuraEnabled(cacheable=true)
    public static List<Contact> fetchSingleRecordSelected(String defaultRecordId) {
        if(defaultRecordId == null) return null;
        
        // System.debug('>>> APEX CONTROLLER >>>');
        // System.debug('>>> APEX CONTROLLER >>> >>> defaultRecordId' + defaultRecordId);
        String sQuery = 'SELECT Id, Name, Email FROM Contact WHERE Id =\'' + defaultRecordId + '\' LIMIT 1';
        return Database.query(sQuery);
    }
    
}