public without sharing class CancelClientOrderSchedule implements Schedulable {
  private final String CANCELATION_EVENT_NAME = 'CLIENT_ORDER_CANCELATION';

  public void execute(SchedulableContext schedulableContext) {
    createEvent();
  }

  private void createEvent() {
    List<Order> orders = queryClientOrdersToCancel();

    EventQueue event = new EventBuilder()
      .createEventFor(CANCELATION_EVENT_NAME)
      .buildEvent();

    event.addPayload(CANCELATION_EVENT_NAME, JSON.serialize(orders));

    event.save();
  }

  private List<Order> queryClientOrdersToCancel() {
    return [
      SELECT Id
      FROM Order
      WHERE
        Cancelation_Date__c <= TODAY
        AND Status != 'Canceled'
        AND AccountId != NULL
    ];
  }
}