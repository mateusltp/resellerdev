@isTest
global with sharing class PipefyIntegrationMock implements HttpCalloutMock{
    
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        System.assertEquals('https://api.pipefy.com/graphql', req.getEndpoint());
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"data":{"createCard":{"card":{"id":"542835944"},"clientMutationId":null}}}');
        res.setStatusCode(200);
        return res;
    }

}