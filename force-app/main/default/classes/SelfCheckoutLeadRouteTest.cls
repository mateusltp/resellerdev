@IsTest
public with sharing class SelfCheckoutLeadRouteTest {
    public SelfCheckoutLeadRouteTest() {

    }

    @IsTest
    public static void execute() {
        SelfCheckoutLeadRequest selfCheckoutLeadRequest = SelfCheckoutLeadRequestMock.getMock();
    
        RestRequest request = new RestRequest();
        request.requestURI = '/services/apexrest/self-checkout-lead';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf(JSON.serialize(selfCheckoutLeadRequest));
    
        Test.startTest();
    
        RestContext.response = new RestResponse();
    
        RestContext.request = request;
        SelfCheckoutLeadRoute.SelfCheckoutLeadResponse response = SelfCheckoutLeadRoute.post();
    
        Test.stopTest();
    
        System.assertEquals(
            204,
            response.getStatusCode(),
            'Error Message: ' + response.getErrorMessage()
        );
    }
}