/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 03-21-2022
 * @last modified by  : alysson.mota@gympass.com
**/
public with sharing class AccountContactRelationSelector  extends ApplicationSelector {

    public List<Schema.SObjectField> getSObjectFieldList() {
		return new List<Schema.SObjectField> {
			AccountContactRelation.Id,
			AccountContactRelation.ContactId,
            AccountContactRelation.AccountId
		};
	}

	public Schema.SObjectType getSObjectType() {
		return AccountContactRelation.sObjectType;
	}

    public List<AccountContactRelation> selectById(Set<Id> ids) {
		return (List<AccountContactRelation>) super.selectSObjectsById(ids);
	}

	public List<AccountContactRelation> selectContactRelationById( Set<Id> accountIds ){

		return new List<AccountContactRelation> ( (List<AccountContactRelation>) Database.query(
			newQueryFactory().
					selectField('Contact.Name').
					selectField('Contact.FirstName').
					selectField('Contact.LastName').
					selectField('Contact.Email').
					selectField('Contact.Phone').
					selectField('Contact.AccountId').
					selectField('Contact.Type_of_Contact__c').
					selectField(AccountContactRelation.ContactId).
					selectField(AccountContactRelation.Type_of_Contact__c).
					selectField(AccountContactRelation.AccountId).
					setCondition('AccountId IN: accountIds').
					toSOQL()
				)
			);
	}

	public List<AccountContactRelation> selectByAccountIdAndContactId( Set<Id> accountIds, Set<Id> contactIds ){

		return new List<AccountContactRelation> ( (List<AccountContactRelation>) Database.query(
			newQueryFactory().
					selectField('Contact.Name').
					selectField('Contact.Email').
					selectField('Contact.AccountId').
					selectField('Contact.Type_of_Contact__c').
					selectField(AccountContactRelation.ContactId).
					selectField(AccountContactRelation.Type_of_Contact__c).
					selectField(AccountContactRelation.AccountId).
					setCondition('AccountId IN: accountIds AND ContactId IN: contactIds').
					toSOQL()
				)
			);
	}

	public List<AccountContactRelation> selectByContactId(Set<Id> contactIds) {
		return new List<AccountContactRelation> ( (List<AccountContactRelation>) Database.query(
				newQueryFactory().
						selectField('Contact.Type_of_Contact__c').
						setCondition('ContactId IN :contactIds').
						toSOQL()
			)
		);
	}


}