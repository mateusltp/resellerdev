global without sharing class PipefyFormSitePageVFController {
    
    global String oppId {get;private set;}
    
    global PipefyFormSitePageVFController(){
        this.oppId = ApexPages.currentPage().getParameters().get('oppid');
        
    }
    
}