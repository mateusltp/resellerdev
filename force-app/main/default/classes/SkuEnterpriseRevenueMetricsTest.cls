/**
 * @description       : 
 * @author            : tania.fartaria@gympass.com
 * @group             : 
 * @last modified on  : 19-04-2022
 * @last modified by  : tania.fartaria@gympass.com
**/
@isTest
public class SkuEnterpriseRevenueMetricsTest {
    
    @TestSetup
    static void createData(){        
        Account acc = DataFactory.newAccount();   
        insert acc;
        
        Pricebook2 standardPricebook = new Pricebook2(
            Id = Test.getStandardPricebookId(),
            IsActive = true
        );
        update standardPricebook;
        Pricebook2 pb = standardPricebook;
        
        Opportunity opp = DataFactory.newOpportunity( acc.Id, standardPricebook, 'Client_Sales_SKU_New_Business' );  

        Product2 lAcessFee = DataFactory.newProduct( 'Enterprise Subscription' , false , 'BRL' );
        lAcessFee.Payment_Type__c = 'One time fee';
        Database.insert( lAcessFee );

        PricebookEntry lAccessFeeEntry = DataFactory.newPricebookEntry( standardPricebook , lAcessFee , opp );
        insert lAccessFeeEntry;

        Product2 lAcessFamily = DataFactory.newProduct( 'SKU Family Subscription' , false , 'BRL' );
        lAcessFamily.Payment_Type__c = 'Recurring fee';
        Database.insert( lAcessFamily );

        PricebookEntry lAccessFamilyEntry = DataFactory.newPricebookEntry( standardPricebook , lAcessFamily , opp );
        insert lAccessFamilyEntry;
       
        SKU_Price__c skuPriceEnterprise = DataFactory.newSKUPrice(lAcessFee.Id, opp,acc, 1, 30);
        insert skuPriceEnterprise;
        SKU_Price__c skuPriceAcessFamily = DataFactory.newSKUPrice(lAcessFamily.Id, opp,acc, 1, 40);
        insert skuPriceAcessFamily;

        insert opp;
        Quote lQuote = [SELECT Id FROM Quote WHERE OpportunityId = :opp.Id];
        QuoteLineItem lQuoteAccessFee = [SELECT Id FROM QuoteLineItem WHERE QuoteId = :lQuote.Id];

        QuoteLineItem lQuoteAccessFamilyFee = DataFactory.newQuoteLineItem( lQuote , lAccessFamilyEntry );
        lQuoteAccessFamilyFee.Discount__c = 50;
        Database.insert( lQuoteAccessFamilyFee ); 

        Payment__c lPayForLineItem = DataFactory.newPayment( lQuoteAccessFee );
        Database.insert( lPayForLineItem );

        Waiver__c lWaiver = DataFactory.newWaiver( lPayForLineItem );
        lWaiver.Number_of_Months_after_Launch__c = 15;
        Database.insert( lWaiver );

        Payment__c lPayFamilyForLineItem = DataFactory.newPayment( lQuoteAccessFamilyFee );
        Database.insert( lPayFamilyForLineItem );

        Waiver__c lWaiverFamily = DataFactory.newWaiver( lPayForLineItem );
        lWaiverFamily.Number_of_Months_after_Launch__c = 15;
        Database.insert( lWaiverFamily );


    }

    @isTest
    static void testById(){
        List<Quote> quotes = [SELECT Id FROM Quote ];
        List<String> quotesId = new List<String>{quotes.get(0).Id};
        SkuEnterpriseRevenueMetrics.calculateEnterpriseRevenue(quotesId);
        
    } 
}