@IsTest
private class ApprovalRequestCommentsControllerTest {

    @IsTest
    private static void test_get_comments() {
        ApprovalRequestCommentsController contr = new ApprovalRequestCommentsController();
        
        Test.startTest();
        
        system.assertEquals('', contr.comments);
        system.assertEquals('', contr.lastSubmitter);
        system.assertEquals('', contr.approverName);

        Test.stopTest();
    }
}