public with sharing class ClientsOfferSummaryViewerController {
    public ClientsOfferSummaryViewerController() {

    }

    @AuraEnabled
    public static FastTrackProposalCreationTO find(String recordId){
        Quote quote = new QuoteRepository().byId(recordId);
        FastTrackProposalCreationTO to = new FastTrackProposalCreationBuilder()
                    .withOpportunity(quote.OpportunityId)
                    .withLastCreatedQuoteWhenExists(quote.OpportunityId)
                    .build();    

        return to;
    }
}