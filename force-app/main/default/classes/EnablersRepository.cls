/**
 * @description       : 
 * @author            : AlyssonMota
 * @group             : 
 * @last modified on  : 06-11-2021
 * @last modified by  : roei@gft.com
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   09-08-2020   Alysson Mota   Initial Version
 * 2.0   25-05-2022   Bruno Pinho    Adding clause in the SOQL for avoiding getting records with null reference on the external ids
**/
public without sharing class EnablersRepository {
    private static Id RecTypeOpportunityStepClient = 
        Schema.SObjectType.Step_Towards_Success1__c.getRecordTypeInfosByDeveloperName().get('Opportunity_Step').getRecordTypeId();
    
    private static Id RecTypeAccountStepClient = 
        Schema.SObjectType.Step_Towards_Success1__c.getRecordTypeInfosByDeveloperName().get('Account_Step').getRecordTypeId();

    public EnablersRepository() {

    }

    public List<Step_Towards_Success1__c> getOpportunityEnablers(Opportunity opp) {
        List<Step_Towards_Success1__c> enablers = [
            SELECT Id, Name, Achieved__c, Master_Account__c, Related_Account__c, Related_Opportunity__c, Step_Number__c
            FROM Step_Towards_Success1__c
            WHERE Related_Opportunity__c =: opp.Id
            AND RecordTypeId =: RecTypeOpportunityStepClient
            AND Related_Opportunity__c != NULL
        ];

        return enablers;
    }

    public List<Step_Towards_Success1__c> getOpportunityEnablersByOppId(Set<Id> oppIdSet) {
        List<Step_Towards_Success1__c> enablers = [
            SELECT Id, Name, Achieved__c, Master_Account__c, Related_Account__c, Related_Opportunity__c,Related_Opportunity__r.AccountId, Step_Number__c
            FROM Step_Towards_Success1__c
            WHERE Related_Opportunity__c IN :oppIdSet
            AND RecordTypeId =: RecTypeOpportunityStepClient
            AND Related_Opportunity__c != NULL
        ];

        return enablers;
    }

    public List<Step_Towards_Success1__c> getAccountEnablersByAccId(Set<Id> accIdSet) {
        List<Step_Towards_Success1__c> enablers = [
            SELECT Id, Name, Achieved__c, Master_Account__c, Related_Account__c, Related_Opportunity__c, Step_Number__c, Expiration_Date__c
            FROM Step_Towards_Success1__c
            WHERE  Related_Account__c IN :accIdSet
            AND RecordTypeId =: RecTypeAccountStepClient
            AND Related_Account__c != NULL
        ];

        return enablers;
    }

    public List<Step_Towards_Success1__c> getAccountEnablers(Account account) {
        List<Step_Towards_Success1__c> enablers = [
            SELECT Id, Name, Achieved__c, Master_Account__c, Related_Account__c, Related_Opportunity__c, Step_Number__c
            FROM Step_Towards_Success1__c
            WHERE Related_Account__c =: account.Id
            AND RecordTypeId =: RecTypeAccountStepClient
            AND Related_Account__c != NULL
        ];

        return enablers;
    }

    public List<Step_Towards_Success1__c> createEnablersForOpportunityClients(Opportunity opp) {
        List<Step_Towards_Success_Definition__mdt> stepTowardsSuccessDefinitionList = getMapStepsTowardsSucessClientsDefinition();
        List<Step_Towards_Success1__c> enablersToInsert = new List<Step_Towards_Success1__c>();
        for (Step_Towards_Success_Definition__mdt definition : stepTowardsSuccessDefinitionList) {
            Step_Towards_Success1__c enabler = new Step_Towards_Success1__c();
            enabler.Master_Account__c = opp.AccountId;
            enabler.Related_Opportunity__c = opp.Id;
            enabler.Achieved__c = 'No';
            enabler.Step_Number__c = definition.Step__c;
            enabler.Name = definition.MasterLabel;
            enabler.RecordTypeId = RecTypeOpportunityStepClient;
            enablersToInsert.add(enabler);
        }
        return enablersToInsert;
    }

    public List<Step_Towards_Success1__c> createEnablersForAccountIdClients(Id accId) {
        List<Step_Towards_Success_Definition__mdt> stepTowardsSuccessDefinitionList = getMapStepsTowardsSucessClientsDefinition();
        List<Step_Towards_Success1__c> enablersToInsert = new List<Step_Towards_Success1__c>();

        for (Step_Towards_Success_Definition__mdt definition : stepTowardsSuccessDefinitionList) {
            Step_Towards_Success1__c enabler = new Step_Towards_Success1__c();
            enabler.Master_Account__c = accId;
            enabler.Related_Account__c = accId;
            enabler.Achieved__c = 'No';
            enabler.Step_Number__c = definition.Step__c;
            enabler.Name = definition.MasterLabel;
            enabler.RecordTypeId = RecTypeAccountStepClient;
            enabler.Manual_changes_allowed__c = definition.Manual_changes_allowed__c;

            enablersToInsert.add(enabler);
        }

        return enablersToInsert;
    }

    public void create(List<Step_Towards_Success1__c> enablers) {
        insert enablers;
    }

    public void create(Step_Towards_Success1__c enabler) {
        insert enabler;
    }

    public void save(List<Step_Towards_Success1__c> enablers) {
        update enablers;
    }

    public void save(Step_Towards_Success1__c enabler) {
        update enabler;
    }

    public static List<Step_Towards_Success_Definition__mdt> getMapStepsTowardsSucessClientsDefinition() {
         Set<String> stepsCategorySet = new Set<String>{
            'Business Enabler','Enrollment Enablers'
        };
        
        List<Step_Towards_Success_Definition__mdt> listStepsTowardsSuccessDefinitions = [
            SELECT DeveloperName, MasterLabel, Category__c, Description__c, Field_API_Name__c, Field_Expected_Value__c, How_are_we_going_to_track_it__c, Object_API_Name__c, Step__c, Manual_changes_allowed__c
            FROM Step_Towards_Success_Definition__mdt
            WHERE Category__c IN :stepsCategorySet
            ORDER BY Step__c ASC
        ];
        
        return listStepsTowardsSuccessDefinitions;
    }
}