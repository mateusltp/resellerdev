@IsTest
public class SelfTerminationCommandTest {

    @TestSetup
    public static void makeData() {
        
        Account acc = DataFactory.newAccount();
        acc.UUID__c = 'tt47c899-7e59-47ad-9ad7-7d0f6310f4ea';
        insert acc;

        Product2 pd = DataFactory.newProduct('Product', True, 'BRL');
        insert pd;

        Pricebook2 pb = DataFactory.newPricebook();

        Opportunity opp = DataFactory.newOpportunity(acc.Id, pb, 'Client_Success_Renegotiation');
        insert opp;

        PricebookEntry pbEntry = DataFactory.newPricebookEntry(pb, pd, opp);   
        pbEntry.UnitPrice= 7.95;
        insert pbEntry;

        Order ord = new Order (
            accountId = acc.Id,
            status = 'Draft',
            EffectiveDate = Date.today(),
            UUID__c = 'da26ed65-debb-48b8-bbfb-6c60326761c3',
            Pricebook2Id = pb.Id,
            CurrencyIsoCode = 'BRL',
            opportunityId = opp.Id,
            Type = 'New Business'
        );
        insert ord;

        OrderItem orItem = new OrderItem(
            OrderId = ord.Id,
            PricebookEntryId = pbEntry.Id,
            Quantity = 1,
            Type__c = '1', 
            UnitPrice = 7.95,
            ListPrice = 7.95
        );

        insert orItem;
        
        ord.status = 'Activated';
        update ord;

        List<EventConfiguration__c> eventConfigurations = new List<EventConfiguration__c>();

        EventConfiguration__c cancelEventConfiguration = new EventConfiguration__c(
            name = 'CANCEL_SELF_TERMINATION_INBOUND',
            commandClassName__c = 'SelfTerminationCommand'
        );
        eventConfigurations.add(cancelEventConfiguration);

        EventConfiguration__c undoEventConfiguration = new EventConfiguration__c(
            name = 'UNDO_SELF_TERMINATION_INBOUND',
            commandClassName__c = 'SelfTerminationCommand'
        );
        eventConfigurations.add(undoEventConfiguration);
        insert eventConfigurations;

    }
  
    @isTest
    public static void executeCancelTest() {

        Test.startTest();

        SelfTerminationRequest selfTerminationRequest = SelfTerminationRequestMock.getMock('CANCEL');
        EventQueue event = new SelfTerminationPublisher(selfTerminationRequest).runCancel();
        Test.stopTest();

        Order order = [SELECT Id, status, Is_Account_On_Tagus__c,Type FROM Order LIMIT 1];
        System.assertEquals( 'Cancel Requested', order.status, 'Order is not cancelled');
    }


    @isTest
    public static void executeUndoTest() {

        Test.startTest();

        SelfTerminationRequest selfTerminationRequest = SelfTerminationRequestMock.getMock('UNDO');
        EventQueue event = new SelfTerminationPublisher(selfTerminationRequest).runUndo();
        Test.stopTest();

        Order order = [SELECT Id, status, opportunityId, AccountId FROM Order LIMIT 1];
        System.assertEquals( 'Activated', order.status, 'Order is not activated');
    }


    @isTest
    public static void executeExceptionTest() {
        
        //String requestJson = '{' +
        // '"action": "' + 'CANCEL' + '"}';
        SelfTerminationRequest selfTerminationRequest = SelfTerminationRequestMock.getMock('UNDO');

        Test.startTest();

        EventQueue event = new SelfTerminationPublisher(selfTerminationRequest, 'Error').runCancel();
        Test.stopTest();

        Order order = [SELECT Id, status FROM Order LIMIT 1];
        System.assertEquals( 'Activated', order.status, 'Exception did not happen');
    }

}