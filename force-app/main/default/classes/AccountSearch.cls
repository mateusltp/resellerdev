public without sharing class AccountSearch{ 
    public static final String ACCOUNT_RECORDTYPEID_RESELLER = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
    private final String InvalidLegalNumber = System.label.Community_Invalid_Legal_Number;
    private final String InvalidLegalNumberTrunk = System.label.Community_Invalid_Legal_Number_Trunk;
    private final String InvalidByWebSite = System.label.Community_Invalid_ByWebSite;
    private final String InvalidByName = System.label.Community_Invalid_ByName;
    public static Boolean isMassive = false;
    public Map<String, AccountSearchWrapper> mapResultAccountByLegalNumber = new Map<String, AccountSearchWrapper>();
    public Map<String, AccountSearchWrapper> mapResultAccountByLegalNumberTrunk = new Map<String, AccountSearchWrapper>();
    public Map<String, AccountSearchWrapper> mapResultAccountByWebSite = new Map<String, AccountSearchWrapper>();
    public Map<String, AccountSearchWrapper> mapResultAccountByName = new Map<String, AccountSearchWrapper>();
    public Map<String,String> mapNameAccount = new Map<String,String>();
    private Set<String> legalNumbers_set = new Set<String>();
    private Set<String> accountName_set = new Set<String>();
    private Set<String> accountWebSite_set = new Set<String>();
    public AccountSearch(){
    }

    public AccountSearch(Set<String> legalNumbers_set, Set<String> accountName_set, Set<String> accountWebSite_set){
        this.legalNumbers_set = legalNumbers_set;
        this.accountName_set = accountName_set;
        this.accountWebSite_set = accountWebSite_set;
        System.debug(logginglevel.INFO, 'New AccountSearch Instance by Lists');
    }

    public AccountSearch(List<Account_Request__c> accountRequest_list){
        for (Account_Request__c account_request : accountRequest_list){
            legalNumbers_set.add(account_request.Unique_Identifier__c);
            accountName_set.add(account_request.Name);
            accountWebSite_set.add(account_request.Website__c);
        }
        System.debug(logginglevel.INFO, 'New AccountSearch Instance by Account_Request__c');
    }

    public Map<String, AccountSearchWrapper> getAccounts(){
        System.debug('Estou aqui');
        System.debug('legalNumbers_set : ' + this.legalNumbers_set);
        System.debug('accountName_set : ' + this.accountName_set);
        System.debug('accountWebSite_set : ' + this.accountWebSite_set);

        return this.managerSearch();
    }

    private Map<String, AccountSearchWrapper> managerSearch(){
        this.legalNumbers_set = this.removeLegalNumberMask(this.legalNumbers_set);
        legalNumbers_set.remove(null);

        Map<String, AccountSearchWrapper> accountByLegalNumber = legalNumbers_set.isEmpty() ? null : this.getAccountByLegalNumber();
        if (accountByLegalNumber != null && !isMassive)
            return accountByLegalNumber;
        if (accountByLegalNumber != null && isMassive){
            mapResultaccountByLegalNumber = accountByLegalNumber;
        }

        Map<String, AccountSearchWrapper> accountByLegalNumberTrunk = legalNumbers_set.isEmpty() ? null : this.getAccountByLegalNumberTrunk();
        if (accountByLegalNumberTrunk != null && !isMassive)
            return accountByLegalNumberTrunk;
        if (accountByLegalNumberTrunk != null && isMassive){
            mapResultaccountByLegalNumberTrunk = accountByLegalNumberTrunk;
        }

        accountWebSite_set.remove(null);
        Map<String, AccountSearchWrapper> accountByWebSite = accountWebSite_set.isEmpty() ? null : this.getAccountByWebSite();
        if (accountByWebSite != null && !isMassive)
            return accountByWebSite;
        if (accountByWebSite != null && isMassive){
            mapResultaccountByWebSite = accountByWebSite;
        }

        accountName_set.remove(null);
        Map<String, AccountSearchWrapper> accountByName = accountName_set.isEmpty() ? null : this.getAccountByName();
        if (accountByName != null && !isMassive)
            return accountByName;
        if (accountByName != null && isMassive){
            mapResultaccountByName = new Map<String, AccountSearchWrapper>(accountByName);
        }

        return null;
    }

    private Map<String, AccountSearchWrapper> getAccountByLegalNumber(){
        Map<String, AccountSearchWrapper> accountSearchWrapper_list = new Map<String, AccountSearchWrapper>();
        List<Account> account_list = new List<Account>();
        if (legalNumbers_set.size() > 0 && legalNumbers_set != null && !legalNumbers_set.isEmpty()){
            account_list = [SELECT Id, Name, BillingCountry, Website, Id_Company__c, Legal_Number_without_mask__c
                            FROM Account
                            WHERE Legal_Number_without_mask__c IN:legalNumbers_set
                            ORDER BY Name, Website ASC
                            LIMIT 10000];
        }

        if (account_list.isEmpty())
            return null;
        for (Account acc : account_list){
            accountSearchWrapper_list.put(acc.Id_Company__c, new AccountSearchWrapper(acc.Id, acc.Name, acc.BillingCountry, acc.Website, acc.Id_Company__c, acc.Legal_Number_without_mask__c, SearchTrack.EXACT_LEGAL_NUMBER));
            legalNumbers_set.remove(acc.Id_Company__c);
        }

        return accountSearchWrapper_list;
    }

    private Map<String, AccountSearchWrapper> getAccountByLegalNumberTrunk(){
        Map<String, AccountSearchWrapper> accountSearchWrapper_list = new Map<String, AccountSearchWrapper>();

        Set<String> legalNumbersTrunk_set = new Set<String>();
        for (String legalNumber : legalNumbers_set)
            legalNumbersTrunk_set.add(legalNumber.left(8));
        List<Account> account_list = new List<Account>([SELECT Id, Name, BillingCountry, Website, Id_Company__c, Legal_Number_without_mask__c
                                                        FROM Account
                                                        WHERE Brazil_Cnpj_Trunk__c IN:legalNumbersTrunk_set
                                                        ORDER BY Name, Website ASC
                                                        LIMIT 10000]);

        if (account_list.isEmpty())
            return null;
        for (Account acc : account_list){
            accountSearchWrapper_list.put(acc.Id_Company__c, new AccountSearchWrapper(acc.Id, acc.Name, acc.BillingCountry, acc.Website, acc.Id_Company__c, acc.Legal_Number_without_mask__c, SearchTrack.TRUNK_LEGAL_NUMBER));
        }

        return accountSearchWrapper_list;
    }

    private Map<String, AccountSearchWrapper> getAccountByWebSite(){
        Map<String, AccountSearchWrapper> accountSearchWrapper_list = new Map<String, AccountSearchWrapper>();
        List<String> accountWebSite_list = new List<String>(accountWebSite_set);

        String siteSearch;
        for (String site : accountWebSite_list){
            site = site.replaceAll('https://', '').replace('http://', '').replace('www.', '');
            site = site.left(site.indexOf('.'));
            site = '%' + site + '%';
            siteSearch = site;
            break;
        }
        system.debug('###### ' + siteSearch);

        Set<string> concatSite = new Set<string>();
        String percentage = '%';
        for (Integer i = 0; i <= accountWebSite_list.size() - 1; i++){
            accountWebSite_list[i] = accountWebSite_list[i].replaceAll('https://', '').replace('http://', '').replace('www.', '');
            accountWebSite_list[i] = accountWebSite_list[i].left(accountWebSite_list[i].indexOf('.'));
            accountWebSite_list[i] = percentage + accountWebSite_list[i] + percentage;
            concatSite.add(accountWebSite_list[i]);
            //if(i == accountWebSite_list.size() - 1)concatSite = concatSite + ' OR ' + accountWebSite_list[i];
            //else if(String.isEmpty(concatSite))concatSite = accountWebSite_list[i];
            //else concatSite = concatSite + ' OR ' + accountWebSite_list[i];
        }
        system.debug('###### ' + concatSite);
        List<Account> account_list = new List<Account>([SELECT Id, Name, BillingCountry, Website, Id_Company__c, Legal_Number_without_mask__c
                                                        FROM Account
                                                        WHERE Website LIKE:concatSite
                                                        ORDER BY Name, Website ASC
                                                        LIMIT 10000]);
        system.debug('######' + account_list);

        if (account_list.isEmpty())
            return null;
        for (Account acc : account_list){
            String key = acc.Id_Company__c == null ? acc.Website : acc.Id_Company__c;
            accountSearchWrapper_list.put(key, new AccountSearchWrapper(acc.Id, acc.Name, acc.BillingCountry, acc.Website, acc.Id_Company__c, acc.Legal_Number_without_mask__c, SearchTrack.EXACT_WEB_SITE));
            legalNumbers_set.remove(acc.Website);
        }
        system.debug('###### ' + accountSearchWrapper_list);
        return accountSearchWrapper_list;
    }

    private Map<String, AccountSearchWrapper> getAccountByName(){
        Map<String, AccountSearchWrapper> accountSearchWrapper_list = new Map<String, AccountSearchWrapper>();
        //Map<String, String> mapGetAccountByName = new Map<String, String>();
        List<Account> account_list = new List<Account>();
        List<String> nameSimilarity = new List<String>();
        List<String> accountName_list = new List<String>(accountName_set);
        system.debug('###### ' + accountName_set);
        String concatNames;
        //String namesAccount;

        for (Integer i = 0; i <= accountName_list.size() - 1; i++){
            if (i == accountName_list.size() - 1)
                concatNames = concatNames + ' OR ' + accountName_list[i];
            else if (String.isEmpty(concatNames))
                concatNames = accountName_list[i];
            else
                concatNames = concatNames + ' OR ' + accountName_list[i];
        }
        system.debug('###### ' + concatNames);

        List<List<SObject>> searchOnAccount = [FIND : concatNames IN NAME FIELDS RETURNING Account(Id, Name, BillingCountry, Website, Id_Company__c, Legal_Number_without_mask__c)];
        if (!searchOnAccount.isEmpty())
            account_list = (List<Account>)searchOnAccount[0];
        if (account_list.isEmpty())
            return null;
        system.debug('###### ' + account_list);

        for (Account acc : account_list){
            accountSearchWrapper_list.put(acc.Name, new AccountSearchWrapper(acc.Id, acc.Name, acc.BillingCountry, acc.Website, acc.Id_Company__c, acc.Legal_Number_without_mask__c, SearchTrack.BY_NAME));
            //namesAccount = namesAccount + ' ' + acc.name;
			nameSimilarity.add(acc.name);       
        }

        //nameSimilarity = namesAccount.split(' ');
        system.debug('###### ' + accountName_list);
        system.debug('###### ' + nameSimilarity);

        if (isMassive){
            for (String accName : accountName_list){
                for (String name : nameSimilarity){
                    if (name.toUpperCase().contains((accName.toUpperCase()))){
                        system.debug('mesmo nome');
                        system.debug('######## ' + accName + ' ' + name);
                        mapNameAccount.put(accName, name);
                    }

                }

            }
        }
        system.debug('mapGetAccountByName ' + mapNameAccount);
        return accountSearchWrapper_list;
    }

    public void searchAccountsMassive(List<Account_Request__c> accountReqs){
        system.debug('searchAccountsMassive ' + accountReqs);
        isMassive = true;

        Set<String> websiteList = new Set<String>();
        Set<String> uniqueIdentifierList = new Set<String>();
        Map<String, Account> mapAccount = new Map<String, Account>();
        Map<String, Account> mapAccount_Website = new Map<String, Account>();
        Map<String, AccountSearchWrapper> mapLegalNumberTrunk = new Map<String, AccountSearchWrapper>();
        List<Account> newAccounts = new List<Account>();
        List<Account_Request__c> updtNewAcc = new List<Account_Request__c>();
        List<Account_Request__c> updtOldAcc = new List<Account_Request__c>();
        
        AccountSearch accountSearch = new AccountSearch(accountReqs);
        Map<String, AccountSearchWrapper> mapResultGetAccounts = accountSearch.getAccounts();

        mapResultAccountByLegalNumber = accountSearch.mapResultAccountByLegalNumber;
        mapResultAccountByLegalNumberTrunk = accountSearch.mapResultAccountByLegalNumberTrunk;
        mapResultAccountByWebSite = accountSearch.mapResultAccountByWebSite;
        //mapResultAccountByName = accountSearch.mapResultAccountByName;
        mapNameAccount = accountSearch.mapNameAccount;

        if (mapResultaccountByLegalNumberTrunk.size() > 0){
            List<AccountSearchWrapper> listResultaccountByLegalNumberTrunk = mapResultaccountByLegalNumberTrunk.values();
            for (AccountSearchWrapper item : listResultaccountByLegalNumberTrunk){
                if (String.valueOf(item.searchTrack) == 'TRUNK_LEGAL_NUMBER'){
                    mapLegalNumberTrunk.put(item.accountIdCompanyWithoutMask.left(8), item);
                }
            }
        }

        for (Account_Request__c accReq : accountReqs){
            uniqueIdentifierList.add(accReq.Unique_Identifier__c);
            websiteList.add(accReq.Website__c);
        }
        uniqueIdentifierList.remove(null);
        websiteList.remove(null);
        uniqueIdentifierList = this.removeLegalNumberMask(uniqueIdentifierList);

        system.debug('websiteList ' + websiteList);
        system.debug('uniqueIdentifierList' + uniqueIdentifierList);

        //try{
        if (uniqueIdentifierList.size() > 0){
            for (Account acc : [SELECT Id, Name, BillingCountry, Website, Id_Company__c, Legal_Number_without_mask__c
                                FROM Account
                                WHERE Legal_Number_without_mask__c IN:uniqueIdentifierList AND Legal_Number_without_mask__c != ''
                                ORDER BY Type DESC, ABM_Prospect__c ASC, LastModifiedDate DESC]){
                mapAccount.put(acc.Legal_Number_without_mask__c, acc);
            }
        }
        if (websiteList.size() > 0){
            for (Account acc2 : [SELECT Id, Name, BillingCountry, Website, Id_Company__c, Legal_Number_without_mask__c
                                 FROM Account
                                 WHERE Website IN:websiteList AND Website != ''
                                 ORDER BY Type DESC, ABM_Prospect__c ASC, LastModifiedDate DESC]){
                mapAccount_Website.put(acc2.Website, acc2);
            }
        }

        system.debug('mapAccount ' + mapAccount);
        system.debug('mapAccount_Website ' + mapAccount_Website);

        system.debug('mapResultaccountByLegalNumber ' + mapAccount);
        system.debug('mapResultaccountByLegalNumberTrunk ' + mapLegalNumberTrunk);
        system.debug('mapResultaccountByWebSite ' + mapResultaccountByWebSite);
        system.debug('mapResultaccountByName ' + mapNameAccount);

        Boolean accountFound;
        for (Integer i = 0; i < accountReqs.size(); i++){
            accountFound = false;

            if (mapAccount.containsKey(accountReqs[i].Unique_Identifier__c.replace('.', '').replace('/', '').replace('-', ''))){
                //system.debug('accountReqs[i].Unique_Identifier__c ' + accountReqs[i].Unique_Identifier__c);
                //system.debug('pegou mesmo cnpj');
                accountReqs[i].AccountId__c = mapAccount.get(accountReqs[i].Unique_Identifier__c.replace('.', '').replace('/', '').replace('-', '')).Id;
                accountReqs[i].Engine_Log__c += ' -|- ' + ResellerEnumRepository.MassiveLog.ACCOUNT_FOUND_BY_CNPJ.name() + ' ' + String.valueOf(System.now());
                accountReqs[i].Existing_In_SalesForce__c = true;
                updtOldAcc.add(accountReqs[i]);
                accountFound = true;
            } else if (mapLegalNumberTrunk.size() > 0 && mapLegalNumberTrunk.containsKey(accountReqs[i].Unique_Identifier__c.replace('.', '').replace('/', '').replace('-', '').left(8))){
                //system.debug('accountReqs[i].Unique_Identifier__c ' + accountReqs[i].Unique_Identifier__c);
                //system.debug('pegou cnpj trunk  ');
                //accountReqs[i].AccountId__c = mapLegalNumberTrunk.get(accountReqs[i].Unique_Identifier__c.replace('.','').replace('/', '').replace('-','').left(8)).Id;
                accountReqs[i].Engine_Log__c += ' -|- ' + ResellerEnumRepository.MassiveLog.ACCOUNT_FOUND_BY_CNPJ.name() + ' ' + String.valueOf(System.now());
                accountReqs[i].Existing_In_SalesForce__c = true;
                accountReqs[i].Engine_Status__c = 'NO GO';
                accountReqs[i].Error_message__c = InvalidLegalNumberTrunk;
                updtOldAcc.add(accountReqs[i]);
                accountFound = true;
            } else if (mapNameAccount.containsKey(accountReqs[i].Name)){
                //system.debug('pegou  name');
                //system.debug('accountReqs[i].Name ' + accountReqs[i].Name);
                //accountReqs[i].AccountId__c = mapResultaccountByName.get(accountReqs[i].Unique_Identifier__c.replace('.','').replace('/', '').replace('-','').left(8)).Id;
                accountReqs[i].Engine_Log__c += ' -|- ' + ResellerEnumRepository.MassiveLog.ACCOUNT_FOUND_BY_CNPJ.name() + ' ' + String.valueOf(System.now());
                accountReqs[i].Existing_In_SalesForce__c = true;
                accountReqs[i].Engine_Status__c = 'NO GO';
                accountReqs[i].Error_message__c = InvalidByName;
                updtOldAcc.add(accountReqs[i]);
                accountFound = true;
            } else if (mapAccount_Website.size() > 0){
                if (mapAccount_Website.containsKey(accountReqs[i].Website__c)){
                    //system.debug('pegou mesmo website');
                    //accountReqs[i].AccountId__c = mapAccount_Website.get(accountReqs[i].Website__c).Id;
                    accountReqs[i].Engine_Log__c += ' -|- ' + ResellerEnumRepository.MassiveLog.ACCOUNT_FOUND_BY_WEBSITE.name() + ' ' + String.valueOf(System.now());
                    accountReqs[i].Existing_In_SalesForce__c = true;
                    accountReqs[i].Engine_Status__c = '';
                    updtOldAcc.add(accountReqs[i]);
                    accountFound = true;
                }
            } else{
                //system.debug('pegou '+accountReqs[i].Website__c);
                //system.debug('pegou '+mapResultaccountByWebSite.get(accountReqs[i].Website__c));
                if (mapResultaccountByWebSite.containsKey(accountReqs[i].Website__c)){
                    system.debug('pegou website');
                    //accountReqs[i].AccountId__c = mapResultaccountByWebSite.get(accountReqs[i].Unique_Identifier__c.replace('.','').replace('/', '').replace('-','').left(8)).Id;
                    accountReqs[i].Engine_Log__c += ' -|- ' + ResellerEnumRepository.MassiveLog.ACCOUNT_FOUND_BY_CNPJ.name() + ' ' + String.valueOf(System.now());
                    accountReqs[i].Existing_In_SalesForce__c = true;
                    accountReqs[i].Engine_Status__c = 'NO GO';
                    accountReqs[i].Error_message__c = InvalidByWebSite;
                    updtOldAcc.add(accountReqs[i]);
                    accountFound = true;
                }
            }

            if (accountFound == false){
                Account account = new Account();
                account.Name = accountReqs[i].Name;
                account.Legal_Document_Type__c = accountReqs[i].Unique_Identifier_Type__c;
                account.Id_Company__c = accountReqs[i].Unique_Identifier__c;
                account.Razao_Social__c = accountReqs[i].Name;
                account.Website = accountReqs[i].Website__c;
                account.NumberOfEmployees = Integer.valueOf(accountReqs[i].Total_number_of_employees__c);
                account.Industry = 'Airlines';
                account.BillingCountry = accountReqs[i].Billing_Country__c;
                account.BillingState = accountReqs[i].Billing_Country__c == 'Brazil' ? 'São Paulo' : '';
                account.RecordTypeId = ACCOUNT_RECORDTYPEID_RESELLER;

                newAccounts.add(account) ;
                updtNewAcc.add(accountReqs[i]);
            }
        }

        Set<String> accountIdSucess = new Set<String>();
        Map<String, Account> uniqueKeyAccount_map = new Map<String, Account>();
        Map<String, Account> webSiteAccount_map = new Map<String, Account>();
        Map<Id, String> error_account_map = new Map<Id, String>();

        Database.SaveResult[] saveResultsAccount = Database.insert (newAccounts, false);

        for (Database.SaveResult sr : saveResultsAccount){
            if (sr.isSuccess()){
                accountIdSucess.add(sr.getId());
            } else{
                for (Database.Error err : sr.getErrors()){
                    error_account_map.put(sr.getId(), err.getMessage());
                }
            }
        }

        for (Account acc : [SELECT Id, Id_Company__c, Website
                            FROM Account
                            WHERE Id IN:accountIdSucess]){
            if (acc.Id_Company__c != null){
                uniqueKeyAccount_map.put(acc.Id_Company__c, acc);
            }
            if (acc.Website != null){
                webSiteAccount_map.put(acc.Website, acc);
            }
        }

        for (Integer i = 0; i < updtNewAcc.size(); i++){
            if (error_account_map.containsKey(newAccounts[i].Id)){
                updtNewAcc[i].Engine_Log__c += ' -|- ' + ResellerEnumRepository.MassiveLog.NEW_ACCOUNT_NOT_CREATED.name() + ' ' + String.valueOf(System.now());
                updtNewAcc[i].Error_message__c = error_account_map.get(newAccounts[i].Id);
                updtNewAcc[i].Engine_Status__c = 'NO GO';
                continue;
            }
            if (uniqueKeyAccount_map.containsKey(updtNewAcc[i].Unique_Identifier__c) && updtNewAcc[i].Unique_Identifier__c != null){
                updtNewAcc[i].AccountId__c = String.valueOf(uniqueKeyAccount_map.get(updtNewAcc[i].Unique_Identifier__c).Id);
                updtNewAcc[i].IsNewAccount__c = true;
                updtNewAcc[i].Engine_Log__c += ' -|- ' + ResellerEnumRepository.MassiveLog.NEW_ACCOUNT_SUCCESSFULLY_CREATED.name();
                updtNewAcc[i].Engine_Log__c += ' -|- ' + ResellerEnumRepository.MassiveLog.NEW_ACCOUNT_CREATED_BY_UNIQUE_KEY.name() + ' ' + String.valueOf(System.now());
            } else if (webSiteAccount_map.containsKey(updtNewAcc[i].Website__c) && updtNewAcc[i].Website__c != null){
                updtNewAcc[i].AccountId__c = String.valueOf(webSiteAccount_map.get(updtNewAcc[i].Website__c).Id);
                updtNewAcc[i].IsNewAccount__c = true;
                updtNewAcc[i].Engine_Log__c += ' -|- ' + ResellerEnumRepository.MassiveLog.NEW_ACCOUNT_SUCCESSFULLY_CREATED.name();
                updtNewAcc[i].Engine_Log__c += ' -|- ' + ResellerEnumRepository.MassiveLog.NEW_ACCOUNT_CREATED_BY_WEB_SITE.name() + ' ' + String.valueOf(System.now());
            }
        }

        system.debug('updtNewAcc ' + updtNewAcc);
        system.debug('updtOldAcc ' + updtOldAcc);
        try{
            updtNewAcc.addAll(updtOldAcc);
            update updtNewAcc;
        } catch (Exception e){
            System.debug(e.getMessage());
        }


        system.debug('updtNewAcc ' + updtNewAcc);
        //     }
        //      catch(Exception ex){
        //         system.debug('entrou no catch');
        //         System.debug(ex.getMessage());
        //    }
    }

    public Set<String> removeLegalNumberMask(Set<String> uniqueIdentifierList){
        Set<String> legalNumberWithoutMask = new Set<String>();
        for (String legalNumber : uniqueIdentifierList){
            String newLegalNumber = legalNumber != null ? legalNumber.replace('.', '').replace('/', '').replace('-', '') : legalNumber;
            legalNumberWithoutMask.add(newLegalNumber);
        }
        return legalNumberWithoutMask;
    }

    public class AccountSearchWrapper{
        public String Id{ get; set; }

        public String accountName{ get; set; }

        public String accountBillingCountry{ get; set; }

        public string accountWebsite{ get; set; }

        public String accountIdCompany{ get; set; }

        public String accountIdCompanyWithoutMask{ get; set; }

        public SearchTrack searchTrack{ get; set; }

        public AccountSearchWrapper(String accountId, String accountName, String accountBillingCountry, String accountWebsite, String accountIdCompany, String accountIdCompanyWithoutMask, SearchTrack searchTrack){
            this.Id = accountId;
            this.accountName = accountName;
            this.accountBillingCountry = accountBillingCountry;
            this.accountWebsite = accountWebsite;
            this.accountIdCompany = accountIdCompany;
            this.accountIdCompanyWithoutMask = accountIdCompanyWithoutMask;
            this.searchTrack = searchTrack;
        }

    }

    public enum SearchTrack{
        EXACT_LEGAL_NUMBER, 
        TRUNK_LEGAL_NUMBER, 
        EXACT_WEB_SITE, 
        BY_NAME
    }

}