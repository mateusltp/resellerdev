/**
 * @description       : 
 * @author            : gilberto.souza@gft.com
 * @group             : 
 * @last modified on  : 03-10-2021
 * @last modified by  : gft.samuel.silva@ext.gympass.com
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   03-03-2021   gilberto.souza@gft.com   Initial Version
**/

public without sharing class DealDeskOperationalChangeBilling {
    
    public void changePaymentBilling(List<Case> ltNewCase){
        Id rtId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Deal_Desk_Operational').getRecordTypeId();
        Map<String, Case> mpCase = new Map<String, Case>();
        List<Id> ltQuoteIds = new List<Id>();
		List<Id> oppIds = new List<Id>();
        for(Case newCase : ltNewCase){
            if(newCase.RecordTypeId == rtId && newCase.Status == 'Approved' ){
                mpCase.put(newCase.OpportunityId__c, newCase);
                ltQuoteIds.add(newCase.QuoteId__c);
                oppIds.add(newCase.OpportunityId__c);
            }
        }

        if(mpCase.size() > 0){
            List<Payment__c> ltPayments = new List<Payment__c>();
            List<Eligibility__c> ltEligibility = new List<Eligibility__c>();
            Map<String, Payment__c> mpPaymentES = new Map<String, Payment__c>();

            PaymentRepository paymentRepository = new PaymentRepository();
            EligibilityRepository eligibilityRepository = new EligibilityRepository();

            List<QuoteLineItem> ltQuoteLine = QuoteLineItemRepository.feesPaymentsForOpp(oppIds);

            for(QuoteLineItem quoteLineItem : ltQuoteLine){
                Payment__c payment = quoteLineItem.Payments__r;
                Case newCase = mpCase.get(quoteLineItem.Quote.OpportunityId);
                
                if(quoteLineItem.PricebookEntry.Product2.Family == 'Enterprise Subscription'){
                    payment.Billing_Day__c = newCase.ES_Billing_Day__c;
                    payment.Payment_Due_Days__c = newCase.ES_Payment_Due_Days__c;
                    payment.Custom_Billing_Day__c = newCase.Custom_ES_Billing_Day__c;
                    payment.Custom_Payment_Due_Days__c = newCase.Custom_ES_Payment_Due_Days__c;
                    mpPaymentES.put(quoteLineItem.Quote.OpportunityId, payment);

                }else if(quoteLineItem.PricebookEntry.Product2.Name == 'Setup Fee'){
                   if(!(newCase.Setup_Fee_Billing_Day__c == 'Not_applicable' || newCase.Setup_Fee_Payment_Due_Days__c == 'Not_applicable')){
                        payment.Billing_Day__c = newCase.Setup_Fee_Billing_Day__c;
                        payment.Payment_Due_Days__c = newCase.Setup_Fee_Payment_Due_Days__c;
                        payment.Custom_Billing_Day__c = newCase.Custom_Setup_Billing_Day__c;
                        payment.Custom_Payment_Due_Days__c = newCase.Custom_Setup_Payment_Due_Day__c;
                    }

                }else if(quoteLineItem.PricebookEntry.Product2.Name == 'Professional Services Setup Fee'){
                    if(!(newCase.Prof_Services_One_Fee_Billing_Day__c == 'Not_applicable' || newCase.Prof_Services_One_Fee_Payment_Due_Days__c == 'Not_applicable')){
                        payment.Billing_Day__c = newCase.Prof_Services_One_Fee_Billing_Day__c;
                        payment.Payment_Due_Days__c = newCase.Prof_Services_One_Fee_Payment_Due_Days__c;
                        payment.Custom_Billing_Day__c = newCase.Custom_Prof_Service_One_Fee_Billing_Day__c;
                        payment.Custom_Payment_Due_Days__c = newCase.Custom_Prof_Services_One_Fee_Payment__c;
                    } 
                    
                }else if(quoteLineItem.PricebookEntry.Product2.Name == 'Professional Services Maintenance Fee'){
                    if(!(newCase.Prof_Services_Main_Fee_Billing_Day__c == 'Not_applicable' || newCase.Prof_Services_Main_Fee_Payment_Due_Days__c == 'Not_applicable')){
                        payment.Billing_Day__c = newCase.Prof_Services_Main_Fee_Billing_Day__c;
                        payment.Payment_Due_Days__c = newCase.Prof_Services_Main_Fee_Payment_Due_Days__c;
                        payment.Custom_Billing_Day__c = newCase.Custom_Prof_Service_Main_Fee_Billing_Day__c;
                        payment.Custom_Payment_Due_Days__c = newCase.Custom_Prof_Services_Main_Fee_Payment__c;
                    }
                }
                ltPayments.add(payment);
            }

            Map<Id, List<Eligibility__c>> mpElegibility = eligibilityRepository.getEligibilitiesByPayments(mpPaymentES.values());
          
           for(Id oppId : mpPaymentES.keySet()){
                Case newCase = mpCase.get(oppId);
                Payment__c paymentES = mpPaymentES.get(oppId);
                if(mpElegibility.get(paymentES.Id)!= null){
                    Eligibility__c newElegibility = mpElegibility.get(paymentES.Id)[0];
                    newElegibility.Billing_Day__c = (newCase.MF_Eligibility_ES_Billing_Day__c==null? 'Custom' :newCase.MF_Eligibility_ES_Billing_Day__c);
                    newElegibility.Payment_Due_Days__c = newCase.MF_Eligibility_ES_Payment_Due_Days__c;
                    newElegibility.Custom_Billing_Day__c = (newCase.MF_Eligibility_ES_Billing_Day__c==null? 0 : newCase.MF_Eligibility_ES_Custom_Billing_Day__c);
                    newElegibility.Custom_Payment_Due_Days__c = newCase.MF_Eligibility_ES_Custom_Payment_Due_Day__c;          
                    ltEligibility.add(newElegibility); 
                }
               
            }

            paymentRepository.edit(ltPayments);
            eligibilityRepository.edit(ltEligibility);
        }
    }
}