/**
 * @description       : 
 * @author            : AlyssonMota
 * @group             : 
 * @last modified on  : 01-20-2021
 * @last modified by  : Alysson Mota
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   01-20-2021   Alysson Mota   Initial Version
**/
global class AutoConvertLeadsBatch implements Database.Batchable<sObject> {
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        if (!Test.isRunningTest())
            return Database.getQueryLocator([SELECT
                                             Name, Website, Company, Country, CNPJ__c, Email, OwnerId, Finance_Contact_Email__c, Finance_Contact_Name__c, HR_Contact_Email__c, HR_Contact_Name__c, Self_Checkout__c, Industry
                                             FROM
                                             Lead
                                             WHERE
                                             LeadSource = 'Inbound'
                                             AND
                                             Lead_Sub__c = 'Corporate Page'
                                             AND
                                             Status != 'Descartado'
                                             AND
                                             NumberOfEmployees < 1000
                                             AND
                                             (Country = 'Brazil' OR Country = 'United States')
                                             AND
                                             IsConverted = FALSE
                                             AND
                                             Owner.IsActive = TRUE
                                             ]);
        else
            return Database.getQueryLocator([SELECT
                                             Id, Name, Company, Country, CNPJ__c, Email, OwnerId, Finance_Contact_Email__c, Finance_Contact_Name__c, HR_Contact_Email__c, HR_Contact_Name__c, Self_Checkout__c
                                             FROM
                                             Lead
                                             WHERE
                                             LeadSource = 'Inbound'
                                             AND
                                             Lead_Sub__c = 'Corporate Page'
                                             AND
                                             NumberOfEmployees < 1000
                                             AND
                                             IsConverted = FALSE
                                             AND
                                             Country = 'Brazil'
                                             LIMIT
                                             200]);
    }
    
    global void execute(Database.BatchableContext BC, List<Lead> listLeads) {
        List<Database.LeadConvert> listLeadConvert = new List<Database.LeadConvert>();
        List<Contact> contactsToInsert = new List<Contact>();
        List<Contact> contactsToUpdate = new List<Contact>();
        List<Contact> oppsToUpdate = new List<Contact>();
        
        Set<String> setCNPJs = new Set<String>();
        Set<String> setEmails = new Set<String>();
        Set<String> setAccountIds = new Set<String>();
        Set<String> setCountries = new Set<String>();
        Set<String> setNames = new Set<String>();
        Set<String> setIndustry = new Set<String>();
        Set<String> setWebsite = new Set<String>();
        Set<Id> setAccountIdSmb = new Set<Id>();
        Set<Id> setIdContatoFinanceiro = new Set<Id>();
        
        //Map<String, Id> mapCreatedContacts = new Map<String, Id>();                
        Map<String, Lead> mapLeads = new Map<String, Lead>();              
        Map<String, Id> mapCountriesPricebookId = new Map<String, Id>();
        Map<String, Account> mapNameIndustryToAccount = new Map<String, Account>();
        Map<String, Lead> idToLeadMap = new Map<String, Lead>();
        
        for (Lead lead : listLeads) {
        	idToLeadMap.put(lead.Id, lead);
        }
        
        for (Lead lead : listLeads) {
            System.debug('Current lead: ' + lead.Id);
            mapLeads.put(lead.Id, lead);
            
            if (!String.isBlank(lead.CNPJ__c))
                setCNPJs.add(lead.CNPJ__c);
            
            if (!String.isBlank(lead.Email))
                setEmails.add(lead.Email);
            
            if (!String.isBlank(lead.Finance_Contact_Email__c))
                setEmails.add(lead.Finance_Contact_Email__c);
            
            if (!String.isBlank(lead.HR_Contact_Email__c))
                setEmails.add(lead.HR_Contact_Email__c);

            if (!String.isBlank(lead.Company)) {
                setNames.add(lead.Company);
            }
            
            if (!String.isBlank(lead.Industry)) {
                setIndustry.add(lead.Industry);
            }
            
            if (!String.isBlank(lead.Website)) {
                setWebsite.add(lead.Website);
            }
        }
        
        System.debug('mapLeads');
 		System.debug(mapLeads);
        
        System.debug('setEmails');
        System.debug(setEmails);
        
        Integer setCNPJsSize = setCNPJs.size();
        
        List<Account> listAccounts = [SELECT
                                      Id, Name, Phone, BillingCity, BillingCountry, BillingPostalCode, BillingState, BillingStreet, Website, Id_Company__c, Industry 
                                      FROM
                                      Account
                                      WHERE
                                      Id_Company__c IN :setCNPJs OR
                                      (Name IN :setNames AND Industry IN :setIndustry) OR
                                      Website IN :setWebsite
                                     ];
        
        Map<String, Account> mapAccountsNameIndustry = new Map<String, Account>();
        Map<String, Account> mapAccountsCnpj = new Map<String, Account>();
        Map<String, Account> mapAccountsWebsite = new Map<String, Account>();
        Map<String, Account> idToAccountsMap = new Map<String, Account>();
	
        for (Account account : listAccounts) {
            mapAccountsNameIndustry.put(account.Name + '/' + account.Industry, account);
            mapAccountsCnpj.put(account.Id_Company__c, account);
            mapAccountsWebsite.put(account.Website, account);
        }
        
        List<Contact> listContacts = [SELECT
                                      Id, Email, Primary_HR_Contact__c, Finance_Contact__c, AccountId
                                      FROM
                                      Contact
                                      WHERE
                                      Email IN : setEmails];
        
        Map<String, Contact> mapContacts = new Map<String, Contact>();
        
        for (Contact contact : listContacts)
            mapContacts.put(contact.Email, contact);
        
        System.debug('mapContacts');
        System.debug(mapContacts);
        
        for (Lead lead : listLeads) {
            Database.LeadConvert leadConvert = new Database.LeadConvert();
            
            // CONTA EXISTE
            if (mapAccountsCnpj.get(lead.CNPJ__c) != null) {
           		Account leadAccount = mapAccountsCnpj.get(lead.CNPJ__c);
               	leadConvert.setAccountId(leadAccount.Id);
                
            }
            
            if (mapAccountsWebsite.get(lead.Website) != null) {
                Account leadAccount = mapAccountsWebsite.get(lead.Website);
               	leadConvert.setAccountId(leadAccount.Id);
            }
            
            if (mapAccountsNameIndustry.get(lead.Company + '/' + lead.Industry) != null) {
                Account leadAccount = mapAccountsNameIndustry.get(lead.Company + '/' + lead.Industry);
                leadConvert.setAccountId(leadAccount.Id);
            }
            
            // CONTATO EXISTE
            if (mapContacts.get(lead.Email) != null) {
                System.debug('achou contato');
                Contact leadContact = mapContacts.get(lead.Email);
                leadConvert.setContactId(leadContact.Id);
                
               	Account account = [
                    SELECT Id
                    FROM Account
                    WHERE Id = :leadContact.AccountId
                ].get(0);
                
                leadConvert.setAccountId(account.Id);
            }
            
            leadConvert.setOwnerId(lead.OwnerId);
            leadConvert.setDoNotCreateOpportunity(FALSE);
            leadConvert.setConvertedStatus('Convertido');
            leadConvert.setLeadId(lead.Id);
            
            listLeadConvert.add(leadConvert);
        }

        List<Database.LeadConvertResult> listLeadConvertResult = null;
        
        try {
            listLeadConvertResult = Database.convertLead(listLeadConvert, false);
        } catch (Exception e) {
            System.debug('The following exception occurred while trying to convert the Lead: ' + e);
            Utils.exceptionDebugger(e);
        }
        
        if (listLeadConvertResult != null) {
            System.debug('listLeadConvertResult != null');
            if (listLeadConvertResult[0].isSuccess()) {
            	System.debug('Convert Success');
                System.debug(listLeadConvertResult[0].getContactId());
            	System.debug(listLeadConvertResult[0].getAccountId());
            	System.debug(listLeadConvertResult[0].getOpportunityId());
            	Map<String, Id> mapCreatedContacts = updateConvertedAccountAsSMB(listLeadConvertResult, mapLeads);
            	mapCreatedContacts = updateContacts(listLeadConvertResult, mapCreatedContacts, mapContacts, mapLeads);
            	updateOpportunities(listLeadConvertResult, mapCreatedContacts, mapLeads);
            } else {
				System.debug('Convert Error');
                System.debug(listLeadConvertResult[0].getErrors());
                
				List<Database.Error> errors = listLeadConvertResult[0].getErrors();
                
                for (Database.Error error : errors) {
                	if (error instanceof Database.DuplicateError) {
                		Database.DuplicateError duplicateError = (Database.DuplicateError)error;
                    	Datacloud.DuplicateResult duplicateResult = duplicateError.getDuplicateResult();
                  		System.debug('*****');
                    	System.debug(duplicateResult.getDuplicateRuleEntityType());
                        
                        String entityType = duplicateResult.getDuplicateRuleEntityType();
                        Id recordId = null;
                        
                        List<DataCloud.MatchResult> mList = duplicateResult.getMatchResults(); 
                        for (DataCloud.MatchResult m : mList) {
                        	System.debug(m.getMatchRecords());
                            System.debug(m.getMatchRecords()[0].getRecord().Id);
      						recordId = m.getMatchRecords()[0].getRecord().Id;
                            break;
                        }
                        
                       	Id leadId = listLeadConvertResult[0].getLeadId();
                        
                        if (entityType == 'Contact' && recordId != null) {
                        	Database.LeadConvert leadConvertForContact = new Database.LeadConvert();
                            Lead leadRetryConversion = idToLeadMap.get(leadId);
							//Account leadAccount = null;
                            
                            /*
                             //if (leadAccount == null) {
                            Contact contact = [
                            	SELECT Id, Name, AccountId, Account.Name
                               	FROM Contact
                              	WHERE Id = :recordId
                           	].get(0);
                                
								leadConvertForContact.setAccountId(contact.AccountId);
                                
                               
                                leadRetryConversion.Company = contact.Account.Name;
                                update leadRetryConversion;
							

                           		 }
                        	*/
                            
                            /*
                            // CONTA EXISTE
            				if (mapAccountsCnpj.get(leadRetryConversion.CNPJ__c) != null) {
           						leadAccount = mapAccountsCnpj.get(leadRetryConversion.CNPJ__c);
               					leadConvertForContact.setAccountId(leadAccount.Id);
            				}
            
            				if (mapAccountsWebsite.get(leadRetryConversion.Website) != null) {
                				leadAccount = mapAccountsWebsite.get(leadRetryConversion.Website);
               					leadConvertForContact.setAccountId(leadAccount.Id);
            				}
            
            				if (mapAccountsNameIndustry.get(leadRetryConversion.Company + '/' + leadRetryConversion.Industry) != null) {
                				leadAccount = mapAccountsNameIndustry.get(leadRetryConversion.Company + '/' + leadRetryConversion.Industry);
                				leadConvertForContact.setAccountId(leadAccount.Id);
            				}
							*/
                            
                            leadConvertForContact.setContactId(recordId);
                            leadConvertForContact.setOwnerId(leadRetryConversion.OwnerId);
            				leadConvertForContact.setDoNotCreateOpportunity(FALSE);
            				leadConvertForContact.setConvertedStatus('Convertido');
            				leadConvertForContact.setLeadId(leadRetryConversion.Id);
                            //leadConvertForContact.setAccountId(leadAccount.Id);
                            
                            List<Contact> contactList = [
                            	SELECT Id, Name, AccountId, Account.Name
                              	FROM Contact
                               	WHERE Id = :recordId
                           	];

                            if (contactList.size() > 0) {
                            	Contact contact = contactList.get(0);
                            	leadConvertForContact.setAccountId(contact.AccountId);    
                            }
                            
                            try {
            					Database.LeadConvertResult contactLeadConvertResult = Database.convertLead(leadConvertForContact, false);
        					
                            	if (contactLeadConvertResult.isSuccess()) {
                                    System.debug('contactRetryConversion Success');
                                } else {
                                	System.debug('contactRetryConversion Error');
                					System.debug(contactLeadConvertResult.getErrors());
                                }
                            } catch (Exception e) {
                                System.debug('Contact retry');
            					System.debug('The following exception occurred while trying to reconvert the Lead: ' + e);
            					Utils.exceptionDebugger(e);
        					}
                        }
                        
                        if (entityType == 'Account' && recordId != null) {
                       		Database.LeadConvert leadConvertForAccount = new Database.LeadConvert();
                            Lead leadRetryConversion = idToLeadMap.get(leadId);
                            System.debug('leadRetryConversion.email');
                            System.debug(leadRetryConversion.email);
                            
                            // CONTATO EXISTE
            				if (mapContacts.get(leadRetryConversion.Email) != null) {
                				System.debug('achou contato');
                				Contact leadContact = mapContacts.get(leadRetryConversion.Email);
                				leadConvertForAccount.setContactId(leadContact.Id);
            				}
                            
                            leadConvertForAccount.setAccountId(recordId);
                            leadConvertForAccount.setOwnerId(leadRetryConversion.OwnerId);
            				leadConvertForAccount.setDoNotCreateOpportunity(FALSE);
            				leadConvertForAccount.setConvertedStatus('Convertido');
            				leadConvertForAccount.setLeadId(leadRetryConversion.Id);
                            
                            try {
            					Database.LeadConvertResult accountLeadConvertResult = Database.convertLead(leadConvertForAccount, false);
                            	
                                if (accountLeadConvertResult.isSuccess()) {
                                    System.debug('acountRetryConversion Success');
                                } else {
                                	System.debug('acountRetryConversion Error');
                					System.debug(accountLeadConvertResult.getErrors());
                                }
                                
                                
        					} catch (Exception e) {
                                System.debug('Account retry');
            					System.debug('The following exception occurred while trying to reconvert the Lead: ' + e);
            					Utils.exceptionDebugger(e);
        					}
                        }
                	}    
               	}
            }
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        if (!Test.isRunningTest()) {
            Datetime sysTime = System.now().addMinutes(5);
            String chron_exp = '' + sysTime.second() + ' ' + sysTime.minute() + ' ' + sysTime.hour() + ' ' + sysTime.day() + ' ' + sysTime.month() + ' ? ' + sysTime.year();
            String strName = 'AutoConvertLeadsBatchSch ' + sysTime.day() + '/' + sysTime.month() + '/' + sysTime.year() + ' - '+ sysTime.hour() + ':' + sysTime.minute() + ':' + sysTime.second();
            //System.schedule(strName, chron_exp, new AutoConvertLeadsBatchSch());
        }
    }

    public static Map<String, Id> updateConvertedAccountAsSMB(List<Database.LeadConvertResult> listLeadConvertResult, Map<String,Lead> mapLeads) {
        Map<String, Id> mapCreatedContacts = new Map<String, Id>();
        Set<Id> accountIdSet = new Set<Id>();              
        
        for (Database.LeadConvertResult leadConvertResult :  listLeadConvertResult) {
            if (leadConvertResult.isSuccess() == TRUE) {
                Id leadId = leadConvertResult.getLeadId();
                Lead lead = mapLeads.get(leadId);
                        
                Id mainLeadContactId = leadConvertResult.getContactId();
                String mainLeadContactEmail = lead.Email;
                        
                mapCreatedContacts.put(mainLeadContactEmail, mainLeadContactId);

                accountIdSet.add(leadConvertResult.getAccountId());
            }
        }

        List<Account> accounts = [
            SELECT Id, Business_Unit__c
            FROM Account
            WHERE Id IN :accountIdSet
        ];

        for (Account acc : accounts) {
            acc.Business_Unit__c = 'SMB';
        }
		
        try {
           update accounts; 
        } catch (Exception e) {
            System.debug('The following exception occurred while updating accounts Lead: ' + e);
            Utils.exceptionDebugger(e);
        }

        return mapCreatedContacts;
    }

    public static Map<String,Id> updateContacts(List<Database.LeadConvertResult> listLeadConvertResult, Map<String,Id> mapCreatedContacts, Map<String,Contact> mapContacts, Map<String, Lead> mapLeads) {
        List<Contact> contactsToUpdate = new List<Contact>();
        List<Contact> contactsToInsert = new List<Contact>();

        for (Database.LeadConvertResult leadConvertResult : listLeadConvertResult) {
            Boolean isSuccess = leadConvertResult.isSuccess();
            
            if (isSuccess) {
                Id leadId = leadConvertResult.getLeadId();
                Lead lead = mapLeads.get(leadId);
                Contact contatoFinanceiro = new Contact();
                Contact contatoRH = new Contact();
                
                String contatoFinanceiroId = '';
                String contatoRHId = '';

                // Finance
                if (mapContacts.get(lead.Finance_Contact_Email__c) == null) {
                    if (!String.isBlank(lead.Finance_Contact_Name__c)) { 
                        if (mapCreatedContacts.get(lead.Finance_Contact_Email__c) != null) {
                            contatoFinanceiroId = mapCreatedContacts.get(lead.Finance_Contact_Email__c);
                            
                            Contact contatoFinanceiroToUpdate = new Contact();
                            contatoFinanceiroToUpdate.Id = contatoFinanceiroId;
                            contatoFinanceiroToUpdate.Finance_Contact__c = TRUE;
                           
                            contactsToUpdate.add(contatoFinanceiroToUpdate);
                        } else {
                            List<String> leadsName = lead.Finance_Contact_Name__c.split(' ');
                            
                            String firstName = '';
                            String lastName = '';
                            
                            if (leadsName.size() > 1) {
                                firstName = leadsName[0];
                                lastName = lead.Finance_Contact_Name__c.replace(leadsName[0], '');
                            } else {
                                lastName = lead.Finance_Contact_Name__c;
                            }
                            
                            contatoFinanceiro.Email = lead.Finance_Contact_Email__c;
                            contatoFinanceiro.AccountId = leadConvertResult.getAccountId();
                            contatoFinanceiro.FirstName = firstName;
                            contatoFinanceiro.LastName = lastName;
                            contatoFinanceiro.Area_Setor__c = 'Finance';
                            contatoFinanceiro.Finance_Contact__c = true;
                            contatoFinanceiro.DecisionMaker__c = 'Yes';
                        
                            contactsToInsert.add(contatoFinanceiro);
                            contatoFinanceiroId = contatoFinanceiro.Id;
                            mapCreatedContacts.put(contatoFinanceiro.Email, contatoFinanceiro.Id);
                        }
                    }
                } else {
                    contatoFinanceiro = mapContacts.get(lead.Finance_Contact_Email__c);
                    
                    if (contatoFinanceiro.Finance_Contact__c == FALSE) {
                        contatoFinanceiro.Finance_Contact__c = TRUE;
                        
                        contactsToUpdate.add(contatoFinanceiro);
                    }
                }

                // HR
                if (mapContacts.get(lead.HR_Contact_Email__c) == null) {
                    if (!String.isBlank(lead.HR_Contact_Name__c)) {
                        if (mapCreatedContacts.get(lead.HR_Contact_Email__c) != null) {
                            contatoRHId = mapCreatedContacts.get(lead.HR_Contact_Email__c);
                            
                            Contact contatoRHToUpdate = new Contact();
                            contatoRHToUpdate.Id = contatoRHId;
                            contatoRHToUpdate.Primary_HR_Contact__c = TRUE;
                            
                            contactsToUpdate.add(contatoRHToUpdate);                                                    
                        } else {
                            List<String> leadsName = lead.HR_Contact_Name__c.split(' ');
                            
                            String firstName = '';
                            String lastName = '';
                            
                            if (leadsName.size() > 1) {
                                firstName = leadsName[0];
                                lastName = lead.HR_Contact_Name__c.replace(leadsName[0], '');
                            } else {
                                lastName = lead.HR_Contact_Name__c;
                            }
                            
                            contatoRH.Email = lead.HR_Contact_Email__c;
                            contatoRH.AccountId = leadConvertResult.getAccountId();
                            contatoRH.FirstName =  firstName;
                            contatoRH.LastName = lastName;
                            contatoRH.Area_Setor__c = 'Human Resources';
                            contatoRH.Primary_HR_Contact__c = true;
                            contatoRH.DecisionMaker__c = 'Yes';
                            
                            contactsToInsert.add(contatoRH);
                            
                            contatoRHId = contatoRH.Id;
                            mapCreatedContacts.put(contatoRH.Email, contatoRH.Id);
                        }
                    }
                } else {
                    contatoRH = mapContacts.get(lead.HR_Contact_Email__c);
                    
                    if (contatoRH.Primary_HR_Contact__c == FALSE) {
                        contatoRH.Primary_HR_Contact__c = TRUE;
                        
                        contactsToUpdate.add(contatoRH);
                    }
                }
            }
        }
		
        try {
        	update contactsToUpdate;
       	} catch (Exception e) {
       		Utils.exceptionDebugger(e);
       	}
		
		try {        
        	insert contactsToInsert;
        } catch (Exception e) {
       		Utils.exceptionDebugger(e);
       	}
        
        return mapCreatedContacts;
    }
    
    public static void updateOpportunities(List<Database.LeadConvertResult> listLeadConvertResult, Map<String,Id> mapCreatedContacts, Map<String, Lead> mapLeads) {
        Set<String> countrySet = new Set<String>();
        Map<String, Pricebook2> countryToPricebookMap = new Map<String, Pricebook2>();
        List<Opportunity> oppsToUpdate = new List<Opportunity>();
        Map<Id, Id> oppIdToLeadIdMap = new Map<Id, Id>();
        List<Id> oppsIdToCreateQuote = new List<Id>();
        
        for (Database.LeadConvertResult leadConvertResult :  listLeadConvertResult) {
            if (leadConvertResult.isSuccess() == TRUE) {
                Id leadId = leadConvertResult.getLeadId();
                Lead lead = mapLeads.get(leadId);
                countrySet.add(lead.Country);
            }
        }
        
        List<Pricebook2> listPricebooks = [SELECT Id, Country__c  FROM Pricebook2 WHERE IsActive = TRUE AND Country__c =: countrySet LIMIT 1];
        
        for (Pricebook2 pricebook : listPricebooks) {
            countryToPricebookMap.put(pricebook.Country__c, pricebook);
        }
        
        for (Database.LeadConvertResult leadConvertResult :  listLeadConvertResult) {
            if (leadConvertResult.isSuccess() == TRUE) {
                Id leadId = leadConvertResult.getLeadId();
                Id OpportunityId = leadConvertResult.getOpportunityId();
                oppIdToLeadIdMap.put(OpportunityId, leadId);
                
                Lead lead = mapLeads.get(leadId);
                        
   				Opportunity opportunity = new Opportunity();
             	opportunity.Id = OpportunityId;
                opportunity.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('SMB').getRecordTypeId();
                opportunity.Pricebook2Id = countryToPricebookMap.get(lead.Country).Id;
                opportunity.StageName = (lead.Country == 'Brazil'? 'Proposta Enviada' : 'Oportunidade Validada Após 1ª Reunião');
                opportunity.CurrencyIsoCode = 'BRL';
                opportunity.Self_Checkout__c = lead.Self_Checkout__c;
                
                Id contatoFinanceiroId = mapCreatedContacts.get(lead.Finance_Contact_Email__c);
                Id contatoRHId = mapCreatedContacts.get(lead.HR_Contact_Email__c);
                                    
                if (!String.isBlank(contatoFinanceiroId))
                	opportunity.Pontos_de_contato_financeiro_no_cliente__c = contatoFinanceiroId;
                                
              	if (!String.isBlank(contatoRHId))
                	opportunity.Pontos_de_contato_RH_no_cliente__c = contatoRHId;
                
                oppsToUpdate.add(opportunity);
                                    
            }
        }
       	
        List<Database.SaveResult> oppSaveResultList = new List<Database.SaveResult>();
        try {
        	oppSaveResultList = Database.update(oppsToUpdate);
        } catch (Exception e) {
       		Utils.exceptionDebugger(e);
       	}
        
        for (Database.SaveResult saveResult : oppSaveResultList) {
            if (saveResult.isSuccess()) {
            	Id oppId = saveResult.getId();
            	Id leadId = oppIdToLeadIdMap.get(oppId);
            
            	if (leadId != null) {
            		Lead lead = mapLeads.get(leadId);
                
                	if (lead.Country == 'Brazil') {
                    	oppsIdToCreateQuote.add(oppId);
                	}
            	}    
            }
        }
        
        if (oppsIdToCreateQuote.size() > 0) {
        	createQuoteAndQuoteLineItemForOpps(oppsIdToCreateQuote);
        }
    }
    
    public static void createQuoteAndQuoteLineItemForOpps(List<Id> oppsIdToCreateQuote) {
    	List<Quote> quotesToInsert = new List<Quote>();
        List<Id> quoteIdList = new List<Id>();
        
        List<Opportunity> listOpportunities = [
            SELECT Id, Name, Pontos_de_contato_RH_no_cliente__c, Pontos_de_contato_financeiro_no_cliente__c, SyncedQuoteId, TotalOpportunityQuantity, Pricebook2Id
            FROM Opportunity
            WHERE Id IN :oppsIdToCreateQuote
     	];
        
        List<Copay_Plan__c> listCopayPlans = [
            SELECT Id
           	FROM Copay_Plan__c
			WHERE Name = 'Brasil Padrão'
			LIMIT 1
        ];
        
        for (Opportunity opportunity : listOpportunities) {
            Quote quote = new Quote();
            quote.RecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByName().get('Direct Channel').getRecordTypeId();
            quote.OpportunityId = opportunity.Id;
            quote.Name = opportunity.Name;
            quote.Are_Family_Members_Included2__c = 'No';
            quote.License_Fee_Waiver__c= 'No';
            quote.Setup_Fee__c = 0;
            quote.License_Fee_Frequency__c = 'Monthly';
            quote.LF_PO_Number_Required__c = 'No';
            quote.MB_PO_Number_Required__c = 'No';
            quote.Payment_Method_License_Fee__c = 'Boleto Bancário';
            quote.Payment_Terms_for_Setup_Fee__c = 0;
            quote.Contact_Permission__c = 'Allowlist';
            quote.License_Fee_cut_off_date__c = 1;
            quote.Employee_Company_ID__c = true;
            quote.Employee_Corporate_Email__c = true;
            quote.Employee_Name__c = true;
            quote.Employee_National_ID_if_applicable__c = true;
            quote.Submission_Date__c = Date.today().addDays(30);
            quote.Total_Setup_Fee_Value__c = 0;
            quote.Payment_method__c = 'Credit Card';
            quote.Employee_Registration_Method__c = 'Eligible file';
            quote.Unique_Identifier__c =  'National ID';
            quote.Administrator__c = opportunity.Pontos_de_contato_RH_no_cliente__c;
            quote.Finance__c = opportunity.Pontos_de_contato_financeiro_no_cliente__c;
            quote.Operations__c = opportunity.Pontos_de_contato_RH_no_cliente__c;
            quote.ContactId = opportunity.Pontos_de_contato_RH_no_cliente__c;
            quote.Pricebook2Id = opportunity.pricebook2Id;
            
            if(!listCopayPlans.isEmpty())
                quote.Gym__c = listCopayPlans[0].Id;
            
            quotesToInsert.add(quote);
        }
        
        List<Database.SaveResult> saveQuoteResultList = new List<Database.SaveResult>();
        
        try {
        	saveQuoteResultList = Database.INSERT(quotesToInsert);    
        } catch (Exception e) {
        	Utils.exceptionDebugger(e);
      	}
        
        
        for (Database.SaveResult saveResult : saveQuoteResultList) {
            if (saveResult.isSuccess()) {
                quoteIdList.add(saveResult.getId());
            } 
        }
        	
        createQuoteLineItemsForQuotes(quoteIdList);
    }
    
    public static void createQuoteLineItemsForQuotes(List<Id> quoteIdList) {
       	List<QuoteLineItem> quoteLineItensToInsert = new List<QuoteLineItem>();
        Set<String> planCodeSet = new Set<String>{'1A100','100A1000'};
        Map<String, PricebookEntry> planToCodeToPricebookEntryMap = new Map<String, PricebookEntry>();
        
        List<Quote> quotes = [
            SELECT Id, Opportunity.TotalOpportunityQuantity, Opportunity.Pricebook2Id
            FROM Quote
            WHERE Id IN :quoteIdList
        ];
        
        List<PricebookEntry> listPricebookEntries = [ 
            SELECT Id, Product2Id, ProductCode
			FROM PricebookEntry
            WHERE ProductCode IN :planCodeSet
     	];
        
        for (PricebookEntry pricebookEntry : listPricebookEntries) {
            if (!planToCodeToPricebookEntryMap.containsKey(pricebookEntry.ProductCode)) {
                planToCodeToPricebookEntryMap.put(pricebookEntry.ProductCode, pricebookEntry);
            }
        }
        
        for (Quote quote : quotes) {
           	QuoteLineItem quoteLineItem = new QuoteLineItem();
            quoteLineItem.QuoteId = quote.Id;
            quoteLineItem.UnitPrice = 30;
            quoteLineItem.Quantity = quote.Opportunity.TotalOpportunityQuantity;
            quoteLineItem.PricebookEntryId = quote.Opportunity.Pricebook2Id;
            quoteLineItem.Flagship_Date__c = Date.today();
            quoteLineItem.Contract_Type__c = 'Flat_Fee';
            quoteLineItem.Max_Total_Montly_Investment_Cofee__c = 30;
            quoteLineItem.Min_Total_Montly_Investment_Cofee__c = 30;
            quoteLineItem.Target_Enrollment_Rate__c = 100;
            quoteLineItem.Description = 'Colaborador';
            quoteLineItem.Type__c = 'Employee';
            
            if (quote.Opportunity.TotalOpportunityQuantity < 100) {
            	quoteLineItem.Product2Id = planToCodeToPricebookEntryMap.get('1A100').Id;    
            } else {
                quoteLineItem.Product2Id = planToCodeToPricebookEntryMap.get('100A1000').Id;  
            }
            
            quoteLineItensToInsert.add(quoteLineItem);
        }
        
        try {
           insert quoteLineItensToInsert; 
        } catch (Exception e) {
        	Utils.exceptionDebugger(e);
      	}
    }
    
    public static void convertLeadForDuplicateAccountRule() {
        
    }
    
    
}