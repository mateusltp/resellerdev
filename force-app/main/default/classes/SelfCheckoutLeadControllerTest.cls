@IsTest
public class SelfCheckoutLeadControllerTest {
    @IsTest
    public static void execute() {
        SelfCheckoutLeadRequest selfCheckoutLeadRequest = SelfCheckoutLeadRequestMock.getMock();
    
        RestRequest request = new RestRequest();
        request.requestURI = '/services/apexrest/self-checkout-lead';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf(JSON.serialize(selfCheckoutLeadRequest));
    
        Test.startTest();
    
        RestContext.response = new RestResponse();
        RestContext.request = request;
    
        RestResponse response = new SelfCheckoutLeadController().post();
    
        Test.stopTest();
    
        System.assertEquals(
            204,
            response.statusCode,
            'Error Message: ' + response?.responseBody?.toString()
        );
    }

    @IsTest
    public static void executeWithNoBody() {
        RestRequest request = new RestRequest();
        request.requestURI = '/services/apexrest/self-checkout-lead';
        request.httpMethod = 'POST';
    
        Test.startTest();
    
        RestContext.response = new RestResponse();
        RestContext.request = request;
    
        RestResponse response = new SelfCheckoutLeadController().post();
    
        Test.stopTest();
    
        System.assertEquals(
            400,
            response.statusCode,
            'With no request body the status code must be 400'
        );
    }

    @IsTest
    public static void executeWithInvalidBody() {
      RestRequest request = new RestRequest();
      request.requestURI = '/services/apexrest/self-checkout-lead';
      request.httpMethod = 'POST';
      request.requestBody = Blob.valueOf('{]');
  
      Test.startTest();
  
      RestContext.response = new RestResponse();
      RestContext.request = request;
  
      RestResponse response = new SelfCheckoutLeadController().post();
  
      Test.stopTest();
  
      System.assertEquals(
        400,
        response.statusCode,
        'With a invalid request body the status code must be 400'
      );
    }
}