public without sharing class TagusAccountOutboundPublisher {
  private final String CREATE_EVENT_NAME = 'CREATE_ACCOUNT_ON_TAGUS';
  private final String UPDATE_EVENT_NAME = 'UPDATE_ACCOUNT_ON_TAGUS';

  private Map<Id, Account> accountMap;

  public TagusAccountOutboundPublisher(List<Account> accounts) {
    setAccountMap(accounts);
  }

  private void setAccountMap(List<Account> accounts) {
    accounts = queryAccounts(accounts);

    Set<Id> parentAccountIds = new Set<Id>();

    for (Account account : accounts) {
      if (String.isNotBlank(account.ParentId)) {
        parentAccountIds.add(account.ParentId);
      }
    }

    List<Account> accountsToPublish = new List<Account>();

    for (Account account : accounts) {
      if (!parentAccountIds.contains(account.Id)) {
        accountsToPublish.add(account);

        if (String.isNotBlank(account.ParentId)) {
          Account parentAccount = new Account(Id = account.ParentId);

          accountsToPublish.add(parentAccount);
        }
      }
    }

    this.accountMap = AccountTagusDTO.getAccountMapWithFieldsToParse(
      accountsToPublish
    );
  }

  private List<Account> queryAccounts(List<Account> accounts) {
    return [
      SELECT Id, ParentId
      FROM Account
      WHERE Id IN :accounts
      WITH SECURITY_ENFORCED
    ];
  }

  public EventQueue runCreate() {
    return sendEvent(CREATE_EVENT_NAME);
  }

  public EventQueue runUpdate() {
    return sendEvent(UPDATE_EVENT_NAME);
  }

  private EventQueue sendEvent(String eventName) {
    EventQueue event = new EventBuilder()
      .createEventFor(eventName)
      .withSender('SALESFORCE')
      .withReceiver('TAGUS')
      .buildEvent();

    event.addPayload(eventName, JSON.serialize(accountMap));

    event.save();

    return event;
  }
}