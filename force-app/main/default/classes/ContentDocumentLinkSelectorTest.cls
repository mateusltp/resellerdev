/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 05-02-2022
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/
@IsTest
public with sharing class ContentDocumentLinkSelectorTest {

    @TestSetup
    static void setupData(){        
        PS_Constants constants = PS_Constants.getInstance();  
        String recordTypePartnerFlow = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(constants.ACCOUNT_RT_PARTNER_FLOW_ACCOUNT).getRecordTypeId();
        Account partnerAcc = PartnerDataFactory.newAccount();
        partnerAcc.RecordTypeId = recordTypePartnerFlow;
        partnerAcc.CRM_ID__c = '1231';
        partnerAcc.Legal_Document_Type__c = 'CNPJ';
        partnerAcc.Legal_Title__c = 'ContentDocument';
        partnerAcc.BillingCountryCode = 'US';
        partnerAcc.BillingCountry = 'United States';
        partnerAcc.BillingState = 'Arizona';
        partnerAcc.BillingPostalCode = '1223';
        partnerAcc.ShippingCountryCode = 'US';
        partnerAcc.ShippingCountry = 'United States';
        partnerAcc.ShippingState = 'Arizona';
        partnerAcc.CurrencyIsoCode = 'USD';
        partnerAcc.Description = 'dasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasda';
        INSERT partnerAcc;
        
        List<ContentVersion> lstContentVersion = new List<ContentVersion>();
        ContentVersion contentVersion_1 = new ContentVersion(
            Title = 'photo 1',
            PathOnClient = '/logoPenguins1.jpg',
            VersionData = Blob.valueOf('Unit Test PartnerSetupController Body 1'),
            origin = 'H',
            Type_Files_fileupload__c = 'Photo'    
        );
        lstContentVersion.add(contentVersion_1);

        ContentVersion contentVersion_2 = new ContentVersion(
            Title = 'photo 2',
            PathOnClient = '/logoPenguins2.jpg',
            VersionData = Blob.valueOf('Unit Test PartnerSetupController Body 2'),
            origin = 'H',
            Type_Files_fileupload__c = 'Photo'    
        );
        lstContentVersion.add(contentVersion_2);

        ContentVersion contentVersion_3 = new ContentVersion(
            Title = 'photo 3',
            PathOnClient = '/logoPenguins3.jpg',
            VersionData = Blob.valueOf('Unit Test PartnerSetupController Body 3'),
            origin = 'H',
            Type_Files_fileupload__c = 'Photo'    
        );
        lstContentVersion.add(contentVersion_3);

        ContentVersion contentVersion_4 = new ContentVersion(
            Title = 'LOGO',
            PathOnClient = '/gymlogo.jpg',
            VersionData = Blob.valueOf('Unit Test PartnerSetupController Logo'),
            origin = 'H',
            Type_Files_fileupload__c = 'Logo'    
        );

        lstContentVersion.add(contentVersion_4);

        ContentVersion contentVersion_5 = new ContentVersion(
            Title = 'W9',
            PathOnClient = '/W9.pdf',
            VersionData = Blob.valueOf('Unit Test PartnerSetupController w9'),
            origin = 'H',
            Type_Files_fileupload__c = 'W9'    
        );

        lstContentVersion.add(contentVersion_5);
        INSERT lstContentVersion;


        Id ContentDocumentId_1 = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: contentVersion_1.Id].ContentDocumentId;
        Id ContentDocumentId_2 = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: contentVersion_2.Id].ContentDocumentId;
        Id ContentDocumentId_3 = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: contentVersion_3.Id].ContentDocumentId;
        Id ContentDocumentId_4 = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: contentVersion_4.Id].ContentDocumentId;
        Id ContentDocumentId_5 = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: contentVersion_5.Id].ContentDocumentId;

        List<ContentDocumentLink> lstContentLink = new List<ContentDocumentLink>();
        ContentDocumentLink ContentDLAcc1 = new ContentDocumentLink();
        ContentDLAcc1.ContentDocumentId = ContentDocumentId_1;
        ContentDLAcc1.LinkedEntityId = partnerAcc.Id;
        lstContentLink.add(ContentDLAcc1);

        ContentDocumentLink ContentDLAcc2 = new ContentDocumentLink();
        ContentDLAcc2.ContentDocumentId = ContentDocumentId_2;
        ContentDLAcc2.LinkedEntityId = partnerAcc.Id;
        lstContentLink.add(ContentDLAcc2);

        ContentDocumentLink ContentDLAcc3 = new ContentDocumentLink();
        ContentDLAcc3.ContentDocumentId = ContentDocumentId_3;
        ContentDLAcc3.LinkedEntityId = partnerAcc.Id;
        lstContentLink.add(ContentDLAcc3);
       
        ContentDocumentLink ContentDLAcc4 = new ContentDocumentLink();
        ContentDLAcc4.ContentDocumentId = ContentDocumentId_4;
        ContentDLAcc4.LinkedEntityId = partnerAcc.Id;
        lstContentLink.add(ContentDLAcc4);

        ContentDocumentLink ContentDLAcc5 = new ContentDocumentLink();
        ContentDLAcc5.ContentDocumentId = ContentDocumentId_5;
        ContentDLAcc5.LinkedEntityId = partnerAcc.Id;
        lstContentLink.add(ContentDLAcc5);
        
        INSERT lstContentLink;
    }

    @IsTest
    private static void getSObjectType_Success() {
        Test.startTest();
            System.assertEquals(new ContentDocumentLink().getSObjectType(), ContentDocumentLink.sObjectType );
        Test.stopTest();
	}

    @IsTest
    private static void getSObjectFieldList_Success() {
        Test.startTest();
            List<Schema.SObjectField> lstExpected = new List<Schema.SObjectField>{ ContentDocumentLink.Id,
                                                                                    ContentDocumentLink.ContentDocumentId,
                                                                                    ContentDocumentLink.ShareType,
                                                                                    ContentDocumentLink.LinkedEntityId };

            System.assertEquals(new ContentDocumentLinkSelector().getSObjectFieldList(), lstExpected );
        Test.stopTest();
	}

    @IsTest
    private static void selectById_Success() {
        Account acc = [ SELECT ID, Name,Gym_Email__c  FROM Account WHERE Legal_Title__c LIKE 'ContentDocument' LIMIT 1];
        ContentDocumentLink contentDocLink = [SELECT ID,LinkedEntityId FROM ContentDocumentLink WHERE  LinkedEntityId =: acc.Id LIMIT 1 ];         
        Test.startTest();
            System.assert(new ContentDocumentLinkSelector().selectById( new Set<Id>{contentDocLink.Id}).size() > 0);
        Test.stopTest();
	}

    @IsTest
    private static void selectByContentDocumentId_Success() {
        Account acc = [ SELECT ID, Name,Gym_Email__c  FROM Account WHERE Legal_Title__c LIKE 'ContentDocument' LIMIT 1];
        ContentDocumentLink contentDocLink = [SELECT ID,LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE  LinkedEntityId =: acc.Id LIMIT 1];    

        Test.startTest();
            System.assert(new ContentDocumentLinkSelector().selectByContentDocumentId( new Set<Id>{contentDocLink.ContentDocumentId}).size() > 0);
        Test.stopTest();
	}

    @IsTest
    private static void selectByLinkedEntityId_Success() {
        Account acc = [ SELECT ID, Name,Gym_Email__c  FROM Account WHERE Legal_Title__c LIKE 'ContentDocument' LIMIT 1];
        ContentDocumentLink contentDocLink = [SELECT ID,LinkedEntityId FROM ContentDocumentLink WHERE  LinkedEntityId =: acc.Id LIMIT 1];         
        Test.startTest();
            System.assert(new ContentDocumentLinkSelector().selectByLinkedEntityId( new Set<Id>{acc.Id}).size() > 0);
        Test.stopTest();
	}
}