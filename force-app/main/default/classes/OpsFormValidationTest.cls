/**
 * @File Name          : OpsFormValidationTest.cls
 * @Description        : 
 * @Author             : David Mantovani - DDMA@gft.com
 * @Group              : 
 * @Last Modified By   : David Mantovani - DDMA@gft.com
 * @Last Modified On   : 07-24-2020 
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    29/06/2020   Samuel Silva - GFT (slml@gft.com)     Initial Version
**/
@Istest
public with sharing class OpsFormValidationTest {
    
    @TestSetup
    static void Setup(){      
        
        Id recordTypeIdAccount =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gym_Partner').getRecordTypeId();
        Id recordTypeIdSmall = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Gyms_Small_Medium_Partner').getRecordTypeId();
        Id recordTypeIdWishList = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Gyms_Wishlist_Partner').getRecordTypeId();
        Id recordTypeIdQuote = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Gyms_Quote_Partner').getRecordTypeId();
        Id recordTypeIdBank = Schema.SObjectType.Bank_Account__c.getRecordTypeInfosByDeveloperName().get('Gyms_Bank_Account').getRecordTypeId(); 
        Id recordTypeIdBankSecond = Schema.SObjectType.Bank_Account__c.getRecordTypeInfosByDeveloperName().get('Gyms_Manage_Bank_Account').getRecordTypeId(); 
        Id recordTypeIdAccBank = Schema.SObjectType.Acount_Bank_Account_Relationship__c.getRecordTypeInfosByDeveloperName().get('Gyms_Account_Bank_Account_Partner').getRecordTypeId();    
        Id recordTypeIdConBank = Schema.SObjectType.Contact_Bank_Account_Relationship__c.getRecordTypeInfosByDeveloperName().get('Gyms_Contact_Bank_Account_Partner').getRecordTypeId();
        Id recordTypeIdContact = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Gyms_Partner').getRecordTypeId();
        Id recordTypeIdProductItem = Schema.SObjectType.Product_Item__c.getRecordTypeInfosByDeveloperName().get('Gyms_Product_Item_Partner').getRecordTypeId();
        Id recordTypeIdPayment = Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Gyms_Payment').getRecordTypeId();
        Id recordTypePartnerForm = Schema.SObjectType.Ops_Setup_Validation_Form__c.getRecordTypeInfosByDeveloperName().get('Partner_Ops_Setup_Validation').getRecordTypeId();
          
         /*ACCOUNT */
        Account acc = new Account(Name='AccTest', Website='www.test.com',  RecordTypeId = recordTypeIdAccount, 
            ShippingCountry = 'Brazil', ShippingState = 'São Paulo',
            ShippingStreet = 'Bermudas', ShippingCity = 'Sorocaba',                       
            Types_of_ownership__c = Label.franchisePicklist,
            Subscription_Type__c = 'Value per class',
            Subscription_Period__c = 'Monthy value',
            Subscription_Type_Estimated_Price__c = 100,
            Has_market_cannibalization__c = 'No',
            Alternative_registration_number__c = 123,
            Legal_Registration__c = 321,
            Gyms_Identification_Document__c = 'CNPJ',
            Gym_Type__c = 'Full Service',
            Gym_Classes__c = 'Cardio',
            Legal_Title__c = 'TEste',
            CAP_Value__c = 220,
            Exclusivity__c = 'Yes',
            Exclusivity_End_Date__c = Date.today().addYears(1),
            Exclusivity_Partnership__c = 'Full Exclusivity',
            Exclusivity_Restrictions__c= 'No',
            Can_use_logo__c='No',                      
            Gym_Email__c = 'gymemail@apex.com',
            Phone = '3222123123' ); 
        INSERT acc;    
     
              
        /**OPPORTUNITY*/                
        List<Opportunity> oppToInsert = new List<Opportunity>();        
        Opportunity accOpp = new Opportunity();
        accOpp.recordTypeId = recordTypeIdSmall;
        accOpp.AccountId = acc.id;
        accOpp.Name = 'OpportunityTest';      
        accOpp.Gym_agreed_to_an_API_integration__c  = 'Yes';
        accOpp.CMS_Used__c = 'Yes';
        accOpp.Club_Management_System__c = 'ABC Financial';
        accOpp.Integration_Fee_Deduction__c = 'No';
        accOpp.CloseDate = Date.today();
        accOpp.Standard_Payment__c = 'Yes';
        accOpp.Success_Look_Like__c = 'Yes';
        accOpp.Success_Look_Like_Description__c = 'Money money';
        accOpp.StageName = 'Qualification';
        accOpp.Should_OPS_team_send_the_training__c = 'Standard Training';
        accOpp.Type = 'Expansion';     
        accOpp.CurrencyIsoCode = 'BRL';
        accOpp.Gympass_Plus__c = 'Yes';   
        accOpp.Request_for_self_checkin__c = 'Yes';   
        INSERT accOpp;
        
        /**PRODUCT ITEM*/     
        List<Product_Item__c> prodList = new List<Product_Item__c>();           
        Product_Item__c prodItem = new Product_Item__c();
        prodItem.recordTypeId = recordTypeIdProductItem;
        prodItem.Opportunity__c = accOpp.id;
        prodItem.Product_Type__c = 'In person';
        prodItem.Is_Network_CAP__c = 'No';
        prodItem.CAP_Value__c = 100;
        prodItem.CurrencyIsoCode = 'BRL';
        prodItem.Do_You_Have_A_No_Show_Fee__c = 'No';
        prodItem.Has_Late_Cancellation_Fee__c = 'No';
        prodItem.Late_Cancellation_Percent__c = '0';
        prodItem.Maximum_Live_Class_Per_Month__c = 0;
        prodItem.No_Show_Fee_Percent__c = '0';
        prodItem.Product_Definition__c  ='Dança';
        prodItem.Product_Restriction__c = 'None';
        prodItem.Name = 'Dança';
        prodItem.Package_Type__c = 'Value per class';      
        prodItem.Max_Monthly_Visit_Per_User__c = 12;
        prodItem.Reference_Price_Value_Unlimited__c = 12;
        prodItem.Net_Transfer_Price__c  = 11;
        prodItem.Price_Visits_Month_Package_Selected__c = 400;
        prodItem.Selected_Plan__c = 'Gold';
        prodItem.Justification_Plan__c = 'Fit';
        prodList.add(prodItem);

        Product_Item__c prod2Item = new Product_Item__c();
        prod2Item.recordTypeId = recordTypeIdProductItem;
        prod2Item.Opportunity__c = accOpp.id;
        prod2Item.Product_Type__c = 'In person';
        prod2Item.Is_Network_CAP__c = 'No';
        prod2Item.CAP_Value__c = 100;
        prod2Item.CurrencyIsoCode = 'BRL';
        prod2Item.Do_You_Have_A_No_Show_Fee__c = 'No';
        prod2Item.Has_Late_Cancellation_Fee__c = 'No';
        prod2Item.Late_Cancellation_Percent__c = '0';
        prod2Item.Maximum_Live_Class_Per_Month__c = 0;
        prod2Item.No_Show_Fee_Percent__c = '0';
        prod2Item.Product_Definition__c  ='Musculacao';
        prod2Item.Product_Restriction__c = 'None';
        prod2Item.Name = 'Musculacao';
        prod2Item.Package_Type__c = 'Value per class';      
        prod2Item.Max_Monthly_Visit_Per_User__c = 12;
        prod2Item.Reference_Price_Value_Unlimited__c = 12;
        prod2Item.Net_Transfer_Price__c  = 11;
        prod2Item.Price_Visits_Month_Package_Selected__c = 400;
        prod2Item.Selected_Plan__c = 'Gold';
        prod2Item.Justification_Plan__c = 'Fit';
        prodList.add(prod2Item);

        Product_Item__c prod3Item = new Product_Item__c();
        prod3Item.recordTypeId = recordTypeIdProductItem;
        prod3Item.Opportunity__c = accOpp.id;
        prod3Item.Product_Type__c = 'In person';
        prod3Item.Is_Network_CAP__c = 'No';
        prod3Item.CAP_Value__c = 100;
        prod3Item.CurrencyIsoCode = 'BRL';
        prod3Item.Do_You_Have_A_No_Show_Fee__c = 'No';
        prod3Item.Has_Late_Cancellation_Fee__c = 'No';
        prod3Item.Late_Cancellation_Percent__c = '0';
        prod3Item.Maximum_Live_Class_Per_Month__c = 0;
        prod3Item.No_Show_Fee_Percent__c = '0';
        prod3Item.Product_Definition__c  ='Bike';
        prod3Item.Product_Restriction__c = 'None';
        prod3Item.Name = 'Bike';
        prod3Item.Package_Type__c = 'Value per class';      
        prod3Item.Max_Monthly_Visit_Per_User__c = 12;
        prod3Item.Reference_Price_Value_Unlimited__c = 12;
        prod3Item.Net_Transfer_Price__c  = 11;
        prod3Item.Price_Visits_Month_Package_Selected__c = 400;
        prod3Item.Selected_Plan__c = 'Gold';
        prod3Item.Justification_Plan__c = 'Fit';
        prodList.add(prod3Item);
        INSERT prodList;
        /**CONTACT*/
        Id ctcRtId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Gyms_Partner').getRecordTypeId();
        List<Contact> contactToInsert = new List<Contact>();
        
        Contact contactAcc = new Contact(
            FirstName = 'Fernando',
            LastName = 'Souza',
            Email = 'fernandoTestSouze@mail.com',
            Phone = '111111111',
            recordTypeId = ctcRtId,
            MailingCountry = 'Brazil',
            Type_of_contact__c = 'Point of Contact;Legal Representative',
            Cargo__c = 'CEO',            
            accountId = acc.id
        );
        contactToInsert.add(contactAcc);

        
        Contact contactNegative = new Contact(
            FirstName = 'Murilo',
            LastName = 'Silva',
            Email = 'murilotest@mail.com',
            Phone = '111111111',
            recordTypeId = ctcRtId,
            MailingCountry = 'Brazil',
            Type_of_contact__c = 'Point of Contact;Legal Representative',
            Cargo__c = 'Analyst',          
            accountId = acc.id
        );
        contactToInsert.add(contactNegative);
        INSERT contactToInsert;        

        /**Bank Account*/
        List<Bank_Account__c> bankAccountLst = new List<Bank_Account__c>();
        Bank_Account__c bankAccount = new Bank_Account__c();
        bankAccount.recordTypeId = recordTypeIdBank;
        bankAccount.Bank_Account_Number_or_IBAN__c = 'abc';
        bankAccount.Bank_Account_Ownership__c = 'Contact text';
        bankAccount.Bank_Name__c = 'ItauFake';
        bankAccount.BIC_or_Routing_Number_or_Sort_Code__c = 'abc'; 
        bankAccount.Routing_Number__c = 1234;
        bankAccount.VAT_Number_or_UTR_number__c = 'tttt1231';
        
        bankAccountLst.add(bankAccount);

        Bank_Account__c bankAccountNegative = new Bank_Account__c();
        bankAccountNegative.recordTypeId = recordTypeIdBank;
        bankAccountNegative.Bank_Account_Number_or_IBAN__c = 'abc'; 
        bankAccount.Bank_Account_Ownership__c = 'Contact text';
        bankAccountNegative.Bank_Name__c = 'BradescoFake';
        bankAccountNegative.BIC_or_Routing_Number_or_Sort_Code__c = 'abc';
        bankAccountNegative.Routing_Number__c = 1234;
        bankAccountNegative.VAT_Number_or_UTR_number__c = 'VVVT1233';
        bankAccountLst.add(bankAccountNegative);
        INSERT bankAccountLst;

        List<Acount_Bank_Account_Relationship__c> abrLst = new List<Acount_Bank_Account_Relationship__c>();
        Acount_Bank_Account_Relationship__c abr = new Acount_Bank_Account_Relationship__c();
        abr.Account__c = acc.Id;
        abr.Bank_Account__c = bankAccount.id;
        abr.RecordTypeId = recordTypeIdAccBank;
        abrLst.add(abr);
        
        Acount_Bank_Account_Relationship__c abrNegative = new Acount_Bank_Account_Relationship__c();
        abrNegative.Account__c = acc.Id;
        abrNegative.Bank_Account__c = bankAccount.id;
        abrNegative.RecordTypeId = recordTypeIdAccBank;
        abrLst.add(abrNegative);     
        INSERT abrLst;
        
        List<Contact_Bank_Account_Relationship__c> conbanLst = new List<Contact_Bank_Account_Relationship__c>();
        Contact_Bank_Account_Relationship__c coban = new Contact_Bank_Account_Relationship__c();
        coban.Contact__c = contactAcc.Id;
        coban.Bank_Account__c = bankAccount.id;
        coban.RecordTypeId = recordTypeIdConBank;
        conbanLst.add(coban);
        
        Contact_Bank_Account_Relationship__c cobanNega = new Contact_Bank_Account_Relationship__c();
        cobanNega.Contact__c = contactAcc.Id;
        cobanNega.Bank_Account__c = bankAccount.id;
        coban.RecordTypeId = recordTypeIdConBank;
        conbanLst.add(cobanNega);     
        INSERT conbanLst;
        
        /** Payment */
        List<Payment__c> payLst = new List<Payment__c>();
        Payment__c payment = new Payment__c();
        payment.recordTypeId = recordTypeIdPayment;
        payment.Payment_Start_Date__c = Date.today();
        payment.Payment_Method__c = 'TED';
        payment.Payment_Frequency__c = 'Every_3_months';  
        payLst.add(payment);

        Payment__c paymentNegative = new Payment__c();
        paymentNegative.recordTypeId = recordTypeIdPayment;
        paymentNegative.Payment_Start_Date__c = Date.today();
        paymentNegative.Payment_Method__c = 'TED';
        paymentNegative.Payment_Frequency__c = 'Every_3_months';  
        payLst.add(paymentNegative);
        
        INSERT payLst;
     
        /** Proposals/Quote */
        List<Quote> propLst = new List<Quote>();
        Quote proposal = new Quote();
        proposal.recordTypeId= recordTypeIdQuote;
        proposal.Name = 'academiaBrasilCompanyQuote';
        proposal.Status = 'Ganho';
        proposal.Signed__c = Date.today();
        proposal.Start_Date__c  = Date.today();
        proposal.Final_Date__c =  Date.today().addYears(4);
        proposal.Priority_Tier__c = 'Tier 1';
        proposal.PR_Activity__c  = 'Yes';
        proposal.Threshold_Value_1__c = 12;
        proposal.Threshold_Value_2__c = 32;                         
        proposal.Threshold_Value_3__c = 42;                
        proposal.Threshold_Value_4__c = 48;                               
        proposal.First_Discount_Range__c = 20;
        proposal.Second_Discount_Range__c = 24; 
        proposal.Third_Discount_Range__c = 56;
        proposal.Fourth_Discount_Range__c = 70;
        proposal.discounts_usage_volume_range__c = 'Yes';
        proposal.Volume_Discount_Type__c = 'By single user volume';
        proposal.OpportunityId = accOpp.id;

        propLst.add(proposal);

        Quote proposalNegative = new Quote();
        proposalNegative.recordTypeId= recordTypeIdQuote;
        proposalNegative.Name = 'proposalNegative';
        proposalNegative.Status = 'Ganho';
        proposalNegative.Signed__c = Date.today();
        proposalNegative.Start_Date__c  = Date.today();
        proposalNegative.Final_Date__c =  Date.today().addYears(1);
        proposalNegative.Priority_Tier__c = 'Tier 1';
        proposalNegative.PR_Activity__c  = 'No';
        
        proposalNegative.discounts_usage_volume_range__c = 'Yes';
        proposalNegative.Threshold_Value_1__c = 12;
        proposalNegative.Threshold_Value_2__c = 32;                         
        proposalNegative.Threshold_Value_3__c = 42;                
        proposalNegative.Threshold_Value_4__c = 48;                               
        proposalNegative.First_Discount_Range__c = 20;
        proposalNegative.Second_Discount_Range__c = 24; 
        proposalNegative.Third_Discount_Range__c = 56;
        proposalNegative.Fourth_Discount_Range__c = 70;
        proposalNegative.Volume_Discount_Type__c = 'By single user volume';
        proposalNegative.OpportunityId = accOpp.id; 
        propLst.add(proposalNegative);
        INSERT propLst;
                  
  
        List<Opportunity> oppToUpdate = new List<Opportunity>();
        accOpp.AccountId = acc.Id;
        accOpp.Legal_representative__c = contactAcc.id;             
        accOpp.StageName = 'Proposta Enviada';
        accOpp.Payment__c = payment.id;
        oppToUpdate.add(accOpp);
        UPDATE oppToUpdate;

        /*steps */
        Map<Decimal, Step_Towards_Success_Definition__mdt> stepNumberToStepTowardsSuccessDefinition = new Map<Decimal, Step_Towards_Success_Definition__mdt>(); 
       
        List<Step_Towards_Success_Definition__mdt> listStepsTowardsSuccessDefinitions = [
            SELECT DeveloperName , MasterLabel, Category__c, Description__c, Field_API_Name__c, Field_Expected_Value__c, How_are_we_going_to_track_it__c, Object_API_Name__c, Step__c
            FROM Step_Towards_Success_Definition__mdt
            WHERE Category__c = '10 Steps'
            ORDER BY Step__c ASC
        ];
        
        for (Step_Towards_Success_Definition__mdt step : listStepsTowardsSuccessDefinitions) {
            stepNumberToStepTowardsSuccessDefinition.put(Integer.valueOf(step.Step__c), step);
        } 

        List<Step_Towards_Success_Partner__c> stepsToInsert = new List<Step_Towards_Success_Partner__c>();
        Id opportunityStepRecordTypeId = Schema.SObjectType.Step_Towards_Success_Partner__c.getRecordTypeInfosByName().get('Gym Partner - Opportunity Step').getRecordTypeId();
        for (Integer i=1; i<=10; i++) {
            
            Step_Towards_Success_Partner__c step = new Step_Towards_Success_Partner__c();
            step.Step_Number__c = i;
            step.Related_Opportunity__c = accOpp.Id;
            step.Related_Account__c = acc.Id;
            step.Name = stepNumberToStepTowardsSuccessDefinition.get(i).MasterLabel;
            step.RecordTypeId = opportunityStepRecordTypeId;
            step.Achieved__c = 'No';
            stepsToInsert.add(step);
        }        
        
        INSERT stepsToInsert;


        Profile prof = [SELECT ID from Profile WHERE Name = 'Gyms for Partner OPS'];

        User us1 = new User();
        us1.ProfileId = prof.Id;
        us1.FirstName = 'Tester';
        us1.LastName = 'Tester';
        us1.username = 'testergftgympass@test.com';
        us1.email= 'testergftgympass@test.com';
        us1.TimeZoneSidKey = 'GMT';
        us1.LanguageLocaleKey = 'en_US';
        us1.LocaleSidKey  = 'en_US';
        us1.EmailEncodingKey = 'ISO-8859-1';
        us1.Alias = 'tsts';
        INSERT us1;

    }

    @isTest 
    static void updateOppWithOpsFormSetUpValidation() {
        Opportunity opp = [SELECT id,Approval_Needed__c, OwnerId, Name, StageName FROM Opportunity WHERE Name = 'OpportunityTest'];       
        Id recordTypePartnerForm = Schema.SObjectType.Ops_Setup_Validation_Form__c.getRecordTypeInfosByDeveloperName().get('Partner_Ops_Setup_Validation').getRecordTypeId();
        
        Test.startTest();
        try{
            opp.StageName = 'Set Up Validation';
            update opp;          
            List<Ops_Setup_Validation_Form__c> opsForm = [SELECT ID FROM Ops_Setup_Validation_Form__c WHERE Opportunity__c =: opp.id];
            System.assert(opsForm.size()>0); 
        }catch(Exception e) {         
            System.debug('ERROR ' + e.getMessage());
        }

        Test.stopTest(); 
    }
    
    
}