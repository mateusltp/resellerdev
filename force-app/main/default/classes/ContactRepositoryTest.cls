/**
* @author vinicius.ferraz
* @description ContactRepository unit test
*/
@isTest(seeAllData=false)
public class ContactRepositoryTest {
    
    public static final String Contact_NAME_TEST_1 = 'Test1';

    @TestSetup
    static void createData(){        
        Account acc = generateAccount();
        insert acc;
        insert generateContact(acc, '1');
    }

    @isTest
    static void testById(){
        Contact contact = [select Id,Name from Contact limit 1];
        ContactRepository contacts = new ContactRepository();
        Contact cnt = null;
        try{
            cnt = contacts.byId(contact.Id);
        }catch(System.QueryException e){
            System.assert(false);
        }
        System.assertEquals(cnt.Id,contact.Id);
    }

    @isTest
    static void testFromAccount(){
        Contact contact = [select Id,Name,AccountId from Contact limit 1];
        ContactRepository contacts = new ContactRepository();
        List<Contact> cnts = null;
        try{
            cnts = contacts.fromAccount(contact.AccountId);
        }catch(System.QueryException e){
            System.assert(false);
        }
        System.assertEquals(cnts.size(),1);
    }

    @isTest
    static void testAdd(){
        ContactRepository contacts = new ContactRepository();
        Account acc = [select Id from Account limit 1];
        Contact cnt = generateContact(acc, '2');        
        System.assertEquals(cnt.Id,null);
        try{
            contacts.add(cnt);
        }catch(System.DmlException e){
            System.assert(false);
        }
        System.assertNotEquals(cnt.Id,null);
    }

    @isTest
    static void testEdit(){
        ContactRepository contacts = new ContactRepository();
        Account acc = [select Id from Account limit 1];
        Contact cnt = generateContact(acc, '3');        
        System.assertEquals(cnt.Id,null);
        
        try{
            contacts.edit(cnt);
        }catch(System.DmlException e){
            System.assert(true);
        }
        System.assertEquals(cnt.Id,null);
        
        contacts.add(cnt);
        
        try{
            contacts.edit(cnt);
        }catch(System.DmlException e){
            System.assert(false);
        }
        System.assertNotEquals(cnt.Id,null);
    }

    @isTest
    static void testAddOrEdit(){
        ContactRepository contacts = new ContactRepository();
        Account acc = [select Id from Account limit 1];
        Contact cnt = generateContact(acc, '4');        
        System.assertEquals(cnt.Id,null);
        
        try{
            contacts.addOrEdit(cnt);
        }catch(System.DmlException e){
            System.assert(false);
        }
        System.assertNotEquals(cnt.Id,null);
        
        try{
            contacts.addOrEdit(cnt);
        }catch(System.DmlException e){
            System.assert(false);
        }
        System.assertNotEquals(cnt.Id,null);
    }

    private static Account generateAccount(){
        Id rtId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gympass_Entity').getRecordTypeId();
        Account acc = new Account();
        acc.name='AcademiaBrasilCompanyPai';
        acc.RecordTypeId = rtId;
        acc.GP_Status__c = 'Active';
        acc.billingState = 'Minas Gerais';
        acc.CAP_Value__c = 120;
        acc.BillingCity = 'CityAcademiaBrasil';
        acc.billingStreet = 'Rua academiabrasilpai';
        acc.billingCountry = 'Brazil';
        acc.Gym_Type__c = 'Studios';
        acc.Gym_Classes__c = 'Cardio';
        acc.Types_of_ownership__c = Label.franchisePicklist;
        acc.Subscription_Type__c = 'Value per class';
        acc.Subscription_Period__c = 'Monthy value';
        acc.Subscription_Type_Estimated_Price__c    = 100;
        acc.Has_market_cannibalization__c = 'No';
        acc.Exclusivity__c = 'Yes';
        acc.Exclusivity_End_Date__c = Date.today().addYears(1);
        acc.Exclusivity_Partnership__c = 'Full Exclusivity';
        acc.Exclusivity_Restrictions__c= 'No';
        acc.Website = 'testing@tesapex.com';
        acc.Gym_Email__c = 'gymemail@apex.com';
        acc.Phone = '3222123123';
        acc.Can_use_logo__c = 'Yes';
        acc.Legal_Registration__c = 12123;
        acc.Legal_Title__c = 'Title LTDA';
        acc.Gyms_Identification_Document__c = 'CNPJ'; 
        return acc;
    }

    private static Contact generateContact(Account acc, String name){
        Contact acccnt = new Contact();
        acccnt.AccountId = acc.Id;
        acccnt.LastName = 'test'+name;
        acccnt.Email = 'test'+name+'@tst.com';
        return acccnt;
    }
}