@RestResource(urlMapping='/self-checkout/*')
global with sharing class SelfCheckoutRoute {
  @HttpPost
  global static SelfCheckoutResponse post() {
    RestResponse restResponse = new SelfCheckoutController().post();

    return new SelfCheckoutResponse(restResponse);
  }

  global class SelfCheckoutResponse {
    private Integer statusCode;
    private String errorMessage;

    private SelfCheckoutResponse(RestResponse restResponse) {
      this.statusCode = restResponse.statusCode;
      this.errorMessage = restResponse?.responseBody?.toString();
    }

    public Integer getStatusCode() {
      return this.statusCode;
    }

    public String getErrorMessage() {
      return this.errorMessage;
    }
  }
}