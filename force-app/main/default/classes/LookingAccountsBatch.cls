global class LookingAccountsBatch implements Database.Batchable<SObject>, Database.stateful {

    String errors = '';
    String targetId = '';
    public String massiveId{get;set;}
    private final String InvalidUniqueIdentifier = System.label.Community_Invalid_Unique_Identifier; //'Invalid Unique Identifier';
    private final String InvalidCompanyName = System.label.Community_Invalid_Company_Name; //'Invalid Company Name';
    private final String InvalidBusinessModel = System.label.Community_Invalid_Business_Model; //'Invalid Business Model';
    private final String InvalidCountry = System.label.Community_Invalid_Country; //'Invalid Country';
    private final String InvalidWebsite = System.label.Community_Invalid_Website; //'Invalid Website';
    private final String InvalidNumberEmployees = System.label.Community_Invalid_Number_Employees; //'Invalid Number Employees';
    private final String InvalidCompanyName80 = System.label.Community_Invalid_Company_Name_80; //'Invalid Company Name 80';

    global Database.QueryLocator start(Database.BatchableContext BC){
        String status = ResellerEnumRepository.MassiveAccountRequestStatus.LookingForAccounts.name();
        String query = 'SELECT Id, Name, Massive_Account_Request__c, Unique_Identifier__c, Website__c, Unique_Identifier_Type__c,Total_number_of_employees__c, Billing_Country__c,Partner_Model__c, Engine_Status__c, Massive_Status__c, Error_message__c, Engine_Log__c, Existing_In_SalesForce__c FROM Account_Request__c WHERE Massive_Account_Request__c =:massiveId AND Bulk_Operation__c = true AND Massive_Status__c =:status';
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<Account_Request__c> records) {
        
        try{
            Set<Id> AccountRequestIds = new Set<Id>();
            Set<Id> massiveAccountRequestId_set = new Set<Id>();
            
            AccountSearch acc = new AccountSearch();
            List<Account_Request__c> accountRequestValidate = new List<Account_Request__c>();
            List<Account_Request__c> accountRequestInvalid = new List<Account_Request__c>();
            Boolean requestInvalid;

            /*Validate*/
            for(Account_Request__c record : records){
               requestInvalid = false;
               //record.Error_message__c = '';
               
                if(record.Error_message__c == 'Invalid Company Name 80'){
                    record.Error_message__c = '\n'+InvalidCompanyName80 +'.';
                    requestInvalid = true;
                }
                if(record.Name == 'ㅤ' || record.Name == null){
                    record.Error_message__c += '\n'+InvalidCompanyName +'.';
                    requestInvalid = true;
                }
                if(record.Partner_Model__c == null){
                    record.Error_message__c += '\n'+InvalidBusinessModel +'.';
                    requestInvalid = true;
                }
                if(record.Billing_Country__c == null){
                    record.Error_message__c += '\n'+InvalidCountry +'.';
                    requestInvalid = true;
                }
                if(record.Total_number_of_employees__c == null || record.Total_number_of_employees__c == 0){
                    record.Error_message__c += '\n'+InvalidNumberEmployees +'.';
                    requestInvalid = true;
                }
                if(record.Unique_Identifier_Type__c == 'CNPJ'){
                    if(record.Unique_Identifier__c != null){
                        String cnpj = record.Unique_Identifier__c;

                        if(UniqueKeyCustomField.isCNPJ(cnpj)){
                            record.Unique_Identifier__c = UniqueKeyCustomField.imprimeCNPJ(cnpj.replace('.','').replace('/', '').replace('-',''));
                            record.Engine_Log__c += ' -|- ' + ResellerEnumRepository.MassiveLog.UNIQUE_KEY_VALIDED.name();     
                        }else{
                            record.Engine_Log__c += ' -|- ' + ResellerEnumRepository.MassiveLog.UNIQUE_KEY_INVALIDED.name();
                            record.Error_message__c += '\n'+InvalidUniqueIdentifier +'.';
                            requestInvalid = true;    
                        }
                    }else{
                        record.Engine_Log__c += ' -|- ' + ResellerEnumRepository.MassiveLog.UNIQUE_KEY_NOT_FOUND.name();
                        record.Error_message__c += '\n'+InvalidUniqueIdentifier +'.';
                        requestInvalid = true;  
                    }
                } 
                else{
                    
                    if(record.Website__c == null){
                        record.Error_message__c += '\n'+InvalidWebsite +'.'; 
                        requestInvalid = true;
                    }
                    else{
                        String webSiteValidate = record.Website__c;
                        String validate = 'www.';
                        String webSiteSubstring = webSiteValidate.substring(0,4);

                        if(validate != webSiteSubstring){
                            record.Error_message__c += '\n'+InvalidWebsite +'.'; 
                            requestInvalid = true;
               
                        }else if(webSiteValidate.length() < 8){
                            record.Error_message__c += '\n'+InvalidWebsite +'.'; 
                            requestInvalid = true;  
                        } 
                    } 
                }
                
                if(requestInvalid == true){
                    record.EngineStatusAnalysis__c = 'Incomplete';
                    record.Error_message__c = record.Error_message__c.replace('null','');
                    record.Engine_Status__c = ResellerEnumRepository.AccountRequestStatus.Invalid.name();
                    record.Massive_Status__c = ResellerEnumRepository.MassiveAccountRequestStatus.Finished.name();
                    accountRequestInvalid.add(record);
                    
                }else if( requestInvalid == false){
                    accountRequestValidate.add(record);
                }
            }
            
            if(accountRequestInvalid.size() > 0){
                records.clear();
                records.addAll(accountRequestValidate);
                update accountRequestInvalid;
                System.debug('accountRequestInvalid antes update' + accountRequestInvalid);
            }
            
   
            if(records.size() > 0){
                acc.searchAccountsMassive(records);
            
                for(Account_Request__c ar : records){
                    
                    AccountRequestIds.add(ar.Id);
                    massiveAccountRequestId_set.add(ar.Massive_Account_Request__c);
                    ar.Massive_Status__c = ResellerEnumRepository.MassiveAccountRequestStatus.LookingForOpportunities.name();
                }
                System.debug('records antes update' + records);

                update records;
                System.debug('records depois update' + records);

                update new Massive_Account_Request__c( Id = massiveId, Status__c = ResellerEnumRepository.MassiveAccountRequestStatus.LookingForOpportunities.name());
            }
            else{

                update new Massive_Account_Request__c( Id = massiveId, Status__c = ResellerEnumRepository.MassiveAccountRequestStatus.Finished.name());
            }
        }
        catch( Exception e){
            errors = errors + e.getMessage() + ' --- ';
            targetId = records[0].Massive_Account_Request__c;
        }

    }
    global void finish(Database.BatchableContext BC) {
        
        if(String.isNotBlank(errors) || Test.isRunningTest()) {

            Set<String> recipientsIds = new Set<String>();
            recipientsIds.add(UserInfo.getUserId());

            CustomNotificationType notificationType = 
            [SELECT Id, DeveloperName 
             FROM CustomNotificationType 
             WHERE DeveloperName='Massive_Account_Request_Batch'];

            Messaging.CustomNotification notification = new Messaging.CustomNotification();
            notification.setTitle('Massive Account Request Error');
            notification.setBody('LookingAccountsBatch Error in your email');
            notification.setNotificationTypeId(notificationType.Id);
            notification.setTargetId(targetId);
            
            try {
                notification.send(recipientsIds);
            }
            catch (Exception e) {
                System.debug('Problem sending notification: ' + e.getMessage());
            }


            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setSubject('Errors occurred during batch process.');
            mail.setTargetObjectId(UserInfo.getUserId());
            mail.setSaveAsActivity(false);
            mail.setPlainTextBody(errors);
            Messaging.sendEmail(new Messaging.Email[] { mail });
        }
        else{

            LookingOpportunitiesBatch bt = new LookingOpportunitiesBatch();
            bt.massiveId = massiveId;
            ID idOpp = Database.executeBatch(bt);

        }
    }
    
    
    
}