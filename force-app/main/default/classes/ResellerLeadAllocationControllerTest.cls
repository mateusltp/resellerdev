@isTest(isParallel=true)
public class ResellerLeadAllocationControllerTest {

    @isTest 
    static void updateToIndirectChannelRecordTypeTest() {
        
        Id indirectChannelRTId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Parceiro').getRecordTypeId();
        Id smbDirectChannelRTId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('SMB_Direct_Channel').getRecordTypeId();
        List<Lead> testLeadsLst = new List<Lead>();
        
        Lead testLead = new Lead();
		testLead.Email = 'a@a.com';
		testLead.LastName = 'updateToIndirectChannel';
		testLead.Company = 'Test';
		testLead.Country = 'Brazil';
		testLead.Business_Unit__c = System.Label.Lead_Indirect_Channel_Business_Unit;
        testLead.RecordTypeId = smbDirectChannelRTId;
        testLeadsLst.add(testLead);
        
        Lead testLead2 = new Lead();
		testLead2.Email = 'b@b.com';
		testLead2.LastName = 'updateToIndirectChannel2';
		testLead2.Company = 'Test2';
		testLead2.Country = 'Brazil';
        testLead2.RecordTypeId = smbDirectChannelRTId;
        testLeadsLst.add(testLead2);
		
        Test.startTest();
        insert testLeadsLst;
        Test.stopTest();
        
        List<Lead> afterTestLeadsLst = [SELECT Id, RecordTypeId FROM Lead WHERE Name LIKE '%updateToIndirectChannel%'];
        Integer leadsWithIndirectChannelRT = 0;
        Integer leadsWithsmbDirectChannelRT = 0;
        for(Lead l : afterTestLeadsLst) {
            if(l.RecordTypeId == indirectChannelRTId) {
                leadsWithIndirectChannelRT++;
            } else {
                leadsWithsmbDirectChannelRT++;
            }
        }
        System.assert(leadsWithIndirectChannelRT == 1);
        System.assert(leadsWithsmbDirectChannelRT == 1);
    }
    
}