public without sharing class CountryStatesService {
    private List<String> countriesIsoCodeSelected;
    public List<CountryStatesPicklistDTO> countryStatesPicklists;
    private Map<String, List<String>> statesByCountry;
    private Map<String,String> statesValueByLabel;
    private String objectName = 'Account';
    private String controllingField = 'BillingCountryCode' ;        
    private String dependentField = 'BillingStateCode' ;
    private  Map<String, Map<String, List<String>>> citiesByStatesByCountries;
    private List<String> countriesSelectedToQuery ;          

    public CountryStatesService(CountryStatesRequest countryStatesRequest) {
      this.countriesIsoCodeSelected = countryStatesRequest.getCountriesIsoCodes();
      this.countryStatesPicklists = new List<CountryStatesPicklistDTO>();
      this.statesByCountry = new Map<String, List<String>>();
      this.statesValueByLabel = new Map<String,String>();
      this.citiesByStatesByCountries = new  Map<String, Map<String, List<String>>>();
      this.countriesSelectedToQuery = new List<String>();          

    }

    public List<CountryStatesPicklistDTO> execute() {
      getCountryStatesCitiesPicklist();
      return this.countryStatesPicklists;
    }
  
    private void getCountryStatesCitiesPicklist() {
    
        Schema.DescribeSObjectResult  describeResult = Schema.getGlobalDescribe().get(this.objectName).getDescribe();        
        List<Schema.PicklistEntry> controllingValues = describeResult.fields.getMap().get(this.controllingField).getDescribe().getPicklistValues();
        List<Schema.PicklistEntry> dependentValues = describeResult.fields.getMap().get(this.dependentField).getDescribe().getPicklistValues();    
       
        addSelectedOrAllCountries(controllingValues);
        getCitiesFromStaticResources();
        setStatesByCountries(dependentValues,controllingValues);
    
        for(CountryStatesPicklistDTO countryDTO : this.countryStatesPicklists) {
            countryDTO.states = new List<CountryStatesPicklistDTO.StateDTO>();

            for(String state : this.statesByCountry.get(countryDTO.countryLabel)){
                CountryStatesPicklistDTO.StateDTO stateDTO = new CountryStatesPicklistDTO.StateDTO();
                stateDTO.label = state;
                stateDTO.code = this.statesValueByLabel.get(state);
                stateDTO.cities = citiesByStatesByCountries?.get(countryDTO.countryCode)?.get(state) != null ? 
                                  citiesByStatesByCountries.get(countryDTO.countryCode).get(state) :new List<String>()  ;
                countryDTO.states.add(stateDTO);
          } 
        } 

    }

    private void addSelectedOrAllCountries ( List<Schema.PicklistEntry> controllingValues){

        for (Schema.PicklistEntry  currControllingValue  :  controllingValues)  {
            CountryStatesPicklistDTO countryPicklist = new CountryStatesPicklistDTO();
            this.statesByCountry.put (currControllingValue.getLabel() , new List<String>());
            if(this.countriesIsoCodeSelected != null && this.countriesIsoCodeSelected.size() > 0){
                for(String countrySelected : countriesIsoCodeSelected){ 
                    this.countriesSelectedToQuery.add('Cities' + countrySelected + '%' );
                    if(countrySelected == currControllingValue.getValue() ) {
                        countryPicklist.countryLabel = currControllingValue.getLabel();
                        countryPicklist.countryCode = currControllingValue.getValue();
                        this.countryStatesPicklists.add(countryPicklist);
                    }
                }
            }
            else{
                countryPicklist.countryLabel = currControllingValue.getLabel();
                countryPicklist.countryCode = currControllingValue.getValue();
                this.countryStatesPicklists.add(countryPicklist);
            }
        }
    }
  
    private void getCitiesFromStaticResources() {
        List<CountriesByStatesByCitiesWrapper> countriesStatesCitiesWrapper = new List< CountriesByStatesByCitiesWrapper>();
        String query;

        if(this.countriesIsoCodeSelected != null && this.countriesIsoCodeSelected.size() > 0){
            query= 'SELECT Id,Name, Body FROM StaticResource WHERE Name LIKE : countriesSelectedToQuery';
        }else{
            String cities = 'Cities%';
            query = 'Select Id,Name, Body FROM StaticResource WHERE Name LIKE :cities ';
        }
        List<StaticResource> citiesStaticResources = Database.query(query);
       
       for(StaticResource citiesResource : citiesStaticResources){
            List<CountriesByStatesByCitiesWrapper> countryByStatesByCitiesWrapper = CountriesByStatesByCitiesWrapper.parse(citiesResource.Body);
            countriesStatesCitiesWrapper.addAll(countryByStatesByCitiesWrapper);
        }

        for(CountriesByStatesByCitiesWrapper cityStateCountry :countriesStatesCitiesWrapper){
            if(this.citiesByStatesByCountries.get(cityStateCountry.Country)== null){
                Map<String, List<String>> citiesByStates = new Map<String, List<String>>();
                citiesByStates.put(cityStateCountry.State, new List<String>{cityStateCountry.City});
                this.citiesByStatesByCountries.put(cityStateCountry.Country,citiesByStates);
            }else if(this.citiesByStatesByCountries.get(cityStateCountry.Country).get(cityStateCountry.State) == null){
                this.citiesByStatesByCountries.get(cityStateCountry.Country).put(cityStateCountry.State, new List<String>{cityStateCountry.City});
            }else{
                this.citiesByStatesByCountries.get(cityStateCountry.Country).get(cityStateCountry.State).add(cityStateCountry.City);
            }
        }
    }

    private void setStatesByCountries(List<Schema.PicklistEntry> dependentValues, List<Schema.PicklistEntry> controllingValues ) {

        for  (Schema.PicklistEntry  currDependentValue  :  dependentValues)  {
            this.statesValueByLabel.put(currDependentValue.getLabel(), currDependentValue.getValue());  
            String  jsonString  =  JSON.serialize(currDependentValue) ;
            MyPickListInfo  info  =  (MyPickListInfo) JSON.deserialize(jsonString, MyPickListInfo.class) ;
            String hexString  =  EncodingUtil.convertToHex (EncodingUtil.base64Decode (info.validFor)).toUpperCase ( ) ;
            Integer  baseCount  =  0;
            for (Integer curr  :  hexString.getChars ())  {
                Integer  val  =  0;
                if  (curr  >=  65) {
                    val  =  curr  -  65  +  10;
                }  else  {
                    val  =  curr  -  48;
                }
                if  ((val & 8)  ==  8)  {
                    this.statesByCountry.get (controllingValues[baseCount + 0].getLabel()).add (currDependentValue.getLabel() ) ;
                }
                if  ((val & 4)  ==  4)  {
                    this.statesByCountry.get (controllingValues[baseCount + 1].getLabel() ).add (currDependentValue.getLabel() ) ;
                }
                if  ((val & 2)  ==  2)  {
                    this.statesByCountry.get (controllingValues[baseCount + 2].getLabel() ).add (currDependentValue.getLabel() ) ;
                }
                
                if  ((val & 1)  ==  1)  {
                    this.statesByCountry.get (controllingValues[baseCount + 3].getLabel() ).add (currDependentValue.getLabel()) ;
                }
                baseCount  +=  4;
            }
        }
    }

    public class MyPickListInfo {
        public String validFor;
    }	
  
}