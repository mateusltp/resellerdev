public class ManagedClauseTriggerHelper {

    public void setLengthOfRichTextField(List<ManagedClause__c> ManagedClauseList ){
        for(ManagedClause__c Clause : ManagedClauseList){
            Clause.LengthOfText__c = 0;
            if (Clause.Text__c != null ) {
                Clause.LengthOfText__c = Clause.Text__c.length();
            }
        }
    }
}