/**
 * @File Name          : OpsFormValidationHelper.cls
 * @Description        : 
 * @Author             : GEPI@GFT.com
 * @Group              : 
 * @Last Modified By   : GEPI@GFT.com
 * @Last Modified On   : 07-24-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    24/06/2020   Samuel Silva - GFT (slml@gft.com)     Initial Version
**/
public without sharing class OpsFormValidationHelper {

    Id recordTypeIdSmall = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Gyms_Small_Medium_Partner').getRecordTypeId();
    Id recordTypeIdWishList = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Gyms_Wishlist_Partner').getRecordTypeId();
    Id recordTypePartnerForm = Schema.SObjectType.Ops_Setup_Validation_Form__c.getRecordTypeInfosByDeveloperName().get('Partner_Ops_Setup_Validation').getRecordTypeId();
    List<Opportunity> lstNew = Trigger.new;    
    
    public void opsFormValidation(){   
        List<Opportunity> oppLst = new List<Opportunity>();        
        Set<Id> accIds = new Set<Id>();
        Set<Id> legalContactIds = new Set<Id>();
        Map<Id, Id> paymentIds = new Map<Id,Id>();

        for(Opportunity opp : lstNew){  
            if((opp.RecordTypeId == recordTypeIdSmall || opp.RecordTypeId == recordTypeIdWishList) && 
                	(opp.StageName == 'Set Up Validation') ){         
                	oppLst.add(opp);
                    accIds.add(opp.AccountId);
                    if(opp.Legal_representative__c != null)  {
                        legalContactIds.add(opp.Legal_representative__c);
                    }                       
                    if(opp.Standard_Payment__c == 'No'){
                        paymentIds.put(opp.id, opp.Payment__c);
                    }
                }
        }

        if(!oppLst.isEmpty()){
            Set<Id> oppWithForm = new Set<Id>();
            for(Ops_Setup_Validation_Form__c form : [SELECT Opportunity__c 
                                                    FROM Ops_Setup_Validation_Form__c
                                                    WHERE Opportunity__c IN: oppLst]) {
                oppWithForm.add(form.Opportunity__c);
            }

            List<Account> accLst = [SELECT ID, ShippingCountry FROM Account WHERE Id IN: accIds];
            List<Acount_Bank_Account_Relationship__c> bkLst = [SELECT Bank_Account__c, Account__c FROM Acount_Bank_Account_Relationship__c WHERE Account__c IN: accIds];
            List<Product_Item__c> prdLst = [SELECT ID, Opportunity__c FROM Product_Item__c WHERE Opportunity__c IN: oppLst];
            List<Quote> quoteLst = [SELECT Id, OpportunityId, AccountId FROM Quote WHERE  OpportunityId IN: oppLst];
            List<Contact> contactLst = [SELECT Id, AccountId FROM Contact WHERE Id IN: legalContactIds];
            List<Wrapper> wrapperLst = new List<Wrapper>();
            System.debug('PRODU LST ' + prdLst.size());
            System.debug(prdLst);
            
            for(Opportunity opp : oppLst){      
                Wrapper wrapper = new Wrapper();          
                if(!oppWithForm.contains(opp.Id)){
                    wrapper.ownerId = opp.OwnerId;
                    wrapper.oppId = opp.Id;
                    wrapper.nameOpp = opp.Name;
                    wrapper.account = opp.AccountId; 
                    wrapper.payment = paymentIds.get(opp.id);                   
                    for(Account acc : accLst){
                        if(acc.Id == opp.AccountId){
                            wrapper.countryAcc = acc.ShippingCountry; 
                            wrapper.account = acc.Id;  
                        }                       
                    }     
                    
                    for(Contact c : contactLst){
                        if(c.AccountId == wrapper.account){
                            wrapper.legalContact = c.id;
                        }
                    }
                    
                    for(Quote quo : quoteLst){
                        if(quo.OpportunityId == opp.Id){
                            wrapper.quote = quo.Id; 
                        }                       
                    }

                    for(Acount_Bank_Account_Relationship__c bankAcc : bkLst){
                        if(bankAcc.Account__c == wrapper.account)
                            wrapper.bankAccount = bankAcc.Bank_Account__c; 
                    }                  

                    for(Product_Item__c prd : prdLst){
                        if(prd.Opportunity__c == opp.Id){
                           wrapper.prods.add(prd.id);
                        }                   
                    }
                    wrapperLst.add(wrapper);
                    System.debug('Wrapper Prods ' + wrapper.prods );
                }
            }                   
            createOpsForm(wrapperLst);
        }
    }


    private void createOpsForm(List<Wrapper> wrapperLst) {
        List<Ops_Setup_Validation_Form__c> formLst = new List<Ops_Setup_Validation_Form__c>();
        for(Wrapper wrapper : wrapperLst) {
            System.debug('wrapper payment ' + wrapper.payment);
            Ops_Setup_Validation_Form__c opsForm = new Ops_Setup_Validation_Form__c(
                recordTypeId = recordTypePartnerForm,
                OwnerId = wrapper.ownerId,
                Name = wrapper.nameOpp + ' - OPS Form',
                Status__c = 'Ops Set-up',
                Country__c= wrapper.countryAcc,
                Opportunity__c = wrapper.oppId,
                Account_Information__c =wrapper.account,  
                Contact_Information__c = wrapper.legalContact,            
                Product_1_Information__c = wrapper.prods.get(0),             
                Bank_Account_Information__c= wrapper.bankAccount,             
                Payment_Information__c = wrapper.payment,
                Proposal_Information__c = wrapper.quote
            );
            System.debug('Ops Payment Information ' + opsForm.Payment_Information__c);
            if(wrapper.prods.size()>1) {
                opsForm.Product_2_Information__c = wrapper.prods.get(1);   
                if(wrapper.prods.size()>2) {
                    opsForm.Product_3_Information__c = wrapper.prods.get(2); 
                }     
            }
            formLst.add(opsForm);
        }

       
        Database.SaveResult[] results = Database.insert(formLst);
        for (Database.SaveResult sr : results) {
            if (!sr.isSuccess()) {
                System.debug('Error creating form.');
            }
        }
    }

    public class Wrapper {
        public Id oppId {get;set;}
        public Id quote {get; set;}
        public Id legalContact {get;set;}
        public String countryAcc {get; set;}
        public Id account {get; set;}
        public Id payment {get; set;}
        public List<Id> prods = new List<Id>();
        public Id bankAccount {get; set;}
        public Id ownerId {get; set;}
        public String nameOpp {get; set;}
    }

}