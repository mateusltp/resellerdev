/**
* @author Bruno Pinho
* @date December/2018
* @description This class extends the TriggerDispatcherBase to provide the dispatching functionality for the trigger actions 
*               on the Account object. The event handlers support allowing and preventing actions for reentrant scenarios. 
*               This is controlled by the flag isBeforeXxxxx and isAfterXxxxx member variables. These variables need to be set
*               to true before invoking the handlers and set to false after the invocation of the handlers. Resetting is MUST
*               as otherwise unit tests MAY fail. The actual actions should be placed in the handlers (in a separate class).
*/
public class EventTriggerDispatcher extends TriggerDispatcherBase
{
    private static Boolean isAfterDeleteProcessing = false; 
    
    public virtual override void afterDelete(TriggerParameters tp)
    {
        if(!isAfterDeleteProcessing)
        {
            isAfterDeleteProcessing = true;
            execute(new EventAfterDeleteTriggerHandler(), tp, TriggerParameters.TriggerEvent.afterDelete);
            isAfterDeleteProcessing = false;
        }
        else 
            execute(null, tp, TriggerParameters.TriggerEvent.afterDelete);
    }
}