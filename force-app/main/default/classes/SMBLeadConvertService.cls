public without sharing class SMBLeadConvertService {

    private Lead lead;
    private Boolean isLeadConverted;
    private Account account;
    private Boolean accountAlreadyExists = false;
    private Contact existingContact;
    private Contact contact;
    private Quote quote;
    private QuoteLineItem quoteLineItem;
    private PricebookEntry pricebookEntry;
    private Boolean contactAlreadyExists = false;

    private Contact billingContact;

    private Opportunity opportunity;

    Boolean isConversionDone = false;

    private class LeadException extends Exception {

    }

    public SMBLeadConvertService(Id leadId){
        this.lead = new Lead(Id = leadId);
    }

    public SMBLeadConvertService(Lead lead, Contact billingContact){
        this.lead = lead;
        this.billingContact = billingContact;
    }

    public Account getAccount() {
        if(isConversionDone) {
            return this.account;
        }
        else {
            throw new LeadException('To get an Account you must execute the Lead Conversion first.');
        }
    }

    public Opportunity getOpportunity() {
        if(isConversionDone) {
            return this.opportunity;
        }
        else {
            throw new LeadException('To get an Opportunity you must execute the Lead Conversion first.');
        }
    }

    public void executeManualLeadConversion() {

        loadLeadById();

        if(checkMandatoryFields()) {

            loadAccountIfExists();

            loadContactIfExists();
    
            if(isToConvertLead()) {
    
                convertLead();
    
                mergeContactsIfNeeded();

                updateAccount(false); // false because it is not a client yet

                updateContact();

                createOpportunity();
    
            } 

            isConversionDone = true;
        }           
            
    }

    public void executeLeadConversion() {

        loadLeadByEmail();

        loadAccountIfExists();

        loadContactIfExists();

        if(isToConvertLead()) {

            upsertLead();

            convertLead();

            mergeContactsIfNeeded();

            createOrUpdateBillingContact();

            updateAccount(true); // true because it is a client already

            updateContact();

            isConversionDone = true;

        }       

    }

    private void loadLeadById() {

        Id recordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('SMB Direct Channel').getRecordTypeId();
        
        List<Lead> leadList = [SELECT Id, Client_UUID__c, Email, Phone, IsConverted, CNPJ__c, Language_Code__c,UTM_url__c, UTM_campaign__c, UTM_source__c, 
                    UTM_medium__c, UTM_content__c, UTM_term__c, Coupon_Code__c, Coupon_Expiration__c, Coupon_Amount__c, Total_Value__c, 
                    Total_Value_W_Discount__c, SKU_Selected__c, Contact_UUID__c, FirstName, LastName, Title, Company, Razao_social__c, 
                    Legal_Document_Type__c, Business_Unit__c, CurrencyIsoCode, NumberOfEmployees, Number_of_Employees_Rounded__c, Street, 
                    State, PostalCode, CountryCode, District__c, Complements__c, City, Finance_Contact_Email__c, Sales_Flow__c
                    FROM Lead 
                    WHERE Id = :this.lead.Id
                    AND recordTypeId = :recordTypeId 
                    ORDER BY LastModifiedDate DESC LIMIT 1];
        
        if(!leadList.isEmpty()) {

            this.lead = leadList[0];

            isLeadConverted = this.lead.IsConverted;
         
        }
        else {
            throw new LeadException('The Lead doesn\'t exist or the record Type is not SMB Direct Channel');
        }
        
    }

    private Boolean checkMandatoryFields() {

        List<String> missingRequiredFields = new List<String>();
        
        if (String.isBlank(this.lead.Client_UUID__c )) {
            missingRequiredFields.add('Client UUID');
        }
    
        if (String.isBlank(this.lead.Company)) {
            missingRequiredFields.add('Company');
        }

        if (String.isBlank(this.lead.Razao_social__c)) {
            missingRequiredFields.add('Legal Name');
        }

        if (String.isBlank(this.lead.CNPJ__c)) {
            missingRequiredFields.add('Legal Number');
        }

        if (String.isBlank(this.lead.Business_Unit__c)) {
            missingRequiredFields.add('Business Unit');
        }
    
        if (String.isBlank(this.lead.CurrencyIsoCode)) {
            missingRequiredFields.add('Lead Currency');
        }

        if (this.lead.NumberOfEmployees == null) {
            missingRequiredFields.add('Total no. of Employees');
        }

        if (String.isBlank(this.lead.Language_Code__c)) {
            missingRequiredFields.add('Language');
        }

        if (String.isBlank(this.lead.Sales_Flow__c)) {
            missingRequiredFields.add('Sales flow');
        }

        if (String.isBlank(this.lead.Contact_UUID__c)) {
            missingRequiredFields.add('Contact UUID');
        }

        if (String.isBlank(this.lead.FirstName)) {
            missingRequiredFields.add('First Name');
        }

        if (String.isBlank(this.lead.LastName)) {
            missingRequiredFields.add('Last Name');
        }

        if (String.isBlank(this.lead.email)) {
            missingRequiredFields.add('Email');
        }
        
        if(missingRequiredFields.isEmpty())
            return true;
        else {

            String errorMessage = 'To convert a Lead it is mandatory to populate the following fields: ';

            throw new LeadException(errorMessage + String.join( missingRequiredFields, ', ' ));
        }

    }

    private void loadLeadByEmail() {
        Id recordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('SMB Direct Channel').getRecordTypeId();
        List<Lead> leadList = [SELECT Id, IsConverted, UTM_URL__c, UTM_Campaign__c, UTM_Source__c, UTM_Medium__c, UTM_Content__c, UTM_Term__c 
                    FROM Lead 
                    WHERE Email = :this.lead.Email
                    AND recordTypeId = :recordTypeId 
                    ORDER BY LastModifiedDate DESC LIMIT 1];
        
        if(!leadList.isEmpty()) {

            Lead existingLead = leadList[0];

            isLeadConverted = existingLead.IsConverted;

            this.lead.Id = existingLead.Id;
            this.lead.UTM_URL__c = existingLead.UTM_URL__c + this.lead.UTM_URL__c;
            this.lead.UTM_Campaign__c = existingLead.UTM_Campaign__c + this.lead.UTM_Campaign__c;
            this.lead.UTM_Source__c = existingLead.UTM_Source__c + this.lead.UTM_Source__c;
            this.lead.UTM_Medium__c = existingLead.UTM_Medium__c + this.lead.UTM_Medium__c;
            this.lead.UTM_Content__c = existingLead.UTM_Content__c + this.lead.UTM_Content__c;
            this.lead.UTM_Term__c = existingLead.UTM_Term__c + this.lead.UTM_Term__c;
            
        }
        else {
            isLeadConverted = false;
        }
        
    }

    private void loadAccountIfExists() {
        List<Account> accountList = [SELECT Id, Type 
                                     FROM Account 
                                     WHERE Id_Company__c = :this.lead.CNPJ__c LIMIT 1];

        if(!accountList.isEmpty()) {
            this.account = accountList[0];
            this.accountAlreadyExists = true;
        }
    }

    private void loadContactIfExists() {
        List<Contact> contactList = [SELECT Id, AccountId, Role__c 
                                FROM Contact 
                                WHERE Email = :this.lead.Email 
                                ORDER BY LastModifiedDate DESC LIMIT 1];
        if(!contactList.isEmpty()) {
            this.existingContact = contactList[0];
            this.contactAlreadyExists = true;
        }
    }

    private Boolean isToConvertLead() {
        
        if(this.accountAlreadyExists && !isLeadConverted) {
            switch on this.account.Type {
                when 'Client' {
                    throw new LeadException('The Lead is already a client.'); // this lead is already a client so doesnt need to be converted
                }
                when else {
                    return true; // this lead account already exist but it is not a client yet
                }
            }
        }
        else {
            if(isLeadConverted) {
                throw new LeadException('The Lead is already converted.'); // lead is converted but the tax id doesnt match any account
            }
            else {
                return true; // lead is not converted and account doesn0t exist (tax id doesnt match)
            }
        }
    }

    private void upsertLead() {
        upsert this.lead;
    }

    private void convertLead() {

        Database.LeadConvert leadConvert = new Database.LeadConvert();
        leadConvert.setLeadId(this.lead.id);
        leadConvert.setDoNotCreateOpportunity(true);
        
        LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true AND MasterLabel = 'Converted' LIMIT 1];
        leadConvert.setConvertedStatus(convertStatus.MasterLabel);

        if(this.accountAlreadyExists) {
            leadConvert.setAccountId(this.account.Id);
            if(this.contactAlreadyExists && this.account.Id == this.existingContact.AccountId) {
                leadConvert.setContactId(this.existingContact.Id);
            }     
        }

        Database.DMLOptions dmlOptions = new Database.DMLOptions();
        dmlOptions.DuplicateRuleHeader.AllowSave = true;
        dmlOptions.OptAllOrNone = true;

        Database.LeadConvertResult leadConvertResult = Database.convertLead(leadConvert, dmlOptions);

        this.account = new Account(Id = leadConvertResult.getAccountId());

        this.contact = new Contact(Id = leadConvertResult.getContactId());

    }

    private void updateAccount(Boolean isClient) {
        
        this.account.Name = this.lead.Company;
        this.account.UUID__c = this.lead.Client_UUID__c;
        this.account.Razao_Social__c =  this.lead.Company;
        this.account.Trade_Name__c = this.lead.Razao_social__c;
        this.account.Legal_Document_Type__c = this.lead.Legal_Document_Type__c;
        this.account.Id_Company__c = this.lead.CNPJ__c;
        this.account.Business_Unit__c = this.lead.Business_Unit__c;
        this.account.CurrencyIsoCode = this.lead.CurrencyIsoCode;
        this.account.NumberOfEmployees = this.lead.NumberOfEmployees;
        this.account.Number_Of_Employees_Rounded__c = this.lead.Number_of_Employees_Rounded__c;
        this.account.BillingStreet = this.lead.Street;
        this.account.BillingState = this.lead.State;
        this.account.BillingPostalCode = this.lead.PostalCode;
        this.account.BillingCountryCode = this.lead.CountryCode;
        this.account.District__c = this.lead.District__c; 
        this.account.Complements__c = this.lead.Complements__c;
        this.account.BillingCity = this.lead.City;

        if(isClient) this.account.Type = 'Client';

        this.account.Send_to_Tagus__c = true;

        this.account.Email__c = this.lead.Finance_Contact_Email__c;

        if(this.billingContact != null) this.account.Attention__c = this.billingContact.Id;

        update this.account;  

    }

    private void updateContact() {     

        this.contact.Email = this.lead.Email;           
        this.contact.Phone = this.lead.Phone;
        this.contact.Language_Code__c = this.lead.Language_Code__c;
        this.contact.UTM_url__c = this.lead.UTM_url__c;
        this.contact.UTM_campaign__c = this.lead.UTM_campaign__c;
        this.contact.UTM_source__c = this.lead.UTM_source__c;
        this.contact.UTM_medium__c = this.lead.UTM_medium__c;
        this.contact.UTM_content__c = this.lead.UTM_content__c;
        this.contact.UTM_term__c = this.lead.UTM_term__c;
        this.contact.Coupon_Code__c = this.lead.Coupon_Code__c;
        this.contact.Coupon_Expiration__c = this.lead.Coupon_Expiration__c;
        this.contact.Coupon_Amount__c = this.lead.Coupon_Amount__c;
        this.contact.Total_Amount__c = this.lead.Total_Value__c;
        this.contact.Total_Amount_w_discount__c = this.lead.Total_Value_W_Discount__c;
        this.contact.SKU_Selected__c = this.lead.SKU_Selected__c;
        this.contact.UUID__c = this.lead.Contact_UUID__c;
        this.contact.FirstName = this.lead.FirstName;
        this.contact.LastName = this.lead.LastName;
        this.contact.Title = this.lead.Title;
        this.contact.Cargo__c = this.lead.Title;
        
        this.contact.MailingStreet = this.lead.Street;
        this.contact.MailingState = this.lead.State;
        this.contact.MailingPostalCode = this.lead.PostalCode;
        this.contact.MailingCountryCode = this.lead.CountryCode;
        this.contact.MailingCity = this.lead.City;
        this.contact.District__c = this.lead.District__c;
        this.contact.Complements__c = this.lead.Complements__c;
              
        if(this.contactAlreadyExists && this.existingContact.Id == this.contact.Id) {
            this.contact.Role__c = this.existingContact.Role__c;
        }

        addAdminPicklistRole(this.contact);
        this.contact.Primary_HR_Contact__c = true;

        if(this.lead.Finance_Contact_Email__c == this.contact.Email) {
            this.contact.Finance_Contact__c = true;
            addFinancePicklistRole(this.contact);
        }

        update this.contact;

    }

    private void mergeContactsIfNeeded() {

        if(this.contactAlreadyExists && this.existingContact.Id != this.contact.Id) {
            
            Id oldRelatedDirectAccountId = this.existingContact.AccountId;

            merge this.contact this.existingContact;

            if(oldRelatedDirectAccountId != null) {
                insert new AccountContactRelation(AccountId = oldRelatedDirectAccountId, ContactId = this.contact.Id);
            }      
        }

    }

    private void addPicklistRole(Contact contact, String rolePicklistValue) {
        if(String.isBlank(contact.Role__c)){
            contact.Role__c = rolePicklistValue;
        }
        else if(!contact.Role__c.contains(rolePicklistValue)) {
            contact.Role__c += ';' + rolePicklistValue;
        }
    }

    private void addFinancePicklistRole(Contact contact) {
        addPicklistRole(contact, 'FINANCE');
    }

    private void addAdminPicklistRole(Contact contact) {
        addPicklistRole(contact, 'ADMIN');
    }

    private void createOrUpdateBillingContact() {

        if(this.billingContact != null) {

            if(this.lead.Finance_Contact_Email__c != this.lead.Email) {
                
                Database.DMLOptions dmlOptions = new Database.DMLOptions();
                dmlOptions.DuplicateRuleHeader.AllowSave = true;
                dmlOptions.OptAllOrNone = true;

                List<Contact> existingBillingContacts = [SELECT Id, AccountId, Role__c FROM Contact WHERE Email = :this.lead.Finance_Contact_Email__c];

                if(existingBillingContacts.isEmpty()) {      

                    this.billingContact.AccountId = this.account.Id;
                    addFinancePicklistRole(this.billingContact);

                    Database.insert(this.billingContact, dmlOptions);

                }
                else {

                    Contact existingBillingContact = existingBillingContacts[0];
                    this.billingContact.Id = existingBillingContact.Id;
                    this.billingContact.AccountId = this.account.Id;
                    addFinancePicklistRole(this.billingContact);
                    
                    Database.update(this.billingContact, dmlOptions);
    
                }

            }
            else {
                this.billingContact.Id = this.contact.Id; // it is needed to have the billingContact id always populated. This is for the case that billing contact and contact are the same
            }
            
        } 

    }

    private void createOpportunity() {
                
        this.opportunity = new Opportunity(
            RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SMB_New_Business').getRecordTypeId(),
            Name = this.account.Name + ' -',
            AccountId = this.Account.Id,
            StageName = 'Creation',
            CloseDate = System.Date.today().addYears(1)
        );
        insert this.opportunity;
        
/*
        this.opportunity = new Opportunity(
            RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SMB_New_Business').getRecordTypeId(),
            Name = this.account.Name + ' ',
            AccountId = this.Account.Id,
            StageName = 'Validated',
            FastTrackStage__c = 'Offer Creation',
            CloseDate = System.Date.today().addYears(1),
            Coupon_Code__c = lead.Coupon_Code__c,
            Coupon_Expiration__c = lead.Coupon_Expiration__c,
            Coupon_Amount__c = lead.Coupon_Amount__c,
            
            Total_Amount_w_discount__c = lead.Total_Value_W_Discount__c,
            Sku_selected__c = lead.SKU_Selected__c,
            Data_do_Lancamento__c = System.today().addDays(21)
        );
        insert this.opportunity;

        this.quote = new Quote(
            Name = 'Proposal - ' + this.opportunity.Name,
            CurrencyIsoCode = this.pricebookEntry.CurrencyIsoCode,
            OpportunityId = this.opportunity.Id,
            Start_Date__c = this.opportunity.Data_do_Lancamento__c,
            End_Date__c = this.opportunity.CloseDate,
            Pricebook2Id = this.pricebookEntry.Pricebook2Id,
            Total_Number_of_Employees__c = this.account.NumberOfEmployees,
            License_Fee_Waiver__c = 'No',
            Employee_Registration_Method__c = 'Eligible file',
            Contact_Permission__c = 'Allowlist',
            Unique_Identifier__c = 'Corporate E-mail',
            RecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId()
            
        );
        Database.upsert(quote);

        this.quoteLineItem = new QuoteLineItem(
            QuoteId = this.quote.Id,
            Quantity = this.quote.Total_Number_of_Employees__c,
            UnitPrice = this.pricebookEntry.UnitPrice,
            Product2Id = this.pricebookEntry.Product2Id,
            PricebookEntryId = this.pricebookEntry.Pricebook2Id
 
        );
        insert this.quoteLineItem;
        */
    }

    private PricebookEntry getPricebookEntry() {
        List<PricebookEntry> pricebookEntries = [
            SELECT Id, CurrencyIsoCode, Pricebook2Id, Product2Id, UnitPrice
            FROM PricebookEntry
            WHERE
                IsActive = TRUE
                AND Product2.IsActive = TRUE
                AND Product2.Family = 'Enterprise Subscription'
                AND Pricebook2.IsStandard = TRUE
                AND Product2.Copay2__c = FALSE
                AND Product2.Family_Member_Included__c = FALSE
                AND Product2.Minimum_Number_of_Employees__c <= :this.opportunity.Quantity_Offer_Number_of_Employees__c
                AND Product2.Maximum_Number_of_Employees__c >= :this.opportunity.Quantity_Offer_Number_of_Employees__c
                AND CurrencyIsoCode = :this.account.CurrencyIsoCode
            LIMIT 1
        ];
        if (pricebookEntries.isEmpty()) {
            throw new IntegrationException(
                'No pricebook found based in the number of employees and the currency id'
            );
        }
        return pricebookEntries[0];
    }

}