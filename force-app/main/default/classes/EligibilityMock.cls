@IsTest
public class EligibilityMock {
  public static Eligibility__c getStandard(Payment__c payment) {
    return new Eligibility__c(
      Name = 'Headquarters',
      Communication_Restriction__c = '3. Accept communications with employees after sign up',
      Is_Default__c = true,
      Payment_Method__c = 'Payroll',
      Payment__c = payment.Id,
      Recurring_Billing_Period__c = 'Monthly',
      Paid_By__c = payment.Account__c,
      Managed_By__c = payment.Account__c
    );
  }
}