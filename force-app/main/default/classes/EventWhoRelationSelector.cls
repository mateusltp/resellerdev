/**
 * @description       : 
 * @author            : alysson.mota@gympass.com
 * @group             : 
 * @last modified on  : 04-08-2022
 * @last modified by  : alysson.mota@gympass.com
**/
public with sharing class EventWhoRelationSelector extends ApplicationSelector{
    public List<Schema.SObjectField> getSObjectFieldList() {
		return new List<Schema.SObjectField> {
			EventWhoRelation.Id
		};
	}

	public Schema.SObjectType getSObjectType() {
		return EventWhoRelation.sObjectType;
	}

    public List<EventWhoRelation> byEventIdAndTypeContact(Set<Id> eventIds) {
        return (List<EventWhoRelation>) Database.query(
				newQueryFactory().
				selectField(EventWhoRelation.Id).
				selectField(EventWhoRelation.RelationId).
				selectField(EventWhoRelation.EventId).
				selectField('Event.WhatId').
				setCondition('EventId IN: eventIds AND Type = \'Contact\'').
				toSOQL()
		);
    }
}