/**
 * @File Name          : LeadWithParentAccountModel.cls
 * @Description        : 
 * @Author             : GEPI@GFT.com
 * @Group              : 
 * @Last Modified By   : GEPI@GFT.com
 * @Last Modified On   : 07-24-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    08/06/2020   Samuel Silva - GFT (slml@gft.com)     Initial Version
**/
public without sharing class LeadWithParentAccountModel {
    
    public static List<Account> findPotentialParentAccount(String companyName, String street){
        return findPotentialParentAccount(companyName, street, null);
    }
    
    public static List<Account> findPotentialParentAccount(String companyName, String street, String website){
        String searchCompany = '%' + companyName+ '%';
        String searchStreet = '%' + street + '%';
         
        String query = 'SELECT Id, Name, ParentId, Parent.Name, ShippingStreet, ShippingCity, ShippingState FROM Account WHERE (Name LIKE: searchCompany OR ShippingStreet LIKE: searchStreet)';
        if(website != null){
              query+= ' OR (Website =: website AND ShippingStreet LIKE: searchStreet)';
        }   
        List<Account> accLst = Database.query(query);
        return accLst;
    }
}