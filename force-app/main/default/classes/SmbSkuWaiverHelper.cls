public without sharing class SmbSkuWaiverHelper {

    public static Boolean checkApprovalNeed(Id productId, List<Waiver__c> waivers, List<Waiver__c> waiversToDelete) {
        populateEndDateAndDuration(productId, waivers);
        return new SmbSkuWaiverApprovalHelper().checkApprovalNeed(waivers, waiversToDelete);
    }

    public static void populateEndDateAndDuration(Id productId, List<Waiver__c> waivers) {
        QuoteLineItem quoteLineItem = [SELECT Quote.Opportunity.Current_Billing_Day__c, QuoteId, Quote.Opportunity.Current_Order_End_Date__c 
                                            FROM QuoteLineItem 
                                            WHERE Id = :productId LIMIT 1];

        Date currentDate = Date.today();
        Integer billingDay = Integer.valueOf(quoteLineItem.Quote.Opportunity.Current_Billing_Day__c);
        if(billingDay < currentDate.day()) {
            currentDate.addMonths(1);
        }
        currentDate = Date.newInstance(currentDate.year(), currentDate.month(), billingDay);

        for(Waiver__c waiver: waivers) {
            List<String> dateSplitted = waiver.End_Date_Text__c.split('/'); // month day year
            Integer year = Integer.valueOf(dateSplitted[2]);
            Integer month = Integer.valueOf(dateSplitted[0]);
            Integer day = Integer.valueOf(dateSplitted[1]);
            waiver.End_Date__c = Date.newInstance(year, month, day);
            if(waiver.Position__c == 1) {
                waiver.Start_Date__c = Date.today();
                Integer duration = waiver.Start_Date__c.monthsBetween(waiver.End_Date__c);
                waiver.Duration__c = duration == 0? duration + 1 : duration;
            }
            if(waiver.Position__c > 1) {
                Integer duration = currentDate.monthsBetween(waiver.End_Date__c);
            	waiver.Duration__c = duration;
                Integer i = Integer.valueOf(waiver.Position__c) - 2;
                Integer y = waivers[i].End_Date__c.year();
                Integer m = waivers[i].End_Date__c.month();
                Integer d = waivers[i].End_Date__c.day();
                Date startDate = Date.newInstance(y, m, d).addDays(1);
                waiver.Start_Date__c = startDate;
            }
            currentDate = waiver.End_Date__c;
        }

    }

}