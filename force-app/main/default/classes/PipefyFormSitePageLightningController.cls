global without sharing class PipefyFormSitePageLightningController {

    @AuraEnabled
    global static responseModel getInfo(Id oppId){
        system.debug(oppId);
        
        responseModel response = new responseModel();
        response.opp = [SELECT Id, Name, Owner.Name,FastTrackStage__c FROM Opportunity WHERE Id =: oppId];
        system.debug(oppId);
        
        Set<Id> accountIdsSet = new Set<Id>();
        for( Account_Opportunity_Relationship__c accOppRel : [SELECT Id, Account__c FROM Account_Opportunity_Relationship__c WHERE Opportunity__c =: oppId]){
            system.debug('accopprel ' + accopprel.Account__c);
            accountIdsSet.add(accOppRel.Account__c);
        }
        system.debug(accountIdsSet.size());
        
        response.accList = [SELECT Id, Name, Razao_Social__c, Id_Company__c, ID_Gympass_CRM__c FROM Account WHERE Id = :accountIdsSet ];
        return response;
    }
    
    @AuraEnabled
    global static void saveAccounts(String idToCrmIdListStringified){
        //List<idToCrmIdModel> idToCrmIdList = (List<idToCrmIdModel>)JSON.deserialize(idToCrmIdListStringified, List<idToCrmIdModel>.class);
        Map<String, String> IdToCrmIdMap = new Map<String, String>();
        for(idToCrmIdModel idToCrmId : (List<idToCrmIdModel>)JSON.deserialize(idToCrmIdListStringified, List<idToCrmIdModel>.class)){
            IdToCrmIdMap.put(idToCrmId.accId , idToCrmId.crmId);
        }
        List<Account> accList = [SELECT Id, ID_Gympass_CRM__c FROM Account WHERE Id=: IdToCrmIdMap.keySet()];
        for(Account acc : accList){
            acc.ID_Gympass_CRM__c = IdToCrmIdMap.get(acc.Id);
        }
        update accList;
    }
    
    
    global class responseModel{
        @AuraEnabled
        global Opportunity opp;
        @AuraEnabled
        global List<Account> accList;
    }
    
    global class idToCrmIdModel{
        @AuraEnabled
        global String accId {get;set;}
        @AuraEnabled
        global String crmId {get;set;}
    }
}