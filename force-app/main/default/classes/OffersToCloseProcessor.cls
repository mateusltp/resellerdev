/**
 * @author vncferraz
 * 
 * Facade implementation class to provide specialization for OfferProcessor on "Close" requests
 */
public class OffersToCloseProcessor extends OfferQueueProcessor{
    
    public OffersToCloseProcessor(List<Offer_Queue__c> offers){
        super(offers);
    }

    override
    protected void init(){
        this.builder = new OffersToCloseWrapperBuilder();
    }

}