/**
 * @description       : 
 * @author            : gft.jorge.stevaux@ext.gympass.com
 * @group             : 
 * @last modified on  : 03-28-2022
 * @last modified by  : gft.jorge.stevaux@ext.gympass.com
**/
    @isTest 
      public class AssignLeadsUsingAssignmentRulesTest{
      static testMethod void createnewlead() {
      User userToCreate = [Select id from user where profile.name='System Administrator' Limit 1];
      
      Test.startTest();    
      Lead leadToCreate =new Lead();
      List<id> Ids= New List<Id>();
      leadToCreate.ownerid= userToCreate.id;
      leadToCreate.LastName ='Gupta';
      leadToCreate.Company='Salesforce';
      leadToCreate.LeadSource='Partner Referral';
      leadToCreate.Rating='';
      leadToCreate.Status='';
      leadToCreate.Phone='1532336589';
      leadToCreate.Email='leadtoCreat@leadtoocreate.com.br';
      leadToCreate.Country='Spain';
      insert leadToCreate; 
      
      Ids.add(leadToCreate.id);
      AssignLeadsUsingAssignmentRules.leadAssign(Ids);
      
      Test.stopTest();
   }
}