/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 04-19-2022
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/
public inherited sharing  class FileObjectSelector extends ApplicationSelector {
  

    public List<Schema.SObjectField> getSObjectFieldList() {
		return new List<Schema.SObjectField> {
			FIleObject__c.Id
		};
	}

	public Schema.SObjectType getSObjectType() {
		return FIleObject__c.sObjectType;
	}

    public Map<Id, List<FIleObject__c>> selectByAccountId (Set<Id> accountIds) {	
		Map<Id, List<FIleObject__c>> result = new Map<Id, List<FIleObject__c>>();
		for(FIleObject__c iFileObj : ( List<FIleObject__c> ) Database.query( 	
																			newQueryFactory().				
																			selectField(FIleObject__c.Id).
																			selectField(FIleObject__c.File_Type__c).
																			selectField(FIleObject__c.Name).
                                                                            selectField(FIleObject__c.Account__c).	
																			selectField(FIleObject__c.conversionId__c).	
																			selectField(FIleObject__c.File_Name__c ).	
																			selectField(FIleObject__c.Format__c ).																				
																			selectField('Account__r.Name').																									
																			setCondition('Account__c != null  AND Account__c IN : accountIds').
																			toSOQL()
							                                            )){
			if(result.containsKey(iFileObj.Account__c)){
				result.get(iFileObj.Account__c).add(iFileObj);
			} else{
				result.put(iFileObj.Account__c, new List<FIleObject__c>{iFileObj});
			}
		}
		return result;
	}
}