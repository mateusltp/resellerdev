/**
 * @description       : 
 * @author            : Samuel Silva - GFT (slml@gft.com)
 * @group             : 
 * @last modified on  : 08-26-2020
 * @last modified by  : Samuel Silva - GFT (slml@gft.com)
 * Modifications Log 
 * Ver   Date         Author                              Modification
 * 1.0   08-24-2020   Samuel Silva - GFT (slml@gft.com)   Initial Version
**/
public with sharing class OpportunityAccountManagerForClients extends OpportunityAccountManagerModel {
 

    public OpportunityAccountManagerForClients(String oppId){
        super(oppId);
    }
    
    protected override Object findRelatedAccounts() {
        Set<Id> relatedAccSet = new Set<Id>();
        Opportunity opp = [SELECT Account.NumberOfEmployees FROM Opportunity WHERE ID =: oppId];
        Integer employees = opp.Account.NumberOfEmployees;
        List<Account_Opportunity_Relationship__c> accsRelatedToOpp = [ SELECT Opportunity__c, Account__c 
                                                                       FROM Account_Opportunity_Relationship__c 
                                                                       WHERE Opportunity__c =: oppId];
        if(!accsRelatedToOpp.isEmpty()){
            for(Account_Opportunity_Relationship__c accR : accsRelatedToOpp){
                relatedAccSet.add(accR.Account__c);
            }
        }
        Set<Account> accAvailableSet = new Set<Account> ([ SELECT Id, NumberOfEmployees, Name, BillingCity, BillingState, BillingCountry 
                                        FROM Account 
                                        WHERE Id IN: totalAccIDs]);

        Map<String, Object> mapResult = new Map<String, Object>();
        Set<Account> accRelatedToOpp = new Set<Account>();
        if(!accAvailableSet.isEmpty()){         
            for(Account acc : accAvailableSet){
                if(relatedAccSet.contains(acc.Id)){
                    accRelatedToOpp.add(acc);
                    employees += acc.NumberOfEmployees;                 
                }
            }
            accAvailableSet.removeAll(accRelatedToOpp);         
        } 
        mapResult.put('accRelatedToOpp',new List<Account>(accRelatedToOpp));
        mapResult.put('accAvailable',new List<Account>(accAvailableSet));
        mapResult.put('numberOfEmployees', employees);
        System.debug('mapresult ' + mapResult);
        return mapResult; 
    } 
}