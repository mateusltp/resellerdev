public without sharing class EligibilityDTO {
  private String id;
  private String salesforce_id;
  private String item_name;
  private String paid_by_client_id;
  private String managed_by_client_id;
  private String default_item;
  private String welcome_email_permitted;
  private Integer max_dependents;
  private IdentifierType identifier_type;
  private ExchangeType exchange_type;
  private CommunicationType communication_permitted_type;
  private PayrollDeductionType payroll_deduction_type;
  private PaymentMethodType payment_method_type;

  public EligibilityDTO(Eligibility__c eligibility) {
    this.id = eligibility.UUID__c != null
      ? eligibility.UUID__c
      : new Uuid().getValue();
    this.salesforce_id = eligibility.Id;
    this.item_name = eligibility.Name;

    this.default_item = eligibility.Is_Default__c == null
      ? null
      : String.valueOf(eligibility.Is_Default__c);

    this.welcome_email_permitted = String.valueOf(
      getWelcomeEmailPermitted(eligibility)
    );
    this.max_dependents = 1; // remove
    this.identifier_type = getIdentifierType(eligibility);
    this.exchange_type = getExchangeType(eligibility);
    this.communication_permitted_type = getCommunicationType(eligibility);
    this.payroll_deduction_type = getPayrollDeductionType(eligibility);
    this.payment_method_type = getPaymentMethodType(eligibility);
    this.paid_by_client_id = eligibility.Paid_By__r.UUID__c;
    this.managed_by_client_id = eligibility.Managed_By__r.UUID__c;
  }

  private Boolean getWelcomeEmailPermitted(Eligibility__c eligibility) {
    return String.isNotBlank(eligibility.Communication_Restriction__c) &&
      eligibility.Communication_Restriction__c.contains(
        '1. Accept welcome email communications only'
      );
  }

  @TestVisible
  private IdentifierType getIdentifierType(Eligibility__c eligibility) {
    switch on eligibility.Identifier_Type__c {
      when 'Company ID' {
        return IdentifierType.EMPLOYEE_NUMBER;
      }
      when 'National ID' {
        return IdentifierType.NATIONAL_ID;
      }
      when ('Corporate E-mail') {
        return IdentifierType.EMAIL;
      }
      when else {
        return IdentifierType.EMAIL;
      }
    }
  }

  @TestVisible
  private ExchangeType getExchangeType(Eligibility__c eligibility) {
    switch on eligibility.Exchange_Type__c {
      when 'SSO' {
        return EligibilityDTO.ExchangeType.SSO;
      }
      when 'API' {
        return EligibilityDTO.ExchangeType.VALIDATION_API;
      }
      when 'Open Domain' {
        return EligibilityDTO.ExchangeType.OPEN_DOMAIN;
      }
      when 'Eligible file' {
        return EligibilityDTO.ExchangeType.FILE_UPLOAD;
      }
      when else {
        return null;
      }
    }
  }

  @TestVisible
  private CommunicationType getCommunicationType(Eligibility__c eligibility) {
    switch on eligibility.Contact_Permission__c {
      when 'Blocklist' {
        return EligibilityDTO.CommunicationType.NO_ONE;
      }
      when ('Sign-up only') {
        return EligibilityDTO.CommunicationType.SIGNED_UP;
      }
      when 'Active members only' {
        return EligibilityDTO.CommunicationType.ENROLLED;
      }
      when 'Allowlist' {
        return EligibilityDTO.CommunicationType.ELIGIBLES;
      }
      when else {
        return null;
      }
    }
  }

  @TestVisible
  private PayrollDeductionType getPayrollDeductionType(
    Eligibility__c eligibility
  ) {
    switch on eligibility.Payment_Method__c {
      when ('Payroll + Credit Card') {
        return EligibilityDTO.PayrollDeductionType.AUTO_APPROVAL;
      }
      when 'Payroll' {
        return EligibilityDTO.PayrollDeductionType.AUTO_APPROVAL;
      }
      when else {
        return EligibilityDTO.PayrollDeductionType.NO_DEDUCTION;
      }
    }
  }

  @TestVisible
  private PaymentMethodType getPaymentMethodType(Eligibility__c eligibility) {
    switch on eligibility.Payment_Method__c {
      when 'Credit Card' {
        return EligibilityDTO.PaymentMethodType.STANDARD_METHODS;
      }
      when ('Payroll + Credit Card') {
        return EligibilityDTO.PaymentMethodType.STANDARD_METHODS;
      }
      when 'Payroll' {
        return EligibilityDTO.PaymentMethodType.STANDARD_METHODS;
      }
      when else {
        return EligibilityDTO.PaymentMethodType.NO_PAYMENT_METHODS;
      }
    }
  }

  public String getId() {
    return this.id;
  }

  public String getSalesforceId() {
    return this.salesforce_id;
  }

  public Eligibility__c parseToSEligibility() {
    return new Eligibility__c(
      UUID__c = this.id,
      Name = this.item_name,
      Is_Default__c = this.default_item != null
        ? Boolean.valueOf(this.default_item)
        : true,
      Communication_Restriction__c = Boolean.valueOf(
          this.welcome_email_permitted
        )
        ? '1. Accept welcome email communications only'
        : null,
      Payment_Method__c = getPaymentMethodType(this)
    );
  }

  @TestVisible
  private String getPaymentMethodType(EligibilityDTO eligibility) {
    switch on eligibility.payment_method_type {
      when STANDARD_METHODS {
        return 'Payroll + Credit Card';
      }
      when NO_PAYMENT_METHODS {
        return null;
      }
      when else {
        return null;
      }
    }
  }

  @TestVisible
  private enum IdentifierType {
    EMAIL,
    NATIONAL_ID,
    EMPLOYEE_NUMBER
  }

  @TestVisible
  private enum ExchangeType {
    SSO,
    VALIDATION_API,
    OPEN_DOMAIN,
    SFTP_INTEGRATION,
    FILE_UPLOAD
  }

  @TestVisible
  private enum CommunicationType {
    ELIGIBLES,
    SIGNED_UP,
    ENROLLED,
    NO_ONE
  }

  @TestVisible
  private enum PayrollDeductionType {
    AUTO_APPROVAL,
    NO_DEDUCTION
  }

  @TestVisible
  private enum PaymentMethodType {
    STANDARD_METHODS,
    NO_PAYMENT_METHODS
  }
}