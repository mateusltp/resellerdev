/**
* @description       : 
* @author            : gft.jorge.stevaux@ext.gympass.com
* @group             : 
* @last modified on  : 09-13-2021
* @last modified by  : gft.jorge.stevaux@ext.gympass.com
**/
@isTest(seeAllData=false)

public with sharing class AccountDataProviderTest {
    
    @TestSetup
    static void createData(){
        
        Account lAcc = PartnerDataFactory.newAccount();  
        lAcc.Name = 'Parent Acct';            
        Database.insert( lAcc );

        Opportunity lOpp = PartnerDataFactory.newOpportunity( lAcc.Id, 'Partner_Flow_Opportunity' );     
        Database.insert( lOpp );

        Account_Opportunity_Relationship__c lAcctOppRel = PartnerDataFactory.newAcctOppRel(lAcc.Id, lOpp.Id);
        Database.insert(lAcctOppRel);

        Product_Item__c lProd = PartnerDataFactory.newProductFlow();
        Database.insert(lProd);

        Commercial_Condition__c lCommercialCond = PartnerDataFactory.newCommercialCondition();
        Database.insert(lCommercialCond);

        Product_Assignment__c lProductAssignment = PartnerDataFactory.newProductAssignment(lProd.id, lCommercialCond.id);
        Database.insert(lProductAssignment);

        Product_Opportunity_Assignment__c lProductOppAssignment = PartnerDataFactory.newProductOppAssignment(lAcctOppRel.id, lProductAssignment.id);
        Database.insert(lProductOppAssignment);

    }
    
    @IsTest
    static void SDG_Test(){
        
        List<Account> acctLst = [SELECT Id, Types_of_ownership__c, 
                                 Partner_Level__c, ShippingState, 
                                 ShippingCountry, ShippingCountryCode 
                                 FROM Account LIMIT 1];
        
        String testRecordId = acctLst.get(0).id;
        
        // instantiate class
        AccountDataProvider accDataProvider = new AccountDataProvider();
        sortablegrid.SDG coreSDG = accDataProvider.LoadSDG('', testRecordId);
        
        // create the user request
        sortablegrid.SDGRequest request = new sortablegrid.SDGRequest();
        request.ParentRecordID = testRecordId;
        request.PageID = 1;
        request.PageSize = 10;
        request.FieldSetName=null;
        request.RelationshipName=null;
        request.SDGTag='Apex:AccountDataProvider';

        
        // get data
        sortablegrid.SDGResult result = accDataProvider.getData(coreSDG, request);
        system.assertNotEquals(null, result);
    }

    @IsTest
    static void isUserSelectableTest(){

        Boolean isUserSelectable = true;
        System.assertEquals(isUserSelectable, AccountDataProvider.isUserSelectable());
    }


}