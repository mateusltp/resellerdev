/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 03-28-2022
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/
    public inherited sharing class BankAccountRelSelector extends ApplicationSelector {

        public List<Schema.SObjectField> getSObjectFieldList() {
            return new List<Schema.SObjectField> {
                Acount_Bank_Account_Relationship__c.Id,
                Acount_Bank_Account_Relationship__c.Name
            };
        }
    
        public Schema.SObjectType getSObjectType() {
            return Acount_Bank_Account_Relationship__c.sObjectType;
        }
    
        public List<Acount_Bank_Account_Relationship__c> selectById(Set<Id> ids) {
            return (List<Acount_Bank_Account_Relationship__c>) super.selectSObjectsById(ids);
        }

        public List<Acount_Bank_Account_Relationship__c> selectAccIdAndBankAccID(Set<Id> ids) {
            return new List<Acount_Bank_Account_Relationship__c>( (List<Acount_Bank_Account_Relationship__c>)Database.query(
                                                                       newQueryFactory().
                                                                       selectField(Acount_Bank_Account_Relationship__c.Account__c).
                                                                       selectField(Acount_Bank_Account_Relationship__c.Bank_Account__c).
                                                                       setCondition('Account__c in: ids').
                                                                       toSOQL()
                                                                   ));									
        }

}