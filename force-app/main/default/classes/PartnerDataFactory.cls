/**
 * @description       : 
 * @author            : gft.jorge.stevaux@ext.gympass.com
 * @group             : 
 * @last modified on  : 04-26-2022
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/

@isTest
public class PartnerDataFactory {

    private static final PS_Constants constants = PS_Constants.getInstance(); 
    private static final String className = PartnerSetupController.class.getName();

    public static Account newAccount(){
        ID accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gym_Partner').getRecordTypeId();
        Account acc = new Account();
        acc.RecordTypeId = accRecordTypeId;
        acc.Name = 'Account 1';
        acc.Id_Company__c = '01.000.409/0001-70';
        acc.Types_of_ownership__c = 'Franchise';
        acc.Partner_Level__c = 'Franchisee';
        acc.BillingCountry = 'Brazil';
        acc.BillingState = 'São Paulo';
        acc.BillingCity = 'Sorocaba';
        acc.BillingStreet = 'Rua João da Silva';
        acc.ShippingState = 'São Paulo';
        acc.ShippingCity = 'Sorocaba';
        acc.ShippingStreet = 'Rua João da Silva';
        acc.ShippingCountry = 'Brazil';
        acc.ShippingCountryCode = 'BR';
        acc.NumberOfEmployees = 2000;
        acc.Gym_Email__c = 'test@test123.com';
        acc.Gym_Type__c = 'Studios';
        acc.CAP_Value__c = 100;
        acc.Exclusivity__c = 'No';
        acc.Has_market_cannibalization__c = 'No';
        acc.Legal_Title__c = 'Account 1';
        acc.Phone = '+5511999999999';
        acc.Website = 'www.test1.com';
        return acc;
    }

    public static Account newChildAccount(Id aAccId){
        ID accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gym_Partner').getRecordTypeId();
        Account accChild = new Account();
        accChild.RecordTypeId = accRecordTypeId;
        accChild.ParentId = aAccId;
        accChild.Name = 'Account 2';
        accChild.Id_Company__c = '01.000.409/0001-60';
        accChild.Types_of_ownership__c = 'Franchise';
        accChild.Partner_Level__c = 'Franchisee';
        accChild.BillingCountry = 'Brazil';
        accChild.BillingState = 'São Paulo';
        accChild.BillingCity = 'Sorocaba';
        accChild.BillingStreet = 'Rua Jose Paulino';
        accChild.ShippingState = 'São Paulo';
        accChild.ShippingCity = 'Sorocaba';
        accChild.ShippingStreet = 'Rua Jose Paulino';
        accChild.ShippingCountry = 'Brazil';
        accChild.ShippingCountryCode = 'BR';
        accChild.NumberOfEmployees = 2100;
        accChild.Gym_Email__c = 'test@test1234.com';
        accChild.Gym_Type__c = 'Studios';
        accChild.CAP_Value__c = 100;
        accChild.Exclusivity__c = 'No';
        accChild.Has_market_cannibalization__c = 'No';
        accChild.Legal_Title__c = 'Account 2';
        accChild.Phone = '+5515969999999';
        accChild.Website = 'www.test2.com';
        return accChild;
    }

    public static Contact newContact (Id aAccId){
        Contact lContact = new Contact();
        lContact.recordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Gyms_Partner').getRecordTypeId();
        lContact.AccountId = aAccId;
        lContact.LastName ='Test Contact'; 
        lContact.Status_do_contato__c = 'Ativo';
        lContact.Type_of_contact__c ='Legal Representative';
        lContact.Phone ='+5519999639999';
        lContact.Email ='contact@testcontact.com';
        return lContact;
    }

    public static Product_Item__c newProduct(Opportunity opp){
        ID productRecordTypeId = Schema.SObjectType.Product_Item__c.getRecordTypeInfosByDeveloperName().get('Gyms_Product_Item_Partner').getRecordTypeId();
        Product_Item__c product = new Product_Item__c();
        product.Name = 'Test Product';
        product.recordTypeId = productRecordTypeId;
        product.Opportunity__c = opp.id;
        product.Product_Type__c = 'Live';
        product.Is_Network_CAP__c = 'No';
        product.CAP_Value__c = 100;
        product.Net_Transfer_Price__c = 10;
        product.Reference_Price_Value_Unlimited__c = 10;
        product.Do_You_Have_A_No_Show_Fee__c = 'No';
        product.Has_Late_Cancellation_Fee__c = 'No';
        product.Max_Monthly_Visit_Per_User__c = 10;
        product.Maximum_Live_Class_Per_Month__c = 10;
        product.Package_Type__c = 'Other';
        product.Price_Visits_Month_Package_Selected__c = 10.00;
        product.Selected_Plan__c = 'Silver';
        return product;
    }  

    public static Product_Item__c newProductFlow(){
        ID productRecordTypeId = Schema.SObjectType.Product_Item__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product').getRecordTypeId();
        Product_Item__c product = new Product_Item__c();
        product.Name = 'Test Product';
        product.recordTypeId = productRecordTypeId;
        product.Type__c = 'Live';
        product.Category__c = 'Aquatic Activities';
        product.Net_Transfer_Price__c = 100;
        return product;
    }  
    
    public static Opportunity newOpportunity(Id aAccId, String recordType){
        Opportunity lOpp = new Opportunity();
        lOpp.CurrencyIsoCode='BRL';
        lOpp.recordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(recordType).getRecordTypeId();
        lOpp.StageName='Waiting Cancellation';
        lOpp.AccountId = aAccId;
        lOpp.CloseDate = Date.today();
        lOpp.Name ='Test Opp';
        lOpp.Cancellation_date__c = Date.today();
        lOpp.Sub_Type__c ='Retention';
        lOpp.Cancellation_Reason__c ='Business Model Complaint';
        lOpp.Cancellation_Reason_subcategory__c ='Competition';
        lOpp.Request_for_self_checkin__c = 'No';
        lOpp.CMS_Used__c = 'No';
        lOpp.Success_Look_Like__c = 'Yes';
        lOpp.Success_Look_Like_Description__c = 'Yes';
        lOpp.Bank_Account_Related__c = true;
        lOpp.Standard_Payment__c = 'Yes';
        //lopp.Justification_Payment__c = 'No';
        //lopp.Payment__c = null;
        return lOpp;
    }

    public static Quote newQuote(Opportunity opp){
        ID qtRecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Gyms_Quote_Partner').getRecordTypeId();
        Quote qt = new Quote(); 
        qt.Name = 'Test Quote'; 
        qt.RecordTypeId = qtRecordTypeId;
        qt.OpportunityId = opp.Id;  
        qt.discounts_usage_volume_range__c = 'No'; 
        qt.Threshold_Value_1__c = 50;
        qt.Threshold_Value_2__c = 100;
        qt.Threshold_Value_3__c = 150;
        qt.Threshold_Value_4__c = 200;
        qt.First_Discount_Range__c = 5;
        qt.Second_Discount_Range__c = 10;
        qt.Third_Discount_Range__c = 15;
        qt.Fourth_Discount_Range__c = 20;
        return qt;
    }

    public static Bank_Account__c newBankAcct (){
        ID bankAcctRecordTypeId = Schema.SObjectType.Bank_Account__c.getRecordTypeInfosByDeveloperName().get('Gyms_Manage_Bank_Account').getRecordTypeId();
        Bank_Account__c bankAcct = new Bank_Account__c(); 
        bankAcct.RecordTypeId = bankAcctRecordTypeId;
        bankAcct.Bank_Account_Ownership__c = 'Test BankAcct'; 
        bankAcct.Bank_Account_Number_or_IBAN__c = '15';
        return bankAcct;
    }
    
    public static Acount_Bank_Account_Relationship__c newBankAcctRel(Id aAccId, Bank_Account__c bankAcct){
        ID bankAcctRelRecordTypeId = Schema.SObjectType.Acount_Bank_Account_Relationship__c.getRecordTypeInfosByDeveloperName().get('Gyms_Account_Bank_Account_Partner').getRecordTypeId();
        Acount_Bank_Account_Relationship__c bankAcctRel = new Acount_Bank_Account_Relationship__c(); 
        bankAcctRel.RecordTypeId = bankAcctRelRecordTypeId;
        bankAcctRel.Account__c = aAccId; 
        bankAcctRel.Bank_Account__c = bankAcct.id;
        return bankAcctRel;
    }

    public static Account_Opportunity_Relationship__c newAcctOppRel (Id aAccId, Id aOppId){
        ID acctOppRelRecordTypeId = Schema.SObjectType.Account_Opportunity_Relationship__c.getRecordTypeInfosByDeveloperName().get('Partners_AOR').getRecordTypeId();
        Account_Opportunity_Relationship__c acctOppRel = new Account_Opportunity_Relationship__c(); 
        acctOppRel.RecordTypeId = acctOppRelRecordTypeId;
        acctOppRel.Account__c = aAccId; 
        acctOppRel.Opportunity__c = aOppId;
        return acctOppRel;
    }

    public static Gym_Activity__c newGymActivity (Id aProductId){
        Gym_Activity__c gymActivity = new Gym_Activity__c(); 
        gymActivity.Product_Item__c = aProductId; 
        gymActivity.Name = 'Gym Activity Test';
        gymActivity.Name = 'Gym Activity Test';
        gymActivity.CurrencyIsoCode='BRL';
        return gymActivity;
    }

    public static EmailTemplate newEmailTemplate(){
        EmailTemplate validEmailTemplate = new EmailTemplate();
        validEmailTemplate.isActive = true;
        validEmailTemplate.Name = 'name';
        validEmailTemplate.DeveloperName = 'unique_name_addSomethingSpecialHere';
        validEmailTemplate.TemplateType = 'text';
        validEmailTemplate.FolderId = UserInfo.getUserId();
        
        
        return validEmailTemplate;
    }

    
    public static Opening_Hours__c newOpeningHours(Id acountId){
        Opening_Hours__c nOpenHours = new Opening_Hours__c();
        nOpenHours.Account_Related__c = acountId;
        nOpenHours.Open_1__c = Time.newInstance(10, 00, 00, 00);
        nOpenHours.Close_1__c = Time.newInstance(18, 00, 00, 00);
        nOpenHours.Days__c = 'Monday';
        // nOpenHours.Close_Every_Weekend_and_Bank_Holidays__c = true;
        nOpenHours.RecordTypeId = Schema.SObjectType.Opening_Hours__c.getRecordTypeInfosByDeveloperName().get('Partner').getRecordTypeId();
        return nOpenHours;
    }
    
    public static Commercial_Condition__c newCommercialCondition(){
        Commercial_Condition__c commercialCondition = new Commercial_Condition__c();
        commercialCondition.Value__c =222;
        return commercialCondition;
    }

    public static Product_Assignment__c newProductAssignment(Id aProduct, Id aCommercialCondition){
        Product_Assignment__c productAssignment = new Product_Assignment__c(); 
        productAssignment.Name = 'Product Assignment 1';
        productAssignment.ProductId__c = aProduct;
        productAssignment.CommercialConditionId__c = aCommercialCondition;
        return productAssignment;
    }

    public static Product_Opportunity_Assignment__c newProductOppAssignment(Id aOpportunityMemberId, Id aProductAssignmentId){
        Product_Opportunity_Assignment__c productOppAssignment = new Product_Opportunity_Assignment__c(); 
        productOppAssignment.OpportunityMemberId__c = aOpportunityMemberId;
        productOppAssignment.ProductAssignmentId__c = aProductAssignmentId;
        return productOppAssignment;
    }

    public static Gym_Activity_Relationship__c newGymActivityRel(){
        Gym_Activity_Relationship__c gymActivityRel = new Gym_Activity_Relationship__c(); 
        gymActivityRel.Name = 'Cardio - Abd';
        return gymActivityRel;
    }

    public static Step_Towards_Success_Partner__c newStepTowardsSuccess(Id aOppid, Id aAccId){
        Step_Towards_Success_Partner__c stepTowardsRel = new Step_Towards_Success_Partner__c(); 
        stepTowardsRel.Name = 'Exclusivity';
        stepTowardsRel.Step_Number__c = 1;
        stepTowardsRel.Achieved_New_Flow__c = false;
        stepTowardsRel.Related_Account__c = aAccId;
        stepTowardsRel.Related_Opportunity__c = aOppid;
        stepTowardsRel.Metadata_Developer_Name__c = 'Partner_Exclusivity';
        return stepTowardsRel;
    }

    public static APXT_Redlining__Contract_Agreement__c newContractAgreement(Opportunity opp, Account acc, String typeOfContract ){
        APXT_Redlining__Contract_Agreement__c ctAgreement = new APXT_Redlining__Contract_Agreement__c();        
        ctAgreement.Opportunity__c = opp.Id; 
        ctAgreement.APXT_Redlining__Account__c = acc.Id;
        ctAgreement.Type_of_contract__c = typeOfContract;
        ctAgreement.RecordTypeId = Schema.SObjectType.APXT_Redlining__Contract_Agreement__c.getRecordTypeInfosByDeveloperName().get('Partner_Contract').getRecordTypeId();
        return ctAgreement;
    }

    public static Ops_Setup_Validation_Form__c newOpsSetupValidationForm(Opportunity opp, Id accountId, String recordType) {
        Id recordTypePartnerForm = Schema.SObjectType.Ops_Setup_Validation_Form__c.getRecordTypeInfosByDeveloperName().get(recordType).getRecordTypeId();
        Ops_Setup_Validation_Form__c accOppForm = new Ops_Setup_Validation_Form__c (
                recordTypeId = recordTypePartnerForm,
                Name = opp.Name + ' - OPS Form',
                Status__c = 'Approved',
                Did_you_send_the_training_email__c = 'Yes',
                Opportunity__c = opp.Id,
                Account_Information__c = accountId);
        return accOppForm;
    }

    public static Threshold__c newThreshold() {
        Threshold__c th = new Threshold__c();
        th.Threshold_value_start__c = 100;
        th.Discount__c = 15;

        return th;
    }

    public static Product_Threshold_Member__c newThresholdMember(Id productId, Id thresholdId) {
        Product_Threshold_Member__c ptm = new Product_Threshold_Member__c();
        ptm.Product__c = productId;
        ptm.Threshold__c = thresholdId;

        return ptm;
    }

}