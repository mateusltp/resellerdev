/**
* @author vinicius.ferraz
* @description Provide save, fields source and options load for fast track cmp   
*/
public without sharing class FastTrackProposalCreationController {
    public FastTrackProposalCreationController() {}

    @AuraEnabled
    public static FastTrackProposalCreationTO find(String recordId, Integer totalOfEmployees,  List<Id> accountsInOpp){
        FastTrackProposalCreationTO to = new FastTrackProposalCreationBuilder()
                    .withOpportunity(recordId)
                    .withLastCreatedQuoteWhenExists(recordId)
                    .withDealHierarchy(totalOfEmployees, accountsInOpp)
                    .withOpsSetupForm(recordId)
                    .build();    

        return to;
    }

    @AuraEnabled
    public static List<Account> findGympassEntities(){
        AccountRepository accounts = new AccountRepository();
        return accounts.gympassEntities();
    }

    @AuraEnabled
    public static List<Contact> findClientManagers(String recordId){
        OpportunityRepository opportunities = new OpportunityRepository();
        ContactRepository contacts = new ContactRepository();
        SObject opportunity =  opportunities.byId(recordId);
        return contacts.fromAccount( (String) opportunity.get('AccountId') );
    }

    @AuraEnabled
    public static List<User> findExecutives(){
        List<User> allUsers = [SELECT Id, Name FROM User ORDER BY Name];
        return allUsers;
    }

     @AuraEnabled
    public static FastTrackStageCmpMapping__mdt[] getStageCmpMappingMetadata(){
        FastTrackStageCmpMapping__mdt[] lLstFastTrackStageMapping = 
            [ SELECT Label, DeveloperName, ChildComponentApiName__c, ChildComponentParameters__c FROM FastTrackStageCmpMapping__mdt ];
        return lLstFastTrackStageMapping;
    }
    
    @AuraEnabled
    public static FastTrackStageCmpMapping__mdt[] getStageCmpMappingMetadata(String recordId){

        OpportunityRepository opportunities = new OpportunityRepository();
        SObject opportunity =  opportunities.byId(recordId); 
        
        Id indirectNewBusinessRecTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Indirect_Channel_New_Business').getRecordTypeId();
        FastTrackStageCmpMapping__mdt[] lLstFastTrackStageMapping;
        boolean isReseller = false;

        if(opportunity.get('RecordTypeId') == indirectNewBusinessRecTypeId){
            isReseller = true;
        }

        lLstFastTrackStageMapping = [ SELECT Label, DeveloperName, ChildComponentApiName__c, ChildComponentParameters__c FROM FastTrackStageCmpMapping__mdt WHERE Reseller__c = :isReseller];

        return lLstFastTrackStageMapping;
    }

    @AuraEnabled
    public static FastTrackProposalCreationTO saveProposal(String proposalData){ 
        FastTrackProposalCreationTO proposal = (FastTrackProposalCreationTO) JSON.deserialize(proposalData, FastTrackProposalCreationTO.class);        
        FastTrackProposalCreation fastTrackProposalCreation = new FastTrackProposalCreation( proposal.opportunityId );
        fastTrackProposalCreation.save(proposal);      
        fastTrackProposalCreation.refreshRevenues(proposal);    
        return proposal;
    }

    @AuraEnabled
    public static FastTrackProposalCreationTO removeItems(String proposalData){ 
        FastTrackProposalCreationTO proposal = (FastTrackProposalCreationTO) JSON.deserialize(proposalData, FastTrackProposalCreationTO.class);        
        FastTrackProposalCreation fastTrackProposalCreation = new FastTrackProposalCreation( proposal.opportunityId );
        fastTrackProposalCreation.removeCurrentItems(proposal.quoteId);      
        return proposal;
    }
        
    @AuraEnabled
    public static void saveAccountWithOpportunity(ID oppId, List<Account> acctLst){        
        FastTrackProposalCreation fastTrackProposalCreation = new FastTrackProposalCreation( oppId );
        fastTrackProposalCreation.saveAccountWithOpportunity(oppId,acctLst);
    }

    @AuraEnabled
    public static void removeAccountFromOpportunity(ID oppId, List<Account> acctLst){        
        FastTrackProposalCreation fastTrackProposalCreation = new FastTrackProposalCreation( oppId );
        fastTrackProposalCreation.removeAccountFromOpportunity(oppId, acctLst);
    }

    @AuraEnabled
    public static void removeAccountRelationshipFromOpportunity(ID oppId, List<Account_Opportunity_Relationship__c> acctLst){   
        AccountOpportunityRepository accountsInOpp = new AccountOpportunityRepository(oppId);
        accountsInOpp.deleteRelationshipAccountWithOpp(acctLst);
    }

    @AuraEnabled
    public static Object getSetUpDealHierarchy(ID oppId){   
        AccountRepository accounts = new AccountRepository();    
        OpportunityRepository opportunities = new OpportunityRepository();
        AccountOpportunityRepository accountsInOpp = new AccountOpportunityRepository(oppId);
        Opportunity opp = opportunities.byId(oppId);        
        Account accOpp = accounts.byId(opp.AccountId);

        Set<Id> accountsRelatedIds = accountsInOpp.findRelatedAccounts();

        if(!accountsRelatedIds.contains(accOpp.Id)){
            saveAccountWithOpportunity(opp.Id, new List<Account>{accOpp});
            accountsRelatedIds = accountsInOpp.findRelatedAccounts();
        }
                
        Set<Id> childAccounts = accounts.findChildAccounts(opp.AccountId);
        System.debug(childAccounts);
        Set<Account> allAccountsInHierarchy = new Set<Account>(accounts.getAccounts(childAccounts));
        //allAccountsInHierarchy.add(accOpp);
        Set<Account> accountNotAvailable = new Set<Account>();
        Integer totalEmployees = accOpp.NumberOfEmployees;
        System.debug(totalEmployees);
        Map<String, Object> setUpDealHierarchy = new Map<String, Object>(); 
        System.debug(allAccountsInHierarchy);       
        if(!allAccountsInHierarchy.isEmpty()){
            for(Account acc : allAccountsInHierarchy){
                if(accountsRelatedIds.contains(acc.Id)){
                    System.debug(acc.NumberOfEmployees);
                    if(acc.NumberOfEmployees!= null){
                        totalEmployees += acc.NumberOfEmployees;
                    }   
                    accountNotAvailable.add(acc);
                }
            }
        }       
        allAccountsInHierarchy.removeAll(accountNotAvailable);
        setUpDealHierarchy.put('accAvailable', allAccountsInHierarchy);
        setUpDealHierarchy.put('accRelatedToOpp', accountNotAvailable);
        setUpDealHierarchy.put('numberOfEmployees', totalEmployees);
        return setUpDealHierarchy;
    }

    @AuraEnabled
    public static FastTrackProposalCreationTO setStage(String newTo, integer index){
        //newTo = JSON.serialize(newTo);
        FastTrackProposalCreationTO to = (FastTrackProposalCreationTO)JSON.deserialize(newTo, FastTrackProposalCreationTO.class);
        FastTrackProposalCreation fastTrackProposalCreation = new FastTrackProposalCreation( to.opportunityId );        
        QuoteRepository quotes = new QuoteRepository();
        OpportunityRepository opportunities = new OpportunityRepository();       
        try{
            Opportunity opportunity = fastTrackProposalCreation.refreshOpportunityForNewStage(to.opportunityId, index);
            switch on opportunity.FastTrackStage__c {
                when 'Offer Review'  {
                        Quote quote = quotes.lastForOpportunity(opportunity.Id);
                    	FastTrackProposalCreationController.proposalEvaluation(opportunity.Id);
                        //opportunities.sync(quote.Id, opportunity.Id);
                    }
                // when 'Setup'{
                //     try {                 
                //         to.withOpsForm(fastTrackProposalCreation.saveClientOpsForm(to, Database.setSavepoint()));
                //     } catch (Exception e){
                //         throw new AuraHandledException('Can not save Ops Setup Form for Id: '+ to.opsSetupForm.opsFormId + '. Error: ' + e.getMessage());
                //     }
                // }            
            }          
            to.fastTrackStage = opportunity.FastTrackStage__c;
            return to;
        } catch (Exception e){
            throw new AuraHandledException(e.getMessage());
        }
    }    
    
    @AuraEnabled
    public static FastTrackProposalCreationTO approveOrRejectDealDesk(Id oppId, Id recordId, Boolean isApproved , String reason ){
        AccountOpportunityRepository accOppRep = new AccountOpportunityRepository(oppId);
        FastTrackProposalCreationTO to = FastTrackProposalCreationController.find(oppId, accOppRep.totalOfEmployees, accOppRep.accountsInOpp);
        try {       
            List<Approval_Item__c> itemsToApprove = new  List<Approval_Item__c>();
            List<Approval_Item__c> items = [SELECT Id, Name, Approval_Status__c, Value__c,Comments__c, Case__c FROM Approval_Item__c WHERE case__c = :recordId ];

            for(integer i = 0; i < items.size(); i++ ){
                if(items.get(i).Approval_Status__c == 'Open'){
                    items.get(i).Approval_Status__c = isApproved ? 'Approved' : 'Rejected';
                    items.get(i).Comments__c = reason;
                }
                if(items.get(i).Approval_Status__c == 'Approved'){
                    itemsToApprove.add(items.get(i)); 
                }
            }
            if(items.size()>0)
                Database.update(items);
     
         }  catch (Exception e) {
            System.debug('ERROR ' + e.getMessage() +' -  stack = '+ e.getStackTraceString());
            if(!Test.isRunningTest())
                throw new AuraHandledException('ERROR. Please contact your support');
        }
        return to;
    }

    @AuraEnabled
    public static FastTrackProposalCreationTO proposalEvaluation(Id oppId){   
        AccountOpportunityRepository accOppRep = new AccountOpportunityRepository(oppId);
        FastTrackProposalCreationTO to = FastTrackProposalCreationController.find(oppId, accOppRep.totalOfEmployees, accOppRep.accountsInOpp);
        System.debug('TO Pronto pra evaluation ' + to);        
        FastTrackProposalCreation fastTrackProposalCreation = new FastTrackProposalCreation( to.opportunityId );
        fastTrackProposalCreation.resetConditionToApproval(to, 1);       
        fastTrackProposalCreation.resetConditionToApproval(to, 2); 

        FastTrackDealDeskHandler lDealDeskHandler = FastTrackDealDeskHandler.getInstanceByOppId( oppId , to );
        lDealDeskHandler.dealDeskEvaluation();
        System.debug('astrproposalEvalw ' + to); 
        
        FastTrackCommercialConditionsHandler.getInstanceByOppId( oppId , to ).commercialConditionsEvaluation();
        return to;
    }
  
    @AuraEnabled
    public static FastTrackProposalCreationTO submitForApproval (String jsonTO){
        System.debug('jsonTO '+ jsonTO);
        FastTrackProposalCreationTO oldTo = (FastTrackProposalCreationTO)JSON.deserialize(jsonTO, FastTrackProposalCreationTO.class);
          
        FastTrackProposalCreationTO to = new FastTrackProposalCreationBuilder()
                    .withOpportunity(oldTo.opportunityId)
                    .withLastCreatedQuoteWhenExists(oldTo.opportunityId)
                    .build();       
        to.proposal.dealDeskDescription =  oldTo.proposal.dealDeskDescription;
        to.proposal.commercialJustification = oldTo.proposal.commercialJustification;
        to.proposal.accessFee.waiversJustification = oldTo.proposal.accessFee.waiversJustification;
        to.proposal.sObj.Deal_Desk_Comments__c = oldTo.proposal.sObj.Deal_Desk_Comments__c;
        to.proposal.sObj.Waiver_Comments__c = oldTo.proposal.sObj.Waiver_Comments__c;
        
        FastTrackDealDeskHandler dealDeskHandler = FastTrackDealDeskHandler.getInstanceByOppId( oldTo.opportunityId , to );

        if(to.proposal.sObj.Deal_Desk_Approval_Needed__c && !to.proposal.sObj.Deal_Desk_Approved__c) {
            dealDeskHandler.submitCaseForApproval();
        }
        FastTrackCommercialConditionsHandler commHandler = FastTrackCommercialConditionsHandler.getInstanceByOppId( oldTo.opportunityId , to );
        commHandler.submitForApproval();
        return to;
    }

    @AuraEnabled
    public static void resetCommercialConditions(FastTrackProposalCreationTO to){        
        to = new FastTrackProposalCreationBuilder()
                    .withOpportunity(to.opportunityId)
                    .withLastCreatedQuoteWhenExists(to.opportunityId)
                    .build();
        new FastTrackProposalCreation( to.opportunityId ).resetDealDeskAndCommercialConditions(to); 
        new FastTrackProposalCreation( to.opportunityId ).refreshRevenues(to);   

        FastTrackDealDeskHandler lDealDeskHandler = FastTrackDealDeskHandler.getInstanceByOppId( to.opportunityId , to );
        lDealDeskHandler.dealDeskEvaluation();
        System.debug('astrproposalDeskw ' + to); 

        FastTrackCommercialConditionsHandler.getInstanceByOppId( to.opportunityId , to ).commercialConditionsEvaluation();
    }

    @AuraEnabled
    public static List<Account_Opportunity_Relationship__c> getAccOppRelationships(Id oppId){
        return new AccountOpportunityRepository(oppId).getAccountsInOpp();
    }

    @AuraEnabled
    public static List <Account_Opportunity_Relationship__c> getAccOppSubsidy(Id oppId) {
        return new AccountOpportunityRepository(oppId).getResellerAccountsInOpp();
    }

    @AuraEnabled
    public static List <Account_Opportunity_Relationship__c> getAccOppSubsidyWithReseller(Id oppId) {

        List<Account_Opportunity_Relationship__c> listAOR = new AccountOpportunityRepository(oppId).getAccountsInOppWhithReseller();
        Map<Id,Account_Opportunity_Relationship__c> mapAOR = new Map<Id,Account_Opportunity_Relationship__c>();
        for(Account_Opportunity_Relationship__c aor :listAOR){
            mapAOR.put(aor.Account__c,aor);
        }
        return mapAOR.values(); 
       
    }

    @AuraEnabled
    public static void saveAccOppRelationships( List<Account_Opportunity_Relationship__c> accOppRelationships, String ltAcc ){
        List<Account> ltAccount = (List<Account>)JSON.deserialize(ltAcc, List<Account>.class);
        try{
            new AccountOpportunityRepository().edit(accOppRelationships);
            new AccountRepository().edit(ltAccount);
        } catch(Exception except) {
            throw new AuraHandledException('There was a problem while updating. Error Message:'+ except.getMessage());
        }
    }

    @AuraEnabled
    public static Case getDealDeskOperationalCase(String oppId){
        System.debug('INTO THE getDealDeskOperationalCase');
        Group lOperationalQueue;
        Case lOperationalCase;
        try{
            List<Case> lLstDealDeskOperationalCases = new CaseRepository().getCaseByOppIdAndRTDevName( oppId , 'Deal_Desk_Operational' );

            lOperationalCase = lLstDealDeskOperationalCases.isEmpty() ?
                new Case() : lLstDealDeskOperationalCases[0];

            lOperationalQueue = [ SELECT Id FROM Group WHERE Name = 'Deal Desk Operational' and Type = 'Queue' ];

            lOperationalCase.Subject = 'Deal Desk Operational Approval Request';
            lOperationalCase.OwnerId = lOperationalQueue == null ? null : lOperationalQueue.Id;
            
            System.debug('### lOperationalCase ###' + lOperationalCase);
            return lOperationalCase;
        } catch(Exception except) {
            throw new AuraHandledException(except.getMessage());
        }
    }

    @AuraEnabled
    public static List<Approval_Item__c>  getApprovalItemsCase(Id caseId){
     
        List<Approval_Item__c> approvalItems = [SELECT Id, Name, Comments__c,Approval_Status__c, Value__c FROM Approval_Item__c WHERE case__c = :caseId  ];
        return approvalItems;
    }
    
    @AuraEnabled
    public static void setModal(String objQuoteItem, String objPayment, String objQuote){

        QuoteLineItem quoteItem = (QuoteLineItem)JSON.deserialize(objQuoteItem,QuoteLineItem.class);
        Payment__c payment = (Payment__c)JSON.deserialize(objPayment,Payment__c.class);
        Quote quote = (Quote)JSON.deserialize(objQuote,quote.class);
        
        FastTrackRegionalManagerApprovalHandler.fastTrackRegionalManagerApproval(quoteItem, quote);
        
        FastTrackProposalCreationTO to = new FastTrackProposalCreationTO();
        to.opportunityId = quote.OpportunityId;
		FastTrackProposalCreation fastTrackProposalCreation = new FastTrackProposalCreation(quote.OpportunityId);
        fastTrackProposalCreation.refreshRevenues(to);
        
    }
    
    @AuraEnabled
    public static List<Comission_Price__c> findComissionValue(Id productId, String currencyIsoCode){
                
        ComissionPriceRepository comissionPriceRepository = new ComissionPriceRepository();
        List<Comission_Price__c> listComissionPrice = comissionPriceRepository.byProductIdAndCurrencyIsoCode(productId, currencyIsoCode);       
		return listComissionPrice;
		        
    }

    @AuraEnabled
    public static String getPayment(String oppId) {
        system.debug('id opportunity '+ oppId);
        List<Eligibility__c> eligibilities = [	
            SELECT  Id, 	
                    Payment__r.Quote_Line_Item__r.Fee_Type__c, 	
                    Payment_Method__c 	
            FROM  eligibility__c	
            WHERE Payment__r.Quote_Line_Item__r.Quote.OpportunityId =: oppId ];	
        
        String paymentMethod = null;
        
        if (eligibilities != null) {
            for (Eligibility__c eligibility : eligibilities) {
                if ( eligibility.Payment_Method__c == 'Credit Card') {
                    paymentMethod = 'Credit Card';
                    break;
                } else if (eligibility.Payment_Method__c == 'Payroll' || eligibility.Payment_Method__c == 'Payroll + Credit Card') {
                    paymentMethod = 'Payroll';
                }
            }
        }
        system.debug('paymentMethod '+ paymentMethod);
        return paymentMethod;
    }

    @AuraEnabled
    public static void resellerCreateProposal(String oppId){

        Account acc = [ select Id,Account.NumberOfEmployees,Account.Id from Opportunity where Id = :oppId limit 1].Account; 

        FastTrackProposalCreationTO to = new FastTrackProposalCreationBuilder()
        .withOpportunity(oppId)
        .withLastCreatedQuoteWhenExists(oppId)
        .withDealHierarchy(acc.NumberOfEmployees, new List<Id>{acc.Id})
        .build();
        FastTrackProposalCreation fastTrackProposalCreation = new FastTrackProposalCreation(oppId);
        fastTrackProposalCreation.save(to);
    }

    @AuraEnabled
    public static Boolean hasProposalInOpp(String oppId){
        List<Quote> quote = new List<Quote>([SELECT Id FROM Quote WHERE OpportunityId =: oppId]);

        if(quote.isEmpty())return false;
        else return true;
    }
    
    @AuraEnabled
    public static Boolean getExecutiveSuccessClient(String oppId){
        
        OpportunityRepository opportunities = new OpportunityRepository();
        Opportunity opp = opportunities.byId(oppId);
        if(opp.Responsavel_Gympass_Relacionamento__c == null)return false;
        else return true;        
    }
}