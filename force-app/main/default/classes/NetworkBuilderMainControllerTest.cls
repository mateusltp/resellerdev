/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 06-09-2022
 * @last modified by  : alysson.mota@gympass.com
**/



@isTest
public with sharing class NetworkBuilderMainControllerTest {

    @isTest
    private static void buildNetwork_UnitTest(){ 

        PS_Constants constants = PS_Constants.getInstance();
        Id rtAccId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(constants.ACCOUNT_RT_PARTNER_FLOW_ACCOUNT).getRecordTypeId();
        fflib_ApexMocks mocks = new fflib_ApexMocks();
        fflib_SObjectUnitOfWork mockUOW = (fflib_SObjectUnitOfWork)mocks.mock(fflib_SObjectUnitOfWork.class);
        AccountSelector mockAccountSelector = (AccountSelector) mocks.mock(AccountSelector.class); 
               
        Id mockParentAccountId = fflib_IDGenerator.generate(Account.SObjectType);
        Id mockChildAccountId = fflib_IDGenerator.generate(Account.SObjectType);

        List<Account> mockParentAccountLst = new List<Account> { new Account (
                                                                id = mockParentAccountId,        
                                                                name = 'Parent',
                                                                shippingStateCode = 'RJ',
                                                                shippingCountryCode = 'BR',
                                                                shippingCountry = 'Brazil',
                                                                shippingStreet= 'Rua SOC Test',
                                                                shippingPostalCode = '111111',
                                                                shippingCity = 'City',
                                                                Types_of_ownership__c = 'Franchise',
                                                                Partner_Level__c = 'Location',
            													recordTypeId = rtAccId,
                                                                billingStateCode = 'RJ',
                                                                billingCountryCode = 'BR',
                                                                billingCountry = 'Brazil',
                                                                billingStreet = 'Rua SOC Test',
                                                                billingPostalCode = '111111',
                                                                billingCity = 'City'
                                                                )};

        List<Account> mockChildAccountLst = new List<Account> { new Account (
                                                                id = mockChildAccountId,        
                                                                parentId = mockParentAccountId,
                                                                name = 'Child',
                                                                shippingStateCode = 'São Paulo',
                                                                shippingCountryCode = 'BR',
                                                                shippingCountry = 'Brazil',
                                                                shippingStreet= 'Rua SOC Test',
                                                                shippingPostalCode = '111111',
                                                                shippingCity = 'City',
                                                                Types_of_ownership__c = 'Franchise',
                                                                Partner_Level__c = 'Location',
            													recordTypeId = rtAccId,
                                                                billingStateCode = 'RJ',
                                                                billingCountryCode = 'BR',
                                                                billingCountry = 'Brazil',
                                                                billingStreet = 'Rua SOC Test',
                                                                billingPostalCode = '111111',
                                                                billingCity = 'City'
                                                            )};
      Map<Id, Account> mockAccountMap = new Map<Id, Account>(mockChildAccountLst);        
        mocks.startStubbing();
            mocks.when(mockAccountSelector.sobjectType()).thenReturn(Account.sObjectType);
            mocks.when(mockAccountSelector.selectByIdForPartners(new Set<Id>{mockParentAccountId})).thenReturn(mockParentAccountLst);
            mocks.when(mockAccountSelector.selectByParentId(new Set<Id>{mockParentAccountId})).thenReturn(mockAccountMap);
        mocks.stopStubbing();

        Application.UnitOfWork.setMock(mockUOW);
        Application.Selector.setMock(mockAccountSelector);
        Test.startTest();
            NetworkBuilderMainController.buildNetwork(mockParentAccountId, 'Partner_Flow_Account');
        Test.stopTest();
                       
    }
    
    @isTest
    private static void buildNetwork_UnitTestFail(){ 

        PS_Constants constants = PS_Constants.getInstance();
        Id rtAccId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(constants.ACCOUNT_RT_PARTNER_FLOW_ACCOUNT).getRecordTypeId();
        fflib_ApexMocks mocks = new fflib_ApexMocks();
        fflib_SObjectUnitOfWork mockUOW = (fflib_SObjectUnitOfWork)mocks.mock(fflib_SObjectUnitOfWork.class);
        AccountSelector mockAccountSelector = (AccountSelector) mocks.mock(AccountSelector.class); 
               
        Id mockParentAccountId = fflib_IDGenerator.generate(Account.SObjectType);
        Id mockChildAccountId = fflib_IDGenerator.generate(Account.SObjectType);

        List<Account> mockParentAccountLst = new List<Account> { new Account (
                                                                id = mockParentAccountId,        
                                                                name = 'Parent',
                                                                shippingStateCode = 'RJ',
                                                                shippingCountryCode = 'BR',
                                                                shippingStreet= 'Rua SOC Test',
                                                                shippingPostalCode = '111111',
                                                                shippingCity = 'City',
                                                                Types_of_ownership__c = 'Franchise',
                                                                Partner_Level__c = 'Location',
            													recordTypeId = rtAccId)};

        List<Account> mockChildAccountLst = new List<Account> { new Account (
                                                                id = mockChildAccountId,        
                                                                parentId = mockParentAccountId,
                                                                name = 'Child',
                                                                shippingStateCode = 'São Paulo',
                                                                shippingCountryCode = 'BR',
                                                                shippingStreet= 'Rua SOC Test',
                                                                shippingPostalCode = '111111',
                                                                shippingCity = 'City',
                                                                Types_of_ownership__c = 'Franchise',
                                                                Partner_Level__c = 'Location',
            													recordTypeId = rtAccId
                                                            )};
      Map<Id, Account> mockAccountMap = new Map<Id, Account>(mockChildAccountLst);        
        mocks.startStubbing();
            mocks.when(mockAccountSelector.sobjectType()).thenReturn(Account.sObjectType);
            mocks.when(mockAccountSelector.selectByIdForPartners(new Set<Id>{mockParentAccountId})).thenReturn(mockParentAccountLst);
            mocks.when(mockAccountSelector.selectByParentId(new Set<Id>{mockParentAccountId})).thenReturn(mockAccountMap);
        mocks.stopStubbing();

        Application.UnitOfWork.setMock(mockUOW);
        Application.Selector.setMock(mockAccountSelector);
        Test.startTest();
            NetworkBuilderMainController.buildNetwork(mockParentAccountId, 'FalseRT');
        Test.stopTest();
                       
    }
    
     @isTest
    private static void buildNetwork_UnitTestFail2(){ 

        PS_Constants constants = PS_Constants.getInstance();
        Id rtAccId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(constants.ACCOUNT_RT_PARTNER_FLOW_ACCOUNT).getRecordTypeId();
        fflib_ApexMocks mocks = new fflib_ApexMocks();
        fflib_SObjectUnitOfWork mockUOW = (fflib_SObjectUnitOfWork)mocks.mock(fflib_SObjectUnitOfWork.class);
        AccountSelector mockAccountSelector = (AccountSelector) mocks.mock(AccountSelector.class); 
               
        Id mockParentAccountId = fflib_IDGenerator.generate(Account.SObjectType);
        Id mockChildAccountId = fflib_IDGenerator.generate(Account.SObjectType);

        List<Account> mockParentAccountLst = new List<Account> { new Account (
                                                                id = mockParentAccountId,        
                                                                name = 'Parent',
                                                                shippingStateCode = 'RJ',
                                                                shippingCountryCode = 'BR',
                                                                shippingStreet= 'Rua SOC Test',
                                                                shippingPostalCode = '111111',
                                                                shippingCity = 'City',
                                                                Types_of_ownership__c = 'Franchise',
                                                                Partner_Level__c = 'Location',
            													recordTypeId = rtAccId)};

        List<Account> mockChildAccountLst = new List<Account> { new Account (
                                                                id = mockChildAccountId,        
                                                                parentId = mockParentAccountId,
                                                                name = 'Child',
                                                                shippingStateCode = 'São Paulo',
                                                                shippingCountryCode = 'BR',
                                                                shippingStreet= 'Rua SOC Test',
                                                                shippingPostalCode = '111111',
                                                                shippingCity = 'City',
                                                                Types_of_ownership__c = 'Franchise',
                                                                Partner_Level__c = 'Location',
            													recordTypeId = rtAccId
                                                            )};
      Map<Id, Account> mockAccountMap = new Map<Id, Account>(mockChildAccountLst);        
        mocks.startStubbing();
            mocks.when(mockAccountSelector.sobjectType()).thenReturn(Account.sObjectType);
            mocks.when(mockAccountSelector.selectByIdForPartners(new Set<Id>{mockParentAccountId})).thenReturn(mockParentAccountLst);
            mocks.when(mockAccountSelector.selectByParentId(new Set<Id>{mockParentAccountId})).thenReturn(mockAccountMap);
        mocks.stopStubbing();

        Application.UnitOfWork.setMock(mockUOW);
        Application.Selector.setMock(mockAccountSelector);
        Test.startTest();
            try {
                NetworkBuilderMainController.buildNetwork(mockParentAccountId, 'Partner_Flow_Account');
            } catch (Exception e) {

            }

        Test.stopTest();
                       
    }
}