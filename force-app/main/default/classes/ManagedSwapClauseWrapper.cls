public class ManagedSwapClauseWrapper {
   
    public string ObjectId;
    public Id StandardId;
    public string StandardClause;
    public Id SwapId;
    public String NewClause;
    public datetime CreatedDateTime;
    public String Status;

    public string Type;
    public string ContractAgreementId;

    public ManagedSwapClauseWrapper(){
        
    }
}