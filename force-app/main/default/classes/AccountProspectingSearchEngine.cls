public without sharing class AccountProspectingSearchEngine {
    static Id indirectChannelRecordTypeId;
    static Id directChannelRecordTypeId;
    
    static {
		indirectChannelRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Indirect Channel').getRecordTypeId();
        directChannelRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Direct Channel').getRecordTypeId();
    }
    
    public List<Account> searchAccountsForAccProspecting(Account_Prospecting__c accountProspecting) {
        System.debug('searchAccountsForAccountProspecting');
        
        Map<Id, Account> searchMapWebsite = new Map<Id, Account>();
        Map<Id, Account> searchMapLegalNumber = new Map<Id, Account>();
		Map<Id, Account> searchMapAccountName = new Map<Id, Account>();
        Map<Id, Account> resultMap = new Map<Id, Account>();
        
       	searchMapWebsite = searchAccountsByWebsite(accountProspecting.Website__c);
        searchMapLegalNumber = searchAccountsByLegalNumber(accountProspecting.Registered_number__c);
        searchMapAccountName = searchAccountsBySOSL(accountProspecting.Name);
        
        resultMap.putAll(searchMapWebsite);
        resultMap.putAll(searchMapLegalNumber);
        resultMap.putAll(searchMapAccountName);

        List<Account> accountsToReturn = filterAccountsByUserLocationAndCountry(resultMap.values());

        return accountsToReturn;
    }
    
    private Map<Id, Account> searchAccountsBySOSL(String term) {
        System.debug('searchAccountsBySOSL');
        List<Account> returnList = new List<Account>();
        Map<Id, Account> idToAccountMap = new Map<Id, Account>();
        List<String> termSplited = term.split(' ');
        List<String> wordList = new List<String>();
        List<String> concatenedWords = new List<String>();
        
        for (String word : termSplited) {
            word = word.replaceAll(' ', '');
            word = escapeCharactersFromString(word);
        }

        for (String word : termSplited) {
            if (word.length() > 2) {
                wordList.add(word);
            }
        }
        
        for (Integer i=0; i<wordList.size()-1; i++) {
            for (Integer j=i+1; i<wordList.size(); i++) {
                concatenedWords.add(wordList.get(i) + ' ' + wordList.get(j));  
            }
        }

        List<List<Account>> searchList = doSoslForAccounts(term, 20);

        Integer wordListIndex = 0;
        while ((searchList.get(0).size() == 0) && (wordListIndex < concatenedWords.size()-1)) {
            searchList = doSoslForAccounts(concatenedWords.get(wordListIndex), 3);
            wordListIndex++;
        }

        wordListIndex = 0;
        System.debug(wordList);
        while ((searchList.get(0).size() == 0) && (wordListIndex < wordList.size()-1)) {
            System.debug('Term: ' + wordList.get(wordListIndex));
            System.debug('wordListIndex: ' + wordListIndex);
            searchList = doSoslForAccounts(wordList.get(wordListIndex), 3);
            wordListIndex++;
        }
        
        for (Account acc : searchList.get(0)) {
            returnList.add(acc);
        }
    
        for (Account acc : returnList) {
            System.debug(acc);
      		idToAccountMap.put(acc.Id, acc);
        }
        
        return idToAccountMap;
    }

    private List<Account> filterAccountsByUserLocationAndCountry(List<Account> accounts) {
        System.debug('filterAccountsByUserLocationAndCountry');
        List<Account> accountsToReturn = new List<Account>();
        String userLocale = UserInfo.getLocale();

        for (Account acc : accounts) {
            if (userLocale == 'en_US' && acc.BillingCountry == 'United States') {
                System.debug('en_US');
                accountsToReturn.add(acc);
            }
            
            if (userLocale == 'pt_BR' && acc.BillingCountry == 'Brazil') {
               System.debug('pt_BR');
               accountsToReturn.add(acc);
            }
        }

        return accountsToReturn;
    }
    
    private Map<Id, Account> searchAccountsByWebsite(String website) {
        System.debug('searchAccountsByWebsite');
        Set<String> websiteSet = new Set<String>();
        String websiteSubstring = null;
        Map<Id, Account> idToAccountMap = new Map<Id, Account>();
        Set<String> prefixesNotAllowed = new Set<String>{'http://', 'https://', 'www.'};

        if (website != null) {
            // Remove any prefix from website
            for (String prefix : prefixesNotAllowed) {
                website = website.replaceAll(prefix, '');
            }

            // Get subtring before first dot from website
            website = website.substringBefore('.');

            websiteSet.add('%' + website + '%');
            
            // SOQL for exact match
            idToAccountMap = new Map<Id, Account>([
                SELECT Id, Name, Razao_Social__c, Id_Company__c, BillingCountry
                FROM Account
                WHERE   
                        (Website LIKE :websiteSet)
                        AND (RecordTypeId = :indirectChannelRecordTypeId OR RecordTypeId = :directChannelRecordTypeId) 
            ]);

            // SOSL for similar correspondence
            website = escapeCharactersFromString(website);
            idToAccountMap.putAll(searchAccountsBySOSL(website));
        }
        
        return idToAccountMap;
    }

    private List<List<Account>> doSoslForAccounts(String term, Integer limitRecords) {
        System.debug('doSoslForAccounts');
        List<List<Account>> searchList = [
            FIND :term IN NAME FIELDS RETURNING Account(Name, Id, BillingCountry, Razao_Social__c, Id_Company__c WHERE RecordType.DeveloperName = 'Empresas' OR RecordType.DeveloperName = 'Reseller')
            LIMIT :limitRecords
        ];

        return searchList;
    }

    private Map<Id, Account> searchAccountsByLegalNumber(String legalNumber) {
        System.debug('searchAccountsByLegalNumber');
        System.debug(legalNumber);
        Set<String> legalNumberSet  = new Set<String>();
        
        if (legalNumber != null) {
            legalNumberSet.add('%'+legalNumber+'%');
            legalNumberSet.add(removeDotsAndDashesFromString(legalNumber));
        }
        
        System.debug(legalNumberSet);
        
        Map<Id, Account> idToAccountMap = new Map<Id, Account>([
            SELECT Id, Name, Razao_Social__c, Id_Company__c, BillingCountry
            FROM Account
            WHERE   
                    Id_Company__c LIKE :legalNumberSet
                    AND (RecordTypeId = :indirectChannelRecordTypeId OR RecordTypeId = :directChannelRecordTypeId) 
        ]);
        
        return idToAccountMap;
    }

    private String escapeCharactersFromString(String s) {
        System.debug('escapeCharactersFromString');
        Set<String> charsToBeEscaped = new Set<String>{'?','&','|','!','{','}','[',']','(',')','^','~','*',':','"','+','-'};
        Integer stringLength = s.length();
        String escapedString = s;

        for (String charToBeEscaped : charsToBeEscaped) {
            if (escapedString.contains(charToBeEscaped)) {
                if (charToBeEscaped != '.') {
                    escapedString = escapedString.replace(charToBeEscaped, '\\' + charToBeEscaped);             
                } else {
                    escapedString = escapedString.replace(charToBeEscaped, '\\' + '\\' + charToBeEscaped);             

                }
            }    
        }

        return escapedString;
    }

    private String removeDotsAndDashesFromString(String s) {
        System.debug('removeDotsAndDashesFromString');
        return s.replaceAll('[\\./-]', '');
    }
}