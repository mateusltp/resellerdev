/**
 * @description       : 
 * @author            : ext.gft.pedro.oliveira@gympass.com
 * @group             : 
 * @last modified on  : 06-08-2021
 * @last modified by  : gft.samuel.silva@ext.gympass.com
 * Modifications Log 
 * Ver   Date         Author                             Modification
 * 1.0   08-06-2021   ext.gft.pedro.oliveira@gympass.com   Initial Version
**/ 
@isTest
public without sharing class WaitingCancelattionNotificationBatchTest {

    @TestSetup
    static void createData(){
     Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gym_Partner').getRecordTypeId();
        Account lAcc = new Account();
        lAcc.name='AcademiaBrasilCompanyPai';
        lAcc.RecordTypeId = rtId;
        lAcc.GP_Status__c = 'Active';
        lAcc.billingState = 'Minas Gerais';
        lAcc.CAP_Value__c = 120;
        lAcc.BillingCity = 'CityAcademiaBrasil';
        lAcc.billingStreet = 'Rua academiabrasilpai';
        lAcc.billingCountry = 'Brazil';
        lAcc.ShippingCountry ='Brazil';
        lAcc.ShippingStreet ='Dona Maria';
        lAcc.ShippingCity ='Uberlandia';
        lAcc.Gym_Type__c = 'Studios';
        lAcc.Gym_Classes__c = 'Cardio';
        lAcc.Legal_Title__c = '11111';
        lAcc.Id_Company__c = '11111';
        lAcc.Types_of_ownership__c = Label.franchisePicklist;
        lAcc.Subscription_Type__c = 'Value per class';
        lAcc.Subscription_Period__c = 'Monthy value';
        lAcc.Subscription_Type_Estimated_Price__c    = 100;
        lAcc.Has_market_cannibalization__c = 'No';
        lAcc.Exclusivity__c = 'Yes';
        lAcc.Exclusivity_End_Date__c = Date.today().addYears(1);
        lAcc.Exclusivity_Partnership__c = 'Full Exclusivity';
        lAcc.Exclusivity_Restrictions__c= 'No';
        lAcc.Website = 'testing@tesapex.com';
        lAcc.Gym_Email__c = 'gymemail@apex.com';
        lAcc.Phone = '3222123123';
        lAcc.Can_use_logo__c = 'Yes';
        lAcc.Legal_Registration__c = 12123;
        lAcc.Legal_Title__c = 'Title LTDA';
        lAcc.Gyms_Identification_Document__c = 'CNPJ'; 
        INSERT lAcc;
    
		Date dateTeste = date.today() - 30;
        Id lOppRt =  Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Partner_Small_and_Medium_Renegotiation').getRecordTypeId();
        Opportunity lAccOpp = new Opportunity();
        lAccOpp.recordTypeId = lOppRt;
        lAccOpp.AccountId = lAcc.id;
        lAccOpp.Name = lAcc.Id; 
        lAccOpp.CMS_Used__c = 'Yes';     
        lAccOpp.Gym_agreed_to_an_API_integration__c  = 'Yes';
        lAccOpp.Club_Management_System__c = 'Companhia Athletica';
        lAccOpp.Integration_Fee_Deduction__c = 'No';
        lAccOpp.CloseDate = Date.today();
        lAccOpp.Success_Look_Like__c = 'Yes';
        lAccOpp.Success_Look_Like_Description__c = 'Money money';
        lAccOpp.StageName = 'Waiting Cancellation';
        lAccOpp.Type = 'Expansion';  
        lAccOpp.Country_Manager_Approval__c = true;
        lAccOpp.Payment_approved__c = true;   
        lAccOpp.CurrencyIsoCode = 'BRL';
        lAccOpp.Gympass_Plus__c = 'Yes';
        lAccOpp.Standard_Payment__c = 'Yes';
        lAccOpp.Request_for_self_checkin__c = 'Yes';  
        lAccOpp.Training_Comments__c = 'teste';
        lAccOpp.Sub_Type__c = 'Retention';
        lAccOpp.Cancellation_Date__c = dateTeste;
        lAccOpp.Should_OPS_team_send_the_training__c = 'Standard Training';
        INSERT lAccOpp;
	}
    	

    @isTest
    static void test() {
        Test.startTest();
            String testTime = '0 0 0 * * ?';
            WaitingCancelattionNotificationBatch.start(testTime);

            CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime 
                                FROM CronTrigger 
                                WHERE CronJobDetail.Name = 'Waiting Cancelation Notification Test'];            
            System.assertEquals(testTime, ct.CronExpression);
            System.assertEquals(0, ct.TimesTriggered);
            String timeNow = String.valueOf(Date.today()+1) + ' 00:00:00';
            System.assertEquals(timeNow, String.valueOf(ct.NextFireTime));
        Test.stopTest();
    }
}