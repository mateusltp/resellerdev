/**
 * @description       : 
 * @author            : roei@gft.com
 * @group             : 
 * @last modified on  : 02-23-2021
 * @last modified by  : roei@gft.com
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   02-23-2021   roei@gft.com   Initial Version
**/
@isTest(seeAllData=false)
public class ActivityUpdateLeadStatusTest {
	@TestSetup
    private static void createData(){ 
    	Id lLeadDirectChannelRtId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Client').getRecordTypeId();
        
        Lead lNewLead = new Lead(
            LastName='Direct Channel Lead',
            RecordTypeId = lLeadDirectChannelRtId,
            Status = 'SQL',
            Country = 'Brazil',
            State = 'São Paulo',
            Street = 'Av Paulista 123',
            City = 'São Paulo',                 
            Phone = '(011)1234-5678',
            Email = 'emailTest@email.com',
            Company = 'Company Test'
        );
        Database.insert( lNewLead );
    }
    
    @isTest
    private static void shouldEventSetInProgressLeadStatus(){
        Id lLeadId = [ SELECT Id FROM Lead WHERE Email = 'emailTest@email.com' ].Id;
        
        Event lEvent = new Event(
            Subject = 'Test Event Created',
            WhoId = lLeadId,
            DurationInMinutes = 60,
            ActivityDateTime = System.now() + 1
        );
        
        Test.startTest();
            Database.insert( lEvent );
        Test.stopTest();
        
        String lLeadStatus = [ SELECT Status FROM Lead WHERE Id =: lLeadId ].Status;
        
        System.assertEquals( 'In Progress' , lLeadStatus );
    }
    
    @isTest
    private static void shouldTaskSetInProgressLeadStatus(){
        Id lLeadId = [ SELECT Id FROM Lead WHERE Email = 'emailTest@email.com' ].Id;
        
        Task lTask = new Task(
            Subject = 'Test Task Created',
            WhoId = lLeadId
        );
        
        Database.insert( lTask );
        
        String lLeadStatus = [ SELECT Status FROM Lead WHERE Id =: lLeadId ].Status;
        
        System.assertNotEquals( 'In Progress' , lLeadStatus );
        
        Test.startTest();
            lTask.Status = 'Concluído';
        
        	Database.update( lTask );
        Test.stopTest();
        
        lLeadStatus = [ SELECT Status FROM Lead WHERE Id =: lLeadId ].Status;
        
        System.assertEquals( 'In Progress' , lLeadStatus );
    }
}