public without sharing class SelfTerminationRequest {
  private String client_id;
  private String client_order_id;
  private ActionType action;
  private Datetime due_date;
  private CancellationReasons reason;
  private String reason_details;

  public List<String> getMissingRequiredFields() {
    List<String> missingRequiredFields = new List<String>();

    if (String.isBlank(this.client_id)) {
      missingRequiredFields.add('client_id');
    }

    if (String.isBlank(this.client_order_id)) {
      missingRequiredFields.add('client_order_id');
    }

    if (this.action == null) {
      missingRequiredFields.add('action');
    }

    if (this.action.name() == 'CANCEL') {
      if (this.due_date == null) {
        missingRequiredFields.add('due_date');
      }
      if (this.reason == null) {
        missingRequiredFields.add('reason');
      }
    }
    return missingRequiredFields;
  }

  public String getClientId() {
    return this.client_id;
  }

  public String getClientOrderId() {
    return this.client_order_id;
  }

  public String getAction() {
    switch on action {
      when CANCEL {
        return 'Cancel';
      }
      when UNDO {
        return 'Undo';
      }
      when else {
        return null;
      }
    }
  }

  public Datetime getDueDate() {
    return this.due_date;
  }

  public String getReasonDetails() {
    return this.reason_details;
  }
  public String getReason() {
    switch on this.reason {
      when LACK_OF_BUDGET {
        return 'Lack of Budget';
      }
      when POOR_PARTNERS_NETWORK {
        return 'Poor Partners Network';
      }
      when LACK_OF_GYMPASS_SUPPORT {
        return 'Lack of Gympass support';
      }
      when FEW_PLAN_OPTIONS {
        return 'Few Plan Options';
      }
      when HIGH_PLAN_PRICE {
        return 'High Plan Price';
      }
      when SIMILAR_BENEFIT {
        return 'Similar Benefit';
      }
      when AGGREGATE_GYMPASS_VALUE {
        return 'Aggregate Gympass value';
      }
      when OTHER {
        return 'Other';
      }
      when else {
        return null;
      }
    }
  }

  @testVisible
  private enum ActionType {
    CANCEL,
    UNDO
  }

  @testVisible
  private enum CancellationReasons {
    LACK_OF_BUDGET,
    POOR_PARTNERS_NETWORK,
    LACK_OF_GYMPASS_SUPPORT,
    FEW_PLAN_OPTIONS,
    HIGH_PLAN_PRICE,
    SIMILAR_BENEFIT,
    AGGREGATE_GYMPASS_VALUE,
    OTHER
  }
}