@isTest
public class CreateSalesOrderCtrlTest {

    @isTest
    public static void functionalTest() {
		Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
        Id oppRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SMB').getRecordTypeId();
        Id quoteRecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Direct_Channel').getRecordTypeId();
        
    	Account acc = new Account();
        acc.Name = 'Test Acc';
        acc.RecordTypeId = accRecordTypeId;
        acc.BillingCountry = 'Brazil';
        acc.BillingState = 'São Paulo';
        acc.BillingCity = 'São Paulo';
        acc.BillingStreet = 'Casa do Ator';
        acc.NumberOfEmployees = 100;
        acc.Business_Unit__c = 'SMB';
        acc.Id_Company__c = '57.454.841/0001-96';
        acc.Legal_Document_Type__c = 'CNPJ';
        acc.Razao_Social__c = 'Test Acc';
        acc.UUID__c = '8b5b7489-a3ea-4647-8208-cf83efc4f09f';
        insert acc;
        
        Contact c1 = new Contact();
        c1.LastName = 'Admin Test';
        c1.Email = 'admintest@test.com';
        c1.MailingCountry = 'Brazil';
        c1.MailingState = 'São Paulo';
        c1.MailingCity = 'São Paulo';
        c1.MailingStreet = 'Casa do Ator';
        c1.Phone = '(11)98765-4321';
        c1.AccountId = acc.Id;
        insert c1;
        
        Contact c2 = new Contact();
        c2.LastName = 'Finance Test';
        c2.Email = 'financetest@test.com';
        c2.MailingCountry = 'United States';
        c2.MailingState = 'New York';
        c2.MailingCity = 'New York';
        c2.MailingStreet = '5th street';
        c2.Phone = '(111)7654321';
        c2.AccountId = acc.Id;
        insert c2;
        
        Test.startTest();
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Test Opp';
        opp.StageName = 'Qualificação';
       	opp.CloseDate = System.now().date();
        opp.RecordTypeId = oppRecordTypeId;
        opp.AccountId = acc.Id;
        opp.Gympass_Entity__c = acc.Id;
        opp.Data_do_Lancamento__c = System.now().addDays(30).date();
       	insert opp;
        
        opp.StageName = 'Oportunidade Validada Após 1ª Reunião';
        update opp;
        
        Quote prop = new Quote();
        prop.Name = 'Test Proposal';
        prop.RecordTypeId = quoteRecordTypeId;
        prop.License_Fee_Waiver__c = 'No';
        prop.OpportunityId = opp.Id;
        prop.Administrator__c = c1.Id;
        prop.Finance__c = c2.Id;
        prop.License_Fee_cut_off_date__c = 1;
		prop.CurrencyIsoCode = 'BRL';
		prop.Payment_Terms_for_License_Fee__c = 30;
        prop.Unique_Identifier__c = 'Corporate E-mail';
        insert prop;
        
        opp.SyncedQuoteId = prop.Id;
        update opp;
        
        CreateSalesOrderCtrl.createSalesOrder(opp.Id);
        Test.stopTest();
    }
    
    @isTest
    public static void functionalTest2() {
		Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
        Id oppRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SMB').getRecordTypeId();
        Id quoteRecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Direct_Channel').getRecordTypeId();
        
    	Account acc = new Account();
        acc.Name = 'Test Acc';
        acc.RecordTypeId = accRecordTypeId;
        acc.BillingCountry = 'Brazil';
        acc.BillingState = 'São Paulo';
        acc.BillingCity = 'São Paulo';
        acc.BillingStreet = 'Casa do Ator';
        acc.BillingPostalCode = '99999-999';
        acc.NumberOfEmployees = 100;
        acc.Business_Unit__c = 'SMB';
        acc.Id_Company__c = '57.454.841/0001-96';
        acc.Legal_Document_Type__c = 'CNPJ';
        acc.Razao_Social__c = 'Test Acc';
        acc.UUID__c = '8b5b7489-a3ea-4647-8208-cf83efc4f09f';
        insert acc;
        
        Contact c1 = new Contact();
        c1.LastName = 'Admin Test';
        c1.Email = 'admintest@test.com';
        c1.MailingCountry = 'Brazil';
        c1.MailingState = 'São Paulo';
        c1.MailingCity = 'São Paulo';
        c1.MailingStreet = 'Casa do Ator';
        c1.Phone = '+5511987654321';
        c1.AccountId = acc.Id;
        insert c1;
        
        Contact c2 = new Contact();
        c2.LastName = 'Finance Test';
        c2.Email = 'financetest@test.com';
        c2.MailingCountry = 'United States';
        c2.MailingState = 'New York';
        c2.MailingCity = 'New York';
        c2.MailingStreet = '5th street';
        c2.Phone = '(111)7654321';
        c2.AccountId = acc.Id;
        insert c2;
        
        Test.startTest();
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Test Opp';
        opp.StageName = 'Qualificação';
       	opp.CloseDate = System.now().date();
        opp.RecordTypeId = oppRecordTypeId;
        opp.AccountId = acc.Id;
        opp.Gympass_Entity__c = acc.Id;
        opp.Data_do_Lancamento__c = System.now().addDays(30).date();
       	insert opp;
        
        opp.StageName = 'Oportunidade Validada Após 1ª Reunião';
        update opp;
        
        Copay_Plan__c membership = new Copay_Plan__c();
        membership.Market_Value_Basic_I__c = 10;
        membership.Market_Value_Basic_II__c = 10;
        membership.Market_Value_Black__c = 10;
        membership.Market_Value_Gold__c = 10;
        membership.Market_Value_Gold_Plus__c = 10;
        membership.Market_Value_Platinum__c = 10;
        membership.Market_Value_Silver__c = 10;
        
        membership.Monthly_Fee_Basic_I__c = 10;
        membership.Monthly_Fee_Basic_II__c = 10;
        membership.Monthly_Fee_Black__c = 10;
        membership.Monthly_Fee_Gold__c = 10;
        membership.Monthly_Fee_Gold_Plus__c = 10;
        membership.Monthly_Fee_Platinum__c = 10;
        membership.Monthly_Fee_Silver__c = 10;
        insert membership;

		Product2 plan = new Product2();
        plan.Name = 'Teste Product';
        plan.IsActive = true;
        insert plan;
        
		insert new PricebookEntry(pricebook2id = Test.getStandardPricebookId(), product2id = plan.id, unitprice=1.0, isActive=true);
        
        Pricebook2 pb = new pricebook2(name='test', IsActive=true);
		insert pb;

		PricebookEntry entry = new PricebookEntry(pricebook2id=pb.id, product2id=plan.id, unitprice=1.0, isActive=true);
		insert entry;
        
        Quote prop = new Quote();
        prop.Name = 'Test Proposal';
        prop.RecordTypeId = quoteRecordTypeId;
        prop.License_Fee_Waiver__c = 'No';
        prop.OpportunityId = opp.Id;
        prop.Administrator__c = c1.Id;
        //prop.Finance__c = c2.Id;
        prop.License_Fee_cut_off_date__c = 1;
		prop.CurrencyIsoCode = 'BRL';
        prop.Pricebook2Id = pb.Id;
		prop.Payment_Terms_for_License_Fee__c = 30;
        prop.Unique_Identifier__c = 'Corporate E-mail';
        prop.Gym__c = membership.Id;
        insert prop;
        
        QuoteLineItem lineItem = new QuoteLineItem();
        lineItem.QuoteId = prop.Id;
        lineItem.Quantity = 200;
        lineItem.UnitPrice = 10.99;
        lineItem.Product2Id = plan.Id;
        lineItem.PricebookEntryId = entry.Id;
        insert lineItem;
        
        opp.SyncedQuoteId = prop.Id;
        update opp;
        
		callSalesOrderCtrl(opp.Id);
        
        Test.stopTest();
    }
    
    @future
    public static void callSalesOrderCtrl(String oppId) {
        List<Profile> profiles = [SELECT Id FROM Profile WHERE Name = 'Global'];
        List<UserRole> roles = [SELECT Id FROM UserRole WHERE Name = 'Global'];
        
        User globalUser = new User();
        globalUser.Username = 'globalUserTest@test.com';
        globalUser.LastName = 'Global';
        globalUser.Email = 'globalUser@test.com'; 
        globalUser.Alias = 'globa'; 
        globalUser.TimeZoneSidKey = 'America/Sao_Paulo';
        globalUser.LocaleSidKey = 'pt_BR';
        globalUser.EmailEncodingKey = 'ISO-8859-1';
        globalUser.ProfileId = profiles.get(0).Id;
        globalUser.UserRoleId = roles.get(0).Id;
        globalUser.LanguageLocaleKey = 'en_US';
        insert globalUser;
        
        Opportunity opp = [
            SELECT Id, StageName
            FROM Opportunity
            WHERE Id =: oppId
        ];
        
        List<Quote> proposals = [
            SELECT Id, Status
            FROM Quote
            WHERE OpportunityId =: opp.Id
        ];
        
        Quote proposal = proposals.get(0);
        
        System.runAs(globalUser) {
            /*
        	opp.StageName = 'Proposta Enviada';
            update opp;
            
            opp.StageName = 'Proposta Aprovada';
            update opp;
            
            opp.StageName = 'Contrato Assinado';
            update opp;
            */
            proposal.Status = 'Ganho';
            update proposal;
            
            opp.StageName = 'Lançado/Ganho';
            opp.Responsavel_Gympass_Relacionamento__c = globalUser.Id;
            update opp;
            
            CreateSalesOrderCtrl.createSalesOrder(opp.Id);
        }
    }
    
    @isTest
    public static void functionalTest3() {
		Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
        Id oppRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SMB').getRecordTypeId();
        Id quoteRecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Direct_Channel').getRecordTypeId();
        
    	Account acc = new Account();
        acc.Name = 'Test Acc';
        acc.RecordTypeId = accRecordTypeId;
        acc.BillingCountry = 'Brazil';
        acc.BillingState = 'São Paulo';
        acc.BillingCity = 'São Paulo';
        acc.BillingStreet = 'Casa do Ator';
        acc.BillingPostalCode = '99999-999';
        acc.NumberOfEmployees = 100;
        acc.Business_Unit__c = 'SMB';
        acc.Id_Company__c = '57.454.841/0001-96';
        acc.Legal_Document_Type__c = 'CNPJ';
        acc.Razao_Social__c = 'Test Acc';
        acc.UUID__c = '8b5b7489-a3ea-4647-8208-cf83efc4f09f';
        insert acc;
        
        Contact c1 = new Contact();
        c1.LastName = 'Admin Test';
        c1.Email = 'admintest@test.com';
        c1.MailingCountry = 'Brazil';
        c1.MailingState = 'São Paulo';
        c1.MailingCity = 'São Paulo';
        c1.MailingStreet = 'Casa do Ator';
        c1.Phone = '+5511987654321';
        c1.AccountId = acc.Id;
        insert c1;
        
        Contact c2 = new Contact();
        c2.LastName = 'Finance Test';
        c2.Email = 'financetest@test.com';
        c2.MailingCountry = 'United States';
        c2.MailingState = 'New York';
        c2.MailingCity = 'New York';
        c2.MailingStreet = '5th street';
        c2.Phone = '(111)7654321';
        c2.AccountId = acc.Id;
        insert c2;
        
        Test.startTest();
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Test Opp';
        opp.StageName = 'Qualificação';
       	opp.CloseDate = System.now().date();
        opp.RecordTypeId = oppRecordTypeId;
        opp.AccountId = acc.Id;
        opp.Gympass_Entity__c = acc.Id;
        opp.Data_do_Lancamento__c = System.now().addDays(30).date();
       	insert opp;
        
        opp.StageName = 'Oportunidade Validada Após 1ª Reunião';
        update opp;
        
        Copay_Plan__c membership = new Copay_Plan__c();
        membership.Market_Value_Basic_I__c = 10;
        membership.Market_Value_Basic_II__c = 10;
        membership.Market_Value_Black__c = 10;
        membership.Market_Value_Gold__c = 10;
        membership.Market_Value_Gold_Plus__c = 10;
        membership.Market_Value_Platinum__c = 10;
        membership.Market_Value_Silver__c = 10;
        
        membership.Monthly_Fee_Basic_I__c = 10;
        membership.Monthly_Fee_Basic_II__c = 10;
        membership.Monthly_Fee_Black__c = 10;
        membership.Monthly_Fee_Gold__c = 10;
        membership.Monthly_Fee_Gold_Plus__c = 10;
        membership.Monthly_Fee_Platinum__c = 10;
        membership.Monthly_Fee_Silver__c = 10;
        insert membership;

		Product2 plan = new Product2();
        plan.Name = 'Teste Product';
        plan.IsActive = true;
        insert plan;
        
		insert new PricebookEntry(pricebook2id = Test.getStandardPricebookId(), product2id = plan.id, unitprice=1.0, isActive=true);
        
        Pricebook2 pb = new pricebook2(name='test', IsActive=true);
		insert pb;

		PricebookEntry entry = new PricebookEntry(pricebook2id=pb.id, product2id=plan.id, unitprice=1.0, isActive=true);
		insert entry;
        
        Quote prop = new Quote();
        prop.Name = 'Test Proposal';
        prop.RecordTypeId = quoteRecordTypeId;
        prop.License_Fee_Waiver__c = 'No';
        prop.OpportunityId = opp.Id;
        prop.Administrator__c = c1.Id;
        //prop.Finance__c = c2.Id;
        prop.License_Fee_cut_off_date__c = 1;
		prop.CurrencyIsoCode = 'BRL';
        prop.Pricebook2Id = pb.Id;
		prop.Payment_Terms_for_License_Fee__c = 30;
        prop.Unique_Identifier__c = 'Corporate E-mail';
        prop.Gym__c = membership.Id;
        insert prop;
        
        QuoteLineItem lineItem = new QuoteLineItem();
        lineItem.QuoteId = prop.Id;
        lineItem.Quantity = 200;
        lineItem.UnitPrice = 10.99;
        lineItem.Product2Id = plan.Id;
        lineItem.PricebookEntryId = entry.Id;
        insert lineItem;
        
        opp.SyncedQuoteId = prop.Id;
        update opp;
        
		CreateSalesOrderCtrl.createSalesOrder(opp.Id);
        
        Test.stopTest();
    }
}