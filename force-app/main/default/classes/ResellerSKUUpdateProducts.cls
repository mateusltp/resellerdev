public without sharing class ResellerSKUUpdateProducts {

    public static void updateQuantityAndPrices(Map<Id, Opportunity> oldMap, List<Opportunity> newList) {

        Id indirectChannelNewBusinessRecordTypeId = 
            Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Indirect_Channel_New_Business').getRecordTypeId();
        //Map<Id, Opportunity> mapIdOpp = new Map<Id, Opportunity>();
        //Set<String> setCurrencies = new Set<String>();
        //Set<String> setCountries = new Set<String>();
        //Decimal maxQuantity = 0;

        Map<Id, Decimal> quantityByOpportunityId = new Map<Id, Decimal>(); 
        Set<Id> quoteIdToProcessList = new Set<Id>(); 

        for(Opportunity opp: newList) {
            if(opp.RecordTypeId == indirectChannelNewBusinessRecordTypeId  
                && oldMap.get(opp.Id).Quantity_Offer_Number_of_Employees__c != opp.Quantity_Offer_Number_of_Employees__c 
                && opp.SyncedQuoteId != null) {
                    //mapIdOpp.put(opp.Id, opp);
                    //setCurrencies.add(opp.CurrencyIsoCode);
                    //setCountries.add(opp.Account_Billing_Country__c);
                    //maxQuantity = opp.Quantity_Offer_Number_of_Employees__c > maxQuantity ? opp.Quantity_Offer_Number_of_Employees__c : maxQuantity;
                    
                    quantityByOpportunityId.put(opp.Id, opp.Quantity_Offer_Number_of_Employees__c);
                    quoteIdToProcessList.add(opp.SyncedQuoteId);
                }
        }

        if(!quoteIdToProcessList.isEmpty()) {
            List<Quote> proposalList = [SELECT Id, OpportunityId, (SELECT Id, Quantity FROM QuoteLineItems) 
                                        FROM Quote 
                                        WHERE Id IN :quoteIdToProcessList FOR UPDATE];
            
            List<QuoteLineItem> productsToUpdate = new List<QuoteLineItem>();

            for(Quote proposal: proposalList) {
                if(proposal.OpportunityId != null && quantityByOpportunityId.containsKey(proposal.OpportunityId)) {
                    Decimal quantity = quantityByOpportunityId.get(proposal.OpportunityId);
                    if(quantity != null) {
                        for(QuoteLineItem product: proposal.QuoteLineItems) {
                            product.Quantity = quantity;
                            productsToUpdate.add(product);
                        }
                    }
                }
            }

            update productsToUpdate;
            //publishOpportunitiesQuantityUpdatedEvents(quantityByOpportunityId.keySet());
        }
    }

    // private void publishOpportunitiesQuantityUpdatedEvents(Set<Id> opportunitiesIds) {
    //     List<Opportunity_Quantity_Updated__e> opportunitiesQuantityUpdatedEvents = new List<Opportunity_Quantity_Updated__e>();
    //     for(Id opportunityId: opportunitiesIds) {
    //         opportunitiesQuantityUpdatedEvents.add(new Opportunity_Quantity_Updated__e(Opportunity_Id__c = opportunityId));
    //     }

    //     List<Database.SaveResult> results = EventBus.publish(opportunitiesQuantityUpdatedEvents);

    //     for (Database.SaveResult sr : results) {
    //         if (!sr.isSuccess()) {
    //             for(Database.Error err : sr.getErrors()) {
    //                 System.debug('Error returned: ' + err.getStatusCode() + ' - ' + err.getMessage());
    //             }
    //         }
    //     }
    // }
}