public class ContactTokenGenerator {
    
    @InvocableMethod
    public static List<Result> getContact(List<ID> ids) {
        
        Integer randomNumber = Integer.valueof((Math.random() * 1000000));
        Integer len = 6;
        Blob blobKey = crypto.generateAesKey(128);
        String key = EncodingUtil.convertToHex(blobKey);
        String pwd = key.substring(0,len);
		
        Result result = new Result();   
		result.password = pwd;
        result.token = string.valueOf(randomNumber);
        result.data =  date.today();   
        
        List<Result> listResult = new  List<Result>();
        listResult.add(result);
        
        return listResult;
    } 
        
    public class Result{
        @InvocableVariable
        public String token;
        @InvocableVariable
        public Date data;
        @InvocableVariable
        public String password;        
        
    }
    
}