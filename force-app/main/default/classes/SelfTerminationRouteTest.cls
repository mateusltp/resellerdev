@IsTest
public class SelfTerminationRouteTest {
    @TestSetup
    public static void makeData() {
      
        Account acc = DataFactory.newAccount();
        acc.UUID__c = 'tt47c899-7e59-47ad-9ad7-7d0f6310f4ea';
        insert acc;

        Product2 pd = DataFactory.newProduct('Product', True, 'BRL');
        insert pd;

        Pricebook2 pb = DataFactory.newPricebook();

        Opportunity opp = DataFactory.newOpportunity(acc.Id, pb, 'Client_Success_Renegotiation');
        insert opp;

        PricebookEntry pbEntry = DataFactory.newPricebookEntry(pb, pd, opp);   
        pbEntry.UnitPrice= 7.95;
        insert pbEntry;

        Order ord = new Order (
            accountId = acc.Id,
            status = 'Draft',
            EffectiveDate = Date.today(),
            UUID__c = 'da26ed65-debb-48b8-bbfb-6c60326761c3',
            Pricebook2Id = pb.Id,
            CurrencyIsoCode = 'BRL',
            Type = 'New Business'
        );
        insert ord;

        OrderItem orItem = new OrderItem(
            OrderId = ord.Id,
            PricebookEntryId = pbEntry.Id,
            Quantity = 1,
            Type__c = '1', 
            UnitPrice = 7.95,
            ListPrice = 7.95
        );

        insert orItem;
        
        ord.status = 'Activated';
        update ord;

    }
  
    @IsTest
    public static void executeCancelAction() {
      SelfTerminationRequest selfTerminationRequest = SelfTerminationRequestMock.getMock('CANCEL');
  
      RestRequest request = buildRequest(selfTerminationRequest);
      
      Test.startTest();
  
      RestContext.response = new RestResponse();
  
      RestContext.request = request;
      SelfTerminationRoute.SelfTerminationResponse response = SelfTerminationRoute.post();
  
      Test.stopTest();
  
      System.assertEquals(
        204,
        response.getStatusCode(),
        'Error Message: ' + response.getErrorMessage()
      );
    }

    @IsTest
    public static void executeUndoAction() {
      Order ord = [SELECT Id, UUID__c, accountId, account.UUID__c,Is_Account_On_Tagus__c,Type FROM Order Limit 1];
      ord.status = 'Cancel Requested';
      update ord;

      SelfTerminationRequest selfTerminationRequest = SelfTerminationRequestMock.getMock('UNDO');
  
      RestRequest request = buildRequest(selfTerminationRequest);
      
      Test.startTest();
  
      RestContext.response = new RestResponse();
  
      RestContext.request = request;
      SelfTerminationRoute.SelfTerminationResponse response = SelfTerminationRoute.post();
  
      Test.stopTest();
  
      System.assertEquals(
        204,
        response.getStatusCode(),
        'Error Message: ' + response.getErrorMessage()
      );
    }

    @IsTest
    public static void executeUndoActionOrderNotCancelRequested() {
      SelfTerminationRequest selfTerminationRequest = SelfTerminationRequestMock.getMock('UNDO');
  
      RestRequest request = buildRequest(selfTerminationRequest);
      
      Test.startTest();
  
      RestContext.response = new RestResponse();
  
      RestContext.request = request;
      SelfTerminationRoute.SelfTerminationResponse response = SelfTerminationRoute.post();
  
      Test.stopTest();
  
      System.assertEquals(
        409,
        response.getStatusCode(),
        'Error Message: ' + response.getErrorMessage()
      );
    }

    @IsTest
    public static void executeMissingParametersAction() {
      SelfTerminationRequest selfTerminationRequest = SelfTerminationRequestMock.getMockMissingParameters();
  
      RestRequest request = buildRequest(selfTerminationRequest);
      
      Test.startTest();
  
      RestContext.response = new RestResponse();
  
      RestContext.request = request;
      SelfTerminationRoute.SelfTerminationResponse response = SelfTerminationRoute.post();
  
      Test.stopTest();
  
      System.assertEquals(
        400,
        response.getStatusCode(),
        'Error Message: ' + response.getErrorMessage()
      );
    }


    @IsTest
    public static void executeGetMockOrderDoesntExist() {
      SelfTerminationRequest selfTerminationRequest = SelfTerminationRequestMock.getMockOrderDoesntExist();
  
      RestRequest request = buildRequest(selfTerminationRequest);
  
      Test.startTest();
  
      RestContext.response = new RestResponse();
  
      RestContext.request = request;
      SelfTerminationRoute.SelfTerminationResponse response = SelfTerminationRoute.post();
  
      Test.stopTest();
  
      System.assertEquals(
        404,
        response.getStatusCode(),
        'Error Message: ' + response.getErrorMessage()
      );
    }


    private static RestRequest buildRequest( SelfTerminationRequest selfTerminationRequest){
      RestRequest request = new RestRequest();
      request.requestURI = '/services/apexrest/self-termination';
      request.httpMethod = 'POST';
      request.requestBody = Blob.valueOf(JSON.serialize(selfTerminationRequest));
      return request;
    }
}