/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 09-13-2021
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/
@isTest(seeAllData=false)
public class AccountPartnerTagusDTOTest {
   
    @TestSetup
    static void setupData(){
        String recordTypePartnerFlow = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Account').getRecordTypeId();
        Account partnerAcc = PartnerDataFactory.newAccount();
        partnerAcc.RecordTypeId = recordTypePartnerFlow;
        partnerAcc.CRM_ID__c = '1231';
        partnerAcc.UUID__c = new Uuid().getValue();
        INSERT partnerAcc;
             
        String recordTypePartnerContactFlow = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Contact').getRecordTypeId();
        Contact partnerContact = PartnerDataFactory.newContact(partnerAcc.Id);
        partnerContact.RecordTypeId = recordTypePartnerContactFlow;        
        partnerContact.Type_of_Contact__c = 'Admin';
        partnerContact.UUID__c = new Uuid().getValue();
        INSERT partnerContact;      
    }

    @isTest 
    static void newAccountPartnerTagusDTO_Test() {
        List<Account> accLst = [SELECT ID,UUID__c FROM ACCOUNT WHERE ParentId = null];      
        Map<Id, Account> mapAccount = AccountPartnerTagusDTO.getAccountMapWithFieldsToParse(  accLst  );
        Account aAcc = mapAccount.get(accLst.get(0).Id);
        Test.startTest();
            AccountPartnerTagusDTO accPartnerDto = new AccountPartnerTagusDTO(aAcc);  
            accPartnerDto.setPartnerType('FULL_SERVICE');
            System.assertEquals(aAcc.UUID__c, accPartnerDto.getId());
            System.assertEquals(aAcc.Id, accPartnerDto.getSalesforceId());
            System.assertEquals('FULL_SERVICE', accPartnerDto.getPartnerType());             
            System.assertEquals(1, accPartnerDto.getContacts().size()); 
            System.assertEquals(aAcc.AccountContactRelations[0].Contact.UUID__c, accPartnerDto.getContacts().get(0).getId());
            System.assertEquals(aAcc.AccountContactRelations[0].Contact.Id,  accPartnerDto.getContacts().get(0).getSalesforceId());
        Test.stopTest();
	}
}