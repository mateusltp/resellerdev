/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 05-11-2022
 * @last modified by  : alysson.mota@gympass.com
 * Modifications Log 
 * Ver   Date         Author                             Modification
 * 1.0   07-05-2021   gft.samuel.silva@ext.gympass.com   Initial Version
**/
public interface IAccountService {

   // void updateNetworkType(Map<Id, String> aParentIdWithTypeOfOwnership);
  //  List<Account> createChildAccountFromForm(List<Account> aFormAccountLst );
    List<Account> avoidDuplicateAccountName(List<Account> lLstAccount);
    List<Account> wishListAndTierDependency(List<Account> lLstAccount);
    void avoidDuplicatedNetworkIds(List<Account> lLstAccount);
    Object buildNetwork(Id recordId);
    Object buildNetworkForContracts(Id recordId, Id oppId);
    List<AccountContactRelation> getContactsFromParent(Id recordId);
    void setAccountTypeProspect(List<Account> accountList);
    void getAccountWishlistFromParent(List<Account> accountList);
    List<AccountContactRelation> getContactRelations(Set<Id> recordIds);
    List<AccountContactRelation> getContactsFromParentThatAreNotRelatedToChild(Id childAccountId, Id parentAccountId);
    List<AccountPartnerServiceImpl.Response> sendAccountsForExclusivityApproval(List<Id> ids, String str);
}