/**
 * @description       : 
 * @author            : AlyssonMota
 * @group             : 
 * @last modified on  : 02-26-2021
 * @last modified by  : Alysson Mota
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   10-14-2020   Alysson Mota   Initial Version
**/
public with sharing class OfferSummaryDeleteController {
    
    @AuraEnabled
    public static Result deleteRecord(String recordId){
        Result result = new Result();
        
        try {
            Id objId = (Id)recordId;
            String sobjectType = objId.getSObjectType().getDescribe().getName();
    
            SObject record = Database.query('Select Id From ' + sobjectType + ' Where Id = :objId');
    
            Database.DeleteResult deleteResult = Database.delete(record);
    
            if (deleteResult.isSuccess()) {
                result.status   = 'SUCCESS';
                result.message  = 'The record has been deleted';
            }
        } catch(Exception e) {
            if (e.getMessage().contains('Elegibility Deletion')) {
                throw new AuraHandledException ('It is not allowed to remove a group when the Billing Setting has only one Eligibility and Membership Fee Setup');
            } else {
                throw new AuraHandledException (e.getMessage());
            }
        }

        return result;
    }

    public class Result {
        @AuraEnabled public String status;
        @AuraEnabled public String message;
    }
}