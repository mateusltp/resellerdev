/**
 * @description       : 
 * @author            : AlyssonMota
 * @group             : 
 * @last modified on  : 01-27-2022
 * @last modified by  : Vinicius Teixeira
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   01-20-2021   Alysson Mota   Initial Version
**/
public without sharing class AutoConvertLeadsInbound {
    private static Map<String, String> countryNameToCurrencyIsoCodeMap;
    private static Map<String, String> countryNameToPriceBookNameMap;
    private static Map<String, String> countryNameToMembershipNameMap;
    private static Map<String, String> countryNameToGympassEntityNameMap;
    private static Map<String, String> countryNameToUserEmailNameMap;
     
    static {
    	// Initialize countryNameToCurrencyIsoCodeMap
    	List<Country_Currency_ISO_Code__mdt> countryCurrencyISOCodeList = [
       		SELECT Country_Name__c, Currency_Iso_Code__c
            FROM Country_Currency_ISO_Code__mdt
        ];
		
        countryNameToCurrencyIsoCodeMap = new Map<String, String>();
        for (Country_Currency_ISO_Code__mdt countryCurrencyISOCode : countryCurrencyISOCodeList) {
        	countryNameToCurrencyIsoCodeMap.put(countryCurrencyISOCode.Country_Name__c, countryCurrencyISOCode.Currency_Iso_Code__c);
        }
        
        // Initialize countryNameToPriceBookNameMap;
       	List<SMB_Lead_Conversion_Pricebook__mdt> smbLeadConversionPricebookList = [
        	SELECT Pricebook_Country__c, Pricebook_Name__c
            FROM SMB_Lead_Conversion_Pricebook__mdt
        ];
        
        countryNameToPriceBookNameMap = new Map<String, String>();
        for (SMB_Lead_Conversion_Pricebook__mdt smbLeadConversionPricebook : smbLeadConversionPricebookList) {
        	countryNameToPriceBookNameMap.put(smbLeadConversionPricebook.Pricebook_Country__c, smbLeadConversionPricebook.Pricebook_Name__c);
        }

        // Initialize countryNameToMembershipNameMap
        List<Membership_Fee__mdt> membershipFeeList = [
            SELECT Name__c, Country__c
            FROM Membership_Fee__mdt
        ];

        countryNameToMembershipNameMap = new Map<String, String>();
        for (Membership_Fee__mdt mf : membershipFeeList) {
            countryNameToMembershipNameMap.put(mf.Country__c, mf.Name__c);
        }

        // Initialize countryNameToGympassEntityNameMap
        List<Gympass_Entity__mdt> gympassEntityList = [
            SELECT Gympass_Entity_Name__c, Country__c
            FROM Gympass_Entity__mdt 
        ];

        countryNameToGympassEntityNameMap = new Map<String, String>();
        for (Gympass_Entity__mdt ge : gympassEntityList) {
            countryNameToGympassEntityNameMap.put(ge.Country__c, ge.Gympass_Entity_Name__c);
        }
        
        // Initialize countryNameToUserEmailNameMap
        List<User_For_SMB_Allocation__mdt> usersForSmbAllocation = [
            SELECT User_email__c, Country__c
            FROM User_For_SMB_Allocation__mdt 
        ];

        countryNameToUserEmailNameMap = new Map<String, String>();
        for (User_For_SMB_Allocation__mdt user : usersForSmbAllocation) {
            countryNameToUserEmailNameMap.put(user.Country__c, user.User_email__c);
        }
    }
    
    @InvocableMethod
    public static void convertLeadInbound(List<Id> listLeadIds) {
   		Map<Id, Lead> leadsToConvertMap = new Map<Id, Lead> ([
        	SELECT Name, Company, Country, CNPJ__c, Email, OwnerId, Finance_Contact_Email__c, Finance_Contact_Name__c, HR_Contact_Email__c, HR_Contact_Name__c, Self_Checkout__c, Error_Message__c
            FROM Lead
            WHERE Id IN :listLeadIds
        ]);
        
		Map<String, Account> legalNumberToAccountMap = getAccounts(leadsToConvertMap);
        Map<String, Contact> emailToContactMap = getContacts(leadsToConvertMap);
        
        Map<Id, List<Opportunity>> accountIdToOpportunityList = new Map<Id, List<Opportunity>>();
        Map<Id, List<Opportunity>> tempMapAccIdToOppList1 = getOpportunitiesForAccounts(legalNumberToAccountMap.values()); 
        Map<Id, List<Opportunity>> tempMapAccIdToOppList2 = getOpportunitiesForContacts(emailToContactMap.values()); 
        accountIdToOpportunityList.putAll(tempMapAccIdToOppList1);
        accountIdToOpportunityList.putAll(tempMapAccIdToOppList2);
        
        List<Lead> leadsWithErrorVerification = new List<Lead>();        
        List<Database.LeadConvert> leadConvertList = new List<Database.LeadConvert>();
        
        for (Lead l : leadsToConvertMap.values()) {
            String errorMessage = verifyIfThereIsErrorWithLead(l);

            if (errorMessage != '') {
                l.Error_Message__c = errorMessage;
                leadsWithErrorVerification.add(l);
            } else {
                Database.LeadConvert lc = getLeadConvertInstance(l, legalNumberToAccountMap, emailToContactMap, accountIdToOpportunityList);
                leadConvertList.add(lc);
            }
        }
        
        updateLeadsWithErrorVerification(leadsWithErrorVerification);

      	List<Database.LeadConvertResult> leadConvertResultList = convertLeads(leadConvertList);
        
        evaluateConvertResult(leadConvertResultList);
    }

    private static String verifyIfThereIsErrorWithLead(Lead l) {
        List<String> errors = new List<String>();
        String errorMessage = '';

        /*
        if (l.CNPJ__c == null && l.Country ==  'Brazil') {
            errors.add('The Legal Number cannot be empty for Brazil.\n');
        }
        */

        if (errors.size() > 0) {
            for (String error : errors) {
                errorMessage += error;
            }
        }

        return errorMessage;
    }

    private static void updateLeadsWithErrorVerification(List<Lead> leadsWithErrorVerification) {
        try {
            update leadsWithErrorVerification;
        } catch(Exception e) {
            createExceptionError(e);
        }
    }
    
    private static List<Database.LeadConvertResult> convertLeads(List<Database.LeadConvert> leadConvertList) {
		List<Database.LeadConvertResult> leadConvertResultList = Database.convertLead(leadConvertList, false);
        return leadConvertResultList;
    }
    
    private static Map<String, Account> getAccounts(Map<Id, Lead> leadsToConvertMap) {
    	Map<String, Account> legalNumberToAccountMap = new Map<String, Account>();
        Set<String> legalNumberSet = new Set<String>();
        
        for (Lead l : leadsToConvertMap.values()) {
       		legalNumberSet.add(l.CNPJ__c);
        }
        
        List<Account> accountList = [
        	SELECT Id, Name, Id_Company__c, BillingCountry
            FROM Account
            WHERE Id_Company__c IN :legalNumberSet
            AND Id_Company__c != null
            AND Business_Unit__c = 'SMB'
            AND NumberOfEmployees <= 500
            AND BillingCountry = 'Brazil'
        ];
        
        for (Account acc : accountList) {
       		legalNumberToAccountMap.put(acc.Id_Company__c, acc);
        }
        
        return legalNumberToAccountMap;
    }
    
    private static Map<Id, List<Opportunity>> getOpportunitiesForAccounts(List<Account> accounts) {
   		Set<Id> accountIdSet = new Set<Id>();
        Map<Id, List<Opportunity>> accountIdToOpportunityList = new Map<Id, List<Opportunity>>();
        
        for (Account acc : accounts) {
        	accountIdSet.add(acc.Id);
        }
        
        Set<String> recordTypeDeveloperNameSet = new Set<String>{
            'SMB', 'Direct_Channel', 'Price_Renegotiation'
        };
        
        List<Opportunity> opportunities = [
        	SELECT Id, Name, StageName, AccountId, Type, RecordType.DeveloperName
            FROM Opportunity
            WHERE AccountId IN :accountIdSet
            AND RecordType.DeveloperName IN :recordTypeDeveloperNameSet
            ORDER BY CreatedDate DESC
        ];

        for (Opportunity opp : opportunities) {
            if (accountIdToOpportunityList.containsKey(opp.AccountId)) {
           		accountIdToOpportunityList.get(opp.AccountId).add(opp);
            } else {
                accountIdToOpportunityList.put(opp.AccountId, new List<Opportunity>{opp});
            }
        }
        
        return accountIdToOpportunityList;
    }

    private static Map<Id, List<Opportunity>> getOpportunitiesForContacts(List<Contact> contacts) {
        Set<Id> accountIdSet = new Set<Id>();
        Map<Id, List<Opportunity>> contactIdToOpportunityList = new Map<Id, List<Opportunity>>();
        Map<Id, List<Opportunity>> accountIdToOpportunityList = new Map<Id, List<Opportunity>>();
        
        for (Contact c : contacts) {
            accountIdSet.add(c.AccountId);
        }
        
        Set<String> recordTypeDeveloperNameSet = new Set<String>{
            'SMB', 'Direct_Channel', 'Price_Renegotiation'
        };
        
        List<Opportunity> opportunities = [
            SELECT Id, Name, StageName, AccountId, Type, RecordType.DeveloperName
            FROM Opportunity
            WHERE AccountId IN :accountIdSet
            AND RecordType.DeveloperName IN :recordTypeDeveloperNameSet
            ORDER BY CreatedDate DESC
        ];

        for (Opportunity opp : opportunities) {
            if (accountIdToOpportunityList.containsKey(opp.AccountId)) {
                    accountIdToOpportunityList.get(opp.AccountId).add(opp);
            } else {
                accountIdToOpportunityList.put(opp.AccountId, new List<Opportunity>{opp});
            }
        }

        return accountIdToOpportunityList;
    }
    
    private static Map<String, Contact> getContacts(Map<Id, Lead> leadsToConvertMap) {
    	Map<String, Contact> emailToContactMap = new Map<String, Contact>();
        Set<String> emailSet = new Set<String>();
        
        for (Lead l : leadsToConvertMap.values()) {
       		emailSet.add(l.Email);
        }
        
        List<Contact> contactList = [
        	SELECT Id, Email, AccountId
            FROM Contact
            WHERE 
            (   Email IN :emailSet
                //AND Account.Id_Company__c != null
                AND Account.Business_Unit__c = 'SMB'
                AND Account.NumberOfEmployees <= 500
                AND Account.BillingCountry = 'Brazil')
            OR( Email IN :emailSet
                AND Account.Business_Unit__c = 'SMB'
                AND Account.NumberOfEmployees <= 1000
                AND Account.BillingCountry = 'United States')
        ];
        
        for (Contact c : contactList) {
       		emailToContactMap.put(c.Email, c);
        }
        
        return emailToContactMap;
    }
    
    private static Database.LeadConvert getLeadConvertInstance(Lead l, Map<String, Account> legalNumberToAccountMap, Map<String, Contact> emailToContactMap, Map<Id, List<Opportunity>> accountIdToOpportunityList) {
        Id accountId = null;
        Database.LeadConvert lc = new Database.LeadConvert();
        lc.setLeadId(l.Id);
        lc.setConvertedStatus('Convertido');
        
        if (emailToContactMap.get(l.Email) != null) {
            lc.setContactId(emailToContactMap.get(l.Email).Id);
        	lc.setAccountId(emailToContactMap.get(l.Email).AccountId);
            accountId = emailToContactMap.get(l.Email).AccountId;
        } else if (legalNumberToAccountMap.get(l.CNPJ__c) != null) {
            lc.setAccountId(legalNumberToAccountMap.get(l.CNPJ__c).Id);
            accountId = legalNumberToAccountMap.get(l.CNPJ__c).Id;
        }
        
        if (accountId != null) {
            if (shouldCreateNewOpportunity(accountId, accountIdToOpportunityList)) {
            	lc.setDoNotCreateOpportunity(false);
            } else {
                lc.setDoNotCreateOpportunity(true);
            }
        }
        
        return lc;
    }
    
    private static Boolean shouldCreateNewOpportunity(Id accountId, Map<Id, List<Opportunity>> accountIdToOpportunityList) {
   		List<Opportunity> opps = accountIdToOpportunityList.get(accountId);
		Boolean shouldCreateOpportunity;
        
        if (opps == null) {
       		shouldCreateOpportunity = true;
        } else {
        	Opportunity lastCreatedOpp = opps.get(0);
            if (lastCreatedOpp.StageName != 'Perdido') {
            	shouldCreateOpportunity = false;
            } else {
                shouldCreateOpportunity = true;
            }
        }
            
        return shouldCreateOpportunity;
    }
    
    
    private static void evaluateConvertResult(List<Database.LeadConvertResult> leadConvertResultList) {
		List<Database.LeadConvertResult> successConvertResults = new List<Database.LeadConvertResult>();
        List<Database.LeadConvertResult> errorConvertResults = new List<Database.LeadConvertResult>();
        
        for (Database.LeadConvertResult convertResult : leadConvertResultList) {
            if (convertResult.isSuccess()) {
     			successConvertResults.add(convertResult);		
            } else {
                errorConvertResults.add(convertResult);
            }
        }
        
        handleSuccessConvertResults(successConvertResults);
        handleErrorConvertResults(errorConvertResults);
    }
    
    private static void handleSuccessConvertResults(List<Database.LeadConvertResult> successConvertResults) {
   		Set<Id> leadIdSet = new Set<Id>();
   		Map<Id, Id> accountIdToLeadId = new Map<Id, Id>();
        Map<Id, Id> leadIdToContactId = new Map<Id, Id>();
        Map<Id, Id> oppIdToLeadId = new Map<Id, Id>();
        
        for (Database.LeadConvertResult lcr : successConvertResults) {
            leadIdSet.add(lcr.getLeadId());
            accountIdToLeadId.put(lcr.getAccountId(), lcr.getLeadId());
            leadIdToContactId.put(lcr.getLeadId(), lcr.getContactId());    
            oppIdToLeadId.put(lcr.getOpportunityId(), lcr.getLeadId());
        }
        
        updateAndCreateRecords(leadIdSet, accountIdToLeadId, leadIdToContactId, oppIdToLeadId);
    }

    @future
    private static void updateAndCreateRecords(Set<Id> leadIdSet, Map<Id, Id> accountIdToLeadId, Map<Id, Id> leadIdToContactId, Map<Id, Id> oppIdToLeadId) {
        Map<Id, Lead> idToLeadMap = new Map<Id, Lead> ([
        	SELECT  Name, Company, Country, CNPJ__c, Email, OwnerId, Finance_Contact_Email__c, Finance_Contact_Name__c, HR_Contact_Email__c, HR_Contact_Name__c, Self_Checkout__c, Error_Message__c, Proposal_Value_With_Discount__c, Proposal_Value_Without_Discount__c,
                    Broker_Company__c, Broker_Email__c, Broker_Last_Name__c, Broker_First_Name__c, State
            FROM Lead
            WHERE Id IN :leadIdSet
        ]);

        List<Account> updatedAccounts = updateAccounts(accountIdToLeadId);
        List<Contact> updatedContacts = updateContacts(leadIdToContactId, idToLeadMap);
        List<Opportunity> updatedOpportunities = updateOpportunities(oppIdToLeadId, idToLeadMap, updatedContacts);
        List<Quote> createdQuotes = createQuotes(updatedOpportunities);
        List<QuoteLineItem> createdQuoteLineItems = createQuoteLineItems(createdQuotes, idToLeadMap, oppIdToLeadId);

        syncQuoteWithOpportunities(createdQuoteLineItems, createdQuotes, updatedOpportunities);
    }
    
    private static void handleErrorConvertResults(List<Database.LeadConvertResult> errorConvertResults) {
        Map<Id, String> leadIdToErrorMessageMap = new Map<Id, String>();

        for (Database.LeadConvertResult cr : errorConvertResults) {
            List<Database.Error> errors = cr.getErrors();
            
            String errorMessage = '';
            
            for (Database.Error error : errors) {
                errorMessage += error.getMessage() + '\n';

                if (error instanceof Database.DuplicateError) {
                    errorMessage += getMessageForDuplicateError(error);
                } else {
        
                    List<String> fieldsAffected = error.getFields();
                    
                    if (fieldsAffected.size() > 0) {
                        errorMessage += 'Fields: ';
                    
                        for (Integer i=0; i<fieldsAffected.size(); i++) {
                            if (i == fieldsAffected.size()-1) {
                                errorMessage += fieldsAffected + '\n';
                            } else {
                                errorMessage += fieldsAffected + ', ';
                            }
                        }
                    }
                }
                errorMessage += 'Error Status Code: ' + error.getStatusCode();
            }

            leadIdToErrorMessageMap.put(cr.getLeadId(), errorMessage);
        }

        List<Lead> leadsToUpdate = [
            SELECT Id, Error_Message__c
            FROM Lead
            WHERE Id IN :leadIdToErrorMessageMap.keySet() 
        ];

        for (Lead l : leadsToUpdate) {
            l.Error_Message__c = leadIdToErrorMessageMap.get(l.Id);
            System.debug('l.Error_Message__c');
            System.debug(l.Error_Message__c);
        }

        

        try {
            update leadsToUpdate;
        } catch(Exception e) {
            createExceptionError(e);
        }
    }

    private static String getMessageForDuplicateError(Database.Error error) {
        String errorMessage = '';
        Database.DuplicateError duplicateError = (Database.DuplicateError)error;
        Datacloud.DuplicateResult duplicateResult = duplicateError.getDuplicateResult();
            
        String entityType = duplicateResult.getDuplicateRuleEntityType();
        Id recordId = null;
            
        List<DataCloud.MatchResult> mList = duplicateResult.getMatchResults(); 
        for (DataCloud.MatchResult m : mList) {
            recordId = m.getMatchRecords()[0].getRecord().Id;
            break;
        }
        
        if (entityType == 'Contact' && recordId != null) {
            errorMessage += 'While converting the lead and trying to create a contact it was noticed that may alerady exist a contact with similar information.';
            errorMessage += 'This contact has the following ID: ' + recordId;
            errorMessage += '\n';
        }
        
        if (entityType == 'Account' && recordId != null) {
            errorMessage += 'While converting the lead and trying to create an account it was noticed that may alerady exist an account with similar information.';
            errorMessage += 'This account has the following ID: ' + recordId;
            errorMessage += '\n';
        }

        return errorMessage;
    }
    
    private static List<Account> updateAccounts(Map<Id, Id> accountIdToLeadId) {
        List<Account> accountsToUpdate = new List<Account>();
        
        List<Account> accounts = [
            SELECT Id, Business_Unit__c, CurrencyIsoCode, BillingCountry, UUID__c, Legal_Document_Type__c
            FROM Account
            WHERE Id =: accountIdToLeadId.keySet()
        ];
        
        for (Account acc : accounts) {
            acc.Business_Unit__c = 'SMB';
            acc.autonomous_marketplace_contract__c = 'Yes';

            // Generate UUid
            Uuid uuid = new Uuid();
            acc.UUID__c = uuid.getValue();
            
            String currencyIsoCode = countryNameToCurrencyIsoCodeMap.get(acc.BillingCountry); 
            if (currencyIsoCode != null) {
            	acc.CurrencyIsoCode = currencyIsoCode;
            }

            if (acc.BillingCountry == 'Brazil') {
                acc.Legal_Document_Type__c = 'CNPJ';
            }
            
           	accountsToUpdate.add(acc);
        }
        
        try {
            update accountsToUpdate;
        } catch(Exception e) {
            createExceptionError(e);
        }
        
        return accountsToUpdate;
    }
    
    private static List<Contact> updateContacts(Map<Id, Id> leadIdToContactId, Map<Id, Lead> idToLeadMap) {
  		List<Contact> contactsToUpdate = new List<Contact>();
        
        Map<Id, Contact> idToContactMap = new Map<Id, Contact> ([
        	SELECT Id, Name, Primary_HR_Contact__c, Finance_Contact__c, Email, AccountId, FirstName, LastName, Area_Setor__c,Account.BillingCountry, DecisionMaker__c
            FROM Contact
            WHERE  Id IN :leadIdToContactId.values()
        ]);
        
        for (Id leadId : idToLeadMap.keySet()) {
      		Lead convertedLead = idToLeadMap.get(leadId);
           	Id contactId = null;
            Contact c = null;
                
            contactId = leadIdToContactId.get(leadId);
            c = idToContactMap.get(contactId);
        	    
            if (contactId != null && c != null) {
                String hrEmail = convertedLead.HR_Contact_Email__c;
                String financeEmail = convertedLead.Finance_Contact_Email__c;

                if (hrEmail == c.Email) {
                	c.Primary_HR_Contact__c = TRUE;
                    c.Area_Setor__c = 'Human Resources';
                }
                
                if (financeEmail == c.Email) {
                	c.Finance_Contact__c = TRUE;
                    c.Area_Setor__c = 'Finance';
                }
                
                if (c.Primary_HR_Contact__c == FALSE && c.Finance_Contact__c == TRUE) {
                    c.Primary_HR_Contact__c = TRUE;
                }

                if (c.Primary_HR_Contact__c == FALSE && c.Finance_Contact__c == FALSE) {
                    c.Primary_HR_Contact__c = TRUE;
                }

                String currencyIsoCode = countryNameToCurrencyIsoCodeMap.get(c.Account.BillingCountry); 
                if ( currencyIsoCode!=null )
                    c.CurrencyIsoCode = currencyIsoCode;
                    
                c.DecisionMaker__c = 'Yes';
                contactsToUpdate.add(c);
            }
        }
        
        try {
            update contactsToUpdate;
        } catch(Exception e) {
            createExceptionError(e);
        }
        
        return contactsToUpdate;
    }
    
    private static List<Opportunity> updateOpportunities(Map<Id, Id> oppIdToLeadId, Map<Id, Lead> idToLeadMap, List<Contact> updatedContacts) {
  		Id oppRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SMB').getRecordTypeId();
        ConvertLeadsResellerHelper leadsResellerHelper = new ConvertLeadsResellerHelper(); 
          
        Set<Id> opportunityIdSet = new Set<Id>();
        
        List<Opportunity> oppsToUpdate = new List<Opportunity>();
        
        Map<String, Id> countryNameToGympassEntityId = new Map<String, Id>();
        Map<Id, List<Contact>> accountIdToContactList = new Map<Id, List<Contact>>();
        Map<String, Id> countryNameToUserId = new Map<String, Id>();
          
        // Map<Id, Account> leadIdToResellerAccount = getResellerAccount(idToLeadMap);
        // Map<Id, Contact> leadIdToResellerContact = getResellerContact(idToLeadMap);
  
        Map<Id, Contact> leadIdToResellerContact = leadsResellerHelper.getOrCreateResellerContacts(idToLeadMap);

        List<Account> gympassEntities = [
            SELECT Id, Name, BillingCountry
            FROM Account
            WHERE Name IN :countryNameToGympassEntityNameMap.values()
        ];

        for (Account gympassEntity : gympassEntities) {
            countryNameToGympassEntityId.put(gympassEntity.BillingCountry, gympassEntity.Id);
        }
        
        for (Contact c : updatedContacts) {
            if (accountIdToContactList.containsKey(c.AccountId)) {
           		accountIdToContactList.get(c.AccountId).add(c);
            } else {
                accountIdToContactList.put(c.AccountId, new List<Contact>{c});
            }
        }

        List<User> users = [
            SELECT Id, Email
            FROM User
            WHERE Email IN :countryNameToUserEmailNameMap.values()
        ];

        for (User u : users) {
            for (String countryName : countryNameToUserEmailNameMap.keySet()) {
                String userEmail = countryNameToUserEmailNameMap.get(countryName);

                if (userEmail == u.Email) {
                    countryNameToUserId.put(countryName, u.Id);
                }
            }
        }
        
        List<Opportunity> opps = [
            SELECT  Id, Name, Gympass_Entity__c, RecordTypeId, Pricebook2Id, StageName, CurrencyIsoCode, Self_Checkout__c, Account.BillingCountry, Account.CurrencyIsoCode,
                    Reseller_del__c, Contact__c
            FROM Opportunity
            WHERE Id IN :oppIdToLeadId.keySet()
        ];
        
        for (Opportunity opp : opps) {
            
            List<Pricebook2> pbStandard = [SELECT Id FROM Pricebook2 WHERE IsActive = true AND IsStandard = true LIMIT 1];
            Id pricebookId = pbStandard.size() > 0 ? pbStandard[0].Id : null;

            String currencyIsoCode = countryNameToCurrencyIsoCodeMap.get(opp.Account.BillingCountry);
            String oppStage = 'Proposta Enviada';
            
            Id convertedLeadId = oppIdToLeadId.get(opp.Id);
            Lead convertedLead = idToLeadMap.get(convertedLeadId);
            
            Id contatoFinanceiroId = null;
            Id contatoRHId = null;
            
           	List<Contact> contacts = accountIdToContactList.get(opp.AccountId);
            
            if (contacts != null) {
                for (Contact c : contacts) {
                    if (c.Primary_HR_Contact__c == true) {
                        contatoRHId = c.Id;
                    }
                    
                    if (c.Finance_Contact__c == true) {
                        contatoFinanceiroId = c.Id;
                    }
                }
            }
            
            opp.RecordTypeId = oppRecordTypeId;
            opp.Pricebook2Id = pricebookId;
            opp.StageName = oppStage;
            opp.CurrencyIsoCode = currencyIsoCode;
            opp.Self_Checkout__c = true;
            opp.Pontos_de_contato_financeiro_no_cliente__c = contatoFinanceiroId;
            opp.Pontos_de_contato_RH_no_cliente__c = contatoRHId;
            opp.CurrencyIsoCode = opp.Account.CurrencyIsoCode;
            opp.Gympass_Entity__c = countryNameToGympassEntityId.get(opp.Account.BillingCountry);
            opp.Type__c = 'New Business';
            opp.Type = 'New Business';
            opp.CloseDate = Date.today();
            opp.Self_Checkout__c = true;
            opp.Responsavel_Gympass_Relacionamento__c = countryNameToUserId.get(opp.Account.BillingCountry);
            opp.Reseller_del__c = (leadIdToResellerContact.get(convertedLeadId) != null ? leadIdToResellerContact.get(convertedLeadId).AccountId : null);
            opp.Contact__c = (leadIdToResellerContact.get(convertedLeadId) != null ? leadIdToResellerContact.get(convertedLeadId).Id : null);
            oppsToUpdate.add(opp);
        }
        
        try {
            update oppsToUpdate;
        } catch(Exception e) {
            createExceptionError(e);
        }
        
        return oppsToUpdate;
    }
    
    private static List<Quote> createQuotes(List<Opportunity> updatedOpportunities) {
		Id quoteRecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Direct_Channel').getRecordTypeId();
        List<Quote> quotesToInsert = new List<Quote>();
        Map<Id, Id> oppIdToCopayPlanIdMap = getCopayPlansForOpportunities(updatedOpportunities);

        for (Opportunity opp : updatedOpportunities) {
        	Quote quote = new Quote();
            quote.RecordTypeId = quoteRecordTypeId;
            quote.OpportunityId = opp.Id;
            quote.Name = opp.Name;
            quote.Are_Family_Members_Included2__c = 'Yes';
            quote.License_Fee_Waiver__c = 'No';
            quote.Setup_Fee__c = 0;
            quote.License_Fee_Frequency__c = 'Monthly';
            quote.LF_PO_Number_Required__c = 'No';
            quote.MB_PO_Number_Required__c = 'No';
            quote.Payment_Method_License_Fee__c = 'Boleto Bancário';
            quote.Payment_Terms_for_Setup_Fee__c = 0;
            quote.Contact_Permission__c = 'Allowlist';
            quote.License_Fee_cut_off_date__c = 1;
            quote.Employee_Company_ID__c = true;
            quote.Employee_Corporate_Email__c = true;
            quote.Employee_Name__c = true;
            quote.Employee_National_ID_if_applicable__c = true;
            quote.Submission_Date__c = Date.today();
            quote.Total_Setup_Fee_Value__c = 0;
            quote.Payment_method__c = 'Credit Card';
            quote.Employee_Registration_Method__c = 'Eligible file';
            quote.Unique_Identifier__c = (opp.CurrencyIsoCode == 'BRL' ? 'National ID' : 'Corporate E-mail') ;
            quote.ContactId = opp.Pontos_de_contato_RH_no_cliente__c;
            quote.Pricebook2Id = opp.Pricebook2Id;
            quote.CurrencyIsoCode = opp.CurrencyIsoCode;
            quote.Gym__c = oppIdToCopayPlanIdMap.get(opp.Id);
            quote.Expansion_100_of_the_eligible__c = true;
            quote.Are_Family_Members_Included2__c = 'Yes';
            quote.Status = 'Draft';

            if (opp.Pontos_de_contato_RH_no_cliente__c != null && opp.Pontos_de_contato_financeiro_no_cliente__c == null) {
                quote.Administrator__c = opp.Pontos_de_contato_RH_no_cliente__c;
                quote.Finance__c = opp.Pontos_de_contato_RH_no_cliente__c;
                quote.Operations__c = opp.Pontos_de_contato_RH_no_cliente__c;
            } else if (opp.Pontos_de_contato_RH_no_cliente__c == null && opp.Pontos_de_contato_financeiro_no_cliente__c != null) {
                quote.Administrator__c = opp.Pontos_de_contato_financeiro_no_cliente__c;
                quote.Finance__c = opp.Pontos_de_contato_financeiro_no_cliente__c;
                quote.Operations__c = opp.Pontos_de_contato_financeiro_no_cliente__c;
            } else if (opp.Pontos_de_contato_RH_no_cliente__c != null && opp.Pontos_de_contato_financeiro_no_cliente__c != null) {
                quote.Administrator__c = opp.Pontos_de_contato_RH_no_cliente__c;
                quote.Finance__c = opp.Pontos_de_contato_financeiro_no_cliente__c;
                quote.Operations__c = opp.Pontos_de_contato_RH_no_cliente__c;
            }
            
            quotesToInsert.add(quote);
        }

        try {
            insert quotesToInsert;
        } catch(Exception e) {
            createExceptionError(e);
        }
        
        return quotesToInsert;
    }

    private static Map<Id, Id> getCopayPlansForOpportunities(List<Opportunity> updatedOpportunities) {
        System.debug('getCopayPlansForOpportunities');
        Map<Id, Id> oppIdToCopayPlanIdMap = new Map<Id, Id>();
        Map<String, Copay_Plan__c> copayPlanNameToCopayPlan = new Map<String, Copay_Plan__c>();

        List<Copay_Plan__c> listCopayPlans = [
            SELECT Id, Name
            FROM Copay_Plan__c
            WHERE Name IN :countryNameToMembershipNameMap.values()
        ];
       
        for (Copay_Plan__c cp : listCopayPlans) {
            copayPlanNameToCopayPlan.put(cp.Name, cp);
        }

        for (Opportunity opp : updatedOpportunities) {
            String mfName = countryNameToMembershipNameMap.get(opp.Account.BillingCountry);
            Copay_Plan__c cp = copayPlanNameToCopayPlan.get(mfName);
            
            if (cp != null) {
                oppIdToCopayPlanIdMap.put(opp.Id, cp.Id);
            }
        }

        return oppIdToCopayPlanIdMap;
    }
     
    private static List<QuoteLineItem> createQuoteLineItems(List<Quote> createdQuotes, Map<Id, Lead> idToLeadMap, Map<Id, Id> oppIdToLeadId) {
        List<QuoteLineItem> quoteLineItemsToInsert = new List<QuoteLineItem>();
        
        try {
            Map<Id, PricebookEntry> quoteIdToPricebookEntry = getPricebookEntryForQuotes(createdQuotes);        
            Set<Id> quoteIdSet = new Set<Id>();

            Map<String, String> countryNameToDescriptionTranslation = new Map<String, String>{
                'Brazil' => 'Colaborador',
                'United States' => 'Employee'
            };

            for (Quote q : createdQuotes) {
                quoteIdSet.add(q.Id);
            }

            List<Quote> selectedQuotes = [
                SELECT Id, Opportunity.Account.NumberOfEmployees, CurrencyIsoCode, Opportunity.Account.BillingCountry
                FROM Quote
                WHERE Id IN :quoteIdSet
            ];

            for (Quote quote : selectedQuotes) {
                if (quoteIdToPricebookEntry.get(quote.Id) != null) {
                    System.debug(quoteIdToPricebookEntry.get(quote.Id));
                    QuoteLineItem quoteLineItem = new QuoteLineItem();
                    quoteLineItem.QuoteId = quote.Id;
                    quoteLineItem.Quantity = (quote.Opportunity.Account.NumberOfEmployees <= 10 ? 10 : quote.Opportunity.Account.NumberOfEmployees);
                    quoteLineItem.PricebookEntryId = quoteIdToPricebookEntry.get(quote.Id).Id;
                    quoteLineItem.Product2Id = quoteIdToPricebookEntry.get(quote.Id).Product2Id;
                    quoteLineItem.Flagship_Date__c = Date.today();
                    quoteLineItem.Contract_Type__c = 'Flat_Fee';
                    quoteLineItem.Target_Enrollment_Rate__c = 100;
                    quoteLineItem.Type__c = 'Employee';
                    
                    Id leadId = oppIdToLeadId.get(quote.OpportunityId);
                    Lead lead = idToLeadMap.get(leadId);
                    PricebookEntry entry = quoteIdToPricebookEntry.get(quote.Id);

                    quoteLineItem.UnitPrice = entry.UnitPrice;
					quoteLineItem.Max_Total_Montly_Investment_Cofee__c = entry.UnitPrice;
                    quoteLineItem.Min_Total_Montly_Investment_Cofee__c = entry.UnitPrice;
                    
                    if (lead != null && lead.Proposal_Value_With_Discount__c != null && lead.Proposal_Value_Without_Discount__c != null) {
                        
                        if ((lead.Proposal_Value_With_Discount__c / lead.Proposal_Value_Without_Discount__c) <= 1) {
                            Double unitPrice = lead.Proposal_Value_With_Discount__c / quoteLineItem.Quantity;
                            quoteLineItem.UnitPrice = unitPrice;
    
                            Double discount = 1 - (lead.Proposal_Value_With_Discount__c / lead.Proposal_Value_Without_Discount__c);
                            quoteLineItem.Discount = discount;
                        }
                    }

                    String description = countryNameToDescriptionTranslation.get(quote.Opportunity.Account.BillingCountry);
                    quoteLineItem.Description = (description != null ? description : 'Employee');

                    quoteLineItemsToInsert.add(quoteLineItem);
                }
            }

            insert quoteLineItemsToInsert;

        } catch(Exception e) {
            createExceptionError(e);
        }
        
        return quoteLineItemsToInsert;
    }

    private static Map<Id, PricebookEntry> getPricebookEntryForQuotes(List<Quote> createdQuotes) {
        Map<Id, PricebookEntry> quoteIdToPricebookEntry = new Map<Id, PricebookEntry>();
        Map<Id, List<PricebookEntry>> pricebookIdToPricebookEntryList = new Map<Id, List<PricebookEntry>>();
        Set<Id> quoteIdSet = new Set<Id>();

        for (Quote quote : createdQuotes) {
            quoteIdSet.add(quote.Id);
        }
        
        List<Quote> selectedQuotes = [
            SELECT Id, Name, Pricebook2Id, Opportunity.Account.NumberOfemployees, Opportunity.Account.CurrencyIsoCode
            FROM Quote
            WHERE Id IN :quoteIdSet
            LIMIT 1
        ];
        
        List<PricebookEntry> pricebookEntries = [
            SELECT Id, CurrencyIsoCode, Pricebook2Id, Product2Id, UnitPrice
            FROM PricebookEntry
            WHERE
            IsActive = TRUE
            AND Product2.IsActive = TRUE
            AND Product2.Family = 'Enterprise Subscription'
            AND Pricebook2.IsStandard = TRUE
            AND Product2.Copay2__c = FALSE
            AND Product2.Family_Member_Included__c = FALSE
            AND Product2.Minimum_Number_of_Employees__c <= : selectedQuotes[0].Opportunity.Account.NumberOfEmployees
            AND Product2.Maximum_Number_of_Employees__c >= : selectedQuotes[0].Opportunity.Account.NumberOfEmployees
            AND CurrencyIsoCode = : selectedQuotes[0].Opportunity.Account.CurrencyIsoCode
            WITH SECURITY_ENFORCED
            LIMIT 1
        ];
        
        if (pricebookEntries.isEmpty()) {
            throw new IntegrationException(
                'No pricebook found based in the number of employees and the currency id'
            );
        }
        else{
            quoteIdToPricebookEntry.put(selectedQuotes[0].Id, pricebookEntries[0]);
        }

        return quoteIdToPricebookEntry;
    }

    private static void syncQuoteWithOpportunities(List<QuoteLineItem> createdQuoteLineItems, List<Quote> createdQuotes, List<Opportunity> updatedOpportunities) {
        Map<Id, Id> quoteLineItemIdToQuoteId = new Map<Id, Id>();
        Map<Id, Id> quoteIdToOpportunityId = new Map<Id, Id>();
        Map<Id, Opportunity> idToOpportunity = new Map<Id, Opportunity>();
        List<Opportunity> oppsToUpdate = new List<Opportunity>();

        for (QuoteLineItem lineItem : createdQuoteLineItems) {
            quoteLineItemIdToQuoteId.put(lineItem.Id, lineItem.QuoteId);
        } 

        for (Quote quote : createdQuotes) {
            quoteIdToOpportunityId.put(quote.Id, quote.OpportunityId);
        }

        for (Opportunity opp : updatedOpportunities) {
            idToOpportunity.put(opp.Id, opp);
        }
        
        for (Id lineItemId : quoteLineItemIdToQuoteId.keySet()) {
            Id quoteId = quoteLineItemIdToQuoteId.get(lineItemId);
            Id oppId = quoteIdToOpportunityId.get(quoteId);
            
            if (oppId != null && idToOpportunity.get(oppId) != null) {
                Opportunity opp = idToOpportunity.get(oppId);
                opp.SyncedQuoteId = quoteId;
                oppsToUpdate.add(opp);
            }
        }

        try {
            update oppsToUpdate;
        } catch(Exception e) {
            createExceptionError(e);
        }
        
    }

    private static void createExceptionError(Exception e) {
        Id recordTypeId = Schema.SObjectType.Exception_Error__c.getRecordTypeInfosByDeveloperName().get('Standart_Exception').getRecordTypeId();
        
        Exception_Error__c exceptionError = new Exception_Error__c();
        exceptionError.Exception_Message__c = e.getMessage();
        exceptionError.Exception_Type_Name__c = e.getTypeName();
        exceptionError.Stack_Trace__c = e.getStackTraceString();
        exceptionError.Line_Number__c = e.getLineNumber();
        exceptionError.RecordTypeId = recordTypeId;
        exceptionError.Type__c = 'SMB Lead Conversion';

        insert exceptionError;
    }
}