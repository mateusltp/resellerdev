@IsTest
public class TagusTransactionDataTest {
  @IsTest
  public static void execute() {
    TagusTransactionData transactionData = new TagusTransactionData(
      TagusTransactionData.SourceEventType.CRUD
    );

    DataMock dataMock = new DataMock();

    transactionData.setData(dataMock);

    System.assertEquals(
      dataMock,
      transactionData.getdata(),
      'Data was not set'
    );
  }

  private class DataMock {
    String fieldMock = 'valueMock';
  }
}