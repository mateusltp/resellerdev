/**
 * Created by gympasser on 30/03/2022.
 */

public with sharing class OpeningHourSelector extends ApplicationSelector {

    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
                Opening_Hours__c.Id,
                Opening_Hours__c.Name
        };
    }

    public Schema.SObjectType getSObjectType() {
        return Opening_Hours__c.sObjectType;
    }

    public List<Opening_Hours__c> selectById(Set<Id> ids) {
        return (List<Opening_Hours__c>) super.selectSObjectsById(ids);
    }

    public List<Opening_Hours__c> selectAllById(Set<Id> accountIds) {
        return (List<Opening_Hours__c>) Database.query(
                newQueryFactory().
                        selectFields(getAllFieldsFromOpeningHours()).
                        setCondition('Account_Related__c IN: accountIds' ).
                        toSOQL());
    }

    public Map<Id,Opening_Hours__c> selectAllByAccountId(Set<Id> accountIds) {
        Map<Id,Opening_Hours__c> response = new Map<Id,Opening_Hours__c>();
        for(Opening_Hours__c openHour : selectAllById(accountIds)){
            response.put(openHour.Account_Related__c, openHour);
        }
        
        return response;
    }


    private Set<String> getAllFieldsFromOpeningHours(){
        Map<String, Schema.SObjectField> openingHourFields = Schema.getGlobalDescribe().get('Opening_Hours__c').getDescribe().fields.getMap();
        return openingHourFields.keySet();
    }

}