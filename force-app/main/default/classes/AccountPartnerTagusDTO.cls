/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 09-23-2021
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/
public with sharing class AccountPartnerTagusDTO {
  
  private String id; 
	private String currency_id;
	private String country_id; 
  private String legal_document_type; 
	private String legal_document;
	private String legal_name;
	private String trade_name;
	private String salesforce_id;
	private String partner_type; // "ENUM(FULL_SERVICE, STUDIO, APP, PERSONAL_TRAINER, WELLNESS_COACH, PHYCOLOGIST, NUTRITIONIST, OTHER)"
  private Integer legacy_id; 
  private Addresses addresses;
  private List<PartnerContactDTO> contacts;

  public AccountPartnerTagusDTO(Account account) {
    this.id = account.UUID__c != null ? account.UUID__c : new Uuid().getValue();
    this.currency_id = account.CurrencyIsoCode;
    this.country_id = account.BillingCountryCode;
    this.legal_document_type = account.Legal_Document_Type__c;
    this.legal_document = String.valueOf(account.Id_Company__c);
    this.legal_name = account.Legal_Title__c;
    this.trade_name = account.Name;
    this.salesforce_id = account.Id;
    this.partner_type = 'FULL_SERVICE';//account.Gym_Type__c;
    this.legacy_id = Integer.valueof(account.CRM_ID__c);

    addresses = new Addresses(account);

    this.contacts = new List<PartnerContactDTO>();    

    for (AccountContactRelation contactRel : account.AccountContactRelations) {
      Contact aContact = new Contact(Id = contactRel.Contact.Id, 
                                    FirstName = contactRel.Contact.FirstName,
                                    LastName = contactRel.Contact.LastName,
                                    Email = contactRel.Contact.Email,
                                    Phone = contactRel.Contact.Phone,
                                    Type_of_Contact__c = contactRel.Contact.Type_of_Contact__c,
                                    UUID__c = contactRel.Contact.UUID__c
                                );
      this.contacts.add(new PartnerContactDTO(aContact));
    }
  }

    
  public String getId() {
    return this.id;
  }

  public Id getSalesforceId() {
    return this.salesforce_id;
  }

  public String getPartnerType() {
    return this.partner_type;
  }

  public void setPartnerType(String aPartnerType) {
    this.partner_type = aPartnerType;
  }

  public List<PartnerContactDTO> getContacts() {
    return this.contacts;
  }

 
  public static Map<Id, Account> getAccountMapWithFieldsToParse(  List<Account> accounts ) {      

    return ((AccountSelector)Application.Selector.newInstance(Account.SObjectType)).selectAccountMapWithFieldsToTagusParse( accounts );        

  }

  @TestVisible
  private class Addresses {
    private Billing billing;

    @TestVisible
    private Addresses(Account account){
      this.billing = new Billing(account);
    }
  }

  @TestVisible
  private class Billing {
    private String address;
    private String complements;
    private String city;
    private String district;
    private String state;
    private String postal_code;
    private String country_id;

    @TestVisible
    private Billing(Account account){
      this.address = account.BillingStreet;
      this.complements = null;
      this.city = account.BillingCity;
      this.district = '';
      this.state = account.BillingState;
      this.postal_code = account.BillingPostalCode;
      this.country_id = account.BillingCountryCode;
  }
}
  

  public class PartnerContactDTO {
    private String id;
    private String email;
    private String phone;
    private String first_name;
    private String last_name;
    private String sales_force_id;
    private List<String> roles = new List<String>();

    @TestVisible
    private PartnerContactDTO(Contact contact) {
      this.id = contact.UUID__c != null ? contact.UUID__c : new Uuid().getValue();
      this.sales_force_id = contact.id;
      this.email = contact.Email;
      this.first_name = contact.FirstName;
      this.last_name = contact.LastName;
      this.phone = contact.phone;
      this.roles.add('partners:owner');
    }

    public String getId() {
      return this.id;
    }

    public string getSalesforceId(){
      return this.sales_force_id;
    }
  }
}