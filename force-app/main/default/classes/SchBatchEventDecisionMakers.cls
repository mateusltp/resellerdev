/*
 * @author Bruno Pinho
 * @date January/2019
 * @description Schedulable class to run BatchEventDecisionMakers
 */
global class SchBatchEventDecisionMakers Implements Schedulable
    {
        global void execute(SchedulableContext sc)
        {
            scheduleBatchEventDecisionMakers();
        }

        public void scheduleBatchEventDecisionMakers()
        {
            Database.executeBatch(new BatchEventDecisionMakers());
        }
    }