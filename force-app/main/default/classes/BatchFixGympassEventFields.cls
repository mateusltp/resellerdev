global class BatchFixGympassEventFields implements Database.Batchable<sObject> {
    global Database.QueryLocator start(Database.BatchableContext BC) {
        if (!Test.isRunningTest())
            return Database.getQueryLocator([SELECT Id,
            									All_Day_Event__c,
            									Assigned_To__c,
            									Description__c,
            									Done__c,
            									End__c,
            									Event_Id__c,
            									Is_There_a_Decision_Maker__c,
            									Related_Account__c,
            									Scheduled_By__c,
            									Scheduled_Date__c,
            									Start__c,
            									Type__c,
            									Name
                                             FROM Gympass_Event__c
                                             WHERE Event_Id__c != NULL
                                             ORDER BY Event_Id__c ASC]);
        else
            return Database.getQueryLocator([SELECT Id,
            									All_Day_Event__c,
            									Assigned_To__c,
            									Description__c,
            									Done__c,
            									End__c,
            									Event_Id__c,
            									Is_There_a_Decision_Maker__c,
            									Related_Account__c,
            									Scheduled_By__c,
            									Scheduled_Date__c,
            									Start__c,
            									Type__c,
            									Name
                                             FROM Gympass_Event__c
                                             WHERE Event_Id__c != NULL
                                             ORDER BY Event_Id__c ASC
                                             LIMIT 200]);
    }

    global void execute(Database.BatchableContext BC, List<Gympass_Event__c> list_gympass_event) {
        Map<Id, Gympass_Event__c> map_gympass_event_to_update = new Map<Id, Gympass_Event__c>();
        Map<Id, Event> map_event_to_update = new Map<Id, Event>();
        Map<Id, Gympass_Event__c> map_gympass_event_to_delete = new Map<Id, Gympass_Event__c>();
        Map<Id, Gympass_Leader_Association__c> map_gympass_leader_association_to_insert = new Map<Id, Gympass_Leader_Association__c>();

        Set<String> set_event_id = new Set<String>();
        Set<String> set_gympass_event_id = new Set<String>();
        
        Map<Id, Event> map_event = new Map<Id,Event>();
        Map<Id, List<Gympass_Leader_Association__c>> map_gympass_event_association = new Map<Id, List<Gympass_Leader_Association__c>>();        
        Map<Id, Integer> map_gympass_event_count_association = new Map<Id, Integer>();
        Map<Id, List<Gympass_Event__c>> map_event_gympass_event = new Map<Id, List<Gympass_Event__c>>();
        Map<Id, Gympass_Leader_Association__c> map_gympass_leader_association_to_delete = new Map<Id,Gympass_Leader_Association__c>();
        
        for(Gympass_Event__c gympass_event : list_gympass_event) {
            set_event_id.add(gympass_event.Event_Id__c);
            set_gympass_event_id.add(gympass_event.Id);
        }
        
        List<Event> list_event = [SELECT Id,
        							IsAllDayEvent,
        							OwnerId,
        							Description,
        							Realizado__c,
        							EndDateTime,
        							AccountId,
        							CreatedById,
        							CreatedDate,
        							StartDateTime,
        							Type,
        							Subject,
        							Is_There_a_Decision_Maker__c
                                  FROM Event
                                  WHERE Id IN :set_event_id
                                  ORDER BY Id ASC];
        
        List<Gympass_Leader_Association__c> list_gympass_leader_association = [SELECT Gympass_Event__c,
        																		Gympass_Leader__c
                                                                             FROM Gympass_Leader_Association__c
                                                                             WHERE Gympass_Event__c IN :set_gympass_event_id
                                                                             ORDER BY Gympass_Event__c, Gympass_Leader__c ASC];
        
        for (Gympass_Leader_Association__c gympass_leader_association : list_gympass_leader_association) {
            if (map_gympass_event_association.get(gympass_leader_association.Gympass_Event__c) == null) {
                List<Gympass_Leader_Association__c> list_iteration_gympass_leader_association = new List<Gympass_Leader_Association__c>();
                list_iteration_gympass_leader_association.add(gympass_leader_association);
                
                map_gympass_event_association.put(gympass_leader_association.Gympass_Event__c, list_iteration_gympass_leader_association);
            } else {
                map_gympass_event_association.get(gympass_leader_association.Gympass_Event__c).add(gympass_leader_association);
            }
        }
        
        for (String gympass_event_id : map_gympass_event_association.keySet())
            map_gympass_event_count_association.put(gympass_event_id, map_gympass_event_association.get(gympass_event_id).size());
        
        for(Event event : list_event)
            map_event.put(event.Id, event);
        
        for (Gympass_Event__c gympass_event : list_gympass_event) {
            Boolean needs_to_update = FALSE;
            Event event = map_event.get(gympass_event.Event_Id__c);
            
            if (event != null) {
                if (map_event_gympass_event.get(event.Id) == null) {
                    List<Gympass_Event__c> list_gympass_event_iterate = new List<Gympass_Event__c>();
                    list_gympass_event_iterate.add(gympass_event);
                    
                    map_event_gympass_event.put(Event.Id, list_gympass_event_iterate);
                } else {
                    map_event_gympass_event.get(Event.Id).add(gympass_event);
                }

				if (event.IsAllDayEvent != gympass_event.All_Day_Event__c || event.OwnerId != gympass_event.Assigned_To__c || event.Description != gympass_event.Description__c || event.Realizado__c != gympass_event.Done__c || event.EndDateTime != gympass_event.End__c || event.AccountId != gympass_event.Related_Account__c || event.CreatedById != gympass_event.Scheduled_By__c || event.CreatedDate != gympass_event.Scheduled_Date__c || event.StartDateTime != gympass_event.Start__c || event.Type != gympass_event.Type__c || event.Is_There_a_Decision_Maker__c != gympass_event.Is_There_a_Decision_Maker__c) {
                    gympass_event.All_Day_Event__c = event.IsAllDayEvent;
                    gympass_event.Assigned_To__c = event.OwnerId;
                    gympass_event.Description__c = event.Description;
                    gympass_event.Done__c = event.Realizado__c;
                    gympass_event.End__c = event.EndDateTime;
                    gympass_event.Related_Account__c = event.AccountId;
                    gympass_event.Scheduled_By__c = event.CreatedById;
                    gympass_event.Scheduled_Date__c = event.CreatedDate;
                    gympass_event.Start__c = event.StartDateTime;
                    gympass_event.Type__c = event.Type;
                    gympass_event.Is_There_a_Decision_Maker__c = event.Is_There_a_Decision_Maker__c;

                    needs_to_update = TRUE;
				}

				if (String.isNotBlank(event.Subject)) {
					if (event.Subject.left(80) != gympass_event.Name) {
	                    gympass_event.Name = event.Subject.left(80);

	                    needs_to_update = TRUE;
					}
				}
                    
                if (needs_to_update)
                    map_gympass_event_to_update.put(gympass_event.Id, gympass_event);
            } else {
                map_gympass_event_to_delete.put(gympass_event.Id, gympass_event);
            }
            
            if (map_gympass_event_association.get(gympass_event.Id) == null)
            {
                Gympass_Leader_Association__c gympass_leader_association = new Gympass_Leader_Association__c();
                gympass_leader_association.Gympass_Event__c = gympass_event.Id;
                gympass_leader_association.Gympass_Leader__c = gympass_event.Assigned_To__c;

                map_gympass_leader_association_to_insert.put(gympass_leader_association.Id, gympass_leader_association);
            } else {
                Boolean owner_assigned = FALSE;
                String associated_users = '';
                
                for (Gympass_Leader_Association__c gympass_leader_association: map_gympass_event_association.get(gympass_event.Id)) {
                    if (gympass_leader_association.Gympass_Leader__c == gympass_event.Assigned_To__c)
                        owner_assigned = TRUE;
                    
                    if (associated_users.contains(gympass_leader_association.Gympass_Leader__c))
                        map_gympass_leader_association_to_delete.put(gympass_leader_association.Id, gympass_leader_association);
                    else
                        associated_users += gympass_leader_association.Gympass_Leader__c + ', ';
                }
                
                if (!owner_assigned) {
                    Gympass_Leader_Association__c gympass_leader_association = new Gympass_Leader_Association__c();
                    gympass_leader_association.Gympass_Event__c = gympass_event.Id;
                    gympass_leader_association.Gympass_Leader__c = gympass_event.Assigned_To__c;

                    map_gympass_leader_association_to_insert.put(gympass_leader_association.Id, gympass_leader_association);
                }
            }
        }
        
        for (String id_event : map_event_gympass_event.keySet()) {
            String gympass_event_with_more_assigneds = '';
            Integer count_assigneds = 0;
            
            if (map_event_gympass_event.get(id_event).size() > 1) {
                for (Gympass_Event__c gympass_event: map_event_gympass_event.get(id_event)) {
                    if (String.isBlank(gympass_event_with_more_assigneds)) {
                        gympass_event_with_more_assigneds = gympass_event.Id;
                        count_assigneds = map_gympass_event_count_association.get(gympass_event.Id);
                    } else if (map_gympass_event_count_association.get(gympass_event.Id) > count_assigneds) {
                        gympass_event_with_more_assigneds = gympass_event.Id;
                        count_assigneds = map_gympass_event_count_association.get(gympass_event.Id);
                    }
                }
                
                for (Gympass_Event__c gympass_event: map_event_gympass_event.get(id_event)) {
                    if (String.isNotBlank(gympass_event_with_more_assigneds) && gympass_event.Id != gympass_event_with_more_assigneds) {
                        Gympass_Event__c gympass_event_to_delete = new Gympass_Event__c(Id = gympass_event.Id);
                        map_gympass_event_to_delete.put(gympass_event_to_delete.Id, gympass_event_to_delete);
                    } else if (gympass_event.Id == gympass_event_with_more_assigneds) {
                        Event event = new Event(Id = id_event, Gympass_Event__c = gympass_event_with_more_assigneds);
                        map_event_to_update.put(event.Id, event);
                    }
                }
                
            }
        }
        
        for (String gympass_event_association : map_gympass_event_association.keySet()) {
            String associated_users = '';
            
            for (Gympass_Leader_Association__c gympass_leader_association : map_gympass_event_association.get(gympass_event_association)) {
                if (associated_users.contains(gympass_leader_association.Gympass_Leader__c))
                    map_gympass_leader_association_to_delete.put(gympass_leader_association.Id, gympass_leader_association);
                else
                    associated_users += gympass_leader_association.Gympass_Leader__c + ',';
            }
        }
        
        if (map_gympass_event_to_update.size() > 0)
            UPDATE map_gympass_event_to_update.values();
        
        if (map_gympass_leader_association_to_insert.size() > 0)
            INSERT map_gympass_leader_association_to_insert.values();
        
        if (map_gympass_leader_association_to_delete.values().size() > 0)
            DELETE map_gympass_leader_association_to_delete.values();
        
        if (map_gympass_event_to_delete.size() > 0)
            DELETE map_gympass_event_to_delete.values();
        
        if (map_event_to_update.size() > 0)
            UPDATE map_event_to_update.values();
    }

    global void finish(Database.BatchableContext BC) {
        if (!Test.isRunningTest()) {
            Datetime sysTime = System.now().addHours(6);
            String chron_exp = '' + sysTime.second() + ' ' + sysTime.minute() + ' ' + sysTime.hour() + ' ' + sysTime.day() + ' ' + sysTime.month() + ' ? ' + sysTime.year();
            String strName = 'HourlyBatchFixGympassEventFields ' + sysTime.day() + '/' + sysTime.month() + '/' + sysTime.year() + ' - '+ sysTime.hour() + ':' + sysTime.minute() + ':' + sysTime.second();
            System.schedule(strName, chron_exp, new SchBatchFixGympassEventFields());
        }
    }
}