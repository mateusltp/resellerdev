/**
 * @description       : 
 * @author            : roei@gft.com
 * @group             : 
 * @last modified on  : 02-23-2021
 * @last modified by  : roei@gft.com
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   02-03-2021   roei@gft.com   Initial Version
**/
public without sharing class EventTriggerHandler extends TriggerHandler {

    public override void beforeUpdate() {}
    
    public override void beforeDelete() {}
    
    public override void afterUpdate() {}

    public override void beforeInsert() {}

    public override void afterInsert() {
        new ActivityUpdateLeadStatus().getLeadEvents();
    }
}