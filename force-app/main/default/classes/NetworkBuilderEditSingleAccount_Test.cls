/**
 * Created by gympasser on 16/03/2022.
 */

@IsTest
public with sharing class NetworkBuilderEditSingleAccount_Test {

    @TestSetup
    private static void setup() {
        List<Account> accounts = new List<Account>();
        // Account without a contact (therefore also no Legal Representative)
        Account account = new Account(
                Name = 'Test Account 1 - Lisboa',
                GP_Status__c = 'Active',
                BillingCity = 'Lisboa',
                BillingCountry = 'Portugal'
        );
        insert account;

        // Account with a contact that will be defined as its legal representative
        Account account2 = new Account(
                Name = 'Test Account 2 - Porto',
                GP_Status__c = 'Active',
                BillingCity = 'Porto',
                BillingCountry = 'Portugal',
                ParentId = account.Id
        );

        insert account2;

        List<Contact> contacts = new List<Contact>();

        Contact contact = new Contact(
                FirstName = 'Johnny',
                LastName = ' Does',
                Type_of_contact__c = 'Legal Representative',
                AccountId = account.Id,
                Email = 'johnny@email.com',
                RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Gyms_Partner').getRecordTypeId()

        );
        contacts.add(contact);
        Contact contact2 = new Contact(
                FirstName = 'Jamie',
                LastName = ' Doesty',
                Type_of_contact__c = 'Decision Maker',
                AccountId = account2.Id,
                Email = 'jamie@email.com',
                RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Gyms_Partner').getRecordTypeId()

        );
        contacts.add(contact2);
        insert contacts;

    }

    @IsTest
    private static void getContactRelations_Test() {
        Account a = [SELECT Id FROM Account WHERE Name = 'Test Account 2 - Porto'];
        Test.startTest();
        List<AccountContactRelation> relations = NetworkBuilderEditSingleAccountCtrl.getContactRelations(a.Id);
        Test.stopTest();
        System.assertEquals(1, relations.size());
    }

    @IsTest
    private static void getContactsFromParentThatAreNotRelatedToChild_Test() {
        Account a = [SELECT Id, ParentId FROM Account WHERE Name = 'Test Account 2 - Porto'];

        Test.startTest();
        List<AccountContactRelation> relations = NetworkBuilderEditSingleAccountCtrl.getContactsFromParentThatAreNotRelatedToChild(a.Id, a.ParentId);
        Test.stopTest();
        System.assertEquals(1, relations.size());
    }

    @IsTest
    private static void getRecordTypeId_Test(){

        Id recordTypeIdPartner = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Gyms_Partner').getRecordTypeId();
        Test.startTest();
        Id recordTypeResult = NetworkBuilderEditSingleAccountCtrl.getRecordTypeId('Gyms_Partner');
        system.assert(recordTypeIdPartner == recordTypeResult);
        Test.stopTest();
    }

}