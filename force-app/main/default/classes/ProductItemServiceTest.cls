/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 05-17-2022
 * @last modified by  : alysson.mota@gympass.com
**/
@IsTest
public with sharing class ProductItemServiceTest {
        private static final PS_Constants constants = PS_Constants.getInstance();

    @TestSetup
    private static void createData(){
        Account accParent = getAccountInstance('Parent Account', null);
        insert accParent;
        
        Contact legalRepContact = getContact(accParent);
        insert legalRepContact;

        assignAccountLeglRep(accParent, legalRepContact);
        update accParent;
        
        Account accChild = getAccountInstance('Child Account', accParent.Id);
        insert accChild;

        Opportunity opp = getOpportunityInstance(accParent.Id);
        insert opp;

        List<Account_Opportunity_Relationship__c> aorLst = new List<Account_Opportunity_Relationship__c>();
        Account_Opportunity_Relationship__c aorParent   = getAorInstance(accParent.Id, opp.Id);
        Account_Opportunity_Relationship__c aorChild    = getAorInstance(accChild.Id, opp.Id);
        aorLst.add(aorParent);
        aorLst.add(aorChild);
        insert aorLst;

        List<Commercial_Condition__c> commCondLst = new List<Commercial_Condition__c>();
        Commercial_Condition__c parentCapCommCondition = getCapCommConditionInstance();
        Commercial_Condition__c childCapCommCondition  = getCapCommConditionInstance();
        commCondLst.add(parentCapCommCondition);
        commCondLst.add(childCapCommCondition);

        Commercial_Condition__c parentLateCancelCommCondition = getLateCancelCommConditionInstance();
        Commercial_Condition__c childLateCancelCommCondition  = getLateCancelCommConditionInstance();
        commCondLst.add(parentLateCancelCommCondition);
        commCondLst.add(childLateCancelCommCondition);

        Commercial_Condition__c parentNoShowCommCondition = getNoShowCommConditionInstance();
        Commercial_Condition__c childNoShowCommCondition  = getNoShowCommConditionInstance();
        commCondLst.add(parentNoShowCommCondition);
        commCondLst.add(childNoShowCommCondition);
        insert commCondLst;

        Product_Item__c parentProduct = getProductInstance(null, opp.Id);
        insert parentProduct;
        
        Product_Item__c childProduct = getProductInstance(parentProduct.Id, null);
        insert childProduct;
        

        List<Product_Assignment__c> paLst = new List<Product_Assignment__c>();
        Product_Assignment__c parentCapPa           = getProdAssignmentInstance(parentCapCommCondition.Id, parentProduct.Id);
        Product_Assignment__c parentLateCancelPa    = getProdAssignmentInstance(parentLateCancelCommCondition.Id, parentProduct.Id);
        Product_Assignment__c parentNoShowPa        = getProdAssignmentInstance(parentNoShowCommCondition.Id, parentProduct.Id);
        Product_Assignment__c childCapPa            = getProdAssignmentInstance(childCapCommCondition.Id, childProduct.Id);
        Product_Assignment__c childLateCancelPa     = getProdAssignmentInstance(childLateCancelCommCondition.Id, childProduct.Id);
        Product_Assignment__c childNoShowPa         = getProdAssignmentInstance(childNoShowCommCondition.Id, childProduct.Id);
        paLst.add(parentCapPa); 
        paLst.add(parentLateCancelPa); 
        paLst.add(parentNoShowPa); 
        paLst.add(childCapPa); 
        paLst.add(childLateCancelPa); 
        paLst.add(childNoShowPa); 
        insert paLst;

        List<Product_Opportunity_Assignment__c> poaLst = new List<Product_Opportunity_Assignment__c>();
        Product_Opportunity_Assignment__c parentCapPoa          = getPoaInstance(parentCapPa.Id, aorParent.Id);
        Product_Opportunity_Assignment__c parentLateCancelPoa   = getPoaInstance(parentLateCancelPa.Id, aorParent.Id);
        Product_Opportunity_Assignment__c parentNoShowPoa       = getPoaInstance(parentNoShowPa.Id, aorParent.Id);
        Product_Opportunity_Assignment__c childCapPoa           = getPoaInstance(childCapPa.Id, aorChild.Id);
        Product_Opportunity_Assignment__c childLateCancelPoa    = getPoaInstance(childLateCancelPa.Id, aorChild.Id);
        Product_Opportunity_Assignment__c childNoShowPoa        = getPoaInstance(childNoShowPa.Id, aorChild.Id);
        poaLst.add(parentCapPoa);
        poaLst.add(parentLateCancelPoa);
        poaLst.add(parentNoShowPoa);
        poaLst.add(childCapPoa);
        poaLst.add(childLateCancelPoa);
        poaLst.add(childNoShowPoa);
        insert poaLst;

        List<Threshold__c> thLst = new List<Threshold__c>();
        Threshold__c th1 = getThresholdInstance();
        Threshold__c th2 = getThresholdInstance();
        thLst.add(th1);
        thLst.add(th2);
        insert thLst;

        List<Product_Threshold_Member__c> ptmLst = new List<Product_Threshold_Member__c>();
        Product_Threshold_Member__c ptm1 = getThresholdMember(th1, parentProduct);
        Product_Threshold_Member__c ptm2 = getThresholdMember(th2, parentProduct);
        ptmLst.add(ptm1);
        ptmLst.add(ptm2);
        insert ptmLst;

        
        List<Gym_Activity_Relationship__c> garLst = new List<Gym_Activity_Relationship__c>();
        Gym_Activity_Relationship__c gar1 = getGymActivityRelInstance(); 
        Gym_Activity_Relationship__c gar2 = getGymActivityRelInstance();
        garLst.add(gar1);
        garLst.add(gar2);
        insert garLst;

        List<Product_Activity_Relationship__c> parLst = new List<Product_Activity_Relationship__c>();
        Product_Activity_Relationship__c par1 = getPrdActivityRelInstance(gar1, parentProduct);
        Product_Activity_Relationship__c par2 = getPrdActivityRelInstance(gar2, parentProduct);
        parLst.add(par1);
        parLst.add(par2);
        insert parLst;
    }

        @IsTest
        private static void saveProduct_Partner_UnitTest(){ 
            Id productFlowRecordTypeId = Schema.SObjectType.Product_Item__c.getRecordTypeInfosByDeveloperName().get(constants.PRODUCT_ITEM_RT_PARTNER_FLOW).getRecordTypeId();
            
            fflib_ApexMocks mocks = new fflib_ApexMocks();
            fflib_SObjectUnitOfWork mockUOW = (fflib_SObjectUnitOfWork)mocks.mock(fflib_SObjectUnitOfWork.class);
            AccountOpportunitySelector mockAccountOpportunitySelector = (AccountOpportunitySelector) mocks.mock(AccountOpportunitySelector.class); 
            Id mockOppId = fflib_IDGenerator.generate(Opportunity.SObjectType);
            Id mockAccId = fflib_IDGenerator.generate(Account.SObjectType);        
            Id mockOppMemberId = fflib_IDGenerator.generate(Account_Opportunity_Relationship__c.SObjectType);
            Id mockGymActivity = fflib_IDGenerator.generate(Gym_Activity_Relationship__c.SObjectType);            
            
            Account_Opportunity_Relationship__c mockOppMember = new Account_Opportunity_Relationship__c( Id = mockOppMemberId, Opportunity__c = mockOppId, Account__c = mockAccId);
            Map<Id, Account_Opportunity_Relationship__c> MockOppMemberResponse = new Map<Id, Account_Opportunity_Relationship__c>{mockOppMember.Id => mockOppMember};
            Product_Item__c aProdItem = new Product_Item__c(recordTypeId=productFlowRecordTypeId);
            Commercial_Condition__c aCommercial = new Commercial_Condition__c();
            Threshold__c aThreshold = new Threshold__c();
    
            mocks.startStubbing();    
                mocks.when(mockAccountOpportunitySelector.sobjectType()).thenReturn(Account_Opportunity_Relationship__c.sObjectType);
                mocks.when(mockAccountOpportunitySelector.selectExistingOpportunityMemberByAccountId(new Set<Id>{mockAccId})).thenReturn(MockOppMemberResponse);
            mocks.stopStubbing();
    
            Application.UnitOfWork.setMock(mockUOW);
            Application.Selector.setMock(mockAccountOpportunitySelector);
            Test.startTest();
                //Product_Item__c prod = ProductItemService.createNewProduct(mockOppId, mockAccId, aProdItem, new Set<Id>{mockGymActivity}, new List<Commercial_Condition__c>{aCommercial},  new List<Threshold__c>{aThreshold});
                List<Commercial_Condition__c> commConditionLst = new List<Commercial_Condition__c>{aCommercial};
                Map<String, List<Commercial_Condition__c>> commercialConditionsMap = new Map<String, List<Commercial_Condition__c>>{'INSERT' => commConditionLst};
                Product_Item__c prod = ProductItemService.createNewProduct(mockOppId, mockAccId, aProdItem, new Set<Id>{mockGymActivity}, commercialConditionsMap,  new List<Threshold__c>{aThreshold});
                System.assert( prod != null );
            Test.stopTest();
        }


        @IsTest
        private static void saveProduct_Partner_ERROR_UnitTest(){ 
            Id productFlowRecordTypeId = Schema.SObjectType.Product_Item__c.getRecordTypeInfosByDeveloperName().get(constants.PRODUCT_ITEM_RT_PARTNER_FLOW).getRecordTypeId();
            
            fflib_ApexMocks mocks = new fflib_ApexMocks();
            fflib_SObjectUnitOfWork mockUOW = (fflib_SObjectUnitOfWork)mocks.mock(fflib_SObjectUnitOfWork.class);
            AccountOpportunitySelector mockAccountOpportunitySelector = (AccountOpportunitySelector) mocks.mock(AccountOpportunitySelector.class); 
            Id mockOppId = fflib_IDGenerator.generate(Opportunity.SObjectType);
            Id mockAccId = fflib_IDGenerator.generate(Account.SObjectType);        
            Id mockOppMemberId = fflib_IDGenerator.generate(Account_Opportunity_Relationship__c.SObjectType);
            Id mockGymActivity = fflib_IDGenerator.generate(Gym_Activity_Relationship__c.SObjectType);            
            
            Account_Opportunity_Relationship__c mockOppMember = new Account_Opportunity_Relationship__c( Id = mockOppMemberId, Opportunity__c = mockOppId, Account__c = mockAccId);
            Map<Id, Account_Opportunity_Relationship__c> MockOppMemberResponse = new Map<Id, Account_Opportunity_Relationship__c>{mockOppMember.Id => mockOppMember};
            Product_Item__c aProdItem = new Product_Item__c(recordTypeId=productFlowRecordTypeId);
            Commercial_Condition__c aCommercial = new Commercial_Condition__c();
            Threshold__c aThreshold = new Threshold__c();
    
            mocks.startStubbing();    
                mocks.when(mockAccountOpportunitySelector.sobjectType()).thenReturn(Account_Opportunity_Relationship__c.sObjectType);
                mocks.when(mockAccountOpportunitySelector.selectExistingOpportunityMemberByAccountId(new Set<Id>{mockAccId})).thenReturn(MockOppMemberResponse);
                ((fflib_SObjectUnitOfWork)mocks.doThrowWhen(new ProductItemPartnerServiceImpl.ProductItemPartnerServiceImplException(), mockUOW)).commitWork(); //for void returns  
  
            mocks.stopStubbing();
    
            Application.UnitOfWork.setMock(mockUOW);
            Application.Selector.setMock(mockAccountOpportunitySelector);

            try {
                Test.startTest();
                //Product_Item__c prod = ProductItemService.createNewProduct(mockOppId, mockAccId, aProdItem, new Set<Id>{mockGymActivity}, new List<Commercial_Condition__c>{aCommercial},  new List<Threshold__c>{aThreshold});
                List<Commercial_Condition__c> commConditionLst = new List<Commercial_Condition__c>{aCommercial};
                Map<String, List<Commercial_Condition__c>> commercialConditionsMap = new Map<String, List<Commercial_Condition__c>>{'INSERT' => commConditionLst};
                Product_Item__c prod = ProductItemService.createNewProduct(mockOppId, mockAccId, aProdItem, new Set<Id>{mockGymActivity}, commercialConditionsMap,  new List<Threshold__c>{aThreshold});
                Test.stopTest();
            } catch (Exception e){
                System.assert(e instanceof ProductItemService.ProductItemServiceException);
            }  

        }
     @IsTest
        private static void deleteProduct_Partner_ERROR_UnitTest2(){ 
            Id productFlowRecordTypeId = Schema.SObjectType.Product_Item__c.getRecordTypeInfosByDeveloperName().get(constants.PRODUCT_ITEM_RT_PARTNER_FLOW).getRecordTypeId();
            
            fflib_ApexMocks mocks = new fflib_ApexMocks();
            fflib_SObjectUnitOfWork mockUOW = (fflib_SObjectUnitOfWork)mocks.mock(fflib_SObjectUnitOfWork.class);
            AccountOpportunitySelector mockAccountOpportunitySelector = (AccountOpportunitySelector) mocks.mock(AccountOpportunitySelector.class); 
            Id mockOppId = fflib_IDGenerator.generate(Opportunity.SObjectType);
            Id mockAccId = fflib_IDGenerator.generate(Account.SObjectType);        
            Id mockOppMemberId = fflib_IDGenerator.generate(Account_Opportunity_Relationship__c.SObjectType);
            Id mockGymActivity = fflib_IDGenerator.generate(Gym_Activity_Relationship__c.SObjectType);            
            
            Account_Opportunity_Relationship__c mockOppMember = new Account_Opportunity_Relationship__c( Id = mockOppMemberId, Opportunity__c = mockOppId, Account__c = mockAccId);
            Map<Id, Account_Opportunity_Relationship__c> MockOppMemberResponse = new Map<Id, Account_Opportunity_Relationship__c>{mockOppMember.Id => mockOppMember};
            Product_Item__c aProdItem = new Product_Item__c();
            Commercial_Condition__c aCommercial = new Commercial_Condition__c();
            Threshold__c aThreshold = new Threshold__c();
    
            mocks.startStubbing();    
                mocks.when(mockAccountOpportunitySelector.sobjectType()).thenReturn(Account_Opportunity_Relationship__c.sObjectType);
                mocks.when(mockAccountOpportunitySelector.selectExistingOpportunityMemberByAccountId(new Set<Id>{mockAccId})).thenReturn(MockOppMemberResponse);
                ((fflib_SObjectUnitOfWork)mocks.doThrowWhen(new ProductItemPartnerServiceImpl.ProductItemPartnerServiceImplException(), mockUOW)).commitWork(); //for void returns  
  
            mocks.stopStubbing();
    
            Application.UnitOfWork.setMock(mockUOW);
            Application.Selector.setMock(mockAccountOpportunitySelector);

            try {
                Test.startTest();
                List<Commercial_Condition__c> commConditionLst = new List<Commercial_Condition__c>{aCommercial};
                ProductItemService.deleteProductRelationships(aProdItem,commConditionLst);
                Test.stopTest();
            } catch (Exception e){
                System.assert(e instanceof ProductItemService.ProductItemServiceException);
            }  
        }
    
        @IsTest
        private static void deleteProduct_Partner_SUCCESS_UnitTest2(){ 
            Id productFlowRecordTypeId = Schema.SObjectType.Product_Item__c.getRecordTypeInfosByDeveloperName().get(constants.PRODUCT_ITEM_RT_PARTNER_FLOW).getRecordTypeId();
            
            fflib_ApexMocks mocks = new fflib_ApexMocks();
            fflib_SObjectUnitOfWork mockUOW = (fflib_SObjectUnitOfWork)mocks.mock(fflib_SObjectUnitOfWork.class);
            AccountOpportunitySelector mockAccountOpportunitySelector = (AccountOpportunitySelector) mocks.mock(AccountOpportunitySelector.class); 
            Id mockOppId = fflib_IDGenerator.generate(Opportunity.SObjectType);
            Id mockAccId = fflib_IDGenerator.generate(Account.SObjectType);        
            Id mockOppMemberId = fflib_IDGenerator.generate(Account_Opportunity_Relationship__c.SObjectType);
            Id mockGymActivity = fflib_IDGenerator.generate(Gym_Activity_Relationship__c.SObjectType);            
            
            Account_Opportunity_Relationship__c mockOppMember = new Account_Opportunity_Relationship__c( Id = mockOppMemberId, Opportunity__c = mockOppId, Account__c = mockAccId);
            Map<Id, Account_Opportunity_Relationship__c> MockOppMemberResponse = new Map<Id, Account_Opportunity_Relationship__c>{mockOppMember.Id => mockOppMember};
            
            Id prodRtId = Schema.SObjectType.Product_Item__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product').getRecordTypeId();
            Product_Item__c aProdItem = new Product_Item__c();
            aProdItem.RecordTypeId = prodRtId;
            
            Commercial_Condition__c aCommercial = new Commercial_Condition__c();
            Threshold__c aThreshold = new Threshold__c();
    
            mocks.startStubbing();    
                mocks.when(mockAccountOpportunitySelector.sobjectType()).thenReturn(Account_Opportunity_Relationship__c.sObjectType);
                mocks.when(mockAccountOpportunitySelector.selectExistingOpportunityMemberByAccountId(new Set<Id>{mockAccId})).thenReturn(MockOppMemberResponse);
                ((fflib_SObjectUnitOfWork)mocks.doThrowWhen(new ProductItemPartnerServiceImpl.ProductItemPartnerServiceImplException(), mockUOW)).commitWork(); //for void returns  
  
            mocks.stopStubbing();
    
            Application.UnitOfWork.setMock(mockUOW);
            Application.Selector.setMock(mockAccountOpportunitySelector);

            try {
                Test.startTest();
                List<Commercial_Condition__c> commConditionLst = new List<Commercial_Condition__c>{aCommercial};
                ProductItemService.deleteProductRelationships(aProdItem,commConditionLst);
                Test.stopTest();
            } catch (Exception e){
                System.assert(e instanceof ProductItemService.ProductItemServiceException);
            }  
        }
        
     @IsTest
        private static void saveProduct_ErrorRT_Partner_UnitTest(){ 
            Id productFlowRecordTypeId = Schema.SObjectType.Product_Item__c.getRecordTypeInfosByDeveloperName().get(constants.PRODUCT_ITEM_CHILD_RT_PARTNER_FLOW).getRecordTypeId();
            
            fflib_ApexMocks mocks = new fflib_ApexMocks();
            fflib_SObjectUnitOfWork mockUOW = (fflib_SObjectUnitOfWork)mocks.mock(fflib_SObjectUnitOfWork.class);
            AccountOpportunitySelector mockAccountOpportunitySelector = (AccountOpportunitySelector) mocks.mock(AccountOpportunitySelector.class); 
            Id mockOppId = fflib_IDGenerator.generate(Opportunity.SObjectType);
            Id mockAccId = fflib_IDGenerator.generate(Account.SObjectType);        
            Id mockOppMemberId = fflib_IDGenerator.generate(Account_Opportunity_Relationship__c.SObjectType);
            Id mockGymActivity = fflib_IDGenerator.generate(Gym_Activity_Relationship__c.SObjectType);            
            
            Account_Opportunity_Relationship__c mockOppMember = new Account_Opportunity_Relationship__c( Id = mockOppMemberId, Opportunity__c = mockOppId, Account__c = mockAccId);
            Map<Id, Account_Opportunity_Relationship__c> MockOppMemberResponse = new Map<Id, Account_Opportunity_Relationship__c>{mockOppMember.Id => mockOppMember};
            Product_Item__c aProdItem = new Product_Item__c(recordTypeId=productFlowRecordTypeId);
            Commercial_Condition__c aCommercial = new Commercial_Condition__c();
            Threshold__c aThreshold = new Threshold__c();
    
            mocks.startStubbing();    
                mocks.when(mockAccountOpportunitySelector.sobjectType()).thenReturn(Account_Opportunity_Relationship__c.sObjectType);
                mocks.when(mockAccountOpportunitySelector.selectExistingOpportunityMemberByAccountId(new Set<Id>{mockAccId})).thenReturn(MockOppMemberResponse);
            mocks.stopStubbing();
    
            Application.UnitOfWork.setMock(mockUOW);
            Application.Selector.setMock(mockAccountOpportunitySelector);
             try {
                Test.startTest();
                List<Commercial_Condition__c> commConditionLst = new List<Commercial_Condition__c>{aCommercial};
                Map<String, List<Commercial_Condition__c>> commercialConditionsMap = new Map<String, List<Commercial_Condition__c>>{'INSERT' => commConditionLst};
                Product_Item__c prod = ProductItemService.createNewProduct(mockOppId, mockAccId, aProdItem, new Set<Id>{mockGymActivity}, commercialConditionsMap,  new List<Threshold__c>{aThreshold});
                System.assert( prod != null );
                Test.stopTest();
            } catch (Exception e){
                System.assert(e instanceof ProductItemService.ProductItemServiceException);
            }
        }
    
    @isTest
    private static void deleteProductsAndRelationships_Partner_UnitTest() {
        List<Product_Item__c> parentProducts = [ SELECT Id FROM Product_Item__c WHERE Parent_Product__c = null ];

        Test.startTest();
        delete parentProducts;

        List<Product_Item__c> prodLst = [SELECT Id FROM Product_Item__c];
        List<Product_Opportunity_Assignment__c> poaLst = [SELECT Id FROM Product_Opportunity_Assignment__c];
        List<Product_Assignment__c> paLst = [SELECT Id FROM Product_Assignment__c];
        List<Commercial_Condition__c> commCondLst = [SELECT Id FROM Commercial_Condition__c];
        List<Product_Threshold_Member__c> ptmLst = [SELECT Id FROM Product_Threshold_Member__c];
        List<Threshold__c> thresholdLst = [SELECT Id FROM Threshold__c];
        List<Product_Activity_Relationship__c> parLst = [SELECT Id FROM Product_Activity_Relationship__c];

        System.assertEquals(0, prodLst.size());
        System.assertEquals(0, poaLst.size());
        System.assertEquals(0, paLst.size());
        System.assertEquals(0, commCondLst.size());
        System.assertEquals(0, ptmLst.size());
        System.assertEquals(0, thresholdLst.size());
        System.assertEquals(0, parLst.size());
        Test.stopTest();
    } 

    
    // Methods for TestSetup
    
    private static Id getPartnerFlowAccRecTypeId() {
        return Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Account').getRecordTypeId();
    }

    private static Id getPartnerFlowOppRecTypeId() {
        return Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Opportunity').getRecordTypeId();
    }

    private static Id getOppStepRecTypeId() {
        return Schema.SObjectType.Step_Towards_Success_Partner__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Opportunity_Step').getRecordTypeId();
    }

    private static Id getAccStepRecTypeId() {
        return Schema.SObjectType.Step_Towards_Success_Partner__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Account_Step').getRecordTypeId();
    }

    private static Account getAccountInstance(String name, Id parentId) {
        Id recordTypeId = getPartnerFlowAccRecTypeId();

        Account acc = new Account();
        acc.Name = name;
        acc.RecordTypeId = recordTypeId;
        acc.ParentId = parentId;
        acc.BillingCountry = 'United States';
        acc.BillingCity = 'New York';
        acc.BillingStreet = 'Av. Test';
        acc.Exclusivity__c = 'Yes';

        return acc;
    }

    private static Opportunity getOpportunityInstance(Id accountId) {
        Id recordTypeId = getPartnerFlowOppRecTypeId();
        
        Opportunity opp = new Opportunity();
        opp.RecordTypeId = recordTypeId;
        opp.Name = 'My Opp';
        opp.CloseDate = System.now().date();
        opp.AccountId = accountId;
        opp.StageName = 'Qualificação';

        return opp;
    }

    private static Account_Opportunity_Relationship__c getAorInstance(Id accountId, Id oppId) {
        Id recordTypeId = Schema.SObjectType.Account_Opportunity_Relationship__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Opportunity_Member').getRecordTypeId();

        Account_Opportunity_Relationship__c aor = new Account_Opportunity_Relationship__c();
        aor.Account__c = accountId;
        aor.Opportunity__c = oppId;
        aor.RecordTypeId = recordTypeId;

        return aor;
    }

    private static Commercial_Condition__c getCapCommConditionInstance() {
        Id recordTypeId = Schema.SObjectType.Commercial_Condition__c.getRecordTypeInfosByDeveloperName().get('CAP').getRecordTypeId();

        Commercial_Condition__c capCommCondition = new Commercial_Condition__c();
        capCommCondition.RecordTypeId = recordTypeId;
        capCommCondition.CAP_Discount__c = 0;
        capCommCondition.Visits_to_CAP__c = 100;

        return capCommCondition;
    }

    private static Commercial_Condition__c getLateCancelCommConditionInstance() {
        Id recordTypeId = Schema.SObjectType.Commercial_Condition__c.getRecordTypeInfosByDeveloperName().get('Late_Cancellation').getRecordTypeId();

        Commercial_Condition__c capCommCondition = new Commercial_Condition__c();
        capCommCondition.RecordTypeId = recordTypeId;

        return capCommCondition;
    }

    private static Commercial_Condition__c getNoShowCommConditionInstance() {
        Id recordTypeId = Schema.SObjectType.Commercial_Condition__c.getRecordTypeInfosByDeveloperName().get('No_Show').getRecordTypeId();

        Commercial_Condition__c capCommCondition = new Commercial_Condition__c();
        capCommCondition.RecordTypeId = recordTypeId;

        return capCommCondition;
    }

    private static Product_Item__c getProductInstance(Id parentProductId, Id oppId) {
        Id recordTypeId = Schema.SObjectType.Product_Item__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product').getRecordTypeId();

        Product_Item__c product = new Product_Item__c();
        product.Name = 'Boxing';
        product.RecordTypeId = recordTypeId;
        product.Parent_Product__c = parentProductId;
        product.Opportunity__c = oppId;
        product.Price_Visits_Month_Package_Selected__c = 100;

        return product;
    }

    private static Product_Assignment__c getProdAssignmentInstance(Id capCommConditionId, Id productId) {
        Id recordTypeId = Schema.SObjectType.Product_Assignment__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product_Assignment').getRecordTypeId();
        
        Product_Assignment__c prodAssignment = new Product_Assignment__c();
        prodAssignment.RecordTypeId = recordTypeId;
        prodAssignment.CommercialConditionId__c = capCommConditionId;
        prodAssignment.ProductId__c = productId;
        prodAssignment.Market_Price__c = 80;

        return prodAssignment;
    }

    private static Product_Opportunity_Assignment__c getPoaInstance(Id paId, Id aorId) {
        Id recordTypeId = Schema.SObjectType.Product_Opportunity_Assignment__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product_Opportunity_Assignment').getRecordTypeId();
        
        Product_Opportunity_Assignment__c poa = new Product_Opportunity_Assignment__c();
        poa.RecordTypeId = recordTypeId;
        poa.ProductAssignmentId__c = paId;
        poa.OpportunityMemberId__c = aorId;

        return poa;
    }

    private static Contact getContact(Account acc) {
        Id recordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Gyms_Partner').getRecordTypeId();
        
        Contact legalRep = new Contact();
        legalRep.RecordTypeId = recordTypeId;
        legalRep.AccountId = acc.Id;
        legalRep.Email = 'legalRepContact@test.com';
        legalRep.LastName = 'Legal';

        return legalRep;
    }

    private static Threshold__c getThresholdInstance() {
        Id recordTypeId = Schema.SObjectType.Threshold__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Treshold').getRecordTypeId();
        
        Threshold__c th = new Threshold__c();
        th.Threshold_value_start__c = 100;
        th.Discount__c = 15;

        return th;
    }

    private static Product_Threshold_Member__c getThresholdMember(Threshold__c th, Product_Item__c product) {
        Product_Threshold_Member__c ptm = new Product_Threshold_Member__c();
        ptm.Product__c = product.Id;
        ptm.Threshold__c = th.Id;

        return ptm;
    }

    private static Gym_Activity_Relationship__c getGymActivityRelInstance() {
        Gym_Activity_Relationship__c gar = new Gym_Activity_Relationship__c();
        gar.Name = 'Boxing';

        return gar;
    }

    private static Product_Activity_Relationship__c getPrdActivityRelInstance(Gym_Activity_Relationship__c gar, Product_Item__c product) {
        Product_Activity_Relationship__c par = new Product_Activity_Relationship__c();
        par.Name = 'PAR';
        par.Gym_Activity_Relationship__c = gar.Id;
        par.Product__c = product.Id;

        return par;
    }

    private static void assignAccountLeglRep(Account accParent, Contact legalRepContact) {
        accParent.Legal_Representative__c = legalRepContact.Id;
    }
}