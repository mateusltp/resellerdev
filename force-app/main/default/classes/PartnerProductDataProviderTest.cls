/**
 * @description       : 
 * @author            : bruno.mendes@gympass.com
 * @group             : 
 * @last modified on  : 26/04/2022
 * @last modified by  : bruno.mendes@gympass.com
**/

@IsTest
public with sharing class PartnerProductDataProviderTest {

    private static final PS_Constants constants = PS_Constants.getInstance();

    @TestSetup
    private static void setup() {
        Account lAcc = PartnerDataFactory.newAccount();
        Database.insert( lAcc );

        Opportunity lOpp = PartnerDataFactory.newOpportunity( lAcc.Id, 'Partner_Flow_Opportunity' );
        Database.insert( lOpp );
    }

    @IsTest
    private static void getData_Test() {
        Id opportunityPartnerFlowRT = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(constants.OPPORTUNITY_RT_PARTNER_FLOW_OPP).getRecordTypeId();
        Id opportunityChildPartnerFlowRT = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(constants.OPPORTUNITY_RT_PARTNER_FLOW_CHILD_OPP).getRecordTypeId();



        fflib_ApexMocks mocks = new fflib_ApexMocks();
        Id mockAccountId = fflib_IDGenerator.generate(Account.SObjectType);
        AccountOpportunitySelector mockAccountOpportunitySelector = (AccountOpportunitySelector)mocks.mock(AccountOpportunitySelector.class);
        OpportunitySelector mockOpportunitySelector = (OpportunitySelector)mocks.mock(OpportunitySelector.class);
        Id mockOppId = fflib_IDGenerator.generate(Opportunity.SObjectType);
        Opportunity mockOpportunity = new Opportunity(Id = mockOppId, AccountId = mockAccountId, RecordTypeId = opportunityPartnerFlowRT);
        List<Opportunity> mockOpportunities = new List<Opportunity>{mockOpportunity};
        Id mockOppMemberId = fflib_IDGenerator.generate(Account_Opportunity_Relationship__c.SObjectType);
        Id mockOppChild = fflib_IDGenerator.generate(Opportunity.SObjectType);

        Id mockProductItemId = fflib_IDGenerator.generate(Product_Item__c.SObjectType);

        Id mockProdAssignmentId = fflib_IDGenerator.generate(Product_Assignment__c.SObjectType);

        Id mockProdOppAssignmentId = fflib_IDGenerator.generate(Product_Opportunity_Assignment__c.SObjectType);
        Product_Opportunity_Assignment__c mockProdOppAssignment = new Product_Opportunity_Assignment__c(Id = mockProdOppAssignmentId, OpportunityMemberId__c = mockOppMemberId, ProductAssignmentId__c = mockProdAssignmentId);
        Map<Id, List<Product_Opportunity_Assignment__c>> mockProdOPpAssignmentMap = new Map<Id, List<Product_Opportunity_Assignment__c>> {mockOppId => new List<Product_Opportunity_Assignment__c>{mockProdOppAssignment}};

        Account_Opportunity_Relationship__c mockOppMember = new Account_Opportunity_Relationship__c( Id = mockOppMemberId, Opportunity__c = mockOppId, Account__c = mockAccountId);
        List<Account_Opportunity_Relationship__c> mockOppMemberResponse = new List<Account_Opportunity_Relationship__c>{mockOppMember};
        List<Opportunity> mockOpportunitiesResponse = (List<Opportunity>) fflib_ApexMocksUtils.makeRelationship(
                List<Opportunity>.class,
                mockOpportunities,
                Opportunity.MasterOpportunity__c,
                new List<List<Opportunity>> {
                        new List<Opportunity>{
                                new Opportunity(Id = mockOppChild, Name = 'Test Opp Child', StageName = 'Waiting Cancellation', RecordTypeId = opportunityChildPartnerFlowRT)
                        }
                }
        );



        mocks.startStubbing();
        mocks.when(mockAccountOpportunitySelector.sObjectType()).thenReturn(Account_Opportunity_Relationship__c.SObjectType);
        mocks.when(mockAccountOpportunitySelector.selectProductAssignmentToOpportunitySDGGrid(new Set<Id> {mockOppId})).thenReturn(mockProdOPpAssignmentMap);

        mocks.when(mockOpportunitySelector.sObjectType()).thenReturn(Opportunity.SObjectType);
        mocks.when(mockOpportunitySelector.selectOpportunityAndChildOppsById(new Set<Id> {mockOppId})).thenReturn(mockOpportunitiesResponse);

        mocks.when(mockOpportunitySelector.selectOpportunityForPartnerProductFlow(new Set<Id> {mockOppId})).thenReturn(mockOpportunities);
        mocks.stopStubbing();

        Application.Selector.setMock(mockAccountOpportunitySelector);
        Application.Selector.setMock(mockOpportunitySelector);

        Test.startTest();
        // instantiate class
        PartnerProductDataProvider dataProvider = new PartnerProductDataProvider();
        sortablegrid.SDG coreSDG = dataProvider.LoadSDG('', mockOppId);

        // create the user request
        sortablegrid.SDGRequest request = new sortablegrid.SDGRequest();
        request.ParentRecordID = mockOppId;
        request.PageID = 1;
        request.PageSize = 10;
        request.FieldSetName=null;
        request.RelationshipName=null;
        request.SDGTag='Apex:PartnerProductDataProvider';



        sortablegrid.SDGResult result = PartnerProductDataProvider.getData(coreSDG, request);
        system.assertNotEquals(null, result);
        Test.stopTest();
    }

    @IsTest
    static void isUserSelectableTest(){

        Boolean isUserSelectable = true;
        System.assertEquals(isUserSelectable, PartnerProductDataProvider.isUserSelectable());
    }
    
}