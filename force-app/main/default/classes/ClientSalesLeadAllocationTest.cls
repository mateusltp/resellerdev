/**
 * @description       : 
 * @author            : roei@gft.com
 * @group             : 
 * @last modified on  : 01-20-2021
 * @last modified by  : roei@gft.com
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   01-20-2021   roei@gft.com   Initial Version
**/
@isTest(seeAllData=false)
public with sharing class ClientSalesLeadAllocationTest {
	
    @TestSetup
    static void createData(){
        Profile lStandardProfile = [ SELECT Id FROM Profile WHERE Name = 'Standard User' ];
        User lTestUser = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',                          
            LocaleSidKey = 'en_US', TimeZoneSidKey = 'America/Los_Angeles',
            ProfileId = lStandardProfile.Id, UserName='standardUserLead@testorg.com');
        
        Database.insert( lTestUser );
        
        Account lAcc = generateAccount( 'Empresas' , lTestUser.Id );              
        Database.insert( lAcc );
        
        Contact lContact = new Contact();
        lContact.AccountId = lAcc.Id;
        lContact.LastName = 'Test';
        lContact.FirstName = 'Contact';
        lContact.Email = 'contact@test.com';
        Database.insert( lContact );
    }
    
    @isTest
    public static void shouldAssignSalesLeadToBDR() {
    	Lead lNewLead = new Lead(
            LastName='Sales Lead',
            RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Client').getRecordTypeId(), 
            Country = 'Brazil',
            State = 'São Paulo',
            Street = 'Av Paulista 283',
            City = 'Sorocaba',                 
            Phone = '(011)3098-1423',
            Email = 'contact@test.com',
            Company = 'Test Account'
            );
        
        Test.startTest();
        	Database.insert( lNewLead );
        Test.stopTest();
    }
    
    @isTest
    public static void byPassToSMB() {
    	Lead lNewLead = new Lead(
            LastName='Sales Lead',
            RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Client').getRecordTypeId(), 
            Country = 'Brazil',
            State = 'São Paulo',
            Street = 'Av Paulista 283',
            City = 'Sorocaba',                 
            Phone = '(011)3098-1423',
            Email = 'contact@test.com',
            Company = 'Test Account',
            LeadSource = 'Inbound',
            NumberOfEmployees = 500,
            Business_Unit__c = 'SMB'
            );
        
        Test.startTest();
        	Database.insert( lNewLead );
        Test.stopTest();
    }
    
    private static Account generateAccount( String aRecordTypeDevName , String aBDRId ){
        ID accRecordTypeId = 
            Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get( aRecordTypeDevName ).getRecordTypeId();
        Account lAcc = new Account();
        lAcc.RecordTypeId = accRecordTypeId;
        lAcc.Name = 'Test Account';
        lAcc.BillingCountry = 'Brazil';
        lAcc.BillingState = 'São Paulo';
        lAcc.NumberOfEmployees = 2000;
        lAcc.BDR__c = aBDRId;
        return lAcc;
    }
}