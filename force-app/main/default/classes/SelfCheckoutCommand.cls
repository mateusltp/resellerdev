/**
 * @author vncferraz
 */
public without sharing class SelfCheckoutCommand extends AbstractCommand {
    public SelfCheckoutCommand() {

    }

    override
    public void execute(){

        SelfCheckoutRequest selfCheckoutRequest = (SelfCheckoutRequest)
                                                     event.getPayloadFromJson(SelfCheckoutRequest.class);
        
        new SelfCheckoutService(selfCheckoutRequest).execute();
    }
}