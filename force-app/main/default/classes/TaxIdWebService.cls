@RestResource(urlMapping='/tax_id/*')
global with sharing class TaxIdWebService {
    

    @HttpPost
    global static String checkTaxId(String taxId, String taxIdType){

        List<Tax_Id_Data__c> taxIdRecordList = [ SELECT Id, Tax_Id__c FROM Tax_Id_Data__c WHERE Tax_Id__c =: taxId LIMIT 1 ];

        try{
            if(!taxIdRecordList.isEmpty()){
                Tax_Id_Event__e taxIdEvent = new Tax_Id_Event__e();
                taxIdEvent.Tax_Id__c = taxIdRecordList[0].Tax_Id__c;
                Database.SaveResult results = EventBus.publish(taxIdEvent);
            }
            else {
                Tax_Id_Data__c taxIdRecord = new Tax_Id_Data__c();
                taxIdRecord.Tax_Id__c = taxId;
                insert taxIdRecord;
    
                TaxIdService.assyncCalloutTaxId(taxIdRecord.Id, taxIdType);
            }
        }catch( ResponseException e ){
            throw new ResponseException();
        }
        
        
        return JSON.serialize(new Response('SUCCESS', 'IN PROCESS'));

    }

    public class Response{
        public String result{get;set;}
        public String status{get;set;}
        public Response(String result, String status){
            this.result = result;
            this.status = status;
        }
    }

    public class ResponseException extends Exception{}


}