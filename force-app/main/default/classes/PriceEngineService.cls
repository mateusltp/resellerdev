public without sharing class PriceEngineService {
    

    @AuraEnabled
    public static string getPercentComissionList(
        String quoteItemId)
    {

        QuoteLineItem quoteItem = [ SELECT Id, QuoteId, Quantity, CurrencyIsoCode, UnitPrice FROM QuoteLineItem WHERE Id =: quoteItemId LIMIT 1 ];
        Product2 enterpriseSubscription = getSKUEnterpriseSubscriptionByProductCode();
        
        Map<String, PriceWrapper> retorno = new Map<String,PriceWrapper>();
        for(PriceEngineItem__c item : [ SELECT Id, CommissionPercentage__c, PercentageAdjustment__c,Absolute_Value__c
                                        FROM PriceEngineItem__c 
                                        WHERE MinimumNumberEmployees__c <=: quoteItem.Quantity  
                                        AND MaximumNumberEmployees__c >=: quoteItem.Quantity 
                                        AND CurrencyIsoCode =: quoteItem.CurrencyIsoCode
                                        AND BusinessRuleFor__c =: ResellerEnumRepository.AccountRequestPartnerModel.Intermediation.name()
                                        AND Plan__c =: enterpriseSubscription.Id
                                        AND Active__c =: true ORDER BY CommissionPercentage__c DESC])
        {
            retorno.put(String.valueOf(item.CommissionPercentage__c),new PriceWrapper(item.Absolute_Value__c, item.CommissionPercentage__c, 0.00));
                                        }
        return JSON.serialize(retorno);
    }

    @InvocableMethod(label='Calculate Price' description='Submit a quotelineitem list to calculate price, for only the first one will be calculated.')
    public static void updatePriceByQuoteLineItem(
        List<QuoteLineItem> quoteLineItens)
    {

        QuoteLineItem lineItem = quoteLineItens[0];
        Quote quote = getQuoteById(lineItem.QuoteId);
        Opportunity currentOpportunity = getOpportunityById(quote.OpportunityId);
        Account reseller = getResellerAccount(currentOpportunity.Reseller_del__c)[0];
        
        Product2 enterpriseSubscription = getSKUEnterpriseSubscriptionByProductCode();
        Product2 familyMembers = getSKUFamilyMembersByProductCode();

        if(lineItem.Product2Id == enterpriseSubscription.Id)updateEnterpriseSubscription(reseller, currentOpportunity, quote, lineItem, enterpriseSubscription);
        else if(lineItem.Product2Id == familyMembers.Id)updateFamilyMember(reseller, currentOpportunity, quote, lineItem, familyMembers);

    }

    public static void updateEnterpriseSubscription(
        Account reseller, 
        Opportunity currentOpportunity, 
        Quote quote, 
        QuoteLineItem lineItem, 
        Product2 enterpriseSubscription)
    {

        List<PriceEngineItem__c> priceEngineItem_List = new List<PriceEngineItem__c>();

        if( (currentOpportunity.B_M__c == 'Total Subsidy' || currentOpportunity.B_M__c == 'Partial Subsidy') && 
            reseller.Indirect_Channel_Pricebook__c == 'Legacy Partners')
        {
            priceEngineItem_List.addAll( getEnterpriseSubscriptionLegacy(  reseller.Id, currentOpportunity.CurrencyIsoCode, 
                                                                    ResellerEnumRepository.PriceEngineItemPartnerCategory.Standard.name(), enterpriseSubscription.Id));
        }
        else if(currentOpportunity.B_M__c == ResellerEnumRepository.AccountRequestPartnerModel.Intermediation.name()){
            String comission = quote.Percent_of_comission__c != null ? quote.Percent_of_comission__c : '30';
            priceEngineItem_List.addAll(getEnterpriseSubscriptionIntermediation( lineItem.Quantity, currentOpportunity.CurrencyIsoCode, 
                                                            ResellerEnumRepository.AccountRequestPartnerModel.Intermediation.name(),
                                                            ResellerEnumRepository.PriceEngineItemPartnerCategory.Standard.name(),
                                                            comission,
                                                            enterpriseSubscription.Id));
        }
        else if(currentOpportunity.B_M__c == 'Total Subsidy' || currentOpportunity.B_M__c == 'Partial Subsidy'){
            priceEngineItem_List.addAll(getEnterpriseSubscriptionSubsidy( lineItem.Quantity, currentOpportunity.CurrencyIsoCode, 
            ResellerEnumRepository.AccountRequestPartnerModel.Subsidy.name(),
            getOldPartnerCategoryOnAccountReseller(reseller.Indirect_Channel_Pricebook__c), enterpriseSubscription.Id));
        }

        if(priceEngineItem_List.isEmpty())throw new AuraHandledException('Price Enterprise Subscription not registered in the price engine.');
        if(priceEngineItem_List.size() > 1)throw new AuraHandledException('Duplicate error, more than 1 price Enterprise Subscription record was found in the engine.');


        lineItem.UnitPrice = priceEngineItem_List[0].Absolute_Value__c;
        lineItem.List_Price__c = priceEngineItem_List[0].Absolute_Value__c;

        update lineItem;

        createPriceMemory(lineItem.Id, priceEngineItem_List[0].Id);
    }
    
    public static void updateFamilyMember(
        Account reseller, 
        Opportunity currentOpportunity, 
        Quote quote, 
        QuoteLineItem lineItem, 
        Product2 familyMembers)
    {

        List<PriceEngineItem__c> priceEngineItem_List = new List<PriceEngineItem__c>();

        if( (currentOpportunity.B_M__c == 'Total Subsidy' || currentOpportunity.B_M__c == 'Partial Subsidy') && 
            reseller.Indirect_Channel_Pricebook__c == 'Legacy Partners')
        {
            priceEngineItem_List.addAll( getFamilyMembersLegacy(  reseller.Id, currentOpportunity.CurrencyIsoCode, 
                                                                    ResellerEnumRepository.PriceEngineItemPartnerCategory.Standard.name(), familyMembers.Id));
        }
        else if(currentOpportunity.B_M__c == ResellerEnumRepository.AccountRequestPartnerModel.Intermediation.name()){
            String comission = quote.Percent_of_comission__c != null ? quote.Percent_of_comission__c : '30';
            priceEngineItem_List.addAll(getFamilyMembersIntermediation( lineItem.Quantity, currentOpportunity.CurrencyIsoCode, 
                                                            ResellerEnumRepository.AccountRequestPartnerModel.Intermediation.name(),
                                                            ResellerEnumRepository.PriceEngineItemPartnerCategory.Standard.name(),
                                                            comission,
                                                            familyMembers.Id));
        }
        else if(currentOpportunity.B_M__c == 'Total Subsidy' || currentOpportunity.B_M__c == 'Partial Subsidy'){
            priceEngineItem_List.addAll(getFamilyMembersSubsidy( lineItem.Quantity, currentOpportunity.CurrencyIsoCode, 
            ResellerEnumRepository.AccountRequestPartnerModel.Subsidy.name(),
            getOldPartnerCategoryOnAccountReseller(reseller.Indirect_Channel_Pricebook__c), familyMembers.Id));
        }

        if(priceEngineItem_List.isEmpty())throw new AuraHandledException('Price Family Member not registered in the price engine.');
        if(priceEngineItem_List.size() > 1)throw new AuraHandledException('Duplicate error, more than 1 price Family Member record was found in the engine.');


        lineItem.UnitPrice = priceEngineItem_List[0].Absolute_Value__c;
        lineItem.List_Price__c = priceEngineItem_List[0].Absolute_Value__c;

        update lineItem;

        createPriceMemory(lineItem.Id, priceEngineItem_List[0].Id);

    }

    public static List<PriceEngineItem__c> getPriceResellerHub( 
        Account_Request__c request)
    {

        request = [ SELECT Id, CurrencyIsoCode, Total_number_of_employees__c, Partner_Model__c FROM Account_Request__c WHERE Id =: request.Id LIMIT 1];
        Account reseller = getResellerByUserId(UserInfo.getUserId())[0];
        Product2 enterpriseSubscription = getSKUEnterpriseSubscriptionByProductCode();

        List<PriceEngineItem__c> priceEngineItem_List = new List<PriceEngineItem__c>();

        if( request.Partner_Model__c == ResellerEnumRepository.AccountRequestPartnerModel.Subsidy.name()  
            && reseller.Indirect_Channel_Pricebook__c == 'Legacy Partners')
        {
            priceEngineItem_List.addAll( getEnterpriseSubscriptionLegacy(  reseller.Id, request.CurrencyIsoCode, 
                                                                  ResellerEnumRepository.PriceEngineItemPartnerCategory.Standard.name(), enterpriseSubscription.Id));
        }
        else if(request.Partner_Model__c == ResellerEnumRepository.AccountRequestPartnerModel.Intermediation.name()){
            priceEngineItem_List.addAll(getEnterpriseSubscriptionIntermediation( request.Total_number_of_employees__c, request.CurrencyIsoCode, 
                                                            ResellerEnumRepository.AccountRequestPartnerModel.Intermediation.name(),
                                                            ResellerEnumRepository.PriceEngineItemPartnerCategory.Standard.name(),
                                                            '30',enterpriseSubscription.Id));
        }
        else if(request.Partner_Model__c == ResellerEnumRepository.AccountRequestPartnerModel.Subsidy.name()){
            priceEngineItem_List.addAll(getEnterpriseSubscriptionSubsidy( request.Total_number_of_employees__c, request.CurrencyIsoCode, 
            ResellerEnumRepository.AccountRequestPartnerModel.Subsidy.name(),
            getOldPartnerCategoryOnAccountReseller(reseller.Indirect_Channel_Pricebook__c), enterpriseSubscription.Id));
        }

        if(priceEngineItem_List.isEmpty())throw new AuraHandledException('Price Enterprise Subscription not registered in the price engine.');
        if(priceEngineItem_List.size() > 1)throw new AuraHandledException('Duplicate error, more than 1 price Enterprise Subscription record was found in the engine.');

        return priceEngineItem_List;

        

    }

    public static List<PriceEngineItem__c> getFamilyMembersSubsidy(
        Decimal quantity,
        String currencyIsoCode,
        String businessRuleFor, 
        String partnerCategory,
        String familyMembersId)
    {
        return [ 
                    SELECT Id, CommissionPercentage__c, PercentageAdjustment__c, Absolute_Value__c, Absolute_Value_Family_Member__c
                    FROM PriceEngineItem__c 
                    WHERE MinimumNumberEmployees__c <=: quantity
                    AND MaximumNumberEmployees__c >=: quantity
                    AND CurrencyIsoCode =: currencyIsoCode
                    AND BusinessRuleFor__c =: businessRuleFor
                    AND PartnerCategory__c =: partnerCategory
                    AND Plan__c =: familyMembersId
                    AND Active__c =: true ];
    }

    public static List<PriceEngineItem__c> getFamilyMembersIntermediation(
        Decimal quantity,  
        String currencyIsoCode, 
        String businessRuleFor, 
        String partnerCategory,
        String commissionPercentage,
        String familyMembersId)
    {
        System.debug('######### '+quantity);
        System.debug('######### '+currencyIsoCode);
        System.debug('######### '+businessRuleFor);
        System.debug('######### '+partnerCategory);
        System.debug('######### '+commissionPercentage);
        System.debug('######### '+familyMembersId);
        return [ 
                    SELECT Id, CommissionPercentage__c, PercentageAdjustment__c, Absolute_Value__c, Absolute_Value_Family_Member__c 
                    FROM PriceEngineItem__c 
                    WHERE MinimumNumberEmployees__c <=: quantity
                    AND MaximumNumberEmployees__c >=: quantity
                    AND CommissionPercentage__c =: Decimal.valueOf(commissionPercentage)
                    AND CurrencyIsoCode =: currencyIsoCode
                    AND BusinessRuleFor__c =: businessRuleFor
                    AND PartnerCategory__c =: partnerCategory
                    AND Plan__c =: familyMembersId
                    AND Active__c =: true ];
    }

    public static List<PriceEngineItem__c> getFamilyMembersLegacy( 
        String resellerId, 
        String currencyIsoCode, 
        String partnerCategory, 
        String familyMembersId )
    {
        return [ 
            SELECT Id, CommissionPercentage__c, PercentageAdjustment__c, Absolute_Value__c, Absolute_Value_Family_Member__c
            FROM PriceEngineItem__c 
            WHERE CurrencyIsoCode =: currencyIsoCode
            AND Reseller__c =: resellerId
            AND BusinessRuleFor__c =: ResellerEnumRepository.AccountRequestPartnerModel.LegacyPartners.name()
            AND PartnerCategory__c =: partnerCategory
            AND Plan__c =: familyMembersId
            AND Active__c =: true ];
    }

    public static List<PriceEngineItem__c> getEnterpriseSubscriptionSubsidy(
        Decimal quantity,  
        String currencyIsoCode, 
        String businessRuleFor, 
        String partnerCategory,
        String enterpriseSubscriptionId )
    {
		system.debug([SELECT Id, CommissionPercentage__c, PercentageAdjustment__c, Absolute_Value__c, Absolute_Value_Family_Member__c,
                      CurrencyIsoCode, BusinessRuleFor__c, PartnerCategory__c, Plan__c, Active__c FROM PriceEngineItem__c]);
        return [ 
                    SELECT Id, CommissionPercentage__c, PercentageAdjustment__c, Absolute_Value__c, Absolute_Value_Family_Member__c
                    FROM PriceEngineItem__c 
                    WHERE MinimumNumberEmployees__c <=: quantity
                    AND MaximumNumberEmployees__c >=: quantity
                    AND CurrencyIsoCode =: currencyIsoCode
                    AND BusinessRuleFor__c =: businessRuleFor
                    AND PartnerCategory__c =: partnerCategory
                    AND Plan__c =: enterpriseSubscriptionId
                    AND Active__c =: true ];
    }

    public static List<PriceEngineItem__c> getEnterpriseSubscriptionIntermediation(
        Decimal quantity,  
        String currencyIsoCode, 
        String businessRuleFor, 
        String partnerCategory,
        String commissionPercentage,
        String enterpriseSubscriptionId)
    {

        return [ 
                    SELECT Id, CommissionPercentage__c, PercentageAdjustment__c, Absolute_Value__c, Absolute_Value_Family_Member__c 
                    FROM PriceEngineItem__c 
                    WHERE MinimumNumberEmployees__c <=: quantity
                    AND MaximumNumberEmployees__c >=: quantity
                    AND CommissionPercentage__c =: Decimal.valueOf(commissionPercentage)
                    AND CurrencyIsoCode =: currencyIsoCode
                    AND BusinessRuleFor__c =: businessRuleFor
                    AND PartnerCategory__c =: partnerCategory
                    AND Plan__c =: enterpriseSubscriptionId
                    AND Active__c =: true ];
    }

    public static List<PriceEngineItem__c> getEnterpriseSubscriptionLegacy( 
        String resellerId, 
        String currencyIsoCode, 
        String partnerCategory, 
        String enterpriseSubscriptionId )
    {
        return [ 
            SELECT Id, CommissionPercentage__c, PercentageAdjustment__c, Absolute_Value__c, Absolute_Value_Family_Member__c
            FROM PriceEngineItem__c 
            WHERE CurrencyIsoCode =: currencyIsoCode
            AND Reseller__c =: resellerId
            AND BusinessRuleFor__c =: ResellerEnumRepository.AccountRequestPartnerModel.LegacyPartners.name()
            AND PartnerCategory__c =: partnerCategory
            AND Plan__c =: enterpriseSubscriptionId
            AND Active__c =: true ];
    } 
    
    public static List<Account> getResellerAccount(
        String resellerId)
    {
        return [ SELECT Id, Indirect_Channel_Pricebook__c FROM Account WHERE Id =: resellerId ];
    }

    public static List<Account> getResellerByUserId(
        Id userId)
    {
        User usuario = [ SELECT Id, ContactId FROM User WHERE Id =: userId LIMIT 1];
        Contact contato = [ SELECT Id, AccountId FROM Contact WHERE Id =: usuario.ContactId LIMIT 1];
        return [ SELECT Id, Indirect_Channel_Pricebook__c FROM Account WHERE Id =: contato.AccountId];
    }

    public static Product2 getSKUEnterpriseSubscriptionByProductCode()
    {
        return [ SELECT Id FROM Product2 WHERE ProductCode = 'SKUEnterpriseSubscription' LIMIT 1 ];
    }

    public static Product2 getSKUFamilyMembersByProductCode()
    {
        return [ SELECT Id FROM Product2 WHERE ProductCode = 'SKUFamilyMembers' LIMIT 1 ];
    }

    public static void createPriceMemory(
        String quoteLineItemId, 
        String priceEngineItemId)
    {
        insert new Price_Memory__c( Price_Engine_Item__c = priceEngineItemId, Quote_Line_Item__c = quoteLineItemId);
    }

    public static String getOldPartnerCategoryOnAccountReseller(
        String partnerCategory)
    {
        if(partnerCategory == '5k FTEs')return ResellerEnumRepository.PriceEngineItemPartnerCategory.Subsidy_5kFTEs.name();
        else if(partnerCategory == '15k FTEs') return ResellerEnumRepository.PriceEngineItemPartnerCategory.Subsidy_15kFTEs.name();
        else return partnerCategory;
    }

    public static Quote getQuoteById(
        String quoteId)
    {
        return [ SELECT Id, OpportunityId, Percent_of_comission__c FROM Quote WHERE Id =: quoteId LIMIT 1 ];
    }

    public static Opportunity getOpportunityById(
        String opportunityId)
    {
        return [ SELECT Id, Reseller_del__c, B_M__c,Family_member_included__c, CurrencyIsoCode FROM Opportunity WHERE Id =: opportunityId LIMIT 1 ];
    }



    public class PriceWrapper{
        public Decimal price{get;set;}
        public Decimal commissionPercentage{get;set;}
        public Decimal percentageAdjustment{get;set;}

        public PriceWrapper(Decimal price, Decimal commissionPercentage, Decimal percentageAdjustment){
            this.price = price;
            this.commissionPercentage = commissionPercentage;
            this.percentageAdjustment = percentageAdjustment;
        }
    }



}