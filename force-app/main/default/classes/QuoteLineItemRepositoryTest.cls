/**
* @author vinicius.ferraz
* @description QuoteLineItemRepository unit test
*/
@isTest(seeAllData=false)
public class QuoteLineItemRepositoryTest {
    
    @TestSetup
    static void createData(){        
        Account acc = generateAccount();
        insert acc;
        Pricebook2 pb = generatePricebook();
        insert pb;
        Opportunity opp = generateOpportunity(acc,pb);
        insert opp;
        Quote quote = generateQuote(opp,'1');
        insert quote;   

        List<Product2> pToInsert = new List<Product2>();   
        Product2 p = generateProduct('Enterprise Subscription'); 
        pToInsert.add(p);
        Product2 pSetupFee = generateProduct('Setup Fee');
        pToInsert.add(pSetupFee);
        Product2 pPSSetupFee = generateProduct('Professional Services Maintenance Fee');
        pToInsert.add(pPSSetupFee);
        Product2 pPSMaintenanceFee = generateProduct('Professional Services Setup Fee');
        pToInsert.add(pPSMaintenanceFee);
        insert pToInsert;

        List<PricebookEntry> pbToInsert = new List<PricebookEntry>();   
        PricebookEntry accessFee = generateAccessFeePricebookEntry(pb, p, opp);
        pbToInsert.add(accessFee);
        PricebookEntry pbSetupFee = generateAccessFeePricebookEntry(pb, pSetupFee, opp);
        pbToInsert.add(pbSetupFee);
        PricebookEntry pbPSSetupFee = generateAccessFeePricebookEntry(pb, pPSSetupFee, opp);
        pbToInsert.add(pbPSSetupFee);
        PricebookEntry pbPSMaintenanceFee = generateAccessFeePricebookEntry(pb, pPSMaintenanceFee, opp);
        pbToInsert.add(pbPSMaintenanceFee);
        insert pbToInsert;

        List<QuoteLineItem> quoteLineItems = new List<QuoteLineItem>();
        quoteLineItems.add(generateQuoteLineItem(quote,accessFee));
        quoteLineItems.add(generateQuoteLineItem(quote,pbSetupFee));
        quoteLineItems.add(generateQuoteLineItem(quote,pbPSSetupFee));
        quoteLineItems.add(generateQuoteLineItem(quote,pbPSMaintenanceFee));
        insert quoteLineItems;
    }

    @isTest
    static void testById(){
        QuoteLineItem QuoteLineItem = [select Id from QuoteLineItem limit 1];
        QuoteLineItemRepository quoteLineItems = new QuoteLineItemRepository();
        QuoteLineItem qt = null;
        try{
            qt = quoteLineItems.byId(quoteLineItem.Id);
        }catch(System.QueryException e){
            System.assert(false);
        }
        System.assertEquals(qt.Id,QuoteLineItem.Id);
    }

    @isTest
    static void testAccessFeeForQuote(){
        QuoteLineItem QuoteLineItem = [SELECT Id,QuoteId FROM QuoteLineItem 
                                        WHERE PricebookEntry.Product2.Family = 'Enterprise Subscription'  limit 1];
        QuoteLineItemRepository quoteLineItems = new QuoteLineItemRepository();
        QuoteLineItem qt = null;
        try{
            qt = quoteLineItems.accessFeeForQuote(quoteLineItem.QuoteId);
        }catch(System.QueryException e){
            System.assert(false);
        }
        System.assertNotEquals(qt.Id,null);
    }

    @isTest
    static void testAdd(){
        QuoteLineItemRepository quoteLineItems = new QuoteLineItemRepository();
        Quote q = [select Id,Name from Quote limit 1];
        PricebookEntry pbEntry = [select Id,Name,Product2Id from PricebookEntry  where Pricebook2.IsStandard=false  limit 1];
        QuoteLineItem qt = generateQuoteLineItem(q,pbEntry);        
        System.assertEquals(qt.Id,null);
        try{
            quoteLineItems.add(qt);
        }catch(System.DmlException e){
            System.assert(false);
        }
        System.assertNotEquals(qt.Id,null);
    }

    @isTest
    static void testEdit(){
        QuoteLineItemRepository quoteLineItems = new QuoteLineItemRepository();
        Quote q = [select Id,Name from Quote limit 1];
        PricebookEntry pbEntry = [select Id,Name,Product2Id from PricebookEntry  where Pricebook2.IsStandard=false  limit 1];
        QuoteLineItem qt = generateQuoteLineItem(q,pbEntry);        
        System.assertEquals(qt.Id,null);
        try{
            quoteLineItems.edit(qt);
        }catch(System.DmlException e){
            System.assert(true);
        }
        System.assertEquals(qt.Id,null);
        
        quoteLineItems.add(qt);
        
        try{
            quoteLineItems.edit(qt);
        }catch(System.DmlException e){
            System.assert(false);
        }
        System.assertNotEquals(qt.Id,null);
    }

    @isTest
    static void testAddOrEdit(){
        QuoteLineItemRepository quoteLineItems = new QuoteLineItemRepository();
        Quote q = [select Id,Name from Quote limit 1];
        PricebookEntry pbEntry = [select Id,Name,Product2Id from PricebookEntry  where Pricebook2.IsStandard=false  limit 1];
        QuoteLineItem qt = generateQuoteLineItem(q,pbEntry);        
        System.assertEquals(qt.Id,null);
        
        try{
            quoteLineItems.addOrEdit(qt);
        }catch(System.DmlException e){
            System.assert(false);
        }
        System.assertNotEquals(qt.Id,null);
        
        try{
            quoteLineItems.addOrEdit(qt);
        }catch(System.DmlException e){
            System.assert(false);
        }
        System.assertNotEquals(qt.Id,null);
    }

    private static Account generateAccount(){
        Id rtId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gympass_Entity').getRecordTypeId();
        Account acc = new Account();
        acc.name='AcademiaBrasilCompanyPai';
        acc.RecordTypeId = rtId;
        acc.GP_Status__c = 'Active';
        acc.billingState = 'Minas Gerais';
        acc.CAP_Value__c = 120;
        acc.BillingCity = 'CityAcademiaBrasil';
        acc.billingStreet = 'Rua academiabrasilpai';
        acc.billingCountry = 'Brazil';
        acc.Gym_Type__c = 'Studios';
        acc.Gym_Classes__c = 'Cardio';
        acc.Types_of_ownership__c = Label.franchisePicklist;
        acc.Subscription_Type__c = 'Value per class';
        acc.Subscription_Period__c = 'Monthy value';
        acc.Subscription_Type_Estimated_Price__c    = 100;
        acc.Has_market_cannibalization__c = 'No';
        acc.Exclusivity__c = 'Yes';
        acc.Exclusivity_End_Date__c = Date.today().addYears(1);
        acc.Exclusivity_Partnership__c = 'Full Exclusivity';
        acc.Exclusivity_Restrictions__c= 'No';
        acc.Website = 'testing@tesapex.com';
        acc.Gym_Email__c = 'gymemail@apex.com';
        acc.Phone = '3222123123';
        acc.Can_use_logo__c = 'Yes';
        acc.Legal_Registration__c = 12123;
        acc.Legal_Title__c = 'Title LTDA';
        acc.Gyms_Identification_Document__c = 'CNPJ'; 
        return acc;
    }
    private static Quote generateQuote(Opportunity opp, String name){
        Quote qt = new Quote();  
        qt.RecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Gympass_Plus').getRecordTypeId();
        qt.Name = opp.Name+name;
        qt.OpportunityId = opp.Id;   
        qt.License_Fee_Waiver__c = 'No';  
        qt.CurrencyIsoCode = opp.CurrencyIsoCode;
        return qt;
    }
    private static QuoteLineItem generateQuoteLineItem(Quote quote, PricebookEntry accessFee){
        QuoteLineItem qt = new QuoteLineItem();
        qt.QuoteId = quote.Id;   
        qt.PricebookEntryId = accessFee.Id;
        qt.Product2Id = accessFee.Product2Id;
        qt.Quantity = 10;
        qt.UnitPrice =100;
        return qt;
    }

    private static Opportunity generateOpportunity(Account acc, Pricebook2 pb){
        Id oppRtId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        Opportunity accOpp = new Opportunity();
        accOpp.recordTypeId = oppRtId;
        accOpp.AccountId = acc.id;
        accOpp.Name = acc.Id; 
        accOpp.CMS_Used__c = 'Yes';     
        accOpp.Gym_agreed_to_an_API_integration__c  = 'Yes';
        accOpp.Club_Management_System__c = 'Companhia Athletica';
        accOpp.Integration_Fee_Deduction__c = 'No';
        accOpp.CloseDate = Date.today();
        accOpp.Success_Look_Like__c = 'Yes';
        accOpp.Success_Look_Like_Description__c = 'Money money';
        accOpp.StageName = 'Proposta Enviada';
        accOpp.Type = 'Expansion';  
        accOpp.Country_Manager_Approval__c = true;
        accOpp.Payment_approved__c = true;   
        accOpp.CurrencyIsoCode = 'BRL';
        accOpp.Gympass_Plus__c = 'Yes';
        accOpp.Standard_Payment__c = 'Yes';
        accOpp.Request_for_self_checkin__c = 'Yes';  
        accOpp.Pricebook2Id = pb.Id;
        return accOpp;
    }

    private static Pricebook2 generatePricebook(){        
        Pricebook2 pb = new Pricebook2();
        pb.Name = 'Br';
        pb.Country__c = 'Brazil';
        pb.IsActive = true;
        return pb;
    }

    private static Product2 generateProduct( String productFamilyName){
        Product2 product = new Product2();
        product.Name =productFamilyName; 
        product.Minimum_Number_of_Employees__c = 0;
        product.Maximum_Number_of_Employees__c = 900;
        product.Family = productFamilyName; 
        product.IsActive = true;
        return product;
    }

    private static PricebookEntry generateAccessFeePricebookEntry(Pricebook2 pb, Product2 product, Opportunity opp){        
        PricebookEntry pbEntry = new PricebookEntry();
        pbEntry.Product2Id = product.Id;
        pbEntry.Pricebook2Id = Test.getStandardPricebookId();
        pbEntry.UnitPrice = 100;
        pbEntry.IsActive = true;
        pbEntry.CurrencyIsoCode = opp.CurrencyIsoCode;
        insert pbEntry;
       
        PricebookEntry custompbEntry = new PricebookEntry();
        custompbEntry.Product2Id = product.Id;
        custompbEntry.Pricebook2Id = pb.Id;
        custompbEntry.UnitPrice = 100;
        custompbEntry.IsActive = true;
        custompbEntry.CurrencyIsoCode = opp.CurrencyIsoCode;
        return custompbEntry;
    }


    @isTest
    public static void testAccessFeeAndPaymentsForQuote() {
        QuoteLineItemRepository repo = new QuoteLineItemRepository();

        Quote q = [SELECT Id FROM Quote LIMIT 1];

        Test.startTest();
        repo.accessFeeAndPaymentsForQuote(q.Id);
        Test.stopTest();
    }


    @isTest
    public static void testGetAccessFeesForQuotes() {
        QuoteLineItemRepository repo = new QuoteLineItemRepository();

        List<QuoteLineItem> quoteLineItems = [SELECT Id FROM QuoteLineItem ];

        List<Id> quoteLineItemIds = new List<Id>();
        for(QuoteLineItem q : quoteLineItems){
            quoteLineItemIds.add(q.Id);
        }
        Test.startTest();
        repo.getAccessFeesForQuotes(quoteLineItemIds);
        Test.stopTest();
    }


    @isTest
    public static void testAccessFeesForQuote() {
        QuoteLineItemRepository repo = new QuoteLineItemRepository();

        Quote q = [SELECT Id FROM Quote LIMIT 1 ];
  
        Test.startTest();
        repo.accessFeesForQuote(q.Id);
        Test.stopTest();
    }

    @isTest
    public static void testGetOppIdToAccessFeeItem() {
        QuoteLineItemRepository repo = new QuoteLineItemRepository();

        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1 ];
  
        Test.startTest();
        repo.getOppIdToAccessFeeItem(new List<Id>{opp.id});
        Test.stopTest();
    }

    @isTest
    public static void testAccessFeeForOpportunities() {
        QuoteLineItemRepository repo = new QuoteLineItemRepository();

        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1 ];
  
        Test.startTest();
        repo.accessFeeForOpportunities(new List<Id>{opp.id});
        Test.stopTest();
    }

    
     
    @isTest
    public static void testSetupFeeAndPaymentsForQuote() {
        QuoteLineItemRepository repo = new QuoteLineItemRepository();

        Quote q = [SELECT Id FROM Quote LIMIT 1 ];
  
        Test.startTest();
        repo.setupFeeAndPaymentsForQuote(q.Id);
        Test.stopTest();
    }

    @isTest
    public static void testSetupFeeForQuote() {
        QuoteLineItemRepository repo = new QuoteLineItemRepository();

        Quote q = [SELECT Id FROM Quote LIMIT 1 ];
  
        Test.startTest();
        repo.setupFeeForQuote(q.Id);
        Test.stopTest();
    }

    @isTest
    public static void testSetupFeesForQuote() {
        QuoteLineItemRepository repo = new QuoteLineItemRepository();

        Quote q = [SELECT Id FROM Quote LIMIT 1 ];
  
        Test.startTest();
        repo.setupFeesForQuote(q.Id);
        Test.stopTest();
    }

    @isTest
    public static void testProServicesOneFeeAndPaymentsForQuote() {
        QuoteLineItemRepository repo = new QuoteLineItemRepository();

        Quote q = [SELECT Id FROM Quote LIMIT 1 ];
  
        Test.startTest();
        repo.proServicesOneFeeAndPaymentsForQuote(q.Id);
        Test.stopTest();
    }

    
    @isTest
    public static void testProServicesOneFeeForQuote() {
        QuoteLineItemRepository repo = new QuoteLineItemRepository();

        Quote q = [SELECT Id FROM Quote LIMIT 1 ];
  
        Test.startTest();
        repo.proServicesOneFeeForQuote(q.Id);
        Test.stopTest();
    }

    @isTest
    public static void testProServicesMainFeeAndPaymentsForQuote() {
        QuoteLineItemRepository repo = new QuoteLineItemRepository();

        Quote q = [SELECT Id FROM Quote LIMIT 1 ];
  
        Test.startTest();
        repo.proServicesMainFeeAndPaymentsForQuote(q.Id);
        Test.stopTest();
    }

    @isTest
    public static void testProServicesMainFeeForQuote() {
        QuoteLineItemRepository repo = new QuoteLineItemRepository();

        Quote q = [SELECT Id FROM Quote LIMIT 1 ];
  
        Test.startTest();
        repo.proServicesMainFeeForQuote(q.Id);
        Test.stopTest();
    }

    
    @isTest
    public static void testAll() {
        QuoteLineItemRepository repo = new QuoteLineItemRepository();

        Quote q = [SELECT Id FROM Quote LIMIT 1 ];
  
        Test.startTest();
        repo.all(q.Id);
        Test.stopTest();
    }

    @isTest
    public static void testAllForOpportunity() {
        QuoteLineItemRepository repo = new QuoteLineItemRepository();

        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1 ];
  
        Test.startTest();
        repo.allForOpportunity(opp.Id);
        Test.stopTest();
    }


    @isTest
    public static void testFeesPaymentsForQuote() {
       List<Id> quoteIds = new List<Id>();

        List<Quote> quotes = [SELECT Id FROM Quote ];
        for(Quote q : quotes){
            quoteIds.add(q.Id);
        }
        Test.startTest();
        QuoteLineItemRepository.feesPaymentsForQuote(quoteIds);
        Test.stopTest();
    }


    @isTest
    public static void testClearItemsForQuote() {
        QuoteLineItemRepository repo = new QuoteLineItemRepository();

        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1 ];
  
        Test.startTest();
        repo.clearItemsForQuote(opp.Id);
        Test.stopTest();
    }



}