/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 07-08-2022
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/
public with sharing class ProductOpportunityAssignmentSelector extends ApplicationSelector {
    
	private static PS_Constants constants = PS_Constants.getInstance();
    public List<Schema.SObjectField> getSObjectFieldList() {
		return new List<Schema.SObjectField> {
			Product_Opportunity_Assignment__c.Id,
			Product_Opportunity_Assignment__c.Name
		};
	}

	public Schema.SObjectType getSObjectType() {
		return Product_Opportunity_Assignment__c.sObjectType;
	}

    public List<Product_Opportunity_Assignment__c> selectById(Set<Id> ids) {
		return (List<Product_Opportunity_Assignment__c>) super.selectSObjectsById(ids);
	}

	public List<Product_Opportunity_Assignment__c> selectCapByProductId(Set<Id> productIds) {
		return ( List<Product_Opportunity_Assignment__c> ) Database.query(
				newQueryFactory().
				selectField(Product_Opportunity_Assignment__c.Id).
				selectField(Product_Opportunity_Assignment__c.Marked_Poa__c).
				selectField('OpportunityMemberId__r.Account__c').
				selectField('OpportunityMemberId__r.Account__r.Name').	
				selectField('OpportunityMemberId__r.Account__r.Gym_Type__c').	 										
				selectField('OpportunityMemberId__r.Opportunity__c').			 													 										
				selectField('ProductAssignmentId__c').			 										
				selectField('ProductAssignmentId__r.CommercialConditionId__c').			 										
				selectField('ProductAssignmentId__r.CommercialConditionId__r.RecordTypeId').			 										
				selectField('ProductAssignmentId__r.CommercialConditionId__r.Amount__c').
				selectField('ProductAssignmentId__r.CommercialConditionId__r.CAP_Discount__c').
				selectField('ProductAssignmentId__r.CommercialConditionId__r.Visits_to_CAP__c'). 											
				selectField('ProductAssignmentId__r.ProductId__c').			 										
				selectField('ProductAssignmentId__r.ProductId__r.Net_Transfer_Price__c').			 										
				selectField('ProductAssignmentId__r.Market_price__c').
				selectField('ProductAssignmentId__r.Late_Cancel__c').			 										
				selectField('ProductAssignmentId__r.Cap_Value__c').			 										
				selectField('ProductAssignmentId__r.Net_Transfer_Price__c').			 										
				selectField('ProductAssignmentId__r.No_Show_Fee__c').	
				setCondition('ProductAssignmentId__r.ProductId__c IN :productIds AND ProductAssignmentId__r.CommercialConditionId__r.RecordType.DeveloperName = \'CAP\'').toSOQL()
		);
	}

	public List<Product_Opportunity_Assignment__c> selectProductIdWithCommConditions(Set<Id> productIds) {
		Set<String> commConditionsRecTypeDevName = new Set<String>{'CAP', 'Late_Cancellation', 'No_Show'};
		
		return ( List<Product_Opportunity_Assignment__c> ) Database.query(
				newQueryFactory().
				selectField(Product_Opportunity_Assignment__c.Id).
				selectField(Product_Opportunity_Assignment__c.Marked_Poa__c).
				selectField('OpportunityMemberId__r.Account__c').
				selectField('OpportunityMemberId__r.Account__r.Name').		 										
				selectField('OpportunityMemberId__r.Opportunity__c').			 													 										
				selectField('OpportunityMemberId__r.From_Child_Opp_Creation_Process__c').			 													 										
				selectField('ProductAssignmentId__c').			 										
				selectField('ProductAssignmentId__r.CommercialConditionId__c').			 										
				selectField('ProductAssignmentId__r.CommercialConditionId__r.RecordTypeId').			 										
				selectField('ProductAssignmentId__r.CommercialConditionId__r.Amount__c').
				selectField('ProductAssignmentId__r.CommercialConditionId__r.CAP_Discount__c').
				selectField('ProductAssignmentId__r.CommercialConditionId__r.Visits_to_CAP__c'). 											
				selectField('ProductAssignmentId__r.ProductId__c').			 										
				selectField('ProductAssignmentId__r.ProductId__r.Net_Transfer_Price__c').			 										
				selectField('ProductAssignmentId__r.Market_price__c').
				selectField('ProductAssignmentId__r.Late_Cancel__c').			 										
				selectField('ProductAssignmentId__r.Cap_Value__c').			 										
				selectField('ProductAssignmentId__r.Net_Transfer_Price__c').			 										
				selectField('ProductAssignmentId__r.No_Show_Fee__c').	
				setCondition('ProductAssignmentId__r.ProductId__c IN :productIds AND ProductAssignmentId__r.CommercialConditionId__r.RecordType.DeveloperName IN :commConditionsRecTypeDevName').toSOQL()
		);
	}

	public List<Product_Opportunity_Assignment__c> selectNonCapByProductId(Set<Id> productIds) {
		Set<String> commConditionsRecTypeDevName = new Set<String>{'Late_Cancellation', 'No_Show'};
		
		return ( List<Product_Opportunity_Assignment__c> ) Database.query(
				newQueryFactory().
				selectField(Product_Opportunity_Assignment__c.Id).
				selectField(Product_Opportunity_Assignment__c.Marked_Poa__c).
				selectField('OpportunityMemberId__r.Account__c').
				selectField('OpportunityMemberId__r.Account__r.Name').		 										
				selectField('OpportunityMemberId__r.Opportunity__c').			 													 										
				selectField('ProductAssignmentId__c').			 										
				selectField('ProductAssignmentId__r.CommercialConditionId__c').			 										
				selectField('ProductAssignmentId__r.CommercialConditionId__r.RecordTypeId').			 										
				selectField('ProductAssignmentId__r.CommercialConditionId__r.Amount__c').
				selectField('ProductAssignmentId__r.CommercialConditionId__r.CAP_Discount__c').
				selectField('ProductAssignmentId__r.CommercialConditionId__r.Visits_to_CAP__c'). 											
				selectField('ProductAssignmentId__r.ProductId__c').			 										
				selectField('ProductAssignmentId__r.ProductId__r.Net_Transfer_Price__c').			 										
				selectField('ProductAssignmentId__r.Market_price__c').
				selectField('ProductAssignmentId__r.Late_Cancel__c').			 										
				selectField('ProductAssignmentId__r.Cap_Value__c').			 										
				selectField('ProductAssignmentId__r.Net_Transfer_Price__c').			 										
				selectField('ProductAssignmentId__r.No_Show_Fee__c').	
				setCondition('ProductAssignmentId__r.ProductId__c IN :productIds AND ProductAssignmentId__r.CommercialConditionId__r.RecordType.DeveloperName IN :commConditionsRecTypeDevName').toSOQL()
		);
	}

	public List<Product_Opportunity_Assignment__c> selectCapByAccountId(Set<Id> accountIds) {
		return ( List<Product_Opportunity_Assignment__c> ) Database.query(
				newQueryFactory().
				selectField(Product_Opportunity_Assignment__c.Id).
				selectField(Product_Opportunity_Assignment__c.Marked_Poa__c).					
				selectField('OpportunityMemberId__r.Account__c').
				selectField('OpportunityMemberId__r.Account__r.Name').		 										
				selectField('OpportunityMemberId__r.Opportunity__c').			 										
				selectField('ProductAssignmentId__c').			 										
				selectField('ProductAssignmentId__r.CommercialConditionId__c').			 										
				selectField('ProductAssignmentId__r.CommercialConditionId__r.RecordTypeId').			 										
				selectField('ProductAssignmentId__r.CommercialConditionId__r.Amount__c').
				selectField('ProductAssignmentId__r.CommercialConditionId__r.CAP_Discount__c').
				selectField('ProductAssignmentId__r.CommercialConditionId__r.Visits_to_CAP__c'). 		 										
				selectField('ProductAssignmentId__r.ProductId__c').			 										
				selectField('ProductAssignmentId__r.ProductId__r.Parent_Product__c').			 										
				selectField('ProductAssignmentId__r.ProductId__r.Net_Transfer_Price__c').		 										
				selectField('ProductAssignmentId__r.Market_price__c').			 										
				selectField('ProductAssignmentId__r.Late_Cancel__c').			 										
				selectField('ProductAssignmentId__r.Cap_Value__c').			 										
				selectField('ProductAssignmentId__r.Net_Transfer_Price__c').			 										
				selectField('ProductAssignmentId__r.No_Show_Fee__c').			 										
				setCondition('OpportunityMemberId__r.Account__r.Id IN :accountIds AND ProductAssignmentId__r.CommercialConditionId__r.RecordType.DeveloperName = \'CAP\'').toSOQL()
		);
	}

	public List<Product_Opportunity_Assignment__c> selectByParentProductId(Id productId) {
		Set<String> commConditionsRecTypeDevName = new Set<String>{'CAP', 'Late_Cancellation', 'No_Show'};
		
		return ( List<Product_Opportunity_Assignment__c> ) Database.query(
				newQueryFactory().
				selectField(Product_Opportunity_Assignment__c.Id).
				selectField(Product_Opportunity_Assignment__c.Marked_Poa__c).
				selectField('OpportunityMemberId__r.Account__c').
				selectField('OpportunityMemberId__r.Account__r.Name').		 										
				selectField('OpportunityMemberId__r.Account__r.Partner_Level__c').		 										
				selectField('OpportunityMemberId__r.Opportunity__c').			 													 										
				selectField('ProductAssignmentId__c').			 										
				selectField('ProductAssignmentId__r.CommercialConditionId__c').			 										
				selectField('ProductAssignmentId__r.CommercialConditionId__r.RecordTypeId').			 										
				selectField('ProductAssignmentId__r.CommercialConditionId__r.Amount__c').
				selectField('ProductAssignmentId__r.CommercialConditionId__r.CAP_Discount__c').
				selectField('ProductAssignmentId__r.CommercialConditionId__r.Visits_to_CAP__c'). 											
				selectField('ProductAssignmentId__r.ProductId__c').			 										
				selectField('ProductAssignmentId__r.ProductId__r.Parent_Product__c').			 										
				selectField('ProductAssignmentId__r.ProductId__r.Net_Transfer_Price__c').			 										
				selectField('ProductAssignmentId__r.Market_price__c').
				selectField('ProductAssignmentId__r.Late_Cancel__c').			 										
				selectField('ProductAssignmentId__r.Cap_Value__c').			 										
				selectField('ProductAssignmentId__r.Net_Transfer_Price__c').			 										
				selectField('ProductAssignmentId__r.No_Show_Fee__c').	
				setCondition('ProductAssignmentId__r.ProductId__r.Parent_Product__c =: productId AND ProductAssignmentId__r.CommercialConditionId__r.RecordType.DeveloperName IN :commConditionsRecTypeDevName').toSOQL()
		);
	}

	public List<Product_Opportunity_Assignment__c> selectSinglePartnerById(Id productId) {
		Set<String> commConditionsRecTypeDevName = new Set<String>{'CAP', 'Late_Cancellation', 'No_Show'};
		String singlePartnerLevel = constants.PARTNER_LEVEL_SINGLE_PARTNER;
		return ( List<Product_Opportunity_Assignment__c> ) Database.query(
				newQueryFactory().
				selectField(Product_Opportunity_Assignment__c.Id).
				selectField(Product_Opportunity_Assignment__c.Marked_Poa__c).
				selectField('OpportunityMemberId__r.Account__c').
				selectField('OpportunityMemberId__r.Account__r.Name').		 										
				selectField('OpportunityMemberId__r.Account__r.Partner_Level__c').		 										
				selectField('OpportunityMemberId__r.Opportunity__c').			 													 										
				selectField('ProductAssignmentId__c').			 										
				selectField('ProductAssignmentId__r.CommercialConditionId__c').			 										
				selectField('ProductAssignmentId__r.CommercialConditionId__r.RecordTypeId').			 										
				selectField('ProductAssignmentId__r.CommercialConditionId__r.Amount__c').
				selectField('ProductAssignmentId__r.CommercialConditionId__r.CAP_Discount__c').
				selectField('ProductAssignmentId__r.CommercialConditionId__r.Visits_to_CAP__c'). 											
				selectField('ProductAssignmentId__r.ProductId__c').			 										
				selectField('ProductAssignmentId__r.ProductId__r.Parent_Product__c').			 										
				selectField('ProductAssignmentId__r.ProductId__r.Net_Transfer_Price__c').			 										
				selectField('ProductAssignmentId__r.Market_price__c').
				selectField('ProductAssignmentId__r.Late_Cancel__c').			 										
				selectField('ProductAssignmentId__r.Cap_Value__c').			 										
				selectField('ProductAssignmentId__r.Net_Transfer_Price__c').			 										
				selectField('ProductAssignmentId__r.No_Show_Fee__c').	
				setCondition('ProductAssignmentId__r.ProductId__c =: productId AND ProductAssignmentId__r.CommercialConditionId__r.RecordType.DeveloperName IN :commConditionsRecTypeDevName AND OpportunityMemberId__r.Account__r.Partner_Level__c =: singlePartnerLevel').toSOQL()
		);
	}
	

	public List<Product_Opportunity_Assignment__c> selectByParentProductIdSet(Set<Id> productIdSet) {
		Set<String> commConditionsRecTypeDevName = new Set<String>{'CAP', 'Late_Cancellation', 'No_Show'};
		
		return ( List<Product_Opportunity_Assignment__c> ) Database.query(
				newQueryFactory().
				selectField(Product_Opportunity_Assignment__c.Id).
				selectField(Product_Opportunity_Assignment__c.Marked_Poa__c).
				selectField('OpportunityMemberId__r.Account__c').
				selectField('OpportunityMemberId__r.Account__r.Name').		 										
				selectField('OpportunityMemberId__r.Account__r.Gym_Type__c').
				selectField('OpportunityMemberId__r.Account__r.Partner_Level__c').
				selectField('OpportunityMemberId__r.Account__r.Exclusivity__c').
				selectField('OpportunityMemberId__r.Opportunity__c').			 													 										
				selectField('ProductAssignmentId__c').			 										
				selectField('ProductAssignmentId__r.CommercialConditionId__c').			 										
				selectField('ProductAssignmentId__r.CommercialConditionId__r.RecordTypeId').			 										
				selectField('ProductAssignmentId__r.CommercialConditionId__r.Amount__c').
				selectField('ProductAssignmentId__r.CommercialConditionId__r.CAP_Discount__c').
				selectField('ProductAssignmentId__r.CommercialConditionId__r.Visits_to_CAP__c'). 											
				selectField('ProductAssignmentId__r.ProductId__c').			 										
				selectField('ProductAssignmentId__r.ProductId__r.Parent_Product__c').			 										
				selectField('ProductAssignmentId__r.ProductId__r.Net_Transfer_Price__c').			 										
				selectField('ProductAssignmentId__r.Market_price__c').
				selectField('ProductAssignmentId__r.Late_Cancel__c').			 										
				selectField('ProductAssignmentId__r.Cap_Value__c').			 										
				selectField('ProductAssignmentId__r.Net_Transfer_Price__c').			 										
				selectField('ProductAssignmentId__r.No_Show_Fee__c').	
				selectField('ProductAssignmentId__r.CommercialConditionId__r.RecordType.DeveloperName').	
				setCondition('ProductAssignmentId__r.ProductId__r.Parent_Product__c IN :productIdSet AND ProductAssignmentId__r.CommercialConditionId__r.RecordType.DeveloperName IN :commConditionsRecTypeDevName').toSOQL()
		);
	}

	public List<Product_Opportunity_Assignment__c> selectByProductAssignmentIds(Set<Id> productAssignmentsIds) {
		return ( List<Product_Opportunity_Assignment__c> ) Database.query(
				newQueryFactory().
						selectField(Product_Opportunity_Assignment__c.Id).
						setCondition('ProductAssignmentId__c IN :productAssignmentsIds').toSOQL()
		);
	}

	public List<Product_Opportunity_Assignment__c> selectByProductAssignmentIdsWithCap(Set<Id> productAssignmentsIds) {
		return ( List<Product_Opportunity_Assignment__c> ) Database.query(
				newQueryFactory().
						selectField(Product_Opportunity_Assignment__c.Id).
						selectField('ProductAssignmentId__c').
						selectField('OpportunityMemberId__r.Account__c').
						setCondition('ProductAssignmentId__c IN :productAssignmentsIds AND ProductAssignmentId__r.CommercialConditionId__r.RecordType.DeveloperName = \'CAP\'').toSOQL()
		);
	}

	public List<Product_Opportunity_Assignment__c> selectAllCommercialConditionsByProductId(Id productId) {
		return ( List<Product_Opportunity_Assignment__c> ) Database.query(
				newQueryFactory().
						selectField('OpportunityMemberId__r.Account__c').
						selectField('OpportunityMemberId__r.Account__r.Wishlist__c').
						selectField('OpportunityMemberId__r.Opportunity__c').
						selectField('ProductAssignmentId__c').
						selectField('ProductAssignmentId__r.CommercialConditionId__c').
						selectField('ProductAssignmentId__r.CommercialConditionId__r.RecordTypeId').
						selectField('ProductAssignmentId__r.CommercialConditionId__r.RecordType.DeveloperName').
						selectField('ProductAssignmentId__r.CommercialConditionId__r.Amount__c').
						selectField('ProductAssignmentId__r.CommercialConditionId__r.CAP_Discount__c').
						selectField('ProductAssignmentId__r.CommercialConditionId__r.Discount__c').
						selectField('ProductAssignmentId__r.CommercialConditionId__r.Discount1__c').
						selectField('ProductAssignmentId__r.CommercialConditionId__r.Discount2__c').
						selectField('ProductAssignmentId__r.CommercialConditionId__r.Discount3__c').
						selectField('ProductAssignmentId__r.CommercialConditionId__r.Discount4__c').
						selectField('ProductAssignmentId__r.CommercialConditionId__r.End_Date__c').
						selectField('ProductAssignmentId__r.CommercialConditionId__r.Exclusivity__c').
						selectField('ProductAssignmentId__r.CommercialConditionId__r.Exclusivity_Fee__c').
						selectField('ProductAssignmentId__r.CommercialConditionId__r.Fee_Percentage__c').
						selectField('ProductAssignmentId__r.CommercialConditionId__r.Frequency__c').
						selectField('ProductAssignmentId__r.CommercialConditionId__r.Installments__c').
						selectField('ProductAssignmentId__r.CommercialConditionId__r.Integration_Fee_Deduction__c').
						selectField('ProductAssignmentId__r.CommercialConditionId__r.Is_It_Network_Applied__c').
						selectField('ProductAssignmentId__r.CommercialConditionId__r.Late_cancel__c').
						selectField('ProductAssignmentId__r.CommercialConditionId__r.Membership_Type__c').
						selectField('ProductAssignmentId__r.CommercialConditionId__r.Network_Value__c').
						selectField('ProductAssignmentId__r.CommercialConditionId__r.No_show_fee__c').
						selectField('ProductAssignmentId__r.CommercialConditionId__r.Number_of_Thresholds__c').
						selectField('ProductAssignmentId__r.CommercialConditionId__r.Restriction__c').
						selectField('ProductAssignmentId__r.CommercialConditionId__r.Start_Date__c').
						selectField('ProductAssignmentId__r.CommercialConditionId__r.Value__c').
						selectField('ProductAssignmentId__r.CommercialConditionId__r.Value1__c').
						selectField('ProductAssignmentId__r.CommercialConditionId__r.Value2__c').
						selectField('ProductAssignmentId__r.CommercialConditionId__r.Value3__c').
						selectField('ProductAssignmentId__r.CommercialConditionId__r.Value4__c').
						selectField('ProductAssignmentId__r.CommercialConditionId__r.Visits_to_CAP__c').
						selectField('ProductAssignmentId__r.ProductId__c').
						selectField('ProductAssignmentId__r.ProductId__r.Type__c').
						selectField('ProductAssignmentId__r.ProductId__r.Price_Visits_Month_Package_Selected__c').
						selectField('ProductAssignmentId__r.ProductId__r.PR_Activity__c').
						selectField('ProductAssignmentId__r.ProductId__r.Package_Type__c').
						selectField('ProductAssignmentId__r.ProductId__r.Max_Monthly_Visit_Per_User__c').
						selectField('ProductAssignmentId__r.ProductId__r.CurrencyIsoCode').
						selectField('ProductAssignmentId__r.ProductId__r.Name').
						selectField('ProductAssignmentId__r.ProductId__r.Max_Weekly_Times__c').
						selectField('ProductAssignmentId__r.Market_price__c').
						selectField('ProductAssignmentId__r.Late_Cancel__c').
						selectField('ProductAssignmentId__r.Cap_Value__c').
						selectField('ProductAssignmentId__r.Net_Transfer_Price__c').
						selectField('ProductAssignmentId__r.No_Show_Fee__c').
						setCondition('ProductAssignmentId__r.ProductId__c =: productId').toSOQL()
		);
	}

	public List<Product_Opportunity_Assignment__c> selectByAccountAndOpportunity( Id accountId, Id opportunityId ) {
		return ( List<Product_Opportunity_Assignment__c> ) Database.query(
				newQueryFactory().
				selectField(Product_Opportunity_Assignment__c.Id).
				selectField(Product_Opportunity_Assignment__c.ProductAssignmentId__c).
				selectField(Product_Opportunity_Assignment__c.OpportunityMemberId__c).
				selectField('ProductAssignmentId__r.ProductId__c').
				selectField('ProductAssignmentId__r.ProductId__r.Parent_Product__c').
				selectField('ProductAssignmentId__r.CommercialConditionId__c').
				selectField('ProductAssignmentId__r.CommercialConditionId__r.RecordType.DeveloperName').
				selectField('ProductAssignmentId__r.Commercial_Condition_Type_Dev_Name__c').
				selectField('OpportunityMemberId__r.Account__c').
				setCondition('OpportunityMemberId__r.Account__c =: accountId AND OpportunityMemberId__r.Opportunity__c =: opportunityId AND ProductAssignmentId__r.ProductId__c != null').toSOQL()
		);
	}

	public List<Product_Opportunity_Assignment__c> selectByParamsToDeleteOppMember( Id opportunityId, Id accountId, Id oppMemberId, Set<Id> prodAssignIds ) {
		return ( List<Product_Opportunity_Assignment__c> ) Database.query(
				newQueryFactory(true).	
				selectField('OpportunityMemberId__c').		 
				setCondition('OpportunityMemberId__r.Opportunity__c =: opportunityId AND OpportunityMemberId__r.Account__c =: accountId AND OpportunityMemberId__c =: oppMemberId AND ProductAssignmentId__c NOT IN: prodAssignIds').toSOQL()
		);
	}

	public List<Product_Opportunity_Assignment__c> selectByAccountsAndOpportunity( Set<Id> accountIds, Id opportunityId ) {
		return ( List<Product_Opportunity_Assignment__c> ) Database.query(
				newQueryFactory().
				selectField(Product_Opportunity_Assignment__c.Id).
				selectField(Product_Opportunity_Assignment__c.ProductAssignmentId__c).
				selectField(Product_Opportunity_Assignment__c.OpportunityMemberId__c).
				selectField('ProductAssignmentId__r.ProductId__c').
				selectField('ProductAssignmentId__r.ProductId__r.Parent_Product__c').
				selectField('ProductAssignmentId__r.CommercialConditionId__c').
				selectField('ProductAssignmentId__r.CommercialConditionId__r.RecordType.DeveloperName').
				selectField('ProductAssignmentId__r.Commercial_Condition_Type_Dev_Name__c').
				selectField('OpportunityMemberId__r.Account__c').
				setCondition('OpportunityMemberId__r.Account__c IN: accountIds AND OpportunityMemberId__r.Opportunity__c =: opportunityId AND ProductAssignmentId__r.ProductId__c != null').toSOQL()
		);
	}


	public List<Product_Opportunity_Assignment__c> selectByOpportunityWithProduct( Id opportunityId ) {
		return ( List<Product_Opportunity_Assignment__c> ) Database.query(
				newQueryFactory().
				selectField(Product_Opportunity_Assignment__c.Id).
				selectField(Product_Opportunity_Assignment__c.ProductAssignmentId__c).
				selectField(Product_Opportunity_Assignment__c.OpportunityMemberId__c).
				selectField('ProductAssignmentId__r.ProductId__c').
				selectField('ProductAssignmentId__r.ProductId__r.Parent_Product__c').
				selectField('ProductAssignmentId__r.CommercialConditionId__c').
				selectField('ProductAssignmentId__r.CommercialConditionId__r.RecordType.DeveloperName').
				selectField('ProductAssignmentId__r.Commercial_Condition_Type_Dev_Name__c').
				selectField('OpportunityMemberId__r.Account__c').
				selectField('OpportunityMemberId__r.Account__r.Partner_Level__c').
				setCondition('OpportunityMemberId__r.Opportunity__c =: opportunityId AND ProductAssignmentId__r.ProductId__c != null').toSOQL()
		);
	}

	public List<Product_Opportunity_Assignment__c> byOppIdSetWithProduct( Set<Id> opportunityId ) {
		return ( List<Product_Opportunity_Assignment__c> ) Database.query(
				newQueryFactory().
				selectField(Product_Opportunity_Assignment__c.Id).
				selectField(Product_Opportunity_Assignment__c.ProductAssignmentId__c).
				selectField(Product_Opportunity_Assignment__c.OpportunityMemberId__c).
				selectField('ProductAssignmentId__r.ProductId__c').
				selectField('ProductAssignmentId__r.ProductId__r.Parent_Product__c').
				selectField('ProductAssignmentId__r.ProductId__r.Name').
				selectField('ProductAssignmentId__r.CommercialConditionId__c').
				selectField('ProductAssignmentId__r.CommercialConditionId__r.RecordType.DeveloperName').
				selectField('ProductAssignmentId__r.Commercial_Condition_Type_Dev_Name__c').
				selectField('OpportunityMemberId__r.Account__c').
				selectField('OpportunityMemberId__r.Account__r.Name').
				selectField('OpportunityMemberId__r.Account__r.Partner_Level__c').
				selectField('OpportunityMemberId__r.Opportunity__c').
				setCondition('OpportunityMemberId__r.Opportunity__c IN :opportunityId AND ProductAssignmentId__r.ProductId__c != null').toSOQL()
		);
	}

	private Set<String> getAllFieldsFromProductOpportunityAssignment(){
		Map<String, Schema.SObjectField> productOpportunityAssigFields = Schema.getGlobalDescribe().get('Product_Opportunity_Assignment__c').getDescribe().fields.getMap();
        return productOpportunityAssigFields.keySet();
    }

	// used in the grid component inside product item page
	public List<Product_Opportunity_Assignment__c> selectOpportunityByProductId (Id productId) {
		return ( List<Product_Opportunity_Assignment__c> ) Database.query(
				newQueryFactory().
						selectField('OpportunityMemberId__r.Account__r.Name').
						selectField('OpportunityMemberId__r.Opportunity__r.Name').
						selectField('OpportunityMemberId__r.Opportunity__r.StageName').
						selectField('OpportunityMemberId__r.Opportunity__r.CloseDate').
						selectField('ProductAssignmentId__r.ProductId__c').
						setCondition('ProductAssignmentId__r.ProductId__c =: productId').toSOQL()
		);
	}

	public List<Product_Opportunity_Assignment__c> selectByOpportunitybyProductId(Set<Id> productIds) {
		return ( List<Product_Opportunity_Assignment__c> ) Database.query(
				newQueryFactory().
				selectField('OpportunityMemberId__r.Opportunity__r.RecordType.DeveloperName').
				setCondition('ProductAssignmentId__r.ProductId__c =: productIds').
				setLimit(1).toSOQL()
		);
	}

}