/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 03-07-2022
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/
@IsTest
public with sharing class SearchCustomLookupControllerTest {
 
    @TestSetup
    static void setupData(){

        Id systemAdminProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1].Id;

        User userExample = new User();
        userExample.Username = 'searchcustomlookupcontrollertest@test.com';
        userExample.FirstName = 'SeachUser';
        userExample.LastName = 'Last';
        userExample.Alias = 'Alias';
        userExample.Email = 'searchcustomlookupcontrollertest@test.com';
        userExample.TimeZoneSidKey = 'America/New_York';
        userExample.LocaleSidKey = 'en_US';
        userExample.EmailEncodingKey = 'ISO-8859-1';
        userExample.ProfileId = systemAdminProfileId;
        userExample.LanguageLocaleKey = 'en_US';

        insert userExample;

        Account lAcc = PartnerDataFactory.newAccount();
        Database.insert(lAcc);

        Contact lContact = PartnerDataFactory.newContact(lAcc.Id);
        Database.insert(lContact);

        Bank_Account__c lBankAcct = PartnerDataFactory.newBankAcct();
        Database.insert(lBankAcct);
    }


    @IsTest
    private static void userSearch(){
        User aUser = [SELECT Id, Name FROM USER WHERE Email='searchcustomlookupcontrollertest@test.com' LIMIT 1];
        Id [] fixedSearchResults= new Id[1];
        fixedSearchResults[0] = aUser.Id;
        Test.setFixedSearchResults(fixedSearchResults);
        Test.startTest();
            List<LookupSearchResult> results = SearchCustomLookupController.userSearch('SeachUser', new List<String>{});
            System.assertEquals(results.get(0).getId(), aUser.Id);
        Test.stopTest();
    }

    @IsTest
    private static void accountSearch_Test() {
        Account account = [SELECT Id FROM Account LIMIT 1];
        Test.setFixedSearchResults(new List<String>{account.Id});
        Test.startTest();
        List<LookupSearchResult> results = SearchCustomLookupController.searchAccount('Account', new List<String>());
        system.assertEquals(1, results.size());
        Test.stopTest();
    }

    @IsTest
    private static void contactSearch_Test() {
        Contact contact = [SELECT Id FROM Contact LIMIT 1];
        Test.setFixedSearchResults(new List<String>{contact.Id});
        Test.startTest();
        List<LookupSearchResult> results = SearchCustomLookupController.searchContact('Contact', new List<String>());
        system.assertEquals(1, results.size());
        Test.stopTest();
    }

    @IsTest
    private static void bankAccountSearch_Test() {
        Bank_Account__c bankAccount = [SELECT Id FROM Bank_Account__c LIMIT 1];
        Test.setFixedSearchResults(new List<String>{bankAccount.Id});
        Test.startTest();
        List<LookupSearchResult> results = SearchCustomLookupController.bankAccountSearch('Test', new List<String>());
        system.assertEquals(1, results.size());
        Test.stopTest();
    }

    
    @IsTest
    private static void getUserRecentlyViewed(){
        Test.startTest();
            List<LookupSearchResult> results = SearchCustomLookupController.getUserRecentlyViewed();
        Test.stopTest();
    }
}