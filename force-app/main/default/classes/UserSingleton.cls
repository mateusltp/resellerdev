public with sharing class UserSingleton {
    
    private  static UserSingleton instance;
    private User user = new User();

    private UserSingleton(Id userId){
        this.user = [ SELECT Id, Name, ContactId, Email, Phone, IsActive, AccountId, CountryCode FROM User WHERE Id =: userId LIMIT 1 ];
    }

    public static UserSingleton getInstance(Id userId){
        if(instance == null)
            instance = new UserSingleton(userId);
        return instance;
    }

    public Id getId(){
        return this.user.Id;
    }

    public String getName(){
        return this.user.Name;
    }

    public Id getContactId(){
        return this.user.ContactId;
    }

    public String getEmail(){
        return this.user.Email;
    }

    public String getPhone(){
        return this.user.Phone;
    }

    public String getAccountId(){
        return this.user.AccountId;
    }

    public Boolean getIsActive(){
        return this.user.IsActive;
    }

    public String getCountryCode(){
        return this.user.CountryCode;
    }
    
}