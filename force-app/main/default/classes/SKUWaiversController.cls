public with sharing class SKUWaiversController {

    private class NullIdException extends Exception{

    }
    
    @AuraEnabled
    public static List<Waiver__c> getWaivers(String productId){
        try {

            List<String> recordTypeNames = new List<String>{'Compelling Waiver', 'Months after Launch'};

            if(!String.isBlank(productId)) {
                return [SELECT Id, RecordTypeId, Position__c, Percentage__c, Duration__c 
                        FROM Waiver__c 
                        WHERE Quote_Line_Item__c = :productId AND RecordType.Name IN :recordTypeNames 
                        ORDER BY Position__c ASC];
            }
            else {
                throw new NullIdException('The productId is blank or null');
            }  
            
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

}