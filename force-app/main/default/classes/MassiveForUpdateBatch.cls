public class MassiveForUpdateBatch {
    
    public static void massiveForUpdateList(List<Account_Request__c> checkAccountRequest_list, Set<Id> massiveAccountRequestId_set){
        
        List<Massive_Account_Request__c> massiveForUpdate_list = new List<Massive_Account_Request__c>();
        
        if(!checkAccountRequest_list.isEmpty())
            for(Account_Request__c accountRequest : checkAccountRequest_list)
            if(massiveAccountRequestId_set.contains(accountRequest.Massive_Account_Request__c))
            massiveAccountRequestId_set.remove(accountRequest.Massive_Account_Request__c);
        
        
        for(Id massiveId : massiveAccountRequestId_set)
            massiveForUpdate_list.add(new Massive_Account_Request__c( Id = massiveId, Status__c = 'Looking for Events'));
        
        if(!massiveForUpdate_list.isEmpty())
            update massiveForUpdate_list;
        
    }
    
    
}