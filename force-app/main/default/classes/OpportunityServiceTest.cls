/**
 * @description       : 
 * @author            : Jorge Stevaux - JZRX@gft.com
 * @group             : 
 * @last modified on  : 05-13-2022
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/
@IsTest
public with sharing class OpportunityServiceTest
{
    private static final PS_Constants constants = PS_Constants.getInstance();
    //this is temporary
    @TestSetup
    static void setupData(){
        String recordTypePartnerFlow = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Account').getRecordTypeId();
        Account partnerAcc = PartnerDataFactory.newAccount();
        partnerAcc.RecordTypeId = recordTypePartnerFlow;
        partnerAcc.CRM_ID__c = '1231';
        partnerAcc.Legal_Document_Type__c = 'CNPJ';
        //partnerAcc.Id_Company__c = 'XX.XXX.XXX/XXXX-XX';
        partnerAcc.Legal_Title__c = 'SAA';
        partnerAcc.BillingCountryCode = 'BR';
        partnerAcc.BillingPostalCode = '1223';
        partnerAcc.ShippingCountryCode = 'US';
        partnerAcc.ShippingCountry = 'United States';
        partnerAcc.ShippingState = 'Washington';
        partnerAcc.UUID__c = new Uuid().getValue();
        partnerAcc.Description = 'dasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasda';
        INSERT partnerAcc;
             
        String recordTypePartnerContactFlow = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Contact').getRecordTypeId();
        Contact partnerContact = PartnerDataFactory.newContact(partnerAcc.Id);
        partnerContact.RecordTypeId = recordTypePartnerContactFlow;        
        partnerContact.Type_of_Contact__c = 'Decision Maker';
        partnerContact.FirstName = 'Admin';
        partnerContact.UUID__c = new Uuid().getValue();
        INSERT partnerContact;    

        Opportunity aOpp = PartnerDataFactory.newOpportunity( partnerAcc.Id, 'Partner_Flow_Opportunity'); 
        INSERT aOpp;

        Quote proposal = PartnerDataFactory.newQuote( aOpp );
        proposal.RecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Proposal').getRecordTypeId();
        proposal.Signed__c = Date.Today() - 100;
        proposal.End_Date__c = Date.Today();
        proposal.Start_Date__c = Date.Today() - 100;
        INSERT proposal;


        Account_Opportunity_Relationship__c oppMember = PartnerDataFactory.newAcctOppRel(partnerAcc.Id, aOpp.Id);
        oppMember.RecordTypeId = Schema.SObjectType.Account_Opportunity_Relationship__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Opportunity_Member').getRecordTypeId();
        INSERT oppMember;

        Product_Item__c aProd = PartnerDataFactory.newProduct( aOpp );
        aProd.Type__c = 'Live';
        aProd.Net_Transfer_Price__c = 120;
        aProd.RecordTypeId = Schema.SObjectType.Product_Item__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product').getRecordTypeId();
        aProd.Opportunity__c = null;       
        INSERT aProd;

        Commercial_Condition__c aComm = new Commercial_Condition__c();
        aComm.RecordTypeId = Schema.SObjectType.Commercial_Condition__c.getRecordTypeInfosByDeveloperName().get('CAP').getRecordTypeId();
        aComm.Amount__c = 120;
        INSERT aComm;

        Product_Assignment__c prodAssign = new Product_Assignment__c();
        prodAssign.RecordTypeId = Schema.SObjectType.Product_Assignment__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product_Assignment').getRecordTypeId();
        prodAssign.ProductId__c = aProd.Id;
        prodAssign.CommercialConditionId__c = aComm.Id;
        INSERT prodAssign;


        Product_Opportunity_Assignment__c prodOppAssign = new Product_Opportunity_Assignment__c();
        prodOppAssign.RecordTypeId = Schema.SObjectType.Product_Opportunity_Assignment__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product_Opportunity_Assignment').getRecordTypeId();
        prodOppAssign.OpportunityMemberId__c = oppMember.Id;
        prodOppAssign.ProductAssignmentId__c = prodAssign.Id;
        INSERT prodOppAssign;

        Threshold__c threshold = new Threshold__c();
        threshold.recordTypeId = Schema.SObjectType.Threshold__c.getRecordTypeInfosByDeveloperName().get(constants.THRESHOLD_RT_PARTNER_FLOW).getRecordTypeId();
        threshold.Threshold_value_start__c = 10;
        threshold.Threshold_value_end__c = 20;
        threshold.Volume_discount_date__c = Date.today();
        INSERT threshold;

        Product_Threshold_Member__c prodThresholdMember = new Product_Threshold_Member__c();
        prodThresholdMember.recordTypeId = Schema.SObjectType.Product_Threshold_Member__c.getRecordTypeInfosByDeveloperName().get(constants.PRODUCT_THRESHOLD_MEMBER_RT_PARTNER_FLOW).getRecordTypeId();
        prodThresholdMember.Product__c = aProd.Id;
        prodThresholdMember.Threshold__c = threshold.Id;
        INSERT prodThresholdMember;

        Gym_Activity_Relationship__c gymAct = new Gym_Activity_Relationship__c();
        gymAct.Name = 'Test Act';
        INSERT gymAct;

        Product_Activity_Relationship__c prodActRel = new Product_Activity_Relationship__c();
        prodActRel.recordTypeId = Schema.SObjectType.Product_Activity_Relationship__c.getRecordTypeInfosByDeveloperName().get(constants.PRODUCT_ACTIVITY_RT_PARTNER_FLOW).getRecordTypeId();
        prodActRel.Gym_Activity_Relationship__c= gymAct.Id;
        prodActRel.Product__c = aProd.Id;
        INSERT prodActRel;

    }



    @IsTest
    private static void Partner_updateStageToLost_UnitTest(){

        //1. create fake data that you want to use
        Id mockOpportunityId = fflib_IDGenerator.generate(Opportunity.SObjectType);
        Set<Id> opportunityIds = new Set<Id>{mockOpportunityId};
        Id rtOppId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Partner_Wishlist_Renegotiation').getRecordTypeId();
        List<Opportunity> opportunityList = new List<Opportunity>{new Opportunity(Id=mockOpportunityId, recordtypeid=rtOppId)};
        //2. make fake mock instances you your class
        fflib_ApexMocks mocks = new fflib_ApexMocks();
        fflib_SObjectUnitOfWork mockUOW = (fflib_SObjectUnitOfWork) mocks.mock(fflib_SObjectUnitOfWork.class);
        //3. make fake returns for your classes methods you are calling
        mocks.startStubbing();
            ((fflib_SObjectUnitOfWork)mocks.doThrowWhen(new OpportunityService.OpportunityServiceException(), mockUOW)).commitWork(); //for void returns  
        mocks.stopStubbing();
        //4. set the mocks of your application
        Application.UnitOfWork.setMock(mockUOW);
        try{
            Test.startTest(); 
            OpportunityService.updateStageToLost(opportunityList);
            Test.stopTest();
        }
        catch(Exception e){
            System.assert(e instanceof OpportunityService.OpportunityServiceException);
        }
    }

    @IsTest
    private static void Partner_notifyOpportunityCancellation_UnitTest(){

        //1. create fake data that you want to use
        Id rtOppId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Partner_Wishlist_Renegotiation').getRecordTypeId();
        Id mockOpportunityId = fflib_IDGenerator.generate(Opportunity.SObjectType);
        Id mockUserId = fflib_IDGenerator.generate(User.SObjectType);
        Id mockEmailTemplateId = fflib_IDGenerator.generate(EmailTemplate.SObjectType);

        EmailTemplate mockEmailTemplate = new EmailTemplate (id=mockEmailTemplateId, Name = 'Waiting Cancelation Notification'); // plus any other fields that you want to set
        Id mockCustomNotificationTypeId = fflib_IDGenerator.generate(CustomNotificationType.SObjectType);        
        List<Opportunity> opportunityList = new List<Opportunity>{new Opportunity(Id=mockOpportunityId, recordtypeid=rtOppId, ownerId=mockUserId)};
        Id mockOweaId = fflib_IDGenerator.generate(OrgWideEmailAddress.SObjectType);
        OrgWideEmailAddress mockOwea = new OrgWideEmailAddress(id = mockOweaId, Address = 'global.b2b@gympass.com');

        //2. make fake mock instances you your class
        fflib_ApexMocks mocks = new fflib_ApexMocks();
        fflib_SObjectUnitOfWork mockUOW = (fflib_SObjectUnitOfWork) mocks.mock(fflib_SObjectUnitOfWork.class);
        CustomNotificationTypeSelector mockCustomNotificationTypeSelector = (CustomNotificationTypeSelector)mocks.mock(CustomNotificationTypeSelector.class);
        EmailTemplateSelector mockEmailTemplateSelector = (EmailTemplateSelector)mocks.mock(EmailTemplateSelector.class);
        OrgWideEmailAddressSelector mockOrgWideEmailAddressSelector = (OrgWideEmailAddressSelector)mocks.mock(OrgWideEmailAddressSelector.class);

        //3. make fake returns for your classes methods you are calling
        mocks.startStubbing();
            mocks.when(mockCustomNotificationTypeSelector.sObjectType()).thenReturn(CustomNotificationType.SObjectType);     
            mocks.when(mockEmailTemplateSelector.sObjectType()).thenReturn(EmailTemplate.SObjectType);          
            mocks.when(mockOrgWideEmailAddressSelector.sObjectType()).thenReturn(OrgWideEmailAddress.SObjectType);  
            mocks.when(mockOrgWideEmailAddressSelector.selectGlobalB2BOrgWideEmailAddress()).thenReturn(mockOwea);  
            mocks.when(mockCustomNotificationTypeSelector.selectCustomNotificationTypeByDeveloperName('Waiting_Cancelation_Notify')).thenReturn(new CustomNotificationType(Id=mockCustomNotificationTypeId)); 
            mocks.when(mockEmailTemplateSelector.selectEmailTemplateByName('Waiting Cancelation Notification')).thenReturn(mockEmailTemplate);          
        mocks.stopStubbing();

        //4. set the mocks of your application
        Application.UnitOfWork.setMock(mockUOW);
        Application.Selector.setMock(mockCustomNotificationTypeSelector);
        Application.Selector.setMock(mockEmailTemplateSelector);
        Application.Selector.setMock(mockOrgWideEmailAddressSelector);
        
        Test.startTest();
            OpportunityService.notifyOpportunityCancellation(opportunityList);
        Test.stopTest();
    }

     @IsTest
    private static void Partner_notifyOpportunityCancellationException_UnitTest(){

        //1. create fake data that you want to use
        Id rtOppId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Partner_Wishlist_Renegotiation').getRecordTypeId();
        Id mockOpportunityId = fflib_IDGenerator.generate(Opportunity.SObjectType);
        Id mockUserId = fflib_IDGenerator.generate(User.SObjectType);
        Id mockEmailTemplateId = fflib_IDGenerator.generate(EmailTemplate.SObjectType);

        EmailTemplate mockEmailTemplate = new EmailTemplate (id=mockEmailTemplateId, Name = 'Waiting Cancelation Notification'); // plus any other fields that you want to set
        Id mockCustomNotificationTypeId = fflib_IDGenerator.generate(CustomNotificationType.SObjectType);        
        List<Opportunity> opportunityList = new List<Opportunity>{new Opportunity(Id=mockOpportunityId, recordtypeid=rtOppId, ownerId=mockUserId)};
        Id mockOweaId = fflib_IDGenerator.generate(OrgWideEmailAddress.SObjectType);
        OrgWideEmailAddress mockOwea = new OrgWideEmailAddress(id = mockOweaId, Address = 'global.b2b@gympass.com');

        //2. make fake mock instances you your class
        fflib_ApexMocks mocks = new fflib_ApexMocks();
        fflib_SObjectUnitOfWork mockUOW = (fflib_SObjectUnitOfWork) mocks.mock(fflib_SObjectUnitOfWork.class);
        CustomNotificationTypeSelector mockCustomNotificationTypeSelector = (CustomNotificationTypeSelector)mocks.mock(CustomNotificationTypeSelector.class);
        EmailTemplateSelector mockEmailTemplateSelector = (EmailTemplateSelector)mocks.mock(EmailTemplateSelector.class);
        OrgWideEmailAddressSelector mockOrgWideEmailAddressSelector = (OrgWideEmailAddressSelector)mocks.mock(OrgWideEmailAddressSelector.class);

        //3. make fake returns for your classes methods you are calling
        mocks.startStubbing();
            mocks.when(mockCustomNotificationTypeSelector.sObjectType()).thenReturn(CustomNotificationType.SObjectType);     
            mocks.when(mockEmailTemplateSelector.sObjectType()).thenReturn(EmailTemplate.SObjectType);          
            mocks.when(mockOrgWideEmailAddressSelector.sObjectType()).thenReturn(OrgWideEmailAddress.SObjectType);  
            mocks.when(mockOrgWideEmailAddressSelector.selectGlobalB2BOrgWideEmailAddress()).thenReturn(mockOwea);  
            mocks.when(mockCustomNotificationTypeSelector.selectCustomNotificationTypeByDeveloperName('Waiting_Cancelation_Notify')).thenThrow(new OpportunityService.OpportunityServiceException()); 
            mocks.when(mockEmailTemplateSelector.selectEmailTemplateByName('Waiting Cancelation Notification')).thenReturn(mockEmailTemplate);          
        mocks.stopStubbing();

        //4. set the mocks of your application
        Application.UnitOfWork.setMock(mockUOW);
        Application.Selector.setMock(mockCustomNotificationTypeSelector);
        Application.Selector.setMock(mockEmailTemplateSelector);
        Application.Selector.setMock(mockOrgWideEmailAddressSelector);

        try{
            Test.startTest();
                OpportunityService.notifyOpportunityCancellation(opportunityList);
            Test.stopTest();
        }catch(Exception e){
            System.assert(e instanceof OpportunityService.OpportunityServiceException);
        }
    }

    @IsTest
    private static void Partner_openRenegotiation_UnitTest(){
                
        //1. create fake data that you want to use
        Id mockOpportunityId = fflib_IDGenerator.generate(Opportunity.SObjectType);
        Id mockAccountId = fflib_IDGenerator.generate(Account.SObjectType);

        Id mockOpportunityIdNoWon = fflib_IDGenerator.generate(Opportunity.SObjectType);
        Id mockAccountIdNoWon = fflib_IDGenerator.generate(Account.SObjectType);

        Opportunity mockOpp = new Opportunity(Id = mockOpportunityId, AccountId = mockAccountId, Name='New Opp');
        Opportunity mockOppWon = new Opportunity(Id = mockOpportunityId, AccountId = mockAccountId, Name='Won Opp', StageName='Lançado/Ganho');
              
        Opportunity mockOppNoWon = new Opportunity(Id = mockOpportunityId, AccountId = mockAccountIdNoWon, Name='New Opp');  
        
        Map<Id, Opportunity> mockAcctOpp = new Map<Id, Opportunity>{mockAccountId => mockOpp, mockAccountIdNoWon => mockOppNoWon};
        Map<Id, Opportunity> mockAcctOppWon = new Map<Id, Opportunity>{mockAccountId => mockOppWon};

        Schema.RecordTypeInfo mockOppDefaultRecordType =  Opportunity.sObjectType.getDescribe().getRecordTypeInfosByName().get('Partner - Small and Medium Renegotiation');


        Id mockQuoteId = fflib_IDGenerator.generate(Quote.SObjectType);
        List<Quote> mockProposalListByWonOpp = new List<Quote>{new Quote(id = mockQuoteId, OpportunityId = mockOpp.Id)};

        Id mockAccOppId = fflib_IDGenerator.generate(Account_Opportunity_Relationship__c.SObjectType);
        List<Account_Opportunity_Relationship__c> mockAccOppByWonOpp = new List<Account_Opportunity_Relationship__c>{new Account_Opportunity_Relationship__c(id=mockAccOppId, Opportunity__c= mockOpp.Id, Account__c =mockAccountId )};
        
        Id mockProdId = fflib_IDGenerator.generate(Product_Item__c.SObjectType);
        List<Product_Item__c> mockProdByWonOpp = new List<Product_Item__c>{new Product_Item__c(id=mockProdId, Opportunity__c= mockOpp.Id )};
        
        Id mockGymActId = fflib_IDGenerator.generate(Gym_Activity__c.SObjectType);
        List<Gym_Activity__c> mockGymActByProd = new List<Gym_Activity__c>{new Gym_Activity__c(id=mockGymActId, Product_Item__c = mockProdId)};

        //2. make fake mock instances you your class
        fflib_ApexMocks mocks = new fflib_ApexMocks();
        fflib_SObjectUnitOfWork mockUOW = (fflib_SObjectUnitOfWork) mocks.mock(fflib_SObjectUnitOfWork.class);
        OpportunitySelector mockOpportunitySelector = (OpportunitySelector)mocks.mock(OpportunitySelector.class);
        ProposalSelector mockProposalSelector = (ProposalSelector)mocks.mock(ProposalSelector.class);        
        AccountOpportunitySelector mockAccountOpportunitySelector = (AccountOpportunitySelector)mocks.mock(AccountOpportunitySelector.class);        
        ProductItemSelector mockProductItemSelector = (ProductItemSelector)mocks.mock(ProductItemSelector.class);        
        GymActivitySelector mockGymActivitySelector = (GymActivitySelector)mocks.mock(GymActivitySelector.class);

        //3. make fake returns for your classes methods you are calling
        mocks.startStubbing();

            //oppSelector methods
            mocks.when(mockOpportunitySelector.sObjectType()).thenReturn(Opportunity.SObjectType);
        
            mocks.when(mockOpportunitySelector.selectLastWonToClone(mockAcctOpp.keySet())).thenReturn(mockAcctOppWon);     
            mocks.when(mockOpportunitySelector.getDefaultRecordType()).thenReturn(mockOppDefaultRecordType);

            //proposalSelector methods
            mocks.when(mockProposalSelector.sObjectType()).thenReturn(Quote.SObjectType);
            mocks.when(mockProposalSelector.selectByOppIdToClone(new Set<Id>{mockOpp.Id})).thenReturn(mockProposalListByWonOpp);

            //AccountOpportunitySelector methods
            mocks.when(mockAccountOpportunitySelector.sObjectType()).thenReturn(Account_Opportunity_Relationship__c.SObjectType);
            mocks.when(mockAccountOpportunitySelector.selectByOppIdToClone(new Set<Id>{mockOpp.Id})).thenReturn(mockAccOppByWonOpp);

            //ProductItemSelector methods
            mocks.when(mockProductItemSelector.sObjectType()).thenReturn(Product_Item__c.SObjectType);
            mocks.when(mockProductItemSelector.selectByOppIdToClone(new Set<Id>{mockOpp.Id})).thenReturn(mockProdByWonOpp);

            //GymActivitySelector methods
            mocks.when(mockGymActivitySelector.sObjectType()).thenReturn(Gym_Activity__c.SObjectType);   
            mocks.when(mockGymActivitySelector.selectByProductIdToClone(new Set<Id>{mockProdId})).thenReturn(mockGymActByProd);          

        mocks.stopStubbing();

        //4. set the mocks of your application
        Application.UnitOfWork.setMock(mockUOW);
        Application.Selector.setMock(mockOpportunitySelector);
        Application.Selector.setMock(mockProposalSelector);
        Application.Selector.setMock(mockAccountOpportunitySelector);
        Application.Selector.setMock(mockProductItemSelector);
        Application.Selector.setMock(mockGymActivitySelector);
        
        Test.startTest();
            OpportunityService.openRenegotiation(mockAcctOpp, 'Partner_Wishlist_Renegotiation');
            ((OpportunitySelector)mocks.verify(mockOpportunitySelector)).selectLastWonToClone(mockAcctOpp.keySet());   

        Test.stopTest();
        
    }


    @IsTest
    private static void Partner_openRenegotiationException_UnitTest(){
                
        //1. create fake data that you want to use
        Id mockOpportunityId = fflib_IDGenerator.generate(Opportunity.SObjectType);
        Id mockAccountId = fflib_IDGenerator.generate(Account.SObjectType);

        Id mockOpportunityIdNoWon = fflib_IDGenerator.generate(Opportunity.SObjectType);
        Id mockAccountIdNoWon = fflib_IDGenerator.generate(Account.SObjectType);

        Opportunity mockOpp = new Opportunity(Id = mockOpportunityId, AccountId = mockAccountId, Name='New Opp');             
        Opportunity mockOppWon = new Opportunity(Id = mockOpportunityId, AccountId = mockAccountId, Name='Won Opp', StageName='Lançado/Ganho');
              
        Opportunity mockOppNoWon = new Opportunity(Id = mockOpportunityId, AccountId = mockAccountIdNoWon, Name='New Opp');  
        
        Map<Id, Opportunity> mockAcctOpp = new Map<Id, Opportunity>{mockAccountId => mockOpp, mockAccountIdNoWon => mockOppNoWon};
        Map<Id, Opportunity> mockAcctOppWon = new Map<Id, Opportunity>{mockAccountId => mockOppWon};

        Schema.RecordTypeInfo mockOppDefaultRecordType =  Opportunity.sObjectType.getDescribe().getRecordTypeInfosByName().get('Partner - Small and Medium Renegotiation');


        Id mockQuoteId = fflib_IDGenerator.generate(Quote.SObjectType);
        List<Quote> mockProposalListByWonOpp = new List<Quote>{new Quote(id = mockQuoteId, OpportunityId = mockOpp.Id)};

        Id mockAccOppId = fflib_IDGenerator.generate(Account_Opportunity_Relationship__c.SObjectType);
        List<Account_Opportunity_Relationship__c> mockAccOppByWonOpp = new List<Account_Opportunity_Relationship__c>{new Account_Opportunity_Relationship__c(id=mockAccOppId, Opportunity__c= mockOpp.Id, Account__c =mockAccountId )};
        
        Id mockProdId = fflib_IDGenerator.generate(Product_Item__c.SObjectType);
        List<Product_Item__c> mockProdByWonOpp = new List<Product_Item__c>{new Product_Item__c(id=mockProdId, Opportunity__c= mockOpp.Id )};
        
        Id mockGymActId = fflib_IDGenerator.generate(Gym_Activity__c.SObjectType);
        List<Gym_Activity__c> mockGymActByProd = new List<Gym_Activity__c>{new Gym_Activity__c(id=mockGymActId, Product_Item__c = mockProdId)};

        //2. make fake mock instances you your class
        fflib_ApexMocks mocks = new fflib_ApexMocks();
        fflib_SObjectUnitOfWork mockUOW = (fflib_SObjectUnitOfWork) mocks.mock(fflib_SObjectUnitOfWork.class);
        OpportunitySelector mockOpportunitySelector = (OpportunitySelector)mocks.mock(OpportunitySelector.class);
        ProposalSelector mockProposalSelector = (ProposalSelector)mocks.mock(ProposalSelector.class);        
        AccountOpportunitySelector mockAccountOpportunitySelector = (AccountOpportunitySelector)mocks.mock(AccountOpportunitySelector.class);        
        ProductItemSelector mockProductItemSelector = (ProductItemSelector)mocks.mock(ProductItemSelector.class);        
        GymActivitySelector mockGymActivitySelector = (GymActivitySelector)mocks.mock(GymActivitySelector.class);

        //3. make fake returns for your classes methods you are calling
        mocks.startStubbing();

            //oppSelector methods
            mocks.when(mockOpportunitySelector.sObjectType()).thenReturn(Opportunity.SObjectType);
        
            mocks.when(mockOpportunitySelector.selectLastWonToClone(mockAcctOpp.keySet())).thenReturn(mockAcctOppWon);     
            mocks.when(mockOpportunitySelector.getDefaultRecordType()).thenReturn(mockOppDefaultRecordType);

            //proposalSelector methods
            mocks.when(mockProposalSelector.sObjectType()).thenReturn(Quote.SObjectType);
            mocks.when(mockProposalSelector.selectByOppIdToClone(new Set<Id>{mockOpp.Id})).thenReturn(mockProposalListByWonOpp);

            //AccountOpportunitySelector methods
            mocks.when(mockAccountOpportunitySelector.sObjectType()).thenReturn(Account_Opportunity_Relationship__c.SObjectType);
            mocks.when(mockAccountOpportunitySelector.selectByOppIdToClone(new Set<Id>{mockOpp.Id})).thenReturn(mockAccOppByWonOpp);

            //ProductItemSelector methods
            mocks.when(mockProductItemSelector.sObjectType()).thenReturn(Product_Item__c.SObjectType);
            mocks.when(mockProductItemSelector.selectByOppIdToClone(new Set<Id>{mockOpp.Id})).thenReturn(mockProdByWonOpp);

            //GymActivitySelector methods
            mocks.when(mockGymActivitySelector.sObjectType()).thenReturn(Gym_Activity__c.SObjectType);   
            mocks.when(mockGymActivitySelector.selectByProductIdToClone(new Set<Id>{mockProdId})).thenReturn(mockGymActByProd);          
            ((fflib_SObjectUnitOfWork)mocks.doThrowWhen(new OpportunityService.OpportunityServiceException(), mockUOW)).commitWork(); //for void returns  
  
        mocks.stopStubbing();

        //4. set the mocks of your application
        Application.UnitOfWork.setMock(mockUOW);
        Application.Selector.setMock(mockOpportunitySelector);
        Application.Selector.setMock(mockProposalSelector);
        Application.Selector.setMock(mockAccountOpportunitySelector);
        Application.Selector.setMock(mockProductItemSelector);
        Application.Selector.setMock(mockGymActivitySelector);
     
        try{
            Test.startTest();
                OpportunityService.openRenegotiation(mockAcctOpp, 'Partner_Wishlist_Renegotiation');
            Test.stopTest();
        }catch(Exception e){
            System.assert(e instanceof OpportunityService.OpportunityServiceException);
        }
    }
 
    //temporary
    /*@IsTest
    private static void Partner_validatePartnerOpportunityForTagus(){

        Opportunity opp = [SELECT ID, RecordTypeId, AccountId FROM Opportunity LIMIT 1];
        Test.startTest();

            opp.StageName = 'Lançado/Ganho';
            //OpportunityPartnerServiceImpl.validateOpportunityForTagus(new List<Opportunity>{opp});
            //update   opp;
               
        Test.StopTest();
 
    }*/

    //temporary
   /* @IsTest
    private static void Partner_setAccountTypePartnerOnOppWon(){

        Opportunity opp = [SELECT AccountId, ID, StageName, RecordTypeId FROM Opportunity LIMIT 1];
        Test.startTest();

        opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Gyms_Small_Medium_Partner').getRecordTypeId();
        opp.StageName = 'Lançado/Ganho';
        opp.IsAutomationBypassed__c = true;
        opp.Type = 'Partnership';
        opp.CMS_Used__c = 'No';
        opp.Bank_Account_Related__c = true;
        update opp;

        Test.StopTest();
        Account acc = [SELECT Type FROM Account WHERE Id = :opp.AccountId];
        system.assertEquals('Partner', acc.Type);

    } */

     @isTest
    static void AccountW9(){
      
        List<Account> accList = new List<Account>();
        Id recordTypePartner = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gym_Partner').getRecordTypeId();

        Account fileAccount = new Account();

        fileAccount.Name = 'Partner_Upload';
        fileAccount.RecordTypeId = recordTypePartner;
        fileAccount.Types_of_ownership__c = Label.privateOwnerPicklist;
        fileAccount.GP_Status__c = 'Pending Approval';
        fileAccount.ShippingState = 'Minas Gerais';
        fileAccount.ShippingStreet = 'RUX ASSS';
        fileAccount.ShippingCity = 'CITY CITY';
        fileAccount.ShippingCountry = 'Brazil';
        fileAccount.Gym_Type__c = 'Studios';
        fileAccount.Subscription_Type__c = 'Value per class';
        fileAccount.Subscription_Period__c = 'Monthy value';
        fileAccount.Subscription_Type_Estimated_Price__c	= 100;
        fileAccount.Has_market_cannibalization__c = 'No';
        fileAccount.Exclusivity__c = 'Yes';
        fileAccount.Exclusivity_End_Date__c = Date.today().addYears(1);
        fileAccount.Exclusivity_Partnership__c = 'Full Exclusivity';
        fileAccount.Exclusivity_Restrictions__c= 'No';
        fileAccount.Website = 'testing@tesapex.com';
        fileAccount.Gym_Email__c = 'gymemail@apex.com';
        fileAccount.Phone = '3222123123';
        accList.add(fileAccount);
        insert fileAccount;
        
        //Create Opportunity by RecordType
        List<Opportunity> oppList = new List<Opportunity>();
        Id recordTypeIdSmall = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Gyms_Small_Medium_Partner').getRecordTypeId();
        Id recordTypeIdWishList = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Gyms_Wishlist_Partner').getRecordTypeId();
        
        Opportunity accOpp = new Opportunity();
        accOpp.recordTypeId = recordTypeIdSmall;
        accOpp.AccountId = fileAccount.id;
        accOpp.Name = 'Upload_Test_Small';      
        accOpp.Gym_agreed_to_an_API_integration__c	= 'Yes';
        accOpp.Integration_Fee_Deduction__c = 'No';
        accOpp.CloseDate = Date.today();
        accOpp.Success_Look_Like__c = 'Yes';
        accOpp.Success_Look_Like_Description__c	= 'Money money';
        accOpp.StageName = 'Qualification';
        accOpp.Type	= 'Expansion';     
        accOpp.CurrencyIsoCode = 'BRL';
        accOpp.Gympass_Plus__c = 'Yes';
        accOpp.Partner_Opp_File_url__c = 'Small_OppFile.jpg';
        INSERT accOpp;

        Opportunity accOpp2 = new Opportunity();
        accOpp2.recordTypeId = recordTypeIdSmall;
        accOpp2.AccountId = fileAccount.id;
        accOpp2.Name = 'Upload_Test_Whishlist';
        accOpp2.Gym_agreed_to_an_API_integration__c	= 'Yes';
        accOpp2.Integration_Fee_Deduction__c = 'No';
        accOpp2.CloseDate = Date.today();
        accOpp2.Success_Look_Like__c = 'Yes';
        accOpp2.Success_Look_Like_Description__c	= 'Money money';
        accOpp2.StageName = 'Qualification';
        accOpp2.Type	= 'Expansion';
        accOpp2.CurrencyIsoCode = 'BRL';
        accOpp2.Gympass_Plus__c = 'Yes';
        accOpp2.Partner_Opp_File_url__c = 'Whishlist_OppFile.jpg';
        INSERT accOpp2;

    }
    
     @isTest static void uploadNewFileInAccount(){
        List<Account> listFileAccount = [SELECT Id, Name FROM Account WHERE Name = 'Partner_Upload'  LIMIT 1];
        
        if (!listFileAccount.isEmpty()) {
            Account fileAccount = listFileAccount[0];
            
            Test.startTest();
            Blob bodyBlob=Blob.valueOf('Unit Test ContentVersion Body');
            ContentVersion contentVersion_1 = new ContentVersion(
                Title = 'Photo Penguins',
                PathOnClient = '/Penguins.png',
                VersionData = bodyBlob,
                origin = 'H',
                Type_Files_fileupload__c = 'Photo'
              );
            Insert  contentVersion_1;
            
            ContentDocumentLink ContentDLAcc = new ContentDocumentLink();
            ContentDLAcc.ContentDocumentId = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: contentVersion_1.Id].ContentDocumentId;
            ContentDLAcc.LinkedEntityId = fileAccount.Id;
            Insert ContentDLAcc;
    
            list<ContentDistribution> lstDistribution = [SELECT id, Name, DistributionPublicUrl FROM ContentDistribution where Name =: 'File ' + contentVersion_1.Id];
    
            system.assertEquals(lstDistribution.size() > 0, true);
    
            Test.stopTest();    
        }
    }

    @IsTest
    private static void Partner_createChildOpportunity(){
        Opportunity opp = [SELECT ID, AccountId, OwnerId, CloseDate FROM Opportunity LIMIT 1];     
        Test.startTest();
            OpportunityService.createChildOpportunity('Test Child Opp', Date.today(), opp.OwnerId, opp.AccountId,opp.Id, new List<Id>{opp.AccountId}, constants.OPPORTUNITY_RT_PARTNER_FLOW_OPP);
        Test.StopTest();
        List<Opportunity> oppResultLst = [SELECT ID FROM Opportunity];
        List<Product_Item__c> productItemLst = [SELECT ID FROM Product_Item__c];
        system.assertEquals(oppResultLst.size(), 2);
        system.assertEquals(productItemLst.size(), 2);
    }

    @IsTest
    private static void Partner_createChildOpportunityError(){
        Opportunity opp = [SELECT ID, AccountId, OwnerId, CloseDate FROM Opportunity LIMIT 1];     

        try{
                Test.startTest();
                    OpportunityService.createChildOpportunity(null, Date.today(), 'ddass', opp.AccountId,opp.Id, new List<Id>{opp.AccountId}, constants.OPPORTUNITY_RT_PARTNER_FLOW_OPP);
                Test.stopTest();
            }catch(Exception e) {
                System.assert(e instanceof OpportunityService.OpportunityServiceException);
        }
    }

    
    @IsTest
    private static void Partner_validateOpportunityStage(){
        PS_Constants constants = PS_Constants.getInstance();
        Opportunity opp = [SELECT ID,StageName, RecordTypeId, AccountId, OwnerId, CloseDate FROM Opportunity LIMIT 1];   
        Test.startTest();
            opp.RecordTypeId =  Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(constants.OPPORTUNITY_RT_PARTNER_FLOW_OPP).getRecordTypeId();
            opp.StageName = constants.OPPORTUNITY_STAGE_QUALIFICACAO;
            update opp;
        Test.stopTest();
          
    }
}