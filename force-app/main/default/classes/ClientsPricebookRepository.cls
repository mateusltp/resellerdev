/**
 * @description       : 
 * @author            : roei@gft.com
 * @group             : 
 * @last modified on  : 03-25-2021
 * @last modified by  : roei@gft.com
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   03-15-2021   roei@gft.com   Initial Version
**/
public with sharing class ClientsPricebookRepository extends PricebookRepository {
    public ClientsPricebookRepository() {}

    public override PricebookEntry accessFee(Pricebook2 pricebook, Decimal eligiblesQuantity, boolean familyMemberIncluded, boolean hasCopay, String currencyIsoCode ){
        String pricebookId = pricebook.Id;

        String query = 'SELECT Id,Name,Product2Id,UnitPrice,Product2.Maximum_Number_of_Employees__c,Product2.Family_Member_Included__c,Has_Minimum_Quantity__c,Product2.Copay2_Enrollment_Rate__c FROM PricebookEntry '+
            'WHERE IsActive = true AND Pricebook2Id=:pricebookId AND Product2.Family = \'Enterprise Subscription\' ';
      
        if( eligiblesQuantity != null && eligiblesQuantity > 0 ){
            
            query += 'AND (Product2.Minimum_Number_of_Employees__c <=:eligiblesQuantity ' +
                'AND Product2.Maximum_Number_of_Employees__c >= :eligiblesQuantity) ';
        }
        
        query += ' AND Product2.Copay2__c = ' + hasCopay;

        if ( !hasCopay ) {
            query += ' AND Product2.Family_Member_Included__c = ' + String.valueOf(familyMemberIncluded);
        }

        query += ' AND CurrencyIsoCode = \''+currencyIsoCode+'\'';
        query += ' LIMIT 1';
            
        return Database.query(query);
    }
    
    public override PricebookEntry setupFee(Pricebook2 pricebook, Decimal eligiblesQuantity, boolean familyMemberIncluded, String currencyIsoCode){
        String pricebookId = pricebook.Id;
        String query = 'SELECT Id,Name,Product2Id,UnitPrice FROM PricebookEntry '+
            'WHERE IsActive = true AND Pricebook2Id=:pricebookId AND Product2.Family = \'Setup Fee\' ';
        
        if( eligiblesQuantity != null && eligiblesQuantity > 0 ){
            //4test, while checking this scenario
            if ( eligiblesQuantity < 500 ){ eligiblesQuantity = 500; }

            query += 'AND (Product2.Minimum_Number_of_Employees__c<=:eligiblesQuantity ' +
                'AND Product2.Maximum_Number_of_Employees__c >= :eligiblesQuantity)';
        }
        query += ' AND CurrencyIsoCode = \''+currencyIsoCode+'\'';

        // real case for setup fee?
        //query += 'and Product2.Family_Member_Included__c = '+String.valueOf(familyMemberIncluded);
        query += ' LIMIT 1';
            
        return Database.query(query);
    }
}