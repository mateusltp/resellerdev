/**
 * @description       : 
 * @author            : alysson.mota@gympass.com
 * @group             : 
 * @last modified on  : 04-12-2022
 * @last modified by  : gepi@gft.com
**/
public with sharing class ContractAgreementSelector extends ApplicationSelector {
    public Schema.SObjectType getSObjectType() {
		return APXT_Redlining__Contract_Agreement__c.sObjectType;
	}

    public List<Schema.SObjectField> getSObjectFieldList() {
		return new List<Schema.SObjectField> {
			APXT_Redlining__Contract_Agreement__c.Id
		};
	}

    public List<APXT_Redlining__Contract_Agreement__c> byOpportunityId(Set<Id> idSet) {
        return new List<APXT_Redlining__Contract_Agreement__c> ( (List<APXT_Redlining__Contract_Agreement__c>) Database.query(
			newQueryFactory().
					selectField(APXT_Redlining__Contract_Agreement__c.Id).
					selectField(APXT_Redlining__Contract_Agreement__c.Opportunity__c).
					selectField(APXT_Redlining__Contract_Agreement__c.Type_of_contract__c).
					setCondition('Opportunity__c IN: idSet').
                    addOrdering( new fflib_QueryFactory.Ordering('APXT_Redlining__Contract_Agreement__c','CreatedDATE',fflib_QueryFactory.SortOrder.DESCENDING)).
					toSOQL()
				)
			);   
    }
}