/**
 * @description       : 
 * @author            : roei@gft.com
 * @group             : 
 * @last modified on  : 01-06-2021
 * @last modified by  : roei@gft.com
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   12-25-2020   roei@gft.com   Initial Version
**/
public class NotifyRenegotiationCancellationBatch implements Database.Batchable<sObject>, Schedulable {
    private Id gRenegotiationRecTypeId = 
        Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Success_Renegotiation').getRecordTypeId();
    
    public void execute( SchedulableContext aSContext ) {
    	Database.executeBatch( new NotifyRenegotiationCancellationBatch() , 200 );
    }
    
    public Database.QueryLocator start( Database.BatchableContext aBC ){
        Date l5DaysFromToday = System.today().addDays(5);
        return Database.getQueryLocator('SELECT Id, OwnerId, Owner.Email FROM Opportunity WHERE RecordTypeId =: gRenegotiationRecTypeId AND cancellation_date__c =: l5DaysFromToday');
    }
    
    public void execute( Database.BatchableContext aBC, List< Opportunity > aOppScope ){
        EmailTemplate lEmailTemplate = [ SELECT Id, Subject, Body, HtmlValue FROM EmailTemplate WHERE DeveloperName = 'Renegotiation_cancellation_date_close_to_complete' LIMIT 1 ];
        
        List< Messaging.SingleEmailMessage > lLstEmailMessage = new List< Messaging.SingleEmailMessage >();
        Messaging.SingleEmailMessage lMessage;
        
        for( Opportunity iOpp : aOppScope ){
           	lMessage = new Messaging.SingleEmailMessage();
            lMessage.setSubject( lEmailTemplate.Subject );
            lMessage.setHtmlBody( lEmailTemplate.Body.replace( '{!OpportunityLink}', URL.getSalesforceBaseUrl().toExternalForm() + '/' + iOpp.Id ) );
            lMessage.setTargetObjectId( iOpp.OwnerId );
            lMessage.setSaveAsActivity( false );
            lMessage.setUseSignature( false );
            
            lLstEmailMessage.add( lMessage );
        }
        
        Messaging.SendEmailResult[] lLstResults = Messaging.sendEmail( lLstEmailMessage , false );
        
        for( Messaging.SendEmailResult iEmailResult : lLstResults ){
            if( !iEmailResult.Success ){
                System.debug('The email failed to send: ' +  iEmailResult.errors[0].message);
            } else {
                System.debug('Email sent ' + aOppScope[0].Owner.Email);
            }
        }
    }
    
    public void finish( Database.BatchableContext aBC ){}
}