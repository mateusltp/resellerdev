/**
 * @description       : 
 * @author            : ext.gft.pedro.oliveira@gympass.com
 * @group             : 
 * @last modified on  : 04-08-2022
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/
public interface IOpsSetupFormService {

    void createOpsFormForAccounts(Id aOpportunityId, Set<Id> accountsId );

}