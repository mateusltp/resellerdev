public class ManagedClauseHelper {



    public List<ManagedClauseTypeWrapper> ClausesTypes;
    private List<ManagedClause> managedClauseList;
    private Set<Id> ContractAgreementsIds;
    private List<APXT_Redlining__Contract_Agreement__c> contractAgreements;
    private List<ManagedClause__c> managedClauseObjectList;
    private List<SubManagedClauses__c> subManagedClauseObjectList;
    private List<ManagedSwapClause__c> swapManagedList = new List<ManagedSwapClause__c>();
    private  Map<Id, List<ManagedSwapClause__c>> ManagedSwapClauseMap = new Map<Id, List<ManagedSwapClause__c>>(); 
    

    public class ManagedClauseTypeWrapper {
        public string TemplateType;
        public string Country;
        public List<ManagedClause> ManagedClauses = new List<ManagedClause>();
        private  Map<Id, ManagedClause> ManagedSwapClauseMap = new Map<Id, ManagedClause>(); 
        
        public void AddSwapList(List<ManagedSwapClause__c> swapList){
            Set<Id> managedSwapClauseSetId = new  Set<Id>();

            /* Start looking for all Ids because we need to get complete information from SwapClause__c */
            for (ManagedSwapClause__c managedClauseSwap : swapList){
                managedSwapClauseSetId.add(managedClauseSwap.SwapClause__c);
            }
            
            List<SwapClause__c> swapClauseList= [SELECT Id, Name, Clause__c, SubClause__c, SubSubClause__c, Text__c FROM SwapClause__c WHERE Id IN : managedSwapClauseSetId ];

            if (!swapClauseList.isEmpty()){

                
                /* For each swap clause , put into a map a new ManagedClause with key equals as stardard clause - it will make easier to find whos going to be changed to a swap */
                for (SwapClause__c swapClause : swapClauseList){
                    ManagedClause swappedClause = new ManagedClause();
                    swappedClause.ClauseId = swapClause.Id;
                    swappedClause.ClauseText = swapClause.Text__c;
                    swappedClause.Name = swapClause.Name;
                    

                    for (ManagedSwapClause__c managedSwapClause : swapList){
                        if (managedSwapClause.SwapClause__c == swapClause.Id){
                            if(managedSwapClause.Clause__c != null){
                                this.ManagedSwapClauseMap.put(managedSwapClause.Clause__c, swappedClause);
                            }else if(managedSwapClause.SubClause__c != null){
                                this.ManagedSwapClauseMap.put(managedSwapClause.SubClause__c, swappedClause);
                            }else if(managedSwapClause.SubSubClause__c != null){
                                this.ManagedSwapClauseMap.put(managedSwapClause.SubSubClause__c, swappedClause);
                            }
                        }
                    }
                    
                }

                for (Id searchId : ManagedSwapClauseMap.keySet() ){
                    for (ManagedClause managedClausewrapper : this.ManagedClauses){
                        for (SubManagedClause subManagedClauseWrapper : managedClausewrapper.getSubClauseList() ){
                            for (SubManagedClause subsubManagedClause : subManagedClauseWrapper.getSubClauses()){
                                if(subsubManagedClause.ClauseId == searchId){
                                    subsubManagedClause.addSwapClause(this.ManagedSwapClauseMap.get(searchId));
                                }
                            }
                            if(subManagedClauseWrapper.ClauseId == searchId){
                                subManagedClauseWrapper.addSwapClause(this.ManagedSwapClauseMap.get(searchId));
                            }
                        }
                    if(managedClausewrapper.ClauseId == searchId){
                        managedClausewrapper.addSwapClause(this.ManagedSwapClauseMap.get(searchId));
                    }
                    }
                }
            }

        }

        public Boolean equals(Object obj) {
            if (obj instanceof ManagedClauseTypeWrapper){
                ManagedClauseTypeWrapper c = (ManagedClauseTypeWrapper)obj;
                return ((c.TemplateType == this.TemplateType) && (c.Country == this.Country));
            } 
            return false;
		}
    
 	    public Integer hashCode() {
    		return (31 * this.TemplateType.hashCode()) ^ this.Country.hashCode();
		}
    }

    private void loadSwappedManagedClauses(List<APXT_Redlining__Contract_Agreement__c> contractAgreementList){
        Set<Id> contractAgreementSetId = (new Map<Id,SObject>(contractAgreementList)).keySet();

        List<ManagedSwapClause__c> managedSwapClauseList = [SELECT  Id, 
                                                                    Name, 
                                                                    ContractAgreement__c, 
                                                                    Clause__c, SubClause__c, 
                                                                    SubSubClause__c, 
                                                                    SwapClause__c, 
                                                                    Status__c 
                                                            FROM ManagedSwapClause__c 
                                                            WHERE ContractAgreement__c 
                                                            IN : contractAgreementSetId];
        if (!managedSwapClauseList.isEmpty()){

            this.ManagedSwapClauseMap = new Map<Id,List<ManagedSwapClause__c>>(); 

            for (ManagedSwapClause__c managedSwap : managedSwapClauseList){
                if (this.ManagedSwapClauseMap.containsKey(managedSwap.ContractAgreement__c)){
                    this.ManagedSwapClauseMap.get(managedSwap.ContractAgreement__c).add(managedSwap);
                }else{
                    this.ManagedSwapClauseMap.put(managedSwap.ContractAgreement__c, new ManagedSwapClause__c[]{managedSwap});
                }
            }

        }                                                            
    }
    
    //constructor - execute all processes
    public ManagedClauseHelper(List<APXT_Redlining__Contract_Agreement__c> contractAgreementList){
        system.debug('contractAgreementList ' + contractAgreementList);
        //process only contract agreements with T&Cs and without Default Clauses already created
        this.contractAgreements = this.getContractsAgreementsWithoutDefaultManagedClausesCreated(contractAgreementList);

        //create type wrapper for each combination of country and type (one for each combination only)
        this.findUniqueTypeOfContracts(this.contractAgreements);

        //create the classes with clauses for each wrapper 
        this.getManagedClauses(this.ClausesTypes);

        //Load the swapped clauses if exist
        this.loadSwappedManagedClauses(contractAgreementList);

        //generate the final managed clauses object
        this.createManagedClauseObject();
        
        //save all clauses
        this.saveManagedClausesCreated();
    }



    //verify id the contract agreement already has default managed clauses
    private List<APXT_Redlining__Contract_Agreement__c> getContractsAgreementsWithoutDefaultManagedClausesCreated(List<APXT_Redlining__Contract_Agreement__c> contractAgreementList){
        List<APXT_Redlining__Contract_Agreement__c> listToCheckIfHasClausesGenerated = new List<APXT_Redlining__Contract_Agreement__c>();
        
        List<APXT_Redlining__Contract_Agreement__c> contractAgreementsWithoutManagedclauses = new List<APXT_Redlining__Contract_Agreement__c>();

        for (APXT_Redlining__Contract_Agreement__c contractAgreement : contractAgreementList){
            if (contractAgreement.Include_full_T_C_s__c == true){
                listToCheckIfHasClausesGenerated.add(contractAgreement);
            }
        }

        if (!listToCheckIfHasClausesGenerated.isEmpty() ){
            Set<Id> SetIdsOfContractAgreementsWithTCs = (new Map<Id,SObject>(listToCheckIfHasClausesGenerated)).keySet();

            contractAgreementsWithoutManagedclauses = 
                                                            [SELECT Id, 
                                                            Template_Group__c, 
                                                            Template_Selection__c ,
                                                            Include_full_T_C_s__c 
                                                            FROM 
                                                            APXT_Redlining__Contract_Agreement__c 
                                                            WHERE 
                                                            Id NOT IN
                                                                (SELECT ContractAgreement__c 
                                                                        FROM 
                                                                        ManagedClause__c
                                                                        WHERE  ContractAgreement__c IN :SetIdsOfContractAgreementsWithTCs)
                                                            AND 
                                                            Include_full_T_C_s__c = true
                                                            AND Id IN :SetIdsOfContractAgreementsWithTCs 
                                                            ];
            
        }
        system.debug('contractAgreementsWithoutManagedclauses ' + contractAgreementsWithoutManagedclauses);
        return contractAgreementsWithoutManagedclauses;
    }

    //separate all types of contracts to avoid creating a lot of same clauses
    private void findUniqueTypeOfContracts(List<APXT_Redlining__Contract_Agreement__c> contractAgreementList){

        Set<ManagedClauseTypeWrapper> countryAndTypeSet = new Set<ManagedClauseTypeWrapper>();

        for (APXT_Redlining__Contract_Agreement__c contractAgreement : contractAgreementList){
            ManagedClauseTypeWrapper mnType = new ManagedClauseTypeWrapper();
            mnType.TemplateType = contractAgreement.Template_Group__c;
            mnType.Country = contractAgreement.Template_Selection__c;
            countryAndTypeSet.add(mnType);
        }
       
        this.ClausesTypes = new List<ManagedClauseTypeWrapper>();
        this.ClausesTypes.addAll(countryAndTypeSet);

    }

    //Create all clauses and subclauses for each type in contract list
    private void getManagedClauses(List<ManagedClauseTypeWrapper> contractsType){
        managedClauseList = new List<ManagedClause>();

        For (ManagedClauseTypeWrapper clauseType : contractsType){
            ManagedClauseCreator mnClause = new ManagedClauseCreator(clauseType.Country, clauseType.TemplateType);
            mnClause.createManagedClauses();
            clauseType.ManagedClauses = mnClause.getManagedClauses().clone();
        }
    }


    //find on the list of types of contracts a specific one 
    private ManagedClauseTypeWrapper getContractType(String country, String Type ){
        system.debug('this.ClausesTypes ' + this.ClausesTypes);
        for (ManagedClauseTypeWrapper contractType: this.ClausesTypes){
            if ((contractType.Country == country) && (contractType.TemplateType == Type)){
                return contractType;
            }
        }
        return null;
    }

    //from the type wrapper , create the managed objects for each contract on the contract agreement list
    private void createManagedClauseObject(){
        
        List<ManagedClause__c> manageClauseObjectList = new List<ManagedClause__c> ();
        List<SubManagedClauses__c> subManageClauseObjectList = new List<SubManagedClauses__c> ();
        for (APXT_Redlining__Contract_Agreement__c contractAgreement : this.contractAgreements){
            ManagedClauseTypeWrapper contractClauseType = this.getContractType(contractAgreement.Template_Selection__c , contractAgreement.Template_Group__c);
            if (this.ManagedSwapClauseMap.containsKey(contractAgreement.Id)){
                contractClauseType.AddSwapList(this.ManagedSwapClauseMap.get(contractAgreement.Id));
            }
            if (contractClauseType != null ){


                for (ManagedClause managedclause :  contractClauseType.ManagedClauses){
                    ManagedClause__c managedClauseObject = new ManagedClause__c();
                    managedClauseObject.ContractAgreement__c = contractAgreement.id;
                    managedClauseObject.UniqueIdentifier__c = contractAgreement.Id +''+ managedclause.Order;
                    managedClauseObject.Category__c = managedclause.Category;
                    managedClauseObject.Order__c = managedclause.Order;
                    managedClauseObject.Text__c = managedclause.ClauseText();

                    manageClauseObjectList.add(managedClauseObject);

                    for (SubManagedClause subManagedclause :  managedclause.getSubClauses()){
                        SubManagedClauses__c subManagedClauseObject = new SubManagedClauses__c();
                        subManagedClauseObject.Order__c = subManagedclause.Order;
                        subManagedClauseObject.Text__c = subManagedclause.ClauseText();
                        subManagedClauseObject.ManagedClause__r = new ManagedClause__c(UniqueIdentifier__c = contractAgreement.Id +''+ managedclause.Order);
                        subManageClauseObjectList.add(subManagedClauseObject);
                    } 

                }
     
                
            }   
        }

        this.managedClauseObjectList = manageClauseObjectList;

        this.subManagedClauseObjectList = subManageClauseObjectList;

    } 

    //save the managed clauses created for all contract agreements in the list
    public void saveManagedClausesCreated(){
        insert this.managedClauseObjectList;
        insert this.subManagedClauseObjectList;
    }

}