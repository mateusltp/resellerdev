@isTest
public class SalesOrderHelperTest {
	
    @isTest
    public static void functionalTest() {
        Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
        Id oppRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SMB').getRecordTypeId();
        Id proposalRecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Direct_Channel').getRecordTypeId();
        
    	Account acc = new Account();
        acc.RecordTypeId = accRecordTypeId;
        acc.Name = 'Teste account';
        acc.BillingCountry = 'Brazil';
        acc.BillingState = 'São Paulo';
        acc.Business_Unit__c = 'SMB';
        insert acc;
        
        Contact financeContact = new Contact();
        financeContact.LastName = 'Finance';
        financeContact.Email = 'finance@teste.com';
        financeContact.AccountId = acc.Id;
        financeContact.MailingCity = 'São Paulo';
        financeContact.MailingCity = 'Brazil';
        financeContact.MailingCountryCode = 'BR';
        financeContact.Phone = '1234566789';
        insert financeContact;
        
        Contact adminContact = new Contact();
        adminContact.LastName = 'Admin';
        adminContact.Email = 'admin@teste.com';
        adminContact.AccountId = acc.Id;
        adminContact.MailingCity = 'São Paulo';
        adminContact.MailingCity = 'Brazil';
        adminContact.MailingCountryCode = 'BR';
        adminContact.Phone = '1234566789';
        adminContact.Document_Number__c = '31170521517';
        insert adminContact;
        
        Contact opsContact = new Contact();
        opsContact.LastName = 'Ops';
        opsContact.Email = 'admin@teste.com';
        opsContact.AccountId = acc.Id;
        opsContact.MailingCity = 'São Paulo';
        opsContact.MailingCity = 'Brazil';
        opsContact.MailingCountryCode = 'BR';
        opsContact.Phone = '1234566789';
        insert opsContact;
        
        Opportunity opp = new Opportunity();
        opp.RecordTypeId = oppRecordTypeId;
        opp.AccountId = acc.Id;
        opp.Name = 'Teste opportunity';
        opp.StageName = 'Qualificação';
        opp.CloseDate = Date.today().addMonths(12);
        insert opp;
        
        opp.StageName = 'Oportunidade Validada Após 1ª Reunião';
        update opp;
        
        Copay_Plan__c membershipFeePlan = new Copay_Plan__c();
        insert membershipFeePlan;
        
        Product2 plan = new Product2();
        plan.Name = 'Teste Product';
        insert plan;
        
		insert new PricebookEntry(pricebook2id = Test.getStandardPricebookId(), product2id = plan.id, unitprice=1.0, isActive=true);
        
        Pricebook2 pb = new pricebook2(name='test');
		insert pb;

		PricebookEntry entry = new PricebookEntry(pricebook2id=pb.id, product2id=plan.id, unitprice=1.0, isActive=true);
		insert entry;
        
        Quote proposal = new Quote();
        proposal.Name = 'Teste proposal';
        proposal.RecordTypeId = proposalRecordTypeId;
        proposal.OpportunityId = opp.Id;
        proposal.License_Fee_Waiver__c = 'No';
        proposal.Gym__c = membershipFeePlan.Id;
        proposal.Pricebook2Id = pb.Id;
        proposal.Finance__c = financeContact.Id;
        proposal.Administrator__c = adminContact.Id;
        proposal.Operations__c = opsContact.Id;
        proposal.Unique_Identifier__c = 'National ID';
        insert proposal;
        
        QuoteLineItem lineItem = new QuoteLineItem();
        lineItem.QuoteId = proposal.Id;
        lineItem.Quantity = 200;
        lineItem.UnitPrice = 10.99;
        lineItem.Product2Id = plan.Id;
        lineItem.PricebookEntryId = entry.Id;
        insert lineItem;
        
        Test.startTest();
        SalesOrderHelper.putSalesOrderForSMB(acc.Id, opp.Id, proposal.Id);
        Test.stopTest();
    }
}