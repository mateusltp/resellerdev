public class ManagedClauseCreator  {
    
    private string Country;
    private string TemplateType;
    private List<SubManagedClause> SubManagedClauses; 
    private List<ManagedClause> ManagedClauses;
    
    private List<ManagedSwapClause__c> swapObjectList;
    private  Map<Id, ManagedClause> ManagedSwapClauseMap = new Map<Id, ManagedClause>(); 
    
    public ManagedClauseCreator(string country, string templateType){
        this.Country = country;
        this.TemplateType = templateType;
    }
    
    public void createManagedClauses(){
        this.loadSubClauses();
        this.loadClauses();
    }
    
    
    
    private void loadSubClauses(){
        
        Map<SubClause__c, List<SubSubClause__c>> SubClauseMap = new Map<SubClause__c, List<SubSubClause__c>>();
        for ( Subclause__c subClause : [SELECT Id, 
                                        order__c , 
                                        Name , 
                                        text__c, 
                                        clause__c  ,
                                            (SELECT Id, 
                                             Name, 
                                             text__c , 
                                             order__c, 
                                             subclause__c 
                                             FROM SubSubClause_SubClause__r 
                                             ORDER BY order__c) 
                                        FROM Subclause__c  
                                        WHERE country__c = : this.Country 
                                        AND type__c =: this.TemplateType 
                                        ORDER BY 	order__c , 
                                        Name
                                       ]) { 
                                           
                                           SubClauseMap.put(subClause,new SubSubClause__c[]{});
                                           for (SubSubClause__c subsubClause : subClause.SubSubClause_SubClause__r ) {
                                               
                                               if(SubClauseMap.containsKey(subClause)){
                                                   SubClauseMap.get(subClause).add(subsubClause);
                                               }
                                               else {
                                                   SubClauseMap.put(subClause, new SubSubClause__c[]{
                                                       subsubClause
                                                           });
                                               }
                                           }
                                       }
        
        List<SubManagedClause> SubManagedClauseList = new List<SubManagedClause>();
        for (SubClause__c subClause : SubClauseMap.keyset()){
            //create the submanaged class
            SubManagedClause subManagedClause =  new SubManagedClause();
            subManagedClause.ClauseId = subClause.id;
            subManagedClause.Name = subClause.Name;
            subManagedClause.Order = subClause.Order__c;
            subManagedClause.ParentId = subClause.clause__c;
            subManagedClause.ClauseText = subClause.Text__c;
            if (!ManagedSwapClauseMap.isEmpty()){
                if (ManagedSwapClauseMap.containsKey(subManagedClause.ClauseId)){
                    subManagedClause.addSwapClause(ManagedSwapClauseMap.get(subManagedClause.ClauseId));
                }
            }
            //if there are subsubClauses create the list of subsubManaged class and add to SubManaged
            if (SubClauseMap.get(subClause) != null){
                List<SubManagedClause> subSubManagedClauseList = new List<SubManagedClause>();
                for(SubSubClause__c subSubClause : SubClauseMap.get(subClause) ){
                    
                    SubManagedClause newSubSubManaged = new SubManagedClause();
                    newSubSubManaged.ClauseId = subSubClause.id;
                    newSubSubManaged.Name = subSubClause.Name;
                    newSubSubManaged.Order = subSubClause.Order__c;
                    newSubSubManaged.ParentId = subClause.Id;
                    newSubSubManaged.ClauseText = subSubClause.Text__c;
                    if (!ManagedSwapClauseMap.isEmpty()){
                        if (ManagedSwapClauseMap.containsKey(newSubSubManaged.ClauseId)){
                            newSubSubManaged.addSwapClause(ManagedSwapClauseMap.get(newSubSubManaged.ClauseId));
                        }
                    }
                    subSubManagedClauseList.add(newSubSubManaged);
                }
                subManagedClause.addSubClause(subSubManagedClauseList);
            }
            
            SubManagedClauseList.add(subManagedClause);
        }
        
        this.SubManagedClauses= SubManagedClauseList;
    }
    
    private void loadClauses(){
        
        List<Clause__c> clauseList = [SELECT Id, 
                                      Name, 
                                      Text__c,
                                      Category__c, 
                                      Country__c, 
                                      Type__c, 
                                      Order__c 
                                      FROM Clause__c 
                                      WHERE Country__c = : this.Country 
                                      AND Type__c = : this.TemplateType  ORDER BY order__c] ;
        
        List<ManagedClause> managedClauses = new List<ManagedClause>();
        
        for (Clause__c clause : clauseList){
            ManagedClause managedClause = new ManagedClause();
            managedClause.ClauseId = clause.id;
            managedClause.Name = clause.Name;
            managedClause.Category = clause.Category__c;
            managedClause.Order = clause.Order__c;
            managedClause.ClauseText = clause.Text__c;

            if (!ManagedSwapClauseMap.isEmpty()){
                if (ManagedSwapClauseMap.containsKey(managedClause.ClauseId)){
                    managedClause.addSwapClause(ManagedSwapClauseMap.get(managedClause.ClauseId));
                }
            }

            managedClauses.add(managedClause);
        }
        
        
        this.ManagedClauses = managedClauses;
        
    }
    
   
    public void addSwapList(List<ManagedSwapClause__c> swapList){
        
      
        Set<Id> managedSwapClauseSetId = new  Set<Id>();

        /* Start looking for all Ids because we need to get complete information from SwapClause__c */
        for (ManagedSwapClause__c managedClauseSwap : swapList){
            managedSwapClauseSetId.add(managedClauseSwap.SwapClause__c);
        }
        
        List<SwapClause__c> swapClauseList= [SELECT Id, Name, Clause__c, SubClause__c, SubSubClause__c, Text__c FROM SwapClause__c WHERE Id IN : managedSwapClauseSetId ];

        if (!swapClauseList.isEmpty()){

            /* For each swap clause , put into a map a new ManagedClause with key equals as stardard clause - it will make easier to find whos going to be changed to a swap */
            for (SwapClause__c swapClause : swapClauseList){
                ManagedClause swappedClause = new ManagedClause();
                swappedClause.ClauseId = swapClause.Id;
                swappedClause.ClauseText = swapClause.Text__c;
                swappedClause.Name = swapClause.Name;
                

                for (ManagedSwapClause__c managedSwapClause : swapList){
                    if (managedSwapClause.SwapClause__c == swapClause.Id){
                        if(managedSwapClause.Clause__c != null){
                            this.ManagedSwapClauseMap.put(managedSwapClause.Clause__c, swappedClause);
                        }else if(managedSwapClause.SubClause__c != null){
                            this.ManagedSwapClauseMap.put(managedSwapClause.SubClause__c, swappedClause);
                        }else if(managedSwapClause.SubSubClause__c != null){
                            this.ManagedSwapClauseMap.put(managedSwapClause.SubSubClause__c, swappedClause);
                        }
                    }
                }
                
            }
        }

        /* set the list just to keep the info stored */
        this.swapObjectList = swapList;

    }
    
    
    public List<SubManagedClause> getDefaultSubClauses(){
        return this.SubManagedClauses;
    }
    
    public List<ManagedClause> getDefaultClauses(){
        return this.ManagedClauses;
    }
    
    public List<ManagedClause> getManagedClauses(){
        
        List<ManagedClause> managedClauses = new List<ManagedClause>();
        List<SubManagedClause> subManagedClauses = new List<SubManagedClause>();
        
        managedClauses = this.getDefaultClauses();
        subManagedClauses = this.getDefaultSubClauses();
        
        for (SubManagedClause subClause : subManagedClauses){
            for (ManagedClause clause : managedClauses){
                if (clause.ClauseId == subclause.ParentId ){
                    clause.addSubClause(subClause);
                    break;
                }
            }
        }
system.debug('list clause ' + managedClauses);
        return managedClauses;
    }
    
    
    
}