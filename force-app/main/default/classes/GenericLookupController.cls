public with sharing class GenericLookupController {
    
    @AuraEnabled(cacheable=true)
    public static List < sObject > fetchLookUpValues(
            String searchKeyWord, 
            String ObjectName, 
            String aditionalFields, 
            String limitStr, 
            String searchBy, 
            List<Id> notInclude, 
            String conditions, 
            String orderBy) {
        
        System.debug('### INTO THE CONTROLLER ###');
        System.debug('### ObjectName ### ' + ObjectName);
        searchBy = String.isBlank(searchBy) ? 'Name' : searchBy;
        if(searchKeyWord != null && searchKeyWord.length() > 2 && searchBy.equalsIgnoreCase('name')) {
            String searchKey = '\'*' + searchKeyWord + '*\'';
            String searchQuery = 'FIND ' + searchKey;
 
            searchQuery += ' IN ' + searchBy +' FIELDS ';
            notInclude = notInclude != null ? notInclude : new List<Id>();
            List < sObject > returnList = new List < sObject > ();
          
            searchQuery += ' RETURNING  ' + ObjectName;
            searchQuery += ' (Id, Name';
            if (!String.isBlank(aditionalFields)) {
                searchQuery += ' , ' + aditionalFields;
            }
          
            searchQuery += ' WHERE Id NOT IN :notInclude ';
            searchQuery += (String.isBlank(conditions) ? '' : ' AND (' + conditions + ') ');
            searchQuery += (String.isBlank(orderBy) ? '' : ' ORDER BY ' + orderBy);
            searchQuery +=  ' LIMIT ' + limitStr + ')';
            System.debug('### searchQuery --> ' + searchQuery);
            try {
               returnList = search.query(searchQuery)[0];
            } catch (Exception e) {
               System.debug('### Error --> ' + e);
               throw new AuraHandledException(e.getMessage());
            }
            return returnList;
        } else {
 
           String searchKey = searchKeyWord + '%';
           notInclude = notInclude != null ? notInclude : new List<Id>();
           List < sObject > returnList = new List < sObject > ();
          
           String sQuery =  'SELECT Id, Name';
           if (!String.isBlank(aditionalFields)) {
               sQuery += ' , ' + aditionalFields;
           }
          
           sQuery += ' FROM ' +ObjectName + ' WHERE ' + searchBy + ' LIKE :searchKey AND Id NOT IN :notInclude';
           sQuery += (String.isBlank(conditions) ? '' : ' AND (' + conditions + ') ');
           sQuery += (String.isBlank(orderBy) ? '' : ' ORDER BY ' + orderBy);
           sQuery +=  ' LIMIT ' + limitStr;
           System.debug('### sQuery --> ' + sQuery);
           try {
               returnList = Database.query(sQuery);
           } catch (Exception e) {
               throw new AuraHandledException(e.getMessage());
           }
           return returnList;
        }
 
    }
   
    @AuraEnabled(cacheable=true)
    public static List <sObject> fetchPreviouslySelectedValues(String objectName, String conditions, String orderBy) {
        System.debug('### INTO THE APEX fetchPreviouslySelectedValues ###');
        System.debug('### objectName ### ' + objectName);
        System.debug('### conditions ### ' + conditions);
        System.debug('### orderBy ### ' + orderBy);

        List < sObject > returnList = new List < sObject > ();
          
        String sQuery =  'SELECT Id, Name';
        sQuery += ' FROM ' + objectName;
        sQuery += (String.isBlank(conditions) ? '' : ' WHERE (' + conditions + ') ');
        sQuery += (String.isBlank(orderBy) ? '' : ' ORDER BY ' + orderBy);
        System.debug('### PreviouslySelectedValues ### sQuery --> ' + sQuery);
        
        try {
            returnList = Database.query(sQuery);
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
        return returnList;
   }  
 
   @AuraEnabled(cacheable=true)
   public static sObject getSelectedRecord(String recordId, String objectApiName, String searchField) {
        System.debug('### INTO THE SINGLE GETRECORD +++ ');
        String query = 'SELECT Id, ' + searchField + ' FROM ' + objectApiName + ' WHERE Id = \'' + recordId + '\'';
        return Database.query(query);
   }

    // @AuraEnabled(cacheable=true)
    // public static sObject getSelectedRecord(List<String> recordIdLst, String objectApiName, String searchField) {
    //     System.debug('### INTO THE SINGLE GETRECORD +++ ');
    //     System.debug(recordIdLst);

    //     if(recordIdLst.isEmpty()) return null;

    //     Set<String> idsSet = new Set<String>(recordIdLst);
    //     String listOfIds = recordIdLst.toString();
    //     String query = 'SELECT Id, ' + searchField + ' FROM ' + objectApiName + ' WHERE Id IN :idsSet';
    //     System.debug(query);
    //     System.debug(Database.query(query));
    //     return Database.query(query);
    // }

    @AuraEnabled(cacheable=true)
    public static List<sObject> getSelectedRecordsLst(List<String> recordIdLst, String objectApiName, String searchField) {
        System.debug('### INTO THE MULTIPLE GETRECORD *** ');

        if(recordIdLst.isEmpty()) return null;

        Set<String> idsSet = new Set<String>(recordIdLst);
        String listOfIds = recordIdLst.toString();
        String query = 'SELECT Id, ' + searchField + ' FROM ' + objectApiName + ' WHERE Id IN :idsSet';
        System.debug(query);
        System.debug(Database.query(query));
        return Database.query(query);
   }
}