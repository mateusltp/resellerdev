public with sharing class PmdWorstPracticesSample {
    
    private String MyName;
    private Integer myAge;

    public PmdWorstPracticesSample() {
        if(true)
            this.myAge = 0;
    }

    public void DoSomething() {

        Account notUsedAccount1;
        Account notUsedAccount2;
        Account notUsedAccount3;
        Account usedAccount;
        List<Account> accList = new List<Account>();

        if(true) {
            if(!false) {
                if(!false) {

                    if(true) {
                        for(Integer i = 0; i < 200; i++)
                        accList = [SELECT Id FROM Account];
                    }
                }
            }
        }
    }

    public void doSomeOtherThings(Integer aNumber, Integer anotherNumber, Integer andAnother, Integer andAnother2, Integer andAnother3, Integer oneMore) {
        System.debug('Do this.');
        System.debug(LoggingLevel.DEBUG,  'Do this.');
    }
}