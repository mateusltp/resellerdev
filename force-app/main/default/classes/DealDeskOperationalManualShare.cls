/**
 * @description       : 
 * @author            : roei@gft.com
 * @group             : 
 * @last modified on  : 02-09-2021
 * @last modified by  : roei@gft.com
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   02-08-2021   roei@gft.com   Initial Version
**/
public without sharing class DealDeskOperationalManualShare {
    private Id gDealDeskOperationalRecTypeId = 
        Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Deal_Desk_Operational').getRecordTypeId();
    
    public void operationalManualShare(){
        List< Case > lSetOperationalCase = new List< Case >();
        
        for( Case iNewCase : (List< Case >)Trigger.new ){
            if( iNewCase.RecordTypeId == gDealDeskOperationalRecTypeId ){
                lSetOperationalCase.add( iNewCase );
            }
        }

        manualShareCaseWithCreator( lSetOperationalCase );
    }
    
    private void manualShareCaseWithCreator( List< Case > aSetOperationalCase ){
        if( aSetOperationalCase.isEmpty() ){ return; }
        List< CaseShare > lLstCaseShare = new List< CaseShare >();

        for( Case iOperationalCase : aSetOperationalCase ){
            CaseShare lCaseShare = new CaseShare(
                CaseId = iOperationalCase.Id,      
                UserOrGroupId = UserInfo.getUserId(),
                CaseAccessLevel = 'Read',
                RowCause = Schema.CaseShare.RowCause.Manual
            );
            lLstCaseShare.add( lCaseShare );
        }
        
        Database.SaveResult[] lLstSaveResult = Database.insert( lLstCaseShare , false );

        for( Database.SaveResult iSaveResult : lLstSaveResult ){
            if( !iSaveResult.isSuccess() ){ Database.Error lError = iSaveResult.getErrors()[0];         
                if( !(lError.getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION) && !lError.getMessage().contains('AccessLevel') ){
                    System.debug('Error sharing case');
                    System.debug(lError.getMessage());
                }
            }
        }
    }
}