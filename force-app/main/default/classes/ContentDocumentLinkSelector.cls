/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 04-22-2022
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/
public with sharing class ContentDocumentLinkSelector extends ApplicationSelector {

    
	public Schema.SObjectType getSObjectType() {
		return ContentDocumentLink.sObjectType;
	}

    public List<Schema.SObjectField> getSObjectFieldList() {
		return new List<Schema.SObjectField> {
			ContentDocumentLink.Id, 
            ContentDocumentLink.ContentDocumentId,
            ContentDocumentLink.ShareType, 
            ContentDocumentLink.LinkedEntityId
		};
	}

    
    public List<ContentDocumentLink> selectById(Set<Id> ids) {
		return (List<ContentDocumentLink>) super.selectSObjectsById(ids);
	}


    public List<ContentDocumentLink> selectByContentDocumentId( Set<Id> contentDocIds ) {	
		return ( List<ContentDocumentLink> ) Database.query( 					
			newQueryFactory(true).					
            selectField('ContentDocument.LatestPublishedVersion.Type_Files_fileupload__c').			
			setCondition('ContentDocumentId in : contentDocIds').toSOQL()
		);
	}

    public List<ContentDocumentLink> selectByLinkedEntityId( Set<Id> entityIds ) {	
		return ( List<ContentDocumentLink> ) Database.query( 					
			newQueryFactory(true).					
            selectField('ContentDocument.LatestPublishedVersion.Type_Files_fileupload__c').			
			setCondition('LinkedEntityId in : entityIds').toSOQL()
		);
	}
    
}