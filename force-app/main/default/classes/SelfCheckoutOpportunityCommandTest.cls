@isTest
public class SelfCheckoutOpportunityCommandTest {
    
    @TestSetup
    static void makeData() {

        EventConfiguration__c eventConfig = new EventConfiguration__c(
          name = 'SELF_CHECKOUT_OPPORTUNITY_INBOUND',
          commandClassName__c = 'SelfCheckoutOpportunityCommand'
        );
        insert eventConfig;
    }
    
    @isTest
    private static void executeTest() {
        Test.startTest();
        
        SelfCheckoutOpportunityRequest selfCheckoutOpportunityRequest = SelfCheckoutOpportunityRequestMock.getMock();
        
        EventQueue event = new SelfCheckoutOpportunityPublisher().publish(selfCheckoutOpportunityRequest);       
        
        Test.stopTest();
    }
    
}