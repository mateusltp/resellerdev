@IsTest
public class SelfCheckoutLeadValidatorTest {
    @IsTest
    public static void execute() {
        String errorMessage;
    
        try {
            new SelfCheckoutLeadValidator(null).validatePostRequest();
        } catch(IntegrationException error) {
            errorMessage = error.getMessage();
        }
    
        System.assertEquals(
            '(Request body cannot be empty)',
            errorMessage,
            'the body do not be null'
        );
    
        try {
            new SelfCheckoutLeadValidator(new SelfCheckoutLeadRequest()).validatePostRequest();
        } catch(IntegrationException error) {
            errorMessage = error.getMessage();
        }
    
        System.assertEquals(
            '(client field is required)',
            errorMessage,
            'the client fields must be required'
        );
    
        SelfCheckoutLeadRequest selfCheckoutLeadRequest = SelfCheckoutLeadRequestMock.getMockWithLeadEmpty();
    
        try {
            new SelfCheckoutLeadValidator(selfCheckoutLeadRequest).validatePostRequest();
        } catch(IntegrationException error) {
            errorMessage = error.getMessage();
        }
    
        List<String> expectedErrorMessages = new List<String>();
        expectedErrorMessages.add('The client required field client_id is missing');
        expectedErrorMessages.add('The client required field trade_name is missing');
        expectedErrorMessages.add('The client required field billing_id is missing');
        expectedErrorMessages.add('The client required field billing_firstname is missing');
        expectedErrorMessages.add('The client required field billing_lastname is missing');
        expectedErrorMessages.add('The client required field billing_email is missing');
        expectedErrorMessages.add('The client required field business_unit is missing');
        expectedErrorMessages.add('The client required field currency_id is missing');
        expectedErrorMessages.add('The client required field number_of_employees is missing');
        expectedErrorMessages.add('The client required field language is missing');
        expectedErrorMessages.add('The client required field contact is missing');
    
        System.assertEquals(
            expectedErrorMessages.toString(),
            errorMessage,
            'Not all validations have been done'
        );
        //////////////////////////////
        SelfCheckoutLeadRequest selfCheckoutContactRequest = SelfCheckoutLeadRequestMock.getMockWithContactEmpty();
    
        try {
            new SelfCheckoutLeadValidator(selfCheckoutContactRequest).validatePostRequest();
        } catch(IntegrationException error) {
            errorMessage = error.getMessage();
        }
    
        List<String> expectedErrorMessagesContact = new List<String>();
        expectedErrorMessagesContact.add('The contact required field contact_id is missing');
        expectedErrorMessagesContact.add('The contact required field first_name is missing');
        expectedErrorMessagesContact.add('The contact required field last_name is missing');
        expectedErrorMessagesContact.add('The contact required field email is missing');
    
        System.assertEquals(
            expectedErrorMessagesContact.toString(),
            errorMessage,
            'Not all validations have been done'
        );

        SelfCheckoutLeadRequest selfCheckoutAddreesRequest = SelfCheckoutLeadRequestMock.getMockWithCountryEmpty();
    
        try {
            new SelfCheckoutLeadValidator(selfCheckoutAddreesRequest).validatePostRequest();
        } catch(IntegrationException error) {
            errorMessage = error.getMessage();
        }
    
        List<String> expectedErrorMessagesAddress = new List<String>();
        expectedErrorMessagesAddress.add('The address required field country_id is missing');
    
        System.assertEquals(
            expectedErrorMessagesAddress.toString(),
            errorMessage,
            'Not all validations have been done'
        );
    }
}