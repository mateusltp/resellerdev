@IsTest
public class SelfCheckoutLeadServiceTest {
    @IsTest
    public static void execute() {
        User integrationSMBJamorUser = [
            SELECT Id
            FROM User
            WHERE Name = 'Integration SMB Jamor'
            LIMIT 1
        ];
        Test.startTest();
        System.runAs(integrationSMBJamorUser) {
            new SelfCheckoutLeadService(SelfCheckoutLeadRequestMock.getMock()).execute();
        }
        Test.stopTest();
        Lead lead = [SELECT Id, Lead_Sub__c FROM Lead LIMIT 1];
        System.assertEquals(
            'Hands Up',
            lead.Lead_Sub__c,
            'The lead was created'
        );
    }

    @IsTest
    public static void updateLead() {
        User integrationSMBJamorUser = [
            SELECT Id
            FROM User
            WHERE Name = 'Integration SMB Jamor'
            LIMIT 1
        ];
        Test.startTest();
        System.runAs(integrationSMBJamorUser) {
            new SelfCheckoutLeadService(SelfCheckoutLeadRequestMock.getMockUpdatedLead()).execute();
        }
        Test.stopTest();
        Lead leadUpdated = [SELECT Id, UTM_Campaign__c FROM Lead LIMIT 1];
        System.assertEquals(
            'One',
            leadUpdated.UTM_Campaign__c,
            'The lead was updated'
        );
    }

}