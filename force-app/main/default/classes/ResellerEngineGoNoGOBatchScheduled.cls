global class ResellerEngineGoNoGOBatchScheduled implements Schedulable  {
    global void execute(SchedulableContext sc) 
    {
        ResellerEngineGoNoGOBatch b = new ResellerEngineGoNoGOBatch(); 
        database.executebatch(b,150);
    }
}