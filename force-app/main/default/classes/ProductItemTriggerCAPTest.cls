/**
 * @File Name          : ProductItemTriggerCAPTest.cls
 * @Description        : 
 * @Author             : Samuel Silva - GFT (slml@gft.com)
 * @Group              : 
 * @Last Modified By   : gft.samuel.silva@ext.gympass.com
 * @Last Modified On   : 05-02-2022
 * @Modification Log   : 
 * Ver       Date      Author                 		 		 Modification
 * 1.0    18/05/2020   Samuel Silva - GFT (slml@gft.com)     Initial Version
 * 2.0    24/07/2020   Rafael Reis - GFT (roei@gft.com)		 Improving test code coverage from 75% to 92%
**/
@isTest(isParallel=true)
public with sharing class ProductItemTriggerCAPTest {
    
	private static Id gProdRtId = Schema.SObjectType.Product_Item__c.getRecordTypeInfosByDeveloperName().get('Gyms_Product_Item_Partner').getRecordTypeId();    
	private static Id gProdItemRtId = Schema.SObjectType.Product_Item__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product').getRecordTypeId();    

    
    @TestSetup
    static void genericDataSetUp(){
        Id rtId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gym_Partner').getRecordTypeId();
        Account acc = new Account();
        acc.name='AcademiaBrasilCompanyPai';
        acc.RecordTypeId = rtId;
        acc.GP_Status__c = 'Pending Approval';
        acc.ShippingState = 'Minas Gerais';
        acc.CAP_Value__c = 120;
        acc.ShippingCity = 'CityAcademiaBrasil';
        acc.ShippingStreet = 'Rua academiabrasilpai';
        acc.ShippingCountry = 'Brazil';
        acc.Gym_Type__c = 'Studios';
        acc.Types_of_ownership__c = Label.franchisePicklist;
        acc.Subscription_Type__c = 'Value per class';
        acc.Subscription_Period__c = 'Monthy value';
        acc.Subscription_Type_Estimated_Price__c    = 100;
        acc.Has_market_cannibalization__c = 'No';
        acc.Exclusivity__c = 'Yes';
        acc.Exclusivity_End_Date__c = Date.today().addYears(1);
        acc.Exclusivity_Partnership__c = 'Full Exclusivity';
        acc.Exclusivity_Restrictions__c= 'No';
        acc.Website = 'testing@tesapex.com';
        acc.Gym_Email__c = 'gymemail@apex.com';
        acc.Phone = '3222123123';
        INSERT acc;

        Id oppRtId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Gyms_Small_Medium_Partner').getRecordTypeId();
        Opportunity accOpp = new Opportunity();
        accOpp.recordTypeId = oppRtId;
        accOpp.AccountId = acc.id;
        accOpp.Name = 'academiaBrasilCompanyOpp'; 
        accOpp.CMS_Used__c = 'Yes';     
        accOpp.Gym_agreed_to_an_API_integration__c  = 'Yes';
        accOpp.Club_Management_System__c = 'ABC Financial';
        accOpp.Integration_Fee_Deduction__c = 'No';
        accOpp.CloseDate = Date.today();
        accOpp.Success_Look_Like__c = 'Yes';
        accOpp.Success_Look_Like_Description__c = 'Money money';
        accOpp.StageName = 'Qualificação';
        accOpp.Type = 'Expansion';     
        accOpp.CurrencyIsoCode = 'BRL';
        accOpp.Gympass_Plus__c = 'Yes';
        accOpp.Standard_Payment__c = 'Yes';
        INSERT accOpp;
    }

    @isTest static void insertNewProduct() {
        
        Account acc = [SELECT Id, CAP_Value__c FROM Account WHERE Name = 'AcademiaBrasilCompanyPai'];
        Opportunity opp = [SELECT Id FROM Opportunity WHERE Name = 'academiaBrasilCompanyOpp'];
            
        Product_Item__c lProductItem = new Product_Item__c(    
            recordTypeId = gProdRtId,
            Product_Type__c = 'In person',
            Is_Network_CAP__c = 'Yes',
            CurrencyIsoCode = 'BRL',
            Do_You_Have_A_No_Show_Fee__c = 'No',
            Has_Late_Cancellation_Fee__c = 'No',
            Late_Cancellation_Percent__c = '0',
            Maximum_Live_Class_Per_Month__c = 0,
            No_Show_Fee_Percent__c = '0',
            Net_Transfer_Price__c  =100,
            Product_Definition__c  ='Dança',
            Product_Restriction__c = 'None',
            Name = 'ProdTestTrigger',
            Opportunity__c = opp.Id,     
            Max_Monthly_Visit_Per_User__c = 12,
            Reference_Price_Value_Unlimited__c = 200,
            Price_Visits_Month_Package_Selected__c = 400
        );
        
        Test.startTest();
        	INSERT lProductItem;
        Test.stopTest();
        
        lProductItem = [SELECT CAP_Value__c FROM Product_Item__c WHERE Id = :lProductItem.Id];
        
        System.assertEquals(acc.CAP_Value__c, lProductItem.CAP_Value__c, 'insertNewProduct failed');
    }
    
    @isTest static void shouldShowErrorCreatingNetCAPProductForNonFranchiseAcc() {
        
        Account acc = [SELECT Id, CAP_Value__c FROM Account WHERE Name = 'AcademiaBrasilCompanyPai'];
        acc.Types_of_ownership__c = 'Private owner';
        Database.update(acc);
        
        Opportunity opp = [SELECT Id FROM Opportunity WHERE Name = 'academiaBrasilCompanyOpp'];
                
        Product_Item__c lProductItem = new Product_Item__c(    
            recordTypeId = gProdRtId,
            Product_Type__c = 'In person',
            Is_Network_CAP__c = 'Yes',
            CurrencyIsoCode = 'BRL',
            Do_You_Have_A_No_Show_Fee__c = 'No',
            Has_Late_Cancellation_Fee__c = 'No',
            Late_Cancellation_Percent__c = '0',
            Maximum_Live_Class_Per_Month__c = 0,
            No_Show_Fee_Percent__c = '0',
            Net_Transfer_Price__c  =100,
            Product_Definition__c  ='Dança',
            Product_Restriction__c = 'None',
            Name = 'ProdTestTrigger',
            Opportunity__c = opp.Id,     
            Max_Monthly_Visit_Per_User__c = 12,
            Reference_Price_Value_Unlimited__c = 200,
            Price_Visits_Month_Package_Selected__c = 400
        );
        
        Test.startTest();

        try{
        	INSERT lProductItem;
    	}catch(Exception e) {
            String lErrorMessage = 'You can not use network CAP value for non franchise account hierarchy type.';
            System.Assert( e.getMessage().contains(lErrorMessage) );
        }
        
        Test.stopTest();
    }
    
    @isTest static void shouldShowErrorCreatingNetCAPProductForEmptyCAPAcc() {
        
        Account acc = [SELECT Id, CAP_Value__c FROM Account WHERE Name = 'AcademiaBrasilCompanyPai'];
        acc.CAP_Value__c = null;
        Database.update(acc);
        
        Opportunity opp = [SELECT Id FROM Opportunity WHERE Name = 'academiaBrasilCompanyOpp'];
        
        Product_Item__c lProductItem = new Product_Item__c(    
            recordTypeId = gProdRtId,
            Product_Type__c = 'In person',
            Is_Network_CAP__c = 'Yes',
            CurrencyIsoCode = 'BRL',
            Do_You_Have_A_No_Show_Fee__c = 'No',
            Has_Late_Cancellation_Fee__c = 'No',
            Late_Cancellation_Percent__c = '0',
            Maximum_Live_Class_Per_Month__c = 0,
            No_Show_Fee_Percent__c = '0',
            Net_Transfer_Price__c  =100,
            Product_Definition__c  ='Dança',
            Product_Restriction__c = 'None',
            Name = 'ProdTestTrigger',
            Opportunity__c = opp.Id,     
            Max_Monthly_Visit_Per_User__c = 12,
            Reference_Price_Value_Unlimited__c = 200,
            Price_Visits_Month_Package_Selected__c = 400
        );
        
        Test.startTest();

        try{
        	INSERT lProductItem;
    	}catch(Exception e) {
            String lErrorMessage = 'CAP value in account is blank. Please, enter a value before saving this product.';
            System.Assert( e.getMessage().contains(lErrorMessage) );
        }
        
        Test.stopTest();
    }
    
       @isTest static void shouldShowErrorCreatingNetCAPProductForNonFranchiseAcc2() {
        
        Account acc = [SELECT Id, CAP_Value__c FROM Account WHERE Name = 'AcademiaBrasilCompanyPai'];
        Database.update(acc);
        List<Product_Item__c> lLstProdItem = new List<Product_Item__c>();
        ProductItemTriggerCAPHelper prodItemTriggerCapHelper = new ProductItemTriggerCAPHelper();
        Opportunity opp = [SELECT Id FROM Opportunity WHERE Name = 'academiaBrasilCompanyOpp'];
                
        Product_Item__c lProductItem = new Product_Item__c(    
            recordTypeId = gProdItemRtId,
            Product_Type__c = 'In person',
            Is_Network_CAP__c = 'Yes',
            CurrencyIsoCode = 'BRL',
            Do_You_Have_A_No_Show_Fee__c = 'No',
            Has_Late_Cancellation_Fee__c = 'No',
            Late_Cancellation_Percent__c = '0',
            Maximum_Live_Class_Per_Month__c = 0,
            No_Show_Fee_Percent__c = '0',
            Net_Transfer_Price__c  =100,
            Product_Definition__c  ='Dança',
            Product_Restriction__c = 'None',
            Name = 'ProdTestTrigger',
            Opportunity__c = opp.Id,     
            Max_Monthly_Visit_Per_User__c = 12,
            Reference_Price_Value_Unlimited__c = 200,
            Price_Visits_Month_Package_Selected__c = 400
        );
        insert lProductItem;
        lLstProdItem.add(lProductItem);
        Test.startTest();

        try{
        	prodItemTriggerCapHelper.populateOnBeforeUpdateCAPNetworkInProduct(lLstProdItem);
    	}catch(Exception e) {
             system.debug('entrouuussss nao');
            String lErrorMessage = 'You can not use network CAP value for non franchise account hierarchy type.';
            System.Assert( e.getMessage().contains(lErrorMessage) );
        }
        
        Test.stopTest();
    }
}