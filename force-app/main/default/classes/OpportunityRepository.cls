/**
* @author vinicius.ferraz
* @description Provide mockable repository data layer for Opportunity domain
*/
public with sharing virtual class OpportunityRepository {
    public OpportunityRepository() {
        
    }

    /**
     * throwable System.QueryException
     */
    public virtual Opportunity byId(String recordId){
        System.debug(recordId);
        return [SELECT Id
                ,Name
                ,Reseller_del__r.Sales_Broker__c
                ,Adm_third_party__c
                ,Account.ID_Gympass_CRM__c
                ,Quantity_Offer_Number_of_Employees__c
                ,Account.autonomous_marketplace_contract__c
                ,Account.BDR__r.Name
                ,Does_it_have_unique_technical_request__c
                ,System_Connectivity__c
                ,Payroll_Deduction__c
                ,Employee_registration_validation_process__c
                ,Communication_to_Employees__c
                ,Eligibility_File_Exchange__c
                ,autonomous_marketplace_contract__c
                ,AccountId
                ,Deal_Desk_Approval_Needed__c
                ,Pontos_de_contato_financeiro_no_cliente__c
                ,Manager__c
                ,RecordType.Name  
                ,Eligible_List_Registration_Method__c
                ,Gympass_Entity__c
                ,Gympass_Entity__r.Id
                ,Gympass_Entity__r.Name
                ,Deal_Desk_Approved__c
                ,CreatedDate
                ,StageName
                ,Achieved_Steps_Towards_Success_Quantity__c
                ,Account.NumberOfEmployees
                ,Pricebook2Id
                ,Account.BillingStreet
                ,Account.BillingState
                ,Account.BillingCity
                ,Account.BillingCountry
                ,Account.BillingPostalCode
                ,FastTrackStage__c
                ,Potential__c
                ,CurrencyIsoCode
                ,Competitor__c
                ,Is_there_a_competitor__c
                ,Pricebook2.Name
                ,Adjusted_Probability__c
                ,TotalOpportunityQuantity
                ,CloseDate
                ,LastModifiedById
                ,CreatedById
                ,LastModifiedBy.Name
                ,CreatedBy.Name
                ,Commercial_Conditions_Approved__c
                ,RecordType.DeveloperName
                ,RecordTypeId
                ,Company_Target__c
                ,Commercial_Conditions_Approval_Needed__c
                ,Data_do_Lancamento__c
                ,OwnerId
                ,No_Won_Opportunity_in_SF__c
                ,Sales_Channel__c
                ,Customize_Token_Field_Text_in_Portal__c
                ,Send_a_Welcome_Email__c
                ,Comments__c
                ,Sub_Type__c
                ,Loss_Reason__c
                ,Owner.Email
                ,Account.Razao_Social__c
                ,Account.Id_Company__c
                ,Pipefy_Card_ID__c
                ,Pipefy_Card_URL__c
                ,Adjusted_Probability_SMB__c
                ,(Select Id, Name, Achieved__c, Step_Number__c FROM Steps_Towards_Success1__r Order By Step_Number__c ASC)
                ,Vendor_Portal__c
                ,portal_address__c
                ,Responsavel_Gympass_Relacionamento__r.Email
                ,Self_Checkout_New_Business__c
                ,B_M__c
                ,Number_of_Eligibles__c
                ,Responsavel_Gympass_Relacionamento__c
                ,Reseller_del__c
                ,GP_Executivo_de_Diretos_del__c
                ,GP_Executivo_de_Diretos_del__r.Name
                FROM Opportunity
                WHERE Id = :recordId ];
    }

    public virtual Opportunity byIdSMBForAccount(String accountId, String opportunityId){
        System.debug(accountId+ ' - '+ opportunityId);
        return [SELECT Id
                ,Eligible_List_Registration_Method__c
                ,Account.ID_Gympass_CRM__c
                ,RecordType.Name
                ,Name
                ,Quantity_Offer_Number_of_Employees__c
                ,Account.autonomous_marketplace_contract__c
                ,Account.BDR__r.Name
                ,Does_it_have_unique_technical_request__c
                ,System_Connectivity__c
                ,Payroll_Deduction__c
                ,Employee_registration_validation_process__c
                ,Communication_to_Employees__c
                ,Eligibility_File_Exchange__c
                ,autonomous_marketplace_contract__c
                ,AccountId
                ,Deal_Desk_Approval_Needed__c
                ,Pontos_de_contato_financeiro_no_cliente__c
                ,Manager__c
                ,Gympass_Entity__c
                ,Gympass_Entity__r.Id
                ,Gympass_Entity__r.Name
                ,Deal_Desk_Approved__c
                ,CreatedDate
                ,StageName
                ,Achieved_Steps_Towards_Success_Quantity__c
                ,Account.NumberOfEmployees
                ,Pricebook2Id
                ,Account.BillingStreet
                ,Account.BillingState
                ,Account.BillingCity
                ,Account.BillingCountry
                ,Account.BillingPostalCode
                ,FastTrackStage__c
                ,Potential__c
                ,CurrencyIsoCode
                ,Competitor__c
                ,Is_there_a_competitor__c
                ,Pricebook2.Name
                ,Adjusted_Probability__c
                ,TotalOpportunityQuantity
                ,CloseDate
                ,LastModifiedById
                ,CreatedById
                ,LastModifiedBy.Name
                ,CreatedBy.Name
                ,Commercial_Conditions_Approved__c
                ,RecordType.DeveloperName
                ,RecordTypeId
                ,Company_Target__c
                ,Commercial_Conditions_Approval_Needed__c
                ,Data_do_Lancamento__c
                ,OwnerId 
                ,No_Won_Opportunity_in_SF__c
                ,Sales_Channel__c
                ,Customize_Token_Field_Text_in_Portal__c
                ,Send_a_Welcome_Email__c
                ,Comments__c
                ,Sub_Type__c
                ,Loss_Reason__c
                ,Owner.Email
                ,Account.Razao_Social__c
                ,Account.Id_Company__c
                ,Pipefy_Card_ID__c
                ,Pipefy_Card_URL__c
                ,Adjusted_Probability_SMB__c
                ,Responsavel_Gympass_Relacionamento__c
                ,(Select Id, Name, Achieved__c, Step_Number__c FROM Steps_Towards_Success1__r Order By Step_Number__c ASC)
                FROM Opportunity
                WHERE AccountId = :accountId 
                 AND Id = :opportunityId
                AND (RecordType.Name = 'SMB - New Business' or RecordType.Name = 'SMB Success - Renegotiation' )
                order by CreatedDate desc limit 1];
    }
    
    /**
* throwable System.QueryException
     */
    public virtual Map<Id, Opportunity> getIdToOppByIds(List<Id> recordIdList){
        Map<Id, Opportunity> oppIdToOpp = new Map<Id, Opportunity>([ 
            SELECT  Id, 
                    Name,
                    Account.autonomous_marketplace_contract__c, 
                    Account.BDR__r.Name, 
                    Does_it_have_unique_technical_request__c,
                    System_Connectivity__c,
                    Payroll_Deduction__c,
                    Employee_registration_validation_process__c, 
                    Communication_to_Employees__c,
                    Eligibility_File_Exchange__c,
                    autonomous_marketplace_contract__c,
                    AccountId,
                    Deal_Desk_Approval_Needed__c, 
                    Pontos_de_contato_financeiro_no_cliente__c, 
                    Manager__c, Gympass_Entity__c, 
                    Gympass_Entity__r.Id,
                    Gympass_Entity__r.Name,
                    Deal_Desk_Approved__c, 
                    CreatedDate, 
                    StageName, 
                    Achieved_Steps_Towards_Success_Quantity__c, 
                    Account.NumberOfEmployees, 
                    Pricebook2Id,
                    Account.BillingStreet, 
                    Account.BillingState, 
                    Account.BillingCity, 
                    Account.BillingCountry, 
                    Account.BillingPostalCode, 
                    FastTrackStage__c, 
                    CurrencyIsoCode, 
                    Competitor__c, 
                    Is_there_a_competitor__c, 
                    Pricebook2.Name, 
                    Adjusted_Probability__c, 
                    TotalOpportunityQuantity, 
                    CloseDate, 
                    LastModifiedById, 
                    CreatedById, 
                    LastModifiedBy.Name, 
                    CreatedBy.Name,
                    Commercial_Conditions_Approved__c,
                    RecordType.DeveloperName,
                    Company_Target__c,
                    Potential__c, 
                    Commercial_Conditions_Approval_Needed__c,
                    Data_do_Lancamento__c, 
                    OwnerId, 
                    Opportunity_Offer_Approved_Date__c, 
                    Opportunity_Offer_Sent_Date__c,
                    Responsavel_Gympass_Relacionamento__c
            from Opportunity
            where Id IN :recordIdList 
        ]);

        return oppIdToOpp;
    }

      /**
     * throwable System.DMLException
     */
    public virtual Opportunity add(Opportunity record){
        Database.insert(record);
        return record;
    }

    /**
     * throwable System.DMLException
     */
    public virtual Opportunity edit(Opportunity record){
        Database.update(record);
        return record;
    }

    /**
     * throwable System.DMLException
     */
    public virtual Opportunity addOrEdit(Opportunity record){
        Database.upsert(record, Opportunity.Id, true);
        return record;
    }

    /**
     * throwable System.DMLException Custom Metadata OppxFastTrackStages__mdt
     */
    public virtual OppxFastTrackStages__mdt getValuesMdt(integer i){
        OppxFastTrackStages__mdt record = [Select Index__c, Fast_Track_Stage__c, Opportunity_Stage__c from OppxFastTrackStages__mdt where Index__c =:i];
        return record;
    }
    
    public virtual void LockOrUnlockOpportunityById(ID recordId){
        if(Approval.isLocked(recordId)) {
            Approval.lock(recordId);
        } else {
            Approval.unlock(recordId);
        }
    }

    public virtual Opportunity sync(Id quoteId, Id opportunityId){
        Opportunity opp = new Opportunity(Id=opportunityId,SyncedQuoteId=quoteId);
        this.edit(opp);
        return opp;
    }
   
}