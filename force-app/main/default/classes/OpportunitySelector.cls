/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 04-25-2022
 * @last modified by  : alysson.mota@gympass.com
 * Modifications Log 
 * Ver   Date         Author                             Modification
 * 1.0   07-19-2021   gft.samuel.silva@ext.gympass.com   Initial Version
**/
public inherited sharing class OpportunitySelector extends ApplicationSelector {
	private static final PS_Constants constants = PS_Constants.getInstance();

	private static final Set<Id> partnerRenegotiationRT = 
            new Set<Id>{    Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Partner_Small_and_Medium_Renegotiation').getRecordTypeId(),
                            Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Partner_Wishlist_Renegotiation').getRecordTypeId()
                        };  

     public List<Schema.SObjectField> getSObjectFieldList() {
		return new List<Schema.SObjectField> {
				Opportunity.Id,
				Opportunity.Name,
				Opportunity.AccountId,
				Opportunity.StageName,
				Opportunity.Owner_Name__c
		};
	}

    public Schema.SObjectType getSObjectType() {
		return Opportunity.sObjectType;
	}

    public List<Opportunity> selectById(Set<Id> ids) {
		return (List<Opportunity>) super.selectSObjectsById(ids);
	}

	public List<Opportunity> byId(Set<Id> ids) {
		return (List<Opportunity>) Database.query(
				newQueryFactory().
				selectField(Opportunity.Id).
				selectField(Opportunity.StageName).
				selectField(Opportunity.RecordTypeId).
				selectField(Opportunity.AccountId).
				selectField(Opportunity.Cancellation_date__c).
				selectField(Opportunity.Sub_Type__c).
				selectField(Opportunity.Cancellation_Reason__c).
				selectField(Opportunity.Cancellation_Reason_subcategory__c).
				selectField(Opportunity.Joint_Business_Plan__c).
				selectField(Opportunity.PR_Activity__c).
				selectField(Opportunity.Legal_representative__c).
				selectField(Opportunity.MasterOpportunity__c).
				selectField(Opportunity.Requires_CMS_Approval_New__c).
				selectField('Legal_representative__r.Name').
				selectField('Legal_representative__r.Email').
				selectField('Legal_representative__r.Phone').
				selectField('RecordType.DeveloperName').
				selectField('RecordType.Name').
				setCondition('Id IN :ids').
				toSOQL()
		);
	}	

	/** tags: clone, cloning, to clone, won, partners clone 
	 * toClone = all fields from sobject
	*/
    public Map<Id, Opportunity> selectLastWonToClone( Set<Id> aAccountList ) {
		Map<Id, Opportunity> mAccountOpp = new Map<Id, Opportunity>();
		List<Opportunity> aOppList = (List<Opportunity>) Database.query(
									newQueryFactory().
										selectFields( getAllFieldsFromOpportunity() ).
										setCondition('AccountId in : aAccountList AND isClosed = true AND StageName = \'Lançado/Ganho\'').
										setOrdering(Opportunity.CreatedDate, fflib_QueryFactory.SortOrder.DESCENDING).
										setLimit(1).
										toSOQL()
									);
		
		for(Opportunity opp : aOppList){
			mAccountOpp.put(opp.AccountId, opp);
		}
		
		return mAccountOpp;
	}

	/** tags: clone, cloning, to clone, won 
	 * toClone = all fields from sobject
	*/
    public Map<Id, Opportunity> selectLastWonToClone( Set<Id> aAccountList, Set<String> aSetRecordTypeDevName) {
		Map<Id, Opportunity> mAccountOpp = new Map<Id, Opportunity>();
        List<Opportunity> aOppList = (List<Opportunity>) Database.query(
									newQueryFactory().
									//fields
										selectFields(getAllFieldsFromOpportunity()).
									//where
										setCondition('AccountId in : aAccountList AND isClosed = true AND StageName = \'Lançado/Ganho\' AND Recordtype.DeveloperName IN: aSetRecordTypeDevName').
										setOrdering(Opportunity.CreatedDate, fflib_QueryFactory.SortOrder.DESCENDING).
										setLimit(1).
										toSOQL()
									);
	
		for(Opportunity opp : aOppList){
			mAccountOpp.put(opp.AccountId, opp);
		}
		
		return mAccountOpp;
	}

	 public Database.QueryLocator selectPartnerToLostQueryLocator() {
		
        return Database.getQueryLocator(newQueryFactory().
										selectField(Opportunity.Id).
										selectField(Opportunity.StageName).
										selectField(Opportunity.RecordTypeId).
										selectField(Opportunity.AccountId).
										selectField(Opportunity.Cancellation_date__c).
										selectField(Opportunity.Sub_Type__c).
										selectField(Opportunity.Cancellation_Reason__c).
										selectField(Opportunity.Cancellation_Reason_subcategory__c).
										setCondition('Cancellation_Date__c = TODAY AND ' +
										'StageName = \'Waiting Cancellation\' AND Sub_Type__c = \'Retention\' AND RecordTypeId IN : partnerRenegotiationRT').
										toSOQL()
									);									
	}

	public Database.QueryLocator selectPartnerToNotifyRenegotiationQueryLocator() {
		
		Date firstNnotification = Date.today() + 30;
   		Date secondNotification = Date.today() + 60;
    	Date thirdNnotification = Date.today() + 90;
		
		return Database.getQueryLocator(newQueryFactory().
										selectField(Opportunity.Id).
										selectField(Opportunity.AccountId).
										selectField(Opportunity.RecordTypeId).
										selectField(Opportunity.OwnerId).
										selectField(Opportunity.Name).
										selectField(Opportunity.Cancellation_Date__c).
										selectField(Opportunity.Sub_Type__c).
										setCondition('RecordTypeId IN : partnerRenegotiationRT AND'+
										' Sub_Type__c = \'Retention\' AND' + 
										' StageName = \'Waiting Cancellation\' AND' +
										' (Cancellation_Date__c  =: firstNnotification OR' +
										' Cancellation_Date__c  =: secondNotification OR' + 
										' Cancellation_Date__c  =: thirdNnotification)').
										toSOQL()
									);													
	}

	public Schema.RecordTypeInfo getDefaultRecordType(){
		Schema.DescribeSObjectResult dsr = Opportunity.SObjectType.getDescribe();
		Schema.RecordTypeInfo defaultRecordType;
		for(Schema.RecordTypeInfo rti : dsr.getRecordTypeInfos()) {
			if(rti.isDefaultRecordTypeMapping()) {
				defaultRecordType = rti;
			}
		}
		return defaultRecordType;
	}

	public List<Opportunity> selectOpportunityForPartnerProductFlow( Set<Id> recordIds ) {	

		return  ( List<Opportunity> ) Database.query( 					
										newQueryFactory(false).
										selectField(Opportunity.Id).
										selectField(Opportunity.AccountId).
										selectField('RecordType.DeveloperName').
										selectField('Account.Wishlist__c').
										setCondition('Id in : recordIds').	
										toSOQL()
									);

	}

	public Opportunity selectOpportunityForPartnerOpsSetupForm(Id opportunityId ) {	

		return  (Opportunity) Database.query( 					
										newQueryFactory(false).
										selectField(Opportunity.Id).
										selectField(Opportunity.RecordTypeId).
                                        selectField(Opportunity.Legal_representative__c).
										selectField(Opportunity.Standard_Payment__c).
										selectField(Opportunity.AccountId).
										selectField(Opportunity.OwnerId).
										selectField(Opportunity.Name).
										selectField(Opportunity.Payment__c).
										setCondition('Id =: opportunityId').	
										toSOQL()
									);

	}

	public List<Opportunity> selectOpportunityAndChildOppsById(Set<Id> oppIds){
		fflib_QueryFactory accountsQueryFactory = newQueryFactory()
				.setCondition('Id IN :oppIds');

		accountsQueryFactory.subSelectQuery('ChildOpportunities__r')
				.selectFields(new Set<String> {'Id', 'Name', 'StageName'})
				.setOrdering('CreatedDate', fflib_QueryFactory.SortOrder.DESCENDING);

		return (List<Opportunity>)Database.query(accountsQueryFactory.toSOQL());
	}

	public Map<Id, Opportunity> selectChildOpportunitiesByMasterOppId(Set<Id> opportunityIds) {
		return new Map<Id, Opportunity> (( List<Opportunity> ) Database.query(
				newQueryFactory()
				.setCondition('MasterOpportunity__c IN :opportunityIds AND RecordType.DeveloperName = \''+constants.OPPORTUNITY_RT_PARTNER_FLOW_CHILD_OPP+'\'')
				.toSOQL())
		);
	}

	public Map<Id, Opportunity> selectOpportunityToCloneChildOpportunity ( Set<Id> opportunityIds ) {
	
		return new Map<Id, Opportunity> ( ( List<Opportunity> ) Database.query(
									newQueryFactory().
										selectFields( getAllFieldsFromOpportunity() ).
										setCondition('Id in : opportunityIds').
										toSOQL()
									));
	}
	
		
	// public Map<Id, List<Opportunity>> selectOpenOpportunityByAccountId ( Set<Id> accountIds ) {
	// 	Map<Id, List<Opportunity>> opportunitiesByAccountId = new Map<Id, List<Opportunity>>();

	// 	List<Opportunity> aOppList = (List<Opportunity>) Database.query(
	// 								newQueryFactory().
	// 									selectField(Opportunity.Id).
	// 									selectField(Opportunity.Name).
	// 									selectField(Opportunity.CloseDate).
	// 									selectField(Opportunity.AccountId).
	// 									selectField('Opportunity.Owner.Name').
	// 									setCondition(' StageName != ' + constants.OPPORTUNITY_STAGE_LANCADO_GANHO + ' AND StageName != ' + constants.OPPORTUNITY_STAGE_PERDIDO + ' AND AccountId in : accountIds').
	// 								//	.setCondition('MasterOpportunity__c IN :opportunityIds AND RecordType.DeveloperName = \''+constants.OPPORTUNITY_RT_PARTNER_FLOW_CHILD_OPP+'\'')
	// 									toSOQL()
	// 								);
	// 	for(Opportunity iOpp : aOppList){
	// 		if(opportunitiesByAccountId.containsKey(iOpp.AccountId)){
	// 			opportunitiesByAccountId.get(iOpp.AccountId).add(iOpp);
	// 		} else {
	// 			opportunitiesByAccountId.put(iOpp.AccountId, new List<Opportunity>{iOpp});
	// 		}
	// 	}

	// 	return opportunitiesByAccountId;
	// }
	

    private Set<String> getAllFieldsFromOpportunity(){
		Map<String, Schema.SObjectField> opportunityFields = Schema.getGlobalDescribe().get('Opportunity').getDescribe().fields.getMap();
        return opportunityFields.keySet();
    }


}