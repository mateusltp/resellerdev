@IsTest
public class ProductMock {
  public static Product2 getSetupFee() {
    return new Product2(
      Family = 'Setup Fee',
      IsActive = true,
      Name = 'Setup Fee',
      ProductCode = 'Setup Fee',
      Family_Member_Included__c = true,
      Maximum_Number_of_Employees__c = 900,
      Minimum_Number_of_Employees__c = 0
    );
  }

  public static Product2 getAccessFee() {
    return new Product2(
      Family = 'Enterprise Subscription',
      IsActive = true,
      Name = 'Enterprise Subscription',
      ProductCode = 'Enterprise Subscription',
      Family_Member_Included__c = true,
      Maximum_Number_of_Employees__c = 900,
      Minimum_Number_of_Employees__c = 0
    );
  }

  public static Product2 getAccessFeeFamilyMemberIncluded() {
    return new Product2(
      Family = 'Enterprise Subscription',
      IsActive = true,
      Name = 'Access Fee Family Member Included',
      ProductCode = 'Access_Fee_with_Family_Members',
      Family_Member_Included__c = true,
      Maximum_Number_of_Employees__c = 900,
      Minimum_Number_of_Employees__c = 0
    );
  }

  public static Product2 getProfServicesMaintenanceFee() {
    return new Product2(
      Family = 'Professional Services',
      IsActive = true,
      Name = 'Professional Services Maintenance Fee',
      ProductCode = 'Professional Services Maintenance Fee',
      Family_Member_Included__c = true,
      Maximum_Number_of_Employees__c = 900,
      Minimum_Number_of_Employees__c = 0
    );
  }
}