@IsTest
public class SelfCheckoutOpportunityPublisherTest {
 
  @IsTest
  public static void publish() {

    SelfCheckoutOpportunityRequest selfCheckoutOpportunityRequest = SelfCheckoutOpportunityRequestMock.getMock();

    Test.startTest();

    SelfCheckoutOpportunityPublisher publisher = new SelfCheckoutOpportunityPublisher();
    publisher.publish( selfCheckoutOpportunityRequest );
    
    List<Queue__c> events =[ select Id,EventName__c,Status__c from Queue__c where EventName__c = 'SELF_CHECKOUT_OPPORTUNITY_INBOUND' ];
    System.assertEquals(events.size(),1);
    System.assertEquals('QUEUED' , events[0].Status__c);

    Test.stopTest();

  }

 
}