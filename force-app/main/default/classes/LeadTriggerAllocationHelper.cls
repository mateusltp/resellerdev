/**
 * @File Name          : LeadTriggerAllocationHelper.cls
 * @Description        : 
 * @Author             : GEPI@GFT.com
 * @Group              : 
 * @Last Modified By   : gft.jorge.stevaux@ext.gympass.com
 * @Last Modified On   : 03-25-2022
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    27/04/2020      MLNC@GFT.com     Initial Version
**/

public without sharing class LeadTriggerAllocationHelper {
    Id rtIdOwnerLead = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Partner_Owner_Referral_Lead').getRecordTypeId();
	List<Lead> leadTrigglst = trigger.new;
  
    public void LeadUpdUser(){
        List<Lead> leadTrigglst = trigger.new;
        List<Lead> leadUpdlst = new List<Lead> ();
        List<Allocation_Base__c> baseAllocation = new List<Allocation_Base__c> ();       

        set<String> setCountry = new set<String>(); 
        for(Lead l : leadTrigglst){ 
            if ( l.RecordTypeId == rtIdOwnerLead && l.Status != 'Convertido' && l.CountryCode != null ){
            	setCountry.add(l.CountryCode);
            }
        }   
 
       baseAllocation = [ SELECT Id,Name, FirstName__c,LastName__c,Id_User__c,CountryCode__c,State__c,StateCode__c,City__c  FROM Allocation_Base__c 
                         WHERE Status__c = true AND CountryCode__c =: setCountry ];
        if ( !baseAllocation.isEmpty() ){   
           for(Lead lercd : leadTrigglst){ 
                for ( Allocation_Base__c  allrcr : baseAllocation  ){       
                       system.debug('LEAD');
                            if (  lercd.RecordTypeId == rtIdOwnerLead && allrcr.Id_User__c != null && ( lercd.Type_of_Allocation__c != 'Manually' || lercd.Type_of_Allocation__c == 'Automatic' )  && lercd.CountryCode != NULL && lercd.CountryCode == allrcr.CountryCode__c && lercd.Is_refer_a_new_gym__c == false  ){
                                   if( lercd.CountryCode == 'BR' ) {
                                         if ( lercd.StateCode == 'SP' && lercd.StateCode == allrcr.StateCode__c  ){
                                             	if ( lercd.City != NULL &&  lercd.City == allrcr.City__c ){                                             		
                                                        lercd.OwnerId = allrcr.Id_User__c; 
                                                        lercd.Type_of_Allocation__c = 'Automatic';
                                                }
                                                
                                   		   }
                                         else if (lercd.StateCode == allrcr.StateCode__c) {
                                                lercd.OwnerId = allrcr.Id_User__c; 
                                                lercd.Type_of_Allocation__c = 'Automatic'; 
                                         }
													
                                   }
                                   else if( lercd.CountryCode == 'US'  ){ 
                                        	if ( lercd.StateCode == 'CA' && lercd.StateCode == allrcr.StateCode__c ){ 
                                                if ( lercd.City != NULL &&  lercd.City == allrcr.City__c  ){
                                                        lercd.OwnerId = allrcr.Id_User__c; 
                                                        lercd.Type_of_Allocation__c = 'Automatic';
                                                }
                                            }
                                        	else if (lercd.StateCode == allrcr.StateCode__c) {
                                                   lercd.OwnerId = allrcr.Id_User__c; 
                                                   lercd.Type_of_Allocation__c = 'Automatic'; 
                                            }
                                    }
                                    else if( lercd.CountryCode == 'MX' && lercd.StateCode == allrcr.StateCode__c  ){  
                                            lercd.OwnerId = allrcr.Id_User__c; 
                                            lercd.Type_of_Allocation__c = 'Automatic';
                                            
                                    }
                                    else if( lercd.CountryCode == 'AR' && lercd.StateCode == allrcr.StateCode__c  ){  
                                         if ( lercd.StateCode == 'BA' && lercd.City == allrcr.City__c && ( lercd.City == 'Ciudad Autónoma de Buenos Aires' || lercd.City == 'Ciudad Autonoma de Buenos Aires'  ) ){ 
                                                        lercd.OwnerId = allrcr.Id_User__c; 
                                                        lercd.Type_of_Allocation__c = 'Automatic';
                                            
                                         }
                                         else if (lercd.StateCode == allrcr.StateCode__c) {
                                                   lercd.OwnerId = allrcr.Id_User__c; 
                                                   lercd.Type_of_Allocation__c = 'Automatic'; 
                                         }
                                        
                                            
                                    }
                                
                                    else if( lercd.CountryCode == 'CL'  ){  
                                            lercd.OwnerId = allrcr.Id_User__c;
                                            lercd.Type_of_Allocation__c = 'Automatic';
                                            
                                    }
                                         
                            }                		
                   
                }   
            }
        }
   }     
}