public class WaiverApprovalUpdateQuote {
    
    private List<Waiver__c> m_lstDataNew;
    private Map<Id, Waiver__c> m_mapDataOld;   
    private Map<Id, Waiver__c> m_mapObjFiltered = new Map<Id, Waiver__c>();
    public static Id QUOTE_RECORD_TYPE_INDIRECT_CHANNEL = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Indirect_Channel').getRecordTypeId();
    
    QuoteRepository quoteRepository = new QuoteRepository();
    QuoteLineItemRepository quoteLineItemRepository = new QuoteLineItemRepository();
    //ResellerSKUEditController resellerSKUEditController = new ResellerSKUEditController();
    
    public WaiverApprovalUpdateQuote(List<Waiver__c> lstDataNew, Map<Id, Waiver__c> mapDataOld) {
        this.m_lstDataNew = lstDataNew;
        this.m_mapDataOld = mapDataOld;
    }
    
    private void filter() {
        for(Waiver__c itemNew : this.m_lstDataNew) {
            Waiver__c itemOld = this.m_mapDataOld.get(itemNew.Id);
            if(itemOld.Approval_Status__c == 'Pending Approval' &&
               itemNew.Approval_Status__c == 'Approved'
              ){
                  m_mapObjFiltered.put(itemNew.Id, itemNew);
              }
            
        }
    } 
    
    public void run(){
        
        filter();
        
        if(!m_mapObjFiltered.isEmpty()){
            updateQuote(m_mapObjFiltered);
        }
        
    }
    
    public void updateQuote(Map<Id, Waiver__c> m_mapObjFiltered){
        
        String quoteLineItemId;
        
        for(Waiver__c waiver : m_mapObjFiltered.values()){ 
            quoteLineItemId = waiver.Quote_Line_Item__c;
        }
        
        QuoteLineItem quoteLineItem = [SELECT Id,QuoteId FROM QuoteLineItem WHERE Id =: quoteLineItemId];
        Quote quote = [SELECT Id,Waiver_approved__c FROM QUOTE WHERE Id =: quoteLineItem.QuoteId];
        quote.Waiver_approved__c = true;
        update quote;
    }
}