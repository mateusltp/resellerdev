/**
 * @description       : Updates waivers to follow contract start date
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 07-04-2022
 * @last modified by  : tiago.ribeiro@gympass.com
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   03-08-2021  gft.samuel.silva@ext.gympass.com   Initial Version
**/
public without sharing class WaiverTemporaryDiscountHandler {

    Map<Id, List<Waiver__c>> gQuoteWaiverMap = new Map<Id, List<Waiver__c>>();
    Map<Id, Quote> gMapQuote = new Map<Id, Quote>();
    Id quoteRecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
    
    List<Waiver__c> gAllWaivers = new List<Waiver__c>();

    //handle call
    public void waiverStartEndDateHandler ( Map<Id,Quote> oldQuotes , List<Quote> newQuotes){
        Set<Id> setIdQuotes = new Set<Id>();
        for ( Quote quote : newQuotes ) {
            if ((quote.get('Start_Date__c') != oldQuotes.get(quote.Id).get('Start_Date__c')) 
                && quote.RecordTypeId == quoteRecordTypeId && quote.IsSyncing && quote.Run_Waiver_Trigger__c){                  
                    gMapQuote.put(quote.Id, quote);
            }             
        }
        
        if( !gMapQuote.isEmpty() ){
            handleWaiversAndTemporaryDiscount( gMapQuote.keySet() );
        }
    }

    //process waivers
    private void handleWaiversAndTemporaryDiscount ( Set<Id> lProposalIdSet ){

        getQuoteWaivers(lProposalIdSet);
        if(!gQuoteWaiverMap.isEmpty()){
            for(ID id : gMapQuote.keySet()){
                calculateWaiversPeriod(gMapQuote.get(id), gQuoteWaiverMap.get(id));
            }
            if(!gAllWaivers.isEmpty()){
               Database.update(gAllWaivers);
            }
        }
    }

    //set date for individuals iWaiver
    private void calculateWaiversPeriod(Quote aQuote, List<Waiver__c> aWaiverLst){
        for(Waiver__c iWaiver : aWaiverLst){
            Integer daysBtw = iWaiver.Start_Date__c.daysBetween(iWaiver.End_Date__c);
            iWaiver.Start_Date__c = aQuote.Start_Date__c;
            iWaiver.End_Date__c = iWaiver.Start_Date__c.addDays(daysBtw);
            gAllWaivers.add(iWaiver);
        }        
    }
    
    //very specific conditions, dont seem like a repository method
    // Find all waivers from proposals that are set to Consider_Contract_Start_Date__c 
    private void getQuoteWaivers ( Set<Id> lProposalIdSet ){
        for(Waiver__c iWaiver : [SELECT Id, Start_Date__c, End_Date__c, 
                                    Payment__r.Quote_Line_Item__r.QuoteId
                                    FROM Waiver__c 
                                    WHERE Payment__r.Quote_Line_Item__r.QuoteId IN: lProposalIdSet
                                    AND Consider_Contract_Start_Date__c = true] ){                                    
                                        
            if (gQuoteWaiverMap.containsKey(iWaiver.Payment__r.Quote_Line_Item__r.QuoteId)) {
                gQuoteWaiverMap.get(iWaiver.Payment__r.Quote_Line_Item__r.QuoteId).add(iWaiver);
            } else {
                gQuoteWaiverMap.put(iWaiver.Payment__r.Quote_Line_Item__r.QuoteId, new List<Waiver__c>{iWaiver});
            }
        }
    }

}