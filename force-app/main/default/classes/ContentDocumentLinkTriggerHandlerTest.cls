/**
* @author Conga Services, gmodica
* @date 20180914
* @version 1.00
* @description Test class for ContentDocumentLinkTriggerHandler called from ContentDocumentLinkAfterInsert Trigger
*/

@isTest
private class ContentDocumentLinkTriggerHandlerTest {

    @isTest
    private static void testAttachDocumentToLegalAgreement() {
        Account a = new Account();
        a.name = 'test account';
        a.BillingCountry = 'United States';
        a.BillingState = 'Colorado';
        insert a;
        
        APXT_Redlining__Contract_Agreement__c la = new APXT_Redlining__Contract_Agreement__c();
        la.APXT_Redlining__Account__c = a.Id;
        la.RecordTypeId = Schema.SObjectType.APXT_Redlining__Contract_Agreement__c.getRecordTypeInfosByDeveloperName().get('B2B_Contract').getRecordTypeId();
        insert la;
        
        ContentDocumentLink cdl = new ContentDocumentLink();

       ContentVersion cv = new ContentVersion();
       cv.PathOnClient = 'any name.pdf';
       cv.Title = 'any name';
       cv.VersionData = Blob.valueOf('pdfBody');
       insert cv;

       ContentVersion udpatedCV = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :cv.Id];

       test.startTest();

       cdl.LinkedEntityId = la.Id;
       cdl.ContentDocumentId = udpatedCV.ContentDocumentId;
       cdl.ShareType = 'I';
       insert cdl;

       test.stopTest();
    }
        
}