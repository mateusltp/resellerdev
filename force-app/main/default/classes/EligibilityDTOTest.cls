@IsTest
public class EligibilityDTOTest {
  @IsTest
  public static void execute() {
    Eligibility__c eligibility = new Eligibility__c();

    new EligibilityDTO(eligibility).getIdentifierType(eligibility);
    new EligibilityDTO(eligibility).getExchangeType(eligibility);
    new EligibilityDTO(eligibility).getCommunicationType(eligibility);

    eligibility.Payment_Method__c = 'Payroll + Credit Card';
    new EligibilityDTO(eligibility).getPayrollDeductionType(eligibility);

    eligibility.Payment_Method__c = 'Payroll';
    new EligibilityDTO(eligibility).getPayrollDeductionType(eligibility);

    eligibility.Payment_Method__c = null;
    new EligibilityDTO(eligibility).getPayrollDeductionType(eligibility);

    eligibility.Payment_Method__c = 'Credit Card';
    new EligibilityDTO(eligibility).getPaymentMethodType(eligibility);

    eligibility.Payment_Method__c = 'Payroll + Credit Card';
    new EligibilityDTO(eligibility).getPaymentMethodType(eligibility);

    eligibility.Payment_Method__c = 'Payroll';
    new EligibilityDTO(eligibility).getPaymentMethodType(eligibility);
    new EligibilityDTO(eligibility).getPaymentMethodType(new EligibilityDTO(eligibility));

    eligibility.Payment_Method__c = null;
    new EligibilityDTO(eligibility).getPaymentMethodType(eligibility);
    new EligibilityDTO(eligibility).getPaymentMethodType(new EligibilityDTO(eligibility));
  }
}