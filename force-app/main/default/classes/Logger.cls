/**
 * @description       : Logger singleton class
 * @author            : bruno.mendes@gympass.com
 *                      1. instantiate a DebugLog__c record  : DebugLog__c log = new DebugLog__c(
                                                                                    Origin__c = '[className][methodName]',                          - required
                                                                                    LogType__c = constants.LOGTYPE_ERROR,                           - required
                                                                                    RelatedToSquad__c = constants.LOGTYPE_SQUAD_PARTNERS,           - required
                                                                                    ExceptonMessage__c = e.getMessage(),                            - optional (required on LogType = Error)
                                                                                    StackTrace__c = e.getStackTrace(),                              - optional
                                                                                    ObjectId__c = scopeObj.Id,                                      - optional
                                                                                    ScopeData__c = JSON.serialize(scope),                           - optional
                                                                                    Comments__c = 'A comment that could help with context or cause' - optional
                                                                            );
                        2. create log                   : Logger.createLog(log);
 * @last modified on  : 29-12-2021
 * @last modified by  : bruno.mendes@gympass.com
 * Modifications Log 
 * Ver   Date         Author                             Modification
 * 1.0   29-12-2021   bruno.mendes@gympass.com   Initial Version
**/
public with sharing class Logger
{

    private static fflib_ISObjectUnitOfWork uow = Application.UnitOfWork.newInstance();

    public static void createLog(DebugLog__c log)
    {
        try
        {
            uow.registerNew(log);
            uow.commitWork();
        }
        catch (Exception e)
        {
            system.debug(LoggingLevel.ERROR, '--- Debug Log was not created. Exception message: ' + e.getMessage());
            system.debug(LoggingLevel.ERROR, '--- Log Exception message: ' + log.ExceptionMessage__c);
            system.debug(LoggingLevel.ERROR, '--- Log Origin: ' + log.Origin__c);
            system.debug(LoggingLevel.ERROR, '--- Log Type: ' + log.LogType__c);
            system.debug(LoggingLevel.ERROR, '--- Log ObjectId: ' + log.ObjectId__c);
            system.debug(LoggingLevel.ERROR, '--- Log Data Scope: ' + log.DataScope__c);
            system.debug(LoggingLevel.ERROR, '--- Log Owner: ' + UserInfo.getUserId());
        }
    }
}