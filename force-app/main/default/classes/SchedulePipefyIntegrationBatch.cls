/**
 * @description       : 
 * @author            : AlyssonMota
 * @group             : 
 * @last modified on  : 08-05-2021
 * @last modified by  : Alysson Mota
**/
global class SchedulePipefyIntegrationBatch implements Schedulable, Database.AllowsCallouts {
    global void execute(SchedulableContext SC) {
        Database.executeBatch(new PipefyIntegrationBatch(), 5);
    }
}