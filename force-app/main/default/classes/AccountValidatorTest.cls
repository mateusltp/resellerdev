@isTest
public with sharing class AccountValidatorTest {
    @TestSetup
    static void makeData(){

        Account account = new Account();  
        account.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Reseller').getRecordTypeId();
        account.Name = 'teste';
        account.Legal_Document_Type__c = 'CNPJ';
        account.Id_Company__c = '56947401000108';
        account.Razao_Social__c = 'TESTE';
        account.Website = 'teste.com';
        account.Industry = 'Airlines'; 						
        account.BillingCountry = 'Brazil';
        insert account;
        
    }

    @isTest
    public static void testResellerAccountValidator(){
        Account account = new Account();  
        account.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Reseller').getRecordTypeId();
        account.Name = 'teste2';
        account.Legal_Document_Type__c = 'CNPJ';
        account.Id_Company__c = '18894491000127';
        account.Razao_Social__c = 'TESTE2';
        account.Website = 'teste.com2';
        account.Industry = 'Airlines'; 						
        account.BillingCountry = 'Brazil';
        insert account;
        
        account.Name = 'teste3';
        update account;
    }
}