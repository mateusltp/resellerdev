/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 03-23-2022
 * @last modified by  : alysson.mota@gympass.com
**/
@isTest(seeAllData=false)
public class AccountContactRelationSelectorTest {


    @TestSetup
    static void setupData(){
        String recordTypePartnerFlow = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Account').getRecordTypeId();
        Account partnerAcc = PartnerDataFactory.newAccount();
        partnerAcc.RecordTypeId = recordTypePartnerFlow;
        INSERT partnerAcc;
        
        Account partnerChildAcc = PartnerDataFactory.newChildAccount(partnerAcc.Id);
        partnerChildAcc.RecordTypeId = recordTypePartnerFlow;
        INSERT partnerChildAcc;

        String recordTypePartnerContactFlow = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Contact').getRecordTypeId();
        Contact partnerContact = PartnerDataFactory.newContact(partnerAcc.Id);
        partnerContact.RecordTypeId = recordTypePartnerContactFlow;
        INSERT partnerContact;

        AccountContactRelation newAccountContactRelation = new AccountContactRelation();
        newAccountContactRelation.AccountId = partnerChildAcc.Id;
        newAccountContactRelation.ContactId = partnerContact.Id;
        INSERT newAccountContactRelation;

    }

    @isTest 
    static void getSObjectFieldList_Test(){
        Test.startTest();
            System.assert(new AccountContactRelationSelector().getSObjectFieldList().contains(AccountContactRelation.Id));
        Test.stopTest();
    }  


    @isTest 
    static void getSObjectType_Test() {
        Test.startTest();
            System.assertEquals(AccountContactRelation.sObjectType, new AccountContactRelationSelector().getSObjectType());
        Test.stopTest();
	}

    @isTest 
    static void selectById_Test() {
        AccountContactRelation aAccountContactRelation = [SELECT Id FROM AccountContactRelation LIMIT 1];
        Test.startTest();
            System.assertEquals(1, new AccountContactRelationSelector().selectById(new Set<Id>{aAccountContactRelation.Id}).size());
        Test.stopTest();
	}

    @isTest 
    static void selectContactRelationById_Test() {
        Account acc = [SELECT Id FROM Account LIMIT 1];
        Test.startTest();
            System.assertEquals(1, new AccountContactRelationSelector().selectContactRelationById(new Set<Id>{acc.Id}).size());
        Test.stopTest();
	}

    @isTest 
    static void selectByAccountIdAndContactId_Test() {
        Contact ctc = [SELECT Id, AccountId FROM Contact LIMIT 1];
        Test.startTest();
            System.assertEquals(1, new AccountContactRelationSelector().selectByAccountIdAndContactId(new Set<Id>{ctc.AccountId}, new Set<Id>{ctc.Id} ).size());
        Test.stopTest();
	}
}