@isTest
public with sharing class FastTrackPBCreateNewOppUpdateAccTest {

    @TestSetup
	static void createData()
    {


        
        Account Acc = DataFactory.newAccount();
        upsert Acc;

        Pricebook2 pb = DataFactory.newPricebook('SMB Price Book', 'Intermediation');
        upsert pb;

   
        Pricebook2 standardPricebook = new Pricebook2(
            Id = Test.getStandardPricebookId(),
            IsActive = true
        );

        upsert standardPricebook;

      
    }

    @isTest
    static void insertNewOpportunity(){

        //Test the set new name and IsNewIndirectChannelNewBusinessEnablersHelper -- call the method class
        Account Acc = [select id, name from Account limit 1];
        Pricebook2 lSmbPricebook = [select id, name from Pricebook2 limit 1];

        List<Opportunity> lopp = new List<Opportunity> ();

        lopp.add(DataFactory.newOpportunity( Acc.Id , lSmbPricebook , 'Indirect_Channel_New_Business' ));

        Opportunity lOpp2 = DataFactory.newOpportunity( Acc.Id , lSmbPricebook , 'Indirect_Channel' );
        lOpp2.name = 'Test name more than 120 characters, Test name more than 120 characters , Test name more than 120 characters , Test name ';

        lopp.add(lOpp2);

        Test.startTest();

            upsert lopp;

            
        Test.stopTest();
        
    }

    @isTest
    static void OpportunityValidated(){
        //create a new Opportunity with StageName != 'Validated' and set to 'Validated' after

        Account Acc = [select id, name from Account limit 1];
        Pricebook2 lSmbPricebook = [select id, name from Pricebook2 limit 1];
        Opportunity opp = DataFactory.newOpportunity( Acc.Id , lSmbPricebook , 'Indirect_Channel' );

        upsert opp;

        Test.startTest();

            Opportunity oppUpdate = [select id, StageName from Opportunity where id =: opp.id limit 1];
            oppUpdate.StageName = 'Validated';
            /*oppUpdate.Name = 'Closed';
            oppUpdate.CloseDate = Date.today();*/

            upsert oppUpdate;
            //System.assertEquals(oppUpdate.StageName != null, true);

        Test.stopTest();
        

    }

    @isTest
    static void OppWonNewBusinessSMBOwner(){
        //create a new Opportunity with StageName != 'Validated' and set to 'Validated' after

        Profile p = [SELECT Id FROM Profile WHERE Name='Direct Channel Relationship BR']; 

        // user SMB
		User usr = new User(
			FirstName ='UserSMBOwner',
			LastName = 'SMBOwner',
			Alias = 'usmbow',
			Email = 'teste.machados@gympass.com',
			Username = 'teste.machados@gympass.com',
			ProfileId = p.id,
			TimeZoneSidKey = 'America/Sao_Paulo',
			LanguageLocaleKey = 'pt_BR',
			EmailEncodingKey = 'ISO-8859-1',
			LocaleSidKey = 'pt_BR'
		);

        insert usr;

        Account Acc = [select id, name, Business_Unit__c, type from Account limit 1];
        Pricebook2 lSmbPricebook = [select id, name from Pricebook2 limit 1];
        System.runAs(usr) 
        {
            Opportunity opp = DataFactory.newOpportunity( Acc.Id , lSmbPricebook , 'Indirect_Channel' );
            upsert opp;
        }


        Test.startTest();

            Opportunity oppUpdate = [select id, name, StageName from Opportunity limit 1];
            oppUpdate.StageName = 'Launched/Won';
            upsert oppUpdate;

            Account AccUpdate = [select id, name, Business_Unit__c, type from Account limit 1];
            System.assertEquals(null, AccUpdate.Business_Unit__c, 'Business_Unit__c need to be SMB');

        Test.stopTest(); 

    }


    @isTest
    static void OppWonNewBusinessNotSMBOwner(){
        //create a new Opportunity with StageName != 'Validated' and set to 'Validated' after

        Profile p = [SELECT Id FROM Profile WHERE Name='Direct Channel Relationship BR']; 

        User usrNotSmb = new User(
			FirstName ='UserNotSMBOwner',
			LastName = 'NOTSMBOwner',
			Alias = 'nusmbow',
			Email = 'notsmbowner@gympass.com',
			Username = 'notsmbowner@gympass.com',
			ProfileId = p.id,
			TimeZoneSidKey = 'America/Sao_Paulo',
			LanguageLocaleKey = 'pt_BR',
			EmailEncodingKey = 'ISO-8859-1',
			LocaleSidKey = 'pt_BR'
		);

        insert usrNotSmb;

        Account Acc = [select id, name, Business_Unit__c from Account limit 1];
        Acc.Business_Unit__c = 'SMB';
        upsert Acc;

        Pricebook2 lSmbPricebook = [select id, name from Pricebook2 limit 1];
        System.runAs(usrNotSmb) 
        {
            Opportunity opp = DataFactory.newOpportunity( Acc.Id , lSmbPricebook , 'Indirect_Channel' );
            upsert opp;

        }

        Test.startTest();

            Opportunity oppUpdate = [select id, name, StageName from Opportunity  limit 1];
            oppUpdate.StageName = 'Launched/Won';
            upsert oppUpdate;

            Account AccUpdate = [select id, name, Business_Unit__c from Account limit 1];
            System.assertEquals(null, AccUpdate.Business_Unit__c, 'Business_Unit__c need to be null');

        Test.stopTest(); 

    }

    @isTest
    static void fastTrackCreateNewOpportunities(){

        Profile p = [SELECT Id FROM Profile WHERE Name='Direct Channel Relationship BR']; 

        // user SMB
		User usr = new User(
			FirstName ='UserSMBOwner',
			LastName = 'SMBOwner',
			Alias = 'usmbow',
			Email = 'teste.machado@gympass.com',
			Username = 'teste.machado@gympass.com',
			ProfileId = p.id,
			TimeZoneSidKey = 'America/Sao_Paulo',
			LanguageLocaleKey = 'pt_BR',
			EmailEncodingKey = 'ISO-8859-1',
			LocaleSidKey = 'pt_BR'
		);

        insert usr;

        Account Acc = [select id, name from Account limit 1];
        Pricebook2 lSmbPricebook = [select id, name from Pricebook2 limit 1];
        Opportunity opp = DataFactory.newOpportunity( Acc.Id , lSmbPricebook , 'Indirect_Channel' );
        opp.Responsavel_Gympass_Relacionamento__c = usr.id;
        upsert opp;

        Test.startTest();
            Opportunity oppUpdate = [select id, name, StageName, Type, Proposal_Payment_Method__c from Opportunity where id =: opp.id limit 1];
            
            oppUpdate.Type = 'New Business';
            oppUpdate.StageName = 'Launched/Won';
            oppUpdate.Proposal_Payment_Method__c = 'Credit Card' ;
            upsert oppUpdate;
        Test.stopTest(); 
    }


}