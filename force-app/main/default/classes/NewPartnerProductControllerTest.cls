/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 05-02-2022
 * @last modified by  : gepi@gft.com
**/
@isTest
public with sharing class NewPartnerProductControllerTest {

    @TestSetup
    static void setupData()
    {
        String recordTypePartnerFlow = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Account').getRecordTypeId();
        Account partnerAcc = PartnerDataFactory.newAccount();
        partnerAcc.RecordTypeId = recordTypePartnerFlow;
        partnerAcc.CRM_ID__c = '1231';
        partnerAcc.Legal_Document_Type__c = 'CNPJ';
        //partnerAcc.Id_Company__c = 'XX.XXX.XXX/XXXX-XX';
        partnerAcc.Legal_Title__c = 'SAA';
        partnerAcc.BillingCountryCode = 'BR';
        partnerAcc.BillingPostalCode = '1223';
        partnerAcc.ShippingCountryCode = 'US';
        partnerAcc.ShippingCountry = 'United States';
        partnerAcc.ShippingState = 'Washington';
        partnerAcc.UUID__c = new Uuid().getValue();
        partnerAcc.Description = 'dasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasda';
        INSERT partnerAcc;

        String recordTypePartnerContactFlow = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Contact').getRecordTypeId();
        Contact partnerContact = PartnerDataFactory.newContact(partnerAcc.Id);
        partnerContact.RecordTypeId = recordTypePartnerContactFlow;
        partnerContact.Type_of_Contact__c = 'Decision Maker';
        partnerContact.FirstName = 'Admin';
        partnerContact.UUID__c = new Uuid().getValue();
        INSERT partnerContact;

        Opportunity aOpp = PartnerDataFactory.newOpportunity(partnerAcc.Id, 'Partner_Flow_Opportunity');
        INSERT aOpp;

        Quote proposal = PartnerDataFactory.newQuote(aOpp);
        proposal.RecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Proposal').getRecordTypeId();
        proposal.Signed__c = Date.Today() - 100;
        proposal.End_Date__c = Date.Today();
        proposal.Start_Date__c = Date.Today() - 100;
        INSERT proposal;


        Account_Opportunity_Relationship__c oppMember = PartnerDataFactory.newAcctOppRel(partnerAcc.Id, aOpp.Id);
        oppMember.RecordTypeId = Schema.SObjectType.Account_Opportunity_Relationship__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Opportunity_Member').getRecordTypeId();
        INSERT oppMember;

        Product_Item__c aProd = PartnerDataFactory.newProduct(aOpp);
        aProd.Type__c = 'Live';
        aProd.Net_Transfer_Price__c = 120;
        aProd.RecordTypeId = Schema.SObjectType.Product_Item__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product').getRecordTypeId();
        aProd.Opportunity__c = null;
        INSERT aProd;

        Commercial_Condition__c aComm = new Commercial_Condition__c();
        aComm.RecordTypeId = Schema.SObjectType.Commercial_Condition__c.getRecordTypeInfosByDeveloperName().get('CAP').getRecordTypeId();
        aComm.Amount__c = 120;
        INSERT aComm;

        Product_Assignment__c prodAssign = new Product_Assignment__c();
        prodAssign.RecordTypeId = Schema.SObjectType.Product_Assignment__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product_Assignment').getRecordTypeId();
        prodAssign.ProductId__c = aProd.Id;
        prodAssign.CommercialConditionId__c = aComm.Id;
        INSERT prodAssign;


        Product_Opportunity_Assignment__c prodOppAssign = new Product_Opportunity_Assignment__c();
        prodOppAssign.RecordTypeId = Schema.SObjectType.Product_Opportunity_Assignment__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product_Opportunity_Assignment').getRecordTypeId();
        prodOppAssign.OpportunityMemberId__c = oppMember.Id;
        prodOppAssign.ProductAssignmentId__c = prodAssign.Id;
        INSERT prodOppAssign;

        Gym_Activity_Relationship__c gymActivityRelationship = new Gym_Activity_Relationship__c();
        gymActivityRelationship.Name = 'Cardio';
        insert gymActivityRelationship;
    }
   
    @IsTest
    private static void showActivities_UnitTest(){ 

        fflib_ApexMocks mocks = new fflib_ApexMocks();
        fflib_SObjectUnitOfWork mockUOW = (fflib_SObjectUnitOfWork)mocks.mock(fflib_SObjectUnitOfWork.class);
        GymActivityRelSelector mockGymActivitySelector = (GymActivityRelSelector) mocks.mock(GymActivityRelSelector.class); 
        Id mockGymActivityId = fflib_IDGenerator.generate(Gym_Activity_Relationship__c.SObjectType);

        List<Gym_Activity_Relationship__c> mockActivitiesLst = new List<Gym_Activity_Relationship__c> { 
                                                                new Gym_Activity_Relationship__c (
                                                                id = mockGymActivityId )
                                                            };
        mocks.startStubbing();
            mocks.when(mockGymActivitySelector.sobjectType()).thenReturn(Gym_Activity_Relationship__c.sObjectType);
            mocks.when(mockGymActivitySelector.selectAllGymActivitiesRel()).thenReturn(mockActivitiesLst);
        mocks.stopStubbing();

        Application.UnitOfWork.setMock(mockUOW);
        Application.Selector.setMock(mockGymActivitySelector);
        Test.startTest();
            List<Gym_Activity_Relationship__c> resultLst = NewPartnerProductController.showActivities();
            System.assertEquals(resultLst.size() > 0, true);
        Test.stopTest();                                                        
    }


    @IsTest
    private static void getRecordTypeIdsFormPartnerProductFlow_UnitTest(){ 
        Test.startTest();
            NewPartnerProductController.RecordTypesPartnerProduct response = NewPartnerProductController.getRecordTypeIdsFormPartnerProductFlow();
        Test.stopTest();                                                        
    }


    @IsTest
    private static void getOpportunityData_UnitTest(){ 

        fflib_ApexMocks mocks = new fflib_ApexMocks();
        fflib_SObjectUnitOfWork mockUOW = (fflib_SObjectUnitOfWork)mocks.mock(fflib_SObjectUnitOfWork.class);

        OpportunitySelector mockOpportunitySelector = (OpportunitySelector) mocks.mock(OpportunitySelector.class); 
        Id mockOppId = fflib_IDGenerator.generate(Opportunity.SObjectType);
        Id mockAccId = fflib_IDGenerator.generate(Account.SObjectType);
        Account mockAcc = new Account(Id = mockAccId,Wishlist__c = true);

        List<Opportunity> mockOppLst = new List<Opportunity> { 
                                                                new Opportunity (
                                                                id = mockOppId,
                                                                AccountId = mockAcc.Id)
                                                            };
        mocks.startStubbing();
            mocks.when(mockOpportunitySelector.sobjectType()).thenReturn(Opportunity.sObjectType);
            mocks.when(mockOpportunitySelector.selectOpportunityForPartnerProductFlow(new Set<Id>{mockOppId})).thenReturn(mockOppLst);
        mocks.stopStubbing();

        Application.UnitOfWork.setMock(mockUOW);
        Application.Selector.setMock(mockOpportunitySelector);
        Test.startTest();
            NewPartnerProductController.OpportunityDataResponse response = NewPartnerProductController.getOpportunityData(mockOppId);
            System.assertEquals(response.AccountId, mockAccId);
        Test.stopTest();                                                        
    }

    @IsTest
    private static void saveProduct_UnitTest(){ 

        fflib_ApexMocks mocks = new fflib_ApexMocks();
        fflib_SObjectUnitOfWork mockUOW = (fflib_SObjectUnitOfWork)mocks.mock(fflib_SObjectUnitOfWork.class);
        AccountOpportunitySelector mockAccountOpportunitySelector = (AccountOpportunitySelector) mocks.mock(AccountOpportunitySelector.class); 
        Id mockOppId = fflib_IDGenerator.generate(Opportunity.SObjectType);
        Id mockAccId = fflib_IDGenerator.generate(Account.SObjectType);        
        Id mockOppMemberId = fflib_IDGenerator.generate(Account_Opportunity_Relationship__c.SObjectType);
        String recordTypeId = Schema.SObjectType.Product_Item__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product').getRecordTypeId();

        String ccRecordTypeId = Schema.SObjectType.Commercial_Condition__c.getRecordTypeInfosByDeveloperName().get('CAP').getRecordTypeId();
        String partnerProduct = '{"currentAccountId":"'+mockAccId+'","currentOpportunityId":"00605000006YyngAAC",'+
        '"productDetails":{"apiName":"Product_Item__c","fields":{"Name":"Test","Type__c":"In person","Package_Type__c":'+
        '"Value per class","Max_Monthly_Visit_Per_User__c":"10","CurrencyIsoCode":"USD","Price_Visits_Month_Package_Selected__c":'+
        '"10","PR_Activity__c":"Yes","RecordTypeId":"'+recordTypeId+'"}},"selectedGymActivities":'+
        '["a4I050000000llgEAA","a4I050000000llhEAA"],"commercialDetails":[{"key":"CAP","value":{"apiName":'+
        '"Commercial_Condition__c","fields":{"Visits_to_CAP__c":"1","CAP_Discount__c":"1","RecordTypeId":"'+ccRecordTypeId+'"}}},'+
        '{"key":"Exclusivity","value":{"apiName":"Commercial_Condition__c","fields":{"Exclusivity_Fee__c":"Full Exclusivity",'+
        '"Restriction__c":"No","Start_Date__c":"2022-01-20","End_Date__c":"2022-01-20","RecordTypeId":"012050000000LKUAA2"}}},'+
        '{"key":"No_Show","value":{"apiName":"Commercial_Condition__c","fields":{"Fee_Percentage__c":"10","RecordTypeId":'+
        '"012050000000LKKAA2"}}},{"key":"Late_Cancellation","value":{"apiName":"Commercial_Condition__c","fields":'+
        '{"Fee_Percentage__c":"30","RecordTypeId":"012050000000LKPAA2"}}}],"thresholds":[{"apiName":"Threshold__c","fields":'+
        '{"Threshold_value_start__c":"1","Discount__c":"2","RecordTypeId":"012050000000PAnAAM","Volume_discount_date__c":'+
        '"2022-01-20"}},{"apiName":"Threshold__c","fields":{"Threshold_value_start__c":"2","Discount__c":"3","RecordTypeId":'+
        '"012050000000PAnAAM","Volume_discount_date__c":"2022-01-20"}},{"apiName":"Threshold__c","fields":{"Threshold_value_start__c":'+
        '"3","Discount__c":"4","RecordTypeId":"012050000000PAnAAM","Volume_discount_date__c":"2022-01-20"}}]}';

        Account_Opportunity_Relationship__c mockOppMember = new Account_Opportunity_Relationship__c( Id = mockOppMemberId, Opportunity__c = mockOppId, Account__c = mockAccId);
        Map<Id, Account_Opportunity_Relationship__c> mockResponse = new Map<Id, Account_Opportunity_Relationship__c>{mockOppMember.Id => mockOppMember};

        mocks.startStubbing();

            mocks.when(mockAccountOpportunitySelector.sobjectType()).thenReturn(Account_Opportunity_Relationship__c.sObjectType);
            mocks.when(mockAccountOpportunitySelector.selectExistingOpportunityMemberByAccountId(new Set<Id>{mockAccId})).thenReturn(mockResponse);   

        mocks.stopStubbing();

        Application.UnitOfWork.setMock(mockUOW);
        Application.Selector.setMock(mockAccountOpportunitySelector);
        Test.startTest();
            String prodId = NewPartnerProductController.saveProduct(partnerProduct );
            System.assert( prodId == null );
        Test.stopTest();                                                        
    }

    @IsTest
    private static void saveProduct_Error_UnitTest(){ 

        fflib_ApexMocks mocks = new fflib_ApexMocks();
        fflib_SObjectUnitOfWork mockUOW = (fflib_SObjectUnitOfWork)mocks.mock(fflib_SObjectUnitOfWork.class);
        AccountOpportunitySelector mockAccountOpportunitySelector = (AccountOpportunitySelector) mocks.mock(AccountOpportunitySelector.class); 
        Id mockOppId = fflib_IDGenerator.generate(Opportunity.SObjectType);
        Id mockAccId = fflib_IDGenerator.generate(Account.SObjectType);        
        Id mockOppMemberId = fflib_IDGenerator.generate(Account_Opportunity_Relationship__c.SObjectType);

        String ccCapRecordTypeId = Schema.SObjectType.Commercial_Condition__c.getRecordTypeInfosByDeveloperName().get('CAP').getRecordTypeId();
        String recordTypeId = Schema.SObjectType.Product_Item__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product').getRecordTypeId();
        String partnerProduct = '{"currentAccountId":"'+mockAccId+'","currentOpportunityId":"00605000006YyngAAC",'+
        '"productDetails":{"apiName":"Product_Item__c","fields":{"Name":"Test","Type__c":"In person","Package_Type__c":'+
        '"Value per class","Max_Monthly_Visit_Per_User__c":"10","CurrencyIsoCode":"USD","Price_Visits_Month_Package_Selected__c":'+
        '"10","PR_Activity__c":"Yes","RecordTypeId":"'+recordTypeId+'"}},"selectedGymActivities":'+
        '["a4I050000000llgEAA","a4I050000000llhEAA"],"commercialDetails":[{"key":"CAP","value":{"apiName":'+
        '"Commercial_Condition__c","fields":{"Visits_to_CAP__c":"1","CAP_Discount__c":"1","RecordTypeId":"'+ccCapRecordTypeId+'"}}},'+
        '{"key":"Exclusivity","value":{"apiName":"Commercial_Condition__c","fields":{"Exclusivity_Fee__c":"Full Exclusivity",'+
        '"Restriction__c":"No","Start_Date__c":"2022-01-20","End_Date__c":"2022-01-20","RecordTypeId":"012050000000LKUAA2"}}},'+
        '{"key":"No_Show","value":{"apiName":"Commercial_Condition__c","fields":{"Fee_Percentage__c":"10","RecordTypeId":'+
        '"012050000000LKKAA2"}}},{"key":"Late_Cancellation","value":{"apiName":"Commercial_Condition__c","fields":'+
        '{"Fee_Percentage__c":"30","RecordTypeId":"012050000000LKPAA2"}}}],"thresholds":[{"apiName":"Threshold__c","fields":'+
        '{"Threshold_value_start__c":"1","Discount__c":"2","RecordTypeId":"012050000000PAnAAM","Volume_discount_date__c":'+
        '"2022-01-20"}},{"apiName":"Threshold__c","fields":{"Threshold_value_start__c":"2","Discount__c":"3","RecordTypeId":'+
        '"012050000000PAnAAM","Volume_discount_date__c":"2022-01-20"}},{"apiName":"Threshold__c","fields":{"Threshold_value_start__c":'+
        '"3","Discount__c":"4","RecordTypeId":"012050000000PAnAAM","Volume_discount_date__c":"2022-01-20"}}]}';

        Account_Opportunity_Relationship__c mockOppMember = new Account_Opportunity_Relationship__c( Id = mockOppMemberId, Opportunity__c = mockOppId, Account__c = mockAccId);
        Map<Id, Account_Opportunity_Relationship__c> mockResponse = new Map<Id, Account_Opportunity_Relationship__c>{mockOppMember.Id => mockOppMember};

        mocks.startStubbing();

            mocks.when(mockAccountOpportunitySelector.sobjectType()).thenReturn(Account_Opportunity_Relationship__c.sObjectType);
            mocks.when(mockAccountOpportunitySelector.selectExistingOpportunityMemberByAccountId(new Set<Id>{mockAccId})).thenReturn(mockResponse);   
            ((fflib_SObjectUnitOfWork)mocks.doThrowWhen(new ProductItemPartnerServiceImpl.ProductItemPartnerServiceImplException(), mockUOW)).commitWork(); //for void returns  
  
                     
        mocks.stopStubbing();

        Application.UnitOfWork.setMock(mockUOW);
        Application.Selector.setMock(mockAccountOpportunitySelector);

        try {
            Test.startTest();
                String prodId = NewPartnerProductController.saveProduct(partnerProduct );
            Test.stopTest();
        } catch (Exception e){
            System.assert(e instanceof NewPartnerProductController.NewPartnerProductControllerException);
        }                                                     
    }

    @IsTest
    private static void getProductData_Test() {
        Id productId = [SELECT Id FROM Product_Item__c LIMIT 1].Id;
        NewPartnerProductController.PartnerProductFlow partnerProductFlow = NewPartnerProductController.getProductData(productId);
        system.assertEquals(0, partnerProductFlow.thresholds.size());
        system.assertEquals(0, partnerProductFlow.selectedGymActivities.size());
        system.assertNotEquals(null, partnerProductFlow.currentProductId);
        system.assertNotEquals(null, partnerProductFlow.currentOpportunityId);
        system.assertNotEquals(null, partnerProductFlow.currentAccountId);
        system.assertNotEquals(null, partnerProductFlow.productDetails);
        system.assertEquals(1, partnerProductFlow.commercialDetails.size());
    }

    @IsTest
    private static void saveProduct_Test() {

        Id accountId = [SELECT Id FROM Account LIMIT 1].Id;
        Id opportunityId = [SELECT Id FROM Opportunity LIMIT 1].Id;
        Id productId = [SELECT Id FROM Product_Item__c LIMIT 1].Id;
        Id commercialCondId = [SELECT Id FROM Commercial_Condition__c LIMIT 1].Id;
        Id gymActivityRelId = [SELECT Id FROM Gym_Activity_Relationship__c LIMIT 1].Id;
        String recordTypeId = Schema.SObjectType.Product_Item__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product').getRecordTypeId();
        String ccCapRecordTypeId = Schema.SObjectType.Commercial_Condition__c.getRecordTypeInfosByDeveloperName().get('CAP').getRecordTypeId();
        String ccExclRecordTypeId = Schema.SObjectType.Commercial_Condition__c.getRecordTypeInfosByDeveloperName().get('Exclusivity').getRecordTypeId();
        String ccNSRecordTypeId = Schema.SObjectType.Commercial_Condition__c.getRecordTypeInfosByDeveloperName().get('No_Show').getRecordTypeId();
        String ccLCRecordTypeId = Schema.SObjectType.Commercial_Condition__c.getRecordTypeInfosByDeveloperName().get('Late_Cancellation').getRecordTypeId();
        String thresholdRecordTypeId = Schema.SObjectType.Threshold__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Treshold').getRecordTypeId();
        String partnerProduct = '{"currentAccountId":"'+accountId+'","currentOpportunityId":"'+opportunityId+'","currentProductId":"'+productId+'",'+
                '"productDetails":{"apiName":"Product_Item__c","fields":{"Name":"Test Edit","Type__c":"In person","Package_Type__c":'+
                '"Value per class","Max_Monthly_Visit_Per_User__c":"10","CurrencyIsoCode":"USD","Price_Visits_Month_Package_Selected__c":'+
                '"10","PR_Activity__c":"Yes","RecordTypeId":"'+recordTypeId+'"}},"selectedGymActivities":'+
                '["'+gymActivityRelId+'"],"commercialDetails":[{"key":"CAP","value":{"apiName":'+
                '"Commercial_Condition__c","fields":{"Id":"'+commercialCondId+'","Visits_to_CAP__c":"1","CAP_Discount__c":"1","RecordTypeId":"'+ccCapRecordTypeId+'"}}},'+
                '{"key":"Exclusivity","value":{"apiName":"Commercial_Condition__c","fields":{"Exclusivity_Fee__c":"Full Exclusivity",'+
                '"Restriction__c":"No","Start_Date__c":"2022-01-20","End_Date__c":"2022-01-20","RecordTypeId":"'+ccExclRecordTypeId+'"}}},'+
                '{"key":"No_Show","value":{"apiName":"Commercial_Condition__c","fields":{"Fee_Percentage__c":"10","RecordTypeId":'+
                '"'+ccNSRecordTypeId+'"}}},{"key":"Late_Cancellation","value":{"apiName":"Commercial_Condition__c","fields":'+
                '{"Fee_Percentage__c":"30","RecordTypeId":"'+ccLCRecordTypeId+'"}}}],"thresholds":[{"apiName":"Threshold__c","fields":'+
                '{"Threshold_value_start__c":"1","Discount__c":"2","RecordTypeId":"'+thresholdRecordTypeId+'","Volume_discount_date__c":'+
                '"2022-01-20"}},{"apiName":"Threshold__c","fields":{"Threshold_value_start__c":"2","Discount__c":"3","RecordTypeId":'+
                '"'+thresholdRecordTypeId+'","Volume_discount_date__c":"2022-01-20"}},{"apiName":"Threshold__c","fields":{"Threshold_value_start__c":'+
                '"3","Discount__c":"4","RecordTypeId":"'+thresholdRecordTypeId+'","Volume_discount_date__c":"2022-01-20"}}]}';

        NewPartnerProductController.saveProduct(partnerProduct);
        Product_Item__c updatedProduct = [SELECT Id, Name FROM Product_Item__c WHERE Id = :productId];
        System.assertEquals('Test Edit', updatedProduct.Name);
        ProductAssignmentSelector productAssignmentSelector = new ProductAssignmentSelector();
        Map<Id, Product_Assignment__c> productAssignmentsByIds = productAssignmentSelector.selectCommercialConditionByProductId(new Set<Id>{productId});
        system.assertEquals(4, productAssignmentsByIds.size());
        ProductThresholdMemberSelector productThresholdMemberSelector = new ProductThresholdMemberSelector();
        Map<Id, Product_Threshold_Member__c> productThresholdMembersByIds = productThresholdMemberSelector.selectThresholdMapByProductId(new Set<Id>{productId});
        system.assertEquals(3, productThresholdMembersByIds.size());
        ProductActivityRelationshipSelector productActivityRelationshipSelector = new ProductActivityRelationshipSelector();
        List<Product_Activity_Relationship__c> productActivityRelationships = productActivityRelationshipSelector.selectActivityByProductId(new Set<Id>{productId});
        system.assertEquals(1, productActivityRelationships.size());
    }

    @IsTest
    static void createLog_Test() {
        Test.startTest();
        
        NewPartnerProductController.createLog('test message', 'test stack trace', 'testmethod', null, null);
        Test.stopTest();
        List<DebugLog__c> logs = [SELECT Id FROM DebugLog__c];
        System.assertEquals(1, logs.size());
    }

    @IsTest
    static void getRegexCustomMetadata_Test() {
        Test.startTest();
        List<NewPartnerProductController.RegexConfig> regexConfigs = NewPartnerProductController.getRegexCustomMetadata();
        Test.stopTest();
        System.assert(!regexConfigs.isEmpty());
    }
}