/**
 * @description       : 
 * @author            : ext.gft.pedro.oliveira@gympass.com
 * @group             : 
 * @last modified on  : 06-15-2022
 * @last modified by  : gft.jorge.stevaux@ext.gympass.com
**/
public with sharing class OpsSetupFormPartnerServiceImpl implements IOpsSetupFormService{

    PS_Constants constants = PS_Constants.getInstance();
    public string oppSetupRT = constants.OPS_SETUP_FORM_RT_PARTNER;
    public Id oppSetupRTId =  Schema.SObjectType.Ops_Setup_Validation_Form__c.getRecordTypeInfosByDeveloperName().get(constants.OPS_SETUP_FORM_RT_PARTNER).getRecordTypeId();

    public void createOpsFormForAccounts(Id aOpportunityId, Set<Id> accountsId){   
        fflib_ISObjectUnitOfWork uow = Application.UnitOfWork.newInstance(); 
        Map<Id,Ops_Setup_Validation_Form__c> lMapAccIdAndOpsSetup = new Map<Id,Ops_Setup_Validation_Form__c>();
        Set<Id> lSetAccId = new Set<Id>();
        Set<Id> legalContactIds = new Set<Id>();
        Map<Id, Id> paymentIds = new Map<Id,Id>();
       
        /** SELECTORS  */ 
        OpsSetupFormSelector opsSetupSelector = (OpsSetupFormSelector)Application.Selector.newInstance(Ops_Setup_Validation_Form__c.SObjectType);
        AccountSelector accountSelector = (AccountSelector)Application.Selector.newInstance(Account.sobjectType); 
        ProductItemSelector prodItemSelector = (ProductItemSelector)Application.Selector.newInstance(Product_Item__c.sobjectType); 
        OpportunitySelector oppSelector = (OpportunitySelector)Application.Selector.newInstance(Opportunity.sobjectType); 
        ContactSelector cttSelector = (ContactSelector)Application.Selector.newInstance(Contact.sobjectType);
        AccountBankAccountSelector accBankAccSelector = (AccountBankAccountSelector)Application.Selector.newInstance(Acount_Bank_Account_Relationship__c.sobjectType);

        lMapAccIdAndOpsSetup = opsSetupSelector.selectOpsSetupFormForPartner(aOpportunityId,accountsId);   
       
        //if (lMapAccIdAndOpsSetup.isempty()){return;}

        for(id accId : accountsId){
            if(!lMapAccIdAndOpsSetup.containsKey(accId)){
                lSetAccId.add(accId);
            }
        }

        System.debug(':: lSetAccId ' + lSetAccId);
     
        
        List<Account> accLst = accountSelector.selectFieldsToSetUp(lSetAccId);
        Opportunity oppForSetup = oppSelector.selectOpportunityForPartnerOpsSetupForm(aOpportunityId);
        List<Opportunity> opportunityLstToUpdate = new List<Opportunity>();
        List<Acount_Bank_Account_Relationship__c> bkLst = accBankAccSelector.selectBankAccToSetUp(lSetAccId);
        List<Product_Item__c> prdLst = prodItemSelector.selectProductForSetUp(aOpportunityId);
        List<Wrapper> wrapperLst = new List<Wrapper>();
       

        if(oppForSetup != NULL){
            if(oppForSetup.Legal_representative__c != null)  {
                legalContactIds.add(oppForSetup.Legal_representative__c);
            }                       
            if(oppForSetup.Standard_Payment__c == 'No'){
                paymentIds.put(oppForSetup.id, oppForSetup.Payment__c);
            }
        }
        List<Contact> contactLst = cttSelector.selectContactsForSetUp(legalContactIds);


        for(Id accId : lSetAccId){

            Wrapper wrapper = new Wrapper();    
            wrapper.ownerId = oppForSetup.OwnerId;
            wrapper.oppId = oppForSetup.Id;
            wrapper.nameOpp = oppForSetup.Name;
            wrapper.account = accId; 
            wrapper.payment = paymentIds.get(oppForSetup.id);                   
            for(Account acc : accLst){
                if(acc.Id == oppForSetup.AccountId){
                    wrapper.countryAcc = acc.ShippingCountry; 
                    //wrapper.account = acc.Id;  
                }                       
            } 
            for(Contact c : contactLst){
                if(c.AccountId == wrapper.account){
                    wrapper.legalContact = c.id;
                }
            }
            for(Acount_Bank_Account_Relationship__c bankAcc : bkLst){
                if(bankAcc.Account__c == wrapper.account)
                    wrapper.bankAccount = bankAcc.Bank_Account__c; 
            }                  
            for(Product_Item__c prd : prdLst){
                if(prd.Opportunity__c == oppForSetup.Id){
                   wrapper.prods.add(prd.id);
                }                   
            }
            wrapperLst.add(wrapper); 
            opportunityLstToUpdate.add(new Opportunity(Id = wrapper.oppId, Setup_Status__c = constants.OPS_SETUP_IN_PROGRESS ));
        }
         
            createOpsForm(wrapperLst, uow);
            uow.registerDirty(opportunityLstToUpdate);
            uow.commitWork();
        }

        private void createOpsForm(List<Wrapper> wrapperLst, fflib_ISObjectUnitOfWork uow) {
            List<Ops_Setup_Validation_Form__c> formLst = new List<Ops_Setup_Validation_Form__c>();
            Integer maxSize = 69;
            for(Wrapper wrapper : wrapperLst) {
                Ops_Setup_Validation_Form__c opsForm = new Ops_Setup_Validation_Form__c(
                    recordTypeId = oppSetupRTId,
                    OwnerId = wrapper.ownerId,
                    Name = (wrapper.nameOpp + ' - OPS Form').Length() <= 80 ? wrapper.nameOpp + ' - OPS Form' : (wrapper.nameOpp).substring(0, maxSize) + ' - OPS Form',
                    Status__c = 'Ops Set-up',
                    Country__c= wrapper.countryAcc,
                    Opportunity__c = wrapper.oppId,
                    Account_Information__c =wrapper.account,  
                    Contact_Information__c = wrapper.legalContact,            
                   //Product_1_Information__c = wrapper.prods.get(0),             
                    Bank_Account_Information__c= wrapper.bankAccount,             
                    Payment_Information__c = wrapper.payment
                );
                // if(wrapper.prods.size()>1) {
                //     opsForm.Product_2_Information__c = wrapper.prods.get(1);   
                // if(wrapper.prods.size()>2) {
                //     opsForm.Product_3_Information__c = wrapper.prods.get(2); 
                //     }     
                // }

                System.debug(':: opsForm ' + opsForm);
                formLst.add(opsForm);

            }
            uow.registerNew(formLst);
    }

   


    public class Wrapper {
        public Id oppId {get;set;}
        public Id legalContact {get;set;}
        public String countryAcc {get; set;}
        public Id account {get; set;}
        public Id payment {get; set;}
        public List<Id> prods = new List<Id>();
        public Id bankAccount {get; set;}
        public Id ownerId {get; set;}
        public String nameOpp {get; set;}
    }
}