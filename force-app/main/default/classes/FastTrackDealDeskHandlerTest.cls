/**
 * @description       : 
 * @author            : tania.fartaria@gympass.com
 * @group             : 
 * @last modified on  : 15-02-2022
 * @last modified by  : tania.fartaria@gympass.com
 * Modifications Log 
 * Ver   Date         Author                              Modification
 * 1.0   09-09-2021   Rafael Reis - GFT (roei@gft.com)    Initial Version
**/
@isTest(SeeAllData=false)
private with sharing class FastTrackDealDeskHandlerTest {

    
    @isTest 
    private static void shouldNotRequestDealDeskApproval() {
        FastTrackProposalCreationTO lProposalTo = createMockData();

        Test.startTest();
        lProposalTo.proposal.proServicesOneFee = null;
        lProposalTo.proposal.proServicesMainFee = null;

        FastTrackDealDeskHandler lDealDeskHandler = FastTrackDealDeskHandler.getInstanceByOppId( lProposalTo.opportunityId , lProposalTo );
        lDealDeskHandler.dealDeskEvaluation();

        Test.stopTest();

        System.assertEquals( false , lProposalTo.proposal.dealDeskApprovalNeeded , 'Deal desk needs to approve' );
    }

    @isTest 
    private static void shouldRequestDealDeskApproval() {

        FastTrackProposalCreationTO lProposalTo = createMockData();
	
        lProposalTo.proposal.accessFee.payments[0].eligibilities[0].sObj.Payment_Method__c = 'Credit Card';
        lProposalTo.achievedEnablersQuantity = 1;
        lProposalTo.autonomousMarketPlace = 'No';
        lProposalTo.orderExpirationDays = FastTrackDealDeskHandler.ORDER_EXPIRATION_DATE + 1;
        lProposalTo.freeTrialDays = FastTrackDealDeskHandler.FREE_TRIAL + 1;
        lProposalTo.proposal.accessFee.payments[0].eligibilities[0].sObj.Payment_Method__c = 'Credit Card';
       
        Test.startTest();
        lProposalTo.proposal.proServicesMainFee.payments[0].sObj.Frequency__c = 'Monthly';
        lProposalTo.proposal.accessFee.payments[0].sObj.Frequency__c = 'Monthly';
        lProposalTo.proposal.proServicesOneFee.payments[0].sObj.Frequency__c = 'Monthly';
        QuoteLineItem lQuoteAccessFee = [SELECT Id FROM QuoteLineItem LIMIT 1];
        lProposalTo.proposal.accessFee.sObj.Id = lQuoteAccessFee.id;
        lProposalTo.proposal.accessFee.sObj.Fee_Contract_Type__c = 'Copay 2';
        lProposalTo.proposal.proServicesOneFee.sObj.Id = lQuoteAccessFee.id;
        lProposalTo.proposal.proServicesMainFee.sObj.Id = lQuoteAccessFee.id;


        FastTrackDealDeskHandler lDealDeskHandler = FastTrackDealDeskHandler.getInstanceByOppId( lProposalTo.opportunityId , lProposalTo );
        lDealDeskHandler.dealDeskEvaluation();

        Test.stopTest();

        System.assertEquals( true , lProposalTo.proposal.dealDeskApprovalNeeded , 'Deal desk doesnt need to approve' );
    }

    @isTest 
    private static void shouldApproveDealDeskCase() {
        FastTrackProposalCreationTO lProposalTo = createMockData();

        Account lAcc = [ SELECT Id FROM Account LIMIT 1 ];

        Pricebook2 lStandardPb = DataFactory.newPricebook();

        Opportunity lOpp = DataFactory.newOpportunity( lAcc.Id , lStandardPb , 'Client_Success_Renegotiation' );
        lOpp.Quantity_Offer_Number_of_Employees__c = 500;
        Database.insert( lOpp );
        Datetime yesterday = Datetime.now().addDays(-10);
        Test.setCreatedDate(lOpp.Id, yesterday);

        Test.startTest();

        Quote lProposal = DataFactory.newQuote( lOpp , 'Quote Test', 'Client_Sales_New_Business' );
        lProposal.ExpirationDate = System.today();
        Database.insert( lProposal );

        Product2 lAcessFee = DataFactory.newProduct( 'Enterprise Subscription' , false , 'BRL' );
        Database.insert( lAcessFee );

        PricebookEntry lAccessFeeEntry = DataFactory.newPricebookEntry( lStandardPb , lAcessFee , lOpp );
        Database.insert( lAccessFeeEntry );

        QuoteLineItem lQuoteAccessFee = DataFactory.newQuoteLineItem( lProposal , lAccessFeeEntry );
        lQuoteAccessFee.Fee_Contract_Type__c = 'Flat Fee';
        Database.insert( lQuoteAccessFee );

        Payment__c lBillingSetting = DataFactory.newPayment( lQuoteAccessFee );
        lBillingSetting.Frequency__c = 'Monthly';
        Database.insert( lBillingSetting );

        Eligibility__c lEligibility = DataFactory.newEligibility( lBillingSetting );
        Database.insert( lEligibility );
        
        List<Assert_Data__c> assertData = new List<Assert_Data__c>();
        Assert_Data__c assert = new Assert_Data__c(
                                Name ='quote.autonomous_marketplace_contract__c	',
                                Object_Name__c = 'opportunity',
                                Field_Name__c = 'achieved_steps_towards_success_quantity__c',
                                Old_Value__c = '4'
        );
        assertData.add(assert);

        List<Assert_Data__c> assertData2 = new List<Assert_Data__c>();
        Assert_Data__c assert2 = new Assert_Data__c(
                                Name ='quote.autonomous_marketplace_contract__c	',
                                Object_Name__c = 'opportunity',
                                Field_Name__c = 'achieved_steps_towards_success_quantity__c',
                                Old_Value__c = '4',
                                Fee_Type__c = 'Enterprise Subscription'
        );
        assertData2.add(assert2);

        List<Assert_Data__c> assertData3 = new List<Assert_Data__c>();
        Assert_Data__c assert3 = new Assert_Data__c(
                                Name ='quote.autonomous_marketplace_contract__c	',
                                Object_Name__c = 'opportunity',
                                Field_Name__c = 'achieved_steps_towards_success_quantity__c',
                                Old_Value__c = '4',
                                Fee_Type__c = 'Setup Fee'
        );
        assertData3.add(assert3);

        List<Assert_Data__c> assertData4 = new List<Assert_Data__c>();
        Assert_Data__c assert4 = new Assert_Data__c(
                                Name ='quote.autonomous_marketplace_contract__c	',
                                Object_Name__c = 'opportunity',
                                Field_Name__c = 'achieved_steps_towards_success_quantity__c',
                                Old_Value__c = '4',
                                Fee_Type__c = 'Professional Services Setup Fee'
        );
        assertData4.add(assert4);

        List<Assert_Data__c> assertData5 = new List<Assert_Data__c>();
        Assert_Data__c assert5 = new Assert_Data__c(
                                Name ='quote.autonomous_marketplace_contract__c	',
                                Object_Name__c = 'opportunity',
                                Field_Name__c = 'achieved_steps_towards_success_quantity__c',
                                Old_Value__c = '4',
                                Fee_Type__c = 'Professional Services Maintenance Fee'
        );

        assertData5.add(assert5);   

        FastTrackProposalCreationTO.Eligibility lToEligibility = new FastTrackProposalCreationTO.Eligibility();
        lToEligibility.sObj = lEligibility;

        FastTrackProposalCreationTO.Payment lToPayment = new FastTrackProposalCreationTO.Payment();
        lToPayment.sObj = lBillingSetting;
        lToPayment.eligibilities = new List< FastTrackProposalCreationTO.Eligibility >{ lToEligibility };

        lProposalTo.opportunityId = lOpp.Id;
        lProposalTo.oppRecTypeDevName = 'Client_Success_Renegotiation';
        lProposalTo.proposal = new FastTrackProposalCreationTO.Proposal();
        lProposalTo.proposal.sObj = lProposal;
   
        lProposalTo.proposal.accessFee = new FastTrackProposalCreationTO.AccessFee();
        lProposalTo.proposal.accessFee.sObj = lQuoteAccessFee;
        lProposalTo.proposal.accessFee.payments = new List< FastTrackProposalCreationTO.Payment >{ lToPayment };

        lProposalTo.proposal.proServicesOneFee = new FastTrackProposalCreationTO.ProServicesOneFee();
        lProposalTo.proposal.proServicesOneFee.sObj = lQuoteAccessFee;
        lProposalTo.proposal.proServicesOneFee.payments = new List< FastTrackProposalCreationTO.Payment >{ lToPayment };

        lProposalTo.proposal.proServicesMainFee = new FastTrackProposalCreationTO.ProServicesMainFee();
        lProposalTo.proposal.proServicesMainFee.sObj = lQuoteAccessFee;
        lProposalTo.proposal.proServicesMainFee.payments = new List< FastTrackProposalCreationTO.Payment >{ lToPayment };

        Case lDealDeskCase = DataFactory.newCase( 
            new Opportunity( Id = lProposalTo.opportunityId ) , new Quote( Id = lProposalTo.proposal.sobj.Id ) , 'Deal_Desk_Approval' );
        lDealDeskCase.Deal_Desk_Evaluation__c = 'Approved';
        Database.insert( lDealDeskCase );
	
        List<Approval_Item__c> commercialApprovalItems = DataFactory.newApprovalItems(lDealDeskCase);
        Database.insert(commercialApprovalItems);

        List<Approval_Item__c> approvalItemsOpp = [SELECT Id, Name, Value__c, Approval_Status__c, Comments__c, Case__c, Case__r.Status 
                                                   FROM Approval_Item__c 
                                                WHERE Case__c = :lDealDeskCase.Id ];

        lProposalTo.proposal.dealDeskApproved = true;
     
        List<Approval_Item__c> approvalItems = new List<Approval_Item__c>();

        Approval_Item__c itemPS_SetupFee = new Approval_Item__c(
            Name = 'Professional Service Setup Fee price out of standard range',
            Value__c = '0.00',
            Approval_Status__c = 'Rejected',
            Case__c = lDealDeskCase.Id
        );
        approvalItems.add(itemPS_SetupFee);
        Database.insert(approvalItems);


        lOpp = [SELECT Id, CloseDate, RecordType.DeveloperName ,AccountId, Account.Proposal_End_Date__c, Achieved_Steps_Towards_Success_Quantity__c,CreatedDate,
            (SELECT Id, Start_Date__c, Autonomous_marketplace_contract__c, ExpirationDate, Free_Trial_Days__c,Proposal_Start_Date__c FROM Quotes)
            FROM Opportunity WHERE Id = : lOpp.Id];  

        List<QuoteLineItem> lineItems = new List<QuoteLineItem> ([SELECT  Id,Approval_Status__c,Fee_Contract_Type__c,Fee_Type__c,Quote.OpportunityId,UnitPrice,
            (SELECT Id, Frequency__c FROM Payments__r ),
             ( SELECT Id, Payment_Method__c FROM Eligibilities__r ), (SELECT Id, Approval_Status__c, Approval_level__c FROM Waivers__r ) FROM QuoteLineItem
            WHERE QuoteId =: lProposal.Id]);  
            
        Map<String, QuoteLineItem> lineItemByFeeType = new Map<String, QuoteLineItem>();
        for( QuoteLineItem lineItem : lineItems){
                lineItemByFeeType.put(lineItem.Fee_Type__c, lineItem);
        }
        Set<Id> caseId = new Set<Id>(new Map<Id, Case>([SELECT Id FROM Case LIMIT 1]).keySet());

        FastTrackDealDeskHandler lDealDeskHandler = FastTrackDealDeskHandler.getInstanceByOppId( lProposalTo.opportunityId , lProposalTo );
        lDealDeskHandler.updateAssertData( lOpp, assertData , lineItemByFeeType, approvalItemsOpp );
        lDealDeskHandler.updateAssertData( lOpp, assertData2 , lineItemByFeeType, approvalItemsOpp );
        lDealDeskHandler.updateAssertData( lOpp, assertData3 , lineItemByFeeType, approvalItemsOpp );
        lDealDeskHandler.updateAssertData( lOpp, assertData4 , lineItemByFeeType, approvalItemsOpp );
        lDealDeskHandler.updateAssertData( lOpp, assertData5 , lineItemByFeeType, approvalItemsOpp );
        //cover getApprovalItemMappingToReject method.
        FastTrackDealDeskHandler istance = new FastTrackDealDeskHandler();
        istance.getApprovalItemMappingToReject(approvalItems);
        istance.approveOrRejectDealDeskSpecialist(caseId);
    
        Test.stopTest();
    }

    
    @isTest 
    private static void shouldApproveDealDeskSpecialistCase() {
        FastTrackProposalCreationTO lProposalTo = createMockData();

        Test.startTest();

        Case lDealDeskOperationalCase = DataFactory.newCase( 
            new Opportunity( Id = lProposalTo.opportunityId ) , new Quote( Id = lProposalTo.proposal.sobj.Id ) , 'Deal_Desk_Operational' );
        lDealDeskOperationalCase.Deal_Desk_Evaluation__c = 'Approved';
        lDealDeskOperationalCase.OwnerId = [ SELECT Id FROM Group WHERE Name = 'Deal Desk Operational' LIMIT 1 ].Id;
        lDealDeskOperationalCase.Justificate_No_Compliant_Topics__c = 'Justification Test';
        lDealDeskOperationalCase.Subject = 'Deal Desk Operational Approval Request';
        lDealDeskOperationalCase.MF_Eligibility_ES_Payment_Due_Days__c = 'Custom';
        lDealDeskOperationalCase.MF_Eligibility_ES_Custom_Payment_Due_Day__c = 31;
        
        Database.insert( lDealDeskOperationalCase );
        
        FastTrackDealDeskHandler lDealDeskHandler = FastTrackDealDeskHandler.getInstanceByOppId( lProposalTo.opportunityId , lProposalTo );
        lDealDeskHandler.dealDeskEvaluation();

        Test.stopTest();
    } 

    @isTest 
    private static void shouldCreateNewDDCase() {
        FastTrackProposalCreationTO lProposalTo = createMockData();

        lProposalTo.proposal.dealDeskDescription = new List< String >{ 'test description' };

        Test.startTest();

        FastTrackDealDeskHandler lDealDeskHandler = FastTrackDealDeskHandler.getInstanceByOppId( lProposalTo.opportunityId , lProposalTo );
        lDealDeskHandler.submitCaseForApproval();

        Test.stopTest();
    }

    @isTest 
    private static void loadApprovedConditionsDataTest() {
        FastTrackProposalCreationTO lProposalTo = createMockData();

        lProposalTo.proposal.dealDeskDescription = new List< String >{ 'test description' };

      
        Assert_Data__c assert = new Assert_Data__c(
                                Name ='quote.autonomous_marketplace_contract__c	',
                                Object_Name__c = 'opportunity',
                                Field_Name__c = 'achieved_steps_towards_success_quantity__c',
                                Old_Value__c = '4',
                                Fee_Type__c = 'Professional Services Maintenance Fee',
                                Opportunity__c = lProposalTo.opportunityId );

        Database.insert(assert);

        Assert_Data__c assert2 = new Assert_Data__c(
            Name ='quote.autonomous_marketplace_contract__c	',
            Object_Name__c = 'opportunity',
            Field_Name__c = 'achieved_steps_towards_success_quantity__c',
            Old_Value__c = '4',
            Fee_Type__c = 'Enterprise Subscription',
            Opportunity__c = lProposalTo.opportunityId );

        Database.insert(assert2);

        Assert_Data__c assert3 = new Assert_Data__c(
            Name ='quote.autonomous_marketplace_contract__c	',
            Object_Name__c = 'opportunity',
            Field_Name__c = 'achieved_steps_towards_success_quantity__c',
            Old_Value__c = '4',
            Fee_Type__c = 'Setup Fee',
            Opportunity__c = lProposalTo.opportunityId );

        Database.insert(assert3);

        Assert_Data__c assert4 = new Assert_Data__c(
            Name ='quote.autonomous_marketplace_contract__c	',
            Object_Name__c = 'opportunity',
            Field_Name__c = 'achieved_steps_towards_success_quantity__c',
            Old_Value__c = '4',
            Fee_Type__c = 'Professional Services Setup Fee',
            Opportunity__c = lProposalTo.opportunityId );

        Database.insert(assert4);

        Test.startTest();

        FastTrackDealDeskHandler lDealDeskHandler = FastTrackDealDeskHandler.getInstanceByOppId( lProposalTo.opportunityId , lProposalTo );
        lDealDeskHandler.loadApprovedConditionsData();

        Test.stopTest();
    }

    private static FastTrackProposalCreationTO createMockData(){    
        Account lParentAcc = DataFactory.newAccount();
        lParentAcc.NumberOfEmployees = 1000;
        Database.insert( lParentAcc );

        Pricebook2 lStandardPb = DataFactory.newPricebook();

        Opportunity lOpp = DataFactory.newOpportunity( lParentAcc.Id , lStandardPb , 'Client_Sales_New_Business' );
        lOpp.Quantity_Offer_Number_of_Employees__c = 500;
        Database.insert( lOpp );

        Product2 lAcessFee = DataFactory.newProduct( 'Enterprise Subscription' , false , 'BRL' );
        Database.insert( lAcessFee );

        Product2 lProfServicesOneFee = DataFactory.newProduct( 'Professional Services Setup Fee' , false , 'BRL' );
        Database.insert( lProfServicesOneFee );

        Product2 lProfServicesMainFee = DataFactory.newProduct( 'Professional Services Maintenance Fee' , false , 'BRL' );
        Database.insert( lProfServicesMainFee );

        PricebookEntry lAccessFeeEntry = DataFactory.newPricebookEntry( lStandardPb , lAcessFee , lOpp );
        Database.insert( lAccessFeeEntry );

        PricebookEntry lProfServicesOneFeeEntry = DataFactory.newPricebookEntry( lStandardPb , lProfServicesOneFee , lOpp );
        Database.insert( lProfServicesOneFeeEntry );

        PricebookEntry lProfServicesMainFeeEntry = DataFactory.newPricebookEntry( lStandardPb , lProfServicesMainFee , lOpp );
        Database.insert( lProfServicesMainFeeEntry );

        Quote lProposal = DataFactory.newQuote( lOpp , 'Quote Test', 'Client_Sales_New_Business' );
        Database.insert( lProposal );

        lOpp.StageName = 'Lançado/Ganho';
        Database.update( lOpp );

        QuoteLineItem lQuoteAccessFee = DataFactory.newQuoteLineItem( lProposal , lAccessFeeEntry );
        lQuoteAccessFee.Fee_Contract_Type__c = 'Flat Fee';
        Database.insert( lQuoteAccessFee );

        QuoteLineItem lQuotePsOneFee = DataFactory.newQuoteLineItem( lProposal , lProfServicesOneFeeEntry );
        lQuotePsOneFee.Fee_Contract_Type__c = 'Flat Fee';

        QuoteLineItem lQuotePsMainFee = DataFactory.newQuoteLineItem( lProposal , lProfServicesMainFeeEntry );
        lQuotePsMainFee.Fee_Contract_Type__c = 'Flat Fee';

        Payment__c lBillingSetting = DataFactory.newPayment( lQuoteAccessFee );
        lBillingSetting.Frequency__c = 'Yearly';
        Database.insert( lBillingSetting );

        Eligibility__c lEligibility = DataFactory.newEligibility( lBillingSetting );
        Database.insert( lEligibility );

        FastTrackProposalCreationTO.Eligibility lToEligibility = new FastTrackProposalCreationTO.Eligibility();
        lToEligibility.sObj = lEligibility;

        FastTrackProposalCreationTO.Payment lToPayment = new FastTrackProposalCreationTO.Payment();
        lToPayment.sObj = lBillingSetting;
        lToPayment.eligibilities = new List< FastTrackProposalCreationTO.Eligibility >{ lToEligibility };

        FastTrackProposalCreationTO lProposalTo = new FastTrackProposalCreationTO();
        lProposalTo.opportunityId = lOpp.Id;
        lProposalTo.oppRecTypeDevName = 'Client_Sales_New_Business';
        lProposalTo.proposal = new FastTrackProposalCreationTO.Proposal();
        lProposalTo.proposal.sObj = lProposal;
        lProposalTo.quoteId = lProposal.Id;
        lProposalTo.achievedEnablersQuantity = FastTrackDealDeskHandler.ENABLERS_THRESHOLD;
        lProposalTo.autonomousMarketPlace = 'Yes';
        lProposalTo.orderExpirationDays = FastTrackDealDeskHandler.ORDER_EXPIRATION_DATE;
        lProposalTo.freeTrialDays = FastTrackDealDeskHandler.FREE_TRIAL;
        
        lProposalTo.proposal.accessFee = new FastTrackProposalCreationTO.AccessFee();
        lProposalTo.proposal.accessFee.sObj = lQuoteAccessFee;
        lProposalTo.proposal.accessFee.payments = new List< FastTrackProposalCreationTO.Payment >{ lToPayment };

        lProposalTo.proposal.proServicesOneFee = new FastTrackProposalCreationTO.ProServicesOneFee();
        lProposalTo.proposal.proServicesOneFee.sObj = lQuotePsOneFee;
        lProposalTo.proposal.proServicesOneFee.payments = new List< FastTrackProposalCreationTO.Payment >{ lToPayment };

        lProposalTo.proposal.proServicesMainFee = new FastTrackProposalCreationTO.ProServicesMainFee();
        lProposalTo.proposal.proServicesMainFee.sObj = lQuotePsMainFee;
        lProposalTo.proposal.proServicesMainFee.payments = new List< FastTrackProposalCreationTO.Payment >{ lToPayment };

        return lProposalTo;
    }
}