/**
 * Created by gympasser on 30/03/2022.
 */

public with sharing class PaymentSelector extends ApplicationSelector {

    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
                Payment__c.Id,
                Payment__c.Name
        };
    }

    public Schema.SObjectType getSObjectType() {
        return Payment__c.sObjectType;
    }

    public List<Payment__c> selectById(Set<Id> ids) {
        return (List<Payment__c>) super.selectSObjectsById(ids);
    }

    public List<Payment__c> selectAllById(Set<Id> ids) {
        return (List<Payment__c>) Database.query(
                newQueryFactory().
                        selectFields(getAllFieldsFromPayments()).
                        setCondition('Id IN: ids' ).
                        toSOQL());
    }

    private Set<String> getAllFieldsFromPayments(){
        Map<String, Schema.SObjectField> paymentFields = Schema.getGlobalDescribe().get('Payment__c').getDescribe().fields.getMap();
        return paymentFields.keySet();
    }

}