/**
 * @description       : 
 * @author            : gft.jorge.stevaux@ext.gympass.com
 * @group             : 
 * @last modified on  : 04-18-2022
 * @last modified by  : gft.jorge.stevaux@ext.gympass.com
**/
@isTest(seeAllData=false)

private class editPartnerProductControllerTest { 

    @isTest
    static void findRecordType_Test(){
        //1. create fake data that you want to use
        
        Id OppRt = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Opportunity').getRecordTypeId();
        Id mockProductId = fflib_IDGenerator.generate(Product_Item__c.SObjectType);
        Id mockOpportunityId = fflib_IDGenerator.generate(Opportunity.SObjectType);
        Opportunity mockOpp = new Opportunity(Id = mockOpportunityId, RecordTypeId = OppRt);
        Id mockOppMemberId = fflib_IDGenerator.generate(Account_Opportunity_Relationship__c.SObjectType);
        Id mockProdOppAssId = fflib_IDGenerator.generate(Product_Opportunity_Assignment__c.SObjectType);

        Account_Opportunity_Relationship__c mockOppMember = 
        new Account_Opportunity_Relationship__c(Id = mockOppMemberId, Opportunity__c = mockOpp.id);

        List<Product_Opportunity_Assignment__c> mockProdOppAssLst = 
        new List<Product_Opportunity_Assignment__c>{new Product_Opportunity_Assignment__c(
            id=mockProdOppAssId, OpportunityMemberId__c= mockOppMember.Id )};
        
        //2. make fake mock instances you your class
        fflib_ApexMocks mocks = new fflib_ApexMocks();
        ProductOpportunityAssignmentSelector mockProductOpportunityAssignmentSelector = (ProductOpportunityAssignmentSelector)mocks.mock(ProductOpportunityAssignmentSelector.class);

        //3. make fake returns for your classes methods you are calling
        mocks.startStubbing();
        mocks.when(mockProductOpportunityAssignmentSelector.selectByOpportunitybyProductId(new Set<Id>{mockProductId})).thenReturn(mockProdOppAssLst);  
        mocks.stopStubbing();
       
        //4. set the mocks of your application
        Application.Selector.setMock(mockProductOpportunityAssignmentSelector);

        Test.startTest();
        system.debug('mockProductId => '+mockProductId);
        editPartnerProductController.findRecordType(mockProductId);
        Test.stopTest();                           
    }
}