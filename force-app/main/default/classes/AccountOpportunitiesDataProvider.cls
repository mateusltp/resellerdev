/**
 * Created by bruno on 13/01/2022.
 */

global with sharing class AccountOpportunitiesDataProvider implements sortablegrid.sdgIDataProvider
{
    global String className = AccountOpportunitiesDataProvider.class.getName();
    global PS_Constants constants = PS_Constants.getInstance();

    global static Boolean isUserSelectable()
    {
        return true;
    }

    global sortablegrid.SDGResult getData(sortablegrid.SDG coreSDG,  sortablegrid.SDGRequest request)
    {
        sortablegrid.SDGResult result = new sortablegrid.SDGResult();
        System.debug('result >>>'+result);

        result.data = new List<Sobject>();
        System.debug('result data'+result.data);
        try {
            Map<Id, Opportunity> opportunitiesByIds = new Map<Id, Opportunity>();
            // retrieve opps related directly to account
            AccountSelector accountSelector = (AccountSelector)Application.Selector.newInstance(Account.SObjectType);
            Map<Id, Account> accountsByIds = accountSelector.selectWithOpportunitiesById(new Set<Id> {request.ParentRecordID});
            system.debug('### accsByIds: '+JSON.serializePretty(accountsByIds));
            system.debug('request.ParentRecordID' + request.ParentRecordID);
            try {
                Set<Id> oppIdsFromAccOppRels = new Set<Id>();
                if (!accountsByIds.get(request.ParentRecordID).Opportunities.isEmpty())
                {
                    opportunitiesByIds.putAll(accountsByIds.get(request.ParentRecordID).Opportunities);
                }
                for (Account_Opportunity_Relationship__c accOppRel : accountsByIds.get(request.ParentRecordID).Inherited_Opportunity__r)
                {
                    system.debug('Inherited_Opportunity__r' + accountsByIds.get(request.ParentRecordID).Inherited_Opportunity__r);
                    if (!opportunitiesByIds.containsKey(accOppRel.Opportunity__c))
                    {
                        oppIdsFromAccOppRels.add(accOppRel.Opportunity__c);
                    }
                }
                if (!oppIdsFromAccOppRels.isEmpty()) {
                    OpportunitySelector opportunitySelector = (OpportunitySelector)Application.Selector.newInstance(Opportunity.SObjectType);
                    List<Opportunity> opps = opportunitySelector.selectById(oppIdsFromAccOppRels);
                    system.debug('### opps: '+opps);
                    opportunitiesByIds.putAll(opportunitySelector.selectById(oppIdsFromAccOppRels));
                }

            } catch (Exception e) {
                createLog(e.getMessage(), e.getStackTraceString(), 'getData', null, request.ParentRecordID);
            }

            // add to result
            system.debug('## opportunitiesByIds: '+json.serializePretty(opportunitiesByIds));
            result.data.addAll(opportunitiesByIds.values());
            result.FullQueryCount = result.data.size();
            result.pagecount = 1;
            result.isError = false;
            result.ErrorMessage = '';
        }
        catch (Exception e) {
            createLog(e.getMessage(), e.getStackTraceString(), 'getData', null, request.ParentRecordID);
            throw new AccountOpportunitiesDataProviderException(e.getMessage() + ' on line '+ e.getLineNumber());
        }

        return result;
    }

    global sortablegrid.SDG LoadSDG(String SDGTag, String ParentRecordId)
    {
        sortablegrid.SDG CoreSDG = new sortablegrid.SDG( 'AccountOpportunities' );
        CoreSDG.SDGFields = GetFields();
        return CoreSDG;
    }

    private List<sortablegrid.SDGField> GetFields()
    {
        List<sortablegrid.SDGField> fields = new List<sortablegrid.SDGField>();

        fields.add( new sortablegrid.SDGField('1', 'Opportunity Name', 'Name', 'STRING', '', false, false, null, 1));
        fields.add( new sortablegrid.SDGField('2', 'Opportunity Owner', 'Owner_Name__c', 'STRING', '', false, false, null, 2));
        fields.add( new sortablegrid.SDGField('3', 'Stage', 'StageName', 'STRING', '', false, false, null, 3));

        return fields;
    }

    @TestVisible
    private void createLog(String exceptionMsg, String exceptionStackTrace, String methodName, String dataScope, Id objectId) {
        DebugLog__c log = new DebugLog__c(
                Origin__c = '['+className+']['+methodName+']',
                LogType__c = constants.LOGTYPE_ERROR,
                RelatedToSquad__c = constants.LOGTYPE_SQUAD_PARTNERS,
                ExceptionMessage__c = exceptionMsg,
                StackTrace__c = exceptionStackTrace,
                DataScope__c = dataScope,
                ObjectId__c = objectId
        );
        Logger.createLog(log);
    }

    public class AccountOpportunitiesDataProviderException extends Exception {}
}