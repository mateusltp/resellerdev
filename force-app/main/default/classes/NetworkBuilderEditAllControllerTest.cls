/**
* Created by gympasser on 16/03/2022.
*/

@IsTest
private class NetworkBuilderEditAllControllerTest {
    
    @TestSetup
    private static void setup() {
        List<Account> accounts = new List<Account>();
        // Account without a contact (therefore also no Legal Representative)
        Account account = new Account(
            Name = 'Test Account 1',
            GP_Status__c = 'Active',
            BillingCity = 'Lisboa',
            BillingCountry = 'Portugal'
        );
        accounts.add(account);
        
        // Account with a contact that will be defined as its legal representative
        Account account2 = new Account(
            Name = 'Test Account 2',
            GP_Status__c = 'Active',
            BillingCity = 'Lisboa',
            BillingCountry = 'Portugal'
        );
        accounts.add(account2);
        
        // Account with a contact that will not be defined as its legal representative
        Account account3 = new Account(
            Name = 'Test Account 3',
            GP_Status__c = 'Active',
            BillingCity = 'Lisboa',
            BillingCountry = 'Portugal'
        );
        accounts.add(account3);
        insert accounts;
        
        List<Contact> contacts = new List<Contact>();
        
        Contact contact = new Contact(
            FirstName = 'Johnny',
            LastName = ' Does',
            Type_of_contact__c = 'Legal Representative',
            AccountId = account2.Id,
            Email = 'johnny@email.com',
            RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Gyms_Partner').getRecordTypeId()
            
        );
        contacts.add(contact);
        Contact contact2 = new Contact(
            FirstName = 'Jamie',
            LastName = ' Doesty',
            Type_of_contact__c = 'Decision Maker',
            AccountId = account3.Id,
            Email = 'jamie@email.com',
            RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Gyms_Partner').getRecordTypeId()
            
        );
        contacts.add(contact2);
        insert contacts;
    }
    
    @IsTest
    static void testUpdateAccountsWithExistingLegalRepresentative() {
        List<Id> accountIds = new List<Id>();
        for (Account a : [SELECT Id FROM Account]) {
            accountIds.add(a.Id);
        }
        Id contactId = [SELECT Id FROM Contact LIMIT 1].Id;
        Test.startTest();
        NetworkBuilderEditAllController.saveDataExistingContact(accountIds, contactId);
        Test.stopTest();
        for (Account a : [SELECT Id, Legal_Representative__c FROM Account]) {
            System.assertEquals(contactId, a.Legal_Representative__c);
        }
    }
    
    @IsTest
    static void testUpdateAccountsWithNewLegalRepresentative() {
        List<Id> accountIds = new List<Id>();
        for (Account a : [SELECT Id FROM Account]) {
            accountIds.add(a.Id);
        }
        
        String contactSerialized = '{"apiName":"Contact","fields":{"Salutation":"Mr.","FirstName":"my test","LastName":"serialized","Status_do_contato__c":"Ativo","Cargo__c":"Analyst","Title":null,"Type_of_contact__c":"Point of Contact","AdditionalInfo__c":null,"Email":"testseria@mail.com","Phone":"", "AccountId":"'+accountIds[0]+'"}}';
        
        Test.startTest();
        NetworkBuilderEditAllController.saveDataNewContact(accountIds, contactSerialized);
        Test.stopTest();
        List<Contact> newContact = [SELECT Id, AccountId FROM Contact WHERE LastName = 'serialized'];
        System.assertEquals(1, newContact.size());
        List<AccountContactRelation> relations = [SELECT Id FROM AccountContactRelation WHERE ContactId = :newContact[0].Id];
        System.assertEquals(accountIds.size(), relations.size());
    }
    
    @isTest
    static void getAccountRecordTypeId_Test(){
        String rtDevName = 'Gym_Partner';
        Id expectedRt = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(rtDevName).getRecordTypeId();
        Id result = NetworkBuilderEditAllController.getAccountRecordTypeId(rtDevName);
        Test.startTest();
        system.assertEquals(expectedRt, result);
        Test.stopTest();
    }
    
    @isTest
    static void getRecordTypeId_Test(){
        String rtContact = 'Partner_Flow_Contact';
        Id expectRt = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get(rtContact).getRecordTypeId();
        Id result =     NetworkBuilderEditAllController.getRecordTypeId(rtContact);
        
        Test.startTest();
        system.assertEquals(expectRt, result);
        Test.stopTest();
    }
    
    @isTest
    static void search_Test(){
        String searchTerm = 'Jamie';
        List<String> selectedIds = new List<String>();
        for(Account acc : [SELECT Id, Name FROM Account WHERE Name = 'Test Account 1']){
            selectedIds.add(acc.id);
        }
        List<List<SObject>> searchResults = [
            FIND :searchTerm
            IN ALL FIELDS
            RETURNING
            Contact(Id, Name, Account.Name WHERE Id NOT IN :selectedIds)
            LIMIT 1
        ];
        
        Contact[] contacts = (List<Contact>) searchResults[0];
        
        Test.startTest();
        NetworkBuilderEditAllController.search(searchTerm, selectedIds);
        Test.stopTest(); 
    }
    
    @isTest
    static void getRecentlyViewed_Test(){
        Id gymsPartnerRT = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Gyms_Partner').getRecordTypeId();
        List<LookupSearchResult> testResult = NetworkBuilderEditAllController.getRecentlyViewed();
        
        List<LookupSearchResult> results = new List<LookupSearchResult>();
        // Get recently viewed records of type Account or Opportunity
        List<RecentlyViewed> recentRecords = [
            SELECT Id, Name
            FROM RecentlyViewed
            WHERE Type = 'Contact' AND RecordTypeId = :gymsPartnerRT
            ORDER BY LastViewedDate DESC
            LIMIT 5
        ];
        // Convert recent records into LookupSearchResult
        for (RecentlyViewed recentRecord : recentRecords) {
            results.add(
                new LookupSearchResult(
                    recentRecord.Id,
                    'Contact',
                    'standard:contact',
                    recentRecord.Name,
                    'Contact • ' + recentRecord.Name
                )
            );
        }
        
        Test.startTest();
        System.assertEquals(results.size(), testResult.size());
        Test.stopTest(); 
    }
    
    @isTest
    static void updateAccountLst_Test(){
        
        map<string, object> mapToSerialize = new map<string, object>();
        mapToSerialize.put('Name', 'TEST');
        string jsonstring = JSON.serialize(mapToSerialize);  
        
        List<Id> selectedRecords = new List<Id>();
        List<Account> selectedRecordsLst = [SELECT Id, Name FROM Account WHERE Name = 'Test Account 1' LIMIT 1];
        for(Account acc : selectedRecordsLst){
            selectedRecords.add(acc.id);
        }
       
        Test.startTest();
        try{
                    NetworkBuilderEditAllController.updateAccountLst(jsonstring, selectedRecords);

        }catch(Exception e){
            system.debug('exception');
        }
        Test.stopTest(); 
    }
    
    @isTest
    static void updateAccountLstException_Test(){
        string jsonstring = 'Name';
        List<Id> selectedRecords = new List<Id>();
        List<Account> selectedRecordsLst = [SELECT Id, Name FROM Account WHERE Name = 'Tst Exception' LIMIT 1];
        for(Account acc : selectedRecordsLst){
            selectedRecords.add(acc.id);
        }
       
        Test.startTest();
        try{
                    NetworkBuilderEditAllController.updateAccountLst(jsonstring, selectedRecords);

        }catch(Exception e){
            system.debug('exception');
        }
        Test.stopTest(); 
    }

    @IsTest
    static void getAccountsData_Test() {
        Map<Id, Account> accountsByIds = new Map<Id, Account>([SELECT Id FROM Account]);
        Test.startTest();

        List<NetworkBuilderEditAllController.AccountWrapper> wrappers = NetworkBuilderEditAllController.getAccountData(new List<Id>(accountsByIds.keySet()));

        Test.stopTest();
        System.assert(wrappers.size() > 0);
    }

    @IsTest
    static void createLog_Test() {
        Test.startTest();
        NetworkBuilderEditAllController.createLog('test message', 'test stack trace', 'testmethod', null, null);
        Test.stopTest();
        List<DebugLog__c> logs = [SELECT Id FROM DebugLog__c];
        System.assertEquals(1, logs.size());
    }
}