public abstract without sharing class OrderTagusDAO {
  public static List<OrderTagusDTO> getOrderDtoByOrders(List<Order> orders) {
    SearchEngine searchEngine = new SearchEngine();
    searchEngine.setOrders(orders);
    searchEngine.setOrderItems(queryOrderItemsByOrders(orders));
    searchEngine.setPayments(queryPaymentsByOrders(orders));

    return getOrderDTOs(searchEngine);
  }

  private static List<OrderItem> queryOrderItemsByOrders(List<Order> orders) {
    Map<String, Object> orderItemFields = (Map<String, Object>) Schema.SObjectType.OrderItem.fields.getMap();
    Map<String, Object> waiverFields = (Map<String, Object>) Schema.SObjectType.Waiver__c.fields.getMap();

    String soqlOrderItemFields = '';

    for (String orderItemFieldName : orderItemFields.keySet()) {
      soqlOrderItemFields += (orderItemFieldName + ',');
    }

    String soqlQuery = 'SELECT ' + soqlOrderItemFields + '(SELECT ';

    String soqlWaiverFields = '';

    for (String waiverFieldName : waiverFields.keySet()) {
      soqlWaiverFields += (waiverFieldName + ',');
    }

    soqlQuery +=
      soqlWaiverFields.removeEnd(',') +
      ' FROM Waiver__r)' +
      ' FROM OrderItem WHERE OrderId IN :orders';

    return (List<OrderItem>) Database.query(soqlQuery.escapeJava());
  }

  private static List<Payment__c> queryPaymentsByOrders(List<Order> orders) {
    Map<String, Object> paymentFields = (Map<String, Object>) Schema.SObjectType.Payment__c.fields.getMap();
    Map<String, Object> eligibilityFields = (Map<String, Object>) Schema.SObjectType.Eligibility__c.fields.getMap();

    String soqlPaymentFields = '';

    for (String paymentFieldName : paymentFields.keySet()) {
      soqlpaymentFields += (paymentFieldName + ',');
    }

    String soqlQuery =
      'SELECT Account__r.UUID__c,RecordType.DeveloperName,' +
      soqlpaymentFields +
      '(SELECT Paid_By__r.UUID__c,Managed_By__r.UUID__c,';

    String soqlEligibilityFields = '';

    for (String eligibilityFieldName : eligibilityFields.keySet()) {
      soqlEligibilityFields += (eligibilityFieldName + ',');
    }

    soqlQuery +=
      soqlEligibilityFields.removeEnd(',') +
      ' FROM Eligibility__r)' +
      ' FROM Payment__c WHERE Order__c IN :orders';

    return (List<Payment__c>) Database.query(soqlQuery.escapeJava());
  }

  private static List<OrderTagusDTO> getOrderDTOs(SearchEngine searchEngine) {
    List<OrderTagusDTO> orderDTOs = new List<OrderTagusDTO>();

    for (Order order : searchEngine.orders) {
      OrderTagusDTO orderDTO = new OrderTagusDTO(order);

      if (searchEngine.orderItemsByOrderId.containsKey(order.Id)) {
        List<OrderItem> orderItems = new List<OrderItem>();

        for (
          OrderItem orderItem : searchEngine.orderItemsByOrderId.get(order.Id)
        ) {
          if (order.Version__c == orderItem.Order_Version__c) {
            orderItems.add(orderItem);
          }
        }

        orderDTO.setOrderItems(orderItems);
      }

      if (searchEngine.paymentsByOrderId.containsKey(order.Id)) {
        List<Payment__c> payments = new List<Payment__c>();

        for (
          Payment__c payment : searchEngine.paymentsByOrderId.get(order.Id)
        ) {
          if (order.Version__c == payment.Order_Version__c) {
            payments.add(payment);
          }
        }

        orderDTO.setPayments(payments);
      }

      orderDTOs.add(orderDTO);
    }

    return orderDTOs;
  }

  private class SearchEngine {
    private List<Order> orders;
    private Map<Id, List<OrderItem>> orderItemsByOrderId;
    private Map<Id, List<Payment__c>> paymentsByOrderId;

    private SearchEngine() {
      this.orders = new List<Order>();
      this.orderItemsByOrderId = new Map<Id, List<OrderItem>>();
      this.paymentsByOrderId = new Map<Id, List<Payment__c>>();
    }

    private void setOrders(List<Order> orders) {
      this.orders = orders;
    }

    private void setOrderItems(List<OrderItem> orderItrems) {
      this.orderItemsByOrderId = getMapOrderItemsByOrderId(orderItrems);
    }

    private Map<Id, List<OrderItem>> getMapOrderItemsByOrderId(
      List<OrderItem> orderItems
    ) {
      Map<Id, List<OrderItem>> orderItemsByQuoteId = new Map<Id, List<OrderItem>>();

      for (OrderItem orderItem : orderItems) {
        if (orderItemsByQuoteId.containsKey(orderItem.OrderId)) {
          orderItemsByQuoteId.get(orderItem.OrderId).add(orderItem);
        } else {
          orderItemsByQuoteId.put(
            orderItem.OrderId,
            new List<OrderItem>{ orderItem }
          );
        }
      }

      return orderItemsByQuoteId;
    }

    private void setPayments(List<Payment__c> payments) {
      this.paymentsByOrderId = getMapPaymentsByOrderId(payments);
    }

    private Map<Id, List<Payment__c>> getMapPaymentsByOrderId(
      List<Payment__c> payments
    ) {
      Map<Id, List<Payment__c>> paymentsByOrderId = new Map<Id, List<Payment__c>>();

      for (Payment__c payment : payments) {
        if (paymentsByOrderId.containsKey(payment.Order__c)) {
          paymentsByOrderId.get(payment.Order__c).add(payment);
        } else {
          paymentsByOrderId.put(
            payment.Order__c,
            new List<Payment__c>{ payment }
          );
        }
      }

      return paymentsByOrderId;
    }
  }
}