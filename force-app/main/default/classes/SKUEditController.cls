public with sharing class SKUEditController {

    @AuraEnabled
    public static void updateProductAndWaivers(QuoteLineItem product, List<Waiver__c> waiversToUpsert, List<Waiver__c> waiversToDelete){
        try {

            update product;

            delete waiversToDelete;

            upsert waiversToUpsert;  
            
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

}