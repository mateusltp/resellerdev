@isTest
public class ResellerAccountTest {

 
    @isTest
    public static void accountTest() {
        
        User thisUser = [SELECT Id, Contact.AccountId FROM User WHERE Profile.name = 'Reseller Community' and IsActive = true limit 1];
       
        Account account = new Account();  
        account.RecordTypeId = '01241000000xyotAAA';
        account.Name = 'teste account7';
        account.Legal_Document_Type__c = 'CNPJ';
        account.Id_Company__c = '56947401000117';
        account.Razao_Social__c = 'TESTE222';
        account.Website = 'teste222.com';        
        account.BillingCountry = 'Brazil';
        account.Industry = 'Government or Public Management';
        account.ABM_Prospect__c = true;
        account.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Reseller').getRecordTypeId();
        insert account;
        
        Contact contact = new Contact();
        contact.FirstName = 'teste contact7';
        contact.LastName = 'testeNovo';
        contact.Email = 'testecontact@bemarius.example';
        contact.Role__c = 'FINANCE';
        insert contact;
        
        Account_Request__c request = new Account_Request__c();  
        request.Name = 'teste7';
        request.Unique_Identifier_Type__c = 'CNPJ';
        request.Unique_Identifier__c = '56947421000106';
        request.Website__c = 'testerequeste7.com';
        request.Billing_Country__c = 'Brazil';
        request.AccountId__c = account.Id;
        request.ContactId__c = contact.Id;
        request.Engine_Status__c = 'Analyze';
        request.Use_My_Own_Data__c = true;
        request.IsNewAccount__c = true;
        
        request.Partner_Account_Owner__c = thisUser.Id;
        insert request;

        Test.startTest();
         
        ResellerEngineGoNoGO resellerEngine = new ResellerEngineGoNoGO(new Set<Id> {request.Id});
        resellerEngine.runSearchAccounts(true);
        resellerEngine.noGO();
        
        ResellerCreateOpportunityGO createOpportunityGO = new ResellerCreateOpportunityGO();
        createOpportunityGO.getDirectExecutive(account.Id);
        
        request.Engine_Status__c = 'GO';
        request.Website__c = 'www.testeNovo2.com';
        request.Partner_Model__c = 'Subsidy';
        update request;
             
        Test.stopTest(); 
        
       System.assertEquals(request.AccountId__r.OwnerId, request.OpportunityId__r.OwnerId);

    }
    
    @isTest
    public static void accountNoContactTest() {
        
        User thisUser = [SELECT Id, Contact.AccountId FROM User WHERE Profile.name = 'Reseller Community' and IsActive = true limit 1];
       
        Account account = new Account();  
        account.RecordTypeId = '01241000000xyotAAA';
        account.Name = 'teste account7';
        account.Legal_Document_Type__c = 'CNPJ';
        account.Id_Company__c = '56947401000117';
        account.Razao_Social__c = 'TESTE222';
        account.Website = 'teste222.com';        
        account.BillingCountry = 'Brazil';
        account.Industry = 'Government or Public Management';
        account.ABM_Prospect__c = true;
        account.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Reseller').getRecordTypeId();
        insert account;
        
        Account_Request__c request = new Account_Request__c();  
        request.Name = 'teste7';
        request.Unique_Identifier_Type__c = 'CNPJ';
        request.Unique_Identifier__c = '56947421000106';
        request.Website__c = 'testerequeste7.com';
        request.Billing_Country__c = 'Brazil';
        request.AccountId__c = account.Id;
        request.Engine_Status__c = 'Analyze';
        request.Use_My_Own_Data__c = false;
        request.IsNewAccount__c = true;
        request.Bulk_Operation__c = false;
        
        request.Partner_Account_Owner__c = thisUser.Id;
        insert request;

        Test.startTest();
         
        ResellerEngineGoNoGO resellerEngine = new ResellerEngineGoNoGO(new Set<Id> {request.Id});
        resellerEngine.runSearchAccounts(true);
        resellerEngine.noGO();
        
        ResellerCreateOpportunityGO createOpportunityGO = new ResellerCreateOpportunityGO();
        createOpportunityGO.getDirectExecutive(account.Id);
        
        request.Engine_Status__c = 'GO';
        request.Website__c = 'www.testeNovo2.com';
        request.Partner_Model__c = 'Subsidy';
        update request;
             
        Test.stopTest(); 
        
       System.assertEquals(request.AccountId__r.OwnerId, request.OpportunityId__r.OwnerId);

    }
    
}