public class ManagedClauseTriggerHandler extends TriggerHandler{
    public override void beforeInsert() {
        new ManagedClauseTriggerHelper().setLengthOfRichTextField(Trigger.new);                
    }
    public override void beforeUpdate(){ 
       new ManagedClauseTriggerHelper().setLengthOfRichTextField(Trigger.new);                
    }
}