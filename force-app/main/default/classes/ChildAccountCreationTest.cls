/**
 * @File Name          : ChildAccountCreationTest.cls
 * @Description        : 
 * @Author             : GEPI@GFT.com
 * @Group              : 
 * @Last Modified By   : Jorge Stevaux - JZRX@gft.com
 * @Last Modified On   : 11-12-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    24/03/2020   Samuel Silva - GFT (slml@gft.com)     Initial Version
**/
@isTest
public with sharing class ChildAccountCreationTest {
   
    @TestSetup
    static void generateGenericFormData(){      
        Id rtId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gym_Partner').getRecordTypeId();
        Id rtIdNewFlow =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Account').getRecordTypeId();
        List<Account> accToInsert = new List<Account>();

        Account acc = new Account();
        acc.name='AcademiaBrasilCompanyPai';
        acc.RecordTypeId = rtId;
        acc.GP_Status__c = 'Pending Approval';
        acc.ShippingCountry = 'Brazil';
        acc.ShippingState = 'São Paulo';
        acc.CAP_Value__c = 120;
        acc.ShippingCity = 'CityAcademiaBrasil';
        acc.ShippingStreet = 'Rua academiabrasilpai';        
        acc.Gym_Type__c = 'Studios';
        acc.Types_of_ownership__c = Label.franchisePicklist;
        acc.Subscription_Type__c = 'Value per class';
        acc.Subscription_Period__c = 'Monthy value';
        acc.Subscription_Type_Estimated_Price__c    = 100;
        acc.Has_market_cannibalization__c = 'No';
        acc.Exclusivity__c = 'Yes';
        acc.Exclusivity_End_Date__c = Date.today().addYears(1);
        acc.Exclusivity_Partnership__c = 'Full Exclusivity';
        acc.Exclusivity_Restrictions__c= 'No';
        acc.Website = 'testing@tesapex.com';
        acc.Gym_Email__c = 'gymemail@apex.com';
        acc.Phone = '3222123123';
        acc.Setup__c = true;
        accToInsert.add(acc);

        INSERT accToInsert;
            
    }
    
    @isTest static void cloneAccountTest() {
        Id rtId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gym_Partner').getRecordTypeId();

        Account accParent = [SELECT id FROM Account WHERE Name = 'AcademiaBrasilCompanyPai' AND RecordTypeId =: rtId];
        ChildAccountCreationModel.CustomFormChildAccountUnwrapper accwrp = 
        new ChildAccountCreationModel.CustomFormChildAccountUnwrapper(                
            accParent.Id,'MCUMAXXXA', false, false, 'SP', 'BR', 'XAXAWWZ Street','Sorocaba','AS1222', 'TEsting class', true);
        Test.startTest();  
            Map<String,String> result = ChildAccountCreationController.cloneAccount((Object)accwrp);
            Id accountId = (Id)result.get('SUCCESS');
            System.assertEquals(Schema.Account.SObjectType, accountId.getSobjectType());
        Test.stopTest();               
    }

    @isTest static void cloneAccountErrorTest() {
        Account accParent = [SELECT id FROM Account WHERE Name = 'AcademiaBrasilCompanyPai'];
        ChildAccountCreationModel.CustomFormChildAccountUnwrapper accwrp = 
        new ChildAccountCreationModel.CustomFormChildAccountUnwrapper(                
            accParent.Id,'MCUMAXXXA', false, false, 'SP', 'BR', 'XAXAWWZ Street','Sorocaba','AS1222', 'TEsting class', true);
        Test.startTest();  
            Map<String,String> result = ChildAccountCreationController.cloneAccount((Object)accwrp);
            System.assertEquals(result.values().size(),result.values().size());
        Test.stopTest();               
    }
       
}