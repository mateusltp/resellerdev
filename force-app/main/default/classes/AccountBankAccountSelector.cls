/**
 * @description       : 
 * @author            : ext.gft.pedro.oliveira@gympass.com
 * @group             : 
 * @last modified on  : 03-28-2022
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/
public with sharing class AccountBankAccountSelector extends ApplicationSelector {
    
    
    public List<Schema.SObjectField> getSObjectFieldList() {
		return new List<Schema.SObjectField> {
			Acount_Bank_Account_Relationship__c.Id,
			Acount_Bank_Account_Relationship__c.Name
		};
	}

    public Schema.SObjectType getSObjectType() {
		return Acount_Bank_Account_Relationship__c.sObjectType;
	}

    public List<Acount_Bank_Account_Relationship__c> selectById(Set<Id> ids) {
		return (List<Acount_Bank_Account_Relationship__c>) super.selectSObjectsById(ids);
	}

	public List<Acount_Bank_Account_Relationship__c> selectBankAccToSetUp(Set<Id> ids) {
		return new List<Acount_Bank_Account_Relationship__c>( (List<Acount_Bank_Account_Relationship__c>)Database.query(
										newQueryFactory().
										selectField(Acount_Bank_Account_Relationship__c.Id).
										selectField(Acount_Bank_Account_Relationship__c.Account__c).
										selectField(Acount_Bank_Account_Relationship__c.Bank_Account__c).
										setCondition('Account__c IN: ids').
										toSOQL()
										));									
	}

	public Map<Id, Acount_Bank_Account_Relationship__c> selectBankAccountByAccountId(Set<Id> accountIds) {
		Map<Id, Acount_Bank_Account_Relationship__c> response = new Map<Id, Acount_Bank_Account_Relationship__c>();
		for(Acount_Bank_Account_Relationship__c iBankAccRel : (List<Acount_Bank_Account_Relationship__c>)Database.query(
																newQueryFactory().
																selectField(Acount_Bank_Account_Relationship__c.Account__c).
																selectField(Acount_Bank_Account_Relationship__c.Bank_Account__c).
																selectField('Bank_Account__c.Agency__c').
																selectField('Bank_Account__c.Bank_Account_Number_or_IBAN__c').
																selectField('Bank_Account__c.Bank_Account_Ownership__c').
																selectField('Bank_Account__c.Bank_Name__c').
																selectField('Bank_Account__c.Bank_Number__c').
																selectField('Bank_Account__c.Bank_Statement__c').
																selectField('Bank_Account__c.BIC_or_Routing_Number_or_Sort_Code__c').
																selectField('Bank_Account__c.MX_Key__c').
																selectField('Bank_Account__c.Registration_Number__c').
																selectField('Bank_Account__c.Routing_Number__c').
																selectField('Bank_Account__c.Sort_Code__c').
																selectField('Bank_Account__c.VAT_Number_or_UTR_number__c').
																setCondition('Account__c IN: accountIds').
																toSOQL()
															   )){

			response.put(iBankAccRel.Account__c, iBankAccRel);
		}								
		return response;
	}

	public List<Acount_Bank_Account_Relationship__c> selectAccountBankAccountRelationshipsByAccountIds(Set<Id> accountIds) {
		return (List<Acount_Bank_Account_Relationship__c>) Database.query(
				newQueryFactory().
						selectFields(getAllFieldsFromAccountBankRelations()).
						setCondition('Account__c IN: accountIds' ).
						toSOQL());
	}

	private Set<String> getAllFieldsFromAccountBankRelations(){
		Map<String, Schema.SObjectField> accountBankRelFields = Schema.getGlobalDescribe().get('Acount_Bank_Account_Relationship__c').getDescribe().fields.getMap();
		return accountBankRelFields.keySet();
	}

}