/**
 * @description       : 
 * @author            : AlyssonMota
 * @group             : 
 * @last modified on  : 06-21-2021
 * @last modified by  : Alysson Mota
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   05-27-2021   Alysson Mota   Initial Version
**/
@isTest
public with sharing class OpportunityValidationHelperTest {

    @isTest
    public static void insertSmbOpp() {
        Id directChannelAccRtId  = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
        Id smbNewBusinessRtId       = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SMB_New_Business').getRecordTypeId();
        Id smbSuccessRenegRtId      = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SMB_Success_Renegotiation').getRecordTypeId();   
        
        Account acc = new Account();
        acc.RecordTypeId = directChannelAccRtId;
        acc.Name = 'Test Account';
        acc.BillingCountry = 'Brazil';
        acc.BillingState = 'São Paulo';
        acc.NumberOfEmployees = 1001;
        insert acc;

        Opportunity smbNewBusOpp = new Opportunity();
        smbNewBusOpp.RecordTypeId = smbNewBusinessRtId;
        smbNewBusOpp.Name = 'Test Opp';
        smbNewBusOpp.AccountId = acc.Id;
        
        try {
            insert smbNewBusOpp; 
        } catch(Exception e) {
            //System.assertEquals(e.getMessage().contains(OpportunityValidationHelper.smbNewBusinessInsertError), true);
        }

        Opportunity smbRenegOpp = new Opportunity();
        smbRenegOpp.RecordTypeId = smbSuccessRenegRtId;
        smbRenegOpp.Name = 'Test Opp';
        smbRenegOpp.AccountId = acc.Id;

        try {
            insert smbRenegOpp; 
        } catch(Exception e) {
            //System.assertEquals(e.getMessage().contains(OpportunityValidationHelper.smbRenegInsertError), true);
        }

    }

    @isTest
    public static void updateSmbOpp() {
        Id directChannelAccRtId  = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
        Id smbNewBusinessRtId       = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SMB_New_Business').getRecordTypeId();
        Id smbSuccessRenegRtId      = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SMB_Success_Renegotiation').getRecordTypeId();   
        
        Account acc = new Account();
        acc.RecordTypeId = directChannelAccRtId;
        acc.Name = 'Test Account';
        acc.BillingCountry = 'Brazil';
        acc.BillingState = 'São Paulo';
        acc.NumberOfEmployees = 500;
        insert acc;

        Opportunity smbNewBusOpp = new Opportunity();
        smbNewBusOpp.RecordTypeId = smbNewBusinessRtId;
        smbNewBusOpp.Name = 'Test Opp';
        smbNewBusOpp.AccountId = acc.Id;
        smbNewBusOpp.StageName ='Qualificação';
        smbNewBusOpp.CloseDate = System.now().date();
        smbNewBusOpp.Quantity_Offer_Number_of_Employees__c = 500;
        insert smbNewBusOpp;
        
        try {
            smbNewBusOpp.Quantity_Offer_Number_of_Employees__c = 2000;
            update smbNewBusOpp; 
        } catch(Exception e) {
            //System.assertEquals(e.getMessage().contains(OpportunityValidationHelper.smbNewBusinessUpdateError), true);
        }

        Opportunity smbRenegOpp = new Opportunity();
        smbRenegOpp.RecordTypeId = smbSuccessRenegRtId;
        smbRenegOpp.Name = 'Test Opp';
        smbRenegOpp.AccountId = acc.Id;
        smbRenegOpp.StageName = 'Qualificação';
        smbRenegOpp.CloseDate = System.now().date();
        smbNewBusOpp.Quantity_Offer_Number_of_Employees__c = 600;
        insert smbRenegOpp;

        try {
            smbRenegOpp.Quantity_Offer_Number_of_Employees__c = 1500;
            update smbRenegOpp; 
        } catch(Exception e) {
            //System.assertEquals(e.getMessage().contains(OpportunityValidationHelper.smbRenegUpdateError), true);
        }

    }
}