public with sharing class TaxIdFactory {

    public static final String TAX_ID_TYPE = 'CNPJ';
    

    public static Tax_Id_Data__c getTraxIdDataByWrapperBrazil(TaxIdBrazilWrapper wrapper){

        Tax_Id_Data__c taxId = new Tax_Id_Data__c();
        //taxId.Data_Situacao__c = wrapper.data_situacao != null ? Date.ValueOf(wrapper.data_situacao) : null;
        taxId.Complemento__c = wrapper.complemento;
        taxId.Tipo__c = wrapper.tipo;
        taxId.Nome__c = wrapper.nome;
        taxId.Uf__c = wrapper.uf;
        taxId.Telefone__c = wrapper.telefone;
        taxId.Email__c = wrapper.email;
        taxId.Situacao__c = wrapper.situacao;
        taxId.Bairro__c = wrapper.bairro;
        taxId.Logradouro__c = wrapper.logradouro;
        taxId.Numero__c = wrapper.numero != null ? Decimal.ValueOf(wrapper.numero) : null;
        taxId.Cep__c = wrapper.cep;
        taxId.Municipio__c = wrapper.municipio;
        taxId.Porte__c = wrapper.porte;
        taxId.Abertura__c = wrapper.abertura;
        taxId.Natureza_Juridica__c = wrapper.natureza_juridica;
        taxId.Nome_Fantasia__c = wrapper.fantasia;
        taxId.Tax_Id__c = wrapper.cnpj;
        //taxId.Ultima_Atualizacao_Receita__c = wrapper.ultima_atualizacao;
        taxId.Status__c = wrapper.status;
        taxId.Motivo_Situacao__c = wrapper.motivo_situacao;
        taxId.Situacao_Especial__c = wrapper.situacao_especial;
        taxId.Data_Situacao_Especial__c = wrapper.data_situacao_especial;
        taxId.Capital_Social__c = wrapper.capital_social;
        taxId.Tax_Id_Type__c = TAX_ID_TYPE;

        return taxId;

    }


}