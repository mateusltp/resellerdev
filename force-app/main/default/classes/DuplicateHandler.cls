/**
 * @File Name          : DuplicateHandler.cls
 * @Description        : 
 * @Author             : GEPI@GFT.com
 * @Group              : 
 * @Last Modified By   : GEPI@GFT.com
 * @Last Modified On   : 08-07-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    04/06/2020   Samuel Silva - GFT (slml@gft.com)     Initial Version
**/
public without sharing class DuplicateHandler {
    
    public static List<SObject> foundDuplicateRecords(SObject sObj){        
        return foundDuplicateRecords(new List<SObject>{sObj});
    }
    
    public static List<SObject> foundDuplicateRecords(List<SObject> sObjLst){
        List<SObject> recordsResult = new List<SObject>();
        Datacloud.FindDuplicatesResult[] results = Datacloud.FindDuplicates.findDuplicates(sObjLst);
        for(Datacloud.FindDuplicatesResult findDupeResult : results) {
            for (Datacloud.DuplicateResult dupeResult : findDupeResult.getDuplicateResults()) {
                for (Datacloud.MatchResult matchResult : dupeResult.getMatchResults()) {  
                      for (Datacloud.MatchRecord mr : matchResult.getMatchRecords()) {                            
                        recordsResult.add(mr.getRecord());
                    }
                }
            }
        }   
        return recordsResult;
    }


    public static Set<Id> foundLeadDuplicateLeads(Lead l){
        Set<Id> leadsResults = new Set<Id>();
        List<SObject> results = foundDuplicateRecords(l);
        for(SObject objR : results) {
            if(objR.getSObjectType().getDescribe().getName() == 'Lead') {
                leadsResults.add(objR.Id);
            }
        }
        return leadsResults;
    }

    
    public static Set<Id> foundLeadDuplicateAccount(Lead l){
        Set<Id> accResults = new Set<Id>();
        List<SObject> results = foundDuplicateRecords(l);
        for(SObject objR : results) {
            if(objR.getSObjectType().getDescribe().getName() == 'Account') {
                accResults.add(objR.Id);
            }
        }
        return accResults;
    } 
        
}