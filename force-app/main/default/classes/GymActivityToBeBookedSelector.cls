/**
 * @description       : 
 * @author            : alysson.mota@gympass.com
 * @group             : 
 * @last modified on  : 06-29-2022
 * @last modified by  : alysson.mota@gympass.com
**/
public with sharing class GymActivityToBeBookedSelector extends ApplicationSelector{
    public List<Schema.SObjectField> getSObjectFieldList() {
		return new List<Schema.SObjectField> {
			Gym_Activity_To_Be_Booked__c.Id,
			Gym_Activity_To_Be_Booked__c.Name
		};
	}

	public Schema.SObjectType getSObjectType() {
		return Gym_Activity_To_Be_Booked__c.sObjectType;
	}

    public List<Gym_Activity_To_Be_Booked__c> byProdId(Set<Id> prodIdSet) {
        
        return (List<Gym_Activity_To_Be_Booked__c>)Database.query(
            newQueryFactory().
                selectField(Gym_Activity_To_Be_Booked__c.Id).
                selectField(Gym_Activity_To_Be_Booked__c.Name).
                selectField(Gym_Activity_To_Be_Booked__c.GymActivityRelationshipId__c).
                selectField('GymActivityRelationshipId__r.Name').
                setCondition('Product_Item__c IN :prodIdSet').
                toSOQL());
    }
}