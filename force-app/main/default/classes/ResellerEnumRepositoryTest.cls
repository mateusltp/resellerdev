@isTest
global class ResellerEnumRepositoryTest {
    
    @isTest
    global static void testAllEnums(){
        Test.startTest();
        ResellerEnumRepository.ResellerLogActions CLICK_RUN_GO_NOGO =  ResellerEnumRepository.ResellerLogActions.CLICK_RUN_GO_NOGO;
        ResellerEnumRepository.ResellerLogActions.CLICK_RUN_GO_NOGO_GET_PRICE.name();
            
        ResellerEnumRepository.AccountRequestPartnerModel.Subsidy.name();
        ResellerEnumRepository.AccountRequestPartnerModel.Intermediation.name();
        
        ResellerEnumRepository.AccountRequestStatus.Analyze.name();
        ResellerEnumRepository.AccountRequestStatus.Invalid.name();
        
        ResellerEnumRepository.AccountRequestBID.Yes.name();
        ResellerEnumRepository.AccountRequestBID.No.name();
    
        ResellerEnumRepository.MassiveAccountRequestStatus.Draft.name();
        ResellerEnumRepository.MassiveAccountRequestStatus.Loading.name();
        ResellerEnumRepository.MassiveAccountRequestStatus.LookingForAccounts.name();
        ResellerEnumRepository.MassiveAccountRequestStatus.LookingForOpportunities.name();
        ResellerEnumRepository.MassiveAccountRequestStatus.LookingForEvents.name();
        ResellerEnumRepository.MassiveAccountRequestStatus.RunEngine.name();
        ResellerEnumRepository.MassiveAccountRequestStatus.Finished.name();

        ResellerEnumRepository.UniqueIdentifierType.CNPJ.name();
        ResellerEnumRepository.UniqueIdentifierType.NIF.name();
        ResellerEnumRepository.UniqueIdentifierType.EIN.name();
        ResellerEnumRepository.UniqueIdentifierType.UTR.name();

        ResellerEnumRepository.CountryCode.BR.name();

        ResellerEnumRepository.MassiveLog.UPLOADED_NEW_ACCOUNT_REQUEST.name();
        ResellerEnumRepository.MassiveLog.UNIQUE_KEY_VALIDED.name();
        ResellerEnumRepository.MassiveLog.UNIQUE_KEY_INVALIDED.name();
        ResellerEnumRepository.MassiveLog.UNIQUE_KEY_NOT_FOUND.name();
        ResellerEnumRepository.MassiveLog.NEW_ACCOUNT_SUCCESSFULLY_CREATED.name();
        ResellerEnumRepository.MassiveLog.NEW_ACCOUNT_NOT_CREATED.name();
        ResellerEnumRepository.MassiveLog.NEW_ACCOUNT_CREATED_BY_UNIQUE_KEY.name();
        ResellerEnumRepository.MassiveLog.NEW_ACCOUNT_CREATED_BY_WEB_SITE.name();
        ResellerEnumRepository.MassiveLog.ACCOUNT_FOUND_BY_CNPJ.name();
        ResellerEnumRepository.MassiveLog.ACCOUNT_FOUND_BY_WEBSITE.name();
        ResellerEnumRepository.MassiveLog.OPPORTUNITY_FOUND.name();
        ResellerEnumRepository.MassiveLog.OPPORTUNITY_NOT_FOUND.name();
        ResellerEnumRepository.MassiveLog.ACCOUNT_EVENT_FOUND.name();
        ResellerEnumRepository.MassiveLog.ACCOUNT_TASK_FOUND.name();
        ResellerEnumRepository.MassiveLog.ACCOUNT_ACTIVITY_NOT_FOUND.name();
        ResellerEnumRepository.MassiveLog.ENGINE_RUN.name();
        ResellerEnumRepository.MassiveLog.INFO_CURRENT_USER_FOUND.name();
        ResellerEnumRepository.MassiveLog.INFO_OWNER_ACCOUNT_REQUEST_FOUND.name();
        ResellerEnumRepository.MassiveLog.INFO_NOT_FOUND.name();
        ResellerEnumRepository.MassiveLog.NEW_OPPORTUNITY_SUCCESSFULLY_CREATED.name();
        ResellerEnumRepository.MassiveLog.NEW_OPPORTUNITY_NOT_CREATED.name();
        System.assertNotEquals(null, ResellerEnumRepository.MassiveLog.NEW_OPPORTUNITY_NOT_CREATED);
        Test.stopTest();
    }

}