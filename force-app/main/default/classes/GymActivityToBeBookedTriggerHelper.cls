public class GymActivityToBeBookedTriggerHelper {
    
    List<Gym_Activity_To_Be_Booked__c> newActivities = trigger.new;
    Map<Id, Gym_Activity_To_Be_Booked__c> gymIdMap = (Map<Id,Gym_Activity_To_Be_Booked__c>)trigger.newMap;
    Map<Id, Gym_Activity_To_Be_Booked__c> gymOldMap = (Map<Id,Gym_Activity_To_Be_Booked__c>)trigger.oldMap;
    
    public void afterInsertMethod(){
        Set<ID> prodIds = new Set<ID>();
        Set<String> gym = new Set<String>();
        List<Product_Item__c> pListUp = new List<Product_Item__c>();
        
        for (Gym_Activity_To_Be_Booked__c na : newActivities){
            if (na.Product_Item__c != null) {
                prodIds.add(na.Product_Item__c);
                gym.add(na.Product_Item__c);
            }
        }
        List<Product_Item__c> pList = [SELECT Id, Name FROM Product_Item__c WHERE ID IN: prodIds];
              
        List<Gym_Activity_To_Be_Booked__c> cList = [Select id, name, Product_Item__c FROM Gym_Activity_To_Be_Booked__c WHERE Product_Item__c IN : prodIds];
        //System.debug('Show activities related to product '+cList);
        
        For(Product_Item__c a : pList){
            // a list of string, you wanna display
            List<String> tempLst = new List<String>();
            for(Gym_Activity_To_Be_Booked__c wrapper: cList) {
                tempLst.add(wrapper.name);
            }
            //system.debug('tempLst está cheia? '+tempLst);
            
            // making a single string with comma seprated from above list
            String commaSepratedList='';
            
            for(String str : tempLst)
            {
                commaSepratedList += str + '; ' ;
            }
            
            // remove last additional comma from string
            commaSepratedList = commaSepratedList.subString(0,commaSepratedList.length());
           // system.debug('comma seprated list is----> '+commaSepratedList);
            
            a.Gym_Activity_To_Be_Booked_Concat__c = commaSepratedList;
            pListUp.add(a);
        }
        update pListUp;
    }
    
    public void afterDeleteMethod(){
        List<Product_Item__c> pListUp = new List<Product_Item__c>();
        
        Set<ID> prodIds = new Set<ID>();
        Set<String> gym = new Set<String>();
        for (Gym_Activity_To_Be_Booked__c na : gymOldMap.values()){
             if (na.Product_Item__c != null) {
                prodIds.add(na.Product_Item__c);
                gym.add(na.Product_Item__c);
            }
            
        }
           
        List<Product_Item__c> pList = [SELECT Id, Name FROM Product_Item__c WHERE ID IN: prodIds];
        
        Map<String, Gym_Activity_To_Be_Booked__c> gymMap = new Map<String, Gym_Activity_To_Be_Booked__c>();
        for (Gym_Activity_To_Be_Booked__c gym2 : [SELECT Product_Item__c FROM Gym_Activity_To_Be_Booked__c WHERE Product_Item__c IN :gym]) {
            gymMap.put(gym2.Product_Item__c, gym2);
        }
        
        List<Gym_Activity_To_Be_Booked__c> cList = [SELECT Id, name, Product_Item__c FROM Gym_Activity_To_Be_Booked__c Where Product_Item__c IN : prodIds ];
       // system.debug('lista das atividades pra concatenar '+cList);
        
        For(Product_Item__c a : pList){
            // a list of string, you wanna display
            List<String> tempLst = new List<String>();
            for(Gym_Activity_To_Be_Booked__c wrapper: cList) {
                tempLst.add(wrapper.name);
            }
          //  system.debug('tempLst está cheia? '+tempLst);
            
            // making a single string with comma seprated from above list
            String commaSepratedList='';
            
            for(String str : tempLst)
            {
                commaSepratedList += str + '; ' ;
            }
            
            // remove last additional comma from string
            commaSepratedList = commaSepratedList.subString(0,commaSepratedList.length());
           // system.debug('comma seprated list is----> '+commaSepratedList);
            
            a.Gym_Activity_To_Be_Booked_Concat__c = commaSepratedList;
            pListUp.add(a);
        }
        update pListUp;
    }
}