/**
 * @description       : 
 * @author            : gepi@gft.com
 * @group             : 
 * @last modified on  : 04-13-2022
 * @last modified by  : gepi@gft.com
**/
public with sharing class editPartnerProductController {
    
    @AuraEnabled
    public static String findRecordType(String recordId){
        ProductOpportunityAssignmentSelector poaSelector = (ProductOpportunityAssignmentSelector)Application.Selector.newInstance(Product_Opportunity_Assignment__c.sObjectType);
        List<Product_Opportunity_Assignment__c> poaLst = poaSelector.selectByOpportunitybyProductId(new set<id>{recordId});
        string rtOpp;
        if(!poaLst.isEmpty()){
            for(Product_Opportunity_Assignment__c poa : poaLst){
                rtOpp = poa.OpportunityMemberId__r.Opportunity__r.RecordType.DeveloperName;
            }
        }
        system.debug('::rtOpp' + rtOpp);
        return rtOpp;
    }
}