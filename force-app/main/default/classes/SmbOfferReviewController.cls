/**
 * @description       : 
 * @author            : vstl@gft.com
 * @group             : 
 * @last modified on  : 06-03-2022
 * @last modified by  : vstl@gft.com
**/
public without sharing class SmbOfferReviewController {
    public SmbOfferReviewController(){}

    public static Map< String , String > gMapDealDeskApprovedConditions;

    @AuraEnabled(cacheable=false)
    public static void sendToApproval( String recordId , String aRationale , Decimal aTotalDiscount , String aQuoteId , String aDealDeskRationale,
        Boolean needQuoteApproval, Boolean needDealDeskApproval ){
        String lWaivers = getWaivers( aQuoteId );
        
        try{
            if( needQuoteApproval ){    
                Approval.ProcessSubmitRequest quoteReq = new Approval.ProcessSubmitRequest();
                quoteReq.setComments( 'Total Discount: ' + aTotalDiscount + '%\n' + lWaivers + '\nRationale:' + aRationale );
                quoteReq.setObjectId( aQuoteId );
                quoteReq.setSubmitterId( UserInfo.getUserId() );
                quoteReq.setNextApproverIds( getInicialApprover( aQuoteId ) );
                quoteReq.setProcessDefinitionNameOrId( 'SKU_Quote_Discount_Approval' );
            
                Approval.ProcessResult result = Approval.process( quoteReq );
            }
        } catch( Exception aException ){
            if( !aException.getMessage().contains( 'ALREADY_IN_PROCESS' ) ){
                if(!Test.isRunningTest()){
                    throw new CustomException( 'Approval has already been requested for this opportunity. You can\'t request it again.' );
                }
            }
            if( !aException.getMessage().contains( 'NO_APPLICABLE_PROCESS' ) ){
                throw new CustomException( aException.getMessage() );
            }
        }
    }

    public static String getWaivers( String aQuoteId ){
        String lAllWaivers = '';
        Map< String , String > lMapItemWaivers = new Map< String , String >();

        for( Waiver__c iWaiver : [ SELECT Id, Quote_Line_Item__r.Product_Name__c, Percentage__c, Duration__c FROM Waiver__c WHERE Quote_Line_Item__r.QuoteId =: aQuoteId ] ){
            if( lMapItemWaivers.get( iWaiver?.Quote_Line_Item__r?.Product_Name__c ) == null ){
                lMapItemWaivers.put( iWaiver.Quote_Line_Item__r.Product_Name__c , ( iWaiver.Percentage__c + '% / ' + iWaiver.Duration__c + ' months' ) );
            } else {
                String lNewWaiver = lMapItemWaivers.get( iWaiver.Quote_Line_Item__r.Product_Name__c );
                lNewWaiver += ( ' - ' + iWaiver.Percentage__c + '% / ' + iWaiver.Duration__c + ' months' );
                lMapItemWaivers.put( iWaiver.Quote_Line_Item__r.Product_Name__c , lNewWaiver );
            }
        }

        for( String iQuoteLineItem : lMapItemWaivers.keySet() ){
            lAllWaivers += (( iQuoteLineItem + ': ' + lMapItemWaivers.get( iQuoteLineItem ) ) + '\n' );
        }

        return lAllWaivers;
    }

    public static Id[] getInicialApprover( String aQuoteId ){
        Quote lQuote = [ SELECT Discount_Approval_Level__c, Waiver_Approval_Level__c FROM Quote WHERE Id =: aQuoteId ];
        User lUser = [ SELECT ManagerId, Manager.ManagerId FROM User WHERE Id =: UserInfo.getUserId() ];

        Decimal aTotalDiscountLevel = lQuote.Discount_Approval_Level__c;
        Decimal aWaiverApprovalLevel = lQuote.Waiver_Approval_Level__c;

        return new Id[]{ ( lQuote.Discount_Approval_Level__c == 0 && lQuote.Waiver_Approval_Level__c > 0 ? lUser.Manager.ManagerId : lUser.ManagerId ) };
    }
}