/**
 * @description       : 
 * @author            : roei@gft.com
 * @group             : 
 * @last modified on  : 06-04-2022
 * @last modified by  : tania.fartaria@gympass.com
**/
public with sharing class SkuEnterpriseRevenueMetrics {



    public SkuEnterpriseRevenueMetrics(){}
    
    @InvocableMethod
    public static void calculateEnterpriseRevenue( List<String> aQuoteIds ){

        List< QuoteLineItem > lLstQuoteLineItems = getQuoteItems( aQuoteIds.get(0) );
       

        if(lLstQuoteLineItems.size() > 0){
            Map<String, List<QuoteLineItem>> gMapFeeTypeLstItem = setMapFeeTypeLstItem(lLstQuoteLineItems);
            Map<String, List<Waiver__c>> gMapItemIdLstWaiver = setMapItemIdLstWaiver(lLstQuoteLineItems);
            Decimal enterpriseMonthly = getEnterpriseSubscription( gMapFeeTypeLstItem.get('Recurring fee'));

            Decimal gEsRevenue = calculateEs( gMapFeeTypeLstItem.get('Recurring fee'));
            Decimal gAesRevenue = calculateAes( gMapFeeTypeLstItem.get('Recurring fee'), gMapItemIdLstWaiver );
            Decimal gErRevenue = calculateEr( enterpriseMonthly, gMapFeeTypeLstItem.get('One time fee' ));
            Decimal gAerRevenue = calculateAer(enterpriseMonthly, gMapFeeTypeLstItem.get('One time fee'), gMapItemIdLstWaiver );

            Quote quoteToUpdate = new Quote (
                Id = aQuoteIds.get(0),
                ES_Revenue__c = gEsRevenue,
                AES_Revenue__c = gAesRevenue,
                ER_Revenue__c = gErRevenue,
                AER_Revenue__c = gAerRevenue
            );
            Database.update(quoteToUpdate);
    
            Decimal potentialSku = calculatePotential( lLstQuoteLineItems );

            Opportunity oppToUpdate = new Opportunity (
                Id = lLstQuoteLineItems.get(0).Quote.OpportunityId,
                PotentialSKU__c = potentialSku
            );
            Database.update(oppToUpdate);
        }
       
    }

    public static decimal calculateEs( List<QuoteLineItem> aLstRecurringItem ){
        Decimal lSumMonthlyRevenue = 0;
        if(aLstRecurringItem == null)
            return lSumMonthlyRevenue;
        for( QuoteLineItem item : aLstRecurringItem ){
            lSumMonthlyRevenue += item?.Subtotal == null ? 0 : item.Subtotal;
        }

        return lSumMonthlyRevenue;
    }

    public static decimal getEnterpriseSubscription( List<QuoteLineItem> aLstRecurringItem ){
        Decimal enterpriseMonthly = 0;
        if(aLstRecurringItem == null)
            return enterpriseMonthly;
        for( QuoteLineItem item : aLstRecurringItem ){
           if(item.Product_Name__c == 'Enterprise Subscription'){
                enterpriseMonthly= item.subtotal;
                break;
           }
        }
        return enterpriseMonthly;
    }

    public static  decimal calculateAes( List<QuoteLineItem> aLstRecurringItem ,Map<String, List<Waiver__c>> gMapItemIdLstWaiver ){
        Decimal lSumAes = 0;
        if(aLstRecurringItem == null)
            return lSumAes;

        for( QuoteLineItem item : aLstRecurringItem ){
            List< Waiver__c > lLstWaiver = gMapItemIdLstWaiver.get( item.Id );
            Decimal sumWaiverItem = 0;
            Decimal waiverMonth = 0;
            Decimal itemAes = 0;

            if( lLstWaiver == null ){ lLstWaiver = new List< Waiver__c >(); }

            for( Waiver__c iWaiver : lLstWaiver ){
                waiverMonth = (iWaiver.Duration__c == null ? 0  : iWaiver.Duration__c);
                if (waiverMonth > 12)
                    waiverMonth = 12;
        
                sumWaiverItem += item.Subtotal * iWaiver.Percentage__c/100 * waiverMonth;
            }

            itemAes = (( 12  *  item.Subtotal) - (sumWaiverItem));
            lSumAes  += itemAES;
        }

        return lSumAes;
    }

    public static decimal calculateEr( Decimal enterpriseMonthly, List<QuoteLineItem> aLstOneTimeFee ){
        Decimal lSumOneTime = 0;
        if(aLstOneTimeFee == null)
            return enterpriseMonthly + lSumOneTime;

        for( QuoteLineItem iItem : aLstOneTimeFee ){
            lSumOneTime += iItem?.Subtotal == null ? 0 : iItem.Subtotal;
        }

        return enterpriseMonthly + lSumOneTime;
    }


    public static decimal calculateAer( Decimal enterpriseMonthly, List<QuoteLineItem> aLstOneTimeFee, Map<String, List< Waiver__c>> gMapItemIdLstWaiver ){
        Decimal sumAerOneTime = 0;
        
        if(aLstOneTimeFee == null)
            return enterpriseMonthly *12;
        
        for( QuoteLineItem item : aLstOneTimeFee ){
            List< Waiver__c > lLstWaiver = gMapItemIdLstWaiver.get( Item.Id );
            Decimal sumWaiverItem = 0;
            Decimal waiverMonth = 0;
            Decimal itemAes = 0;

            if( lLstWaiver == null ){ lLstWaiver = new List< Waiver__c >(); }

            for( Waiver__c iWaiver : lLstWaiver ){
                waiverMonth = (iWaiver.Duration__c ==   null ? 0  : iWaiver.Duration__c);
                if (waiverMonth > 12)
                    waiverMonth = 12;

                sumWaiverItem += item.Subtotal * iWaiver.Percentage__c/100 * waiverMonth;
            }

            itemAes = (( 12  *  item.Subtotal) - (sumWaiverItem));
            sumAerOneTime  += itemAES;
        }


        return enterpriseMonthly*12 + sumAerOneTime;
    }

    public static decimal calculatePotential( List<QuoteLineItem> allQuoteLineItems ){
        Decimal potential = 0;
        for( QuoteLineItem item : allQuoteLineItems ){
            potential += item?.List_Price__c == null ? 0 : item.List_Price__c * item.Quantity;
        }
        return potential;
    }




    private static List<QuoteLineItem> getQuoteItems(Id aQuoteId){

        List<QuoteLineItem> lineItems = [SELECT Id, 
                                                TotalPrice, 
                                                List_Price__c,
                                                Subtotal,
                                                Quantity,
                                                Type__c, 
                                                Fee_Type__c, 
                                                Product_Name__c,
                                                Product2.Payment_Type__c,
                                                (SELECT Id, Duration__c,Percentage__c
                                                FROM Waivers__r) , 
                                                quote.OpportunityId
                                        FROM QuoteLineItem 
                                        WHERE quoteId = : aQuoteId];
        return lineItems;
    }

    private static Map<String, List<QuoteLineItem>> setMapFeeTypeLstItem(List<QuoteLineItem> lLstQuoteLineItems){
        Map< String , List< QuoteLineItem > > gMapFeeTypeLstItem = new Map< String , List< QuoteLineItem > >();
        
        for(QuoteLineItem item : lLstQuoteLineItems){
            if(item.Product2.Payment_Type__c == 'Recurring fee'){
                if(gMapFeeTypeLstItem.get('Recurring fee') == null ){
                    gMapFeeTypeLstItem.put('Recurring fee', new List<QuoteLineItem>{item});
                }else{
                    List<QuoteLineItem> items = gMapFeeTypeLstItem.get('Recurring fee');
                    items.add(item);
                    gMapFeeTypeLstItem.put('Recurring fee', items);
                } 
            }
            else if(item.Product2.Payment_Type__c == 'One time fee'){
                if(gMapFeeTypeLstItem.get('One time fee') == null ){
                    gMapFeeTypeLstItem.put('One time fee', new List<QuoteLineItem>{item});
                }else{
                    List<QuoteLineItem> items = gMapFeeTypeLstItem.get('One time fee');
                    items.add(item);
                    gMapFeeTypeLstItem.put('One time fee', items);
                }
            }     
        }
        return gMapFeeTypeLstItem;
    }

    private static Map<String, List<Waiver__c>> setMapItemIdLstWaiver(List<QuoteLineItem> lLstQuoteLineItems){
        Map< String , List< Waiver__c >> gMapItemIdLstWaiver = new Map< String , List< Waiver__c >>();
        for(QuoteLineItem item : lLstQuoteLineItems ){
            gMapItemIdLstWaiver.put(item.Id, item.Waivers__r);
        }
        return gMapItemIdLstWaiver;
    }
}