/**
 * @description       : 
 * @author            : gft.jorge.stevaux@ext.gympass.com
 * @group             : 
 * @last modified on  : 04-13-2022
 * @last modified by  : gft.jorge.stevaux@ext.gympass.com
**/
global with sharing class ProdOppAssDataProvider implements sortablegrid.sdgIDataProvider  {

    global static Boolean isUserSelectable()
    {
        System.debug('$$$$$');
        return true;
    }

    global sortablegrid.SDGResult getData(sortablegrid.SDG coreSDG,  sortablegrid.SDGRequest request)
    {
        sortablegrid.SDGResult result = new sortablegrid.SDGResult();

        Map<Id, Product_Opportunity_Assignment__c> prodAssignMap = new Map<Id, Product_Opportunity_Assignment__c>();

       ProductOpportunityAssignmentSelector prodOppSelector = (ProductOpportunityAssignmentSelector)Application.Selector.newInstance(Product_Opportunity_Assignment__c.sobjectType);
       List<Product_Opportunity_Assignment__c> paLst = prodOppSelector.selectOpportunityByProductId(request.ParentRecordID);
        

        if (paLst != null) {
            for(Product_Opportunity_Assignment__c prod : paLst){
                    prodAssignMap.put(prod.ProductAssignmentId__r.ProductId__c, prod);
            }
        }

        result.data = new List<Sobject>();

        result.data.addAll((List<sObject>)prodAssignMap.values());
        result.FullQueryCount = result.data.size();
        result.pagecount = 1;
        result.isError = false;
        result.ErrorMessage = '';
        return result;
    }

    global sortablegrid.SDG LoadSDG(String SDGTag, String ParentRecordId)
    {
        sortablegrid.SDG CoreSDG = new sortablegrid.SDG( 'teste' );
        CoreSDG.SDGFields = GetFields();
        return CoreSDG;
    }

   
    private List<sortablegrid.SDGField> GetFields()
    {
        List<sortablegrid.SDGField> fields = new List<sortablegrid.SDGField>();

        fields.add( new sortablegrid.SDGField('1', 'Account', 'OpportunityMemberId__r.Account__r.Name', 'STRING', '', false, false, null, 1));      
        fields.add( new sortablegrid.SDGField('2', 'Opportunity', 'OpportunityMemberId__r.Opportunity__r.Name', 'STRING', '', false, false, null, 2));      
        fields.add( new sortablegrid.SDGField('3', 'Stage', 'OpportunityMemberId__r.Opportunity__r.StageName', 'STRING', '', false, false, null, 3));      
        fields.add( new sortablegrid.SDGField('4', 'Close Date', 'OpportunityMemberId__r.Opportunity__r.CloseDate', 'STRING', '', false, false, null, 4));      
        return fields;
    }  

}