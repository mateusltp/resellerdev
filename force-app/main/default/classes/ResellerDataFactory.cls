public with sharing class ResellerDataFactory {

    public static Account createClientAccountBrazil(){

        return new Account( Name = 'Reseller Gympass Teste', 
                            Legal_Document_Type__c = 'CNPJ', 
                            Id_Company__c = '56947401000108',
                            Razao_Social__c = 'Reseller Brazileiro', 
                            Website = 'resellerBrazilTeste@teste.com', 
                            Industry = 'Airlines',
                            BillingCountry = 'Brazil');
        

    }

    public static Account createIndirectAccountBrazil(){
        Account account = new Account();  
        account.Name = 'IndirectAccount Reseller';
        account.Legal_Document_Type__c = 'CNPJ';
        account.Id_Company__c = '43712486000101';
        account.Razao_Social__c = 'IndirectAccount';
        account.Website = 'IndirectAccount.com';
        account.Industry = 'Airlines';
        account.BillingCountry = 'Brazil';
        account.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
        return account;
    }
    

    public static Account_Request__c createGenericAccountRequestBrazil(Account account){
        return new Account_Request__c(  Name = 'Reseller Brazil', 
                                        Unique_Identifier_Type__c = 'CNPJ',
                                        Unique_Identifier__c = '56947401000108',
                                        Website__c = 'www.resellerBrazilTeste.com',
                                        Contact_Name__c = 'Brazil Contact Teste',
                                        AccountId__c = account.Id,
                                        Billing_Country__c = 'Brazil',
                                        Email__c = 'resellerBrazilTeste@teste.com'); 

    }

    public static Account_Request__c createGenericAccountRequestBrazil(){
        return new Account_Request__c(  Name = 'Reseller Brazil', 
                                        Unique_Identifier_Type__c = 'CNPJ',
                                        Unique_Identifier__c = '01117581000109',
                                        Website__c = 'www.resellerBrazilTesteSemConta.com',
                                        Contact_Name__c = 'Brazil Contact Teste Sem Conta',
                                        Billing_Country__c = 'Brazil',
                                        Email__c = 'resellerBrazilTesteSemConta@teste.com');

    }


}