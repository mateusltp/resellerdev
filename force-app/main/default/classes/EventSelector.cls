/**
 * @description       : 
 * @author            : alysson.mota@gympass.com
 * @group             : 
 * @last modified on  : 04-07-2022
 * @last modified by  : alysson.mota@gympass.com
**/
public with sharing class EventSelector extends ApplicationSelector {
    public List<Schema.SObjectField> getSObjectFieldList() {
		return new List<Schema.SObjectField> {
			Event.Id
		};
	}

	public Schema.SObjectType getSObjectType() {
		return Event.sObjectType;
	}

    public List<Event> byOppIdOrAccId(Set<Id> oppIds, Set<Id> accIds) {
        return (List<Event>) Database.query(
				newQueryFactory().
				selectField(Event.Id).
				selectField(Event.WhoId).
				selectField(Event.WhatId).
				setCondition('(WhatId IN: oppIds AND StartDateTime = LAST_N_DAYS:120) OR (WhatId IN: accIds AND StartDateTime = LAST_N_DAYS:90)').
				toSOQL()
		);
    }
}