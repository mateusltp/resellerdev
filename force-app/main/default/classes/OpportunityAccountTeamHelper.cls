/**
 * @description       : 
 * @author            : AlyssonMota
 * @group             : 
 * @last modified on  : 05-28-2021
 * @last modified by  : Alysson Mota
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   05-10-2021   Alysson Mota   Initial Version
**/
public without sharing class OpportunityAccountTeamHelper {
    Id clientNewBusinessRecType = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName()
    .get('Client_Sales_New_Business').getRecordTypeId();

    Id smbNewBusinessRecType = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName()
    .get('SMB_New_Business').getRecordTypeId();

    public void assignUserAccountTeamMembersOnLaunch(Map<Id, Opportunity> oldIdToOpp, Map<Id, Opportunity> newIdToOpp) {
        System.debug('assignUserAccountTeamMembersOnLaunch');
        Map<Id, Opportunity> idToWonNewBusinessOpp = new Map<Id, Opportunity>();
        
        for (Opportunity newOpp : newIdToOpp.values()) {
            Opportunity oldOpp = oldIdToOpp.get(newOpp.Id);
            if ((newOpp.RecordTypeId == clientNewBusinessRecType || newOpp.RecordTypeId == smbNewBusinessRecType) 
                && oldOpp.StageName != 'Lançado/Ganho' && newOpp.StageName == 'Lançado/Ganho')
                idToWonNewBusinessOpp.put(newOpp.Id, newOpp);
        }
        
        if (idToWonNewBusinessOpp.size() > 0)
            assignUserAccountTeamMembers(idToWonNewBusinessOpp);
        else
            return;
    }
    
    private void assignUserAccountTeamMembers(Map<Id, Opportunity> idToOpp) {
        System.debug('assignUserAccountTeamMembers');
        FormRepository formRepo = new FormRepository();
        
        Map<Id, Form__c> oppIdToM0Form = formRepo.getOppIdToM0Map(new List<Id>(idToOpp.keySet()));
        
        Set<Id> userIdSet = getUserIdSet(idToOpp, oppIdToM0Form);
        
        Map<Id, List<UserAccountTeamMember>> ownerIdToUserAccountTeamMemberList = getUserAccountTeamMembersAsMap(userIdSet);

        insertAccountTeamMembers(idToOpp, ownerIdToUserAccountTeamMemberList);
    }

    private void insertAccountTeamMembers(Map<Id, Opportunity> idToOpp, Map<Id, List<UserAccountTeamMember>> ownerIdToUserAccountTeamMemberList) {
        System.debug('insertAccountTeamMembers');
        List<AccountTeamMember> accountTeamMembersToInsert = new List<AccountTeamMember>();
        List<AccountShare> accountShareList = new List<AccountShare>();

        for (Opportunity opp : idToOpp.values()) {
            List<UserAccountTeamMember> uatmList = ownerIdToUserAccountTeamMemberList.get(opp.Responsavel_Gympass_Relacionamento__c);
            
            if (uatmList != null)
                accountTeamMembersToInsert.addAll(createAccountTeamInstances(opp, uatmList));
        }

        Database.SaveResult[] lsr = Database.insert(accountTeamMembersToInsert,false);

        for(Database.SaveResult sr:lsr) {
            if(!sr.isSuccess()) {
                Database.Error emsg =sr.getErrors()[0];
                system.debug('\n\nERROR ADDING TEAM MEMBER:'+emsg);
            } else {

                System.debug('Success');            
            }
        }
    }

    private List<AccountTeamMember> createAccountTeamInstances(Opportunity opp, List<UserAccountTeamMember> uatmList) {
        System.debug('createAccountTeamInstances');
        List<AccountTeamMember> accountTeamMembers = new List<AccountTeamMember>();

        for (UserAccountTeamMember uatm : uatmList) {
            AccountTeamMember atm = new AccountTeamMember();
            atm.UserId = uatm.UserId;
            atm.AccountId = opp.AccountId;
            atm.TeamMemberRole = uatm.TeamMemberRole;
            atm.AccountAccessLevel = uatm.AccountAccessLevel;
            atm.OpportunityAccessLevel = uatm.OpportunityAccessLevel;
            atm.CaseAccessLevel = uatm.CaseAccessLevel;
            
            accountTeamMembers.add(atm);
        }

        return accountTeamMembers;
    }

    private Set<Id> getUserIdSet(Map<Id, Opportunity> idToOpp, Map<Id, Form__c> oppIdToM0Form) {
        Set<Id> userIdSet = new Set<Id>();
        
        for (Opportunity opp : idToOpp.values()) {
            Form__c m0 = oppIdToM0Form.get(opp.Id);

            if ((opp.RecordTypeId == clientNewBusinessRecType && m0 != null && m0.SMB_CS_success_executive__c == true && opp.Responsavel_Gympass_Relacionamento__c != null) ||
                (opp.RecordTypeId == smbNewBusinessRecType && opp.Responsavel_Gympass_Relacionamento__c != null)) {
                userIdSet.add(opp.Responsavel_Gympass_Relacionamento__c);
            }
        }

        return userIdSet;
    }

    private Map<Id, List<UserAccountTeamMember>> getUserAccountTeamMembersAsMap(Set<Id> userIdSet) {
        Map<Id, List<UserAccountTeamMember>> ownerIdToUserAccountTeamMemberList = new Map<Id, List<UserAccountTeamMember>>();
        List<UserAccountTeamMember> userAccTeamMemberList = getUserAccountTeamMembersByOwnerId(userIdSet);

        for (UserAccountTeamMember uatm : userAccTeamMemberList) {
            if (ownerIdToUserAccountTeamMemberList.containsKey(uatm.OwnerId))
                ownerIdToUserAccountTeamMemberList.get(uatm.OwnerId).add(uatm);
            else
                ownerIdToUserAccountTeamMemberList.put(uatm.OwnerId, new List<userAccountTeamMember>{uatm});
        }

        return ownerIdToUserAccountTeamMemberList;
    }

    private List<UserAccountTeamMember> getUserAccountTeamMembersByOwnerId(Set<Id> userIdSet) {
        List<UserAccountTeamMember> userAccTeamMemberList = [
            SELECT Id, TeamMemberRole, UserId, OwnerId, User.Name, AccountAccessLevel, OpportunityAccessLevel, CaseAccessLevel
            FROM UserAccountTeamMember
            WHERE OwnerId IN :userIdSet
        ];

        return userAccTeamMemberList;
    }

    public void changeAccountOwnerToCsUser(Map<Id, Opportunity> oldIdToOpp, Map<Id, Opportunity> newIdToOpp) {
        System.debug('assignUserAccountTeamMembersOnLaunch');
        Map<Id, Opportunity> accIdToWonNewBusinessOpp = new Map<Id, Opportunity>();
        
        for (Opportunity newOpp : newIdToOpp.values()) {
            Opportunity oldOpp = oldIdToOpp.get(newOpp.Id);
            
            if ((newOpp.RecordTypeId == clientNewBusinessRecType || newOpp.RecordTypeId == smbNewBusinessRecType) && 
                    oldOpp.StageName != 'Lançado/Ganho' && 
                    newOpp.StageName == 'Lançado/Ganho' &&
                    newOpp.Responsavel_Gympass_Relacionamento__c != null)

                    accIdToWonNewBusinessOpp.put(newOpp.AccountId, newOpp);
        }
        
        if (accIdToWonNewBusinessOpp.size() > 0)
            assignNewAccountOwner(accIdToWonNewBusinessOpp);
        else
            return;
    }

    private void assignNewAccountOwner(Map<Id, Opportunity> accIdToWonNewBusinessOpp) {
        List<Account> accounts = new AccountRepository().getAccounts(accIdToWonNewBusinessOpp.keySet());
        List<Account> accountsToUpdate = new List<Account>();
        
        for (Account acc : accounts) {
            Opportunity opp = accIdToWonNewBusinessOpp.get(acc.Id);

            if (opp != null) {
                acc.OwnerId = opp.Responsavel_Gympass_Relacionamento__c;
                accountsToUpdate.add(acc);
            }
        }

        update accounts;
    }
}