public with sharing class SplitBillingController {
    
    private class SplitBillingControllerException extends Exception{}
    
    @AuraEnabled
    public static Map<String, List<PaymentInfo>> getAccountHierarchy(String oppId){
        
        try {
            if(!String.isBlank(oppId)) {
                AccountRepository accounts = new AccountRepository(); 
                OpportunityRepository opportunities = new OpportunityRepository();
                PaymentRepository payments = new PaymentRepository(); 
                
                Opportunity opp = opportunities.byId(oppId);   
                Set<Id> childAccounts = accounts.findChildAccounts(opp.AccountId);

                Set<Account> allAccountsInHierarchy = new Set<Account>(accounts.getAccounts(childAccounts));
                System.debug(allAccountsInHierarchy);
                
                Map<Id, Payment__c> paymentsRecAccMap = payments.getAccPaymentsMapForOpp(oppId, payments.getRecordTypeIdByDeveloperName('Recurring')); 

                Map<Id, Payment__c> paymentsNonRecAccMap = payments.getAccPaymentsMapForOpp(oppId, payments.getRecordTypeIdByDeveloperName('Non_Recurring')); 
                List<Account> accountWithPaymentsRec = new List<Account>(accounts.getAccounts(paymentsRecAccMap.keyset()));
               
                Set<Account> accountNotAvailable = new Set<Account>();
                for (Integer i = 0; i<accountWithPaymentsRec.size() ; i ++){
                    if(accountWithPaymentsRec.get(i).Id == opp.AccountId){
                        accountNotAvailable.add(accountWithPaymentsRec.get(i));
                        accountWithPaymentsRec.remove(i);
                    }
                }
                accountNotAvailable.addAll(accountWithPaymentsRec);

                if(accountNotAvailable.size()>0)
                    allAccountsInHierarchy.removeAll(accountNotAvailable);                
                
                List<PaymentInfo> allPaymInfoInHierarchyList = new List<PaymentInfo>();
                List<PaymentInfo> allPaymInfoWithPaymentList = new List<PaymentInfo>();
                Map<String,List<PaymentInfo>> returnMap = new Map<String,List<PaymentInfo>>();
                System.debug('### GOT HERE ###');
                for(Account acc : allAccountsInHierarchy){
                    System.debug('### acc (1) ###');
                    System.debug(acc);
                    PaymentInfo paym = new PaymentInfo();
                    paym.AccountId = acc.Id;
                    paym.AccountName = acc.Name;
                    paym.AccountBillingCity = acc.BillingCity;
                    paym.AccountBillingState = acc.BillingState;
                    paym.AccountBillingCountry = acc.BillingCountry;
                    paym.AccountLegalName = acc.Razao_Social__c;
                    paym.AccountLegalNumber = acc.Id_Company__c;
                    paym.nonRecurringFeePercentage = 0;
                    paym.recurringFeePercentage = 0;
                    allPaymInfoInHierarchyList.add(paym);  
                }
                
                for(Account acc : accountNotAvailable){
                    PaymentInfo paym = new PaymentInfo();
                    System.debug('### acc (2) ###');
                    System.debug(acc);
                    System.debug('###' + acc.Razao_Social__c);
                    System.debug('###' + acc.Id_Company__c);
                    paym.AccountId = acc.Id;
                    paym.AccountName = acc.Name;
                    paym.AccountBillingCity = acc.BillingCity;
                    paym.AccountBillingState = acc.BillingState;
                    paym.AccountBillingCountry = acc.BillingCountry;
                    paym.AccountLegalName = acc.Razao_Social__c;
                    paym.AccountLegalNumber = acc.Id_Company__c;
                    paym.nonRecurringFeePercentage = paymentsNonRecAccMap.containsKey(acc.Id) ? paymentsNonRecAccMap.get(acc.Id).Percentage__c : 0;
                    paym.recurringFeePercentage = paymentsRecAccMap.containsKey(acc.Id) ? paymentsRecAccMap.get(acc.Id).Percentage__c : 0;
                    allPaymInfoWithPaymentList.add(paym);  
                }
                
                returnMap.put('allPaymInfoInHierarchyList',allPaymInfoInHierarchyList);
                returnMap.put('allPaymInfoWithPaymentList',allPaymInfoWithPaymentList);
                return returnMap;
            }
            else{
                throw new SplitBillingControllerException('The oppId is blank or null'); 
            }
        }
        catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
    
    
    @AuraEnabled
    public static void saveAccountWithOpportunity(ID oppId, List<PaymentInfo> PaymentInfoList){
        
        if(!String.isBlank(oppId) && PaymentInfoList.size()>0){
            PaymentRepository payments = new PaymentRepository(); 
            EligibilityRepository eligibilities = new EligibilityRepository();
            OpportunityRepository opportunities = new OpportunityRepository();
            Opportunity opp = opportunities.byId(oppId); 

            List<Account> accountsToUpdate = new List<Account>();
            List<Payment__c> paymentsToUpsert = new List<Payment__c>();
            List<Payment__c> paymentsToAddEligibility = new List<Payment__c>();
            
            Payment__c newRecPay;
            
            Id PaymentRt_Recurring = payments.getRecordTypeIdByDeveloperName('Recurring');
            Id PaymentRt_NonRecurring = payments.getRecordTypeIdByDeveloperName('Non_Recurring');
            
            
            Map<Id, Payment__c> paymentsRecAccMap = payments.getAccPaymentsMapForOpp(oppId, PaymentRt_Recurring); 
            Map<Id, Payment__c> paymentsNonRecAccMap = payments.getAccPaymentsMapForOpp(oppId, PaymentRt_NonRecurring);  
            
            List<Eligibility__c> eligibilityList =  new List<Eligibility__c>();
            List<Eligibility__c> eligibilityToUpsert = new List<Eligibility__c>();
            
            if(paymentsRecAccMap.containsKey(opp.AccountId))
                eligibilityList = eligibilities.getEligibilitiesByPaymentId(new List<Id>{paymentsRecAccMap.get(opp.AccountId).Id});
            
            for(PaymentInfo pay : PaymentInfoList){

                if(pay.AccountId != null){
                    Account acc = new Account(Id = pay.AccountId,Razao_Social__c=pay.AccountLegalName ,Id_Company__c= pay.AccountLegalNumber );
                    accountsToUpdate.add(acc);
                }
                 

                if(paymentsRecAccMap.containsKey(pay.AccountId)){
                    newRecPay = paymentsRecAccMap.get(pay.AccountId);
                    
                }
                else{
                    newRecPay = new Payment__c();
                    newRecPay.Opportunity__c = oppId;
                    newRecPay.Account__c = pay.AccountId;
                    newRecPay.RecordTypeId = PaymentRt_Recurring;
                    paymentsToAddEligibility.add(newRecPay);
                }
                
                newRecPay.Percentage__c = pay.recurringFeePercentage;
                paymentsToUpsert.add(newRecPay);  
                if(paymentsNonRecAccMap.containsKey(opp.AccountId)){
                    if(paymentsNonRecAccMap.containsKey(pay.AccountId)){
                        newRecPay = paymentsNonRecAccMap.get(pay.AccountId);
                    }
                    else{
                        newRecPay = new Payment__c();
                        newRecPay.Opportunity__c = oppId;
                        newRecPay.Account__c = pay.AccountId;
                        newRecPay.RecordTypeId = PaymentRt_NonRecurring;
                    }
                    
                    newRecPay.Percentage__c = pay.nonRecurringFeePercentage;
                    paymentsToUpsert.add(newRecPay);  
                }
                
            }

            try{
                Database.upsert( paymentsToUpsert, Payment__c.Id );             
            }catch(System.DmlException e){
                throw new SplitBillingControllerException('Can not save Payments. Error: ' + e.getMessage() );        
            }

            for(Payment__c paym : paymentsToAddEligibility){
                for(Eligibility__c eli : eligibilityList){
                    Eligibility__c newEli = eli.clone(false, true, false, false);
                    newEli.Managed_By__c = paym.Account__c;
                    newEli.Payment__c = paym.Id;
                    eligibilityToUpsert.add(newEli);
                } 
            }
        
            try{
                Database.insert(eligibilityToUpsert);             
            }catch(System.DmlException e){
                throw new SplitBillingControllerException('Can not save Eligibilities. Error: ' + e.getMessage() );        
            }
            
            try{
                Database.update(accountsToUpdate );             
            }catch(System.DmlException e){
                throw new SplitBillingControllerException('Can not update Accounts. Error: ' + e.getMessage() );        
            }
        }
        else{
            throw new SplitBillingControllerException('Entry data are blank or null'); 
        }
        
    }
    
    @AuraEnabled
    public static void removeAccountFromOpportunity(ID oppId, List<PaymentInfo> PaymentInfoList){
        
        PaymentRepository payments = new PaymentRepository(); 
        EligibilityRepository eligibilities = new EligibilityRepository();
        Id PaymentRt_Recurring = payments.getRecordTypeIdByDeveloperName('Recurring');
        Id PaymentRt_NonRecurring = payments.getRecordTypeIdByDeveloperName('Non_Recurring');
        
        Map<Id, Payment__c> paymentsRecAccMap = payments.getAccPaymentsMapForOpp(oppId, PaymentRt_Recurring); 
        Map<Id, Payment__c> paymentsNonRecAccMap = payments.getAccPaymentsMapForOpp(oppId, PaymentRt_NonRecurring); 
        
        List<Payment__c> PaymentsToDelete = new List<Payment__c>();
        
        for(PaymentInfo paym : PaymentInfoList){
            if(paymentsRecAccMap.containsKey(paym.AccountId)){
                PaymentsToDelete.add(paymentsRecAccMap.get(paym.AccountId));
            }
            if(paymentsNonRecAccMap.containsKey(paym.AccountId)){
                PaymentsToDelete.add(paymentsNonRecAccMap.get(paym.AccountId));
            }
        }
        
        List<Eligibility__c> EligibilityToDelete = eligibilities.getEligibilitiesByPaymentId(new List<Id>((new Map<Id,Payment__c>(PaymentsToDelete)).keySet()));
        
        try{
            Database.delete(EligibilityToDelete); 
            Database.delete(PaymentsToDelete);             
        }catch(System.DmlException e){
            throw new SplitBillingControllerException('Can not delete records. Error: ' + e.getMessage() );        
        }
        
    }
    
    public class PaymentInfo{
        @AuraEnabled public String AccountName {get; set;}
        @AuraEnabled public String AccountId {get; set;}
        @AuraEnabled public String AccountLegalName {get; set;}
        @AuraEnabled public String AccountLegalNumber {get; set;}
        @AuraEnabled public String AccountBillingCity {get; set;}
        @AuraEnabled public String AccountBillingState {get; set;}
        @AuraEnabled public String AccountBillingCountry {get; set;}
        @AuraEnabled public Decimal recurringFeePercentage {get; set;}
        @AuraEnabled public Decimal nonRecurringFeePercentage {get; set;}
    }
    
    
}