@isTest
public class TagusIntegrationControllerTest {
	
    
    @isTest
    public static void testIntegrationRequestForAccount() {
    	Account acc = new Account();
        acc.Name = 'Test Account';
        acc.Id_Company__c = '92.855.257/0001-43';
        acc.Site = 'testaccount@test.com';
        acc.BillingCountry = 'Brazil';
        acc.BillingState = 'São Paulo';
        insert acc;
        
        List<Id> idList = new List<Id>{acc.Id};
        
        TagusIntegrationController.startIntegrationRequestCreation(idList, 'Test');
        
        List<Integration_Request__c> integrationRequestList = [
        	SELECT Id, Name
            FROM Integration_Request__c
        ];
        
        System.debug(JSON.serialize(integrationRequestList));
    }
	    
	
    @isTest
    public static void testIntegrationRequestForSalesOrder() {
    	Account acc = new Account();
        acc.Name = 'Test Account';
        acc.Id_Company__c = '92.855.257/0001-43';
        acc.Site = 'testaccount@test.com';
        acc.BillingCountry = 'Brazil';
        acc.BillingState = 'São Paulo';
        insert acc;
        
        Sales_Order__c so = new Sales_Order__c();
        so.Account__c = acc.Id;
        insert so;
        
        List<Sales_Order_Parameter__c> soParamList = new List<Sales_Order_Parameter__c>();
        for (Integer i=0; i<3; i++) {
        	Sales_Order_Parameter__c soParam = new Sales_Order_Parameter__c();
            soParam.Sales_Order__c = so.Id;
            soParam.Name = 'Param ' + i;
            soParam.Value__c = 'Value ' + i;
            soParamList.add(soParam);
        }
        List<Database.SaveResult> soParamsaveResult = Database.insert(soParamList);
        
        // SO Item
        List<Sales_Order_Item__c> soItemsParamList = new List<Sales_Order_Item__c>();
        for (Integer i=0; i<3; i++) {
       		Sales_Order_Item__c soParam = new Sales_Order_Item__c();
            soParam.Sales_Order__c = so.Id;
            soParam.Name = 'Item ' + i;
            soParam.UUID__c = '12345678867663461';
            
            soItemsParamList.add(soParam);
        }
        List<Database.SaveResult> soItemSR = Database.insert(soItemsParamList);
        
       	List<Sales_Order_Item_Parameter__c> soItemParams = new List<Sales_Order_Item_Parameter__c>();
        for (Integer i = 0; i < 3; i++) {
        	Sales_Order_Item_Parameter__c soItemParam = new Sales_Order_Item_Parameter__c();
            soItemParam.Sales_Order_Item__c = soItemSR.get(0).getId();
            soItemParam.Name = 'Sales Order Item Parameter ' + i;
            soItemParam.Value__c = 'Sales Order Item Parameter Value ' + i;
            soItemParams.add(soItemParam);
        }
        
        for (Integer i = 0; i < 3; i++) {
        	Sales_Order_Item_Parameter__c soItemParam = new Sales_Order_Item_Parameter__c();
            soItemParam.Sales_Order_Item__c = soItemSR.get(1).getId();
            soItemParam.Name = 'Sales Order Item Parameter ' + i;
            soItemParam.Value__c = 'Sales Order Item Parameter Value ' + i;
            soItemParams.add(soItemParam);
        }
        
        List<Database.SaveResult> soItemParamsSR = Database.insert(soItemParams);
        
        List<Id> idList = new List<Id>{so.Id};
        
        //TagusIntegrationController.startIntegrationRequestCreation(idList, 'Test');
        
       	IntegrationRequestCreationPlug.createIntegrationRequest(idList);
            
        List<Integration_Request__c> integrationRequestList = [
        	SELECT Id, Name
            FROM Integration_Request__c
        ];
        
        System.debug(JSON.serialize(integrationRequestList));
    }
}