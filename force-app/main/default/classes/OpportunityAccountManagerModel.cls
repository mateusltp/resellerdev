/**
 * @File Name          : OpportunityAccountManagerModel.cls
 * @Description        : 
 * @Author             : Samuel Silva - GFT (slml@gft.com)
 * @Group              : 
 * @Last Modified By   : Samuel Silva - GFT (slml@gft.com)
 * @Last Modified On   : 08-24-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    25/03/2020   Samuel Silva - GFT (slml@gft.com)     Initial Version
**/
public without sharing virtual class OpportunityAccountManagerModel {
    
    protected Set<Id> totalAccIDs = new Set<Id>();
    public String oppId;

    public OpportunityAccountManagerModel(String oppId){
        this.oppId = oppId;
    }

    public Object getRelatedAccounts(){        
        System.debug('Entrou relatedAccounts');         
        Opportunity currentOpp = [SELECT Id, AccountId, Name FROM Opportunity WHERE Id =: oppId];          
        totalAccIDs.add(currentOpp.accountId);       
        AccountHierarchy accHiearchyObj = new AccountHierarchy();
        accHiearchyObj.findlAllChildAccounts(totalAccIDs);
        totalAccIDs.clear();
        totalAccIDs.addAll(accHiearchyObj.getTotalAccounts());
        return findRelatedAccounts(); 
    }


    protected virtual Object findRelatedAccounts(){      
                
        List<Opportunity> oppInHierarchy = [SELECT Id, AccountId FROM Opportunity WHERE AccountId IN: totalAccIDs
                                           AND StageName != 'Lançado/Ganho' AND StageName != 'Perdido'];
        
        /* accs that shouldnt be related - oppt still in negotiation and accst related to other opp*/
        Set<Id> accsNotAvailable = new Set<Id>(); 
           
        if(oppInHierarchy.size()>0){
            for(Opportunity o : oppInHierarchy){
                accsNotAvailable.add(o.AccountId);
            }
        }       

        Set<Id> accsWithOpenOppRelatedSet = new Set<Id>();  

        List<Account_Opportunity_Relationship__c> accountsRelatedToOtherOpps = [SELECT Opportunity__c, Account__c FROM Account_Opportunity_Relationship__c WHERE (Account__c IN: totalAccIDs)];
        for(Account_Opportunity_Relationship__c aor : accountsRelatedToOtherOpps) {
            if(aor.Opportunity__c == oppId){
                accsWithOpenOppRelatedSet.add(aor.Account__c);
            } else {
                accsNotAvailable.add(aor.Account__c);
            }            
        }
        
        Set<Id> accsWithNoOppSet = new Set<Id>();  
        accsWithNoOppSet.addAll(totalAccIDs);   
             
        accsWithNoOppSet.removeAll(accsNotAvailable);        
        accsWithNoOppSet.removeAll(accsWithOpenOppRelatedSet);
        
        List<Account> accWithOppLst = [SELECT Id, Name, BillingCity, BillingState, BillingCountry, ShippingCity, ShippingState,ShippingCountry FROM Account WHERE Id IN: accsWithOpenOppRelatedSet];
        List<Account> accWithNoOppLst = [SELECT Id, Name,BillingCity, BillingState, BillingCountry, ShippingCity, ShippingState,ShippingCountry FROM Account WHERE Id IN: accsWithNoOppSet];
        Map<String, List<Account>> mapResult = new Map<String, List<Account>>();
        mapResult.put('AccWithOpp',accWithOppLst);
        mapResult.put('AccWithNoOpp',accWithNoOppLst);
        return mapResult;
    }

    public void setRelationshipAccountWithOpp(List<Account> accLst){       
        List<Account_Opportunity_Relationship__c> aorLst = new List<Account_Opportunity_Relationship__c>();
       for(Account acc : accLst){
            Account_Opportunity_Relationship__c aor = new Account_Opportunity_Relationship__c();
            aor.Opportunity__c = oppId;
            aor.Account__c = acc.Id;
            aorLst.add(aor);          
        }      
        
        Database.insert(aorLst);
    }
    
    public void breakRelationshipAccountWithOpp(List<Account> accLst){      
       List<Account_Opportunity_Relationship__c> aorLst = new List<Account_Opportunity_Relationship__c>();
       Set<Id> accIdSet = new Set<Id>();
       for(Account acc : accLst){           
           accIdSet.add(acc.Id);
       }
       aorLst = [SELECT Id FROM Account_Opportunity_Relationship__c WHERE Account__c IN: accIdSet AND Opportunity__c =: oppId];
       if(aorLst.size() > 0){           
            Database.delete(aorLst);
       }    
    }   
}