/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 07-12-2022
 * @last modified by  : alysson.mota@gympass.com
**/
public with sharing class ProductItemPartnerServiceImpl implements IProductItemService{

    private static final PS_Constants constants = PS_Constants.getInstance();
    private static final String className = ProductItemPartnerServiceImpl.class.getName();

    public static Product_Item__c createNewProduct (Id opportunityId, Id accountId, Product_Item__c prodItem, Set<Id> gymActivities, Map<String, List<Commercial_Condition__c>> commercialConditionsByDML, List<Threshold__c> thresholds ){
        fflib_ISObjectUnitOfWork uow = Application.UnitOfWork.newInstance();
   
        try{
            /**
             * Bruno - 02-01-2022 - IT-2181 - check if prodItem already has an Id in order o insert or update the product
            */
            if (prodItem.Id != null) {
                uow.registerDirty(prodItem);
            } else {
                prodItem.Opportunity__c = opportunityId;
                uow.registerNew(prodItem);
            }

            
            if( !gymActivities.isEmpty() ){
              registerGymActivitiesToBeBookedToDelete(prodItem, gymActivities, uow);
              registerGymActivitiesForProductItem( prodItem, gymActivities , uow );
            }
            
            if( !commercialConditionsByDML.isEmpty() ){
                registerCommercialConditions( opportunityId, accountId, prodItem, commercialConditionsByDML, uow );
            }

            if( !thresholds.isEmpty() ){
                registerThresholds( prodItem, thresholds, uow );
            }

            uow.commitWork();
            return prodItem;
        } catch (Exception e){
          /*DebugLog__c log = new DebugLog__c(
                  Origin__c = '['+className+'][createNewProduct]',
                  LogType__c = constants.LOGTYPE_ERROR,
                  RelatedToSquad__c = constants.LOGTYPE_SQUAD_PARTNERS,
                  ExceptionMessage__c = e.getMessage(),
                  StackTrace__c = e.getStackTraceString(),
                  DataScope__c = JSON.serialize(prodItem));
          Logger.createLog(log);*/
          throw new ProductItemPartnerServiceImplException(e.getMessage());
        }
    }

    /**
     * @description       : IT-2181 - Edit button product layout. Delete prior existing relationships before creating new ones
     * @author            : bruno.mendes@gympass.com
     * @group             :
     * @last modified on  : 07-12-2022
     * @last modified by  : alysson.mota@gympass.com
    **/
    public static Map<String, List<Commercial_Condition__c>> deleteProductRelationships(Id productId, List<Commercial_Condition__c> commercialConditions) {
        fflib_ISObjectUnitOfWork uow = Application.UnitOfWork.newInstance();

        // create selectors
        ProductActivityRelationshipSelector productActivityRelationshipSelector = new ProductActivityRelationshipSelector();
        ProductAssignmentSelector productAssignmentSelector = new ProductAssignmentSelector();
        ProductOpportunityAssignmentSelector productOpportunityAssignmentSelector = new ProductOpportunityAssignmentSelector();
        CommercialConditionSelector commercialConditionSelector = new CommercialConditionSelector();
        ProductThresholdMemberSelector productThresholdMemberSelector = new ProductThresholdMemberSelector();
        ThresholdSelector thresholdSelector = new ThresholdSelector();

        // delete previously created related records if they exist
        try {
            Map<Id, Product_Assignment__c> productAssignmentsByIds = new Map<Id, Product_Assignment__c>(productAssignmentSelector.selectCommercialConditionByProductId(new Set<Id> { productId }));
            Set<Id> commercialConditionsToDelete = new Set<Id>();
            Map<Id, Product_Assignment__c> productAssignmentsToDelete = new Map<Id, Product_Assignment__c>();
            Map<Id, Commercial_Condition__c> newCommercialConditionsByIds = new Map<Id, Commercial_Condition__c>();
            Map<Id, Id> existingCommercialConditionIdsByRecordTypeIds = new Map<Id, Id>();
            for (Commercial_Condition__c cc : commercialConditions) {
                newCommercialConditionsByIds.put(cc.RecordTypeId, cc);
            }
            Map<String, List<Commercial_Condition__c>> commercialConditionByDML = new Map<String, List<Commercial_Condition__c>>();
            commercialConditionByDML.put('INSERT', new List<Commercial_Condition__c>());
            commercialConditionByDML.put('UPDATE', new List<Commercial_Condition__c>());

            // filter commercial conditions that no longer apply and should be deleted
            for (Product_Assignment__c pa : productAssignmentsByIds.values()) {
                if (!newCommercialConditionsByIds.keySet().contains(pa.CommercialConditionId__r.RecordTypeId)) {
                    commercialConditionsToDelete.add(pa.CommercialConditionId__c);
                    productAssignmentsToDelete.put(pa.Id, pa);
                } else {
                    existingCommercialConditionIdsByRecordTypeIds.put(pa.CommercialConditionId__r.RecordTypeId, pa.CommercialConditionId__c);
                }
            }

            // split new commercial conditions that need to be inserted from existing commercial conditions that should be updated
            for (Id newCCRecordTypeId : newCommercialConditionsByIds.keySet()) {
                if (existingCommercialConditionIdsByRecordTypeIds.containsKey(newCCRecordTypeId)) {
                    Commercial_Condition__c newCC = newCommercialConditionsByIds.get(newCCRecordTypeId);
                    newCC.Id = existingCommercialConditionIdsByRecordTypeIds.get(newCCRecordTypeId);
                    commercialConditionByDML.get('UPDATE').add(newCommercialConditionsByIds.get(newCCRecordTypeId));
                } else {
                    commercialConditionByDML.get('INSERT').add(newCommercialConditionsByIds.get(newCCRecordTypeId));
                }
            }

            // register records to be deleted
            // product activities
            uow.registerDeleted(productActivityRelationshipSelector.selectActivityByProductId(new Set<Id> {productId}));
            // product opportunity assignments
            uow.registerDeleted(productOpportunityAssignmentSelector.selectByProductAssignmentIds(productAssignmentsToDelete.keySet()));
            // commercial conditions
            uow.registerDeleted(commercialConditionSelector.selectById(commercialConditionsToDelete));
            // product assignments
            uow.registerDeleted(productAssignmentsToDelete.values());
            Set<Id> thresholdIds = new Set<Id>();
            Map<Id, Product_Threshold_Member__c> productThresholdMembersByIds = new Map<Id, Product_Threshold_Member__c>(productThresholdMemberSelector.selectThresholdMapByProductId(new Set<Id> {productId}));
            for (Product_Threshold_Member__c ptm : productThresholdMembersByIds.values()) {
                thresholdIds.add(ptm.Threshold__c);
            }
            // thresholds
            uow.registerDeleted(thresholdSelector.selectById(thresholdIds));
            // threshold members
            uow.registerDeleted(productThresholdMembersByIds.values());
            uow.commitWork();
            return commercialConditionByDML;
        } catch (Exception e) {
            DebugLog__c log = new DebugLog__c(
                    Origin__c = '['+className+'][deleteProductRelationships]',
                    LogType__c = constants.LOGTYPE_ERROR,
                    RelatedToSquad__c = constants.LOGTYPE_SQUAD_PARTNERS,
                    ExceptionMessage__c = e.getMessage(),
                    StackTrace__c = e.getStackTraceString(),
                    ObjectId__c = productId);
            Logger.createLog(log);
            throw new ProductItemPartnerServiceImplException(e.getMessage());
        }

    }

    private static void registerGymActivitiesForProductItem ( Product_Item__c prodItem, Set<Id> gymActivities , fflib_ISObjectUnitOfWork uow ) {
        System.debug('registerGymActivitiesForProductItem');
        Id prodActRt =  Schema.SObjectType.Product_Activity_Relationship__c.getRecordTypeInfosByDeveloperName().get(constants.PRODUCT_ACTIVITY_RT_PARTNER_FLOW).getRecordTypeId();
        for( Id iGymAct :  gymActivities ){
            Product_Activity_Relationship__c prodActRel = new Product_Activity_Relationship__c();
            prodActRel.Gym_Activity_Relationship__c = iGymAct;
            uow.registerNew( prodActRel, Product_Activity_Relationship__c.Product__c, prodItem );
        }
    }

    /**
    * IT-2889
    * @author alysson.mota@gympass.com | 07-06-2022 
    * @param prodItem 
    * @param gymActivities 
    * @param uow 
    **/
    private static void registerGymActivitiesToBeBookedToDelete(Product_Item__c prodItem, Set<Id> gymActivities, fflib_ISObjectUnitOfWork uow) {
        GymActivityToBeBookedSelector gymBookedSelector = new GymActivityToBeBookedSelector();
        
        List<Gym_Activity_To_Be_Booked__c> gymActivitiesToBeBooked = new List<Gym_Activity_To_Be_Booked__c>();
        List<Gym_Activity_To_Be_Booked__c> gymActivitiesToBeBookedToDelete = new List<Gym_Activity_To_Be_Booked__c>();
        
        if (prodItem.Id != null) {
            gymActivitiesToBeBooked = gymBookedSelector.byProdId(new Set<Id>{prodItem.Id});
        }

        for (Gym_Activity_To_Be_Booked__c gymBooked : gymActivitiesToBeBooked) {
            if (!gymActivities.contains(gymBooked.GymActivityRelationshipId__c)) {
                gymActivitiesToBeBookedToDelete.add(gymBooked);
            }
        }

        uow.registerDeleted(gymActivitiesToBeBookedToDelete);
    }
    
    private static void registerCommercialConditions ( Id oppId, Id accId, Product_Item__c prodItem, Map<String, List<Commercial_Condition__c>> commercialConditionsByDML , fflib_ISObjectUnitOfWork uow  ) {
        System.debug('registerCommercialConditions');
        Id prodAssignRt =  Schema.SObjectType.Product_Assignment__c.getRecordTypeInfosByDeveloperName().get(constants.PRODUCT_ASSIGNMEN_RT_PARTNER_FLOW).getRecordTypeId();
        Account_Opportunity_Relationship__c oppMember = registerOpportunityMember(oppId, accId, uow );
        if (commercialConditionsByDML.containsKey('INSERT')) {
            for( Commercial_Condition__c newCC :  commercialConditionsByDML.get('INSERT') ){
                System.debug('Commercial Condition INSERT: '+json.serialize(newCC));
                uow.registerNew(newCC);
                Product_Assignment__c prodAssign = new Product_Assignment__c();
                prodAssign.RecordTypeId = prodAssignRt;
                uow.registerNew( prodAssign, Product_Assignment__c.CommercialConditionId__c, newCC );
                uow.registerRelationship( prodAssign, Product_Assignment__c.ProductId__c, prodItem );
                registerProductOpportunityAssignment(prodAssign, oppMember, uow);
            }
        }

        if (commercialConditionsByDML.containsKey('UPDATE'))
        {
            for (Commercial_Condition__c existingCC : commercialConditionsByDML.get('UPDATE'))
            {
                System.debug('Commercial Condition UPDATE: ' + json.serialize(existingCC));
                uow.registerDirty(existingCC);
            }
        }
    }


    private static void registerThresholds ( Product_Item__c prodItem, List<Threshold__c> thresholds , fflib_ISObjectUnitOfWork uow ) {
        System.debug('registerThresholds');
        Id threholdMemberRt =  Schema.SObjectType.Product_Threshold_Member__c.getRecordTypeInfosByDeveloperName().get(constants.PRODUCT_THRESHOLD_MEMBER_RT_PARTNER_FLOW).getRecordTypeId();       
        for( Threshold__c threhold :  thresholds ){
            threhold.Id = null;
            uow.registerNew(threhold);
            Product_Threshold_Member__c thrMember = new Product_Threshold_Member__c();
            thrMember.RecordTypeId = threholdMemberRt;
            uow.registerNew( thrMember, Product_Threshold_Member__c.Threshold__c, threhold );
            uow.registerRelationship( thrMember, Product_Threshold_Member__c.Product__c, prodItem );
        }
    }


    private static void registerProductOpportunityAssignment ( Product_Assignment__c prodAssign, Account_Opportunity_Relationship__c accOppMember , fflib_ISObjectUnitOfWork uow ) {
        System.debug('registerProductOpportunityAssignment');
        Id prodOppAssignRT =  Schema.SObjectType.Product_Opportunity_Assignment__c.getRecordTypeInfosByDeveloperName().get(constants.PRODUCT_OPPORTUNITY_ASSIGNMEN_RT_PARTNER_FLOW).getRecordTypeId();       
        Product_Opportunity_Assignment__c prodOppAssign = new Product_Opportunity_Assignment__c();
        prodOppAssign.RecordTypeId = prodOppAssignRT;
        uow.registerNew( prodOppAssign, Product_Opportunity_Assignment__c.OpportunityMemberId__c, accOppMember );
        uow.registerRelationship( prodOppAssign, Product_Opportunity_Assignment__c.ProductAssignmentId__c, prodAssign );
        system.debug('cheguei ao fim');
    }

    private static Account_Opportunity_Relationship__c registerOpportunityMember( Id oppId, Id accId , fflib_ISObjectUnitOfWork uow ) {
        AccountOpportunitySelector accOppSelector = (AccountOpportunitySelector)Application.Selector.newInstance(Account_Opportunity_Relationship__c.sobjectType);
        Account_Opportunity_Relationship__c oppMember = new Account_Opportunity_Relationship__c();
        Map<Id, Account_Opportunity_Relationship__c> oppMemberMap = new  Map<Id, Account_Opportunity_Relationship__c>();
        oppMemberMap = accOppSelector.selectExistingOpportunityMemberByAccountId( new Set<Id>{accId} );
        if( !oppMemberMap.isEmpty()){
            for(Account_Opportunity_Relationship__c iOppMember : oppMemberMap.values()){
                if(iOppMember.Opportunity__c == oppId){
                    oppMember = iOppMember;
                }
            }
        }
        
        if( oppMember.Id == null ){
            Id oppMemberRt = Schema.SObjectType.Account_Opportunity_Relationship__c.getRecordTypeInfosByDeveloperName().get(constants.OPPORTUNITY_MEMBER_RT_PARTNER_FLOW).getRecordTypeId();       
            oppMember.recordTypeId = oppMemberRt;
            oppMember.Opportunity__c = oppId;
            oppMember.Account__c = accId;
            uow.registerNew(oppMember);
        }

        return oppMember;
    }
    
    /**
    * @description 
    * @author alysson.mota@gympass.com | 05-13-2022 
    * @param prodItemLst 
    **/
    public void deleteProductsAndRelationships(List<Product_Item__c> prodItemLst) {
        Set<Id> prodIdSet = new Set<Id>();
        Set<Id> commConditionIdSet = new Set<Id>();
        Set<Id> prodAssignmentIdSet = new Set<Id>();
        Set<Id> thresholdIdSet = new Set<Id>();

        ProductOpportunityAssignmentSelector poaSelector = new ProductOpportunityAssignmentSelector();
        ProductItemSelector prodSelector = new ProductItemSelector();
        ProductAssignmentSelector paSelector = new ProductAssignmentSelector();
        CommercialConditionSelector commCondSelector = new CommercialConditionSelector();
        ProductThresholdMemberSelector ptmSelector = new ProductThresholdMemberSelector();
        ThresholdSelector threshSelector = new ThresholdSelector();
        ProductActivityRelationshipSelector parSelector = new ProductActivityRelationshipSelector();

        Map<Id, Product_Opportunity_Assignment__c> poaMap = new Map<Id, Product_Opportunity_Assignment__c>();

        fflib_ISObjectUnitOfWork uow = Application.UnitOfWork.newInstance();

        for (Product_Item__c prodItem : prodItemLst) {
            prodIdSet.add(prodItem.Id);
        }

        poaMap.putAll(poaSelector.selectProductIdWithCommConditions(prodIdSet));
        poaMap.putAll(poaSelector.selectByParentProductIdSet(prodIdSet));
        
        for (Product_Opportunity_Assignment__c poa : poaMap.values()) {
            commConditionIdSet.add(poa.ProductAssignmentId__r.CommercialConditionId__c);
            prodAssignmentIdSet.add(poa.ProductAssignmentId__c);
            prodIdSet.add(poa.ProductAssignmentId__r.ProductId__c);
        }

        List<Product_Item__c> prodLst = prodSelector.selectAllByParentId(prodIdSet);
        List<Product_Assignment__c> paLst = paSelector.selectProductAssignmentByID(prodAssignmentIdSet);
        List<Commercial_Condition__c> commCondLst = commCondSelector.selectCommConditionByID(commConditionIdSet);
        List<Product_Threshold_Member__c> ptmLst = ptmSelector.selectThresholdByProductId(prodIdSet);
        List<Product_Activity_Relationship__c> parLst = parSelector.selectActivityByProductId(prodIdSet);

        for (Product_Threshold_Member__c ptm : ptmLst) {
            thresholdIdSet.add(ptm.Threshold__c);
        }

        List<Threshold__c> threshLst = threshSelector.selectById(thresholdIdSet);

        uow.registerDeleted(commCondLst);
        uow.registerDeleted(paLst);
        uow.registerDeleted(prodLst);
        uow.registerDeleted(poaMap.values());
        uow.registerDeleted(threshLst);

        uow.commitWork();
    }

    public class ProductItemPartnerServiceImplException extends Exception {} 	

}