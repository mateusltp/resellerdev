/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 05-17-2022
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/
public with sharing class ProductAssignmentSelector extends ApplicationSelector {

    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
            Product_Assignment__c.Id,
            Product_Assignment__c.Name
        };
    }

    public Schema.SObjectType getSObjectType() {
        return Product_Assignment__c.sObjectType;
    }

    public List<Product_Assignment__c> selectById(Set<Id> ids) {
        return (List<Product_Assignment__c>) super.selectSObjectsById(ids);
    }

    public List<Product_Assignment__c> selectProductAssignmentByID(Set<Id> ids) {
        return new List<Product_Assignment__c>( (List<Product_Assignment__c>)Database.query(
                                        newQueryFactory().
                                        selectField(Product_Assignment__c.Id).
                                        selectField(Product_Assignment__c.CommercialConditionId__c).
                                        selectField(Product_Assignment__c.ProductId__c).
                                        selectField(Product_Assignment__c.CurrencyIsoCode).
                                        selectField(Product_Assignment__c.Market_Price__c).
                                        selectField(Product_Assignment__c.Cap_Value__c).
                                        selectField(Product_Assignment__c.Net_Transfer_Price__c).
                                        selectField(Product_Assignment__c.Late_Cancel__c).
                                        selectField(Product_Assignment__c.No_Show_Fee__c).
                                        selectField('CommercialConditionId__r.RecordTypeId').
                                        setCondition('Id in: ids').
                                        toSOQL()
                                        ));									
    }

    public List<Product_Assignment__c> selectCapByParentProdId(Set<Id> parentProdIdSet) {
        return new List<Product_Assignment__c>( (List<Product_Assignment__c>)Database.query(
                                        newQueryFactory().
                                        selectField(Product_Assignment__c.Id).
                                        selectField(Product_Assignment__c.CommercialConditionId__c).
                                        selectField(Product_Assignment__c.ProductId__c).
                                        selectField(Product_Assignment__c.CurrencyIsoCode).
                                        selectField(Product_Assignment__c.Market_Price__c).
                                        selectField(Product_Assignment__c.Cap_Value__c).
                                        selectField(Product_Assignment__c.Net_Transfer_Price__c).
                                        selectField(Product_Assignment__c.Late_Cancel__c).
                                        selectField(Product_Assignment__c.No_Show_Fee__c).
                                        selectField('CommercialConditionId__r.RecordTypeId').
                                        selectField('ProductId__r.Parent_Product__c').
                                        setCondition('ProductId__r.Parent_Product__c IN :parentProdIdSet AND CommercialConditionId__r.RecordType.DeveloperName = \'CAP\'').
                                        toSOQL()
                                        ));									
    }

    public List<Product_Assignment__c> selectNonCapByParentProdId(Set<Id> parentProdIdSet) {
        return new List<Product_Assignment__c>( (List<Product_Assignment__c>)Database.query(
                                        newQueryFactory().
                                        selectField(Product_Assignment__c.Id).
                                        selectField(Product_Assignment__c.CommercialConditionId__c).
                                        selectField(Product_Assignment__c.ProductId__c).
                                        selectField(Product_Assignment__c.CurrencyIsoCode).
                                        selectField(Product_Assignment__c.Market_Price__c).
                                        selectField(Product_Assignment__c.Cap_Value__c).
                                        selectField(Product_Assignment__c.Net_Transfer_Price__c).
                                        selectField(Product_Assignment__c.Late_Cancel__c).
                                        selectField(Product_Assignment__c.No_Show_Fee__c).
                                        selectField('CommercialConditionId__r.RecordTypeId').
                                        selectField('ProductId__r.Parent_Product__c').
                                        setCondition('ProductId__r.Parent_Product__c IN :parentProdIdSet AND CommercialConditionId__r.RecordType.DeveloperName != \'CAP\'').
                                        toSOQL()
                                        ));									
    }

    public Map<Id, Product_Assignment__c> selectProductAssignmentByIdAsMap(Set<Id> ids) {
        Map<Id, Product_Assignment__c>	idToProdAssignment = new Map<Id, Product_Assignment__c>();
        List<Product_Assignment__c> prodAssignmentLst = selectProductAssignmentByID(ids);     

        for (Product_Assignment__c prodAssignment : prodAssignmentLst) {
            idToProdAssignment.put(prodAssignment.Id, prodAssignment);
        }

        return idToProdAssignment;
    }

    public Map<Id, Product_Assignment__c> selectCommercialConditionByProductId(Set<Id> productIds) {
        return new Map<Id, Product_Assignment__c>(( List<Product_Assignment__c> ) Database.query(
                newQueryFactory(true).
                        selectField(Product_Assignment__c.Id).
                        selectField(Product_Assignment__c.CommercialConditionId__c).
                       selectField(Product_Assignment__c.ProductId__c).
                        selectField(Product_Assignment__c.Market_Price__c). 
                        selectField('CommercialConditionId__r.RecordTypeId').
                        setCondition('ProductId__c IN :productIds').
                        toSOQL()
        ));
    }

    public Map<Id, Product_Assignment__c> selectNonCapByProdId(Set<Id> productIds) {
		Set<String> commConditionsRecTypeDevName = new Set<String>{'Late_Cancellation', 'No_Show'};
        
        return new Map<Id, Product_Assignment__c>(( List<Product_Assignment__c> ) Database.query(
                newQueryFactory(true).
                        selectField(Product_Assignment__c.Id).
                        selectField(Product_Assignment__c.CommercialConditionId__c).
                        selectField(Product_Assignment__c.ProductId__c).
                        selectField('CommercialConditionId__r.RecordTypeId').
                        setCondition('ProductId__c IN :productIds AND CommercialConditionId__r.RecordType.DeveloperName IN :commConditionsRecTypeDevName').
                        toSOQL()
        ));
    }

    public List<Product_Assignment__c> selectByCommConditionId(Set<Id> ids) {
        return new List<Product_Assignment__c>( (List<Product_Assignment__c>)Database.query(
                                        newQueryFactory().
                                        selectField(Product_Assignment__c.Id).
                                        selectField(Product_Assignment__c.CommercialConditionId__c).
                                        selectField(Product_Assignment__c.ProductId__c).
                                        selectField(Product_Assignment__c.CurrencyIsoCode).
                                        selectField(Product_Assignment__c.Market_Price__c).
                                        selectField(Product_Assignment__c.Cap_Value__c).
                                        selectField(Product_Assignment__c.Net_Transfer_Price__c).
                                        selectField(Product_Assignment__c.Late_Cancel__c).
                                        selectField(Product_Assignment__c.No_Show_Fee__c).
                                        selectField('CommercialConditionId__r.RecordTypeId').
                                        setCondition('CommercialConditionId__c in: ids').
                                        toSOQL()
                                        ));									
    }

    public List<Product_Assignment__c> selectAllByProductId(Set<Id> productIds) {
        return (List<Product_Assignment__c>) Database.query(
                newQueryFactory().
                        selectFields(getAllFieldsFromProductAssignment()).
                        setCondition('ProductId__c IN: productIds' ).
                        toSOQL());
    }

    public List<Product_Assignment__c> selectAllFromChildProductByParentIds(Set<Id> productIds) {
        return (List<Product_Assignment__c>) Database.query(
                newQueryFactory().
                        selectFields(getAllFieldsFromProductAssignment()).
                        selectField('ProductId__r.Parent_Product__r.Opportunity__c').
                        selectField('ProductId__r.Name').
                        setCondition('CommercialConditionId__r.RecordType.DeveloperName = \'CAP\' AND ProductId__r.Parent_Product__c IN: productIds' ).
                        toSOQL());
    }
    
    public Map<Id, Product_Assignment__c> selectAllFieldsFromProductAssignWithCap(Set<Id> productIds) {
        return new Map<Id, Product_Assignment__c>( (List<Product_Assignment__c>) Database.query(
                newQueryFactory().
                        selectFields(getAllFieldsFromProductAssignment()).
                        selectField(Product_Assignment__c.Market_Price__c).                        
                        selectField('ProductId__r.Parent_Product__r.Opportunity__c').
                        selectField('ProductId__r.Parent_Product__c').
                        selectField('ProductId__r.Name').
                        setCondition('CommercialConditionId__r.RecordType.DeveloperName = \'CAP\' AND ProductId__r.Parent_Product__c IN: productIds' ).
                        toSOQL()));
    }

    public Map<Id, Product_Assignment__c> selectAllFieldsFromProductAssignWithCapById(Set<Id> productIds) {
        return new Map<Id, Product_Assignment__c>( (List<Product_Assignment__c>) Database.query(
                newQueryFactory().
                        selectFields(getAllFieldsFromProductAssignment()).
                        selectField(Product_Assignment__c.Market_Price__c).                        
                        selectField('ProductId__r.Parent_Product__r.Opportunity__c').
                        selectField('ProductId__r.Parent_Product__c').
                        selectField('ProductId__r.Name').
                        setCondition('CommercialConditionId__r.RecordType.DeveloperName = \'CAP\' AND ProductId__c IN: productIds' ).
                        toSOQL()));
    }

    private Set<String> getAllFieldsFromProductAssignment(){
        Map<String, Schema.SObjectField> productFields = Schema.getGlobalDescribe().get('Product_Assignment__c').getDescribe().fields.getMap();
        return productFields.keySet();
    }
}