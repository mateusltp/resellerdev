/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 05-23-2022
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/
@isTest(seeAllData=false)
public class ProductAssignmentSelectorTest {  

    @TestSetup
    static void setupData(){
        String recordTypePartnerFlow = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Account').getRecordTypeId();
        Account partnerAcc = PartnerDataFactory.newAccount();
        partnerAcc.RecordTypeId = recordTypePartnerFlow;
        partnerAcc.CRM_ID__c = '1231';
        partnerAcc.UUID__c = new Uuid().getValue();
        INSERT partnerAcc;

        /* Single Partner Account*/ 
        Account singlePartnerAcc = PartnerDataFactory.newAccount();
        singlePartnerAcc.RecordTypeId = recordTypePartnerFlow;
        singlePartnerAcc.Partner_Level__c = 'Single Partner';
        singlePartnerAcc.Name = 'Single Partner';
        singlePartnerAcc.UUID__c = new Uuid().getValue();
        INSERT singlePartnerAcc;
             
        String recordTypePartnerContactFlow = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Contact').getRecordTypeId();
        Contact partnerContact = PartnerDataFactory.newContact(partnerAcc.Id);
        partnerContact.RecordTypeId = recordTypePartnerContactFlow;        
        partnerContact.Type_of_Contact__c = 'Decision Maker';
        partnerContact.UUID__c = new Uuid().getValue();
        INSERT partnerContact;      

        Opportunity aOpp = PartnerDataFactory.newOpportunity( partnerAcc.Id, 'Partner_Flow_Opportunity'); 
        aOpp.UUID__c = new Uuid().getValue();
        INSERT aOpp;

          /* Single Partner Opportunity */ 
        Opportunity aSingleOpp = PartnerDataFactory.newOpportunity( singlePartnerAcc.Id, 'Partner_Flow_Opportunity'); 
        aSingleOpp.UUID__c = new Uuid().getValue();
        INSERT aSingleOpp;

        Quote proposal = PartnerDataFactory.newQuote( aOpp );
        proposal.RecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Proposal').getRecordTypeId();
        proposal.Signed__c = Date.Today() - 100;
        proposal.End_Date__c = Date.Today();
        proposal.UUID__c = new Uuid().getValue();
        INSERT proposal;

        Account_Opportunity_Relationship__c oppMember = PartnerDataFactory.newAcctOppRel(partnerAcc.Id, aOpp.Id);
        oppMember.RecordTypeId = Schema.SObjectType.Account_Opportunity_Relationship__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Opportunity_Member').getRecordTypeId();
        INSERT oppMember;

         /* Single OppMember */ 
        Account_Opportunity_Relationship__c singleOppMember = PartnerDataFactory.newAcctOppRel(singlePartnerAcc.Id, aSingleOpp.Id);
        singleOppMember.RecordTypeId = Schema.SObjectType.Account_Opportunity_Relationship__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Opportunity_Member').getRecordTypeId();
        INSERT singleOppMember;

        Product_Item__c aProd = PartnerDataFactory.newProduct( aOpp );
        aProd.RecordTypeId = Schema.SObjectType.Product_Item__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product').getRecordTypeId();
        aProd.Opportunity__c = null;   
        aProd.UUID__c = new Uuid().getValue();    
        INSERT aProd;
 
         /* Single Partner Prod */ 
        Product_Item__c aSingleProd = PartnerDataFactory.newProduct( aOpp );
        aSingleProd.RecordTypeId = Schema.SObjectType.Product_Item__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product').getRecordTypeId();
        aSingleProd.Opportunity__c = null;   
        aSingleProd.Name = 'Single Partner';
        aSingleProd.UUID__c = new Uuid().getValue();    
        INSERT aSingleProd;

        Commercial_Condition__c aComm = new Commercial_Condition__c();
        aComm.RecordTypeId = Schema.SObjectType.Commercial_Condition__c.getRecordTypeInfosByDeveloperName().get('CAP').getRecordTypeId();
        INSERT aComm;

         /* Single Partner Prod */ 
        Commercial_Condition__c aSingleComm = new Commercial_Condition__c();
        aSingleComm.RecordTypeId = Schema.SObjectType.Commercial_Condition__c.getRecordTypeInfosByDeveloperName().get('CAP').getRecordTypeId();
        INSERT aSingleComm;
 
        Product_Assignment__c prodAssign = new Product_Assignment__c();
        prodAssign.RecordTypeId = Schema.SObjectType.Product_Assignment__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product_Assignment').getRecordTypeId();
        prodAssign.ProductId__c = aProd.Id;
        prodAssign.CommercialConditionId__c = aComm.Id;
        INSERT prodAssign;

        /* Single Partner Product Assignment */ 
        Product_Assignment__c singleProdAssign = new Product_Assignment__c();
        singleProdAssign.RecordTypeId = Schema.SObjectType.Product_Assignment__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product_Assignment').getRecordTypeId();
        singleProdAssign.ProductId__c = aSingleProd.Id;
        singleProdAssign.CommercialConditionId__c = aSingleComm.Id;
        INSERT singleProdAssign;

        Product_Opportunity_Assignment__c prodOppAssign = new Product_Opportunity_Assignment__c();
        prodOppAssign.RecordTypeId = Schema.SObjectType.Product_Opportunity_Assignment__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product_Opportunity_Assignment').getRecordTypeId();
        prodOppAssign.OpportunityMemberId__c = oppMember.Id;
        prodOppAssign.ProductAssignmentId__c = prodAssign.Id;
        INSERT prodOppAssign;

        /* Single Partner Product Opportunity Assignment */        
        Product_Opportunity_Assignment__c singleProdOppAssign = new Product_Opportunity_Assignment__c();
        singleProdOppAssign.RecordTypeId = Schema.SObjectType.Product_Opportunity_Assignment__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product_Opportunity_Assignment').getRecordTypeId();
        singleProdOppAssign.OpportunityMemberId__c = singleOppMember.Id;
        singleProdOppAssign.ProductAssignmentId__c = singleProdAssign.Id;
        INSERT singleProdOppAssign;        

        Product_Item__c childProd = PartnerDataFactory.newProduct( aOpp );
        childProd.RecordTypeId = Schema.SObjectType.Product_Item__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product').getRecordTypeId();
        childProd.Parent_Product__c = aProd.Id;   
        childProd.UUID__c = new Uuid().getValue();    
        INSERT childProd;

        Commercial_Condition__c childComm = new Commercial_Condition__c();
        childComm.RecordTypeId = Schema.SObjectType.Commercial_Condition__c.getRecordTypeInfosByDeveloperName().get('CAP').getRecordTypeId();
        INSERT childComm;

        Product_Assignment__c childProdAssign = new Product_Assignment__c();
        childProdAssign.RecordTypeId = Schema.SObjectType.Product_Assignment__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product_Assignment').getRecordTypeId();
        childProdAssign.ProductId__c = childProd.Id;
        childProdAssign.CommercialConditionId__c = childComm.Id;
        INSERT childProdAssign;

        Product_Opportunity_Assignment__c childProdOppAssign = new Product_Opportunity_Assignment__c();
        childProdOppAssign.RecordTypeId = Schema.SObjectType.Product_Opportunity_Assignment__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product_Opportunity_Assignment').getRecordTypeId();
        childProdOppAssign.OpportunityMemberId__c = oppMember.Id;
        childProdOppAssign.ProductAssignmentId__c = childProdAssign.Id;
        INSERT childProdOppAssign;
    }

    @isTest
    static void selectProductAssignmentByID_Test(){
        Product_Assignment__c prodAssign = [SELECT ID, ProductId__c FROM Product_Assignment__c WHERE ProductId__r.Name='Single Partner' LIMIT 1];
        Test.startTest(); 
            System.assertEquals(prodAssign.Id  , new ProductAssignmentSelector().selectProductAssignmentByID(new Set<Id>{prodAssign.Id})[0].Id); 
        Test.stopTest();
    }

    
    @isTest
    static void selectCapByParentProdId_Test(){
        Product_Assignment__c prodAssign = [SELECT ID, ProductId__r.Parent_Product__c FROM Product_Assignment__c WHERE ProductId__r.Parent_Product__c != null LIMIT 1];
        Test.startTest(); 
            System.assertEquals(prodAssign.Id  , new ProductAssignmentSelector().selectCapByParentProdId(new Set<Id>{prodAssign.ProductId__r.Parent_Product__c})[0].Id); 
        Test.stopTest();
    }

    @isTest
    static void selectNonCapByParentProdId_Test(){
        Product_Assignment__c prodAssign = [SELECT ID, ProductId__r.Parent_Product__c FROM Product_Assignment__c WHERE ProductId__r.Parent_Product__c != null LIMIT 1];
        Test.startTest(); 
            System.assert( (new ProductAssignmentSelector().selectNonCapByParentProdId(new Set<Id>{prodAssign.ProductId__r.Parent_Product__c})).size() == 0); 
        Test.stopTest();
    }

    @isTest
    static void selectProductAssignmentByIdAsMap_Test(){
        Product_Assignment__c prodAssign = [SELECT ID, ProductId__r.Parent_Product__c FROM Product_Assignment__c WHERE ProductId__r.Parent_Product__c != null LIMIT 1];
        Test.startTest(); 
            System.assert( (new ProductAssignmentSelector().selectProductAssignmentByIdAsMap(new Set<Id>{prodAssign.Id})).size() > 0); 
        Test.stopTest();
    }

    @isTest
    static void selectCommercialConditionByProductId_Test(){
        Product_Assignment__c prodAssign = [SELECT ID, ProductId__c FROM Product_Assignment__c WHERE ProductId__r.Parent_Product__c != null LIMIT 1];
        Test.startTest(); 
            System.assert( (new ProductAssignmentSelector().selectCommercialConditionByProductId(new Set<Id>{prodAssign.ProductId__c})).size() > 0); 
        Test.stopTest();
    }

    @isTest
    static void selectNonCapByProdId_Test(){
        Product_Assignment__c prodAssign = [SELECT ID, ProductId__c FROM Product_Assignment__c WHERE ProductId__r.Parent_Product__c != null LIMIT 1];
        Test.startTest(); 
            System.assert( (new ProductAssignmentSelector().selectNonCapByProdId(new Set<Id>{prodAssign.ProductId__c})).size() == 0); 
        Test.stopTest();
    }

    
    @isTest
    static void selectByCommConditionId_Test(){
        Commercial_Condition__c comm = [SELECT ID  FROM Commercial_Condition__c LIMIT 1];
        Test.startTest(); 
            System.assert( (new ProductAssignmentSelector().selectByCommConditionId(new Set<Id>{comm.Id})).size() > 0); 
        Test.stopTest();
    }


    @isTest
    static void selectAllByProductId_Test(){
        Product_Assignment__c prodAssign = [SELECT ID, ProductId__c FROM Product_Assignment__c WHERE ProductId__r.Parent_Product__c != null LIMIT 1];
        Test.startTest(); 
            System.assert( (new ProductAssignmentSelector().selectAllByProductId(new Set<Id>{prodAssign.ProductId__c})).size() > 0); 
        Test.stopTest();
    }


    @isTest
    static void selectAllFromChildProductByParentIds_Test(){
        Product_Assignment__c prodAssign = [SELECT ID, ProductId__c FROM Product_Assignment__c WHERE ProductId__r.Parent_Product__c = null LIMIT 1];
        Test.startTest(); 
            System.assert( (new ProductAssignmentSelector().selectAllFromChildProductByParentIds(new Set<Id>{prodAssign.ProductId__c})).size() > 0); 
        Test.stopTest();
    }

    @isTest
    static void selectAllFieldsFromProductAssignWithCap_Test(){
        Product_Assignment__c prodAssign = [SELECT ID, ProductId__c FROM Product_Assignment__c WHERE ProductId__r.Parent_Product__c = null LIMIT 1];
        Test.startTest(); 
            System.assert( (new ProductAssignmentSelector().selectAllFieldsFromProductAssignWithCap(new Set<Id>{prodAssign.ProductId__c})).size() > 0); 
        Test.stopTest();
    }

    @isTest
    static void selectAllFieldsFromProductAssignWithCapById_Test(){
        Product_Assignment__c prodAssign = [SELECT ID, ProductId__c FROM Product_Assignment__c WHERE ProductId__r.Parent_Product__c = null LIMIT 1];
        Test.startTest(); 
            System.assert( (new ProductAssignmentSelector().selectAllFieldsFromProductAssignWithCapById(new Set<Id>{prodAssign.ProductId__c})).size() > 0); 
        Test.stopTest();
    }
 
}