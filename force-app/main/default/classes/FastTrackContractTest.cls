@isTest(seeAllData=false)
public with sharing class FastTrackContractTest {
    
    @TestSetup
    static void createData(){        
        Account acc = generateAccount();
        insert acc;
        insert generateContact(acc, acc.Name);
        // Pricebook2 pb = generatePricebook();
        // insert pb;
        Pricebook2 pb = getStandartPricebook();
        Opportunity opp = new Opportunity(CurrencyIsoCode='BRL');
        Product2 p = generateAccessFeeProduct();
        insert p;
        Product2 pFamilyMember = generateAccessFeeFamilyMemberIncludedProduct();
        insert pFamilyMember;
        PricebookEntry accessFee = generatePricebookEntry(pb, p, opp);
        insert accessFee;
        PricebookEntry accessFeeFamilyMember = generatePricebookEntry(pb, pFamilyMember, opp);
        insert accessFeeFamilyMember;
        Product2 pSetupFee = generateSetupFeeProduct();
        insert pSetupFee;
        PricebookEntry setupFee = generatePricebookEntry(pb, pSetupFee, opp);
        insert setupFee;
        Product2 pProfServicesOneFee = generateProfServicesOneFeeProduct();
        insert pProfServicesOneFee;
        PricebookEntry profServicesOneFee = generatePricebookEntry(pb, pProfServicesOneFee, opp);
        insert profServicesOneFee;
        Product2 pProfServicesMainFee = generateProfServicesMainFeeProduct();
        insert pProfServicesMainFee;
        PricebookEntry profServicesMainFee = generatePricebookEntry(pb, pProfServicesMainFee, opp);
        insert profServicesMainFee;
        opp = generateOpportunity(acc,pb);
        insert opp;  

        insert generateGympassEntity('gympass');

    }
    
    
    
    @isTest
    static void testContractSignedNotFound(){
        Opportunity opp = [select Id,AccountId from Opportunity];
        Test.startTest();
        FastTrackContract ftcon = new FastTrackContract();
        boolean bolContractFound = FastTrackContract.isContractSignedForOpportunity(opp.id);
       
        System.assertEquals(false, bolContractFound);
        Test.stopTest();
    }

    
    
    private static Account generateGympassEntity(String name){
        Id rtId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gympass_Entity').getRecordTypeId();
        Account acc = new Account();
        acc.name=name;
        acc.RecordTypeId = rtId;
        acc.GP_Status__c = 'Active';
        acc.billingState = 'Florida';
        acc.CAP_Value__c = 120;
        acc.BillingCity = 'CityAcademiaBrasil';
        acc.billingStreet = 'Rua academiabrasilpai';
        acc.billingCountry = 'United States';
        acc.Gym_Type__c = 'Studios';
        acc.Gym_Classes__c = 'Cardio';
        acc.Types_of_ownership__c = Label.franchisePicklist;
        acc.Subscription_Type__c = 'Value per class';
        acc.Subscription_Period__c = 'Monthy value';
        acc.Subscription_Type_Estimated_Price__c    = 100;
        acc.Has_market_cannibalization__c = 'No';
        acc.Exclusivity__c = 'Yes';
        acc.Exclusivity_End_Date__c = Date.today().addYears(1);
        acc.Exclusivity_Partnership__c = 'Full Exclusivity';
        acc.Exclusivity_Restrictions__c= 'No';
        acc.Website = 'testing'+name+'@tesapex.com';
        acc.Gym_Email__c = 'gymemail'+name+'@apex.com';
        acc.Phone = '3222'+name+'23123';
        acc.Can_use_logo__c = 'Yes';
        acc.Legal_Registration__c = 12123;
        acc.Legal_Title__c = 'Title LTDA'+name;
        acc.Gyms_Identification_Document__c = 'CNPJ'; 
        return acc;
    }

    private static Contact generateContact(Account acc, String name){
        Contact acccnt = new Contact();
        acccnt.AccountId = acc.Id;
        acccnt.LastName = 'test'+name;
        acccnt.Email = 'test'+name+'@tst.com';
        return acccnt;
    }
    
    private static Account generateAccount(){
        Id rtId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gympass_Entity').getRecordTypeId();
        Account acc = new Account();
        acc.name='AcademiaBrasilCompanyPai';
        acc.RecordTypeId = rtId;
        acc.GP_Status__c = 'Active';
        acc.billingState = 'Minas Gerais';
        acc.CAP_Value__c = 120;
        acc.BillingCity = 'CityAcademiaBrasil';
        acc.billingStreet = 'Rua academiabrasilpai';
        acc.billingCountry = 'Brazil';        
        acc.Gym_Type__c = 'Studios';
        acc.Gym_Classes__c = 'Cardio';
        acc.Types_of_ownership__c = Label.franchisePicklist;
        acc.Subscription_Type__c = 'Value per class';
        acc.Subscription_Period__c = 'Monthy value';
        acc.Subscription_Type_Estimated_Price__c    = 100;
        acc.Has_market_cannibalization__c = 'No';
        acc.Exclusivity__c = 'Yes';
        acc.Exclusivity_End_Date__c = Date.today().addYears(1);
        acc.Exclusivity_Partnership__c = 'Full Exclusivity';
        acc.Exclusivity_Restrictions__c= 'No';
        acc.Website = 'testing@tesapex.com';
        acc.Gym_Email__c = 'gymemail@apex.com';
        acc.Phone = '3222123123';
        acc.Can_use_logo__c = 'Yes';
        acc.Legal_Registration__c = 12123;
        acc.Legal_Title__c = 'Title LTDA';
        acc.Gyms_Identification_Document__c = 'CNPJ'; 
        return acc;
    }
    
    
    
    
    

    private static Opportunity generateOpportunity(Account acc, Pricebook2 pb){
        Id oppRtId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        Opportunity accOpp = new Opportunity();
        accOpp.recordTypeId = oppRtId;
        accOpp.AccountId = acc.id;
        accOpp.Name = acc.Id; 
        accOpp.CMS_Used__c = 'Yes';     
        accOpp.Gym_agreed_to_an_API_integration__c  = 'Yes';
        accOpp.Club_Management_System__c = 'Companhia Athletica';
        accOpp.Integration_Fee_Deduction__c = 'No';
        accOpp.CloseDate = Date.today();
        accOpp.Success_Look_Like__c = 'Yes';
        accOpp.Success_Look_Like_Description__c = 'Money money';
        accOpp.StageName = 'Proposta Enviada';
        accOpp.Type = 'Expansion';  
        accOpp.Country_Manager_Approval__c = true;
        accOpp.Payment_approved__c = true;   
        accOpp.CurrencyIsoCode = 'BRL';
        accOpp.Gympass_Plus__c = 'Yes';
        accOpp.Standard_Payment__c = 'Yes';
        accOpp.Request_for_self_checkin__c = 'Yes';  
        accOpp.Pricebook2Id = pb.Id;
        return accOpp;
    }

    private static Pricebook2 generatePricebook(){        
        Pricebook2 pb = new Pricebook2();
        pb.Name = 'Br';
        pb.Country__c = 'Brazil';
        pb.IsActive = true;
        return pb;
    }

    private static Product2 generateAccessFeeProduct(){
        Product2 product = new Product2();
        product.Name = 'Enterprise Subscription';
        product.Minimum_Number_of_Employees__c = 0;
        product.Maximum_Number_of_Employees__c = 900;
        product.Family_Member_Included__c = false;
        product.Family = 'Enterprise Subscription';
        product.IsActive = true;
        return product;
    }

    private static Product2 generateAccessFeeFamilyMemberIncludedProduct(){
        Product2 product = new Product2();
        product.Name = 'Enteprise FM';
        product.Minimum_Number_of_Employees__c = 0;
        product.Maximum_Number_of_Employees__c = 900;
        product.Family_Member_Included__c = true;
        product.Family = 'Enterprise Subscription';
        product.IsActive = true;
        return product;
    }

    private static Product2 generateSetupFeeProduct(){
        Product2 product = new Product2();
        product.Name = 'Setup Fee';
        product.Minimum_Number_of_Employees__c = 0;
        product.Maximum_Number_of_Employees__c = 900;
        product.Family = 'Setup Fee';
        product.IsActive = true;
        product.Family_Member_Included__c = false;
        return product;
    }

    private static PricebookEntry generatePricebookEntry(Pricebook2 pb, Product2 product, Opportunity opp){        
        PricebookEntry pbEntry = new PricebookEntry();
        pbEntry.Product2Id = product.Id;
        pbEntry.Pricebook2Id = Test.getStandardPricebookId();
        pbEntry.UnitPrice = 100;
        pbEntry.IsActive = true;
        pbEntry.CurrencyIsoCode = opp.CurrencyIsoCode;
        return pbEntry;       
    }

    private static Product2 generateProfServicesOneFeeProduct(){
        Product2 product = new Product2();
        product.Name = 'Professional Services Setup Fee';
        product.Minimum_Number_of_Employees__c = 0;
        product.Maximum_Number_of_Employees__c = 900;
        product.Family = 'Professional Services';
        product.IsActive = true;
        product.Family_Member_Included__c = false;
        return product;
    }

    private static Product2 generateProfServicesMainFeeProduct(){
        Product2 product = new Product2();
        product.Name = 'Professional Services Maintenance Fee';
        product.Minimum_Number_of_Employees__c = 0;
        product.Maximum_Number_of_Employees__c = 900;
        product.Family = 'Professional Services';
        product.IsActive = true;
        product.Family_Member_Included__c = false;
        return product;
    }

    private static Pricebook2 getStandartPricebook() {
        Pricebook2 standardPricebook = new Pricebook2(
	        Id = Test.getStandardPricebookId(),
	        IsActive = true
        );
        update standardPricebook;

        return standardPricebook;
    }
}