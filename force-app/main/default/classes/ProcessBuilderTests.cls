/*
* @Author: Bruno Pinho
* @Date: January/2019
* @Description: Test class for all Process Builders
*/
@isTest
private class ProcessBuilderTests
{
    static void SetUp() {
        test.StartTest();
    }
    
    static void TearDown() {
        test.StopTest();
    }
    
    public static testmethod void testContract() {
        SetUp();
        List<Pricebook2> listPricebooks = [SELECT Id
                                           FROM Pricebook2
                                           LIMIT 1];
        
        if (listPricebooks.size() > 0)
        {
            Account account = new Account();
            account.Name = 'Contract Test Account';
            account.BillingCountry = 'Brazil';
            account.BillingState = 'São Paulo';
            account.Id_Company__c = '00.000.000/0000-00';
            INSERT account;
            
            Contract contract = new Contract();
            contract.AccountId = account.Id;
            contract.Pricebook2Id = listPricebooks[0].Id;
            INSERT contract;   
        }
        
        TearDown();
    }
    
    public static testmethod void testEvent() {
        SetUp();
        Event event = new Event(Subject = 'Test Event', EndDateTime = System.now().addDays(2), StartDateTime = System.now().addDays(1));
        INSERT event;
        TearDown();
    }
    
    public static testmethod void testOpportunity() {
        SetUp();
        
        Account account = new Account();
        account.Name = 'Contract Test Account';
        account.BillingCountry = 'Brazil';
        account.BillingState = 'São Paulo';
        account.Id_Company__c = '00.000.000/0000-00';
        account.Type = 'Partner';
        INSERT account;
        
        Opportunity opportunity1 = new Opportunity();
        opportunity1.AccountId = account.Id;
        opportunity1.Name = 'op test';
        opportunity1.StageName = 'Offer Sent';
        opportunity1.CloseDate = Date.today();
        opportunity1.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Partner - Small and Medium').getRecordTypeId();
        
        INSERT opportunity1;
        
        opportunity1.StageName = 'Opportunity Validated';        
        UPDATE opportunity1;
        
        TearDown();
    }
    
    public static testmethod void testProposal() {
        SetUp();
        
        Account account = new Account();
        account.Name = 'Contract Test Account';
        account.BillingCountry = 'Brazil';
        account.BillingState = 'São Paulo';
        account.Id_Company__c = '00.000.000/1000-00';
        account.Type = 'Partner';
        INSERT account;
                
        Opportunity opportunity1 = new Opportunity();
        opportunity1.AccountId = account.Id;
        opportunity1.Name = 'op test';
        opportunity1.StageName = 'Offer Sent';
        opportunity1.CloseDate = Date.today();
        opportunity1.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Partner - Small and Medium').getRecordTypeId();
                
        INSERT opportunity1;
                
        opportunity1.StageName = 'Opportunity Validated';        
        UPDATE opportunity1;
        
        Copay_Plan__c copayPlan = new Copay_Plan__c();
        copayPlan.isCustom__c = 'Yes';
        INSERT copayPlan;    
                
        Quote quote = new Quote();
        quote.Name = 'Test';
        quote.OpportunityId = opportunity1.Id;
        quote.Gym__c = copayPlan.Id;
        quote.License_Fee_Waiver__c = 'No';
        quote.Volume_Discount_Type__c = 'By single user volume';
        quote.Threshold_Value_1__c= 1;
		quote.Threshold_Value_2__c= 1;
		quote.Threshold_Value_3__c= 1;
		quote.First_Discount_Range__c= 1;
		quote.Second_Discount_Range__c= 1;
		quote.Third_Discount_Range__c= 1;
		quote.Fourth_Discount_Range__c= 1;
		quote.Threshold_Value_4__c= 1;
        quote.RecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByName().get('Gyms - Quote Partner').getRecordTypeId();
        INSERT quote;
        
        TearDown();
    }
    
    public static testmethod void testTask() {
        SetUp();        
        Task task = new Task();
        INSERT task;
        TearDown();
    }
    
    public static testmethod void testLead() {
        SetUp();
        Lead lead = new Lead();
        lead.Email = 'a@a.com';
        lead.LastName = 'Teste';
        lead.Company = 'Teste';
        lead.Country = 'Brazil';
        INSERT lead;
        TearDown();
    }
    
    public static testmethod void testGympassEvent() {
        SetUp();
        Gympass_Event__c gympassEvent = new Gympass_Event__c();
        INSERT gympassEvent;
        TearDown();
    }
}