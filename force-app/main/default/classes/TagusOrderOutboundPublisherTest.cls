@IsTest
public class TagusOrderOutboundPublisherTest {
  @TestSetup
  public static void makeData() {
    EventQueueFixtureFactory.createEventConfigForEvent(
      'CREATE_ORDER_ON_TAGUS',
      'UpsertOrderOnTagusCommand'
    );

    EventQueueFixtureFactory.createEventConfigForEvent(
      'UPDATE_ORDER_ON_TAGUS',
      'UpsertOrderOnTagusCommand'
    );
  }

  @IsTest
  public static void execute() {
    Test.setMock(HttpCalloutMock.class, new HttpResponseMock());

    Account gympassEntity = AccountMock.getGympassEntity();
    gympassEntity.UUID__c = new Uuid().getValue();
    insert gympassEntity;

    Account account = AccountMock.getStandard('Empresas');
    account.Send_To_Tagus__c = true;
    insert account;

    Contact contact = ContactMock.getStandard(account);
    insert contact;

    account.Attention__c = contact.Id;
    update account;

    Pricebook2 pricebook = new Pricebook2(
      CurrencyIsoCode = 'BRL',
      Id = Test.getStandardPricebookId(),
      IsActive = true
    );
    upsert pricebook;

    List<Product2> products = new List<Product2>();

    Product2 setupFee = ProductMock.getSetupFee();
    products.add(setupFee);

    Product2 accessFee = ProductMock.getAccessFee();
    products.add(accessFee);

    Product2 accessFeeWithFamilyMember = ProductMock.getAccessFeeFamilyMemberIncluded();
    products.add(accessFeeWithFamilyMember);

    insert products;

    List<PricebookEntry> pricebookEntries = new List<PricebookEntry>();

    PricebookEntry setupFeePricebookEntry = PricebookMock.getPricebookEntry(
      pricebook,
      setupFee
    );
    pricebookEntries.add(setupFeePricebookEntry);

    PricebookEntry accessFeePricebookEntry = PricebookMock.getPricebookEntry(
      pricebook,
      accessFee
    );
    pricebookEntries.add(accessFeePricebookEntry);

    PricebookEntry accessFeeWithFamilyMemberPricebookEntry = PricebookMock.getPricebookEntry(
      pricebook,
      accessFeeWithFamilyMember
    );
    pricebookEntries.add(accessFeeWithFamilyMemberPricebookEntry);

    insert pricebookEntries;

    Opportunity newBusiness = OpportunityMock.getClientSalesSkuNewBusiness(
      account,
      pricebook
    );
    newBusiness.Gympass_Entity__c = gympassEntity.Id;
    insert newBusiness;

    Payment__c newBusinessNonRecurringPayment = PaymentMock.getStandard(
      newBusiness,
      account
    );
    newBusinessNonRecurringPayment.RecordTypeId = Schema.SObjectType.Payment__c
      .getRecordTypeInfosByDeveloperName()
      .get('Non_Recurring')
      .getRecordTypeId();

    insert newBusinessNonRecurringPayment;

    Payment__c newBusinessRecurringPayment = PaymentMock.getStandard(
      newBusiness,
      account
    );
    newBusinessRecurringPayment.RecordTypeId = Schema.SObjectType.Payment__c
      .getRecordTypeInfosByDeveloperName()
      .get('Recurring')
      .getRecordTypeId();

    insert newBusinessRecurringPayment;

    Eligibility__c newBusinessEligibility = EligibilityMock.getStandard(
      newBusinessRecurringPayment
    );

    insert newBusinessEligibility;

    Quote newBusinessQuote = QuoteMock.getStandard(newBusiness);
    insert newBusinessQuote;

    List<QuoteLineItem> newBusinessQuoteLineItems = new List<QuoteLineItem>();

    QuoteLineItem newBusinessSetupFeeLine = QuoteLineItemMock.getSetupFee(
      newBusinessQuote,
      setupFeePricebookEntry
    );
    newBusinessQuoteLineItems.add(newBusinessSetupFeeLine);

    QuoteLineItem newBusinessAccessFeeLine = QuoteLineItemMock.getEnterpriseSubscription(
      newBusinessQuote,
      accessFeePricebookEntry
    );
    newBusinessQuoteLineItems.add(newBusinessAccessFeeLine);

    QuoteLineItem newBusinessAccessFeeWithFamilyMemberLine = QuoteLineItemMock.getEnterpriseSubscription(
      newBusinessQuote,
      accessFeeWithFamilyMemberPricebookEntry
    );
    newBusinessQuoteLineItems.add(newBusinessAccessFeeWithFamilyMemberLine);

    insert newBusinessQuoteLineItems;

    Waiver__c newBusinessWaiver = WaiverMock.getStandard(
      newBusinessAccessFeeLine
    );

    insert newBusinessWaiver;

    Test.startTest();

    newBusiness.StageName = 'Lançado/Ganho';
    update newBusiness;

    Test.stopTest();

    List<Order> orders = queryOrders();

    EventQueue event = new TagusOrderOutboundPublisher(orders).runCreate();

    System.assertNotEquals(
      'ERROR',
      event.getStatus(),
      'The status should not be ERROR in the create event'
    );

    event = new TagusOrderOutboundPublisher(orders).runUpdate();

    System.assertNotEquals(
      'ERROR',
      event.getStatus(),
      'The status should not be ERROR in the updtede event'
    );
  }

  private static List<Order> queryOrders() {
    Map<String, Object> orderFields = (Map<String, Object>) Schema.SObjectType.Order.fields.getMap();

    String soqlOrderFields = '';

    for (String orderFieldName : orderFields.keySet()) {
      soqlOrderFields += (orderFieldName + ',');
    }

    String soqlQuery =
      'SELECT ' +
      soqlOrderFields.removeEnd(',') +
      ' FROM Order';

    return (List<Order>) Database.query(soqlQuery.escapeJava());
  }

  private class HttpResponseMock implements HttpCalloutMock {
    public HTTPResponse respond(HTTPRequest req) {
      HttpResponse res = new HttpResponse();
      res.setHeader('Content-Type', 'application/json');
      res.setStatusCode(200);
      return res;
    }
  }
}