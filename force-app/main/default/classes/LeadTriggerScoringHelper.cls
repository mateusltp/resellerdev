/**
 * @File Name          : LeadTriggerScoringHelper.cls
 * @Description        : 
 * @Author             : David Mantovani - DDMA@gft.com
 * @Group              : 
 * @Last Modified By   : David Mantovani - DDMA@gft.com
 * @Last Modified On   : 08-11-2020 
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    29/05/2020   Samuel Silva - GFT (slml@gft.com)     Initial Version
**/
public without sharing class LeadTriggerScoringHelper implements Queueable {
    
    private List<Lead> lstNew = new List<Lead>();     
    private Map<Id, Set<Id>> dupLeadWithLeadMap = new Map<Id, Set<Id>>();
    private Map<Id, Id> accWithScoreMap = new Map<Id, Id>();
    private Set<Id> leadNoDupIds = new Set<Id>();
    private Set<Id> allLeadsId = new Set<Id>();
    private Map<String, Lead_Score_by_Country__mdt> scoreMap = new Map<String, Lead_Score_by_Country__mdt>();
    private Map<String, Score_Data__c> scoreDataMap = new Map<String, Score_Data__c>();
    

    public LeadTriggerScoringHelper(Map<String, Lead_Score_by_Country__mdt>  scoreByCountryMap, Map<String, Score_Data__c> scoreData ,List<Lead> lstLeads){
        this.scoreMap = scoreByCountryMap;
        this.scoreDataMap = scoreData;
        this.lstNew = lstLeads;
        
    }

    public void execute(QueueableContext context){  
        executeScore();
    }      

    private void executeScore(){
        for(Lead l : lstNew){           
            Set<Id> dupLead = new Set<Id>();
            List<SObject> mrObjLst = DuplicateHandler.foundDuplicateRecords(l);        
            if(!mrObjLst.isEmpty()) {
                for(SObject mrObj : mrObjLst){
                    if(mrObj.getSObjectType().getDescribe().getName() == 'Lead'){                                       
                        dupLead.add(mrObj.Id);
                    }                      
                    if(mrObj.getSObjectType().getDescribe().getName() == 'Account'){                        
                        accWithScoreMap.put(l.Id, mrObj.Id); 
                    }
                }  
                if(!dupLead.isEmpty()){
                    allLeadsId.add(l.Id);
                    allLeadsId.addAll(dupLead);
                    dupLeadWithLeadMap.put(l.Id, dupLead);
                }  

                if(!accWithScoreMap.isEmpty()){
                    allLeadsId.addAll(accWithScoreMap.keySet());
                }
            } else {
                leadNoDupIds.add(l.Id);
            }                
        }
        if(!allLeadsId.isEmpty()){ 
            List<Lead> leadLst = [  SELECT Id, Number_of_Referrals__c,Referral_Account__c, Referral_Score__c, City, Country
                                    FROM Lead
                                    WHERE Id IN: allLeadsId
                                ];
            if(!dupLeadWithLeadMap.isEmpty()) {
                for(Id lId : dupLeadWithLeadMap.keySet()){ 
                    Set<Lead> lstLeadToScore = new Set<Lead>();
                    for(Id mappedId : dupLeadWithLeadMap.get(lId)){
                        for(Lead nextLead : leadLst){
                            if(nextLead.id == mappedId || nextLead.id == lId){
                                lstLeadToScore.add(nextLead);
                            }
                        }   
                    } 
                    scoreLeads(lstLeadToScore);
                }      
            }
        
            if(!accWithScoreMap.isEmpty()){
                List<Account> accLst = [    Select Id, Referral_Score__c
                                            FROM Account 
                                            WHERE ID IN: accWithScoreMap.values()
                                        ];              
    
                System.debug(accWithScoreMap);          
                for(Lead l: leadLst){
                    for(Account acc : accLst){                 
                        if(acc.Id == accWithScoreMap.get(l.Id)){
                            acc.Referral_Score__c = l.Number_of_Referrals__c;                            
                            l.Referral_Account__c = acc.Id;
                        }
                    }              
                }
                System.debug('AccList size ' + accLst.size());
                updateLst(accLst);  
            }           
            
            System.debug('leadLst size ' + leadLst.size());
            updateLst(leadLst);
        }

        if(!leadNoDupIds.isEmpty()) {
            List<Lead> leadNoDupLst = [SELECT Id, Number_Of_Referrals__c, Referral_Account__c, Referral_Score__c , Country , City FROM Lead WHERE ID IN: leadNoDupIds];
             
            scoreLeadNoDuplicates(leadNoDupLst);
            scoreLeadTableauData(leadNoDupLst);
            
            System.debug('leadNoDupLst size ' + leadNoDupLst.size());

            updateLst(leadNoDupLst);
            
        }

    }
    private void scoreLeadNoDuplicates(List<Lead> leadNoDupLst){
        System.debug('scoreLeadNoDuplicates -------> ' );
        for(Lead l : leadNoDupLst) {
            String lCity = l.City;
            String lRegion=l.Country;
            Lead_Score_by_Country__mdt scoreBase = scoreMap.get(lRegion);
            if(scoreBase == null){
                scoreBase = scoreMap.get('Standard');
            }
        
            System.debug('SCORE NODUP -----> ' );
            l.Referral_Score__c = scoreBase.X1st_Range_of_Referral_Qty_Percentage__c;
            l.Referral_Account__c = null;
            l.Number_Of_Referrals__c = 1;
            System.debug('Referral SCORE leadNoDupIds  ---------->' + l.Referral_Score__c); 
            
        }   
    }

    private void scoreLeadTableauData(List<Lead> leadLst){
        System.Debug('scoreLeadTableauData ');
        for(lead l : leadLst){
            String lCity = l.City;
            String lRegion=l.Country;
            Lead_Score_by_Country__mdt scoreBase = scoreMap.get(lRegion);
            Score_Data__c scoreDataBase = scoreDataMap.get(lCity + lRegion);
            
            if(scoreBase == null) {
                scoreBase = scoreMap.get('Standard');
            }

            if(scoreDataBase == null) {
                scoreDataBase = scoreDataMap.get('StandardStandard');
            }
            System.Debug('scoreBase ----------> ' + scoreBase);
            System.Debug('scoreDataBase ----------> ' + scoreDataBase);

            Decimal SignUpQty = scoreDataBase.Number_Of_Sign_Ups__c;
            Decimal ActivesQty = scoreDataBase.Number_Of_Active_Users__c;
            System.Debug('TableauData SignUpQty => ' + SignUpQty);
            System.Debug('TableauData ActivesQty => ' + ActivesQty);
            Decimal scoreActives = 0;
            Decimal scoreSignUp = 0;
            

            if(scoreDataBase != null) {
                //SignUP Score Calculator   
                if(SignUpQty>= scoreBase.SignUP_Value_1__c && SignUpQty < scoreBase.SignUP_Value_2__c){
                    scoreSignUp = scoreBase.SignUp_Score_1__c;
                    System.debug('scoreSignUp  1 if => ' + scoreSignUp);
                }else if(SignUpQty>= scoreBase.SignUP_Value_2__c && SignUpQty < scoreBase.SignUP_Value_3__c){
                    scoreSignUp = scoreBase.SignUp_Score_2__c;
                    System.debug('scoreSignUp 2 if => ' + scoreSignUp);
                }else if(SignUpQty>= scoreBase.SignUP_Value_3__c && SignUpQty < scoreBase.SignUP_Value_4__c){
                    scoreSignUp = scoreBase.SignUp_Score_3__c;
                    System.debug('scoreSignUp 3 if => ' + scoreSignUp);
                }else if(SignUpQty>= scoreBase.SignUP_Value_4__c){
                    scoreSignUp = scoreBase.SignUp_Score_4__c;
                    System.debug('scoreSignUp 4 if => ' + scoreSignUp);
                }
                //Active Score Calculator
                if(ActivesQty>= scoreBase.Active_Value_1__c && ActivesQty < scoreBase.Active_Value_2__c){
                    scoreActives = scoreBase.Active_Score_1__c;
                    System.debug('scoreActives 1 if => ' + scoreActives);
                }else if(ActivesQty>= scoreBase.Active_Value_2__c && ActivesQty < scoreBase.Active_Value_3__c){
                    scoreActives = scoreBase.Active_Score_2__c;
                    System.debug('scoreActives 2 if => ' + scoreActives);
                }else if(ActivesQty>= scoreBase.Active_Value_3__c && ActivesQty < scoreBase.Active_Value_4__c){
                    scoreActives = scoreBase.Active_Score_3__c;
                    System.debug('scoreActives 3 if => ' + scoreActives);
                }else if(ActivesQty>= scoreBase.Active_Value_4__c){
                    scoreActives = scoreBase.Active_Score_4__c;
                    System.debug('scoreActives 4 if => ' + scoreActives);
                }
            }
            l.Referral_Score__c +=  scoreActives + scoreSignUp ;
            l.Number_of_SignUp__c = SignUpQty;
            l.Number_of_Actives__c = ActivesQty;
            System.debug('Referral SCORE Value   ---------->' + l.Referral_Score__c);
        }
        

    }

    private void scoreLeads(Set<Lead> lstLeadToScore){
        System.Debug('scoreLeads');
        String lCity = (new list<Lead>(lstLeadToScore))[0].City;
        String lRegion = (new list<Lead>(lstLeadToScore))[0].Country; 
        Lead_Score_by_Country__mdt scoreBase = scoreMap.get(lRegion);
        
        if(scoreBase == null) {
            scoreBase = scoreMap.get('Standard');
        }     
        System.Debug('scoreLeads --- scoreBase  ==> '  + scoreBase);

        Decimal scoreNumberOfReferral = 0;   
        Integer referralQty = lstLeadToScore.size();       
                
        //Number of Referrals Calculator
        if(referralQty<= scoreBase.X1st_Range_of_Referral_Quantity__c && referralQty < scoreBase.X2nd_Range_of_Referral_Quantity__c){
            scoreNumberOfReferral = scoreBase.X1st_Range_of_Referral_Qty_Percentage__c;   
        }else if(referralQty>= scoreBase.X2nd_Range_of_Referral_Quantity__c && referralQty < scoreBase.X3rd_Range_of_Referral_Quantity__c){
            scoreNumberOfReferral = scoreBase.X2nd_Range_of_Referral_Qty_Percentage__c;
        }else if(referralQty >= scoreBase.X3rd_Range_of_Referral_Quantity__c && referralQty < scoreBase.X4th_Range_of_Referral_Quantity__c){
            scoreNumberOfReferral = scoreBase.X3rd_Range_of_Referral_Qty_Percentage__c;
        }else if(referralQty >= scoreBase.X4th_Range_of_Referral_Quantity__c && referralQty < scoreBase.X5th_Range_of_Referral_Quantity__c){
            scoreNumberOfReferral = scoreBase.X4th_Range_of_Referral_Qty_Percentage__c;
        }else if(referralQty >= scoreBase.X5th_Range_of_Referral_Quantity__c && referralQty < scoreBase.X6th_Range_of_Referral_Quantity__c){
            scoreNumberOfReferral = scoreBase.X5th_Range_of_Referral_Qty_Percentage__c;
        }else if(referralQty >= scoreBase.X6th_Range_of_Referral_Quantity__c){
            scoreNumberOfReferral = scoreBase.X6th_Range_of_Referral_Qty_Percentage__c;
        }
        System.Debug('Sum - scoreNumberOfReferral => ' + scoreNumberOfReferral);
        
        for(Lead l : lstLeadToScore){   
            l.Referral_Score__c = scoreNumberOfReferral;  
            l.Number_of_Referrals__c = referralQty;     
        } 
        scoreLeadTableauData(new list<Lead>(lstLeadToScore));
    } 
    
    private void updateLst(List<SObject> lstObj){    
        Database.SaveResult[] results = Database.update(lstObj, false);
        for(Database.SaveResult r : results){            
            System.debug('Success ID ' + r.getId());  
            if(!r.isSuccess()){               
                for(Database.Error e : r.getErrors()){
                    System.debug('Error ' + e.message +' ID '+  r.id);
                }
            }
        }        
    }

}