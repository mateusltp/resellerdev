/**
 * @File Name          : ContentFileHelper.cls
 * @Description        : 
 * @Author             : GEPI@GFT.com
 * @Group              : 
 * @Last Modified By   : gft.samuel.silva@ext.gympass.com
 * @Last Modified On   : 04-07-2022
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    06/07/2020   DDMA@GFT.com     Initial Version
**/
public class ContentFileHelper {

    Id recordTypeIdSmall = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Gyms_Small_Medium_Partner').getRecordTypeId();
    Id recordTypeIdWishList = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Gyms_Wishlist_Partner').getRecordTypeId();
    Id recordTypePartner = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gym_Partner').getRecordTypeId();
    Id recordTypePartnerFlow = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Account').getRecordTypeId();

    Id recordTypeIdFileObject= Schema.SObjectType.FIleObject__c.getRecordTypeInfosByDeveloperName().get('Partner').getRecordTypeId();
    
    List<ContentDocumentLink> lstNew = trigger.new;
    List<ContentDocumentLink> lstOld = trigger.old;
    
    List<ContentDistribution> stDocList = new list<ContentDistribution>();
    public Set<Schema.SObjectType> restrictedSobjects;
    static boolean hasRun = false;

    public void generatePublickLink(){
        System.debug(':: ContentFileHelper generatePublickLink b4');
     
        generatepublicklinkImages(lstNew);
    }
    
    public void deleteimage(){
        deleteimage(lstOld);
    }

    public ContentFileHelper()
    {
		restrictedSobjects = new Set<Schema.SObjectType> {
			Account.getSObjectType(),
			Opportunity.getSObjectType()
        };
    }

    public void generatepublicklinkImages(List<ContentDocumentLink> documents) {

        System.debug('1.Number of Queries used in this apex code so far: ' + Limits.getQueries());
        System.debug('2.Number of rows queried in this apex code so far: ' + Limits.getDmlRows());
        System.debug('3. Number of script statements used so far : ' + Limits.getDmlStatements());
        
        Set<ContentDocumentLink> contentDocumentLink = new Set<ContentDocumentLink> ();
        Set<ID> contentDocumentLinkIds = new Set<ID> ();
        Map<Id, contentVersion> mapIdContentVersion = new Map<Id, contentVersion>();
        Set<ID> contentVersionIds = new Set<ID> ();
        Set<ID> AccIds = new Set<ID>();
        Set<ID> OppIds = new Set<ID>();
        List<Account> accRt = new List<Account>();
        List<Opportunity> oppRt = new List<Opportunity>();
        Set<ID> oppRTIds = new Set<ID>();
        Set<ID> accRTIds = new Set<ID>();
        set<ID> rtPartners = new Set<ID>();

        for (ContentDocumentLink contentDocLink : documents) {  
            if(contentDocLink.LinkedEntity.Type != 'User'){
                if (restrictedSobjects.contains(contentDocLink.linkedEntityId.getSobjecttype())){
                    if(contentDocLink.linkedEntityId.getSobjectType() == Account.getSObjectType()){
                        AccIds.add(contentDocLink.linkedEntityId);
                    }else if(contentDocLink.linkedEntityId.getSobjectType() == Opportunity.getSObjectType()){
                        OppIds.add(contentDocLink.linkedEntityId);
                    }
                    contentDocumentLink.add(contentDocLink);
                }  
            }            

        }

        if(!AccIds.isEmpty()){
            accRt= [SELECT RecordTypeId FROM Account WHERE id IN: AccIds];

            for(Account acc : accRt){
                accRTIds.add(acc.RecordTypeId);
            }
        }

        if(!OppIds.isEmpty()){
            oppRt = [SELECT RecordTypeId FROM Opportunity WHERE id IN: OppIds];

            for(Opportunity opp : oppRt){
                oppRTIds.add(opp.RecordTypeId);
            }
        }

        for(contentDocumentLink cntdoclink : contentDocumentLink){            
            if(accRtIds.contains(recordTypePartner) || accRtIds.contains(recordTypePartnerFlow) || oppRTIds.contains(recordTypeIdWishList) || oppRTIds.contains(recordTypeIdSmall)){
                contentDocumentLinkIds.add(cntdoclink.contentdocumentid);
            }
        }

        if(!contentDocumentLinkIds.isEmpty()){
            for (ContentVersion contentVesion :[SELECT id,Title, ContentBodyId, RecordTypeId, Type_Files_fileupload__c FROM contentversion WHERE contentdocumentid IN :contentDocumentLinkIds]) {  
                System.debug('::ContentFileHelper cvname ' + contentVesion.Title);
                System.debug('::ContentFileHelper cvType_Files_fileupload__c' + contentVesion.Type_Files_fileupload__c);
                contentVersionIds.add(contentVesion.Id);
                mapIdContentVersion.put(contentVesion.Id, contentVesion);
            }

            System.debug('::mapIdContentVersion.size() ' + mapIdContentVersion.size());
            for(ID ids : contentVersionIds){
                ContentDistribution cd = new ContentDistribution();
                cd.Name = 'File ' + ids;
                cd.ContentVersionId = ids;
                cd.PreferencesAllowViewInBrowser= true;
                cd.PreferencesLinkLatestVersion=true;
                cd.PreferencesNotifyOnVisit=false;
                cd.PreferencesPasswordRequired=false;
                cd.PreferencesAllowOriginalDownload= true;
                stDocList.add(cd);
            }

            insert stDocList;

            //Add url file in Account
            List<Account> AccListUrl = new List<Account>();
            List<FIleObject__c> fileObjLst = new List<FIleObject__c>();
            //Add url file in Opportunity
            List<Opportunity> OppListUrl = [select Id, Partner_Opp_File_url__c from Opportunity where id IN : OppIds ];
            List<Account> accUrlFileLst = [select id, Name from Account where id IN : AccIds ];
        

            for(ContentDistribution fileUrl : [select id, Name, ContentVersionId, DistributionPublicUrl from ContentDistribution where id IN : stDocList]){
                
               System.debug('CONTENT FILE HELPER CADE ' + mapIdContentVersion.get(fileUrl.ContentVersionId).Type_Files_fileupload__c);
                for(Opportunity oppUrlFileId : OppListUrl){
                    oppUrlFileId.Partner_Opp_File_url__c =  fileUrl.DistributionPublicUrl;
                    //OppListUrl.add(oppUrlFileId);
                }
                for(Account accUrlFileId : accUrlFileLst){
                    FileObject__c fileobj = new FileObject__c();
                    fileobj.recordTypeId = recordTypeIdFileObject;
                    fileobj.conversionId__c = fileUrl.ContentVersionId;
                    fileobj.PublicLink__c = fileUrl.DistributionPublicUrl;
                    fileobj.Account__c = accUrlFileId.Id;
                    fileObj.Name = accUrlFileId.Name;
                    fileobj.contentId__c = mapIdContentVersion.get(fileUrl.ContentVersionId).ContentBodyId;
                    fileobj.File_Type__c = mapIdContentVersion.get(fileUrl.ContentVersionId).Type_Files_fileupload__c;
                    fileObjLst.add(fileobj);
                    System.debug(fileObjLst);
                }
            } 
            insert fileObjLst;
            update OppListUrl;        
        }
        
    }
    
    public void deleteimage(List<ContentDocumentLink> documents) {
        Set<ID> contentDocumentLinkIds = new Set<ID> ();
        Set<ID> contentVersionIds = new Set<ID> ();
        List<FileObject__c> fileObjLst = new List<FileObject__c>();

            for (ContentDocumentLink contentDocLink : documents) {  
                contentDocumentLinkIds.add(contentDocLink.contentdocumentid);
            }
            
            for (ContentVersion contentVesion :[SELECT id FROM contentversion WHERE contentdocumentid IN :contentDocumentLinkIds]) {  
                contentVersionIds.add(contentVesion.Id);
            }

            if(!contentVersionIds.isEmpty()){
                fileObjLst= [SELECT Id FROM FileObject__c WHERE conversionId__c IN:contentVersionIds];

                delete fileObjLst;
            }
        
    }
        

        

}