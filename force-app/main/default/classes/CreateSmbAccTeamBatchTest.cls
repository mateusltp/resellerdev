/**
 * @description       : 
 * @author            : AlyssonMota
 * @group             : 
 * @last modified on  : 07-15-2021
 * @last modified by  : Alysson Mota
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   07-15-2021   Alysson Mota   Initial Version
**/
@isTest
public with sharing class CreateSmbAccTeamBatchTest {
    @isTest   
    public static void createAccountTeams() {
        Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
        List<Account> accountsToInsert = new List<Account>();
        List<User> usersToInsert = new List<User>();
        List<UserAccountTeamMember> uatmToInsert = new List<UserAccountTeamMember>();
        
        User user = [SELECT Id, Name FROM User WHERE Email LIKE 'eduardo.pinheiro%' LIMIT 1];

        List<UserAccountTeamMember> userAccTeamMemberList = [
            SELECT Id, TeamMemberRole, UserId, OwnerId, User.Name, AccountAccessLevel, OpportunityAccessLevel, CaseAccessLevel
            FROM UserAccountTeamMember
            WHERE OwnerId =: user.Id
        ];
        
        for (Integer i=0; i<100; i++) {
            Account acc = new Account();
            acc.Name = 'Test Account ' + i;
            acc.RecordTypeId = accRecordTypeId;
            acc.BillingCountry = 'Brazil';
            acc.BillingState = 'São Paulo';
            acc.NumberOfEmployees = 200;
            acc.Industry = 'Airlines';
            acc.OwnerId = user.Id;
            accountsToInsert.add(acc);
        }

        insert accountsToInsert;

        Test.startTest();

            String CRON_EXP = '0 0 0 * * ? *';
            
            ScheduleCreateSmbAccTeam sch = new ScheduleCreateSmbAccTeam();
            String jobId = System.schedule('ScheduledApexTest', CRON_EXP, sch);         
            sch.execute(null);
            
        Test.stopTest();

        List<AccountTeamMember> actList = [
            SELECT Id
            FROM AccountTeamMember
        ];

        System.assertEquals(accountsToInsert.size() * userAccTeamMemberList.size(), actList.size());
    }
}