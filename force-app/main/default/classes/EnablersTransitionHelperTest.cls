/**
 * @description       : 
 * @author            : AlyssonMota
 * @group             : 
 * @last modified on  : 06-11-2021
 * @last modified by  : roei@gft.com
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   11-27-2020   Alysson Mota   Initial Version
**/
@isTest
public with sharing class EnablersTransitionHelperTest {

    @isTest
    public static void testPassEnablersToAccountFromB2BOpps() {
        Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
        Id oppRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        Id quoteRecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        
        EnablersRepository enablersRepo = new EnablersRepository();
        EnablersFactory enablersFactory = new EnablersFactory();
        
        Account acc = new Account();
        acc.RecordTypeId = accRecordTypeId;
        acc.Name = 'Test Account';
        acc.BillingCountry = 'Brazil';
        acc.BillingState = 'São Paulo';
        insert acc;

        List<Step_Towards_Success1__c> accountEnablers = enablersFactory.createEnablersForAccountIdClients(acc.Id);
        enablersRepo.create(accountEnablers);

        Opportunity opp1 = new Opportunity();
        opp1.Name = 'Test Opp';
        opp1.StageName = 'Lançado/Ganho';
        opp1.RecordTypeId = oppRecordTypeId;
        opp1.CloseDate = System.now().date();
        opp1.AccountId = acc.Id;
        insert opp1;
        
        opp1.StageName = 'Lançado/Ganho';
        update opp1; 
        
        Quote proposal1 = new Quote();
        proposal1.Name = 'Test Proposal1';
        proposal1.RecordTypeId = quoteRecordTypeId;
        proposal1.OpportunityId = opp1.Id;
        proposal1.License_Fee_Waiver__c = 'No';
        proposal1.Start_Date__c = System.now().date();
        proposal1.New_Hires__c = true;
        insert proposal1;

        ID rtId = Schema.SObjectType.Form__c.getRecordTypeInfosByDeveloperName().get('M0_12_Steps').getRecordTypeId();
        Form__c m0 = new Form__c();
        m0.RecordTypeId = rtId;
        m0.Opportunity__c= opp1.Id;
        insert m0;
       
        List<Step_Towards_Success1__c> oppEnablers = enablersFactory.createEnablersForOpportunityClients(opp1);
        
        for (Step_Towards_Success1__c step : oppEnablers) step.Achieved__c = 'Yes';
        
        enablersRepo.create(oppEnablers);

        EnablersTransitionHelper.passEnablersToAccountFromOpps(new List<Id>{opp1.Id});
    }

    @isTest
    public static void testPassEnablersToAccountFromB2BPassSMBCS() {
        Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
        Id oppRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        Id quoteRecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        
        EnablersRepository enablersRepo = new EnablersRepository();
        EnablersFactory enablersFactory = new EnablersFactory();
        
        Account acc = new Account();
        acc.RecordTypeId = accRecordTypeId;
        acc.Name = 'Test Account';
        acc.BillingCountry = 'Brazil';
        acc.BillingState = 'São Paulo';
        acc.NumberOfEmployees = 800;
        insert acc;

        List<Step_Towards_Success1__c> accountEnablers = enablersFactory.createEnablersForAccountIdClients(acc.Id);
        enablersRepo.create(accountEnablers);

        Opportunity opp1 = new Opportunity();
        opp1.Name = 'Test Opp';
        opp1.StageName = 'Lançado/Ganho';
        opp1.RecordTypeId = oppRecordTypeId;
        opp1.CloseDate = System.now().date();
        opp1.AccountId = acc.Id;
        insert opp1;
        
        opp1.StageName = 'Lançado/Ganho';
        update opp1; 
        
        Quote proposal1 = new Quote();
        proposal1.Name = 'Test Proposal1';
        proposal1.RecordTypeId = quoteRecordTypeId;
        proposal1.OpportunityId = opp1.Id;
        proposal1.License_Fee_Waiver__c = 'No';
        proposal1.Start_Date__c = System.now().date();
        proposal1.New_Hires__c = true;
        insert proposal1;

        ID rtId = Schema.SObjectType.Form__c.getRecordTypeInfosByDeveloperName().get('M0_12_Steps').getRecordTypeId();
        Form__c m0 = new Form__c();
        m0.RecordTypeId = rtId;
        m0.Opportunity__c= opp1.Id;
        insert m0;
       
        List<Step_Towards_Success1__c> oppEnablers = enablersFactory.createEnablersForOpportunityClients(opp1);
        
        for (Step_Towards_Success1__c step : oppEnablers) step.Achieved__c = 'Yes';
        
        enablersRepo.create(oppEnablers);

        EnablersTransitionHelper.passEnablersToAccountFromOpps(new List<Id>{opp1.Id});
    }

    @isTest
    public static void testPassEnablersToAccountFromSmbOpps() {
        Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
        Id oppRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SMB_New_Business').getRecordTypeId();
        Id quoteRecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        
        EnablersRepository enablersRepo = new EnablersRepository();
        EnablersFactory enablersFactory = new EnablersFactory();
        
        Account acc = new Account();
        acc.RecordTypeId = accRecordTypeId;
        acc.Name = 'Test Account';
        acc.BillingCountry = 'Brazil';
        acc.BillingState = 'São Paulo';
        insert acc;

        List<Step_Towards_Success1__c> accountEnablers = enablersFactory.createEnablersForAccountIdClients(acc.Id);
        enablersRepo.create(accountEnablers);

        Opportunity opp1 = new Opportunity();
        opp1.Name = 'Test Opp';
        opp1.StageName = 'Lançado/Ganho';
        opp1.RecordTypeId = oppRecordTypeId;
        opp1.CloseDate = System.now().date();
        opp1.AccountId = acc.Id;
        insert opp1;
        
        opp1.StageName = 'Lançado/Ganho';
        update opp1; 
        
        Quote proposal1 = new Quote();
        proposal1.Name = 'Test Proposal1';
        proposal1.RecordTypeId = quoteRecordTypeId;
        proposal1.OpportunityId = opp1.Id;
        proposal1.License_Fee_Waiver__c = 'No';
        proposal1.Start_Date__c = System.now().date();
        proposal1.New_Hires__c = true;
        
        insert proposal1;
       
        List<Step_Towards_Success1__c> oppEnablers = enablersFactory.createEnablersForOpportunityClients(opp1);
        enablersRepo.create(oppEnablers);

        EnablersTransitionHelper.passEnablersToAccountFromOpps(new List<Id>{opp1.Id});
    }

    @isTest
    public static void testCreateStepsForOpp() {
        Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
        Id oppRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        
        EnablersRepository enablersRepo = new EnablersRepository();
        EnablersFactory enablersFactory = new EnablersFactory();
        
        Account acc =  new Account();
        acc.RecordTypeId = accRecordTypeId;
        acc.Name = 'Test Account';
        acc.BillingCountry = 'Brazil';
        acc.BillingState = 'São Paulo';
        insert acc;

        List<Step_Towards_Success1__c> accountEnablers = enablersFactory.createEnablersForAccountIdClients(acc.Id);
        enablersRepo.create(accountEnablers);

        Opportunity opp1 = new Opportunity();
        opp1.Name = 'Test Opp';
        opp1.StageName = 'Lançado/Ganho';
        opp1.RecordTypeId = oppRecordTypeId;
        opp1.CloseDate = System.now().date();
        opp1.AccountId = acc.Id;
        insert opp1;
       
        List<Step_Towards_Success1__c> oppEnablers = enablersFactory.createEnablersForOpportunityClients(opp1);
        enablersRepo.create(oppEnablers);

        opp1.StageName = 'Lançado/Ganho';
        update opp1;

        List<Step_Towards_Success1__c> retrievedAccEnablers = enablersRepo.getAccountEnablers(acc);
        
        EnablersTransitionHelper.createStepsForRenegociationOpp(opp1, new Map<Id, List<Step_Towards_Success1__c>>{acc.Id => retrievedAccEnablers});
    }
    
    @isTest
    public static void testUpdateAccStepsForRenegociationOpp() {
        Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
        Id oppRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Success_Renegotiation').getRecordTypeId();
        Id quoteRecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        
        EnablersRepository enablersRepo = new EnablersRepository();
        EnablersFactory enablersFactory = new EnablersFactory();

        Account acc = new Account();
        acc.RecordTypeId = accRecordTypeId;
        acc.Name = 'Test Account';
        acc.BillingCountry = 'Brazil';
        acc.BillingState = 'São Paulo';
        insert acc;
        
        List<Step_Towards_Success1__c> accountEnablers = enablersFactory.createEnablersForAccountIdClients(acc.Id);
        enablersRepo.create(accountEnablers);
        

        Opportunity opp1 = new Opportunity();
        opp1.Name = 'Test Opp';
        opp1.StageName = 'Lançado/Ganho';
        opp1.RecordTypeId = oppRecordTypeId;
        opp1.CloseDate = System.now().date();
        opp1.AccountId = acc.Id;
        insert opp1;
        
        Quote proposal1 = new Quote();
        proposal1.Name = 'Test Proposal1';
        proposal1.RecordTypeId = quoteRecordTypeId;
        proposal1.OpportunityId = opp1.Id;
        proposal1.License_Fee_Waiver__c = 'No';
        proposal1.Start_Date__c = System.now().date();
        proposal1.New_Hires__c = true;
        
        insert proposal1;
        
        List<Step_Towards_Success1__c> oppEnablers = enablersFactory.createEnablersForOpportunityClients(opp1);
        enablersRepo.create(oppEnablers);

        EnablersTransitionHelper.passEnablersToAccountFromOpps(new List<Id>{opp1.Id});
    }


}