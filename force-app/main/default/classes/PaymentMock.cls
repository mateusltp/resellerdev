@IsTest
public class PaymentMock {
  public static Payment__c getStandard(QuoteLineItem quoteLineItem) {
    return new Payment__c(
      Billing_Day__c = '01',
      Frequency__c = 'Yearly',
      Payment_Due_Days__c = '30 days',
      Quote_Line_Item__c = quoteLineItem.Id,
      Payment_Method__c = 'Boleto'
    );
  }

  public static Payment__c getStandard(OrderItem orderItem) {
    return new Payment__c(
      Billing_Day__c = '01',
      Frequency__c = 'Yearly',
      Payment_Due_Days__c = '30 days',
      Order_Item__c = orderItem.Id,
      Payment_Method__c = 'Boleto'
    );
  }

  public static Payment__c getStandard(
    Opportunity opportunity,
    Account account
  ) {
    return new Payment__c(
      Account__c = account.Id,
      Billing_Day__c = '01',
      Frequency__c = 'Yearly',
      Opportunity__c = opportunity.Id,
      Payment_Due_Days__c = '30 days',
      Payment_Method__c = 'Boleto'
    );
  }

  public static Payment__c getStandard(Order order, Account account) {
    return new Payment__c(
      Account__c = account.Id,
      Billing_Day__c = '01',
      Frequency__c = 'Yearly',
      Order__r = order,
      Payment_Due_Days__c = '30 days',
      Payment_Method__c = 'Boleto'
    );
  }
}