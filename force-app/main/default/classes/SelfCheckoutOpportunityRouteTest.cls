@IsTest
public class SelfCheckoutOpportunityRouteTest {

  @IsTest
  public static void postTest() {

    RestRequest request = new RestRequest();
    request.requestURI = '/services/apexrest/self-checkout-opportunity';
    request.httpMethod = 'POST';
    request.requestBody = Blob.valueOf(SelfCheckoutOpportunityRequestMock.getRequestBody());

    Test.startTest();

    RestContext.response = new RestResponse();

    RestContext.request = request;
    SelfCheckoutOpportunityRoute.SelfCheckoutOpportunityResponse response = SelfCheckoutOpportunityRoute.post();

    Test.stopTest();

    System.assertEquals(
      204,
      response.getStatusCode(),
      'Error Message: ' + response.getErrorMessage()
    );
  }
  
}