@IsTest
public class WaiverMock {
  public static Waiver__c getStandard(Payment__c payment) {
    return new Waiver__c(
      End_Date__c = Date.today().addMonths(1),
      Payment__c = payment.Id,
      Percentage__c = 100,
      Start_Date__c = Date.today()
    );
  }

  public static Waiver__c getStandard(QuoteLineItem quoteLineItem) {
    return new Waiver__c(
      End_Date__c = Date.today().addMonths(1),
      Quote_Line_Item__c = quoteLineItem.Id,
      Percentage__c = 100,
      Start_Date__c = Date.today()
    );
  }

  public static Waiver__c getStandard(OrderItem orderItem) {
    return new Waiver__c(
      End_Date__c = Date.today().addMonths(1),
      Order_Item__c = orderItem.Id,
      Percentage__c = 100,
      Start_Date__c = Date.today()
    );
  }
}