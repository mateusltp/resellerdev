/**
 * @description       : 
 * @author            : Mateus Augusto - GFT (moes@gft.com)
 * @group             : 
 * @last modified on  : 06-13-2022
 * @last modified by  : Mateus Augusto - GFT (moes@gft.com)
**/
public without sharing class ResellerCreateOpportunityGO {
    
    private final String GO = 'GO';
    private final String Invalid = 'Invalid';
    private final String Analyze = 'Analyze';
    //private Id IndirectRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Indirect_Channel').getRecordTypeId();
    private Id IndirectRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Indirect_Channel_New_Business').getRecordTypeId();
    private List<Account_Request__c> accountRequestListNew = new List<Account_Request__c>();
    private Map<Id, Account_Request__c> accountRequestOldMap = (Map<Id, Account_Request__c>) Trigger.oldMap;
    private List<Account_Request__c> filteredRequestList = new List<Account_Request__c>();
    private List<Opportunity> opportunityList = new List<Opportunity>();
    private List<Contact> ownerContact = new List<Contact>();
    private List<OpportunityTeamMember> oppTeamList = new List<OpportunityTeamMember>();
    private Map<String, Id> mapPriceBooks = new Map<String, Id>();
    private String userCurrency;
    Set<String> opportunityIdSucess = new Set<String>();
    Map<Id, String> error_opportunity_map = new Map<Id, String>();

    public ResellerCreateOpportunityGO(){}    
    
    public void run(List<Account_Request__c> accountRequestListNew){
        this.accountRequestListNew = accountRequestListNew;
        this.searchPricebooks();   
        this.getPriceBook(); 
        this.filterGO();
        if(opportunityList != null){
            this.insertOpportunity(); 
            this.setOpportunityInAccountRequest();
            insert this.oppTeamList;    
           
        } 
    }
    
    public void filterGO(){
        
        for(Account_Request__c request : this.accountRequestListNew){

            Account_Request__c requestOld = new Account_Request__c();
            requestOld = this.accountRequestOldMap.get(request.Id);
           
            if(request.Engine_Status__c == GO && request.OpportunityId__c == null && !(String.isEmpty(request.Partner_Model__c))){
                if(requestOld.Engine_Status__c == Invalid || requestOld.Engine_Status__c == Analyze){
                    this.createOpportunity(request); 
                }
            }    
        }
    }
    
    public void createOpportunity(Account_Request__c request){
        Date newDate = Date.today();
        
        Opportunity newOpp = new Opportunity(
            Name = 'Indirect Channel', 
            StageName = 'Creation',
            Probability = 10,
            AccountId = request.AccountId__c,
            RecordTypeId = IndirectRecordTypeId,
            CloseDate = Date.Today().addDays(90),
            Pricebook2Id = (request.Partner_Model__c == 'Subsidy' ? this.setPriceBookInOpportunity(request) : this.mapPriceBooks.get('Intermediation')),
            TotalOpportunityQuantity = request.Total_number_of_employees__c,
            Quantity_Offer_Number_of_Employees__c= request.Total_number_of_employees__c,
            Contact__c = request.ContactId__c,
            B_M__c = (request.Partner_Model__c == 'Subsidy' ? 'Total Subsidy' : 'Intermediation'),
            Reseller_del__c = request.Partner_Account__c,
            Reseller_name__c = request.Partner_Account__c,
            CurrencyIsoCode = this.userCurrency,
            OwnerId = Test.isRunningTest() ? UserInfo.getUserId() : request.Partner_Account_Owner__c, //Setando Owner da Opp com o Executivo de Indiretos (Owner da Partner)
            GP_Executivo_de_Diretos_del__c = (request.Joint_Effort__c ? this.getDirectExecutive(request.AccountId__c) : null),
            Origin_Process__c = (request.Bulk_Operation__c) ? ResellerEnumRepository.ResellerOpportunityOriginProcess.Multiple.name() : ResellerEnumRepository.ResellerOpportunityOriginProcess.Single.name()
        );
        
        this.opportunityList.add(newOpp);
        this.filteredRequestList.add(request);  
    }

    public void insertOpportunity(){
        
        Database.SaveResult[] saveResultsOpportunity = Database.insert(this.opportunityList, false);
                
        for(Database.SaveResult sr : saveResultsOpportunity){
            if(sr.isSuccess()){
                this.opportunityIdSucess.add(sr.getId());
            }
            else{
                for(Database.Error err : sr.getErrors()) { 
                    this.error_opportunity_map.put(sr.getId(), err.getMessage());
                }
            }
        }
    }
    
    public void setOpportunityInAccountRequest(){
        for(Integer i = 0; i < this.filteredRequestList.size(); i++){

            if(this.error_opportunity_map.containsKey(this.opportunityList[i].Id)){
                filteredRequestList[i].Engine_Log__c += ' -|- ' + ResellerEnumRepository.MassiveLog.NEW_OPPORTUNITY_NOT_CREATED.name() + ' ' + String.valueOf(System.now());
                filteredRequestList[i].Error_message__c = this.error_opportunity_map.get(this.opportunityList[i].Id);
                filteredRequestList[i].Engine_Status__c = 'NO GO';
                continue;
            }

            filteredRequestList[i].Engine_Log__c += ' -|- ' + ResellerEnumRepository.MassiveLog.NEW_OPPORTUNITY_SUCCESSFULLY_CREATED.name() + ' ' + String.valueOf(System.now());
            this.filteredRequestList[i].OpportunityId__c = this.opportunityList[i].Id;

            // --- Inserindo Reseller no OpportunityTeam ---
            this.oppTeamList.add(new OpportunityTeamMember(OpportunityId = this.opportunityList[i].Id, 
                                                           UserId = UserInfo.getUserId(), 
                                                           TeamMemberRole = 'Reseller Account Executive', 
                                                           OpportunityAccessLevel = 'Edit'));
            
            // --- Inserindo Executivo Direto no OpportunityTeam ---
            if(filteredRequestList[i].Joint_Effort__c){ //Se Venda Conjunta
                this.oppTeamList.add(new OpportunityTeamMember(OpportunityId = this.opportunityList[i].Id, 
                                                               UserId = this.getDirectExecutive(filteredRequestList[i].AccountId__c), 
                                                               TeamMemberRole = 'Direct Channel Account Executive', 
                                                               OpportunityAccessLevel = 'Edit'));
            }

        }
    }

    public void searchPricebooks(){
        for (Pricebook2 pb : [SELECT Id, Business_Model__c FROM Pricebook2 WHERE Business_Model__c In ('Subsidy 5k', 'Subsidy 15k', 'Subsidy', 'Intermediation')]) {
            this.mapPriceBooks.put(pb.Business_Model__c, pb.Id);         
        }
    }
    
    public Id setPriceBookInOpportunity(Account_Request__c request){
        String priceBook = request.Indirect_Channel_Pricebook__c;
       
        switch on priceBook {
            when '5k FTEs'{
                return this.mapPriceBooks.get('Subsidy 5k');
            }
            when '15k FTEs'{
                return this.mapPriceBooks.get('Subsidy 15k');
            } 
            when 'Standard', 'Legacy Partners'{
                return this.mapPriceBooks.get('Subsidy');
            } 
        }
        return null;
    }
    
    public void getPriceBook(){
        List<User> users = [SELECT Id, Contact.AccountId, DefaultCurrencyIsoCode FROM User WHERE Id =: UserInfo.getUserId() limit 1];
        if(users.size() > 0){
            this.userCurrency = users[0].DefaultCurrencyIsoCode;
        }
    } 
    
    // --- Pegando ID do Diretor Direto ---
    public Id getDirectExecutive(Id AccountId){
        List<Account> directExecutive = [SELECT OwnerId FROM Account WHERE Id =: AccountId limit 1];    
        if(directExecutive.size() > 0){
            return directExecutive[0].OwnerId;
        }
        return null;
    }

    public static void creationStandardOffer(String opp_Id){

        List<Clients_Standard_Offer_Creation__e> event = new List<Clients_Standard_Offer_Creation__e>();
        event.add(new Clients_Standard_Offer_Creation__e(OpportunityId__c = opp_Id));
        List<Database.SaveResult> results = EventBus.publish(event);
    }

}