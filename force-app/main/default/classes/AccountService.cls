/**
 * @description       : Account Service (SOC)
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 05-20-2022
 * @last modified by  : alysson.mota@gympass.com
 * Modifications Log 
 * Ver   Date         Author                             Modification
 * 1.0   07-05-2021   gft.samuel.silva@ext.gympass.com   Initial Version
**/
public with sharing class AccountService {

    private static final PS_Constants constants = PS_Constants.getInstance();
    private static final String className = AccountService.class.getName();

    private static final Set<Id> accountPartner =
	new Set<Id>{Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(constants.ACCOUNT_RT_GYM_PARTNER).getRecordTypeId(),
	Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(constants.ACCOUNT_RT_PARTNER_FLOW_ACCOUNT).getRecordTypeId()};


    // /** Partner - Add a new child account in hierarchy *
    //  * @param aAccountForm - it's a form filled in the LWC called hierarchyAddLocation
    // */
    // public static List<Account> createChildAccountFromForm(List<Account> aAccountForm, String recordTypeDevName){
    //     try{
    //         return service(recordTypeDevName).createChildAccountFromForm(aAccountForm);  
    //     }catch (Exception e){
    //         if(e.getMessage().contains('No Implementation registered for record type')){
    //             System.debug('No Implementation registered for record type ' + recordTypeDevName);
    //             return new List<Account>();
    //         } else {
    //             throw new AccountServiceException(e.getMessage());  
    //         }
    //     }         
    // }  
    
    
    // /** 
    //  * @param aParentIdTypeOwnerLst  - Map <Id,String>  parent Id and Type of ownership of children
    // */
    // public static void updateTypeOfOwnershipInNetwork(Map<Id, String> aParentIdWithTypeOfOwnership, String recordTypeDevName){       
    //     try{
    //         service(recordTypeDevName).updateNetworkType(aParentIdWithTypeOfOwnership); 
    //     }catch (Exception e){
    //         if(e.getMessage().contains('No Implementation registered for record type')){
    //             System.debug('No Implementation registered for record type ' + recordTypeDevName);
    //         } else {
    //             throw new AccountServiceException(e.getMessage());  
    //         }
    //     }  
    // } 
    
    public List<Account> avoidDuplicateAccountName(List<Account> lstAccount){
        String recordTypeDevName = Schema.SObjectType.Account.getRecordTypeInfosById().get(lstAccount.get(0).recordTypeId).getDeveloperName();      
        try{
            if(recordTypeDevName == constants.ACCOUNT_RT_GYM_PARTNER || recordTypeDevName == constants.ACCOUNT_RT_PARTNER_FLOW_ACCOUNT ){
                return service(recordTypeDevName).avoidDuplicateAccountName(lstAccount);
            }  
            else {
                return new List<Account>();
            }        
        }catch (Exception e){
            createLog(e, 'avoidDuplicateAccountName', JSON.serialize(lstAccount), null);
            if(e.getMessage().contains(constants.ERROR_NO_IMPLEMENTATION_FOR_RT)){
                System.debug(constants.ERROR_NO_IMPLEMENTATION_FOR_RT + ': ' + recordTypeDevName);
                return new List<Account>();
            } else {
                throw new AccountServiceException(e.getMessage() + ' - ' + e.getLineNumber());
            }
        }  
        
      }

    public void setAccountTypeProspect(List<Account> lstAccount)
    {
        String recordTypeDevName = Schema.SObjectType.Account.getRecordTypeInfosById().get(lstAccount.get(0).recordTypeId).getDeveloperName();
        try
        {
            if (recordTypeDevName == constants.ACCOUNT_RT_GYM_PARTNER || recordTypeDevName == constants.ACCOUNT_RT_PARTNER_FLOW_ACCOUNT)
            {
                service(recordTypeDevName).setAccountTypeProspect(lstAccount);
            }
        }
        catch (Exception e)
        {
            createLog(e, 'setAccountTypeProspect', JSON.serialize(lstAccount), null);
            if (e.getMessage().contains(constants.ERROR_NO_IMPLEMENTATION_FOR_RT))
            {
                System.debug(constants.ERROR_NO_IMPLEMENTATION_FOR_RT + ': ' + recordTypeDevName);
            }
            else
            {
                throw new AccountServiceException(e.getMessage());
            }
        }

    }

    public void getAccountWishlistFromParent(List<Account> lstAccount)
    {
        String recordTypeDevName = Schema.SObjectType.Account.getRecordTypeInfosById().get(lstAccount.get(0).recordTypeId).getDeveloperName();
        try
        {
            if (recordTypeDevName == constants.ACCOUNT_RT_GYM_PARTNER || recordTypeDevName == constants.ACCOUNT_RT_PARTNER_FLOW_ACCOUNT)
            {
                service(recordTypeDevName).getAccountWishlistFromParent(lstAccount);
            }
        }
        catch (Exception e)
        {
            createLog(e, 'getAccountWishlistFromParent', JSON.serialize(lstAccount), null);
            if (e.getMessage().contains(constants.ERROR_NO_IMPLEMENTATION_FOR_RT))
            {
                System.debug(constants.ERROR_NO_IMPLEMENTATION_FOR_RT + ': ' + recordTypeDevName);
            }
            else
            {
                throw new AccountServiceException(e.getMessage());
            }
        }

    }

    private static String getRecordTypeAndSObjectName (String recordTypeDevName){
        return constants.ACCOUNT_OBJECT+'.'+recordTypeDevName;
    }
    
    private static IAccountService service(String recordTypeDevName) {
        try{
            return (IAccountService) Application.ServiceByRecordType.newInstanceByRecordType(getRecordTypeAndSObjectName(recordTypeDevName));
        }catch (Exception e){
            createLog(e, 'service', null, null);
            throw new AccountServiceException(e.getMessage());
        }  
      
    }

    public List<Account> wishListAndTierDependency(List<Account> lLstAccount) {
        String recordTypeDevName = Schema.SObjectType.Account.getRecordTypeInfosById().get(lLstAccount.get(0).recordTypeId).getDeveloperName();      
        try{
            if(recordTypeDevName == constants.ACCOUNT_RT_GYM_PARTNER || recordTypeDevName == constants.ACCOUNT_RT_PARTNER_FLOW_ACCOUNT){
                return service(recordTypeDevName).wishListAndTierDependency(lLstAccount);
            }  
            else {
                return new List<Account>();
            }}catch (Exception e){
                createLog(e, 'wishListAndTierDependency', JSON.serialize(lLstAccount), null);
                if(e.getMessage().contains(constants.ERROR_NO_IMPLEMENTATION_FOR_RT)){
                    System.debug(constants.ERROR_NO_IMPLEMENTATION_FOR_RT + ': ' + recordTypeDevName);
                    return new List<Account>();
                } else {
                    throw new AccountServiceException(e.getMessage());  
                }
            }
    }  

    public static Object buildNetwork(Id recordId, String recordTypeDevName){    
        try{
            return service(recordTypeDevName).buildNetwork(recordId); 
        }catch (Exception e){
            createLog(e, 'buildNetwork', null, recordId);
            throw new AccountServiceException(e.getMessage());  
        }
    }

    public static Object buildNetworkForContracts(Id accountId, Id oppId, String recordTypeDevName){    
        try{
            return service(recordTypeDevName).buildNetworkForContracts(accountId, oppId); 
        }catch (Exception e){
            createLog(e, 'buildNetworkForContracts', null, oppId);
            throw new AccountServiceException(e.getMessage());  
        }
    }

    public static List<AccountContactRelation> getContactRelations(Set<Id> recordIds, String recordTypeDevName){
        try{
            return service(recordTypeDevName).getContactRelations(recordIds);
        }catch (Exception e){
            createLog(e, 'getContactRelations', JSON.serialize(recordIds), null);
            throw new AccountServiceException(e.getMessage());
        }
    }

    public static List<AccountContactRelation> getContactsFromParentThatAreNotRelatedToChild(Id childAccountId, Id parentAccountId, String recordTypeDevName){
        try{
            return service(recordTypeDevName).getContactsFromParentThatAreNotRelatedToChild(childAccountId, parentAccountId);
        }catch (Exception e){
            createLog(e, 'getContactsFromParentThatAreNotRelatedToChild', null, childAccountId);
            throw new AccountServiceException(e.getMessage());
        }
    }

    public static List<AccountContactRelation> getContactsFromParent(Id recordId, String recordTypeDevName){    
        try{
            return service(recordTypeDevName).getContactsFromParent(recordId); 
        }catch (Exception e){
            createLog(e, 'getContactsFromParent', null, recordId);
            throw new AccountServiceException(e.getMessage());  
        }
    } 

    public void avoidDuplicatedNetworkIds (List<Account> lLstAccount) {
        String recordTypeDevName = Schema.SObjectType.Account.getRecordTypeInfosById().get(lLstAccount.get(0).recordTypeId).getDeveloperName();      





        try{
            if(recordTypeDevName == constants.ACCOUNT_RT_GYM_PARTNER || recordTypeDevName == constants.ACCOUNT_RT_PARTNER_FLOW_ACCOUNT){
                service(recordTypeDevName).avoidDuplicatedNetworkIds(lLstAccount);
            }  
           }catch (Exception e){
                createLog(e, 'avoidDuplicatedNetworkIds', JSON.serialize(lLstAccount), null);
                if(e.getMessage().contains(constants.ERROR_NO_IMPLEMENTATION_FOR_RT)){
                    System.debug(constants.ERROR_NO_IMPLEMENTATION_FOR_RT + ': ' + recordTypeDevName);
                    
                } else {
                    throw new AccountServiceException(e.getMessage() + ' - ' + e.getLineNumber());
                }
            }
    }  

	public static List<AccountPartnerServiceImpl.Response> sendAccountsForExclusivityApproval(List<Id> accountIds, String comments, String recordTypeDevName){
        return service(recordTypeDevName).sendAccountsForExclusivityApproval(accountIds, comments);
    }

    private static void createLog(Exception e, String methodName, String dataScope, Id objectId) {
        DebugLog__c log = new DebugLog__c(
            Origin__c = '['+className+']['+methodName+']',
            LogType__c = constants.LOGTYPE_ERROR,
            RelatedToSquad__c = constants.LOGTYPE_SQUAD_PARTNERS,
            ExceptionMessage__c = e.getMessage(),
            StackTrace__c = e.getStackTraceString(),
            DataScope__c = dataScope,
            ObjectId__c = objectId
        );
        Logger.createLog(log);
    }

    public class AccountServiceException extends Exception {}
}