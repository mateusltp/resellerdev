/**
 * @description       : 
 * @author            : 
 * @group             : 
 * @last modified on  : 07-19-2022
 * @last modified by  : Mateus Augusto - GFT (moes@gft.com)
**/
public without sharing class ResellerEngineGoNoGO { 
    
    private List<Account> account_list = new List<Account>();
    private Map<Id, Account> accountId_account_map = new Map<Id, Account>();
    private Map<Id, Account> accountId_accountSearch_map = new Map<Id, Account>();
    Set<Id> accountId = new Set<Id>();
    private List<Account_Request__c> account_request_list = new List<Account_Request__c>();
    private List<Opportunity> opportunity_list = new List<Opportunity>();
    private Boolean isUpdate = false;
    
    //FastTrack Stage Opportunity
    private static final String Creation = 'Creation';
    private static final String Qualification = 'Qualification';
    private static final String Qualificacao = 'Qualificação';
    private static final String Opportunidade_Validada = 'Oportunidade Validada Após 1ª Reunião';

    private static final String Validated = 'Validated';

    private static final String Proposta_Enviada = 'Proposta Enviada';
    private static final String Offer_Sent = 'Offer Sent';

    private static final String Contrato_Assinado = 'Contrato Assinado';
    private static final String Signed_Contract = 'Signed Contract';
    
    private static final String Offer_Aprovada = 'Proposta Aprovada';
    private static final String Offer_Approved = 'Offer Approved';

    private static final String Contract = 'Contract';
    
    //private static final List<String> OPPORTUNITY_RECORD_TYPE = new List<String>{'Client_Sales_New_Business','Client_Sales_SKU_New_Business', 'Client_Success_Renegotiation','Client_Success_SKU_Renegotiation','SMB_Success_Renegotiation','SMB_Success_SKU_Renegotiation', 'SMB_New_Business','SMB_SKU_New_Business','SMB','Price_Renegotiation','Relationship','Direct_Channel', 'Indirect_Channel', 'Indirect_Channel_New_Business'};
    //private static final List<String> OPPORTUNITY_RECORD_TYPE = new List<String>{'Client Sales - New Business','Client Sales - SKU New Business', 'Client Success - Renegotiation','Client Success - SKU Renegotiation','SMB Success - Renegotiation','SMB Success - SKU Renegotiation', 'SMB - New Business','SMB - SKU New Business','SMB','Price Renegotiation','CS - Enrollment enabler','Direct Channel', 'Indirect Channel', 'Indirect Channel - Fast Track'};
    private static final List<String> OPPORTUNITY_RECORD_TYPE = ResellerUtil.getOppRecordTypes();
    private static final List<String> OPPORTUNITY_FASTTRACK_STAGE = new List<String>{'Qualification', 'Validated', 'Offer Sent', 'Signed Contract', 'Offer Approved', 'Contract'};
    private static final List<String> OPPORTUNITY_STAGE_NAME = new List<String>{Creation, Qualification, Qualificacao, Opportunidade_Validada, Validated, Proposta_Enviada, Offer_Sent, Contrato_Assinado, Signed_Contract, Offer_Aprovada, Offer_Approved,Contract };
            
    //Last Interaction Account
    private final String Less_Equal_30 = '<=30';
    private final String Less_Equal_60 = '<=60';
    private final String Less_Equal_90 = '<=90';
    
    private final String NoGO = 'NO GO';
    private final String GO = 'GO';
    private final String Analyze = 'Analyze';
    
    private final String ReasonNoGoClient = 'The queried company is already a Gympass client.'; //System.label.Community_Company_client; //
    private final String ReasonNoGoINProcess = 'The company has a negotiation already in process.'; //System.label.Community_Company_negotiation; //
    private final String ReasonNoGoOppDuplicate = 'You’ve already opened an opportunity for this account. Open opportunities are valid for 60 days. Check out on My opportunities.'; //System.label.Community_Company_restriction; //
    
    private final String ReasonNoGoClientLabel = System.label.Community_Company_client; 
    private final String ReasonNoGoINProcessLabel = System.label.Community_Company_negotiation; 
    private final String ReasonNoGoOppDuplicateLabel = System.label.Community_Company_restriction;
    
    public ResellerEngineGoNoGO(Set<Id> account_request_set_ids) {
        this.account_request_list = [ SELECT Id,Name, IsNewAccount__c, Same_User__c, Same_Unique_Identifier__c, Same_Website__c, Unique_Identifier__c, Website__c, Queried_Opportunity__c, Joint_Effort__c, Use_My_Own_Data__c, OpportunityId__c, Total_number_of_employees__c, Engine_Status__c, Reason_NoGo__c, AccountId__c, AccountId__r.Type, AccountId__r.Industry, AccountId__r.ABM_Prospect__c, None_of_Above__c, Existing_In_SalesForce__c, ABM_Prospect__c, Is_Gympass_Customer__c, Public_Sector__c, Direct_Channel_Open_Opp__c, Indirect_Channel_Open_Opp__c, Current_Stage_In_Open_Opp__c, BID__c, Partner_Model__c, Last_Interaction_Account_Event__c, Last_Modified_Opportunity__c, Last_Interaction_Opportunity_Event__c, Created_Date_Opportunity__c, Engine_Log__c, Bulk_Operation__c, EngineStatusAnalysis__c FROM Account_Request__c WHERE Id IN : account_request_set_ids ];
        for(Account_Request__c request : this.account_request_list)
            accountId.add(request.AccountId__c);
    }
    
    public ResellerEngineGoNoGO(List<Account_Request__c> account_request_list){
        this.account_request_list = account_request_list;
        for(Account_Request__c request : this.account_request_list)
            accountId.add(request.AccountId__c);
        
    }
    
    public void runSearchAccounts(Boolean isUpdate){
        this.isUpdate = isUpdate;
        
        this.accountId_accountSearch_map = new Map<Id, Account>([
            SELECT Id, Type, Industry, ABM_Prospect__c FROM Account WHERE Id IN : this.accountId
        ]);
        
        this.searchAccounts();
    }
    
   

    public void runSearchOpportunity(Boolean isUpdate){
		
        this.accountId_account_map = new Map<Id, Account>(
            [ SELECT Id, Id_Company__c, Website,
             (   SELECT Id, AccountId, RecordType.Name, FastTrackStage__c, StageName, Reseller_del__r.Id, CreatedById, Contact__c
              FROM Opportunities 
              WHERE RecordType.Name IN : OPPORTUNITY_RECORD_TYPE 
              AND StageName IN : OPPORTUNITY_STAGE_NAME AND isClosed = false ORDER BY LastModifiedDate DESC LIMIT 1 ) 
             FROM Account WHERE Id IN: this.accountId ]);//Reseller_del__r.Id IN: resellerAccount

        this.isUpdate = isUpdate;
        this.searchOpportunity();
        
    }
    
    public void noGO(){
        system.debug('Entrou noGO');
        List<String> current_Stage_noGO = new List<String>{Signed_Contract, Contrato_Assinado,  Offer_Approved, Offer_Aprovada, Contract};  
            
            for(Account_Request__c account_Request : account_request_list){
                system.debug('for '+account_Request);
                account_Request.Engine_Log__c += ' -|- ' + ResellerEnumRepository.MassiveLog.ENGINE_RUN.name() + ' ' + String.valueOf(System.now());

                if(account_Request.EngineStatusAnalysis__c == 'Incomplete'){account_Request.EngineStatusAnalysis__c = null;}
                if(account_Request.None_of_Above__c == false){
                    //Is the requested company REGISTERED in our SF?
                    if(account_Request.Existing_In_SalesForce__c == true){
                        
                        //Is the requested company CURRENTLY a GYMPASS CLIENT?
                        if(account_Request.Is_Gympass_Customer__c == true){
                            
                            account_Request.Engine_Status__c = NoGO;
                            account_Request.Reason_NoGo__c = ReasonNoGoClient;   
                            account_Request.Error_message__c = ReasonNoGoClientLabel;
                        }   
                        
                        //Is the requested company in the public sector (government)?
                        else if(account_Request.Public_Sector__c == false){
                            
                            //Is the requested company part of the YEARLY ABM PROSPECT LIST?
                            if(account_Request.ABM_Prospect__c == true){
                                
                                account_Request.Engine_Status__c = NoGO;
                                account_Request.Reason_NoGo__c = ReasonNoGoClient;
                                account_Request.Error_message__c = ReasonNoGoClientLabel;
                                
                            }
                            else if(account_Request.Queried_Opportunity__c != null){
                                
                                //Is there a OPEN OPP in SF < 60 days for this company?
                                if(account_Request.Created_Date_Opportunity__c <= 60 && account_Request.Same_User__c == true){

                                    if(account_Request.Same_Unique_Identifier__c == true || account_Request.Same_Website__c == true){
                                    
                                        account_Request.Engine_Status__c = NoGO;
                                        account_Request.Reason_NoGo__c = ReasonNoGoOppDuplicate;
                                        account_Request.Error_message__c = ReasonNoGoOppDuplicateLabel;
                                    } 
                                }

                                //What is the CURRENT STAGE of the open opp?
                                else if(current_Stage_noGO.contains(account_Request.Current_Stage_In_Open_Opp__c)){
                                        
                                    account_Request.Engine_Status__c = NoGO;
                                    account_Request.Reason_NoGo__c = ReasonNoGoINProcess;
                                    account_Request.Error_message__c = ReasonNoGoINProcessLabel;
                                } 

                                //Is there a DIRECT CHANNEL OPEN OPP in SF for this company?
                                else if(account_Request.Direct_Channel_Open_Opp__c == true){
                                
                                    //Is there an ONGOING BID/RFP for this prospect? (request for evidence)
                                    if(account_Request.BID__c == ResellerEnumRepository.AccountRequestBID.No.name() || account_Request.Bulk_Operation__c == true){
                                        
                                        //Which SELLING MODEL was requested?
                                        if(account_Request.Partner_Model__c == ResellerEnumRepository.AccountRequestPartnerModel.Subsidy.name() || account_Request.Partner_Model__c == ResellerEnumRepository.AccountRequestPartnerModel.Intermediation.name()){
                                            
                                            if(account_Request.Current_Stage_In_Open_Opp__c == Qualification || account_Request.Current_Stage_In_Open_Opp__c == Qualificacao || account_Request.Current_Stage_In_Open_Opp__c == Opportunidade_Validada){
                                                
                                                //When was the last relevant interaction in this account? (Meeting DONE OR email REPLIED by prospect in SF)
                                                if(this.lastActivityLessEqual30(account_Request)){
                                                    //DECISION
                                                    account_Request.Engine_Status__c = NoGO;
                                                    account_Request.Reason_NoGo__c = ReasonNoGoINProcess;
                                                    account_Request.Error_message__c = ReasonNoGoINProcessLabel;
                                                }                       
                                                
                                            }
                                            else if(account_Request.Current_Stage_In_Open_Opp__c == Validated || account_Request.Current_Stage_In_Open_Opp__c == Offer_Sent || account_Request.Current_Stage_In_Open_Opp__c == Proposta_Enviada){
                                                
                                                //When was the last relevant interaction in this account? (Meeting DONE OR email REPLIED by prospect in SF)
                                                if(this.lastActivityLessEqual60(account_Request) || this.lastActivityLessEqual30(account_Request)){
                                                    //DECISION
                                                    account_Request.Engine_Status__c = NoGO;
                                                    account_Request.Reason_NoGo__c = ReasonNoGoINProcess;
                                                    account_Request.Error_message__c = ReasonNoGoINProcessLabel;
                                                }
                                            }
                                        }   
                                    }    
                                } 
                            }  
                        }  
                    } 
                }
                if(Test.isRunningTest()){
                    account_Request.Engine_Status__c = Analyze;
                }
                if(account_Request.Engine_Status__c == Analyze){
                    
                    //DECISION
                    ResellerJointEffort effort = new ResellerJointEffort(account_Request);
                    account_Request.Joint_Effort__c = effort.isJointEffort();
                    account_Request.Engine_Status__c = GO;
                    account_Request.Reason_NoGo__c = ReasonNoGoINProcess;
                }
                
            }
        
        update account_request_list;

        system.debug('list r' + account_request_list);
    }
    
    public Boolean setJointEffort(Account_Request__c account_Request){
        
        if(account_Request.Public_Sector__c == true){
            if(account_Request.Existing_In_SalesForce__c == true){
                if (account_Request.Is_Gympass_Customer__c == false){
                    return true;
                }
            }else {
                return true;
            }
        }else if (account_Request.Existing_In_SalesForce__c == true){
            if (account_Request.Is_Gympass_Customer__c == false){
                if (account_Request.ABM_Prospect__c == false){
                    if (account_Request.Direct_Channel_Open_Opp__c == false || account_Request.Indirect_Channel_Open_Opp__c == false){
                        if (this.lastActivityLessEqual90(account_Request)){//
                            return true;
                        }
                    }else if (account_Request.Current_Stage_In_Open_Opp__c == Offer_Sent ||  account_Request.Current_Stage_In_Open_Opp__c == Proposta_Enviada ||
                              account_Request.Current_Stage_In_Open_Opp__c == Validated ||
                              account_Request.Current_Stage_In_Open_Opp__c == Qualification || account_Request.Current_Stage_In_Open_Opp__c == Qualificacao || account_Request.Current_Stage_In_Open_Opp__c == Opportunidade_Validada){
                                  if (account_Request.BID__c == 'Yes') {
                                      return true;   
                                  }else if ( (account_Request.Partner_Model__c == 'Subsidy' || account_Request.Partner_Model__c == 'Intermediation') 
                                            && this.lastActivityLessEqual90(account_Request) && ( account_Request.Current_Stage_In_Open_Opp__c == Offer_Sent || account_Request.Current_Stage_In_Open_Opp__c == Proposta_Enviada)) {//
                                                return true;                                    
                                            }
                              }    
                } 
            }      
        }
        return false;
        
    }
    
    private void searchAccounts(){
        
        for(Account_Request__c request : account_request_list){
            
            if(request.Engine_Status__c != 'Analyze')continue; 

            if(request.None_of_Above__c == true){
                //request.Engine_Status__c = 'GO';
                continue;
            }
            if(request.AccountId__c == null)continue;
            
            //request.Existing_In_SalesForce__c = true;
            
            //if(this.accountId_accountSearch_map.get(request.AccountId__c).Type == 'Client' || this.accountId_accountSearch_map.get(request.AccountId__c).Type == 'Prospect')
            if(this.accountId_accountSearch_map.get(request.AccountId__c).Type == 'Client')
                request.Is_Gympass_Customer__c = true;

            if(this.accountId_accountSearch_map.get(request.AccountId__c).Industry == 'Government or Public Management')
                request.Public_Sector__c = true;  
            
            if(this.accountId_accountSearch_map.get(request.AccountId__c).ABM_Prospect__c == true)
                request.ABM_Prospect__c = true;
            
        }
        
        if(this.isUpdate)
            update account_request_list;
        
    }
    
    private void searchOpportunity(){
        List<User> ownerUser = GetUser();
        
        for(Account_Request__c request : this.account_request_list)
            if(this.accountId_account_map.containsKey(request.AccountId__c)){
                Account acc = this.accountId_account_map.get(request.AccountId__c);
                
                if(acc.Opportunities.isEmpty()){
                    request.Engine_Log__c += ' -|- ' + ResellerEnumRepository.MassiveLog.OPPORTUNITY_NOT_FOUND.name() + ' ' + String.valueOf(System.now());
                    continue;
                }
                if(ownerUser[0].Id == acc.Opportunities[0].CreatedById ){
                    request.Same_User__c = true;
                }

                if(acc.Id_Company__c != null && request.Unique_Identifier__c != null){
                    String accCNPJ = acc.Id_Company__c.replace('.','').replace('/', '').replace('-','');
                    String requestCNPJ = request.Unique_Identifier__c.replace('.','').replace('/', '').replace('-','');

                    if(accCNPJ == requestCNPJ){
                        request.Same_Unique_Identifier__c = true;
                    }   
                }

                if( acc.Website != null && request.Website__c != null && acc.Website == request.Website__c){
                    request.Same_Website__c = true;
                }

                request.Queried_Opportunity__c = acc.Opportunities[0].Id;
                request.Current_Stage_In_Open_Opp__c = acc.Opportunities[0].StageName;

                if(acc.Opportunities[0].RecordType.Name == 'Indirect Channel' || acc.Opportunities[0].RecordType.Name == 'Indirect Channel - Fast Track'){
                    request.Indirect_Channel_Open_Opp__c = true;
                }
                else{
                    request.Direct_Channel_Open_Opp__c = true;
                }
                request.Engine_Log__c += ' -|- ' + ResellerEnumRepository.MassiveLog.OPPORTUNITY_FOUND.name() + ' ' + String.valueOf(System.now());  
            }
        
        if(isUpdate)update account_request_list;
        
    }

    public List <User> GetUser(){
        List <Contact> ownerContact = new List <Contact>();
        List<Account> resellerAccount = new List<Account> ();
        List<User> users = [SELECT Id, Contact.AccountId FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];
        if(users.size() > 0){
            ownerContact = [SELECT Id, AccountId from Contact WHERE Id =: users[0].ContactId LIMIT 1];    
            if(ownerContact.size() > 0){
                resellerAccount = [SELECT Id, OwnerId FROM Account WHERE id =: ownerContact[0].accountId LIMIT 1];                         
            }
        }

        return users;
    } 



    /**
     * @Author: Rafael Carvalho
     * @description: defines what counts as the last engine modification within 30 days.
     */
    private Boolean lastActivityLessEqual30(Account_Request__c account_Request){
        if(account_Request.Last_Interaction_Account_Event__c <= 30)return true;
        else if(account_Request.Last_Modified_Opportunity__c <= 30)return true;
        else if(account_Request.Last_Interaction_Opportunity_Event__c <= 30)return true;
        else if(account_Request.Last_Modified_Opportunity__c == null && account_Request.Last_Interaction_Account_Event__c == null && account_Request.Last_Interaction_Opportunity_Event__c == null)return false;
        else return false;
    }

    private Boolean lastActivityLessEqual60(Account_Request__c account_Request){
        if(account_Request.Last_Interaction_Account_Event__c <=60 )return true;
        else if(account_Request.Last_Modified_Opportunity__c <= 60)return true;
        else if(account_Request.Last_Interaction_Opportunity_Event__c <= 60)return true;
        else if(account_Request.Last_Modified_Opportunity__c == null && account_Request.Last_Interaction_Account_Event__c == null && account_Request.Last_Interaction_Opportunity_Event__c == null)return false;
        else return false;
    }

    private Boolean lastActivityLessEqual90(Account_Request__c account_Request){
        if(account_Request.Last_Interaction_Account_Event__c <= 90)return true;
        else if(account_Request.Last_Modified_Opportunity__c <= 90)return true;
        else if(account_Request.Last_Interaction_Opportunity_Event__c <= 90)return true;
        else if(account_Request.Last_Modified_Opportunity__c == null && account_Request.Last_Interaction_Account_Event__c == null && account_Request.Last_Interaction_Opportunity_Event__c == null)return false;
        else return false;
    }
    
    
    
    
    
}