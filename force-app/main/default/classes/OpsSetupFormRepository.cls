/**
 * @description       : 
 * @author            : Samuel Silva - GFT (slml@gft.com)
 * @group             : 
 * @last modified on  : 10-07-2020
 * @last modified by  : Samuel Silva - GFT (slml@gft.com)
 * Modifications Log 
 * Ver   Date         Author                              Modification
 * 1.0   09-29-2020   Samuel Silva - GFT (slml@gft.com)   Initial Version
**/
public with sharing class OpsSetupFormRepository {
 
    
    public OpsSetupFormRepository() {
  
    }

 // opsForm.Enterprise_Subscription_Fee__c = to.proposal.accessFee.sObj.id;
        // opsForm.SetupFee__c = to.proposal.setupFee.sObj.id;
        // opsForm.Professional_Services_Setup_Fee__c = to.proposal.proServicesOneFee.sObj.id;
        // opsForm.Professional_Services_Maintenance_Fee__c = to.proposal.proServicesMainFee.sObj.id;
        // to.dealHierarchy.totalOfEmployees;
    

    public Ops_Setup_Validation_Form__c byOppId (ID oppId){
        return [SELECT ID,Name,Approver__c, Status__c, 
                recordtypeId,Opportunity__c,Account_Information__c,
                Proposal_Information__c,Setup_Fee__c,Enterprise_Subscription_Fee__c,
                Professional_Services_Setup_Fee__c,Professional_Services_Maintenance_Fee__c, Region__c,
                Is_this_a_Global_Deal__c,Countries_in_the_Deal__c,Sales_Executive__c    
                FROM Ops_Setup_Validation_Form__c
                WHERE Opportunity__c =: oppId               
                ORDER BY CreatedDate DESC
                LIMIT 1
            ];
    }

    
    public Ops_Setup_Validation_Form__c byId (ID recordId){
        return [SELECT ID,Name,Approver__c, Status__c, recordtypeId,
                        Opportunity__c,Account_Information__c,Proposal_Information__c,
                        Setup_Fee__c,Enterprise_Subscription_Fee__c,    
                        Professional_Services_Setup_Fee__c,Professional_Services_Maintenance_Fee__c,Region__c,
                        Is_this_a_Global_Deal__c,Countries_in_the_Deal__c,Sales_Executive__c   
                FROM Ops_Setup_Validation_Form__c
                WHERE Id =: recordId               
            ];
    }
    
    public Ops_Setup_Validation_Form__c addOrEdit (Ops_Setup_Validation_Form__c opsform){
        Database.upsert(opsform);
        return opsform;
    }
}