/**
 * @description       : 
 * @author            : bruno.mendes@gympass.com
 * @group             : 
 * @last modified on  : 03/05/2022
 * @last modified by  : bruno.mendes@gympass.com
**/
@IsTest
public with sharing class AccountBankAccountSelector_TGN_TEST {
    @TestSetup
    static void setup() {
        Account account = PartnerDataFactory.newAccount();
        insert account;

        Bank_Account__c bankAccount = PartnerDataFactory.newBankAcct();
        insert bankAccount;

        Acount_Bank_Account_Relationship__c accountBankAccountRelationship = PartnerDataFactory.newBankAcctRel(account.Id, bankAccount);
        insert accountBankAccountRelationship;
    }

    @isTest
    static void getSObjectFieldList_Test(){

        List<Schema.SObjectField> schSObjLst = new List<Schema.SObjectField> {
                Acount_Bank_Account_Relationship__c.Id,
                Acount_Bank_Account_Relationship__c.Name
        };

        Test.startTest();
        System.assertEquals(schSObjLst, new AccountBankAccountSelector().getSObjectFieldList());
        Test.stopTest();
    }

    @isTest
    static void getSObjectType_Test(){

        Test.startTest();
        System.assertEquals(Acount_Bank_Account_Relationship__c.sObjectType, new AccountBankAccountSelector().getSObjectType());
        Test.stopTest();
    }

    @isTest
    static void selectById_Test(){

        List<Acount_Bank_Account_Relationship__c> acountBankAccountRelationships = [SELECT Id FROM Acount_Bank_Account_Relationship__c LIMIT 1];
        Set<Id> accountBankAccountIds = (new Map<Id, Acount_Bank_Account_Relationship__c>(acountBankAccountRelationships)).keySet();

        Test.startTest();
        system.assertEquals(acountBankAccountRelationships[0].Id, new AccountBankAccountSelector().selectById(accountBankAccountIds)[0].Id);
        Test.stopTest();
    }

    @isTest
    static void selectBankAccToSetUp_Test(){

        List<Acount_Bank_Account_Relationship__c> acountBankAccountRelationships = [SELECT Id, Account__c FROM Acount_Bank_Account_Relationship__c LIMIT 1];

        Test.startTest();
        system.assertEquals(acountBankAccountRelationships[0].Id, new AccountBankAccountSelector().selectBankAccToSetUp(new Set<Id> {acountBankAccountRelationships[0].Account__c})[0].Id);
        Test.stopTest();
    }

    @isTest
    static void selectBankAccountByAccountId_Test(){

        List<Acount_Bank_Account_Relationship__c> acountBankAccountRelationships = [SELECT Id, Account__c FROM Acount_Bank_Account_Relationship__c LIMIT 1];

        Test.startTest();
        system.assertEquals(acountBankAccountRelationships[0].Id, new AccountBankAccountSelector().selectBankAccountByAccountId(new Set<Id> {acountBankAccountRelationships[0].Account__c}).values()[0].Id);
        Test.stopTest();
    }

    @isTest
    static void selectAccountBankAccountRelationshipsByAccountIds_Test(){

        List<Acount_Bank_Account_Relationship__c> acountBankAccountRelationships = [SELECT Id, Account__c FROM Acount_Bank_Account_Relationship__c LIMIT 1];

        Test.startTest();
        system.assertEquals(acountBankAccountRelationships[0].Id, new AccountBankAccountSelector().selectAccountBankAccountRelationshipsByAccountIds(new Set<Id> {acountBankAccountRelationships[0].Account__c})[0].Id);
        Test.stopTest();
    }
}