/**
 * @description       : 
 * @author            : bruno.mendes@gympass.com
 * @group             : 
 * @last modified on  : 28/04/2022
 * @last modified by  : bruno.mendes@gympass.com
**/

@IsTest
private class OpsFormDataProviderTest {
    @IsTest
    static void getData_Test() {
        fflib_ApexMocks mocks = new fflib_ApexMocks();

        OpportunitySelector mockOpportunitySelector = (OpportunitySelector)mocks.mock(OpportunitySelector.class);
        OpsSetupFormSelector mockOpsSetupSelector = (OpsSetupFormSelector)mocks.mock(OpsSetupFormSelector.class);
        AccountContractAgreementRelSelector mockAccContAgreementRelSelector = (AccountContractAgreementRelSelector)mocks.mock(AccountContractAgreementRelSelector.class);

        Id mockAccountId = fflib_IDGenerator.generate(Account.SObjectType);

        Id mockOpportunityId = fflib_IDGenerator.generate(Opportunity.SObjectType);
        Opportunity mockOpportunity = new Opportunity(Id = mockOpportunityId, AccountId = mockAccountId);
        List<Opportunity> mockOpportunities = new List<Opportunity>{mockOpportunity};

        Id mockAccContAgreementId = fflib_IDGenerator.generate(Account_Contract_Agreement_Relationship__c.SObjectType);
        Account_Contract_Agreement_Relationship__c mockAccContAgreement = new Account_Contract_Agreement_Relationship__c(Id = mockAccContAgreementId, AccountId__c = mockAccountId);
        Map<Id, List<Account_Contract_Agreement_Relationship__c>> mockAccContAgreementsByOppId = new Map<Id, List<Account_Contract_Agreement_Relationship__c>>{mockOpportunityId => new List<Account_Contract_Agreement_Relationship__c> {mockAccContAgreement}};

        Id mockOpsSetupValFormId = fflib_IDGenerator.generate(Ops_Setup_Validation_Form__c.SObjectType);
        Ops_Setup_Validation_Form__c mockOpsSetupValForm = new Ops_Setup_Validation_Form__c(Id = mockOpsSetupValFormId, Account_Information__c = mockAccountId, Opportunity__c = mockOpportunityId);
        Map<Id, Ops_Setup_Validation_Form__c> mockOpsSetupValFormsByAccountId = new Map<Id, Ops_Setup_Validation_Form__c> {mockAccountId => mockOpsSetupValForm};

        mocks.startStubbing();

        mocks.when(mockOpportunitySelector.sObjectType()).thenReturn(Opportunity.SObjectType);
        mocks.when(mockOpportunitySelector.selectById(new Set<Id>{mockOpportunityId})).thenReturn(mockOpportunities);

        mocks.when(mockOpsSetupSelector.sObjectType()).thenReturn(Ops_Setup_Validation_Form__c.SObjectType);
        mocks.when(mockOpsSetupSelector.selectOpsSetupFormForPartner(mockOpportunityId, new Set<Id> {mockAccountId})).thenReturn(mockOpsSetupValFormsByAccountId);

        mocks.when(mockAccContAgreementRelSelector.sObjectType()).thenReturn(Account_Contract_Agreement_Relationship__c.SObjectType);
        mocks.when(mockAccContAgreementRelSelector.selectByOpportunityId(new Set<Id> {mockOpportunityId})).thenReturn(mockAccContAgreementsByOppId);

        mocks.stopStubbing();

        Application.Selector.setMock(mockOpportunitySelector);
        Application.Selector.setMock(mockOpsSetupSelector);
        Application.Selector.setMock(mockAccContAgreementRelSelector);

        Test.startTest();

            OpsFormDataProvider dataProvider = new OpsFormDataProvider();
            sortablegrid.SDG coreSDG = dataProvider.LoadSDG('', mockOpportunityId);

            sortablegrid.SDGRequest request = new sortablegrid.SDGRequest();
            request.ParentRecordID = mockOpportunityId;
            request.PageID = 1;
            request.PageSize = 10;
            request.FieldSetName=null;
            request.RelationshipName=null;
            request.SDGTag='Apex:OpsFormDataProvider';

            sortablegrid.SDGResult result = OpsFormDataProvider.getData(coreSDG, request);
            system.assertNotEquals(null, result);

        Test.stopTest();
    }

    @IsTest
    static void isUserSelectable_Test(){

        Boolean isUserSelectable = true;
        System.assertEquals(isUserSelectable, OpsFormDataProvider.isUserSelectable());
    }
}