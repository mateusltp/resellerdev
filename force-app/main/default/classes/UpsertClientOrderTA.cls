public with sharing class UpsertClientOrderTA {
  private List<Opportunity> opportunities;

  public UpsertClientOrderTA() {
    if (!Trigger.isExecuting) {
      throw new TriggerException(
        'This class can only be used in a Trigger context'
      );
    }

    this.opportunities = new List<Opportunity>();

    for (Opportunity newOpportunity : (List<Opportunity>) Trigger.new) {
      if (isToUpsertOrder(newOpportunity)) {
        this.opportunities.add(newOpportunity);
      }
    }
  }

  private Boolean isToUpsertOrder(Opportunity newOpportunity) {
    Map<Id, Opportunity> oldOpportunityMap = (Map<Id, Opportunity>) Trigger.oldMap;
    Opportunity oldOpportunity = oldOpportunityMap.get(newOpportunity.Id);

    return (oldOpportunity.StageName != newOpportunity.StageName ||
      oldOpportunity.RecordTypeId != newOpportunity.RecordTypeId) &&
      (newOpportunity.StageName.equals('Lançado/Ganho') ||
      (newOpportunity.StageName.equals('Perdido') &&
      newOpportunity.Sub_Type__c == 'Retention')) &&
      (newOpportunity.Record_Type_Name__c.equals('SMB - SKU New Business') ||
      newOpportunity.Record_Type_Name__c.equals(
        'Client Sales - SKU New Business'
      ) ||
      newOpportunity.Record_Type_Name__c.equals(
        'SMB Success - SKU Renegotiation'
      ) ||
      newOpportunity.Record_Type_Name__c.equals(
        'Client Success - SKU Renegotiation'
      ));
  }

  public void execute() {
    Savepoint savepoint = Database.setSavepoint();

    try {
      new UpsertClientOrderService(this.opportunities).execute();
    } catch (DmlException error) {
      for (Opportunity opportunity : this.opportunities) {
        opportunity.addError(error.getDmlMessage(0));
      }
      Database.rollback(savepoint);
    }
  }
}