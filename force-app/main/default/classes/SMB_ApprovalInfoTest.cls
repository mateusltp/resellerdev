@isTest(SeeAllData=false)
public with sharing class SMB_ApprovalInfoTest {
    
    @isTest
    static void getAccountHierarchyTest(){
        
        Opportunity lOpp = [ SELECT Id , Name , AccountId , CurrencyIsoCode FROM Opportunity ];

        Quote qt = DataFactory.newQuote( lOpp , 'Quote Test', 'Client_Sales_New_Business' );
        qt.Deal_Desk_Approved_Conditions__c = '[{"isApproved":true,"conditionCode":"CustomPayFrequency","approvalCondition":"Payment frequency is custom"},{"isApproved":true,"conditionCode":"NoAutonomousMarketPlace","approvalCondition":"Autonomous Market Place is not defined for your Offer"},{"isApproved":true,"conditionCode":"CustomPayMethod","approvalCondition":"Eligibility/MembershipFee with Custom payment settings or Credit Card method"}]';
        qt.Waiver_Approval_Level__c = 2;
        qt.Discount_Approval_Level__c = 2;
        qt.Waiver_approved__c = false;
        qt.Total_Discount_approved__c = false;
        Database.insert( qt );

       
        
        List<String> InputList = new List<String>();
        InputList.add(qt.Id);
     
        
        Test.startTest();
        List<SMB_ApprovalInfo.FlowOutput> responseMap = SMB_ApprovalInfo.getApprovalInfo( InputList );
        Test.stopTest();
    }
    
     @TestSetup
    static void createData(){    
        Account lParentAcc = DataFactory.newAccount();
        lParentAcc.NumberOfEmployees = 1000;
        Database.insert( lParentAcc );

        Account lAcc = DataFactory.newAccount();
        lAcc.ParentId = lParentAcc.Id;
        lAcc.NumberOfEmployees = 1000;
        Database.insert( lAcc );

        Contact lNewContact = DataFactory.newContact( lParentAcc , 'Test Contact' );
        Database.insert( lNewContact );

        Pricebook2 lStandardPb = DataFactory.newPricebook();
        Database.update( lStandardPb );

        Opportunity lOpp = new Opportunity(CurrencyIsoCode='BRL');

        Product2 lAcessFee = DataFactory.newProduct( 'Enterprise Subscription' , false , 'BRL' );
        Database.insert( lAcessFee );

        Product2 lAccessFeeFamilyMember = DataFactory.newProduct( 'Enterprise Subscription' , true , 'BRL' );
        Database.insert( lAccessFeeFamilyMember );

        PricebookEntry lAccessFeeEntry = DataFactory.newPricebookEntry( lStandardPb , lAcessFee , lOpp );
        Database.insert( lAccessFeeEntry );

        PricebookEntry lAccessFeeFamilyMemberEntry = DataFactory.newPricebookEntry( lStandardPb , lAccessFeeFamilyMember , lOpp );
        Database.insert( lAccessFeeFamilyMemberEntry );

        Product2 lSetupFee = DataFactory.newProduct( 'Setup Fee' , false , 'BRL' );
        Database.insert( lSetupFee );

        PricebookEntry lSetupFeeEntry = DataFactory.newPricebookEntry( lStandardPb , lSetupFee , lOpp );
        Database.insert( lSetupFeeEntry );

        Product2 lProfServicesOneFee = DataFactory.newProduct( 'Professional Services Setup Fee' , false , 'BRL' );
        Database.insert( lProfServicesOneFee );

        PricebookEntry lProfServicesOneFeeEntry = DataFactory.newPricebookEntry( lStandardPb , lProfServicesOneFee , lOpp );
        Database.insert( lProfServicesOneFeeEntry );

        Product2 lProfServicesMainFee = DataFactory.newProduct( 'Professional Services Maintenance Fee' , false , 'BRL' );
        Database.insert( lProfServicesMainFee );

        PricebookEntry lProfServicesMainFeeEntry = DataFactory.newPricebookEntry( lStandardPb , lProfServicesMainFee , lOpp );
        Database.insert( lProfServicesMainFeeEntry );

        lOpp = DataFactory.newOpportunity( lParentAcc.Id , lStandardPb , 'Client_Sales_New_Business' );
        lOpp.Quantity_Offer_Number_of_Employees__c = 500;
        Database.insert( lOpp );
    }
}