@isTest
public with sharing class ResellerJointEffortTest {


    @IsTest
    public static void testAllJoinEffort(){

        Account_Request__c request = new Account_Request__c(
            Public_Sector__c = false,
            Existing_In_SalesForce__c = true,
            Is_Gympass_Customer__c = false,
            ABM_Prospect__c = false,
            Direct_Channel_Open_Opp__c = true,
            Current_Stage_In_Open_Opp__c = 'Qualificação',
            BID__c = 'No'
        );
        Test.startTest();
        ResellerJointEffort effort = new ResellerJointEffort(request);
        System.assertEquals(false, effort.isJointEffort());
        Test.stopTest();

    }

    @isTest
    public static void testAllVisibleOlynTestEvents(){
        Account_Request__c request = new Account_Request__c(
            Public_Sector__c = false,
            Existing_In_SalesForce__c = true,
            Is_Gympass_Customer__c = false,
            ABM_Prospect__c = false,
            Partner_Model__c = 'Subsidy',
            Direct_Channel_Open_Opp__c = true,
            Last_Interaction_Account_Event__c = 90,
            Current_Stage_In_Open_Opp__c = 'Qualificação',
            BID__c = 'No'
        );

        Test.startTest();
        ResellerJointEffort effort = new ResellerJointEffort(request);
        System.assertEquals(true, effort.isSubsidy());
        System.assertEquals(false, effort.isIntermediation());
        System.assertEquals(true, effort.lastActivityLessEqual90());
        System.assertEquals(false, effort.lastActivityMore90());
        System.assertEquals(false, effort.lastActivityLessEqual60());
        System.assertEquals(true, effort.lastActivityMore60());
        System.assertEquals(false, effort.lastActivityLessEqual30());
        System.assertEquals(true, effort.lastActivityMore30());
        Test.stopTest();
    }

}