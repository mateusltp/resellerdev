@isTest(seeAllData=false)
public class LegacyPartnerPriceRepositoryTest {
    
    @TestSetup
    static void Setup(){
        
        Account acc = DataFactory.newAccount();
        acc.Indirect_Channel_Pricebook__c = '5k FTEs';
        Database.insert(acc);
        
        Legacy_Partner_Price__c legacyPartner = DataFactory.newLegacyPartnerPrice('Exclusive', 'BRL', acc.Id, 'Exclusive');
        Database.insert(legacyPartner);
        
        Legacy_Partner_Price__c legacyPartnerSubsidy = DataFactory.newLegacyPartnerPrice('Subsidy', 'BRL', acc.Id, 'Subsidy');
        Database.insert(legacyPartnerSubsidy);
        
    }
    
    
    @isTest
    static void isPartnerByAccountIdtest(){
        Account account = [select Id,Name from Account limit 1];
        LegacyPartnerPriceRepository legacyPartnerPriceRepository = new LegacyPartnerPriceRepository();
        List<Legacy_Partner_Price__c> listLegacyPartner = null;
        try{
            listLegacyPartner = legacyPartnerPriceRepository.isPartnerByAccountId(account.id, 'BRL');
        }catch(System.QueryException e){
            System.assert(false);
        }
        System.assertEquals(listLegacyPartner.size(),1);
    }
    
    @isTest
    static void getExclusivePricetest(){
        Account account = [select Id,Name from Account limit 1];
        LegacyPartnerPriceRepository legacyPartnerPriceRepository = new LegacyPartnerPriceRepository();
        List<Legacy_Partner_Price__c> listLegacyPartner = null;
        try{
            listLegacyPartner = legacyPartnerPriceRepository.getExclusivePrice(account.id, 'BRL');
        }catch(System.QueryException e){
            System.assert(false);
        }
        System.assertEquals(listLegacyPartner.size(),1);
    }
}