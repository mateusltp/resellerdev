global class ResellerEngineGoNoGOBatch implements Database.Batchable<SObject>, Database.stateful {
    
    String errors = '';
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        String query = 'SELECT Id, Name, Engine_Status__c FROM Account_Request__c WHERE Engine_Status__c = \'Analyze\'';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Account_Request__c> records) {
        
        try{
            Set<Id> AccountRequestIds = new Set<Id>();
            for(Account_Request__c ar : records){
                AccountRequestIds.add(ar.Id);
            }

            ResellerEngineGoNoGO engine = new ResellerEngineGoNoGO(AccountRequestIds);
            engine.noGO();
        }
        
       catch( Exception e){
            errors = errors + e.getMessage() + ';';
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        
        AsyncApexJob a = [Select Id, Status,ExtendedStatus,NumberOfErrors, JobItemsProcessed,
        TotalJobItems, CreatedBy.Email
        from AsyncApexJob where Id =:BC.getJobId()];

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {a.CreatedBy.Email};
        mail.setToAddresses(toAddresses);
        mail.setSubject('Match Merge Batch ' + a.Status);
        mail.setPlainTextBody('The batch Apex job processed ' + a.TotalJobItems +
        ' batches with '+ a.NumberOfErrors + ' failures.');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }   
}