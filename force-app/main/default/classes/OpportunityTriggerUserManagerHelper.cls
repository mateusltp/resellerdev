/**
 * @File Name          : OpportunityTriggerUserManagerHelper.cls
 * @Description        : 
 * @Author             : GEPI@GFT.com
 * @Group              : 
 * @Last Modified By   : gft.samuel.silva@ext.gympass.com
 * @Last Modified On   : 05-13-2022
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    25/06/2020   GEPI@GFT.com     Initial Version
 * 1.1    04/05/2022    bruno.mendes@gympass.com    added Partner_Flow_Opportunity record type
**/
public class OpportunityTriggerUserManagerHelper {
    Id recordTypeIdSmall = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Gyms_Small_Medium_Partner').getRecordTypeId();
    Id recordTypeIdWishList = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Gyms_Wishlist_Partner').getRecordTypeId();
    Id recordTypeIdPartner = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Opportunity').getRecordTypeId();
    List<Opportunity> lstNew = Trigger.new;  

    public void setUserManagerInOpp(){
        List<User> u = new List<User>();
        if (!Test.isRunningTest()) {
            u = [Select ManagerId From User Where Id = :UserInfo.getUserId()];
        } else {
            u = [Select ManagerId From User Where Username = 'r.user@test.com'];
        }

        
        if(!u.isEmpty()){
            system.debug('usuario: ' + u);
            for(Opportunity opp : lstNew){  
                if(opp.RecordTypeId == recordTypeIdSmall || opp.RecordTypeId == recordTypeIdWishList || opp.RecordTypeId == recordTypeIdPartner){
                    opp.Manager__c = u[0].ManagerId;
                }
            }
        }

    }

}