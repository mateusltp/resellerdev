/**
 * @description       : 
 * @author            : David Mantovani - DDMA@gft.com
 * @group             : 
 * @last modified on  : 06-22-2022
 * @last modified by  : gft.jorge.stevaux@ext.gympass.com
 * Modifications Log 
 * Ver   Date          Author                           Modification 
 * 1.0   07-13-2020    David Mantovani - DDMA@gft.com   Initial Version
**/
public  class ContentFileValidationHelper {

    private static final PS_Constants constants = PS_Constants.getInstance();
    private static final String className = OpportunityPartnerServiceImpl.class.getName();

    Id recordTypePartner = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(constants.ACCOUNT_RT_GYM_PARTNER).getRecordTypeId();
    Id recordTypePartnerFlow = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(constants.ACCOUNT_RT_PARTNER_FLOW_ACCOUNT).getRecordTypeId();
    Id contentVersionRt =  Schema.SObjectType.ContentVersion.getRecordTypeInfosByDeveloperName().get(constants.CONTENTVERSION_PARTNER_RT).getRecordTypeId(); 

    List<ContentDocumentLink> lstNew = trigger.new;
    fflib_ISObjectUnitOfWork uow = Application.UnitOfWork.newInstance();

    public Set<Schema.SObjectType> restrictedSobjects;

    public void validateContentDocument(){
        validateRecordTypeList(lstNew);
    }     

    public ContentFileValidationHelper() {
        restrictedSobjects = new Set<Schema.SObjectType> {
			Account.getSObjectType()			
        };
    }
       
    public void validateRecordTypeList(List<ContentDocumentLink> documents) {
        AccountSelector accSelector = new AccountSelector();
        Set<ContentDocumentLink> contentDocumentLink = new Set<ContentDocumentLink> ();
        Set<ID> contentDocumentLinkIds = new Set<ID> ();
        Set<ID> contentVersionIds = new Set<ID> ();
        Set<ID> AccIds = new Set<ID>();
        Map<Id, Account> accRtMap = new Map<Id, Account>();
        Set<ID> accRTIds = new Set<ID>();
        set<ID> rtPartners = new Set<ID>();

        for (ContentDocumentLink contentDocLink : documents) {  
            if(contentDocLink.LinkedEntity.Type != 'User'){
                if (restrictedSobjects.contains(contentDocLink.linkedEntityId.getSobjecttype())){
                    if(contentDocLink.linkedEntityId.getSobjectType() == Account.getSObjectType()){
                        AccIds.add(contentDocLink.linkedEntityId);
                    }
                    contentDocumentLink.add(contentDocLink);
                }  
            }          
        }

        if(!AccIds.isEmpty()){
            accRtMap = accSelector.selectByIdForPartnersAsMap(AccIds);

            for(Account acc : accRtMap.values()){
                accRTIds.add(acc.RecordTypeId);
            }
        }

        for(contentDocumentLink cntdoclink : contentDocumentLink){            
            if(accRtIds.contains(recordTypePartner) || accRtIds.contains(recordTypePartnerFlow)){
                contentDocumentLinkIds.add(cntdoclink.contentdocumentid);
            }
        }

        if(!contentDocumentLinkIds.isEmpty()){
            System.debug('AccIds antes do validate ' + accRtMap.keySet() );
            validateFile(contentDocumentLinkIds,contentDocumentLink, accRtMap);  
        }
    }
    
    public void validateFile(Set<ID> contentDocumentLinkIds,Set<ContentDocumentLink> contentDocumentLink, Map<Id, Account> AccIds){
        Map<String, String> mapFileParamValues = new Map<String, String>();
        
        List<FIleObject__c> lstAccCount = new List<FIleObject__c>([SELECT Id, Name, conversionId__c,File_Type__c,  account__c, account__r.name FROM FIleObject__c WHERE Account__c IN: AccIds.keySet()]);
        Map<Id, List<FIleObject__c>> mapFileObj = new Map<Id, List<FIleObject__c>>();
        for(FIleObject__c fileObj : lstAccCount){
            if(mapFileObj.containsKey(fileObj.account__c)){
                mapFileObj.get(fileObj.account__c).add(fileObj);
            } else {
                mapFileObj.put(fileObj.account__c, new List<FileObject__c>{fileObj});
            }
        }
        
        System.debug('Lista de File Objects da Account  ' + lstAccCount);
        System.debug('Quantidade de File Objects da Account   ' + lstAccCount.size());
        
        List<File_Setting__mdt> listFileSettings = [SELECT
                                                    	Id, Parameter_Name__c, Parameter_Value__c
                                                    FROM
                                                   		File_Setting__mdt
                                                    WHERE
                                                   		MasterLabel LIKE '%Partner%'];
        
        for (File_Setting__mdt fileSetting : listFileSettings)
            mapFileParamValues.put(fileSetting.Parameter_Name__c, fileSetting.Parameter_Value__c);
        
        List<String> listValidFileFormats = new List<String>();
        
        if (mapFileParamValues.get(constants.FILESETTING_PARTNERS_FILE_TYPE) != NULL) {
            String validFormats = mapFileParamValues.get(constants.FILESETTING_PARTNERS_FILE_TYPE);
            
            listValidFileFormats = validFormats.split(';');
        }
        
        Integer fileSizeLimit = 0;
        
        if (mapFileParamValues.get(constants.FILESETTING_PARTNERS_FILE_SIZE_LIMIT) != NULL)
            fileSizeLimit = Integer.valueOf(mapFileParamValues.get(constants.FILESETTING_PARTNERS_FILE_SIZE_LIMIT));
        
        Integer fileQuantityLimit = 0;
        
        if (mapFileParamValues.get(constants.FILESETTING_PARTNERS_FILE_QTY_LIMIT) != NULL)
            fileQuantityLimit = Integer.valueOf(mapFileParamValues.get(constants.FILESETTING_PARTNERS_FILE_QTY_LIMIT));
        
        List<ContentVersion> ctVersionLst = [SELECT
                                             	Title, ContentDocumentId, ContentSize, FileType, Type_Files_fileupload__c, Id,RecordTypeId, FirstPublishLocationId
                                             FROM
                                             	ContentVersion
                                             WHERE
                                             	(ContentDocumentId IN :contentDocumentLinkIds)];
        
        for (ContentVersion ctv : ctVersionLst) {     
            Integer indexPhotos = 1;
            for (ContentDocumentLink ctdLink : contentDocumentLink) {
                if (ctdLink.ContentDocumentId == ctv.ContentDocumentId) {                   
                
                    String errorMessage = '';
                    String logo = 'LOGO';
                    String photo = 'Photo';
                    String partner = 'Partner';
                    String w9 = 'W9';
                    String jbp = 'JBP';
                    
                    ctv.Title = ctv.Title.toUpperCase();
                    
                    if(ctv.Title.startsWith('LOGO') && ctv.Type_Files_fileupload__c == null){                  
                        if(ctv.Title != logo + ctv.Title.replace('LOGO','') + ' - ' + lstAccCount.get(0).account__r.name){
                            ctv.Title = logo + ctv.Title.replace('LOGO','') + ' - ' + lstAccCount.get(0).account__r.name;                    
                            uow.registerDirty(ctv);
                        }
                    } else if(ctv.Title.startsWith('LOGO') && ctv.Type_Files_fileupload__c != null){
                            System.debug('>>>>> ctv.Title  else' + ctv.Title); 
                            if( !ctv.Title.contains('LOGO PARTNER - ')){
                                ctv.Title = 'LOGO PARTNER - ' + ctv.Title;    
                            }

                            uow.registerDirty(ctv);
                    }

                 
                    if( (ctv.Title.startsWith('PHOTO') || ctv.Title.startsWith('FOTO')) && ctv.Type_Files_fileupload__c == null){
                        System.debug('If photo');
                        if(ctv.Title != photo + ctv.Title.replace('PHOTO','').replace('FOTO', '') + ' - ' + lstAccCount.get(0).account__r.name){
                            ctv.Title = photo + ctv.Title.replace('PHOTO','').replace('FOTO', '') + ' - ' + lstAccCount.get(0).account__r.name;
                            uow.registerDirty(ctv);
                        }                    
                    } else if( (ctv.Title.startsWith('PHOTO') || ctv.Title.startsWith('FOTO')) && ctv.Type_Files_fileupload__c != null){ 
                        System.debug('ELSE PHOTO');                       
                        if( !ctv.Title.contains('PHOTO PARTNER - ')){
                            ctv.Title = 'PHOTO PARTNER - ' + ctv.Title;    
                        }
                       
                        uow.registerDirty(ctv);
                    }
                   

                    if(ctv.Title.startsWith('W9') && ctv.Type_Files_fileupload__c == null){
                        System.debug('If w9');
                        if(ctv.Title != w9 + ctv.Title.replace('W9','') + ' - ' + lstAccCount.get(0).account__r.name){
                            ctv.Title = w9 + ctv.Title.replace('W9','') + ' - ' + lstAccCount.get(0).account__r.name;
                            uow.registerDirty(ctv);
                        } 
                    } else if (ctv.Title.startsWith('W9') && ctv.Type_Files_fileupload__c != null){
                        if( !ctv.Title.contains('W9 PARTNER - ')){
                            ctv.Title = 'W9 PARTNER - ' + ctv.Title;
                            uow.registerDirty(ctv); 
                        }
                    }

                    if(ctv.Title.startsWith('JBP')){
                        if(ctv.Title != jbp + ctv.Title.replace('JBP','') + ' - ' + lstAccCount.get(0).account__r.name){
                            ctv.Title = jbp + ctv.Title.replace('JBP','') + ' - ' + lstAccCount.get(0).account__r.name;
                            uow.registerDirty(ctv);
                        } 
                    }

                    System.debug(' ctv.Title  ' +  ctv.Title );

                    Account partnerAccount = AccIds.get(ctdLink.LinkedEntityId);

                    if ((partnerAccount != null && ctv.Type_Files_fileupload__c == null)  ) {
                        ctdLink.addError(constants.ERROR_VALID_TYPE);
                        System.debug('IF ERROR_VALID_TYPE');
                    }

                    
                    if (!listValidFileFormats.contains(ctv.FileType)) {
                        errorMessage = 'Sorry, but the file type: ' + ctv.FileType + ' isn\'t allowed. Here are the valid formats: ';
                        errorMessage += String.valueOf(listValidFileFormats).replace('{', '').replace('}', '');
                        
                        ctdLink.addError(errorMessage);
                        System.debug('IF FileType');
                        return;
                    }
                    
                    if (ctv.ContentSize > fileSizeLimit) {
                        errorMessage = 'Sorry, but the file size doesn\'t match the limits. Please upload a file lighter than: ' + fileSizeLimit/1024000 + 'Mb';
                        
                        ctdLink.addError(errorMessage);

                        System.debug('IF FileType');
                        
                        return;   
                    }
                    
                    if (lstAccCount.size() > fileQuantityLimit && partnerAccount.RecordTypeId == recordTypePartner ) {
                        errorMessage = 'Sorry, but the this record has reached the limit of uploaded files. Please review them and try it again';
                        
                        ctdLink.addError(errorMessage);
                        System.debug('IF fileQuantityLimit');
                        return;   
                    }
                }
            }       
        }

        List<FileObject__c> fileObjsToUpdate = insertNameFileObject(ctVersionLst,AccIds.keySet(), contentDocumentLink, lstAccCount);
        uow.registerDirty(fileObjsToUpdate);

        uow.commitWork();
      
    }

       public List<FileObject__c> insertNameFileObject(List<ContentVersion> ctVersionLst, Set<ID> AccIds , Set<ContentDocumentLink> contentDocumentLink, List<FIleObject__c> lstAccCount){
        List<FileObject__c> fileObjsToUpdate = new List<FileObject__c>();

        for(ContentVersion ctvName : ctVersionLst){
            for(ContentDocumentLink contLink : contentDocumentLink){
                    if(contLink.ContentDocumentId == ctvName.ContentDocumentId){
                        for(FileObject__c fileObj : lstAccCount){                            
                            if(ctvName.Id == fileObj.conversionId__c){
                                fileObj.Name = fileObj.account__r.name;//ctvName.Title;
                                fileObj.File_Type__c = ctvName.Type_Files_fileupload__c;
                                fileObj.File_Name__c = ctvName.Title;
                                fileObj.Format__c = ctvName.FileType;
                                fileObjsToUpdate.add(fileObj);
                            }
                        }
                }
            }
        }
        
        return fileObjsToUpdate;
    }
}