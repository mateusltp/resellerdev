/**
 * @author vinicius.ferraz
 */
public with sharing class QuoteTriggerHandler extends triggerhandler {

    public override void beforeUpdate() {
        new OfferChangesForApprovalHandler().quoteHandler( (List<Quote>) trigger.new, (Map<Id,Quote>) trigger.oldMap);
        new ProposalExceptionsRejected().validarExceptionsRejected();  
        new SKUDealDeskHandler().checkDealDeskFieldsApproved( (Map<Id,Quote>) trigger.oldMap , (List<Quote>) trigger.new ); 
        new ResellerSKUDealDeskHandler().checkDealDeskFieldsApproved( (Map<Id,Quote>) trigger.oldMap , (List<Quote>) trigger.new ); 
       // new ResellerSkuOfferReviewController().checkEnablersFieldsApproved((Map<Id,Quote>) trigger.oldMap , (List<Quote>) trigger.new);
    }
    
    public override void beforeDelete() {
    }
    
    public override void afterUpdate() {
        new WaiverTemporaryDiscountHandler().waiverStartEndDateHandler((Map<Id,Quote>) trigger.oldMap, (List<Quote>) trigger.new);
        new ApprovalRevampFeeDiscount()
            .saveFeesApprovedDiscount( (Map<Id,Quote>) trigger.oldMap , (List<Quote>) trigger.new );
        new ApprovalRevampSkuTotalDiscountAndWaiver()
            .saveFeesApprovedDiscountAndWaiver( (Map<Id,Quote>) trigger.oldMap , (List<Quote>) trigger.new );
    }

    public override void beforeInsert() {
        
    }

    public override void afterInsert() {
    }
    
    
}