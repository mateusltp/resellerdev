public with sharing class TaxIdService {

    private static final String RECEITA_WS_FREE = 'Free_Receita_Service';

    public enum TaxIdType { CNPJ , CNI, SIRET, TIN, HRB, CRN, KVK, NIF, CIF, PF}

    @future(callout=true)
    public static void assyncCalloutTaxId(Id recordId , String taxIdType){
        TaxIdService.calloutTaxId(recordId, taxIdType);
    }

    public static void calloutTaxId(Id recordId , String taxIdType){

        Tax_Id_Data__c taxIdRecord = [ SELECT Id, Tax_Id__c FROM Tax_Id_Data__c WHERE Id =: recordId LIMIT 1 ];
        HttpResponse response = new HttpResponse();

        if('CNPJ' == taxIdType){
            GenericCallout genericCallout = new GenericCallout(RECEITA_WS_FREE);
            genericCallout.setParam(taxIdRecord.Tax_Id__c);
            response = genericCallout.getResponse();
            if(response.getStatusCode() != 200)
                throw new AuraHandledException('TaxIdService Error: StatusCode : ' + response.getStatusCode() + ' getBody : ' + response.getBody());
            
            TaxIdBrazilWrapper taxIdBrazilWrapper = (TaxIdBrazilWrapper)JSON.deserialize(response.getBody(), TaxIdBrazilWrapper.class);
            
            Tax_Id_Data__c newRecord = TaxIdFactory.getTraxIdDataByWrapperBrazil(taxIdBrazilWrapper);
            newRecord.Id = taxIdRecord.Id;
            update newRecord;

            Tax_Id_Event__e taxIdEvent = new Tax_Id_Event__e();
            taxIdEvent.Tax_Id__c = taxIdRecord.Tax_Id__c;
            Database.SaveResult results = EventBus.publish(taxIdEvent);
        }
    }
}