/**
 * @description       : 
 * @author            : AlyssonMota
 * @group             : 
 * @last modified on  : 09-09-2020
 * @last modified by  : Alysson Mota
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   09-09-2020   Alysson Mota   Initial Version
**/
public without sharing class FastTrackEnablersController {

    @AuraEnabled
    public static List<Step_Towards_Success1__c> getEnablers(String oppId){
        System.debug('getEnablers');
        System.debug(oppId);
        OpportunityRepository opportunityRepository = new OpportunityRepository();
        Opportunity opp = opportunityRepository.byId(oppId);
        System.debug(opp);
        EnablersRepository enablers = new EnablersRepository();
        return enablers.getOpportunityEnablers(opp);        
    }
}