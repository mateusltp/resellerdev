@isTest
public with sharing class SKUResellerRevenueMetricsTest {
    
    @TestSetup
    static void createData() {

		User adminUser = getAdminUser();
		Database.insert(adminUser);
        
        System.runAs(adminUser) {
            //-- create test account
        	Account testAccount = newClientsAccount();
        	Database.insert(testAccount);
        	//System.debug('### TEST ### Account created SUCCESS');
        	
        	Pricebook2 testStandardPb = new Pricebook2(Id = Test.getStandardPricebookId(), IsActive = true);
       		Database.update(testStandardPb);
        	//System.debug('### TEST ### Pricebook #1 created SUCCESS');
        	
            Pricebook2 testPriceBook = newPricebook('SKU Price Book','Intermediation');
       		Database.insert( testPriceBook );
        	//System.debug('### TEST ### Pricebook #2 created SUCCESS');
            
            Product2 testAccessFeeProduct = newProduct('Enterprise Subscription', false ,'USD');
            testAccessFeeProduct.Payment_Type__c = 'Recurring fee';
       		Database.insert(testAccessFeeProduct);
        	//System.debug('### TEST ### Access Fee Product created SUCCESS');
        	
        	Product2 testAccessFamilyProduct = newProduct('SKU Family Subscription', false ,'USD');
            testAccessFamilyProduct.Payment_Type__c = 'One time fee';
       		Database.insert(testAccessFamilyProduct);
        	//System.debug('### TEST ### Access Family Product created SUCCESS');
        	
        	PricebookEntry testStandardPBE = new PricebookEntry(
            	Pricebook2Id = Test.getStandardPricebookId(),  
            	Product2Id = testAccessFeeProduct.Id, 
            	UnitPrice = 400, 
            	IsActive = true,
            	CurrencyIsoCode = 'USD');
      		Database.insert(testStandardPBE);
			//System.debug('### TEST ### Access Fee Pricebook created SUCCESS');

			PricebookEntry lAccessFamilyEntry = new PricebookEntry(
                Pricebook2Id = Test.getStandardPricebookId(),
                Product2Id = testAccessFamilyProduct.Id,
                UnitPrice = 400, 
            	IsActive = true,
            	CurrencyIsoCode = 'USD');
        	Database.insert(lAccessFamilyEntry);
        
        	//-- create test offer approved opportunity
			Opportunity testOpportunity = newOpportunity(testAccount.Id, testStandardPb.Id, 'Offer Approved', 'Intermediation');
        	Database.insert(testOpportunity);
        	//System.debug('### TEST ### Opportunity created SUCCESS');
        	
        	SKU_Price__c skuPriceEnterprise = newSKUPrice(testAccessFeeProduct.Id, testOpportunity, testAccount, 1, 30);
        	Database.insert(skuPriceEnterprise);
        	
            SKU_Price__c skuPriceAcessFamily = DataFactory.newSKUPrice(testAccessFamilyProduct.Id, testOpportunity, testAccount, 1, 40);
        	Database.insert(skuPriceAcessFamily);
            
            Quote testQuote = newStdQuote(testOpportunity);
            Database.insert(testQuote);
            //System.debug('### TEST ### Quote created SUCCESS');
        	
            QuoteLineItem lQuoteAccessFee = DataFactory.newQuoteLineItem( testQuote, testStandardPBE );
            lQuoteAccessFee.Discount__c = 10;
            Database.insert( lQuoteAccessFee);
            
            QuoteLineItem lQuoteAccessFamilyFee = DataFactory.newQuoteLineItem( testQuote, lAccessFamilyEntry );
        	lQuoteAccessFamilyFee.Discount__c = 50;
        	Database.insert(lQuoteAccessFamilyFee);
            
            Payment__c lPayForLineItem = DataFactory.newPayment( lQuoteAccessFee );
        	Database.insert( lPayForLineItem );

        	Waiver__c lWaiver = DataFactory.newWaiver( lPayForLineItem );
        	lWaiver.Number_of_Months_after_Launch__c = 15;
        	lWaiver.Quote_Line_Item__c = lQuoteAccessFee.Id;
        	Database.insert( lWaiver );

        	Payment__c lPayFamilyForLineItem = DataFactory.newPayment( lQuoteAccessFamilyFee );
        	Database.insert( lPayFamilyForLineItem );

        	Waiver__c lWaiverFamily = DataFactory.newWaiver(lPayForLineItem);
        	lWaiverFamily.Number_of_Months_after_Launch__c = 15;
        	lWaiverFamily.Quote_Line_Item__c = lQuoteAccessFamilyFee.Id;
        	Database.insert(lWaiverFamily);
        }
    }

    @isTest
    static void calculateResellerMetricsTest(){
        List<Quote> quotes = [SELECT Id FROM Quote];
        List<String> quotesId = new List<String>{quotes.get(0).Id};
        
        Test.startTest();
        SKUResellerRevenueMetrics.calculateResellerMetrics(quotesId);
        Test.stopTest();
    }
    
    
    //-- private helper methods -- START ----------------------------------------------
    private static Account newClientsAccount() {
        Account testAccount = new Account();  
        testAccount.Name = 'Reseller Account';
        testAccount.Legal_Document_Type__c = 'CNPJ';
        testAccount.Id_Company__c = '43712486000101';
        testAccount.Razao_Social__c = 'IndirectAccount';
        testAccount.Website = 'IndirectAccount.com';
        testAccount.Industry = 'Airlines';
        testAccount.BillingCountry = 'Brazil';
        testAccount.NumberOfEmployees = 600;
        testAccount.CurrencyIsoCode = 'USD';
        testAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
        return testAccount;
    }
    
    private static List<Contact> newContactsLst(Account acc, Integer numberOfContacts) {
        List<Contact> contactsLst = new List<Contact>();
        
        for(Integer index = 0; index < numberOfContacts; index++) {
            Contact cont = new Contact();
        	cont.AccountId = acc.Id;
        	cont.LastName = 'test ' + index;
        	cont.Email = 'test' + index +'@tst.com';
            contactsLst.add(cont);
        }
        return contactsLst;
    }
    
    private static Pricebook2 generatePricebook() {        
        Pricebook2 pb = new Pricebook2();
        pb.Name = 'Br';
        pb.Country__c = 'Brazil';
        pb.IsActive = true;
        return pb;
    }
    
    private static Pricebook2 newPricebook(String name, String businessModel) {
        Pricebook2 pricebook = new Pricebook2(
           	Name = name,
            Business_Model__c = businessModel,
            IsActive = true
        );
        return pricebook;
    }
    
    private static Product2 newProduct(String ProductName , Boolean isFamilyMemberIncluded , String currencyIsoCode) {
        Product2 product = new Product2();
        product.Name = ProductName;
        product.Minimum_Number_of_Employees__c = 0;
        product.Maximum_Number_of_Employees__c = 900;
        product.Family = ProductName;
        product.IsActive = true;
        product.Family_Member_Included__c = isFamilyMemberIncluded;
        product.ProductCode = ProductName.replace(' ', '_');
        product.Copay2__c = false;
        product.CurrencyIsoCode = currencyIsoCode;
        return product;
    }  
    
    private static Opportunity newOpportunity(String accountId, String stdPricebookId, String fastTrackStage, String businessModel) {
        Id oppRtId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Indirect_Channel_New_Business').getRecordTypeId();
        
    	Opportunity Opp = new Opportunity();
       	Opp.recordTypeId = oppRtId;
       	Opp.TotalOpportunityQuantity = 1;
       	Opp.AccountId = accountId;
       	Opp.Name = 'TestOpp'; 
       	Opp.CMS_Used__c = 'Yes';     
       	Opp.Gym_agreed_to_an_API_integration__c  = 'Yes';
       	Opp.Club_Management_System__c = 'Companhia Athletica';
       	Opp.Integration_Fee_Deduction__c = 'No';
       	Opp.CloseDate = Date.today();
       	Opp.Success_Look_Like__c = 'Yes';
       	Opp.Success_Look_Like_Description__c = 'Money money';
       	Opp.StageName = 'Validated';
       	Opp.Type = 'Expansion';  
       	Opp.Country_Manager_Approval__c = true;
       	Opp.Payment_approved__c = true;   
       	Opp.CurrencyIsoCode = 'USD';
       	Opp.Gympass_Plus__c = 'Yes';
       	Opp.B_M__c = businessModel;
       	Opp.Standard_Payment__c = 'Yes';
       	Opp.Request_for_self_checkin__c = 'Yes';  
       	Opp.Pricebook2Id = stdPricebookId;
       	
        return Opp;
    }
    
    private static SKU_Price__c newSKUPrice(Id productId, Opportunity opp, Account acc, Integer minQuantity, Decimal unitPrice) {
        SKU_Price__c skuPrice = new SKU_Price__c();
        skuPrice.Product__c = productId;
        skuPrice.Start_Date__c = Date.today();
        skuPrice.End_Date__c = Date.today().addYears(1);
        skuPrice.Minimum_Quantity__c = minQuantity;
        skuPrice.CountrySKU__c = acc.BillingCountry;
        skuPrice.CurrencyIsoCode = opp.CurrencyIsoCode;
        skuPrice.Unit_Price__c = unitPrice;
        return skuPrice;
    }
    
    private static User getAdminUser() {
        Id adminProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1].Id;
        User adminUser = new User();
        adminUser.ProfileId = adminProfileId;
        adminUser.FirstName = 'Tester';
        adminUser.LastName = 'Tester';
        adminUser.Username = 'testergftgympass@test.com';
        adminUser.Email = 'testergftgympass@test.com';
        adminUser.TimeZoneSidKey = 'GMT';
        adminUser.LanguageLocaleKey = 'en_US';
        adminUser.LocaleSidKey = 'en_US';
        adminUser.EmailEncodingKey = 'ISO-8859-1';
        adminUser.Alias = 'tsts';
        adminUser.Bypass_Automations__c = true;
        
        return adminUser;
    }
    
    private static Quote newStdQuote(Opportunity opp) {
   		return new Quote(
      		Name = opp.Id,
      		OpportunityId = opp.Id,
      		Contact_Permission__c = 'Allowlist',
      		Employee_Registration_Method__c = 'Eligible file',
      		End_Date__c = Date.today().addMonths(1),
      		License_Fee_Waiver__c = 'No',
      		Start_Date__c = Date.today(),
      		Unique_Identifier__c = 'Corporate E-mail'
			);
  	}
    //-- private helper methods --  END  ----------------------------------------------


    /*@TestSetup
    static void createData() {        
        Account acc = DataFactory.newAccount();   
        insert acc;
        
        Pricebook2 standardPricebook = new Pricebook2(
            Id = Test.getStandardPricebookId(),
            IsActive = true
        );
        update standardPricebook;
        Pricebook2 pb = standardPricebook;
        
        Opportunity opp = DataFactory.newOpportunity( acc.Id, standardPricebook, 'Indirect_Channel_New_Business' );  

        Product2 lAcessFee = DataFactory.newProduct( 'Enterprise Subscription' , false , 'BRL' );
        //lAcessFee.Payment_Type__c = 'One time fee';
        lAcessFee.Payment_Type__c = 'Recurring fee';
        Database.insert( lAcessFee );

        PricebookEntry lAccessFeeEntry = DataFactory.newPricebookEntry( standardPricebook , lAcessFee , opp );
        insert lAccessFeeEntry;

        Product2 lAcessFamily = DataFactory.newProduct( 'SKU Family Subscription' , false , 'BRL' );
        //lAcessFamily.Payment_Type__c = 'Recurring fee';
        lAcessFamily.Payment_Type__c = 'One time fee';
        Database.insert( lAcessFamily );

        PricebookEntry lAccessFamilyEntry = DataFactory.newPricebookEntry( standardPricebook , lAcessFamily , opp );
        insert lAccessFamilyEntry;
       
        SKU_Price__c skuPriceEnterprise = DataFactory.newSKUPrice(lAcessFee.Id, opp,acc, 1, 30);
        insert skuPriceEnterprise;
        SKU_Price__c skuPriceAcessFamily = DataFactory.newSKUPrice(lAcessFamily.Id, opp,acc, 1, 40);
        insert skuPriceAcessFamily;

        insert opp;
        Quote lQuote = [SELECT Id FROM Quote WHERE OpportunityId = :opp.Id];
        QuoteLineItem lQuoteAccessFee = [SELECT Id, Product_Name__c FROM QuoteLineItem WHERE QuoteId = :lQuote.Id];

        QuoteLineItem lQuoteAccessFamilyFee = DataFactory.newQuoteLineItem( lQuote , lAccessFamilyEntry );
        lQuoteAccessFamilyFee.Discount__c = 50;
        Database.insert( lQuoteAccessFamilyFee ); 

        Payment__c lPayForLineItem = DataFactory.newPayment( lQuoteAccessFee );
        Database.insert( lPayForLineItem );

        Waiver__c lWaiver = DataFactory.newWaiver( lPayForLineItem );
        lWaiver.Number_of_Months_after_Launch__c = 15;
        lWaiver.Quote_Line_Item__c = lQuoteAccessFee.Id;
        Database.insert( lWaiver );

        Payment__c lPayFamilyForLineItem = DataFactory.newPayment( lQuoteAccessFamilyFee );
        Database.insert( lPayFamilyForLineItem );

        Waiver__c lWaiverFamily = DataFactory.newWaiver(lPayForLineItem);
        lWaiverFamily.Number_of_Months_after_Launch__c = 15;
        lWaiverFamily.Quote_Line_Item__c = lQuoteAccessFamilyFee.Id;
        Database.insert(lWaiverFamily);
    }

    @isTest
    static void testById(){
        List<Quote> quotes = [SELECT Id FROM Quote];
        List<String> quotesId = new List<String>{quotes.get(0).Id};
        SKUResellerRevenueMetrics.calculateResellerMetrics(quotesId);
    }*/
}