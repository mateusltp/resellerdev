@isTest
public with sharing class ResellerLogServiceTest {
    
    @isTest
    public static void testPublishEventLog(){

        ResellerLogDomain domain = new ResellerLogDomain('action', 'message', 'tracking', 'type', 'lineNumber', 'referenceId', 'referenceSobject');

        Test.startTest();

        ResellerLogService.publishLogs(new List<ResellerLogDomain>{domain});

        Test.stopTest();

        Reseller_Log__c errors = [ SELECT Id FROM Reseller_Log__c LIMIT 1];
        System.assertNotEquals(null, errors);

    }

}