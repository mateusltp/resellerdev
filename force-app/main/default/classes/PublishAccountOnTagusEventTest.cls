@IsTest
public class PublishAccountOnTagusEventTest {
  @IsTest
  public static void execute() {
    Test.setMock(HttpCalloutMock.class, new HttpResponseMock());

    Account account = AccountMock.getStandard('Empresas');
    account.UUID__c = new Uuid().getValue();
    account.Is_Account_On_Tagus__c = true;
    insert account;

    account.Send_To_Tagus__c = true;
    update account;

    System.assertEquals(
      1,
      [SELECT Id FROM Queue__c].size(),
      'Wrong number of events created'
    );

    account.Phone = '11 99999-9999';
    update account;

    System.assertEquals(
      2,
      [SELECT Id FROM Queue__c].size(),
      'Wrong number of events created'
    );
  }

  private class HttpResponseMock implements HttpCalloutMock {
    public HTTPResponse respond(HTTPRequest req) {
      HttpResponse respinse = new HttpResponse();
      respinse.setHeader('Content-Type', 'application/json');
      respinse.setStatusCode(200);
      return respinse;
    }
  }
}