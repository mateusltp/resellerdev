public with sharing class SlackPublisher {
    
    //
    //_text_ will produce italicized text
    //*text* will produce bold text
    //~text~ will produce strikethrough text
    //\n - linebreak
    //` - code
    //<!here> - @Here
    
    private static final String slackURL = Label.Slack_Webhook_URL;
    
    public static void postToSlack(String message) {
        Map<String, Object> mapMessages = new Map<String, Object>();
        
        mapMessages.put('text', message);
        mapMessages.put('mrkdwn', true);
        String body = JSON.serialize(mapMessages);
        
        HttpRequest req = new HttpRequest();
        req.setEndpoint(slackURL);
        req.setMethod('POST');
        req.setBody(body);
        Http http = new Http();
        HttpResponse res = http.send(req);
    }
    
    public static void postToSlackQueueable(String message) {
        Map<String, Object> mapMessages = new Map<String, Object>();
        
        mapMessages.put('text', message);
        mapMessages.put('mrkdwn', true);
        String body = JSON.serialize(mapMessages);
        
        System.enqueueJob(new SlackCallQueueable(slackURL, 'POST', body));
    }
    
    @future(Callout=true)
    public static void futurePostToSlack(String message) {
        Map<String, Object> mapMessages = new Map<String, Object>();
        
        mapMessages.put('text', message);
        mapMessages.put('mrkdwn', true);
        String body = JSON.serialize(mapMessages);
        
        System.enqueueJob(new SlackCallQueueable(slackURL, 'POST', body));
    }
}