/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 12-20-2021
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/
public with sharing class NetworkBuilderMainController {
   
    
    @AuraEnabled(cacheable=true)
    public static Object buildNetwork(Id recordId, String recordTypeDevName){
        try {            
            return AccountService.buildNetwork(recordId,recordTypeDevName);
        } catch(Exception e){
            if(e.getMessage().contains('No Implementation registered for record type')){
                System.debug('No Implementation registered for record type ' + recordTypeDevName);
                return new List<Account>();
            }
            throw new NetworkBuilderMainControllerException(e.getMessage());
        }        
    }

    public class NetworkBuilderMainControllerException extends Exception {}
}