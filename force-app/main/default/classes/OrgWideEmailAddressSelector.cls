/**
 * @description       : 
 * @author            : Jorge Stevaux - JZRX@gft.com
 * @group             : 
 * @last modified on  : 07-30-2021
 * @last modified by  : Jorge Stevaux - JZRX@gft.com
**/
public with sharing class OrgWideEmailAddressSelector extends ApplicationSelector
 {
     public List<Schema.SObjectField> getSObjectFieldList() {
		return new List<Schema.SObjectField> {
			OrgWideEmailAddress.Id
		};
	}

    public Schema.SObjectType getSObjectType() {
		return OrgWideEmailAddress.sObjectType;
	}

    public List<OrgWideEmailAddress> selectById(Set<Id> ids) {
		return (List<OrgWideEmailAddress>) super.selectSObjectsById(ids);
	}
    
    public OrgWideEmailAddress selectGlobalB2BOrgWideEmailAddress() {

			return ((OrgWideEmailAddress)Database.query(newQueryFactory().
	 									setCondition('Address = \'global.b2b@gympass.com\'').
                                        setLimit(1).
	 									toSOQL()
     								));			

	}
}