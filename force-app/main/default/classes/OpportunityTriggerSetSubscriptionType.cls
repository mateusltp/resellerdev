public class OpportunityTriggerSetSubscriptionType {
    Id recordTypeIdSmall = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Gyms_Small_Medium_Partner').getRecordTypeId();
    Id recordTypeIdWishList = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Gyms_Wishlist_Partner').getRecordTypeId();
    List<Opportunity> lstNew = Trigger.new;  
    
    public void setEnterpriseSubscriptionType(){ 
        User u = [Select ManagerId From User Where Id = :UserInfo.getUserId()];
        
        if(u != null){
            system.debug('usuario: ' + u);
            for(Opportunity opp : lstNew){  
                if(opp.RecordTypeId == recordTypeIdSmall || opp.RecordTypeId == recordTypeIdWishList){
                    opp.Manager__c = u.ManagerId;
                }
            }
        }
        
    }
    
}