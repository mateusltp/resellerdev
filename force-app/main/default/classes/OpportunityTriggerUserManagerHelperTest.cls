/**
 * @description       : 
 * @author            : bruno.mendes@gympass.com
 * @group             : 
 * @last modified on  : 04/05/2022
 * @last modified by  : bruno.mendes@gympass.com
**/
@IsTest
private class OpportunityTriggerUserManagerHelperTest {

    @TestSetup
    static void setup() {
        List<Profile> profiles = [Select Id From Profile Where Name='Gyms'];

        User manager = new User();
        manager.Username = 'r.manager@test.com';
        manager.LastName = 'manager';
        manager.Email = 'r.manager@test.com';
        manager.Alias = 'tmana';
        manager.TimeZoneSidKey = 'America/Los_Angeles';
        manager.LocaleSidKey = 'en_US';
        manager.EmailEncodingKey = 'UTF-8';
        manager.ProfileId = profiles.get(0).id;
        manager.LanguageLocaleKey = 'en_US';

        insert manager;

        User user = new User();
        user.Username = 'r.user@test.com';
        user.LastName = 'user';
        user.Email = 'r.user@test.com';
        user.Alias = 'tuser';
        user.TimeZoneSidKey = 'America/Los_Angeles';
        user.LocaleSidKey = 'en_US';
        user.EmailEncodingKey = 'UTF-8';
        user.ProfileId = profiles.get(0).id;
        user.LanguageLocaleKey = 'en_US';
        user.ManagerId = manager.Id;

        insert user;

        Id recordTypeIdAccPartner = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Account').getRecordTypeId();

        Account acc = PartnerDataFactory.newAccount();
        acc.RecordTypeId = recordTypeIdAccPartner;
        acc.OwnerId = user.Id;
        insert acc;
    }

    @IsTest
    static void setUserManagerInOpp_Test() {
        Id accountId = [SELECT Id FROM Account LIMIT 1].Id;

        Id recordTypeIdOppPartner = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Opportunity').getRecordTypeId();

        Opportunity opp = new Opportunity();
        opp.Name = 'Opp Test';
        opp.StageName = 'Qualification';
        opp.CloseDate = System.now().addDays(30).date();
        opp.RecordTypeId = recordTypeIdOppPartner;
        opp.AccountId = accountId;
        insert opp;

        Opportunity updatedOpp = [SELECT Manager__c FROM Opportunity WHERE Id = :opp.Id];

        System.assertNotEquals(null, updatedOpp.Manager__c);
    }
}