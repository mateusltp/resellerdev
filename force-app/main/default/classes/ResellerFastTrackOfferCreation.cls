public with sharing class ResellerFastTrackOfferCreation {

    @InvocableMethod(label='[RESELLER] Create Offer' description='Generates the Proposal during the Offer Creation process')
    public static void resellerCreateProposal(List<String> oppsIdLst) {
        
        if(oppsIdLst == null || oppsIdLst.size() == 0) return;

        Account acc = [SELECT Id, Account.NumberOfEmployees, Account.Id 
                       FROM Opportunity 
                       WHERE Id = :oppsIdLst.get(0) LIMIT 1].Account; 

        FastTrackProposalCreationTO to = new FastTrackProposalCreationBuilder()
        .withOpportunity(oppsIdLst.get(0))
        .withLastCreatedQuoteWhenExists(oppsIdLst.get(0))
        .withDealHierarchy(acc.NumberOfEmployees, new List<Id>{acc.Id})
        .build();
        FastTrackProposalCreation fastTrackProposalCreation = new FastTrackProposalCreation(oppsIdLst.get(0));
        fastTrackProposalCreation.save(to);
    }
}