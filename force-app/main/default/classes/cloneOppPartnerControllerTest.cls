/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 08-12-2021
 * @last modified by  : gft.samuel.silva@ext.gympass.com
 * Modifications Log 
 * Ver   Date         Author                             Modification
 * 1.0   05-26-2021   gft.samuel.silva@ext.gympass.com   Initial Version
**/

@isTest(seeAllData=false)
public without sharing class cloneOppPartnerControllerTest {

    private static Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gym_Partner').getRecordTypeId();
    private static Id lOppRt = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Partner_Small_and_Medium_Renegotiation').getRecordTypeId();
    private static Id ctcRtId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Gyms_Partner').getRecordTypeId();
    private static Id quoteRtId = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Gyms_Quote_Partner').getRecordTypeId();
    private static Id recordTypePartnerForm = Schema.SObjectType.Ops_Setup_Validation_Form__c.getRecordTypeInfosByDeveloperName().get('Partner_Ops_Setup_Validation').getRecordTypeId();
    private static Id recordTypeIdBank = Schema.SObjectType.Bank_Account__c.getRecordTypeInfosByDeveloperName().get('Gyms_Bank_Account').getRecordTypeId();
    private static Id recordTypeIdAccBank = Schema.SObjectType.Acount_Bank_Account_Relationship__c.getRecordTypeInfosByDeveloperName().get('Gyms_Account_Bank_Account_Partner').getRecordTypeId();    
    private static Id recordTypeIdProductItem = Schema.SObjectType.Product_Item__c.getRecordTypeInfosByDeveloperName().get('Gyms_Product_Item_Partner').getRecordTypeId();
    
    @TestSetup
    static void setupData(){
        Account acc = generatePartnerAccount();
        insert acc;        
        Contact partnerContact = generatePartnerContact(acc);
        insert partnerContact;     

        Pricebook2 standardPricebook = new Pricebook2(
            Id = Test.getStandardPricebookId(),
            IsActive = true
        );
        update standardPricebook;
        Pricebook2 pb = standardPricebook;  

        Opportunity opp = generateOpportunity(acc,pb);
        insert opp;	
        
    }

    @isTest
    private static void getFieldOptionsTest(){        
        Test.startTest();        
            Test.setMock(HttpCalloutMock.class, new Mock());
            String response = cloneOppPartnerController.getFieldOptions('Opportunity', 'Sub_Type__c', 'Partner_Wishlist_Renegotiation');
            List<cloneOppPartnerController.PicklistOption> lstPickValues =  (List<cloneOppPartnerController.PicklistOption>)JSON.deserialize(response, List<cloneOppPartnerController.PicklistOption>.class);
            System.assertEquals(2, lstPickValues.size());
            System.assertEquals('Renegotiation', lstPickValues.get(0).label);
        Test.stopTest();             
    } 

    @isTest
    private static void getFieldOptionsWithErrorTest(){        
        Test.startTest();        
            try{
                Test.setMock(HttpCalloutMock.class, new Mock());
                String response = cloneOppPartnerController.getFieldOptions('INVALIDOBJ', 'INVALIDFIELD', 'INVALIDORT');
            } catch(Exception e ){
                System.assert(e.getMessage().contains('Script-thrown exception'));
            }            
        Test.stopTest();             
    }  
    
    @isTest
    private static void getDependentPickListTest(){        
        Test.startTest();        
            Test.setMock(HttpCalloutMock.class, new Mock());
            Map<String, List<String>> response = cloneOppPartnerController.getDependentPickList('Opportunity', 'Cancellation_Reason__c', 'Partner_Wishlist_Renegotiation');

        Test.stopTest();             
    }  

    @isTest
    private static void getDependentPickListTestWithError(){        
        Test.startTest();        
            try{
                Test.setMock(HttpCalloutMock.class, new Mock());
                Map<String, List<String>> response = cloneOppPartnerController.getDependentPickList('INVALIDOBJ', 'INVALIDFIELD', 'INVALIDORT');
            } catch(Exception e ){
                System.assert(e.getMessage().contains('Script-thrown exception'));
            }            
        Test.stopTest();             
    }  
    
    @isTest
    private static void createNewPartnerRenegotiationOppTest(){        
        //( String aAccId , String aRecordTypeDevName, String aSubTypeValue, String aCancellationReason ){
        Test.startTest();        
            Account acc = [SELECT id FROM Account LIMIT 1];
            Id oppId = cloneOppPartnerController.createNewPartnerRenegotiationOpp(acc.id, 'Partner_Small_and_Medium_Renegotiation', 'Retention', 'Fraud', 'Visit manipulation', 'CX Team');
            Opportunity opp = [SELECT Cancellation_Reason__c FROM Opportunity WHERE Id =: oppId];
            System.assertEquals('Fraud', opp.Cancellation_Reason__c);
        Test.stopTest();             
    }   

    // @isTest
    // private static void errorCreateNewPartnerRenegotiationOppTest(){        
    //     //( String aAccId , String aRecordTypeDevName, String aSubTypeValue, String aCancellationReason ){
    //     Test.startTest();  
    //         try{
    //             Id oppId = cloneOppPartnerController.createNewPartnerRenegotiationOpp('INVALIDID', 'Partner_Small_and_Medium_Renegotiation', 'Retention', 'Fraud','Visit manipulation', 'CX Team');
    //         }catch (AuraHandledException e) {
    //             System.assert(e.getMessage().contains('Script-thrown exception'));            
    //         }      
    //     Test.stopTest();             
    // } 

    private static Account generatePartnerAccount(){
    Account lAcc = new Account();
        lAcc.name='AcademiaBrasilCompanyPai';
        lAcc.RecordTypeId = rtId;
        lAcc.GP_Status__c = 'Active';
        lAcc.billingState = 'Minas Gerais';
        lAcc.CAP_Value__c = 120;
        lAcc.BillingCity = 'CityAcademiaBrasil';
        lAcc.billingStreet = 'Rua academiabrasilpai';
        lAcc.billingCountry = 'Brazil';
        lAcc.ShippingCountry ='Brazil';
        lAcc.ShippingStreet ='Dona Maria';
        lAcc.ShippingCity ='Uberlandia';
        lAcc.Gym_Type__c = 'Studios';
        lAcc.Gym_Classes__c = 'Cardio';
        lAcc.Legal_Title__c = '11111';
        lAcc.Id_Company__c = '11111';
        lAcc.Types_of_ownership__c = Label.franchisePicklist;
        lAcc.Subscription_Type__c = 'Value per class';
        lAcc.Subscription_Period__c = 'Monthy value';
        lAcc.Subscription_Type_Estimated_Price__c    = 100;
        lAcc.Has_market_cannibalization__c = 'No';
        lAcc.Exclusivity__c = 'Yes';
        lAcc.Exclusivity_End_Date__c = Date.today().addYears(1);
        lAcc.Exclusivity_Partnership__c = 'Full Exclusivity';
        lAcc.Exclusivity_Restrictions__c= 'No';
        lAcc.Website = 'testing@tesapex.com';
        lAcc.Gym_Email__c = 'gymemail@apex.com';
        lAcc.Phone = '3222123123';
        lAcc.Can_use_logo__c = 'Yes';
        lAcc.Legal_Registration__c = 12123;
        lAcc.Legal_Title__c = 'Title LTDA';
        lAcc.Gyms_Identification_Document__c = 'CNPJ'; 
        return lAcc;
    }

    private static Opportunity generateOpportunity(Account lAcc, Pricebook2 lPricebook){
    Opportunity lAccOpp = new Opportunity();
        lAccOpp.CurrencyIsoCode='BRL';
        lAccOpp.recordTypeId = lOppRt;
        lAccOpp.AccountId = lAcc.id;
        lAccOpp.Name = lAcc.Id; 
        lAccOpp.CMS_Used__c = 'Yes';     
        lAccOpp.Gym_agreed_to_an_API_integration__c  = 'Yes';
        lAccOpp.Club_Management_System__c = 'Companhia Athletica';
        lAccOpp.Integration_Fee_Deduction__c = 'No';
        lAccOpp.CloseDate = Date.today();
        lAccOpp.Success_Look_Like__c = 'Yes';
        lAccOpp.Success_Look_Like_Description__c = 'Money money';
        lAccOpp.Cancellation_Reason__c = 'Fraud';
        lAccOpp.StageName = 'Proposta Enviada';
        lAccOpp.Type = 'Expansion';  
        lAccOpp.Country_Manager_Approval__c = true;
        lAccOpp.Payment_approved__c = true;   
        lAccOpp.CurrencyIsoCode = 'BRL';
        lAccOpp.Gympass_Plus__c = 'Yes';
        lAccOpp.Standard_Payment__c = 'Yes';
        lAccOpp.Request_for_self_checkin__c = 'Yes';  
        lAccOpp.Pricebook2Id = lPricebook.Id;
        lAccOpp.Training_Comments__c = 'teste';
        lAccOpp.Should_OPS_team_send_the_training__c = 'Standard Training';
        return lAccOpp;
    }

    private static Pricebook2 generatePricebook(){        
        Pricebook2 pb = new Pricebook2();
        pb.Name = 'Br';
        pb.Country__c = 'Brazil';
        pb.IsActive = true;
        return pb;
    }

    private static PricebookEntry generatePricebookEntry(Pricebook2 pb, Product2 product, Opportunity opp){        
        PricebookEntry pbEntry = new PricebookEntry();
        pbEntry.Product2Id = product.Id;
        pbEntry.Pricebook2Id = Test.getStandardPricebookId();
        pbEntry.UnitPrice = 100;
        pbEntry.IsActive = true;
        pbEntry.CurrencyIsoCode = opp.CurrencyIsoCode;
        return pbEntry;
    }


    private static Contact generatePartnerContact(Account acc) {
        return new Contact(
            FirstName = 'Fernando',
            LastName = 'Souza',
            Email = 'fernandoTestSouze@mail.com',
            Phone = '+5519999639999',
            recordTypeId = ctcRtId,
            MailingCountry = 'Brazil',
            Type_of_contact__c = 'Point of Contact;Legal Representative',
            Cargo__c = 'CEO',            
            accountId = acc.id
        );       

    }

    public class Mock implements HttpCalloutMock {
    	protected Integer code = 200;
    	protected String status = 'OK';
    	protected String body = '{"controllerValues":{},"defaultValue":null,"eTag":"aTag5","url":"/services/data/v51.0/ui-api/object-info/Opportunity/picklist-values/012050000000J4oAAE/Sub_Type__c","values":[{"attributes":null,"label":"Renegotiation","validFor":[],"value":"Renegotiation"},{"attributes":null,"label":"Retention", "validFor":[],"value":"Retention"}]}';
        
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse resp = new HttpResponse();
            resp.setStatusCode(code);
            resp.setStatus(status);
            resp.setBody(body);
            return resp;
        }
    }   
}