/**
 * @author: Eduardo Ribeiro de Carvalho - ercarval
 */
public abstract class EventQueueActiveRecord {
  private Map<String, EventConfiguration__c> config;

  public static String[] PENDING_EVENTS = new List<String>{
    EventQueueStatusType.SCHEDULED.name()
  };

  public EventQueueActiveRecord() {
    config = EventConfiguration__c.getAll();
  }

  public EventConfiguration__c getEventConfiguration(String eventName) {
    return config.get(eventName);
  }

  public Boolean hasHandlerFor(String eventName) {
    return config.containsKey(eventName);
  }

  public static void updateAll(List<Queue__c> events) {
    update events;
  }

  public static void createAll(List<Queue__c> events) {
    insert events;
  }

  public Queue__c save(Queue__c event) {
    if (event.id == null) {
      insert event;
      return event;
    }

    update event;
    return event;
  }

  public static void storePayloads(List<Attachment> attachments) {
    System.debug('\n\n\n Storing Payloads ' + attachments);

    try {
      upsert attachments;
    } catch (Exception e) {
      System.debug(
        '\n\n\n [ EXCEPTION ] UNABLE TO STORE PAYLOAD ' + attachments
      );
      System.debug('\n\n\n ' + e);
      System.debug('\n\n\n --------------------------------------------- ');
      System.debug(e.getStackTraceString());
    }
  }

  public static List<Queue__c> findQueuedEvents(Set<String> keys) {
    return [
      SELECT
        q.status__c,
        q.statusMessage__c,
        q.sender__c,
        q.payload__c,
        q.retryCount__c,
        q.receiver__c,
        q.objectId__c,
        q.internalId__c,
        q.externalCreationDate__c,
        q.exceptionStackTrace__c,
        q.eventName__c,
        q.businessDocument__c,
        q.businessDocumentCorrelatedNumber__c,
        q.Name,
        q.Id
      FROM Queue__c q
      WHERE q.id IN :keys
    ];
  }

  virtual public Queue__c findOne(String id) {
    List<Queue__c> queues = findQueuedEvents(new Set<String>{ id });
    return (!queues.isEmpty()) ? queues.get(0) : null;
  }

  public boolean hasQueuedEventsForBusinessDocument(
    String eventName,
    String businessDocumentNumber
  ) {
    List<Queue__c> events = [
      SELECT id, name
      FROM Queue__c q
      WHERE
        q.eventName__c = :eventName
        AND q.status__c = :EventQueueStatusType.QUEUED.name()
        AND q.businessDocument__c = :businessDocumentNumber
      LIMIT 1
    ];
    return !events.isEmpty();
  }

  virtual public List<Queue__c> findLastEventsByNameAndBusinessDocumentNumber(
    String eventName,
    String businessDocumentNumber
  ) {
    return [
      SELECT
        q.status__c,
        q.statusMessage__c,
        q.sender__c,
        q.retryCount__c,
        q.receiver__c,
        q.payload__c,
        q.objectId__c,
        q.internalId__c,
        q.externalCreationDate__c,
        q.exceptionStackTrace__c,
        q.eventName__c,
        q.businessDocument__c,
        q.businessDocumentCorrelatedNumber__c,
        q.Name,
        q.Id
      FROM Queue__c q
      WHERE
        q.eventName__c = :eventName
        AND q.businessDocument__c = :businessDocumentNumber
      ORDER BY Queue__c.externalCreationDate__c DESC
      LIMIT 2
    ];
  }

  public static List<Queue__c> findPendingQueuedEvents(
    DateTime lastCreationDate,
    Integer listSize
  ) {
    return findPendingQueuedEvents(PENDING_EVENTS, lastCreationDate, listSize);
  }

  public static List<Queue__c> findPendingQueuedEvents(
    String[] status,
    DateTime lastCreationDate,
    Integer listSize
  ) {
    return [
      SELECT
        q.parentEvent__c,
        q.businessDocument__c,
        q.businessDocumentCorrelatedNumber__c,
        q.Status__c,
        q.StatusMessage__c,
        q.Sender__c,
        q.RetryCount__c,
        q.Receiver__c,
        q.Payload__c,
        q.ObjectId__c,
        q.Name,
        q.LastModifiedDate,
        q.IsRetryDisabled__c,
        q.InternalId__c,
        q.Id,
        q.ExternalCreationDate__c,
        q.ExceptionStackTrace__c,
        q.EventName__c,
        q.CreatedDate
      FROM Queue__c q
      WHERE q.status__c IN :status AND q.createdDate < :lastCreationDate
      ORDER BY q.createdDate ASC
      LIMIT :listSize
    ];
  }

  public static List<Queue__c> findEventsWithError(
    DateTime lastCreationDate,
    Integer listSize
  ) {
    return [
      SELECT
        q.parentEvent__c,
        q.businessDocument__c,
        q.businessDocumentCorrelatedNumber__c,
        q.Status__c,
        q.StatusMessage__c,
        q.Sender__c,
        q.RetryCount__c,
        q.Receiver__c,
        q.Payload__c,
        q.ObjectId__c,
        q.Name,
        q.LastModifiedDate,
        q.IsRetryDisabled__c,
        q.InternalId__c,
        q.Id,
        q.ExternalCreationDate__c,
        q.ExceptionStackTrace__c,
        q.EventName__c,
        q.CreatedDate
      FROM Queue__c q
      WHERE
        q.status__c = 'ERROR'
        AND q.createdDate < :lastCreationDate
        AND q.retryCount__c > 0
        AND q.IsRetryDisabled__c = FALSE
      ORDER BY q.createdDate ASC
      LIMIT :listSize
    ];
  }

  public static List<Queue__c> findPendingQueuedEvents(
    EventQueueStatusType status,
    Integer listSize
  ) {
    return [
      SELECT
        q.parentEvent__c,
        q.businessDocument__c,
        q.businessDocumentCorrelatedNumber__c,
        q.Status__c,
        q.StatusMessage__c,
        q.Sender__c,
        q.RetryCount__c,
        q.Receiver__c,
        q.Payload__c,
        q.ObjectId__c,
        q.Name,
        q.LastModifiedDate,
        q.IsRetryDisabled__c,
        q.InternalId__c,
        q.Id,
        q.ExternalCreationDate__c,
        q.ExceptionStackTrace__c,
        q.EventName__c,
        q.CreatedDate
      FROM Queue__c q
      WHERE q.status__c = :status.name()
      ORDER BY q.createdDate ASC
      LIMIT :listSize
    ];
  }

  virtual public List<Attachment> findAttachmentsForEvent(String eventId) {
    return [
      SELECT
        Body,
        BodyLength,
        ContentType,
        CreatedById,
        CreatedDate,
        Description,
        Id,
        LastModifiedDate,
        Name,
        ParentId
      FROM Attachment
      WHERE ParentId = :eventId
      ORDER BY LastModifiedDate DESC
    ];
  }

  virtual public List<Attachment> findLastPayloadProcessedForEvent(
    String eventId,
    String eventName
  ) {
    return [
      SELECT
        Body,
        BodyLength,
        ContentType,
        CreatedById,
        CreatedDate,
        Description,
        Id,
        LastModifiedDate,
        Name,
        ParentId
      FROM Attachment
      WHERE ParentId = :eventId AND name LIKE :eventName + '%'
      ORDER BY LastModifiedDate DESC
      LIMIT 1
    ];
  }

  public static List<Queue__c> findEventTypeForObject(
    String eventName,
    String objectId
  ) {
    return [
      SELECT Id
      FROM Queue__c
      WHERE EventName__c = :eventName AND ObjectId__c = :objectId
    ];
  }
}