@IsTest
public class TriggerUpdateEnablersAccountTest {
    
    @TestSetup
    static void Setup(){
        
        Account accReseller = DataFactory.newAccountReseller();
        accReseller.Indirect_Channel_Pricebook__c = '5k FTEs';
        Database.insert(accReseller); 
        
        Pricebook2 pb5 = DataFactory.newPricebook('Reseller 5k', 'Subsidy 5k');
        Database.insert(pb5);
        
        Opportunity oppSubsidy = DataFactory.newOpportunity(accReseller.id, pb5, 'Indirect_Channel_New_Business');
        oppSubsidy.Reseller_del__c = accReseller.id;
        oppSubsidy.B_M__c = 'Total Subsidy';
        Database.insert(oppSubsidy);
        
    }
    
    @isTest 
    public static void updateEnablersAccountTest() {
        
        Opportunity opp = [SELECT Id, CurrencyIsoCode FROM Opportunity WHERE B_M__c = 'Total Subsidy' LIMIT 1];
        
        opp.StageName = 'Validated';
        update opp;
        Test.startTest(); 
        opp.Responsavel_Gympass_Relacionamento__c = userinfo.getuserId();
        opp.StageName = 'Lançado/Ganho';
        update opp;
        //System.assertEquals('No', quote.Copay__c);
        //System.assertEquals(false, quote.Enterprise_Discount_Approval_Needed__c);
        //System.assertEquals(true, quote.Enterprise_Subscription_Approved__c);      
        Test.stopTest();
        
    }
    
    
    
    
    
}