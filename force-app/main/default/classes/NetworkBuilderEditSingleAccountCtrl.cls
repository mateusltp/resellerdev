/**
 * Created by gympasser on 02/03/2022.
 */

public with sharing class NetworkBuilderEditSingleAccountCtrl {

    @AuraEnabled(Cacheable=false)
    public static List<AccountContactRelation> getContactRelations(Id accountId) {
        return AccountService.getContactRelations(new Set<Id> {accountId}, 'Partner_Flow_Account');
    }

    @AuraEnabled(Cacheable=false)
    public static List<AccountContactRelation> getContactsFromParentThatAreNotRelatedToChild(Id childAccountId, Id parentAccountId) {
        return AccountService.getContactsFromParentThatAreNotRelatedToChild(childAccountId, parentAccountId, 'Partner_Flow_Account');

    }

    @AuraEnabled(cacheable=true)
    public static Id getRecordTypeId(String recordTypeDevName){
        return ContactService.getRecordTypeId(recordTypeDevName);
    }

}