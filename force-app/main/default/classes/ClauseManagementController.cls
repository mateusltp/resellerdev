/**
 * @description       : 
 * @author            : gft.jorge.stevaux@ext.gympass.com
 * @group             : 
 * @last modified on  : 04-27-2022
 * @last modified by  : gft.jorge.stevaux@ext.gympass.com
**/
public class ClauseManagementController {

    @AuraEnabled
    /* Method to be used in Aura - */
    public static String getSwapClauses(Id contractAgreementId){
        system.debug('getSwapClauses ' + contractAgreementId);
        try {
            return Json.serialize(getSwapClausesOptions(contractAgreementId));
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    

    @AuraEnabled
    /* Method to be used in Aura - */
    public static string getSwappedClauses(Id contractAgreementId){
        try {
            return Json.serialize(getActiveSwapClauses(contractAgreementId));
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    /* Method to be used in Aura - */
    public static Boolean saveSwapClauses(string swappedClausesToSave){

        try {
            List<ManagedSwapClauseWrapper> swappedClauseList = (List<ManagedSwapClauseWrapper>)JSON.deserialize(swappedClausesToSave, List<ManagedSwapClauseWrapper>.class);
            return saveSwappedClauses(swappedClauseList);
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    /* Method to be used in Aura - */
    public static Boolean deleteSwapClauses(string swappedClausesToDelete){

        try {
            List<ManagedSwapClauseWrapper> swappedClauseList = (List<ManagedSwapClauseWrapper>)JSON.deserialize(swappedClausesToDelete, List<ManagedSwapClauseWrapper>.class);
            return deleteSwappedClauses(swappedClauseList);
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    /* Get Clauses that can be replaced by other (swap option) fro Setup in a Wrapper list */
    public static List<SwapClauseWrapper> getSwapClausesOptions(Id contractAgreementId){
        string sCountry ;
        string sType;

        try{ 
            
            APXT_Redlining__Contract_Agreement__c contractagreement = [SELECT Id, 
                                                                            Template_Group__c, 
                                                                            Template_Selection__c
                                                                            FROM
                                                                            APXT_Redlining__Contract_Agreement__c
                                                                            WHERE 
                                                                            Id =:contractAgreementId ];
            if (contractagreement != null){
                sCountry = contractagreement.Template_Selection__c;
                if (contractagreement.Template_Group__c.contains('B2B')){
                    sType = 'B2B';
                }else if (contractagreement.Template_Group__c.contains('Gym')){
                    sType = 'Gym';
                }else if (contractagreement.Template_Group__c.contains('Commercial Contract')){
                    sType = 'Commercial Contract';
                }else if (contractagreement.Template_Group__c.contains('Intention Contract')){
                    sType = 'Intention Contract';
                }else if (contractagreement.Template_Group__c.contains('Hybrid Contract')){
                    sType = 'Hybrid Contract';
                }
            }

system.debug('sCountry ' + sCountry);
system.debug('sType ' + sType);

            List<SwapClauseWrapper> returnSwapList = new List<SwapClauseWrapper>();
            /* Get all clauses starting from subclauses  */
            List<SwapClause__c> SwapClauseList = [Select Id, Name, text__c,
                                                    clause__c , clause__r.name , clause__r.order__c  ,clause__r.text__c , clause__r.Category__c 
                                                    ,subClause__c , Subclause__r.name , Subclause__r.order__c , Subclause__r.text__c  , Subclause__r.Category__c
                                                    ,SubsubClause__c , SubSubclause__r.name , SubSubclause__r.order__c   , Subsubclause__r.text__c, SubSubclause__r.Category__c
                                                    FROM SwapClause__c
                                                    WHERE 
                                                    (clause__r.country__c = : sCountry OR Subclause__r.country__c = : sCountry OR Subsubclause__r.country__c = : sCountry  )
                                                    AND
                                                    (clause__r.type__c = :sType OR Subclause__r.type__c =  :sType OR Subsubclause__r.type__c =  :sType )
                                                    ];
            
            if (!SwapClauseList.isEmpty()) {

                Map<id, SwapClauseWrapper> SwapClauseMap = new Map<id, SwapClauseWrapper>();
                /* Build all wrappers - each one will carry the main clause (could be sub and subsubclause) its own list of clause option*/
                for (SwapClause__c sClause : SwapClauseList){

                    if(sClause.clause__c != null ){
                        
                        if (SwapClauseMap.containsKey(sClause.clause__c)){
                            SwapClauseWrapper clause = SwapClauseMap.get(sClause.clause__c);
                            SwapClauseWrapper swap = new SwapClauseWrapper();
                            swap.Name = sClause.name;
                            swap.ClauseText = sClause.text__c;
                            swap.Id = sClause.Id;
                            swap.Category = sClause.clause__r.Category__c ;
                            swap.Type = 'Clause';
                            swap.generateSequenceOrder();

                            clause.SwapOptions.add(swap);

                        }else{
                            SwapClauseWrapper clause = new SwapClauseWrapper();
                            clause.Category = sClause.clause__r.Category__c ;
                            clause.Name = sClause.clause__r.name;
                            clause.Order = sClause.clause__r.order__c ;
                            clause.ClauseText = sClause.clause__r.text__c;
                            clause.Id = sClause.clause__c;
                            clause.Type = 'Clause';
                            clause.generateSequenceOrder();
        
                            SwapClauseWrapper swap = new SwapClauseWrapper();
                            swap.Name = sClause.name;
                            swap.ClauseText = sClause.text__c;
                            swap.Id = sClause.Id;
                            swap.Category = sClause.clause__r.Category__c ;
                            swap.Type = 'Clause';
                            swap.generateSequenceOrder();

                            clause.SwapOptions.add(swap);
                            SwapClauseMap.put(sClause.clause__c,clause );
                            
                        }

                    }else if (sClause.Subclause__c != null){
                        if (SwapClauseMap.containsKey(sClause.Subclause__c)){
                            SwapClauseWrapper clause = SwapClauseMap.get(sClause.Subclause__c);
                            SwapClauseWrapper swap = new SwapClauseWrapper();
                            swap.Name = sClause.name;
                            swap.ClauseText = sClause.text__c;
                            swap.Id = sClause.id;
                            swap.Category = sClause.subclause__r.Category__c ;
                            swap.Type = 'SubClause';
                            swap.generateSequenceOrder();
        
                            clause.SwapOptions.add(swap);

                        }else{
                            SwapClauseWrapper clause = new SwapClauseWrapper();
                            clause.Category = sClause.subclause__r.Category__c ;
                            clause.Name = sClause.subclause__r.name;
                            clause.Order = sClause.subclause__r.order__c;
                            clause.ClauseText = sClause.subclause__r.text__c;
                            clause.Id = sClause.Subclause__c;
                            clause.Type = 'SubClause';
                            clause.generateSequenceOrder();
        
                            SwapClauseWrapper swap = new SwapClauseWrapper();
                            swap.Name = sClause.name;
                            swap.ClauseText = sClause.text__c;
                            swap.Id = sClause.Id;
                            swap.Category = sClause.subclause__r.Category__c ;
                            swap.Type = 'SubClause';
                            swap.generateSequenceOrder();
        
                            clause.SwapOptions.add(swap);
                            SwapClauseMap.put(sClause.Subclause__c, clause );
                        }
                    }else if (sClause.SubSubclause__c != null ){
                        if (SwapClauseMap.containsKey(sClause.SubSubclause__c)){
                            SwapClauseWrapper clause = SwapClauseMap.get(sClause.SubSubclause__c);
                            SwapClauseWrapper swap = new SwapClauseWrapper();
                            swap.Name = sClause.name;
                            swap.ClauseText = sClause.text__c;
                            swap.Id = sClause.Id;
                            swap.Category = sClause.subsubclause__r.Category__c ;
                            swap.Type = 'SubSubClause';
                            swap.generateSequenceOrder();
        
                            clause.SwapOptions.add(swap);

                        }else{
                            SwapClauseWrapper clause = new SwapClauseWrapper();
                            clause.Category = sClause.subsubclause__r.Category__c ;
                            clause.Name = sClause.subclause__r.name;
                            clause.Order = sClause.subclause__r.order__c ;
                            clause.ClauseText = sClause.subclause__r.text__c;
                            clause.Id = sClause.SubSubclause__c;
                            clause.Type = 'SubSubClause';
                            clause.generateSequenceOrder();
        
                            SwapClauseWrapper swap = new SwapClauseWrapper();
                            swap.Name = sClause.name;
                            swap.ClauseText = sClause.text__c;
                            swap.Id = sClause.Id;
                            swap.Category = sClause.subsubclause__r.Category__c ;
                            swap.Type = 'SubSubClause';
                            swap.generateSequenceOrder();
        
                            clause.SwapOptions.add(swap);
                            SwapClauseMap.put(sClause.SubSubclause__c, clause );
                        }
                    }
                }
                returnSwapList = SwapClauseMap.values();
                returnSwapList.sort();
            }      
        return returnSwapList;
                                                  
        } catch (Exception e) {
            throw e;
         }
           
    }

    /* Delete swapped clauses */
    public static Boolean deleteSwappedClauses(List<ManagedSwapClauseWrapper> swappedClauseList){
        try {
            Boolean bolReturn = false;
            if (!swappedClauseList.isEmpty()){
                List<ManagedSwapClause__c> managedSwapClauseList = new List<ManagedSwapClause__c>();

                for(ManagedSwapClauseWrapper swapClauseWrapper : swappedClauseList){
                    ManagedSwapClause__c managedSwapClause = new ManagedSwapClause__c();
                    managedSwapClause.Id = swapClauseWrapper.ObjectId;
                    managedSwapClauseList.add(managedSwapClause);
                }

                if (!managedSwapClauseList.isEmpty()){
                    delete managedSwapClauseList;
                    return true;
                }
            }
            return false;
        }
        catch (Exception ex){
            throw ex;
            
        }
    }


    /* Get swapped clauses from the record */
    public static List<ManagedSwapClauseWrapper> getActiveSwapClauses(Id ContractAgreementId){
        try {
            
            List<ManagedSwapClause__c> ActiveManagedClauseList = [SELECT    Id, 
                                                                            ContractAgreement__c, 
                                                                            Clause__c, 
                                                                            SubClause__c, 
                                                                            SubSubClause__c, 
                                                                            SwapClause__c ,
                                                                            Status__c,
                                                                            CreatedDate
                                                                FROM ManagedSwapClause__c 
                                                                WHERE ContractAgreement__c =:ContractAgreementId ];
            /* Get all Ids of clauses, subclauses and subsubclauses on swapped object to find all objects */
            Set<id> ClauseNameSet = new Set<id>();
            for(ManagedSwapClause__c swappedClause : ActiveManagedClauseList){
                ClauseNameSet.add(swappedClause.SwapClause__c);
                if(swappedClause.Clause__c != null){
                    ClauseNameSet.add(swappedClause.Clause__c);
                }else if (swappedClause.SubClause__c != null){
                    ClauseNameSet.add(swappedClause.SubClause__c);
                }else if(swappedClause.SubSubClause__c != null){
                    ClauseNameSet.add(swappedClause.SubSubClause__c);
                }
            
            }
            /* Getting all objects separated by clause type to retrieve all names that will be used to get the clauses names */
            map<Id,Clause__c> ClauseNameMap = new map<Id,Clause__c>([SELECT Id, Name FROM Clause__c WHERE Id IN : ClauseNameSet] );

            map<Id,SubClause__c> SubClauseNameMap = new map<Id,SubClause__c>([SELECT Id, Name FROM SubClause__c WHERE Id IN : ClauseNameSet] );

            map<Id,SubSubClause__c> SubSubClauseNameMap = new map<Id,SubSubClause__c>([SELECT Id, Name FROM SubSubClause__c WHERE Id IN : ClauseNameSet] );

            map<Id,SwapClause__c> SwapClauseNameMap = new map<Id,SwapClause__c>([SELECT Id, Name FROM SwapClause__c WHERE Id IN : ClauseNameSet] );

            
            /* Add all map to only one making easier to retrieve the objects */
            Map<id,SObject> AllClausesMap = new Map<id,SObject>();
            AllClausesMap.putall(ClauseNameMap);
            AllClausesMap.putall(SubClauseNameMap);
            AllClausesMap.putall(SubSubClauseNameMap);
            AllClausesMap.putall(SwapClauseNameMap);

            /* Creating all the wrappers (using the map created to retrieve the clauses names) */
            List<ManagedSwapClauseWrapper> ManagedSwapClauseWrapperList = new List<ManagedSwapClauseWrapper>();
            if (!ActiveManagedClauseList.isEmpty()){
                
                for(ManagedSwapClause__c swappedClause : ActiveManagedClauseList){
                    ManagedSwapClauseWrapper MnSwapClause = new ManagedSwapClauseWrapper();

                    if(swappedClause.Clause__c != null){
                        MnSwapClause.ObjectId = swappedClause.Id;
                        MnSwapClause.StandardId = swappedClause.Clause__c;
                        MnSwapClause.StandardClause =  ((Clause__c)AllClausesMap.get(swappedClause.Clause__c)).Name;
                        MnSwapClause.SwapId = swappedClause.SwapClause__c;
                        MnSwapClause.Newclause =  ((SwapClause__c)AllClausesMap.get(swappedClause.SwapClause__c)).Name;
                        MnSwapClause.CreatedDateTime = swappedClause.CreatedDate;
                        MnSwapClause.Status = swappedClause.Status__c;
                        MnSwapClause.Type = 'Clause';
                        ManagedSwapClauseWrapperList.add(MnSwapClause);
                    } else if(swappedClause.SubClause__c != null){
                        MnSwapClause.ObjectId = swappedClause.Id;
                        MnSwapClause.StandardId = swappedClause.SubClause__c;
                        MnSwapClause.StandardClause = ((SubClause__c)AllClausesMap.get(swappedClause.SubClause__c)).Name;
                        MnSwapClause.SwapId = swappedClause.SwapClause__c;
                        MnSwapClause.Newclause =  ((SwapClause__c)AllClausesMap.get(swappedClause.SwapClause__c)).Name;
                        MnSwapClause.CreatedDateTime = swappedClause.CreatedDate;
                        MnSwapClause.Status = swappedClause.Status__c;
                        MnSwapClause.Type = 'SubClause';
                        ManagedSwapClauseWrapperList.add(MnSwapClause);
                    } else if (swappedClause.SubSubClause__c != null){
                        MnSwapClause.ObjectId = swappedClause.Id;
                        MnSwapClause.StandardId = swappedClause.SubSubClause__c;
                        MnSwapClause.StandardClause = ((SubSubClause__c)AllClausesMap.get(swappedClause.SubSubClause__c)).Name;
                        MnSwapClause.SwapId = swappedClause.SwapClause__c;
                        MnSwapClause.Newclause = ((SwapClause__c)AllClausesMap.get(swappedClause.SwapClause__c)).Name;
                        MnSwapClause.CreatedDateTime = swappedClause.CreatedDate;
                        MnSwapClause.Status = swappedClause.Status__c;
                        MnSwapClause.Type = 'SubSubClause';
                        ManagedSwapClauseWrapperList.add(MnSwapClause);
                    }
                }
                return ManagedSwapClauseWrapperList;
            }
            return ManagedSwapClauseWrapperList;

        } catch (Exception e) {
            throw e;
        }
    }


    public static Boolean saveSwappedClauses(List<ManagedSwapClauseWrapper> swappedClauseList){
        try {
            Boolean bolReturn = false;
            if (!swappedClauseList.isEmpty()){
                List<ManagedSwapClause__c> managedSwapClauseList = new List<ManagedSwapClause__c>();
                //Need to update the contract to Terms and conditions when a swap is added
                Set<Id> ContractAgreementSetId = new Set<Id>();

                for(ManagedSwapClauseWrapper swapClauseWrapper : swappedClauseList){
                    ManagedSwapClause__c managedSwapClause = new ManagedSwapClause__c();
                    if (swapClauseWrapper.objectId ==null){
                    switch on swapClauseWrapper.Type {
                        when  'Clause'{
                            managedSwapClause.id = swapClauseWrapper.objectId !=null? swapClauseWrapper.objectId : null;
                            managedSwapClause.Clause__c = swapClauseWrapper.StandardId;
                            managedSwapClause.ContractAgreement__c = swapClauseWrapper.ContractAgreementId;
                            managedSwapClause.Status__c = swapClauseWrapper.Status;
                            managedSwapClause.SwapClause__c = swapClauseWrapper.SwapId;
                            managedSwapClauseList.add(managedSwapClause);
                            
                            ContractAgreementSetId.add(managedSwapClause.ContractAgreement__c);
                        }
                        when  'SubClause'{
                            managedSwapClause.id = swapClauseWrapper.objectId !=null? swapClauseWrapper.objectId : null;
                            managedSwapClause.SubClause__c = swapClauseWrapper.StandardId;
                            managedSwapClause.ContractAgreement__c = swapClauseWrapper.ContractAgreementId;
                            managedSwapClause.Status__c = swapClauseWrapper.Status;
                            managedSwapClause.SwapClause__c = swapClauseWrapper.SwapId;
                            managedSwapClauseList.add(managedSwapClause);

                            ContractAgreementSetId.add(managedSwapClause.ContractAgreement__c);
                        }
                        when  'SubSubClause'{
                            managedSwapClause.id = swapClauseWrapper.objectId !=null? swapClauseWrapper.objectId : null;
                            managedSwapClause.SubSubClause__c = swapClauseWrapper.StandardId;
                            managedSwapClause.ContractAgreement__c = swapClauseWrapper.ContractAgreementId;
                            managedSwapClause.Status__c = swapClauseWrapper.Status;
                            managedSwapClause.SwapClause__c = swapClauseWrapper.SwapId;
                            managedSwapClauseList.add(managedSwapClause);

                            ContractAgreementSetId.add(managedSwapClause.ContractAgreement__c);
                        }
                    }
                }
                }

                //Need to update the contract to Terms and conditions when a swap is added
                List<APXT_Redlining__Contract_Agreement__c> contractAgreementListToSetToTCs = [SELECT Id, Include_full_T_C_s__c 
                                                                                               FROM   
                                                                                               APXT_Redlining__Contract_Agreement__c
                                                                                               WHERE  Include_full_T_C_s__c = false
                                                                                               AND Id 
                                                                                               IN :  ContractAgreementSetId  ];
                for (APXT_Redlining__Contract_Agreement__c contractAgreement : contractAgreementListToSetToTCs){
                    contractAgreement.Include_full_T_C_s__c = true;
                }

                update contractAgreementListToSetToTCs;

                insert managedSwapClauseList;
                bolReturn = true;
            }
            return bolReturn;
        }
        catch (Exception ex){
            throw ex;
        }
        
    }

    }