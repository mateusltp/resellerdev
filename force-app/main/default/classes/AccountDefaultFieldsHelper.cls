/**
 * @description       : 
 * @author            : AlyssonMota
 * @group             : 
 * @last modified on  : 06-18-2021
 * @last modified by  : Alysson Mota
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   06-18-2021   Alysson Mota   Initial Version
**/
public without sharing class AccountDefaultFieldsHelper {
    private Id clientRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
    
    public void assignGympassEntity() {
        List<Account> gympassEntites = new AccountRepository().gympassEntities();
        Map<String, Account> countryToAccount = getMapGympassEntities(gympassEntites);
        List<Account> lstNew = trigger.new;

        for (Account acc : lstNew) {    
            Account entity = countryToAccount.get(acc.BillingCountryCode);
            
            if (acc.RecordTypeId == clientRecTypeId && entity != null) {
                acc.Standard_Gympass_Account_Entity__c = entity.Id; 
            }
        }
    }

    private Map<String, Account> getMapGympassEntities(List<Account> entities) {
        Map<String, Account> countryToAccount = new Map<String, Account>();
        
        for (Account acc : entities) {
            countryToAccount.put(acc.BillingCountryCode, acc);
        }

        return countryToAccount;
    }
}