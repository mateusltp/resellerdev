@isTest
public class OfferLetterPdfControllerTest {

    @isTest
    public static void functionalTest() {
        Account acc = new Account();
        acc.Name = 'Acc Teste';
        acc.BillingState = 'São Paulo';
        acc.BillingCountry = 'Brazil';
        insert acc;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Opp Test';
        opp.StageName = 'Qualificação';
        opp.CloseDate = Date.today();
        opp.AccountId = acc.Id;
        opp.A_Authority_Opp_Val__c = 'Yes';
        opp.CH_Challenges_Opp_Val__c = 'Yes';
        opp.M_Money_Opp_Val__c = 'Yes';
        opp.P_Priority_Opp_Val__c = 'Yes';
        insert opp;

        opp.StageName = 'Oportunidade Validada Após 1ª Reunião';
        update opp;

        Quote proposal = new Quote();
        proposal.Name = 'Test Proposal';
        proposal.OpportunityId = opp.Id;
        proposal.License_Fee_Waiver__c = 'No';
        proposal.Submission_Date__c = Date.today();
        proposal.Proposal_End_Date__c = Date.today().addDays(20);
        insert proposal;

        opp.SyncedQuoteId = proposal.Id;
        update opp;

        PageReference pageRef = Page.OfferLetterPdf;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id', String.valueOf(opp.Id));
        ApexPages.StandardController sc = new ApexPages.StandardController(opp);        

        OfferLetterPdfController testOfferLetter = new OfferLetterPdfController(sc);
    }

}