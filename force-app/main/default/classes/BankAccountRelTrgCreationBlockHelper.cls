/**
 * @File Name          : BankAccountRelationTriggerCreationBlockHelper.cls
 * @Description        : 
 * @Author             : GEPI@GFT.com
 * @Group              : 
 * @Last Modified By   : gft.samuel.silva@ext.gympass.com
 * @Last Modified On   : 04-26-2022
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    20/05/2020   GEPI@GFT.com     Initial Version
**/
public class BankAccountRelTrgCreationBlockHelper {
    
     //Get RecordTypeId for Gym Partner Account
     Id recordTypeId = Schema.SObjectType.Acount_Bank_Account_Relationship__c.getRecordTypeInfosByDeveloperName().get('Gyms_Account_Bank_Account_Partner').getRecordTypeId();

     public void blockOnBeforeInsert(List<Acount_Bank_Account_Relationship__c> lstNew){
        
        Set<id> triggerBnkIds = new Set<id>();

        for(Acount_Bank_Account_Relationship__c bnkItem:lstNew)
        {
            if(bnkItem.RecordTypeID == recordTypeId){
                triggerBnkIds.add(bnkItem.Account__c);   
            }
        }

        if(!triggerBnkIds.isEmpty()){
            List<Acount_Bank_Account_Relationship__c> lstBnk = [SELECT Account__c FROM Acount_Bank_Account_Relationship__c where Account__c IN :triggerBnkIds];
      
            if(lstBnk.size()>=1){
                (new InsertMoreThanOneError()).doAction();
            }
        }
    }

    public class InsertMoreThanOneError {
        public InsertMoreThanOneError() {}

        public void doAction() {
            // Trigger.new is valid here
            SObject[] sobjects = Trigger.new;
            for (Sobject sobj : sobjects) {
                sobj.addError('Can\'t insert more than one Bank Account related to a Account');
            }
        }
    }
}