/*
* @author Bruno Pinho
* @date January/2019
* @description This class has helper methods.
*/

public with sharing class UtilityClass
{
    public static String getSObjectTypeName(SObject so)
    {
        return so.getSObjectType().getDescribe().getName();
    }
}