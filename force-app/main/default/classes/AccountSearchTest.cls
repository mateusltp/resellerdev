@IsTest
public class AccountSearchTest {  

     @TestSetup
     static void makeData(){

         Account account = new Account();  
         account.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Reseller').getRecordTypeId();
         account.Name = 'teste';
         account.Legal_Document_Type__c = 'CNPJ';
         account.Id_Company__c = '56947401000108';
         account.Razao_Social__c = 'TESTE';
         account.Website = 'teste.com';
         account.Industry = 'Airlines'; 						
         account.BillingCountry = 'Brazil';
         insert account;
         

        
     }

     @isTest
     public static void testAccountSearchLegalNumber(){

         Map<String, AccountSearch.AccountSearchWrapper> accountSearchWrapper_map = new Map<String,AccountSearch.AccountSearchWrapper>();
        
         List<Account_Request__c> acc_req_list = new List<Account_Request__c>();
         acc_req_list.add(new Account_Request__c(Name = 'Teste 21', Unique_Identifier__c = '56947401000108', Website__c = 'teste.com'));
         acc_req_list.add(new Account_Request__c(Name = 'Teste 24', Unique_Identifier__c = '10417635000142', Website__c = 'teste24.com'));
         insert acc_req_list;

         Test.startTest();
         AccountSearch accountSearch = new AccountSearch(acc_req_list);
         accountSearchWrapper_map = accountSearch.getAccounts();
         accountSearch.SearchAccountsMassive(acc_req_list);
         
         AccountSearch accountSearch2 = new AccountSearch(new Set<String>{acc_req_list[0].Unique_Identifier__c}, new Set<String>{ acc_req_list[0].Name}, new Set<String>{acc_req_list[0].Website__c});
         Test.stopTest();

         System.assertEquals(false, accountSearchWrapper_map.isEmpty());

     }

     @isTest
     public static void testAccountSearchLegalNumberTrunk(){

         Map<String, AccountSearch.AccountSearchWrapper> accountSearchWrapper_map = new Map<String,AccountSearch.AccountSearchWrapper>();
        
         List<Account_Request__c> acc_req_list = new List<Account_Request__c>();
         acc_req_list.add(new Account_Request__c(Name = 'Teste 21', Unique_Identifier__c = '56947401', Website__c = 'teste.com'));
         insert acc_req_list;

         Test.startTest();
         AccountSearch accountSearch = new AccountSearch(acc_req_list);
         accountSearchWrapper_map = accountSearch.getAccounts();
         Test.stopTest();

         System.assertEquals(false, accountSearchWrapper_map.isEmpty());

     }

     @isTest
     public static void testAccountSearchWebSite(){

         Map<String, AccountSearch.AccountSearchWrapper> accountSearchWrapper_map = new Map<String,AccountSearch.AccountSearchWrapper>();
        
         List<Account_Request__c> acc_req_list = new List<Account_Request__c>();
         acc_req_list.add(new Account_Request__c(Name = 'Teste 21', Unique_Identifier__c = '1231231231', Website__c = 'teste.com'));
         insert acc_req_list;

         Test.startTest();
         AccountSearch accountSearch = new AccountSearch(acc_req_list);
         accountSearchWrapper_map = accountSearch.getAccounts();
         Test.stopTest();

         System.assertEquals(false, accountSearchWrapper_map.isEmpty());

     }

     @isTest
     public static void testAccountSearchName(){

         Map<String, AccountSearch.AccountSearchWrapper> accountSearchWrapper_map = new Map<String,AccountSearch.AccountSearchWrapper>();
        
         List<Account_Request__c> acc_req_list = new List<Account_Request__c>();
         acc_req_list.add(new Account_Request__c(Name = 'teste', Unique_Identifier__c = '1231231231', Website__c = 'www.google.com'));
         
         insert acc_req_list;

         Test.startTest();
         AccountSearch accountSearch = new AccountSearch(acc_req_list);
         accountSearchWrapper_map = accountSearch.getAccounts();
         
         Test.stopTest();

     }

    /*@isTest
	public static void uniqueIdentifierSearchTest() {
      
      Account account = new Account();  
      account.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Reseller').getRecordTypeId();
      account.Name = 'teste';
      account.Legal_Document_Type__c = 'CNPJ';
      account.Id_Company__c = '56947401000108';
      account.Razao_Social__c = 'TESTE';
      account.Website = 'teste.com';
      account.Industry = 'Airlines'; 						
      account.BillingCountry = 'Brazil';
      
      insert account;
      
      Namesake__c namesake = new Namesake__c();
      
      List <Namesake__c> namesakeList = new List<Namesake__c>();
      
      namesake.name = 'testeeee';
      namesake.Account__c = account.Id;
      
      Namesake__c namesake2 = new Namesake__c();
      
      namesake2.name = 'teste';
      namesake2.Account__c = account.Id;
      
      namesakeList.add(namesake);
      namesakeList.add(namesake2);
      
      insert namesakeList;
      
      
      Test.startTest();
      
          AccountSearch.uniqueIdentifierSearch(new List<String> {'56947401000108'}, 'Brazil', '','', 'single');
          
          AccountSearch.uniqueIdentifierSearch(new List<String> {'56947401000'}, 'Brazil', 'teste','', 'single');
        
        AccountSearch.uniqueIdentifierSearch(new List<String> {'55555'}, 'Brazil', '','teste', 'single');
      
      Test.stopTest(); 
	}
    
     @isTest
	public static void searchQuerySOSLTest() {
      
      List<String> teste = new List<String>();
      
      Account account = new Account();  
      account.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Reseller').getRecordTypeId();
      account.Name = 'teste2';
      account.Legal_Document_Type__c = 'CNPJ';
      account.Id_Company__c = '56947401000108';
      account.Razao_Social__c = 'TESTE';
      account.Website = 'teste2.com';
      account.Industry = 'Airlines'; 						
      account.BillingCountry = 'Brazil';
      
      insert account;
      
      List <Namesake__c> namesakeList = new List<Namesake__c>();
      
      Namesake__c namesake = new Namesake__c();
      
      namesake.name = 'teste22';
      namesake.Account__c = account.Id;
      
      Namesake__c namesake2 = new Namesake__c();
      
      namesake2.name = 'teste';
      namesake2.Account__c = account.Id;
      
      namesakeList.add(namesake);
      namesakeList.add(namesake2);
      
      insert namesakeList;
      
      Test.startTest();
      
          AccountSearch.uniqueIdentifierSearch(teste, '', 'teste2','', 'single');
      
      Test.stopTest(); 
	}*/
}