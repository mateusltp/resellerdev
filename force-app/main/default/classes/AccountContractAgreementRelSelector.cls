/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 04-19-2022
 * @last modified by  : alysson.mota@gympass.com
**/
public inherited sharing  class AccountContractAgreementRelSelector extends ApplicationSelector {
  

    public List<Schema.SObjectField> getSObjectFieldList() {
		return new List<Schema.SObjectField> {
			Account_Contract_Agreement_Relationship__c.Id
		};
	}

	public Schema.SObjectType getSObjectType() {
		return Account_Contract_Agreement_Relationship__c.sObjectType;
	}

	public List<Account_Contract_Agreement_Relationship__c> byOppId(Set<Id> oppIdSet) {
		return (List<Account_Contract_Agreement_Relationship__c>) Database.query( 	
			newQueryFactory().				
			selectField(Account_Contract_Agreement_Relationship__c.AccountId__c).
			selectField('AccountId__r.Id').
			selectField('ContractAgreementId__c').
			selectField('ContractAgreementId__r.Opportunity__c').
			selectField('AccountId__r.Name').
			selectField('AccountId__r.Gym_Title_on_Gympass_App__c').
			selectField('AccountId__r.Phone').		
			selectField('AccountId__r.Email__c').	
			selectField('AccountId__r.Gym_Email__c').	
			selectField('AccountId__r.Description').	
			selectField('AccountId__r.Website').
			selectField('AccountId__r.Opening_Status__c').	
			selectField('AccountId__r.Validation_type__c').	
			selectField('AccountId__r.Bank_Account_Owner__c').
			selectField('AccountId__r.ShippingCountryCode').	
			selectField('AccountId__r.BillingCountryCode').										
			setCondition('ContractAgreementId__r.Opportunity__c IN :oppIdSet').
			toSOQL());
	}

	public Map<Id, List<Account_Contract_Agreement_Relationship__c>> selectByOpportunityId (Set<Id> opportunityId) {	
		Map<Id, List<Account_Contract_Agreement_Relationship__c>> result = new Map<Id, List<Account_Contract_Agreement_Relationship__c>>();
		for(Account_Contract_Agreement_Relationship__c iAccContractRl : ( List<Account_Contract_Agreement_Relationship__c> ) Database.query( 	
																			newQueryFactory().				
																			selectField('AccountId__r.Id').
																			selectField('ContractAgreementId__c').
																			selectField('ContractAgreementId__r.Opportunity__c').
																			selectField('AccountId__r.Name').
																			selectField('AccountId__r.Gym_Title_on_Gympass_App__c').
																			selectField('AccountId__r.Phone').		
																			selectField('AccountId__r.Email__c').	
																			selectField('AccountId__r.Gym_Email__c').	
																			selectField('AccountId__r.Description').	
																			selectField('AccountId__r.Website').
																			selectField('AccountId__r.Opening_Status__c').	
																			selectField('AccountId__r.Validation_type__c').	
																			selectField('AccountId__r.Bank_Account_Owner__c').
																			selectField('AccountId__r.ShippingCountryCode').	
																			selectField('AccountId__r.BillingCountryCode').										
																			setCondition('ContractAgreementId__r.Opportunity__c =: opportunityId').
																			toSOQL()
										)){
			if(result.containsKey(iAccContractRl.ContractAgreementId__r.Opportunity__c)){
				result.get(iAccContractRl.ContractAgreementId__r.Opportunity__c).add(iAccContractRl);
			} else{
				result.put(iAccContractRl.ContractAgreementId__r.Opportunity__c, new List<Account_Contract_Agreement_Relationship__c>{iAccContractRl});
			}
		}
		return result;
	}
}