/**
* @description       :
* @author            : GEPI@GFT.com
* @group             :
* @last modified on  : 10-10-2021
* @last modified by  : eurikesse.ferreira@gympass.com
* Modifications Log
* Ver   Date         Author         Modification
* 1.0   02-05-2021   GEPI@GFT.com   Initial Version
**/
@isTest
public class PipefyIntegrationTest{
  

   @isTest
   private static void testCreationForSmbAccessFeeEntry(){
      
       Account lAcc = DataFactory.newAccount();
       lAcc.NumberOfEmployees = 500;
       Database.insert( lAcc );

       Pricebook2 lStandardPb = DataFactory.newPricebook();
       Database.update( lStandardPb );
 
       Opportunity lOpp = new Opportunity(CurrencyIsoCode='BRL');
 
       lOpp = DataFactory.newOpportunity( lAcc.Id , lStandardPb , 'SMB_New_Business' );
       lOpp.Quantity_Offer_Number_of_Employees__c = 500;
       lopp.FastTrackStage__c = 'Launched/Won';
       lopp.StageName = 'Lançado/Ganho';
       lopp.Type = 'Expansion';  
       lopp.Gympass_Plus__c = 'Yes';
       lopp.Country_Manager_Approval__c = true;
       lopp.Payment_approved__c = true;  
       lopp.Standard_Payment__c = 'Yes';
       lopp.Request_for_self_checkin__c = 'Yes';
       lopp.Comments__c = 'Comments';
       Database.insert( lOpp );

       AccountOpportunityRepository accountsInOpp = new AccountOpportunityRepository(lOpp.Id);
       accountsInOpp.setRelationshipAccountWithOppForClients(new List<Account>{lAcc});
 
       Product2 lAcessFee = DataFactory.newProduct( 'Enterprise Subscription' , false , 'BRL' );
       Database.insert( lAcessFee );
 
       PricebookEntry lAccessFeeEntry = DataFactory.newPricebookEntry( lStandardPb , lAcessFee , lOpp );
       Database.insert( lAccessFeeEntry );
 
       Quote lProposal = DataFactory.newQuote( lOpp , 'Quote Test', 'Client_Sales_New_Business' );
       lProposal.License_Fee_Waiver__c = 'Yes';
       lProposal.Waiver_Termination_Date__c = system.today().addDays(10);
       lProposal.Will_this_company_have_the_Free_Product__c = 'No';
       Database.insert( lProposal );

       //access fee
       QuoteLineItem lEnterpriseSubscription = DataFactory.newQuoteLineItem( lProposal , lAccessFeeEntry );
       lEnterpriseSubscription.Discount__c = 49.0;
       Database.insert( lEnterpriseSubscription );

      Payment__c payForAccessFee = DataFactory.newPayment(lEnterpriseSubscription);
      payForAccessFee.Payment_Due_Days__c = '30 days';
      payForAccessFee.Account__c =  lAcc.Id;
      Database.insert( payForAccessFee );

      Eligibility__c eli = DataFactory.newEligibility(payForAccessFee);
      eli.Payment__c = payForAccessFee.Id;
      eli.Payment_Due_Days__c = '30 days';
      eli.Billing_Day__c = '05';
      Database.insert( eli );

      Waiver__c wai = DataFactory.newWaiver(payForAccessFee);
      Database.insert( wai );

      Waiver__c wai2 = DataFactory.newWaiver(payForAccessFee);
      wai2.Start_Date__c = system.today().adddays(11);
      wai2.End_Date__c = system.today().addDays(20);
      Database.insert( wai2 );
 
       Test.startTest();
     
       Test.setMock(HttpCalloutMock.class, new Mock());
     
       Opportunity opp = [select Id,Name,CurrencyIsoCode from Opportunity where recordtype.developername = 'SMB_New_Business' limit 1];
 
       Integer statusCode;
       try {
           statusCode = PipefyIntegrationController.auraHandler(opp.Id, 'Opportunity');

       } catch(Exception e) {
           System.debug('Error');
       }
      
       opp = [select Id,Name,CurrencyIsoCode, Pipefy_Card_ID__c from Opportunity where recordtype.developername = 'SMB_New_Business' limit 1];
      
       Test.stopTest();
      
       System.assertEquals('405722846', opp.Pipefy_Card_ID__c);
       System.assertEquals(200, statusCode);
   }
    
     @isTest
   private static void testCreationForIndirectChannel(){

       Account lAcc = DataFactory.newAccount();
       lAcc.NumberOfEmployees = 500;
       Database.insert( lAcc );

       Pricebook2 lStandardPb = DataFactory.newPricebook();
       Database.update( lStandardPb );
 
       Opportunity lOpp = new Opportunity(CurrencyIsoCode='BRL');

       TriggerHandler.bypass('OpportunityTriggerHandler');
       lOpp = DataFactory.newOpportunity( lAcc.Id , lStandardPb , 'Indirect_Channel_New_Business' );
       lOpp.Quantity_Offer_Number_of_Employees__c = 500;
       lopp.FastTrackStage__c = 'Setup';
       lOpp.Comments__c = 'Test';
       Database.insert( lOpp );

       EnablersFactory enablersFactory = new EnablersFactory();
       List<Step_Towards_Success1__c> oppEnablers = enablersFactory.createEnablersForOpportunityClients(lOpp);
       insert oppEnablers ;

       Product2 lAcessFee = DataFactory.newProduct( 'Enterprise Subscription' , false , 'BRL' );
       Database.insert( lAcessFee );
 
       PricebookEntry lAccessFeeEntry = DataFactory.newPricebookEntry( lStandardPb , lAcessFee , lOpp );
       Database.insert( lAccessFeeEntry );

       PriceEngine__c priceEngine = new PriceEngine__c( CurrencyIsoCode = 'USD');
       insert priceEngine;

       PriceEngineItem__c priceEngineItem = new PriceEngineItem__c(Absolute_Value__c = 12.99, 
       PriceEngineId__c = priceEngine.Id, 
       BusinessRuleFor__c = 'Intermediation', 
       CommissionPercentage__c = 30, 
       MaximumNumberEmployees__c = 1000,
       CurrencyIsoCode = 'USD', 
       MinimumNumberEmployees__c = 1,
       PartnerCategory__c = 'Standard');
      insert priceEngineItem;

       TriggerHandler.bypass('OpportunityTriggerHandler');
       Quote lProposal = DataFactory.newQuote( lOpp , 'Quote Test', 'Indirect_Channel');
       Database.insert( lProposal );
 
       QuoteLineItem lEnterpriseSubscription = DataFactory.newQuoteLineItem( lProposal , lAccessFeeEntry );
       lEnterpriseSubscription.Discount__c = 49.0;
       Database.insert( lEnterpriseSubscription );

       SKU_Price__c skuPriceFamily = DataFactory.newSKUPrice(lAcessFee.Id, lOpp, lAcc, 1, 1);
       insert skuPriceFamily;

       Payment__c payForAccessFee = DataFactory.newPayment(lOpp);
       payForAccessFee.Payment_Due_Days__c = '30 days';
       payForAccessFee.Account__c =  lAcc.Id;
       payForAccessFee.Quote_Line_Item__c = lEnterpriseSubscription.id;
       Database.insert( payForAccessFee );
       
       Test.startTest();
     
       Test.setMock(HttpCalloutMock.class, new Mock());
       Integer statusCode;
       try {
           statusCode = PipefyIntegrationController.auraHandler(lOpp.Id, 'Opportunity');
       } catch(Exception e) {
           System.debug('Error');
       }
      
       Test.stopTest();
       System.assertEquals(200, statusCode);
   }
    
       @isTest
   private static void testCreationForSmbSetupFee(){
      
       Account lAcc = DataFactory.newAccount();
       lAcc.NumberOfEmployees = 500;
       Database.insert( lAcc );

       Pricebook2 lStandardPb = DataFactory.newPricebook();
       Database.update( lStandardPb );
 
       Opportunity lOpp = new Opportunity(CurrencyIsoCode='BRL');
 
       lOpp = DataFactory.newOpportunity( lAcc.Id , lStandardPb , 'SMB_New_Business' );
       lOpp.Quantity_Offer_Number_of_Employees__c = 500;
       //lopp.Pipefy_Card_ID__c = '405722846';
       lopp.FastTrackStage__c = 'Launched/Won';
       lopp.StageName = 'Lançado/Ganho';
       lopp.Type = 'Expansion';  
       lopp.Gympass_Plus__c = 'Yes';
       lopp.Country_Manager_Approval__c = true;
       lopp.Payment_approved__c = true;  
       lopp.Standard_Payment__c = 'Yes';
       lopp.Request_for_self_checkin__c = 'Yes';
       lopp.Comments__c = 'Comments';
       Database.insert( lOpp );

       AccountOpportunityRepository accountsInOpp = new AccountOpportunityRepository(lOpp.Id);
       accountsInOpp.setRelationshipAccountWithOppForClients(new List<Account>{lAcc});
 
       Product2 lAcessFee = DataFactory.newProduct( 'Setup Fee' , false , 'BRL' );
       Database.insert( lAcessFee );
 
       PricebookEntry lAccessFeeEntry = DataFactory.newPricebookEntry( lStandardPb , lAcessFee , lOpp );
       Database.insert( lAccessFeeEntry );
 
       Quote lProposal = DataFactory.newQuote( lOpp , 'Quote Test', 'Client_Sales_New_Business' );
       lProposal.License_Fee_Waiver__c = 'Yes';
       lProposal.Waiver_Termination_Date__c = system.today().addDays(10);
       lProposal.Will_this_company_have_the_Free_Product__c = 'No';
       Database.insert( lProposal );

       //setup fee
       QuoteLineItem lEnterpriseSubscription = DataFactory.newQuoteLineItem( lProposal , lAccessFeeEntry );
       lEnterpriseSubscription.Discount__c = 49.0;
       Database.insert( lEnterpriseSubscription );

      Payment__c payForAccessFee = DataFactory.newPayment(lEnterpriseSubscription);
      payForAccessFee.Payment_Due_Days__c = '30 days';
      payForAccessFee.Account__c =  lAcc.Id;
      Database.insert( payForAccessFee );

      Eligibility__c eli = DataFactory.newEligibility(payForAccessFee);
      eli.Payment__c = payForAccessFee.Id;
      eli.Payment_Due_Days__c = '30 days';
      eli.Billing_Day__c = '05';
      Database.insert( eli );

      Waiver__c wai = DataFactory.newWaiver(payForAccessFee);
      Database.insert( wai );

       Test.startTest();
     
       Test.setMock(HttpCalloutMock.class, new Mock());
     
       Opportunity opp = [select Id,Name,CurrencyIsoCode, Pipefy_Card_ID__c from Opportunity where recordtype.developername = 'SMB_New_Business' limit 1];
 
       Integer statusCode;
       try {
           statusCode = PipefyIntegrationController.auraHandler(opp.Id, 'Opportunity');

       } catch(Exception e) {
           System.debug('Error');
       }
      
       Test.stopTest();
      
        System.assertEquals(null, opp.Pipefy_Card_ID__c);
       System.assertEquals(null, statusCode);
   }

 @isTest
   private static void testCreationProServiceOneFee(){
      
       Account lAcc = DataFactory.newAccount();
       lAcc.NumberOfEmployees = 500;
       Database.insert( lAcc );

       Pricebook2 lStandardPb = DataFactory.newPricebook();
       Database.update( lStandardPb );
 
       Opportunity lOpp = new Opportunity(CurrencyIsoCode='BRL');
 
       lOpp = DataFactory.newOpportunity( lAcc.Id , lStandardPb , 'Client_Sales_New_Business' );
       lOpp.Quantity_Offer_Number_of_Employees__c = 500;
       lopp.FastTrackStage__c = 'Launched/Won';
       lopp.StageName = 'Lançado/Ganho';
       lopp.Type = 'Expansion';  
       lopp.Gympass_Plus__c = 'Yes';
       lopp.Country_Manager_Approval__c = true;
       lopp.Payment_approved__c = true;  
       lopp.Standard_Payment__c = 'Yes';
       lopp.Request_for_self_checkin__c = 'Yes';
       lopp.Comments__c = 'Comments';
       Database.insert( lOpp );

       AccountOpportunityRepository accountsInOpp = new AccountOpportunityRepository(lOpp.Id);
       accountsInOpp.setRelationshipAccountWithOppForClients(new List<Account>{lAcc});
 
       Product2 lAcessFee = DataFactory.newProduct( 'Professional Services Setup Fee' , false , 'BRL' );
       lAcessFee.Minimum_Number_of_Employees__c = 0;
       lAcessFee.Maximum_Number_of_Employees__c = 900;
       lAcessFee.Family = 'Professional Services';
       lAcessFee.IsActive = true;
       lAcessFee.Family_Member_Included__c = true;
       Database.insert( lAcessFee );
 
       PricebookEntry lAccessFeeEntry = DataFactory.newPricebookEntry( lStandardPb , lAcessFee , lOpp );
       Database.insert( lAccessFeeEntry );
 
       Quote lProposal = DataFactory.newQuote( lOpp , 'Quote Test', 'Client_Sales_New_Business' );
       lProposal.License_Fee_Waiver__c = 'Yes';
       lProposal.Waiver_Termination_Date__c = system.today().addDays(10);
       lProposal.Will_this_company_have_the_Free_Product__c = 'No';
       Database.insert( lProposal );

          //pro services one fee
       QuoteLineItem lEnterpriseSubscription = DataFactory.newQuoteLineItem( lProposal , lAccessFeeEntry );
       lEnterpriseSubscription.Discount__c = 49.0;
       Database.insert( lEnterpriseSubscription );

      Payment__c payForAccessFee = DataFactory.newPayment(lEnterpriseSubscription);
      payForAccessFee.Payment_Due_Days__c = '30 days';
      payForAccessFee.Account__c =  lAcc.Id;
      Database.insert( payForAccessFee );

       Test.startTest();
     
       Test.setMock(HttpCalloutMock.class, new Mock());
     
       Opportunity opp = [select Id,Name,CurrencyIsoCode,Pipefy_Card_ID__c from Opportunity where recordtype.developername = 'Client_Sales_New_Business' limit 1];
 
       Integer statusCode;
       try {
           statusCode = PipefyIntegrationController.auraHandler(opp.Id, 'Opportunity');

       } catch(Exception e) {
           System.debug('Error');
       }
      
       Test.stopTest();
      
       System.assertEquals(null, opp.Pipefy_Card_ID__c);
       System.assertEquals(null, statusCode);
   }
    
     @isTest
   private static void testCreationProServiceMainFee(){ 
      
       Account lAcc = DataFactory.newAccount();
       lAcc.NumberOfEmployees = 500;
       Database.insert( lAcc );

       Pricebook2 lStandardPb = DataFactory.newPricebook();
       Database.update( lStandardPb );
 
       Opportunity lOpp = new Opportunity(CurrencyIsoCode='BRL');
 
       lOpp = DataFactory.newOpportunity( lAcc.Id , lStandardPb , 'Client_Success_Renegotiation' );
       lOpp.Quantity_Offer_Number_of_Employees__c = 500;
       lopp.FastTrackStage__c = 'Launched/Won';
       lopp.StageName = 'Lançado/Ganho';
       lopp.Type = 'Expansion';  
       lopp.Gympass_Plus__c = 'Yes';
       lopp.Country_Manager_Approval__c = true;
       lopp.Payment_approved__c = true;  
       lopp.Standard_Payment__c = 'Yes';
       lopp.Request_for_self_checkin__c = 'Yes';
       lopp.Comments__c = 'Comments';
       Database.insert( lOpp );

       AccountOpportunityRepository accountsInOpp = new AccountOpportunityRepository(lOpp.Id);
       accountsInOpp.setRelationshipAccountWithOppForClients(new List<Account>{lAcc});
 
       Product2 lAcessFee = DataFactory.newProduct( 'Professional Services Maintenance Fee' , false , 'BRL' );
       lAcessFee.Minimum_Number_of_Employees__c = 0;
       lAcessFee.Maximum_Number_of_Employees__c = 900;
       lAcessFee.Family = 'Professional Services';
       lAcessFee.IsActive = true;
       lAcessFee.Family_Member_Included__c = true;
       Database.insert( lAcessFee );

 
       PricebookEntry lAccessFeeEntry = DataFactory.newPricebookEntry( lStandardPb , lAcessFee , lOpp );
       Database.insert( lAccessFeeEntry );
 
       Quote lProposal = DataFactory.newQuote( lOpp , 'Quote Test', 'Client_Sales_New_Business' );
       lProposal.License_Fee_Waiver__c = 'Yes';
       lProposal.Waiver_Termination_Date__c = system.today().addDays(10);
       lProposal.Will_this_company_have_the_Free_Product__c = 'No';
       Database.insert( lProposal );

       // pro services main fee
       QuoteLineItem lEnterpriseSubscription = DataFactory.newQuoteLineItem( lProposal , lAccessFeeEntry );
       lEnterpriseSubscription.Discount__c = 49.0;
       Database.insert( lEnterpriseSubscription );

      Payment__c payForAccessFee = DataFactory.newPayment(lEnterpriseSubscription);
      payForAccessFee.Payment_Due_Days__c = '30 days';
      payForAccessFee.Account__c =  lAcc.Id;
      Database.insert( payForAccessFee );

       Test.startTest();
     
       Test.setMock(HttpCalloutMock.class, new Mock());
     
       Opportunity opp = [select Id,Name,CurrencyIsoCode, Pipefy_Card_ID__c from Opportunity where recordtype.developername = 'Client_Success_Renegotiation' limit 1];
 
       Integer statusCode;
       try {
           statusCode = PipefyIntegrationController.auraHandler(opp.Id, 'Opportunity');

       } catch(Exception e) {
           System.debug('Error');
       }
           
       Test.stopTest();
      
       System.assertEquals(null, opp.Pipefy_Card_ID__c);
       System.assertEquals(null, statusCode);
   }
    
     @isTest
   private static void testCreationForSmbNull(){
      
       Account lAcc = DataFactory.newAccount();
       lAcc.NumberOfEmployees = 500;
       Database.insert( lAcc );
 
       Pricebook2 lStandardPb = DataFactory.newPricebook();
       Database.update( lStandardPb );
 
       Opportunity lOpp = new Opportunity(CurrencyIsoCode='BRL');
 
       lOpp = DataFactory.newOpportunity( lAcc.Id , lStandardPb , 'SMB_Success_Renegotiation' );
       lOpp.Quantity_Offer_Number_of_Employees__c = 500;
       lopp.FastTrackStage__c = 'Launched/Won';
       lopp.StageName = 'Lançado/Ganho';
       lopp.Self_Checkout_New_Business__c = false;
       Database.insert( lOpp );

       AccountOpportunityRepository accountsInOpp = new AccountOpportunityRepository(lOpp.Id);
       accountsInOpp.setRelationshipAccountWithOppForClients(new List<Account>{lAcc});
 
       Test.startTest();
     
       Test.setMock(HttpCalloutMock.class, new Mock());
     
       Opportunity opp = [select Id,Name,CurrencyIsoCode, Pipefy_Card_ID__c from Opportunity where recordtype.developername = 'SMB_Success_Renegotiation' limit 1];
 
       Integer statusCode;
       try {
           statusCode = PipefyIntegrationController.auraHandler(opp.Id, 'Opportunity');

       } catch(Exception e) {
           System.debug('Error');
       }
     
      
       Test.stopTest();
      
       System.assertEquals(null, opp.Pipefy_Card_ID__c);
       System.assertEquals(null, statusCode);
   }

    @isTest
   private static void testCreationForSmbToCase(){
      
       Account lAcc = DataFactory.newAccount();
       lAcc.NumberOfEmployees = 500;
       Database.insert( lAcc );
 
       Pricebook2 lStandardPb = DataFactory.newPricebook();
       Database.update( lStandardPb );
 
       Opportunity lOpp = new Opportunity(CurrencyIsoCode='BRL');
 
       lOpp = DataFactory.newOpportunity( lAcc.Id , lStandardPb , 'Client_Success_Renegotiation' );
       lOpp.Quantity_Offer_Number_of_Employees__c = 500;
       lOpp.FastTrackStage__c = 'Lost';
       lOpp.Sub_Type__c = 'Retention';
       
       Database.insert( lOpp );
 
       Product2 lAcessFee = DataFactory.newProduct( 'Enterprise Subscription' , false , 'BRL' );
       Database.insert( lAcessFee );
 
       PricebookEntry lAccessFeeEntry = DataFactory.newPricebookEntry( lStandardPb , lAcessFee , lOpp );
       Database.insert( lAccessFeeEntry );
 
       Quote lProposal = DataFactory.newQuote( lOpp , 'Quote Test', 'Client_Sales_New_Business' );
       Database.insert( lProposal );
 
       QuoteLineItem lEnterpriseSubscription = DataFactory.newQuoteLineItem( lProposal , lAccessFeeEntry );
       lEnterpriseSubscription.Discount__c = 49.0;
       Database.insert( lEnterpriseSubscription );
 
       AccountOpportunityRepository accountsInOpp = new AccountOpportunityRepository(lOpp.Id);
       accountsInOpp.setRelationshipAccountWithOppForClients(new List<Account>{lAcc});
 
       
      Case ddo = DataFactory.newCase(lOpp.Id, lProposal.id, 'Deal_Desk_Operational');
      ddo.Auto_Approved_Case__c = true;
      ddo.Eligible_File_Update_Frequency__c = 'Once a month';
      ddo.ES_Payment_Due_Days__c = '10 days';
      ddo.Eligible_List_Registration_Method__c =  'GDPR & SSO';
      ddo.Status = 'Confirmed';
      Database.insert( ddo );
 
 
       Test.startTest();
     
       Test.setMock(HttpCalloutMock.class, new Mock());
 
       Integer statusCode;
       try {
           statusCode = PipefyIntegrationController.auraHandler(ddo.Id, 'Case');
       } catch(Exception e) {
           System.debug('Error');
       }
      
       Test.stopTest();

   }
    
     @isTest
   private static void testCreationForCommentnull(){
      
       Account lAcc = DataFactory.newAccount();
       lAcc.NumberOfEmployees = 500;
       Database.insert( lAcc );

       AccountRepository accountRep = new AccountRepository();
       accountRep.byId(lAcc.Id);
 
       Pricebook2 lStandardPb = DataFactory.newPricebook();
       Database.update( lStandardPb );
 
       Opportunity lOpp = new Opportunity(CurrencyIsoCode='BRL');      
       lOpp = DataFactory.newOpportunity( lAcc.Id , lStandardPb , 'Client_Sales_New_Business' );
       lOpp.Quantity_Offer_Number_of_Employees__c = 500;
       lopp.Pipefy_Card_ID__c = '405722846';
       lopp.FastTrackStage__c = 'Launched/Won';
       lopp.StageName = 'Lançado/Ganho';
       lopp.Type = 'Expansion';  
       lopp.Gympass_Plus__c = 'Yes';
       lopp.Country_Manager_Approval__c = true;
       lopp.Payment_approved__c = true;  
       lopp.Standard_Payment__c = 'Yes';
       lopp.Request_for_self_checkin__c = 'Yes';
       lopp.Comments__c = 'Comments';
       Database.insert( lOpp );

       Test.startTest();
     
       Test.setMock(HttpCalloutMock.class, new Mock());
 
       Integer statusCode;
       try {
           statusCode = PipefyIntegrationController.auraHandler(lOpp.Id, 'Opportunity');

       } catch(Exception e) {
           System.debug('Error');
       }
      
       Test.stopTest();
   }
   @Future(callout=true)
   public static void futureHandler(String recordId, String sObjectName){
       PipefyIntegrationController.auraHandler(recordId, sObjectName);
      // PipefyIntegrationController.callServiceFromBatch(recordId);
   }
 
   public class Mock implements HttpCalloutMock {
       protected Integer code = 200;
       protected String status = 'OK';
       protected String body = '{"data":{"createCard":{"card":{"id":"405722846"},"clientMutationId":null}}}';
 
       /**
        * @description Returns an HTTP response for the given request.
        * @param req HTTP request for which response should be returned
        * @return mocked response
        */
       public HTTPResponse respond(HTTPRequest req) {
           HttpResponse resp = new HttpResponse();
           resp.setStatusCode(code);
           resp.setStatus(status);
           resp.setBody(body);
           return resp;
       }
   }
    
  
   public class PipefyIntegrationHttpCalloutMock implements HttpCalloutMock {
       // Implement this interface method
       public HTTPResponse respond(HTTPRequest request) {
           // Create a fake response
           HttpResponse response = new HttpResponse();
           response.setHeader('Content-Type', 'application/json');
           response.setBody('{"data":{"createCard":{"card":{"id":"405722846"},"clientMutationId":null}}}');
           response.setStatusCode(200);
           return response;
       }
   }
}