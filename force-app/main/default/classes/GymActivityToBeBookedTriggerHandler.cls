public class GymActivityToBeBookedTriggerHandler extends triggerhandler {
    
    public override void beforeInsert() {
        System.debug('B4Insert');
        
    }
    
    public override void afterInsert(){
        System.debug('AfterInsert');
        new GymActivityToBeBookedTriggerHelper().afterInsertMethod(); 
        
    }

    
    public override void AfterDelete(){
        System.debug('AfterDelete');
        new GymActivityToBeBookedTriggerHelper().afterdeleteMethod(); 
    }
    
}