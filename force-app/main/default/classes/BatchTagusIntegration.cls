/**
 * @description       : 
 * @author            : AlyssonMota
 * @group             : 
 * @last modified on  : 09-17-2020
 * @last modified by  : Alysson Mota
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   09-14-2020   Alysson Mota   Initial Version
**/
global without sharing class BatchTagusIntegration implements Database.Batchable<sObject>, Database.AllowsCallouts {
    global List<Integration_Request__c> start(Database.BatchableContext bc) {
        List<Integration_Request__c> requests = [
            SELECT Id, Name, Endpoint__c, HTTP_Request_Method__c, Integration_Request_Status__c, Payload__c  
            FROM Integration_Request__c 
            WHERE Integration_Request_Status__c = 'Pending Integration'
        ];
        
        return requests;        
    }
    
    global void execute(Database.BatchableContext BC, List<Integration_Request__c> scope) {
        Map<Id, Utils.CalloutStructResponse> irIdToCalloutStructResponse = new Map<Id, Utils.CalloutStructResponse>();
        List<Utils.CalloutStructResponse> calloutStructResponseList = new List<Utils.CalloutStructResponse>();
        
        for (Integration_Request__c request : scope) {
     		replaceTransactionData(request, BC); 
        }
                
        // Make callouts
        for (Integration_Request__c ir : scope) {
       		Utils.CalloutStructResponse calloutStructResponse;
			calloutStructResponse = Utils.doHTTPCallout(ir);
            calloutStructResponseList.add(calloutStructResponse);
        }
        
        evaluateCalloutResponses(calloutStructResponseList);
    }
    
    global void finish(Database.BatchableContext bc) {
       	Datetime sysTime = System.now().addMinutes(10);
    	String chron_exp = '' + sysTime.second() + ' ' + sysTime.minute() + ' ' + sysTime.hour() + ' ' + sysTime.day() + ' ' + sysTime.month() + ' ? ' + sysTime.year();
       	String strName = 'HourlyBatchTagusIntegration ' + sysTime.day() + '/' + sysTime.month() + '/' + sysTime.year() + ' - '+ sysTime.hour() + ':' + sysTime.minute() + ':' + sysTime.second();
      	
        if (!Test.isRunningTest()) {
       		System.schedule(strName, chron_exp, new ScheduleTagusIntegration());    
        }
    }

	private void evaluateCalloutResponses(List<Utils.CalloutStructResponse> calloutStructResponseList) {
        List<Integration_Request__c> integrationRequestsToUpdate = new List<Integration_Request__c>();
		List<Integration__c> integrationLogsToInsert = new List<Integration__c>();
        
        for (Utils.CalloutStructResponse csr : calloutStructResponseList) {
            if (csr.calloutExecutionStatus == Utils.CalloutExecutionStatus.EXECUTED) {
              	Integration_Request__c ir = getUpdatedIntegrationRequestForCallout(csr);
              	integrationRequestsToUpdate.add(ir);
                    
               	Integration__c integrationLog = createIntegrationLogFromCalloutResponse(csr);
            	integrationLogsToInsert.add(integrationLog);
                
            } else if (csr.calloutExecutionStatus == Utils.CalloutExecutionStatus.EXCEPTION_ERROR) {
				Integration__c integrationLog = createIntegrationLogForNonExecutedCallout(csr);
               	integrationLogsToInsert.add(integrationLog);
            }
        }
        
        updateIntegrationRequests(integrationRequestsToUpdate);
        insertIntegrationLogs(integrationLogsToInsert);
    }
    
    private Integration__c createIntegrationLogFromCalloutResponse(Utils.CalloutStructResponse calloutStructResponse) {
		Id recordTypeId = Schema.SObjectType.Integration__c.getRecordTypeInfosByDeveloperName().get('Standart_Integration').getRecordTypeId();
        
        Integration__c integrationLog = new Integration__c();
        integrationLog.RecordTypeId = recordTypeId;
        integrationLog.Integration_Request__c = calloutStructResponse.integrationRequest.Id;
        integrationLog.Type__c = 'OUT';
        integrationLog.Subtype__c = 'Tagus Integration';
        integrationLog.HTTP_Status_Code__c = calloutStructResponse.httpResponse.getStatusCode();
        integrationLog.HTTP_Status_Message__c = calloutStructResponse.httpResponse.getStatus();
        integrationLog.Response_body__c = calloutStructResponse.httpResponse.getBody();
        
        return integrationLog;
    }
    
    private Integration__c createIntegrationLogForNonExecutedCallout(Utils.CalloutStructResponse calloutStructResponse) {
 		Id recordTypeId = Schema.SObjectType.Integration__c.getRecordTypeInfosByDeveloperName().get('Standart_Integration').getRecordTypeId();
        
        Integration__c integrationLog = new Integration__c();
        integrationLog.RecordTypeId = recordTypeId;
        integrationLog.Integration_Request__c = calloutStructResponse.integrationRequest.Id;
        integrationLog.Type__c = 'OUT';
        integrationLog.Subtype__c = 'Tagus Integration';
		integrationLog.Error_Message__c = calloutStructResponse.exceptionErrorMessage;
        
        return integrationLog;
    }

    private List<Database.SaveResult> updateIntegrationRequests(List<Integration_Request__c> integrationRequests) {
        List<Database.SaveResult> saveResult = null;
        
        try {
        	update integrationRequests;
        } catch(Exception e) {
      		System.debug('Error when trying to update Integration Requests');
        }
        
        return saveResult;
    }
    
    private List<Database.SaveResult> insertIntegrationLogs(List<Integration__c> integrationLogs) {
        List<Database.SaveResult> saveResult = null;
        
        try {
        	saveResult = Database.insert(integrationLogs, false);
        	insert integrationLogs;
        } catch(Exception e) {
      		System.debug('Error when trying to insert Integration Logs');
        }
        
        return saveResult;
    }
    
    private Integration_Request__c getUpdatedIntegrationRequestForCallout(Utils.CalloutStructResponse csr) {
    	Integration_Request__c ir = csr.integrationRequest;
        
        if (csr.httpResponse.getStatusCode() == 200) {
       		ir.Integration_Request_Status__c = 'Sent';
        } else {
            ir.Integration_Request_Status__c = 'error';
            ir.Error_Message__c = 'Error ' + csr.httpResponse.getStatusCode();
            ir.Text_Error_Message__c = csr.httpResponse.getBody();
        }
        
        return ir;
    }
    
    private void replaceTransactionData(Integration_Request__c request, Database.BatchableContext BC) {
        System.debug('configureRequestPayload');
        
        request.Payload__c = request.Payload__c.replace('@transaction_id', BC.getJobId());
        request.Payload__c = request.Payload__c.replace('@transaction_timestamp', String.valueOf(System.now()));
        
        System.debug(JSON.serializePretty(request.Payload__c));
    }
}