/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 09-08-2021
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/
public with sharing class TagusAccountPartnerOutboundPublisher {

    //change to partner
    private final String CREATE_EVENT_NAME = 'CREATE_PARTNER_ON_TAGUS';
    private final String UPDATE_EVENT_NAME = 'UPDATE_PARTNER_ON_TAGUS';
  
    private Map<Id, Account> partnerMap;
  
    public TagusAccountPartnerOutboundPublisher(List<Account> partners) {
      setAccountMap(partners);
    }
  
    private void setAccountMap(List<Account> partners) {
  
      List<Account> partnersToPublish = new List<Account>();
  
      for (Account partner : partners) {
        partnersToPublish.add(partner);
      }            
      this.partnerMap = AccountPartnerTagusDTO.getAccountMapWithFieldsToParse(  partnersToPublish  );
  
    }  

  
    public EventQueue runCreate() {
      return sendEvent(CREATE_EVENT_NAME);
    }
  
    public EventQueue runUpdate() {
      return sendEvent(UPDATE_EVENT_NAME);
    }
  
    private EventQueue sendEvent(String eventName) {
        EventQueue event = new EventBuilder()
        .createEventFor(eventName)
        .withSender('SALESFORCE')
        .withReceiver('TAGUS')
        .buildEvent();
       
        event.addPayload(eventName, JSON.serialize(partnerMap));
        
        event.save();
        return event;
    }
}