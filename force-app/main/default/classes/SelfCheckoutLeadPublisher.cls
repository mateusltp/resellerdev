public without sharing class SelfCheckoutLeadPublisher {
    private final String EVENT_NAME = 'SELF_CHECKOUT_LEAD_INBOUND';
    
    public EventQueue publish(SelfCheckoutLeadRequest selfCheckoutleadRequest) {
        EventQueue event = new EventBuilder()
            .createEventFor(EVENT_NAME)
            .withStatus(EventQueueStatusType.QUEUED.name())
            .withSender('JAMOR')
            .withReceiver('SALESFORCE')
            .withBusinessDocumentNumber(selfCheckoutleadRequest.getLeadDTO().parseToSLead().FirstName + ' ' 
                                        + selfCheckoutleadRequest.getLeadDTO().parseToSLead().LastName)
            .withBusinessDocumentCorrelatedNumber(selfCheckoutleadRequest.getLeadDTO().parseToSLead().Funnel_Stage__c)
            .buildEvent();

            event.addPayload(EVENT_NAME, JSON.serialize(selfCheckoutleadRequest));
            
        event.save();
        return event;
    }
}