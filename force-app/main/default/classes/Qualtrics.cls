public class Qualtrics {
    
    public static String authQualtrics(String mailingListName){
        
        String listName = mailingListName.replace(' ','_');
        Qualtrics__mdt mdtQualtrics = Qualtrics__mdt.getInstance(listName);
        
        string ClientId = mdtQualtrics.ClientId__c;
        string ClientSecret = mdtQualtrics.ClientSecret__c;
        string endpoint = mdtQualtrics.EndPoint_OAuth__c;
        string body = 'grant_type='+mdtQualtrics.GrantType__c+'&client_id='+ClientId+'&client_secret='+ClientSecret+'&scope='+mdtQualtrics.Scope__c;
        
        // variables to store token response if we are successful
        string AccessToken = '';
        string AccessScope = '';
        
        Blob headerValue = Blob.valueOf(ClientId + ':' + ClientSecret);
        String authorizationHeader = 'Basic ' +  EncodingUtil.base64Encode(headerValue);
        system.debug('Encoder Value' + EncodingUtil.base64Encode(headerValue));
        
        // Construct the request
        http http = new http();
        httpRequest request = new httpRequest();
        request.setMethod('POST');
        request.setEndpoint(endpoint);
        request.setHeader('Accept', 'application/json');
        request.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        request.setHeader('Authorization', authorizationHeader);
        request.setBody(body);
            
        // Send the request
        httpResponse Res = new httpResponse();
        if(!Test.isRunningTest()){
            Res = http.send(request);
        }else {
            Res.setStatusCode(200);
            Res.setBody('{"access_token":"33fdb05c-c444-4faf-a792-7f40df7b72cd","token_type":"Bearer","expires_in":3599,"scope":"manage:all"}');
        }
        
        system.debug('response status code ' + Res.getStatusCode() );
        if (Res.getStatusCode() == 200) {
            Map<String, Object> ResponseMap = (Map<String, Object>) JSON.deserializeUntyped(Res.getBody());
            system.debug('response OK ' + Res.getBody() );
            
            AccessToken = (string)ResponseMap.get('access_token');
            system.debug('access token: ' + AccessToken );
            
            AccessScope = (string)ResponseMap.get('scope');
            system.debug('access Scope: ' + AccessScope );
            
        } else {
            system.debug('response error ' + Res.getBody() );
        }
        
        return AccessToken;
    }
    
    public static void createMailingList( String mailingListName,List<Contact> lstCon, Map<String, Account> mapAccCon ) {
        
        String token;
        token = authQualtrics(mailingListName);
        
        String listName = mailingListName.replace(' ','_');
        Qualtrics__mdt mdtQualtrics = Qualtrics__mdt.getInstance(listName);
        
        Http http = new Http();
        HttpRequest httpReq = new  HttpRequest();
        
        httpReq.setEndpoint( mdtQualtrics.EndPointCreateList__c );
        httpReq.setMethod( 'POST' );
        httpReq.setHeader( 'Content-Type', 'application/json' );
        httpReq.setHeader( 'Accept', 'application/json' );
        httpReq.setHeader( 'Authorization', 'Bearer ' + token );
        
        Map<String, Object> obj = new Map<String, Object>();
        obj.put('name', string.valueOf(system.Today()) + ' - ' + mailingListName);
        httpReq.setBody( JSON.Serialize(obj) );
        
        HttpResponse httpRes = new HttpResponse();
        if(!Test.isRunningTest()){
            httpRes = http.send( httpReq );
        }else {
            httpRes.setStatusCode(200);
            httpRes.setBody('{"result":{"id":"CG_2SjHTp4ctJmhXH6"},"meta":{"httpStatus":"200 - OK","requestId":"745d5c12-482b-44c0-a226-8fc23e446b67"}}');
        }
        if (httpRes.getStatusCode() == 200) {
            QualtricsMailingListDTO mailingList = QualtricsMailingListDTO.parse(httpRes.getBody());
            system.debug('response OK ' + httpRes.getBody() );
            contactsImport(mailingListName, mailingList.result.id, token, lstCon, mapAccCon);
        }  else {
            system.debug('response error ' + httpRes.getBody() );
        }
    }
    
    public static void contactsImport(String mailingListName, String mailingListId, String token, List<Contact> lstCon, Map<String, Account> mapAccCon ) {
        
        Http http = new Http();
        HttpRequest httpReq = new  HttpRequest();
        
        String listName = mailingListName.replace(' ','_');
        Qualtrics__mdt mdtQualtrics = Qualtrics__mdt.getInstance(listName);
        String endpoint = mdtQualtrics.EndPointContactsImport__c.replace('varMailingListId',mailingListId);
        httpReq.setEndpoint( endpoint );
        httpReq.setMethod( 'POST' );
        httpReq.setHeader( 'Content-Type', 'application/json' );
        httpReq.setHeader( 'Accept', 'application/json' );
        httpReq.setHeader( 'Authorization', 'Bearer ' + token );
        
        List<QualtricsContactDTO.contacts> lstqc = new List<QualtricsContactDTO.contacts>();
        for(Contact con: lstCon){
            Account acc = new Account();
            acc = mapAccCon.get(con.AccountId);
            
            QualtricsContactDTO.Contacts objContact = new QualtricsContactDTO.Contacts();
            QualtricsContactDTO.EmbeddedData objEmbeddedData = new QualtricsContactDTO.EmbeddedData(); 
            objContact.firstName = con.FirstName;
            objContact.lastName = con.LastName == null ? '': con.LastName;
            objContact.email = con.Email;
            objContact.extRef = con.Id;
            objContact.Language = con.Language_Code__c == null ? 'EN' : con.Language_Code__c.toUppercase();
            objContact.unsubscribed = con.HasOptedOutOfEmail;
            objEmbeddedData.LaunchDate = String.valueOfGmt(acc.Launch_date__c) == null ? '' : String.valueOfGmt(acc.Launch_date__c);
            objEmbeddedData.ContractSignatureDate = String.valueOfGmt(acc.Contract_Signed_At__c) == null ? '' : String.valueOfGmt(acc.Contract_Signed_At__c);
            objEmbeddedData.ContractEndDate = String.valueOfGmt(acc.Proposal_End_Date__c) == null ? '' : String.valueOfGmt(acc.Proposal_End_Date__c);
            objEmbeddedData.MobilePhone = String.valueOf(con.MobilePhone) == null ? '' : String.valueOf(con.MobilePhone);
            objEmbeddedData.Role = con.Role__c == null ? '' : con.Role__c;
            objEmbeddedData.AccountId = acc.Id;
            objEmbeddedData.AccountRecordType = acc.RecordType.DeveloperName == null ? '' : acc.RecordType.DeveloperName;
            objEmbeddedData.BillingCountry = acc.BillingCountry == null ? '' : acc.BillingCountry;
            objEmbeddedData.TotalNofEmployees = String.valueOf(acc.NumberOfEmployees);
            objEmbeddedData.GympassCRMID = acc.ID_Gympass_CRM__c == null ? '' : acc.ID_Gympass_CRM__c;
            objEmbeddedData.Size = acc.Size__c == null ? '' : acc.Size__c;
            objEmbeddedData.AccountOwnerFullName = acc.Owner.Name;
            objEmbeddedData.Tagus = acc.Tagus_Based__c == null ? '' : acc.Tagus_Based__c;
            objEmbeddedData.BusinessUnit = acc.Business_Unit__c == null ? '' : acc.Business_Unit__c;
            objEmbeddedData.NumberofDependents = String.valueOf(acc.GP_NumberOfDependents__c) == null ? '' : String.valueOf(acc.GP_NumberOfDependents__c);
            objEmbeddedData.PaymentMethod = acc.Payment_method__c == null ? '' : acc.Payment_method__c;
            objEmbeddedData.AccountName = acc.Name;
            objContact.embeddedData = objEmbeddedData;
            lstqc.add(objContact);
        }
        String body = '{"contacts": '+JSON.serialize(lstqc)+'}';
        String bodyFormat = body.replace('GympassCRMID','Gympass CRM ID').replace('PaymentMethod', 'Payment method').replace('LaunchDate','Launch Date').replace('ContractSignatureDate','Contract Signature Date').replace('ContractEndDate','Contract End Date').replace('MobilePhone','Mobile Phone').replace('AccountRecordType','Account Record Type').replace('BillingCountry','Billing Country').replace('TotalNofEmployees','Total Nº of Employees').replace('AccountOwnerFullName','Account Owner: Full Name').replace('Tagus','Tagus?').replace('BusinessUnit','Business Unit').replace('NumberofDependents','Number of Dependents').replace('AccountName','Account Name');
        
        httpReq.setBody( bodyFormat );
        
        HttpResponse httpRes = new HttpResponse();
        if(!Test.isRunningTest()){
            httpRes = http.send( httpReq );
        }else {
            httpRes.setStatusCode(202);
            httpRes.setBody('{"result":{"id":"PGRS_0oHkAu4Y1QA3pk2","contacts":{"unprocessed":[]},"tracking":{"url":"https://iad1.qualtrics.com/API/v3/directories/POOL_CkaKl9k11swcANP/mailinglists/CG_2SjHTp4ctJmhXH6/transactioncontacts/PGRS_0oHkAu4Y1QA3pk2"},"status":"in progress"},"meta":{"httpStatus":"202 - Accepted","requestId":"c80b528f-37f6-42a5-976c-cb89f9191aef"}}');
        }
        if (httpRes.getStatusCode() == 202) {
            QualtricsMailingListDTO mailingList = QualtricsMailingListDTO.parse(httpRes.getBody());
            system.debug('response OK ' + httpRes.getBody() );
            createDistribution(mailingListName, mailingListId, token);
        }  else {
            system.debug('response error ' + httpRes.getBody() );
        }
    }
    
    public static void createDistribution(String mailingListName, String mailingListId, String token) {
        
        Http http = new Http();
        HttpRequest httpReq = new  HttpRequest();
        
        String listName = mailingListName.replace(' ','_');
        Qualtrics__mdt mdtQualtrics = Qualtrics__mdt.getInstance(listName);
        
        httpReq.setEndpoint( mdtQualtrics.EndPointDistribution__c );
        httpReq.setMethod( 'POST' );
        httpReq.setHeader( 'Content-Type', 'application/json' );
        httpReq.setHeader( 'Accept', 'application/json' );
        httpReq.setHeader( 'Authorization', 'Bearer ' + token );
        
        String sendDate = string.valueOfGMT(system.Now().addHours(1)).replace(' ','T');
        String expirationDate = string.valueOfGMT(system.Now().addDays(4)).replace(' ','T');
        String body = '{\n' +
            '  "message" : {\n' +
            '  		"libraryId" : "'+mdtQualtrics.libraryId__c+'",\n' +
            '  		"messageId" : "'+mdtQualtrics.messageId__c+'"\n' +
            '   },\n' +
            '  "recipients" : {\n' +
            '  		"mailingListId" : "'+mailingListId+'"\n' +
            '   },\n' +
            '  "header" : {\n' +
            '  		"fromName" : "'+mdtQualtrics.fromName__c+'",\n' +
            '  		"fromEmail" : "'+mdtQualtrics.fromEmail__c+'",\n' +
            '  		"subject" : "'+mdtQualtrics.subject__c+'"\n' +
            '   },\n' +
            '  "surveyLink" : {\n' +
            '  		"surveyId" : "'+mdtQualtrics.surveyId__c+'",\n' +
            '  		"expirationDate" : "'+expirationDate+'Z",\n' +
            '  		"type" : "Individual"\n' +
            '   },\n' +
            '  "sendDate" : "'+sendDate+'Z"\n' +
            '}';
        
        httpReq.setBody( body );
        
        HttpResponse httpRes = new HttpResponse();
        if(!Test.isRunningTest()){
            httpRes = http.send( httpReq );
        }else {
            httpRes.setStatusCode(200);
            httpRes.setBody('{"message": {"libraryId": "UR_1M4aHozEkSxUfCl","messageId": "MS_0Vdgn7nLGSQBlYN","messageText": "Example Message Text"},"recipients": {"mailingListId": "CG_012345678901234","contactId": "CGC_012345678901234","transactionBatchId": "BT_ZZZZ45678901234"},"header": {"fromEmail": "apiexample@qualtrics.com","replyToEmail": "apiexample@qualtrics.com","fromName": "Test Name","subject": "Example Subject"},"surveyLink": {"surveyId": "SV_cHbKMOdeT8NetF3","expirationDate": "2019-08-24T14:15:22Z","type": "Individual"},"sendDate": "2019-08-24T14:15:22Z"}');
        }
        if (httpRes.getStatusCode() == 200) {
            QualtricsMailingListDTO mailingList = QualtricsMailingListDTO.parse(httpRes.getBody());
            system.debug('response OK ' + httpRes.getBody() );
        }  else {
            system.debug('response error ' + httpRes.getBody() );
        }
    }
    
}