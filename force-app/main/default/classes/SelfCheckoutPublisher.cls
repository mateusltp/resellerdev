/**
 * @author vncferraz
 */
public without sharing class SelfCheckoutPublisher {
    public final String SELF_CHECKOUT_EVENT_NAME = 'SELF_CHECKOUT_INBOUND';
   
  
    public EventQueue publish(SelfCheckoutRequest selfCheckoutRequest) {
      EventQueue event = new EventBuilder()
        .createEventFor(SELF_CHECKOUT_EVENT_NAME)
        .withStatus(EventQueueStatusType.SCHEDULED.name())
        .withSender('JAMOR')
        .withReceiver('SALESFORCE')
        .withBusinessDocumentNumber(selfCheckoutRequest.getAccountDTO().parseToSAccount().Name)
        .withBusinessDocumentCorrelatedNumber(selfCheckoutRequest.getOpportunityDTO().parseToSOpportunity().StageName)
        .buildEvent();
  
      event.addPayload(SELF_CHECKOUT_EVENT_NAME, JSON.serialize(selfCheckoutRequest));
  
      event.save();
  
      return event;
    }
  }