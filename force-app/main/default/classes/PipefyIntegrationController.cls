/**
 * @description       : 
 * @author            : VRDE@GFT.com
 * @group             : 
 * @last modified on  : 08-05-2021
 * @last modified by  : Alysson Mota
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   12-02-2021   VRDE@GFT.com   Initial Version
**/
global without sharing class PipefyIntegrationController {
    
    @AuraEnabled
    global static Integer auraHandler(String recordId, String sObjectName){
        PipefyIntegrationService.ResponseData respData = callService(recordId, sObjectName);
        updateOpportunity(respData);
        return respData.httpResponse.getStatusCode();
    }

    private static void updateOpportunity(PipefyIntegrationService.ResponseData respData) {
        if (respData.pipefyCardId != null) {
            try {
                respData.records.opp.Pipefy_Card_ID__c = respData.pipefyCardId;
                respData.records.opp.Pipefy_Card_URL__c = respData.pipefyCardUrl;

                update respData.records.opp;
            } catch(Exception e) {
                throw new AuraHandledException('Something went wrong: ' + e.getMessage());
            }
        }
    }

    public static PipefyIntegrationService.ResponseData callServiceFromBatch(String recordId) {
        return callService(recordId, 'Opportunity');
    }

    private static PipefyIntegrationService.ResponseData callService(String recordId, String sObjectName) {
        //Initial local variables
        PipefyIntegrationService.IntegrationRecordsForRelatedObjects records = new PipefyIntegrationService.IntegrationRecordsForRelatedObjects();
        PipefyIntegrationService.ResponseData respData;
        
        //Check parent object
        if(sObjectName == 'Opportunity'){
            records.opp = new OpportunityRepository().byId(recordId);
        }else if(sObjectName == 'Case'){
            records.cs = new CaseRepository().byId(recordId);
            records.opp = new OpportunityRepository().byId(records.cs.OpportunityId__c);
        }
       
        if((records.opp.RecordType.DeveloperName == 'Client_Sales_New_Business' || records.opp.RecordType.DeveloperName == 'Client_Success_Renegotiation') && (records.opp.Comments__c == null || records.opp.Comments__c == '' ) && sObjectName == 'Opportunity'){
            throw new AuraHandledException('The field \'Negotiation Details Summary\' cannot be empty');
        }

        if((records.opp.RecordType.DeveloperName == 'SMB_New_Business' || records.opp.RecordType.DeveloperName == 'SMB_Success_Renegotiation') 
            && (records.opp.Comments__c == null || records.opp.Comments__c == '' )
            && records.opp.Self_Checkout_New_Business__c == false
            && sObjectName == 'Opportunity'){
            throw new AuraHandledException('The field \'Negotiation Details Summary\' cannot be empty');
        }

        if(records.opp.Pipefy_Card_ID__c !=null && records.opp.Pipefy_Card_ID__c != '' ){
            throw new AuraHandledException('This was already sent to Pipefy.');
        }
        
        //Get the related Account
        records.acc = new AccountRepository().byId(records.opp.AccountId);
        
        //Get the related Proposal
        records.qt = new QuoteRepository().lastForOpportunity( records.opp.Id );
        
        if( records.qt != null ){
            List<Payment__c> allPayments = new List<Payment__c>();
            
            //Get Access Fee and related Payments
            List<QuoteLineItem> acFeeList = new QuoteLineItemRepository().accessFeesForQuote(records.qt.Id); 
            if(!acFeeList.isEmpty()){
                records.accessFee = acFeeList[0];
                records.paymentsForAccessFee = PipefyIntegrationService.getPaymentsForQuoteLineItem(records.accessFee.Id);
                if(records.paymentsForAccessFee != null && !records.paymentsForAccessFee.isEmpty()){
                    allPayments.addAll(records.paymentsForAccessFee);
                }
            }
            
            //Get Setup Fee and related Payments
            List<QuoteLineItem> stFeeList = new QuoteLineItemRepository().setupFeesForQuote(records.qt.Id);
            if(!stFeeList.isEmpty()){
                records.setupFee = stFeeList[0];
                records.paymentsForSetupFee = PipefyIntegrationService.getPaymentsForQuoteLineItem(records.setupFee.Id);
                if(records.paymentsForSetupFee != null && !records.paymentsForSetupFee.isEmpty()){
                    allPayments.addAll(records.paymentsForSetupFee);
                }
            }
            
            //Get Pro Services One Fee and related payments
            List<QuoteLineItem> proServicesOneFeeList = PipefyIntegrationService.getFeeByQuoteIdAndProductName(records.qt.Id, 'Professional Services Setup Fee');
            if(!proServicesOneFeeList.isEmpty()){
                records.proServicesOneFee = proServicesOneFeeList[0];
                records.paymentsForProServicesOneFee = PipefyIntegrationService.getPaymentsForQuoteLineItem(records.proServicesOneFee.Id);
            }
            
            //Get Pro Services Maintenance Fee and related payments
            List<QuoteLineItem> proServicesMaintenanceFeeList = PipefyIntegrationService.getFeeByQuoteIdAndProductName(records.qt.Id, 'Professional Services Maintenance Fee');
            if(!proServicesMaintenanceFeeList.isEmpty()){
                records.proServicesMaintenanceFee = proServicesMaintenanceFeeList[0];
                records.paymentsForProServicesOneFee = PipefyIntegrationService.getPaymentsForQuoteLineItem(records.proServicesMaintenanceFee.Id);
            }
            
            if(!allPayments.isEmpty() ){
                
                //Get all Waivers
                records.waiversForPayments = new WaiverRepository().getWaiversByPayments(allPayments);
                
                //Get all Eligibilities
                records.eligibilitiesForPayments = new EligibilityRepository().getEligibilitiesByPayments(allPayments);
            }
        }
        
        //Get Split Billing Relationships
        String recordTypeDevName = records.opp.RecordType.DeveloperName;
        if(recordTypeDevName.contains('Indirect'))
            records.accountOpportunityRelationship = PipefyIntegrationService.getAccountOpportunityRelationshipsReseller(records.opp.Id);
        else
        	records.accountOpportunityRelationship = PipefyIntegrationService.getAccountOpportunityRelationships(records.opp.Id);
      
        //Set Demand type
        if(records.opp.RecordType.DeveloperName == 'Client_Sales_New_Business' && (records.opp.FastTrackStage__c == 'Setup' || records.opp.FastTrackStage__c == 'Launched/Won') ){
            records.demand = 'Creation';
            
            //Get last closed M0 for the opp
            records.m0 = PipefyIntegrationService.getLastClosedM0ForOpp(records.opp.Id);
            
            //Get last closed Deal Desk Operational Case for opp or get Eligible_List_Registration_Method__c by Opp from Offer QUeue upload
            //try{
            //    records.dealDeskOperationalCase = PipefyIntegrationService.getDealDeskOperationalCase(records.opp.Id);
            //}catch(System.QueryException e){
            records.dealDeskOperationalCase = new Case(
                Eligible_List_Registration_Method__c=records.opp.Eligible_List_Registration_Method__c,
                MF_Eligibility_ES_Payment_Due_Days__c= records.eligibilitiesForPayments.get(records.paymentsForAccessFee[0].Id)[0].Payment_Due_Days__c,
                ES_Billing_Day__c=records.paymentsForAccessFee[0].Billing_Day__c,
                ES_Payment_Due_Days__c=records.paymentsForAccessFee[0].Payment_Due_Days__c,
                MF_Eligibility_ES_Billing_Day__c=records.eligibilitiesForPayments.get(records.paymentsForAccessFee[0].Id)[0].Billing_Day__c
            );
            //}
        }
        else if(records.opp.RecordType.DeveloperName == 'SMB_New_Business' && (records.opp.FastTrackStage__c == 'Setup' || records.opp.FastTrackStage__c == 'Launched/Won') ){
            records.demand = 'Creation';
            
            //Get last closed Deal Desk Operational Case for opp or get Eligible_List_Registration_Method__c by Opp from Offer QUeue upload
            try{
                records.dealDeskOperationalCase = PipefyIntegrationService.getDealDeskOperationalCase(records.opp.Id);
            }catch(System.QueryException e){
                records.dealDeskOperationalCase = new Case(Eligible_List_Registration_Method__c=records.opp.Eligible_List_Registration_Method__c);
            }
        }
        else if(records.opp.RecordType.DeveloperName == 'Indirect_Channel_New_Business' && (records.opp.FastTrackStage__c == 'Setup' || records.opp.FastTrackStage__c == 'Launched/Won') ){
            records.demand = 'Creation';
            //Get last closed Deal Desk Operational Case for opp or get Eligible_List_Registration_Method__c by Opp from Offer QUeue upload
            try{
                records.dealDeskOperationalCase = PipefyIntegrationService.getDealDeskOperationalCase(records.opp.Id);
            }catch(System.QueryException e){
                records.dealDeskOperationalCase = new Case(Eligible_List_Registration_Method__c=records.opp.Eligible_List_Registration_Method__c);
            }
            
        }
        else if((records.opp.RecordType.DeveloperName == 'Client_Success_Renegotiation' || records.opp.RecordType.DeveloperName == 'SMB_Success_Renegotiation') 
                    && (records.opp.FastTrackStage__c == 'Setup' || records.opp.FastTrackStage__c == 'Launched/Won') ){
            records.demand = 'Renewal';
            
            //Get last closed Deal Desk Operational Case for opp or get Eligible_List_Registration_Method__c by Opp from Offer QUeue upload
            try{
                records.dealDeskOperationalCase = PipefyIntegrationService.getDealDeskOperationalCase(records.opp.Id);
            }catch(System.QueryException e){
                records.dealDeskOperationalCase = new Case(Eligible_List_Registration_Method__c=records.opp.Eligible_List_Registration_Method__c);
            }

        }else if((records.opp.RecordType.DeveloperName == 'Client_Success_Renegotiation' || records.opp.RecordType.DeveloperName == 'SMB_Success_Renegotiation') 
                    && records.opp.FastTrackStage__c == 'Lost' && records.opp.Sub_Type__c == 'Retention'  && records.cs.Status == 'Confirmed' && sObjectName == 'Case'){
            records.demand = 'Cancellation';
            
        }else{
            throw new AuraHandledException('Invalid Conditions for sending to Pipefy.');
        }
        
        //Call the callouthandler
        respData = PipefyIntegrationService.makePostCallout(records);
        
        return respData;
    }
}