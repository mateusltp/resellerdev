/**
* @description       : 
* @author            : vinicius.ferraz
* @group             : 
* @last modified on  : 06-28-2022
* @last modified by  : Mateus Augusto - GFT (moes@gft.com)
* Modifications Log 
* Ver   Date         Author         Modification
* 1.0   -          vinicius.ferraz   Initial Version 
**/
public with sharing virtual class PricebookRepository {
    public static List<String> INDIRECT_CHANNEL_BUSINESS_MODEL = new List<String>{'Subsidy', 'Subsidy 5k', 'Subsidy 15k', 'Intermediation', 'Exclusive'};

    public PricebookRepository() {}

    public static PricebookRepository getInstanceByOppId( String aOppId ){
        /* Opportunity flow as written in class name */
        Map< String , String > gMapRTDevNameOppFlow = new Map< String , String >{
            'Client_Sales_New_Business' => 'Clients',
            'Client_Success_Renegotiation' => 'Clients',
            'SMB_New_Business' => 'Clients',
            'SMB_Success_Renegotiation' => 'Clients',
            'Indirect_Channel_New_Business' => ''
        };
        String lClassToInstanciate;
        try{
            Opportunity lOpp = [ SELECT RecordType.DeveloperName FROM Opportunity WHERE Id = :aOppId ];
            lClassToInstanciate = gMapRTDevNameOppFlow.get( lOpp.RecordType.DeveloperName ) + 'PricebookRepository';
            PricebookRepository lPricebookRepository = (PricebookRepository) Type.forName( lClassToInstanciate ).newInstance();
            return lPricebookRepository;
        } catch( Exception aException ){
            return new PricebookRepository();
        }
    }
    /**
     * throwable System.QueryException
     */
    public virtual Pricebook2 byId(String recordId){
        return [SELECT Id, Business_Model__c, IsStandard, Name FROM Pricebook2 WHERE Id = :recordId];
    }

    public Pricebook2 forCountry(String country){
        return [SELECT Id,Name, Business_Model__c FROM Pricebook2 WHERE IsActive = true AND (Country__c =:country OR IsStandard = true) order by Country__c NULLS LAST  limit 1];
    }

    public Pricebook2 standard(){
        return [SELECT Id,Name, Business_Model__c FROM Pricebook2 WHERE IsActive = true AND IsStandard = true limit 1];
    }

    public virtual Pricebook2 getPricebook(){
        return standard();
    }
    
    public Pricebook2 standard(String pricebookName){
        return [SELECT Id,Name, Business_Model__c FROM Pricebook2 WHERE IsActive = true AND name =:pricebookName];
    }
    
    public virtual Pricebook2 getPricebook(String pricebookName){
        return standard(pricebookName);
    }

    public List<Pricebook2> forNames(Set<String> pricebookNameSet) {
        return [
            SELECT Id, Name, Country__c, Business_Model__c 
            FROM Pricebook2 
            WHERE IsActive = true AND Name IN :pricebookNameSet
        ];
    }

    public virtual PricebookEntry accessFee(Pricebook2 pricebook, Decimal eligiblesQuantity, boolean familyMemberIncluded, boolean hasCopay, String currencyIsoCode ){
        String pricebookId = pricebook.Id;
        
        String query = 'SELECT Id,Name,Product2Id,UnitPrice,Product2.Family_Member_Included__c, Product2.Copay2_Enrollment_Rate__c,Pricebook2.name, Has_Minimum_Quantity__c,Pricebook2Id,Product2.Maximum_Number_of_Employees__c FROM PricebookEntry '+
            'WHERE IsActive = true AND Pricebook2Id=:pricebookId AND Product2.Family = \'Enterprise Subscription\' ';
        /*
        if( eligiblesQuantity != null && eligiblesQuantity > 0 ){   
            if ( eligiblesQuantity < 500 ){ eligiblesQuantity = 500; }
  
            query += 'AND (Product2.Minimum_Number_of_Employees__c <=:eligiblesQuantity ' +
                'AND Product2.Maximum_Number_of_Employees__c >= :eligiblesQuantity) ';
        }
       
        if(pricebook.Business_Model__c != 'Subsidy' || pricebook.Business_Model__c != 'Subsidy 5k' ||
           pricebook.Business_Model__c != 'Subsidy 15k' ||pricebook.Business_Model__c != 'Intermediation' ||
           pricebook.Business_Model__c != 'Exclusive'){
               
               query += ' AND Product2.Copay2__c = ' + hasCopay;
           }
        
        if ( !hasCopay ) {
            query += ' AND Product2.Family_Member_Included__c = ' + String.valueOf(familyMemberIncluded);
        }
         
        query += ' AND CurrencyIsoCode = \''+currencyIsoCode+'\''; 
        query += ' LIMIT 1';
        System.debug('QUERY : ' + query);
        */
        if( eligiblesQuantity != null && eligiblesQuantity > 0 ){
            if(pricebook.Business_Model__c != 'Subsidy' && pricebook.Business_Model__c != 'Subsidy 5k' &&
            pricebook.Business_Model__c != 'Subsidy 15k' && pricebook.Business_Model__c != 'Intermediation' &&
            pricebook.Business_Model__c != 'Exclusive'){
            
                query += ' AND Product2.Copay2__c = ' + hasCopay;

        }else{
            if ( eligiblesQuantity < 500 ){ eligiblesQuantity = 500; }
        }
                
            query += ' AND (Product2.Minimum_Number_of_Employees__c <=:eligiblesQuantity ' +
                ' AND Product2.Maximum_Number_of_Employees__c >= :eligiblesQuantity) ';
        }
        if ( !hasCopay ) {
            query += ' AND Product2.Family_Member_Included__c = ' + String.valueOf(familyMemberIncluded);
        }
        query += ' AND CurrencyIsoCode = \''+currencyIsoCode+'\''; 
        query += ' LIMIT 1';
        return Database.query(query);
    }

    public virtual PricebookEntry setupFee(Pricebook2 pricebook, Decimal eligiblesQuantity, boolean familyMemberIncluded, String currencyIsoCode){
        String pricebookId = pricebook.Id;
        String query = 'SELECT Id,Name,Product2Id,UnitPrice,Has_Minimum_Quantity__c FROM PricebookEntry '+
            'WHERE IsActive = true AND Pricebook2Id=:pricebookId AND Product2.Family = \'Setup Fee\' ';
        query += ' AND CurrencyIsoCode = \''+currencyIsoCode+'\'';
        query += ' LIMIT 1';
        
        return Database.query(query);
    }
    
    public virtual PricebookEntry proServicesOneFee(Pricebook2 pricebook, Decimal eligiblesQuantity, boolean familyMemberIncluded, String currencyIsoCode){
        String pricebookId = pricebook.Id;
        
        String query = 'SELECT Id,Name,Product2Id,UnitPrice,Has_Minimum_Quantity__c FROM PricebookEntry '+
            'WHERE IsActive = true AND Pricebook2Id=:pricebookId AND Product2.Name = \'Professional Services Setup Fee\' ';
        
        query += ' AND CurrencyIsoCode = \''+currencyIsoCode+'\'';
        
        query += ' LIMIT 1';
        
        return Database.query(query);
    }
    
    public virtual PricebookEntry proServicesMainFee(Pricebook2 pricebook, Decimal eligiblesQuantity, boolean familyMemberIncluded, String currencyIsoCode){
        String pricebookId = pricebook.Id;
        
        String query = 'SELECT Id,Name,Product2Id,UnitPrice,Has_Minimum_Quantity__c FROM PricebookEntry '+
            'WHERE IsActive = true AND Pricebook2Id=:pricebookId AND Product2.Name = \'Professional Services Maintenance Fee\' ';    
        
        query += ' AND CurrencyIsoCode = \''+currencyIsoCode+'\'';
        
        query += ' LIMIT 1';
        
        return Database.query(query);
    }
    
     //select to return list with setupFee, proServicesOneFee e proServicesMainFee (PricebookEntry)
    public virtual List<PricebookEntry> AllPriceBookByPricebook2Id(Pricebook2 pricebook, Decimal eligiblesQuantity, boolean familyMemberIncluded, boolean hasCopay, String currencyIsoCode){
        String pricebookId = pricebook.Id;
        String query = 'SELECT Id,Name,Product2Id,UnitPrice, Has_Minimum_Quantity__c FROM PricebookEntry '+
            'WHERE IsActive = true AND Pricebook2Id=:pricebookId AND (Product2.Family = \'Setup Fee\' or Product2.Name = \'Professional Services Setup Fee\' or Product2.Name = \'Professional Services Maintenance Fee\') ';
        query += ' AND CurrencyIsoCode = \''+currencyIsoCode+'\'';
        System.debug('QUERY setupFee: ' + query);
        return Database.query(query);
    }
    
    public virtual Map<String, Pricebook2> getIndirectChannelPricebookMap(){
        Map<String, Pricebook2> pricebookMap = new Map<String, Pricebook2>();
        for(Pricebook2 pb : [ SELECT Id, Business_Model__c FROM Pricebook2 WHERE IsActive = true AND Business_Model__c IN: INDIRECT_CHANNEL_BUSINESS_MODEL])
            pricebookMap.put(pb.Business_Model__c, pb);
        
        return pricebookMap; 
    }
    
    public static List<PricebookEntry> retrievePricebookEntries(Set<Id> setPricebookEntries){
        return [
            SELECT Id, Has_Minimum_Quantity__c, Product2.Maximum_Number_of_Employees__c, UnitPrice
            FROM PricebookEntry
            WHERE Id IN :setPricebookEntries
        ];
    }

    public static List<PricebookEntry> getIndirectPricebookEntries(List<Product2> product_list, List<String> pricebookId_list, String currencyIsoCode){
        return [    SELECT Pricebook2Id, Product2Id, UnitPrice, Price5Comission__c, Price10Comission__c, Price20Comission__c,
                    Product2.Minimum_Number_of_Employees__c, Product2.Maximum_Number_of_Employees__c 
                    FROM PricebookEntry WHERE Pricebook2Id IN: pricebookId_list AND Product2Id IN : product_list AND CurrencyIsoCode =: currencyIsoCode ];
    }
}