/**
 * @description       : Originally created for IT-2520 to update the Requires CMS Approval New checkbox on opportunity records
 * @author            : bruno.mendes@gympass.com
 * @tests             : OpportunityStageValidator_Test
 * @last modified on  : 25/05/2022
 * @last modified by  : bruno.mendes@gympass.com
**/

public with sharing class PartnerEventTriggerHelper {
    /* Constants */
    private static final PS_Constants constants = PS_Constants.getInstance();
    private static final String className = PartnerEventTriggerHelper.class.getName();

    /* Unit of Work */
    private static fflib_ISObjectUnitOfWork unitOfWork;

    /* Collections */
    private static Set<Id> opportunityIdsRequiringCMSApprovalTrue;

    /* Selectors */
    private static OpportunitySelector opportunitySelector;

    public static void handleEvents(List<Partner_Event__e> partnerEvents) {
        initializeVariables();
        for (Partner_Event__e partnerEvent : partnerEvents) {
            switch on partnerEvent.Type__c {
                when 'UPDATE_REQUIRES_CMS_APPROVAL_TRUE' {
                    groupOpportunitityIdsForCMSApproval(partnerEvent);
                }
            }
        }
        if (!opportunityIdsRequiringCMSApprovalTrue.isEmpty()) {
            updateOpportunityRequiresCMSApprovalTrue(opportunityIdsRequiringCMSApprovalTrue);
        }
    }

    private static void initializeVariables() {
        /* Unit of Work*/
        unitOfWork = Application.UnitOfWork.newInstance();

        /* Collections */
        opportunityIdsRequiringCMSApprovalTrue = new Set<Id>();

        /* Selectors */
        opportunitySelector = (OpportunitySelector)Application.Selector.newInstance(Opportunity.SObjectType);
    }

    private static void groupOpportunitityIdsForCMSApproval(Partner_Event__e partnerEvent) {
        opportunityIdsRequiringCMSApprovalTrue.add(partnerEvent.Object_Id__c);
    }

    private static void updateOpportunityRequiresCMSApprovalTrue(Set<Id> opportunityIds) {
        List<Opportunity> opportunities = opportunitySelector.byId(opportunityIds);
        for (Opportunity opportunity : opportunities) {
            System.debug('## opportunity: '+opportunity);
            opportunity.Requires_CMS_Approval_New__c = true;
            unitOfWork.registerDirty(opportunity);
        }
        TriggerHandler.bypass('OpportunityTriggerHandler');

        unitOfWork.commitWork();

        TriggerHandler.clearBypass('OpportunityTriggerHandler');
    }
}