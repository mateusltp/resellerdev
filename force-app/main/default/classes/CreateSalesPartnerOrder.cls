/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 05-02-2022
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/
public with sharing class CreateSalesPartnerOrder {

    private static final PS_Constants constants = PS_Constants.getInstance();
    private static final String className = CreateSalesPartnerOrder.class.getName();
    private List<Opportunity> aOppLst;
    private Id partnerFlowRt = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(constants.OPPORTUNITY_RT_PARTNER_FLOW_OPP).getRecordTypeId();

    public CreateSalesPartnerOrder(){
    
    }

    public void execute( ){  
        System.debug('::Execute ');
        if(System.Label.TAGUS_PARTNER !=  constants.PARTNER && !Test.isRunningTest()) return;   
        System.debug('::Execute after if ');
        this.aOppLst = new List<Opportunity>();
        for(Opportunity iOpp :  (List<Opportunity>) Trigger.new){
            if(isToCreatePartnerOrder(iOpp)){
                this.aOppLst.add(iOpp);
            }
        }
      
        if(aOppLst.isEmpty()){
            return;
        }             

        fflib_ISObjectUnitOfWork uow = Application.UnitOfWork.newInstance();
        Map<Id, Quote> mProposalByOpp = getProposalByOppId(aOppLst);
        List<Order> aOrderLst = new List<Order>();
        for(Opportunity iOpp : aOppLst){                
            uow.registerNew( createPartnerOrder( mProposalByOpp.get( iOpp.Id ) ) );
        }
        try {
            uow.commitWork();
        } catch(Exception e){
            DebugLog__c log = new DebugLog__c(
                    Origin__c = '['+className+'][execute]',
                    LogType__c = constants.LOGTYPE_ERROR,
                    RelatedToSquad__c = constants.LOGTYPE_SQUAD_PARTNERS,
                    ExceptionMessage__c = e.getMessage(),
                    StackTrace__c = e.getStackTraceString(),
                    DataScope__c = JSON.serialize(uow) // test to verify how the uow would look like at this point
            );
            Logger.createLog(log);
            aOppLst[0].addError(constants.ERROR_CREATING_ORDER_PARTNER + e.getMessage());
        }
       
    }

    private Map<Id, Quote> getProposalByOppId( List<Opportunity> aOppLst ){   
     
        return ((ProposalSelector)Application.Selector.newInstance(Quote.SObjectType)).selectQuoteByOppId( aOppLst );
    }
  
    private Order createPartnerOrder( Quote proposal ){
        return new Order (
        AccountId = proposal.Opportunity.AccountId,
        QuoteId = proposal.Id,
        CurrencyIsoCode = proposal.Opportunity.CurrencyIsoCode,
        EffectiveDate = proposal.Signed__c,
        EndDate = proposal.End_Date__c,
        OpportunityId = proposal.OpportunityId,
        Status = constants.ORDER_STATUS_INACTIVATED,
        Type = constants.PARTNER
      );
    }

    private Boolean isToCreatePartnerOrder(Opportunity aOpp){    
        Map<Id, Opportunity> oldOpportunityMap = (Map<Id, Opportunity>) Trigger.oldMap;
        Opportunity oldOpportunity = oldOpportunityMap.get(aOpp.Id);

        return aOpp.RecordTypeId.equals(partnerFlowRt) && (
            aOpp.StageName.equals('Lançado/Ganho') &&
            oldOpportunity.StageName != aOpp.StageName
        );
    }
}