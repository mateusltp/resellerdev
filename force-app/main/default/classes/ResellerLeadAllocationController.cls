/**
 * @Author              JSilva
 * @Last Modified By    jose.dasilva@gympass.com
 * @Last Modified On    27-07-2022
 * @Description         Manage the Lead records that will be assigned to Indirect Channel record type
**/
public without sharing class ResellerLeadAllocationController {
    
    Id indirectChannelRTId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Parceiro').getRecordTypeId();
    Id smbDirectChannelRTId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('SMB_Direct_Channel').getRecordTypeId();
    Id directChannelRTId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Client').getRecordTypeId();
    
    List<Lead> leadslst = trigger.new;
	
    public void updateToIndirectChannelRecordType() {
        List<Lead> leadslst = this.leadslst;

        for(Lead newLead : leadslst) {
            if(newLead.Business_Unit__c == System.Label.Lead_Indirect_Channel_Business_Unit 
                && (newLead.RecordTypeId == this.smbDirectChannelRTId || newLead.RecordTypeId == this.directChannelRTId)) {
                newLead.RecordTypeId = indirectChannelRTId;
            }
        }
    }
}