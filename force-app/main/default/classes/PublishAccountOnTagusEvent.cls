public without sharing class PublishAccountOnTagusEvent {
  public void execute() {
    if (UserInfo.getName().equals('Integration SMB Jamor')) {
      return;
    }

    List<Account> accounts = new List<Account>();

    for (Account newAccount : (List<Account>) Trigger.new) {
      if (isToPublishAccount(newAccount)) {
        accounts.add(newAccount);
      }
    }

    if (!accounts.isEmpty()) {
      new TagusAccountOutboundPublisher(accounts).runUpdate();
    }
  }

  private Boolean isToPublishAccount(Account newAccount) {
    Map<Id, Account> oldAccountMap = (Map<Id, Account>) Trigger.oldMap;
    Account oldAccount = oldAccountMap?.get(newAccount.Id);

    return (Trigger.isUpdate &&
    oldAccount.Is_Account_On_Tagus__c &&
    newAccount.Is_Account_On_Tagus__c &&
    newAccount.recordTypeId ==
    Schema.SObjectType.Account
      .getRecordTypeInfosByDeveloperName()
      .get('Empresas')
      .getRecordTypeId() &&
    (oldAccount.Send_To_Tagus__c != newAccount.Send_To_Tagus__c ||
    oldAccount.ParentId != newAccount.ParentId ||
    oldAccount.BillingStreet != newAccount.BillingStreet ||
    oldAccount.BillingCity != newAccount.BillingCity ||
    oldAccount.BillingState != newAccount.BillingState ||
    oldAccount.BillingPostalCode != newAccount.BillingPostalCode ||
    oldAccount.BillingCountry != newAccount.BillingCountry ||
    oldAccount.Id_Company__c != newAccount.Id_Company__c ||
    oldAccount.Razao_Social__c != newAccount.Razao_Social__c ||
    oldAccount.Business_Unit__c != newAccount.Business_Unit__c ||
    oldAccount.Email__c != newAccount.Email__c ||
    oldAccount.Legal_Document_Type__c != newAccount.Legal_Document_Type__c ||
    oldAccount.Phone != newAccount.Phone));
  }
}