@IsTest
public class UpsertClientOrderTATest {
  @IsTest
  public static void execute() {
    Test.setMock(HttpCalloutMock.class, new HttpResponseMock());

    Account gympassEntity = AccountMock.getGympassEntity();
    gympassEntity.UUID__c = new Uuid().getValue();
    insert gympassEntity;

    Account account = AccountMock.getStandard('Empresas');
    account.Send_To_Tagus__c = true;
    insert account;

    Contact contact = ContactMock.getStandard(account);
    insert contact;

    account.Attention__c = contact.Id;
    update account;

    Pricebook2 pricebook = new Pricebook2(
      CurrencyIsoCode = 'BRL',
      Id = Test.getStandardPricebookId(),
      IsActive = true
    );
    upsert pricebook;

    List<Product2> products = new List<Product2>();

    Product2 setupFee = ProductMock.getSetupFee();
    products.add(setupFee);

    Product2 accessFee = ProductMock.getAccessFee();
    products.add(accessFee);

    insert products;

    List<PricebookEntry> pricebookEntries = new List<PricebookEntry>();

    PricebookEntry setupFeePricebookEntry = PricebookMock.getPricebookEntry(
      pricebook,
      setupFee
    );
    pricebookEntries.add(setupFeePricebookEntry);

    PricebookEntry accessFeePricebookEntry = PricebookMock.getPricebookEntry(
      pricebook,
      accessFee
    );
    pricebookEntries.add(accessFeePricebookEntry);

    insert pricebookEntries;

    Opportunity newBusiness = OpportunityMock.getClientSalesSkuNewBusiness(
      account,
      pricebook
    );
    newBusiness.Gympass_Entity__c = gympassEntity.Id;
    insert newBusiness;

    Payment__c newBusinessPayment = PaymentMock.getStandard(
      newBusiness,
      account
    );

    insert newBusinessPayment;

    Eligibility__c newBusinessEligibility = EligibilityMock.getStandard(
      newBusinessPayment
    );

    insert newBusinessEligibility;

    Quote newBusinessQuote = QuoteMock.getStandard(newBusiness);
    insert newBusinessQuote;

    List<QuoteLineItem> newBusinessQuoteLineItems = new List<QuoteLineItem>();

    QuoteLineItem newBusinessSetupFeeLine = QuoteLineItemMock.getSetupFee(
      newBusinessQuote,
      setupFeePricebookEntry
    );
    newBusinessQuoteLineItems.add(newBusinessSetupFeeLine);

    QuoteLineItem newBusinessAccessFeeLine = QuoteLineItemMock.getEnterpriseSubscription(
      newBusinessQuote,
      accessFeePricebookEntry
    );
    newBusinessAccessFeeLine.UUID__c = new Uuid().getValue();
    newBusinessQuoteLineItems.add(newBusinessAccessFeeLine);

    insert newBusinessQuoteLineItems;

    Waiver__c newBusinessWaiver = WaiverMock.getStandard(
      newBusinessAccessFeeLine
    );

    insert newBusinessWaiver;

    Opportunity renegotiation = OpportunityMock.getClientSuccessSkuRenegotiation(
      account,
      pricebook
    );
    renegotiation.Gympass_Entity__c = gympassEntity.Id;
    renegotiation.Sub_Type__c = 'Retention';
    insert renegotiation;

    Payment__c renegotiationPayment = PaymentMock.getStandard(
      renegotiation,
      account
    );

    insert renegotiationPayment;

    Eligibility__c renegotiationEligibility = EligibilityMock.getStandard(
      renegotiationPayment
    );

    insert renegotiationEligibility;

    Quote renegotiationQuote = QuoteMock.getStandard(renegotiation);
    insert renegotiationQuote;

    List<QuoteLineItem> renegotiationQuoteLineItems = new List<QuoteLineItem>();

    QuoteLineItem renegotiationSetupFeeLine = QuoteLineItemMock.getSetupFee(
      renegotiationQuote,
      setupFeePricebookEntry
    );
    renegotiationQuoteLineItems.add(renegotiationSetupFeeLine);

    QuoteLineItem renegotiationAccessFeeLine = QuoteLineItemMock.getEnterpriseSubscription(
      renegotiationQuote,
      accessFeePricebookEntry
    );
    renegotiationQuoteLineItems.add(renegotiationAccessFeeLine);

    insert renegotiationQuoteLineItems;

    Waiver__c renegotiationWaiver = WaiverMock.getStandard(
      renegotiationAccessFeeLine
    );

    insert renegotiationWaiver;

    Test.startTest();

    newBusiness.StageName = 'Lançado/Ganho';
    update newBusiness;

    renegotiation.StageName = 'Perdido';
    renegotiation.Loss_Reason__c = 'Technology issues';
    renegotiation.Cancellation_Reason__c = 'Technology issues';
    renegotiation.cancellation_date__c = Date.today().addMonths(2);
    update renegotiation;

    Test.stopTest();

    List<Order> orders = [
      SELECT Id, Status, Cancelation_Date__c, Cancelation_Reason__c
      FROM Order
    ];

    System.assertEquals(1, orders.size(), 'Should have just one order created');

    System.assertEquals(
      'Activated',
      orders[0].Status,
      'Should stay in active status'
    );

    System.assertEquals(
      renegotiation.cancellation_date__c,
      orders[0].Cancelation_Date__c,
      'Should update cancelation date to the same of the retention opportunity'
    );

    System.assertEquals(
      renegotiation.Cancellation_Reason__c,
      orders[0].Cancelation_Reason__c,
      'Should update cancelation reason to the same of the retention opportunity'
    );
  }

  private class HttpResponseMock implements HttpCalloutMock {
    public HTTPResponse respond(HTTPRequest request) {
      HttpResponse response = new HttpResponse();
      response.setHeader('Content-Type', 'application/json');
      response.setStatusCode(200);
      return response;
    }
  }
}