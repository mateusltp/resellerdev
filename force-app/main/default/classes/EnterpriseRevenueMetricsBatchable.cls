/**
 *  @vncferraz
 * 
 */
global class EnterpriseRevenueMetricsBatchable implements Database.Batchable<sObject>{
   

    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator('select Id,ES_Revenue__c,ER_Revenue__c,AES_Revenue__c, AER_Revenue__c from Quote where (Opportunity.RecordType.Name IN (\'Client Sales - New Business\',\'Client Success - Renegotiation\',\'SMB Success - Renegotiation\',\'SMB - New Business\')) and  ( (Opportunity.StageName <> \'Perdido\' and Opportunity.StageName <> \'Lançado/Ganho\') or (Opportunity.StageName = \'Lançado/Ganho\' and (ES_Revenue__c=null or AES_Revenue__c = null) ))');
    }

    public void execute(Database.BatchableContext BC, List<Quote> scope){
        List<Quote> quotesToUpdateRevenue = new List<Quote>();
        for ( Quote proposal : scope ){

            EnterpriseRevenueMetrics revenueMetrics = new EnterpriseRevenueMetrics();          
            revenueMetrics.refreshQuoteRevenues(proposal.Id);
          
            if ( ! hasReveuneDivergence(proposal, revenueMetrics)  )
                continue;

            proposal.ES_Revenue__c = revenueMetrics.getEsRevenue();
            proposal.AES_Revenue__c = revenueMetrics.getAesRevenue();
            proposal.ER_Revenue__c = revenueMetrics.getErRevenue();
            proposal.AER_Revenue__c = revenueMetrics.getAerRevenue();

            quotesToUpdateRevenue.add( proposal );
            
        }
        System.debug( ' quotesToUpdateRevenue = ' +  JSON.serialize(quotesToUpdateRevenue) );
        update quotesToUpdateRevenue;
        
    }

    public void finish(Database.BatchableContext BC){}

    public Decimal scale(Decimal d){
        return d.setScale(2);
    }

    public boolean hasReveuneDivergence(Quote proposal, EnterpriseRevenueMetrics revenueMetrics){        
       return (
        ( proposal.ES_Revenue__c==null || proposal.ER_Revenue__c==null || proposal.AES_Revenue__c==null || proposal.AER_Revenue__c==null )
        ||
        ( scale(proposal.ES_Revenue__c) <> scale(revenueMetrics.getEsRevenue()) || 
          scale(proposal.ER_Revenue__c) <> scale(revenueMetrics.getErRevenue()) ||
          scale(proposal.AES_Revenue__c) <> scale(revenueMetrics.getAesRevenue()) ||
          scale(proposal.AER_Revenue__c) <> scale(revenueMetrics.getAerRevenue()) )
       );
    }

}