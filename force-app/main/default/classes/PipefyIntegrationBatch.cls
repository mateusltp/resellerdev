/**
 * @description       : 
 * @author            : AlyssonMota
 * @group             : 
 * @last modified on  : 08-05-2021
 * @last modified by  : Alysson Mota
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   07-16-2021   Alysson Mota   Initial Version
**/
global with sharing class PipefyIntegrationBatch implements Database.Batchable<sObject>, Database.AllowsCallouts {
    global Database.QueryLocator start(Database.BatchableContext bc) {
       
        return Database.getQueryLocator([
            SELECT Id,Name,CloseDate,CreatedDate,RecordType.Name,Description, Pipefy_Card_ID__c
                FROM Opportunity 
                WHERE StageName = 'Lançado/Ganho' 
                AND Pipefy_Card_ID__c = null 
                AND RecordType.DeveloperName in ('SMB_Success_Renegotiation','SMB_New_Business')
                AND AccountId != null
                AND Account.Send_To_Tagus__c = false
                AND Self_Checkout_New_Business__c = false
                AND  AccountId in   (select Account_ID__c from Offer_Queue__c where Status__c = 'Success')  
                AND CreatedDate >=:System.today().addDays(-4) Order By CreatedDate ASC
        ]);
    }
    
    global void execute(Database.BatchableContext bc, List<Opportunity> scope){
        Map<Id, DataStruct> oppIdToDataStruct = new Map<Id, DataStruct>();
        
        for (Opportunity opp : scope) {
            PipefyIntegrationService.ResponseData respData = PipefyIntegrationController.callServiceFromBatch((String)opp.Id);
            DataStruct dStruct = new DataStruct();
            dStruct.opportunity = opp;
            dStruct.respData = respData;
            
            oppIdToDataStruct.put(opp.Id, dStruct);
        }

        updateOpps(oppIdToDataStruct);
    }

    private void updateOpps(Map<Id, DataStruct> oppIdToDataStruct) {
        List<Opportunity> oppsToUpdate = new List<Opportunity>();

        for (Id oppId : oppIdToDataStruct.keySet()) {
            DataStruct dStruct = oppIdToDataStruct.get(oppId);
            
            if (dStruct.respData.pipefyCardId != null) {
                dStruct.opportunity.Pipefy_Card_ID__c = dStruct.respData.pipefyCardId;
                dStruct.opportunity.Pipefy_Card_URL__c = dStruct.respData.pipefyCardUrl;
            }

            dStruct.opportunity.Description += '\n('+System.now()+') Pipefy Integration: Status Code ' + dStruct.respData.httpResponse.getStatusCode() + '\n' + dStruct.respData.httpResponse.getStatus();            

            oppsToUpdate.add(dStruct.opportunity);
        }

        update oppsToUpdate;
    }

    global void finish(Database.BatchableContext bc){
        
    }

    private class DataStruct {
        public Opportunity opportunity {get; set;}    
        public PipefyIntegrationService.ResponseData respData {get; set;}    
    }
}