@isTest(seeAllData=false)
public with sharing class PriceEngineServiceTest {
    

    // @TestSetup
    // static void makeData(){

    //     Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        
        
    //  	User user = new User(LastName = 'priceEngineServiceTest',
    //                        FirstName='priceEngineServiceTest',
    //                        Alias = 'jliv',
    //                        Email = 'priceEngineServiceTest@PriceEngineServiceTest.com',
    //                        Username = 'priceEngineServiceTest@PriceEngineServiceTest.com',
    //                        ProfileId = profileId.id,
    //                        Bypass_automations__c = true,
    //                        TimeZoneSidKey = 'GMT',
    //                        LanguageLocaleKey = 'en_US',
    //                        EmailEncodingKey = 'UTF-8',
    //                        LocaleSidKey = 'en_US'
    //                        );
    //     insert user;                   
    //     System.runAs(user)
    //     {
    //     Pricebook2 lStandardPb = DataFactory.newPricebook();
    //     update lStandardPb;

    //     Account acc = generateAccount();
    //     insert acc;

    //     TriggerHandler.bypass('OpportunityTriggerHandler');
    //     Opportunity lOpp = generateOpportunity(acc, lStandardPb);
    //     insert lOpp;
            
    //     EnablersFactory enablersFactory = new EnablersFactory();
    //     List<Step_Towards_Success1__c> oppEnablers = enablersFactory.createEnablersForOpportunityClients(lOpp);
    //     insert oppEnablers ;

    //     Product2 lAcessFee = DataFactory.newProduct( 'Enterprise Subscription' , false , 'USD' );
    //     Database.insert( lAcessFee );

    //     PricebookEntry lAccessFeeEntry = DataFactory.newPricebookEntry( lStandardPb , lAcessFee , lOpp );
    //     Database.insert( lAccessFeeEntry );

    //     PriceEngine__c priceEngine = new PriceEngine__c( CurrencyIsoCode = 'USD');
    //     insert priceEngine;

    //     PriceEngineItem__c priceEngineItem = new PriceEngineItem__c(    Absolute_Value__c = 12.99, 
    //                                                                     PriceEngineId__c = priceEngine.Id, 
    //                                                                     BusinessRuleFor__c = 'Intermediation', 
    //                                                                     CommissionPercentage__c = 30, 
    //                                                                     MaximumNumberEmployees__c = 1000,
    //                                                                     CurrencyIsoCode = 'USD', 
    //                                                                     MinimumNumberEmployees__c = 1,
    //                                                                     PartnerCategory__c = 'Standard');
    //     insert priceEngineItem;

    //     priceEngine.Active__c = true;
    //     update priceEngine;


    //     Quote lProposal = DataFactory.newQuote( lOpp , 'Quote Test', 'Indirect_Channel' );
    //     lProposal.License_Fee_Waiver__c = 'Yes';
    //     lProposal.Percent_of_comission__c = '30';
    //     lProposal.Waiver_Termination_Date__c = system.today().addDays(10);
    //     lProposal.Will_this_company_have_the_Free_Product__c = 'No';
    //     Database.insert( lProposal );
 
    //     QuoteLineItem lEnterpriseSubscription = DataFactory.newQuoteLineItem( lProposal , lAccessFeeEntry );
    //     Database.insert( lEnterpriseSubscription );

    //     SKU_Price__c skuPriceFamily = DataFactory.newSKUPrice(lAcessFee.Id, lOpp, acc, 1, 1);
    //     insert skuPriceFamily;
    //     }
        

        
    // }

    // @isTest
    // public static void testGetPercentComissionList(){

    //     QuoteLineItem item = [ SELECT Id, QuoteId, Quantity, UnitPrice, List_Price__c FROM QuoteLineItem LIMIT 1];

    //     String result = PriceEngineService.getPercentComissionList(item.Id);

    //     PriceEngineService.updatePriceByQuoteLineItem(new List<QuoteLineItem> {item});

    //     System.assertNotEquals(null, result);

    // }

    // @isTest
    // public static void testGetOldPartnerCategoryOnAccountReseller(){
    //     String retorno = PriceEngineService.getOldPartnerCategoryOnAccountReseller('5k FTEs');
    //     String retorno2 = PriceEngineService.getOldPartnerCategoryOnAccountReseller('15k FTEs');
    //     String retorno3 = PriceEngineService.getOldPartnerCategoryOnAccountReseller('Teste');

    //     System.assertEquals(retorno, ResellerEnumRepository.PriceEngineItemPartnerCategory.Subsidy_5kFTEs.name());
    //     System.assertEquals(retorno2, ResellerEnumRepository.PriceEngineItemPartnerCategory.Subsidy_15kFTEs.name());
    //     System.assertEquals(retorno3, 'Teste');
    // }

    // @isTest
    // public static void testGetPriceEngineItemLegacy(){
    //     List<PriceEngineItem__c> itens = PriceEngineService.getPriceEngineItemLegacy(null, 'USD', null);
    //     List<PriceEngineItem__c> itens2 = PriceEngineService.getPriceEngineItem(null, 'USD', null, null);
        
    // }


    // private static Account generateAccount(){
    //     Id rtId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Reseller').getRecordTypeId();
    //     Account acc = new Account();
    //     acc.name='AcademiaBrasilCompanyPai';
    //     acc.RecordTypeId = rtId;
    //     acc.GP_Status__c = 'Active';
    //     acc.billingState = 'Minas Gerais';
    //     acc.CAP_Value__c = 120;
    //     acc.BillingCity = 'CityAcademiaBrasil';
    //     acc.billingStreet = 'Rua academiabrasilpai';
    //     acc.billingCountry = 'Brazil';
    //     acc.Gym_Type__c = 'Other';
    //     acc.Gym_Classes__c = 'Cardio';
    //     acc.Indirect_Channel_Pricebook__c = ResellerEnumRepository.PriceEngineItemPartnerCategory.Standard.name();
    //     acc.Subscription_Type__c = 'Value per class';
    //     acc.Subscription_Period__c = 'Monthy value';
    //     acc.Subscription_Type_Estimated_Price__c    = 100;
    //     acc.Has_market_cannibalization__c = 'No';
    //     acc.Exclusivity__c = 'No';
    //     acc.Exclusivity_End_Date__c = Date.today().addYears(1);
    //     //acc.Exclusivity_Partnership__c = 'Full Exclusivity';
    //     //acc.Exclusivity_Restrictions__c= 'No';
    //     acc.Website = 'testing@tesapex.com';
    //     acc.Gym_Email__c = 'gymemail@apex.com';
    //     acc.Phone = '3222123123';
    //     acc.Can_use_logo__c = 'Yes';
    //     acc.Legal_Registration__c = 12123;
    //     acc.Legal_Title__c = 'Title LTDA';
    //     acc.Gyms_Identification_Document__c = 'CNPJ'; 
    //     return acc;
    // }

    // private static Opportunity generateOpportunity(Account acc, Pricebook2 pb){
    //     Id oppRtId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Indirect_Channel_New_Business').getRecordTypeId();
    //     Opportunity accOpp = new Opportunity();
    //     accOpp.recordTypeId = oppRtId;
    //     accOpp.TotalOpportunityQuantity = 100;
    //     accOpp.Reseller_del__c = acc.Id;
    //     accOpp.AccountId = acc.id;
    //     accOpp.Name = 'Test1'; 
    //     accOpp.CMS_Used__c = 'Yes';     
    //     accOpp.Gym_agreed_to_an_API_integration__c  = 'Yes';
    //     accOpp.Club_Management_System__c = 'Companhia Athletica';
    //     accOpp.Integration_Fee_Deduction__c = 'No';
    //     accOpp.CloseDate = Date.today();
    //     accOpp.Success_Look_Like__c = 'Yes';
    //     accOpp.Success_Look_Like_Description__c = 'Money money';
    //     accOpp.StageName = 'Validated';
    //     accOpp.Type = 'Expansion';  
    //     accOpp.Country_Manager_Approval__c = true;
    //     accOpp.Payment_approved__c = true;   
    //     accOpp.CurrencyIsoCode = 'USD';
    //     accOpp.Gympass_Plus__c = 'Yes';
    //     accOpp.B_M__c = 'Intermediation';
    //     accOpp.Standard_Payment__c = 'Yes';
    //     accOpp.Request_for_self_checkin__c = 'Yes';  
    //     accOpp.Pricebook2Id = Test.getStandardPricebookId();//pb.Id;
    //     return accOpp;
    // }

    // private static Pricebook2 generatePricebook(){        
    //     Pricebook2 pb = new Pricebook2();
    //     pb.Name = 'Br';
    //     pb.Country__c = 'Brazil';
    //     pb.IsActive = true;
    //     return pb;
    // }

    // private static Product2 generateAccessFeeProduct(){
    //     Product2 product = new Product2();
    //     product.Name = 'Enterprise Subscription';
    //     product.Minimum_Number_of_Employees__c = 1;
    //     product.Maximum_Number_of_Employees__c = 900;
    //     product.Family = 'Enterprise Subscription';
    //     return product;
    // }

    // private static void generateAccessFeePricebookEntry(Pricebook2 pb, Product2 product){        
    //     PricebookEntry pbEntry = new PricebookEntry();
    //     pbEntry.Product2Id = product.Id;
    //     pbEntry.Pricebook2Id = Test.getStandardPricebookId();
    //     pbEntry.UnitPrice = 1;
    //     pbEntry.CurrencyISOCode = 'USD';
    //     pbEntry.IsActive = true;
    //     insert pbEntry;
       
    //     PricebookEntry custompbEntry = new PricebookEntry();
    //     custompbEntry.Product2Id = product.Id;
    //     custompbEntry.Pricebook2Id = pb.Id;
    //     custompbEntry.UnitPrice = 1;
    //     custompbEntry.CurrencyISOCode = 'USD';
    //     custompbEntry.IsActive = true;
    //     insert custompbEntry;
    // }

    // public static SKU_Price__c newSKUPrice(Id prodId, Account acc, Integer minQuantity, Decimal unitPrice){
    //     SKU_Price__c skuPrice = new SKU_Price__c();
    //     skuPrice.Product__c = prodId;
    //     skuPrice.Start_Date__c = Date.today();
    //     skuPrice.End_Date__c = null;
    //     skuPrice.Minimum_Quantity__c = 1;
    //     skuPrice.CountrySKU__c = acc.BillingCountry;
    //     skuPrice.CurrencyIsoCode = 'USD';
    //     skuPrice.Unit_Price__c = 10;
    //     skuPrice.Business_Model__c = 'Intermediation';
    //     return skuPrice;
    // }

}