/**
 * @description       : 
 * @author            : marcus.silva@gft.com
 * @group             : 
 * @last modified on  : 17-06-2021
 * @last modified by  : marcus.silva@gft.com
 * Modifications Log 
 * Ver   Date         Author         			Modification
 * 1.0   07-05-2021   marcus.silva@gft.com      Initial Version
 * 2.0   24-05-2021   laerte.kimura@gft.com     Added after insert with createContractData method
 * 3.0   17-06-2021   marcus.silva@gft.com      Change beforeInsert to call a init method 
**/
public with sharing class ContractAgreementHandler extends TriggerHandler{
    public override void beforeInsert() {
        ContractAgreementTriggerHelper.init(Trigger.New);        
    }
    public override void afterInsert(){ 
        ContractAgreementTriggerHelper.createContractData(Trigger.new);
        /* Used to keep a clone from contract agreement until Conga app be removed */
        new CloneObject().CloneDataFromContractAgreement(Trigger.new);
    }
    public override void afterUpdate(){ 
        new ManagedClauseHelper(Trigger.new);
         /* Used to keep a clone from contract agreement until Conga app be removed */
         new CloneObject().CloneDataFromContractAgreement(Trigger.new);
    }
}