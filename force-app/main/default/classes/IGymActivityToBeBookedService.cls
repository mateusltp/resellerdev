/**
 * @description       : 
 * @author            : alysson.mota@gympass.com
 * @group             : 
 * @last modified on  : 06-29-2022
 * @last modified by  : alysson.mota@gympass.com
**/
public interface IGymActivityToBeBookedService {
    void createNewToProdFromGymActivityRels(Id prodId, List<Gym_Activity_Relationship__c> gymActivityRels);
}