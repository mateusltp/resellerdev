@isTest(seeAllData=false)
public with sharing class ResellerSKUDealDeskHandlerTest {
    
    @TestSetup
    private static void createData(){  
        Pricebook2 lStandardPricebook = DataFactory.newPricebook();
        Database.update( lStandardPricebook );

        Account lAcc = DataFactory.newAccount();
        lAcc.Id_Company__c = '00614573000105';
        lAcc.Legal_Document_Type__c = 'CNPJ';
        lAcc.NumberOfEmployees = 100;
        lAcc.Website = 'testesku2.com';
        Database.insert( lAcc );
        
        Product2 lEsProduct = DataFactory.newProduct( 'Enterprise Subscription' , false , 'BRL' );
        lEsProduct.Payment_Type__c = 'Recurring fee';
        Database.insert( lEsProduct );

        Opportunity lSKUNewOpp = DataFactory.newOpportunity( lAcc.Id , lStandardPricebook , 'Indirect_Channel_New_Business' );
        lSKUNewOpp.Name = 'OppTestSKU';
        lSKUNewOpp.Billing_Period__c = 'Monthly';
        lSKUNewOpp.TotalOpportunityQuantity = 1000;
        lSKUNewOpp.Quantity_Offer_Number_of_Employees__c = 1000;
        lSKUNewOpp.Purchase_Order_number_is_needed__c = 'Yes';
        lSKUNewOpp.Membership_Fee_PO_Duration__c = 'Annual';
        lSKUNewOpp.B_M__c = 'Intermediation';
        
        PricebookEntry lEnterpriseSubscriptionEntry = DataFactory.newPricebookEntry( lStandardPricebook , lEsProduct , lSKUNewOpp );
        Database.insert( lEnterpriseSubscriptionEntry );

        SKU_Price__c lSkuPrice = DataFactory.newSKUPrice( lEsProduct.Id , lSKUNewOpp , lAcc , 1 , 50 );
        Database.insert( lSkuPrice );

        Database.insert( lSKUNewOpp );

        Quote lQuote = DataFactory.newQuote( lSKUNewOpp , 'QuoteTest' , 'Client_Sales_SKU_New_Business' );
        Database.insert( lQuote );
        
        lSKUNewOpp.SyncedQuoteId = lQuote.Id;
        Database.update( lSKUNewOpp );

        QuoteLineItem lEnterpriseSubscriptionItem = DataFactory.newQuoteLineItem( lQuote , lEnterpriseSubscriptionEntry );
        lEnterpriseSubscriptionItem.Discount = 50;
        Database.insert( lEnterpriseSubscriptionItem );
        
        Payment__c lPayment = DataFactory.newPayment( lSKUNewOpp );
        Database.insert( lPayment );
        
        Eligibility__c lEligibility = DataFactory.newEligibility( lPayment );
        Database.insert( lEligibility );
        
        
    }

    @isTest
    private static void shouldAskForApproval(){
        Opportunity lOpp = [ SELECT Id, SyncedQuoteId FROM Opportunity WHERE Name LIKE '%OppTestSKU' ];

        List< Assert_Data__c > lLstAssertData = new List< Assert_Data__c >{
            new Assert_Data__c( 	
                Opportunity__c = lOpp.Id,
                Object_Name__c = 'opportunity', 
                Field_Name__c = 'billing_period__c',
                Old_Value__c  = 'Biannually'
                ),
            new Assert_Data__c( 	
                Opportunity__c = lOpp.Id,
                Object_Name__c = 'opportunity', 
                Field_Name__c = 'achieved_steps_towards_success_quantity__c',
                Old_Value__c  = '4'
                )
        };
        
        Database.insert( lLstAssertData );

        lOpp.Billing_Period__c = 'Quarterly';
        lOpp.achieved_steps_towards_success_quantity__c = 3;

        Test.startTest();

        Database.update( lOpp );
        System.assertEquals('Quarterly', lOpp.Billing_Period__c);
        
        Test.stopTest();
    }

    @isTest
    private static void QuoteLineItemUpdate(){
        Opportunity lOpp = [ SELECT Id, SyncedQuoteId FROM Opportunity WHERE Name LIKE '%OppTestSKU' ];
        Quote lQuote = [ SELECT Id, Deal_Desk_Approved_Conditions__c FROM Quote WHERE Id =: lOpp.SyncedQuoteId ];
         QuoteLineItem lQuoteLineItem = [ SELECT Id, Fee_contract_type__c, Unitprice FROM QuoteLineItem WHERE QuoteId =: lQuote.Id ];
    
        lQuoteLineItem.Fee_contract_type__c = 'Variable per eligible';
		lQuoteLineItem.Unitprice = 2.5;
        
        Test.startTest();

        Database.update( lQuoteLineItem );
        System.assertEquals('Variable per eligible', lQuoteLineItem.Fee_contract_type__c);
        System.assertNotEquals(null, lQuote.Deal_Desk_Approved_Conditions__c);
        
        Test.stopTest();
    }
    
    @isTest
    private static void PaymentUpdate(){
        Opportunity lOpp = [ SELECT Id, SyncedQuoteId FROM Opportunity WHERE Name LIKE '%OppTestSKU' ];
        Payment__c lPayment = [ SELECT Id, Frequency__c FROM Payment__c WHERE Opportunity__c =: lOpp.Id ];
       
        List< Assert_Data__c > lLstAssert_Data = new List< Assert_Data__c >{
            new Assert_Data__c(
               	Opportunity__c = lOpp.id,
                Object_Name__c = 'payment__c', 
                Field_Name__c = 'frequency__c' ,
                Old_Value__c  = 'Payroll',
                New_Value__c = '',
                Fee_type__c = 'Enterprise Subscription'
            ),
            new Assert_Data__c(
                Opportunity__c = lOpp.id,
                Object_Name__c = 'payment__c', 
                Field_Name__c = 'frequency__c' ,
                Old_Value__c  = 'Payroll',
                New_Value__c = '',
                Fee_type__c = 'Setup Fee'
            ),
            new Assert_Data__c(
                Opportunity__c = lOpp.id,
                Object_Name__c = 'quotelineitem', 
                Field_Name__c = 'unitprice' ,
                Old_Value__c  = '0.5',
                New_Value__c = '',
                Fee_type__c = 'Professional Services Setup Fee'
            ),
            new Assert_Data__c(
                Opportunity__c = lOpp.id,
                Object_Name__c = 'quotelineitem', 
                Field_Name__c = 'unitprice' ,
                Old_Value__c  = '2.5',
                New_Value__c = '',
                Fee_type__c = 'Professional Services Maintenance Fee'
            )
        };

        Database.insert( lLstAssert_Data );

        lPayment.Frequency__c = 'Yearly';
        
        Test.startTest();

        Database.update( lPayment );
        
        System.assertEquals('Yearly', lPayment.Frequency__c);
        
        
        Test.stopTest();
    }
    
 @isTest
    private static void EligibilityUpdate(){
        Opportunity lOpp = [ SELECT Id, SyncedQuoteId FROM Opportunity WHERE Name LIKE '%OppTestSKU' ];
        Eligibility__c lEligibility = [ SELECT Id, Payment_Method__c FROM Eligibility__c WHERE Opportunity_Id__c =: lOpp.Id ];
    
         Assert_Data__c cDataOpp = new Assert_Data__c( 	Opportunity__c = lOpp.id,
                                                         Object_Name__c = 'eligibility__c', 
                                                         Field_Name__c = 'payment_method__c' ,
                                                         Old_Value__c  = 'Payroll',
                                                         New_Value__c = '',
                                                         Fee_type__c = '');
        insert cDataOpp;
        
        lEligibility.Payment_Method__c = 'Credit Card';
        
        Test.startTest();

        Database.update( lEligibility );
        
        System.assertEquals('Credit Card', lEligibility.Payment_Method__c);
        
        Test.stopTest();
    }    

    @isTest
    private static void shouldAskForQuoteApproval(){
        Opportunity lOpp = [ SELECT Id, SyncedQuoteId FROM Opportunity WHERE Name LIKE '%OppTestSKU' ];
        Quote lQuote = [ SELECT Id, Deal_Desk_Approved_Conditions__c FROM Quote WHERE Id =: lOpp.SyncedQuoteId ];
        
        List< Assert_Data__c > lLstAssertData = new List< Assert_Data__c >{
            new Assert_Data__c( 	
                Opportunity__c = lOpp.Id,
                Object_Name__c = 'quote', 
                Field_Name__c = 'autonomous_marketplace_contract__c' ,
                Old_Value__c  = 'Yes'
                )
        };
        
        Database.insert( lLstAssertData );

        lQuote.autonomous_marketplace_contract__c = 'No';

        Test.startTest();

        Database.update( lQuote );
        
        System.assertEquals('No', lQuote.autonomous_marketplace_contract__c);
        System.assertNotEquals(null, lQuote.Deal_Desk_Approved_Conditions__c);
        
        Test.stopTest();
    }

    @isTest
    private static void shouldApproveDealDeskCase(){
        Opportunity lOpp = [ SELECT Id, SyncedQuoteId FROM Opportunity WHERE Name LIKE '%OppTestSKU' ];
        Quote lQuote = [ SELECT Id FROM Quote WHERE Id =: lOpp.SyncedQuoteId ];

        Case lDealDeskCase = DataFactory.newCase( lOpp , lQuote , 'Deal_Desk_Approval' );
        lDealDeskCase.Deal_Desk_Evaluation__c = 'Approved';
        Database.insert( lDealDeskCase );

        List< Approval_Item__c > lLstApprovalItem = new List< Approval_Item__c >{
            new Approval_Item__c(
                Name = 'Custom payment frequency',
                Value__c = 'Monthly',
                Approval_Status__c = 'Open',
                Case__c = lDealDeskCase.Id
            ),
            new Approval_Item__c(
                Name = 'No autonomous marketplace',
                Value__c = 'No',
                Approval_Status__c = 'Open',
                Case__c = lDealDeskCase.Id
            ),
            new Approval_Item__c(
                Name = 'Less than 5 enablers',
                Value__c = '3',
                Approval_Status__c = 'Open',
                Case__c = lDealDeskCase.Id
            )
        };

        Database.insert( lLstApprovalItem );

        Test.startTest();

        for( Approval_Item__c iItem : lLstApprovalItem ){
            iItem.Approval_Status__c = 'Approved';
            iItem.Comments__c = 'ok';
        }

        Database.update( lLstApprovalItem );
        
        System.assertEquals('Approved', lDealDeskCase.Deal_Desk_Evaluation__c);

        Test.stopTest();
    }
}