/**
* @author vncferraz
* 
*/
@isTest(seeAllData=false)
public with sharing class EnterpriseRevenueMetricsBatchableTest {
    
    @TestSetup
    static void createData(){        
        Account acc = generateAccount();
        insert acc;
        Pricebook2 standardPricebook = new Pricebook2(
            Id = Test.getStandardPricebookId(),
            IsActive = true
        );
        update standardPricebook;
        Pricebook2 pb = standardPricebook;                      
        Product2 accessFee = generateProduct('Enterprise Subscription');
        insert accessFee;
        Product2 setupFee = generateProduct('Setup Fee');
        insert setupFee;        
        Opportunity opp = new Opportunity(CurrencyIsoCode='BRL');
        PricebookEntry accessFeeEntry = generatePricebookEntry(pb, accessFee, opp);
        insert accessFeeEntry;
        Product2 pFamilyMember = generateAccessFeeFamilyMemberIncludedProduct();
        insert pFamilyMember;
        PricebookEntry accessFeeFamilyMember = generatePricebookEntry(pb, pFamilyMember, opp);
        insert accessFeeFamilyMember;
        PricebookEntry setupFeeEntry = generatePricebookEntry(pb, setupFee, opp);
        insert setupFeeEntry;
        Product2 pProfServicesOneFee = generateProfServicesOneFeeProduct();
        insert pProfServicesOneFee;
        PricebookEntry profServicesOneFee = generatePricebookEntry(pb, pProfServicesOneFee, opp);
        insert profServicesOneFee;
        Product2 pProfServicesMainFee = generateProfServicesMainFeeProduct();
        insert pProfServicesMainFee;
        PricebookEntry profServicesMainFee = generatePricebookEntry(pb, pProfServicesMainFee, opp);
        insert profServicesMainFee;
        opp = generateOpportunity(acc,pb);
        insert opp;        
        
        generateWaiver();
    }
    
    @isTest
    private static void testSave(){        
        Opportunity opp = [select Id,AccountId from Opportunity];
        
        FastTrackProposalCreationTO to = new FastTrackProposalCreationBuilder()
            .withOpportunity(opp.Id)
            .withDealHierarchy(2, new List<Id>{opp.AccountId} )
            .withLastCreatedQuoteWhenExists(opp.Id)            
            .build();
        
        
        to.customIntegrationsRequired = 'Yes';
        to.areDependentsIncluded = 'Yes';
        to.quantity = 501;
        FastTrackProposalCreation proposalCreation = new FastTrackProposalCreation(opp.Id);
        
        Test.startTest();
        PlatformEventsHelper.publishStandardOfferCreationEvent(new List<Opportunity>{opp});
        proposalCreation.save(to);
        
        
        Quote quote = [select Id,ER_Revenue__c,AER_Revenue__c,ES_Revenue__c,AES_Revenue__c from Quote where OpportunityId = :opp.Id];
        quote.ER_Revenue__c = 0;
        quote.AER_Revenue__c = 0;
        quote.AES_Revenue__c = 0;
        quote.ES_Revenue__c = 0;
        update quote;
        
        quote = [select Id,ER_Revenue__c,AER_Revenue__c,ES_Revenue__c,AES_Revenue__c from Quote where OpportunityId = :opp.Id];
        
        System.assertEquals(quote.ER_Revenue__c,0);
        System.assertEquals(quote.AER_Revenue__c,0);
        System.assertEquals(quote.ES_Revenue__c,0);
        System.assertEquals(quote.AES_Revenue__c,0);
        System.debug( '---- before ER_Revenue__c = ' + quote.ER_Revenue__c);
        
        EnterpriseRevenueMetricsBatchable revenueBatchable = new EnterpriseRevenueMetricsBatchable();
        Database.executeBatch( revenueBatchable );
        Test.stopTest();
        
        quote = [select Id,ER_Revenue__c,AER_Revenue__c,ES_Revenue__c,AES_Revenue__c from Quote where OpportunityId = :opp.Id];
        
        System.assertNotEquals(quote.ER_Revenue__c,0);
        System.assertNotEquals(quote.AER_Revenue__c,0);
        System.assertNotEquals(quote.ES_Revenue__c,0);
        System.assertNotEquals(quote.AES_Revenue__c,0);
        System.debug( '----after  ER_Revenue__c = ' + quote.ER_Revenue__c);
    }
    
    static testmethod void testSchedule() {
        Test.StartTest();
        EnterpriseRevenueMetricsSchedulable s = new EnterpriseRevenueMetricsSchedulable();
        String sch = '0 0 1 * * ?'; 
        String jobID = system.schedule('EnterpriseRevenueMetricsSchedulableTest', sch, s);
        Test.stopTest();
        
    }
    
    private static Account generateAccount(){
        Id rtId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gympass_Entity').getRecordTypeId();
        Account acc = new Account();
        acc.name='AcademiaBrasilCompanyPai';
        acc.RecordTypeId = rtId;
        acc.GP_Status__c = 'Active';
        acc.billingState = 'Minas Gerais';
        acc.CAP_Value__c = 120;
        acc.BillingCity = 'CityAcademiaBrasil';
        acc.billingStreet = 'Rua academiabrasilpai';
        acc.billingCountry = 'Brazil';
        acc.Id_Company__c = '00.000.000/0001-74';
        acc.Gym_Type__c = 'Studios';
        acc.Gym_Classes__c = 'Cardio';
        acc.Types_of_ownership__c = Label.franchisePicklist;
        acc.Subscription_Type__c = 'Value per class';
        acc.Subscription_Period__c = 'Monthy value';
        acc.Subscription_Type_Estimated_Price__c    = 100;
        acc.Has_market_cannibalization__c = 'No';
        acc.Exclusivity__c = 'Yes';
        acc.Exclusivity_End_Date__c = Date.today().addYears(1);
        acc.Exclusivity_Partnership__c = 'Full Exclusivity';
        acc.Exclusivity_Restrictions__c= 'No';
        acc.Website = 'testing@tesapex.com';
        acc.Gym_Email__c = 'gymemail@apex.com';
        acc.Phone = '3222123123';
        acc.Can_use_logo__c = 'Yes';
        acc.Legal_Registration__c = 12123;
        acc.Legal_Title__c = 'Title LTDA';
        acc.Gyms_Identification_Document__c = 'CNPJ'; 
        acc.NumberOfEmployees = 1500;
        return acc;
    }
    
    private static void generateWaiver(){
        Waiver__c w = new Waiver__c();
        w.Start_Date__c = System.today();
        w.End_Date__c = System.today().addDays(1);
        w.Percentage__c = 20;
        w.RecordTypeId = Schema.SObjectType.Waiver__c.getRecordTypeInfosByDeveloperName().get('Fixed_Date').getRecordTypeId();
        insert w;
    }
    
    private static Opportunity generateOpportunity(Account acc, Pricebook2 pb){
        Id oppRtId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        Opportunity accOpp = new Opportunity();
        accOpp.recordTypeId = oppRtId;
        accOpp.AccountId = acc.id;
        accOpp.Name = acc.Id; 
        accOpp.CMS_Used__c = 'Yes';     
        accOpp.Gym_agreed_to_an_API_integration__c  = 'Yes';
        accOpp.Club_Management_System__c = 'Companhia Athletica';
        accOpp.Integration_Fee_Deduction__c = 'No';
        accOpp.CloseDate = Date.today();
        accOpp.Success_Look_Like__c = 'Yes';
        accOpp.Success_Look_Like_Description__c = 'Money money';
        accOpp.StageName = 'Proposta Enviada';
        accOpp.Type = 'Expansion';  
        accOpp.Country_Manager_Approval__c = true;
        accOpp.Payment_approved__c = true;   
        accOpp.CurrencyIsoCode = 'BRL';
        accOpp.Gympass_Plus__c = 'Yes';
        accOpp.Standard_Payment__c = 'Yes';
        accOpp.Request_for_self_checkin__c = 'Yes';  
        accOpp.Pricebook2Id = pb.Id;
        return accOpp;
    }
    
    private static Pricebook2 generatePricebook(){        
        Pricebook2 pb = new Pricebook2();
        pb.Name = 'Br';
        pb.Country__c = 'Brazil';
        pb.IsActive = true;
        return pb;
    }
    
    private static Product2 generateProduct(String ProductName){
        Product2 product = new Product2();
        product.Name = ProductName;
        product.Minimum_Number_of_Employees__c = 0;
        product.Maximum_Number_of_Employees__c = 900;
        product.Family = ProductName;
        product.IsActive = true;
        return product;
    }
    
    private static PricebookEntry generatePricebookEntry(Pricebook2 pb, Product2 product, Opportunity opp){        
        PricebookEntry pbEntry = new PricebookEntry();
        pbEntry.Product2Id = product.Id;
        pbEntry.Pricebook2Id = Test.getStandardPricebookId();
        pbEntry.UnitPrice = 100;
        pbEntry.IsActive = true;
        pbEntry.CurrencyIsoCode = opp.CurrencyIsoCode;
        return pbEntry;
    }
    
    private static Product2 generateProfServicesOneFeeProduct(){
        Product2 product = new Product2();
        product.Name = 'Professional Services Setup Fee';
        product.Minimum_Number_of_Employees__c = 0;
        product.Maximum_Number_of_Employees__c = 900;
        product.Family = 'Professional Services';
        product.IsActive = true;
        product.Family_Member_Included__c = true;
        return product;
    }
    
    private static Product2 generateProfServicesMainFeeProduct(){
        Product2 product = new Product2();
        product.Name = 'Professional Services Maintenance Fee';
        product.Minimum_Number_of_Employees__c = 0;
        product.Maximum_Number_of_Employees__c = 900;
        product.Family = 'Professional Services';
        product.IsActive = true;
        product.Family_Member_Included__c = true;
        return product;
    }
    
    private static Product2 generateAccessFeeFamilyMemberIncludedProduct(){
        Product2 product = new Product2();
        product.Name = 'Enteprise FM';
        product.Minimum_Number_of_Employees__c = 0;
        product.Maximum_Number_of_Employees__c = 900;
        product.Family_Member_Included__c = true;
        product.Family = 'Enterprise Subscription';
        product.IsActive = true;
        return product;
    }
    
}