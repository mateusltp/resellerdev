/**
 * @description       : 
 * @author            : roei@gft.com
 * @group             : 
 * @last modified on  : 01-29-2021
 * @last modified by  : roei@gft.com
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   01-29-2021   roei@gft.com   Initial Version
**/
@isTest(seeAllData=false)
public class AssociateCampaignNewOppTest {
    
	@TestSetup
    static void createData(){        
        Account lAcc = generateClientsAccount();              
        Database.insert( lAcc );
    	
        Contact lContact1 = new Contact(
        	FirstName = 'Contact',
            LastName = 'Test1',
            Email = 'contact1@test.com',
            AccountId = lAcc.Id	
        );
        
        Database.insert( lContact1 );
        
        Pricebook2 lStandardPricebook = new Pricebook2(
            Id = Test.getStandardPricebookId(),
            IsActive = true
        );
        Database.update( lStandardPricebook );
    }
    
    @isTest
    public static void shouldAssignCampaignToOpp() {
        Account lAcc = [ SELECT Id FROM Account LIMIT 1 ];
        
        Contact lContact2 = new Contact(
        	FirstName = 'Contact',
            LastName = 'Test2',
            Email = 'contact2@test.com',
            AccountId = lAcc.Id	
        );
        Database.insert( lContact2 );
        
        Campaign campaign = new Campaign();
        campaign.Name = 'Test';
        insert campaign;
        
        CampaignMember iCampMember = new CampaignMember(
            //LeadId = testLead2.get(i).Id,
            ContactId = lContact2.Id,
            CampaignId = campaign.Id, 
            Status = 'Sent'
        );
        Database.insert( iCampMember );
        
    	Opportunity lOpp = new Opportunity(
            RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId(),
            CurrencyIsoCode='BRL',
            Pricebook2Id = Test.getStandardPricebookId(),
            StageName='Qualification',
            Name='Opp Test',
            AccountId = lAcc.Id,
            CloseDate = Date.today()
        );
        
        Test.startTest();
        	Database.insert( lOpp );
        Test.stopTest();
        
        /*lOpp = [ SELECT CampaignId FROM Opportunity WHERE Id =: lOpp.Id ];
        
        CampaignMember lCampaignMember = [ SELECT CampaignId FROM CampaignMember LIMIT 1 ];
        
        System.assertEquals( lCampaignMember.CampaignId , lOpp.CampaignId );*/
    }
    
    private static Account generateClientsAccount(){
        ID lAccRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
        Account lAcc = new Account();
        lAcc.RecordTypeId = lAccRecordTypeId;
        lAcc.Name = 'Test Account';
        lAcc.BillingCountry = 'Brazil';
        lAcc.BillingState = 'São Paulo';
        lAcc.NumberOfEmployees = 2000;
        return lAcc;
    }
}