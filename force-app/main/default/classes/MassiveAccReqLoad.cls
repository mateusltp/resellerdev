public without sharing class MassiveAccReqLoad {
   

    public static void loadMassiveAccountRequest(Id massId){
        String InvalidBusinessModel = System.label.Community_Invalid_Business_Model; //'Invalid Business Model';
        String InvalidCountry = System.label.Community_Invalid_Company_Name; //'Invalid Country';

        List<Account_Request__c> newAccountRequest_list = new List<Account_Request__c>();
        List<ContentVersion> contVers = new List<ContentVersion>();
        Map<String, String> partnerModel_map = new Map<String,String>{'intermediação'=>'intermediation','subsídio'=>'subsidy','intermediation'=>'intermediation','subsidy'=>'subsidy'};
        Map<String, String> countries_map = new Map<String,String>{'alemanha'=>'germany','brasil'=>'brazil','espanha'=>'spain','estadosunidos'=>'united states','reinounido'=>'united kingdom','germany'=>'germany','brazil'=>'brazil','spain'=>'spain','unitedstates'=>'united states','unitedkingdom'=>'united kingdom'};

        UserSingleton currentUser = UserSingleton.getInstance(UserInfo.getUserId());
        MassiveAccountRequestSingleton massive = MassiveAccountRequestSingleton.getInstance(massId);
        Boolean hasPermission = FeatureManagement.checkPermission('Reseller_Admin');

        Massive_Account_Request__c massReq = [
            SELECT Id, Name, OwnerId, Status__c, Companies__c, Reseller__c, Reseller__r.OwnerId, Reseller__r.Indirect_Channel_Pricebook__c FROM Massive_Account_Request__c WHERE Id =: massId
            LIMIT 1
        ];
        
        List<ContentDocumentLink> contLink = [
            SELECT Id, ContentDocumentId, LinkedEntityId, ContentDocument.Title, LinkedEntity.Name
            FROM ContentDocumentLink WHERE LinkedEntityId =: massId
            LIMIT 1
        ];

        if(contLink.size() > 0){
            contVers = [
                Select Id, VersionData 
                FROM ContentVersion 
                WHERE ContentDocumentId =: contLink[0].ContentDocumentId
            ];
        }
        try{
            Blob csvFileBody = contVers[0].VersionData;
            String csvAsString = csvFileBody.toString();
            List<String> csvFileLines = csvAsString.split('\n');
            System.debug('Linhas no csv:' + csvFileLines.size());

            if(csvFileLines.size() > 2){
                for(Integer i=1; i < csvFileLines.size(); i++){
                    List<String> csvRecordData = csvFileLines[i].split(csvAsString.contains(';') ? ';' : ',');
                    if(!validateLine(csvRecordData))continue;
                    
                    Account_Request__c newAccountRequest = new Account_Request__c();
                    String partnerModel = MassiveAccReqLoad.isFilled(String.valueOf(csvRecordData[2])) ? String.valueOf(csvRecordData[2]).toLowerCase() : '';
                    String country = MassiveAccReqLoad.isFilled(String.valueOf(csvRecordData[4])) ? String.valueOf(csvRecordData[4]).toLowerCase() : '';
                    String numberEmployees = MassiveAccReqLoad.isFilled(String.valueOf(csvRecordData[3])) ? String.valueOf(csvRecordData[3]) : '';
                    String name = MassiveAccReqLoad.isFilled(String.valueOf(csvRecordData[0])) ? String.valueOf(csvRecordData[0]) : 'ㅤ';
                    Integer maxSizeName = 80;

                    if(name.length() > maxSizeName ){
                        newAccountRequest.Name =  name.substring(0, maxSizeName);
                        newAccountRequest.Error_message__c = 'Invalid Company Name 80';
                    
                    }else{ newAccountRequest.Name = name;}
                    //newAccountRequest.Name = MassiveAccReqLoad.isFilled(String.valueOf(csvRecordData[0])) ? String.valueOf(csvRecordData[0]) : 'ㅤ';
                    newAccountRequest.Total_number_of_employees__c = MassiveAccReqLoad.isFilled(String.valueOf(numberEmployees)) ? Integer.valueOf(numberEmployees) : 0;
                    newAccountRequest.Unique_Identifier__c = MassiveAccReqLoad.isCountryBrazil(currentUser.getCountryCode()) ? String.valueOf(csvRecordData[1]) : null;
                    newAccountRequest.Unique_Identifier_Type__c = MassiveAccReqLoad.isCountryBrazil(currentUser.getCountryCode()) ? ResellerEnumRepository.UniqueIdentifierType.CNPJ.name() : null;
                    newAccountRequest.Website__c = !MassiveAccReqLoad.isCountryBrazil(currentUser.getCountryCode()) ? String.valueOf(csvRecordData[1]) : null;
                    newAccountRequest.OwnerId = massive.getOwnerId();
                    newAccountRequest.Massive_Account_Request__c = massId;
                    newAccountRequest.Bulk_Operation__c = true;
                    newAccountRequest.Partner_Model__c = partnerModel_map.containsKey(partnerModel.replaceAll('(\\s+)', '')) ? partnerModel_map.get(partnerModel.replaceAll('(\\s+)', '')) : '';
                    newAccountRequest.Billing_Country__c = countries_map.containsKey(country.replaceAll('(\\s+)', '')) ? countries_map.get(country.replaceAll('(\\s+)', '')) : '';
                    newAccountRequest.Partner_Account__c = massReq.Reseller__c;
                    newAccountRequest.Partner_Account_Owner__c = massReq.Reseller__r.OwnerId;
                    newAccountRequest.Indirect_Channel_Pricebook__c = massReq.Reseller__r.Indirect_Channel_Pricebook__c;
                    newAccountRequest.Massive_Status__c = ResellerEnumRepository.MassiveAccountRequestStatus.LookingForAccounts.name();
                    newAccountRequest.Engine_Log__c += ' -|- ' + ResellerEnumRepository.MassiveLog.UPLOADED_NEW_ACCOUNT_REQUEST.name() + ' ' + String.valueOf(System.now());
                    newAccountRequest_list.add(newAccountRequest);
                    
                }
            }
            
            insert newAccountRequest_list;
        }
        catch(Exception ex){
            system.debug('Exception => ' + ex);
            new SendCustomNotification().sendNotificationError(massReq);
            massive.setMassiveLogError(ex.getMessage());

        }
        massive.setStatus(ResellerEnumRepository.MassiveAccountRequestStatus.LookingForAccounts.name());
        Integer countAccRequest = [SELECT COUNT() FROM Account_Request__c WHERE Massive_Account_Request__c = :massId];
		massive.setCompanies(String.valueOf(countAccRequest));
        massive.dataUpdate();

        LookingAccountsBatch bt = new LookingAccountsBatch();
        bt.massiveId = massId;
        ID idAcc = Database.executeBatch(bt);
    }

    private static Boolean isCountryBrazil(String currentUserCountryCode){
        if(currentUserCountryCode == ResellerEnumRepository.CountryCode.BR.name())return true;
        else return false;
    }

    private static Boolean isFilled(String name){
        if(name != '')return true;
        else return false;
    }

    private static Boolean validateLine(List<String> lineFields){
        Boolean validated = false;
        if(lineFields.isEmpty())return false;

        for(String item : lineFields)
            if(String.isNotBlank(item))validated = true;

        return validated;
    }


    @AuraEnabled
    public static void runBatchesAgain(Id massId){

        List<Account_Request__c> accReqs = new List<Account_Request__c>();
        accReqs =[SELECT Id, Name, Massive_Status__c FROM Account_Request__c WHERE Massive_Account_Request__c =: massId and Engine_Status__c = 'Invalid'];
        
        if (accReqs.size() > 0){
            List<Account_Request__c> aReq = new List<Account_Request__c>();
            for(Account_Request__c accReq : accReqs){
                if(accReq.Name.length() > 1)accReq.Name = accReq.Name.replaceAll('ㅤ', '');
                //Account_Request__c accReq = new Account_Request__c();
                
                accReq.Massive_Status__c = ResellerEnumRepository.MassiveAccountRequestStatus.LookingForAccounts.name();
                accReq.Engine_Status__c = ResellerEnumRepository.AccountRequestStatus.Analyze.name();
                aReq.add(accReq);
                System.debug('Id account: '+ accReq.Id + 'status: '+ accReq.Massive_Status__c);
            }
            update aReq;

            Massive_Account_Request__c massAccReq = [
                SELECT Id, Status__c FROM Massive_Account_Request__c WHERE Id =: massId
                LIMIT 1
            ];
            massAccReq.Status__c = ResellerEnumRepository.MassiveAccountRequestStatus.LookingForAccounts.name();
            update massAccReq;
            System.debug('Id massive: '+ massAccReq.Id + ' status: '+ massAccReq.Status__c);

            LookingAccountsBatch bt = new LookingAccountsBatch();
            bt.massiveId = massId;
            ID idAcc = Database.executeBatch(bt);

        }else System.debug('List null account Request');
    }

    @AuraEnabled
    public static Integer quantityMassiveAccountRequest(Id massId){
        List<Account_Request__c> accReqs = new List<Account_Request__c>();
        List<ContentVersion> contVers = new List<ContentVersion>();
        
        List<ContentDocumentLink> contLink = [
            SELECT Id, ContentDocumentId, LinkedEntityId, ContentDocument.Title, LinkedEntity.Name
            FROM ContentDocumentLink WHERE LinkedEntityId =: massId
            LIMIT 1
        ];

        if(contLink.size() > 0){
            contVers = [
                Select Id, VersionData 
                FROM ContentVersion 
                WHERE ContentDocumentId =: contLink[0].ContentDocumentId
            ];
        }
        
        Blob csvFileBody = contVers[0].VersionData;
        String csvAsString = csvFileBody.toString();
        List<String> csvFileLines = csvAsString.split('\n');

        return csvFileLines.size();
    }
    
}