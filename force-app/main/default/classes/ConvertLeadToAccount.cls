/**
*
* @File Name          : ConvertLeadToAccount.cls
* @Description        :
* @Author             : GEPI@GFT.com
* @Group              :
* @Last Modified By   : Jorge Stevaux - JZRX@gft.com
* @Last Modified On   : 12-08-2020
* @Modification Log   :
* Ver       Date            Author                  Modification
* 1.0    29/06/2020   BOBA@GFT.com     Initial Version
**/
public class ConvertLeadToAccount{
    List<Lead> newLeads = trigger.new;
    Map<Id, Lead> leadIdMap = (Map<Id,Lead>)trigger.newMap;
    Map<Id, Lead> leadIdOldMap = (Map<Id,Lead>)trigger.oldMap;
    Map<Id, Account> mapLeadAccount = new Map<Id,Account>();
    Map<Id, Account> mapLeadExistingAccount = new Map<Id,Account>();
    Map<Id, Contact> mapLeadIdContact = new Map<Id, Contact>();
    
    Id rtIdOwnerReferral = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Partner_Owner_Referral_Lead').getRecordTypeId();
    Id rtIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gym_Partner').getRecordTypeId();
    Id rtIdContact = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Gyms_Partner').getRecordTypeId();
    List<Account> newAccts = new List<Account>();
    List<Lead> updLeads = new List<Lead>();
    
    public void addLeadAccount(){  
        System.debug('Class ConvertLeadToAccount - Entrou AddLeadAccount ');     
        for (Lead l : newLeads){
            if(l.recordTypeId == rtIdOwnerReferral && leadIdOldMap.get(l.id).Status != leadIdMap.get(l.id).Status  &&   l.Status == 'Convertido'){
                Set<Id> mrObjLst = DuplicateHandler.foundLeadDuplicateAccount(l);
                system.debug('mrObjLst apos Set<Id> mrObjLst =======> ' + mrObjLst.size());				
                if (mrObjLst.isempty()){
                    System.debug('Class ConvertLeadToAccount - Entrou no if do mrObjLst se está vazio e chama createAccount()');
                    system.debug('Class ConvertLeadToAccount - mrObjLst - addLeadAccount() Qtde --->' + mrObjLst.size());
                    createAccount(l);
                }
                if (!mrObjLst.isempty()){
                    Id accountId = new List<Id> (mrObjLst).get(0); //returns the first element (unique account)
                    mapLeadExistingAccount.put(l.id, new Account(Id=accountId));
                }
            } 
            
        }
        
        
        if (!mapLeadExistingAccount.isempty()){
            system.debug('Entrou no if que verifica se mapLeadExistingAccount não está vazio e Chamou updateReferralAccountInLeads()');
            updateLeads();
        }
        if (!mapLeadAccount.isempty()){
            system.debug('Entrou no if que verifica se mapLeadAccount não está vazio e Chamou insertAccounts() e updateReferralAccountInLeads()');
            insertAccounts();
            updateReferralAccountInLeads();
        }
        if (!mapLeadIdContact.isempty()){
            system.debug('Entrou no if que verifica se mapLeadContact não está vazio e Chamou insertContact() ');
            insertContacts();
        }
    }
    
    private void insertObjects(List<SObject> objs){
        Database.SaveResult[] results = Database.insert(objs, false);
        for (Database.SaveResult r : results){
            if (r.isSuccess()){
                System.debug('Class ConvertLeadToAccount - Successfully inserted Object. Object ID: ' + r.getId());
            } else{
                for (Database.Error err : r.getErrors()){
                }
            }
        }
    }
    
    private void createAccount(Lead l){
        System.debug('Class ConvertLeadToAccount - Entrou no createAccount() ');
        Account acct = new Account();
        acct.RecordTypeId = rtIdAccount;
        acct.ParentId = l.Parent_Account_Related__c;
        acct.Name = l.Company;
        acct.phone = l.Phone;
        acct.Email__c = l.Email;
        acct.ShippingStreet = l.Street;
        acct.ShippingCity = l.City;
        acct.ShippingStateCode = l.StateCode;
        acct.ShippingCountryCode = l.CountryCode;
        acct.Referral_Score__c = l.Number_of_Referrals__c;
        acct.AccountSource = l.LeadSource;
        acct.Types_of_ownership__c = l.Types_of_ownership__c;
        
        mapLeadAccount.put(l.id, acct);	
        System.debug('createAccount() - mapLeadAccount =========> ' + mapLeadAccount.values());
        if(l.Is_User_Owner__c == 'Yes'){
            System.debug('Chamou createContact() ');
            createContact(l,acct);
        } System.Debug('createAccount()--> Account Created -----> ' + acct);
    }
    private void createContact(Lead l, account acct){
        System.debug('Entrou no createContact() ');
        
        Contact ctt = new Contact();
        ctt.RecordTypeId = rtIdContact;
        ctt.lastName = l.GP_Referral_Name__c;
        ctt.phone = l.Referral_Phone__c;
        ctt.Email = l.GP_Referral_Email__c;
        mapLeadIdContact.put(l.id, ctt);
        
        System.debug('createContact()---> Contact Created ------> ' + ctt);
    }	
    
    private void updateReferralAccountInLeads(){
        System.debug('Entrou no metodo updateReferralAccountInLeads ');  
        List<Lead> leadDupLst = new List<Lead>();
        for(Id lId : mapLeadAccount.keySet()) {  
            Lead l = leadIdMap.get(lId);
            System.debug('updateReferralAccountInLeads - Lead l recebendo map de leads em updateReferralAccountInLeads -------> ' + l);
            System.debug('=====================Lead l => Types of Ownership=================== ' + l.Types_of_ownership__c );
            Set<ID> dupIds = DuplicateHandler.foundLeadDuplicateLeads(l); 
            System.debug('updateReferralAccountInLeads - DupIds in updateReferralAccountInLeads ----------> ' + dupIds);
            System.debug('updateReferralAccountInLeads - DupIds in updateReferralAccountInLeads Qtd ------> ' + dupIds.size());
            System.debug('updateReferralAccountInLeads - lId Before dupIds.remove() ----------> ' + lId);
            dupIds.remove(lId);
            if(!dupIds.isEmpty()){
                for(Id idDup : dupIds) {
                    System.debug('Entrou no For que popula a leadDupLst ');                   
                    leadDupLst.add(new Lead(id=idDup, Referral_Account__c = mapLeadAccount.get(lId).Id));
                    System.debug('Tamanho leadDupLst ============== >  ' + leadDupLst.size());
                    System.debug('Conteudo leadDupLst ============= >  ' + leadDupLst);
                }	        
            }
            for(lead ld : leadDupLst){
                ld.Types_of_ownership__c = l.Types_of_ownership__c;
            }
            System.debug('updateReferralAccountInLeads - Lista de Lead com Referral Account ------>' + leadDupLst);
            System.debug('updateReferralAccountInLeads - Lista de Lead com Referral Account Qtd------>' + leadDupLst.size()); 
            l.Referral_Account__c = mapLeadAccount.get(lId).Id;
            System.debug('updateReferralAccountInLeads - Lead Referral Account ---------> ' + l.Referral_Account__c);
            System.debug('updateReferralAccountInLeads - Lead Id --> ' + l.Id + '===== Lead Status --> ' + l.Status);
            System.debug('updateReferralAccountInLeads - Lead Type of Ownership --> ' + l.Types_of_ownership__c);
        }
        if(!leadDupLst.isEmpty()){
            System.debug('leadDupLst Não Está Vazia Entou no if  ');
            System.debug('updateReferralAccountInLeads - Qtde de Update updateReferralAccountInLeads() --> ' + leadDupLst.size());
            updateLeadsStatus(leadDupLst);
        }     
    }
    
    private void updateLeads(){
        System.debug('Entrou no metodo updateLeads ');  
        List<Lead> leadDupLst = new List<Lead>();
        for(Id lId : mapLeadExistingAccount.keySet()) {  
            Lead l = leadIdMap.get(lId);
            System.debug('updateLeads - Lead l recebendo map de leads em updateLeads -------> ' + l);
            System.debug('=====================Lead l => Types of Ownership=================== ' + l.Types_of_ownership__c );
            Set<ID> dupIds = DuplicateHandler.foundLeadDuplicateLeads(l); 
            System.debug('updateLeads - DupIds in updateLeads ----------> ' + dupIds);
            System.debug('updateLeads - DupIds in updateLeads Qtd ------> ' + dupIds.size());
            System.debug('updateLeads - lId Before dupIds.remove() ----------> ' + lId);
            dupIds.remove(lId);
            if(!dupIds.isEmpty()){
                for(Id idDup : dupIds) {
                    System.debug('Entrou no For que popula a leadDupLst ');                   
                    leadDupLst.add(new Lead(id=idDup, Referral_Account__c = mapLeadExistingAccount.get(lId).Id));
                    System.debug('Tamanho leadDupLst ============== >  ' + leadDupLst.size());
                    System.debug('Conteudo leadDupLst ============= >  ' + leadDupLst);
                }	        
            }
            for(lead ld : leadDupLst){
                ld.Types_of_ownership__c = l.Types_of_ownership__c;
            }
            System.debug('updateLeads - Lista de Lead com Referral Account ------>' + leadDupLst);
            System.debug('updateLeads - Lista de Lead com Referral Account Qtd------>' + leadDupLst.size()); 
            l.Referral_Account__c = mapLeadExistingAccount.get(lId).Id;
            System.debug('updateLeads - Lead Referral Account ---------> ' + l.Referral_Account__c);
            System.debug('updateLeads - Lead Id --> ' + l.Id + '===== Lead Status --> ' + l.Status);
            System.debug('updateLeads - Lead Type of Ownership --> ' + l.Types_of_ownership__c);
        }
        if(!leadDupLst.isEmpty()){
            System.debug('leadDupLst Não Está Vazia Entou no if  ');
            updateLeadsStatusWithAccount(leadDupLst);
        }     
    }
    
    private void updateLeadsStatus(List<Lead> leadDupLst){
        system.debug('Entrou no updateLeadsStatus');
        List<Lead> lstLeadtoUpdate = new List<Lead>();
        List<lead> lstUpStatus = [SELECT Id, Status ,Types_of_ownership__c , RecordTypeId FROM Lead WHERE Id IN:leadDupLst];
        system.debug('lstUpStatus ############## ' + lstUpStatus);
        for(Lead ld : lstUpStatus){
            if(ld.recordTypeId == rtIdOwnerReferral && ld.Status != 'Convertido'){
                ld.Status = 'Convertido';	
                lstLeadtoUpdate.add(ld);	
            }
        }
        
        System.debug('lstUpStatus Qtd ==========> ' + lstUpStatus.size());
        Database.SaveResult[] srList = Database.update(lstLeadtoUpdate, false);//Database method to update the records in List
        
        // Iterate through each returned result by the method
        for (Database.SaveResult sr : srList) {
            if (sr.isSuccess()) {
                 //This condition will be executed for successful records and will fetch the ids of successful records
                System.debug('Successfully updated Lead. Lead ID is : ' + sr.getId());
            }
            else {
                 //This condition will be executed for failed records
                for(Database.Error objErr : sr.getErrors()) {
                    System.debug('The following error has occurred.');//Printing error message in Debug log
                    System.debug(objErr.getStatusCode() + ': ' + objErr.getMessage());
                    System.debug('Lead oject field which are affected by the error: ' + objErr.getFields());
                }
            }
        }
    }
    
    private void updateLeadsStatusWithAccount(List<Lead> leadDupLst){
        system.debug('Entrou no updateLeadsStatusWithAccount');
        List<Lead> lstLeadtoUpdate = new List<Lead>();
        List<lead> lstUpStatus = [SELECT Id, Status ,Types_of_ownership__c , RecordTypeId FROM Lead WHERE Id IN:leadDupLst];
        system.debug('lstUpStatus ############## ' + lstUpStatus);
        for(Lead ld : lstUpStatus){
            if(ld.recordTypeId == rtIdOwnerReferral && ld.Status != 'Convertido'){
                ld.Status = 'Convertido';
                lstLeadtoUpdate.add(ld);
            }
        }
        System.debug('lstUpStatus&& Qtd ==========> ' + lstUpStatus.size());
        System.debug('lstUpStatus&&  ==========> ' + lstUpStatus);
        
        Database.SaveResult[] srList = Database.update(lstLeadtoUpdate, false);//Database method to update the records in List
        
        // Iterate through each returned result by the method
        for (Database.SaveResult sr : srList) {
            if (sr.isSuccess()) {
                 //This condition will be executed for successful records and will fetch the ids of successful records
                System.debug('Successfully updated Lead. Lead ID is : ' + sr.getId());
            }
            else {
                 //This condition will be executed for failed records
                for(Database.Error objErr : sr.getErrors()) {
                    System.debug('The following error has occurred.');//Printing error message in Debug log
                    System.debug(objErr.getStatusCode() + ': ' + objErr.getMessage());
                    System.debug('Lead oject field which are affected by the error: ' + objErr.getFields());
                }
            }
        }
        
    }
    
    private void insertAccounts(){
        System.debug('Entrou no insertAccounts() ');	
        insertObjects(mapLeadAccount.values());
        System.debug('insertAccounts()--> MapLeadAccount ----->' + mapLeadAccount.values());
        System.debug('insertAccounts()--> MapLeadAccount  Qtd ----->' + mapLeadAccount.size());
    }
    
    private void insertContacts(){
        System.debug('Entrou no  insertContacts ');
        for(Id lId : mapLeadIdContact.keySet() ) {
            Account acc = mapLeadAccount.get(lId);
            Contact c = mapLeadIdContact.get(lId);
            c.AccountId = acc.Id;
        }
        insertObjects((mapLeadIdContact.values()));
        System.debug('insertContacts()--> MapLeadContact -----> ' + mapLeadIdContact.values());
        System.debug('insertContacts()--> MapLeadContact Qtd -----> ' + mapLeadIdContact.size());
    }
    
    public class IntegrityError{
        public IntegrityError(){}
        public void doAction(String msg){
            System.debug('Entrou no IntegityError - doAction');
            SObject[] sobjects = Trigger.new;
            for (Sobject sobj : sobjects){
                system.debug(sobj);
                sobj.addError(msg);
            }
        }
    }
}