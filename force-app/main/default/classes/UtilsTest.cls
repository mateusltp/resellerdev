/**
 * @description       : 
 * @author            : Samuel Silva - GFT (slml@gft.com)
 * @group             : 
 * @last modified on  : 02-18-2021
 * @last modified by  : Samuel Silva - GFT (slml@gft.com)
 * Modifications Log 
 * Ver   Date         Author                              Modification
 * 1.0   09-24-2020   Samuel Silva - GFT (slml@gft.com)   Initial Version
**/
@isTest//(SeeAllData=true)
public class UtilsTest
{
	static void SetUp(){
		test.StartTest();
	}

    static void TearDown(){
		test.StopTest();
	}
	
	public static testmethod void Test_getQueryWithAllFieldsFromObject(){
		SetUp();
		string objectname = 'Account';
        Utils.getQueryWithAllFieldsFromObject(objectname);
		TearDown();
	}

	public static testmethod void Test_getQueryWithSomeFieldsFromObject(){
		SetUp();
		string objectname = 'Account';
		List<string> customfields = new List<string>();
        Utils.getQueryWithSomeFieldsFromObject(objectname, customfields);
		TearDown();
	}

	public static testmethod void Test_getSetWithAllFieldsFromObject(){
		SetUp();
		string objectname = 'Account';
        Utils.getSetWithAllFieldsFromObject(objectname);
		TearDown();
	}
    
	public static testmethod void Test_getMapWithValidFieldsForCSV(){
		SetUp();
		string objname = 'Account';
        Utils.getMapWithValidFieldsForCSV(objname);
		TearDown();
	}
    
    public static testmethod void Test_getSetWithAllFieldsFromObjectEvent() {
        SetUp();
		string objectname = 'Event';
		List<string> customfields = new List<string>();
        Utils.getQueryWithSomeFieldsFromObject(objectname, customfields);
		TearDown();
    }
    
     public static testmethod void Test_doHTTPCalloutForIntegtrationRequestException() {
        SetUp(); 
        Integration_Request__c request = new Integration_Request__c();
        request.HTTP_Request_Method__c = 'POST';
        request.Endpoint__c = 'http://test.com';
        request.Payload__c = '{"teste" : "teste"}';
        request.Integration_Request_Status__c = 'Pending Integration';
        
        insert request;
        
        HTTPMockFactory mock = new HttpMockFactory(
            new UtilsTest(), 
            500, 
            'ERROR', 
            'Internal Server Error!', 
            new Map<String,String>()
        );
        
        Test.setMock(HttpCalloutMock.class, mock);
         
        Utils.doHTTPCallout(request);
		TearDown();
    }
    
     public static testmethod void Test_doHTTPCalloutForIntegtrationRequestSuccess() {
        SetUp(); 
        Integration_Request__c request = new Integration_Request__c();
        request.HTTP_Request_Method__c = 'POST';
        request.Endpoint__c = 'http://test.com';
        request.Payload__c = '{"teste" : "teste"}';
        request.Integration_Request_Status__c = 'Pending Integration';
        
        insert request;
        
        HTTPMockFactory mock = new HttpMockFactory(
            new UtilsTest(), 
            200, 
            'Success', 
            'I found it!', 
            new Map<String,String>()
        );
        
        Test.setMock(HttpCalloutMock.class, mock);
         
        Utils.doHTTPCallout(request);
		TearDown();
    }
    
    public static testmethod void Test_doHTTPCallout() {
        SetUp(); 
        Utils.CalloutWrapper wrapper = new Utils.CalloutWrapper();
        wrapper.endpoint = 'http://teste.com';
        wrapper.body = '{teste: teste}';
        wrapper.headerParams = new Map<String, String>{'Teste' => 'Teste'};
        wrapper.method = 'POST';
        
        HTTPMockFactory mock = new HttpMockFactory(
            new UtilsTest(), 
            200, 
            'Success', 
            'I found it!', 
            new Map<String,String>()
        );
        
        Test.setMock(HttpCalloutMock.class, mock);
         
        Utils.doHTTPCallout(wrapper);
		TearDown();
    }
    
    public static testmethod void Test_doHTTPCalloutWithoutBody() {
        SetUp(); 
        Utils.CalloutWrapper wrapper = new Utils.CalloutWrapper();
        wrapper.endpoint = 'http://teste.com';
        wrapper.headerParams = new Map<String, String>{'Teste' => 'Teste'};
        wrapper.method = 'POST';
        
        HTTPMockFactory mock = new HttpMockFactory(
            new UtilsTest(), 
            200, 
            'Success', 
            'I found it!', 
            new Map<String,String>()
        );
        
        Test.setMock(HttpCalloutMock.class, mock);
         
        Utils.doHTTPCalloutWithoutBody(wrapper);
		TearDown();
    }

    public static testmethod void Test_getRegionByCountry(){
		SetUp();
		string countryName = 'Brazil';
        Utils.getRegionByCountry(countryName);
		TearDown();
	}
    
    @isTest
    public static void Test_getMapContryCodeRegionFromMdt(){
        SetUp();
        Map< String , String > lMapCountryCodeRegion = Utils.getMapContryCodeRegionFromMdt();
        TearDown();
    	List< RegionByCountry__mdt > lLstRegionByCountry = [ SELECT MasterLabel, Region__c, Country_Code__c FROM RegionByCountry__mdt ];
        
        System.assert( !lMapCountryCodeRegion.isEmpty() );
        System.assertEquals( lLstRegionByCountry.size() , lMapCountryCodeRegion.size() );
    }

      
    @isTest
    public static void Test_getCountryCodeByCountry(){
        SetUp();
            Map< String , String > lMapCountryCode = Utils.getCountryCodeByCountry();
        TearDown();    	
        System.assert( !lMapCountryCode.isEmpty() );
        System.assertEquals( 'IE', lMapCountryCode.get('Ireland'));
    }


          
    @isTest
    public static void Test_capitalizeFully(){
        SetUp();
            String usa = Utils.capitalizeFully('uNited sTates');
        TearDown();    	
        System.assertEquals(true, usa.equals('United States'));
    }
}