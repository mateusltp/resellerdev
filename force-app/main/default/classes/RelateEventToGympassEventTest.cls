/*
* @Author: Bruno Pinho
* @Date: January/2019
* @Description: Test class for RelateEventToGympassEvent
*/
@isTest(SeeAllData=true)
private class RelateEventToGympassEventTest
{
    static void SetUp()
    {
        test.StartTest();
    }
    
    static void TearDown()
    {
        test.StopTest();
    }
    
    public static testmethod void Test_EventToGympassEvent()
    {
        SetUp();
        List<Id> gympasseventsid = new List<Id>();
        List<Event> listEventsToInsert = new List<Event>();
        List<Gympass_Event__c> listGympassEventsToInsert = new List<Gympass_Event__c>();
        List<Gympass_Leader_Association__c> listGympassLeaderAssociationsToInsert = new List<Gympass_Leader_Association__c>();
        
        for (Integer i=0; i<10; i++)
        {
            Event event = new Event(Subject = 'Test Event', EndDateTime = System.now().addDays(2), StartDateTime = System.now().addDays(1));
            listEventsToInsert.add(event);
        }
        
        INSERT listEventsToInsert;
        
        for (Event e: listEventsToInsert)
        {
            Gympass_Event__c gpe = new Gympass_Event__c();
            gpe.Name = e.Subject;
            gpe.Start__c = e.StartDateTime;
            gpe.End__c = e.EndDateTime;
            gpe.Event_Id__c = e.Id;
            listGympassEventsToInsert.add(gpe);
            gympasseventsid.add(e.Id);
        }
        
        INSERT listGympassEventsToInsert;
        
        for (Gympass_Event__c gpe: listGympassEventsToInsert)
        {
            Gympass_Leader_Association__c gpla = new Gympass_Leader_Association__c();
            gpla.Gympass_Event__c = gpe.Id;
            gpla.Gympass_Leader__c = UserInfo.getUserId();
            listGympassLeaderAssociationsToInsert.add(gpla);
        }
        
        INSERT listGympassLeaderAssociationsToInsert;
        
        RelateEventToGympassEvent.EventToGympassEvent(gympasseventsid);
        TearDown();
    }
}