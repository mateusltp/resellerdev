/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 05-12-2021
 * @last modified by  : gft.samuel.silva@ext.gympass.com
 * Modifications Log 
 * Ver   Date         Author                             Modification
 * 1.0   05-11-2021   gft.samuel.silva@ext.gympass.com   Initial Version
**/
global without sharing class EngagementJourneyStageBatch implements Database.Batchable<sObject>, Schedulable {
    //Seconds Minutes Hours Day_of_month Month Day_of_week Optional_year
    private static String gSchedTime = '0 0 0 * * ?';  //Every Day at Midnight 
    private static final Id gAccountRecTypeId = 
    Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();

    global EngagementJourneyStageBatch() { }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        String q = 'SELECT Id,Proposal_End_Date__c,Engagement_Journey_Stage__c,'+
                     'M2_Launch_Period_Expiration_Date__c,M3_Post_Launch_Period_Expiration_Date__c,'+
                     'M4_Nurturing_Period_Expiration_Date__c FROM Account '+ 
                     'WHERE RecordTypeId =: gAccountRecTypeId AND ' +
                     '((Engagement_Journey_Stage__c =\'M2\' AND M2_Launch_Period_Expiration_Date__c < TODAY) OR '+
                     '(Engagement_Journey_Stage__c = \'M3\' AND M3_Post_Launch_Period_Expiration_Date__c < TODAY) OR '+ 
                     '(Engagement_Journey_Stage__c = \'M4\' AND M4_Nurturing_Period_Expiration_Date__c < TODAY))';
        
        return Database.getQueryLocator(q);   
    }

    global void execute(Database.BatchableContext BC, List<Account> accScope) {       
        List<Account> accToUpdate;
        for(Account acc : accScope){    
            switch on acc.Engagement_Journey_Stage__c{
                when 'M2' {
                    acc.Engagement_Journey_Stage__c = 'M3';
                    acc.M3_Post_Launch_Period_Expiration_Date__c = acc.M2_Launch_Period_Expiration_Date__c+30;
                }
                when 'M3' {
                    acc.Engagement_Journey_Stage__c = 'M4';
                    acc.M4_Nurturing_Period_Expiration_Date__c = acc.Proposal_End_Date__c-90;
                }
                when 'M4' {                    
                    acc.Engagement_Journey_Stage__c = 'M5';
                }
            }
        }
        updateAccounts(accScope);       
    }

    global void execute(SchedulableContext SC) {
        Database.executebatch(new EngagementJourneyStageBatch());
    }

 
    global void finish(Database.BatchableContext BC) {}   

    global static void start(String schedTime){
        gSchedTime = schedTime == null || String.isBlank(schedTime) ?  '0 0 0 * * ?' : schedTime;      
        String jobName =  Test.isRunningTest() ? 'Engagement Journey Stage Test' : 'Engagement Journey Stage';
	    System.Schedule(jobName, gSchedTime, new EngagementJourneyStageBatch());
    }
    
    private void updateAccounts(List<Account> accLst){
        Database.SaveResult[] results = Database.update(accLst, false);
        for (Database.SaveResult sr : results) {
            if (sr.isSuccess()) {
                System.debug('Successfully updated account. Account ID: ' + sr.getId());
            }
            else {            
                for(Database.Error err : sr.getErrors()) {
                    System.debug('The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('Account fields that affected this error: ' + err.getFields());
                }
            }
        }
    }
}