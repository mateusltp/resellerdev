/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 12-15-2021
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/
@IsTest

public with sharing class NetworkBuilderCreateNewAccControllerTest {
 
    @IsTest
    private static void getContactsFromParent_Test(){
        //1. create fake data that you want to use
        Id mockAccountId = fflib_IDGenerator.generate(Account.SObjectType);
        Id mockContactId = fflib_IDGenerator.generate(Contact.SObjectType);
        Id mockAccountContactRelationId = fflib_IDGenerator.generate(AccountContactRelation.SObjectType);

        Account mockAccount = new Account(  Id=mockAccountId, CurrencyIsoCode='BRL', BillingStreet='Test', BillingPostalCode='123',
                                            BillingCountryCode='BR', BillingState='SP', BillingCity='Sorocaba',
                                            Legal_Document_Type__c='CNPJ', Id_Company__c = '1234', Legal_Title__c='TEST GYM',
                                            CRM_ID__c = '1231', Name='TEst Gym Service', Gym_Type__c='Full Service');


        Contact mockContact = new Contact(  Id = mockContactId, FirstName='Admin', LastName='Admin', 
                                            Type_of_contact__c ='Admin',Phone='+5599999999999', Email='teste@ddddd.com');

        AccountContactRelation mockAccountContactRelation = new AccountContactRelation(AccountId = mockAccountId, ContactId = mockContactId);

        List<AccountContactRelation> mockContactLst = new List<AccountContactRelation>{mockAccountContactRelation};
        //2. make fake mock instances you your class
        fflib_ApexMocks mocks = new fflib_ApexMocks();
        fflib_SObjectUnitOfWork mockUOW = (fflib_SObjectUnitOfWork) mocks.mock(fflib_SObjectUnitOfWork.class);
        AccountContactRelationSelector mockAccountContactRelationSelector = (AccountContactRelationSelector)mocks.mock(AccountContactRelationSelector.class);
        //3. make fake returns for your classes methods you are calling
        mocks.startStubbing();
            mocks.when(mockAccountContactRelationSelector.sObjectType()).thenReturn(AccountContactRelation.SObjectType);    
            mocks.when(mockAccountContactRelationSelector.selectContactRelationById(new Set<Id>{mockAccountId})).thenReturn(mockContactLst);    
              
        mocks.stopStubbing();

        //4. set the mocks of your application
        Application.UnitOfWork.setMock(mockUOW);
        Application.Selector.setMock(mockAccountContactRelationSelector);
        Test.startTest();
            List<AccountContactRelation> contactLstResult = NetworkBuilderCreateNewAccountController.getContactsFromParent(mockAccountId);
            system.assert(!contactLstResult.isEmpty());
        Test.stopTest();
    }


    @IsTest
    private static void getRecordTypeId_Test(){

        Id recordTypeIdPartner = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Contact').getRecordTypeId();
        Test.startTest();
            Id recordTypeResult = NetworkBuilderCreateNewAccountController.getRecordTypeId('Partner_Flow_Contact');
            system.assert(recordTypeIdPartner == recordTypeResult);
        Test.stopTest();
    }
}