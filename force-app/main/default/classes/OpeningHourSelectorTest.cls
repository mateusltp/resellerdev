/**
 * @description       : 
 * @author            : bruno.mendes@gympass.com
 * @group             : 
 * @last modified on  : 03/05/2022
 * @last modified by  : bruno.mendes@gympass.com
**/

@IsTest
private class OpeningHourSelectorTest {

    @TestSetup
    static void setup() {
        Account account = PartnerDataFactory.newAccount();
        insert account;

        Opening_Hours__c openingHours = PartnerDataFactory.newOpeningHours(account.Id);
        insert openingHours;
    }

    @isTest
    static void getSObjectFieldList_Test(){

        List<Schema.SObjectField> schSObjLst = new List<Schema.SObjectField> {
                Opening_Hours__c.Id,
                Opening_Hours__c.Name
        };

        Test.startTest();
        System.assertEquals(schSObjLst, new OpeningHourSelector().getSObjectFieldList());
        Test.stopTest();
    }

    @isTest
    static void getSObjectType_Test(){

        Test.startTest();
        System.assertEquals(Opening_Hours__c.sObjectType, new OpeningHourSelector().getSObjectType());
        Test.stopTest();
    }

    @isTest
    static void selectById_Test(){

        List<Opening_Hours__c> openingHours = [SELECT Id, Name FROM Opening_Hours__c LIMIT 1];
        Set<Id> openingHoursIDs = (new Map<Id, Opening_Hours__c>(openingHours)).keySet();

        Test.startTest();
        system.assertEquals(openingHours.get(0).Id, new OpeningHourSelector().selectById(openingHoursIDs)[0].Id);
        Test.stopTest();
    }

    @IsTest
    static void selectAllById_Test() {
        List<Opening_Hours__c> openingHours = [SELECT Id, Account_Related__c FROM Opening_Hours__c LIMIT 1];

        Test.startTest();
        system.assertEquals(openingHours.get(0).Id, new OpeningHourSelector().selectAllById(new Set<Id>{openingHours[0].Account_Related__c})[0].Id);
        Test.stopTest();
    }

    @IsTest
    static void selectAllByAccountId_Test() {
        List<Opening_Hours__c> openingHours = [SELECT Id, Account_Related__c FROM Opening_Hours__c LIMIT 1];

        Test.startTest();
        system.assertEquals(openingHours.get(0).Id, new OpeningHourSelector().selectAllByAccountId(new Set<Id>{openingHours[0].Account_Related__c}).values()[0].Id);
        Test.stopTest();
    }
}