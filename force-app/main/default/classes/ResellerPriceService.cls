public without sharing class ResellerPriceService {
  
    @AuraEnabled public static ResellerPriceResponse getPriceValues(Id opportunityId){
        
        ResellerPriceResponse response = new ResellerPriceResponse();
        String userCurrency;
        
        try{
            Id legacyId = legacyId(UserInfo.getUserId());
            
            Opportunity opp = [SELECT Pricebook2Id, TotalOpportunityQuantity, B_M__c, CurrencyIsoCode FROM Opportunity WHERE Id =: opportunityId LIMIT 1];
            
            List<Product2> prod = [SELECT Id FROM Product2 
                                   WHERE Family_Member_Included__c = false AND Minimum_Number_of_Employees__c <=: opp.TotalOpportunityQuantity
                                   AND Maximum_Number_of_Employees__c >=: opp.TotalOpportunityQuantity];
            
            List<PricebookEntry> pbEntry = [SELECT Pricebook2Id, Product2Id, UnitPrice, Price5Comission__c, Price10Comission__c, Price20Comission__c 
                                            FROM PricebookEntry WHERE Pricebook2Id =: opp.Pricebook2Id AND Product2Id IN : prod AND CurrencyIsoCode =: opp.CurrencyIsoCode LIMIT 1];
            
            if(legacyId != null && opp.B_M__c != 'Intermediation'){
                List<Legacy_Partner_Price__c> legacy =  [SELECT Legacy_Price__c FROM Legacy_Partner_Price__c WHERE Account__c =: legacyId AND isActive__c = true LIMIT 1];
                if (legacy.size() > 0){
                    ResellerPriceFactory factory = new ResellerPriceFactory(opp, legacy[0]);
                    response = factory.build();
                }
            }else{
                if (pbEntry.size() > 0){
                    ResellerPriceFactory factory = new ResellerPriceFactory(opp, pbEntry[0]);
                    response = factory.build();
                }
            }
        }catch (Exception e){
            system.debug(e.getMessage());
            system.debug(e.getStackTraceString());
        }
        return response;
    }   
    
    public static Id legacyId(Id userId){
        List<User> users = [SELECT Id, ContactId FROM User WHERE Id =: userId LIMIT 1];
        if(users.size() > 0){
            List<Contact> ownerContact;
            if(!Test.isRunningTest()){
                ownerContact = [SELECT Id, AccountId from Contact WHERE Id =: users[0].ContactId LIMIT 1]; 
            }else{
                ownerContact = [SELECT Id, AccountId from Contact WHERE FirstName = 'contactLegacy' LIMIT 1]; 
            }  
            if(ownerContact.size() > 0){
                List<Account> partnerAccount = [SELECT Id, Indirect_Channel_Pricebook__c FROM Account WHERE id =: ownerContact[0].AccountId LIMIT 1];
                if(partnerAccount.size() > 0){
                    if(partnerAccount[0].Indirect_Channel_Pricebook__c == 'Legacy Partners'){
                        return partnerAccount[0].Id;
                    }
                }
            }
        }
        return null;
    } 
   
}