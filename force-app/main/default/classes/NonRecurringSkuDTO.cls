public without sharing class NonRecurringSkuDTO {
  private List<PaymentDTO> payments;
  private List<Sku> skus;

  public NonRecurringSkuDTO() {
    this.payments = new List<PaymentDTO>();
    this.skus = new List<Sku>();
  }

  public void addPayment(Payment__c payment) {
    this.payments.add(new PaymentDTO(payment));
  }

  public void addSku(OrderItem orderItem) {
    this.skus.add(new Sku(orderItem));
  }

  private class Sku {
    private String id;
    private String salesforce_id;
    private SkuType sku_type;
    private Decimal unit_price;
    private Decimal discount_percentage;
    private Integer baseline_quantity;
    private Datetime billing_date;

    private Sku(OrderItem orderItem) {
      this.id = orderItem.UUID__c != null
        ? orderItem.UUID__c
        : new Uuid().getValue();
      this.salesforce_id = orderItem.Id;
      this.unit_price = orderItem.UnitPrice;
      this.discount_percentage = orderItem.Discount__c;
      this.baseline_quantity = (Integer) orderItem.Quantity;
      this.billing_date = (Datetime) orderItem.Billing_Date__c;
      this.sku_type = getSkuType(orderItem);
    }

    private SkuType getSkuType(OrderItem orderItem) {
      if (String.isBlank(orderItem.Type__c)) {
        return null;
      }

      if (orderItem.Type__c.equals('Setup Fee')) {
        return SkuType.SETUP_SERVICE;
      }

      if (orderItem.Type__c.contains('Professional Service')) {
        return SkuType.PROFESSIONAL_SERVICES;
      }

      return null;
    }
  }

  private enum SkuType {
    DIGITAL_PLAN,
    PROFESSIONAL_SERVICES,
    SETUP_SERVICE
  }
}