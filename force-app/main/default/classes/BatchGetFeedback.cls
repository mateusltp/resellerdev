global class BatchGetFeedback implements Database.Batchable<sObject> {
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([
            select Id, Proposal_End_Date__c, RecordTypeId, Contract_Signed_At__c
            from Account
        ]);
    }
    
    global void execute(Database.BatchableContext bc, List<Account> scope) {
        
        List<Account> lstAccPreRenewal = new List<Account>();
        List<Account> lstAccPostLaunch = new List<Account>();
        List<Account> lstAccPartnerSalesExperience = new List<Account>();
        
        List<Contact> lstUpdateCon = new List<Contact>();

        
        Id rtDirectChannel = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
        Id rtIndirectChannel = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Reseller').getRecordTypeId();
        Id rtPartners = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gym_Partner').getRecordTypeId();
        system.debug('@@@@@@@@@@@@@@' + scope);
        for(Account acc: scope){
            system.debug('contract' + acc.Contract_Signed_At__c.addMonths(4));
                    system.debug('hj' + Date.today());
            if(acc.RecordTypeId == rtDirectChannel || acc.RecordTypeId == rtIndirectChannel){
                //Add lista de ACCOUNT Pre Renewal
                if(acc.Proposal_End_Date__c == Date.today().addDays(90)){
                    lstAccPreRenewal.add(acc); 
                }
                //Add lista de ACCOUNT Post Launch
                if(acc.Contract_Signed_At__c.addMonths(4) == Date.today()){
                    system.debug('contract' + acc.Contract_Signed_At__c.addMonths(4));
                    system.debug('hj' + Date.today());
                    lstAccPostLaunch.add(acc);
                }
            }
            //Add lista de ACCOUNT Partner Sales Experience
            if(acc.RecordTypeId == rtPartners){
                if(acc.Contract_Signed_At__c.addMonths(1) == Date.today()){
                    lstAccPartnerSalesExperience.add(acc);
                }
            }
        }
        system.debug('@@@@@@@@@@@@@@' + lstAccPreRenewal);
        
        //Add lista de CONTACT Pre Renewal
        if(!lstAccPreRenewal.isEmpty()){
            for(Contact con: [Select Id, Status_do_contato__c, GFSendSurvey__c, Email,Primary_HR_Contact__c,DecisionMaker__c FROM Contact WHERE AccountID IN :lstAccPreRenewal]){
                if(con.Status_do_contato__c == 'Ativo' && con.Email != null && con.Primary_HR_Contact__c == TRUE){
                    con.GFSendSurvey__c = 'Clients CSAT - Pre-Renewal';
                    lstUpdateCon.add(con); 
                }
            }
        }
        system.debug('@@@@@@@@@@@@@@' + lstUpdateCon);
        //Add lista de CONTACT Post Launch
        if(!lstAccPostLaunch.isEmpty()){
            for(Contact con: [Select Id, Status_do_contato__c, GFSendSurvey__c, Email,Primary_HR_Contact__c,DecisionMaker__c FROM Contact WHERE AccountID IN :lstAccPostLaunch]){
                if(con.Status_do_contato__c == 'Ativo' && con.Email != null && con.Primary_HR_Contact__c == TRUE){
                    con.GFSendSurvey__c = 'Clients CSAT - Post Launch';
                    lstUpdateCon.add(con); 
                }
            }
        }
        
        //Add lista de CONTACT Partner Sales Experience
        if(!lstAccPartnerSalesExperience.isEmpty()){
            for(Contact con: [Select Id, Status_do_contato__c, GFSendSurvey__c, Email,Primary_HR_Contact__c,DecisionMaker__c FROM Contact WHERE AccountID IN :lstAccPartnerSalesExperience]){
                if(con.Status_do_contato__c == 'Ativo' && con.Email != null && con.Primary_HR_Contact__c == TRUE){
                    con.GFSendSurvey__c = 'Partners CSAT - Sales Experience';
                    lstUpdateCon.add(con); 
                }
            }
        }
        
        if(!lstUpdateCon.isEmpty()){
            update lstUpdateCon;
        }
    }
    
    global void finish(Database.BatchableContext bc) {
        
    }
}