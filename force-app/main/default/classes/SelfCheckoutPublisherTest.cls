/**
 * @author vncferraz
 */
@IsTest
public class SelfCheckoutPublisherTest {
  @TestSetup
  public static void makeData() {
    Account gympassEntity = AccountMock.getGympassEntity();
    gympassEntity.CurrencyIsoCode='BRL';
    gympassEntity.UUID__c = SelfCheckoutRequestMock.GYMPASS_ENTITY_UUID;
    insert gympassEntity;

    Pricebook2 pricebook = new Pricebook2(
      CurrencyIsoCode='BRL',
      Id = Test.getStandardPricebookId(),
      IsActive = true
    );

    update pricebook;

    List<Product2> products = new List<Product2>();

    Product2 setupFee = ProductMock.getSetupFee();
    setupFee.Minimum_Number_of_Employees__c = 0;
    setupFee.Maximum_Number_of_Employees__c = 10000;
    setupFee.IsActive = true;
    setupFee.Family = 'Enterprise Subscription';
    setupFee.Copay2__c = false;
    setupFee.Family_Member_Included__c = false;
    products.add(setupFee);

    Product2 accessFee = ProductMock.getAccessFee();
    accessFee.Minimum_Number_of_Employees__c = 0;
    accessFee.Maximum_Number_of_Employees__c = 10000;
    accessFee.IsActive = true;
    accessFee.Family = 'Enterprise Subscription';
    accessFee.Copay2__c = false;
    accessFee.Family_Member_Included__c = false;
    products.add(accessFee);

    insert products;

    List<PricebookEntry> pricebookEntries = new List<PricebookEntry>();

    PricebookEntry setupFeePricebookEntry = PricebookMock.getPricebookEntry(pricebook, setupFee);
    setupFeePricebookEntry.UnitPrice = 10;
    pricebookEntries.add(setupFeePricebookEntry);

    PricebookEntry accessFeePricebookEntry = PricebookMock.getPricebookEntry(pricebook, accessFee);
    accessFeePricebookEntry.UnitPrice = 10;
    pricebookEntries.add(accessFeePricebookEntry);

    insert pricebookEntries;

    EventConfiguration__c eventConfig = new EventConfiguration__c();
    eventConfig.name = 'SELF_CHECKOUT_INBOUND';
    eventConfig.commandClassName__c = 'SelfCheckoutCommand';
    insert eventConfig;
  }

  @IsTest
  public static void publish() {

    SelfCheckoutRequest selfCheckoutRequest = SelfCheckoutRequestMock.getMock();

    Test.startTest();
    SelfCheckoutPublisher publisher = new SelfCheckoutPublisher();
    publisher.publish( selfCheckoutRequest );
    Test.stopTest();
    
    List<Queue__c> events =[ select Id,EventName__c,Status__c from Queue__c where EventName__c = 'SELF_CHECKOUT_INBOUND' ];
    System.assertEquals(events.size(),1);
    System.assertEquals(events.get(0).Status__c , 'SCHEDULED');

    List<Opportunity> opps = [select Id,Name,Account.Name,StageName from Opportunity];

    //System.assertEquals(opps.size(),1);
  }

 
}