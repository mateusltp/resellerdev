/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 03-11-2022
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/

@Istest
public with sharing class PartnerLeadsConversionTest {

    private static final PS_Constants constants = PS_Constants.getInstance();

    @TestSetup
    static void Setup(){   

        Id recordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(constants.ACCOUNT_RT_GYM_PARTNER).getRecordTypeId();
        Id rtIdOwnerReferral = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get(constants.LEAD_RT_OWNER_REFERRAL).getRecordTypeId();
        
        Score_Data__c standard = new Score_Data__c();
        standard.Country_Score_Data__c = 'Standard';
        standard.State_Score_Data__c = 'Standard';
        standard.City_Score_Data__c = 'Standard';
        standard.Number_Of_Active_Users__c = 0;
        standard.Number_Of_Sign_Ups__c = 0;
        INSERT standard;
        System.Debug('Score Data ----- >  Standard -----------  >>> ' + standard);
        
        Score_Data__c sdt = new Score_Data__c();
        sdt.Country_Score_Data__c = 'Brazil';
        sdt.State_Score_Data__c = 'São Paulo';
        sdt.City_Score_Data__c = 'Sorocaba';
        sdt.Number_Of_Active_Users__c = 0;
        sdt.Number_Of_Sign_Ups__c = 0;
        INSERT sdt;
        System.Debug('Score Data ----- >  Sorocaba ------------  >>> ' + sdt);
        
        Score_Data__c sdtUp = new Score_Data__c();
        sdtUp.Country_Score_Data__c = 'Brazil';
        sdtUp.State_Score_Data__c = 'São Paulo';
        sdtUp.City_Score_Data__c = 'São Paulo';
        sdtUp.Number_Of_Active_Users__c = 29;
        sdtUp.Number_Of_Sign_Ups__c = 3001;
        INSERT sdtUp;
        System.Debug('Score Data ----- >  São Paulo ------------  >>> ' + sdtUp);
        
        Score_Data__c sdtUpAct = new Score_Data__c();
        sdtUpAct.Country_Score_Data__c = 'Brazil';
        sdtUpAct.State_Score_Data__c = 'São Paulo';
        sdtUpAct.City_Score_Data__c = 'Campinas';
        sdtUpAct.Number_Of_Active_Users__c = 1001;
        sdtUpAct.Number_Of_Sign_Ups__c = 3001;
        INSERT sdtUpAct;
        System.Debug('Score Data ----- >  Campinas ------------  >>> ' + sdtUpAct);
        
        Score_Data__c sdtARGSignUp = new Score_Data__c();
        sdtARGSignUp.Country_Score_Data__c = 'Argentina';
        sdtARGSignUp.State_Score_Data__c = 'Buenos Aires';
        sdtARGSignUp.City_Score_Data__c = 'Almirante Brown';
        sdtARGSignUp.Number_Of_Active_Users__c = 49;
        sdtARGSignUp.Number_Of_Sign_Ups__c = 51;
        INSERT sdtARGSignUp ;
        System.Debug('Score Data ----- >  Argentina - Almirante Brown  ------------  >>> ' + sdtARGSignUp );
        
        Score_Data__c sdtARGActive = new Score_Data__c();
        sdtARGActive.Country_Score_Data__c = 'Argentina';
        sdtARGActive.State_Score_Data__c = 'Buenos Aires';
        sdtARGActive.City_Score_Data__c = 'Arrecifes';
        sdtARGActive.Number_Of_Active_Users__c = 51;
        sdtARGActive.Number_Of_Sign_Ups__c = 51;
        INSERT sdtARGActive ;
        System.Debug('Score Data ----- >  Argentina - Arrecifes ------------  >>> ' + sdtARGActive );
        
        Score_Data__c sdtARGAAzul = new Score_Data__c();
        sdtARGAAzul.Country_Score_Data__c = 'Argentina';
        sdtARGAAzul.State_Score_Data__c = 'Buenos Aires';
        sdtARGAAzul.City_Score_Data__c = 'Azul';
        sdtARGAAzul.Number_Of_Active_Users__c = 101;
        sdtARGAAzul.Number_Of_Sign_Ups__c = 101;
        INSERT sdtARGAAzul ;

        List<Lead> leadLst = new List<Lead>();
        Lead ld1 = new Lead(
            LastName='Gympass Lead', 
            RecordTypeId = rtIdOwnerReferral, 
            Country = 'Brazil', State = 'São Paulo',
            Street = 'Ipanema', City = 'Sorocaba',                       
            Phone = '(015)99835-3692',
            Company = 'Test lead',
            Status = 'em aberto',
            PostalCode = '18071801',
            Is_User_Owner__c = 'Yes',
            Email = 'sasdasd@dasdasd.com',
            Type_of_contact__c = 'Decision Maker',
            Types_of_ownership__c = 'Private Owner');
        
        leadLst.add(ld1);
                
        Lead ld2 = new Lead(
            LastName='Gympass Lead', 
            RecordTypeId = rtIdOwnerReferral, 
            Country = 'Brazil', State = 'São Paulo',
            Street = 'Ipanema', City = 'Sorocaba',                       
            Phone = '(015)99835-3692',
            Company = 'Test lead',
            Status = 'em aberto',
            PostalCode = '18031230',
            Type_of_contact__c = 'Decision Maker',
            Is_User_Owner__c = 'Yes',
            Email = 'sasdasd@dasdasd.com',
            Types_of_ownership__c = 'Private Owner');
        
        leadLst.add(ld2);
        
        INSERT leadLst;

        Id pricebookId = Test.getStandardPricebookId();
        Pricebook2 standardPricebook = new Pricebook2(
                Id = pricebookId,
                IsActive = true
        );
        update standardPricebook;
    }

    @isTest 
    static void updateDuplicatedLeads(){

        Lead lead1 = [SELECT Id, Status, IsConverted FROM LEAD WHERE Company = 'Test lead' LIMIT 1];
        Test.startTest();
            Database.LeadConvert lc = new Database.LeadConvert();
            lc.setLeadId(lead1.id);
            LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
            lc.setConvertedStatus(convertStatus.MasterLabel);
            Database.convertLead(lc);
        Test.stopTest();   
        
        List<Lead> leadLst = [SELECT Id, Status, IsConverted, Referral_Account__c FROM LEAD WHERE Company = 'Test lead'];

        System.assertEquals(leadLst[0].Status, constants.LEAD_STATUS_CONVERTED);
        System.assertEquals(leadLst[1].Status, constants.LEAD_STATUS_CONVERTED);
        System.assertEquals(leadLst[0].Referral_Account__c, leadLst[1].Referral_Account__c);
    }

}