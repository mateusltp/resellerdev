/**
 * @description       : 
 * @author            : ext.gft.marcus.silva@gympass.com
 * @group             : 
 * @last modified on  : 03-16-2022
 * @last modified by  : ext.gft.marcus.silva@gympass.com
**/
public with sharing class DocumentDeviationForm {    
    
    public static Map<String, String> getLabelsOfFields( List<String> aFieldApiName ){
        String type = 'DocumentDeviation__c';

        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType documentDeviationSchema = schemaMap.get(type);
        Map<String, Schema.SObjectField> fieldMap = documentDeviationSchema.getDescribe().fields.getMap();        
        Map<String, String> fieldApiNameAndLabel = new Map<String, String>();       
        String label = ''; 

        for (String fieldName : aFieldApiName) {
            label = fieldMap.get(fieldName).getDescribe().getLabel();
 
            if( label.contains(' | B2B') ){
                label = label.remove(' | B2B');                
            }else if( label.contains(' | Gyms') ){
                label = label.remove(' | Gyms');
            }
            
            fieldApiNameAndLabel.put(fieldName, label);
            
        }
        return fieldApiNameAndLabel;
    }
    
    public static List<String> getFieldOfFieldSets( String aRecordId ){
        List<String> documentFieldsApiName = new List<String>();                
        List<Schema.FieldSetMember> documentDeviationFields = new List<Schema.FieldSetMember>();        

        documentDeviationFields = getFields(aRecordId);
        
            for (Schema.FieldSetMember f : documentDeviationFields) {
                documentFieldsApiName.add(f.getFieldPath());
            }
        return documentFieldsApiName;
    }

    public static List<Schema.FieldSetMember> getFields( String aRecordId ){
        String businessUnit = DocumentDeviationFormController.getBusinessUnit(aRecordId);
        List<Schema.FieldSetMember> lstField = new List<Schema.FieldSetMember>();
        if(businessUnit.contains('B2B') || businessUnit.contains('Indirect')){
            return SObjectType.DocumentDeviation__c.FieldSets.Document_Deviation_Form_B2B.getFields();
        }else if (businessUnit.contains('Partner')){
            return SObjectType.DocumentDeviation__c.FieldSets.Document_Deviation_Form_Partners.getFields();
        }
        return lstField;
    }

    public static String getDocumentId( String aRecordId ){        
        return [SELECT Document__c FROM Case WHERE Id = :aRecordId].Document__c;
    }

    public static List<DocumentDeviation__c> getDocumentDeviationQuery( String aRecordId ){
        String query = 'SELECT ';
        for(Schema.FieldSetMember f : getFields(aRecordId)) {
            query += f.getFieldPath() + ', ';
        }
        query += 'Id, AnyOtherDetail__c FROM DocumentDeviation__c WHERE Case__c =: aRecordId';        
        List<DocumentDeviation__c> documentDeviation = Database.query( String.escapeSingleQuotes(query));

        return documentDeviation;
    }

    public static String getDefaultValuePickList( String aFieldApiName ){
        String defaultVal;
        List<Schema.PicklistEntry> pickListEntry = new List<Schema.PicklistEntry>();

        pickListEntry = getPickListEntry(aFieldApiName);            
            for (Schema.PicklistEntry pv: pickListEntry) {
                if (pv.isDefaultValue()) {
                    defaultVal = pv.getValue();
                }     
            }            
            return defaultVal;
    }

    public static List<DocumentDeviationFormController.RadioOptions> getPickListValues( String aFieldApiName ){
        List<Schema.PicklistEntry> pickListEntry = new List<Schema.PicklistEntry>();
        List<String> pickListValuesList= new List<String>();
        List<DocumentDeviationFormController.RadioOptions> radioOptions = new List<DocumentDeviationFormController.RadioOptions>();

        pickListEntry = getPickListEntry(aFieldApiName);

		for( Schema.PicklistEntry pickListVal : pickListEntry){
			pickListValuesList.add(pickListVal.getLabel());
		}

        for(String valeuList : pickListValuesList){
            radioOptions.add(new DocumentDeviationFormController.RadioOptions(
                valeuList,
                valeuList
            ));
        }
		return radioOptions;         
    }

    public static List<Schema.PicklistEntry> getPickListEntry( String aFieldApiName ){
        String objectName = 'DocumentDeviation__c';
        List<String> pickListValuesList= new List<String>();                
        List<DocumentDeviationFormController.RadioOptions> radioOptions = new List<DocumentDeviationFormController.RadioOptions>();
        
        Schema.SObjectType schemaDocumentDeviation = Schema.getGlobalDescribe().get(objectName);
        Schema.DescribeSObjectResult documentDeviationResult = schemaDocumentDeviation.getDescribe();
        Map<String,Schema.SObjectField> fields = documentDeviationResult.fields.getMap();

        
        Schema.DescribeFieldResult fieldResult = fields.get(aFieldApiName).getDescribe();


        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

        return ple;
    }

    public static Map<String, String> getMpFieldAndSection( List<Document_Deviation_Form__mdt> aDocumentDeviationForm ){
        Map<String, String> mpFieldAndSection = new Map<String, String>();
        for (Document_Deviation_Form__mdt docDeviationForm : aDocumentDeviationForm) {
            mpFieldAndSection.put(docDeviationForm.FieldApiName__c, docDeviationForm.AccordionSection__c);
        }
        return mpFieldAndSection;
    }
    public static Map<String, String> getMpFieldAndTemplate( List<Document_Deviation_Form__mdt> aDocumentDeviationForm ){
        Map<String, String> mpFieldAndTemplate = new Map<String, String>();
        for (Document_Deviation_Form__mdt docDeviationForm : aDocumentDeviationForm) {
            mpFieldAndTemplate.put(docDeviationForm.FieldApiName__c, docDeviationForm.Template__c);
        }
        return mpFieldAndTemplate;
    }
    public static Map<String, String> getMpFieldAndOtherField( List<Document_Deviation_Form__mdt> aDocumentDeviationForm ){
        Map<String, String> mpFieldAndOtherField = new Map<String, String>();
        for (Document_Deviation_Form__mdt docDeviationForm : aDocumentDeviationForm) {
            mpFieldAndOtherField.put(docDeviationForm.FieldApiName__c, docDeviationForm.FieldOtherDeviation__c);
        }
        return mpFieldAndOtherField;
    }
    public static Map<String, String> getMpFieldAndTextB2B( List<Document_Deviation_Form__mdt> aDocumentDeviationForm ){
        Map<String, String> mpFieldAndTextB2B = new Map<String, String>(); 
        for (Document_Deviation_Form__mdt docDeviationForm : aDocumentDeviationForm) {
            mpFieldAndTextB2B.put(docDeviationForm.FieldApiName__c, docDeviationForm.TextAboutFieldLabelB2B__c);
        }
        return mpFieldAndTextB2B;
    }
    public static Map<String, String> getMpFieldAndTextPartner( List<Document_Deviation_Form__mdt> aDocumentDeviationForm ){
        Map<String, String> mpFieldAndTextPartner = new Map<String, String>(); 
        for (Document_Deviation_Form__mdt docDeviationForm : aDocumentDeviationForm) {
            mpFieldAndTextPartner.put(docDeviationForm.FieldApiName__c, docDeviationForm.TextAboutFieldLabelPartner__c);
        }
        return mpFieldAndTextPartner;
    }
}