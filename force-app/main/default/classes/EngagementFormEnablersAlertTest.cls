/**
 * @description       : 
 * @author            : AlyssonMota
 * @group             : 
 * @last modified on  : 12-16-2021
 * @last modified by  : roei@gft.com
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   11-27-2020   Alysson Mota   Initial Version
**/
@isTest
public with sharing class EngagementFormEnablersAlertTest {
    
   /*@isTest
    public static void testEvaluateEnablersSMB() {
        Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
        Id oppRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        Id fm0RecordTypeId = Schema.SObjectType.Form__c.getRecordTypeInfosByDeveloperName().get('M0_12_Steps').getRecordTypeId();
        Id quoteRecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
		
        List<Profile> profiles = [
            SELECT Id, Name
            FROM Profile
            WHERE Name = 'Direct Sales'
            LIMIT 1
        ];
        
        User u = new user();
        u.LastName = 'EJ Owner';
        u.Email = 'owner@ej.com';
        u.Alias = 'ejowner';
        u.Username = 'owner@ej.com';
        u.CommunityNickname = 'ejowner';
        u.LocaleSidKey = 'en_US';
        u.TimeZoneSidKey = 'GMT';
        u.ProfileID = profiles.get(0).Id;
        u.LanguageLocaleKey = 'en_US';
        u.EmailEncodingKey = 'UTF-8';
        insert u;
        
        Account acc = new Account();
        acc.RecordTypeId = accRecordTypeId;
        acc.Name = 'Test Account';
        acc.BillingCountry = 'Brazil';
        acc.BillingState = 'São Paulo';
        acc.NumberOfEmployees = 1000;
        acc.OwnerId = u.Id;
        insert acc;

        Opportunity opp1 = new Opportunity();
        opp1.Name = 'Test Opp';
        opp1.StageName = 'Qualification';
        opp1.RecordTypeId = oppRecordTypeId;
        opp1.CloseDate = System.now().date();
        opp1.AccountId = acc.Id;
        opp1.OwnerId = u.Id;
        insert opp1;

        Quote proposal1 = new Quote();
        proposal1.Name = 'Test Proposal1';
        proposal1.RecordTypeId = quoteRecordTypeId;
        proposal1.OpportunityId = opp1.Id;
        proposal1.License_Fee_Waiver__c = 'No';
        proposal1.OwnerId = u.Id;
        insert proposal1;

        opp1.SyncedQuoteId = proposal1.Id;
        update opp1;

        Form__c m0 = new Form__c();
        m0.RecordTypeId = fm0RecordTypeId;
        m0.Opportunity__c = opp1.Id;
        m0.OwnerId = u.Id;
        insert m0;

        EngagementFormEnablersAlertController.evaluateEnablers((String)m0.Id);
    }
    */

    @isTest
    public static void testEvaluateEnablersNotSMB() {
        Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
        Id oppRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        Id fm0RecordTypeId = Schema.SObjectType.Form__c.getRecordTypeInfosByDeveloperName().get('M0_12_Steps').getRecordTypeId();
        Id quoteRecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();

        Account acc = new Account();
        acc.RecordTypeId = accRecordTypeId;
        acc.Name = 'Test Account';
        acc.BillingCountry = 'Brazil';
        acc.BillingState = 'São Paulo';
        acc.NumberOfEmployees = 2000;
        insert acc;

        Opportunity opp1 = new Opportunity();
        opp1.Name = 'Test Opp';
        opp1.StageName = 'Qualification';
        opp1.RecordTypeId = oppRecordTypeId;
        opp1.CloseDate = System.now().date();
        opp1.AccountId = acc.Id;
        insert opp1;

        Quote proposal1 = new Quote();
        proposal1.Name = 'Test Proposal1';
        proposal1.RecordTypeId = quoteRecordTypeId;
        proposal1.OpportunityId = opp1.Id;
        proposal1.License_Fee_Waiver__c = 'No';        
        insert proposal1;

        opp1.SyncedQuoteId = proposal1.Id;
        update opp1;

        Form__c m0 = new Form__c();
        m0.RecordTypeId = fm0RecordTypeId;
        m0.Opportunity__c = opp1.Id;
        insert m0;

        EngagementFormEnablersAlertController.evaluateEnablers((String)m0.Id);
    }
}