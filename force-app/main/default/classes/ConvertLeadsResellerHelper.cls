/**
 * @description       : 
 * @author            : AlyssonMota
 * @group             : 
 * @last modified on  : 03-11-2021
 * @last modified by  : Alysson Mota
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   03-11-2021   Alysson Mota   Initial Version
**/
public with sharing class ConvertLeadsResellerHelper {

    public Map<Id, Contact> getOrCreateResellerContacts(Map<Id, Lead> idToLeadMap) {
        Map<Id, Account> leadIdToResellerAccount = getResellerAccount(idToLeadMap);
        Map<Id, Contact> leadIdToResellerContact = getResellerContact(idToLeadMap);

        List<Contact> contactsToInsert = new List<Contact>();
        List<Lead> leadsWithoutResellerAccountContact = new List<Lead>();
        List<Contact> contactsForLeadsWihoutAccount = new List<Contact>();
        Map<String, Contact> emailToContactMap = new Map<String, Contact>();

        for (Id leadId : idToLeadMap.keySet()) {
            Account account = leadIdToResellerAccount.get(leadId);
            Contact contact = leadIdToResellerContact.get(leadId);
            Lead    lead    = idToLeadMap.get(leadId);

            if (contact != null) {
                replaceEntryInContactMap(leadId, contact, leadIdToResellerContact);
            } else if (account != null) {
                contact = createContactForAccount(account, lead);
                contactsToInsert.add(contact);
            } else {
                leadsWithoutResellerAccountContact.add(lead);
            }
        }

        contactsForLeadsWihoutAccount = evaluateForleadsWithoutReseller(leadsWithoutResellerAccountContact);
        contactsToInsert.addAll(contactsForLeadsWihoutAccount);
        
        insert contactsToInsert;

        emailToContactMap = getEmailToContactMap(contactsToInsert);

        for (Id leadId : idToLeadMap.keySet()) {
            Lead lead = idToLeadMap.get(leadId);
            Contact contact = emailToContactMap.get(lead.Broker_Email__c);

            if (contact != null) {
                replaceEntryInContactMap(leadId, contact, leadIdToResellerContact);
            }
        }

        return leadIdToResellerContact;
    }
    
    private static Map<Id, Account> getResellerAccount(Map<Id, Lead> idToLeadMap) {
        Map<Id, Account> leadIdToAccountMap = new Map<Id, Account>();
        Set<String> brokerCompanyNames = new Set<String>();
        List<Account> accounts = new List<Account>();
        Map<String, List<Account>> countryToAccountListMap = new Map<String, List<Account>>();

        for (Lead lead : idToLeadMap.values()) {
            if (lead.Broker_Company__c != null) {
                brokerCompanyNames.add(lead.Broker_Company__c + '%');      
            }
        }        

        accounts = getAccountsByBrokerCompanyNames(brokerCompanyNames);
        countryToAccountListMap = getCountryToAccountMap(accounts);
        leadIdToAccountMap = getLeadIdToAccountMap(countryToAccountListMap, idToLeadMap);
        
        return leadIdToAccountMap;
    }

    private static List<Account> getAccountsByBrokerCompanyNames(Set<String> brokerCompanyNames) {
        List<Account> accounts = [
            SELECT Id, Name, BillingCountry
            FROM Account
            WHERE Name LIKE :brokerCompanyNames AND (Name LIKE '%reseller%' OR Name Like '%partner%') AND Type = 'Channel Partner'
        ];

        return accounts;
    }

    private static Map<String, List<Account>> getCountryToAccountMap(List<Account> accounts) {
        Map<String, List<Account>> countryToAccountListMap = new Map<String, List<Account>>();
        
        for (Account acc :  accounts) {
            if (countryToAccountListMap.containsKey(acc.BillingCountry)) {
                countryToAccountListMap.get(acc.BillingCountry).add(acc);
            } else {
                countryToAccountListMap.put(acc.BillingCountry, new List<Account>{acc});
            }
        }

        return countryToAccountListMap;
    }

    private static Map<Id, Contact> getResellerContact(Map<Id, Lead> idToLeadMap) {
        Map<Id, Contact> leadIdToContactMap = new Map<Id, Contact>();
        Set<String> emails = new Set<String>();
        List<Contact> contacts = new List<Contact>();
        Map<String, Contact> emailToContactMap = new Map<String, Contact>();

        for (Lead lead : idToLeadMap.values()) {
            if (lead.Broker_Email__c != null)
                emails.add(lead.Broker_Email__c);
        }
        
        contacts = getContactsByEmails(emails);
        emailToContactMap = getEmailToContactMap(contacts);
        leadIdToContactMap = getLeadIdToContactMap(idToLeadMap, emailToContactMap);

        return leadIdToContactMap;
    }

    private static List<Contact> getContactsByEmails(Set<String> emails) {
        List<Contact> contactList = [
            SELECT Id, AccountId, Email
            FROM Contact
            WHERE Email IN :emails
        ];

        return contactList;
    }

    private static Map<String, Contact> getEmailToContactMap(List<Contact> contacts) {
        Map<String, Contact> emailToContactMap = new Map<String, Contact>();

        for (Contact contact : contacts) {
            emailToContactMap.put(contact.Email, contact);
        }

        return emailToContactMap;
    }

    private static Map<Id, Contact> getLeadIdToContactMap(Map<Id, Lead> idToLeadMap, Map<String, Contact> emailToContactMap) {
        Map<Id, Contact> leadIdToContactMap = new Map<Id, Contact>();

        for (Lead lead : idToLeadMap.values()) {
            Contact c = emailToContactMap.get(lead.Broker_Email__c);
            if (c != null) {
                leadIdToContactMap.put(lead.Id, c);
            }
        }

        return leadIdToContactMap;
    }

    private static void replaceEntryInContactMap(Id leadId, Contact contact, Map<Id, Contact> leadIdToResellerContact) {
        leadIdToResellerContact.put(leadId, contact);
    }

    private static Map<Id, Account> getLeadIdToAccountMap(Map<String, List<Account>> countryToAccountListMap, Map<Id, Lead> idToLeadMap) {
        Map<Id, Account> leadIdToAccountIdMap = new Map<Id, Account>();

        for (Id leadId : idToLeadMap.keySet()) {
            Lead lead = idToLeadMap.get(leadId);

            List<Account> accounts = countryToAccountListMap.get(lead.Country);

            if (accounts != null) {
                for (Account account : accounts) {
                    if (account.Name.startsWithIgnoreCase(lead.Broker_Company__c)) {
                        leadIdToAccountIdMap.put(lead.Id, account);
                        break;
                    }
                }
            }
        }

        return leadIdToAccountIdMap;
    }

    private static Contact createContactForAccount(Account account, Lead lead) {
        Id contactRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Direct_Channel').getRecordTypeId();
        
        Contact newResellerContact = new Contact();
        newResellerContact.AccountId = account.Id;
        newResellerContact.FirstName = lead.Broker_First_Name__c;
        newResellerContact.LastName = lead.Broker_Last_Name__c;
        newResellerContact.Email = lead.Broker_Email__c;
        newResellerContact.RecordTypeID = contactRecordTypeId;

        return newResellerContact;
    }

    private static List<Account> createAccountsForLeads(List<Lead> leads) {
        Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Reseller').getRecordTypeId();
        List<Account> newAccounts = new List<Account>();

        for (Lead lead : leads) {
            if (lead.Broker_Company__c != null) {
                Account newResellerAccount = new Account();
                newResellerAccount.Name = lead.Broker_Company__c + ' Reseller';
                newResellerAccount.BillingCountry = lead.Country;
                newResellerAccount.BillingState = lead.State;
                newResellerAccount.Type = 'Channel Partner';
                newResellerAccount.RecordTypeId = accRecordTypeId;
                newResellerAccount.Email__c = lead.Broker_Email__c;
                
                newAccounts.add(newResellerAccount);
            }

        }

        return newAccounts;
    }

    private static List<Contact> evaluateForleadsWithoutReseller(List<Lead> leads) {
        List<Account> accountsToInsert = new List<Account>();
        List<Contact> contactsToInsert = new List<Contact>();
        Map<String, Lead> brokerEmailToLeadMap = new Map<String, Lead>();

        for (Lead lead : leads) {
            brokerEmailToLeadMap.put(lead.Broker_Email__c, lead);
        }

        accountsToInsert = createAccountsForLeads(leads);
        insert accountsToInsert;
        
        for (Account account : accountsToInsert) {
            Lead lead = brokerEmailToLeadMap.get(account.Email__c);

            if (lead != null && lead.Broker_Email__c != null && lead.Broker_Last_Name__c != null) {
                Contact contact = createContactForAccount(account, lead);
                contactsToInsert.add(contact);
            }
        }

        return contactsToInsert;
    }
}