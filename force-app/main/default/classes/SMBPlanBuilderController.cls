public without sharing class SMBPlanBuilderController {

    private class ProposalNotFoundException extends Exception{

    }

    @TestVisible
    private class DataWrapper {
        @AuraEnabled
        public Id proposalId;
        @AuraEnabled
        public List<QuoteLineItem> products;
        @AuraEnabled
        public String currencyIsoCode;
    }
    
    @AuraEnabled
    public static DataWrapper getData(Id opportunityId){
        try {
            
            Opportunity opp = [SELECT Id, CurrencyIsoCode
                                FROM Opportunity 
                                WHERE Id = :opportunityId LIMIT 1];

            List<Quote> proposals = [SELECT Id
                                FROM Quote 
                                WHERE OpportunityId = :opportunityId AND IsSyncing = TRUE
                                LIMIT 1];
            
            

            if(proposals.size() == 0) {
                throw new ProposalNotFoundException('There is no syncing proposal for this opportunity');
            }
            else {
                Quote proposal = proposals[0];
                List<QuoteLineItem> products = [SELECT Id, Plan_Name__c, Subtotal, Discount, CurrencyIsoCode, Allow_Waivers__c, Product2Id,
                                                    Quantity, List_Price__c, Total_List_Price__c, UnitPrice, Product_Fee_type__c,
                                                    (SELECT Id, Position__c, Percentage__c, End_Date_Text__c, QuoteId__c 
                                                        FROM Waivers__r 
                                                        ORDER BY Position__c ASC) 
                                                FROM QuoteLineItem 
                                                WHERE QuoteId = :proposal.Id];
                DataWrapper data = new DataWrapper();
                data.proposalId = proposal.Id;
                data.products = products;
                data.currencyIsoCode = opp.CurrencyIsoCode;
                return data;
            }
            
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
}