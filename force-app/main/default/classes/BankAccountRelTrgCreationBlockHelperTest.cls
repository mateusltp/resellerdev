/**
 * @File Name          : BankAccountRelTrgCreationBlockHelperTest.cls
 * @Description        : 
 * @Author             : GEPI@GFT.com
 * @Group              : 
 * @Last Modified By   : GEPI@GFT.com
 * @Last Modified On   : 07-24-2020
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    28/05/2020   GEPI@GFT.com     Initial Version
**/
@isTest
public class BankAccountRelTrgCreationBlockHelperTest {
    @TestSetup
    static void Setup(){ 
        Id recordTypeIdAccBank = Schema.SObjectType.Acount_Bank_Account_Relationship__c.getRecordTypeInfosByDeveloperName().get('Gyms_Account_Bank_Account_Partner').getRecordTypeId();    
        Id recordTypeIdBank = Schema.SObjectType.Bank_Account__c.getRecordTypeInfosByDeveloperName().get('Gyms_Bank_Account').getRecordTypeId(); 
        Id recordTypeIdAccount =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gym_Partner').getRecordTypeId();

        Account acc = new Account(Name='AccTest', Website='www.test.com',  RecordTypeId = recordTypeIdAccount, 
        ShippingCountry = 'Brazil', ShippingState = 'São Paulo',
        ShippingStreet = 'Bermudas', ShippingCity = 'Sorocaba',                       
        Types_of_ownership__c = Label.franchisePicklist,
        Subscription_Type__c = 'Value per class',
        Subscription_Period__c = 'Monthy value',
        Subscription_Type_Estimated_Price__c    = 100,
        Has_market_cannibalization__c = 'No',
        Exclusivity__c = 'Yes',
        Exclusivity_End_Date__c = Date.today().addYears(1),
        Exclusivity_Partnership__c = 'Full Exclusivity',
        Exclusivity_Restrictions__c= 'No', 
        Gym_Email__c = 'gymemail@apex.com',
        Phone = '3222123123' ); 
        INSERT acc; 

        /**Bank Account*/
        List<Bank_Account__c> bankAccountLst = new List<Bank_Account__c>();
        Bank_Account__c bankAccount = new Bank_Account__c();
        bankAccount.recordTypeId = recordTypeIdBank;
        bankAccount.Bank_Account_Number_or_IBAN__c = 'abc';
        bankAccount.Bank_Account_Ownership__c = 'test Owner';
        bankAccount.Bank_Name__c = 'ItauFake';
        bankAccount.BIC_or_Routing_Number_or_Sort_Code__c = 'abc'; 
        bankAccount.Routing_Number__c = 1234;
        bankAccount.VAT_Number_or_UTR_number__c = 'tttt1231';
        
        INSERT bankAccount;

        Bank_Account__c bankAccountNegative = new Bank_Account__c();
        bankAccountNegative.recordTypeId = recordTypeIdBank;
        bankAccountNegative.Bank_Account_Number_or_IBAN__c = 'abc'; 
        bankAccountNegative.Bank_Account_Ownership__c = 'test Owner Negative';
        bankAccountNegative.Bank_Name__c = 'BradescoFake';
        bankAccountNegative.BIC_or_Routing_Number_or_Sort_Code__c = 'abc';
        bankAccountNegative.Routing_Number__c = 1234;
        bankAccountNegative.VAT_Number_or_UTR_number__c = 'VVVT1233';
        INSERT bankAccountNegative;

        Acount_Bank_Account_Relationship__c abr = new Acount_Bank_Account_Relationship__c();
        abr.Account__c = acc.Id;
        abr.Bank_Account__c = bankAccount.id;
        abr.RecordTypeId = recordTypeIdAccBank;
        INSERT abr;
    }

    
    @isTest static void insertBankAccountRelation() {
        Id recordTypeIdAccBank = Schema.SObjectType.Acount_Bank_Account_Relationship__c.getRecordTypeInfosByDeveloperName().get('Gyms_Account_Bank_Account_Partner').getRecordTypeId();    
        Account acc = [SELECT id, Name FROM Account WHERE Name = 'AccTest'];
        
        Bank_Account__c bankAccount = [SELECT id, Bank_Name__c FROM Bank_Account__c WHERE Bank_Name__c = 'BradescoFake'];
        
        Test.startTest(); 
        Acount_Bank_Account_Relationship__c abrNegative = new Acount_Bank_Account_Relationship__c();
        abrNegative.Account__c = acc.Id;
        abrNegative.Bank_Account__c = bankAccount.id;
        abrNegative.RecordTypeId = recordTypeIdAccBank;   

        try{
            INSERT abrNegative;
        } catch(Exception ex) {
            System.Assert(ex.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'));
            System.Assert(ex.getMessage().contains('Can\'t insert more than one Bank Account related to a Account'));
        }
        Test.stopTest();               
    }
}