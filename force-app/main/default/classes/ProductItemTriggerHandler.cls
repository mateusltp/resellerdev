/**
 * @File Name          : ProductItemTriggerHandler.cls
 * @Description        : 
 * Test Classes        : ProductItemServiceTest, ProductItemHelperTest
 * @Author             : GEPI@GFT.com
 * @Group              : 
 * @Last Modified By   : alysson.mota@gympass.com
 * @Last Modified On   : 05-17-2022
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    18/05/2020   GEPI@GFT.com     Initial Version
**/
public class ProductItemTriggerHandler extends triggerhandler{

    public override void afterInsert() {
        new ProductItemTriggerCAPHelper().populateCAPNetworkInProduct();

    }

    public override void beforeUpdate() {
        new ProductItemTriggerCAPHelper().populateCAPNetworkInProduct();
        new ProductItemHelper().avoidDuplicatedProducts();
    }

    public override void beforeInsert() {
        new ProductItemHelper().avoidDuplicatedProducts();
    }

    public override void afterUpdate() {
        new ProductItemHelper().updateChildProducts();
    }

    /**
    * @description 
    * @author alysson.mota@gympass.com | 03-08-2022 
    **/
    public override void beforeDelete() {
        ProductItemService prodService = new ProductItemService();
        prodService.deleteProductsAndRelationships(Trigger.old);
    }
}