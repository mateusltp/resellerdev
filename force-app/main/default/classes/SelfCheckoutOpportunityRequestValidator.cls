public without sharing class SelfCheckoutOpportunityRequestValidator {
    private SelfCheckoutOpportunityRequest request;
  
    public SelfCheckoutOpportunityRequestValidator(SelfCheckoutOpportunityRequest request) {
      this.request = request;
    }
  
    public void validatePostRequest() {
      List<String> errorMessages = getAllErrors();
  
      if (!errorMessages.isEmpty()) {
        IntegrationException.setMessages(errorMessages);
  
        throw new IntegrationException(errorMessages.toString());
      }
    }
  
    private List<String> getAllErrors() {
      List<String> errorMessages = new List<String>();
  
      if (request == null) {
        errorMessages.add('Request body cannot be empty');
        return errorMessages;
      }
  
      errorMessages.addAll(getAllClientErrors());
  
      errorMessages.addAll(getAllOpportunityErrors());
  
      return errorMessages;
    }
  
    private List<String> getAllClientErrors() {
      List<String> errorMessages = new List<String>();
  
      LeadDTO leadDTO = request.getLeadDTO();
  
      if (leadDTO == null) {
        errorMessages.add('client field is required');
        return errorMessages;
      }
  
      for(String missingRequiredField : leadDTO.getMissingRequiredFieldsOnSalesCompleted()) {
        errorMessages.add('The client required field ' + missingRequiredField + ' is missing');
      }

      LeadDTO.ContactDTO contactDTO = leadDTO.getContact();
      
      if(contactDTO != null) {
        for(String missingRequiredField : contactDTO.getMissingRequiredFields()) {
            errorMessages.add('The contact required field ' + missingRequiredField + ' is missing');
        }
      }
  
      return errorMessages;
    }
  
    private List<String> getAllOpportunityErrors() {
      List<String> errorMessages = new List<String>();
  
      if (request.getOpportunityDTO() == null) {
        errorMessages.add('opportunity field is required');
        return errorMessages;
      }
  
      return errorMessages;
    }
  }