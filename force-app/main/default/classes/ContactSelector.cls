/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 04-08-2022
 * @last modified by  : alysson.mota@gympass.com
 * Modifications Log 
 * Ver   Date         Author                             Modification
 * 1.0   07-22-2021   gft.samuel.silva@ext.gympass.com   Initial Version
**/
public with sharing class ContactSelector extends ApplicationSelector{
	//private static final id partnerFlowRT = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Contact').getRecordTypeId();
	private static final Id gymsPartnerRT = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Gyms_Partner').getRecordTypeId();

    public List<Schema.SObjectField> getSObjectFieldList() {
		return new List<Schema.SObjectField> {
			Contact.Id,
			Contact.Name
		};
	}

    public Schema.SObjectType getSObjectType() {
		return Contact.sObjectType;
	}

    public List<Contact> selectById(Set<Id> ids) {
		return (List<Contact>) super.selectSObjectsById(ids);
	}

	public List<Contact> byIdAndRole(Set<Id> ids, Set<String> roleSet) {
		return new List<Contact>( (List<Contact>)Database.query(
										newQueryFactory().
										selectField(Contact.Id).
										selectField(Contact.AccountId).
										selectField(Contact.RecordTypeId).
										selectField(Contact.Email).
										selectField(Contact.Phone).
										selectField(Contact.FirstName).
										selectField(Contact.LastName).
										selectField(Contact.Status_do_contato__c).
										selectField(Contact.Type_of_contact__c).
										//setCondition('RecordTypeId = :partnerFlowRT and AccountId in :ids').
										setCondition('RecordTypeId = :gymsPartnerRT AND Id IN :ids AND Cargo__c IN :roleSet').
										toSOQL()
										));	
	}

	public List<Contact> selectContactsByID(Set<Id> ids) {
		return new List<Contact>( (List<Contact>)Database.query(
										newQueryFactory().
										selectField(Contact.Id).
										selectField(Contact.AccountId).
										selectField(Contact.RecordTypeId).
										selectField(Contact.Email).
										selectField(Contact.Phone).
										selectField(Contact.FirstName).
										selectField(Contact.LastName).
										selectField(Contact.Status_do_contato__c).
										selectField(Contact.Type_of_contact__c).
										//setCondition('RecordTypeId = :partnerFlowRT and AccountId in :ids').
										setCondition('RecordTypeId = :gymsPartnerRT and AccountId in :ids').
										toSOQL()
										));									
	}

	public List<Contact> selectContactsForSetUp(Set<Id> ids) {
		return new List<Contact>( (List<Contact>)Database.query(
										newQueryFactory().
										selectField(Contact.Id).
										selectField(Contact.AccountId).
										setCondition('Id in :ids').
										toSOQL()
										));									
	}

	public List<Contact> selectAllByAccountId(Set<Id> accountIds) {
		return (List<Contact>) Database.query(
				newQueryFactory().
						selectFields(getAllFieldsFromContact()).
						setCondition('AccountId IN: accountIds' ).
						toSOQL());
	}

	private Set<String> getAllFieldsFromContact(){
		Map<String, Schema.SObjectField> contactFields = Schema.getGlobalDescribe().get('Contact').getDescribe().fields.getMap();
		return contactFields.keySet();
	}
}