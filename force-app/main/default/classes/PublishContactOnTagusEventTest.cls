@IsTest
public class PublishContactOnTagusEventTest {
  @IsTest
  public static void execute() {
    Test.setMock(HttpCalloutMock.class, new HttpResponseMock());

    Account account = AccountMock.getGympassEntity();
    account.Send_To_Tagus__c = true;
    account.Is_Account_On_Tagus__c = true;
    account.UUID__c = new Uuid().getValue();
    insert account;

    Contact contact = ContactMock.getStandard(account);
    insert contact;

    System.assert(![SELECT Id FROM Queue__c].isEmpty(), 'Event not published');

    contact.Phone = '11 99999-9999';
    update contact;

    System.assertEquals(
      2,
      [SELECT Id FROM Queue__c].size(),
      'Wrong number of events generated'
    );
  }

  private class HttpResponseMock implements HttpCalloutMock {
    public HTTPResponse respond(HTTPRequest req) {
      HttpResponse res = new HttpResponse();
      res.setHeader('Content-Type', 'application/json');
      res.setStatusCode(200);
      return res;
    }
  }
}