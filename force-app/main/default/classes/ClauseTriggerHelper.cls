public class ClauseTriggerHelper {
   
    public void setLengthOfRichTextField(List<Clause__c> ClauseList ){
        for(Clause__c clause : ClauseList){
            clause.Length_of_Text__c = 0;
            if (clause.Text__c != null) {
                clause.Length_of_Text__c = clause.Text__c.length();
            }
        }
    }

}