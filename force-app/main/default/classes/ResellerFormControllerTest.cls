@IsTest
public class ResellerFormControllerTest {


    @TestSetup
    static void makeData(){
        Account clientAccount = ResellerDataFactory.createClientAccountBrazil();
        insert clientAccount;

        Account_Request__c requestBrazil = ResellerDataFactory.createGenericAccountRequestBrazil(clientAccount);
        insert requestBrazil;
    }


    @isTest
    public static void testCreateContact(){

        Account_Request__c request = [ SELECT Id, AccountId__c, Contact_Name__c, Email__c, Department__c, Role__c FROM Account_Request__c LIMIT 1];

        Test.startTest();

        ResellerFormController.createContact(request);
        ResellerFormController.createContact(request);
        Contact contato = [ SELECT Id FROM Contact LIMIT 1];
        System.assertNotEquals(null, contato);

        Test.stopTest();

    }

    @isTest
    public static void testGetAccounts(){

        Account_Request__c request = [ SELECT Id FROM Account_Request__c LIMIT 1];

        Test.startTest();
        String retorno = ResellerFormController.getAccounts(request.Id);
        System.assertNotEquals(null, retorno);
        Test.stopTest();

    }
    @isTest
    public static void testCreateLogError(){

        Test.startTest();
        
        ResellerFormController.createLogError(null, 'Error Teste');

        Test.stopTest();
    }

    @isTest
    public static void testSetAccountInAccountRequest(){

        Account_Request__c request = [ SELECT Id, AccountId__c FROM Account_Request__c LIMIT 1];

        Test.startTest();
          try
             {
        ResellerFormController.setAccountInAccountRequest(request.AccountId__c, request.Id, false, request.Id, UserInfo.getUserId());
           }
           catch(exception e){
           }     
        Account_Request__c setAccount = [ SELECT Id, AccountId__c FROM Account_Request__c WHERE Id =:request.Id ];
     
        System.assertNotEquals(null, setAccount);
        Test.stopTest();

    }

    @isTest
    public static void testCreateNewAccount(){
        Account_Request__c request = ResellerDataFactory.createGenericAccountRequestBrazil();
        insert request;

        Test.startTest();
        ResellerFormController.createNewAccount(request);

        Account acc = [ SELECT Id FROM Account WHERE Id_Company__c =: request.Unique_Identifier__c LIMIT 1];
        System.assertNotEquals(null, acc);

        Test.stopTest();

    }

    @isTest
    public static void testGetInfo(){
        Account_Request__c request = [ SELECT Id, AccountId__c FROM Account_Request__c LIMIT 1];

        Test.startTest();
        String retorno = ResellerFormController.getInfo(request.Id);
        System.assertNotEquals(null, retorno);
        Test.stopTest();
    }

    @isTest 
    public static void testRunGoNoGO(){

        Account_Request__c request = [ SELECT Id, AccountId__c FROM Account_Request__c LIMIT 1];

        Test.startTest();
        try{
        ResellerFormController.runGoNoGO(request.Id);
             }  catch(exception e){
            system.debug('any erro'+e.getMessage());
        }
        Test.stopTest();

    }

    @isTest
    public static void testSetTotalPrice(){
        Account_Request__c request = [ SELECT Id, AccountId__c FROM Account_Request__c LIMIT 1];

        Test.startTest();
        try{
        String retorno = ResellerFormController.setTotalPrice(request.Id);
        System.assertNotEquals(null, retorno);
        }  catch(exception e){
            system.debug('any erro'+e.getMessage());
        }
        
        Test.stopTest();
    }
    @isTest
    public static void testSetTotalPrice2(){
        Account_Request__c request = ResellerDataFactory.createGenericAccountRequestBrazil();
        request.Engine_Status__c = 'GO';
        insert request;

        Test.startTest();
        try{
        String retorno = ResellerFormController.setTotalPrice(request.Id);
        System.assertNotEquals(null, retorno);
        }  catch(exception e){
            system.debug('any erro'+e.getMessage());
        }
        
        Test.stopTest();
    }

    @isTest
    public static void testGetResellerList(){
        Account acc = ResellerDataFactory.createIndirectAccountBrazil();
        insert acc;

        Test.startTest();
        List<Account> acc_list = ResellerFormController.getResellerList();
        System.assertEquals(true, acc_list.isEmpty());
        Test.stopTest();
    }

    @isTest
    public static void testGetResellerResource(){

        Test.startTest();
        String retorno = ResellerFormController.getResellerResource('Presentation_English');

        System.assertNotEquals(null, retorno);
        Test.stopTest();
    }

    @isTest
    public static void testCreateMassiveAccountRequest(){
        Account_Request__c request = [ SELECT Id, AccountId__c FROM Account_Request__c LIMIT 1];

        Test.startTest();
        String retorno = ResellerFormController.createMassiveAccountRequest(request.AccountId__c, null);
        System.assertNotEquals(null, retorno);
        Test.stopTest();
    }

    @isTest
    public static void testCreateMassiveAccountRequestNull(){
        Account_Request__c request = [ SELECT Id, AccountId__c FROM Account_Request__c LIMIT 1];

        Test.startTest();
        String retorno = ResellerFormController.createMassiveAccountRequest(null, UserInfo.getUserId());
        System.assertNotEquals(null, retorno);
        Test.stopTest();
    }

    @isTest
    public static void testWapperClasses(){

        ResellerFormController.GoNoGoResultWrapper GoNoGoResultWrapper = new ResellerFormController.GoNoGoResultWrapper();
        GoNoGoResultWrapper.go = true;
        GoNoGoResultWrapper.reasonNoGo = 'Test';
        GoNoGoResultWrapper.oppId = '';
        GoNoGoResultWrapper.nameAccountRequest = 'Name';
        GoNoGoResultWrapper.website = 'www.google.com';
        GoNoGoResultWrapper.billingCountry = 'BR';
        GoNoGoResultWrapper.staticResource = 'WTP';
        GoNoGoResultWrapper.totalPrice = 12.02;
        GoNoGoResultWrapper.pricePerElegible = 1.1;


        ResellerFormController.AccountList accountList = new ResellerFormController.AccountList();
        accountList.account_list = new List<Account>();
        accountList.uniqueKey = false;

    }


    @isTest
    public static void testsetStatusLoading(){

        Account acc = ResellerDataFactory.createIndirectAccountBrazil();
        insert acc;

        Massive_Account_Request__c accMass = new Massive_Account_Request__c();
        accMass.MassiveLogError__c = 'test';
        accMass.Name = 'test';
        accMass.Reseller__c = acc.id;
        insert accMass;
       
        Massive_Account_Request__c request = [ SELECT Id, Reseller__c FROM Massive_Account_Request__c LIMIT 1];
        String test = request.id;
   
        System.Test.startTest();
        try{
        ResellerFormController.setStatusLoading(test);
    }  catch(exception e){
        system.debug('any erro'+e.getMessage());
    }
        System.Test.stopTest();
   


    }

}