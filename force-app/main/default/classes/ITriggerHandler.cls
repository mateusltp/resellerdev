/*
* @author Bruno Pinho
* @date January/2019
* @description Defines the interface for trigger handlers. Logic for the first time events are placed under the mainEntry 
*               method and the logic for the subsequent events raised on the same transaction (reentrant) are placed under 
*               the inProgressEntry method.
*/
public interface ITriggerHandler
{
    void mainEntry(TriggerParameters tp);
    
    void inProgressEntry(TriggerParameters tp);
    
    void updateObjects();
}