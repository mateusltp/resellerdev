/**
 * @author vncferraz
 * 
 * Provide multiple objects Wrapper for OfferQueueProcessor
 */
public virtual class OfferWrapper extends BaseOfferWrapper {
    
    protected Opportunity opp;
    protected Quote proposal;
    protected QuoteLineItem enterpriseSubscription;
    protected Payment__c billingSettings;
    protected Eligibility__c eligibility;
    protected List<Waiver__c> waivers;

    protected Account_Opportunity_Relationship__c accountInOpportunity;
    
    public OfferWrapper(){
        super();
    }

    override
    public void setOpportunity(Opportunity opp){
        this.opp = opp;
    }

    override
    public void setProposal(Quote proposal){
        this.proposal = proposal;
    }

    override
    public void setEnterpriseSubscription(QuoteLineItem enterpriseSubscription){
        this.enterpriseSubscription = enterpriseSubscription;
    }

    override
    public void setBillingSettings(Payment__c billingSettings){
        this.billingSettings = billingSettings;
    }

    override
    public void setEligibility(Eligibility__c eligibility){
        this.eligibility = eligibility;
    }
    
    override
    public void setWaivers(List<Waiver__c> waivers){
        this.waivers = waivers;
    }  

    override
    public void setAccountInOpportunity(Account_Opportunity_Relationship__c accountInOpportunity){
        this.accountInOpportunity = accountInOpportunity;
    }

    override
    public Opportunity getOpportunity(){
        return this.opp;
    }

    override
    public Quote getProposal(){
        return this.proposal;
    }

    override
    public virtual ProcessResult finish(){
        ProcessResult result = new ProcessResult();
        result.status = 'Error';        
        if ( hasError ){            
            result.statusDetail = errors.toString();
            return result;
        }

        return save(result);    
    }

    public virtual ProcessResult save(ProcessResult result){
        Savepoint sp = Database.setSavepoint();

        try{
            saveOpportunity();
            saveProposal();
            saveAccountInOpportunity();
            saveEnterpriseSubscription();
            saveBillingSettings();
            saveEligibility();
            saveEligibility();
            saveWaivers();
            result.status = 'Success';
        }catch(System.DMLException e){

            Database.rollback(sp);
            result.statusDetail = 'Unexpected Error, please contact your Admin: ' + e.getMessage();
        }

        return result;
    }

    override
    public void saveOpportunity(){
        Database.upsert( this.opp, Opportunity.Id );

    }

    override
    public void saveProposal(){
        this.proposal.OpportunityId = proposal.Opportunity.Id;
        this.proposal.Opportunity = null;        
        Database.upsert( this.proposal, Quote.Id );
        EnterpriseRevenueMetrics revenueMetrics = new EnterpriseRevenueMetrics();          
        revenueMetrics.refreshQuoteRevenues(this.proposal.Id);
        Database.update(this.proposal);
        this.opp.SyncedQuoteId = this.proposal.Id;
        saveOpportunity();
    }

    override
    public void saveEnterpriseSubscription(){
        this.enterpriseSubscription.QuoteId = this.proposal.Id;
        this.enterpriseSubscription.Quote = null;
        Database.upsert( this.enterpriseSubscription, QuoteLineItem.Id );
    }

    override
    public void saveBillingSettings(){
        this.billingSettings.Quote_Line_Item__c = this.enterpriseSubscription.Id;
        this.billingSettings.Quote_Line_Item__r = null;
        Database.upsert( this.billingSettings, Payment__c.Id );
    }

    override
    public void saveEligibility(){
        this.eligibility.Payment__c = this.billingSettings.Id;
        this.eligibility.Payment__r = null;
        Database.upsert( this.eligibility, Eligibility__c.Id );
    }

    override
    public void saveWaivers(){
        if ( this.waivers == null )
            return;
        for(Waiver__c w : this.waivers){
            w.Payment__c = this.billingSettings.Id;
            w.Payment__r = null;
        }
       
        Database.upsert( this.waivers, Waiver__c.Id );
    }

    override
    public void saveAccountInOpportunity(){
        this.accountInOpportunity.Opportunity__c = this.opp.Id;
        Database.upsert( this.accountInOpportunity, Account_Opportunity_Relationship__c.Id );
    }
}