global without sharing class SMBSkuGetProductsService {
    global SMBSkuGetProductsService() {}

    @InvocableMethod
    global static List< List< QuoteLineItem > > getSkuProducts( ProductsRequest[] productsRequest ) {
        ProductsRequest productRequest = productsRequest.get(0);

        List< SKU_Price__c > lLstSkuPrice = getSkuPrice( productRequest.syncedQuote , productRequest.currencyCode , productRequest.quantity );

        Map< String , String > lMapSkuProductIdPbEntryId = getMapSkuProductIdPbEntryId( lLstSkuPrice , productRequest.currencyCode , productRequest.pricebookId);

        List< QuoteLineItem > lLstQuoteLineItem = new List< QuoteLineItem >();

        for( SKU_Price__c iSkuPrice : lLstSkuPrice ){
            QuoteLineItem lQuoteLineItem = new QuoteLineItem();
            lQuoteLineItem.QuoteId = productRequest.syncedQuote;
            lQuoteLineItem.Product2Id = iSkuPrice.Product__c;
            lQuoteLineItem.Plan_Name__c = iSkuPrice.Product_Name__c;
            lQuoteLineItem.PricebookEntryId = lMapSkuProductIdPbEntryId.get( iSkuPrice.Product__c );
            lQuoteLineItem.Quantity = productRequest.quantity;
            lQuoteLineItem.UnitPrice = iSkuPrice.Unit_Price__c;
            lQuoteLineItem.Total_Price__c = productRequest.quantity * iSkuPrice.Unit_Price__c;
            lQuoteLineItem.Discount = 0;
            lQuoteLineItem.List_Price__c = iSkuPrice.Unit_Price__c;
            lLstQuoteLineItem.add( lQuoteLineItem );
        }

        if(!String.isBlank(productRequest.productName)) {
            for(QuoteLineItem product: lLstQuoteLineItem) {
                if(productRequest.productName == product.Plan_Name__c) {
                    return new List< List<QuoteLineItem> >{  new List<QuoteLineItem>{product} };
                }
            }
            return new  List< List<QuoteLineItem> >();
        }

        return new List< List< QuoteLineItem > >{ lLstQuoteLineItem };
    }
    
    private static List< SKU_Price__c > getSkuPrice( String aQuoteId , String aCurrencyCode , Integer aNoEmployees ) {
        Set< String > lSetProducts = new Set< String >();
        
        for( QuoteLineItem iQuoteLineItem : [ SELECT Id, Product2Id FROM QuoteLineItem WHERE QuoteId =: aQuoteId ]){
            lSetProducts.add( iQuoteLineItem.Product2Id );
        }

        Map< String , SKU_Price__c > lMapProductSkuPrice = new Map< String , SKU_Price__c >();
        
        for( SKU_Price__c iSkuPrice : 
            [ SELECT Id, Unit_Price__c, Product_Name__c, Product__c, Minimum_Quantity__c 
              FROM SKU_Price__c WHERE CurrencyIsoCode =: aCurrencyCode AND Product__c NOT IN : lSetProducts AND Minimum_Quantity__c <= :aNoEmployees AND Expired__c = false ORDER BY Minimum_Quantity__c DESC ] ){
            if( lMapProductSkuPrice.get( iSkuPrice.Product__c ) == null ){
                lMapProductSkuPrice.put( iSkuPrice.Product__c , iSkuPrice );
            }
        }

        return lMapProductSkuPrice.values();
    }
    
    private static Map< String , String > getMapSkuProductIdPbEntryId( List< SKU_Price__c > aLstSkuPrice , String aCurrencyCode , String pricebookId) {
        Map< String , String > lMapSkuProductIdPbEntryId = new Map< String , String >();
        List< String > lLstPbEntryId = new List< String >();

        for( SKU_Price__c iSkuPrice : aLstSkuPrice ){ lLstPbEntryId.add( iSkuPrice.Product__c ); }

        for( PricebookEntry iPbEntry : [ SELECT Id, Product2Id FROM PricebookEntry WHERE Product2Id =: lLstPbEntryId AND Pricebook2Id = :pricebookId AND CurrencyIsoCode =: aCurrencyCode] ){
            lMapSkuProductIdPbEntryId.put( iPbEntry.Product2Id , iPbEntry.Id );
        }

        return lMapSkuProductIdPbEntryId;
    }
    
    global class ProductsRequest {
        @InvocableVariable
        public String recordId;

        @InvocableVariable
        public String currencyCode;

        @InvocableVariable
        public String syncedQuote;

        @InvocableVariable
        public Integer quantity;

        @InvocableVariable
        public String pricebookId;

        @InvocableVariable
        public String productName; // set this variable only when you need to get a specific product
    }

}