/**
* @description       : 
* @author            : Jorge Stevaux - JZRX@gft.com
* @group             : 
* @last modified on  : 02-08-2021
* @last modified by  : Jorge Stevaux - JZRX@gft.com
* Modifications Log 
* Ver   Date         Author                         Modification
* 1.0   12-08-2020   Jorge Stevaux - JZRX@gft.com   Initial Version
**/
public class ConvertLeadInMass {
    
    List<Lead> newLeads = trigger.new;
    Map<Id, Lead> leadIdMap = (Map<Id,Lead>)trigger.newMap;
    Map<Id, Lead> leadIdOldMap = (Map<Id,Lead>)trigger.oldMap;
    Id rtIdOwnerReferral = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Partner_Owner_Referral_Lead').getRecordTypeId();
    List<Lead> leadDupLst = new List<Lead>();
    
    public void convertToBeQualifiedLeads() {
        for (Lead l : newLeads){
            if(l.recordTypeId == rtIdOwnerReferral && leadIdOldMap.get(l.id).Status != leadIdMap.get(l.id).Status  &&   l.Status == 'Em qualificação'){
                Set<ID> dupIds = DuplicateHandler.foundLeadDuplicateLeads(l); 
                system.debug('ids duplicados duplsIds>>> '+dupIds);
                
                if(!dupIds.isEmpty()){
                    for(Id idDup : dupIds) {
                        leadDupLst.add(new Lead(id=idDup));
                        system.debug('ids duplicados adicionados na lista leadDupLst >>> '+leadDupLst);
                    }
                }
            } 
            for(lead ld : leadDupLst){
                ld.Types_of_ownership__c = l.Types_of_ownership__c;
                system.debug('type of ownership atualizado >>> '+ld.Types_of_ownership__c);
            }
            
        }
        if(!leadDupLst.isEmpty()){
            system.debug('entrou no if leadDupLst não está vazia');
            List<Lead> lstLeadtoUpdate = new List<Lead>();
            List<lead> lstUpStatus = [SELECT Id, Status ,Types_of_ownership__c , RecordTypeId FROM Lead WHERE Id IN:leadDupLst];
            system.debug('SOQL dos leads da lista de duplicados para update >>> '+lstUpStatus);
            
            for(Lead ld : lstUpStatus){
                if(ld.recordTypeId == rtIdOwnerReferral && ld.Status != 'Em qualificação'){
                    ld.Status = 'Em qualificação';	
                    lstLeadtoUpdate.add(ld);	
                    system.debug('lstLeadtoUpdate em qualificação >>> '+lstLeadtoUpdate);
                }
            }
            Database.SaveResult[] srList = Database.update(lstLeadtoUpdate, false);//Database method to update the records in List
            for (Database.SaveResult sr : srList) {
                if (sr.isSuccess()) {
                }
                else {
                    for(Database.Error objErr : sr.getErrors()) {
                    }
                }
            }
        }
    }
    
    public void convertRejectedLeads() {
        for (Lead l : newLeads){
            if(l.recordTypeId == rtIdOwnerReferral && leadIdOldMap.get(l.id).Status != leadIdMap.get(l.id).Status  &&   l.Status == 'Descartado'){
                Set<ID> dupIds = DuplicateHandler.foundLeadDuplicateLeads(l); 
                system.debug('ids duplicados duplsIds>>> '+dupIds); 
                
                if(!dupIds.isEmpty()){
                    for(Id idDup : dupIds) {
                        leadDupLst.add(new Lead(id=idDup));
                        system.debug('ids duplicados adicionados na lista leadDupLst >>> '+leadDupLst); 
                    }
                }
            } 
            for(lead ld : leadDupLst){
                ld.Types_of_ownership__c = l.Types_of_ownership__c;
                ld.GP_Discard_Reason__c = l.GP_Discard_Reason__c;
                system.debug('type of ownership atualizado >>> '+ld.Types_of_ownership__c);
                system.debug('discard reason atualizado >>> '+ld.GP_Discard_Reason__c);
            }
            
            if(!leadDupLst.isEmpty()){
                system.debug('entrou no if leadDupLst não está vazia');
                List<Lead> lstLeadtoUpdate = new List<Lead>();
                List<lead> lstUpStatus = [SELECT Id, Status ,Types_of_ownership__c , RecordTypeId, GP_Discard_Reason__c FROM Lead WHERE Id IN:leadDupLst];
                system.debug('SOQL dos leads da lista de duplicados para update >>> '+lstUpStatus);
                
                for(Lead ld : lstUpStatus){
                    if(ld.recordTypeId == rtIdOwnerReferral && ld.Status != 'Descartado'){
                        ld.Status = 'Descartado';
                        ld.GP_Discard_Reason__c = l.GP_Discard_Reason__c;
                        lstLeadtoUpdate.add(ld);	
                        system.debug('lstLeadtoUpdate descartado >>> '+lstLeadtoUpdate);
                    }
                }
                Database.SaveResult[] srList = Database.update(lstLeadtoUpdate, false);
                for (Database.SaveResult sr : srList) {
                    if (sr.isSuccess()) {
                    }
                    else {
                        for(Database.Error objErr : sr.getErrors()) {
                        }
                    }
                }
            }
        }
    }
}