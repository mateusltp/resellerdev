/**
 * @description       : 
 * @author            : roei@gft.com
 * @group             : 
 * @last modified on  : 01-19-2022
 * @last modified by  : roei@gft.com
**/
@isTest(seeAllData=false)
public with sharing class CaseRepositoryTest {

    @TestSetup
    private static void createData(){        
        Account lAcc = DataFactory.newAccount();
        Database.insert( lAcc );

        Pricebook2 lStandardPB = DataFactory.newPricebook();

        Opportunity lOpp = DataFactory.newOpportunity( lAcc.Id , lStandardPB , 'Client_Sales_New_Business');
        Database.insert( lOpp );

        Quote lQuote = DataFactory.newQuote( lOpp , ' Quote Test' , 'Client_Sales_New_Business');
        Database.insert( lQuote );

        Case lDealDeskCase = DataFactory.newCase( lOpp , lQuote , 'Deal_Desk_Approval' );
        Database.insert( lDealDeskCase );
    }

    @isTest
    private static void shouldFindCaseById(){
        String lCaseId = [ SELECT Id FROM Case LIMIT 1 ].Id;

        Test.startTest();
        Case lReturnedCase = new CaseRepository().byId( lCaseId );
        Test.stopTest();

        System.assert( lReturnedCase != null );
        System.assertEquals( lCaseId , lReturnedCase.Id );
    }

    @isTest
    private static void shouldFindOpenCaseByQuoteId(){
        Case lCase = [ SELECT Id FROM Case LIMIT 1 ];
        lCase.Status = 'New';
        Database.update( lCase );

        String lQuoteId = [ SELECT Id FROM Quote LIMIT 1 ].Id;
        
        Test.startTest();
        Case lReturnedCase = new CaseRepository().getOpenCasebyQuoteId( lQuoteId );
        Test.stopTest();

        System.assert( lReturnedCase.Id != null );
    }

    @isTest
    private static void shouldFindCaseByOppIdNCaseRt(){
        String lOppId = [ SELECT Id FROM Opportunity LIMIT 1 ].Id;
        
        Test.startTest();
        List< Case > lLstReturnedCase = new CaseRepository().getCaseByOppIdAndRTDevName( lOppId , 'Deal_Desk_Approval' );
        Test.stopTest();

        System.assert( lLstReturnedCase.size() > 0 );
        System.assert( lLstReturnedCase[0].Id != null );
    }

    @isTest
    private static void shouldReturnDdCase(){
        String lOppId = [ SELECT Id FROM Opportunity LIMIT 1 ].Id;
        String lQuoteId = [ SELECT Id FROM Quote LIMIT 1 ].Id;

        Test.startTest();
        Case lCase = new CaseRepository().createDealDeskCaseForOpp( lOppId , lQuoteId , new List<String>() );
        Test.stopTest();

        System.assert( lCase != null );
        System.assertEquals( lOppId , lCase.OpportunityId__c );
        System.assertEquals( lQuoteId , lCase.QuoteId__c );
    }

    @isTest
    private static void shouldCreateDdCase(){
        String lOppId = [ SELECT Id FROM Opportunity LIMIT 1 ].Id;
        String lQuoteId = [ SELECT Id FROM Quote LIMIT 1 ].Id;

        Test.startTest();
        CaseRepository lCaseRepo = new CaseRepository();
        Case lCase = lCaseRepo.createDealDeskCaseForOpp( lOppId , lQuoteId , new List<String>() );
        lCase = lCaseRepo.add( lCase , true );
        Test.stopTest();

        System.assert( lCase.Id != null );
    }

    @isTest
    private static void shouldUpdateCaseSuccessfully(){
        String lTestSubject = 'Random Subject for testing';

        Case lCase = [ SELECT Id FROM Case LIMIT 1 ];
        lCase.Description = lTestSubject;
        
        Test.startTest();
        Case lReturnedCase = new CaseRepository().edit( lCase );
        Test.stopTest();

        lReturnedCase = [ SELECT Description FROM Case WHERE Id =: lReturnedCase.Id ];
        System.assertEquals( lTestSubject , lReturnedCase.Description );
    }


    @isTest
    private static void shouldManualShareCase(){
        Profile lStandardProfile = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User lUserTest = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = lStandardProfile.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.gympass.com');
        Database.insert( lUserTest );

        Case lCase = [ SELECT Id FROM Case LIMIT 1 ];

        Test.startTest();
        Boolean lIsSuccess = new CaseRepository().manualShareRead( lCase.Id , lUserTest.Id );
        Test.stopTest();

        List< Case > lLstCase;

        System.runAs( lUserTest ){
            lLstCase = [ SELECT Id FROM Case ];
        }

        System.assertEquals( false , lLstCase.isEmpty() );
        System.assertEquals( 1 , lLstCase.size() );
    }
}