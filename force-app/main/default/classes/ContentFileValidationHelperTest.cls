/**
* @description       : 
* @author            : GEPI@GFT.com
* @group             : 
* @last modified on  : 06-24-2022
* @last modified by  : gft.jorge.stevaux@ext.gympass.com
* Modifications Log 
* Ver   Date          Author                           Modification
* 1.0   07-15-2020    David Mantovani - DDMA@gft.com   Initial Version
**/
@isTest
public  class ContentFileValidationHelperTest {
    private static final PS_Constants constants = PS_Constants.getInstance();
    @TestSetup
    static void setup(){
        //Create Account by RecordType
        List<Account> accList = new List<Account>();
        Id recordTypePartner = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gym_Partner').getRecordTypeId();
        Account fileAccountBR = new Account();
        
        fileAccountBR.Name = 'Partner_Upload';
        fileAccountBR.RecordTypeId = recordTypePartner;
        fileAccountBR.Types_of_ownership__c = Label.privateOwnerPicklist;
        fileAccountBR.GP_Status__c = 'Pending Approval';
        fileAccountBR.ShippingState = 'Minas Gerais';
        fileAccountBR.ShippingStreet = 'RUX ASSS';
        fileAccountBR.ShippingCity = 'CITY CITY';
        fileAccountBR.ShippingCountry = 'Brazil';
        fileAccountBR.Gym_Type__c = 'Studios';
        fileAccountBR.Subscription_Type__c = 'Value per class';
        fileAccountBR.Subscription_Period__c = 'Monthy value';
        fileAccountBR.Subscription_Type_Estimated_Price__c	= 100;
        fileAccountBR.Has_market_cannibalization__c = 'No';
        fileAccountBR.Exclusivity__c = 'Yes';
        fileAccountBR.Exclusivity_End_Date__c = Date.today().addYears(1);
        fileAccountBR.Exclusivity_Partnership__c = 'Full Exclusivity';
        fileAccountBR.Exclusivity_Restrictions__c= 'No';
        fileAccountBR.Website = 'testing@tesapex.com';
        fileAccountBR.Gym_Email__c = 'gymemail@apex.com';
        fileAccountBR.Phone = '3222123123';
        accList.add(fileAccountBR);
        
        Account fileAccountUS = new Account();
        
        fileAccountUS.Name = 'Us Partner File';
        fileAccountUS.RecordTypeId = recordTypePartner;
        fileAccountUS.Types_of_ownership__c = Label.privateOwnerPicklist;
        fileAccountUS.GP_Status__c = 'Pending Approval';
        fileAccountUS.ShippingState = 'New York';
        fileAccountUS.ShippingStreet = 'RUX ASSS';
        fileAccountUS.ShippingCity = 'CITY CITY';
        fileAccountUS.ShippingCountry = 'United States';
        fileAccountUS.Gym_Type__c = 'Studios';
        fileAccountUS.Subscription_Type__c = 'Value per class';
        fileAccountUS.Subscription_Period__c = 'Monthy value';
        fileAccountUS.Subscription_Type_Estimated_Price__c	= 100;
        fileAccountUS.Has_market_cannibalization__c = 'No';
        fileAccountUS.Exclusivity__c = 'Yes';
        fileAccountUS.Exclusivity_End_Date__c = Date.today().addYears(1);
        fileAccountUS.Exclusivity_Partnership__c = 'Full Exclusivity';
        fileAccountUS.Exclusivity_Restrictions__c= 'No';
        fileAccountUS.Website = 'testing@tesapex.com';
        fileAccountUS.Gym_Email__c = 'usgymemail@apex.com';
        fileAccountUS.Phone = '3222123123';
        accList.add(fileAccountUS);
        
        insert accList;
    }
    
    
    @isTest static void uploadFile(){
        Account fileAccount = [SELECT Id, Name FROM Account WHERE Name = 'Partner_Upload' LIMIT 1] ;
        
        Test.startTest();
        Blob bodyBlob=Blob.valueOf('Unit Test ContentVersion Body');
        ContentVersion contentVersion_1 = new ContentVersion(
            Title = 'photo',
            PathOnClient = '/logoPenguins.jpg',
            VersionData = bodyBlob,
            origin = 'H',
            Type_Files_fileupload__c = 'Photo'
            
        );
        Insert  contentVersion_1;
        
        ContentVersion contentVersion_2 = new ContentVersion(
            Title = 'photoPenguins',
            PathOnClient = '/photoPenguins.png',
            VersionData = bodyBlob,
            origin = 'H',
            Type_Files_fileupload__c = 'Photo'
            
        );
        Insert  contentVersion_2;
        
        ContentDocumentLink ContentDLAcc = new ContentDocumentLink();
        ContentDLAcc.ContentDocumentId = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: contentVersion_1.Id].ContentDocumentId;
        ContentDLAcc.LinkedEntityId = fileAccount.Id;
        Insert ContentDLAcc;
        
        ContentDocumentLink ContentDLAcc1 = new ContentDocumentLink();
        ContentDLAcc1.ContentDocumentId = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: contentVersion_2.Id].ContentDocumentId;
        ContentDLAcc1.LinkedEntityId = fileAccount.Id;
        Insert ContentDLAcc1;
        
        list<ContentDistribution> lstDistribution = [SELECT id, Name, DistributionPublicUrl FROM ContentDistribution where Name =: 'File ' + contentVersion_1.Id];
        list<ContentDistribution> lstDistribution2 = [SELECT id, Name, DistributionPublicUrl FROM ContentDistribution where Name =: 'File ' + contentVersion_2.Id];
        
        system.assertEquals(lstDistribution.size() > 0, true);
        system.assertEquals(lstDistribution2.size() > 0, true);
        
        Test.stopTest();
    }
    
    @isTest static void validFile(){
        Account fileAccount = [SELECT Id, Name FROM Account WHERE Name = 'Partner_Upload'  LIMIT 1] ;
        Blob bodyBlob=Blob.valueOf('Unit Test ContentVersion Body');
        ContentVersion contentVersion_3 = new ContentVersion(
            Title = 'Foto',
            PathOnClient = '/Penguins.pdf',
            VersionData = bodyBlob,
            origin = 'H',
            Type_Files_fileupload__c = 'Photo'
            
        );
        
        Test.startTest(); 
        Insert  contentVersion_3;
        
        ContentDocumentLink ContentDLAcc3 = new ContentDocumentLink();
        ContentDLAcc3.ContentDocumentId = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: contentVersion_3.Id].ContentDocumentId;
        ContentDLAcc3.LinkedEntityId = fileAccount.Id;
        
        try{
            Insert ContentDLAcc3;
        }catch(DmlException e){
            Boolean expectedExceptionThrown =  (e.getMessage().contains('Here are the valid formats')) ? true : false;
            System.AssertEquals(expectedExceptionThrown, true);
        }
        Test.stopTest();
    }
    
    @isTest static void validNames(){
        Account fileAccount = [SELECT Id, Name FROM Account WHERE Name = 'Partner_Upload'  LIMIT 1] ;
        Blob bodyBlob=Blob.valueOf('Unit Test ContentVersion Body');
        ContentVersion contentVersion_3 = new ContentVersion(
            Title = 'Teste',
            PathOnClient = '/Penguins.jpg',
            VersionData = bodyBlob,
            origin = 'H',
            Type_Files_fileupload__c = 'Photo'
            
        );
        
        Test.startTest(); 
        Insert  contentVersion_3;
        
        ContentDocumentLink ContentDLAcc3 = new ContentDocumentLink();
        ContentDLAcc3.ContentDocumentId = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: contentVersion_3.Id].ContentDocumentId;
        ContentDLAcc3.LinkedEntityId = fileAccount.Id;
        
        try{
            Insert ContentDLAcc3;
        }catch(DmlException e){
            Boolean expectedExceptionThrown =  (e.getMessage().contains(constants.ERROR_VALID_NAME) ? true : false);
            System.AssertEquals(expectedExceptionThrown, true);
        }
        Test.stopTest();
    }
    
    @isTest static void validNamesW9Us(){
        Account fileAccount = [SELECT Id, Name FROM Account WHERE Name = 'Us Partner File'  LIMIT 1] ;
        Blob bodyBlob=Blob.valueOf('Unit Test ContentVersion Body');
        ContentVersion contentVersion_3 = new ContentVersion(
            Title = 'w9 Teste',
            PathOnClient = '/Penguins.jpg',
            VersionData = bodyBlob,
            origin = 'H',
            Type_Files_fileupload__c = 'Photo'
            
        );
        
        Test.startTest(); 
        Insert  contentVersion_3;
        
        ContentDocumentLink ContentDLAcc3 = new ContentDocumentLink();
        ContentDLAcc3.ContentDocumentId = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: contentVersion_3.Id].ContentDocumentId;
        ContentDLAcc3.LinkedEntityId = fileAccount.Id;
        
        try{
            Insert ContentDLAcc3;
        }catch(DmlException e){
            Boolean expectedExceptionThrown =  (e.getMessage().contains(constants.ERROR_VALID_NAME) ? true : false);
        }
        
        ContentVersion cv = [SELECT Title FROM ContentVersion LIMIT 1];
        System.AssertEquals(cv.Title.startsWith('W9'), true);
        
        Test.stopTest();
    }

    @isTest static void validNamesJBPUs(){
        Account fileAccount = [SELECT Id, Name FROM Account WHERE Name = 'Us Partner File'  LIMIT 1] ;
        Blob bodyBlob=Blob.valueOf('Unit Test ContentVersion Body');
        ContentVersion contentVersion_3 = new ContentVersion(
            Title = 'JBP Teste',
            PathOnClient = '/Penguins.jpg',
            VersionData = bodyBlob,
            origin = 'H',
            Type_Files_fileupload__c = 'Photo'
            
        );
        
        Test.startTest(); 
        Insert  contentVersion_3;
        
        ContentDocumentLink ContentDLAcc3 = new ContentDocumentLink();
        ContentDLAcc3.ContentDocumentId = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: contentVersion_3.Id].ContentDocumentId;
        ContentDLAcc3.LinkedEntityId = fileAccount.Id;
        
        Insert ContentDLAcc3;
        
        ContentVersion cv = [SELECT Title FROM ContentVersion LIMIT 1];
        System.AssertEquals(cv.Title.startsWith('JBP'), true);
        
        Test.stopTest();
    }
}