/**
 * @description       : 
 * @author            : Samuel Silva - GFT (slml@gft.com)
 * @group             : 
 * @last modified on  : 05-26-2022
 * @last modified by  : roei@gft.com
 * Modifications Log 
 * Ver   Date         Author                              Modification
 * 1.0   12-07-2020   Samuel Silva - GFT (slml@gft.com)   Initial Version
**/
public without sharing class CaseTriggerHandler extends triggerhandler {
    
    public override void beforeUpdate() {        
    }
    
    public override void beforeDelete() {     
    }
    
    public override void afterUpdate() {     
        new DealHierarchyStatusCtrl().validateGpStatus(trigger.new, Trigger.oldMap);
        new DealDeskOperationalChangeBilling().changePaymentBilling(trigger.new);
        new SKUDealDeskHandler().updateAssertDataWhenCaseApproved( (Map< Id , Case >)Trigger.oldMap , (List< Case >)Trigger.new );
    }

    public override void beforeInsert() { 
        new DealDeskOperationalAutoApproveCase().autoApproveCaseWhenStandardAnswers( Trigger.new );
    }

    public override void afterInsert() {
        new DealDeskOperationalResetApprovedFlag().resetSpecialistApprovedFromOpp( Trigger.new );
        new DealDeskOperationalManualShare().operationalManualShare();        
        new DealDeskOperationalChangeBilling().changePaymentBilling(trigger.new);
        new DealDeskOperationalApprovalItems().createApprovalItems(Trigger.new);
    }
}