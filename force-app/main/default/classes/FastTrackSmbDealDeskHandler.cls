/**
 * @description       : 
 * @author            : roei@gft.com
 * @group             : 
 * @last modified on  : 03-29-2022
 * @last modified by  : roei@gft.com
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   03-24-2021   roei@gft.com   Initial Version
**/
public without sharing class FastTrackSmbDealDeskHandler extends FastTrackDealDeskHandler {

    protected String YEARLY_MONTHLY = 'Yearly Monthly';

    public FastTrackSmbDealDeskHandler(){}

    public override void dealDeskAccessFeeEvaluation(){ 
        if( to.proposal == null || to.proposal.accessFee == null ){ return; }
        Boolean lIsConditionApproved;

        if( to.proposal.accessFee.sObj.Fee_Contract_Type__c != FLAT_FEE_TYPE ){
            to.proposal.dealDeskApprovalNeeded = true;
            dealDeskApprovalDescription.add( accessFeeType );

            lIsConditionApproved = feeContractTypeApproved();

            to.proposal.dealDeskApproved &= lIsConditionApproved;
            gLstApprovalConditions.add(
                new DealDeskConditions( accessFeeType , lIsConditionApproved )
            );
        }
        
        if( to.proposal.accessFee.payments == null || to.proposal.accessFee.payments.isEmpty() ){ return; }

        FastTrackProposalCreationTO.Payment lPay = to.proposal.accessFee.payments[0];

        if( ( !YEARLY_MONTHLY.contains( lPay.sObj.Frequency__c ) && isWorseFrequency( lPay.sObj.Frequency__c ) ) || 
            lPay.sObj.Billing_Day__c == 'Custom' || lPay.sObj.Payment_Due_Days__c == 'Custom' ){
                to.proposal.dealDeskApprovalNeeded = true;
            dealDeskApprovalDescription.add( accessFeeFrequency );

            lIsConditionApproved = !( !YEARLY_MONTHLY.contains( lPay.sObj.Frequency__c ) && isWorseFrequency( lPay.sObj.Frequency__c ) );

            to.proposal.dealDeskApproved &= lIsConditionApproved;
            gLstApprovalConditions.add(
                new DealDeskConditions( accessFeeFrequency , lIsConditionApproved )
            );
        }










    }

    public override void dealDeskProServiceOneFeeEvaluation(){ 
        //ProfServicesOneFee PaymentFrequency and Price Range and Eligibility 
        if( to.proposal == null || to.proposal.proServicesOneFee == null ){ return; }
        Boolean lIsConditionApproved;

        if( to.proposal.proServicesOneFee.sObj.Id != null && ( to.proposal.proServicesOneFee.sObj.UnitPrice <= psPriceRange.minSetupFee || 
            to.proposal.proServicesOneFee.sObj.UnitPrice >= psPriceRange.maxSetupFee ) &&
            !psOneFeePriceApproved( to.proposal.proServicesOneFee.sObj.UnitPrice ) ){
            to.proposal.dealDeskApprovalNeeded = true;
            dealDeskApprovalDescription.add(proservicePrice);

            lIsConditionApproved = psOneFeePriceApproved( to.proposal.proServicesOneFee.sObj.UnitPrice );

            to.proposal.dealDeskApproved &= lIsConditionApproved;
            gLstApprovalConditions.add(
                new DealDeskConditions( proservicePrice , lIsConditionApproved )
            );
        }


        if( to.proposal.proServicesOneFee.payments == null || to.proposal.proServicesOneFee.payments.isEmpty() ){ return; }

        FastTrackProposalCreationTO.Payment lPay = to.proposal.proServicesOneFee.payments[0];

        if( ( !YEARLY_MONTHLY.contains( lPay.sObj.Frequency__c ) && !psOneFeefequencyApproved( lPay.sObj.Frequency__c ) ) || 
            lPay.sObj.Billing_Day__c == 'Custom' || lPay.sObj.Payment_Due_Days__c == 'Custom' ){
            to.proposal.dealDeskApprovalNeeded = true;
            dealDeskApprovalDescription.add( proserviceFrequency );

            lIsConditionApproved = !( !YEARLY_MONTHLY.contains( lPay.sObj.Frequency__c ) && !psOneFeefequencyApproved( lPay.sObj.Frequency__c ) );

            to.proposal.dealDeskApproved &= lIsConditionApproved;
            gLstApprovalConditions.add(
                new DealDeskConditions( proserviceFrequency , lIsConditionApproved )
            );
        }










    }
}