/**
 * @description       : 
 * @author            : GEPI@GFT.com
 * @group             : 
 * @last modified on  : 06-21-2021
 * @last modified by  : Alysson Mota
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   02-05-2021   GEPI@GFT.com   Initial Version
**/
@isTest 
public class PipefyIntegrationCancellationTest{
    
    @TestSetup
    static void createData(){ 
       
        User us = getClientSuccessExecutive();
        
        Account acc = getAccount();
        insert acc;
        
        Pricebook2 standardPricebook = new Pricebook2(
            Id = Test.getStandardPricebookId(),
            IsActive = true
        );
        update standardPricebook;
        Pricebook2 pb = standardPricebook;
        
        Product2 accessFeeProduct = getProduct('Enterprise Subscription');
        insert accessFeeProduct;
        
        Product2 setupFeeProduct = getProduct('Setup Fee');
        insert setupFeeProduct; 
        
        Opportunity opp = new Opportunity(CurrencyIsoCode='BRL');

        PricebookEntry accessFeeEntry = getPricebookEntry(pb, accessFeeProduct, opp);
        insert accessFeeEntry;
        
        PricebookEntry setupFeeEntry = getPricebookEntry(pb, setupFeeProduct, opp);
        insert setupFeeEntry;
        
        Product2 pProfServicesOneFee = getProfServicesOneFeeProduct();
        insert pProfServicesOneFee;
        
        PricebookEntry profServicesOneFeeEntry = getPricebookEntry(pb, pProfServicesOneFee, opp);
        insert profServicesOneFeeEntry;
        
        Product2 pProfServicesMainFee = getProfServicesMainFeeProduct();
        insert pProfServicesMainFee;
        
        PricebookEntry profServicesMainFeeEntry = getPricebookEntry(pb, pProfServicesMainFee, opp);
        insert profServicesMainFeeEntry;
        
        opp = getRenewalOpp(acc, pb);
        insert opp;
        
        AccountOpportunityRepository accountsInOpp = new AccountOpportunityRepository(opp.Id);
        accountsInOpp.setRelationshipAccountWithOppForClients(new List<Account>{acc});

        Case ddo = getDealDeskOperationalApproval(opp);
        insert ddo;
        
        Quote qt = getQuote(opp);
        insert qt;
        
        
        //access fee
        QuoteLineItem accessFee = getAccessFee(qt, accessFeeEntry);
        insert accessFee;
        
        Payment__c payForAccessFee = getPaymentForFee(accessFee, acc);
        insert payForAccessFee;
        
        Eligibility__c eli = getEligibilityForPayment(payForAccessFee);
        insert eli;
        
        Waiver__c wai = getWaiverForPayment(payForAccessFee);
        insert wai;
        
        Waiver__c wai2 = getWaiverForPayment(payForAccessFee);
        wai2.Start_Date__c = system.today().adddays(11);
        wai2.End_Date__c = system.today().addDays(20);
        insert wai2;
        
        //setup fee
        QuoteLineItem setupFee = getSetupFee(qt, setupFeeEntry);
        insert setupFee;
        
        Payment__c payForSetupFee = getPaymentForFee(setupFee, acc);
        insert payForSetupFee;
        
        
        //pro services one fee
        QuoteLineItem proServicesOneFee = getProServicesOneFee(qt, profServicesOneFeeEntry);
        insert proServicesOneFee;
        
        Payment__c payForProOneFee = getPaymentForFee(proServicesOneFee, acc);
        insert payForProOneFee;
        
        
        // pro services main fee
        QuoteLineItem proServicesMainFee = getProServicesMaintenanceFee(qt, profServicesMainFeeEntry);
        insert proServicesMainFee;
        
        Payment__c payForProMainFee = getPaymentForFee(proServicesMainFee, acc);
        insert payForProMainFee;
        
        test.startTest();
        //cancellation case
        Case cancellationCase = getCancellationCase(opp.id);
        insert cancellationCase;
        
        //Form__c m0 = getM0(opp, us);
        //insert m0;

        
        Opportunity oppNewBusiness = [select Id,Name,CurrencyIsoCode from Opportunity where recordtype.developername = 'Client_Sales_New_Business']; 
     
        oppNewBusiness.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Success_Renegotiation').getRecordTypeId();
        oppNewBusiness.Sub_Type__c = 'Retention';
        oppNewBusiness.cancellation_date__c = system.today();
        oppNewBusiness.FastTrackStage__c = 'Lost';
        oppNewBusiness.StageName = 'Perdido';
        oppNewBusiness.CloseDate = system.today();
        oppNewBusiness.Loss_Reason__c = 'Lost';
        update oppNewBusiness;

        Case cs = [select id, OpportunityId__c from case where recordtype.developername = 'Contract_Cancellation' limit 1];
        cs.OpportunityId__c = oppNewBusiness.Id;
        update cs;
        
        //Form__c m02 = getM0(oppNewBusiness, us);
        //insert m02;
        test.stopTest();
        
    }
   
    @isTest
    private static void testCancellation(){
        Test.setMock(HttpCalloutMock.class, new PipefyIntegrationHttpCalloutMock());
        test.startTest();
        Case cs = [select id, OpportunityId__c from case where recordtype.developername = 'Contract_Cancellation' limit 1];
        PipefyIntegrationController.auraHandler(cs.Id, 'Case');
        test.stopTest();
      
    }
    
     
   
    
    
    private static User getClientSuccessExecutive(){

        Profile profileSuccess = [
            SELECT Id, Name FROM Profile WHERE Name LIKE '%Success%' LIMIT 1
        ];

        User testUser = new User();
        testUser.Username = 'user@gympass.com';
        testUser.LastName = 'Gympass';
        testUser.Email = 'user@gympass.com';
        testUser.Alias = 'user';
        testUser.TimeZoneSidKey = 'America/Sao_Paulo';
        testUser.LocaleSidKey = 'en_US';
        testUser.EmailEncodingKey = 'ISO-8859-1';
        testUser.ProfileId = profileSuccess.Id;
        testUser.LanguageLocaleKey = 'en_US';
        insert testUser;

        return testUser;
    }
    
    private static Account getAccount(){
        Account acc = new Account();
        acc.Name = 'Test Name';
        acc.Razao_Social__c = 'Test Legal Name';
        acc.BillingCity = 'Test Billig City';
        acc.BillingCountry = 'Brazil';
        acc.BillingCountryCode = 'BR';
        acc.BillingPostalCode = '00000-000';
        acc.BillingStreet = 'Test Street 999';
        acc.Id_Company__c = '26.556.823/0001-91';
        
        return acc;
    }
     
    private static Case getCancellationCase(Id idopp){
        Case cs = new Case();
        cs.Justificate_No_Compliant_Topics__c = 'Justification Test';
        cs.Subject = 'Deal Desk Operational Approval Request';
        cs.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Contract_Cancellation').getRecordTypeId();
        cs.OpportunityId__c = idopp;
        return cs;
    }
    
    private static Product2 getProduct(String ProductName){
        Product2 product = new Product2();
        product.Name = ProductName;
        product.Minimum_Number_of_Employees__c = 0;
        product.Maximum_Number_of_Employees__c = 800;
        product.Family = ProductName;
        product.IsActive = true;
        return product;
    }
    
    private static PricebookEntry getPricebookEntry(Pricebook2 pb, Product2 prd, Opportunity opp){        
        PricebookEntry pbEntry = new PricebookEntry();
        pbEntry.Product2Id = prd.Id;
        pbEntry.Pricebook2Id = Test.getStandardPricebookId();
        pbEntry.UnitPrice = 200;
        pbEntry.IsActive = true;
        pbEntry.CurrencyIsoCode = opp.CurrencyIsoCode;
        return pbEntry;
    }
    
    private static Opportunity getClientSalesOpp(Account acc, Pricebook2 pb){
        Opportunity opp = new Opportunity();
        opp.AccountId = acc.Id;
        opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        opp.Name = 'Opp Test Name';
        opp.CloseDate = system.today().addDays(30);
        opp.StageName = 'Contract';
        opp.FastTrackStage__c = 'Setup';
        opp.Pricebook2Id = pb.Id;
        opp.Type = 'Expansion';  
        opp.Country_Manager_Approval__c = true;
        opp.Payment_approved__c = true;   
        opp.CurrencyIsoCode = 'BRL';
        opp.Gympass_Plus__c = 'Yes';
        opp.Standard_Payment__c = 'Yes';
        opp.Request_for_self_checkin__c = 'Yes';
        return opp;
    }
    
    private static Opportunity getRenewalOpp(Account acc, Pricebook2 pb){
        Opportunity opp = new Opportunity();
        opp.AccountId = acc.Id;
        opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        opp.Name = 'Opp Test Name Renew';
        opp.CloseDate = system.today().addDays(30);
        opp.FastTrackStage__c = 'Launched/Won';
        opp.StageName = 'Lançado/Ganho';
        opp.Pricebook2Id = pb.Id;
        opp.Type = 'Expansion';  
        opp.Country_Manager_Approval__c = true;
        opp.Payment_approved__c = true;   
        opp.CurrencyIsoCode = 'BRL';
        opp.Gympass_Plus__c = 'Yes';
        opp.Standard_Payment__c = 'Yes';
        opp.Request_for_self_checkin__c = 'Yes';
        return opp;
    }
    
    private static Form__c getM0(Opportunity opp, User clientSalesExecutive){
        Form__c m0 = new  Form__c();
        m0.Opportunity__c = opp.Id;
        m0.Client_Success_Executive__c = clientSalesExecutive.Id;
        m0.Client_Success_Approval__c = true;
        m0.Approval_Status__c = 'Approved By Regional CEO';
        m0.Form_Status__c = 'Closed';
        m0.RecordTypeId = Schema.SObjectType.Form__c.getRecordTypeInfosByDeveloperName().get('M0_12_Steps').getRecordTypeId();
        //User us = [select id from user where alias = 'sadmin' and isactive = true limit 1];
        User us = [select id from user limit 1];
        m0.OwnerId = us.id;
        return m0;
    }
    
    private static Case getDealDeskOperationalApproval(Opportunity opp){
        Case ddo = new Case();
        ddo.Justificate_No_Compliant_Topics__c = 'Justification Test';
        ddo.Subject = 'Deal Desk Operational Approval Request';
        ddo.Auto_Approved_Case__c = true;
        ddo.OpportunityId__c = opp.Id;
        ddo.Status = 'Approved';
        ddo.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Deal_Desk_Operational').getRecordTypeId(); 
        ddo.Deal_Desk_Evaluation__c = 'Approved';
        return ddo;
    }
    
    private static Quote getQuote(Opportunity opp){
        Quote qt = new Quote();
        qt.Name = 'Test';
        qt.OpportunityId = opp.Id;
        qt.License_Fee_Waiver__c = 'Yes';
        qt.Waiver_Termination_Date__c = system.today().addDays(10);
        return qt;
    }
    
    private static QuoteLineItem getAccessFee(Quote qt, PricebookEntry pbEntry){
        QuoteLineItem accessFee = new QuoteLineItem();
        accessFee.QuoteId = qt.Id;
        accessFee.PricebookEntryId = pbEntry.Id;
        accessFee.Description = '';
        accessFee.Quantity = 100;
        accessFee.UnitPrice = pbEntry.UnitPrice;  
        
        return accessFee;
    }
    
    private static QuoteLineItem getSetupFee(Quote qt, PricebookEntry pbEntry){
        QuoteLineItem SetupFee = new QuoteLineItem();
        SetupFee.QuoteId = qt.Id;
        SetupFee.PricebookEntryId = pbEntry.Id;
        SetupFee.Description = '';
        SetupFee.Quantity = 100;
        SetupFee.UnitPrice = pbEntry.UnitPrice; 
        
        return SetupFee;
    }
    
    private static Product2 getProfServicesOneFeeProduct(){
        Product2 product = new Product2();
        product.Name = 'Professional Services Setup Fee';
        product.Minimum_Number_of_Employees__c = 0;
        product.Maximum_Number_of_Employees__c = 900;
        product.Family = 'Professional Services';
        product.IsActive = true;
        product.Family_Member_Included__c = true;
        return product;
    }

    private static Product2 getProfServicesMainFeeProduct(){
        Product2 product = new Product2();
        product.Name = 'Professional Services Maintenance Fee';
        product.Minimum_Number_of_Employees__c = 0;
        product.Maximum_Number_of_Employees__c = 900;
        product.Family = 'Professional Services';
        product.IsActive = true;
        product.Family_Member_Included__c = true;
        return product;
    }
    
    private static QuoteLineItem getProServicesOneFee(Quote qt, PricebookEntry pbEntry){
        QuoteLineItem proServicesOneFee = new QuoteLineItem();
        proServicesOneFee.QuoteId = qt.Id;
        proServicesOneFee.PricebookEntryId = pbEntry.Id;
        proServicesOneFee.Description = '';
        proServicesOneFee.Quantity = 100;
        proServicesOneFee.UnitPrice = pbEntry.UnitPrice;
        
        return proServicesOneFee;
    }
    
    private static QuoteLineItem getProServicesMaintenanceFee(Quote qt, PricebookEntry pbEntry){
        QuoteLineItem proServicesMaintenanceFee = new QuoteLineItem();
        proServicesMaintenanceFee.QuoteId = qt.Id;
        proServicesMaintenanceFee.PricebookEntryId = pbEntry.Id;
        proServicesMaintenanceFee.Description = '';
        proServicesMaintenanceFee.Quantity = 100;
        proServicesMaintenanceFee.UnitPrice = pbEntry.UnitPrice;
        
        return proServicesMaintenanceFee;
    }
    
    private static Waiver__c getWaiverForPayment(Payment__c pay){
        Waiver__c w = new Waiver__c();
        w.Start_Date__c = System.today();
        w.End_Date__c = System.today().addDays(10);
        w.Percentage__c = 10;
        w.RecordTypeId = Schema.SObjectType.Waiver__c.getRecordTypeInfosByDeveloperName().get('Fixed_Date').getRecordTypeId();
        w.Payment__c = pay.Id;
        return w;
    }
    
    private static Payment__c getPaymentForFee(QuoteLineItem qli, Account acc){
        Payment__c afp = new Payment__c();
        afp.RecordTypeId = Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Payment').getRecordTypeId();
        afp.Quote_Line_Item__c = qli.Id;
        afp.Percentage__c = 100;
        afp.Payment_Method__c = 'Wire Transfer';
        afp.PO_Required__c = 'Yes';
        afp.Frequency__c = 'Monthly';
        afp.Cutoff_Day__c = 1;   
        afp.Account__c =  acc.Id;
        return afp;
    }
    
    private static Eligibility__c getEligibilityForPayment(Payment__c payment){
        Eligibility__c eli = new Eligibility__c();
        eli.Name = 'Headquarters';
        eli.Communication_Restriction__c = '3. Accept communications with employees after sign up';
        eli.Is_Default__c = true;
        eli.Group_Name__c = 'Main';
        eli.Launch_Date__c = system.today().addDays(10);
        return eli;
    }

    @Future(callout=true)
    public static void futureHandler(String recordId, String sObjectName){
        PipefyIntegrationController.auraHandler(recordId, sObjectName);
    }

    public class Mock implements HttpCalloutMock {
    	protected Integer code = 200;
    	protected String status = 'OK';
    	protected String body = '{"data":{"createCard":{"card":{"id":"405722846"},"clientMutationId":null}}}';

        /**
         * @description Returns an HTTP response for the given request.
         * @param req HTTP request for which response should be returned
         * @return mocked response
         */
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse resp = new HttpResponse();
            resp.setStatusCode(code);
            resp.setStatus(status);
            resp.setBody(body);
            return resp;
        }
    }
    
    public class PipefyIntegrationHttpCalloutMock implements HttpCalloutMock {
        // Implement this interface method
        public HTTPResponse respond(HTTPRequest request) {
            // Create a fake response
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody('{"data":{"createCard":{"card":{"id":"405722846"},"clientMutationId":null}}}');
            response.setStatusCode(200);
            return response; 
        }
    }
    
    
}