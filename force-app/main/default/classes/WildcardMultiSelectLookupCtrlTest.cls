/*
* @Author: Bruno Pinho
* @Date: January/2019
* @Description: Test to Wildcard Multi Select Lookup Ctrl
*/
@isTest
private class WildcardMultiSelectLookupCtrlTest
{

    static void SetUp()
    {
        test.StartTest();
    }
    
    static void TearDown()
    {
        test.StopTest();
    }
    
    public static testmethod void Test_fetchLookUpValues()
    {
        SetUp();
        string searchkeyword = '';
        string objectname = 'User';
        List<sObject> excludeitemslist = new List<sObject>();
        System.Assert(WildcardMultiSelectLookupCtrl.fetchLookUpValues(searchkeyword, objectname, excludeitemslist) != null);
        TearDown();
    }
}