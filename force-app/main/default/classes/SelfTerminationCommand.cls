public without sharing class SelfTerminationCommand extends AbstractCommand  {
    private Queue__c eventQueue;
    private String eventQueueName;
    private Order orderRequested;
    private SelfTerminationRequest terminationRequest;
    private final String API = 'API';
    private final String CANCEL = 'CANCEL';
    private final String UNDO = 'UNDO';
    private final String CANCEL_REQUESTED= 'Cancel Requested';
    private final String ACTIVATED = 'Activated';
    private final String OFFER_CREATION = 'Offer Creation';
    private final String VALIDATED = 'Validated';
    private final String LANCADO_GANHO = 'Lançado/Ganho';
	private final String LAUNCHED_WON = 'Launched/Won';
    private final String REQUEST_PAYLOAD = 'REQUEST_PAYLOAD';
    private final String SELF_TERMINATION_OPP_NAME = 'Self Termination';
    private final String RETENTION = 'Retention';
    private final String UPDATE_ORDER_TAGUS_EVENT = 'UPDATE_ORDER_ON_TAGUS';

    override public void preExecute(){
        this.eventQueue = event.get();
        this.eventQueueName = this.eventQueue.EventName__c;
        setPayloadsToSObjects();
    }

    override public void execute(){
        //To ensure all the remaining users can rerun the EventQueue in case there is some error
        String profileName = [SELECT Name FROM Profile WHERE Id =:UserInfo.getProfileId()].Name;
        if(profileName == API){
            TriggerHandler.bypass('OrderTriggerHandler');
        }

        if( this.eventQueueName.contains(CANCEL)){
            Opportunity retentionOppToCreate = createRetentionOpp();
            Database.insert(retentionOppToCreate);
            this.orderRequested.Status = CANCEL_REQUESTED;
            this.orderRequested.OpportunityId = retentionOppToCreate.Id;
            this.orderRequested.Cancelation_Date__c = this.terminationRequest.getDueDate();
            this.orderRequested.Cancelation_Reason__c = this.terminationRequest.getReason();
            this.orderRequested.Cancelation_Description__c = this.terminationRequest.getReasonDetails();

        }else if( this.eventQueueName.contains(UNDO) ){
            Opportunity oppRetToCloseWon = closeWonRetentionOpp();
            if(oppRetToCloseWon != null)
                Database.update(oppRetToCloseWon);
            this.orderRequested.Status = ACTIVATED;
            this.orderRequested.Cancelation_Date__c = null;
            this.orderRequested.Cancelation_Reason__c = '';
            this.orderRequested.Cancelation_Description__c = '';
        }

        this.orderRequested.Is_Order_On_Tagus__c = true;
        Database.update(this.orderRequested);
    }

    
    private void setPayloadsToSObjects(){
    
        List<Attachment> attachs = new List<Attachment>([SELECT
                                                            Body,
                                                            BodyLength,
                                                            ContentType,
                                                            CreatedById,
                                                            CreatedDate,
                                                            Description,
                                                            Id,
                                                            Name,
                                                            ParentId
                                                        FROM Attachment
                                                        WHERE ParentId = :eventQueue.Id 
                                                        ]);

        for(Attachment attach : attachs ){
            if(attach.Name.contains(REQUEST_PAYLOAD)){
                this.terminationRequest = (SelfTerminationRequest) JSON.deserialize(attach.Body.toString(), SelfTerminationRequest.class);
            }else if(attach.Name.contains(eventQueue.EventName__c)){
                this.orderRequested = (Order) JSON.deserialize(attach.Body.toString(), Order.class);
            }
        }
    }

    private Opportunity createRetentionOpp(){
        Id SMBRenegotiationId =   Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SMB_Success_Renegotiation').getRecordTypeId();

        Opportunity retOpp = new Opportunity (
            Name = SELF_TERMINATION_OPP_NAME,
            recordTypeId = SMBRenegotiationId, 
            Type='New Business',
            Sub_Type__c = RETENTION,
            AccountId = this.orderRequested.AccountId,
            StageName = VALIDATED,
            FastTrackStage__c = OFFER_CREATION,
            CloseDate =  date.today(),
            cancellation_date__c = this.terminationRequest.getDueDate().date(),
            Cancellation_Reason__c =  this.terminationRequest.getReason(),
            Cancelation_Description__c = this.terminationRequest.getReasonDetails()
        );
        return retOpp;
    }

    private Opportunity closeWonRetentionOpp(){
        Opportunity opp = [SELECT Id FROM OPPORTUNITY WHERE Id = :this.orderRequested.OpportunityId LIMIT 1];
        if(opp == null){
            return null;
        }
        opp.StageName = LANCADO_GANHO;
        opp.FastTrackStage__c = LAUNCHED_WON;
        opp.cancellation_date__c = null;
        opp.Cancellation_Reason__c = '';
        opp.Cancelation_Description__c ='';
        return opp;
    }

}