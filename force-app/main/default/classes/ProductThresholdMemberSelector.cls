/**
 * Created by bruno on 31/01/2022.
 */

/**
 * @description       :
 * @author            : bruno.mendes@gympass.com
 * @group             :
 * @last modified on  : 04-29-2022
 * @last modified by  : ext.gft.pedro.oliveira@gympass.com
**/
public with sharing class ProductThresholdMemberSelector extends ApplicationSelector {

    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
                Product_Threshold_Member__c.Id
        };
    }

    public Schema.SObjectType getSObjectType() {
        return Product_Threshold_Member__c.sObjectType;
    }

    public List<Product_Threshold_Member__c> selectById(Set<Id> ids) {
        return (List<Product_Threshold_Member__c>) super.selectSObjectsById(ids);
    }

    public List<Product_Threshold_Member__c> selectThresholdByProductId(Set<Id> productIds)
    {
        return (List<Product_Threshold_Member__c>) Database.query(
                newQueryFactory().
                        selectField(Product_Threshold_Member__c.Id).
                        selectField(Product_Threshold_Member__c.Product__c).
                        selectField('Threshold__c').
                        selectField('Threshold__r.Id').
                        selectField('Threshold__r.Volume_discount_date__c').
                        selectField('Threshold__r.Discount__c').
                        selectField('Threshold__r.Threshold_value_start__c').
                      //  selectField('Threshold__r.Replicate_Discount__c').
                        selectField('Threshold__r.CurrencyIsoCode').
                        selectField('Product__r.CurrencyIsoCode').
                        selectField('Threshold__r.Volume_Discount_Type__c').
                        setCondition('Product__c IN :productIds').toSOQL()
        );
    }

    public List<Product_Threshold_Member__c> selectThresholdToReplicate(id productIds)
    {
        return (List<Product_Threshold_Member__c>) Database.query(
                newQueryFactory().
                        selectField(Product_Threshold_Member__c.Id).
                        selectField(Product_Threshold_Member__c.Product__c).
                        selectField('Threshold__c').
                        selectField('Threshold__r.Id').
                        setCondition('Product__c = :productIds').toSOQL()
        );
    }

    public List<Product_Threshold_Member__c> selectThresholdsByProdId(Set<Id> productIds)
    {
        return (List<Product_Threshold_Member__c>) Database.query(
                newQueryFactory().
                        selectField(Product_Threshold_Member__c.Product__c).
                        selectField('Threshold__c').
                        selectField('Threshold__r.Id').
                        selectField('Threshold__r.Volume_discount_date__c').
                        selectField('Threshold__r.Threshold_value_start__c').
                        selectField('Threshold__r.Discount__c').
                        setCondition('Product__c = :productIds').toSOQL()
        );
    }

    public Map<Id, Product_Threshold_Member__c> selectThresholdMapByProductId(Set<Id> productIds) {
        return new Map<Id, Product_Threshold_Member__c>(( List<Product_Threshold_Member__c> ) Database.query(
                newQueryFactory(false).
                        selectField(Product_Threshold_Member__c.Threshold__c).
                        setCondition('Product__c IN :productIds').
                        toSOQL()
        ));
    }
}