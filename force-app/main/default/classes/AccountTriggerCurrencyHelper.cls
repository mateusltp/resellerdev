/**
 * @File Name          : AccountTriggerCurrencyHelper.cls
 * @Description        : 
 * @Author             : GEPI@GFT.com
 * @Group              : 
 * @Last Modified By   : alysson.mota@gympass.com
 * @Last Modified On   : 06-10-2022
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    02/04/2020   GEPI@GFT.com     Initial Version
**/
public class AccountTriggerCurrencyHelper {
    
    //Get the Custom Metadata type that have the currency type by country
    List<CurrencyByCountry__mdt> lst =[Select Country__c, Currency__c, Code__c  from CurrencyByCountry__mdt];
    List<Opportunity> opplst = new List<Opportunity>();

    //Get RecordTypeId for Partner Account
    Id partnerRT        = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gym_Partner').getRecordTypeId();
    Id partnerNewFlowRT = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Account').getRecordTypeId();

    //Get RecordTypeId for Client Account
    Id clientRT = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();

    public void setCurrencyOnBeforeInsert(){
        List<Account> lstNew = trigger.new;
        setAccCurrencyOnBeforeInsert(lstNew);
    }
    
    public void setCurrencyOnBeforeUpdate() {
        Map<Id,Account> lstOld = (Map<Id,Account>)Trigger.oldMap;
        Map<Id,Account> mapNew = (Map<Id,Account>)Trigger.newMap;
        setAccCurrencyOnCountryChange(mapNew, lstOld);
    }

    public void setCurrencyForOppsOnAfterUpdate() {
        Map<Id,Account> lstOld = (Map<Id,Account>)Trigger.oldMap;
        Map<Id,Account> mapNew = (Map<Id,Account>)Trigger.newMap;
        setOppCurrencyOnAccCountryChange(mapNew, lstOld);
    }

    public void setAccCurrencyOnBeforeInsert(List<Account> lstNew) {
        Map<String, CurrencyByCountry__mdt> countryCodeToCurrencyMdt = getCountryCodeToCurrencyMdt(lst);
        
        for (Account acc : lstNew ){
            if ( acc.RecordTypeId == clientRT || acc.RecordTypeId ==  partnerNewFlowRT ){
                setAccountCurrency(acc, acc.BillingCountryCode, countryCodeToCurrencyMdt);
            }
            else if ( acc.RecordTypeId == partnerRT ){
                setAccountCurrency(acc, acc.ShippingCountryCode, countryCodeToCurrencyMdt);
            }
        }
    }

    public void setAccCurrencyOnCountryChange(Map<Id, Account> mapNew,Map<Id,Account> mapOld) {
        Map<String, CurrencyByCountry__mdt> countryCodeToCurrencyMdt = getCountryCodeToCurrencyMdt(lst);
        
        for( Id accountId : mapNew.keySet() ){   
            Id accountRecTypeId = mapNew.get(accountId).RecordTypeId;
            Account newAcc = mapNew.get(accountId);
            Account oldAcc = mapOld.get(accountId);

            if ( accountRecTypeId == clientRT || accountRecTypeId == partnerNewFlowRT ){                
                if ( oldAcc.BillingCountryCode != newAcc.BillingCountryCode ){
                    setAccountCurrency(newAcc, newAcc.BillingCountryCode, countryCodeToCurrencyMdt);
                }
            }
            else if ( accountRecTypeId == partnerRT ){
                if ( oldAcc.ShippingCountryCode != newAcc.ShippingCountryCode ){
                    setAccountCurrency(newAcc, newAcc.ShippingCountryCode, countryCodeToCurrencyMdt);
                }
            }
        }
    }
    
    public void setOppCurrencyOnAccCountryChange(Map<Id, Account> mapNew,Map<Id,Account> mapOld){
        Map<String, CurrencyByCountry__mdt> countryCodeToCurrencyMdt = getCountryCodeToCurrencyMdt(lst);
        list<Id> lstRecordIdChanged = new list<Id>();

        for( Id accountId : mapNew.keySet() ){   
            Id accountRecTypeId = mapNew.get(accountId).RecordTypeId;

            if ( accountRecTypeId == clientRT || accountRecTypeId == partnerNewFlowRT ){
                if ( mapOld.get(accountId).BillingCountryCode != mapNew.get(accountId).BillingCountryCode ){
                    lstRecordIdChanged.add(accountId);
                }
            }
            else if ( accountRecTypeId == partnerRT ){
                if ( mapOld.get(accountId).ShippingCountryCode != mapNew.get(accountId).ShippingCountryCode ){
                    lstRecordIdChanged.add(accountId);
                }
            }
        }

        if(!lstRecordIdChanged.isEmpty()){
            updateOppsCurrencies(mapNew, lstRecordIdChanged);
        }
        
    }
    
    /**
    * Add bypass to Wishlists migration https://gympass.atlassian.net/browse/IT-2682
    * @author alysson.mota@gympass.com | 06-10-2022 
    * @param mapNew 
    * @param lstRecordIdChanged 
    **/
    private void updateOppsCurrencies(Map<Id, Account> mapNew, list<Id> lstRecordIdChanged) {
        List<Opportunity> newOpplst = new List<Opportunity>();
        
        List<Opportunity> opplst = [
            SELECT CurrencyIsoCode, AccountId 
            FROM Opportunity 
            WHERE StageName NOT IN ('Perdido','Lançado/Ganho') AND AccountId IN :lstRecordIdChanged
        ];
            
        for (Opportunity opp : opplst) {
            Account acc = mapNew.get(opp.AccountId);
                
            if (acc != null && acc.Other_description__c == null) {
                opp.CurrencyIsoCode = acc.CurrencyIsoCode;
                newOpplst.add(opp);
            }
        }

        try {
            Database.update(newOpplst);
        } catch( DmlException ex ){
            (new IntegrityError()).doAction();
            //throw new IntegrityException('Can\'t update the Country/Currency when the related opportunity have products or proposals');
        }catch(Exception ex){
            System.debug('Exception when update opprtunity => ' + ex);
        }
    }

    private Map<String, CurrencyByCountry__mdt> getCountryCodeToCurrencyMdt(List<CurrencyByCountry__mdt> currencyMdtLst) {
        Map<String, CurrencyByCountry__mdt> countryCodeToCurrencyMdt = new Map<String, CurrencyByCountry__mdt>();

        for (CurrencyByCountry__mdt currencyMdt : currencyMdtLst) {
            countryCodeToCurrencyMdt.put(CurrencyMdt.code__c, currencyMdt);
        }

        return countryCodeToCurrencyMdt;
    }

    private CurrencyByCountry__mdt setAccountCurrency( Account acc, String countryCode, Map<String, CurrencyByCountry__mdt> countryCodeToCurrencyMdt ) {
        CurrencyByCountry__mdt currencyMdt = countryCodeToCurrencyMdt.get(countryCode);

        if (currencyMdt != null) 
            acc.CurrencyIsoCode = currencyMdt.Currency__c;
        else {
            currencyMdt = countryCodeToCurrencyMdt.get('US'); // Default to US
            acc.CurrencyIsoCode = currencyMdt.Currency__c;
        }
        return currencyMdt; 
    }

    public class IntegrityError {
        public IntegrityError() {}

        public void doAction() {
            // Trigger.new is valid here
            SObject[] sobjects = Trigger.new;
            for (Sobject sobj : sobjects) {
                sobj.addError('Can\'t update the Country/Currency when the related opportunity have products or proposals');
            }
        }
    }
}