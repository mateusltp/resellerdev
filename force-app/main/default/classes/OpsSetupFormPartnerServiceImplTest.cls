/**
 * @description       : 
 * @author            : bruno.mendes@gympass.com
 * @group             : 
 * @last modified on  : 28/04/2022
 * @last modified by  : bruno.mendes@gympass.com
**/
@IsTest
private class OpsSetupFormPartnerServiceImplTest {

    @IsTest
    static void createOpsFormForAccounts_Test() {
        fflib_ApexMocks mocks = new fflib_ApexMocks();
        fflib_SObjectUnitOfWork mockUOW = (fflib_SObjectUnitOfWork)mocks.mock(fflib_SObjectUnitOfWork.class);


        OpsSetupFormSelector mockOpsSetupFormSelector = (OpsSetupFormSelector)mocks.mock(OpsSetupFormSelector.class);
        AccountSelector mockAccountSelector = (AccountSelector)mocks.mock(AccountSelector.class);
        ProductItemSelector mockProductItemSelector = (ProductItemSelector)mocks.mock(ProductItemSelector.class);
        OpportunitySelector mockOpportunitySelector = (OpportunitySelector)mocks.mock(OpportunitySelector.class);
        ContactSelector mockContactSelector = (ContactSelector)mocks.mock(ContactSelector.class);
        AccountBankAccountSelector mockAccountBankAccountSelector = (AccountBankAccountSelector)mocks.mock(AccountBankAccountSelector.class);

        Id mockAccountId = fflib_IDGenerator.generate(Account.SObjectType);
        Account mockAccount = new Account(Id = mockAccountId);
        List<Account> mockAccounts = new List<Account> { mockAccount };

        Id mockContactId = fflib_IDGenerator.generate(Contact.SObjectType);
        Contact mockContact = new Contact(Id = mockContactId, AccountId = mockAccountId);
        List<Contact> mockContacts = new List<Contact> {mockContact};

        Id mockPaymentId = fflib_IDGenerator.generate(Payment__c.SObjectType);

        Id mockOpportunityId = fflib_IDGenerator.generate(Opportunity.SObjectType);
        Opportunity mockOpportunity = new Opportunity(Id = mockOpportunityId, AccountId = mockAccountId, Legal_representative__c = mockContactId, Standard_Payment__c = 'No', Payment__c = mockPaymentId);

        Id mockBankAccountId = fflib_IDGenerator.generate(Bank_Account__c.SObjectType);

        Id mockAccountBankAccountId = fflib_IDGenerator.generate(Acount_Bank_Account_Relationship__c.SObjectType);
        Acount_Bank_Account_Relationship__c mockAccountBankAccount = new Acount_Bank_Account_Relationship__c(Id = mockAccountBankAccountId, Account__c = mockAccountId, Bank_Account__c = mockBankAccountId);
        List<Acount_Bank_Account_Relationship__c> mockAccountBankAccounts = new List<Acount_Bank_Account_Relationship__c> { mockAccountBankAccount };

        Id mockProductId = fflib_IDGenerator.generate(Product_Item__c.SObjectType);
        Product_Item__c mockProduct = new Product_Item__c(Id = mockProductId, Opportunity__c = mockOpportunityId);
        List<Product_Item__c> mockProducts = new List<Product_Item__c> { mockProduct };

        Id mockOpsSetupValFormId = fflib_IDGenerator.generate(Ops_Setup_Validation_Form__c.SObjectType);
        Ops_Setup_Validation_Form__c mockOpsSetupValForm = new Ops_Setup_Validation_Form__c(Id = mockOpsSetupValFormId, Account_Information__c = mockAccountId, Opportunity__c = mockOpportunityId);
        Map<Id, Ops_Setup_Validation_Form__c> mockOpsSetupValFormsByAccountId = new Map<Id, Ops_Setup_Validation_Form__c> {mockAccountId => mockOpsSetupValForm};

        mocks.startStubbing();

        mocks.when(mockOpsSetupFormSelector.sObjectType()).thenReturn(Ops_Setup_Validation_Form__c.SObjectType);
        mocks.when(mockOpsSetupFormSelector.selectOpsSetupFormForPartner(mockOpportunityId, new Set<Id> {mockAccountId})).thenReturn(new Map<Id, Ops_Setup_Validation_Form__c>());

        mocks.when(mockAccountSelector.sObjectType()).thenReturn(Account.SObjectType);
        mocks.when(mockAccountSelector.selectFieldsToSetUp(new Set<Id> {mockAccountId})).thenReturn(mockAccounts);

        mocks.when(mockProductItemSelector.sObjectType()).thenReturn(Product_Item__c.SObjectType);
        mocks.when(mockProductItemSelector.selectProductForSetUp(mockOpportunityId)).thenReturn(mockProducts);

        mocks.when(mockOpportunitySelector.sObjectType()).thenReturn(Opportunity.SObjectType);
        mocks.when(mockOpportunitySelector.selectOpportunityForPartnerOpsSetupForm(mockOpportunityId)).thenReturn(mockOpportunity);

        mocks.when(mockContactSelector.sObjectType()).thenReturn(Contact.SObjectType);
        mocks.when(mockContactSelector.selectContactsForSetUp(new Set<Id> {mockContactId})).thenReturn(mockContacts);

        mocks.when(mockAccountBankAccountSelector.sObjectType()).thenReturn(Acount_Bank_Account_Relationship__c.SObjectType);
        mocks.when(mockAccountBankAccountSelector.selectBankAccToSetUp(new Set<Id> {mockAccountId})).thenReturn(mockAccountBankAccounts);

        mocks.stopStubbing();

        Application.Selector.setMock(mockOpportunitySelector);
        Application.Selector.setMock(mockOpsSetupFormSelector);
        Application.Selector.setMock(mockAccountSelector);
        Application.Selector.setMock(mockProductItemSelector);
        Application.Selector.setMock(mockContactSelector);
        Application.Selector.setMock(mockAccountBankAccountSelector);
        Application.UnitOfWork.setMock(mockUOW);

        Test.startTest();

        //OpsSetupFormService.createOpsFormForAccounts(mockOpportunityId, new Set<Id> {mockAccountId}, opsSetupRTId);

        /*OpsSetupFormPartnerServiceImpl impl = new OpsSetupFormPartnerServiceImpl();
        impl.createOpsFormForAccounts(mockOpportunityId, new Set<Id> {mockAccountId});*/

        OpsSetupFormService.createOpsFormForAccounts(mockOpportunityId, new Set<Id> {mockAccountId}, 'Partner_Flow_Ops_Form');

        Test.stopTest();

    }

    @IsTest
    static void createOpsFormForAccountsFailure_Test() {
        Id mockAccountId = fflib_IDGenerator.generate(Account.SObjectType);
        Id mockOpportunityId = fflib_IDGenerator.generate(Opportunity.SObjectType);

        try {
            OpsSetupFormService.createOpsFormForAccounts(mockOpportunityId, new Set<Id> {mockAccountId}, 'Gyms_Bank_Account');
        } catch (Exception e) {
            system.assert(e instanceof OpsSetupFormService.OppsSetupFormServiceException);
        }
    }
}