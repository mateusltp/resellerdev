public class GymActivitiesController {
    
    @AuraEnabled
    public static List < GymActivities__c > fetchAccount(String searchKeyWord) {
        String searchKey = '%' + searchKeyWord + '%';
        List < GymActivities__c > returnList = new List < GymActivities__c > ();
        List < GymActivities__c > lstOfAccount = [select id, Label__c from GymActivities__c
                                                  where Label__c LIKE: searchKey ORDER BY Label__c ASC LIMIT 500];
        for (GymActivities__c acc: lstOfAccount) {
            returnList.add(acc);
        }
        return returnList;
    }
    
    @AuraEnabled
    public static List<GymActivities__c> getContacts() {
        List<GymActivities__c> totalList = new List<GymActivities__c>();
        for(GymActivities__c cc : [SELECT id, Label__c from GymActivities__c ORDER BY Label__c ASC]){
            totalList.add(cc);
        }	
        return totalList;
    }
    
    @AuraEnabled
    public static void addParentAccount(String ParentId , List<String> lstOfContactIds){
        list<Gym_Activity__c> lstContactstoInsert = new list<Gym_Activity__c>();
        List<GymActivities__c> gymActivitiesQuery = [SELECT id, Label__c FROM GymActivities__c where id IN : lstOfContactIds ];
        for(GymActivities__c gymActivity : gymActivitiesQuery){
            Gym_Activity__c oContact = new Gym_Activity__c();
            oContact.Name = gymActivity.Label__c;
            oContact.Product_Item__c = ParentId;
            lstContactstoInsert.add(oContact);
        }
        Database.SaveResult[] srList = Database.insert(lstContactstoInsert, false);
        
        for (Database.SaveResult sr : srList) {
            if (sr.isSuccess()) {
                System.debug('Successfully inserted Activity. Activity ID: ' + sr.getId());
            }
            else {
                for(Database.Error objErr : sr.getErrors()) {
                    System.debug('The following error has occurred.');  
                    System.debug(objErr.getStatusCode() + ': ' + objErr.getMessage());
                    System.debug('Activity oject field which are affected by the error: ' + objErr.getFields());
                }
            }
        }
        
    }
}