/**
 * @description       : 
 * @author            : AlyssonMota
 * @group             : 
 * @last modified on  : 06-18-2021
 * @last modified by  : Alysson Mota
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   06-18-2021   Alysson Mota   Initial Version
**/

// This class is not used anymore
global class CreateStepsForOpportunitiesBatch implements Database.Batchable<sObject>, Database.Stateful {
	global Database.QueryLocator start(Database.BatchableContext bc) {
        
        String query = 'SELECT Id, Name, Step_Number__c, CreatedDate, LastModifiedDate, Achieved_Field_Changed_Date__c FROM Step_Towards_Success1__c WHERE Achieved_Field_Changed_Date__c = null';
        
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext bc, List<Step_Towards_Success1__c> scope){
        List<Step_Towards_Success1__c> enablersToUpdate = new List<Step_Towards_Success1__c>();
        
        for (Step_Towards_Success1__c enabler : scope) {
            enabler.Achieved_Field_Changed_Date__c = enabler.LastModifiedDate.date();
        	enablersToUpdate.add(enabler);
        }
        
        update enablersToUpdate;
    }
    
    global void finish(Database.BatchableContext bc){
        
    }   
}