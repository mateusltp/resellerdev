public with sharing class SelfCheckoutLeadCommand extends AbstractCommand{

    override
    public void execute(){

        SelfCheckoutLeadRequest selfCheckoutLeadRequest = (SelfCheckoutLeadRequest)
                                                     event.getPayloadFromJson(SelfCheckoutLeadRequest.class);
        
        new SelfCheckoutLeadService(selfCheckoutLeadRequest).execute();
    }
}