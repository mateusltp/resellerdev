/**
 * @description       : 
 * @author            : alysson.mota@gympass.com
 * @group             : 
 * @last modified on  : 03-08-2022
 * @last modified by  : alysson.mota@gympass.com
**/
public with sharing class SelectPartnersAccountsController {
    
    @AuraEnabled(cacheable=true)
    public static Object buildNetwork(Id recordId, String recordTypeDevName){
        System.debug('build network new');
        try {
            Schema.sObjectType entityType = recordId.getSObjectType();

            if (entityType == Opportunity.sObjectType) {
                Opportunity opp = [SELECT Id, AccountId FROM Opportunity WHERE Id =: recordId];
                Id accountId = opp.accountId;
                return buildNetworkForOpportunityId(accountId, opp.Id, recordTypeDevName);
            } else if (entityType == Product_Item__c.sObjectType) {
                return buildNetworkForProductId(recordId, recordTypeDevName);
            } else {
                return new List<Account>();
            }
        } catch(Exception e) {
            throw new SelectPartnersAccountsControllerException(e.getMessage());
        }
    }

    private static Object buildNetworkForOpportunityId(Id accountId, Id oppId, String recordTypeDevName) {
        System.debug('buildNetworkForOpportunityId');
        System.debug(oppId);
        try {
            return AccountService.buildNetworkForContracts(accountId, oppId, recordTypeDevName);
        } catch(Exception e){
            System.debug('Error');
            System.debug(e.getMessage());
            if(e.getMessage().contains('No Implementation registered for record type')){
                System.debug('No Implementation registered for record type ' + recordTypeDevName);
                return new List<Account>();
            }
            throw new SelectPartnersAccountsControllerException(e.getMessage());
        } 
    }
    
    private static Object buildNetworkForProductId(Id recordId, String recordTypeDevName) {
        System.debug('buildNetworkForProductId');
        try {
            List<Product_Opportunity_Assignment__c> poaLst = selectPoasForProductId(recordId);
            
            if (poaLst.size() > 0) {
                Product_Opportunity_Assignment__c poa =  selectPoasForProductId(recordId).get(0);
                Id accId = poa.OpportunityMemberId__r.Account__c;
                return AccountService.buildNetwork(accId,recordTypeDevName);
            } else {
                throw new SelectPartnersAccountsControllerException('This product does not have an standard Product Assignment');
            }
        } catch(Exception e){
            if(e.getMessage().contains('No Implementation registered for record type')){
                System.debug('No Implementation registered for record type ' + recordTypeDevName);
                return new List<Account>();
            }
            throw new SelectPartnersAccountsControllerException(e.getMessage());
        }
    }

    private static List<Product_Opportunity_Assignment__c> selectPoasForProductId(Id recordId) {
        ProductOpportunityAssignmentSelector poaSelector = new ProductOpportunityAssignmentSelector();
        List<Product_Opportunity_Assignment__c> poaLst = poaSelector.selectCapByProductId(new Set<Id>{recordId});
        return poaLst;
    }

    private class SelectPartnersAccountsControllerException extends Exception {
        
    }
}