/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 07-30-2021
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/
public with sharing class CustomNotificationTypeSelector extends ApplicationSelector {

    public List<Schema.SObjectField> getSObjectFieldList() {
		return new List<Schema.SObjectField> {
            CustomNotificationType.Id,
            CustomNotificationType.DeveloperName
		};
	}

    public Schema.SObjectType getSObjectType() {
		return CustomNotificationType.sObjectType;
	}

    public List<CustomNotificationType> selectById(Set<Id> ids) {
		return (List<CustomNotificationType>) super.selectSObjectsById(ids);
	}
    
    public CustomNotificationType selectCustomNotificationTypeByDeveloperName(String aDevName) {

        return ((CustomNotificationType)Database.query(newQueryFactory().
	 									setCondition('DeveloperName =: aDevName').
	 									toSOQL()
     								));			

    }
}