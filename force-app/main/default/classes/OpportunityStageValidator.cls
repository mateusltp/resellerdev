/**
 * Created by gympasser on 22/03/2022.
 */

public with sharing class OpportunityStageValidator {
    private PS_Constants constants = PS_Constants.getInstance();

    /* Collections */
    private Map<Id, Opportunity> opportunitiesByIds;
    @TestVisible
    private List<Opportunity_Stage_Validation_Mapping__mdt> mappingMdt;
    private List<Product_Item__c> productItems;
    //private List<Product_Item__c> productItemsChild;
    private Set<Id> accountIds;
    @TestVisible
    private Map<Id, Account> accountsByIds;
    private Map<Id, List<Contact>> contactsListByAccountId;
    private Map<Id, List<AccountContactRelation>> contactRelationsListByAccountId;
    private Set<Id> paymentIds;
    @TestVisible
    private List<Payment__c> payments;
    @TestVisible
    private Map<Id, List<FileObject__c>> fileObjectsByAccountIds;
    private Map<Id, List<Product_Activity_Relationship__c>> activitiesByProductId;
    private Map<Id, List<Product_Assignment__c>> assignmentsByProductId;
    @TestVisible
    private Map<Id, List<Product_Item__c>> productItemsByOpportunityId;
    private Map<Id, List<Product_Assignment__c>> childProductAssignmentsByOpportunityId;
    private Map<Id, List<Acount_Bank_Account_Relationship__c>> bankAccountsByAccountId;
    private Map<Id, List<Opening_Hours__c>> openingHoursByAccountId;
    private Map<Id, List<APXT_Redlining__Contract_Agreement__c>> contractAgreementsByOpportunityId;
    @TestVisible
    private Map<Id, List<Product_Opportunity_Assignment__c>> productOppportunityAssigByOppId;
    private Map<Id, Set<Id>> accountOppMembersByOppId;
    private Set<Id> opportunityIdsRequiringCmsApproval = new Set<Id>();

    /* Selectors */
    private ProductItemSelector productItemSelector;
    private ProductActivityRelationshipSelector productActivityRelationshipSelector;
    private ProductAssignmentSelector productAssignmentSelector;
    private AccountSelector accountSelector;
    private ContactSelector contactSelector;
    private AccountContactRelationSelector accountContactRelationSelector;
    private PaymentSelector paymentSelector;
    private AccountBankAccountSelector accountBankAccountSelector;
    private OpeningHourSelector openingHoursSelector;
    private ContentVersionSelector contentVersionSelector;
    private FileObjectSelector fileObjectSelector;
    private ContractAgreementSelector contractAgreementSelector;
    private AccountOpportunitySelector opportunityMemberSelector;
    Id childProductRt = Schema.SObjectType.Product_Item__c.getRecordTypeInfosByDeveloperName().get(constants.PRODUCT_ITEM_CHILD_RT_PARTNER_FLOW).getRecordTypeId();


    public OpportunityStageValidator(Map<Id, Opportunity> opportunitiesByIds) {
        this.opportunitiesByIds = opportunitiesByIds;
    }

    public void validate() {
        initializeData();
        for (Opportunity opportunity : opportunitiesByIds.values()) {
            switch on opportunity.StageName {
                when 'Oportunidade Validada Após 1ª Reunião' {
                    validateStage_OpportunityValidated(opportunity);
                }
                when 'Review Opportunity' {
                    validateStage_ReviewOpportunity(opportunity);
                }
                when 'Proposta Enviada' {
                    validateStage_OfferSent(opportunity);
                }
                when 'Proposta Aprovada' {
                    validateStage_OfferApproved(opportunity);
                }
                when 'Review Contract' {
                    validateStage_ReviewContract(opportunity);
                }
                when 'Contrato Assinado' {
                    validateStage_SignedContract(opportunity);
                }
                when 'Set Up Validation' {
                    validateStage_SetUpValidation(opportunity);
                }
            }
        }
    }

    private void validateStage_OpportunityValidated(Opportunity opportunity) {
        validateOpportunityData(opportunity, getMappings(constants.OPPORTUNITY_OBJECT, constants.OPPORTUNITY_STAGE_OPORTUNIDADE_VALIDADA));
        if (opportunity.hasErrors()) return;
        validateProductData(opportunity, getMappings(constants.PRODUCT_ITEM_OBJECT, constants.OPPORTUNITY_STAGE_OPORTUNIDADE_VALIDADA));
        if (opportunity.hasErrors()) return;
        validateActivitiesData(opportunity);
        if (opportunity.hasErrors()) return;
        validateAssignmentsData(opportunity);
        if (opportunity.hasErrors()) return;
        validateAccountData(opportunity, getMappings(constants.ACCOUNT_OBJECT, constants.OPPORTUNITY_STAGE_OPORTUNIDADE_VALIDADA));
    }

    private void validateStage_ReviewOpportunity(Opportunity opportunity) {
        validateOpportunityData(opportunity, getMappings(constants.OPPORTUNITY_OBJECT, constants.OPPORTUNITY_STAGE_REVIEW_OPPORTUNITY));
        if (opportunity.hasErrors()) return;
        validateAccountData(opportunity, getMappings(constants.ACCOUNT_OBJECT, constants.OPPORTUNITY_STAGE_REVIEW_OPPORTUNITY));
    }

    private void validateStage_OfferSent(Opportunity opportunity) {
        validateOpportunityData(opportunity, getMappings(constants.OPPORTUNITY_OBJECT, constants.OPPORTUNITY_STAGE_PROPOSTA_ENVIADA));
        if (opportunity.hasErrors()) return;
        validateContactRelationsData(opportunity);
        if (opportunity.hasErrors()) return;
        validateAccountExclusivityApproval(opportunity);
        if (opportunity.hasErrors()) return;
        validateAccountCMSValues(opportunity);


    }

    private void validateStage_OfferApproved(Opportunity opportunity) {
        validateOpportunityData(opportunity, getMappings(constants.OPPORTUNITY_OBJECT, constants.OPPORTUNITY_STAGE_PROPOSTA_APROVADA));
        if (opportunity.hasErrors()) return;
        validateAccountData(opportunity, getMappings(constants.ACCOUNT_OBJECT, constants.OPPORTUNITY_STAGE_PROPOSTA_APROVADA));
    }

    private void validateStage_ReviewContract(Opportunity opportunity) {
        validateContractAgreementsData(opportunity);
    }

    private void validateStage_SignedContract(Opportunity opportunity) {
        validateOpportunityData(opportunity, getMappings(constants.OPPORTUNITY_OBJECT, constants.OPPORTUNITY_SIGNED_CONTRACT));
        if (opportunity.hasErrors()) return;
        validateAccountData(opportunity, getMappings(constants.ACCOUNT_OBJECT, constants.OPPORTUNITY_SIGNED_CONTRACT));
        if (opportunity.hasErrors()) return;
        validatePaymentData(opportunity, getMappings(constants.PAYMENT_OBJECT, constants.OPPORTUNITY_SIGNED_CONTRACT));
    }

    private void validateStage_SetUpValidation(Opportunity opportunity) {
        List<APXT_Redlining__Contract_Agreement__c> contractlst= contractAgreementsByOpportunityId.get(opportunity.Id);
        Boolean flag = true;
        if(!contractlst.isEmpty()){
            for (APXT_Redlining__Contract_Agreement__c contractAgreement : contractlst) {
                if (contractAgreement.Type_of_contract__c == constants.INTENTION_CONTRACT) {
                    flag= false;
                }
            }
        }
        if(flag){
            validateOpportunityData(opportunity, getMappings(constants.OPPORTUNITY_OBJECT, constants.OPPORTUNITY_SET_UP_VALIDATION));
            if (opportunity.hasErrors()) return;
        }
    }

    @TestVisible
    private void initializeData() {
        this.productItemSelector = (ProductItemSelector)Application.Selector.newInstance(Product_Item__c.SObjectType);
        this.productActivityRelationshipSelector = (ProductActivityRelationshipSelector)Application.Selector.newInstance(Product_Activity_Relationship__c.SObjectType);
        this.productAssignmentSelector = (ProductAssignmentSelector)Application.Selector.newInstance(Product_Assignment__c.SObjectType);
        this.accountSelector = (AccountSelector)Application.Selector.newInstance(Account.SObjectType);
        this.contactSelector = (ContactSelector)Application.Selector.newInstance(Contact.SObjectType);
        this.accountBankAccountSelector = (AccountBankAccountSelector)Application.Selector.newInstance(Acount_Bank_Account_Relationship__c.SObjectType);
        this.paymentSelector = (PaymentSelector)Application.Selector.newInstance(Payment__c.SObjectType);
        this.openingHoursSelector = (OpeningHourSelector)Application.Selector.newInstance(Opening_Hours__c.SObjectType);
        this.contentVersionSelector = (ContentVersionSelector)Application.Selector.newInstance(ContentVersion.SObjectType);
        this.fileObjectSelector = (FileObjectSelector)Application.Selector.newInstance(FileObject__c.SObjectType);
        this.contractAgreementSelector = (ContractAgreementSelector)Application.Selector.newInstance(APXT_Redlining__Contract_Agreement__c.SObjectType);
        this.accountContactRelationSelector = (AccountContactRelationSelector)Application.Selector.newInstance(AccountContactRelation.SObjectType);
        this.opportunityMemberSelector = (AccountOpportunitySelector)Application.Selector.newInstance(Account_Opportunity_Relationship__c.sobjectType);


        // get mappings for objects/fields to be validated metadata
        this.mappingMdt = [SELECT MasterLabel, Object__r.QualifiedApiName, Field_to_Validate__r.Label, Field_to_Validate__r.QualifiedApiName, Stage__c FROM Opportunity_Stage_Validation_Mapping__mdt];
        // get parent products data
        this.productOppportunityAssigByOppId = opportunityMemberSelector.selectProductAssignmentToOpportunitySDGGrid(opportunitiesByIds.keySet());
        Set<Id> productIds = new Set<Id>();

        for(Id oppId : productOppportunityAssigByOppId.keySet()){
            for(Product_Opportunity_Assignment__c prodOppAssign : productOppportunityAssigByOppId.get(oppId)){
                if ((prodOppAssign.ProductAssignmentId__r.ProductId__r.RecordTypeId == childProductRt && prodOppAssign.ProductAssignmentId__r.ProductId__r.Parent_Product__c != null) ||
                        (prodOppAssign.ProductAssignmentId__r.ProductId__r.RecordTypeId != childProductRt && prodOppAssign.ProductAssignmentId__r.ProductId__r.Parent_Product__c == null)) {
                    productIds.add(prodOppAssign.ProductAssignmentId__r.ProductId__c);
                }
            }
        }

        // get opportunity members
        groupOpportunityMembersByOpportunityId(opportunityMemberSelector.selectByOppIdToClone(opportunitiesByIds.keySet()));

        Map<Id, Product_Item__c> producItemsById = productItemSelector.selectProductByIdToClone(productIds);

        groupProductsByOpportunityId( producItemsById );

        Set<Id> parentProductIds = new Set<Id>();
        for (Product_Item__c productItem : producItemsById.values()) {
            parentProductIds.add(productItem.Id);
        }

        // get activities related to parent products data
        groupActivitiesByProductId(productActivityRelationshipSelector.selectAllByProductIds(parentProductIds));

        // get product assignments for parent and child products data
        groupAssignmentsByProductId(productAssignmentSelector.selectAllByProductId(parentProductIds));

        Map<Id, Product_Assignment__c> productAssignmentsChild = productAssignmentSelector.selectAllFieldsFromProductAssignWithCap(parentProductIds);

        if(!productAssignmentsChild.isEmpty()){
            groupChildProductsByOpportunities( productAssignmentsChild );
        } else {
            /** it means we dont have a child assign product, so it might be a single partner negotiation (non-network deal) */
            productAssignmentsChild = productAssignmentSelector.selectAllFieldsFromProductAssignWithCapById(parentProductIds);
            groupChildProductsByOpportunities( productAssignmentsChild );
        }

        // get accounts data
        getAccountIds(opportunitiesByIds.values());
        this.accountsByIds = accountSelector.selectAccountToClone(accountIds);

        // get contacts data
        groupContactsByAccountId(contactSelector.selectAllByAccountId(accountIds));

        // get contact relations data
        groupContactRelationsByAccountId(accountContactRelationSelector.selectContactRelationById(accountIds));

        // get payments data
        getPaymentIds(opportunitiesByIds.values());
        this.payments = paymentSelector.selectAllById(paymentIds);

        // get bank accounts data
        groupBankAccountsByOpportunityAccountId(accountBankAccountSelector.selectAccountBankAccountRelationshipsByAccountIds(accountIds));

        // get opening hours data
        groupOpeningHoursByOpportunityAccountId(openingHoursSelector.selectAllById(accountIds));

        // get contract agreements
        groupContractAgreementsByAccountId(contractAgreementSelector.byOpportunityId(opportunitiesByIds.keySet()));

        // get files
        this.fileObjectsByAccountIds = fileObjectSelector.selectByAccountId(accountIds);
    }

    @TestVisible
    private void validateOpportunityData(Opportunity opportunity, Map<String, String> fieldMappings) {
        List<String> fieldsFailedValidation = new List<String>();
        for (String field : fieldMappings.keySet()) {
            if (opportunity.get(field) == null) {
                fieldsFailedValidation.add(fieldMappings.get(field));
            }

            if( field == constants.OPS_SETUP_STATUS_FIELD ){
                if( Opportunity.get(field) == constants.OPS_SETUP_IN_PROGRESS ) {
                    fieldsFailedValidation.add(constants.OPS_SETUP_ERROR_STATUS);
                }
            }
        }
        if(!fieldsFailedValidation.isEmpty()) {
            opportunity.addError(constants.OPPORTUNITY_FIELDS_MISSING+fieldsFailedValidation);
        }
    }

    @TestVisible
    private void validateProductData(Opportunity opportunity, Map<String, String> fieldMappings) {

        List<String> fieldsFailedValidation = new List<String>();
        if (!productItemsByOpportunityId.containsKey(opportunity.Id)) {
            opportunity.addError(constants.OPPORTUNITY_NO_PRODUCTS);
            return;
        }

        for (Product_Item__c productItem : productItemsByOpportunityId.get(opportunity.Id)) {
            if(productItem != null && productItem.Parent_Product__c == null){
                for (String field : fieldMappings.keySet()) {
                    if (productItem.get(field) == null) {
                        fieldsFailedValidation.add(fieldMappings.get(field));
                    }
                }
            }

            if(!fieldsFailedValidation.isEmpty()) {
                opportunity.addError(constants.PRODUCTS_FIELDS_MISSING+fieldsFailedValidation);
            }

        }

    }

    @TestVisible
    private void validateAssignmentsData(Opportunity opportunity) {
        if (!childProductAssignmentsByOpportunityId.containsKey(opportunity.Id)) {
            opportunity.addError(constants.OPPORTUNITY_NO_CHILD_PRODUCTS);
            return;
        }
        Set<String> oppProductItemsWithAssignments = new Set<String>();
        Set<String> productsWithoutMarketPrice = new Set<String>();
        for (Product_Assignment__c childAssignment : childProductAssignmentsByOpportunityId.get(opportunity.Id)) {
            if (childAssignment.Market_Price__c == null) {
                productsWithoutMarketPrice.add(childAssignment.ProductId__r.Name);
            }
            oppProductItemsWithAssignments.add(childAssignment.ProductId__r.Name);
        }
        if (!productsWithoutMarketPrice.isEmpty()) {
            opportunity.addError(constants.PRODUCTS_NO_MARKET_PRICE+new List<String>(productsWithoutMarketPrice));
        } else {
            Boolean allProductsHaveAssignments = true;
            for (Product_Item__c productItem : productItemsByOpportunityId.get(opportunity.Id)) {
                if (!oppProductItemsWithAssignments.contains(productItem.Name)) {
                    productsWithoutMarketPrice.add(productItem.Name);
                    allProductsHaveAssignments = false;
                }
            }
            if (!allProductsHaveAssignments) {
                opportunity.addError(constants.PRODUCTS_NO_MARKET_PRICE+new List<String>(productsWithoutMarketPrice));
            }
        }

    }

    @TestVisible
    private void validateAccountData(Opportunity opportunity, Map<String, String> fieldMappings) {
        Set<String> fieldsFailedValidation = new Set<String>();
        for (Id accountId : accountOppMembersByOppId.get(opportunity.Id)) {
            Account account = accountsByIds.get(accountId);
            for (String field : fieldMappings.keySet()) {
                if (account.get(field) == null) {
                    fieldsFailedValidation.add(fieldMappings.get(field));
                }
            }
        }

        if(!fieldsFailedValidation.isEmpty()) {
            opportunity.addError(constants.ACCOUNT_FIELDS_MISSING+String.join(new List<String>(fieldsFailedValidation), ', '));
        }
    }

    @TestVisible
    private void validateAccountExclusivityApproval(Opportunity opportunity) {
        List<String> accountsFailedValidation = new List<String>();
        Set<String> statusToBlock = new Set<String> {constants.APPROVAL_PENDING, constants.APPROVAL_REJECTED};
        for (Id accountId : accountOppMembersByOppId.get(opportunity.Id)) {
            Account account = accountsByIds.get(accountId);
            if(account.Wishlist__c && statusToBlock.contains(account.Approval_Status_Exclusivity__c)) {
                accountsFailedValidation.add(account.Name);
            }
        }
        if(!accountsFailedValidation.isEmpty()) {
            opportunity.addError(constants.ACCOUNT_EXCLUSIVITY_APPROVAL+accountsFailedValidation);
        }
    }

    @TestVisible
    private void validateAccountCMSValues(Opportunity opportunity) {
        if (opportunity.Approval_Status_CMS__c == constants.APPROVAL_APPROVED) return;
        List<String> accountsFailedValidation = new List<String>();
        for (Id accountId : accountOppMembersByOppId.get(opportunity.Id)) {
            Account account = accountsByIds.get(accountId);
            if(account.Wishlist__c && account.Approval_Status_CMS__c != constants.APPROVAL_APPROVED && (account.Does_this_partner_use_CMS__c == constants.NO || account.Partner_agreed_to_have_integration_API__c == constants.NO)) {
                accountsFailedValidation.add(account.Name);
            }
        }
        System.debug('## accountsFailedValidation: '+accountsFailedValidation);
        if(!accountsFailedValidation.isEmpty()) {
            Partner_Event__e partnerEvent = new Partner_Event__e(
                    Object_Id__c = opportunity.Id,
                    Type__c = constants.UPDATE_REQUIRES_CMS_APPROVAL_TRUE
            );
            EventBus.publish(partnerEvent);
            opportunity.addError(constants.OPPORTUNITY_REQUIRES_APPROVAL);
            //opportunity.addError(constants.ACCOUNT_CMS_APPROVAL+accountsFailedValidation);
        }
    }

    /**
    * @description 
    * alysson.mota@gympass.com | 06-30-2022 - Add getLegalRepContactRelsByAccountId() call
    * @param opportunity 
    **/
    @TestVisible
    private void validateContactRelationsData(Opportunity opportunity) {
        if (!contactRelationsListByAccountId.containsKey(opportunity.AccountId)) {
            opportunity.addError(constants.ACCOUNT_NO_CONTACTS);
            return;
        }

        List<String> fieldsFailedValidation = new List<String>();
        Boolean foundContactValid = true; 
        
        Map<Id, List<AccountContactRelation>> legalRepContactRelsByAccountId = getLegalRepContactRelsByAccountId(contactRelationsListByAccountId);
        
        if (legalRepContactRelsByAccountId.containsKey(opportunity.AccountId)) {
            foundContactValid = false;

            for (AccountContactRelation accountContactRelation : legalRepContactRelsByAccountId.get(opportunity.AccountId)) {
                Integer countValidFields = 0;

                if (String.isBlank(accountContactRelation.Contact.FirstName)) {
                    fieldsFailedValidation.add(accountContactRelation.Contact.LastName + ': First Name');
                }
                else {
                    countValidFields++;
                }
                if (String.isBlank(accountContactRelation.Contact.Email)) {
                    fieldsFailedValidation.add(accountContactRelation.Contact.Name + ': Email');
                }
                else {
                    countValidFields++;
                }
                if (String.isBlank(accountContactRelation.Contact.Phone)) {
                    fieldsFailedValidation.add(accountContactRelation.Contact.Name + ': Business Phone');
                }
                else {
                    countValidFields++;
                }
    
                if (countValidFields == 3) {
                    foundContactValid = true;
                }
            }
        }

        if (!foundContactValid) {
            opportunity.addError(constants.CONTACTS_FIELDS_MISSING+fieldsFailedValidation);
        }
    }

    /**
    * Filter only the Legal Rep contacts
    * @author alysson.mota@gympass.com | 06-30-2022 
    * @param contactRelationsListByAccountId 
    * @return Map<Id, List<AccountContactRelation>> 
    **/
    private Map<Id, List<AccountContactRelation>> getLegalRepContactRelsByAccountId(Map<Id, List<AccountContactRelation>> contactRelationsListByAccountId) {
        Map<Id, List<AccountContactRelation>> legalRepContactRelsByAccountId = new Map<Id, List<AccountContactRelation>>();
        
        for (Id accountId : contactRelationsListByAccountId.keySet()) {
            List<AccountContactRelation> accContactRelLst = contactRelationsListByAccountId.get(accountId);

            for (AccountContactRelation accContactRel : accContactRelLst) {          
                if (accContactRel.Contact.Type_of_Contact__c != null && accContactRel.Contact.Type_of_Contact__c.contains('Legal Representative')) {
                    
                    if (legalRepContactRelsByAccountId.containsKey(accContactRel.AccountId)) {
                        legalRepContactRelsByAccountId.get(accContactRel.AccountId).add(accContactRel);
                    } else {
                        legalRepContactRelsByAccountId.put(accContactRel.AccountId, new List<AccountContactRelation>{accContactRel});
                    }
                }
            }
        }

        return legalRepContactRelsByAccountId;
    }

    @TestVisible
    private void validateActivitiesData(Opportunity opportunity) {
        List<String> productsWithoutActivities = new List<String>();

        if (!productItemsByOpportunityId.containsKey(opportunity.Id)) {
            opportunity.addError(constants.OPPORTUNITY_NO_PRODUCTS);
            return;
        }

        for (Product_Item__c product : productItemsByOpportunityId.get(opportunity.Id)) {
            if(product != null){
                if (!activitiesByProductId.containsKey(product.Id) && product.recordTypeId != childProductRt) {
                    productsWithoutActivities.add(product.Name);
                }
            }
        }
        if(!productsWithoutActivities.isEmpty()) {
            opportunity.addError(constants.PRODUCTS_NO_ACTIVITIES+productsWithoutActivities);
        }
    }

    @TestVisible
    private void validatePaymentData(Opportunity opportunity, Map<String, String> fieldMappings) {
        List<String> fieldsFailedValidation = new List<String>();
        for (Payment__c payment : payments) {
            if (payment.Id == opportunity.Payment__c) {
                for (String field : fieldMappings.keySet()) {
                    if (payment.get(field) == null) {
                        fieldsFailedValidation.add(fieldMappings.get(field));
                    }
                }
                if(!fieldsFailedValidation.isEmpty()) {
                    opportunity.addError(constants.PAYMENT_FIELDS_MISSING+fieldsFailedValidation);
                }
            }
        }
    }

    @TestVisible
    private void validateBankAccountsData(Opportunity opportunity) {
        if(!bankAccountsByAccountId.containsKey(opportunity.AccountId)) {
            opportunity.addError(constants.ACCOUNT_NO_BANK_ACCOUNTS);
        }
    }

    @TestVisible
    private void validateOpeningHoursData(Opportunity opportunity) {
        if(!openingHoursByAccountId.containsKey(opportunity.AccountId)) {
            opportunity.addError(constants.ACCOUNT_NO_OPENING_HOURS);
        }
    }

    @TestVisible
    private void validateContractAgreementsData(Opportunity opportunity) {
        if(!contractAgreementsByOpportunityId.containsKey(opportunity.Id)) {
            opportunity.addError(constants.OPPORTUNITY_NO_CONTRACTS);
        }
    }

    @TestVisible
    private void validateFiles(Opportunity opportunity) {
        List<String> requiredFilesSetUp = new List<String> {constants.FILEOBJECT_FILETYPE_W9, constants.FILEOBJECT_FILETYPE_PHOTO, constants.FILEOBJECT_FILETYPE_LOGO};

        if(!fileObjectsByAccountIds.containsKey(opportunity.AccountId)) {
            opportunity.addError(constants.ACCOUNT_NO_FILES);
            return;
        }
        for (FIleObject__c fileObject : fileObjectsByAccountIds.get(opportunity.AccountId)) {
            if(requiredFilesSetUp.contains(fileObject.File_Type__c)) {
                requiredFilesSetUp.remove(requiredFilesSetUp.indexOf(fileObject.File_Type__c));
            }
        }

        if (requiredFilesSetUp.size() > 0) {
            opportunity.addError(constants.ACCOUNT_FILES_MISSING+requiredFilesSetUp);
        }
    }

    @TestVisible
    private void getAccountIds(List<Opportunity> opportunities) {
        accountIds = new Set<Id>();
        for (Opportunity opportunity : opportunities) {
            if (accountOppMembersByOppId.get(opportunity.Id) != null) {
                accountIds.addAll(accountOppMembersByOppId.get(opportunity.Id));
            }
        }
    }

    @TestVisible
    private void getPaymentIds(List<Opportunity> opportunities) {
        paymentIds = new Set<Id>();
        for (Opportunity opportunity : opportunities) {
            paymentIds.add(opportunity.Payment__c);
        }
    }

    @TestVisible
    private void groupBankAccountsByOpportunityAccountId(List<Acount_Bank_Account_Relationship__c> bankAccountRelationships) {
        bankAccountsByAccountId = new Map<Id, List<Acount_Bank_Account_Relationship__c>>();
        for (Acount_Bank_Account_Relationship__c bankAccountRelationship : bankAccountRelationships) {
            if (bankAccountsByAccountId.containsKey(bankAccountRelationship.Account__c)) {
                bankAccountsByAccountId.get(bankAccountRelationship.Account__c).add(bankAccountRelationship);
            } else {
                bankAccountsByAccountId.put(bankAccountRelationship.Account__c, new List<Acount_Bank_Account_Relationship__c> { bankAccountRelationship });
            }
        }
    }

    @TestVisible
    private void groupOpeningHoursByOpportunityAccountId(List<Opening_Hours__c> openingHours) {
        openingHoursByAccountId = new Map<Id, List<Opening_Hours__c>>();
        for (Opening_Hours__c openingHour : openingHours) {
            if (openingHoursByAccountId.containsKey(openingHour.Account_Related__c)) {
                openingHoursByAccountId.get(openingHour.Account_Related__c).add(openingHour);
            } else {
                openingHoursByAccountId.put(openingHour.Account_Related__c, new List<Opening_Hours__c> { openingHour });
            }
        }
    }

    @TestVisible
    private void groupContractAgreementsByAccountId(List<APXT_Redlining__Contract_Agreement__c> contractAgreements) {
        contractAgreementsByOpportunityId = new Map<Id, List<APXT_Redlining__Contract_Agreement__c>>();
        for (APXT_Redlining__Contract_Agreement__c contractAgreement : contractAgreements) {
            if (contractAgreementsByOpportunityId.containsKey(contractAgreement.Opportunity__c)) {
                contractAgreementsByOpportunityId.get(contractAgreement.Opportunity__c).add(contractAgreement);
            } else {
                contractAgreementsByOpportunityId.put(contractAgreement.Opportunity__c, new List<APXT_Redlining__Contract_Agreement__c> { contractAgreement });
            }
        }
    }

    // @TestVisible
    // private void groupChildProductsByOpportunities(List<Product_Assignment__c> childProductItems) {
    //     childProductAssignmentsByOpportunityId = new Map<Id, List<Product_Assignment__c>>();
    //     for (Product_Assignment__c childProductAssignment : childProductItems) {
    //         if (childProductAssignmentsByOpportunityId.containsKey(childProductAssignment.ProductId__r.Parent_Product__r.Opportunity__c)) {
    //             childProductAssignmentsByOpportunityId.get(childProductAssignment.ProductId__r.Parent_Product__r.Opportunity__c).add(childProductAssignment);
    //         } else {
    //             childProductAssignmentsByOpportunityId.put(childProductAssignment.ProductId__r.Parent_Product__r.Opportunity__c, new List<Product_Assignment__c>{
    //                     childProductAssignment
    //             });
    //         }
    //     }
    // }

    @TestVisible
    private void groupChildProductsByOpportunities(  Map<Id,Product_Assignment__c> childProductItems) {
        childProductAssignmentsByOpportunityId = new Map<Id, List<Product_Assignment__c>>();

        for( Id oppId : productOppportunityAssigByOppId.keySet() ){
            for( Product_Opportunity_Assignment__c prodOppAssign : productOppportunityAssigByOppId.get(oppId) ){
                if( prodOppAssign.ProductAssignmentId__c != null && prodOppAssign.ProductAssignmentId__r.ProductId__c != null && childProductItems.get( prodOppAssign.ProductAssignmentId__c ) != null) {
                    if (childProductAssignmentsByOpportunityId.containsKey(oppId)) {
                        childProductAssignmentsByOpportunityId.get(oppId).add( childProductItems.get( prodOppAssign.ProductAssignmentId__c ));
                    } else {
                        childProductAssignmentsByOpportunityId.put(oppId, new List<Product_Assignment__c>{ childProductItems.get( prodOppAssign.ProductAssignmentId__c  )});
                    }
                }
            }
        }

    }

    @TestVisible
    private void groupProductsByOpportunityId( Map<Id, Product_Item__c> productsItemByIds) {

        this.productItemsByOpportunityId = new Map<Id, List<Product_Item__c>>();

        for( Id oppId : productOppportunityAssigByOppId.keySet() ){
            for( Product_Opportunity_Assignment__c prodOppAssign : productOppportunityAssigByOppId.get(oppId) ){
                if( prodOppAssign.ProductAssignmentId__c != null && prodOppAssign.ProductAssignmentId__r.ProductId__c != null) {
                    if (productItemsByOpportunityId.containsKey(oppId)) {
                        productItemsByOpportunityId.get(oppId).add( productsItemByIds.get( prodOppAssign.ProductAssignmentId__r.ProductId__c ));
                    } else {
                        productItemsByOpportunityId.put(oppId, new List<Product_Item__c>{ productsItemByIds.get( prodOppAssign.ProductAssignmentId__r.ProductId__c  )});
                    }
                }
            }
        }
    }

    @TestVisible
    private void groupOpportunityMembersByOpportunityId(List<Account_Opportunity_Relationship__c> accountOpportunityRelationships) {

        this.accountOppMembersByOppId = new Map<Id, Set<Id>>();

        for (Account_Opportunity_Relationship__c accountOpportunityRelationship : accountOpportunityRelationships) {
            if (accountOppMembersByOppId.containsKey(accountOpportunityRelationship.Opportunity__c)) {
                accountOppMembersByOppId.get(accountOpportunityRelationship.Opportunity__c).add(accountOpportunityRelationship.Account__c);
            } else {
                accountOppMembersByOppId.put(accountOpportunityRelationship.Opportunity__c, new Set<Id> { accountOpportunityRelationship.Account__c });
            }
        }
    }

    @TestVisible
    private void groupContactsByAccountId(List<Contact> contacts) {
        this.contactsListByAccountId = new Map<Id, List<Contact>>();
        for (Contact contact : contacts) {
            if (contactsListByAccountId.containsKey(contact.AccountId)) {
                contactsListByAccountId.get(contact.AccountId).add(contact);
            } else {
                contactsListByAccountId.put(contact.AccountId, new List<Contact>{
                        contact
                });
            }
        }
    }

    @TestVisible
    private void groupContactRelationsByAccountId(List<AccountContactRelation> relations) {
        this.contactRelationsListByAccountId = new Map<Id, List<AccountContactRelation>>();
        for (AccountContactRelation accountContactRelation : relations) {
            if (contactRelationsListByAccountId.containsKey(accountContactRelation.AccountId)) {
                contactRelationsListByAccountId.get(accountContactRelation.AccountId).add(accountContactRelation);
            } else {
                contactRelationsListByAccountId.put(accountContactRelation.AccountId, new List<AccountContactRelation>{
                        accountContactRelation
                });
            }
        }
    }

    @TestVisible
    private void groupActivitiesByProductId(List<Product_Activity_Relationship__c> activities) {
        this.activitiesByProductId = new Map<Id, List<Product_Activity_Relationship__c>>();
        for (Product_Activity_Relationship__c activity : activities) {
            if (activitiesByProductId.containsKey(activity.Product__c)) {
                activitiesByProductId.get(activity.Product__c).add(activity);
            } else {
                activitiesByProductId.put(activity.Product__c, new List<Product_Activity_Relationship__c>{
                        activity
                });
            }
        }
    }

    @TestVisible
    private void groupAssignmentsByProductId(List<Product_Assignment__c> assignments) {
        this.assignmentsByProductId = new Map<Id, List<Product_Assignment__c>>();
        for (Product_Assignment__c assignment : assignments) {
            if (assignmentsByProductId.containsKey(assignment.ProductId__c)) {
                assignmentsByProductId.get(assignment.ProductId__c).add(assignment);
            } else {
                assignmentsByProductId.put(assignment.ProductId__c, new List<Product_Assignment__c>{
                        assignment
                });
            }
        }
    }

    private Map<String, String> getMappings(String objectName, String stageName) {
        Map<String, String> fieldLabelsByApiName = new Map<String, String>();
        for (Opportunity_Stage_Validation_Mapping__mdt mapping : mappingMdt) {
            if(mapping.Stage__c == stageName && mapping.Object__r.QualifiedApiName == objectName) {
                fieldLabelsByApiName.put(mapping.Field_to_Validate__r.QualifiedApiName, mapping.Field_to_Validate__r.Label);
            }
        }
        return fieldLabelsByApiName;
    }



    class OpportunityStageValidator_Exception extends Exception {}

}