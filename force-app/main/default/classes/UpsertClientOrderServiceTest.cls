@IsTest
public class UpsertClientOrderServiceTest {
  @IsTest
  public static void execute() {
    Test.setMock(HttpCalloutMock.class, new HttpResponseMock());

    Account account = AccountMock.getStandard('Empresas');
    account.Send_To_Tagus__c = true;
    insert account;

    Contact contact = ContactMock.getStandard(account);
    insert contact;

    account.Attention__c = contact.Id;
    update account;

    Pricebook2 pricebook = new Pricebook2(
      CurrencyIsoCode = 'BRL',
      Id = Test.getStandardPricebookId(),
      IsActive = true
    );
    upsert pricebook;

    List<Product2> products = new List<Product2>();

    Product2 setupFee = ProductMock.getSetupFee();
    products.add(setupFee);

    Product2 accessFee = ProductMock.getAccessFee();
    products.add(accessFee);

    insert products;

    List<PricebookEntry> pricebookEntries = new List<PricebookEntry>();

    PricebookEntry setupFeePricebookEntry = PricebookMock.getPricebookEntry(
      pricebook,
      setupFee
    );
    pricebookEntries.add(setupFeePricebookEntry);

    PricebookEntry accessFeePricebookEntry = PricebookMock.getPricebookEntry(
      pricebook,
      accessFee
    );
    pricebookEntries.add(accessFeePricebookEntry);

    insert pricebookEntries;

    List<Opportunity> opportunities = new List<Opportunity>();

    Opportunity opportunity = OpportunityMock.getClientSalesSkuNewBusiness(
      account,
      pricebook
    );
    opportunity.Gympass_Entity__c = account.Id;

    opportunities.add(opportunity);

    insert opportunities;

    List<Payment__c> payments = new List<Payment__c>();

    Payment__c payment = PaymentMock.getStandard(opportunity, account);
    payments.add(payment);

    insert payments;

    List<Eligibility__c> eligibles = new List<Eligibility__c>();

    Eligibility__c eligibility = EligibilityMock.getStandard(payment);
    eligibles.add(eligibility);

    insert eligibles;

    Quote quote = QuoteMock.getStandard(opportunity);
    insert quote;

    List<QuoteLineItem> quoteLineItems = new List<QuoteLineItem>();

    QuoteLineItem setupFeeLine = QuoteLineItemMock.getSetupFee(
      quote,
      setupFeePricebookEntry
    );
    quoteLineItems.add(setupFeeLine);

    QuoteLineItem accessFeeLine = QuoteLineItemMock.getEnterpriseSubscription(
      quote,
      accessFeePricebookEntry
    );
    quoteLineItems.add(accessFeeLine);

    insert quoteLineItems;

    List<Waiver__c> waivers = new List<Waiver__c>();

    Waiver__c accessFeeWaiver = WaiverMock.getStandard(accessFeeLine);
    waivers.add(accessFeeWaiver);

    insert waivers;

    Test.startTest();

    new UpsertClientOrderService(opportunities).execute();

    Test.stopTest();

    Order order = [
      SELECT Id
      FROM Order
      WHERE OpportunityId = :opportunity.Id
      LIMIT 1
    ];

    System.assert(order != null, 'Order not created');
  }

  private class HttpResponseMock implements HttpCalloutMock {
    public HTTPResponse respond(HTTPRequest req) {
      HttpResponse respinse = new HttpResponse();
      respinse.setHeader('Content-Type', 'application/json');
      respinse.setStatusCode(200);
      return respinse;
    }
  }
}