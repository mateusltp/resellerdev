public class QualtricsMailingListDTO {        
    public class Meta {
        public String httpStatus;
        public String requestId;
    }
    
    public Result result;
    public Meta meta;
    
    public class Elements {
        public String mailingListId;
        public String name;
        public String ownerId;
        public Long lastModifiedDate;
        public Long creationDate;
        public Integer contactCount;
    }
    
    public class Result {
        public List<Elements> elements;
        public Object nextPage;
        public String Id;
    }
    
    
    public static QualtricsMailingListDTO parse(String json) {
        return (QualtricsMailingListDTO) System.JSON.deserialize(json, QualtricsMailingListDTO.class);
    }
}