/*
* @Author: Bruno Pinho
* @Date: January/2019
* @Description: Test to Trigger Framework
*/
@isTest
public with sharing class TriggerFrameworkTest
{ 
    public static testMethod void testAccountTrigger()
    {
        Event event = new Event(Subject = 'Test Event', EndDateTime = System.now().addDays(2), StartDateTime = System.now().addDays(1));
        INSERT event;
        event.Subject = 'Test';
        UPDATE event;
        DELETE event;
        UNDELETE event;
    }
}