@isTest
public class BatchTagusIntegrationTest {
    
    @isTest
    public static void testIntegrationBatchSuccessResponse() {
   		Integration_Request__c request = new Integration_Request__c();
        request.HTTP_Request_Method__c = 'POST';
        request.Endpoint__c = 'http://test.com';
        request.Payload__c = '{"teste" : "teste"}';
        request.Integration_Request_Status__c = 'Pending Integration';
        
        insert request;
        
        HTTPMockFactory mock = new HttpMockFactory(
            new BatchTagusIntegrationTest(), 
            200, 
            'OK', 
            'I found it!', 
            new Map<String,String>()
        );
        
        Test.setMock(HttpCalloutMock.class, mock);
        
        Test.startTest();
       	Database.executeBatch(new BatchTagusIntegration());
        Test.stopTest();
    }
    
    @isTest
    public static void testIntegrationBatchErrorResponse() {
   		Integration_Request__c request = new Integration_Request__c();
        request.HTTP_Request_Method__c = 'POST';
        request.Endpoint__c = 'http://test.com';
        request.Payload__c = '{"teste" : "teste"}';
        request.Integration_Request_Status__c = 'Pending Integration';
        
        insert request;
        
        HTTPMockFactory mock = new HttpMockFactory(
            new BatchTagusIntegrationTest(), 
            500, 
            'ERROR', 
            'Internal Server Error!', 
            new Map<String,String>()
        );
        
        Test.setMock(HttpCalloutMock.class, mock);
        
        Test.startTest();
       	Database.executeBatch(new BatchTagusIntegration());
        Test.stopTest();
    }
    
    @isTest
    public static void testIntegrationBatchExceptionError() {
   		Integration_Request__c request = new Integration_Request__c();
        request.HTTP_Request_Method__c = 'POST';
        request.Endpoint__c = 'http://test.com';
        request.Payload__c = '{"teste" : "teste"}';
        request.Integration_Request_Status__c = 'Pending Integration';
        
        insert request;
        
        HTTPMockFactory mock = new HttpMockFactory(
            new BatchTagusIntegrationTest(), 
            401, 
            'ERROR', 
            'Unauthorized', 
            new Map<String,String>()
        );
        
        Test.setMock(HttpCalloutMock.class, mock);
        
        Test.startTest();
       	Database.executeBatch(new BatchTagusIntegration());
        Test.stopTest();
    }
}