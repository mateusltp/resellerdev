public without sharing class SmbSkuWaiversController {

    private class GenericException extends Exception{

    }
    
    @AuraEnabled
    public static DataWrapper getData(String productId){
        try {

            List<String> recordTypeNames = new List<String>{'Months after Launch'};

            List<Waiver__c> waivers;

            if(!String.isBlank(productId)) {
                waivers = [SELECT Id, RecordTypeId, Position__c, Percentage__c, Duration__c, End_Date__c, End_Date_Text__c, Quote_Line_Item__c, QuoteId__c
                        FROM Waiver__c 
                        WHERE Quote_Line_Item__c = :productId AND RecordType.Name IN :recordTypeNames 
                        ORDER BY Position__c ASC];
            }
            else {
                throw new GenericException('The productId is blank or null.');
            }  

            QuoteLineItem quoteLineItem = [SELECT Quote.Opportunity.Current_Billing_Day__c, Quote.Opportunity.Current_Order_End_Date__c, QuoteId
                                            FROM QuoteLineItem 
                                            WHERE Id = :productId LIMIT 1];
            
            List<DurationOption> durationOptions = new List<DurationOption>();

            Date currentDate = Date.today();
            Integer billingDay = Integer.valueOf(quoteLineItem.Quote.Opportunity.Current_Billing_Day__c);
            Date orderEndDate = quoteLineItem.Quote.Opportunity.Current_Order_End_Date__c;

            if(billingDay == null)
                throw new GenericException('Current Billing Day is null.');
            
            if(orderEndDate == null)
                throw new GenericException('Current Order End Date is null.');

            Date nextDate = Date.newInstance(currentDate.year(), currentDate.month(), billingDay); 

            while(nextDate <= orderEndDate) {
                if(nextDate > currentDate ) {
                    String dateString = nextDate.month() + '/' + nextDate.day() + '/' + nextDate.year();
                    DurationOption option = new DurationOption();
                    option.label = 'to ' + dateString;
                    option.value = dateString;
                    option.endDate = nextDate;
                    durationOptions.add(option);
                }    
                nextDate = nextDate.addMonths(1);
            }

            DataWrapper data = new DataWrapper();
            data.waivers = waivers;
            data.durationOptions = durationOptions;
            data.quoteId = quoteLineItem.quoteId;

            return data;            


        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }


    public class DataWrapper {
        @AuraEnabled
        public List<Waiver__c> waivers;
        @AuraEnabled
        public Id quoteId;
        @AuraEnabled
        public List<DurationOption> durationOptions;
    }

    public class DurationOption {
        @AuraEnabled
        public String label;
        @AuraEnabled
        public String value;      
        @AuraEnabled
        public Date endDate;
    }

    @AuraEnabled
    public static Boolean checkApprovalNeed(Id productId, List<Waiver__c> waivers, List<Waiver__c> waiversToDelete){
        try {
            
            return SmbSkuWaiverHelper.checkApprovalNeed(productId, waivers, waiversToDelete);
            
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

}