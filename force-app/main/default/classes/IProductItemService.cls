/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 05-17-2022
 * @last modified by  : alysson.mota@gympass.com
**/
public interface IProductItemService {

    Product_Item__c createNewProduct (Id opportunityId, Id accountId, Product_Item__c prodItem, Set<Id> gymActivities, Map<String, List<Commercial_Condition__c>> commercialConditions, List<Threshold__c> thresholds );
    Map<String, List<Commercial_Condition__c>> deleteProductRelationships(Id productId, List<Commercial_Condition__c> commercialConditions);
    void deleteProductsAndRelationships(List<Product_Item__c> prodItemLst);
}