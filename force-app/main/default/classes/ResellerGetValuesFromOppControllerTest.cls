@isTest
public with sharing class ResellerGetValuesFromOppControllerTest {
    
    @TestSetup
    static void makeData(){
        Id IndirectRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();

        Account account = new Account();  
        account.Name = 'teste acc';
        account.Legal_Document_Type__c = 'CNPJ';
        account.Id_Company__c = '56947401000108';
        account.Razao_Social__c = 'TESTE';
        account.Website = 'testeaccount.com';
        account.Industry = 'Airlines';
        account.BillingCountry = 'Brazil';
        account.RecordTypeId = IndirectRecordTypeId;    
        insert account;

        Contact contact = new Contact();
        contact.FirstName = 'contactLegacy';
        contact.LastName = 'contactLegacy';
        contact.AccountId = account.Id;
        contact.Email = 'contactLegacy@gmail.com';
        contact.Role__c = 'FINANCE';
        insert contact;
        
        Legacy_Partner_Price__c legacy = new Legacy_Partner_Price__c();
        legacy.Legacy_Price__c = 3;
        legacy.Account__c = account.Id;
        legacy.isActive__c = true;
        insert legacy;

        Pricebook2 pb = new Pricebook2();
        pb.Name = 'Reseller 5k';
        pb.Business_Model__c = 'Subsidy 5k';
        pb.IsActive = true;
        insert pb;
        
        Pricebook2 standardPricebook = new Pricebook2(
            Id = Test.getStandardPricebookId(),
            IsActive = true
        );
        update standardPricebook; 

        Product2 prod = new Product2();
        prod.Name = 'Prod Teste 1000';
        prod.Family_Member_Included__c = false;
        prod.Minimum_Number_of_Employees__c = 0;
        prod.Maximum_Number_of_Employees__c = 1000;
        insert prod;
        
        PricebookEntry standardPBE = new PricebookEntry(Pricebook2Id = standardPricebook.Id, Product2Id = prod.Id, UnitPrice = 8, IsActive = true, UseStandardPrice = false);
        insert standardPBE;
        
        PricebookEntry pbEntry = new PricebookEntry();
        pbEntry.Pricebook2Id = pb.Id;
        pbEntry.Product2Id = prod.Id;
        pbEntry.UnitPrice = 9;
        pbEntry.CurrencyIsoCode = 'USD';
        insert pbEntry; 

        Opportunity opp = new Opportunity(
            Name = 'Indirect Channel', 
            StageName = 'Creation',
            Probability = 10,
            AccountId = account.Id,
            RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Indirect_Channel').getRecordTypeId(),
            CloseDate = Date.Today().addDays(90),
            Pricebook2Id = pb.Id,
            B_M__c = 'Total Subsidy',
            TotalOpportunityQuantity = 999,
            CurrencyIsoCode = 'USD'
        );
        insert opp;
        
    }

    @isTest
    public static void testgetPriceValues(){

        Opportunity opp = [ SELECT Id FROM Opportunity LIMIT 1 ];


        Test.startTest();
        ResellerPriceResponse response = ResellerGetValuesFromOppController.getPriceValues(opp.Id);
        System.assertNotEquals(null, response);
        Test.stopTest();

    }
}