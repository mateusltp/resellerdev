public class GympassCore {
    
    public static void createSMBCompany(Opportunity opportunity) {
        
        if (opportunity.SyncedQuoteId != null) {
            List<Quote> listQuotes = [SELECT
                                        Id, Are_Family_Members_Included2__c, License_Fee_Waiver__c, Setup_Fee__c, License_Fee_Frequency__c, LF_PO_Number_Required__c, MB_PO_Number_Required__c, 
                                        Payment_Method_License_Fee__c, Payment_Terms_for_Setup_Fee__c, Contact_Permission__c, License_Fee_cut_off_date__c, Employee_Company_ID__c, Employee_Corporate_Email__c, 
                                        Employee_Name__c, Employee_National_ID_if_applicable__c, Submission_Date__c, Total_Setup_Fee_Value__c, Payment_method__c, Employee_Registration_Method__c, 
                                        Unique_Identifier__c, Administrator__c, Finance__c, Operations__c, ContactId, Pricebook2Id, Gym__r.Id, Gym__r.Market_Value_Basic_II__c, Gym__r.Market_Value_Basic_I__c, 
                                        Gym__r.Market_Value_Black__c,Gym__r.Market_Value_Gold_Plus__c, Gym__r.Market_Value_Gold__c, Gym__r.Market_Value_Platinum__c, Gym__r.Market_Value_Silver__c, 
                                        Gym__r.Market_Value_Starter__c, Gym__r.Monthly_Fee_Basic_II__c, Gym__r.Monthly_Fee_Basic_I__c, Gym__r.Monthly_Fee_Black__c, Gym__r.Monthly_Fee_Family_Members_Basic_II__c, 
                                        Gym__r.Monthly_Fee_Family_Members_Basic_I__c, Gym__r.Monthly_Fee_Family_Members_Black__c, Gym__r.Monthly_Fee_Family_Members_Gold_Plus__c, 
                                        Gym__r.Monthly_Fee_Family_Members_Gold__c, Gym__r.Monthly_Fee_Family_Members_Platinum__c, Gym__r.Monthly_Fee_Family_Members_Silver__c, 
                                        Gym__r.Monthly_Fee_Family_Members_Starter__c, Gym__r.Monthly_Fee_Gold_Plus__c, Gym__r.Monthly_Fee_Gold__c, Gym__r.Monthly_Fee_Platinum__c, Gym__r.Monthly_Fee_Silver__c, 
                                        Gym__r.Monthly_Fee_Starter__c, Gym__r.Name
                                      FROM
                                        Quote
                                      WHERE
                                        Id = :opportunity.SyncedQuoteId
                                      LIMIT
                                        1];
            
            if (!listQuotes.isEmpty()) {
                Quote quote = listQuotes[0];
            
                List<QuoteLineItem> listQuoteLineItems = [SELECT
                                                            Id, Product2.ProductCode, UnitPrice, Quantity, PricebookEntryId, Product2Id, Flagship_Date__c, Contract_Type__c, Max_Total_Montly_Investment_Cofee__c, Min_Total_Montly_Investment_Cofee__c, Target_Enrollment_Rate__c, Description, Type__c
                                                          FROM
                                                            QuoteLineItem
                                                          WHERE
                                                            QuoteId = :quote.Id];
                
                SMBPayload smbPayload = new SMBPayload();
                smbPayload.opportunityData = opportunity;
                smbPayload.quoteData = quote;
                smbPayload.listQuoteLineItems = listQuoteLineItems;
                
                Map<String, String> mapHeader = new Map<String, String>();
                mapHeader.put('Content-Type', 'application/json');
                
                Utils.CalloutWrapper payloadSMB = new Utils.CalloutWrapper();
                payloadSMB.endpoint = 'https://gymtegration-stg.herokuapp.com/company';
                payloadSMB.method = 'POST';
                payloadSMB.body = JSON.serialize(smbPayload);
                payloadSMB.headerParams = mapHeader;
                
                Utils.doHTTPCallout(payloadSMB);
            }
        }
    }
    
    public class SMBPayload {
        public Opportunity opportunityData {get; set;}
        public Quote quoteData {get; set;}
        public List<QuoteLineItem> listQuoteLineItems {get; set;}
    }
}