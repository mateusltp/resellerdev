/**
 * @description       : 
 * @author            : alysson.mota@gympass.com
 * @group             : 
 * @last modified on  : 02-25-2022
 * @last modified by  : alysson.mota@gympass.com
**/
public with sharing class ProductAssignmentTriggerHandler extends TriggerHandler{
    /**
    * Runs on after insert event
    * @author alysson.mota@gympass.com | 02-21-2022 
    **/
    public override void afterInsert() {
        new ProductAssignmentService().createAndSyncPAsAndRelatedRecords((Map<Id, Product_Assignment__c>)Trigger.newMap);
    }
    
    /**
    * Runs on after update event
    * @author alysson.mota@gympass.com | 02-21-2022 
    **/
    public override void afterUpdate() {
        new ProductAssignmentService().evaluateAndSyncPAsAndRelatedRecords((Map<Id, Product_Assignment__c>)Trigger.newMap, (Map<Id,Product_Assignment__c>) Trigger.oldMap);
    }

    /**
    * Runs on before delete event 
    * @author alysson.mota@gympass.com | 02-21-2022 
    **/
    public override void beforeDelete() {
        new ProductAssignmentService().deleteRelatedPaRecords((Map<Id,Product_Assignment__c>) Trigger.oldMap);
    }
}