/**
 * @author vncferraz
 * 
 * Provide multiple objects Wrapper for OfferQueueProcessor for Subsidiary type of request
 */
public class SubsidiaryOfferWrapper extends OfferWrapper { 
    
    public SubsidiaryOfferWrapper() {
        super();
    }
    
    override
    public ProcessResult save(ProcessResult result){
         
        Savepoint sp = Database.setSavepoint();

        try{
            saveAccountInOpportunity();
            result.status = 'Success';
        }catch(System.DMLException e){

            Database.rollback(sp);
            result.statusDetail = 'Unexpected Error, please contact your Admin: ' + e.getMessage();
        }

        return result;    
    }
}