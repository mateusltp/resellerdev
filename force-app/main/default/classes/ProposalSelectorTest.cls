/**
 * @description       : 
 * @author            : gft.jorge.stevaux@ext.gympass.com
 * @group             : 
 * @last modified on  : 09-14-2021
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/

@isTest(seeAllData=false)

private class ProposalSelectorTest {

    @TestSetup
    static void createData(){
            
        Account lAcc = PartnerDataFactory.newAccount();              
        Database.insert( lAcc );

        Contact lContact = PartnerDataFactory.newContact(lAcc.Id);
        Database.insert(lContact);

        Bank_Account__c lBankAcct = PartnerDataFactory.newBankAcct();
        Database.insert(lBankAcct);

        Acount_Bank_Account_Relationship__c lBankAcctRel = PartnerDataFactory.newBankAcctRel(lAcc.Id, lBankAcct);
        Database.insert(lBankAcctRel);
                                  
        Opportunity lOpp = PartnerDataFactory.newOpportunity( lAcc.Id, 'Partner_Wishlist_Renegotiation' );     
        Database.insert( lOpp );
        
        Quote lQuote = PartnerDataFactory.newQuote(lOpp);
        Database.insert(lQuote);
    }

    @isTest
    static void getSObjectFieldList_Test(){

        List<Schema.SObjectField> expectedResult = new List<Schema.SObjectField> {
            Quote.Id,
            Quote.Name
		};

        Test.startTest();
        System.assertEquals(expectedResult, new ProposalSelector().getSObjectFieldList());
        Test.stopTest();
    }

    @isTest
    static void getSObjectType_Test(){

        Test.startTest();
        system.assertEquals(Quote.sObjectType, new ProposalSelector().getSObjectType());
        Test.stopTest();
    }

    @isTest
    static void selectById_Test(){

        List<Quote> qLst = [SELECT Id, Name FROM Quote LIMIT 1];
        Set<Id> qIdSet = (new Map<Id, Quote>(qLst)).keySet();

        Test.startTest();
        System.assertEquals(qLst.get(0).id, new ProposalSelector().selectById(qIdSet).get(0).id);
        Test.stopTest();
    }

    @isTest
    static void selectByOppIdToClone_Test(){

        List<Opportunity> oppLst = [SELECT Id, Name FROM Opportunity];
        List<Quote> qLst = [SELECT Id, Name FROM Quote WHERE OpportunityId IN : oppLst];
        Set<Id> oppIds = (new Map<Id, Opportunity>(oppLst)).keySet();


        Test.startTest();
        System.assertEquals(qLst.get(0).id, new ProposalSelector().selectByOppIdToClone(oppIds).get(0).id);
        Test.stopTest();

    }

    
    @isTest
    static void selectQuoteByOppId_Test(){
        List<Opportunity> oppLst = [SELECT Id, Name FROM Opportunity];  
        List<Quote> proposalLst = [SELECT Id, Name FROM Quote];  
        Test.startTest();
            System.assertEquals(proposalLst.get(0).id, (new ProposalSelector().selectQuoteByOppId(oppLst)).get(oppLst[0].Id).Id);
        Test.stopTest();

    }
      
}