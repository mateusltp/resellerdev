/**
 * @description       : 
 * @author            : Mateus Augusto - GFT (moes@gft.com)
 * @group             : 
 * @last modified on  : 04-29-2022
 * @last modified by  : Mateus Augusto - GFT (moes@gft.com)
**/
@isTest
public with sharing class OpportunityTriggerPricebookHelperTest {

    @TestSetup
    static void makeData(){

        User thisUser = [SELECT Id FROM User WHERE Id = : UserInfo.getUserId()];
        System.runAs(thisUser){
            Account account = new Account();  
            account.Name = 'Test Account';
            account.Legal_Document_Type__c = 'CNPJ';
            account.Id_Company__c = '56947401000155';
            account.Razao_Social__c = 'TESTE';
            account.Website = 'teste.com';
            account.Industry = 'Airlines';             
            account.BillingCountry = 'Brazil';
            account.Type = 'Prospect';
            account.Industry = 'Government or Public Management';
            account.Indirect_Channel_Pricebook__c = 'Standard';
            account.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Reseller').getRecordTypeId();
            insert account;
            
            account.IsPartner = true;
            update account;
            
            Contact contact = new Contact();
            contact.FirstName = 'testeMateus';
            contact.LastName = 'testeNovo';
            contact.AccountId = account.Id;
            contact.Email = 'testeMateus@bemarius.example';
            contact.Role__c = 'FINANCE';
            insert contact;
            
           
            List<Profile> profiles = [Select Id From Profile Where Name='Reseller Community'];
            
            UserRole role = new UserRole();
            role.OpportunityAccessForAccountOwner = 'Edit';
            role.PortalType = 'Partner';
            role.PortalAccountId = account.Id;
            insert role;
        }

        Pricebook2 standardPricebook = new Pricebook2( Id = Test.getStandardPricebookId(), IsActive = true);
        update standardPricebook;

        Pricebook2 customPricebook = new Pricebook2( Name = 'Br', Country__c = 'Brazil', IsActive = true);
        insert customPricebook;

        Pricebook2 exclusivePricebook = new Pricebook2( Name = 'Exclusive', Country__c = 'Brazil', Business_Model__c = 'Exclusive', IsActive = true);
        insert exclusivePricebook;

        Pricebook2 resellerPricebook = new Pricebook2( Name = 'Reseller', Country__c = 'Brazil', Business_Model__c = 'Subsidy', IsActive = true);
        insert resellerPricebook;

        Pricebook2 intermediationPricebook = new Pricebook2( Name = 'Intermediation', Country__c = 'Brazil', Business_Model__c = 'Intermediation', IsActive = true);
        insert intermediationPricebook;

        Pricebook2 reseller5kPricebook = new Pricebook2( Name = 'Reseller 5k', Country__c = 'Brazil', Business_Model__c = 'Subsidy 5k', IsActive = true);
        insert reseller5kPricebook;

        Pricebook2 reseller15kPricebook = new Pricebook2( Name = 'Reseller 15k', Country__c = 'Brazil', Business_Model__c = 'Subsidy 15k', IsActive = true);
        insert reseller15kPricebook;

        Product2 product = new Product2();
        product.Name = 'Enterprise Subscription';
        product.Minimum_Number_of_Employees__c = 0;
        product.Maximum_Number_of_Employees__c = 900;
        product.Family = 'Enterprise Subscription';
        insert product;

        PricebookEntry pbEntry = new PricebookEntry();
        pbEntry.Product2Id = product.Id;
        pbEntry.Pricebook2Id = Test.getStandardPricebookId();
        pbEntry.UnitPrice = 100;
        pbEntry.CurrencyISOCode = 'USD';
        insert pbEntry;

        Set<Id> pbIds = new Set<Id>{customPricebook.Id, exclusivePricebook.Id, intermediationPricebook.Id, resellerPricebook.Id, reseller5kPricebook.Id, reseller15kPricebook.Id };
        List<PricebookEntry> customEntry = new List<PricebookEntry>();

        for(Id id : pbIds)
            customEntry.add(new PricebookEntry( Product2Id = product.Id,
                                                Pricebook2Id = id,
                                                UnitPrice = 100,
                                                CurrencyISOCode = 'USD'));

        insert customEntry;

        Id IndirectRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
        
        Account account = new Account();  
        account.Name = 'teste acc';
        account.Legal_Document_Type__c = 'CNPJ';
        account.Id_Company__c = '56947401000108';
        account.Razao_Social__c = 'TESTE';
        account.Website = 'testeaccount.com';
        account.Industry = 'Airlines';
        account.BillingCountry = 'Brazil';
        account.RecordTypeId = IndirectRecordTypeId;    
        insert account;

        
    }

    @isTest 
    public static void createOppIndirectSubsidy(){
        
        Account acc = [ SELECT Id FROM Account WHERE Id_Company__c = '56947401000108' LIMIT 1];
        Account reseller = [ SELECT Id, Indirect_Channel_Pricebook__c FROM Account WHERE Id_Company__c = '56947401000155' LIMIT 1];
        Opportunity newOpp = new Opportunity(
            Name = 'Indirect Channel', 
            StageName = 'Creation',
            Probability = 10,
            AccountId = acc.Id,
            RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Indirect_Channel_New_Business').getRecordTypeId(),
            CloseDate = Date.Today().addDays(90),
            Pricebook2Id = Test.getStandardPricebookId(),
            TotalOpportunityQuantity = 900,
            Quantity_Offer_Number_of_Employees__c= 900,
            B_M__c = 'Total Subsidy',
            Reseller_del__c = reseller.Id,
            CurrencyIsoCode = 'USD',
            OwnerId = UserInfo.getUserId());

        Test.startTest();
        insert newOpp;
        System.assertNotEquals(null, newOpp.Id);
        Test.stopTest();

    }

    @isTest 
    public static void createOppIndirectSubsidy5k(){
        
        Account acc = [ SELECT Id FROM Account WHERE Id_Company__c = '56947401000108' LIMIT 1];
        Account reseller = [ SELECT Id, Indirect_Channel_Pricebook__c FROM Account WHERE Id_Company__c = '56947401000155' LIMIT 1];
        Opportunity newOpp = new Opportunity(
            Name = 'Indirect Channel', 
            StageName = 'Creation',
            Probability = 10,
            AccountId = acc.Id,
            RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Indirect_Channel_New_Business').getRecordTypeId(),
            CloseDate = Date.Today().addDays(90),
            Pricebook2Id = Test.getStandardPricebookId(),
            TotalOpportunityQuantity = 900,
            Quantity_Offer_Number_of_Employees__c= 900,
            B_M__c = 'Total Subsidy',
            Reseller_del__c = reseller.Id,
            CurrencyIsoCode = 'USD',
            OwnerId = UserInfo.getUserId());

        Test.startTest();
        reseller.Indirect_Channel_Pricebook__c = '5k FTEs';
        update reseller;
        insert newOpp;
        System.assertNotEquals(null, newOpp.Id);
        Test.stopTest();

    }

    @isTest 
    public static void createOppIndirectSubsidy15k(){
        
        Account acc = [ SELECT Id FROM Account WHERE Id_Company__c = '56947401000108' LIMIT 1];
        Account reseller = [ SELECT Id, Indirect_Channel_Pricebook__c FROM Account WHERE Id_Company__c = '56947401000155' LIMIT 1];
        Opportunity newOpp = new Opportunity(
            Name = 'Indirect Channel', 
            StageName = 'Creation',
            Probability = 10,
            AccountId = acc.Id,
            RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Indirect_Channel_New_Business').getRecordTypeId(),
            CloseDate = Date.Today().addDays(90),
            Pricebook2Id = Test.getStandardPricebookId(),
            TotalOpportunityQuantity = 900,
            Quantity_Offer_Number_of_Employees__c= 900,
            B_M__c = 'Total Subsidy',
            Reseller_del__c = reseller.Id,
            CurrencyIsoCode = 'USD',
            OwnerId = UserInfo.getUserId());

        Test.startTest();
        reseller.Indirect_Channel_Pricebook__c = '15k FTEs';
        update reseller;
        insert newOpp;
        System.assertNotEquals(null, newOpp.Id);
        Test.stopTest();

    }

    @isTest 
    public static void createOppIndirectSubsidyIntermediation(){
        
        Account acc = [ SELECT Id FROM Account WHERE Id_Company__c = '56947401000108' LIMIT 1];
        Account reseller = [ SELECT Id, Indirect_Channel_Pricebook__c FROM Account WHERE Id_Company__c = '56947401000155' LIMIT 1];
        Opportunity newOpp = new Opportunity(
            Name = 'Indirect Channel', 
            StageName = 'Creation',
            Probability = 10,
            AccountId = acc.Id,
            RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Indirect_Channel_New_Business').getRecordTypeId(),
            CloseDate = Date.Today().addDays(90),
            Pricebook2Id = Test.getStandardPricebookId(),
            TotalOpportunityQuantity = 900,
            Quantity_Offer_Number_of_Employees__c= 900,
            B_M__c = 'Intermediation',
            Reseller_del__c = reseller.Id,
            CurrencyIsoCode = 'USD',
            OwnerId = UserInfo.getUserId());

        Test.startTest();
        insert newOpp;
        System.assertNotEquals(null, newOpp.Id);
        Test.stopTest();

    }

    @isTest 
    public static void createOppIndirectSubsidyExclusive(){
        
        Account acc = [ SELECT Id FROM Account WHERE Id_Company__c = '56947401000108' LIMIT 1];
        Account reseller = [ SELECT Id, Indirect_Channel_Pricebook__c FROM Account WHERE Id_Company__c = '56947401000155' LIMIT 1];
        Opportunity newOpp = new Opportunity(
            Name = 'Indirect Channel', 
            StageName = 'Creation',
            Probability = 10,
            AccountId = acc.Id,
            RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Indirect_Channel_New_Business').getRecordTypeId(),
            CloseDate = Date.Today().addDays(90),
            Pricebook2Id = Test.getStandardPricebookId(),
            TotalOpportunityQuantity = 900,
            Quantity_Offer_Number_of_Employees__c= 900,
            B_M__c = 'Exclusive',
            Reseller_del__c = reseller.Id,
            CurrencyIsoCode = 'USD',
            OwnerId = UserInfo.getUserId());

        Test.startTest();
        insert newOpp;
        System.assertNotEquals(null, newOpp.Id);
        Test.stopTest();

    }

    @isTest
    public static void testGetPricebookNamesFromMetadata(){

        Test.startTest();
        OpportunityTriggerPricebookHelper help = new OpportunityTriggerPricebookHelper();
        Set<String> priceNames = help.getPricebookNamesFromMetadata();
        System.assertEquals(true, priceNames.isEmpty());
        Test.stopTest();
    }

    @isTest
    public static void testGetCountryToPricebookMap(){

        List<Pricebook2> pb = [ SELECT Id, Country__c FROM Pricebook2 ];

        Test.startTest();
        OpportunityTriggerPricebookHelper help = new OpportunityTriggerPricebookHelper();
        Map<String, Pricebook2> retorno = help.getCountryToPricebookMap(pb);

        System.assertEquals(false, retorno.isEmpty());
        Test.stopTest();
    }
    
    @isTest
    public static void getMapClassInstanceByRTIdTest(){

        List<Pricebook2> pb = [ SELECT Id, Country__c FROM Pricebook2 ];
        Map< String , String > lMapRtIdPricebookId = new Map< String , String >();

        Test.startTest();
        OpportunityTriggerPricebookHelper help = new OpportunityTriggerPricebookHelper();
        lMapRtIdPricebookId = help.getMapClassInstanceByRTId();

        System.assertEquals(false, lMapRtIdPricebookId.isEmpty());
        Test.stopTest();
    }    
}