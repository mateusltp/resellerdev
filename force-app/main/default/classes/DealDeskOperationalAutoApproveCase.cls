/**
 * @description       : 
 * @author            : roei@gft.com
 * @group             : 
 * @last modified on  : 02-24-2022
 * @last modified by  : vads@gft.com
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   12-09-2020   roei@gft.com   Initial Version
**/
public without sharing class DealDeskOperationalAutoApproveCase {
    private Id gDealDeskOperationalRecTypeId = 
        Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Deal_Desk_Operational').getRecordTypeId();
    private Map< String , Set< String > > gMapCaseFieldStandardValues = new Map< String , Set< String > >();
    private Map< String , Set< String > > gMapCaseFieldStandardValuesLowerCase = new Map< String , Set< String > >();
    private Map< Id , List< Payment__c > > mpPayments = new Map < Id , List< Payment__c > >();    
    Map< String, List<Assert_Data__c> > previousCaseInAssertDataToCompare = new Map < String, List<Assert_Data__c> >();    
    Map< String, Map<String, String> > previousCaseInAssertDataForEvaluation = new Map < String, Map<String, String> >();    
    Map<String, String> oldCaseESFrequency = new Map <String, String>();
    Map<String, String> caseFieldAndCustomField = new Map <String, String>();
    Map < String, Map<String, String> > FieldAndValueAssertData = new Map < String, Map<String, String> >();
    Map <String, String> recordType = new Map <String, String>();
    Map<String, String> mapAppItemCaseField = new Map<String, String>();
    Map<String, String> mapAppItemCustomField = new Map<String, String>();
    Map<String, String> mapApprovalItemValue = new Map<String, String>();
    Map <String, String> businessModel = new Map <String, String>();
    Map <String, Integer> numberEligibles = new Map <String, Integer>();

    public DealDeskOperationalAutoApproveCase(){
        gMapCaseFieldStandardValues = getFieldsAndStandardValuesFromMdt();        
    }
    
    public void autoApproveCaseWhenStandardAnswers( List< Case > aLstNewCase ){
                
        if( gMapCaseFieldStandardValues.isEmpty() ){ return; }
        Set< Id > lSetOppIdToApproveBySpecialist = new Set< Id >();
        DealDeskOperationApproveSameOpportunity caseToUpdt = new DealDeskOperationApproveSameOpportunity();

        mapAppItemCaseField = mapApprovalItemsMetadata();
        mpPayments = getPayments(aLstNewCase);
        populateMapAssertDataToCompare(aLstNewCase);                
        generateMapAssertDataForEvaluation(aLstNewCase);
        getRecordTypeOpportunityByCase(aLstNewCase);
        for( Case iNewCase : aLstNewCase ){
           //Map to get Approval Items needed
           mapApprovalItemValue = new Map<String, String>();
           if( iNewCase.RecordTypeId == gDealDeskOperationalRecTypeId && iNewCase.Status == 'New'){
               if(!isStandardAnswers(iNewCase) || checkIfCaseValuesChanged(iNewCase)){
                   checkIfCaseChangesAlreadyApproved(iNewCase);
                }
                if(mapApprovalItemValue.isEmpty()){ //When no Approval Items needed, Case can be auto approved
                iNewCase.Status = 'Approved';
                iNewCase.OwnerId = UserInfo.getUserId();
                iNewCase.Deal_Desk_Evaluation__c = 'Auto approved.';
                iNewCase.Auto_Approved_Case__c = true;
                    
                if( String.isNotBlank( iNewCase.OpportunityId__c ) ){ 
                    lSetOppIdToApproveBySpecialist.add( iNewCase.OpportunityId__c );
                }
                caseToUpdt.approveAtTheSameOpportunity(iNewCase);

                }else{
                    //Store on the Case Description the Approval Items
                    iNewCase.Description = generateCaseDescription(mapApprovalItemValue);
                }
            }
        }
        approveOpportunities( lSetOppIdToApproveBySpecialist );
    }    
    
    private Boolean checkIfCaseValuesChanged (Case aCase){        
        if(previousCaseInAssertDataToCompare.get(aCase.OpportunityId__c).isEmpty()){return true; }        

        for( Assert_Data__c caseInAssertData : previousCaseInAssertDataToCompare.get(aCase.OpportunityId__c) ){
            if(string.valueOf(aCase.get(caseInAssertData.Field_Name__c)) != caseInAssertData.Old_Value__c ){                
                if(caseInAssertData.Old_Value__c != null && aCase.get(caseInAssertData.Field_Name__c) == null){ continue; }
                return true;
            }
        }
        return false;               
    }

    private void checkIfCaseChangesAlreadyApproved (Case aCase){
        if(FieldAndValueAssertData.get(aCase.OpportunityId__c).isEmpty()){ return;}

        Map<String, String> mpFieldAndValueAssertData = FieldAndValueAssertData.get(aCase.OpportunityId__c);               

        for (String caseInAssertData : mpFieldAndValueAssertData.keyset()){
            String approvalItemName = returnApprovalItemName(caseInAssertData);
            if(String.isBlank(approvalItemName) || !mapApprovalItemValue.containsKey(approvalItemName)) continue;
            if(caseInAssertData.equals('prof_services_main_fee_payment_due_days__c') || caseInAssertData.equals('prof_services_main_fee_billing_day__c') || caseInAssertData.equals('setup_fee_payment_due_days__c')){
                mapApprovalItemValue.remove(approvalItemName);
            }
        
            if( gMapCaseFieldStandardValuesLowerCase.get(caseInAssertData) != null && aCase.get(caseInAssertData) != null && !gMapCaseFieldStandardValuesLowerCase.get(caseInAssertData).contains((String)aCase.get(caseInAssertData)) ){
                if( caseInAssertData.contains('setup') ){
                    if(!recordType.get(aCase.OpportunityId__c).equals('Client_Success_Renegotiation') && !recordType.get(aCase.OpportunityId__c).equals('SMB_Success_Renegotiation')){
                        if( !gMapCaseFieldStandardValuesLowerCase.get(caseInAssertData).contains(mpFieldAndValueAssertData.get(caseInAssertData)) ){
                            if( mpFieldAndValueAssertData.get(caseInAssertData) == string.valueOf( aCase.get(caseFieldAndCustomField.get(caseInAssertData)) ) ){                                                            
                                mapApprovalItemValue.remove(approvalItemName);
                            }
                        }
                    }else{
                        mapApprovalItemValue.remove(approvalItemName);
                    }
                }else{
                    if( !gMapCaseFieldStandardValuesLowerCase.get(caseInAssertData).contains(mpFieldAndValueAssertData.get(caseInAssertData)) ){
                        if( mpFieldAndValueAssertData.get(caseInAssertData) == string.valueOf( aCase.get(caseFieldAndCustomField.get(caseInAssertData)) ) ){                                                        
                            mapApprovalItemValue.remove(approvalItemName);
                        }
                    }
                }
            }else if ( caseInAssertData.contains('due_days') ){ 
                checkIfDueDaysNeedApproval(caseInAssertData, aCase, mpFieldAndValueAssertData, approvalItemName);                            
            }
        }
    }
    
    private void checkIfDueDaysNeedApproval(String caseInAssertData, Case aCase, Map<String, String> mpFieldAndValueAssertData, String approvalItemName){
        List<Payment__c> ltPayment = mpPayments.get(aCase.QuoteId__c);        
        if( aCase.get(caseInAssertData) != null ){
            
            if (caseInAssertData.equals('mf_eligibility_es_payment_due_days__c') || caseInAssertData.equals('prof_services_one_fee_payment_due_days__c')){                
                if( aCase.get(caseInAssertData).equals('Custom') && Integer.valueOf(aCase.get(caseFieldAndCustomField.get(caseInAssertData))) > 30 && string.valueOf(aCase.get(caseFieldAndCustomField.get(caseInAssertData))) > mpFieldAndValueAssertData.get(caseInAssertData) ){                    
                    return;
                }else{
                    mapApprovalItemValue.remove(approvalItemName);
                }
            }else if(caseInAssertData.equals('es_payment_due_days__c') ){
                for(Payment__c pay : ltPayment){
                    if( pay.Payment_Code__c.contains('Enterprise_Subscription') ){
                        if ( pay.Frequency__c.equals('Monthly') || pay.Frequency__c.equals('Quarterly') || pay.Frequency__c.equals('Biannually') ){
                            if( aCase.get(caseInAssertData).equals('60 days') ){
                                break;
                            }else if( aCase.get(caseInAssertData).equals('Custom') && Integer.valueOf(aCase.get(caseFieldAndCustomField.get(caseInAssertData))) > 30 && string.valueOf(aCase.get(caseFieldAndCustomField.get(caseInAssertData))) > mpFieldAndValueAssertData.get(caseInAssertData) ){                                
                                break;
                            }else{
                               mapApprovalItemValue.remove(approvalItemName);
                               break;
                            }
                        }else if ( pay.Frequency__c.equals('Yearly') ){
                            if( aCase.get(caseInAssertData).equals('Custom') && Integer.valueOf(aCase.get(caseFieldAndCustomField.get(caseInAssertData))) > 60 && string.valueOf(aCase.get(caseFieldAndCustomField.get(caseInAssertData))) > mpFieldAndValueAssertData.get(caseInAssertData) ){                                                            
                                break;
                            }else{
                                mapApprovalItemValue.remove(approvalItemName);
                                break;
                            }
                        }
                    }
                }
            }
        }
    }

    private void populateMapAssertDataToCompare(List<Case> ltCase){
        Set<Id> opportunity = new Set<Id>();

        for(Case newCase : ltCase){
            opportunity.add(newCase.OpportunityId__c);
        }      
        
        for (Case actualCase : ltCase){
            previousCaseInAssertDataToCompare.put(actualCase.OpportunityId__c, new List<Assert_Data__c>());

            for (Assert_Data__c oldCase : [SELECT Opportunity__c, Old_Value__c, Field_Name__c, Fee_Type__c, Object_Name__c FROM Assert_Data__c WHERE Object_Name__c = 'case' AND Opportunity__c IN :opportunity]){
                
                if( actualCase.OpportunityId__c == oldCase.Opportunity__c ){
                    
                    if( previousCaseInAssertDataToCompare.containsKey(oldCase.Opportunity__c) ){
                        previousCaseInAssertDataToCompare.get(oldCase.Opportunity__c).add(oldCase);
                    }else{
                        previousCaseInAssertDataToCompare.put(oldCase.Opportunity__c, new List<Assert_Data__c>{oldCase});
                    }
                }
            }
        }
        populateMapAssertDataForEvaluation(ltCase);
    }

    private void populateMapAssertDataForEvaluation(List<Case> ltCase){
        Set<Id> opportunity = new Set<Id>();

        List<Deal_Desk_Operational_Case_Field__mdt> lstCaseFields = [SELECT Field_Name__c, Field_Custom_Name__c FROM Deal_Desk_Operational_Case_Field__mdt];

        for(Deal_Desk_Operational_Case_Field__mdt caseField : lstCaseFields){            
            caseFieldAndCustomField.put(caseField.Field_Name__c.toLowerCase(), caseField.Field_Custom_Name__c);
        }        
        
        
        for(Case newCase : ltCase){
            opportunity.add(newCase.OpportunityId__c);
        }

        List<Assert_Data__c> lstAssertData = [SELECT Opportunity__c, Old_Value__c, Field_Name__c, Fee_Type__c, Object_Name__c FROM Assert_Data__c WHERE Opportunity__c IN :opportunity];
        
        //System.debug('lstAssertData: ' + Pattern.compile('(.{299})').matcher(JSON.serialize( lstAssertData )).replaceAll('$1\n'));
        for (Case actualCase : ltCase){
            previousCaseInAssertDataForEvaluation.put(actualCase.OpportunityId__c, new Map<String, String>());

            for (Assert_Data__c oldCase : lstAssertData){
                
                if( actualCase.OpportunityId__c == oldCase.Opportunity__c ){
                    if( oldCase.Object_Name__c != null && oldCase.Object_Name__c.equals('case') ){                        
                        previousCaseInAssertDataForEvaluation.get(actualCase.OpportunityId__c).put(oldCase.Field_Name__c, oldCase.Old_Value__c);
                    }else{                        
                        if( oldCase.Fee_Type__c != null && oldCase.Fee_Type__c.equals('Enterprise Subscription') && oldCase.Field_Name__c.equals('frequency__c')){                          
                            oldCaseESFrequency.put(actualCase.OpportunityId__c, oldCase.Old_Value__c);
                        }    
                    }
                }
            }
        }
    }

    private void getRecordTypeOpportunityByCase(List<Case> ltCase){
        Id indirectNewBusinessRecTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Indirect_Channel_New_Business').getRecordTypeId();

        Set<Id> opp = new Set<Id>();
        for (Case newCase : ltCase){
            opp.add(newCase.OpportunityId__c);
        }

        List<Opportunity> lstOpp = [SELECT Id, RecordType.DeveloperName, B_M__c, Quantity_Offer_Number_of_Employees__c FROM Opportunity WHERE Id IN :opp];

        for (Opportunity caseOpportunity : lstOpp){
            recordType.put(caseOpportunity.Id, caseOpportunity.RecordType.DeveloperName);

            if(caseOpportunity.RecordType.DeveloperName == 'Indirect_Channel_New_Business'){
                numberEligibles.put(caseOpportunity.Id, Integer.valueOf(caseOpportunity.Quantity_Offer_Number_of_Employees__c));
                businessModel.put(caseOpportunity.Id, caseOpportunity.B_M__c);
            }
        }
    }
    
    private void generateMapAssertDataForEvaluation(List<Case> ltCase){
        Set<Id> opportunity = new Set<Id>();        

        for(Case newCase : ltCase){
            opportunity.add(newCase.OpportunityId__c);
        }
        for (Case actualCase : ltCase){
            FieldAndValueAssertData.put(actualCase.OpportunityId__c, new Map<String, String>());
            for (Assert_Data__c oldCase : [SELECT Opportunity__c, Old_Value__c, Field_Name__c, Fee_Type__c, Object_Name__c FROM Assert_Data__c WHERE Object_Name__c = 'case' AND Opportunity__c IN :opportunity]){
                
                if( actualCase.OpportunityId__c == oldCase.Opportunity__c ){
                    
                    if(oldCase.Old_Value__c != null && oldCase.Old_Value__c.equals('Custom')){                        
                        if(caseFieldAndCustomField.get(oldCase.Field_Name__c) != null){
                        FieldAndValueAssertData.get(oldCase.Opportunity__c).put(oldCase.Field_Name__c, previousCaseInAssertDataForEvaluation.get(oldCase.Opportunity__c).get(caseFieldAndCustomField.get(oldCase.Field_Name__c).toLowerCase()));    }
                    }else if(!oldCase.Field_Name__c.contains('custom')){                        
                        FieldAndValueAssertData.get(oldCase.Opportunity__c).put(oldCase.Field_Name__c, oldCase.Old_Value__c);
                    }
                }
            }
        }
    }

    private void approveOpportunities( Set< Id > aSetOppIdToApproveBySpecialist ){
        if( aSetOppIdToApproveBySpecialist.isEmpty() ){ return; }
        
        List< Opportunity > lLstOppToApprove = new List< Opportunity >();
        
        for( Id iOppId : aSetOppIdToApproveBySpecialist ){
            lLstOppToApprove.add(
                new Opportunity( Id = iOppId , Specialist_Approved__c = true )
            );
        }        
        Database.update( lLstOppToApprove );
    }
    
    private Boolean isStandardAnswers( Case aCase ){
        Set< String > lSetStandardFieldValues;
        String lCaseFieldValue;
        Boolean isStandardValues = true;

        if(recordType.get(aCase.OpportunityId__c).equals('Indirect_Channel_New_Business') && aCase.recordType.name == 'Deal Desk Operational'){
            List<String> StandardValuesEligibleList = new List<String> {'HR Portal', 'GDPR Portal - Full file solution', 'GDPR Portal - Joiners and Leavers solution'};
            
            if(numberEligibles.get(aCase.OpportunityId__c) < 500 && !aCase.Eligible_List_Registration_Method__c.equals('Self Service (appplies for SMB)')){ 
                isStandardValues = false;
            }
            if(numberEligibles.get(aCase.OpportunityId__c) >= 500 && !StandardValuesEligibleList.contains(aCase.Eligible_List_Registration_Method__c)){ 
                isStandardValues = false;
            }         
        }
        
        for( String iCaseField : gMapCaseFieldStandardValues.keySet() ){
            lSetStandardFieldValues = gMapCaseFieldStandardValues.get( iCaseField );
            lCaseFieldValue = (String)aCase.get( iCaseField );            
            
            //Not Standard answers so put on Map to check if Approval Item needs to be created
            if(!String.isBlank( lCaseFieldValue) && !lSetStandardFieldValues.contains( lCaseFieldValue )){
                String approvalItemName = returnApprovalItemName(iCaseField);
                if(!String.isBlank(approvalItemName)){
                    if(lCaseFieldValue == 'Custom'){
                        lCaseFieldValue = lCaseFieldValue + ' - ' + String.valueOf(aCase.get(mapAppItemCustomField.get(approvalItemName)));
                    }
                    mapApprovalItemValue.put(approvalItemName, lCaseFieldValue);
                }
            }
        }

        isStandardDueDays(aCase);
        
        if( !mapApprovalItemValue.isEmpty() && String.isBlank( aCase.Justificate_No_Compliant_Topics__c ) ) {
            aCase.addError('Your case is non-compliant.' + 
                           ' Please fill in your justification on Justificate No Compliant Topics field.');
        }
        
        if(!mapApprovalItemValue.isEmpty()) isStandardValues = false;

        return isStandardValues;
    }

    private void isStandardDueDays( Case aCase ){ 
        //Boolean lIsStandard = true;    
        List<Payment__c> ltPayment = mpPayments.get(aCase.QuoteId__c);
        String approvalItemName = '';

        if(!recordType.get(aCase.OpportunityId__c).equals('Indirect_Channel_New_Business')){
            if(aCase.MF_Eligibility_ES_Payment_Due_Days__c == 'Custom' && aCase.MF_Eligibility_ES_Custom_Payment_Due_Day__c > 30){
                approvalItemName = returnApprovalItemName('MF_Eligibility_ES_Payment_Due_Days__c');
                mapApprovalItemValue.put(approvalItemName, aCase.MF_Eligibility_ES_Payment_Due_Days__c + ' - ' + String.valueOf(aCase.MF_Eligibility_ES_Custom_Payment_Due_Day__c));
            }
    
            if(ltPayment != null){
                for(Payment__c pay : ltPayment){    
                    if(pay.Payment_Code__c != null){
                        if(pay.Payment_Code__c == 'Professional_Services_Maintenance_Fee'){
                            approvalItemName = returnApprovalItemName('Prof_Services_Main_Fee_Payment_Due_Days__c');
                            if(pay.Frequency__c == 'Yearly'){
                                if(aCase.Prof_Services_Main_Fee_Payment_Due_Days__c == 'Custom' && aCase.Custom_Prof_Services_Main_Fee_Payment__c > 60){
                                    mapApprovalItemValue.put(approvalItemName, aCase.Prof_Services_Main_Fee_Payment_Due_Days__c + ' - ' + String.valueOf(aCase.Custom_Prof_Services_Main_Fee_Payment__c) + ' - ' + pay.Frequency__c);
                                }  
                            }else if(pay.Frequency__c == 'Monthly' || pay.Frequency__c == 'Quarterly' || pay.Frequency__c == 'Biannually'){
                                if(aCase.Prof_Services_Main_Fee_Payment_Due_Days__c == '60 days'){ 
                                    mapApprovalItemValue.put(approvalItemName, aCase.Prof_Services_Main_Fee_Payment_Due_Days__c + ' - ' + pay.Frequency__c);
                                }    
                                if(aCase.Prof_Services_Main_Fee_Payment_Due_Days__c == 'Custom' && aCase.Custom_Prof_Services_Main_Fee_Payment__c > 30){ 
                                    mapApprovalItemValue.put(approvalItemName, aCase.Prof_Services_Main_Fee_Payment_Due_Days__c + ' - ' + aCase.Custom_Prof_Services_Main_Fee_Payment__c + ' - ' + pay.Frequency__c);
                                }
                            }
            
                        }else if(pay.Payment_Code__c == 'Professional_Services_Setup_Fee'){   
                                approvalItemName =  returnApprovalItemName('Prof_Services_One_Fee_Payment_Due_Days__c');
                               if(aCase.Prof_Services_One_Fee_Payment_Due_Days__c == 'Custom' && aCase.Custom_Prof_Services_One_Fee_Payment__c > 30){ 
                                    mapApprovalItemValue.put(approvalItemName, aCase.Prof_Services_One_Fee_Payment_Due_Days__c + ' - ' + String.valueOf(aCase.Custom_Prof_Services_One_Fee_Payment__c)); 
                                }
                                    
                        }else if(pay.Payment_Code__c == 'Setup_Fee'){   
                                approvalItemName = returnApprovalItemName('Setup_Fee_Payment_Due_Days__c');
                                if(aCase.Setup_Fee_Payment_Due_Days__c == 'Custom' && aCase.Custom_Setup_Payment_Due_Day__c > 30){ 
                                    mapApprovalItemValue.put(approvalItemName, aCase.Setup_Fee_Payment_Due_Days__c + ' - ' + String.valueOf(aCase.Custom_Setup_Payment_Due_Day__c));
                                }
                            
                        }else if(pay.Payment_Code__c.contains('Enterprise_Subscription')){
                            approvalItemName = returnApprovalItemName('ES_Payment_Due_Days__c');
                            if(pay.Frequency__c == 'Yearly'){
                                if(aCase.ES_Payment_Due_Days__c == 'Custom' && aCase.Custom_ES_Payment_Due_Days__c > 60){ 
                                    mapApprovalItemValue.put(approvalItemName, aCase.ES_Payment_Due_Days__c + ' - ' + aCase.Custom_ES_Payment_Due_Days__c + ' - ' + pay.Frequency__c);
                                }
            
                            }else if(pay.Frequency__c == 'Monthly' || pay.Frequency__c == 'Quarterly' || pay.Frequency__c == 'Biannually'){
                                if(aCase.ES_Payment_Due_Days__c == '60 days'){
                                    mapApprovalItemValue.put(approvalItemName, aCase.ES_Payment_Due_Days__c + ' - ' + pay.Frequency__c);
                                }
                                if(aCase.ES_Payment_Due_Days__c == 'Custom' && aCase.Custom_ES_Payment_Due_Days__c > 30){ 
                                    mapApprovalItemValue.put(approvalItemName, aCase.ES_Payment_Due_Days__c + ' - ' + aCase.Custom_ES_Payment_Due_Days__c + ' - ' + pay.Frequency__c);
                                }
                            }
                        }  
                    }       
                }
            } 
        }    
        else if(recordType.get(aCase.OpportunityId__c).equals('Indirect_Channel_New_Business')){

            if(ltPayment != null){
                for(Payment__c pay : ltPayment){    
                    if(pay.Payment_Code__c != null){
                        if(pay.Payment_Code__c == 'Professional_Services_Maintenance_Fee'){
                            if(aCase.Prof_Services_Main_Fee_Payment_Due_Days__c == '60 days'){ 
                                mapApprovalItemValue.put(approvalItemName, aCase.Prof_Services_Main_Fee_Payment_Due_Days__c + ' - ' + pay.Frequency__c);
                             }  
            
                        }else if(pay.Payment_Code__c.contains('Enterprise_Subscription')){
                            if(businessModel.get(aCase.OpportunityId__c).equals('Intermediation')){
                                if(pay.Frequency__c == 'Monthly' || pay.Frequency__c == 'Quarterly' || pay.Frequency__c == 'Biannually'){
                                    if(aCase.ES_Payment_Due_Days__c == '60 days'){ 
                                        mapApprovalItemValue.put(approvalItemName, aCase.ES_Payment_Due_Days__c + ' - ' + pay.Frequency__c);
                                    } 
                                }
                            }
                        }  
                    }       
                }
            }     
        }    
    }
    
    private Map< String , Set< String > > getFieldsAndStandardValuesFromMdt(){
        Map< String , Set< String > > lMapCaseFieldStandardValues = new Map< String , Set< String > >();        
        
        for( Specialist_form_Standard_Field_Values__mdt iSpecialistMdt : 
            [ SELECT Field_API_Name__c, Standard_values__c FROM Specialist_form_Standard_Field_Values__mdt ] ){
                if( String.isBlank( iSpecialistMdt.Standard_values__c ) ){ continue; }
                
                lMapCaseFieldStandardValues.put( iSpecialistMdt.Field_API_Name__c , new Set< String >( iSpecialistMdt.Standard_values__c.split(';') ) );
                gMapCaseFieldStandardValuesLowerCase.put( iSpecialistMdt.Field_API_Name__c.toLowerCase() , new Set< String >( iSpecialistMdt.Standard_values__c.split(';') ) );
        }
        return /*Test.isRunningTest() ? 
            new Map< String , Set< String > >{
             'Description' => new Set< String >{ 'isTest' } } : */lMapCaseFieldStandardValues;
    }

    private Map<Id, List<Payment__c>> getPayments(List<Case> ltCase){
        PaymentRepository paymentRep = new PaymentRepository();  
        Set<Id> ltQuoteId = new Set<Id>();
        
        for(Case newCase : ltCase){
            ltQuoteId.add(newCase.QuoteId__c);
        }
        return paymentRep.getPaymentsForQuotes(ltQuoteId);
    }

    private Map<String, String> mapApprovalItemsMetadata(){
        Map<String, String> mapAppItemCaseField = new Map<String,String>();
        for(Approval_Items_Case_Mapping__mdt appItemMetadata : [SELECT Id, Approval_Item_Name__c, Case_Fields_To_Map__c FROM Approval_Items_Case_Mapping__mdt WHERE Case_Fields_To_Map__c != null]){
            List<String> listFieldsCase = appItemMetadata.Case_Fields_To_Map__c.split(',');
            for(String fieldCase : listFieldsCase){
                mapAppItemCaseField.put(fieldCase.toUpperCase(), appItemMetadata.Approval_Item_Name__c);
                if(fieldCase.contains('Custom_')){
                    mapAppItemCustomField.put(appItemMetadata.Approval_Item_Name__c, fieldCase);
                }
                
            }
        }

        return mapAppItemCaseField;
    }

    private String generateCaseDescription(Map<String, String> mapApprovalItemValue){
        String description = '';
        for(String key : mapApprovalItemValue.keySet()){
            String approvalItemName = key;
            String approvalItemValue = mapApprovalItemValue.get(key);
            description += approvalItemName + ':' + approvalItemValue + ',';
        }

        return description;
    }

    private String returnApprovalItemName(String fieldName){
        String approvalItemName = '';
        fieldName = fieldName.toUpperCase();
        if(mapAppItemCaseField.containsKey(fieldName)){
            approvalItemName = mapAppItemCaseField.get(fieldName);
        }

        return approvalItemName;
    }
}