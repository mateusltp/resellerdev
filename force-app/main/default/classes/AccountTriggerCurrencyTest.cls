/**
 * @File Name          : AccountTriggerCurrencyTest.cls
 * @Description        : 
 * @Author             : GEPI@GFT.com
 * @Group              : 
 * @Last Modified By   : gepi@gft.com
 * @Last Modified On   : 04-08-2022
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    08/04/2020   GEPI@GFT.com     Initial Version
**/
@isTest
public class AccountTriggerCurrencyTest {
    @TestSetup
    static void Setup(){      
        
        //Id rtId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gym_Partner').getRecordTypeId();
        
        List<Account> listAcc = new List<Account>();
        Id rtId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gym_Partner').getRecordTypeId();
       
               
        Account accParent = new Account();

        accParent.name='AcademiaBrasilCompanyPai';
        accParent.RecordTypeId = rtId;
        accParent.Types_of_ownership__c = Label.privateOwnerPicklist;
        accParent.GP_Status__c = 'Pending Approval';
        accParent.ShippingState = 'Minas Gerais';
        accParent.ShippingStreet = 'RUX ASSS';
        accParent.ShippingCity = 'CITY CITY';
        accParent.ShippingCountry = 'Brazil';
        accParent.BillingCountry = 'Brazil';
        accParent.BillingState = 'São Paulo';
        accParent.BillingCity = 'São Paulo';
        accParent.BillingStreet = 'Casa do Ator';
        accParent.Gym_Type__c = 'Studios';
        accParent.Subscription_Type__c = 'Value per class';
        accParent.Subscription_Period__c = 'Monthy value';
        accParent.Subscription_Type_Estimated_Price__c	= 100;
        accParent.Has_market_cannibalization__c = 'No';
        accParent.Exclusivity__c = 'No';
        // accParent.Exclusivity_End_Date__c = Date.today().addYears(1);
        // accParent.Exclusivity_Partnership__c = 'Full Exclusivity';
        // accParent.Exclusivity_Restrictions__c= 'No';
        accParent.Website = 'testing@tesapex.com';
        accParent.Gym_Email__c = 'gymemail@apex.com';
        accParent.Phone = '3222123123';
        listAcc.add(accParent);
      
        Account child = new Account();
        child.name='child1'; 
        child.ParentId = accParent.Id;
        child.ShippingStreet = 'RUX AS333';
        child.Types_of_ownership__c = Label.privateOwnerPicklist;
        child.GP_Status__c = 'Pending Approval';
        child.website='www.ChildOne.com';
        child.RecordTypeId = rtId; 
        child.Gym_Type__c ='Studios';
        child.ShippingCountry = 'Brazil'; 
        child.ShippingCity ='ddddd';
        child.ShippingState = 'Roraima';
        child.BillingCountry = 'Brazil';
        child.BillingState = 'São Paulo';
        child.BillingCity = 'São Paulo';
        child.BillingStreet = 'Casa do Ator';
        child.Subscription_Type__c = 'Value per class';
        child.Subscription_Period__c = 'Monthy value';
        child.Subscription_Type_Estimated_Price__c = 100;
        child.Exclusivity__c = 'No';
        // child.Exclusivity_End_Date__c = Date.today().addYears(1);
        // child.Exclusivity_Partnership__c = 'Full Exclusivity';
        // child.Exclusivity_Restrictions__c= 'No';
        child.Gym_Email__c = 'gymemail@apex.com';
        child.Phone = '3222123123';
        
        listAcc.add(child); 

        Account partnerFlowAcc = getAccountPartnerFlow();
        listAcc.add(partnerFlowAcc);

        Account clientAccount = getClientAccount();
        listAcc.add(clientAccount);

        INSERT listAcc;  

        /*pricebooks */
        Pricebook2 standardPricebook = new Pricebook2(
            Id = Test.getStandardPricebookId(),
            IsActive = true
        );

        UPDATE standardPricebook;
        
        standardPricebook = [
            SELECT Id,CurrencyIsoCode, IsStandard 
            FROM Pricebook2 
            WHERE Id =:standardPricebook.Id
        ];
        
        Pricebook2 customPriceBook = new Pricebook2(
            Name = 'BR Partner2',
            Description = 'This is the Price Book for BR.',
            CurrencyIsoCode = 'BRL',
            IsActive = true
        );
        
        INSERT customPriceBook;   

        Product2 plan = new Product2();
        plan.Name = 'Ferro';
        plan.IsActive = true;
        plan.CurrencyIsoCode = 'BRL';

        INSERT plan;
        
        PricebookEntry priceStandardEntry = new PricebookEntry(
            Pricebook2Id = standardPricebook.Id,
            Product2Id = plan.Id,
            CurrencyIsoCode = standardPricebook.CurrencyIsoCode,
            UnitPrice = 119,
            UseStandardPrice = false,
            IsActive = true
        );
        INSERT priceStandardEntry;

        PricebookEntry priceBookEntryCustom = new PricebookEntry(
            Pricebook2Id = customPriceBook.Id,
            Product2Id = plan.Id,
            CurrencyIsoCode = customPriceBook.CurrencyIsoCode,
            UnitPrice = 109,
            UseStandardPrice = false,
            IsActive = true
        );
        INSERT priceBookEntryCustom;
         
        /**Opportunity*/                
        List<Opportunity> oppToInsert = new List<Opportunity>();        
        Id oppRtId =   Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Gyms_Small_Medium_Partner').getRecordTypeId();
        
        Opportunity accOpp = new Opportunity();
        accOpp.recordTypeId = oppRtId;
        accOpp.AccountId = accParent.id;
        accOpp.Name = 'academiaBrasilCompanyOpp';      
        accOpp.Gym_agreed_to_an_API_integration__c	= 'Yes';
        accOpp.Integration_Fee_Deduction__c = 'No';
        accOpp.CloseDate = Date.today();
        accOpp.Success_Look_Like__c = 'Yes';
        accOpp.Success_Look_Like_Description__c	= 'Money money';
        accOpp.StageName = 'Qualification';
        accOpp.Type	= 'Expansion';     
        accOpp.CurrencyIsoCode = 'BRL';
        accOpp.Gympass_Plus__c = 'Yes';
        accOpp.Pricebook2Id = customPriceBook.id;
        INSERT accOpp;

        Opportunity accOpp2 = new Opportunity();
        accOpp2.recordTypeId = oppRtId;
        accOpp2.AccountId = child.id;
        accOpp2.Name = 'academiaBrasilCompanyOpp';      
        accOpp2.Gym_agreed_to_an_API_integration__c	= 'Yes';
        accOpp2.Integration_Fee_Deduction__c = 'No';
        accOpp2.CloseDate = Date.today();
        accOpp2.Success_Look_Like__c = 'Yes';
        accOpp2.Success_Look_Like_Description__c	= 'Money money';
        accOpp2.StageName = 'Qualification';
        accOpp2.Type	= 'Expansion';     
        accOpp2.CurrencyIsoCode = 'BRL';
        accOpp2.Gympass_Plus__c = 'Yes';
        INSERT accOpp2;
        

        Id prodRtId = Schema.SObjectType.Product_Item__c.getRecordTypeInfosByDeveloperName().get('Gyms_Product_Item_Partner').getRecordTypeId();
       
        Product_Item__c prod = new Product_Item__c();    
        prod.recordTypeId = prodRtId;
        prod.CurrencyIsoCode = 'BRL';
        prod.CAP_Value__c = 120;
        prod.Do_You_Have_A_No_Show_Fee__c = 'No';
        prod.Has_Late_Cancellation_Fee__c = 'No';
        prod.Late_Cancellation_Percent__c = '0';
        prod.Maximum_Live_Class_Per_Month__c = 0;
        prod.No_Show_Fee_Percent__c = '0';
        prod.Net_Transfer_Price__c	=10;
        prod.Product_Definition__c	='Dança';
        prod.Product_Restriction__c	= 'None';
        prod.Name = 'Dança';
        
        INSERT prod;

    }


    @isTest 
    static void updateAccountPartnerNewFlow() {
        Account acc = [SELECT id, Name, BillingCountry, CurrencyIsoCode FROM Account WHERE Name = 'Partner Flow Acc'];

        Test.startTest();  
        acc.BillingCountry = 'Argentina';
        acc.BillingCountryCode = 'AR';
        update acc;

        Account updatedAcc = [SELECT id, Name, CurrencyIsoCode FROM Account WHERE Name = 'Partner Flow Acc'];

        // Asserts for update
        System.assertEquals(updatedAcc.CurrencyIsoCode, 'ARS');
        Test.stopTest();               
    }

    @isTest 
    static void updateClientAccount() {
        Account acc = [SELECT id, Name, CurrencyIsoCode FROM Account WHERE Name = 'Client Acc'];

        Test.startTest();  
        acc.BillingCountry = 'Argentina';
        acc.BillingCountryCode = 'AR';
        update acc;

        Account updatedAcc = [SELECT id, Name, CurrencyIsoCode FROM Account WHERE Name = 'Client Acc'];

        // Asserts for update
        System.assertEquals(updatedAcc.CurrencyIsoCode, 'ARS');
        Test.stopTest();               
    }

    @isTest 
    static void updatePartnerAccount() {
        Account acc = [SELECT id, Name, CurrencyIsoCode FROM Account WHERE Name = 'AcademiaBrasilCompanyPai'];

        Test.startTest();  
        acc.ShippingCountry = 'Argentina';
        acc.ShippingState = 'Buenos Aires';
        acc.ShippingCountryCode = 'AR';
        update acc;

        Account updatedAcc = [SELECT id, Name, CurrencyIsoCode FROM Account WHERE Name = 'AcademiaBrasilCompanyPai'];

        // Asserts for update
        System.assertEquals(updatedAcc.CurrencyIsoCode, 'ARS');
        Test.stopTest();               
    }

    @isTest 
    static void updateOppPartnerAccount() {
        Account acc = [SELECT id, Name, CurrencyIsoCode FROM Account WHERE Name = 'AcademiaBrasilCompanyPai'];
    
        Test.startTest();  
        acc.ShippingCountry = 'Argentina';
        acc.ShippingCountryCode = 'AR';
        update acc;

        Opportunity opp = [SELECT id, Name, CurrencyIsoCode FROM Opportunity WHERE Account.Name = 'AcademiaBrasilCompanyPai' LIMIT 1];

        System.assertEquals(opp.CurrencyIsoCode, 'ARS');
        Test.stopTest();               
    }

    @isTest static void errorMethod() {
        Account accParent = [SELECT id, Name FROM Account WHERE Name = 'AcademiaBrasilCompanyPai'];
    
        Test.startTest();  
        accParent.ShippingCountry = 'Argentina';
        accParent.ShippingState = 'Buenos Aires';
        try{
            update accParent;
        }catch(DmlException e){
            Boolean expectedExceptionThrown =  (e.getMessage().contains('Can\'t update the Country/Currency when the related opportunity have products or proposals')) ? true : false;
            System.AssertEquals(expectedExceptionThrown, true);
        }
        Test.stopTest();               
    }
    
    private static Account getAccountPartnerFlow() {
        Id partnerAccRecType = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Account').getRecordTypeId();

        Account acc = new Account();
        acc.RecordTypeId = partnerAccRecType;
        acc.Name = 'Partner Flow Acc';
        acc.Partner_Level__c = 'Brand';
        acc.BillingCountry = 'Mexico';
        acc.BillingCountryCode = 'MX';
        acc.BillingState = 'Estado de México';
        acc.BillingCity = 'México';
        acc.BillingStreet = 'Casa do Ator';
        return acc;
    }   
    
    private static Account getClientAccount() {
        Id partnerAccRecType = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();

        Account acc = new Account();
        acc.RecordTypeId = partnerAccRecType;
        acc.Name = 'Client Acc';
        acc.BillingCountry = 'Brazil';
        acc.BillingState = 'São Paulo';
        acc.BillingCity = 'São Paulo';
        acc.BillingStreet = 'Casa do Ator';
        acc.NumberOfEmployees = 2000;
        return acc;
    }
}