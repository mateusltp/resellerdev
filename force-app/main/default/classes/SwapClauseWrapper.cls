public class SwapClauseWrapper  implements Comparable{
    
    public Id Id; 
    public string Name;
    public Decimal Order;
    public string Category;
    public string Type;
    public string ClauseText;

    public List<SwapClauseWrapper> SwapOptions;
    
    public SwapClauseWrapper() {
        this.SwapOptions = new List<SwapClauseWrapper>();
    }

    public Integer compareTo(Object compareTo) {
        SwapClauseWrapper compareToSwapClauseWrapper = (SwapClauseWrapper)compareTo;
        if (Order == compareToSwapClauseWrapper.Order) return 0;
        if (Order > compareToSwapClauseWrapper.Order) return 1;
        return -1;        
    }

    public void generateSequenceOrder(){
        try{
        Pattern p = Pattern.compile('[0-9]+.[0-9]*|[0-9]*.[0-9]+|[0-9]+^\\S*');
        Matcher m = p.matcher(this.Name);
        decimal orderFound= 0;

        List<string> orderList = new List<string>();
        Boolean foundComma = false;
        while (m.find()) {
                orderList.add(m.group().replaceAll( '\\s+', ''));       
        }

        integer limitTocheck = 3;
        if (orderList.size() < 3){
            limitTocheck = orderList.size();
        }

        Integer sequenceCreated = 0;

        for (integer i = 0; i < limitTocheck; i++){
            if (orderList[i].replace('.', '').isNumeric()){
                if (i == 0){
                    sequenceCreated += integer.valueOf(orderList[i].replace('.', '')) * 1000;
                }
                if (i==1){
                    sequenceCreated += integer.valueOf(orderList[i].replace('.', '')) * 100;
                }
                if (i==2){
                    sequenceCreated += integer.valueOf(orderList[i].replace('.', '')) * 10;
                }
                
            }
        }

        orderFound = decimal.valueOf(sequenceCreated);
        this.Order = orderFound;
       

    }
    catch (Exception ex){
        this.Order = 0;
    }
        
     
    }
    
}