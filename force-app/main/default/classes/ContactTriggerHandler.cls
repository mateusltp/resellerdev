/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 10-13-2021
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/
public without sharing class ContactTriggerHandler extends TriggerHandler {
  override public void afterInsert() {
    new PublishContactOnTagusEvent().execute();
    new PublishContactPartnerOnTagusEvent().execute();
    new ContactService().updateAccountLegalRepresentative(Trigger.new);
    new ContactService().updateRelationshipRole(Trigger.new);
  }

  override public void afterUpdate() {
    new PublishContactOnTagusEvent().execute();
    new PublishContactPartnerOnTagusEvent().execute();
    new ContactService().updateAccountLegalRepresentative(Trigger.new, (Map<Id, Contact>)Trigger.oldMap);
    new ContactService().updateRelationshipRole(Trigger.new, (Map<Id, Contact>)Trigger.oldMap);
  }
}