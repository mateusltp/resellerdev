/**
* @author vinicius.ferraz
* @description Provide unit test for fast track proposal save
*/
@isTest(seeAllData=false)
public with sharing class FastTrackProposalCreationTest {
    
    @isTest
    private static void shouldSaveProposal(){        
        Opportunity lOpp = [ SELECT Id, AccountId, CurrencyIsoCode, Name FROM Opportunity  limit 1];
        
        Quote lProposal = DataFactory.newQuote( lOpp , 'Quote Test', 'Client_Sales_New_Business' );
        lProposal.Contact_Permission__c = 'Allowlist';
        lProposal.Employee_Corporate_Email__c = true;
        lProposal.Total_Number_of_Employees__c = 5000;
        lProposal.Copay__c = 'Yes';
        Database.insert( lProposal );
        
        FastTrackProposalCreationTO lTo = new FastTrackProposalCreationBuilder()
            .withOpportunity( lOpp.Id )
            .withDealHierarchy( 2 , new List< Id >{ lOpp.AccountId } )
            .withLastCreatedQuoteWhenExists( lOpp.Id )            
            .build();
        lTo.customIntegrationsRequired = 'Yes';
        lTo.areDependentsIncluded = 'Yes';
        lTo.quantity = 5000;
        lTo.hasCopay = 'Yes';
        
        FastTrackProposalCreation lProposalCreation = new FastTrackProposalCreation();
        
        Test.startTest();
        PlatformEventsHelper.publishStandardOfferCreationEvent( new List< Opportunity >{ lOpp } );
        
        lProposalCreation.init( lOpp.Id );
        lProposalCreation.save( lTo );
        Test.stopTest();
        
        Quote lQuote = [ SELECT Id, ( SELECT Id FROM QuoteLineItems ) FROM Quote WHERE OpportunityId = :lOpp.Id ];
        
        System.assertNotEquals( lQuote , null );
        System.assert( !lQuote.getSObjects('QuoteLineItems').isEmpty() );
        System.assertEquals( lQuote.getSObjects('QuoteLineItems').size() , 4 );
    }
    
    @isTest
    private static void shouldPopulateOpsForm(){
        Opportunity lOpp = [ SELECT Id, AccountId, CurrencyIsoCode, Name FROM Opportunity ];
        
        FastTrackProposalCreationTO lTo = new FastTrackProposalCreationBuilder()
            .withOpportunity( lOpp.Id )
            .withDealHierarchy( 2 , new List< Id >{ lOpp.AccountId } )
            .withLastCreatedQuoteWhenExists( lOpp.Id )            
            .build();
        
        FastTrackProposalCreation lProposalCreation = new FastTrackProposalCreation();
        
        Ops_Setup_Validation_Form__c lOpsForm = new Ops_Setup_Validation_Form__c();
        lTo.opsSetupForm.sObj = lOpsForm;
        Test.startTest();
        
        lProposalCreation.init( lOpp.Id );
        
        lOpsForm = lProposalCreation.saveClientOpsForm( lTo , Database.setSavepoint() );
        
        Test.stopTest();
        
        System.assertEquals( 1 , [ SELECT Id FROM Ops_Setup_Validation_Form__c WHERE Id =: lOpsForm.Id ].size() ,
                            'Ops form did not save' );
    }
    @isTest
    private static void shouldPopulateOpsForm2(){

        Account lAcc = DataFactory.newGympassEntity( 'account' );
        lAcc.billingState = 'São Paulo';
        lAcc.BillingCity = 'CityAcademiaBrasil';
        lAcc.billingStreet = 'Rua brazil';
        lAcc.billingCountry = 'Brazil';
        Database.insert( lAcc );

        Account accReseller = DataFactory.newAccountReseller();
        accReseller.Id_Company__c = '74.112.631/0001-39';
        Database.insert( accReseller );

        Pricebook2 lStandardPricebook = DataFactory.newPricebook();
        Database.update( lStandardPricebook );

        Opportunity lOpp = new Opportunity( CurrencyIsoCode = 'USD' );

        lOpp = DataFactory.newOpportunity( lAcc.Id , lStandardPricebook , 'Client_Success_Renegotiation' );
        lOpp.CurrencyIsoCode = 'USD';
        lOpp.Quantity_Offer_Number_of_Employees__c = 5000;
        lopp.Reseller_del__c = accReseller.id;
        lopp.B_M__c = 'Exclusive';
        Database.insert( lOpp );
        
        FastTrackProposalCreationTO lTo = new FastTrackProposalCreationBuilder()
            .withOpportunity( lOpp.Id )
            .withDealHierarchy( 2 , new List< Id >{ lOpp.AccountId } )
            .withLastCreatedQuoteWhenExists( lOpp.Id )            
            .build();
        
        FastTrackProposalCreation lProposalCreation = new FastTrackProposalCreation();
        
        Ops_Setup_Validation_Form__c lOpsForm = new Ops_Setup_Validation_Form__c();
        lTo.opsSetupForm.sObj = lOpsForm;
        Test.startTest();
        
        lProposalCreation.init( lOpp.Id );
        
        lOpsForm = lProposalCreation.saveClientOpsForm( lTo , Database.setSavepoint() );
        
        Test.stopTest();
        
        System.assertEquals( 1 , [ SELECT Id FROM Ops_Setup_Validation_Form__c WHERE Id =: lOpsForm.Id ].size() ,
                            'Ops form did not save' );
    }
    
    @TestSetup
    static void createData(){        
        Account lAcc = DataFactory.newGympassEntity( 'Gympass' );
        Account accReseller = DataFactory.newAccountReseller();
        Database.insert( lAcc );
        Database.insert( accReseller );
        
        Pricebook2 lStandardPricebook = DataFactory.newPricebook();
        Database.update( lStandardPricebook );
        
        Product2 lAccessFee = DataFactory.newProduct( 'Enterprise Subscription' , false , 'USD' );
        lAccessFee.Maximum_Number_of_Employees__c = 9000;
        lAccessFee.Copay2__c = true;
        lAccessFee.Copay2_Enrollment_Rate__c = 100;
        Database.insert( lAccessFee );
        
        Product2 lSetupFee = DataFactory.newProduct( 'Setup Fee' , false , 'USD' );
        lSetupFee.Maximum_Number_of_Employees__c = 9000;
        lSetupFee.Copay2__c = true;
        lSetupFee.Copay2_Enrollment_Rate__c = 100;
        Database.insert( lSetupFee );        
        
        Opportunity lOpp = new Opportunity( CurrencyIsoCode = 'USD' );
        
        
        PricebookEntry lAccessFeeEntry = DataFactory.newPricebookEntry( lStandardPricebook , lAccessFee , lOpp );
        Database.insert( lAccessFeeEntry );
        
        Product2 lAccessFeeFamilyMember = DataFactory.newProduct( 'Enterprise Subscription' , true , 'USD' );
        lAccessFeeFamilyMember.Copay2__c = true;
        lAccessFeeFamilyMember.Maximum_Number_of_Employees__c = 9000;
        lAccessFeeFamilyMember.Copay2_Enrollment_Rate__c = 100;
        Database.insert( lAccessFeeFamilyMember );
        
        PricebookEntry lAccessFeeFamilyMemberEntry = DataFactory.newPricebookEntry( lStandardPricebook , lAccessFeeFamilyMember , lOpp );
        Database.insert( lAccessFeeFamilyMemberEntry );
        
        PricebookEntry lSetupFeeEntry = DataFactory.newPricebookEntry( lStandardPricebook , lSetupFee , lOpp );
        Database.insert( lSetupFeeEntry );
        
        Product2 lProfServicesOneFee = DataFactory.newProduct( 'Professional Services Setup Fee' , true , 'USD' );
        lProfServicesOneFee.Maximum_Number_of_Employees__c = 9000;
        lProfServicesOneFee.Copay2__c = true;
        lProfServicesOneFee.Copay2_Enrollment_Rate__c = 100;
        Database.insert( lProfServicesOneFee );
        
        PricebookEntry lProfServicesOneFeeEntry = DataFactory.newPricebookEntry( lStandardPricebook , lProfServicesOneFee , lOpp );
        Database.insert( lProfServicesOneFeeEntry );
        
        Product2 lProfServicesMainFee = DataFactory.newProduct( 'Professional Services Maintenance Fee' , true , 'USD' );
        lProfServicesMainFee.Maximum_Number_of_Employees__c = 9000;
        lProfServicesMainFee.Copay2__c = true;
        lProfServicesMainFee.Copay2_Enrollment_Rate__c = 100;
        Database.insert( lProfServicesMainFee );
        
        PricebookEntry lProfServicesMainFeeEntry = DataFactory.newPricebookEntry( lStandardPricebook , lProfServicesMainFee , lOpp );
        Database.insert( lProfServicesMainFeeEntry );
        
        lOpp = DataFactory.newOpportunity( lAcc.Id , lStandardPricebook , 'Client_Sales_New_Business' );
        lOpp.CurrencyIsoCode = 'USD';
        lOpp.Quantity_Offer_Number_of_Employees__c = 5000;
        lopp.Reseller_del__c = accReseller.id;
        lopp.B_M__c = 'Exclusive';
        Database.insert( lOpp );

    }
    
    @isTest
    private static void refreshRevenuesTest(){        
        Opportunity lOpp = [ SELECT Id, AccountId, CurrencyIsoCode, Name FROM Opportunity ];
        
        Quote lProposal = DataFactory.newQuote( lOpp , 'Quote Test', 'Client_Sales_New_Business' );
        lProposal.Contact_Permission__c = 'Allowlist';
        lProposal.Employee_Corporate_Email__c = true;
        lProposal.Total_Number_of_Employees__c = 5000;
        lProposal.Copay__c = 'Yes';
        Database.insert( lProposal );
        
        FastTrackProposalCreationTO lTo = new FastTrackProposalCreationBuilder()
            .withOpportunity( lOpp.Id )
            .withDealHierarchy( 2 , new List< Id >{ lOpp.AccountId } )
            .withLastCreatedQuoteWhenExists( lOpp.Id )            
            .build();
        lTo.customIntegrationsRequired = 'Yes';
        lTo.areDependentsIncluded = 'Yes';
        lTo.quantity = 5000;
        lTo.hasCopay = 'Yes';
        
        FastTrackProposalCreation lProposalCreation = new FastTrackProposalCreation();
        
        Test.startTest();
        PlatformEventsHelper.publishStandardOfferCreationEvent( new List< Opportunity >{ lOpp } );
        
        lProposalCreation.init( lOpp.Id );
        lProposalCreation.save( lTo );
        lProposalCreation.refreshRevenues( lTo );
        Test.stopTest();
        
        Quote lQuote = [ SELECT Id, ( SELECT Id FROM QuoteLineItems ) FROM Quote WHERE OpportunityId = :lOpp.Id ];
        
        System.assertNotEquals( lQuote , null );
        System.assert( !lQuote.getSObjects('QuoteLineItems').isEmpty() );
        System.assertEquals( lQuote.getSObjects('QuoteLineItems').size() , 4 );
    }    
    
    @isTest
    private static void resetDealDeskAndCommercialConditionsTest(){        
        Opportunity lOpp = [ SELECT Id, AccountId, CurrencyIsoCode, Name FROM Opportunity ];
        
        Quote lProposal = DataFactory.newQuote( lOpp , 'Quote Test', 'Client_Sales_New_Business' );
        lProposal.Contact_Permission__c = 'Allowlist';
        lProposal.Employee_Corporate_Email__c = true;
        lProposal.Total_Number_of_Employees__c = 5000;
        lProposal.Copay__c = 'Yes';
        Database.insert( lProposal );
        
        FastTrackProposalCreationTO lTo = new FastTrackProposalCreationBuilder()
            .withOpportunity( lOpp.Id )
            .withDealHierarchy( 2 , new List< Id >{ lOpp.AccountId } )
            .withLastCreatedQuoteWhenExists( lOpp.Id )            
            .build();
        lTo.customIntegrationsRequired = 'Yes';
        lTo.areDependentsIncluded = 'Yes';
        lTo.quantity = 5000;
        lTo.hasCopay = 'Yes';
        
        FastTrackProposalCreation lProposalCreation = new FastTrackProposalCreation();
        
        Test.startTest();
        PlatformEventsHelper.publishStandardOfferCreationEvent( new List< Opportunity >{ lOpp } );
        
        lProposalCreation.init( lOpp.Id );
        lProposalCreation.save( lTo );
        lProposalCreation.resetDealDeskAndCommercialConditions( lTo );
        Test.stopTest();
        
        Quote lQuote = [ SELECT Id, ( SELECT Id FROM QuoteLineItems ) FROM Quote WHERE OpportunityId = :lOpp.Id ];
        
        System.assertNotEquals( lQuote , null );
        System.assert( !lQuote.getSObjects('QuoteLineItems').isEmpty() );
        System.assertEquals( lQuote.getSObjects('QuoteLineItems').size() , 4 );
    }   
    
    @isTest
    private static void createSMBExceptionMessageTest(){        
        Opportunity lOpp = [ SELECT Id, AccountId, CurrencyIsoCode, Name FROM Opportunity ];
        Pricebook2 pb = DataFactory.newPricebook('Intermediation', 'Intermediation');
        Database.insert(pb);
        Quote lProposal = DataFactory.newQuote( lOpp , 'Quote Test', 'Client_Sales_New_Business' );
        lProposal.Contact_Permission__c = 'Allowlist';
        lProposal.Employee_Corporate_Email__c = true;
        lProposal.Total_Number_of_Employees__c = 5000;
        lProposal.Copay__c = 'Yes';
        Database.insert( lProposal );
        
        FastTrackProposalCreationTO lTo = new FastTrackProposalCreationBuilder()
            .withOpportunity( lOpp.Id )
            .withDealHierarchy( 2 , new List< Id >{ lOpp.AccountId } )
            .withLastCreatedQuoteWhenExists( lOpp.Id )            
            .build();
        lTo.customIntegrationsRequired = 'Yes';
        lTo.areDependentsIncluded = 'Yes';
        lTo.quantity = 5000;
        lTo.hasCopay = 'Yes';
        
        FastTrackProposalCreation lProposalCreation = new FastTrackProposalCreation();
        
        Test.startTest();
        PlatformEventsHelper.publishStandardOfferCreationEvent( new List< Opportunity >{ lOpp } );
        try {
        lProposalCreation.init( lOpp.Id );
        lProposalCreation.save( lTo );
        lProposalCreation.createSMBExceptionMessage(lTo,pb);
    } catch(Exception e) {
        System.debug('Error');
    }
        Test.stopTest();
        
        Quote lQuote = [ SELECT Id, ( SELECT Id FROM QuoteLineItems ) FROM Quote WHERE OpportunityId = :lOpp.Id ];
        
        System.assertNotEquals( lQuote , null );
        System.assert( !lQuote.getSObjects('QuoteLineItems').isEmpty() );
        System.assertEquals( lQuote.getSObjects('QuoteLineItems').size() , 4 );
    } 

    @isTest
    private static void createSMBExceptionMessageTest2(){        
    
       Account lAcc = DataFactory.newGympassEntity( 'SMBACCOUNT' );
       lAcc.billingState = 'São Paulo';
       lAcc.BillingCity = 'CityAcademiaBrasil';
       lAcc.billingStreet = 'Rua brazil';
       lAcc.billingCountry = 'Brazil';
       Database.insert( lAcc );

       Account accReseller = DataFactory.newAccountReseller();
       accReseller.Id_Company__c = '18.825.681/0001-92';
       Database.insert( accReseller );

       Pricebook2 lStandardPricebook = DataFactory.newPricebook();
       Database.update( lStandardPricebook );

       Opportunity lOpp = new Opportunity( CurrencyIsoCode = 'USD' );

       lOpp = DataFactory.newOpportunity( lAcc.Id , lStandardPricebook , 'SMB_Success_Renegotiation' );
       lOpp.CurrencyIsoCode = 'USD';
       lOpp.Quantity_Offer_Number_of_Employees__c = 5000;
       lopp.Reseller_del__c = accReseller.id;
       lopp.B_M__c = 'Exclusive';
       Database.insert( lOpp );

        Pricebook2 pb = DataFactory.newPricebook('Intermediation', 'Intermediation');
        Database.insert(pb);
        Quote lProposal = DataFactory.newQuote( lOpp , 'Quote Test', 'Client_Sales_New_Business' );
        lProposal.Contact_Permission__c = 'Allowlist';
        lProposal.Employee_Corporate_Email__c = true;
        lProposal.Total_Number_of_Employees__c = 5000;
        lProposal.Copay__c = 'Yes';
        Database.insert( lProposal );
        
        FastTrackProposalCreationTO lTo = new FastTrackProposalCreationBuilder()
            .withOpportunity( lOpp.Id )
            .withDealHierarchy( 2 , new List< Id >{ lOpp.AccountId } )
            .withLastCreatedQuoteWhenExists( lOpp.Id )            
            .build();
        lTo.customIntegrationsRequired = 'Yes';
        lTo.areDependentsIncluded = 'Yes';
        lTo.quantity = 5000;
        lTo.hasCopay = 'Yes';
        
        FastTrackProposalCreation lProposalCreation = new FastTrackProposalCreation();
        
        Test.startTest();
        PlatformEventsHelper.publishStandardOfferCreationEvent( new List< Opportunity >{ lOpp } );
        try {
        lProposalCreation.init( lOpp.Id );
        lProposalCreation.save( lTo );
        lProposalCreation.createSMBExceptionMessage(lTo,pb);
    } catch(Exception e) {
        System.debug('Error');
    }
        Test.stopTest();

    } 

    
@isTest
private static void throwAccessFeeExceptionTest(){        
Opportunity lOpp = [ SELECT Id, AccountId, CurrencyIsoCode, Name FROM Opportunity ];
Pricebook2 lStandardPricebook = DataFactory.newPricebook();
Database.update(lStandardPricebook);        
Pricebook2 pb = DataFactory.newPricebook('Intermediation', 'Intermediation');
Database.insert(pb);
Product2 productBRL = DataFactory.newProduct('teste', false, 'BRL');
Database.insert(productBRL);
PricebookEntry pbEntry = DataFactory.newPricebookEntry(lStandardPricebook, productBRL, lOpp);
Database.insert(pbEntry);        
PricebookEntry pbEntryIntermediation = DataFactory.newPricebookEntry(pb, productBRL, lOpp);
Database.insert(pbEntryIntermediation);
Quote lProposal = DataFactory.newQuote( lOpp , 'Quote Test', 'Client_Sales_New_Business' );
lProposal.Contact_Permission__c = 'Allowlist';
lProposal.Employee_Corporate_Email__c = true;
lProposal.Total_Number_of_Employees__c = 500;
lProposal.Copay__c = 'Yes';
Database.insert( lProposal );

FastTrackProposalCreationTO lTo = new FastTrackProposalCreationBuilder()
.withOpportunity( lOpp.Id )
.withDealHierarchy( 2 , new List< Id >{ lOpp.AccountId } )
.withLastCreatedQuoteWhenExists( lOpp.Id )            
.build();
lTo.customIntegrationsRequired = 'Yes';
lTo.areDependentsIncluded = 'Yes';
lTo.quantity = 5000;
lTo.hasCopay = 'Yes';

FastTrackProposalCreation lProposalCreation = new FastTrackProposalCreation();

Test.startTest();
PlatformEventsHelper.publishStandardOfferCreationEvent( new List< Opportunity >{ lOpp } );
   try {
lProposalCreation.init( lOpp.Id );
lProposalCreation.save( lTo );
lProposalCreation.throwAccessFeeException(lTo,pb);
  } catch(Exception e) {
           System.debug('Error');
       }
Test.stopTest();

Quote lQuote = [ SELECT Id, ( SELECT Id FROM QuoteLineItems ) FROM Quote WHERE OpportunityId = :lOpp.Id ];

System.assertNotEquals( lQuote , null );
System.assert( !lQuote.getSObjects('QuoteLineItems').isEmpty() );
System.assertEquals( lQuote.getSObjects('QuoteLineItems').size() , 4 );
}   
   
    @isTest
    private static void saveAccountWithOpportunityTest(){        
        Opportunity lOpp = [ SELECT Id, AccountId, CurrencyIsoCode, Name FROM Opportunity ];
        List<Account> listAccount = [ SELECT Id, name FROM Account ];
        Quote lProposal = DataFactory.newQuote( lOpp , 'Quote Test', 'Client_Sales_New_Business' );
        lProposal.Contact_Permission__c = 'Allowlist';
        lProposal.Employee_Corporate_Email__c = true;
        lProposal.Total_Number_of_Employees__c = 5000;
        lProposal.Copay__c = 'Yes';
        Database.insert( lProposal );
        
        FastTrackProposalCreationTO lTo = new FastTrackProposalCreationBuilder()
            .withOpportunity( lOpp.Id )
            .withDealHierarchy( 2 , new List< Id >{ lOpp.AccountId } )
            .withLastCreatedQuoteWhenExists( lOpp.Id )            
            .build();
        lTo.customIntegrationsRequired = 'Yes';
        lTo.areDependentsIncluded = 'Yes';
        lTo.quantity = 5000;
        lTo.hasCopay = 'Yes';
        
        FastTrackProposalCreation lProposalCreation = new FastTrackProposalCreation();
        
        Test.startTest();
        PlatformEventsHelper.publishStandardOfferCreationEvent( new List< Opportunity >{ lOpp } );
        
        lProposalCreation.init( lOpp.Id );
        lProposalCreation.save( lTo );
        lProposalCreation.saveAccountWithOpportunity(lOpp.id,listAccount);
        Test.stopTest();
        
        Quote lQuote = [ SELECT Id, ( SELECT Id FROM QuoteLineItems ) FROM Quote WHERE OpportunityId = :lOpp.Id ];
        
        System.assertNotEquals( lQuote , null );
        System.assert( !lQuote.getSObjects('QuoteLineItems').isEmpty() );
        System.assertEquals( lQuote.getSObjects('QuoteLineItems').size() , 4 );
    }  
    
    @isTest
    private static void removeAccountFromOpportunityTest(){        
        Opportunity lOpp = [ SELECT Id, AccountId, CurrencyIsoCode, Name FROM Opportunity ];
        List<Account> listAccount = [ SELECT Id, name FROM Account ];
        Quote lProposal = DataFactory.newQuote( lOpp , 'Quote Test', 'Client_Sales_New_Business' );
        lProposal.Contact_Permission__c = 'Allowlist';
        lProposal.Employee_Corporate_Email__c = true;
        lProposal.Total_Number_of_Employees__c = 5000;
        lProposal.Copay__c = 'Yes';
        Database.insert( lProposal );
        
        FastTrackProposalCreationTO lTo = new FastTrackProposalCreationBuilder()
            .withOpportunity( lOpp.Id )
            .withDealHierarchy( 2 , new List< Id >{ lOpp.AccountId } )
            .withLastCreatedQuoteWhenExists( lOpp.Id )            
            .build();
        lTo.customIntegrationsRequired = 'Yes';
        lTo.areDependentsIncluded = 'Yes';
        lTo.quantity = 5000;
        lTo.hasCopay = 'Yes';
        
        FastTrackProposalCreation lProposalCreation = new FastTrackProposalCreation();
        
        Test.startTest();
        PlatformEventsHelper.publishStandardOfferCreationEvent( new List< Opportunity >{ lOpp } );
        
        lProposalCreation.init( lOpp.Id );
        lProposalCreation.save( lTo );
        lProposalCreation.removeAccountFromOpportunity(lOpp.id,listAccount);
        Test.stopTest();
        
        Quote lQuote = [ SELECT Id, ( SELECT Id FROM QuoteLineItems ) FROM Quote WHERE OpportunityId = :lOpp.Id ];
        
        System.assertNotEquals( lQuote , null );
        System.assert( !lQuote.getSObjects('QuoteLineItems').isEmpty() );
        System.assertEquals( lQuote.getSObjects('QuoteLineItems').size() , 4 );
    } 
    
    @isTest
    private static void refreshOpportunityForNewStageTest(){        
        Opportunity lOpp = [ SELECT Id, AccountId, CurrencyIsoCode, Name FROM Opportunity ];
        List<Account> listAccount = [ SELECT Id, name FROM Account ];
        Quote lProposal = DataFactory.newQuote( lOpp , 'Quote Test', 'Client_Sales_New_Business' );
        lProposal.Contact_Permission__c = 'Allowlist';
        lProposal.Employee_Corporate_Email__c = true;
        lProposal.Total_Number_of_Employees__c = 5000;
        lProposal.Copay__c = 'Yes';
        Database.insert( lProposal );
        
        FastTrackProposalCreationTO lTo = new FastTrackProposalCreationBuilder()
            .withOpportunity( lOpp.Id )
            .withDealHierarchy( 2 , new List< Id >{ lOpp.AccountId } )
            .withLastCreatedQuoteWhenExists( lOpp.Id )            
            .build();
        lTo.customIntegrationsRequired = 'Yes';
        lTo.areDependentsIncluded = 'Yes';
        lTo.quantity = 5000;
        lTo.hasCopay = 'Yes';
        
        FastTrackProposalCreation lProposalCreation = new FastTrackProposalCreation();
        
        Test.startTest();
        PlatformEventsHelper.publishStandardOfferCreationEvent( new List< Opportunity >{ lOpp } );
        
        lProposalCreation.init( lOpp.Id );
        lProposalCreation.save( lTo );
        lProposalCreation.refreshOpportunityForNewStage(lOpp.id,2);
        Test.stopTest();
        
        Quote lQuote = [ SELECT Id, ( SELECT Id FROM QuoteLineItems ) FROM Quote WHERE OpportunityId = :lOpp.Id ];
        
        System.assertNotEquals( lQuote , null );
        System.assert( !lQuote.getSObjects('QuoteLineItems').isEmpty() );
        System.assertEquals( lQuote.getSObjects('QuoteLineItems').size() , 4 );
    }
    
    @isTest
    private static void setStageTest(){        
        Opportunity lOpp = [ SELECT Id, AccountId, CurrencyIsoCode, Name FROM Opportunity ];
        List<Account> listAccount = [ SELECT Id, name FROM Account ];
        Quote lProposal = DataFactory.newQuote( lOpp , 'Quote Test', 'Client_Sales_New_Business' );
        lProposal.Contact_Permission__c = 'Allowlist';
        lProposal.Employee_Corporate_Email__c = true;
        lProposal.Total_Number_of_Employees__c = 5000;
        lProposal.Copay__c = 'Yes';
        Database.insert( lProposal );
        
        FastTrackProposalCreationTO lTo = new FastTrackProposalCreationBuilder()
            .withOpportunity( lOpp.Id )
            .withDealHierarchy( 2 , new List< Id >{ lOpp.AccountId } )
            .withLastCreatedQuoteWhenExists( lOpp.Id )            
            .build();
        lTo.customIntegrationsRequired = 'Yes';
        lTo.areDependentsIncluded = 'Yes';
        lTo.quantity = 5000;
        lTo.hasCopay = 'Yes';
        
        FastTrackProposalCreation lProposalCreation = new FastTrackProposalCreation();
        
        Test.startTest();
        PlatformEventsHelper.publishStandardOfferCreationEvent( new List< Opportunity >{ lOpp } );
        
        lProposalCreation.init( lOpp.Id );
        lProposalCreation.save( lTo );
        lProposalCreation.setStage(lOpp.id,2);
        Test.stopTest();
        
        Quote lQuote = [ SELECT Id, ( SELECT Id FROM QuoteLineItems ) FROM Quote WHERE OpportunityId = :lOpp.Id ];
        
        System.assertNotEquals( lQuote , null );
        System.assert( !lQuote.getSObjects('QuoteLineItems').isEmpty() );
        System.assertEquals( lQuote.getSObjects('QuoteLineItems').size() , 4 );
    }   
    
    @isTest
    private static void resetConditionToApprovalTest(){        
        Opportunity lOpp = [ SELECT Id, AccountId, CurrencyIsoCode, Name FROM Opportunity ];
        List<Account> listAccount = [ SELECT Id, name FROM Account ];
        Quote lProposal = DataFactory.newQuote( lOpp , 'Quote Test', 'Client_Sales_New_Business' );
        lProposal.Contact_Permission__c = 'Allowlist';
        lProposal.Employee_Corporate_Email__c = true;
        lProposal.Total_Number_of_Employees__c = 5000;
        lProposal.Copay__c = 'Yes';
        Database.insert( lProposal );
        
        FastTrackProposalCreationTO lTo = new FastTrackProposalCreationBuilder()
            .withOpportunity( lOpp.Id )
            .withDealHierarchy( 2 , new List< Id >{ lOpp.AccountId } )
            .withLastCreatedQuoteWhenExists( lOpp.Id )            
            .build();
        lTo.customIntegrationsRequired = 'Yes';
        lTo.areDependentsIncluded = 'Yes';
        lTo.quantity = 5000;
        lTo.hasCopay = 'Yes';
        
        FastTrackProposalCreation lProposalCreation = new FastTrackProposalCreation();
        
        Test.startTest();
        PlatformEventsHelper.publishStandardOfferCreationEvent( new List< Opportunity >{ lOpp } );
        
        lProposalCreation.init( lOpp.Id );
        lProposalCreation.save( lTo );
        lProposalCreation.resetConditionToApproval(lTo,2);
        Test.stopTest();
        
        Quote lQuote = [ SELECT Id, ( SELECT Id FROM QuoteLineItems ) FROM Quote WHERE OpportunityId = :lOpp.Id ];
        
        System.assertNotEquals( lQuote , null );
        System.assert( !lQuote.getSObjects('QuoteLineItems').isEmpty() );
        System.assertEquals( lQuote.getSObjects('QuoteLineItems').size() , 4 );
    } 
    
    @isTest
    private static void getProductTest(){   
        Savepoint savePoint = Database.setSavepoint();
        Opportunity lOpp = [ SELECT Id, AccountId, CurrencyIsoCode,Pricebook2Id,Name FROM Opportunity WHERE CurrencyIsoCode = 'USD' ];
        Pricebook2 pb = [SELECT Id,Name, Business_Model__c FROM Pricebook2 WHERE IsActive = true AND IsStandard = true limit 1];
        List<Account> listAccount = [ SELECT Id, name FROM Account ];
        Quote lProposal = DataFactory.newQuote( lOpp , 'Quote Test', 'Client_Sales_New_Business' );
        lProposal.Contact_Permission__c = 'Allowlist';
        lProposal.Employee_Corporate_Email__c = true;
        lProposal.Total_Number_of_Employees__c = 5000;
        lProposal.Copay__c = 'Yes';
        Database.insert( lProposal );
        
        FastTrackProposalCreationTO lTo = new FastTrackProposalCreationBuilder()
            .withOpportunity( lOpp.Id )
            .withDealHierarchy( 2 , new List< Id >{ lOpp.AccountId } )
            .withLastCreatedQuoteWhenExists( lOpp.Id )            
            .build();
        lTo.customIntegrationsRequired = 'Yes';
        lTo.areDependentsIncluded = 'Yes';
        lTo.quantity = 5000;
        lTo.hasCopay = 'Yes';
        
        FastTrackProposalCreation lProposalCreation = new FastTrackProposalCreation();
        
        Test.startTest();
        PlatformEventsHelper.publishStandardOfferCreationEvent( new List< Opportunity >{ lOpp } );
        
        lProposalCreation.init( lOpp.Id );
        lProposalCreation.getSetupFee(lTo, pb);
        lProposalCreation.getProServicesOneFee(lTo, pb);
        lProposalCreation.getProServicesMainFee(lTo, pb); 
        lProposalCreation.saveOpportunity(lTo, pb, savePoint);

        Test.stopTest();
        
        Quote lQuote = [ SELECT Id, ( SELECT Id FROM QuoteLineItems ) FROM Quote WHERE OpportunityId = :lOpp.Id ];
        
    } 
    

}