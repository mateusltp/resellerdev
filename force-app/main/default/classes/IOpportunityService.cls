/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 02-24-2022
 * @last modified by  : gft.samuel.silva@ext.gympass.com
 * Modifications Log 
 * Ver   Date         Author                             Modification
 * 1.0   07-02-2021   gft.samuel.silva@ext.gympass.com   Initial Version
**/
public interface IOpportunityService {

    List< Opportunity > openRenegotiation(Map<Id, Opportunity> mAccountNewOpp);
    
    List<Opportunity> updateStageToLost (List<Opportunity> oppScopeLst);

    List<Opportunity> notifyOpportunityCancellation (List<Opportunity> aOppList);
    
    List<Opportunity> validateW9Form (List<Opportunity> lstOpp, Map<Id,Opportunity> lMapOld);

    List<Opportunity> validate5Photos (List<Opportunity> lstOpp, Map<Id,Opportunity> lMapOld);
    //void validateOpportunityForTagus(List<Opportunity> newOppLst);

    Opportunity createChildOpportunity (String childOppName, Date closeDate, String ownerId, String accountId, String masterOpportunityId, List<Id> accountInOppIds );
    void validateOpportunityStage(Map<Id, Opportunity> opportunitiesByIds);
    /*void setAccountTypePartnerOnOppWon(List<Opportunity> newOppLst); */
}