/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 04-20-2022
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/
public with sharing class ContentDocumentLinkHelper {
    
    Map<Id,ContentDocumentLink> mapOld = (Map<Id,ContentDocumentLink>)Trigger.oldMap;
    Map<Id,ContentDocumentLink> mapNew = (Map<Id,ContentDocumentLink>)Trigger.newMap;     
    private static final PS_Constants constants = PS_Constants.getInstance(); 
    private static final String className = ContentDocumentLinkHelper.class.getName();

    static boolean hasRun = false;

    public void deleteRelatedRecords(){   
    
        if(hasRun){
            return;
        }

        Set<Id> contentDocumentIds = new Set<Id>();
        Set<Id> recordIds = new Set<Id>();
        Set<Id> contentVersionIds = new Set<Id>();
        List<FileObject__c> deleteFileObjLst = new List<FileObject__c>();
        ContentVersionSelector contentVersionSlc = (ContentVersionSelector)Application.Selector.newInstance(ContentVersion.sobjectType); 
        FileObjectSelector fileObjSlc = (FileObjectSelector)Application.Selector.newInstance(FileObject__c.sobjectType);
        fflib_ISObjectUnitOfWork uow = Application.UnitOfWork.newInstance(); 

        for(ContentDocumentLink cdl : mapOld.values()){
            contentDocumentIds.add(cdl.ContentDocumentId);              
            recordIds.add(cdl.LinkedEntityId);            
        }
        
        if(!contentDocumentIds.isEmpty()){
            for(ContentVersion cv : contentVersionSlc.selectByContentDocumentId(contentDocumentIds)){
                contentVersionIds.add(cv.Id);
            }

            Map<Id, List<FIleObject__c>> fileObjMap = fileObjSlc.selectByAccountId(recordIds);
            if( !fileObjMap.isEmpty() && !contentVersionIds.isEmpty() ){
                for(Id iAccId : recordIds){
                    List<FIleObject__c> fileObjLstByAccount = fileObjMap.get(iAccId);
                    for(FIleObject__c fileObj : fileObjLstByAccount){
                        if(contentVersionIds.contains(Id.valueOf(fileObj.conversionId__c))){
                            deleteFileObjLst.add(fileObj);
                        }
                    }
                }
            }

            if(!deleteFileObjLst.isEmpty()){
                uow.registerDeleted(deleteFileObjLst);
                uow.commitWork();
            }
        }

        hasRun = true;
    }
    
}