/**
 * @description       : 
 * @author            : Samuel Silva - GFT (slml@gft.com)
 * @group             : 
 * @last modified on  : 03-11-2021
 * @last modified by  : gft.samuel.silva@ext.gympass.com
 * Modifications Log 
 * Ver   Date         Author                              Modification
 * 1.0   09-14-2020   Samuel Silva - GFT (slml@gft.com)   Initial Version
**/
public with sharing class FastTrackDealDeskHandle {
    
    public FastTrackProposalCreationTO to {get;private set;}
    private ProfessionalServiceRange psPriceRange;
    private String YEARLY_MONTHLY = 'Yearly';
    private Integer ENABLERS_THRESHOLD = 5;
    private Integer ENABLERS_THRESHOLD_2 = 9;  
    private Opportunity opp;
    public static final String FLAT_FEE_TYPE = 'Flat Fee';
    private CaseRepository cases = new CaseRepository();
    private Case dealDeskCase = null;
    private OpportunityRepository opportunities = new OpportunityRepository();
    private Quote proposal;
    private List<String> dealDeskApprovalDescription;
    private String region;
    String enablers;
    String autonomousMp;
    String orderExpirationDays;
    String freeTrial;
    String accessFeeType; 
    String proserviceFrequency;
    String proservicePrice;
    String proservicePriceMain;
    String setupFeeType;
    String eligibility;
    String accessFeeFrequency;
    String setupFeeFrequency;

    public class ProfessionalServiceRange{
        public Decimal minSetupFee {get; set;}
        public Decimal maxSetupFee {get; set;}
        public Decimal minMonthlyFee {get; set;}
        public Decimal maxMonthlyFee {get; set;}

        public ProfessionalServiceRange(String region){
            initProServiceStandardPrice(region);
        }

        
        private void initProServiceStandardPrice(String region){
            Professional_Services_Pricing__mdt proServiceStandardFees = null;
            
            try{
                proServiceStandardFees = [  SELECT Min_Setup_Fee__c, Max_Setup_Fee__c,Min_Maintenance_Fee__c,Max_Maintenance_Fee__c
                                              FROM Professional_Services_Pricing__mdt 
                                              WHERE MasterLabel =: region LIMIT 1];
                this.minSetupFee = proServiceStandardFees.Min_Setup_Fee__c;
                this.maxSetupFee = proServiceStandardFees.Max_Setup_Fee__c;
                this.minMonthlyFee = proServiceStandardFees.Min_Maintenance_Fee__c;
                this.maxMonthlyFee = proServiceStandardFees.Max_Maintenance_Fee__c;
            }catch(System.QueryException e){
                this.minSetupFee = 0;
                this.maxSetupFee = 0;
                this.minMonthlyFee = 0;
                this.maxMonthlyFee = 0;
            }
                
        }
   }

    public FastTrackDealDeskHandle(FastTrackProposalCreationTO proposalTO) {
        this.to = proposalTO;
        
        init();
    }

    private void init(){
        if(to.proposal.sObj.id == null) {return;}     
        initApprovalDescription();     
        proposal = new QuoteRepository().lastForOpportunity(to.opportunityId);        
        opp = new OpportunityRepository().byId(to.opportunityId);
        region = Utils.getRegionByCountry(this.opp.Account.BillingCountry);
        psPriceRange = new ProfessionalServiceRange(region);     
    }

    private void initApprovalDescription(){
        //metadata, customlabel - TBD
        dealDeskApprovalDescription = new List<String>();
        enablers = 'You haven\'t reached the minimum number of Enablers';
        freeTrial = 'Free Trial greater than 7 days';
        orderExpirationDays = 'Order expiration days greater than 90 days';
        accessFeeType = 'Enterprise Subscription fee type is not standard';
        accessFeeFrequency = 'Enterprise Subscription payment frequency is custom';
        setupFeeType = 'Setup fee type is not standard'; 
        setupFeeFrequency = 'Setup fee payment frequency is custom';
        proserviceFrequency = 'Proservice payment frequency is custom';
        proservicePrice = 'Professional Service Setup Fee price out of standard range';
        proservicePriceMain = 'Professional Service  Monthly Fee price out of standard range';
        eligibility = 'Eligibility/MembershipFee with Custom payment settings or Credit Card method';
        autonomousMp = 'Autonomous Market Place is not defined for your Offer';
    }



    public void dealDeskEvaluation() {    
        if(to.proposal.sObj.id == null) {return;}      
          
        to.proposal.dealDeskApprovalNeeded = false;

        if(to.achievedEnablersQuantity < ENABLERS_THRESHOLD ){
            to.proposal.dealDeskApprovalNeeded = false;
            to.notifyEnablersForDealDesk = true;
        }else{
            to.notifyEnablersForDealDesk = false;
        }

        
        if ( to.autonomousMarketPlace=='No' ){
            to.proposal.dealDeskApprovalNeeded = true;
            dealDeskApprovalDescription.add(autonomousMp);
        }

        if(to.orderExpirationDays>90){
            to.proposal.dealDeskApprovalNeeded = true;
            dealDeskApprovalDescription.add(orderExpirationDays);
        }

        if(to.freeTrialDays>7){
            to.proposal.dealDeskApprovalNeeded = true;
            dealDeskApprovalDescription.add(freeTrial);
        }       
        //access fee type and Eligibility 
        if(to.proposal.accessFee != null){
            if(to.proposal.accessFee.sObj.Fee_Contract_Type__c != FLAT_FEE_TYPE){
               to.proposal.dealDeskApprovalNeeded = true;
               dealDeskApprovalDescription.add(accessFeeType);
            }
            
            if(!to.proposal.accessFee.payments.isEmpty()){
                FastTrackProposalCreationTO.Payment iPay = to.proposal.accessFee.payments[0];
                //for(FastTrackProposalCreationTO.Payment iPay : to.proposal.accessFee.payments){
                if(!YEARLY_MONTHLY.contains(iPay.sObj.Frequency__c)){
                    to.proposal.dealDeskApprovalNeeded = true;
                    dealDeskApprovalDescription.add(accessFeeFrequency);
                }
                //}
            
                if(!iPay.eligibilities.isEmpty()){
                    for(FastTrackProposalCreationTO.Eligibility iEligibility : iPay.eligibilities){
                        if(iEligibility.sObj.Payment_Method__c == 'Credit Card'){
                            to.proposal.dealDeskApprovalNeeded = true;
                            dealDeskApprovalDescription.add(eligibility);
                            break;
                        }
                    }
                }
            }
        }

        //ProfServicesOneFee PaymentFrequency and Price Range and Eligibility 
        if(to.proposal.proServicesOneFee != null){
            //paymentFrequency
            if(!to.proposal.proServicesOneFee.payments.isEmpty()){
                FastTrackProposalCreationTO.Payment iPay = to.proposal.proServicesOneFee.payments[0];
                //for(FastTrackProposalCreationTO.Payment iPay : to.proposal.proServicesOneFee.payments){
                if(!YEARLY_MONTHLY.contains(iPay.sObj.Frequency__c)){
                    to.proposal.dealDeskApprovalNeeded = true;
                    dealDeskApprovalDescription.add(proserviceFrequency);
                }
                //}
            /*
                if(!iPay.eligibilities.isEmpty()){
                    for(FastTrackProposalCreationTO.Eligibility iEligibility : iPay.eligibilities){
                        if(iEligibility.sObj.Payment_Method__c == 'Credit Card'){
                            to.proposal.dealDeskApprovalNeeded = true;
                            dealDeskApprovalDescription.add(eligibility);
                            break;
                        }
                    }
                } */
            }
            //listPrice range
            if(to.proposal.proServicesOneFee.sObj.Id != null && (to.proposal.proServicesOneFee.sObj.UnitPrice <= psPriceRange.minSetupFee || 
            to.proposal.proServicesOneFee.sObj.UnitPrice >= psPriceRange.maxSetupFee )){
                   to.proposal.dealDeskApprovalNeeded = true;
                   dealDeskApprovalDescription.add(proservicePrice);
            } 
            // else if(to.proposal.proServicesOneFee.price < psPriceRange.minSetupFee){
            //         to.proposal.dealDeskApprovalNeeded = true;
            //         dealDeskApprovalDescription.add(proservicePrice);
            // } 
        }
        //ProfServicesMainFee PaymentFrequency and Price Range and Eligibility 
        if(to.proposal.proServicesMainFee != null){
            if(!to.proposal.proServicesMainFee.payments.isEmpty()){
                FastTrackProposalCreationTO.Payment iPay = to.proposal.proServicesMainFee.payments[0];
                //for(FastTrackProposalCreationTO.Payment iPay : to.proposal.proServicesMainFee.payments){
                if(!YEARLY_MONTHLY.contains(iPay.sObj.Frequency__c)){
                    to.proposal.dealDeskApprovalNeeded = true;
                    dealDeskApprovalDescription.add(proserviceFrequency);
                   
                }
            /*    //}
                if(!iPay.eligibilities.isEmpty()){
                    for(FastTrackProposalCreationTO.Eligibility iEligibility : iPay.eligibilities){                        
                        to.proposal.dealDeskApprovalNeeded = true;
                        dealDeskApprovalDescription.add(eligibility);
                        break;                        
                    }
                } */
            }
            if( to.proposal.proServicesMainFee.sObj.Id != null && (to.proposal.proServicesMainFee.sObj.UnitPrice <= psPriceRange.minMonthlyFee || 
                to.proposal.proServicesMainFee.sObj.UnitPrice >= psPriceRange.maxMonthlyFee )){
                   to.proposal.dealDeskApprovalNeeded = true;
                   dealDeskApprovalDescription.add(proservicePriceMain);
            }             
        }

        if(!dealDeskApprovalDescription.isEmpty()){
            to.proposal.dealDeskDescription = dealDeskApprovalDescription;
        }

        //Setup and Eligibility 
     /*   if(!to.proposal.setupFee.payments.isEmpty()){
            FastTrackProposalCreationTO.Payment iPay = to.proposal.setupFee.payments[0];
            if(!YEARLY_MONTHLY.contains(iPay.sObj.Frequency__c)){
                to.proposal.dealDeskApprovalNeeded = true;
                dealDeskApprovalDescription.add(setupFeeFrequency);            
            }

            if(!iPay.eligibilities.isEmpty()){
                for(FastTrackProposalCreationTO.Eligibility iEligibility : iPay.eligibilities){
                    to.proposal.dealDeskApprovalNeeded = true;
                    dealDeskApprovalDescription.add(setupFeeFrequency);
                    break;
                }
            } 
        } */

        
        proposal.Deal_Desk_Approval_Needed__c = to.proposal.dealDeskApprovalNeeded;
        proposal.Deal_Desk_Description__c = String.join(dealDeskApprovalDescription, ','); 
        proposal.Skip_Enablers_Flow__c = true;
        new QuoteRepository().edit(proposal);
        
    }

    public void submitCaseForApproval(){        
        dealDeskCaseWhenExists();        
    }

    public void dealDeskCaseWhenExists(){
        try {        
            dealDeskCase = cases.getOpenCasebyQuoteId(to.quoteId); 
        }catch (System.QueryException e) {
            dealDeskCase = cases.createDealDeskCaseForOpp(this.opp.id, to.quoteId, to.proposal.dealDeskDescription);
            cases.add(dealDeskCase, true);
            Boolean isShared = cases.manualShareRead(dealDeskCase.Id, opp.OwnerId);
        }
    }

    public FastTrackProposalCreationTO approveOrRejectDealDeskCase (Id recordId, Boolean isApproved){  
        
        try {
            this.dealDeskCase = cases.byId(recordId);   
            to.proposal.dealDeskApproved = isApproved;
            this.proposal.Deal_Desk_Approved__c = to.proposal.dealDeskApproved;
            dealDeskCase.status = to.proposal.dealDeskApproved ? 'Approved' : 'Rejected';      
            cases.edit(dealDeskCase);
        // opportunities.LockOrUnlockOpportunityById(opp.Id);
            proposal.Skip_Enablers_Flow__c = true;
            new QuoteRepository().edit(proposal);
            //opportunities.edit(this.opp);        
            return to;
        } catch (DmlException e) {
            throw new DmlException (e.getMessage());
        }

    }

    public FastTrackProposalCreationTO approveOrRejectDealDeskSpecialist (Id oppId, Id recordId, Boolean isApproved){
        Case lSpecialistCase = new Case( 
            Id = recordId ,
            Status = isApproved ? 'Approved' : 'Rejected' 
        );

        cases.edit(lSpecialistCase);

        if( isApproved ){
            new OpportunityRepository().edit(
                new Opportunity( Id = oppId , Specialist_Approved__c = true )
                );
        }

        return to;
    }
}