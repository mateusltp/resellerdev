/**
* @author vinicius.ferraz
* @description QuoteRepository unit test
*/
@isTest(seeAllData=false)
public class QuoteRepositoryTest {
    
    public static final String Quote_NAME_TEST_1 = 'Test1';

    @TestSetup
    static void createData(){        
        Account acc = generateAccount();
        insert acc;
        Opportunity opp = generateOpportunity(acc);
        insert opp;

        insert generateQuote(opp,'1');
    }

    @isTest
    static void testById(){
        Quote quote = [select Id,Name from Quote limit 1];
        QuoteRepository quotes = new QuoteRepository();
        Quote qt = null;
        try{
            qt = quotes.byId(quote.Id);
        }catch(System.QueryException e){
            System.assert(false);
        }
        System.assertEquals(qt.Id,Quote.Id);
    }

    @isTest
    static void testAdd(){
        QuoteRepository quotes = new QuoteRepository();
        Opportunity opp = [select Id,Name from Opportunity limit 1];
        Quote qt = generateQuote(opp,'2');        
        System.assertEquals(qt.Id,null);
        try{
            quotes.add(qt);
        }catch(System.DmlException e){
            System.assert(false);
        }
        System.assertNotEquals(qt.Id,null);
    }

    @isTest
    static void testEdit(){
        QuoteRepository quotes = new QuoteRepository();
        Opportunity opp = [select Id,Name from Opportunity limit 1];
        Quote qt = generateQuote(opp,'3');        
        System.assertEquals(qt.Id,null);
        try{
            quotes.edit(qt);
        }catch(System.DmlException e){
            System.assert(true);
        }
        System.assertEquals(qt.Id,null);
        
        quotes.add(qt);
        
        try{
            quotes.edit(qt);
        }catch(System.DmlException e){
            System.assert(false);
        }
        System.assertNotEquals(qt.Id,null);
    }

    @isTest
    static void testAddOrEdit(){
        QuoteRepository quotes = new QuoteRepository();
        Opportunity opp = [select Id,Name from Opportunity limit 1];
        Quote qt = generateQuote(opp,'4');        
        System.assertEquals(qt.Id,null);
        
        try{
            quotes.addOrEdit(qt);
        }catch(System.DmlException e){
            System.assert(false);
        }
        System.assertNotEquals(qt.Id,null);
        
        try{
            quotes.addOrEdit(qt);
        }catch(System.DmlException e){
            System.assert(false);
        }
        System.assertNotEquals(qt.Id,null);
    }

    @isTest
    static void testGetLastForOpportunity(){
        QuoteRepository quotes = new QuoteRepository();
        Opportunity opp = [select Id,Name from Opportunity limit 1];
        Quote qt = null; 
        
        try{
            qt = quotes.lastForOpportunity((String)opp.Id);
        }catch(System.DmlException e){
            System.assert(false);
        }
        System.assertNotEquals(qt.Id,null);
    }

    @isTest
    static void testAddOrEditFlow(){
        QuoteRepository quotes = new QuoteRepository();
        Opportunity opp = [select Id,Name from Opportunity limit 1];
        Quote qt = generateQuote(opp,'4');        
        System.assertEquals(qt.Id,null);
        
        try{
            quotes.editWithFlow(qt);
        }catch(System.DmlException e){
            System.assert(false);
        }
        System.assertNotEquals(qt.Id,null);
         
        try{
            quotes.addOrEditWithFlow(qt);
        }catch(System.DmlException e){
            System.assert(false);
        }
        System.assertNotEquals(qt.Id,null);
    }

    @isTest
    static void testGetQuotesAsMap(){
        QuoteRepository quotes = new QuoteRepository();
        Opportunity opp = [select Id,Name from Opportunity limit 1];
        Quote qt = generateQuote(opp,'5');        
        System.assertEquals(qt.Id,null);
        
        try{
            quotes.getLastForOpportunitiesAsOppIdToQuoteMap(new List<Id>{opp.Id});
        }catch(System.DmlException e){
            System.assert(false);
        }
    }                       

    private static Account generateAccount(){
        Id rtId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gympass_Entity').getRecordTypeId();
        Account acc = new Account();
        acc.name='AcademiaBrasilCompanyPai';
        acc.RecordTypeId = rtId;
        acc.GP_Status__c = 'Active';
        acc.billingState = 'Minas Gerais';
        acc.CAP_Value__c = 120;
        acc.BillingCity = 'CityAcademiaBrasil';
        acc.billingStreet = 'Rua academiabrasilpai';
        acc.billingCountry = 'Brazil';
        acc.Gym_Type__c = 'Studios';
        acc.Gym_Classes__c = 'Cardio';
        acc.Types_of_ownership__c = Label.franchisePicklist;
        acc.Subscription_Type__c = 'Value per class';
        acc.Subscription_Period__c = 'Monthy value';
        acc.Subscription_Type_Estimated_Price__c    = 100;
        acc.Has_market_cannibalization__c = 'No';
        acc.Exclusivity__c = 'Yes';
        acc.Exclusivity_End_Date__c = Date.today().addYears(1);
        acc.Exclusivity_Partnership__c = 'Full Exclusivity';
        acc.Exclusivity_Restrictions__c= 'No';
        acc.Website = 'testing@tesapex.com';
        acc.Gym_Email__c = 'gymemail@apex.com';
        acc.Phone = '3222123123';
        acc.Can_use_logo__c = 'Yes';
        acc.Legal_Registration__c = 12123;
        acc.Legal_Title__c = 'Title LTDA';
        acc.Gyms_Identification_Document__c = 'CNPJ'; 
        return acc;
    }
    private static Quote generateQuote(Opportunity opp, String name){
        Quote qt = new Quote();  
        qt.Name = opp.Name+name;
        qt.OpportunityId = opp.Id;   
        qt.License_Fee_Waiver__c = 'No';  
        return qt;
    }

    private static Opportunity generateOpportunity(Account acc){
        Id oppRtId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        Opportunity accOpp = new Opportunity();
        accOpp.recordTypeId = oppRtId;
        accOpp.AccountId = acc.id;
        accOpp.Name = Quote_NAME_TEST_1; 
        accOpp.CMS_Used__c = 'Yes';     
        accOpp.Gym_agreed_to_an_API_integration__c  = 'Yes';
        accOpp.Club_Management_System__c = 'Companhia Athletica';
        accOpp.Integration_Fee_Deduction__c = 'No';
        accOpp.CloseDate = Date.today();
        accOpp.Success_Look_Like__c = 'Yes';
        accOpp.Success_Look_Like_Description__c = 'Money money';
        accOpp.StageName = 'Proposta Enviada';
        accOpp.Type = 'Expansion';  
        accOpp.Country_Manager_Approval__c = true;
        accOpp.Payment_approved__c = true;   
        accOpp.CurrencyIsoCode = 'BRL';
        accOpp.Gympass_Plus__c = 'Yes';
        accOpp.Standard_Payment__c = 'Yes';
        accOpp.Request_for_self_checkin__c = 'Yes';  
        return accOpp;
    }
}