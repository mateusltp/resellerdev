/**
* @File Name          : BatchGFServiceUtilization.cls
* @Description        :
* @Author             : JRDL@GFT.com
* @Group              :
* @Last Modified By   : 
* @Last Modified On   : 
* @Modification Log   :
* Ver       Date            Author      		    Modification
* 1.0    25/09/2021   		JRDL@GFT.com     		Initial Version
* 2.0    19/11/2021         PAPK@GFT.com            Daily to Monthly Schedule Update
**/
global class BatchGFServiceUtilization implements Database.Batchable<sObject>, Database.AllowsCallouts {
    
    global Integer monthsFromLauch = 4;
    global Integer monthsToEnd = 3;
    global Integer unansweredMonths = 1;
    global Integer answeredMonths = 3;
    global Integer todayYear = Date.today().year();
    global Integer todayMonth = Date.today().month();
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        
        Id rtAcc = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId(); 
        
        Id rtOppClientNew = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        Id rtOppSMBNew = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SMB_New_Business').getRecordTypeId();
        Id rtOppClientReneg = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Success_Renegotiation').getRecordTypeId();
        Id rtOppSMBReneg = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SMB_Success_Renegotiation').getRecordTypeId(); 
        
        List<Id> oppRTList = new List<Id>{rtOppClientNew, rtOppSMBNew, rtOppClientReneg, rtOppSMBReneg};
            
            
            Integer min = todayMonth - monthsFromLauch;
        Integer max = todayMonth + monthsToEnd;
        
        
        return Database.getQueryLocator([
            SELECT Id, Data_do_Lancamento__c, AccountId, Account.Type, Account.RecordTypeId, Account.Proposal_End_Date__c, RecordTypeId, StageName, Synced_Quote_End_Date__c,Account.EnvioServiceUtilization__c, Account.RespostaServiceUtilization__c,
            Account.Name,Account.Payment_method__c,Account.GP_NumberOfDependents__c,Account.Business_Unit__c,Account.Tagus_Based__c,Account.Size__c,Account.Owner.Name,Account.ID_Gympass_CRM__c,Account.NumberOfEmployees,Account.BillingCountry,Account.RecordType.DeveloperName,Account.Contract_Signed_At__c,Account.Launch_date__c
            FROM Opportunity
            WHERE StageName = 'Lançado/Ganho' AND 
            Account.RecordTypeId = :rtAcc AND
            Account.Type != 'Cancel' AND
            RecordTypeId IN : oppRTList AND 
            ((CALENDAR_YEAR(Data_do_Lancamento__c) = :todayYear AND CALENDAR_MONTH(Data_do_Lancamento__c) <= :min) OR
             (CALENDAR_YEAR(Data_do_Lancamento__c) < :todayYear)) AND
            (Account.Proposal_End_Date__c = null OR ((CALENDAR_YEAR(Account.Proposal_End_Date__c) = :todayYear AND CALENDAR_MONTH(Account.Proposal_End_Date__c) >= :max)) OR
             (CALENDAR_YEAR(Account.Proposal_End_Date__c) > :todayYear)) 
        ]);
        
    }
    
    global void execute(Database.BatchableContext bc, List<Opportunity> scope) {
        
        Map<Id, Account> mapAccToUpdate = new Map<Id, Account>();
        Map<Id, Contact> mapUpdateCon = new Map<Id, Contact>();
        Map<String, Account> mapAcc = new Map<String, Account>();
        List<Contact> lstCon = new List<Contact>();
        
        for(Opportunity o: scope){
            system.debug('@@@@@@@@' + o);
            if(o.Data_do_Lancamento__c.monthsBetween(Date.Today()) >= monthsFromLauch &&
               (o.Account.Proposal_End_Date__c == null || Date.Today().monthsBetween(o.Account.Proposal_End_Date__c) >= monthsToEnd)){
                   //completou 4 (monthsFromLauch) meses este mês e nunca recebeu a survey
                   if(o.Data_do_Lancamento__c.monthsBetween(Date.Today()) == monthsFromLauch && o.Account.EnvioServiceUtilization__c == null){
                       o.Account.EnvioServiceUtilization__c = System.today();
                       mapAccToUpdate.put(o.AccountId, o.Account);
                       mapAcc.put(o.AccountId, o.Account);
                   }
                   //completou 4 (monthsFromLauch) meses este mês, já recebeu a survey antes, mas não neste mês
                   if(o.Data_do_Lancamento__c.monthsBetween(Date.Today()) == monthsFromLauch && o.Account.EnvioServiceUtilization__c != null && o.Account.EnvioServiceUtilization__c.Month() != todayMonth){
                       o.Account.EnvioServiceUtilization__c = System.today();
                       mapAccToUpdate.put(o.AccountId, o.Account);
                       mapAcc.put(o.AccountId, o.Account);
                   }
                   //já tem mais de 4 meses, já recebeu a survey antes, mas não este mês 
                   if(o.Data_do_Lancamento__c.monthsBetween(Date.Today()) > monthsFromLauch && o.Account.EnvioServiceUtilization__c != null && o.Account.EnvioServiceUtilization__c.Month() != todayMonth){
                       system.debug('segundo if ---' + o);
                       //recebeu 1 (unansweredMonths) mês atrás e não respondeu 
                       if(o.Account.EnvioServiceUtilization__c.monthsBetween(Date.Today()) == unansweredMonths && 
                          (o.Account.EnvioServiceUtilization__c > o.Account.RespostaServiceUtilization__c || o.Account.RespostaServiceUtilization__c == null)){
                              o.Account.EnvioServiceUtilization__c = System.today();
                              mapAccToUpdate.put(o.AccountId, o.Account);
                              mapAcc.put(o.AccountId, o.Account);
                          }
                       //recebeu há 3 (answeredMonths) meses atrás e respondeu - saindo da quarentena
                       if(o.Account.EnvioServiceUtilization__c.monthsBetween(Date.Today()) == answeredMonths &&
                          o.Account.EnvioServiceUtilization__c <= o.Account.RespostaServiceUtilization__c){
                              o.Account.EnvioServiceUtilization__c = System.today();
                              mapAccToUpdate.put(o.AccountId, o.Account);
                              mapAcc.put(o.AccountId, o.Account);
                          }
                   }
                   //já tem mais de 4 (monthsFromLauch) meses e nunca recebeu a survey 
                   if(o.Data_do_Lancamento__c.monthsBetween(Date.Today()) > monthsFromLauch && o.Account.EnvioServiceUtilization__c == null){
                       o.Account.EnvioServiceUtilization__c = System.today();
                       mapAccToUpdate.put(o.AccountId, o.Account);
                       mapAcc.put(o.AccountId, o.Account);
                   }
               }
        }
        
        system.debug('-------------' + mapAccToUpdate);
        
        if(!mapAccToUpdate.isEmpty()){
            for(Contact con: [Select Id, Status_do_contato__c, GFSendSurvey__c, Email, Primary_HR_Contact__c,DecisionMaker__c, Role__c,AccountId,MobilePhone,FirstName,LastName,Language_Code__c,HasOptedOutOfEmail FROM Contact WHERE AccountID IN :mapAccToUpdate.keySet()]){
                if(con.Status_do_contato__c == 'Ativo' && con.Email != null && con.Primary_HR_Contact__c == TRUE){
                    con.GFSendSurvey__c = 'Client Service Utilization';
                    mapUpdateCon.put(con.Id, con);
                    lstCon.add(con);
                }
            }
        }
        
        Qualtrics__mdt mdtQualtrics = Qualtrics__mdt.getInstance('Client_Service_Utilization');
        if(!mapAccToUpdate.isEmpty() && !mapUpdateCon.isEmpty() && mdtQualtrics.Active__c == true){
            Qualtrics.createMailingList('Client Service Utilization',lstCon, mapAcc);
        }
        
        
        if (!mapAccToUpdate.isEmpty()) {
            update mapAccToUpdate.values();
        }
        
        if(!mapUpdateCon.isEmpty()){
            update mapUpdateCon.values();
        }
        
        
    }
    
    global void finish(Database.BatchableContext bc) {
        
    }
}