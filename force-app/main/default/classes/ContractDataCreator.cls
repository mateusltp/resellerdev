/**
* @description       : 
* author            : Laerte Kimura - leka@gft.com
* @group             : 
* @last modified on  : 19-05-2021
* @last modified by  : Laerte Kimura - leka@gft.com
* Modifications Log 
* Ver   Date         Author                         Modification
* 1.0   19-05-2021   Laerte Kimura - leka@gft.com   Created 
**/

public class ContractDataCreator {
    public class ContractDataHandlerException extends Exception {}
        
    private List<APXT_Redlining__Contract_Agreement__c> MainContractList; 
    private Map<string, List<String>> MapObjectAndFieldsToTrack;
    private List<APXT_Redlining__Contract_Agreement__c> AllContracts;
    private List<Quote> AllQuotes; 
    private List<Opportunity> AllOpportunity;
    private List<QuoteLineItem> AllEnterpriseSubscriptions;
    private List<Payment__c> AllPayments;
    private List<Eligibility__c> AllEligibility;
    private List<Assert_Data__c> AllContractData;
    private List<ContractDataObject> AllContractsToCompare;
    
    
    private Set<Id> SetContractIds;
    private Set<id> SetQuoteIds;
    private Set<id> SetOpportunityIds;
    private Set<id> SetEnterpriseSubscriptionIds;
    private Set<id> SetEligibilityIds;
    private Set<id> SetPaymentIds;
    private Set<id> SetMainOpportunityIds;
    
    
    
    public ContractDataCreator(List<APXT_Redlining__Contract_Agreement__c> mainContractList){
        this.SetContractIds= new Set<Id>();
        this.SetQuoteIds = new Set<Id>();
        this.SetOpportunityIds = new Set<Id>();
        this.SetEnterpriseSubscriptionIds = new Set<Id>();
        this.SetEligibilityIds = new Set<Id>();
        this.SetPaymentIds = new Set<Id>();
        this.SetMainOpportunityIds = new Set<Id>();
        this.MainContractList = mainContractList;
        this.MapObjectAndFieldsToTrack = this.getObjectAndFieldsToTrackChanges();
        this.loadSetIdsOfAllRelatedObjects();
        if  (!this.AllContractsToCompare.isEmpty()){
            this.loadAllObjects();
            this.loadContracts();
            
            List<Assert_Data__c> ContractDataToSave = new List<Assert_Data__c>();
            for (ContractDataObject contrato : this.AllContractsToCompare){
                contrato.createRecord(this.MapObjectAndFieldsToTrack);
                ContractDataToSave.addall(contrato.getAllContractDataRecords());
            }
            
            upsert ContractDataToSave;
        }
        
    }
    
    private void LoadSetIdsOfAllRelatedObjects(){
        
        this.AllContractsToCompare = new List<ContractDataObject>();
        
        try{
            for (APXT_Redlining__Contract_Agreement__c contractAgreementEach: this.MainContractList ){
                if(contractAgreementEach.APXT_Renegotiation__c){
                    
                    ContractDataObject contract = new ContractDataObject();
                    contract.ContractId = contractAgreementEach.id;
                    contract.ParentContractId = contractAgreementEach.APXT_Redlining__Contract_Agreement_Family_Parent__c;                
                    contract.OpportunityId = contractAgreementEach.Opportunity__c;
                    contract.ParentOpportunityId = contractAgreementEach.Parent_Opportunity__c; 
                    contract.QuoteId = contractAgreementEach.Proposal__c   ;
                    contract.ParentQuoteId = contractAgreementEach.Parent_Proposal__c;
                    
                    this.SetContractIds.add(contractAgreementEach.Id);
                    this.SetContractIds.add(contractAgreementEach.APXT_Redlining__Contract_Agreement_Family_Parent__c);
                    this.SetOpportunityIds.add(contractAgreementEach.Parent_Opportunity__c);
                    this.SetOpportunityIds.add(contractAgreementEach.Opportunity__c);
                    this.SetMainOpportunityIds.add(contractAgreementEach.Opportunity__c); /* Used to Contract data only */
                    this.SetQuoteIds.add(contractAgreementEach.Proposal__c);
                    this.SetQuoteIds.add(contractAgreementEach.Parent_Proposal__c);
                    
                    this.AllContractsToCompare.add(contract);
                }
            }
            
            if (!this.AllContractsToCompare.isEmpty()){
                /* Load Eligibility , Payment and Quote_Line_Item set Ids */
                Set<Id> querySetId = this.SetQuoteIds;
                this.AllEligibility = database.query(this.getsObjectQuery('eligibility__c', 'Payment__r.Quote_Line_Item__c, Quote_Line_Item__r.QuoteId ', 'WHERE Payment__r.Quote_Line_Item__r.QuoteId IN : querySetId', 'AND Payment__r.Quote_Line_Item__r.Fee_Type__c = \'Enterprise Subscription\''));
                
                for (Eligibility__c eligibility: AllEligibility){
                    this.SetEligibilityIds.add(eligibility.Id);
                    this.SetPaymentIds.add(eligibility.Payment__c);
                    this.SetEnterpriseSubscriptionIds.add(eligibility.Payment__r.Quote_Line_Item__c);
                }
                
                Set<Id> queryOppSetId = this.SetMainOpportunityIds;
                this.AllContractData = database.query(this.getsObjectQuery('assert_data__c' , 'WHERE Opportunity__c IN : queryOppSetId' ,''));
            }
        }
        catch(ContractDataHandlerException ex){
            throw new ContractDataHandlerException('Error in LoadSetIdsOfAllRelatedObjects. ' + ex.getMessage());
        }
    }
    
    private void loadAllObjects(){
        
        Set<id> setIdsToQuery= this.SetContractIds;
        
        for(String ObjectToTrack: this.MapObjectAndFieldsToTrack.keyset()){
            setIdsToQuery= new Set<id>();
            try{
                switch on ObjectToTrack.toLowerCase(){
                    /*  when 'eligibility__c' WAS FULLFILLED BEFORE */  
                    when 'quote' {
                        setIdsToQuery= this.SetQuoteIds;
                        string query = this.generateQueryOfTrackObjects(ObjectToTrack.toLowerCase() , MapObjectAndFieldsToTrack.get(ObjectToTrack) ,'') ; 
                        AllQuotes = database.query(query += ': setIdsToQuery');
                    }
                    when 'opportunity' {
                        setIdsToQuery= this.SetOpportunityIds;
                        string query = this.generateQueryOfTrackObjects(ObjectToTrack.toLowerCase() , MapObjectAndFieldsToTrack.get(ObjectToTrack) ,'') ; 
                        AllOpportunity = database.query(query += ': setIdsToQuery');
                    }
                    when 'quotelineitem' {
                        setIdsToQuery= this.SetEnterpriseSubscriptionIds;
                        string query = this.generateQueryOfTrackObjects(ObjectToTrack.toLowerCase() , MapObjectAndFieldsToTrack.get(ObjectToTrack) ,  'QuoteId , Fee_type__c' ) ; 
                        AllEnterpriseSubscriptions = database.query(query += ': setIdsToQuery');
                    }
                    when 'payment__c' {
                        setIdsToQuery= this.SetPaymentIds;
                        string query = this.generateQueryOfTrackObjects(ObjectToTrack.toLowerCase() , MapObjectAndFieldsToTrack.get(ObjectToTrack) ,  'Quote_Line_Item__r.QuoteId, Opportunity_Payment__c' ) ;
                        AllPayments = database.query(query += ': setIdsToQuery');
                    }  
                }
                
            }
            catch (ContractDataHandlerException ex){
                throw new ContractDataHandlerException('Error loading objects in loadAllObjects. '  + ex.getMessage());
                
            }
            
        }
        
    }
    
    
    private void loadContracts(){
        try{
            for (ContractDataObject contract : this.AllContractsToCompare){
                for(Quote quote : AllQuotes){
                    if ((quote.id == contract.QuoteId) || (quote.id == contract.ParentQuoteId)){
                        contract.ProposalList.add(quote);
                    }
                }
                for (QuoteLineItem enterpriseSubscription: AllEnterpriseSubscriptions){
                    if ((enterpriseSubscription.QuoteId == contract.QuoteId) || enterpriseSubscription.QuoteId == contract.ParentQuoteId){
                        contract.EnterpriseSubscriptionList.add(enterpriseSubscription);
                    }
                }
                for(Opportunity opportunity : this.AllOpportunity){
                    if ((opportunity.id == contract.OpportunityId) || (opportunity.id == contract.ParentOpportunityId)){
                        contract.OpportunityList.add(opportunity);
                    }
                }
                for(Payment__c payment : this.AllPayments){
                    for (Quotelineitem enterpriseSubscription: contract.EnterpriseSubscriptionList){
                        if (enterpriseSubscription.id == payment.Quote_Line_Item__c){
                            contract.PaymentList.add(payment);
                        }
                    }
                    
                }
                for(Eligibility__c eligibility : this.AllEligibility){
                    for (Payment__c payment:  contract.PaymentList){
                        if (eligibility.payment__c == payment.id ){
                            contract.EligibilityList.add(eligibility);
                        }
                    }
                }
                for (Assert_Data__c contractData : this.AllContractData){
                    if (contract.OpportunityId == contractData.opportunity__c){
                        contract.ContractDataRecords.add(contractData);
                    }
                }
            }   
        }
        catch (ContractDataHandlerException ex){
            throw new ContractDataHandlerException('Error in loadContracts. ' + ex.getMessage());
        }
        
    }
    
    
    private string generateQueryOfTrackObjects(String objectName,  List<String> fieldsToTrackChanges, string joinLink ){
        
        Map<String, Schema.SObjectType> mapSchemaObject = Schema.getGlobalDescribe();
        Schema.SObjectType schemaObject = mapSchemaObject.get(objectName);
        Schema.DescribeSObjectResult resultOfDescribe = schemaObject.getDescribe();
        Map<String,Schema.SObjectField> fieldsToAddInQuery = resultOfDescribe.fields.getMap();
        
        String listOfFieldsSeparatedByComma = '';
        
        try {
            for(String fieldFoundInObjectDescription : fieldsToAddInQuery.keyset()) 
            {
                
                if (fieldsToTrackChanges.contains(fieldFoundInObjectDescription.toLowerCase())){
                    listOfFieldsSeparatedByComma += fieldFoundInObjectDescription.toLowerCase() + ' , ';
                }   
            }
            if (!String.isEmpty(listOfFieldsSeparatedByComma)){
                if (!String.isEmpty(joinLink)){
                    listOfFieldsSeparatedByComma = 'SELECT Id, ' + joinLink + ' , ' + listOfFieldsSeparatedByComma.left(listOfFieldsSeparatedByComma.length() - 2) + ' FROM ' + objectName + ' WHERE Id IN ';
                }else{
                    listOfFieldsSeparatedByComma = 'SELECT Id, ' + listOfFieldsSeparatedByComma.left(listOfFieldsSeparatedByComma.length() - 2) + ' FROM ' + objectName + ' WHERE Id IN ';
                }
            }
            return listOfFieldsSeparatedByComma;
        }
        catch(ContractDataHandlerException ex){
            throw new ContractDataHandlerException('Error generating query in generateQueryOfTrackObjects.'  + ex.getMessage());
        }
    }
    
    
    
    private Map<String, List<String>> getObjectAndFieldsToTrackChanges(){
        
        try {
            Map<string, List<String>> mapObjectFieldsToTrackChanges = new Map<string, List<String>>();
            
            List<Assert_Data_Objects__mdt> listSetupFieldsToTrackChanges = [SELECT Object_Name__c  , Field_Name__c FROM Assert_Data_Objects__mdt WHERE contract__c = true];
            
            for (Assert_Data_Objects__mdt objContractDataObject: listSetupFieldsToTrackChanges){
                if (mapObjectFieldsToTrackChanges.containsKey(objContractDataObject.Object_Name__c)){
                    List<string> fieldListToTrack = mapObjectFieldsToTrackChanges.get(objContractDataObject.Object_Name__c);
                    fieldListToTrack.add(objContractDataObject.Field_Name__c.toLowerCase());
                    mapObjectFieldsToTrackChanges.put(objContractDataObject.Object_Name__c,fieldListToTrack);
                }else{
                    mapObjectFieldsToTrackChanges.put(objContractDataObject.Object_Name__c, new List<string>{objContractDataObject.Field_Name__c.toLowerCase()});
                }
            }
            if (listSetupFieldsToTrackChanges.isEmpty()){
                throw new ContractDataHandlerException('No records found to compare values in the process in function getObjectAndFieldsToTrackChanges'); 
            }else{
                return mapObjectFieldsToTrackChanges;
            }
        }
        catch(ContractDataHandlerException ex){
            throw new ContractDataHandlerException('Error to find the mapped objects and fields to process in function getObjectAndFieldsToTrackChanges.' + ex.getMessage() ); 
        }
    }    
    
    
    private String getsObjectQuery(string objectName, string whereClause , string ConditionClause){
        return 
            'SELECT ' + Utils.getSobjectFieldsDevNameForQuery(objectName) + 
            ' FROM ' + objectName + ' ' +
            whereClause + ' ' +
            ConditionClause ;
    }
    
    private String getsObjectQuery(string objectName, string relationalFields, string whereClause , string ConditionClause){
        
        return 
            'SELECT ' + Utils.getSobjectFieldsDevNameForQuery(objectName) + ', ' + relationalFields + 
            ' FROM ' + objectName + ' ' +
            whereClause + ' ' +
            ConditionClause ;
    } 
}