public class ApprovalItemTriggerHelper {
    private Map<Id, List<Approval_Item__c>> allItemsByCaseId = new Map<Id, List<Approval_Item__c>>();
    private Set<Id> caseIds = new Set<Id>();
    private Map<Id, Case> casesToApproveOrRejectById = new Map<Id, Case>();
    private List<Opportunity> oppsToApproveOrReject = new List<Opportunity>();
    private List<Quote> proposalsToApproveOrReject = new List<Quote>();
    private Map<Id,List<FastTrackDealDeskHandler.DealDeskConditions>> lLstApprovalConditionsByQuote = new Map<Id,List<FastTrackDealDeskHandler.DealDeskConditions>>();
    private Map<Id, List<Approval_Item__c>> itemsApprovedByCase = new Map<Id, List<Approval_Item__c>>();
    private Set<Id> allOppsToApprove = new Set<Id>();
    private Map<Id,Case> operationalCasesApprovedByIds = new Map<Id,Case>();
    private Map<Id, Opportunity> oppsById = new Map<Id, Opportunity>();
    private Map<Id, List<Assert_Data__c>> assertDataByOppId = new Map<Id, List<Assert_Data__c>>();
    private Map<Id, Map<String,QuoteLineItem>> mapLineItemsByFeeTypeByOppId = new Map<Id, Map<String,QuoteLineItem>> ();
    private Map<String, Approval_Items_Case_Mapping__mdt> approvalItemsMappingByNameMdt = Approval_Items_Case_Mapping__mdt.getAll();

    
    private List< Assert_Data__c > lLstAssertDataToUpdt = new List< Assert_Data__c >();
    Case dealDeskCase = null;

    public void checkAllItemsAreApprovedOrRejected(List<Approval_Item__c> approvalItems){
        getApprovalItemsByCase(approvalItems);

        evaluateCasesToApproveOrReject();

        if(casesToApproveOrRejectById.size()> 0 && casesToApproveOrRejectById.values().size()>0){
            Database.update(casesToApproveOrRejectById.values());

            if(oppsToApproveOrReject.size()>0)
                Database.update(oppsToApproveOrReject);

            if(proposalsToApproveOrReject.size()>0)
                Database.update(proposalsToApproveOrReject);
        }

        if(this.itemsApprovedByCase.size() > 0){

            getOppMainInfo();
            getAssertDataByOppId();

            FastTrackDealDeskHandler lDeadDeskHandle = new FastTrackDealDeskHandler();
            lDeadDeskHandle.setTermsQualifiedMap();

            if( this.operationalCasesApprovedByIds.size() >0 ){
                this.operationalCasesApprovedByIds = getCasesOperational(operationalCasesApprovedByIds.keySet());
    
            }
 
            for(Id csId : this.itemsApprovedByCase.keySet()){
                Case cs = itemsApprovedByCase.get(csId)?.get(0).Case__r;
                if(casesToApproveOrRejectById.get(csId) != null){
                    String caseRecordtypeDevName = cs.Recordtype.DeveloperName;
                    Opportunity opp = oppsById.get(cs.OpportunityId__r.Id);
                    List<Assert_Data__c> assertDatas = !this.assertDataByOppId.isEmpty() && this.assertDataByOppId.get(opp.Id)!= null ?this.assertDataByOppId.get(opp.Id): new List<Assert_Data__c>();
    
                    if( caseRecordtypeDevName == 'Deal_Desk_Approval' ){     
                        List<Approval_Item__c> itemList =  this.allItemsByCaseId.get(csId);
                        Map<String,QuoteLineItem> lineItemsByFeeType = this.mapLineItemsByFeeTypeByOppId?.get(opp.Id);
    
                        lLstAssertDataToUpdt.addAll(lDeadDeskHandle.updateAssertData( opp, assertDatas , lineItemsByFeeType, allItemsByCaseId.get(csId))); 
    
                    }else if( caseRecordtypeDevName == 'Deal_Desk_Operational' && this.operationalCasesApprovedByIds.get(csId) != null ) {
                        cs = this.operationalCasesApprovedByIds.get(csId);
                        Map<String, Approval_Item__c> itemsRejectedByCaseFieldName = getitemsRejectedByCaseFieldName(allItemsByCaseId.get(csId));
                       
                        DealDeskOperationApproveSameOpportunity DealDeskOperationApproveSameOpportunity = new DealDeskOperationApproveSameOpportunity();
                        lLstAssertDataToUpdt.addAll(DealDeskOperationApproveSameOpportunity.approveAtTheSameOpportunity(cs, assertDatas, itemsRejectedByCaseFieldName ));
                    }    
                }  

            if(lLstAssertDataToUpdt.size()>0){
                Database.upsert( lLstAssertDataToUpdt );
            }
        }
      }   
    }



    private  void getApprovalItemsByCase(List<Approval_Item__c> newItems){
        for(Approval_Item__c newItem: newItems){
            caseIds.add(newItem.case__c);
        }

        List<Approval_Item__c> allApprovalItems = [SELECT Id, Name, Comments__c, Approval_Status__c, Value__c, Case__c, Case__r.Id, Case__r.QuoteId__c,Case__r.IsClosed,
                                                        Case__r.Cancellation_Date__c, Case__r.Status, Case__r.OpportunityId__c , Case__r.Recordtype.DeveloperName,
                                                        Case__r.OpportunityId__r.Id, Case__r.OpportunityId__r.AccountId
                                                  FROM Approval_Item__c 
                                                  WHERE case__c IN :caseIds  
                                                  ];

        for(Approval_Item__c item : allApprovalItems){
            if(this.allItemsByCaseId.get(item.case__c) != null ){
                List<Approval_Item__c> items = this.allItemsByCaseId.get(item.case__c);
                items.add(item);
                this.allItemsByCaseId.put(item.case__c, items);
            }else{
                List<Approval_Item__c> items = new List<Approval_Item__c>{item};
                this.allItemsByCaseId.put(item.case__c, items);
            }
        }

    }
 

    private void evaluateCasesToApproveOrReject(){

        for(List<Approval_Item__c> items : this.allItemsByCaseId.values()){
            Integer itemsRejected = 0;  
            Integer itemsOpen = 0;  
            Integer allItems = items.size();  
            String dealEvaluation = '';
            for( integer i = 0 ; i < items.size(); i++){
                List<Approval_Item__c> itemsToApprove = new  List<Approval_Item__c>();
                Boolean isApproved = false;
                Approval_Item__c item = items.get(i);
                
                if( item.Approval_Status__c == 'Open'){
                    itemsOpen = itemsOpen + 1;
                }else if(item.Approval_Status__c == 'Rejected'){
                    itemsRejected = itemsRejected + 1;
                }else if(item.Approval_Status__c == 'Approved'){
                    isApproved = true;
                    this.allOppsToApprove.add( item.Case__r.OpportunityId__r.Id );
                    itemsToApprove.add(item);
                    this.itemsApprovedByCase.put(item.Case__r.Id, itemsToApprove);
                }

                dealEvaluation = dealEvaluation +  (i+1) + '. ' + item.Name + ' ' 
                                +  (item.Value__c != null? ', ' + item.Value__c:'' )
                                +  ': ' + item.Approval_Status__c + ' ' +  item.Comments__c +' ' ;
               
                FastTrackDealDeskHandler.DealDeskConditions condition = new FastTrackDealDeskHandler.DealDeskConditions(item.Name, isApproved);

                if(lLstApprovalConditionsByQuote.get(item.Case__r.QuoteId__c) == null){
                    List<FastTrackDealDeskHandler.DealDeskConditions> conditions = new List<FastTrackDealDeskHandler.DealDeskConditions>{condition};
                    lLstApprovalConditionsByQuote.put(item.Case__r.QuoteId__c , conditions);
                }else{
                    List<FastTrackDealDeskHandler.DealDeskConditions> conditions = lLstApprovalConditionsByQuote.get(item.Case__r.QuoteId__c);
                    conditions.add(condition);
                    lLstApprovalConditionsByQuote.put(item.Case__r.QuoteId__c , conditions);
                }
            }
            if(itemsOpen == 0){
                Boolean isApproved;
               if( itemsRejected != 0 ){
                    isApproved = false;
               } else{
                    isApproved = true;
                }

                Case cs = new Case( 
                    Id =  items.get(0).Case__c ,
                    Status = isApproved ? 'Approved' : 'Rejected' ,
                    Deal_Desk_Evaluation__c = dealEvaluation
                );
                this.casesToApproveOrRejectById.put(cs.Id, cs);

                if(items.get(0).Case__r.Recordtype.DeveloperName == 'Deal_Desk_Operational' ) {
                    
                    Opportunity opp = new Opportunity(
                        Id = items.get(0).Case__r.OpportunityId__c,
                        Specialist_Approved__c = isApproved
                    ); 
                    this.oppsToApproveOrReject.add(opp);

                    if(isApproved || allItems != itemsRejected ){
                        operationalCasesApprovedByIds.put(items.get(0).Case__r.Id,items.get(0).Case__r );
                    }

                }else if(items.get(0).Case__r.Recordtype.DeveloperName == 'Deal_Desk_Approval'){
                    Quote opp = new Quote(
                        Id = items.get(0).Case__r.QuoteId__c,
                        Deal_Desk_Approved__c = isApproved,
                        Skip_Enablers_Flow__c = isApproved,
                        Deal_Desk_Approved_Conditions__c = JSON.serialize( lLstApprovalConditionsByQuote.get(items.get(0).Case__r.QuoteId__c))
                    ); 
                    this.proposalsToApproveOrReject.add(opp);
                }
                
            }
        }
    }

    private void getOppMainInfo(){
        List<Quote> allProposals = new List<Quote>();
        this.oppsById = new Map<Id, Opportunity> (
                        [SELECT Id, CloseDate, RecordType.DeveloperName ,AccountId, Account.Proposal_End_Date__c, Achieved_Steps_Towards_Success_Quantity__c,CreatedDate,
                        (SELECT Id, Start_Date__c, Autonomous_marketplace_contract__c, ExpirationDate, Free_Trial_Days__c,Proposal_Start_Date__c FROM Quotes)
                        FROM Opportunity WHERE Id IN : this.allOppsToApprove]);    
                        
        for(Opportunity opp : oppsById.values()){
            allProposals.add(opp.quotes.get(0));
        }

        List<QuoteLineItem> lineItems = new List<QuoteLineItem> ([SELECT  Id,Approval_Status__c,Fee_Contract_Type__c,Fee_Type__c,Quote.OpportunityId,UnitPrice,
            (SELECT Id, Frequency__c FROM Payments__r ),
             ( SELECT Id, Payment_Method__c FROM Eligibilities__r ), (SELECT Id, Approval_Status__c, Approval_level__c FROM Waivers__r ) FROM QuoteLineItem
            WHERE QuoteId IN : allProposals]);  

        for( QuoteLineItem lineItem : lineItems){
            if(this.mapLineItemsByFeeTypeByOppId.get(lineItem.Quote.OpportunityId) == null){
                Map<String, QuoteLineItem> lineItemByFeeType = new Map<String, QuoteLineItem>();
                lineItemByFeeType.put(lineItem.Fee_Type__c, lineItem);
                this.mapLineItemsByFeeTypeByOppId.put(lineItem.Quote.OpportunityId,lineItemByFeeType);
            }else{
                Map<String, QuoteLineItem> lineItemsByFeeType = this.mapLineItemsByFeeTypeByOppId.get(lineItem.Quote.OpportunityId);
                lineItemsByFeeType.put(lineItem.Fee_Type__c, lineItem);
                this.mapLineItemsByFeeTypeByOppId.put(lineItem.Quote.OpportunityId, lineItemsByFeeType);
            }
        }
    }

   
    private void getAssertDataByOppId(){
        List<Assert_Data__c> assertDatas = new List<Assert_Data__c>(
                                        [SELECT Id, Object_Name__c, Field_Name__c, Fee_Type__c, Old_Value__c, Approved_Value__c, New_Value__c, Pre_Approved_Condition__c, Opportunity__c
                                        FROM Assert_Data__c WHERE Opportunity__c IN: this.allOppsToApprove ]);

        for(Assert_Data__c assert :assertDatas ){
            if(this.assertDataByOppId.get(assert.Opportunity__c) == null){
                List<Assert_Data__c> listAssert = new List<Assert_Data__c>{assert};
                this.assertDataByOppId.put(assert.Opportunity__c, listAssert);
            }else{
                List<Assert_Data__c> listAssert = this.assertDataByOppId.get(assert.Opportunity__c);
                listAssert.add(assert);
                this.assertDataByOppId.put(assert.Opportunity__c, listAssert);
            }
        }
    }

    private Map<Id, Case> getCasesOperational(Set<Id> caseOperationalIds){
        
        String query = 'SELECT ';

        for(Schema.FieldSetMember f : SObjectType.Case.FieldSets.New_Deal_Desk_Operational_Case.getFields()){
            query += f.getFieldPath() + ', ';
        }
        query += 'Id, OpportunityId__c, RecordTypeId, QuoteId__c , isClosed FROM Case WHERE Id IN :caseOperationalIds';
        Map<Id, Case> operationalCases = new  Map<Id, Case> ((List<Case>)Database.query(query));    

        return operationalCases;
    }


    private Map<String, Approval_Item__c> getitemsRejectedByCaseFieldName(List<Approval_Item__c> approvalItems){
       Map<String, Approval_Item__c> itemsRejectedByCaseFieldName = new  Map<String, Approval_Item__c>();
       Map<String, Approval_Items_Case_Mapping__mdt> itemsMdtByApprovalItemName = new Map<String, Approval_Items_Case_Mapping__mdt>();
       for(Approval_Items_Case_Mapping__mdt itemMdt : approvalItemsMappingByNameMdt.values()){
            itemsMdtByApprovalItemName.put(itemMdt.Approval_Item_Name__c, itemMdt);
       }
       
        for(Approval_Item__c item : approvalItems){
            if(item.Approval_Status__c == 'Rejected' && itemsMdtByApprovalItemName.get(item.Name) != null){
                List<String> caseFields = new List<String>();
                if( itemsMdtByApprovalItemName.get(item.Name).Case_Fields_To_Map__c.split(',') != null){
                   caseFields = itemsMdtByApprovalItemName.get(item.Name).Case_Fields_To_Map__c.split(',');
                }else{
                    caseFields.add(itemsMdtByApprovalItemName.get(item.Name).Case_Fields_To_Map__c);
                }

                for(String caseField : caseFields){
                    itemsRejectedByCaseFieldName.put(caseField.toLowerCase(), item);
                }
            }
        }

        return itemsRejectedByCaseFieldName;
    }
}