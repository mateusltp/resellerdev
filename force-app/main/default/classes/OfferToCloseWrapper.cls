/**
 * @author vncferraz
 * 
 * Provide multiple objects Wrapper for OfferQueueProcessor for "Close" type of request
 */
public class OfferToCloseWrapper extends OfferWrapper { 
    
    public OfferToCloseWrapper() {
        super();
    }
    
    override
    public ProcessResult save(ProcessResult result){
         
        Savepoint sp = Database.setSavepoint();

        try{
            saveOpportunity();
            result.status = 'Success';
        }catch(System.DMLException e){

            Database.rollback(sp);
            result.statusDetail = 'Unexpected Error, please contact your Admin: ' + e.getMessage();
        }

        return result;    
    }
}