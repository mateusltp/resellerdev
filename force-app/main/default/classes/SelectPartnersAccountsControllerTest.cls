/**
 * @description       : 
 * @author            : alysson.mota@gympass.com
 * @group             : 
 * @last modified on  : 03-23-2022
 * @last modified by  : alysson.mota@gympass.com
**/
@isTest
public with sharing class SelectPartnersAccountsControllerTest {

    @TestSetup
    static void makeData(){
        Account accParent = getAccountInstance('Parent Account', null);
        insert accParent;
        
        Account accChild = getAccountInstance('Child Account', accParent.Id);
        insert accChild;

        Opportunity opp = getOpportunityInstance(accParent.Id);
        insert opp;

        List<Account_Opportunity_Relationship__c> aorLst = new List<Account_Opportunity_Relationship__c>();
        Account_Opportunity_Relationship__c aorParent   = getAorInstance(accParent.Id, opp.Id);
        Account_Opportunity_Relationship__c aorChild    = getAorInstance(accChild.Id, opp.Id);
        aorLst.add(aorParent);
        aorLst.add(aorChild);
        insert aorLst;

        List<Commercial_Condition__c> commCondLst = new List<Commercial_Condition__c>();
        Commercial_Condition__c parentCapCommCondition = getCapCommConditionInstance();
        Commercial_Condition__c childCapCommCondition  = getCapCommConditionInstance();
        commCondLst.add(parentCapCommCondition);
        commCondLst.add(childCapCommCondition);

        Commercial_Condition__c parentLateCancelCommCondition = getLateCancelCommConditionInstance();
        Commercial_Condition__c childLateCancelCommCondition  = getLateCancelCommConditionInstance();
        commCondLst.add(parentLateCancelCommCondition);
        commCondLst.add(childLateCancelCommCondition);

        Commercial_Condition__c parentNoShowCommCondition = getNoShowCommConditionInstance();
        Commercial_Condition__c childNoShowCommCondition  = getNoShowCommConditionInstance();
        commCondLst.add(parentNoShowCommCondition);
        commCondLst.add(childNoShowCommCondition);
        insert commCondLst;

        Product_Item__c parentProduct = getProductInstance(null);
        insert parentProduct;
        
        Product_Item__c childProduct = getProductInstance(parentProduct.Id);
        insert childProduct;

        List<Product_Assignment__c> paLst = new List<Product_Assignment__c>();
        Product_Assignment__c parentCapPa           = getProdAssignmentInstance(parentCapCommCondition.Id, parentProduct.Id);
        Product_Assignment__c parentLateCancelPa    = getProdAssignmentInstance(parentLateCancelCommCondition.Id, parentProduct.Id);
        Product_Assignment__c parentNoShowPa        = getProdAssignmentInstance(parentNoShowCommCondition.Id, parentProduct.Id);
        Product_Assignment__c childCapPa            = getProdAssignmentInstance(childCapCommCondition.Id, childProduct.Id);
        Product_Assignment__c childLateCancelPa     = getProdAssignmentInstance(childLateCancelCommCondition.Id, childProduct.Id);
        Product_Assignment__c childNoShowPa         = getProdAssignmentInstance(childNoShowCommCondition.Id, childProduct.Id);
        paLst.add(parentCapPa); 
        paLst.add(parentLateCancelPa); 
        paLst.add(parentNoShowPa); 
        paLst.add(childCapPa); 
        paLst.add(childLateCancelPa); 
        paLst.add(childNoShowPa); 
        insert paLst;

        List<Product_Opportunity_Assignment__c> poaLst = new List<Product_Opportunity_Assignment__c>();
        Product_Opportunity_Assignment__c parentCapPoa          = getPoaInstance(parentCapPa.Id, aorParent.Id);
        Product_Opportunity_Assignment__c parentLateCancelPoa   = getPoaInstance(parentLateCancelPa.Id, aorParent.Id);
        Product_Opportunity_Assignment__c parentNoShowPoa       = getPoaInstance(parentNoShowPa.Id, aorParent.Id);
        Product_Opportunity_Assignment__c childCapPoa           = getPoaInstance(childCapPa.Id, aorChild.Id);
        Product_Opportunity_Assignment__c childLateCancelPoa    = getPoaInstance(childLateCancelPa.Id, aorChild.Id);
        Product_Opportunity_Assignment__c childNoShowPoa        = getPoaInstance(childNoShowPa.Id, aorChild.Id);
        poaLst.add(parentCapPoa);
        poaLst.add(parentLateCancelPoa);
        poaLst.add(parentNoShowPoa);
        poaLst.add(childCapPoa);
        poaLst.add(childLateCancelPoa);
        poaLst.add(childNoShowPoa);
        insert poaLst;
    }

    @isTest
    public static void testBuildNetworkForOpportunity() {
        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];

        SelectPartnersAccountsController.buildNetwork(opp.Id, 'Partner_Flow_Account');
    }
    
    @isTest
    public static void testBuildNetworkForProduct() {
        Product_Item__c product = [SELECT Id FROM Product_Item__c LIMIT 1];

        SelectPartnersAccountsController.buildNetwork(product.Id, 'Partner_Flow_Account');
    }

     private static Account getAccountInstance(String name, Id parentId) {
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Account').getRecordTypeId();
        
        Account acc = new Account();
        acc.Name = name;
        acc.RecordTypeId = recordTypeId;
        acc.ParentId = parentId;
         acc.BillingStreet = 'Street A';
         acc.BillingCity = 'City A';
         acc.BillingCountry = 'United States';

        return acc;
    }

    private static Opportunity getOpportunityInstance(Id accountId) {
        Id recordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Opportunity').getRecordTypeId();
        
        Opportunity opp = new Opportunity();
        opp.RecordTypeId = recordTypeId;
        opp.Name = 'My Opp';
        opp.CloseDate = System.now().date();
        opp.AccountId = accountId;
        opp.StageName = 'Qualificação';

        return opp;
    }

    private static Account_Opportunity_Relationship__c getAorInstance(Id accountId, Id oppId) {
        Id recordTypeId = Schema.SObjectType.Account_Opportunity_Relationship__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Opportunity_Member').getRecordTypeId();

        Account_Opportunity_Relationship__c aor = new Account_Opportunity_Relationship__c();
        aor.Account__c = accountId;
        aor.Opportunity__c = oppId;
        aor.RecordTypeId = recordTypeId;

        return aor;
    }

    private static Commercial_Condition__c getCapCommConditionInstance() {
        Id recordTypeId = Schema.SObjectType.Commercial_Condition__c.getRecordTypeInfosByDeveloperName().get('CAP').getRecordTypeId();

        Commercial_Condition__c capCommCondition = new Commercial_Condition__c();
        capCommCondition.RecordTypeId = recordTypeId;

        return capCommCondition;
    }

    private static Commercial_Condition__c getLateCancelCommConditionInstance() {
        Id recordTypeId = Schema.SObjectType.Commercial_Condition__c.getRecordTypeInfosByDeveloperName().get('Late_Cancellation').getRecordTypeId();

        Commercial_Condition__c capCommCondition = new Commercial_Condition__c();
        capCommCondition.RecordTypeId = recordTypeId;

        return capCommCondition;
    }

    private static Commercial_Condition__c getNoShowCommConditionInstance() {
        Id recordTypeId = Schema.SObjectType.Commercial_Condition__c.getRecordTypeInfosByDeveloperName().get('No_Show').getRecordTypeId();

        Commercial_Condition__c capCommCondition = new Commercial_Condition__c();
        capCommCondition.RecordTypeId = recordTypeId;

        return capCommCondition;
    }

    private static Product_Item__c getProductInstance(Id parentProductId) {
        Id recordTypeId = Schema.SObjectType.Product_Item__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product').getRecordTypeId();

        Product_Item__c product = new Product_Item__c();
        product.Name = 'Boxing';
        product.RecordTypeId = recordTypeId;
        product.Parent_Product__c = parentProductId;

        return product;
    }

    private static Product_Assignment__c getProdAssignmentInstance(Id capCommConditionId, Id productId) {
        Id recordTypeId = Schema.SObjectType.Product_Assignment__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product_Assignment').getRecordTypeId();
        
        Product_Assignment__c prodAssignment = new Product_Assignment__c();
        prodAssignment.RecordTypeId = recordTypeId;
        prodAssignment.CommercialConditionId__c = capCommConditionId;
        prodAssignment.ProductId__c = productId;

        return prodAssignment;
    }

    private static Product_Opportunity_Assignment__c getPoaInstance(Id paId, Id aorId) {
        Id recordTypeId = Schema.SObjectType.Product_Opportunity_Assignment__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product_Opportunity_Assignment').getRecordTypeId();
        
        Product_Opportunity_Assignment__c poa = new Product_Opportunity_Assignment__c();
        poa.RecordTypeId = recordTypeId;
        poa.ProductAssignmentId__c = paId;
        poa.OpportunityMemberId__c = aorId;

        return poa;
    }
}