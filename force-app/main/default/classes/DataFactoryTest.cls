@IsTest
public class DataFactoryTest {
    
    @isTest 
    public static void dataFactoryTest(){
        
        Pricebook2 newPricebook = DataFactory.newPricebook();
        Pricebook2 newPricebookByBusinessModel = DataFactory.newPricebookByBusinessModel('Intermediation','Intermediation');
        Pricebook2 newPricebook2 = DataFactory.newPricebook('Intermediation','Intermediation');
        Account newAccount = DataFactory.newAccount();
        Product2 newProduct = DataFactory.newProduct('teste', true, 'BRL');
        Opportunity newOpportunity = DataFactory.newOpportunity(newAccount.id, newPricebookByBusinessModel, 'Indirect_Channel_New_Business');
        Quote newQuote = DataFactory.newQuote(newOpportunity, 'newQuote', 'Client_Sales_New_Business');
        Case newCase = DataFactory.newCase(newOpportunity, newQuote, 'Deal_Desk_Operational');
        PricebookEntry newPricebookEntry  = DataFactory.newPricebookEntry(newPricebook, newProduct, newOpportunity);
        QuoteLineItem newQuoteLineItem = DataFactory.newQuoteLineItem(newQuote,newPricebookEntry);
        QuoteLineItem newQuoteLineItem2 = DataFactory.newQuoteLineItem(newQuote,newPricebookEntry,10);
        Payment__c newPayment = DataFactory.newPayment(newQuoteLineItem);
        Payment__c newPayment2 = DataFactory.newPayment(newQuoteLineItem,'Boleto');
        Waiver__c newWaiver = DataFactory.newWaiver(newPayment);
        //Waiver__c newWaiver2 = DataFactory.newWaiver(newQuoteLineItem,1,1);
        //List<Waiver__c> createWaivers = DataFactory.createWaivers(newQuoteLineItem,2);
        Eligibility__c newEligibility = DataFactory.newEligibility(newPayment);
        Contact newContact = DataFactory.newContact(newAccount,'teste');
        Account newGympassEntity = DataFactory.newGympassEntity('testeGYM');
        APXT_Redlining__Contract_Agreement__c newContractAgreement = DataFactory.newContractAgreement(newOpportunity,newQuote,newAccount);
        Eligibility__c newEligibility2 = DataFactory.newEligibility(newPayment, true);
        APXT_Redlining__Contract_Agreement__c newContractAgreement2 = DataFactory.newContractAgreement(newOpportunity.id,newQuote.id,newAccount.id);
        Case newCase2 = DataFactory.newCase(newOpportunity.id, newQuote.id, 'Deal_Desk_Operational');
        Opportunity newOpportunity2 = DataFactory.newOpportunity(newAccount.id, newPricebookByBusinessModel, 'Indirect_Channel_New_Business',true);
        Assert_Data__c newAssertData = DataFactory.newAssertData(newOpportunity2.id,'Indirect_Channel_New_Business');
        Comission_Price__c newComissionPrice = DataFactory.newComissionPrice(2,2,newProduct.id,'BRL');
        Account newAccountReseller = DataFactory.newAccountReseller();
        List<Approval_Item__c> newApprovalItems = DataFactory.newApprovalItems(newCase);
        List<EventConfiguration__c> newEventConfigurations =  DataFactory.newEventConfigurations(2);
        Offer_Queue__c newOfferQueue = DataFactory.newOfferQueue(newOpportunity2.id, newAccount.id);
		        
    }

}