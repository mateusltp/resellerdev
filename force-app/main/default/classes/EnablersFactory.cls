/**
 * @description       : 
 * @author            : AlyssonMota
 * @group             : 
 * @last modified on  : 02-23-2022
 * @last modified by  : vads@gft.com
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   04-14-2021   Alysson Mota   Initial Version
**/
public with sharing class EnablersFactory {
    private List<Step_Towards_Success_Definition__mdt> StepTowardsSuccessDefinitionList;
    
    private static Id RecTypeOpportunityStepClient = 
        Schema.SObjectType.Step_Towards_Success1__c.getRecordTypeInfosByDeveloperName().get('Opportunity_Step').getRecordTypeId();
    
    private static Id RecTypeAccountStepClient = 
        Schema.SObjectType.Step_Towards_Success1__c.getRecordTypeInfosByDeveloperName().get('Account_Step').getRecordTypeId();

   
    public EnablersFactory() {
        initMetadata();    
    }

    private void initMetadata()    {
        Set<String> stepsCategorySet = new Set<String>{
            'Business Enabler','Enrollment Enablers'
        };
        
        stepTowardsSuccessDefinitionList = [
            SELECT DeveloperName, MasterLabel, Category__c, Description__c, Field_API_Name__c, Field_Expected_Value__c, How_are_we_going_to_track_it__c, Object_API_Name__c, Step__c, Manual_changes_allowed__c, Standard_Achieved_Value_for_SMB__c
            FROM Step_Towards_Success_Definition__mdt
            WHERE Category__c IN :stepsCategorySet
            ORDER BY Step__c ASC
        ];        
    }

    public List<Step_Towards_Success1__c> createEnablersForOpportunityClients(Opportunity opp) {
        List<Step_Towards_Success1__c> enablersToInsert = new List<Step_Towards_Success1__c>();

        for (Step_Towards_Success_Definition__mdt definition : StepTowardsSuccessDefinitionList) {
            Step_Towards_Success1__c enabler = new Step_Towards_Success1__c();
            enabler.Master_Account__c = opp.AccountId;
            enabler.Related_Opportunity__c = opp.Id;
            enabler.Achieved__c = getAchievedValueBasedOnOppRecType(opp, definition);
            enabler.Step_Number__c = definition.Step__c;
            enabler.Name = definition.MasterLabel;
            enabler.RecordTypeId = RecTypeOpportunityStepClient;

            enablersToInsert.add(enabler);
        }

        return enablersToInsert;
    }

    public List<Step_Towards_Success1__c> createEnablersForAccountIdClients(Id accId) {
        List<Step_Towards_Success1__c> enablersToInsert = new List<Step_Towards_Success1__c>();

        for (Step_Towards_Success_Definition__mdt definition : StepTowardsSuccessDefinitionList) {
            Step_Towards_Success1__c enabler = new Step_Towards_Success1__c();
            enabler.Master_Account__c = accId;
            enabler.Related_Account__c = accId;
            enabler.Achieved__c = 'No';
            enabler.Step_Number__c = definition.Step__c;
            enabler.Name = definition.MasterLabel;
            enabler.RecordTypeId = RecTypeAccountStepClient;
            enabler.Manual_changes_allowed__c = definition.Manual_changes_allowed__c;

            enablersToInsert.add(enabler);
        }

        return enablersToInsert;
    }

    private String getAchievedValueBasedOnOppRecType(Opportunity opp, Step_Towards_Success_Definition__mdt definition) {
        if (opp.RecordType.DeveloperName == 'SMB_New_Business' || opp.RecordType.DeveloperName == 'SMB_Success_Renegotiation') {
            return definition.Standard_Achieved_Value_for_SMB__c;
        } 
        else if(opp.RecordType.DeveloperName == 'Indirect_Channel_New_Business'){
            return 'Yes';
        }
        else {
            return 'No';
        }              
    }
}