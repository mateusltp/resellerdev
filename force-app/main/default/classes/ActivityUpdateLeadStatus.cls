/**
 * @description       : 
 * @author            : roei@gft.com
 * @group             : 
 * @last modified on  : 02-23-2021
 * @last modified by  : roei@gft.com
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   02-23-2021   roei@gft.com   Initial Version
**/
public without sharing class ActivityUpdateLeadStatus {
    private Id gLeadDirectChannelRTId = 
        Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Client').getRecordTypeId(); 

    public void getLeadTasks(){
        List< Id > lLstLeadId = new List< Id >();
        
        for( Task iTask : (List< Task >)Trigger.new ){
            Boolean lIsLeadTask = String.isNotBlank( iTask.WhoId ) && iTask.WhoId.getSObjectType() == Schema.Lead.SObjectType;

            if( lIsLeadTask && iTask.Status == 'Concluído' && 
                ( Trigger.isInsert || ((Task)Trigger.oldMap.get( iTask.Id )).Status != iTask.Status ) ){
                lLstLeadId.add( iTask.WhoId );
            }
        }

        updateLeadStatus( lLstLeadId );
    }

    public void getLeadEvents(){
        List< Id > lLstLeadId = new List< Id >();
        
        for( Event iEvent : (List< Event >)Trigger.new ){
            if( String.isNotBlank( iEvent.WhoId ) && iEvent.WhoId.getSObjectType() == Schema.Lead.SObjectType ){
                lLstLeadId.add( iEvent.WhoId );
            }
        }

        updateLeadStatus( lLstLeadId );
    }

    private void updateLeadStatus( List< Id > aLstLeadId ){
        if( aLstLeadId.isEmpty() ){ return; }

        List< Lead > lLstLeadToChangeStatus = 
            [ SELECT Id FROM Lead WHERE Id =: aLstLeadId AND RecordTypeId =: gLeadDirectChannelRTId AND Status = 'SQL' ];

        if( lLstLeadToChangeStatus.isEmpty() ){ return; }

        for( Lead iLead : lLstLeadToChangeStatus ){
            iLead.Status = 'In Progress';
        }

        Database.SaveResult[] lLstResultLead = Database.update( lLstLeadToChangeStatus );

        for( Database.SaveResult iSaveResult : lLstResultLead ){
            if ( !iSaveResult.isSuccess() ) { for( Database.Error iError : iSaveResult.getErrors() ) {
                    System.debug( 'The following error has occurred.' );                    
                    System.debug( iError.getStatusCode() + ': ' + iError.getMessage() );
                    System.debug( 'Lead fields that affected this error: ' + iError.getFields() );
                }
            }
        }
    }
}