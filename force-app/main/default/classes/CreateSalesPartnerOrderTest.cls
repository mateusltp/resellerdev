/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 05-02-2022
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/

@isTest(seeAllData=false)
public class CreateSalesPartnerOrderTest {   
       
    @TestSetup
    static void setupData(){
        String recordTypePartnerFlow = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Account').getRecordTypeId();
        Account partnerAcc = PartnerDataFactory.newAccount();
        partnerAcc.RecordTypeId = recordTypePartnerFlow;
        partnerAcc.CRM_ID__c = '1231';
        partnerAcc.Legal_Document_Type__c = 'CNPJ';
        //partnerAcc.Id_Company__c = 'XX.XXX.XXX/XXXX-XX';
        partnerAcc.Legal_Title__c = 'SAA';
        partnerAcc.BillingCountryCode = 'BR';
        partnerAcc.BillingPostalCode = '1223';        
        partnerAcc.UUID__c = new Uuid().getValue();
        INSERT partnerAcc;
             
        String recordTypePartnerContactFlow = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Contact').getRecordTypeId();
        Contact partnerContact = PartnerDataFactory.newContact(partnerAcc.Id);
        partnerContact.RecordTypeId = recordTypePartnerContactFlow;        
        partnerContact.Type_of_Contact__c = 'Decision Maker';
        partnerContact.FirstName = 'Admin';
        partnerContact.UUID__c = new Uuid().getValue();
        INSERT partnerContact;    

        Opportunity aOpp = PartnerDataFactory.newOpportunity( partnerAcc.Id, 'Partner_Flow_Opportunity'); 
        INSERT aOpp;

        Quote proposal = PartnerDataFactory.newQuote( aOpp );
        proposal.RecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Proposal').getRecordTypeId();
        proposal.Signed__c = Date.Today() - 100;
        proposal.End_Date__c = Date.Today();
        proposal.Start_Date__c = Date.Today() - 100;
        INSERT proposal;


        Account_Opportunity_Relationship__c oppMember = PartnerDataFactory.newAcctOppRel(partnerAcc.Id, aOpp.Id);
        oppMember.RecordTypeId = Schema.SObjectType.Account_Opportunity_Relationship__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Opportunity_Member').getRecordTypeId();
        INSERT oppMember;

        Product_Item__c aProd = PartnerDataFactory.newProduct( aOpp );
        aProd.Type__c = 'Live';
        aProd.Net_Transfer_Price__c = 120;
        aProd.Reference_Price_Value_Unlimited__c=200;
        aProd.RecordTypeId = Schema.SObjectType.Product_Item__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product').getRecordTypeId();
        aProd.Opportunity__c = null;       
        INSERT aProd;

        Commercial_Condition__c aComm = new Commercial_Condition__c();
        aComm.RecordTypeId = Schema.SObjectType.Commercial_Condition__c.getRecordTypeInfosByDeveloperName().get('CAP').getRecordTypeId();
        //aComm.Name = 'CAP';
        aComm.Amount__c = 80;
        INSERT aComm;

        Product_Assignment__c prodAssign = new Product_Assignment__c();
        prodAssign.RecordTypeId = Schema.SObjectType.Product_Assignment__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product_Assignment').getRecordTypeId();
        prodAssign.ProductId__c = aProd.Id;
        prodAssign.CommercialConditionId__c = aComm.Id;
        INSERT prodAssign;


        Product_Opportunity_Assignment__c prodOppAssign = new Product_Opportunity_Assignment__c();
        prodOppAssign.RecordTypeId = Schema.SObjectType.Product_Opportunity_Assignment__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product_Opportunity_Assignment').getRecordTypeId();
        prodOppAssign.OpportunityMemberId__c = oppMember.Id;
        prodOppAssign.ProductAssignmentId__c = prodAssign.Id;
        INSERT prodOppAssign;

    }


    @isTest 
    static void newPartnerSalesOrder_Test() {
        Opportunity opp = [SELECT ID FROM Opportunity LIMIT 1];     
        System.debug(' OPP MEMBER >>>> ' + [SELECT ID FROM Account_Opportunity_Relationship__c ]);
        Test.startTest();

            opp.StageName = 'Lançado/Ganho';
            update   opp;
               
        Test.StopTest();

        List<Order> lstOrder = [SELECT ID FROM ORDER WHERE Type = 'Partner'];
        System.assertEquals(1, lstOrder.size());   
    }
}