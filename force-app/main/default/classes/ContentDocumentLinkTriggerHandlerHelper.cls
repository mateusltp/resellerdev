/**
 * @description       : 
 * @author            : ext.gft.marcus.silva@gympass.com
 * @group             : 
 * @last modified on  : 03-17-2022
 * @last modified by  : ext.gft.marcus.silva@gympass.com
**/
public class ContentDocumentLinkTriggerHandlerHelper {
    public static void cloneContractAgreementFileFromContentDocumentLinkTrigger(List<ContentDocumentLink> cdlList){
        
        Map<Id, ContentDocumentLink> mapContractAndContentDocLink = new Map<Id, ContentDocumentLink>();
        
        for (ContentDocumentLink contentDocLink : cdlList ){
            Schema.SObjectType sobjectType = contentDocLink.LinkedEntityId.getSObjectType();
            if (sobjectType.getDescribe().getName() == 'APXT_Redlining__Contract_Agreement__c' ){                    
                mapContractAndContentDocLink.put(contentDocLink.LinkedEntityId, contentDocLink);
            }
        }
        //Send notification when a new file has been attached to the contract agreement
        LegalSendEmailNewCaseFile.sendNotification(mapContractAndContentDocLink.keyset()); 
        
        
        List<Document__c> listDocument = [SELECT Id, ContractAgreement__c FROM Document__c WHERE ContractAgreement__c IN : mapContractAndContentDocLink.keyset()];
        
        List<ContentDocumentLink> conClone = new List<ContentDocumentLink>();
        if(mapContractAndContentDocLink.values().size()> 0 ){
            for (Id linkedEntityId:  mapContractAndContentDocLink.keyset()){
                for (Document__c doc : listDocument ){
                    if (doc.ContractAgreement__c == linkedEntityId){
                        ContentDocumentLink newclnk = mapContractAndContentDocLink.get(linkedEntityId).clone();
                        newclnk.LinkedEntityId = doc.Id;
                        newclnk.ShareType = 'V';
                        conClone.add(newclnk);
                    }
                }
            }
        }
        
        if (!conClone.isEmpty()){
            try{
               upsert conClone;
            }
            catch(exception e){
                if (Test.isRunningTest()){
                    System.debug(e);
                }
            }
        }
    }
    
}