/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 05-31-2022
 * @last modified by  : alysson.mota@gympass.com
**/
public with sharing class PartnerLeadsConversion {
   
    private static final PS_Constants constants = PS_Constants.getInstance();
    private static final String className = PartnerLeadsConversion.class.getName();
    
    public void updateReferralAccountInLeads(){
        Id rtIdOwnerReferral = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get(constants.LEAD_RT_OWNER_REFERRAL).getRecordTypeId();
        Map<Id, Lead> leadIdMap = (Map<Id,Lead>)trigger.newMap;
        Map<Id, Lead> leadIdOldMap = (Map<Id,Lead>)trigger.oldMap;   
        for (Lead l : leadIdMap.values()){
            if(l.recordTypeId == rtIdOwnerReferral && leadIdOldMap.get(l.id).IsConverted != leadIdMap.get(l.id).IsConverted  && l.IsConverted ) { 
              l.Referral_Account__c = l.ConvertedAccountId;
            }
        } 
    }

    /*standard lead conversion - Lead fields map uses the billing address, so we need to copy it to shipping address to meet business requirement and pass validation rule as well*/
    public void updateAccountAddressFromBillingToShipping() {
        Id rtOldFlowAccount  =  Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(constants.ACCOUNT_RT_GYM_PARTNER).getRecordTypeId();
        List<Account> lstNew = trigger.new;
        
        for (Account iAccount : lstNew ){
            if (iAccount.recordTypeId == rtOldFlowAccount && (String.isBlank(iAccount.ShippingCountry) && String.isBlank(iAccount.ShippingStreet) && String.isBlank(iAccount.ShippingCity))) {
                iAccount.ShippingCountryCode = iAccount.BillingCountryCode;
                iAccount.ShippingStateCode = iAccount.BillingStateCode;
                iAccount.ShippingStreet = iAccount.BillingStreet;
                iAccount.ShippingCity = iAccount.BillingCity;
            }
        }
    }

    public without sharing class PartnerLeadsConversionWithoutSharing {

        public void updateDuplicatedLeads() {
            Id rtIdOwnerReferral = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get(constants.LEAD_RT_OWNER_REFERRAL).getRecordTypeId();
            Map<Id, Lead> leadIdMap = (Map<Id,Lead>)trigger.newMap;
            Map<Id, Lead> leadIdOldMap = (Map<Id,Lead>)trigger.oldMap;

            List<Lead> dupListToUpdate = new List<Lead>();
            List<Account> accLst = new List<Account>();
            for (Lead l : leadIdMap.values()){
                if(l.recordTypeId == rtIdOwnerReferral && leadIdOldMap.get(l.id).IsConverted != leadIdMap.get(l.id).IsConverted  && l.IsConverted ) {
                    Set<Id> duplicatedIds = DuplicateHandler.foundLeadDuplicateLeads(leadIdOldMap.get(l.id));
                    accLst.add( new Account(Id = l.ConvertedAccountId, Referral_Score__c = l.Number_of_Referrals__c ));
                    for(Id iLeadId : duplicatedIds){
                        Lead aLeadDup = new Lead(id = IleadId, Referral_Account__c = l.ConvertedAccountId,
                                Status = constants.LEAD_STATUS_CONVERTED,
                                Types_of_ownership__c = l.Types_of_ownership__c,
                                Type_of_contact__c = l.Type_of_contact__c,
                                Is_Duplicated_Lead__c = true);
                        dupListToUpdate.add(aLeadDup);
                    }
                }
            }
            if(!dupListToUpdate.isEmpty()){
                update dupListToUpdate;
            }

            if(!accLst.isEmpty()){
                update accLst;
            }
        }
    }
}