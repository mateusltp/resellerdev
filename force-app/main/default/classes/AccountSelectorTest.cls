/**
 * @description       : 
 * @author            : gft.jorge.stevaux@ext.gympass.com
 * @group             : 
 * @last modified on  : 08-12-2021
 * @last modified by  : gft.jorge.stevaux@ext.gympass.com
**/

@isTest(seeAllData=false)

private class AccountSelectorTest {

    @TestSetup
    static void createData(){
            
        Account lAcc = PartnerDataFactory.newAccount();  
        lAcc.Name = 'Parent Acct';   
        lAcc.Send_To_Tagus__c = true;   
        lAcc.UUID__c = '123456';      
        Database.insert( lAcc );

        Account lChildAcct = PartnerDataFactory.newChildAccount(lAcc.Id);
        lChildAcct.Name = 'Child Acct';
        Database.insert( lChildAcct );

        Contact lContact = PartnerDataFactory.newContact(lAcc.Id);
        Database.insert(lContact);

        Bank_Account__c lBankAcct = PartnerDataFactory.newBankAcct();
        Database.insert(lBankAcct);

        Acount_Bank_Account_Relationship__c lBankAcctRel = PartnerDataFactory.newBankAcctRel(lAcc.Id, lBankAcct);
        Database.insert(lBankAcctRel);
                                  
        Opportunity lOpp = PartnerDataFactory.newOpportunity( lAcc.Id, 'Partner_Wishlist_Renegotiation' );     
        Database.insert( lOpp );

    }

    @isTest
     static void getSObjectFieldList_Test(){

        List<Schema.SObjectField> expectedReturn = new List<Schema.SObjectField> {
			Account.Id,
			Account.Name,
            Account.ParentID
		};

        Test.startTest();
        System.assertEquals(expectedReturn, new AccountSelector().getSObjectFieldList());
        Test.stopTest();
    }

    @isTest 
     static void getSObjectType_Test(){

        Test.startTest();
        System.assertEquals(Account.sObjectType, new AccountSelector().getSObjectType());
        Test.stopTest();
    }

    @isTest 
     static void selectById_Test(){

        List<Account> acctLst = [SELECT Id, Name FROM Account LIMIT 1];
        Set<Id> acctIds = (new Map<Id, Account>(acctLst)).keySet();

        Test.startTest();
        system.assertEquals(acctLst.get(0).id, new AccountSelector().selectById(acctIds).get(0).id);
        Test.stopTest();
    }

    @isTest 
     static void selectByIdForPartners_Test(){

        List<Account> acctLst = [SELECT Id, Types_of_ownership__c, 
                                Partner_Level__c, ShippingState, 
                                ShippingCountry, ShippingCountryCode 
                                FROM Account LIMIT 1];

        Set<Id> acctIds = (new Map<Id, Account>(acctLst)).keySet();

        Test.startTest();
        system.assertEquals(acctLst.get(0).id, new AccountSelector().selectByIdForPartners(acctIds).get(0).id);
        Test.stopTest();
    }

    @isTest 
     static void selectAccountToClone_Test(){

        List<Account> acctLst = [SELECT Id, Name FROM Account LIMIT 1];
        Set<Id> acctIds = (new Map<Id, Account>(acctLst)).keySet();
        Map<Id, Account> acctIdMap = new Map<Id, Account>(acctLst);

        Test.startTest();
        System.assertEquals(acctIdMap.keySet(), new AccountSelector().selectAccountToClone(acctIds).keySet());
        Test.stopTest();
    }

    @isTest 
     static void selectChildAccountTree_Test(){

        List<Account> parentAcctList = [SELECT Id, Name, ParentId, Types_of_ownership__c,
                                        Partner_Level__c, ShippingState, 
                                        ShippingCountry, ShippingCountryCode  
                                        FROM Account WHERE Name = 'Parent Acct' LIMIT 1];

        Set<Id> parentIds = (new Map<Id, Account>(parentAcctList)).keySet();

        List<Account> childAcctLst = [SELECT Id, Name, ParentId, Types_of_ownership__c,
                                        Partner_Level__c, ShippingState, 
                                        ShippingCountry, ShippingCountryCode  
                                        FROM Account WHERE ParentId IN: parentIds LIMIT 1];

        Map<Id, Account> mapIdAcct = new Map<Id, Account>(childAcctLst);

        Set<Id> gChildAccountTree = new Set<Id>();
        Set<Id> lChildreenIds = new Set<ID>(mapIdAcct.keySet());
        if(lChildreenIds.size()>0) {
            gChildAccountTree.addAll(lChildreenIds);
        }
        
        Test.startTest();
        System.assertEquals(gChildAccountTree, new AccountSelector().selectChildAccountTree(parentIds));
        Test.stopTest();
    }


    @isTest 
     static void selectByParentId_Test(){

        List<Account> acctList = [SELECT Id, Name, Types_of_ownership__c,
                                        Partner_Level__c, ShippingState, 
                                        ShippingCountry, ShippingCountryCode  
                                        FROM Account WHERE Name = 'Child Acct' LIMIT 1];
        
        Set<Id> parentIds = (new Map<Id, Account>(acctList)).keySet();

        List<Account> parentLst = [SELECT Id, Name, Types_of_ownership__c,
                                        Partner_Level__c, ShippingState, 
                                        ShippingCountry, ShippingCountryCode  
                                        FROM Account WHERE ParentId IN: parentIds LIMIT 1];

        Map<Id, Account> mapIdAcct = new Map<Id, Account>(parentLst);

        Test.startTest();
        System.assertEquals(mapIdAcct, new AccountSelector().selectByParentId(parentIds));
        Test.stopTest();
    }

    @isTest
    static void selectAccountMapWithFieldsToTagusParse_Test(){

        List<Account> aAccountLst = [SELECT Id, Name FROM Account LIMIT 1];
        List<Account> acctLst = [SELECT Id, Name, UUID__c, CurrencyIsoCode,
                                ShippingCountryCode, Legal_Document_Type__c, Id_Company__c
                                FROM Account WHERE Id IN : aAccountLst];
        
        Map<Id, Account> mapIdAcct = new Map<Id, Account>(acctLst);

        System.assertEquals(mapIdAcct.keySet(), new AccountSelector().selectAccountMapWithFieldsToTagusParse(acctLst).keySet());
    }

    @isTest
    static void selectToTagus_Test(){

        List<Account> acctLst = [SELECT Id, Name, Send_To_Tagus__c, UUID__c FROM Account LIMIT 1];
        Set<Id> aAccountIds = (new Map<Id, Account>(acctLst)).keySet();

        Test.startTest();
        system.assertEquals(acctLst.get(0).id, new AccountSelector().selectToTagus(aAccountIds).get(0).id);
        Test.stopTest();
    }
     
}