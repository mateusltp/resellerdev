/**
* @author vinicius.ferraz
* @description Provide mockable repository data layer for Quote domain
*/
public without sharing virtual class QuoteRepository {
    public QuoteRepository() {
        
    }

    /**
     * throwable System.QueryException
     */
    public virtual Quote byId(String recordId){
        return [select Id,OpportunityId from Quote where Id = :recordId];
    }

    /**
     * throwable System.QueryException
     */
    public virtual Quote lastForOpportunity(String opportunityId){
        return [SELECT Id
                ,Family_Member_Payment_Method__c
                ,Custom_Integrations_Required__c
                ,BillingStreet
                ,BillingCity
                ,BillingState
                ,BillingCountry
                ,BillingPostalCode
                ,Company_Tax_Reference__c
                ,Eligible_Employee_Database_deadline__c
                ,Access_Fee_Adjustment_Date__c
                ,Unique_Identifier__c
                ,autonomous_marketplace_contract__c
                ,TotalPrice
                ,Discount_Approval_Level__c
                ,Total_Number_of_Employees__c
                ,Setup_Discount_Approval_Needed__c
                ,Setup_Discount_Approved__c
                ,Enterprise_Discount_Approval_Needed__c
                ,Account_Tax_Reference__c
                ,Enterprise_Subscription_Approved__c
                ,Deal_Desk_Approval_Needed__c
                ,Contract_Communications__c
                ,Lenght_of_the_subsidy_period_months__c
                ,Contact_Permission__c
                ,Why_should_we_accept_this_subsidy__c
                ,Total_amount_subsidized__c
                ,Subsidy_basis_eligible_or_enrolled__c
                ,Will_the_MF_be_subsidized__c
                ,Employee_Registration_Method__c
                ,Domain__c
                ,Deal_Desk_Approved__c
                ,Description_Prof_Svcs_custom_request__c
                ,Other_Contract_Conditions__c
                ,Other_Conditions__c
                ,Deal_Desk_Description__c
                ,FastTrackActiveStep__c
                ,Submission_Date__c
                ,Are_Family_Members_Included2__c
                ,Start_Date__c
                ,End_Date__c
                ,ExpirationDate
                ,License_Fee_Waiver__c
                ,CurrencyIsoCode
                ,Name
                ,Pricebook2Id
                ,Pricebook2.Name
                ,Pricebook2.Business_Model__c
                ,Payroll_Debite_Memo_Contact__c
                ,Payroll_Debite_Memo_Contact__r.Email
                ,Payroll_Debite_Memo_Contact__r.Name
                ,Finance__c
                ,Finance__r.Email
                ,Finance__r.Name
                ,Operations__c
                ,Operations__r.Email
                ,Operations__r.Name
                ,OpportunityId
                ,Additional_Considerations__c
                ,ContactId
                ,Contact.Email
                ,Contact.Name
                ,Gympass_Relationship_E_mail__c
                ,Employee_Corporate_Email__c
                ,Employee_Company_ID__c
                ,Signed__c
                ,Employee_National_ID__c
                ,Employee_National_ID_if_applicable__c
                ,Employee_Name__c
                ,Rationale__c
                ,PR_in_contract__c
                ,Exclusivity_clause__c
                ,HR_Communication__c
                ,What_does_success_looks_like_Joint_BP__c
                ,New_Hires__c
                ,Expansion_100_of_the_eligible__c
                ,ES_Revenue__c
                ,ER_Revenue__c
                ,AES_Revenue__c
                ,AER_Revenue__c
                ,Has_Relevant_Changes_for_Offer_Approval__c
                ,Has_Relevant_Changes_for_Offer_Deal_Desk__c
                ,Free_Trial_Days__c
                ,Notify_Enablers_For_Deal_Desk__c
                ,Corporate_E_mail__c
                ,Name_of_the_Benefit_Admin__c
                ,Will_this_company_have_the_Free_Product__c
                ,identification_number__c
                ,Copay__c
                ,Opportunity.Account.BillingCountryCode
                ,Enablers_Approval_Needed__c
                ,Enablers_Approved__c
                ,Enablers_out_of_standard_condition__c
                ,Are_Setup_Fee_Included__c
                ,Deal_Desk_Approved_Conditions__c
                ,Is_Wellz_being_offered_to_this_company__c
                ,Percent_of_comission__c
                ,Comission_Percent__c
                ,Total_comission__c
                ,Approval_Level__c
                ,User_Discount_Approval_Level__c
                ,Start_Billing_Date__c
                ,Deal_Desk_Comments__c
                ,Waiver_Comments__c
                ,Waiver_approved__c
                FROM Quote 
                WHERE OpportunityId = :opportunityId 
                ORDER BY CreatedDate DESC 
                LIMIT 1];
    }

    /**
     * throwable System.QueryException
     */
    public virtual Map<Id, Quote> getLastForOpportunitiesAsOppIdToQuoteMap(List<Id> opportunityIds) {
        Map<Id, Quote> oppIdToQuoteMap = new Map<Id, Quote>();
        List<Quote> quotes = forOpportunities(opportunityIds);

        for (Quote quote : quotes) {
            if (!oppIdToQuoteMap.containsKey(quote.OpportunityId)) {
                oppIdToQuoteMap.put(quote.OpportunityId, quote);
            } else {
                continue;
            }
        }

        return oppIdToQuoteMap;
    }

    /**
     * throwable System.QueryException
     */
    public virtual List<Quote> forOpportunities(List<Id> opportunityIds){
        return [select Id,Family_Member_Payment_Method__c,Custom_Integrations_Required__c,BillingStreet, BillingCity, BillingState, BillingCountry, BillingPostalCode,Company_Tax_Reference__c,Eligible_Employee_Database_deadline__c,Access_Fee_Adjustment_Date__c,Unique_Identifier__c,autonomous_marketplace_contract__c,TotalPrice,Discount_Approval_Level__c,Total_Number_of_Employees__c,Setup_Discount_Approval_Needed__c,Setup_Discount_Approved__c,Enterprise_Discount_Approval_Needed__c,Account_Tax_Reference__c,
        Enterprise_Subscription_Approved__c, Deal_Desk_Approval_Needed__c,Contract_Communications__c,Lenght_of_the_subsidy_period_months__c,Contact_Permission__c,Why_should_we_accept_this_subsidy__c,Total_amount_subsidized__c,Subsidy_basis_eligible_or_enrolled__c,Will_the_MF_be_subsidized__c,Domain__c,Employee_Registration_Method__c,Deal_Desk_Approved__c,Description_Prof_Svcs_custom_request__c,Other_Contract_Conditions__c,Other_Conditions__c,Deal_Desk_Description__c,FastTrackActiveStep__c,Submission_Date__c,Are_Family_Members_Included2__c,Start_Date__c,
        End_Date__c,ExpirationDate,License_Fee_Waiver__c,Will_this_company_have_the_Free_Product__c,CurrencyIsoCode,Name,Pricebook2Id,Pricebook2.Name,Pricebook2.Business_Model__c,Payroll_Debite_Memo_Contact__c,Payroll_Debite_Memo_Contact__r.Name,Finance__c,Finance__r.Name,Operations__c,Operations__r.Name,
        OpportunityId,Additional_Considerations__c,ContactId,Contact.Name,Gympass_Relationship_E_mail__c,Employee_Corporate_Email__c,Employee_Company_ID__c, Administrator__c, Are_Setup_Fee_Included__c,
        Employee_National_ID__c,Employee_Name__c,Rationale__c,PR_in_contract__c,Exclusivity_clause__c,HR_Communication__c,What_does_success_looks_like_Joint_BP__c,Opportunity.RecordType.DeveloperName, Proposal_Start_Date__c,
        New_Hires__c,Expansion_100_of_the_eligible__c,ES_Revenue__c,ER_Revenue__c,AES_Revenue__c,AER_Revenue__c, Has_Relevant_Changes_for_Offer_Approval__c,Has_Relevant_Changes_for_Offer_Deal_Desk__c,Free_Trial_Days__c,Notify_Enablers_For_Deal_Desk__c, Is_Wellz_being_offered_to_this_company__c,Percent_of_comission__c,Comission_Percent__c,Total_comission__c,Start_Billing_Date__c,Deal_Desk_Comments__c,Waiver_Comments__c from Quote where OpportunityId IN :opportunityIds order by CreatedDate desc];
    }

    public virtual List<Quote> getQuoteById(Set<Id> quoteIds){
        return [SELECT Id, Opportunity.Account_Billing_Country__c FROM Quote WHERE Id IN :quoteIds];
    }

      /**
     * throwable System.DMLException
     */
    public virtual Quote add(Quote record){
        Database.insert(record);
        return record;
    }

    /**
     * throwable System.DMLException
     */
    public virtual Quote edit(Quote record){
        record.Skip_Enablers_Flow__c = true;
        Database.update(record);
        return record;
    }

    /**
     * throwable System.DMLException
     */
    public virtual Quote editWithFlow(Quote record){
        record.Skip_Enablers_Flow__c = false;
        Database.upsert(record, Quote.Id, true);
        return record;
    }

    /**
     * throwable System.DMLException
     */
    public virtual Quote addOrEdit(Quote record){
        record.Skip_Enablers_Flow__c = true;
        Database.upsert(record, Quote.Id, true);
        return record;
    }

    /**
     * throwable System.DMLException
     */
    public virtual Quote addOrEditWithFlow(Quote record){
        record.Skip_Enablers_Flow__c = false;
        Database.upsert(record, Quote.Id, true);
        return record;
    }
 

}