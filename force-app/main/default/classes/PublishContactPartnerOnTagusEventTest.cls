/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 06-30-2022
 * @last modified by  : alysson.mota@gympass.com
**/
@IsTest
public class PublishContactPartnerOnTagusEventTest {
    
  @IsTest
  public static void execute() {
    Test.setMock(HttpCalloutMock.class, new HttpResponseMock());

    Account account = PartnerDataFactory.newAccount();
    account.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Account').getRecordTypeId();
    account.UUID__c = new Uuid().getValue();
    account.BillingCity = 'Itu';
    account.BillingCountry = 'Brazil';
    account.BillingCountryCode = 'BR';
    account.BillingState = 'São Paulo';
    account.BillingStreet = 'Praça Conselho do Povo';
    account.Legal_Registration__c = null;
    account.Legal_Title__c = null;
    account.Email__c = null;
    account.Legal_Document_Type__c = null;
    account.Send_To_Tagus__c = true;
    account.Phone = null;
    insert account;

    Contact contact = PartnerDataFactory.newContact(account.Id);
    contact.Email = 'test@gmail.com';
    contact.Role__c = null;
    contact.FirstName = 'contact';
    contact.MailingCity = 'Itu';
    contact.MailingCountry = 'Brazil';
    contact.MailingState = 'São Paulo';
    contact.MailingStreet = 'Praça Conselho do Povo';
    contact.MailingPostalCode = '13123123';
    contact.Phone = null;
    contact.Status_do_contato__c = 'Ativo';
    contact.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Contact').getRecordTypeId();
    insert contact;    

    System.assertEquals(
      1,
      [SELECT Id FROM Queue__c].size(),
      'Wrong number of events created'
    );

    contact.LastName = 'newLastName';
    update contact;

    System.assertEquals(
      2,
      [SELECT Id FROM Queue__c].size(),
      'Wrong number of events created'
    );
  }

  private class HttpResponseMock implements HttpCalloutMock {
    public HTTPResponse respond(HTTPRequest req) {
      HttpResponse respinse = new HttpResponse();
      respinse.setHeader('Content-Type', 'application/json');
      respinse.setStatusCode(200);
      return respinse;
    }
  }
}