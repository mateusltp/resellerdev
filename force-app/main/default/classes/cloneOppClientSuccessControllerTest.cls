/**
 * @description       : 
 * @author            : roei@gft.com
 * @group             : 
 * @last modified on  : 06-11-2021
 * @last modified by  : roei@gft.com
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   05-17-2021   roei@gft.com   Initial Version
**/
@isTest
public with sharing class cloneOppClientSuccessControllerTest {
    @isTest
    public static void shouldReturnRenegotiationRecordtypes(){ 
        List< RecordType > lLstRenegotiationRt = null;
        Test.startTest();
        lLstRenegotiationRt = cloneOppClientSuccessController.getRenegotiationRecordTypes(); 
        Test.stopTest();

        System.assertEquals( lLstRenegotiationRt.size() , 2 ,
            'The Renegotiation Recordtypes havent been returned' );
    }

    @isTest
    public static void shouldReturnOppSubtypeOptions(){ 
        
        Test.startTest();
        String lPicklistOptions = cloneOppClientSuccessController.getPicklistOptions(); 
        Test.stopTest();

        List< cloneOppClientSuccessController.PicklistOption > lSubtypeOptions = ( List< cloneOppClientSuccessController.PicklistOption > )
          JSON.deserialize( lPicklistOptions , List< cloneOppClientSuccessController.PicklistOption >.class );

        System.assert( lSubtypeOptions.size() > 0 ,
            'Couldnt get Opp Sub-type picklist values' );
    }

    @isTest
    public static void shouldCreateNewRenegotiationOpp(){ 
        Account lAcc = DataFactory.newGympassEntity( 'NewAccTest' );
        Database.insert( lAcc );

        Test.startTest();
        String lOppId = cloneOppClientSuccessController.createNewRenegotiationOpp( lAcc.Id , 'Client_Success_Renegotiation' , 'Renegotiation' ); 
        Test.stopTest();
        
        System.assertEquals([ SELECT Id FROM Opportunity WHERE Id =: lOppId ].size() , 1 ,
            'Renegotiation Opp hasnt been created');
    }
}