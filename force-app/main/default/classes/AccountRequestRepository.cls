public without sharing class AccountRequestRepository {

    public static Account_Request__c getById(Id requestId){
        requestId = String.escapeSingleQuotes(requestId);
        return Database.query('SELECT ' + ResellerUtil.getQueryFieldsByFieldSetName('Account_Request__c', 'Engine_Fields') + ',AccountId__r.OwnerId, Partner_Account__r.OwnerId, Queried_Opportunity__r.RecordTypeId FROM Account_Request__c WHERE Id =: requestId');
    }
    
}