public class OfferQueueSchedulable implements Schedulable{
    
    public void execute(SchedulableContext context){
        Database.executeBatch( new OfferQueueBatchable() ,1 );
        Database.executeBatch( new OffersToCloseBatchable() ,1 );
    }
}