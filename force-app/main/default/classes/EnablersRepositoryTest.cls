@IsTest(seeAllData=false)
public class EnablersRepositoryTest {
    
    
    @TestSetup
    static void Setup(){
        
        Pricebook2 lStandardPricebook = DataFactory.newPricebook();
        Database.update(lStandardPricebook);
        
        Pricebook2 pb = DataFactory.newPricebook('Intermediation', 'Intermediation');
        Database.insert(pb);
        
        Pricebook2 pb5 = DataFactory.newPricebook('Reseller 5k', 'Subsidy 5k');
        Database.insert(pb5);
        
        Pricebook2 pb15 = DataFactory.newPricebook('Reseller 15k', 'Subsidy 15k');
        Database.insert(pb15);
        
        Account acc = DataFactory.newAccount();
        Database.insert(acc);
        
        Opportunity opp = DataFactory.newOpportunity(acc.id, pb, 'Indirect_Channel_New_Business');
        Database.insert(opp);
        
        Opportunity oppSubsidy = DataFactory.newOpportunity(acc.id, pb15, 'Indirect_Channel_New_Business');
        oppSubsidy.B_M__c = 'Total Subsidy';
        Database.insert(oppSubsidy);
        
        Opportunity oppSubsidyUSD = DataFactory.newOpportunity(acc.id, pb15, 'Indirect_Channel_New_Business');
        oppSubsidyUSD.B_M__c = 'Total Subsidy';
        oppSubsidyUSD.CurrencyIsoCode = 'USD';
        Database.insert(oppSubsidyUSD);
        
        Quote quote = DataFactory.newQuote(opp, 'testeMateus', 'Client_Sales_New_Business');
        Database.insert(quote);
        
        Quote quoteSubsidy = DataFactory.newQuote(oppSubsidy, 'testSubsidy', 'Client_Sales_New_Business');
        Database.insert(quoteSubsidy);
        
        Quote quoteSubsidyUSD = DataFactory.newQuote(oppSubsidyUSD, 'testSubsidy', 'Client_Sales_New_Business');
        Database.insert(quoteSubsidyUSD);
        
        Product2 productBRL = DataFactory.newProduct('teste', false, 'BRL');
        Database.insert(productBRL);
        
        PricebookEntry pbEntry = DataFactory.newPricebookEntry(lStandardPricebook, productBRL, opp);
        Database.insert(pbEntry);
        
        PricebookEntry pbEntryIntermediation = DataFactory.newPricebookEntry(pb, productBRL, opp);
        Database.insert(pbEntryIntermediation);
        
        PricebookEntry pbEntry5 = DataFactory.newPricebookEntry(pb5, productBRL, opp);
        Database.insert(pbEntry5);
        
        PricebookEntry pbEntry15 = DataFactory.newPricebookEntry(pb15, productBRL, opp);
        Database.insert(pbEntry15);
        
        QuoteLineItem accessFee = DataFactory.newQuoteLineItem(quote, pbEntryIntermediation);
        Database.insert(accessFee);
        
        Payment__c payment = DataFactory.newPayment(accessFee);
        Database.insert(payment);
        
        
        /*  */
        
        
        
    }
    
    private static Id RecTypeOpportunityStepClient = 
        Schema.SObjectType.Step_Towards_Success1__c.getRecordTypeInfosByDeveloperName().get('Opportunity_Step').getRecordTypeId();
    
    private static Id RecTypeAccountStepClient = 
        Schema.SObjectType.Step_Towards_Success1__c.getRecordTypeInfosByDeveloperName().get('Account_Step').getRecordTypeId();
    
    
    @isTest
    static void getOpportunityEnablersTest(){
        Opportunity opp = [SELECT ID FROM Opportunity limit 1];
        EnablersRepository enablersRepository = new EnablersRepository();
        List<Step_Towards_Success1__c> listStepTowardsSuccess1 = new List<Step_Towards_Success1__c>();
        try {
            listStepTowardsSuccess1 = EnablersRepository.getOpportunityEnablers(opp);
        } catch(Exception e){
            System.assert(false);
        }
    }
    
    @isTest
    static void getAccountEnablersTest(){
        Account acc = [SELECT ID FROM Account limit 1];
        EnablersRepository enablersRepository = new EnablersRepository();
        List<Step_Towards_Success1__c> listStepTowardsSuccess1 = new List<Step_Towards_Success1__c>();
        try {
            listStepTowardsSuccess1 = EnablersRepository.getAccountEnablers(acc);
        } catch(Exception e){
            System.assert(false);
        }
    }
    
    @isTest
    static void createEnablersForOpportunityClientsTest(){
        Opportunity opp = [SELECT ID,AccountId FROM Opportunity limit 1];
        EnablersRepository enablersRepository = new EnablersRepository();
        List<Step_Towards_Success1__c> listStepTowardsSuccess1 = new List<Step_Towards_Success1__c>();
        try {
            listStepTowardsSuccess1 = enablersRepository.createEnablersForOpportunityClients(opp);
        } catch(Exception e){
            System.assert(false);
        }
    }
    
    @isTest
    static void createEnablersForAccountIdClientsTest(){
        Account acc = [SELECT ID FROM Account limit 1];
        EnablersRepository enablersRepository = new EnablersRepository();
        List<Step_Towards_Success1__c> listStepTowardsSuccess1 = new List<Step_Towards_Success1__c>();
        try {
            listStepTowardsSuccess1 = enablersRepository.createEnablersForAccountIdClients(acc.id);
        } catch(Exception e){
            System.assert(false);
        }
    }
    
    @isTest
    static void createTest(){
        Account acc = [SELECT ID FROM Account limit 1];
        EnablersRepository enablersRepository = new EnablersRepository();
        List<Step_Towards_Success1__c> listStepTowardsSuccess1 = new List<Step_Towards_Success1__c>();
        try {
            listStepTowardsSuccess1 = enablersRepository.createEnablersForAccountIdClients(acc.id);
            enablersRepository.create(listStepTowardsSuccess1[0]);
        } catch(Exception e){
            System.assert(false);
        }
    }
    
    
    @isTest
    static void saveListTest(){
        Opportunity opp = [SELECT ID,AccountId FROM Opportunity limit 1];
        EnablersRepository enablersRepository = new EnablersRepository();
        List<Step_Towards_Success1__c> listStepTowardsSuccess1 = new List<Step_Towards_Success1__c>();
        try {
            listStepTowardsSuccess1 = enablersRepository.getOpportunityEnablers(opp);
            enablersRepository.save(listStepTowardsSuccess1);
        } catch(Exception e){
            System.assert(false);
        }
    }
    
    
    @isTest
    static void saveTest(){
        Opportunity opp = [SELECT ID,AccountId FROM Opportunity limit 1];
        EnablersRepository enablersRepository = new EnablersRepository();
        List<Step_Towards_Success1__c> listStepTowardsSuccess1 = new List<Step_Towards_Success1__c>();
        try {
            listStepTowardsSuccess1 = enablersRepository.getOpportunityEnablers(opp);
            enablersRepository.save(listStepTowardsSuccess1[0]);
        } catch(Exception e){
            System.assert(false);
        }
    }    
    
}