public with sharing class GenericCallout {

    private String genericCalloutDeveloperName{get;set;}
    private Generic_Integration_Callout__mdt metaDataType = new Generic_Integration_Callout__mdt();
    private String paramUrl{get;set;}

    private Http http = new Http();
    private HttpRequest request = new HttpRequest();
    private HttpResponse response = new HttpResponse();

    public GenericCallout(String genericCalloutDeveloperName) {
        this.genericCalloutDeveloperName = genericCalloutDeveloperName;
        this.setMetaDataType();
        this.setRequest();
    }

    public void setParam(String param){
        this.paramUrl = param;
        this.setRequest();
    }

    public HttpResponse getResponse(){
        this.response = http.send(this.request);
        return this.response;
    }

    private void setMetaDataType(){
        this.metaDataType = Generic_Integration_Callout__mdt.getInstance(genericCalloutDeveloperName);
    }

    private void setRequest(){
        this.request.setEndpoint(this.getUrl());
        this.request.setMethod(this.metaDataType.Http_Method__c);
        if(this.metaDataType.AuthorizationType__c != 'Without')
            this.request.setHeader('Authorization ',this.metaDataType.Token__c);
    }

    private String getUrl(){
        String url = this.metaDataType.URL__c;
        if(this.paramUrl != null && url != null)
            url = url.replace('{param}',this.paramUrl);
        return url;
    }
}