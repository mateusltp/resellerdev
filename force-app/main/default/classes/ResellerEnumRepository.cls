global with sharing class ResellerEnumRepository {

    global enum ResellerLogActions {
        CLICK_GET_ACCOUNTS,
        CLICK_SET_ACCOUNT_IN_ACCOUNT_REQUEST,
        CLICK_RUN_GO_NOGO,
        CLICK_RUN_GO_NOGO_GET_PRICE,
        JS_RUN_GO_NOGO_GET_PRICE
        
    }

    global enum ResellerOpportunityFastTrackStage {
        Lost
    }

    global enum ResellerOpportunityStageName {
        Perdido
    }

    global enum OpportunityFamilyMemberIncluded{
        Yes,
        No
    }

    global enum ResellerOpportunityOriginProcess {
        Single,
        Multiple
    }
    
    global enum AccountRequestPartnerModel {
        Subsidy,
        Intermediation,
        Exclusive,
        LegacyPartners
    }

    global enum PriceEngineItemPartnerCategory{
        Standard,
        Subsidy_5kFTEs,
        Subsidy_15kFTEs,
        ProductivityLow,
        ProductivityHigh
    }

    global enum AccountRequestStatus{
        Analyze,
        Invalid
    }

    global enum AccountRequestBID{
        Yes,
        No
    }

    global enum MassiveAccountRequestStatus{
        Draft,
        Loading,
        LookingForAccounts,
        LookingForOpportunities,
        LookingForEvents,
        RunEngine,
        RunPrice,
        Finished
    }

    global enum UniqueIdentifierType{
        CNPJ,
        NIF,
        EIN,
        UTR
    }

    global enum CountryCode{
        BR
    }

    global enum MassiveLog{
        UPLOADED_NEW_ACCOUNT_REQUEST,
        UNIQUE_KEY_VALIDED,
        UNIQUE_KEY_INVALIDED,
        UNIQUE_KEY_NOT_FOUND,
        NEW_ACCOUNT_SUCCESSFULLY_CREATED,
        NEW_ACCOUNT_NOT_CREATED,
        NEW_ACCOUNT_CREATED_BY_UNIQUE_KEY,
        NEW_ACCOUNT_CREATED_BY_WEB_SITE,
        ACCOUNT_FOUND_BY_CNPJ,
        ACCOUNT_FOUND_BY_WEBSITE,
        OPPORTUNITY_FOUND,
        OPPORTUNITY_NOT_FOUND,
        ACCOUNT_EVENT_FOUND,
        ACCOUNT_TASK_FOUND,
        ACCOUNT_ACTIVITY_NOT_FOUND,
        ENGINE_RUN,
        INFO_CURRENT_USER_FOUND,
        INFO_OWNER_ACCOUNT_REQUEST_FOUND,
        INFO_NOT_FOUND,
        NEW_OPPORTUNITY_SUCCESSFULLY_CREATED,
        NEW_OPPORTUNITY_NOT_CREATED
        

    }

}