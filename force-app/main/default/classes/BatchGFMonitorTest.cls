/**
 * @description       : 
 * @author            : JRDL@GFT.com
 * @group             : 
 * @last modified on  : 
 * @last modified by  : 
 * Modifications Log 
 * Ver   Date         Author                              Modification
 * 1.0   10-06-2021   jrdl@gft.com   Initial Version
**/
@isTest
public class BatchGFMonitorTest {
    
    @isTest
    public static void testBatchGFPreRenewal() {
   		Id rtId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
        Account acc = new Account();
        acc.name='Test ';
        acc.RecordTypeId = rtId;
        acc.GP_Status__c = 'Active';
        acc.Type = 'Prospect';
        acc.billingState = 'São Paulo';
        acc.BillingCity = 'São Paulo';
        acc.billingStreet = 'Rua Test';
        acc.billingCountry = 'Brazil';        
        acc.NumberOfEmployees = 520;
        insert acc;
        
        Id rtCtt = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Direct_Channel').getRecordTypeId();
        Contact accCtt = new Contact();
        accCtt.recordTypeId = rtCtt;
        accCtt.AccountId = acc.id;
        accCtt.Email = 'test@test.com';
        accCtt.FirstName = 'Contato';
        accCtt.LastName = 'Teste';
        accCtt.DecisionMaker__c = 'Yes';
        accCtt.GFSendSurvey__c = 'Client Sales PreRenewal';
        accCtt.Primary_HR_Contact__c = TRUE;
        insert accCtt;
        
        Id rtOpp = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        Opportunity opp = new Opportunity();
        opp.AccountId = acc.Id;
        opp.RecordTypeId = rtOpp;
        opp.Name = 'test';
        opp.StageName = 'Lançado/Ganho';
        opp.Probability = 100;
        opp.CurrencyIsoCode = 'BRL';
        opp.CloseDate = Date.today().addDays(90);
        insert opp;
        
        Id rtQ = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        Quote q = new Quote();
        q.Name = 'test';
        q.OpportunityId = opp.Id;
        q.RecordTypeId = rtQ;
        q.Submission_Date__c = Date.today().addDays(-90);
        q.End_Date__c = Date.today().addDays(90);
        q.License_Fee_Waiver__c = 'No';
        insert q;
        
        opp.SyncedQuoteId = q.Id;
        upsert opp;
        
        Test.startTest();
       	Database.executeBatch(new BatchGFPreRenewalMonitor(0));
        Test.stopTest();
    }
    
    @isTest
    public static void testBatchGFPostLaunch() {
   		Id rtId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
        Account acc = new Account();
        acc.name='Test ';
        acc.RecordTypeId = rtId;
        acc.GP_Status__c = 'Active';
        acc.Type = 'Prospect';
        acc.billingState = 'São Paulo';
        acc.BillingCity = 'São Paulo';
        acc.billingStreet = 'Rua Test';
        acc.billingCountry = 'Brazil';        
        acc.NumberOfEmployees = 520;
        insert acc;
        
        Id rtCtt = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Direct_Channel').getRecordTypeId();
        Contact accCtt = new Contact();
        accCtt.recordTypeId = rtCtt;
        accCtt.AccountId = acc.id;
        accCtt.Email = 'test@test.com';
        accCtt.FirstName = 'Contato';
        accCtt.LastName = 'Teste';
        accCtt.DecisionMaker__c = 'Yes';
        accCtt.GFSendSurvey__c = 'Client Launch/Post Launch';
        accCtt.Primary_HR_Contact__c = TRUE;
        insert accCtt;
        
        Id rtOpp = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        Opportunity opp = new Opportunity();
        opp.AccountId = acc.Id;
        opp.RecordTypeId = rtOpp;
        opp.Name = 'test';
        opp.StageName = 'Lançado/Ganho';
        opp.Probability = 100;
        opp.CurrencyIsoCode = 'BRL';
        opp.CloseDate = Date.today().addDays(-90);
        opp.Data_do_Lancamento__c = Date.today().addDays(-90);
        insert opp;
        
        Id rtQ = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        Quote q = new Quote();
        q.Name = 'test';
        q.OpportunityId = opp.Id;
        q.RecordTypeId = rtQ;
        q.Submission_Date__c = Date.today().addDays(-90);
        q.End_Date__c = Date.today().addDays(90);
        q.License_Fee_Waiver__c = 'No';
        insert q;
        
        Test.startTest();
       	Database.executeBatch(new BatchGFPostLaunchMonitor(0));
        Test.stopTest();

    }
    
    
    @isTest
    public static void testBatchGFProspectingSales() {
        List<Account> lstAcc = new List<Account>();
        List<Opportunity> lstOpp = new List<Opportunity>();
   		Id rtId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
        Account acc = new Account();
        acc.name='Test ';
        acc.RecordTypeId = rtId;
        acc.GP_Status__c = 'Active';
        acc.Type = 'Prospect';
        acc.billingState = 'São Paulo';
        acc.BillingCity = 'São Paulo';
        acc.billingStreet = 'Rua Test';
        acc.billingCountry = 'Brazil';        
        acc.NumberOfEmployees = 520;
        //insert acc;
        lstAcc.add(acc);
        
        Account acc2 = new Account();
        acc2.name='gft';
        acc2.RecordTypeId = rtId;
        acc2.GP_Status__c = 'Active';
        acc2.Type = 'Prospect';
        acc2.billingState = 'São Paulo';
        acc2.BillingCity = 'São Paulo';
        acc2.billingStreet = 'Rua Test';
        acc2.billingCountry = 'Brazil';        
        acc2.NumberOfEmployees = 520;
        //insert acc2;
        lstAcc.add(acc2);
        
        insert lstAcc;
        
        Id rtCtt = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Direct_Channel').getRecordTypeId();
        Contact accCtt = new Contact();
        accCtt.recordTypeId = rtCtt;
        accCtt.AccountId = acc.id;
        accCtt.Email = 'test@test.com';
        accCtt.FirstName = 'Contato';
        accCtt.LastName = 'Teste';
        accCtt.DecisionMaker__c = 'Yes';
        accCtt.GFSendSurvey__c = 'Client Sales Won';
        accCtt.Primary_HR_Contact__c = TRUE;
        insert accCtt;
        
        Contact accCtt2 = new Contact();
        accCtt2.recordTypeId = rtCtt;
        accCtt2.AccountId = acc2.id;
        accCtt2.Email = 'gft@test.com';
        accCtt2.FirstName = 'Contato';
        accCtt2.LastName = 'Teste';
        accCtt2.DecisionMaker__c = 'Yes';
        accCtt2.GFSendSurvey__c = 'Client Sales PostRenewal';
        accCtt2.Primary_HR_Contact__c = TRUE;
        insert accCtt2;
        
        Id rtOpp = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        Opportunity opp = new Opportunity();
        opp.AccountId = acc.Id;
        opp.RecordTypeId = rtOpp;
        opp.Name = 'test';
        opp.StageName = 'Lançado/Ganho';
        opp.Probability = 100;
        opp.CurrencyIsoCode = 'BRL';
        opp.CloseDate = Date.today().addDays(-1);
        opp.Data_do_Lancamento__c = Date.today().addDays(-1);
        //insert opp;
        lstOpp.add(opp);
        
        Id rtOppClientReneg = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Success_Renegotiation').getRecordTypeId();
        Opportunity opp2 = new Opportunity();
        opp2.AccountId = acc2.Id;
        opp2.RecordTypeId = rtOpp;
        opp2.Name = 'test';
        opp2.StageName = 'Perdido';
        opp2.Loss_Reason__c = 'Gym Network';
        opp2.Probability = 0;
        opp2.CurrencyIsoCode = 'BRL';
        opp2.CloseDate = Date.today().addDays(-1);
        opp2.Opportunity_Lost_Date__c = Date.today().addDays(-1);
        //insert opp2;
        lstOpp.add(opp2);
        
        insert lstOpp;
        
        system.debug('@@@@@@2222 lstAcc' + lstAcc);
        system.debug('@@@@@@@@@222 lstOpp' + lstOpp);
        
        /*Id rtQ = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        Quote q = new Quote();
        q.Name = 'test';
        q.OpportunityId = opp.Id;
        q.RecordTypeId = rtQ;
        q.Submission_Date__c = Date.today().addDays(-1);
        q.End_Date__c = Date.today().addDays(-1);
        q.License_Fee_Waiver__c = 'No';
        insert q;
        
        Id rtQ2 = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Direct_Channel').getRecordTypeId();
        Quote q2 = new Quote();
        q2.Name = 'test';
        q2.OpportunityId = opp2.Id;
        q2.RecordTypeId = rtQ2;
        q2.Status = 'Ganho';
        q2.Submission_Date__c = Date.today().addDays(-1);
        q2.End_Date__c = Date.today().addDays(-1);
        q2.License_Fee_Waiver__c = 'No';
        insert q2;
        
        q2.Status = 'Ganho';
        update q2;
        opp2.StageName = 'Lançado/Ganho';
        update opp2;*/
        
        Test.startTest();
       	Database.executeBatch(new BatchGFProspectingSalesMonitor(0));
        Test.stopTest();

    }
    
   
}