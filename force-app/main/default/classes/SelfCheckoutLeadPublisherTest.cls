@IsTest
public class SelfCheckoutLeadPublisherTest {
    @IsTest
    public static void publish() {
  
      SelfCheckoutLeadRequest selfCheckoutLeadRequest = SelfCheckoutLeadRequestMock.getMock();
  
      Test.startTest();
  
      SelfCheckoutLeadPublisher publisher = new SelfCheckoutLeadPublisher();
      publisher.publish( selfCheckoutLeadRequest );
      
      List<Queue__c> events =[ select Id,EventName__c,Status__c from Queue__c where EventName__c = 'SELF_CHECKOUT_LEAD_INBOUND' ];
      System.assertEquals(events.size(),1);
      System.assertEquals('QUEUED' , events[0].Status__c);
  
      Test.stopTest();
  
    }
}