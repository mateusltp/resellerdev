/**
 * @description       : 
 * @author            : GEPI@GFT.com
 * @group             : 
 * @last modified on  : 06-11-2021
 * @last modified by  : roei@gft.com
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   11-20-2020   GEPI@GFT.com   Initial Version
**/
public without sharing class cloneOppClientSuccessController {
    public cloneOppClientSuccessController(){}

    @AuraEnabled
    public static List< RecordType > getRenegotiationRecordTypes(){
        Set< String > lLstRenegotiationRtDevName = new Set< String >{
            'Client_Success_Renegotiation',
            'SMB_Success_Renegotiation'
        };
        Map< Id , Schema.RecordTypeInfo > lMapIdRtInfo = 
                Schema.SObjectType.Opportunity.SObjectType.getDescribe().getRecordTypeInfosById();
                
        try {
            List< RecordType > lLstRenegotiationRt = new List< RecordType >();
            
            for( RecordType iRecordtype : [ SELECT Id, Name, DeveloperName, Description FROM RecordType WHERE SObjectType = 'Opportunity' AND isActive = true 
                    AND DeveloperName =: lLstRenegotiationRtDevName ] ){

                Schema.RecordTypeInfo lRecordtypeInfo = lMapIdRtInfo.get( iRecordtype.Id );

                if( lRecordtypeInfo.isAvailable() ){
                    lLstRenegotiationRt.add( iRecordtype );
                }
            }
            
            return lLstRenegotiationRt;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static String getPicklistOptions(){
        List< PicklistOption > lPicklistOptions = new List< PicklistOption >();

        Schema.DescribeFieldResult lFieldResult = Opportunity.Sub_Type__c.getDescribe();

        for( Schema.PicklistEntry iPicklistEntry : lFieldResult.getPicklistValues() ){
            lPicklistOptions.add( new PicklistOption( iPicklistEntry.getLabel() , iPicklistEntry.getValue() ) );
        }     

        return JSON.serialize(lPicklistOptions);
    }

    @AuraEnabled
    public static String createNewRenegotiationOpp( String aAccId , String aRecordTypeDevName, String aSubTypeValue ){
        try{
            List< Opportunity > lLstOpp = 
                ClientSuccessAutoRenewalFlow.cloneLastClosedOpp( new List< Id > { aAccId } , true , aRecordTypeDevName , aSubTypeValue );

            return lLstOpp[0].Id;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    public class PicklistOption{
        public PicklistOption( String aLabel , String aValue ){
            label = aLabel;
            value = aValue;
        }
        public String label{get;set;}
        public String value{get;set;}
    }
}