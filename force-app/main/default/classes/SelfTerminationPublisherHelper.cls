public class SelfTerminationPublisherHelper {
    
    private EventQueue event;
    private Object targetOrder;
    private List<OrderTagusDTO> ordersTagus;
    private Order orderToUpdate;
    private IntegrationRequest integrationRequest;
    private Object responseObject;
    private final String UPDATE_ORDER_TAGUS_EVENT = 'UPDATE_ORDER_ON_TAGUS';
    private BaseRestProxy proxy;

    public SelfTerminationPublisherHelper(EventQueue event, Order orderToUpdate){
        this.event = event;
        this.orderToUpdate = orderToUpdate;
        this.proxy = getHttpRequestProxy(event);
    
    }

    public void updateOrderOnTagus(){

        this.ordersTagus = OrderTagusDAO.getOrderDtoByOrders(
            new List<Order>{this.orderToUpdate} 
          );

        event.addPayload(UPDATE_ORDER_TAGUS_EVENT, JSON.serialize(this.ordersTagus));

        this.targetOrder = transformToSend();
        event.log('Http Header ' + this.proxy?.getAuthorizationHeader());
        event.log('Send Message to Target System ' + this.event.config.endPointUrl__c);
        
        this.event.addPayload(
            'REQUEST_UPDATE_ORDER_ON_TAGUS_PAYLOAD_' + System.now(),
            JSON.serialize(targetOrder)
          );
        
        responseObject = send();
        event.log('Target System Response ' + JSON.serialize(responseObject));

        this.event.addPayload(
            'RESPONSE_UPDATE_ORDER_ON_TAGUS_PAYLOAD_' + System.now(),
            JSON.serialize(responseObject)
        );

    }
    private Object transformToSend(){
        TagusTransaction.TransactionType transactionType = TagusTransaction.TransactionType.UPDATED;
        this.integrationRequest = new IntegrationRequest();
        integrationRequest.transaction_info = new TagusTransaction(event,transactionType);
        integrationRequest.transaction_data = new TagusTransactionData(
        TagusTransactionData.SourceEventType.CRUD
        );

        integrationRequest.transaction_data.setData(new IntegrationRequestData(this.ordersTagus));
        return integrationRequest;
    }


  private BaseRestProxy getHttpRequestProxy(EventQueue event) {
    if(!Test.isRunningTest()){
      return new RestProxy(event);
    }else{
      return null;
    }
  }

   private Object send() {
    if(!Test.isRunningTest()){
      return this.proxy.send(targetOrder);
    }else{
      return null;
    }
  }


private class IntegrationRequest {
    private TagusTransaction transaction_info;
    private TagusTransactionData transaction_data;
  }

  private class IntegrationRequestData {
    private List<OrderTagusDTO> orders;

    private IntegrationRequestData(List<OrderTagusDTO> orders) {
      this.orders = orders;
    }
  }
}