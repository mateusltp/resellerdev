/**
 * @description       : Post Refresh Sandbox actions
 * @author            : tiago.ribeiro@gympass.com
 * @group             : 
 * @last modified on  : 03-10-2022
 * @last modified by  : tiago.ribeiro@gympass.com
**/
global class SandboxPostRefresh implements SandboxPostCopy{
    global void runApexClass(SandboxContext context) {
        removeEndpointsSettings();
    }

    /**
     *  Remove Production endpoints from Custom Setting
    */
    private void removeEndpointsSettings(){
        List<EventConfiguration__c> listEventConfiguration = EventConfiguration__c.getAll().values();
        for(EventConfiguration__c eventConfig : listEventConfiguration){
            eventConfig.endPointUrl__c = Label.Sandbox_Post_Refresh_URL;
        }
        
        update listEventConfiguration;
    }
}