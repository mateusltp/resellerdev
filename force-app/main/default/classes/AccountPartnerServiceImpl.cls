/**
 * @description       : 
 * @test classes      : AccountServiceTest
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 06-14-2022
 * @last modified by  : alysson.mota@gympass.com
 * Modifications Log 
 * Ver   Date         Author                             Modification
 * 1.0   07-16-2021   gft.samuel.silva@ext.gympass.com   Initial Version
 *       05-18-2022   bruno.mendes@gympass.com           Added sendAccountsForExclusivityApproval method for NetworkBuilderApprovalController
**/
public with sharing class AccountPartnerServiceImpl implements IAccountService {

    private static final PS_Constants constants = PS_Constants.getInstance();
    private static final String className = AccountPartnerServiceImpl.class.getName();

    private static final Set<String> statusToIgnore = new Set<String> {constants.APPROVAL_PENDING, constants.APPROVAL_APPROVED};
    private static List<Response> responses = new List<Response>();
    private static List<Account> accountsToSendForApproval = new List<Account>();
    private static List<Id> accountIds;

    public List<Account> avoidDuplicateAccountName( List<Account> lstNewAccount ) {
        
        AccountSelector accSelector = (AccountSelector)Application.Selector.newInstance(Account.sobjectType); 
        Map<String,Account> mapAccSelector = accSelector.selectAccountNamePartners(lstNewAccount);        
        if (mapAccSelector != null && !mapAccSelector.isEmpty()){
                for (Account newAcc : lstNewAccount ){
                    if (mapAccSelector.containsKey(newAcc.name)){
                        newAcc.addError(constants.ERROR_ACCOUNT_DUPLICATE_NAME + newAcc.Name);
                    }
            }
            return mapAccSelector.values();
        } else {
            return new List<Account>();
        }

    }

    public List<Account> wishListAndTierDependency( List<Account> lstNewAccount ) {
        for (Account acc : lstNewAccount){
            if( acc.Wishlist__c == true && (acc.Tier__c == constants.ACCOUNT_TIER_T4)){
                acc.addError(constants.ERROR_ACCOUNT_WISHLIST_CANNOT_BE_T4);
            }
            else if( acc.Wishlist__c == false && (acc.Tier__c == constants.ACCOUNT_TIER_T1 || acc.Tier__c == constants.ACCOUNT_TIER_T2 ||acc.Tier__c == constants.ACCOUNT_TIER_T3)){
                acc.addError(constants.ERROR_ACCOUNT_LONGTAIL_T4_OR_BLANK);
            }
        }
        return lstNewAccount;
    }

    public List<AccountContactRelation> getContactRelations(Set<Id> recordIds) {
        AccountContactRelationSelector accountSelector = (AccountContactRelationSelector)Application.Selector.newInstance(AccountContactRelation.sobjectType);
        return accountSelector.selectContactRelationById(recordIds);
    }

    public List<AccountContactRelation> getContactsFromParentThatAreNotRelatedToChild(Id childAccountId, Id parentAccountId) {
        List<AccountContactRelation> accountContactRelationsFromParentAndChild = getContactRelations(new Set<Id> {parentAccountId, childAccountId});
        Map<Id, AccountContactRelation> accountContactRelationsByIdsFromParent = new Map<Id, AccountContactRelation>();
        List<AccountContactRelation> accountContactRelationsFromChild = new List<AccountContactRelation>();
        System.debug('## accountContactRelationsFromParentAndChild: '+accountContactRelationsFromParentAndChild);
        for (AccountContactRelation acr : accountContactRelationsFromParentAndChild) {
            if (acr.AccountId == childAccountId) {
                accountContactRelationsFromChild.add(acr);
            } else {
                accountContactRelationsByIdsFromParent.put(acr.Id, acr);
            }
        }
        for (AccountContactRelation accountContactRelationChild : accountContactRelationsFromChild) {
            for (AccountContactRelation accountContactRelationParent : accountContactRelationsByIdsFromParent.values() ) {
                if (accountContactRelationParent.ContactId == accountContactRelationChild.ContactId) accountContactRelationsByIdsFromParent.remove(accountContactRelationParent.Id);
            }
        }
        return accountContactRelationsByIdsFromParent.values();
    }

    public List<AccountContactRelation> getContactsFromParent(Id recordId){
        AccountContactRelationSelector accountSelector = (AccountContactRelationSelector)Application.Selector.newInstance(AccountContactRelation.sobjectType); 
        return accountSelector.selectContactRelationById(new Set<Id>{recordId});
    }

    public void setAccountTypeProspect(List<Account> accountList) {
        for (Account account : accountList) {
            account.Type = constants.ACCOUNT_TYPE_PROSPECT;
        }
    }

    public void getAccountWishlistFromParent(List<Account> accountList) {
        Set<Id> parentAccountIds = new Set<Id>();
        for (Account account : accountList) {
            if (!String.isBlank(account.ParentId)) {
                parentAccountIds.add(account.ParentId);
            }
        }
        AccountSelector accountSelector = (AccountSelector)Application.Selector.newInstance(Account.sobjectType);
        Map<Id, Account> parentAccountsByIds = accountSelector.selectWishlist(parentAccountIds);
        for (Account account : accountList) {
            if (!String.isBlank(account.ParentId)) {
                account.Wishlist__c = parentAccountsByIds.get(account.ParentId).Wishlist__c;
                account.Tier__c = null;
            }
        }
    }

    public static List<Response> sendAccountsForExclusivityApproval(List<Id> selectedAccountIds, String comments) {
        accountIds = selectedAccountIds;
        // get accounts
        List<Account> accounts = getAccounts();

        // create requests
        List<Approval.ProcessSubmitRequest> requests = createApprovalProcessRequests(accounts, comments);

        // process requests
        List<Approval.ProcessResult> results = processRequests(requests, false);

        // process results
        processResults(results);
        return responses;
    }

    private static List<Account> getAccounts() {
        AccountSelector selector = (AccountSelector)Application.Selector.newInstance(Account.SObjectType);
        return selector.selectWithExclusivityApprovalStatusById(new Set<Id>(accountIds));
    }

    private static String getApprovalProcessApiName() {
        List<Partner__mdt> approvalProcessCustomMetadata = [SELECT Value__c FROM Partner__mdt WHERE DeveloperName = :constants.EXCLUSIVITY_APPROVAL_PROCESS_DEV_NAME];
        return !approvalProcessCustomMetadata.isEmpty() ? approvalProcessCustomMetadata[0].Value__c : '';
    }

    private static List<Approval.ProcessSubmitRequest> createApprovalProcessRequests(List<Account> accounts, String comments) {
        String accountApprovedOrPending = constants.ACCOUNT_APPROVED_OR_PENDING;
        List<Approval.ProcessSubmitRequest> requests = new List<Approval.ProcessSubmitRequest> ();
        String approvalProcessAPIName = getApprovalProcessApiName();
        for (Account account : accounts) {
            if (statusToIgnore.contains(account.Approval_Status_Exclusivity__c)) {
                responses.add(new Response(accountApprovedOrPending, account.Id, account.Name));
                continue;
            }
            accountsToSendForApproval.add(account);
            Approval.ProcessSubmitRequest request = new Approval.ProcessSubmitRequest();
            request.setComments(comments);
            request.setObjectId(account.Id);
            if (!String.isBlank(approvalProcessAPIName)) {
                request.setProcessDefinitionNameOrId(approvalProcessAPIName);
            }
            requests.add(request);
        }
        return requests;
    }

    private static List<Approval.ProcessResult> processRequests(List<Approval.ProcessSubmitRequest> requests, Boolean allOrNone) {
        List<Approval.ProcessResult> processResults = new List<Approval.ProcessResult>();
        try {
            processResults = Approval.process(requests, allOrNone);
        }catch (System.DmlException e) {
            createLog('processRequests', e.getMessage(), e.getStackTraceString());
            throw new AuraHandledException(e.getMessage());
        }
        return processResults;
    }

    private static void processResults(List<Approval.ProcessResult> results) {
        for (Integer i = 0; i<results.size(); i++) {
            if (!results[i].isSuccess()) {
                String errorMessage = '';
                for (Database.Error error : results[i].getErrors()) {
                    if (error.message.contains(constants.NO_APPLICABLE_APPROVAL)) {
                        errorMessage = constants.ACCOUNT_NOT_SENT_FOR_APPROVAL;
                    } else {
                        errorMessage += error.message+' ';
                    }
                }
                responses.add(new Response(errorMessage, accountsToSendForApproval[i].Id, accountsToSendForApproval[i].Name));
            } else {
                responses.add(new Response(accountsToSendForApproval[i].Id, accountsToSendForApproval[i].Name));
            }
        }
    }

    private static void createLog(String method, String message, String stackTrace) {
        DebugLog__c log = new DebugLog__c(
                Origin__c = '['+className+']['+method+']',
                LogType__c = constants.LOGTYPE_ERROR,
                RelatedToSquad__c = constants.LOGTYPE_SQUAD_PARTNERS,
                ExceptionMessage__c = message,
                StackTrace__c = stackTrace,
                DataScope__c = JSON.serialize(accountIds)
        );

        Logger.createLog(log);
    }

    public class Response {
        @AuraEnabled
        public String errorMessage;
        @AuraEnabled
        public String accountId;
        @AuraEnabled
        public String accountName;

        public Response (String accountId, String accountName) {
            this.accountId = accountId;
            this.accountName = accountName;
        }

        public Response (String errorMessage, String accountId, String accountName) {
            this.errorMessage = errorMessage;
            this.accountId = accountId;
            this.accountName = accountName;
        }
    }

    
    public TreeResponse buildNetwork(Id recordId) {      
        System.debug('tree result');
        TreeResponse response = new DynamicAccountTreeStructure().getNetwork(recordId);
        System.debug(response);
        return response;
    }

    /**
    * Builds the network structure for contract creation. 
    * @author alysson.mota@gympass.com | 03-07-2022 
    * @param accountId
    * @param oppId
    * @return TreeResponse 
    **/
    public TreeResponse buildNetworkForContracts(Id accountId, Id oppId) {      
        OpportunitySelector oppSelector = (OpportunitySelector)Application.Selector.newInstance(Opportunity.sobjectType);
        AccountOpportunitySelector accOppSelector = (AccountOpportunitySelector)Application.Selector.newInstance(Account_Opportunity_Relationship__c.sobjectType); 
        AccountContractAgreementRelSelector accContractRelSelector = (AccountContractAgreementRelSelector)Application.Selector.newInstance(Account_Contract_Agreement_Relationship__c.sObjectType);
        DynamicAccountTreeStructure dynamicAccTreeStruct = new DynamicAccountTreeStructure();
        
        Opportunity opp = oppSelector.byId(new Set<Id>{oppId}).get(0);

        TreeResponse response = dynamicAccTreeStruct.getNetwork(accountId, opp);
        return response;
    }

    /* Hierarchy Builder Inner Class */
    private class DynamicAccountTreeStructure{        
        private Map<Id, AccountTreeStructure> result;
        private List<AccountTreeStructure> accountLstInTree = new List<AccountTreeStructure>();
        private Map<Id, Id> childIdMap;
        private Account topTreeAccount;

        private TreeResponse getNetwork(Id recordId){
            AccountSelector accountSelector = (AccountSelector)Application.Selector.newInstance(Account.sobjectType); 
            
            findAccountOnTop(recordId, accountSelector);
            result = new Map<Id, AccountTreeStructure>();
            childIdMap = new Map<Id, Id>();
            Map<Id, Account> accMap = new Map<Id, Account>(new List<Account>{topTreeAccount});
            if(!accMap.isEmpty()){
                startFetchingAccountDetails(accMap, accountSelector);
            }
            TreeResponse response = new TreeResponse(accountLstInTree, result.values());
            return response;
        }

        private List<AccountTreeStructure> startFetchingAccountDetails(Map<Id, Account> accMap,  AccountSelector accountSelector){
            Map<Id, AccountTreeStructure> parentStructure = gatherAllAccountInformation(accMap);
            if(result == null || result.isEmpty()){
                result.putAll(parentStructure);
            }

            Map<Id, Account> childMap = accountSelector.selectByParentId(accMap.keySet());

            if(childMap != null && !childMap.isEmpty() && childMap.size() > 0){
                Map<Id, Id> accChildIdMap = new Map<Id, Id>();
                for(Id childAccountId : childMap.keySet()){
                    Account child = childMap.get(childAccountId);             
                    childIdMap.put(child.Id, child.ParentId);                
                }
                
                List<AccountTreeStructure> childStructure = startFetchingAccountDetails(childMap, accountSelector);   
                for(AccountTreeStructure child : childStructure){
                    AccountTreeStructure parent = parentStructure.get(childIdMap.get(child.id));      
                    if(parent.items == null){
                        parent.items = new List<AccountTreeStructure>();
                    }
                    parent.items.add(child);
                } 
            }
            return parentStructure.values();
        }
        
        /**
        * Gets network for account only with accounts with relation to the given oppId 
        * @author alysson.mota@gympass.com | 03-07-2022 
        * @param accountId 
        * @param oppId 
        * @return TreeResponse 
        **/
        private TreeResponse getNetwork(Id accountId, Opportunity opp){
            System.debug('getNetwork');
            AccountSelector accountSelector = (AccountSelector)Application.Selector.newInstance(Account.sobjectType); 
            ProductOpportunityAssignmentSelector poaSelector = (ProductOpportunityAssignmentSelector)Application.Selector.newInstance(Product_Opportunity_Assignment__c.sObjectType);
            Set<Id> accIdSetFromPoaWithProduct = getAccIdSetFromPoaWithProduct( searchPoaForOppId(poaSelector, opp) );
                
            findAccountOnTop(accountId, accountSelector);
            
            result = new Map<Id, AccountTreeStructure>();
            childIdMap = new Map<Id, Id>();
            
            Map<Id, Account> accMap = new Map<Id, Account>(new List<Account>{topTreeAccount});
            
            if(!accMap.isEmpty()){
                startFetchingAccountDetailsForContracts(accMap, accountSelector, accIdSetFromPoaWithProduct, opp);
            }
            
            TreeResponse response = new TreeResponse(accountLstInTree, result.values());
            return response;
        }

        /**
        * @author alysson.mota@gympass.com | 03-07-2022 
        * @param poaSelector 
        * @param oppId 
        * @return List<Product_Opportunity_Assignment__c> 
        **/
        private List<Product_Opportunity_Assignment__c> searchPoaForOppId(ProductOpportunityAssignmentSelector poaSelector, Opportunity opp) {
            List<Product_Opportunity_Assignment__c> poaLst = poaSelector.selectByOpportunityWithProduct(opp.Id);
    
            if (poaLst.size() > 0) {
                return poaLst;
            } else {
                return new List<Product_Opportunity_Assignment__c>();
            }
        }

        /**
        * @author alysson.mota@gympass.com | 03-07-2022 
        * @param poaLst 
        * @return Set<Id> 
        **/
        private Set<Id> getAccIdSetFromPoaWithProduct(List<Product_Opportunity_Assignment__c> poaLst) {
            System.debug('getAccIdSetFromPoaWithProduct');
            Set<Id> accIdSetFromPoaWithProduct = new Set<Id>();

            for (Product_Opportunity_Assignment__c poa : poaLst) {
                accIdSetFromPoaWithProduct.add(poa.OpportunityMemberId__r.Account__c);
            }

            System.debug(accIdSetFromPoaWithProduct);
            return accIdSetFromPoaWithProduct;
        }
        
        /**
        * @author alysson.mota@gympass.com | 03-07-2022 
        * @param accMap 
        * @param accountSelector 
        * @param accIdSetFromPoaWithProduct 
        * @return List<AccountTreeStructure> 
        **/
        private List<AccountTreeStructure> startFetchingAccountDetailsForContracts(Map<Id, Account> accMap,  AccountSelector accountSelector, Set<Id> accIdSetFromPoaWithProduct, Opportunity opp){
            Map<Id, AccountTreeStructure> parentStructure = gatherAllAccountInformation(accMap, opp, accIdSetFromPoaWithProduct);
            if(result == null || result.isEmpty()){
                result.putAll(parentStructure);
            }

            Map<Id, Account> childMap = accountSelector.selectByParentId(accMap.keySet());

            if(childMap != null && !childMap.isEmpty() && childMap.size() > 0){
                Map<Id, Id> accChildIdMap = new Map<Id, Id>();
                for(Id childAccountId : childMap.keySet()){
                    Account child = childMap.get(childAccountId);
                    
                    if (opp.RecordType.DeveloperName == 'Partner_Flow_Child_Opportunity') {
                        childIdMap.put(child.Id, child.ParentId);
                    } else {
                        
                        if (child.Partner_Level__c != 'Location') {
                            childIdMap.put(child.Id, child.ParentId);                
                        } else {
                            
                            if (accIdSetFromPoaWithProduct.contains(child.Id)) {
                                childIdMap.put(child.Id, child.ParentId);
                            }

                        }
                    }
                }
               
                List<AccountTreeStructure> childStructure = startFetchingAccountDetailsForContracts(childMap, accountSelector, accIdSetFromPoaWithProduct, opp);   
                
                for(AccountTreeStructure child : childStructure){
                    AccountTreeStructure parent = parentStructure.get(childIdMap.get(child.id));      
                    if (parent != null) {
                        if(parent.items == null){
                            parent.items = new List<AccountTreeStructure>();
                        }
                        parent.items.add(child);
                    }
                }
            }

            return parentStructure.values();
        }
    
        /**
        * Method to gather all information for all accounts recieved
        */
        private Map<Id, AccountTreeStructure> gatherAllAccountInformation( Map<Id, Account> accMap ){
            Map<Id, AccountTreeStructure> result = new Map<Id, AccountTreeStructure>();
            for(Id accountId : accMap.keySet()){
                Account acc = accMap.get(accountId);

                AccountTreeStructure accStructure = getAccountTreeStructureInstance(acc);
                accountLstInTree.add(accStructure);        
                result.put(accountId, accStructure);
            }
            return result;
        }
    
        /**
        * gatherAllAccountInformation@description for contract creation flow 
        * @author alysson.mota@gympass.com | 04-19-2022 
        * @param accMap 
        * @param opp 
        * @param accIdSetFromPoaWithProduct 
        * @return Map<Id, AccountTreeStructure> 
        **/
        private Map<Id, AccountTreeStructure> gatherAllAccountInformation( Map<Id, Account> accMap, Opportunity opp,  Set<Id> accIdSetFromPoaWithProduct){
            Map<Id, AccountTreeStructure> result = new Map<Id, AccountTreeStructure>();

            for(Id accountId : accMap.keySet()){
                Account iAcc = accMap.get(accountId);
                String oppRTypeDevName = opp.RecordType.DeveloperName;
                String accPartnerLevel = iAcc.Partner_Level__c;

                if (iAcc.Partner_Level__c != 'Location' || 
                    (oppRTypeDevName == 'Partner_Flow_Child_Opportunity' && accPartnerLevel == 'Location') ||
                    (oppRTypeDevName == 'Partner_Flow_Opportunity'       && accPartnerLevel == 'Location' && accIdSetFromPoaWithProduct.contains(accountId))) {
                    
                    AccountTreeStructure accStructure = getAccountTreeStructureInstance(iAcc);
                    accountLstInTree.add(accStructure);        
                    result.put(accountId, accStructure);
                }
            }

            return result;
        }

        private AccountTreeStructure getAccountTreeStructureInstance(Account acc) {    
            String contactName = null;
            String contactId = null;
            String contactEmail = null;

            if (acc.Legal_Representative__c != null) {
                contactName     = acc.Legal_Representative__r.Name;
                contactId       = acc.Legal_Representative__c;
                contactEmail    = acc.Legal_Representative__r.Email;
            }

            return new AccountTreeStructure(acc.Id, acc.name, acc.Partner_Level__c, contactName, contactId, contactEmail, null, acc.BillingCountryCode);
        }

         /**
        * Recursion method to get top account;
        */
        private void findAccountOnTop(ID recordId, AccountSelector accountSelector){
            Account acc = accountSelector.selectByIdForPartners(new Set<Id>{recordId})[0]; 
            if(acc.ParentId != null ){
                findAccountOnTop(acc.ParentId, accountSelector);
            }else {
                this.topTreeAccount = acc;
            }    
        }
    }

    public class TreeResponse {        
        @AuraEnabled
        public List<AccountTreeStructure> accounts { get; set;}
        @AuraEnabled
        public List<AccountTreeStructure> tree {get; set;}
        @AuraEnabled
        public Boolean hasExclusivityAccounts {get; set;}

        public TreeResponse(List<AccountTreeStructure> accLst, List<AccountTreeStructure> tree){
            this.accounts = new List<AccountTreeStructure>(accLst);
            this.tree = new List<AccountTreeStructure>(tree);
            this.hasExclusivityAccounts = checkIfAccountsCountryNeedsExclusivityApproval(this.accounts);
            system.debug('## has exclusivity accounts: '+hasExclusivityAccounts);
        }

        // To add more countries, please update the Value field on the Exclusivity Approval Countries from the Partner__mdt custom metadata with the BillingCountryCode
        private boolean checkIfAccountsCountryNeedsExclusivityApproval(List<AccountTreeStructure> accLst) {
            Boolean exclusivityApprovalNeeded = false;
            // change to custom metadata in a future release to include more countries - adicionar record na metadata Partner
            String countryCodesRequiringApproval = getCountryCodesRequiringApproval();
            if (!String.isBlank(countryCodesRequiringApproval)) {
                for (AccountTreeStructure accountTreeStructure : accLst) {
                    if (accountTreeStructure.CountryCode != null && countryCodesRequiringApproval.contains(accountTreeStructure.CountryCode)) {
                        exclusivityApprovalNeeded = true;
                    }
                }
            }
            return exclusivityApprovalNeeded;
        }

        private String getCountryCodesRequiringApproval() {
            List<Partner__mdt> approvalProcessCustomMetadata = [SELECT Value__c FROM Partner__mdt WHERE DeveloperName = :constants.EXCLUSIVITY_APPROVAL_COUNTRIES];
            return !approvalProcessCustomMetadata.isEmpty() ? approvalProcessCustomMetadata.get(0).Value__c : '';
        }
    }

    public class AccountTreeStructure{      
        @AuraEnabled public ID id;
        @AuraEnabled public String Name;
        @AuraEnabled public String NameUrl;
        @AuraEnabled public String PartnerLevel;     
        @AuraEnabled public List<AccountTreeStructure> items;
        @AuraEnabled public String LegalRepresentativeName;
        @AuraEnabled public String LegalRepresentativeUrl;
        @AuraEnabled public String LegalRepresentativeEmail;
        @AuraEnabled public String CountryCode;

        public AccountTreeStructure(ID id, String label, String partnerLevel, String legalRepresentativeName, String legalRepresentativeId, String legalRepresentativeEmail,  List<AccountTreeStructure> items, String country){
            this.id = id;
            this.Name = label;
            this.NameUrl = '/'+Id;
            this.PartnerLevel = partnerLevel;  
            this.LegalRepresentativeName = legalRepresentativeName == null ? null : legalRepresentativeName;
            this.LegalRepresentativeUrl = legalRepresentativeId == null ? null : '/'+legalRepresentativeId;
            this.LegalRepresentativeEmail = legalRepresentativeEmail == null ? null : legalRepresentativeEmail;
            if(items != null && items.size() > 0){
                this.items = items;
            }
            this.CountryCode = country;
        }
    }

    public void avoidDuplicatedNetworkIds( List<Account> lstNewAccount ) {

        Set<String> networkIdsSet = new Set<String>();
        Set<String> accountIdsSet = new Set<String>();

        List<Account> accountWithNetworkLst = new List<Account>();
        for (Account acct : lstNewAccount) {
            if(acct.Network_ID__c != null && !String.isBlank(acct.Network_ID__c)) {
                networkIdsSet.add(acct.Network_ID__c);
                accountIdsSet.add(acct.id);
                accountWithNetworkLst.add(acct);
        }

    }
        if(!accountWithNetworkLst.isEmpty()){
            AccountSelector accSelector = (AccountSelector)Application.Selector.newInstance(Account.sobjectType); 
            List<Account> acctLst = accSelector.selectByNetworkId(networkIdsSet, accountIdsSet);  
            for (Account acct : accountWithNetworkLst) {
                System.debug('Minha conta '+ acct.Name + ' ' + acct.Network_ID__c);
                for (Account a : acctLst) {
                    System.debug('Qual a conta? '+ a.Name + ' ' + a.Network_ID__c);
                    
                    if (!String.isBlank(acct.Network_ID__c) && acct.Network_ID__c == a.Network_ID__c && acct.Ultimate_Parent__c != a.Ultimate_Parent__c) {
                        acct.addError(constants.ERROR_ACCOUNT_DUPLICATE_NETWORKID + acct.Network_ID__c);
                    }
                }
            }
        }
       
    }
    
    public class AccountPartnerServiceImplException extends Exception {}
}