/**
 * @description       : 
 * @author            : AlyssonMota
 * @group             : 
 * @last modified on  : 07-01-2021
 * @last modified by  : Alysson Mota
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   06-18-2021   Alysson Mota   Initial Version
**/
public without sharing class OpportunityTriggerSmbHelper {
    private Id SmbNewBusinessRtId       = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SMB_New_Business').getRecordTypeId();
    private Id SmbSuccessRenegRtId      = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SMB_Success_Renegotiation').getRecordTypeId();
    
    public void fillRelatedObjsFields(Map<Id, Opportunity> oldIdToOpp, Map<Id, Opportunity> newIdToOpp) {
        System.debug('fillRelatedObjsFields');
        Map<Id, Opportunity> accIdToApprovedOpps = new Map<Id, Opportunity>();
        
        for (Opportunity newOpp : newIdToOpp.values()) {
            Opportunity oldOpp = oldIdToOpp.get(newOpp.Id);

            if ((newOpp.RecordTypeId == SmbNewBusinessRtId || newOpp.RecordTypeId == SmbSuccessRenegRtId ) && 
                (newOpp.FastTrackStage__c == 'Offer Creation' && oldOpp.FastTrackStage__c == 'Validated')) {
                accIdToApprovedOpps.put(newOpp.AccountID, newOpp);
            }
        }

        if (accIdToApprovedOpps.size() > 0) onApproved(accIdToApprovedOpps, newIdToOpp);
    }

    private void onApproved(Map<Id, Opportunity> accIdToApprovedOpps, Map<Id, Opportunity> newIdToOpp) {
        Map<Id, Account> idToAcc = getAccounts(accIdToApprovedOpps.keySet());
        Map<Id, Quote> oppIdToQuote = getQuotes(newIdToOpp.keySet());
        List<Account_Opportunity_Relationship__c> accOppRelList = getAccOppRelList(newIdToOpp.keySet());
        Map<Id, List<Account_Opportunity_Relationship__c>> oppIdToAccOppRelList = getAccOppRelMap(accOppRelList);
        assignProposalContacts(accIdToApprovedOpps, idToAcc, oppIdToQuote);
    }

    private Map<Id, Account> getAccounts(Set<Id> accIdSet) {
        return new AccountRepository().getMapByIds(accIdSet);
    }

    private Map<Id, Quote> getQuotes(Set<Id> oppIdSet) {
        return new QuoteRepository().getLastForOpportunitiesAsOppIdToQuoteMap(new List<Id>(oppIdSet));
    }

    private List<Account_Opportunity_Relationship__c> getAccOppRelList(Set<Id> oppIdSet) {
        return new AccountOpportunityRepository().getAccountsInOpps(new List<Id>(oppIdSet));
    }

    private Map<Id, List<Account_Opportunity_Relationship__c>> getAccOppRelMap(List<Account_Opportunity_Relationship__c> accOppRelList) {
        Map<Id, List<Account_Opportunity_Relationship__c>> oppIdToAccOppRelList = new Map<Id, List<Account_Opportunity_Relationship__c>>();

        for (Account_Opportunity_Relationship__c accOppRel : accOppRelList) {
            if (oppIdToAccOppRelList.containsKey(accOppRel.Opportunity__c)) {
                oppIdToAccOppRelList.get(accOppRel.Opportunity__c).add(accOppRel);
            } else {
                oppIdToAccOppRelList.put(accOppRel.Opportunity__c, new List<Account_Opportunity_Relationship__c>{accOppRel});
            }
        }

        return oppIdToAccOppRelList;
    }

    private void  assignProposalContacts(Map<Id, Opportunity> accIdToApprovedOpps, Map<Id, Account> idToAcc,  Map<Id, Quote> oppIdToQuote) {
        List<Quote> quotesToUpdate = new List<Quote>();

        for (Opportunity opp : accIdToApprovedOpps.values()) {
            Account acc = idToAcc.get(opp.AccountId);
            Quote proposal = oppIdToQuote.get(opp.Id);

            for (Contact contact : acc.contacts) {
                if (proposal != null && contact.Primary_HR_Contact__c == true) {
                    proposal.Administrator__c = contact.Id;
                    proposal.Finance__c = contact.Id;
                    proposal.Operations__c = contact.Id;
                    proposal.Payroll_Debite_Memo_Contact__c = contact.Id;
                    proposal.ContactId = contact.Id;

                    quotesToUpdate.add(proposal);
                    break;
                }
            }
        }

        update quotesToUpdate;
    }
}