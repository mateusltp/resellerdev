/*
* @author Bruno Pinho
* @date December/2018
* @description Event trigger handler for after delete operations
*/

public class EventAfterDeleteTriggerHandler extends TriggerHandlerBase
{
    public override void mainEntry(TriggerParameters tp)
    {
        process((List<Event>)tp.oldList);
    }
    
    private static void process(List<Event> listOldEvents)
    {
        EventTriggerHelper.deleteRelatedGympassEvents(listOldEvents);
    }
    
    public override void inProgressEntry(TriggerParameters tp)
    {
        System.debug('This is an example for reentrant code...');
    }
}