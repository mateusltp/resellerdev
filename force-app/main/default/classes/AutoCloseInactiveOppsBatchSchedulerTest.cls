@isTest
public class AutoCloseInactiveOppsBatchSchedulerTest {
	public static String CRON_EXP = '0 0 7 * * ?';
    
    @isTest 
    static void testAutoCloseInactiveOppsBatchScheduler() {
        
        Test.startTest();
        String jobId = System.Schedule('TestAutoCloseInactiveOppsBatch', CRON_EXP, new AutoCloseInactiveOppsBatchScheduler());
        Test.stopTest();
        
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE Id = :jobId];
        System.assert(String.valueOf(ct.NextFireTime).contains(String.valueOf(Datetime.now().year())));
    }
}