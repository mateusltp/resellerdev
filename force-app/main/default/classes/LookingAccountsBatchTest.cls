@isTest
public class LookingAccountsBatchTest {

        @isTest
        public static void ProcessesCasesReschedulingBatchTest() {
             Massive_Account_Request__c massReq = new Massive_Account_Request__c(
            name='test_loadMassiveAccountRequest',
            Status__c=ResellerEnumRepository.MassiveAccountRequestStatus.LookingForAccounts.name()   
        );
  
        insert massReq;
        Account account = new Account();
        account.Name = 'Account';
        account.BillingCountry = 'Brazil';
        account.BillingState = 'São Paulo';
        account.Id_Company__c = '82.898.430/0001-37';
        insert account;
            
        List<Account_Request__c> requests = new List<Account_Request__c>();
        
        Account_Request__c request = new Account_Request__c();  
        request.Name = 'teste';
        request.Unique_Identifier_Type__c = 'CNPJ';
        request.Unique_Identifier__c = '70922695000144';
        request.Partner_Model__c = 'Intermediation';
        request.Billing_Country__c = 'Brazil';
        request.Total_number_of_employees__c = 100;
        request.Massive_Account_Request__c = massReq.Id;
        request.AccountId__c = account.Id;
        request.Bulk_Operation__c = true;
        request.Massive_Status__c = ResellerEnumRepository.MassiveAccountRequestStatus.LookingForAccounts.name();
        requests.add(request);
        
        Account_Request__c request2 = new Account_Request__c();  
        request2.Name = 'teste2';
        request2.Unique_Identifier_Type__c = 'CNPJ';
        request2.Unique_Identifier__c = '05451000';
        request2.Billing_Country__c = 'Brazil';
        request2.Massive_Account_Request__c = massReq.Id;
        request2.Bulk_Operation__c = true;
        request2.Massive_Status__c = ResellerEnumRepository.MassiveAccountRequestStatus.LookingForAccounts.name();
        requests.add(request2);
            
        Account_Request__c request3 = new Account_Request__c();  
        request3.Name = 'ㅤ';
        request3.Unique_Identifier_Type__c = 'CNPJ';
        request3.Unique_Identifier__c = null;
        request3.Billing_Country__c = '';
        request3.Massive_Account_Request__c = massReq.Id;
        request3.Bulk_Operation__c = true;
        request3.Massive_Status__c = ResellerEnumRepository.MassiveAccountRequestStatus.LookingForAccounts.name();
        requests.add(request3);
            
        Account_Request__c request4 = new Account_Request__c();  
        request4.Name = 'teste 4';
        request4.Unique_Identifier_Type__c = 'EIN';
        request4.Website__c = null;
        request4.Massive_Account_Request__c = massReq.Id;
        request4.Bulk_Operation__c = true;
        request4.Massive_Status__c = ResellerEnumRepository.MassiveAccountRequestStatus.LookingForAccounts.name();
        requests.add(request4);
            
        Account_Request__c request5 = new Account_Request__c();  
        request5.Name = 'teste 5';
        request5.Unique_Identifier_Type__c = 'EIN';
        request5.Website__c = 'www.tes';
        request5.Massive_Account_Request__c = massReq.Id;
        request5.Bulk_Operation__c = true;
        request5.Massive_Status__c = ResellerEnumRepository.MassiveAccountRequestStatus.LookingForAccounts.name();
        requests.add(request5);
            
        Account_Request__c request6 = new Account_Request__c();  
        request6.Name = 'teste 6';
        request6.Unique_Identifier_Type__c = 'EIN';
        request6.Website__c = 'teste6.com';
        request6.Massive_Account_Request__c = massReq.Id;
        request6.Bulk_Operation__c = true;
        request6.Massive_Status__c = ResellerEnumRepository.MassiveAccountRequestStatus.LookingForAccounts.name();
        requests.add(request6);
            
        insert requests;

        Test.startTest();
          
			 LookingAccountsBatch bt = new LookingAccountsBatch();
             bt.massiveId = massreq.Id;
             ID idAcc = DataBase.executeBatch(bt);
            
            request.Massive_Status__c = ResellerEnumRepository.MassiveAccountRequestStatus.LookingForOpportunities.name();
            massReq.Status__c=ResellerEnumRepository.MassiveAccountRequestStatus.LookingForOpportunities.name();  
            
            LookingOpportunitiesBatch bt2 = new LookingOpportunitiesBatch();
            bt.massiveId = massReq.Id;
            ID idOpp = DataBase.executeBatch(bt2);

    
        Test.stopTest();
        
        System.assertEquals(massReq.Status__c, 'LookingForOpportunities');

        //Account accountAssert = [ SELECT Id, Name FROM Account WHERE BillingCountry = 'Brazil' limit 1];
        //System.assertEquals(accountAssert.name, 'Account');
       
        }
        
    }