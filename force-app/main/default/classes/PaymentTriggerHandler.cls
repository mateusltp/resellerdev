/**
 * @author vinicius.ferraz
 */
public with sharing class PaymentTriggerHandler extends triggerhandler {

    public override void beforeUpdate() {
        new OfferChangesForApprovalHandler().billingHandler( (List<Payment__c>) trigger.new, (Map<Id,Payment__c>) trigger.oldMap );
		new PaymentTriggerHelper().checkIfFrequencyHaschanged( (List<Payment__c>) trigger.new, (Map<Id,Payment__c>) trigger.oldMap );
        new ResellerSKUDealDeskHandler().checkDealDeskFieldsApproved( (Map<Id,Payment__c>) trigger.oldMap , (List<Payment__c>) trigger.new ); 

    }
    
    public override void beforeDelete() {
    }
    
    public override void afterUpdate() {
    }

    public override void beforeInsert() {
        
    }

    public override void afterInsert() {
    }
}