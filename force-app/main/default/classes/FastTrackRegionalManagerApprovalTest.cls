/**
 * @description       : 
 * @author            : 
 * @group             : 
 * @last modified on  : 04-27-2022
 * @last modified by  : Mateus Augusto - GFT (moes@gft.com)
**/
@IsTest
public class FastTrackRegionalManagerApprovalTest {
    
    
    @TestSetup
    static void Setup(){
        
        Pricebook2 lStandardPricebook = DataFactory.newPricebook();
        Database.update(lStandardPricebook);
                
        Account acc = DataFactory.newAccount();
        acc.Indirect_Channel_Pricebook__c = '5k FTEs';
        Database.insert(acc);
        
        Account accReseller = DataFactory.newAccountReseller();
        accReseller.Indirect_Channel_Pricebook__c = '5k FTEs';
        Database.insert(accReseller);  
        
        Product2 productBRL = DataFactory.newProduct('teste', false, 'BRL');
        Database.insert(productBRL);
        
        Pricebook2 pbReseller = DataFactory.newPricebook('Reseller', 'Subsidy');
        Database.insert(pbReseller); 
        
        Pricebook2 pb5 = DataFactory.newPricebook('Reseller 5k', 'Subsidy 5k');
        Database.insert(pb5);
        
        Pricebook2 pb15 = DataFactory.newPricebook('Reseller 15k', 'Subsidy 15k');
        Database.insert(pb15);  
        
        Opportunity oppSubsidy = DataFactory.newOpportunity(acc.id, pb5, 'Indirect_Channel_New_Business');
        oppSubsidy.Reseller_del__c = accReseller.id;
        oppSubsidy.B_M__c = 'Total Subsidy';
        Database.insert(oppSubsidy); 
                
        PricebookEntry pbEntry = DataFactory.newPricebookEntry(lStandardPricebook, productBRL, oppSubsidy);
        Database.insert(pbEntry);
        
        PricebookEntry pbEntry5 = DataFactory.newPricebookEntry(pb5, productBRL, oppSubsidy);
        pbEntry5.UnitPrice = 30;
        Database.insert(pbEntry5);        
        
        PricebookEntry pbEntry15 = DataFactory.newPricebookEntry(pb15, productBRL, oppSubsidy);
        Database.insert(pbEntry15); 
        
        Quote quoteSubsidy = DataFactory.newQuote(oppSubsidy, 'testSubsidy', 'Client_Sales_New_Business');
        Database.insert(quoteSubsidy); 
        
        QuoteLineItem accessFeeSubsidy = DataFactory.newQuoteLineItem(quoteSubsidy, pbEntry5);
        accessFeeSubsidy.UnitPrice = 26;
        Database.insert(accessFeeSubsidy);
        
        Pricebook2 pbExclusive = DataFactory.newPricebook('Exclusive', 'Exclusive');
        Database.insert(pbExclusive);
        
        Opportunity oppExclusive = DataFactory.newOpportunity(acc.id, pbExclusive, 'Indirect_Channel_New_Business');
        oppExclusive.Reseller_del__c = accReseller.id;
        oppExclusive.B_M__c = 'Exclusive';
        Database.insert(oppExclusive);        

        PricebookEntry pbEntryExclusive = DataFactory.newPricebookEntry(pbExclusive, productBRL, oppExclusive);
        Database.insert(pbEntryExclusive);
        
        Quote quoteExclusive = DataFactory.newQuote(oppExclusive, 'testoppExclusive', 'Client_Sales_New_Business');
        Database.insert(quoteExclusive);
        
        QuoteLineItem accessFeeExclusive = DataFactory.newQuoteLineItem(quoteExclusive, pbEntryExclusive);
        accessFeeExclusive.UnitPrice = 26;
        Database.insert(accessFeeExclusive);

        Pricebook2 pbIntermediation = DataFactory.newPricebook('Intermediation', 'Intermediation');
        Database.insert(pbIntermediation);        
                
        Opportunity oppIntermediation = DataFactory.newOpportunity(acc.id, pbIntermediation, 'Indirect_Channel_New_Business');
        oppIntermediation.Reseller_del__c = accReseller.id;
        oppIntermediation.B_M__c = 'Intermediation';
        Database.insert(oppIntermediation);

        PricebookEntry pbEntryIntermediation = DataFactory.newPricebookEntry(pbIntermediation, productBRL, oppIntermediation);
        pbEntryIntermediation.UnitPrice = 20;
        Database.insert(pbEntryIntermediation);
        
        Quote quoteIntermediation = DataFactory.newQuote(oppIntermediation, 'testoppIntermediation', 'Client_Sales_New_Business');
        Database.insert(quoteIntermediation);
       
        QuoteLineItem accessFeeIntermediation = DataFactory.newQuoteLineItem(quoteIntermediation, pbEntryIntermediation);
        accessFeeIntermediation.UnitPrice = 25;
        Database.insert(accessFeeIntermediation);
        /*
        Opportunity oppSubsidyUSD = DataFactory.newOpportunity(acc.id, pb5, 'Indirect_Channel_New_Business');
        oppSubsidyUSD.Reseller_del__c = accReseller.id;
        oppSubsidyUSD.B_M__c = 'Total Subsidy';
        oppSubsidyUSD.CurrencyIsoCode = 'USD';
        Database.insert(oppSubsidyUSD); 
        
        Quote quoteSubsidyUSD = DataFactory.newQuote(oppSubsidyUSD, 'testSubsidy', 'Client_Sales_New_Business');
        Database.insert(quoteSubsidyUSD); 
        
        PricebookEntry pbEntryUSD = DataFactory.newPricebookEntry(lStandardPricebook, productBRL, oppSubsidyUSD);
        Database.insert(pbEntryUSD);
        
        PricebookEntry pbEntry5USD = DataFactory.newPricebookEntry(pb5, productBRL, oppSubsidyUSD);
        pbEntry5USD.UnitPrice = 30;
        Database.insert(pbEntry5USD);  
        
        QuoteLineItem accessFeeSubsidyUSD = DataFactory.newQuoteLineItem(quoteSubsidyUSD, pbEntry5USD);
        accessFeeSubsidy.UnitPrice = 26;
        Database.insert(accessFeeSubsidyUSD);
		*/
    } 
    
    @isTest 
    public static void NoNeedApprovalIntermediation() {
        
        Opportunity opp = [SELECT Id, CurrencyIsoCode FROM Opportunity WHERE B_M__c = 'Intermediation' LIMIT 1];
        Quote quote = [SELECT Id,OpportunityId,Quote.Pricebook2Id,CurrencyIsoCode FROM Quote WHERE OpportunityId =: opp.Id LIMIT 1];
        QuoteLineItem accessFee = [SELECT Id,UnitPrice,List_Price__c,Product2Id FROM QuoteLineItem WHERE quote.Id =: quote.id LIMIT 1];
        Payment__c payment = DataFactory.newPayment(accessFee);
        Database.insert(payment);
        Assert_Data__c lastPrice = DataFactory.newAssertData(opp.id, 'Opportunity');
        lastPrice.Field_Name__c = 'UnitPrice';
        lastPrice.New_Value__c = '2';
        insert lastPrice;
        
        Test.startTest();   
        FastTrackRegionalManagerApprovalHandler.fastTrackRegionalManagerApproval(accessFee, payment, quote);
        System.assertEquals('No', quote.Copay__c);
        System.assertEquals(false, quote.Enterprise_Discount_Approval_Needed__c);
        System.assertEquals(true, quote.Enterprise_Subscription_Approved__c);      
        Test.stopTest();
        
    }
    
    @isTest 
    public static void NeedApprovalIntermediation() {
        
        Opportunity opp = [SELECT Id, CurrencyIsoCode FROM Opportunity WHERE B_M__c = 'Intermediation' LIMIT 1];
        Quote quote = [SELECT Id,OpportunityId,Quote.Pricebook2Id,CurrencyIsoCode FROM Quote WHERE OpportunityId =: opp.Id LIMIT 1];
        QuoteLineItem accessFee = [SELECT Id,UnitPrice,List_Price__c,Product2Id FROM QuoteLineItem WHERE quote.Id =: quote.id LIMIT 1];
        Payment__c payment = DataFactory.newPayment(accessFee);
        Database.insert(payment);
        accessFee.UnitPrice = 15;
       
		Comission_Price__c comissionPrice = new Comission_Price__c();
        comissionPrice.Comission_Percent__c = 0;
        comissionPrice.Price__c = 29.90;
        comissionPrice.Product__c = accessFee.Product2Id;
        comissionPrice.CurrencyIsoCode = quote.CurrencyIsoCode;
        insert comissionPrice;
        
        Assert_Data__c lastPrice = DataFactory.newAssertData(opp.id, 'Opportunity');
        lastPrice.Field_Name__c = 'UnitPrice';   
        lastPrice.New_Value__c = '100';
        insert lastPrice;
        
        Test.startTest();   
        FastTrackRegionalManagerApprovalHandler.fastTrackRegionalManagerApproval(accessFee, payment, quote);
        System.assertEquals('Yes', quote.Copay__c);
        System.assertEquals(true, quote.Enterprise_Discount_Approval_Needed__c);
        System.assertEquals(false, quote.Enterprise_Subscription_Approved__c);      
        Test.stopTest();
        
    }    
   
    @isTest 
    public static void NeedApprovalSubsidy() {
        
        Opportunity opp = [ SELECT Id, CurrencyIsoCode,Pricebook2Id,Reseller_del__c FROM Opportunity WHERE B_M__c = 'Total Subsidy' LIMIT 1];
        Quote quote = [SELECT Id,OpportunityId,Quote.Pricebook2Id,CurrencyIsoCode FROM Quote WHERE OpportunityId =: opp.Id LIMIT 1];
        QuoteLineItem accessFee = [SELECT Id,UnitPrice,List_Price__c,QuoteLineItem.Product2Id FROM QuoteLineItem WHERE quote.Id =: quote.id LIMIT 1];
        Payment__c payment = DataFactory.newPayment(accessFee);
        Database.insert(payment);
        
        //Legacy_Partner_Price__c legacyPartner = DataFactory.newLegacyPartnerPrice('teste', opp.CurrencyIsoCode, opp.Reseller_del__c, 'Subsidy');
        //legacyPartner.Legacy_Price__c = 2;
        //Database.insert(legacyPartner); 
        
        Test.startTest();       
        FastTrackRegionalManagerApprovalHandler.fastTrackRegionalManagerApproval(accessFee, payment, quote);
        System.assertEquals('Yes', quote.Copay__c);
        System.assertEquals(true, quote.Enterprise_Discount_Approval_Needed__c);
        System.assertEquals(false, quote.Enterprise_Subscription_Approved__c);      
        Test.stopTest();
        
    }
 
    @isTest 
    public static void NoNeedApprovalExclucisve() {
        
        Opportunity opp = [ SELECT Id, CurrencyIsoCode,AccountId,Reseller_del__c FROM Opportunity WHERE B_M__c = 'Exclusive' LIMIT 1]; 
        Quote quote = [SELECT Id,OpportunityId,Quote.Pricebook2Id,CurrencyIsoCode FROM Quote WHERE OpportunityId =: opp.Id LIMIT 1];
        QuoteLineItem accessFee = [SELECT Id,UnitPrice,List_Price__c,QuoteLineItem.Product2Id FROM QuoteLineItem WHERE quote.Id =: quote.id LIMIT 1];

        Payment__c payment = DataFactory.newPayment(accessFee);
        Database.insert(payment);
        
        Test.startTest();       
        FastTrackRegionalManagerApprovalHandler.fastTrackRegionalManagerApproval(accessFee, payment, quote);
        System.assertEquals('No', quote.Copay__c);
        System.assertEquals(false, quote.Enterprise_Discount_Approval_Needed__c);
        System.assertEquals(True, quote.Enterprise_Subscription_Approved__c);      
        Test.stopTest();
        
    }
    
    @isTest 
    public static void NeedApprovalSubsidyLegacy() {
        
        Opportunity opp = [SELECT Id, CurrencyIsoCode,Reseller_del__c FROM Opportunity WHERE B_M__c = 'Total Subsidy' LIMIT 1];
        Account acc = [SELECT Id,Indirect_Channel_Pricebook__c FROM Account WHERE id=: opp.Reseller_del__c LIMIT 1];
        acc.Indirect_Channel_Pricebook__c = 'Legacy Partners';
        update acc;
        Quote quote = [SELECT Id,OpportunityId,Quote.Pricebook2Id,CurrencyIsoCode FROM Quote WHERE OpportunityId =: opp.Id LIMIT 1];
        QuoteLineItem accessFee = [SELECT Id,UnitPrice,List_Price__c,Product2Id FROM QuoteLineItem WHERE quote.Id =: quote.id LIMIT 1];
        Payment__c payment = DataFactory.newPayment(accessFee);
        Database.insert(payment);
        accessFee.UnitPrice = 15;
       
		Comission_Price__c comissionPrice = new Comission_Price__c();
        comissionPrice.Comission_Percent__c = 0;
        comissionPrice.Price__c = 29.90;
        comissionPrice.Product__c = accessFee.Product2Id;
        comissionPrice.CurrencyIsoCode = quote.CurrencyIsoCode;
        insert comissionPrice;  
        
        
        Legacy_Partner_Price__c legacyPartner = DataFactory.newLegacyPartnerPrice('teste', opp.CurrencyIsoCode, acc.id, 'Subsidy');
        legacyPartner.Legacy_Price__c = 100;
        Database.insert(legacyPartner);
        
        Test.startTest();   
        FastTrackRegionalManagerApprovalHandler.fastTrackRegionalManagerApproval(accessFee, payment, quote);
        System.assertEquals('Yes', quote.Copay__c);
        System.assertEquals(true, quote.Enterprise_Discount_Approval_Needed__c);
        System.assertEquals(false, quote.Enterprise_Subscription_Approved__c);      
        Test.stopTest();
        
    } 
    
    @isTest 
    public static void NoNeedApprovalSubsidyLegacy() {
        
        Opportunity opp = [SELECT Id, CurrencyIsoCode,Reseller_del__c FROM Opportunity WHERE B_M__c = 'Total Subsidy' LIMIT 1];
        Account acc = [SELECT Id,Indirect_Channel_Pricebook__c FROM Account WHERE id=: opp.Reseller_del__c LIMIT 1];
        acc.Indirect_Channel_Pricebook__c = 'Legacy Partners';
        update acc;
        Quote quote = [SELECT Id,OpportunityId,Quote.Pricebook2Id,CurrencyIsoCode FROM Quote WHERE OpportunityId =: opp.Id LIMIT 1];
        QuoteLineItem accessFee = [SELECT Id,UnitPrice,List_Price__c,Product2Id FROM QuoteLineItem WHERE quote.Id =: quote.id LIMIT 1];
        Payment__c payment = DataFactory.newPayment(accessFee);
        Database.insert(payment);
        accessFee.UnitPrice = 15;
       
		Comission_Price__c comissionPrice = new Comission_Price__c();
        comissionPrice.Comission_Percent__c = 0;
        comissionPrice.Price__c = 29.90;
        comissionPrice.Product__c = accessFee.Product2Id;
        comissionPrice.CurrencyIsoCode = quote.CurrencyIsoCode;
        insert comissionPrice;  
        
        
        Legacy_Partner_Price__c legacyPartner = DataFactory.newLegacyPartnerPrice('teste', opp.CurrencyIsoCode, acc.id, 'Subsidy');
        legacyPartner.Legacy_Price__c = 2;
        Database.insert(legacyPartner);
        
        Test.startTest();   
        FastTrackRegionalManagerApprovalHandler.fastTrackRegionalManagerApproval(accessFee, payment, quote);
        System.assertEquals('No', quote.Copay__c);
        System.assertEquals(false, quote.Enterprise_Discount_Approval_Needed__c);
        System.assertEquals(true, quote.Enterprise_Subscription_Approved__c);      
        Test.stopTest();        
    } 
/*
    @isTest 
    public static void NeedApprovalSubsidyUSD() {
        
        Opportunity opp = [ SELECT Id, CurrencyIsoCode,Pricebook2Id,Reseller_del__c FROM Opportunity WHERE B_M__c = 'Total Subsidy' and CurrencyIsoCode = 'USD' LIMIT 1];
        system.debug('opp '+opp);
        Quote quote = [SELECT Id,OpportunityId,Quote.Pricebook2Id,CurrencyIsoCode FROM Quote WHERE OpportunityId =: opp.Id LIMIT 1];
        QuoteLineItem accessFee = [SELECT Id,UnitPrice,List_Price__c,QuoteLineItem.Product2Id FROM QuoteLineItem WHERE quote.Id =: quote.id LIMIT 1];
        Payment__c payment = DataFactory.newPayment(accessFee);
        Database.insert(payment);
   
        Test.startTest();       
        FastTrackRegionalManagerApprovalHandler.fastTrackRegionalManagerApproval(accessFee, payment, quote);
        System.assertEquals('Yes', quote.Copay__c);
        System.assertEquals(true, quote.Enterprise_Discount_Approval_Needed__c);
        System.assertEquals(false, quote.Enterprise_Subscription_Approved__c);      
        Test.stopTest();
        
    }  */  
}