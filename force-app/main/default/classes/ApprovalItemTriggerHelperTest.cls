@isTest(seeAllData=false)
public class ApprovalItemTriggerHelperTest {

    @TestSetup
    static void createData(){  

        Pricebook2 lStandardPricebook = DataFactory.newPricebook();
        Database.update( lStandardPricebook );
        
        Account lAcc = DataFactory.newAccount();              
        Database.insert( lAcc );

        Opportunity lNewBusinessOpp = DataFactory.newOpportunity( lAcc.Id, lStandardPricebook, 'Client_Sales_New_Business' );     
        Database.insert( lNewBusinessOpp );
        Datetime yesterday = Datetime.now().addDays(-10);
        Test.setCreatedDate(lNewBusinessOpp.Id, yesterday);

        Product2 lAcessFee = DataFactory.newProduct( 'Enterprise Subscription' , false , 'BRL' );

        PricebookEntry lAccessFeeEntry = DataFactory.newPricebookEntry( lStandardPricebook , lAcessFee , lNewBusinessOpp );

        Quote lQuote = DataFactory.newQuote( lNewBusinessOpp , 'Quote Test', 'Client_Sales_New_Business' );
        lQuote.ExpirationDate = System.today();
        Database.insert( lQuote );

        QuoteLineItem lQuoteAccessFee = DataFactory.newQuoteLineItem( lQuote , lAccessFeeEntry );
        lQuoteAccessFee.Fee_Contract_Type__c = 'Copay 2';

        Payment__c lBillingSetting = DataFactory.newPayment( lQuoteAccessFee );
        lBillingSetting.Frequency__c = 'Yearly';
        lBillingSetting.Billing_Day__c = 'Custom';
        Database.insert( lBillingSetting );

        Product2 lProfServicesOneFee = DataFactory.newProduct( 'Professional Services Setup Fee' , false , 'BRL' );
        Database.insert( lProfServicesOneFee );
        
        PricebookEntry lProfServicesOneFeeEntry = DataFactory.newPricebookEntry( lStandardPricebook , lProfServicesOneFee , lNewBusinessOpp );
        Database.insert( lProfServicesOneFeeEntry );

        QuoteLineItem lQuotePsOneFee = DataFactory.newQuoteLineItem( lQuote , lProfServicesOneFeeEntry );
        lQuotePsOneFee.Fee_Contract_Type__c = 'Copay 2';
        Database.insert( lQuotePsOneFee );

        Payment__c lBillingSettingPsOneFee = DataFactory.newPayment( lQuotePsOneFee );
        lBillingSettingPsOneFee.Frequency__c = 'Yearly';
        lBillingSettingPsOneFee.Billing_Day__c = 'Custom';
        Database.insert( lBillingSettingPsOneFee );

        Case lOperationalCase = DataFactory.newCase(lNewBusinessOpp, lQuote, 'Deal_Desk_Operational');
        lOperationalCase.Justificate_No_Compliant_Topics__c = 'Justification Test';
        lOperationalCase.Subject = 'Deal Desk Operational Approval Request';
        Database.insert(lOperationalCase);

        lOperationalCase.Status = 'New';
        Database.update(lOperationalCase);

        List<Approval_Item__c> opplApprovalItems = DataFactory.newApprovalItems(lOperationalCase);
        Database.insert(opplApprovalItems);

        Case lCommercialCase = DataFactory.newCase(lNewBusinessOpp, lQuote, 'Deal_Desk_Approval');
        lCommercialCase.Justificate_No_Compliant_Topics__c = 'Justification Test';
        lOperationalCase.Status = 'New';
        lCommercialCase.Subject = 'Deal Desk Operational Approval Request';
        Database.insert(lCommercialCase);

    }

    @isTest
    private static void testCommercialCase_ApproveAllItems(){

        List<Case> cases = [SELECT Id, Status, RecordType.DeveloperName FROM Case WHERE RecordType.DeveloperName = 'Deal_Desk_Approval' ];
        List<Approval_Item__c> commercialApprovalItems = DataFactory.newApprovalItems(cases.get(0));
        Database.insert(commercialApprovalItems);

        List<Approval_Item__c> approvalItemsOpp = [SELECT Id, Name, Value__c, Approval_Status__c, Comments__c, Case__c, Case__r.Status 
                                                   FROM Approval_Item__c 
                                                WHERE Case__c = :cases.get(0).Id ];

        Test.startTest();
        for( Integer i = 0; i < approvalItemsOpp.size(); i++){
            approvalItemsOpp.get(i).Approval_Status__c = 'Approved';
            approvalItemsOpp.get(i).Comments__c = 'Approved';
        }

        Database.update(approvalItemsOpp);
        Test.stopTest();
        cases = [SELECT Id, Status FROM Case WHERE Id = :cases.get(0).Id ];
        System.assertEquals( 'Approved' , cases.get(0).Status, 'Case is not Approved' );
    }


    @isTest
    private static void testCommercialCase_rejectAllItems(){

        List<Case> cases = [SELECT Id, Status, RecordType.DeveloperName FROM Case WHERE RecordType.DeveloperName = 'Deal_Desk_Approval' ];
        List<Approval_Item__c> commercialApprovalItems = DataFactory.newApprovalItems(cases.get(0));
        Database.insert(commercialApprovalItems);

        List<Approval_Item__c> approvalItemsOpp = [SELECT Id, Name, Value__c, Approval_Status__c, Comments__c, Case__c, Case__r.Status 
                                                   FROM Approval_Item__c 
                                                WHERE Case__c = :cases.get(0).Id ];

        Test.startTest();
        for( Integer i = 0; i < approvalItemsOpp.size(); i++){
            approvalItemsOpp.get(i).Approval_Status__c = 'Rejected';
            approvalItemsOpp.get(i).Comments__c = 'Rejected';
        }
        Database.update(approvalItemsOpp);
        Test.stopTest();

        cases = [SELECT Id, Status FROM Case WHERE Id = :cases.get(0).Id ];
        System.assertEquals( 'Rejected' , cases.get(0).Status, 'Case is not Rejected' );
    }
    

    @isTest
    private static void testOppCase_approveAllItems(){

        List<Case> cases = [SELECT Id, Status, RecordType.DeveloperName FROM Case WHERE RecordType.DeveloperName = 'Deal_Desk_Operational' ];

        List<Approval_Item__c> approvalItemsOpp = [SELECT Id, Name, Value__c, Approval_Status__c, Comments__c, Case__c, Case__r.Status 
                                                   FROM Approval_Item__c 
                                                WHERE case__r.RecordType.DeveloperName = 'Deal_Desk_Operational'];
        Test.startTest();                                  
        for( Integer i = 0; i < approvalItemsOpp.size(); i++){
            approvalItemsOpp.get(i).Approval_Status__c = 'Approved';
            approvalItemsOpp.get(i).Comments__c = 'Approved';
        }
        Database.update(approvalItemsOpp);
        Test.stopTest();

        cases = [SELECT Id, Status FROM Case WHERE RecordType.DeveloperName = 'Deal_Desk_Operational' ];
        System.assertEquals( 'Approved' , cases.get(0).Status, 'Case is not approved' );
    }
  
    @isTest
    private static void testOppCase_rejectOneItem(){

        List<Case> cases = [SELECT Id, Status, RecordType.DeveloperName FROM Case WHERE RecordType.DeveloperName = 'Deal_Desk_Operational' ];

        List<Approval_Item__c> approvalItemsOpp = [SELECT Id, Name, Value__c, Approval_Status__c, Comments__c, Case__c, Case__r.Status FROM Approval_Item__c WHERE case__r.RecordType.DeveloperName = 'Deal_Desk_Operational'];
       
        Test.startTest();
        approvalItemsOpp.get(0).Approval_Status__c = 'Rejected';
        approvalItemsOpp.get(0).Comments__c = 'Rejected';
       
        ApprovalItemTriggerHelper helper = new ApprovalItemTriggerHelper();
        helper.checkAllItemsAreApprovedOrRejected(approvalItemsOpp);
        Test.stopTest();

        cases = [SELECT Id, Status FROM Case WHERE RecordType.DeveloperName = 'Deal_Desk_Operational' ];
        System.assertEquals( 'New' , cases.get(0).Status, 'Case is not new' );
    }

    @isTest
    private static void testOppCase_approveOneItem(){

        List<Approval_Item__c> approvalItemsOpp = [SELECT Id, Name, Value__c, Approval_Status__c, Comments__c, Case__c, Case__r.Status FROM Approval_Item__c WHERE case__r.RecordType.DeveloperName = 'Deal_Desk_Operational'];
        approvalItemsOpp.get(0).Approval_Status__c = 'Approved';
        approvalItemsOpp.get(0).Comments__c = 'Approved';
        
        Test.startTest();
        ApprovalItemTriggerHelper helper = new ApprovalItemTriggerHelper();
        helper.checkAllItemsAreApprovedOrRejected(approvalItemsOpp);
        Test.stopTest();

        List<Case> cases = [SELECT Id, Status FROM Case WHERE RecordType.DeveloperName = 'Deal_Desk_Operational' ];
        System.assertEquals( 'New' , cases.get(0).Status, 'Case is not new' );
    }

}