@IsTest
public class LeadMock {

    public static Lead getSMBLead() {
        return new Lead(
            RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('SMB Direct Channel').getRecordTypeId(),
            LastName = 'test name',
            Company = 'test company',
            Country = 'Portugal',
            Email = 'test@test.com'            
        );
    }

}