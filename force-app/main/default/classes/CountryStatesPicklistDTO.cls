public without sharing class CountryStatesPicklistDTO {
    public String countryLabel;
    public String countryCode;
    public List<StateDTO> states;
    
    public class StateDTO {
        public List<String> cities;
        public String label;
        public String code;
    }
  
}