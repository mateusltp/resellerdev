/**
* @File Name          : OpportunityTriggerValOppRelHelper.cls
* @Description        : 
* @Author             : MLNC@GFT.com
* @Group              : 
* @Last Modified By   : gft.samuel.silva@ext.gympass.com
* @Last Modified On   : 06-22-2022
* @Modification Log   : 
* Ver       Date            Author      		    Modification
* 1.0    17/04/2020   MLNC@GFT.com     Initial Version
* 2.0    19/05/2020   MLNC@GFT.com     Initial Version
**/
public class OpportunityTriggerValOppRelHelper {
    
    // Id recordTypeIdSmall = 
    // Id recordTypeIdWishList = ;
    // Id recordTypeIdSmall = ;
    // Id recordTypeIdWishList = ;
    Set<Id> recordTypeIds = new Set<Id>{Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Gyms_Small_Medium_Partner').getRecordTypeId(),
                                        Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Gyms_Wishlist_Partner').getRecordTypeId(),
                                        Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Partner_Small_and_Medium_Renegotiation').getRecordTypeId(),
                                        Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Partner_Wishlist_Renegotiation').getRecordTypeId()
                                        };
    Set<Id> recordTypeSuccessId = new Set<Id>{Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Partner_Small_and_Medium_Renegotiation').getRecordTypeId(),
                                        Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Partner_Wishlist_Renegotiation').getRecordTypeId()
                                        };
    Id recordTypeIdQuote = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Gyms_Quote_Partner').getRecordTypeId();
    Id recordTypeIdAccBank = Schema.SObjectType.Acount_Bank_Account_Relationship__c.getRecordTypeInfosByDeveloperName().get('Gyms_Account_Bank_Account_Partner').getRecordTypeId();
    Id recordTypeIdBankFirst = Schema.SObjectType.Bank_Account__c.getRecordTypeInfosByDeveloperName().get('Gyms_Bank_Account').getRecordTypeId();
    Id recordTypeIdBankSecond = Schema.SObjectType.Bank_Account__c.getRecordTypeInfosByDeveloperName().get('Gyms_Manage_Bank_Account').getRecordTypeId();
    Id recordTypeIdConBank = Schema.SObjectType.Contact_Bank_Account_Relationship__c.getRecordTypeInfosByDeveloperName().get('Gyms_Contact_Bank_Account_Partner').getRecordTypeId();
    Id recordTypeIdContact = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Gyms_Partner').getRecordTypeId();
    Id recordTypeIdProduct = Schema.SObjectType.Product_Item__c.getRecordTypeInfosByDeveloperName().get('Gyms_Product_Item_Partner').getRecordTypeId();
    Id recordTypeIdFile = Schema.SObjectType.FIleObject__c.getRecordTypeInfosByDeveloperName().get('Partner').getRecordTypeId();
    Id recordTypeIdOpeningHours = Schema.SObjectType.Opening_Hours__c.getRecordTypeInfosByDeveloperName().get('Partner').getRecordTypeId();
    Id recordTypeIdOppNewFlow = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Opportunity').getRecordTypeId();
    Id idopp;
    Id idaccOpp;
    Id idcontact;
    Id idtypeOfContact;
    Id idtypeLegalRepresentative;
    Set<ID> idLogos = new Set<ID>();
    List<String> lstLogo = new List<String>();
    
    Boolean hasDecisionMaker = false;
    List<Opportunity> lstNew = trigger.new;
    
    public void updateOppWithRelated(){
        List<Acount_Bank_Account_Relationship__c> accbankLst = new List<Acount_Bank_Account_Relationship__c>();
        List<Product_Item__c> productLst = new List<Product_Item__c>();
        
        
        List<Quote> quoteLst = new List<Quote>();
        List<Contact> contactLst = new List<Contact>();
        List<FIleObject__c> fileLst = new List<FIleObject__c>();
        List<FIleObject__c> fileLstLogo = new List<FIleObject__c>();
        List<Opportunity> oppLst = new List<Opportunity>(); 
        List<Opportunity> oppbankAccLst = new List<Opportunity>();
        List<Ops_Setup_Validation_Form__c> opsFormLst = new List<Ops_Setup_Validation_Form__c>();
        List<Opening_Hours__c> openingHoursLst = new List<Opening_Hours__c>();
        
        
        for(Opportunity opp : lstNew){  
            if (!recordTypeIds.contains(opp.RecordTypeId) ){
                return;
            }
            if (recordTypeIds.contains(opp.RecordTypeId)  && ( opp.AccountId != null ) ){                        
                if(opp.StageName != 'Perdido' || opp.StageName != 'Lançado/Ganho' ){         
                    idaccOpp=opp.AccountId;
                    idopp = opp.Id;
                    
                }
                oppLst.add(opp);
                oppbankAccLst.add(opp);    
                
            }
            
        }
        
        contactLst = [SELECT Id,Phone, Email, Name, Type_of_contact__c FROM Contact WHERE RecordTypeId =: recordTypeIdContact AND AccountId =: idaccOpp];
        for (Contact c :  contactLst)
        {	
            if(c.Type_of_contact__c != null){                
                set<String> setTypeOfContact = new set<String>();
                setTypeOfContact.addAll(c.Type_of_contact__c.split(';'));
                if (setTypeOfContact.contains('Point of Contact')){
                    idtypeOfContact = c.id;
                }
                if (setTypeOfContact.contains('Legal Representative')){
                    idtypeLegalRepresentative = c.id;
                }
            }
        }    
        
        fileLst = [SELECT Id, Name FROM FIleObject__c WHERE RecordTypeId =: recordTypeIdFile AND Account__c != null  AND Account__c =: idaccOpp ];
        
        String searchKey = '%' + 'logo' + '%';
        fileLstLogo = [SELECT Id, Name FROM FIleObject__c WHERE Id IN : fileLst AND (Name LIKE: searchKey OR File_Type__c = 'Logo')];
        system.debug('fileLstLogo >>> '+fileLstLogo);

        openingHoursLst = [SELECT Id, Name FROM Opening_Hours__c WHERE RecordTypeId =: recordTypeIdOpeningHours AND Account_Related__c != null AND Account_Related__c =: idaccOpp];
        
        accbankLst = [SELECT Id,Bank_Account__c,Account__c FROM Acount_Bank_Account_Relationship__c WHERE RecordTypeId =: recordTypeIdAccBank AND Account__c != null  AND Account__c =: idaccOpp ];
        productLst = [SELECT Id, Gym_Activity_Concat__c FROM Product_Item__c WHERE RecordTypeId =: recordTypeIdProduct AND Opportunity__c != null AND Opportunity__c =: idopp ];
        
        quoteLst = [ SELECT Id, PR_Activity__c,Start_Date__c,Final_Date__c, Priority_Tier__c,Signed__c,Description FROM Quote WHERE RecordTypeId =: recordTypeIdQuote AND OpportunityId != null AND OpportunityId =: idopp ];
        opsFormLst = [SELECT Id, Opportunity__c, Status__c FROM Ops_Setup_Validation_Form__c WHERE Opportunity__c IN: oppLst];
        
        for ( Opportunity opp: oppLst  )
        {     
            validadeContactFields(opp, contactLst);
            validatePhotosAndLogo(opp, fileLst, fileLstLogo);
            
            if( (accbankLst.size() > 0  ) ){  
                opp.Bank_Account_Related__c = true;
            } else {
                opp.Bank_Account_Related__c = false;
            }
            
            
            if(productLst.size() > 0){  
                opp.Product_Related__c = true;               
            } else {
                opp.Product_Related__c = false;
            }

            for(Product_Item__c objProd : productLst){
                if(objProd.Gym_Activity_Concat__c != null){
                    opp.Activity_Related__c = true; 
                }
                else {
                    opp.Activity_Related__c = false;
                }
            }

            if(openingHoursLst.size() > 0){  
                opp.OpeningHours_Related__c = true;               
            } else {
                opp.OpeningHours_Related__c = false;
            }
            
            if(quoteLst.size() > 0){  
                opp.Proposal_Related__c = true;
                validateProposalFields(opp, quoteLst);
            } else {
                opp.Proposal_Related__c = false;
            }
            
            if( idtypeOfContact != null ){  
                opp.Contact__c = idtypeOfContact ;
            } else {
                opp.Contact__c = null;
            }
            if( idtypeLegalRepresentative != null ){  
                opp.Legal_representative__c = idtypeLegalRepresentative ;
            } else {
                opp.Legal_representative__c = null ;
            }
            
            if(opp.StageName == 'Lançado/Ganho' && !recordTypeSuccessId.contains(opp.RecordTypeId)) {
                validateOpsForm(opp, opsFormLst);
            }
        }
        
        if( (oppbankAccLst.size() > 0  && accbankLst.size() > 0  ) ){  
            List<Bank_Account__c> lst = new List<Bank_Account__c> ();
            for ( Opportunity oppbankacc: oppbankAccLst  )
            {  
                if(oppbankacc.StageName == 'Contrato Assinado' || oppbankacc.StageName == 'Set Up Validation'  || oppbankacc.StageName == 'Lançado/Ganho' || oppbankacc.StageName == 'Perdido')
                {
                    lst.addAll(changeBankAccount(oppbankacc));
                }
            }
            
            if(!lst.isempty()){
                update lst;
            }
            
        } 
        
    }
    
    public List<Bank_Account__c>  changeBankAccount(Opportunity oppbankacc){
        List<Bank_Account__c> newbaList = new List<Bank_Account__c> ();
        List<Acount_Bank_Account_Relationship__c> relba = new List<Acount_Bank_Account_Relationship__c>();      
        List<Acount_Bank_Account_Relationship__c> bankaccountLst  = [   SELECT Id,Bank_Account__c, RecordTypeId , Account__c 
                                                                        FROM Acount_Bank_Account_Relationship__c 
                                                                        WHERE RecordTypeId =: recordTypeIdAccBank AND Account__c != null AND Account__c =: oppbankacc.AccountId  limit 1];
        if( !bankaccountLst.isEmpty() ){
            Id idbankaccount = bankaccountLst.get(0).Bank_Account__c;
            List<Bank_Account__c> baList = new List<Bank_Account__c> ();          
            if (idbankaccount != null) {
                baList = [SELECT Id, RecordTypeId FROM Bank_Account__c WHERE id=: idbankaccount AND RecordTypeId =: recordTypeIdBankFirst ];   
                if ( !baList.isEmpty() ){ 
                    for ( Bank_Account__c  b: baList   ){
                        b.RecordTypeId = recordTypeIdBankSecond ;
                        newbaList.add(b);                 
                    }
                }
            }
        }   
        return newbaList ;
        
        
    }
    
    public void validateOpsForm(Opportunity opp, List<Ops_Setup_Validation_Form__c> opsFormLst){  
        
        if(opsFormLst.isEmpty()){
            (new IntegrityError()).doAction('Set-Up Validation Stage is mandatory.');
        }
        
        for(Ops_Setup_Validation_Form__c form : opsFormLst){
            if(form.Opportunity__c == opp.Id && form.Status__c != 'Approved'){
                (new IntegrityError()).doAction('Please, complete Set-Up Validation Approval before proceeding.');
            }
        }
    }
    
    
    
    public void validateProposalFields(Opportunity opp, List<Quote> proposals){
        
        for(Quote p : proposals){         
            if(opp.StageName == 'Review Contract' && p.PR_Activity__c == null) {
                (new IntegrityError()).doAction('Please, enter PR Activity in Proposal(s).');
            }            
        }  
    }
    
    public void validadeContactFields(Opportunity opp, List<Contact> contactLst){  
        
        if(contactLst.isEmpty() && opp.StageName == 'Proposta Enviada') {
            (new IntegrityError()).doAction('Please, return to Account and create at least one contact with Name, Phone and Email');
        } else if(opp.StageName == 'Proposta Enviada'){
            for(Contact c : contactLst){
                if(c.phone == null || c.email == null){
                    (new IntegrityError()).doAction('Please, return to Account and enter Contact(s) Name, Phone and Email before proceeding.');
                }             
            }
        }
    }
    
    public void validatePhotosAndLogo(Opportunity opp, List<FIleObject__c> fileLst, List<FIleObject__c> fileLstLogo){
        
        if ( fileLst.size() < 6 && opp.StageName == 'Set Up Validation'  ) {
            (new IntegrityError()).doAction('Please, return to Account and upload at least five photos and only one logo');
        } else if(fileLstLogo.size() == 0 && opp.StageName == 'Set Up Validation'){
            (new IntegrityError()).doAction('Please, return to Account and upload only one logo');
        }else if(fileLstLogo.size() > 1 && opp.StageName == 'Set Up Validation'){
            (new IntegrityError()).doAction('You have more than one logo! Please, return to Account and upload at least five photos and only one logo');
        }
    }
    
    public void validateStepsTowardsSuccessForPartners(){     
        List<Opportunity> lstNew = trigger.new;
        List<Opportunity> oppLst = new List<Opportunity>();
        for(Opportunity opp : lstNew){  
            if (!recordTypeIds.contains(opp.RecordTypeId) ){
                return;
            }
            if(recordTypeIds.contains(opp.RecordTypeId) && (opp.StageName == 'Proposta Aprovada') && (opp.Country_Manager_Approval__c == false)){         
                oppLst.add(opp);            
            }
        }
        
        if(!oppLst.isEmpty()) {   
            ApprovalProcessHandler approval = new ApprovalProcessHandler();         
            for(Opportunity opp : oppLst) {
                Boolean isValidSteps = approval.validateStepsTowardsSuccessApproval(opp);
                System.debug('IsValidSteps ' + isValidSteps);
                if(!isValidSteps)
                    opp.Approval_Needed__c = 'Yes';
                else 
                    opp.Approval_Needed__c = 'No';
            }           
        }
    }
    
    public void validateEnablersNewFlow(Map<Id, Opportunity> newMap, Map<Id, Opportunity> oldMap) {
        AccountSelector accSelector = (AccountSelector)Application.Selector.newInstance(Account.sObjectType);
        StepTowardsSuccessSelector stepSelector = (StepTowardsSuccessSelector)Application.Selector.newInstance(Step_Towards_Success_Partner__c.sObjectType);
        ApprovalProcessHandler approval = new ApprovalProcessHandler();

        Set<Id> accIdSet = new Set<Id>();
        Set<Id> oppIdSet = new Set<Id>();

        for (Opportunity newOpp : newMap.values()) {
            
            if (newOpp.RecordTypeId == recordTypeIdOppNewFlow && newOpp.StageName == 'Review Opportunity') {
                accIdSet.add(newOpp.AccountId);
                oppIdSet.add(newOpp.Id);
            }            
        }

        if (oppIdSet.size() > 0) {
            Map<Id, Account> idToAccount = new Map<Id, Account>(accSelector.selectByIdForPartnersWithoutContactRels(accIdSet));
            List<Step_Towards_Success_Partner__c> steps = stepSelector.oppPartnerStepByOppId(oppIdSet);
            Map<Id, List<Step_Towards_Success_Partner__c>> oppIdToSteps = getMapOfSteps(steps);

            for (Id oppId : oppIdSet) {
                Opportunity opp = newMap.get(oppId);
                Account acc = idToAccount.get(opp.AccountId);
                List<Step_Towards_Success_Partner__c> stepsFromOpp = oppIdToSteps.get(opp.Id);
                
                if (stepsFromOpp != null) {
                    Boolean enablersValid = approval.isEnablersApprovalNewFlowsValid(acc, stepsFromOpp);
                    
                    opp.Achieved_Score__c = getAchievedStepScore(stepsFromOpp);

                    if(!enablersValid) {
                        opp.Approval_Needed__c = 'Yes';
                    } else {
                        opp.Approval_Needed__c = 'No';
                    }
                }
            }
        }
    }

    private Integer getAchievedStepScore(List<Step_Towards_Success_Partner__c> stepsFromOpp) {
        Integer stepScore = 0;

        for (Step_Towards_Success_Partner__c step : stepsFromOpp) {
            if (step.Achieved_New_Flow__c) {
                stepScore++;
            }
        }

        return stepScore;
    }

    private Map<Id, List<Step_Towards_Success_Partner__c>> getMapOfSteps(List<Step_Towards_Success_Partner__c> steps) {
        Map<Id, List<Step_Towards_Success_Partner__c>> oppIdToSteps = new Map<Id, List<Step_Towards_Success_Partner__c>>();

        for (Step_Towards_Success_Partner__c step : steps) {
            if (oppIdToSteps.containsKey(step.Related_Opportunity__c)) {
                oppIdToSteps.get(step.Related_Opportunity__c).add(step);
            } else {
                oppIdToSteps.put(step.Related_Opportunity__c, new List<Step_Towards_Success_Partner__c>{step});
            }
        }

        return oppIdToSteps;
    }

    public class IntegrityError {
        public IntegrityError() {}
        
        public void doAction(String msg) {
            // Trigger.new is valid here
            SObject[] sobjects = Trigger.new;
            for (Sobject sobj : sobjects) {
                sobj.addError(msg);
            }
        }
    }
    
}