/**
 * @description       : 
 * @author            : ext.gft.marcus.silva@gympass.com
 * @group             : 
 * @last modified on  : 05-17-2022
 * @last modified by  : ext.gft.marcus.silva@gympass.com
**/
@istest
public class DocumentDeviationFormCtrlTest {
    @TestSetup
    static void setup(){
        
        Pricebook2 standardPricebook = DataFactory.newPricebook();
        Database.update( standardPricebook );
        
        Account acc = DataFactory.newAccount();
        Database.insert(acc);
        
        Opportunity opp = DataFactory.newOpportunity( acc.Id, standardPricebook, 'Client_Sales_New_Business',true );  
        
        opp.fasttrackstage__c='Qualification';                
        Database.insert(opp);        
        
        Quote qt = DataFactory.newQuote(opp, '1', 'Gympass_Plus');
        qt.Employee_Corporate_Email__c = false;
        qt.end_date__c = Date.today();
        qt.contact_permission__c = 'Sign-up only';
        qt.ExpirationDate = date.valueOf('2021-08-16');
        
        qt.autonomous_marketplace_contract__c = 'Yes';
        qt.Discount_Approval_Level__c = 4;
        qt.Free_Trial_Days__c = 7;                
        Database.insert(qt);
        
        opp.Name = 'New Business';
        opp.SyncedQuoteId = qt.Id;
        opp.StageName = 'Lançado/Ganho';
        opp.FastTrackStage__c = 'Offer Approved';                
        Database.update(opp);      
        
        Document__c document = new Document__c();
        document.Opportunity__c = opp.Id;
        document.Proposal__c = qt.Id;
        insert document;

        Case caseContractReview = new Case();
        caseContractReview.RecordTypeId =  Schema.SObjectType.Case.getRecordTypeInfosByName().get('Contract Review').getRecordTypeId();
        caseContractReview.Document__c = document.Id;
        caseContractReview.AccountId = acc.Id;
        caseContractReview.OpportunityId__c = opp.Id;
        caseContractReview.QuoteId__c = qt.Id;

        insert caseContractReview;
    }  

    @isTest
    public static void createDeviation() {                
        Case legalCase = [SELECT Id FROM Case];

        List<DocumentDeviationFormController.DocumentDeviationFormTO> documentDeviationFormTO = new List<DocumentDeviationFormController.DocumentDeviationFormTO>();

        documentDeviationFormTO = DocumentDeviationFormController.getDocumentDeviation( legalCase.Id  );        

        String anotherDetail = DocumentDeviationFormController.getAnyOtherDetailField( legalCase.Id );
        System.AssertEquals('', anotherDetail);        

        String businessUnit = DocumentDeviationFormController.getBusinessUnit( legalCase.Id );
        System.AssertEquals('B2B', businessUnit);

        DocumentDeviationFormController.saveDocumentDeviationForm( JSON.serialize(documentDeviationFormTO), legalCase.Id, anotherDetail );

        System.AssertEquals( 1, [SELECT Id FROM DocumentDeviation__c].size() );
        
    }
}