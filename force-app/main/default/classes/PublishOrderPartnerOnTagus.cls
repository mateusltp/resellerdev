/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 10-13-2021
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/
public with sharing class PublishOrderPartnerOnTagus implements Schedulable {
    private List<Order> newOrders;
    private Map<Id, Order> oldOrderMap;
  
    public void execute() {
      this.newOrders = (List<Order>) Trigger.new;
      this.oldOrderMap = (Map<Id, Order>) Trigger.oldMap; 
      List<Account> partners = new List<Account>();
  
      for(Order order : newOrders) {
        if ( !order.Is_Partner_On_Tagus__c && Order.Type.equals('Partner')) {    
            partners.add(new Account(Id = order.AccountId));
        }     
      }
      
  
      if ( partners.isEmpty() ) {
        publishOrder();
      } else {     
        new TagusAccountPartnerOutboundPublisher(partners).runCreate();    
       // publishOrder();  
       	new ScheduleHelper().scheduleIntoNextSeconds(this, 2 );
      }
  
    }
  
    //coment
     public void execute(SchedulableContext sc) {
       publishOrder();
     } 
  
    private void publishOrder() {
      List<Order> ordersToCreateOnTagus = new List<Order>();
      List<Order> ordersToUpdateOnTagus = new List<Order>();   
      for(Order order : this.newOrders) {     
          if (isToPublishOrder(order)) {
            if (String.isBlank(order.UUID__c)) {        
                ordersToCreateOnTagus.add(order);               
            } else {
                ordersToUpdateOnTagus.add(order); 
            }
        } 
            
      }
  
      if (!ordersToCreateOnTagus.isEmpty()) {
        new TagusOrderPartnerOutboundPublisher(ordersToCreateOnTagus).runCreate();
      }
  
      if (!ordersToUpdateOnTagus.isEmpty()) {
        new TagusOrderPartnerOutboundPublisher(ordersToUpdateOnTagus).runUpdate();
      }
  
    }
  
    private Boolean isToPublishOrder(Order newOrder) {           
      return newOrder.Type.equals('Partner');
    }
}