/**
 * @File Name          : ApprovalProcessHandlerTest.cls
 * @Description        : 
 * @Author             : GEPI@GFT.com
 * @Group              : 
 * @Last Modified By   : alysson.mota@gympass.com
 * @Last Modified On   : 04-08-2022
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    18/05/2020   Samuel Silva - GFT (slml@gft.com)     Initial Version
**/
@isTest
public with sharing class ApprovalProcessHandlerTest {
   
    
    @TestSetup
    static void setUpData(){      
        Id rtId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gym_Partner').getRecordTypeId();
       
        Account parentAcc = new Account();
        parentAcc.name='ParentAccTest';
        parentAcc.RecordTypeId = rtId;
        parentAcc.GP_Status__c = 'Active';        
        parentAcc.ShippingState = 'São Paulo';
        parentAcc.CAP_Value__c = 120;
        parentAcc.ShippingCity = 'VVVVVV';
        parentAcc.ShippingStreet = 'Rua sssssssssssxxxx';
        parentAcc.ShippingCountry = 'Brazil';
        parentAcc.Gym_Type__c = 'Studios';
        parentAcc.Types_of_ownership__c = Label.franchisePicklist;
        parentAcc.Subscription_Type__c = 'Value per class';
        parentAcc.Subscription_Period__c = 'Monthy value';
        parentAcc.Subscription_Type_Estimated_Price__c    = 100;
        parentAcc.Has_market_cannibalization__c = 'No';
        parentAcc.Exclusivity__c = 'Yes';
        parentAcc.Exclusivity_End_Date__c = Date.today().addYears(1);
        parentAcc.Exclusivity_Partnership__c = 'Full Exclusivity';
        parentAcc.Exclusivity_Restrictions__c= 'No';
        parentAcc.Website = 'testing@tesapex.com';
        parentAcc.Gym_Email__c = 'gymemail@apex.com';
        parentAcc.Phone = '3222123123';
        parentAcc.Creation_Reason_Details__c = 'TESTING';
        parentAcc.Wishlist__c = true;
        parentAcc.Tier__c = 'T1';

        INSERT parentAcc;
       
        Account acc = new Account();
        acc.name='AcademiaBrasilCompanyPai';
        acc.RecordTypeId = rtId;
        acc.GP_Status__c = 'Pending Approval';        
        acc.ShippingState = 'Minas Gerais';
        acc.CAP_Value__c = 120;
        acc.ShippingCity = 'CityAcademiaBrasil';
        acc.ShippingStreet = 'Rua academiabrasilpai';
        acc.ShippingCountry = 'Brazil';
        acc.Gym_Type__c = 'Studios';
        acc.Types_of_ownership__c = Label.franchisePicklist;
        acc.Subscription_Type__c = 'Value per class';
        acc.Subscription_Period__c = 'Monthy value';
        acc.Subscription_Type_Estimated_Price__c    = 100;
        acc.Has_market_cannibalization__c = 'No';
        acc.Exclusivity__c = 'Yes';
        acc.Exclusivity_End_Date__c = Date.today().addYears(1);
        acc.Exclusivity_Partnership__c = 'Full Exclusivity';
        acc.Exclusivity_Restrictions__c= 'No';
        acc.Website = 'testing@tesapex.com';
        acc.Gym_Email__c = 'gymemail@apex.com';
        acc.Phone = '3222123123';
        acc.Creation_Reason_Details__c = 'TESTING';
        acc.Wishlist__c = false;
        acc.Tier__c = 'T4';
       
        Account wlAcc = new Account();
        wlAcc.name='wlAccBrasil';
        wlAcc.ParentId = parentAcc.Id;
        wlAcc.RecordTypeId = rtId;
        wlAcc.GP_Status__c = 'Pending Approval';        
        wlAcc.ShippingState = 'Rio de Janeiro';
        wlAcc.CAP_Value__c = 120;
        wlAcc.Wishlist__c = true;
        wlAcc.Wishlist_type__c = 'Retention';
        wlAcc.Wishlist_year__c = '2018';       
        wlAcc.ShippingCity = 'wlAccBrasil';
        wlAcc.ShippingStreet = 'Rua do apex vamos la';
        wlAcc.ShippingCountry = 'Brazil';
        wlAcc.Gym_Type__c = 'Studios';
        wlAcc.Types_of_ownership__c = Label.franchisePicklist;
        wlAcc.Subscription_Type__c = 'Value per class';
        wlAcc.Subscription_Period__c = 'Monthy value';
        wlAcc.Subscription_Type_Estimated_Price__c    = 100;
        wlAcc.Has_market_cannibalization__c = 'No';
        wlAcc.Exclusivity__c = 'Yes';
        wlAcc.Exclusivity_End_Date__c = Date.today().addYears(1);
        wlAcc.Exclusivity_Partnership__c = 'Full Exclusivity';
        wlAcc.Exclusivity_Restrictions__c= 'No';
        wlAcc.Website = '567@tesapex.com';
        wlAcc.Gym_Email__c = '3322saaaa@apex.com';
        wlAcc.Phone = '33345677';
        wlAcc.Creation_Reason_Details__c = 'TESTING';
        wlAcc.Tier__c = 'T1';
        wlAcc.Wishlist__c = true;

        List<Account> accLst = new List<Account>();
        accLst.add(acc);
        accLst.add(wlAcc);

        INSERT accLst;
    
        Id oppRtId =   Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Gyms_Small_Medium_Partner').getRecordTypeId();
        Opportunity accOpp = new Opportunity();
        accOpp.recordTypeId = oppRtId;
        accOpp.AccountId = acc.id;
        accOpp.Name = 'academiaBrasilCompanyOpp'; 
        accOpp.CMS_Used__c = 'Yes';     
        accOpp.Should_OPS_team_send_the_training__c = 'Standard Training';
        accOpp.Gym_agreed_to_an_API_integration__c	= 'Yes';
        accOpp.Club_Management_System__c = 'ABC Financial';
        accOpp.Integration_Fee_Deduction__c = 'No';
        accOpp.CloseDate = Date.today();
        accOpp.Success_Look_Like__c = 'Yes';
        accOpp.Success_Look_Like_Description__c	= 'Money money';
        accOpp.StageName = 'Qualificação';
        accOpp.Type	= 'Expansion';     
        accOpp.CurrencyIsoCode = 'BRL';
        accOpp.Gympass_Plus__c = 'Yes';

        Id oppWlsRt =   Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Gyms_Wishlist_Partner').getRecordTypeId();
        
        Opportunity wlOpp = new Opportunity();
        wlOpp.recordTypeId = oppWlsRt;
        wlOpp.AccountId = wlAcc.Id;
        wlOpp.Name = 'wlOppBR'; 
        wlOpp.Should_OPS_team_send_the_training__c = 'Standard Training';
        wlOpp.CMS_Used__c = 'Yes';     
        wlOpp.Gym_agreed_to_an_API_integration__c	= 'Yes';
        wlOpp.Club_Management_System__c = 'Angular';
        wlOpp.Integration_Fee_Deduction__c = 'No';
        wlOpp.CloseDate = Date.today();
        wlOpp.Success_Look_Like__c = 'Yes';
        wlOpp.StageName = 'Qualificação';
        wlOpp.Type	= 'Expansion';     
        wlOpp.CurrencyIsoCode = 'BRL';
        wlOpp.Gympass_Plus__c = 'Yes';
        List<Opportunity> oppLst = new List<Opportunity>();
        oppLst.add(accOpp);
        oppLst.add(wlOpp);

        INSERT oppLst;
        
        Map<Decimal, Step_Towards_Success_Definition__mdt> stepNumberToStepTowardsSuccessDefinition = new Map<Decimal, Step_Towards_Success_Definition__mdt>(); 
       
        List<Step_Towards_Success_Definition__mdt> listStepsTowardsSuccessDefinitions = [
            SELECT DeveloperName , MasterLabel, Category__c, Description__c, Field_API_Name__c, Field_Expected_Value__c, How_are_we_going_to_track_it__c, Object_API_Name__c, Step__c
            FROM Step_Towards_Success_Definition__mdt
            WHERE Category__c = '10 Steps'
            ORDER BY Step__c ASC
        ];
        
        for (Step_Towards_Success_Definition__mdt step : listStepsTowardsSuccessDefinitions) {
            stepNumberToStepTowardsSuccessDefinition.put(Integer.valueOf(step.Step__c), step);
        } 

        List<Step_Towards_Success_Partner__c> stepsToInsert = new List<Step_Towards_Success_Partner__c>();
        Id opportunityStepRecordTypeId = Schema.SObjectType.Step_Towards_Success_Partner__c.getRecordTypeInfosByName().get('Gym Partner - Opportunity Step').getRecordTypeId();
        for (Integer i=1; i<=10; i++) {
            
			Step_Towards_Success_Partner__c step = new Step_Towards_Success_Partner__c();
            step.Step_Number__c = i;
            step.Related_Opportunity__c = accOpp.Id;
            step.Related_Account__c = acc.Id;
            step.Name = stepNumberToStepTowardsSuccessDefinition.get(i).MasterLabel;
            step.RecordTypeId = opportunityStepRecordTypeId;
            step.Achieved__c = 'No';
            stepsToInsert.add(step);
            
            Step_Towards_Success_Partner__c step2 = new Step_Towards_Success_Partner__c();
            step2.Step_Number__c = i;
            step2.Related_Opportunity__c = wlOpp.Id;
            step2.Related_Account__c = wlAcc.Id;
            step2.Name = stepNumberToStepTowardsSuccessDefinition.get(i).MasterLabel;
            step2.RecordTypeId = opportunityStepRecordTypeId;
            step2.Achieved__c = 'No';            
            stepsToInsert.add(step2); 
            
        }        
        
        INSERT stepsToInsert;


        Profile prof = [SELECT ID from Profile WHERE Name = 'Gyms for Partner OPS'];

        User us1 = new User();
        us1.ProfileId = prof.Id;
        us1.FirstName = 'Tester';
        us1.LastName = 'Tester';
        us1.username = 'testergftgympass@test.com';
        us1.email= 'testergftgympass@test.com';
        us1.TimeZoneSidKey = 'GMT';
        us1.LanguageLocaleKey = 'en_US';
        us1.LocaleSidKey  = 'en_US';
        us1.EmailEncodingKey = 'ISO-8859-1';
        us1.Alias = 'tsts';
        INSERT us1;

        setupDataForNewFlow();
    }

    private static void setupDataForNewFlow() {
        Account accParent = getAccountInstance('Parent Account', null);
        insert accParent;
        
        Contact legalRepContact = getContact(accParent);
        insert legalRepContact;

        assignAccountLeglRep(accParent, legalRepContact);
        update accParent;
        
        Account accChild = getAccountInstance('Child Account', accParent.Id);
        insert accChild;

        Opportunity opp = getOpportunityInstance(accParent.Id);
        insert opp;

        List<Step_Towards_Success_Partner__c> steps = getEnablersForOpp(opp);
        insert steps;

        Event ev = getEvent(opp.Id, legalRepContact.Id);
        insert ev;

        List<Account_Opportunity_Relationship__c> aorLst = new List<Account_Opportunity_Relationship__c>();
        Account_Opportunity_Relationship__c aorParent   = getAorInstance(accParent.Id, opp.Id);
        Account_Opportunity_Relationship__c aorChild    = getAorInstance(accChild.Id, opp.Id);
        aorLst.add(aorParent);
        aorLst.add(aorChild);
        insert aorLst;

        List<Commercial_Condition__c> commCondLst = new List<Commercial_Condition__c>();
        Commercial_Condition__c parentCapCommCondition = getCapCommConditionInstance();
        Commercial_Condition__c childCapCommCondition  = getCapCommConditionInstance();
        commCondLst.add(parentCapCommCondition);
        commCondLst.add(childCapCommCondition);

        Commercial_Condition__c parentLateCancelCommCondition = getLateCancelCommConditionInstance();
        Commercial_Condition__c childLateCancelCommCondition  = getLateCancelCommConditionInstance();
        commCondLst.add(parentLateCancelCommCondition);
        commCondLst.add(childLateCancelCommCondition);

        Commercial_Condition__c parentNoShowCommCondition = getNoShowCommConditionInstance();
        Commercial_Condition__c childNoShowCommCondition  = getNoShowCommConditionInstance();
        commCondLst.add(parentNoShowCommCondition);
        commCondLst.add(childNoShowCommCondition);
        insert commCondLst;

        Product_Item__c parentProduct = getProductInstance(null, opp.Id);
        insert parentProduct;
        
        Product_Item__c childProduct = getProductInstance(parentProduct.Id, null);
        insert childProduct;
        

        List<Product_Assignment__c> paLst = new List<Product_Assignment__c>();
        Product_Assignment__c parentCapPa           = getProdAssignmentInstance(parentCapCommCondition.Id, parentProduct.Id);
        Product_Assignment__c parentLateCancelPa    = getProdAssignmentInstance(parentLateCancelCommCondition.Id, parentProduct.Id);
        Product_Assignment__c parentNoShowPa        = getProdAssignmentInstance(parentNoShowCommCondition.Id, parentProduct.Id);
        Product_Assignment__c childCapPa            = getProdAssignmentInstance(childCapCommCondition.Id, childProduct.Id);
        Product_Assignment__c childLateCancelPa     = getProdAssignmentInstance(childLateCancelCommCondition.Id, childProduct.Id);
        Product_Assignment__c childNoShowPa         = getProdAssignmentInstance(childNoShowCommCondition.Id, childProduct.Id);
        paLst.add(parentCapPa); 
        paLst.add(parentLateCancelPa); 
        paLst.add(parentNoShowPa); 
        paLst.add(childCapPa); 
        paLst.add(childLateCancelPa); 
        paLst.add(childNoShowPa); 
        insert paLst;

        List<Product_Opportunity_Assignment__c> poaLst = new List<Product_Opportunity_Assignment__c>();
        Product_Opportunity_Assignment__c parentCapPoa          = getPoaInstance(parentCapPa.Id, aorParent.Id);
        Product_Opportunity_Assignment__c parentLateCancelPoa   = getPoaInstance(parentLateCancelPa.Id, aorParent.Id);
        Product_Opportunity_Assignment__c parentNoShowPoa       = getPoaInstance(parentNoShowPa.Id, aorParent.Id);
        Product_Opportunity_Assignment__c childCapPoa           = getPoaInstance(childCapPa.Id, aorChild.Id);
        Product_Opportunity_Assignment__c childLateCancelPoa    = getPoaInstance(childLateCancelPa.Id, aorChild.Id);
        Product_Opportunity_Assignment__c childNoShowPoa        = getPoaInstance(childNoShowPa.Id, aorChild.Id);
        poaLst.add(parentCapPoa);
        poaLst.add(parentLateCancelPoa);
        poaLst.add(parentNoShowPoa);
        poaLst.add(childCapPoa);
        poaLst.add(childLateCancelPoa);
        poaLst.add(childNoShowPoa);
        insert poaLst;

        List<Threshold__c> thLst = new List<Threshold__c>();
        Threshold__c th1 = getThresholdInstance();
        Threshold__c th2 = getThresholdInstance();
        thLst.add(th1);
        thLst.add(th2);
        insert thLst;

        List<Product_Threshold_Member__c> ptmLst = new List<Product_Threshold_Member__c>();
        Product_Threshold_Member__c ptm1 = getThresholdMember(th1, parentProduct);
        Product_Threshold_Member__c ptm2 = getThresholdMember(th2, parentProduct);
        ptmLst.add(ptm1);
        ptmLst.add(ptm2);
        insert ptmLst;

        
        List<Gym_Activity_Relationship__c> garLst = new List<Gym_Activity_Relationship__c>();
        Gym_Activity_Relationship__c gar1 = getGymActivityRelInstance(); 
        Gym_Activity_Relationship__c gar2 = getGymActivityRelInstance();
        garLst.add(gar1);
        garLst.add(gar2);
        insert garLst;

        List<Product_Activity_Relationship__c> parLst = new List<Product_Activity_Relationship__c>();
        Product_Activity_Relationship__c par1 = getPrdActivityRelInstance(gar1, parentProduct);
        Product_Activity_Relationship__c par2 = getPrdActivityRelInstance(gar2, parentProduct);
        parLst.add(par1);
        parLst.add(par2);
        insert parLst;
    }

    @isTest static void validateStepsSMBNegativeTest() {
        Opportunity accOpp = [SELECT Id, CMS_Used__c, RecordTypeId FROM Opportunity WHERE Name = 'academiaBrasilCompanyOpp'];
        Test.startTest();           
            ApprovalProcessHandler appvl = new ApprovalProcessHandler();
            Boolean result = appvl.validateStepsTowardsSuccessApproval(accOpp);
            System.assertEquals(false,result);
    
        Test.stopTest();               
    }

    @isTest static void validateStepsSMBPositiveTest() {
        Opportunity accOpp = [SELECT Id, RecordTypeId FROM Opportunity WHERE Name = 'academiaBrasilCompanyOpp'];
        List<Step_Towards_Success_Partner__c> steps = [SELECT Step_Number__c, Achieved__c FROM Step_Towards_Success_Partner__c WHERE Related_Opportunity__c =: accOpp.Id];
        Test.startTest(); 
            for(Integer i =0; i<10; i++){
                steps[i].Achieved__c = 'Yes';
            }        
            UPDATE steps;
            ApprovalProcessHandler appvl = new ApprovalProcessHandler();
            Boolean result = appvl.validateStepsTowardsSuccessApproval(accOpp);
            System.assertEquals(true,result);
    
        Test.stopTest();               
    }

    
    @isTest static void validateStepsSMBIntegrationApprovalNeededTest() {
        Opportunity accOpp = [SELECT Id,CMS_Used__c, RecordTypeId FROM Opportunity WHERE Name = 'academiaBrasilCompanyOpp'];
        List<Step_Towards_Success_Partner__c> steps = [SELECT Step_Number__c, Achieved__c FROM Step_Towards_Success_Partner__c WHERE Related_Opportunity__c =: accOpp.Id];
        Integer integrationStep = 4;
        Test.startTest();   
            for(Integer i =0; i<10; i++){
                steps[i].Achieved__c = 'Yes';
            }
            steps[integrationStep].Achieved__c = 'No';
            UPDATE steps;
            accOpp.CMS_Used__c = 'Yes';
            UPDATE accOpp;           
            ApprovalProcessHandler appvl = new ApprovalProcessHandler();
            Boolean result = appvl.validateStepsTowardsSuccessApproval(accOpp);
            System.assertEquals(true,result);
    
        Test.stopTest();               
    }

    @isTest static void validateStepsSMBIntegrationNoApprovalTest() {
        Opportunity accOpp = [SELECT Id,CMS_Used__c, RecordTypeId FROM Opportunity WHERE Name = 'academiaBrasilCompanyOpp'];
        List<Step_Towards_Success_Partner__c> steps = [SELECT Step_Number__c, Achieved__c FROM Step_Towards_Success_Partner__c WHERE Related_Opportunity__c =: accOpp.Id];
        Integer integrationStep = 4;
        Test.startTest();   
            for(Integer i =0; i<10; i++){
                steps[i].Achieved__c = 'Yes';
            }
            steps[integrationStep].Achieved__c = 'No';
            UPDATE steps;
            accOpp.CMS_Used__c = 'No';
            UPDATE accOpp;           
            ApprovalProcessHandler appvl = new ApprovalProcessHandler();
            Boolean result = appvl.validateStepsTowardsSuccessApproval(accOpp);
            System.assertEquals(true,result);
    
        Test.stopTest();               
    }


   @isTest static void validateStepsWLTest() {
        Opportunity accOppWl = [SELECT Id, RecordTypeId FROM Opportunity WHERE Name = 'wlOppBR'];        
        Test.startTest();              
            ApprovalProcessHandler appvl = new ApprovalProcessHandler();
            Boolean result = appvl.validateStepsTowardsSuccessApproval(accOppWl);
            System.assertEquals(false, result);
        Test.stopTest();               
    }  

    @isTest static void validateStepsWLPositiveTest() {
        Opportunity accOppWl = [SELECT Id, RecordTypeId FROM Opportunity WHERE Name = 'wlOppBR']; 
        List<Step_Towards_Success_Partner__c> steps = [SELECT Step_Number__c, Achieved__c FROM Step_Towards_Success_Partner__c WHERE Related_Opportunity__c =: accOppWl.Id];
             
        Test.startTest();  
            for(Integer i =0; i<10; i++){
                steps[i].Achieved__c = 'Yes';
            }
            UPDATE steps;            
            ApprovalProcessHandler appvl = new ApprovalProcessHandler();
            Boolean result = appvl.validateStepsTowardsSuccessApproval(accOppWl);
            System.assertEquals(true, result);
        Test.stopTest();               
    } 

    @isTest static void approveAccount() {
        Account acc = [SELECT Id,RecordTypeId, ParentId, ShippingCountry, GP_Status__c, Creation_Reason_Details__c FROM Account WHERE Name = 'AcademiaBrasilCompanyPai'];
        Test.startTest();              
            ApprovalProcessHandler appvl = new ApprovalProcessHandler();
            appvl.submitApproval(acc);
            System.assertEquals('Pending Approval', acc.GP_Status__c);
        Test.stopTest();               
    }

    @isTest static void approveOppNewFlow() {
        Opportunity opp = [SELECT Id, StageName FROM Opportunity WHERE recordTypeId =: getPartnerFlowOppRecTypeId() LIMIT 1];
        opp.StageName = 'Oportunidade Validada Após 1ª Reunião';       
        update opp;

        Test.startTest();
            opp.StageName = 'Review Opportunity';       
            update opp;

            opp = [SELECT Id, StageName, Approval_Needed__c, Approval_Status__c  FROM Opportunity WHERE Id =: opp.Id];
            System.assertEquals('Yes', opp.Approval_Needed__c);

        Test.stopTest();               
    }

    // New methods
    private static Id getPartnerFlowAccRecTypeId() {
        return Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Account').getRecordTypeId();
    }

    private static Id getPartnerFlowOppRecTypeId() {
        return Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Opportunity').getRecordTypeId();
    }

    private static Id getOppStepRecTypeId() {
        return Schema.SObjectType.Step_Towards_Success_Partner__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Opportunity_Step').getRecordTypeId();
    }

    private static Id getAccStepRecTypeId() {
        return Schema.SObjectType.Step_Towards_Success_Partner__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Account_Step').getRecordTypeId();
    }

    private static Account getAccountInstance(String name, Id parentId) {
        Id recordTypeId = getPartnerFlowAccRecTypeId();

        Account acc = new Account();
        acc.Name = name;
        acc.RecordTypeId = recordTypeId;
        acc.ParentId = parentId;
        acc.BillingCountry = 'United States';
        acc.BillingCity = 'New York';
        acc.BillingStreet = 'Av. Test';
        acc.Exclusivity__c = 'Yes';

        return acc;
    }

    private static Opportunity getOpportunityInstance(Id accountId) {
        Id recordTypeId = getPartnerFlowOppRecTypeId();
        
        Opportunity opp = new Opportunity();
        opp.RecordTypeId = recordTypeId;
        opp.Name = 'My Opp';
        opp.CloseDate = System.now().date();
        opp.AccountId = accountId;
        opp.StageName = 'Qualificação';

        return opp;
    }

    private static Account_Opportunity_Relationship__c getAorInstance(Id accountId, Id oppId) {
        Id recordTypeId = Schema.SObjectType.Account_Opportunity_Relationship__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Opportunity_Member').getRecordTypeId();

        Account_Opportunity_Relationship__c aor = new Account_Opportunity_Relationship__c();
        aor.Account__c = accountId;
        aor.Opportunity__c = oppId;
        aor.RecordTypeId = recordTypeId;

        return aor;
    }

    private static Commercial_Condition__c getCapCommConditionInstance() {
        Id recordTypeId = Schema.SObjectType.Commercial_Condition__c.getRecordTypeInfosByDeveloperName().get('CAP').getRecordTypeId();

        Commercial_Condition__c capCommCondition = new Commercial_Condition__c();
        capCommCondition.RecordTypeId = recordTypeId;
        capCommCondition.CAP_Discount__c = 0;
        capCommCondition.Visits_to_CAP__c = 100;

        return capCommCondition;
    }

    private static Commercial_Condition__c getLateCancelCommConditionInstance() {
        Id recordTypeId = Schema.SObjectType.Commercial_Condition__c.getRecordTypeInfosByDeveloperName().get('Late_Cancellation').getRecordTypeId();

        Commercial_Condition__c capCommCondition = new Commercial_Condition__c();
        capCommCondition.RecordTypeId = recordTypeId;

        return capCommCondition;
    }

    private static Commercial_Condition__c getNoShowCommConditionInstance() {
        Id recordTypeId = Schema.SObjectType.Commercial_Condition__c.getRecordTypeInfosByDeveloperName().get('No_Show').getRecordTypeId();

        Commercial_Condition__c capCommCondition = new Commercial_Condition__c();
        capCommCondition.RecordTypeId = recordTypeId;

        return capCommCondition;
    }

    private static Product_Item__c getProductInstance(Id parentProductId, Id oppId) {
        Id recordTypeId = Schema.SObjectType.Product_Item__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product').getRecordTypeId();

        Product_Item__c product = new Product_Item__c();
        product.Name = 'Boxing';
        product.RecordTypeId = recordTypeId;
        product.Parent_Product__c = parentProductId;
        product.Opportunity__c = oppId;
        product.Price_Visits_Month_Package_Selected__c = 100;

        return product;
    }

    private static Product_Assignment__c getProdAssignmentInstance(Id capCommConditionId, Id productId) {
        Id recordTypeId = Schema.SObjectType.Product_Assignment__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product_Assignment').getRecordTypeId();
        
        Product_Assignment__c prodAssignment = new Product_Assignment__c();
        prodAssignment.RecordTypeId = recordTypeId;
        prodAssignment.CommercialConditionId__c = capCommConditionId;
        prodAssignment.ProductId__c = productId;
        prodAssignment.Market_Price__c = 80;

        return prodAssignment;
    }

    private static Product_Opportunity_Assignment__c getPoaInstance(Id paId, Id aorId) {
        Id recordTypeId = Schema.SObjectType.Product_Opportunity_Assignment__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product_Opportunity_Assignment').getRecordTypeId();
        
        Product_Opportunity_Assignment__c poa = new Product_Opportunity_Assignment__c();
        poa.RecordTypeId = recordTypeId;
        poa.ProductAssignmentId__c = paId;
        poa.OpportunityMemberId__c = aorId;

        return poa;
    }

    private static Contact getContact(Account acc) {
        Id recordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Gyms_Partner').getRecordTypeId();
        
        Contact legalRep = new Contact();
        legalRep.RecordTypeId = recordTypeId;
        legalRep.AccountId = acc.Id;
        legalRep.Email = 'legalRepContact@test.com';
        legalRep.LastName = 'Legal';
        legalRep.Cargo__c = 'CEO';

        return legalRep;
    }

    private static Threshold__c getThresholdInstance() {
        Id recordTypeId = Schema.SObjectType.Threshold__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Treshold').getRecordTypeId();
        
        Threshold__c th = new Threshold__c();
        th.Threshold_value_start__c = 100;
        th.Discount__c = 15;

        return th;
    }

    private static Product_Threshold_Member__c getThresholdMember(Threshold__c th, Product_Item__c product) {
        Product_Threshold_Member__c ptm = new Product_Threshold_Member__c();
        ptm.Product__c = product.Id;
        ptm.Threshold__c = th.Id;

        return ptm;
    }

    private static Gym_Activity_Relationship__c getGymActivityRelInstance() {
        Gym_Activity_Relationship__c gar = new Gym_Activity_Relationship__c();
        gar.Name = 'Boxing';

        return gar;
    }

    private static Product_Activity_Relationship__c getPrdActivityRelInstance(Gym_Activity_Relationship__c gar, Product_Item__c product) {
        Product_Activity_Relationship__c par = new Product_Activity_Relationship__c();
        par.Name = 'PAR';
        par.Gym_Activity_Relationship__c = gar.Id;
        par.Product__c = product.Id;

        return par;
    }

    private static void assignAccountLeglRep(Account accParent, Contact legalRepContact) {
        accParent.Legal_Representative__c = legalRepContact.Id;
    }

    private static Event getEvent(Id oppId, Id contactId) {
        Event e = new Event();
        e.WhatId = oppId;
        e.WhoId = contactId;
        e.DurationInMinutes = 60;
        e.ActivityDateTime = System.now();
        
        return e;
    }

    private static List<Step_Towards_Success_Partner__c> getEnablersForOpp(Opportunity opp) {
        Id recTypeId = Schema.SObjectType.Step_Towards_Success_Partner__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Opportunity_Step').getRecordTypeId();

        List<Step_Towards_Success_Partner__c> steps = new List<Step_Towards_Success_Partner__c>();

        List<Step_Towards_Success_Definition__mdt> metadataLst = [
            SELECT Step__c, DeveloperName, MasterLabel, Is_Active__c, New_Label__c
            FROM Step_Towards_Success_Definition__mdt 
            WHERE Category__c = 'Partner Flow' AND Is_Active__c = true
            ORDER BY Step__c ASC
        ];

        for (Step_Towards_Success_Definition__mdt stepMetadata : metadataLst) {
            Step_Towards_Success_Partner__c step = new Step_Towards_Success_Partner__c();
            step.RecordTypeId               = recTypeId;
            step.Name                       = stepMetadata.New_Label__c;
            step.Step_Number__c             = stepMetadata.Step__c;
            step.Related_Opportunity__c     = opp.Id;
            step.Metadata_Developer_Name__c = stepMetadata.DeveloperName;

            steps.add(step);
        }

        return steps;
    }
}