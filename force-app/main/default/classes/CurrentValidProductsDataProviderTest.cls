/**
 * Created by bruno on 11/01/2022.
 */

@IsTest
private class CurrentValidProductsDataProviderTest
{
    @TestSetup
    static void createData(){

        Account lAcc = PartnerDataFactory.newAccount();
        lAcc.Name = 'Parent Acct';
        Database.insert( lAcc );

        Opportunity lOpp = PartnerDataFactory.newOpportunity( lAcc.Id, 'Partner_Flow_Opportunity' );
        Database.insert( lOpp );

        Account_Opportunity_Relationship__c lAcctOppRel = PartnerDataFactory.newAcctOppRel(lAcc.Id, lOpp.Id);
        Database.insert(lAcctOppRel);

        Product_Item__c lProd = PartnerDataFactory.newProductFlow();
        Database.insert(lProd);

        Commercial_Condition__c lCommercialCond = PartnerDataFactory.newCommercialCondition();
        lCommercialCond.RecordTypeId = Schema.SObjectType.Commercial_Condition__c.getRecordTypeInfosByDeveloperName().get('CAP').getRecordTypeId();
        Database.insert(lCommercialCond);

        Product_Assignment__c lProductAssignment = PartnerDataFactory.newProductAssignment(lProd.id, lCommercialCond.id);
        Database.insert(lProductAssignment);

        Product_Opportunity_Assignment__c lProductOppAssignment = PartnerDataFactory.newProductOppAssignment(lAcctOppRel.id, lProductAssignment.id);
        Database.insert(lProductOppAssignment);

        Quote proposal = PartnerDataFactory.newQuote(lOpp);
        proposal.RecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Proposal').getRecordTypeId();
        proposal.Signed__c = Date.Today() - 100;
        proposal.End_Date__c = Date.Today();
        proposal.Start_Date__c = Date.Today() - 100;
        INSERT proposal;

    }

    @IsTest
    static void SDG_Test_negative(){

        List<Account> acctLst = [SELECT Id, Types_of_ownership__c,
                Partner_Level__c, ShippingState,
                ShippingCountry, ShippingCountryCode
        FROM Account LIMIT 1];

        String testRecordId = acctLst.get(0).id;

        // instantiate class
        CurrentValidProductsDataProvider cvpDataProvider = new CurrentValidProductsDataProvider();
        sortablegrid.SDG coreSDG = cvpDataProvider.LoadSDG('', testRecordId);

        // create the user request
        sortablegrid.SDGRequest request = new sortablegrid.SDGRequest();
        request.ParentRecordID = testRecordId;
        request.PageID = 1;
        request.PageSize = 10;
        request.FieldSetName=null;
        request.RelationshipName=null;
        request.SDGTag='Apex:CurrentValidProductsDataProvider';


        // get data
        try {
            sortablegrid.SDGResult result = cvpDataProvider.getData(coreSDG, request);
        } catch (Exception e) {
            // verify that a log was created
            List<DebugLog__c> logs = [SELECT Id, ExceptionMessage__c FROM DebugLog__c];
            system.assertEquals(1, logs.size());
        }
    }

    @IsTest
    static void SDG_Test_positive(){
        Test.startTest();
        Opportunity opp = [SELECT Id, StageName FROM Opportunity limit 1];
        opp.StageName = 'Lançado/Ganho';
        update opp;

        List<Account> acctLst = [SELECT Id, Types_of_ownership__c,
                Partner_Level__c, ShippingState,
                ShippingCountry, ShippingCountryCode
        FROM Account LIMIT 1];

        String testRecordId = acctLst.get(0).id;

        // instantiate class
        CurrentValidProductsDataProvider cvpDataProvider = new CurrentValidProductsDataProvider();
        sortablegrid.SDG coreSDG = cvpDataProvider.LoadSDG('', testRecordId);

        // create the user request
        sortablegrid.SDGRequest request = new sortablegrid.SDGRequest();
        request.ParentRecordID = testRecordId;
        request.PageID = 1;
        request.PageSize = 10;
        request.FieldSetName=null;
        request.RelationshipName=null;
        request.SDGTag='Apex:CurrentValidProductsDataProvider';


        // get data
        sortablegrid.SDGResult result = cvpDataProvider.getData(coreSDG, request);
        system.assertEquals(1, result.data.size());
        Test.StopTest();
    }

    @IsTest
    static void isUserSelectableTest(){

        Boolean isUserSelectable = true;
        System.assertEquals(isUserSelectable, CurrentValidProductsDataProvider.isUserSelectable());
    }
}