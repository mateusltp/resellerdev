/**
 * @description       : 
 * @author            : bruno.mendes@gympass.com
 * @group             : 
 * @last modified on  : 07/06/2022
 * @last modified by  : bruno.mendes@gympass.com
**/

@IsTest
private class PartnerEventTest {

    @TestSetup
    static void setup() {
        Account lAcc = PartnerDataFactory.newAccount();
        Database.insert( lAcc );

        Opportunity lOpp = PartnerDataFactory.newOpportunity( lAcc.Id, 'Partner_Flow_Opportunity' );
        Database.insert( lOpp );
    }

    @IsTest
    static void updateOpportunityRequiresCMSApprovalTrue_test() {
        Id opportunityId = [SELECT Id FROM Opportunity LIMIT 1].Id;

        Partner_Event__e partnerEvent = new Partner_Event__e(
                Object_Id__c = opportunityId,
                Type__c = 'UPDATE_REQUIRES_CMS_APPROVAL_TRUE'
        );

        Test.startTest();
        EventBus.publish(partnerEvent);
        Test.stopTest();

        Opportunity opportunity = [SELECT Requires_CMS_Approval_New__c FROM Opportunity LIMIT 1];
        system.assertEquals(true, opportunity.Requires_CMS_Approval_New__c);
    }
}