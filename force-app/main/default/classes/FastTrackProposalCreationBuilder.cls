/**
* @author vinicius.ferraz
* @description Provide fields source and options load for fast track cmp
*/
public with sharing class FastTrackProposalCreationBuilder {
    
    public class FastTrackProposalCreationBuildException extends Exception{}

    public OpportunityRepository opportunities;
    public QuoteRepository quotes;
    public QuoteLineItemRepository items;
    public WaiverRepository waivers;
    public EligibilityRepository eligibilities;
    public Opportunity opportunity;
    public Quote quote;
    public QuoteLineItem accessFee;
    public QuoteLineItem setupFee;
    public QuoteLineItem proServicesOneFee;
    public QuoteLineItem proServicesMainFee;    
    public FastTrackProposalCreationTO.DealHierarchy dealHierarchy;
    public Map<String, List<Payment__c>> mapFeesPayment;
    public Map<String, List<Eligibility__c>> mapFeesEligibility;
    public Map<String, List<Waiver__c>> mapFeesWaiver;
    public Map<Id, List<Waiver__c>> mapPaymentIdWaiverList;
    public Map<Id, List<Eligibility__c>> mapPaymentIdEligibilityList;
    public Map<String, QuoteLineItem> mapFeeTypeQuoteLineItem;
    public Ops_Setup_Validation_Form__c clientOpsForm;

    public FastTrackProposalCreationBuilder() {
        init();
    }

    /**
     * provide dependency injection for unit test
     */
    public virtual void init(){
        this.opportunities = new OpportunityRepository();
        this.quotes = new QuoteRepository();
        this.items = new QuoteLineItemRepository();
        this.waivers = new WaiverRepository();
        this.eligibilities = new EligibilityRepository();
        this.opportunity = new Opportunity();
        this.quote = new Quote();
        this.accessFee = new QuoteLineItem();
        this.setupFee = new QuoteLineItem();
        this.proServicesOneFee = new QuoteLineItem();
        this.proServicesMainFee = new QuoteLineItem();
        this.mapFeesPayment = new Map<String, List<Payment__c>>();
        this.mapFeesEligibility = new Map<String, List<Eligibility__c>>();
        this.mapFeesWaiver = new Map<String, List<Waiver__c>>();
        this.mapPaymentIdWaiverList = new Map<Id, List<Waiver__c>>();
        this.dealHierarchy = new FastTrackProposalCreationTO.DealHierarchy();
        this.mapPaymentIdEligibilityList = new Map<Id, List<Eligibility__c>>();
        this.mapFeeTypeQuoteLineItem = new Map<String, QuoteLineItem>();
        this.clientOpsForm = new Ops_Setup_Validation_Form__c();
    }

    public FastTrackProposalCreationBuilder withOpportunity(String opportunityId){        
        try{
            this.opportunity = opportunities.byId( opportunityId );
        }catch(System.QueryException e){
            
            throw new FastTrackProposalCreationBuildException('Can not find Opportunity for Id: '+ opportunityId);
        }        
        return this;
    }
    
    public FastTrackProposalCreationBuilder withOpsSetupForm(ID oppId){
        try {
            this.clientOpsForm = new OpsSetupFormRepository().byOppId(oppId);
        }catch (System.QueryException e){
            this.clientOpsForm = new Ops_Setup_Validation_Form__c();
        }

        System.debug('clientOpsForm >>> ' + clientOpsForm);
        return this;
    }

    public FastTrackProposalCreationBuilder withLastCreatedQuoteWhenExists(String opportunityId){ 
        String quoteId = getLastQuoteWhenExists(opportunityId);
        getLastAccessFeeWhenExists(quoteId);
        getLastSetupFeeWhenExists(quoteId);
        getLastProServicesOneFeeWhenExists(quoteId);
        getLastProServicesMainFeeWhenExists(quoteId);
        System.debug(' mapFeeTypeQuoteLineItem .. = '+JSON.serialize(mapFeeTypeQuoteLineItem));
        return this;        
    }

    public FastTrackProposalCreationBuilder withDealHierarchy (Integer newTotalOfEmployees, List<ID> newAccountsInOpp){       
       
        this.dealHierarchy = new FastTrackProposalCreationTO.DealHierarchy(newTotalOfEmployees, newAccountsInOpp);
        System.debug('dealHierarchy no builder ' + this.dealHierarchy); 
        return this;        
    }

    public FastTrackProposalCreationTO build(){        
        System.debug('clientOpsForm no build >>> ' + clientOpsForm);
        return new FastTrackProposalCreationTO()
            .withHeader(opportunity,
                        quote,
                        opportunity.Account.BillingStreet,
                        opportunity.Account.BillingCity,
                        opportunity.Account.BillingState,
                        opportunity.Account.BillingCountry,
                        opportunity.Account.BillingPostalCode)
            .withDealHierarchy(dealHierarchy)
            .withItems(mapFeeTypeQuoteLineItem)
            .withItemsDetails(mapFeesPayment,mapPaymentIdWaiverList,mapPaymentIdEligibilityList)
            .withEnablers(opportunity)
            .withOpsForm(clientOpsForm);
    }

    public String getLastQuoteWhenExists(String opportunityId){
        try{
            quote = quotes.lastForOpportunity(opportunityId);
        }catch(System.QueryException e){
            quote = new Quote();
        }
        return quote.Id;
    }

    public void getLastAccessFeeWhenExists(String quoteId){
        List<Payment__c> payments;
        List<Eligibility__c> eligibilities;
        List<Waiver__c> waivers;
        Map<Id, List<Waiver__c>> mapPaymentIdWaiverListTemp;
        Map<Id, List<Eligibility__c>> mapPaymentIdEligibilityListTemp;

        try{
            accessFee = items.accessFeeAndPaymentsForQuote(quoteId);
            payments = accessFee.Payments__r;
            accessFee = items.accessFeeForQuote(quoteId);
        } catch(System.QueryException e) {
            accessFee = new QuoteLineItem();
        }

        mapFeesPayment.put('accessFee', payments == null || payments.isEmpty() ? new List<Payment__c>() : payments );
        
        mapPaymentIdWaiverListTemp = this.waivers.getWaiversByPayments(mapFeesPayment.get('accessFee'));
        mapPaymentIdWaiverList.putAll(mapPaymentIdWaiverListTemp);

        mapPaymentIdEligibilityListTemp = this.eligibilities.getEligibilitiesByPayments(mapFeesPayment.get('accessFee'));
        mapPaymentIdEligibilityList.putAll(mapPaymentIdEligibilityListTemp);

        mapFeeTypeQuoteLineItem.put('accessFee', accessFee);
    }   

    public void getLastSetupFeeWhenExists(String quoteId){
        List<Payment__c> payments;
        List<Eligibility__c> eligibilities;
        List<Waiver__c> waivers;
        Map<Id, List<Waiver__c>> mapPaymentIdWaiverListTemp;
        Map<Id, List<Eligibility__c>> mapPaymentIdEligibilityListTemp;

        try{
            setupFee = items.setupFeeAndPaymentsForQuote(quoteId);
            payments = setupFee.Payments__r;
            setupFee = items.setupFeeForQuote(quoteId);
        }catch(System.QueryException e){
            setupFee = new QuoteLineItem();
        }

        mapFeesPayment.put('setupFee', payments == null || payments.isEmpty() ? new List<Payment__c>() : payments );

        mapPaymentIdWaiverListTemp = this.waivers.getWaiversByPayments(mapFeesPayment.get('setupFee'));
        mapPaymentIdWaiverList.putAll(mapPaymentIdWaiverListTemp);

        mapPaymentIdEligibilityListTemp = this.eligibilities.getEligibilitiesByPayments(mapFeesPayment.get('setupFee'));
        mapPaymentIdEligibilityList.putAll(mapPaymentIdEligibilityListTemp);

        mapFeeTypeQuoteLineItem.put('setupFee', setupFee);
    }   
    
    public void getLastProServicesOneFeeWhenExists(String quoteId){
        List<Payment__c> payments;
        List<Eligibility__c> eligibilities;
        List<Waiver__c> waivers;
        Map<Id, List<Waiver__c>> mapPaymentIdWaiverListTemp;
        Map<Id, List<Eligibility__c>> mapPaymentIdEligibilityListTemp;

        try{
            proServicesOneFee = items.proServicesOneFeeAndPaymentsForQuote(quoteId);
            payments = proServicesOneFee.Payments__r;
            proServicesOneFee = items.proServicesOneFeeForQuote(quoteId);
        }catch(System.QueryException e){
            proServicesOneFee = new QuoteLineItem();
        }
        
        mapFeesPayment.put('proServicesOneFee', payments == null || payments.isEmpty() ? new List<Payment__c>() : payments );

        mapPaymentIdWaiverListTemp = this.waivers.getWaiversByPayments(mapFeesPayment.get('proServicesOneFee'));
        mapPaymentIdWaiverList.putAll(mapPaymentIdWaiverListTemp);

        mapPaymentIdEligibilityListTemp = this.eligibilities.getEligibilitiesByPayments(mapFeesPayment.get('proServicesOneFee'));
        mapPaymentIdEligibilityList.putAll(mapPaymentIdEligibilityListTemp);

        mapFeeTypeQuoteLineItem.put('proServicesOneFee', proServicesOneFee);
    }

    public void getLastProServicesMainFeeWhenExists(String quoteId){
        List<Payment__c> payments;
        List<Eligibility__c> eligibilities;
        List<Waiver__c> waivers;
        Map<Id, List<Waiver__c>> mapPaymentIdWaiverListTemp;
        Map<Id, List<Eligibility__c>> mapPaymentIdEligibilityListTemp;

        try{
            proServicesMainFee = items.proServicesMainFeeAndPaymentsForQuote(quoteId);
            payments = proServicesMainFee.Payments__r;
            proServicesMainFee = items.proServicesMainFeeForQuote(quoteId);
        }catch(System.QueryException e){
            proServicesMainFee = new QuoteLineItem();
        }
        
        mapFeesPayment.put('proServicesMainFee', payments == null || payments.isEmpty() ? new List<Payment__c>() : payments );

        mapPaymentIdWaiverListTemp = this.waivers.getWaiversByPayments(mapFeesPayment.get('proServicesMainFee'));
        mapPaymentIdWaiverList.putAll(mapPaymentIdWaiverListTemp);

        mapPaymentIdEligibilityListTemp = this.eligibilities.getEligibilitiesByPayments(mapFeesPayment.get('proServicesMainFee'));
        mapPaymentIdEligibilityList.putAll(mapPaymentIdEligibilityListTemp);

        mapFeeTypeQuoteLineItem.put('proServicesMainFee', proServicesMainFee);
    }

}