/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 05-13-2021
 * @last modified by  : gft.samuel.silva@ext.gympass.com
 * Modifications Log 
 * Ver   Date         Author                             Modification
 * 1.0   05-11-2021   gft.samuel.silva@ext.gympass.com   Initial Version
**/
@isTest
public without sharing class EngagementJourneyStageBatchTest {

    @TestSetup
    static void createData(){
        ID lAccRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
        List<Account> accLst = new List<Account>();
        for(Integer i=1;i<=5;i++){
            Account acc = DataFactory.newAccount();   
            acc.RecordTypeId = lAccRecordTypeId;
            acc.Name = 'Client ' + i;
            acc.Proposal_End_Date__c = Date.today() - 5;
            acc.Engagement_Journey_Stage__c = 'M'+i;
            acc.M2_Launch_Period_Expiration_Date__c = Date.today() - 250;        
            if(i==3){
                acc.M3_Post_Launch_Period_Expiration_Date__c = Date.today() - 100;    
            } else if(i==4){
                acc.M4_Nurturing_Period_Expiration_Date__c = Date.today() - 90;
            }
            accLst.add( acc );
        }  
        Database.insert( accLst ); 
        
    }


    @isTest
    static void test() {   
        Test.startTest();    
            String testTime = '0 0 0 * * ?'; 
            EngagementJourneyStageBatch.start(testTime);   
            CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime 
                                FROM CronTrigger 
                                WHERE CronJobDetail.Name = 'Engagement Journey Stage Test'];            
            System.assertEquals(testTime, ct.CronExpression);
            System.assertEquals(0, ct.TimesTriggered);
            String timeNow = String.valueOf(Date.today()+1) + ' 00:00:00';
            System.assertEquals(timeNow, String.valueOf(ct.NextFireTime));
        Test.stopTest();
    }
}