/**
 * @author vncferraz
 * 
 * Facade implementation class to provide specialization for OfferProcessor on Subsidiary requests
 */
public class OfferSubsidiaryProcessor extends OfferQueueProcessor{
    
    public OfferSubsidiaryProcessor(List<Offer_Queue__c> offers){
        super(offers);
    }

    override
    protected void init(){
        this.builder = new OfferWrapperSubsidiaryBuilder();
    }   

}