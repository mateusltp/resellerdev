/**
 * @description       : holds the relationship between account and opp (multiple entity)
 * @author            : Samuel Silva - GFT (slml@gft.com)
 * @group             : 
 * @last modified on  : 05-19-2022
 * @last modified by  : Mateus Augusto - GFT (moes@gft.com)
 * Modifications Log 
 * Ver   Date         Author                              Modification
 * 1.0   08-26-2020   Samuel Silva - GFT (slml@gft.com)   Initial Version
**/
public without sharing virtual class AccountOpportunityRepository {

    public ID oppId {get; private set;}
    public Integer totalOfEmployees {get;private set;}
    public List<Id> accountsInOpp {get;private set;}
    public String businessModel {get;set;}

    public AccountOpportunityRepository() {}

    public AccountOpportunityRepository(ID oppId) {
        this.oppId = oppId;  
        init();      
    }
   
    private void init(){
        this.totalOfEmployees = 0;
        this.accountsInOpp = new List<Id>();

        try {            
            Set<Id> accSet = findRelatedAccounts();
            if(!accSet.isEmpty()) {
                this.accountsInOpp.addAll(accSet);
                List<Account> accLst = new AccountRepository().getAccounts(accSet);              
                for(Account acc : accLst ){
                    System.debug(' acc.NumberOfEmployees '  +  acc.NumberOfEmployees);
                    if(acc.NumberOfEmployees!= null){
                        this.totalOfEmployees += acc.NumberOfEmployees; 
                    }
                }
            }
        } catch(System.QueryException e){
            
        }
    }


    public virtual Set<Id> findRelatedAccounts() {
        Set<ID> accIds = new Set<ID>();
        List<Account_Opportunity_Relationship__c> aorLst = [ 
                SELECT Account__c 
                FROM Account_Opportunity_Relationship__c
                WHERE Opportunity__c =: oppId];  
        if(!aorLst.isEmpty()){
            for(Account_Opportunity_Relationship__c aor : aorLst){
                accIds.add(aor.Account__c);
            }
        }
        return accIds; 
    } 



    public virtual List<Account_Opportunity_Relationship__c> getAccountsInOpp(){
        return [SELECT Id, Account__c, Account__r.Name, Account__r.BillingCity, Account__r.BillingState, Account__r.BillingCountry,
                        Billing_Percentage__c, Account__r.NumberOfEmployees, Account__r.ParentId, Account__r.Razao_Social__c, Account__r.Id_Company__c, 
                        Maintenance_Fee_Billing_Percentage__c, Setup_Fee_Billing_Percentage__c, Gympass_Entity__c, Prof_Serv_Setup_Fee_Billing_Percentage__c,
                        Main_Order__c
                        FROM Account_Opportunity_Relationship__c
                        WHERE Opportunity__c =: oppId ];    
    }


    public virtual List<Account_Opportunity_Relationship__c> getAccountsInOpps(List<Id> oppIdList){
        return [SELECT Id, Account__c, Account__r.Name, Account__r.BillingCity, Account__r.BillingState, Account__r.BillingCountry,
                        Billing_Percentage__c, Account__r.NumberOfEmployees, Account__r.ParentId, Account__r.Razao_Social__c, Account__r.Id_Company__c, 
                        Maintenance_Fee_Billing_Percentage__c, Setup_Fee_Billing_Percentage__c, Gympass_Entity__c, Prof_Serv_Setup_Fee_Billing_Percentage__c,
                        Main_Order__c, Opportunity__c
                        FROM Account_Opportunity_Relationship__c
                        WHERE Opportunity__c IN :oppIdList ];    
    }

    public virtual List<Account_Opportunity_Relationship__c> getResellerAccountsInOpp(){
        return [SELECT Id, Account__c, Account__r.Name, Account__r.BillingCity, Account__r.BillingState, Account__r.BillingCountry,
                        Billing_Percentage__c, Account__r.NumberOfEmployees, Account__r.ParentId, Account__r.Razao_Social__c, Account__r.Id_Company__c, 
                        Maintenance_Fee_Billing_Percentage__c, Setup_Fee_Billing_Percentage__c, Gympass_Entity__c, Prof_Serv_Setup_Fee_Billing_Percentage__c,
                        Main_Order__c
                        FROM Account_Opportunity_Relationship__c
                        WHERE Opportunity__c =: oppId 
                        LIMIT 1];  
    }

    public virtual List<Account_Opportunity_Relationship__c> getAccountsInOppWhithReseller(){
        return [SELECT Account__c, Account__r.Name, Account__r.BillingCity, Account__r.BillingState, Account__r.BillingCountry,
                        Billing_Percentage__c, Account__r.NumberOfEmployees, Account__r.ParentId, Account__r.Razao_Social__c, Account__r.Id_Company__c, 
                        Maintenance_Fee_Billing_Percentage__c, Setup_Fee_Billing_Percentage__c, Gympass_Entity__c, Prof_Serv_Setup_Fee_Billing_Percentage__c,
                        Main_Order__c
                        FROM Account_Opportunity_Relationship__c
                        WHERE Opportunity__c =: oppId and Account__r.Type !='Channel Partner'];     
    }

    public void setRelationshipAccountWithOpp(List<Account> accLst){     
        List<Account_Opportunity_Relationship__c> aorLst = new List<Account_Opportunity_Relationship__c>();
        for(Account acc : accLst){
            Account_Opportunity_Relationship__c aor = new Account_Opportunity_Relationship__c();
            aor.Opportunity__c = oppId;
            aor.Account__c = acc.Id;
            aorLst.add(aor);          
        }     
        Database.insert(aorLst);
    }

    public void setRelationshipAccountWithOppForClients(List<Account> accLst){     
        ID rtId = Schema.SObjectType.Account_Opportunity_Relationship__c.getRecordTypeInfosByDeveloperName().get('Client_Sales').getRecordTypeId(); 
        List<Account_Opportunity_Relationship__c> aorLst = new List<Account_Opportunity_Relationship__c>();
        Opportunity opp = new OpportunityRepository().byId(this.oppId);
        List<Account_Opportunity_Relationship__c> oppAorList = getAccountsInOpp();

        Map<Id, Id> accIdToOppId = new Map<Id, Id>();
        
        for (Account_Opportunity_Relationship__c aor : oppAorList) {
            accIdToOppId.put(aor.Account__c, this.oppId);
        }
        
        for (Account acc : accLst) {
            if (!accIdToOppId.containsKey(acc.Id)) {
                accIdToOppId.put(acc.Id, null);    
            }
        }

        for(Account acc : accLst){
            Id oppId = accIdToOppId.get(acc.Id);

            if (oppId == null) {
                Account_Opportunity_Relationship__c aor = new Account_Opportunity_Relationship__c();
                aor.Opportunity__c = this.oppId;
                aor.Account__c = acc.Id;
                aor.RecordTypeId = rtId;
                aor.Main_Order__c = acc.Id == opp.AccountId ? true : false;
                aorLst.add(aor);
            }
        }
        Database.insert(aorLst);
    }
    
    public void breakRelationshipAccountWithOpp(List<Account> accLst){      
       List<Account_Opportunity_Relationship__c> aorLst = new List<Account_Opportunity_Relationship__c>();
       Set<Id> accIdSet = new Set<Id>();
       for(Account acc : accLst){           
           accIdSet.add(acc.Id);
       }
       aorLst = [SELECT Id FROM Account_Opportunity_Relationship__c WHERE Account__c IN: accIdSet AND Opportunity__c =: oppId];
       if(aorLst.size() > 0){           
            Database.delete(aorLst);
       }    
    }

    public virtual List<Account_Opportunity_Relationship__c> edit(List<Account_Opportunity_Relationship__c> accOppRelationships){
        Database.update(accOppRelationships, true);
        System.debug('accOppRelationships: ' + accOppRelationships);
        return accOppRelationships;
    }

    public virtual List<Account_Opportunity_Relationship__c> create(List<Account_Opportunity_Relationship__c> accOppRelationships){
        System.debug('create');
        System.debug('accOppRelationships : ' + accOppRelationships);
        Database.insert(accOppRelationships, true);
        return accOppRelationships;
    }

    public void deleteRelationshipAccountWithOpp(List<Account_Opportunity_Relationship__c> accOppRelationships){
        Database.delete(accOppRelationships, true);
    }

    public Set<String> countriesInDeal(Set<Id> accountsInOpp){
        Set<String> countriesInDealLst = new Set<String>();
        for(Account a : [SELECT BillingCountry FROM Account WHERE ID IN: accountsInOpp]){
            countriesInDealLst.add(a.BillingCountry);
        }
        return countriesInDealLst;
    }
}