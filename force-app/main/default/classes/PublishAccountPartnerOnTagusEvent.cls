/**
 * @description       :
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             :
 * @last modified on  : 10-13-2021
 * @last modified by  : gft.samuel.silva@ext.gympass.com
 **/
public without sharing class PublishAccountPartnerOnTagusEvent {
    private static final PS_Constants constants = PS_Constants.getInstance();
    
  public void execute() {
    List<Account> accounts = new List<Account>();
	if(System.Label.TAGUS_PARTNER !=  constants.PARTNER && !Test.isRunningTest()) return;
    for (Account newAccount : (List<Account>) Trigger.new) {
      if (isToPublishPartner(newAccount)) {
        accounts.add(newAccount);
      }
    }

    if (!accounts.isEmpty()) {
      new TagusAccountPartnerOutboundPublisher(accounts).runUpdate();
    }
  }
  private Boolean isToPublishPartner(Account newAccount) {
    final Id ACCOUNT_PARTNER_RECORDTYPE = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName()
      .get('Partner_Flow_Account')
      .getRecordTypeId();
    Map<Id, Account> oldAccountMap = (Map<Id, Account>) Trigger.oldMap;

    Account oldAccount = oldAccountMap?.get(newAccount.Id);

    return (Trigger.isUpdate &&
    newAccount.RecordTypeId == ACCOUNT_PARTNER_RECORDTYPE &&
    newAccount.UUID__c != null &&
    newAccount.Send_To_Tagus__c &&
    (oldAccount.Send_To_Tagus__c != newAccount.Send_To_Tagus__c ||
    oldAccount.Name != newAccount.Name ||
    oldAccount.BillingStreet != newAccount.BillingStreet ||
    oldAccount.BillingCity != newAccount.BillingCity ||
    oldAccount.BillingState != newAccount.BillingState ||
    oldAccount.BillingPostalCode != newAccount.BillingPostalCode ||
    oldAccount.BillingCountry != newAccount.BillingCountry ||
    oldAccount.Legal_Registration__c != newAccount.Legal_Registration__c ||
    oldAccount.Legal_Title__c != newAccount.Legal_Title__c ||
    oldAccount.Email__c != newAccount.Email__c ||
    oldAccount.Legal_Document_Type__c != newAccount.Legal_Document_Type__c ||
    oldAccount.Phone != newAccount.Phone));
  }
}