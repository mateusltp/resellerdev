public with sharing class GetCases {
    @AuraEnabled(Cacheable=true)
    public static List<Case> getCases() {
        
        List<RegionByCountry__mdt> regions = RegionByCountry__mdt.getAll().values();

        List<String> ListEU     = new List<String>();
        List<String> ListUS     = new List<String>();
        List<String> ListLATAM  = new List<String>();

        for (RegionByCountry__mdt region: regions){
            switch on region.Region__c {
                when 'EU' {
                    ListEU.add(region.Country_Legal__c);
                }
                when 'US' {
                    ListUS.add(region.Country_Legal__c);
                }
                when 'LATAM' {
                    ListLATAM.add(region.Country_Legal__c);
                }	
            }
        }
        Map<String,List<String>> RegionCountriesMap = new Map<String,List<String>>();

        RegionCountriesMap.put('EU',ListEU);
        RegionCountriesMap.put('US',ListUS);
        RegionCountriesMap.put('LATAM',ListLATAM);

        List<User> user = [SELECT Region__c FROM User WHERE username=:UserInfo.getUsername()];
        List<String> userCountries = new List<String>();

        if(!user.isEmpty()){
            if (user[0].Region__c != null){
                for (String region: user[0].Region__c.split(';')){
                    userCountries.addAll(RegionCountriesMap.get(region));                    
                }
            }
        }

        List<Case> listCase = [SELECT  Id, CaseNumber,
                                        CompanyName__c, 
                                        Country__c,
                                        OwnerId, 
                                        Owner.Name, 
                                        Opportunity_Record_Type__c,
                                        ContractAgreement__c, 
                                        ContractAgreement__r.Name, 
                                        Status, 
                                        CreatedBy__c, 
                                        CreatedDate 
                                   FROM CASE 
                                  WHERE RecordTypeId = :Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('RTContractReview').getRecordTypeId()
                                    AND Country__c in :userCountries
                                    AND OwnerType__c !=  'User'];
        
        return(listCase);
    }
}