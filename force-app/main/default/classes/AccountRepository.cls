/**
* @author vinicius.ferraz
* @description Provide mockable repository data layer for Account domain
*/
public with sharing class AccountRepository {

    public static final String RECORD_TYPE_GYMPASS_ENTITY = 'Gympass_Entity';

    public AccountRepository() {

    }

    /**
     * throwable System.QueryException
     * 
     */
    public virtual Account byId(String recordId){
        return [select Id
                ,Name
                ,BillingCity
                ,BillingState
                ,BillingCountry
                ,NumberOfEmployees
                ,Id_Company__c
                ,Razao_Social__c
                ,BillingAddress
                ,BillingStreet
                ,BillingStateCode
                ,BillingPostalCode
                ,BillingCountryCode
                ,Contract_type__c
                ,Standard_Gympass_Entity__c
                ,ID_Gympass_CRM__c
                ,Owner.Email
                ,Sales_Broker__c
                ,Indirect_Channel_Pricebook__c
                from Account
                where Id = :recordId];
    }

    public virtual Map<Id, Account> getMapByIds(Set<Id> recordIdSet){
        Map<Id, Account> idToAccount = new Map<Id, Account>([
            select Id,Name, BillingCity, BillingState, BillingCountry, NumberOfEmployees, Indirect_Channel_Pricebook__c,
            (SELECT Id, Primary_HR_Contact__c FROM Contacts) 
            from Account
            where Id = :recordIdSet
        ]);   
        return idToAccount;
    }

      /**
     * throwable System.DMLException
     */
    public virtual Account add(Account record){
        Database.insert(record);
        return record;
    }

    /**
     * throwable System.DMLException
     */
    public virtual Account edit(Account record){
        Database.update(record);
        return record;
    }

    public virtual List<Account> edit(List<Account> ltAccount){
        Database.update(ltAccount, true);
        return ltAccount;
    }

    /**
     * throwable System.DMLException
     */
    public virtual Account addOrEdit(Account record){
        Database.upsert(record, Account.Id, true);
        return record;
    }

    public virtual List<Account> gympassEntities(){
        return [select Id,Name, BillingCountryCode from Account where RecordType.DeveloperName = :RECORD_TYPE_GYMPASS_ENTITY];
    }
    public virtual Map<String,Id> gympassEntitiesByExternalId(){
        List<Account> entities= [select Id,Name,Id_Company__c from Account where RecordType.DeveloperName = :RECORD_TYPE_GYMPASS_ENTITY];
        Map<String,Id> entitiesByExternalId = new Map<String,Id>();
        for ( Account entity : entities )
            entitiesByExternalId.put(entity.Id_Company__c,entity.Id);
        return entitiesByExternalId;
    }

    /* mapping hierarchy*/
    public virtual Set<Id> findChildAccounts(ID recordId) {
        Set<Id> childAccounts = new Set<Id>();
        Set<Id> initialAccount = new Set<Id>{recordId};
        mapChildAccounts(initialAccount, childAccounts);
        return childAccounts;
    }
    private void mapChildAccounts(Set<ID> iterationIds, Set<ID> childAccounts){
        Set<Id> newIterationIds = new Set<ID>(new Map<Id,Account>([SELECT Id FROM Account WHERE ParentId IN : iterationIds]).keySet());
        if(newIterationIds.size()>0) {
            childAccounts.addAll(newIterationIds);
            mapChildAccounts(newIterationIds, childAccounts);
        }    
    }
    
    public virtual List<Account> getAccounts(Set<Id> accountIds){
        return [ SELECT Id, NumberOfEmployees, Name, BillingCity, BillingState, BillingCountry, OwnerId , Razao_Social__c, Id_Company__c
        FROM Account 
        WHERE Id IN: accountIds];
    }


}