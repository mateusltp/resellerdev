/** 
* @description       : 
* @author            : ChangeMeIn@UserSettingsUnder.SFDoc 
* @group             : 
* @last modified on  : 06-20-2022
* @last modified by  : Mateus Augusto - GFT (moes@gft.com)
**/
public without sharing virtual class FastTrackRegionalManagerApprovalHandler {
    
    public FastTrackProposalCreationTO to {get;private set;}    
    
    public static void fastTrackRegionalManagerApproval(QuoteLineItem objQuoteItem, Quote objQuote){
        Opportunity opp = [SELECT id, Reseller_del__c,CurrencyIsoCode,B_M__c FROM Opportunity WHERE id =: objQuote.OpportunityId]; 
        Account acc = [SELECT id, Indirect_Channel_Pricebook__c FROM Account WHERE id =: opp.Reseller_del__c]; 
        //Pricebook2 pricebook = [SELECT id,Name,Business_Model__c,CurrencyIsoCode FROM pricebook2 WHERE Id = : objQuote.Pricebook2Id];
        //Assert_Data__c lastApprovedSalePrice = [SELECT id, New_Value__c from Assert_Data__c WHERE Opportunity__c =: objQuote.OpportunityId AND Field_Name__c = 'UnitPrice' LIMIT 1]; 
        
        LegacyPartnerPriceRepository legacyPartnerPriceRepository = new LegacyPartnerPriceRepository();
        
        List<Legacy_Partner_Price__c> legacyPartner = legacyPartnerPriceRepository.isPartnerByAccountId(opp.Reseller_del__c, opp.CurrencyIsoCode);  
        
        List <Assert_Data__c> listLastApprovedSalePrice = [SELECT id, New_Value__c from Assert_Data__c WHERE Opportunity__c =: objQuote.OpportunityId AND Field_Name__c = 'UnitPrice']; 
        system.debug('@@@assert_data__c'+listLastApprovedSalePrice);
        Assert_Data__c lastApprovedSalePrice = new Assert_Data__c();
        
        if(listLastApprovedSalePrice.size() > 0){
            lastApprovedSalePrice = listLastApprovedSalePrice[0];
        }else{
            lastApprovedSalePrice = null;
        }
        
        if(opp.B_M__c != 'Intermediation'){
            objQuote.Comission_Percent__c = 0;
            objQuote.Percent_of_comission__c = '0';                
        }
        
        if(objQuoteItem.monthlyCap__c == null){
            objQuoteItem.monthlyCap__c = 0;
            objQuoteItem.minInvestment__c = 0;
            objQuoteItem.priceperEnrolled__c = 0;                   
        }
        system.debug('objQuoteItem.Plan_Name__c '+ objQuoteItem);
        if(objQuoteItem.Plan_Name__c == 'Setup Fee'){
            noNeedApproval(objQuote);
        }else{
            if(opp.B_M__c == 'Exclusive'){
                noNeedApproval(objQuote);  
            }else{          
                if(lastApprovedSalePrice != null){            
                    if(objQuoteItem.UnitPrice < decimal.valueOf(lastApprovedSalePrice.New_Value__c)){
                        needApproval(objQuoteItem, objQuote, acc, opp);                
                    }else{
                        noNeedApproval(objQuote);               
                    }
                }else{
                    if(legacyPartner !=null && !legacyPartner.isEmpty() && opp.B_M__c != 'Intermediation'){
                        if(acc.Indirect_Channel_Pricebook__c == 'Legacy Partners'){
                            if(objQuoteItem.UnitPrice < legacyPartner[0].Legacy_Price__c){
                                needApproval(objQuoteItem, objQuote, acc, opp);  
                            }else{
                                noNeedApproval(objQuote);
                            }                    
                        }
                        
                    }else{
                        if(objQuoteItem.UnitPrice < objQuoteItem.List_Price__c){   
                            needApproval(objQuoteItem, objQuote, acc, opp);               
                        }else{ 
                            noNeedApproval(objQuote);              
                        }
                        
                    }     
                }
            }        
        }
        
        
        update objQuote; 
        update objQuoteItem;
        
    }
    
    public static void needApproval(QuoteLineItem objQuoteItem, Quote objQuote, Account acc, Opportunity opp){
        
        String query = 'select id,User_First_Level_Approval__c,User_Second_Level_Approval__c from User_Setting__mdt where ';
        
        if(opp.CurrencyIsoCode == 'BRL'){
            query += 'DeveloperName =\'BR_Reseller_Approvers\' limit 1';
        }
        if(opp.CurrencyIsoCode == 'USD'){
            query += 'DeveloperName =\'US_Reseller_Approvers\' limit 1';
        }
        if(opp.CurrencyIsoCode == 'MXN'){
            query += 'DeveloperName =\'MXN_Reseller_Approvers\' limit 1';
        }
        if(opp.CurrencyIsoCode == 'ARS'){
            query += 'DeveloperName =\'ARS_Reseller_Approvers\' limit 1';
        }
        if(opp.CurrencyIsoCode == 'CLP'){
            query += 'DeveloperName =\'CLP_Reseller_Approvers\' limit 1';
        }
        if(opp.CurrencyIsoCode == 'EUR'){
            query += 'DeveloperName =\'EU_Reseller_Approvers\' limit 1';
        }    
        else if(opp.CurrencyIsoCode == 'GBP'){
            query += 'DeveloperName =\'EU_Reseller_Approvers\' limit 1';     
        }        
        User_Setting__mdt useLevelApproval = Database.query(query);
        
        User firstLevelApproval = [SELECT id FROM User WHERE Alias = : useLevelApproval.User_First_Level_Approval__c];
        User secondLevelApproval = [SELECT id FROM User WHERE Alias = : useLevelApproval.User_Second_Level_Approval__c]; 
        
        objQuote.Copay__c = 'No';
        objQuote.Enterprise_Discount_Approval_Needed__c = true;
        objQuote.Enterprise_Subscription_Approved__c = false;
        objQuote.User_Discount_Approval_Level__c = 1;
        objQuote.User_First_Level_Approval__c = firstLevelApproval.id;  
        objQuote.Commercial_Approved_Conditions__c = '[{"isApproved":false,"approvalCondition":"Regional Manager"}]';
        
        System.debug('FastTrackProposalCreationTO - Need Approval:'+ objQuote.Enterprise_Discount_Approval_Needed__c+objQuote.Enterprise_Subscription_Approved__c+objQuote.User_Discount_Approval_Level__c);
        
        if(opp.B_M__c == 'Intermediation'){
            if(opp.CurrencyIsoCode == 'USD' && objQuote.Comission_Percent__c == 0){
                noNeedApproval(objQuote);
            }else{
                objQuote.User_Discount_Approval_Level__c = 2;
                objQuote.User_Second_Level_Approval__c  = secondLevelApproval.id;
            }         
        }else{          
            if(acc.Indirect_Channel_Pricebook__c == 'Legacy Partners'){
                objQuote.User_Discount_Approval_Level__c = 2;
                objQuote.User_Second_Level_Approval__c  = secondLevelApproval.id;                                
            }else{
                if(objQuote.CurrencyIsoCode != 'BRL'){                              
                    objQuote.User_Discount_Approval_Level__c = 2;
                    objQuote.User_Second_Level_Approval__c  = secondLevelApproval.id;                                    
                }         
            }                      
        }       
    }
    
    public static void noNeedApproval(Quote objQuote){
        
        objQuote.Copay__c = 'No';
        objQuote.Enterprise_Discount_Approval_Needed__c = false;
        objQuote.Enterprise_Subscription_Approved__c = true;
        objQuote.User_Discount_Approval_Level__c = 0; 
        objQuote.User_First_Level_Approval__c = null; 
        objQuote.User_Second_Level_Approval__c  = null;
        objQuote.Commercial_Approved_Conditions__c = '';
        System.debug('FastTrackProposalCreationTO - No need Approval:'+ objQuote.Enterprise_Discount_Approval_Needed__c+objQuote.Enterprise_Subscription_Approved__c+objQuote.User_Discount_Approval_Level__c);
        
    }   
}