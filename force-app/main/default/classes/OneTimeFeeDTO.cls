public without sharing class OneTimeFeeDTO extends FeeItemDTO {
  private Datetime billing_date;
  private Integer baseline_quantity;
  private Decimal unit_price;
  private Decimal total_price;
  private Decimal discount_percentage;
  private FeeType fee_type;

  public OneTimeFeeDTO(OrderItem orderItem) {
    this.id = orderItem.UUID__c != null
      ? orderItem.UUID__c
      : new Uuid().getValue();
    this.salesforce_id = orderItem.Id;
    this.currency_id = orderItem.CurrencyIsoCode;
    this.unit_price = orderItem.UnitPrice;
    this.total_price = orderItem.TotalPrice;
    this.discount_percentage = orderItem.Discount__c;
    this.baseline_quantity = (Integer) orderItem.Quantity;
    this.fee_type = getFeeType(orderItem);
    this.payment_due_days = (Integer) orderItem.Payment_Due_Days__c;
    this.billing_date = orderItem.Billing_Date__c != null // Don't forget about me
      ? orderItem.Billing_Date__c
      : Datetime.newInstance(
        Date.today().year(),
        Date.today().month(),
        (Integer) orderItem.Billing_Day__c
      );
  }

  public FeeType getFeeType() {
    return this.fee_type;
  }

  private FeeType getFeeType(OrderItem orderItem) {
    if (orderItem.Type__c.contains('Professional Services')) {
      return OneTimeFeeDTO.FeeType.PROFESSIONAL_SETUP_FEE;
    }

    return OneTimeFeeDTO.FeeType.SETUP_FEE;
  }

  override
  public QuoteLineItem parseToQuoteLineItem() {
    return new QuoteLineItem(
      UUID__c = this.id,
      UnitPrice = this.unit_price,
      Discount = this.discount_percentage,
      Quantity = this.baseline_quantity,
      Payment_Due_Days__c = this.payment_due_days
    );
  }

  public enum FeeType {
    SETUP_FEE,
    PROFESSIONAL_SETUP_FEE
  }
}