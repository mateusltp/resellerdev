/**
* @description       : 
* @author            : Gilberto Souza - GFT (gilberto.souza@gft.com)
* @group             : 
* @last modified on  : 05-03-2022
* @last modified by  : Mateus Augusto - GFT (moes@gft.com)
* Modifications Log
* Ver   Date         Author                                          Modification
* 1.0   12-03-2021   Gilberto Souza - GFT (gilberto.souza@gft.com)   Initial Version
**/

public without sharing class DataFactory {
     
    
    public static Pricebook2 newPricebook(){
        Pricebook2 lStandardPricebook = new Pricebook2(
            Id = Test.getStandardPricebookId(),
            IsActive = true
        );
        return lStandardPricebook;
    }    
    
    public static Pricebook2 newPricebookByBusinessModel(String name, String businessModel){
        Pricebook2 pricebook = new Pricebook2();
        pricebook.Name = name;
        pricebook.Business_Model__c = businessModel;
        pricebook.IsActive = true;
        return pricebook;
    }     
    
    public static Account newAccount(){
        ID accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
        Account acc = new Account();
        acc.RecordTypeId = accRecordTypeId;
        acc.Name = 'Account 1';
        acc.BillingCountry = 'Brazil';
        acc.BillingState = 'São Paulo';
        acc.NumberOfEmployees = 2000;
        acc.Proposal_End_Date__c = Date.today();
        return acc;
    }
    
    public static Product2 newProduct(String ProductName , Boolean isFamilyMemberIncluded , String currencyIsoCode){
        Product2 product = new Product2();
        product.Name = ProductName;
        product.Minimum_Number_of_Employees__c = 0;
        product.Maximum_Number_of_Employees__c = 900;
        product.Family = ProductName;
        product.IsActive = true;
        product.Family_Member_Included__c = isFamilyMemberIncluded;
        product.ProductCode = ProductName.replace(' ', '_');
        product.Copay2__c = false;
        product.CurrencyIsoCode = currencyIsoCode;
        return product;
    }  
    
    public static Opportunity newOpportunity(Id aAccId, Pricebook2 aPriceBook, String recordType){
        Opportunity lOpp = new Opportunity();
        lOpp.CurrencyIsoCode='BRL';
        lOpp.recordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(recordType).getRecordTypeId();
        lOpp.StageName='Qualification';
        lOpp.AccountId = aAccId;
        lOpp.CloseDate = Date.today();
        lOpp.Engagement_Journey_Completed__c = true;
        lOpp.Name='Renegotiation'; 
        lOpp.Pricebook2Id = aPriceBook.Id;
        return lOpp;
    }
    
    public static Quote newQuote(Opportunity opp, String name, String recordType){
        Quote qt = new Quote();  
        qt.RecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get(recordType).getRecordTypeId();
        qt.Name = opp.Name+name;
        qt.OpportunityId = opp.Id;   
        qt.License_Fee_Waiver__c = 'No';  
        qt.CurrencyIsoCode = opp.CurrencyIsoCode;
        qt.Start_Date__c = Date.today();
        qt.Deal_Desk_Approved_Conditions__c = '[{"isApproved":false,"approvalCondition":"Enterprise Subscription fee type is not standard"},{"isApproved":false,"approvalCondition":"Professional Service Setup Fee price out of standard range"},{"isApproved":false,"approvalCondition":"Professional Service  Monthly Fee price out of standard range"},{"isApproved":false,"approvalCondition":"Enterprise Subscription payment frequency is custom"},{"isApproved":false,"approvalCondition":"Proservice payment frequency is custom"},{"isApproved":false,"approvalCondition":"Autonomous Market Place is not defined for your Offer"},{"isApproved":false,"approvalCondition":"Eligibility/MembershipFee with Custom payment settings or Credit Card method"}]';
        return qt;
    }
    
    public static Case newCase(Opportunity opp, Quote qt, String recordType){
        Case lOperationalCase = new Case();
        lOperationalCase.Description = 'isTest';
        lOperationalCase.OpportunityId__c = opp.Id;
        lOperationalCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get(recordType).getRecordTypeId();
        lOperationalCase.QuoteId__c = qt.Id;
        lOperationalCase.Subject = 'Deal Desk Operational Approval Request';        
        lOperationalCase.Justificate_No_Compliant_Topics__c = 'Justification Test';        
        return lOperationalCase;
    }
    
    public static PricebookEntry newPricebookEntry(Pricebook2 pb, Product2 prd, Opportunity opp){        
        PricebookEntry pbEntry = new PricebookEntry();
        pbEntry.Product2Id = prd.Id;
        pbEntry.Pricebook2Id = pb.Id;
        pbEntry.UnitPrice = 200;
        pbEntry.IsActive = true;
        pbEntry.CurrencyIsoCode = opp.CurrencyIsoCode;
        return pbEntry;
    }
    
    public static QuoteLineItem newQuoteLineItem(Quote qt, PricebookEntry pbEntry){
        QuoteLineItem accessFee = new QuoteLineItem();
        accessFee.QuoteId = qt.Id;
        accessFee.PricebookEntryId = pbEntry.Id;
        accessFee.Product2Id = pbEntry.Product2Id;
        accessFee.Description = '';
        accessFee.Quantity = 100;
        accessFee.UnitPrice = pbEntry.UnitPrice;  
        
        return accessFee;
    }
    
    public static QuoteLineItem newQuoteLineItem(Quote qt, PricebookEntry pbEntry, decimal unitPrice){
        QuoteLineItem accessFee = new QuoteLineItem();
        accessFee.QuoteId = qt.Id;
        accessFee.PricebookEntryId = pbEntry.Id;
        accessFee.Product2Id = pbEntry.Product2Id;
        accessFee.Description = '';
        accessFee.Quantity = 90;
        accessFee.UnitPrice = unitPrice;  
        
        return accessFee;
    }
    
    
    public static Payment__c newPayment(Opportunity opp){
        Payment__c afp = new Payment__c();
        afp.RecordTypeId = Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Payment').getRecordTypeId();
        afp.Opportunity__c = opp.Id;
        afp.Percentage__c = 100;
        afp.Payment_Method__c = 'Wire Transfer';
        afp.PO_Required__c = 'Yes';
        afp.Frequency__c = 'Monthly';
        afp.Cutoff_Day__c = 1;   
        afp.Billing_Day__c = '05';  
        return afp;
    }
    
    
    public static Payment__c newPayment(QuoteLineItem qli){
        Payment__c afp = new Payment__c();
        afp.RecordTypeId = Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Payment').getRecordTypeId();
        afp.Quote_Line_Item__c = qli.Id;
        afp.Percentage__c = 100;
        afp.Payment_Method__c = 'Wire Transfer';
        afp.PO_Required__c = 'Yes';
        afp.Frequency__c = 'Monthly';
        afp.Cutoff_Day__c = 1;   
        afp.Billing_Day__c = '05';  
        return afp;
    }
    
    public static Payment__c newPayment(QuoteLineItem qli, String PaymentMethod){
        Payment__c afp = new Payment__c();
        afp.RecordTypeId = Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Payment').getRecordTypeId();
        afp.Quote_Line_Item__c = qli.Id;
        afp.Percentage__c = 100;
        afp.Payment_Method__c = PaymentMethod;
        afp.PO_Required__c = 'Yes';
        afp.Frequency__c = 'Monthly';
        afp.Cutoff_Day__c = 1;   
        afp.Billing_Day__c = '15';
        return afp;
    }
    
    public static Waiver__c newWaiver(Payment__c pay){
        Waiver__c w = new Waiver__c();
        w.Start_Date__c = System.today();
        w.End_Date__c = System.today().addDays(10);
        w.Consider_Contract_Start_Date__c = true;
        w.Percentage__c = 10;
        w.RecordTypeId = Schema.SObjectType.Waiver__c.getRecordTypeInfosByDeveloperName().get('Fixed_Date').getRecordTypeId();
        w.Payment__c = pay.Id;
        return w;
    }
    
    public static Waiver__c newWaiver(QuoteLineItem quoteLineItem, Integer duration, Integer position){
        Waiver__c waiver = new Waiver__c();
        waiver.Duration__c = duration;
        waiver.Position__c = position;
        waiver.Percentage__c = 10;
        waiver.Quote_Line_Item__c = quoteLineItem.Id;
        return waiver;
    }
    
    public static List<Waiver__c> createWaivers(QuoteLineItem quoteItem, Integer numberOfRecords){
        List<Waiver__c> listWaivers = new List<Waiver__c>();
        for(Integer i = 0; i<numberOfRecords; i++){
            listWaivers.add(DataFactory.newWaiver(quoteItem, 2, i + 1));
        }
        
        return listWaivers;
    }
    
    public static Eligibility__c newEligibility(Payment__c payment){
        Eligibility__c eli = new Eligibility__c();
        eli.Payment__c = payment.Id;
        eli.Name = 'Headquarters';
        eli.Communication_Restriction__c = '3. Accept communications with employees after sign up';
        eli.Is_Default__c = true;
        eli.Group_Name__c = 'Main';
        eli.Launch_Date__c = system.today().addDays(10);
        return eli;
    }
    
    public static Contact newContact(Account acc, String name){
        Contact contactTest = new Contact();
        contactTest.AccountId = acc.Id;
        contactTest.LastName = 'test'+name;
        contactTest.Email = 'test'+ name.replace(' ', '') +'@tst.com';
        return contactTest;
    }
    
    public static Account newGympassEntity( String accName ){
        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gympass_Entity').getRecordTypeId();
        Account acc = new Account();
        acc.name = accName;
        acc.RecordTypeId = rtId;
        acc.GP_Status__c = 'Active';
        acc.billingState = 'Florida';
        acc.CAP_Value__c = 120;
        acc.BillingCity = 'CityAcademiaBrasil';
        acc.billingStreet = 'Rua academiabrasilpai';
        acc.billingCountry = 'United States';
        acc.Gym_Type__c = 'Studios';
        acc.Gym_Classes__c = 'Cardio';
        acc.Types_of_ownership__c = Label.franchisePicklist;
        acc.Subscription_Type__c = 'Value per class';
        acc.Subscription_Period__c = 'Monthy value';
        acc.Subscription_Type_Estimated_Price__c    = 100;
        acc.Has_market_cannibalization__c = 'No';
        acc.Exclusivity__c = 'Yes';
        acc.Exclusivity_End_Date__c = Date.today().addYears(1);
        acc.Exclusivity_Partnership__c = 'Full Exclusivity';
        acc.Exclusivity_Restrictions__c= 'No';
        acc.Website = 'testing'+accName+'@tesapex.com';
        acc.Gym_Email__c = 'gymemail'+accName+'@apex.com';
        acc.Phone = '3222'+accName+'23123';
        acc.Can_use_logo__c = 'Yes';
        acc.Legal_Registration__c = 12123;
        acc.Legal_Title__c = 'Title LTDA'+accName;
        acc.Gyms_Identification_Document__c = 'CNPJ'; 
        return acc;
    }
    
    public static APXT_Redlining__Contract_Agreement__c newContractAgreement(Opportunity opp, Quote qt, Account acc  ){
        APXT_Redlining__Contract_Agreement__c ctAgreement = new APXT_Redlining__Contract_Agreement__c();        
        ctAgreement.Opportunity__c = opp.Id;
        ctAgreement.Proposal__c = qt.Id;
        ctAgreement.APXT_Redlining__Account__c = acc.Id;
        return ctAgreement;
    }
    
    /******************************************************************************************************************************/
    /* New objects */
    
    public static Eligibility__c newEligibility(Payment__c payment , boolean isFull){
        Eligibility__c ObjUpsert = new Eligibility__c();
        ObjUpsert.ownerid='0051L00000Dn9Q7QAJ' ; 
        ObjUpsert.name='Headquarters' ; 
        ObjUpsert.currencyisocode='USD' ; 
        ObjUpsert.communication_restriction__c='3. Accept communications with employees after sign up' ; 
        ObjUpsert.group_name__c='Main' ; 
        ObjUpsert.has_relevant_changes_for_offer_approval__c=false ; 
        ObjUpsert.has_relevant_changes_for_offer_deal_desk__c=false ; 
        ObjUpsert.is_default__c=true ;  
        ObjUpsert.payment_method__c='Payroll' ;  
        ObjUpsert.payment__c= payment.id ;  
        ObjUpsert.recurring_billing_period__c='Monthly' ; 
        return ObjUpsert;
    }
    
    public static APXT_Redlining__Contract_Agreement__c newContractAgreement(Id oppId , Id quoteId, Id accountId){
        
        APXT_Redlining__Contract_Agreement__c ctAgreement = new APXT_Redlining__Contract_Agreement__c();        
        ctAgreement.Opportunity__c = oppId;
        ctAgreement.Proposal__c = quoteId;
        ctAgreement.APXT_Redlining__Account__c = accountId;
        ctAgreement.APXT_Redlining__Agreement_Type__c = 'Master';
        ctAgreement.APXT_Redlining__Group__c = 'B2B - EMEA - Sales';
        ctAgreement.APXT_Redlining__Type__c = 'B2B Standard';
        ctAgreement.RecordTypeId = Schema.SObjectType.APXT_Redlining__Contract_Agreement__c.getRecordTypeInfosByDeveloperName().get('B2B_Contract').getRecordTypeId();
        ctAgreement.APXT_Redlining__Status__c = 'Active';
        ctAgreement.APXT_Renegotiation__c = false;
        return ctAgreement;
    }
    
    public static Case newCase(Id oppId, id qtId, String recordType){
        Case lOperationalCase = new Case();
        lOperationalCase.Description = 'isTest';
        lOperationalCase.OpportunityId__c = oppId;
        lOperationalCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get(recordType).getRecordTypeId();
        lOperationalCase.QuoteId__c = qtId;
        lOperationalCase.Subject = 'Deal Desk Operational Approval Request';        
        lOperationalCase.Justificate_No_Compliant_Topics__c = 'Justification Test';
        return lOperationalCase;
    }
    
    
    
    public static Opportunity newOpportunity(Id aAccId, Pricebook2 aPriceBook, String recordType, boolean isFull){
        
        Opportunity ObjUpsert = new Opportunity(); 
        ObjUpsert.accountid = aAccId; 
        ObjUpsert.recordtypeid=Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(recordType).getRecordTypeId(); 
        ObjUpsert.isprivate=false ; 
        ObjUpsert.name='Opptest' ; 
        ObjUpsert.stagename='Qualification' ; 
        ObjUpsert.CurrencyIsoCode='BRL';
        ObjUpsert.amount= 20955 ; 
        ObjUpsert.probability= 100 ; 
        ObjUpsert.totalopportunityquantity=1000.00 ; 
        ObjUpsert.closedate=Date.today(); 
        ObjUpsert.type='New Business' ; 
        ObjUpsert.forecastcategoryname='Closed' ; 
        ObjUpsert.pricebook2id= aPriceBook.Id ;  
        ObjUpsert.a_authority__c='Yes' ; 
        ObjUpsert.ch_challenges__c='Yes' ; 
        ObjUpsert.is_there_a_competitor__c='No' ; 
        ObjUpsert.decision_maker_has_been_identified__c=false ; 
        ObjUpsert.m_money__c='Yes' ; 
        ObjUpsert.country_manager_approval__c=false ; 
        ObjUpsert.emailsent__c=false ; 
        ObjUpsert.account_status_on_creation__c='Active' ; 
        ObjUpsert.adjusted_probability__c= 100 ; 
        ObjUpsert.standard_probability__c= 10 ; 
        ObjUpsert.forecast_type__c='Standard' ; 
        ObjUpsert.a_authority_opp_val__c='Yes' ; 
        ObjUpsert.ch_challenges_opp_val__c='Yes' ; 
        ObjUpsert.lid__is_influenced__c=false ; 
        ObjUpsert.m_money_opp_val__c='Yes' ; 
        ObjUpsert.oa_eligibility_file__c='Aligned and approved' ; 
        ObjUpsert.oa_it_data_security_review__c='Aligned and approved' ; 
        ObjUpsert.oa_launch_process__c='Already covered and aligned' ; 
        ObjUpsert.oa_legal_review__c='Aligned and approved' ; 
        ObjUpsert.oa_nda_in_place__c='Signed' ; 
        ObjUpsert.oa_payroll_deduction_set_up__c='Aligned and approved' ; 
        ObjUpsert.oa_supplier_registration__c='Not required' ; 
        ObjUpsert.oa_whitelisting__c='Aligned and approved' ; 
        ObjUpsert.os_eligibility_file__c='In discussion' ; 
        ObjUpsert.os_it_data_security_review__c='In discussion' ; 
        ObjUpsert.p_priority_opp_val__c='Yes' ; 
        ObjUpsert.os_legal_review__c='In discussion' ; 
        ObjUpsert.os_payroll_deduction_set_up__c='In discussion' ; 
        ObjUpsert.os_whitelisting__c='In discussion' ; 
        ObjUpsert.ov_payroll_deduction_set_up__c='In discussion' ; 
        ObjUpsert.does_it_have_unique_technical_request__c='No' ; 
        ObjUpsert.budget_confirmed__c=false ; 
        ObjUpsert.discovery_completed__c=false ; 
        ObjUpsert.roi_analysis_completed__c=false ; 
        ObjUpsert.marketplace_login__c=false ; 
        ObjUpsert.self_checkout__c=false ; 
        ObjUpsert.turbo_plan__c=false ; 
        ObjUpsert.exclusivity__c='Yes' ; 
        ObjUpsert.nda_has_been_signed__c=false ; 
        ObjUpsert.did_you_offer_gympass_w_for_your_client__c='Yes' ; 
        ObjUpsert.did_the_client_accept_it_or_not__c='No' ; 
        ObjUpsert.achieved_steps_towards_success_quantity__c=10 ; 
        ObjUpsert.please_describe_the_reasons_why__c='We have our own digital solution in place' ; 
        ObjUpsert.technical_call_has_been_completed_with_i__c=false ; 
        ObjUpsert.technical_feasibility_has_been_discussed__c=false ; 
        ObjUpsert.the_commercial_terms_have_been_approved__c=false ; 
        ObjUpsert.digital_incentive_applied__c=false ; 
        ObjUpsert.partner_has_agreed_to_share_information__c=false ; 
        ObjUpsert.partner_has_agreed_to_remove_users__c=false ; 
        ObjUpsert.agreed_to_integrate_with_our_platform__c=false ; 
        ObjUpsert.bank_account_related__c=false ; 
        ObjUpsert.number_of_locations_opportunity__c=0 ; 
        ObjUpsert.payment_approved__c=false ; 
        ObjUpsert.product_related__c=false ; 
        ObjUpsert.proposal_related__c=false ; 
        ObjUpsert.special_conditions_approved__c=false ; 
        ObjUpsert.offer_letter_sent__c=true ; 
        ObjUpsert.has_validation_errors__c=false ; 
        ObjUpsert.autonomous_marketplace_contract__c='No' ; 
        ObjUpsert.tax_form_w_9__c=false ; 
        ObjUpsert.banking_details_file__c=false ; 
        ObjUpsert.commercial_conditions_approval_needed__c=false ; 
        ObjUpsert.commercial_conditions_approved__c=false ;
        ObjUpsert.deal_desk_approval_needed__c=false ; 
        ObjUpsert.deal_desk_approved__c=false ; 
        //ObjUpsert.fasttrackstage__c='Qualification' ; 
        ObjUpsert.ismultipleentity__c=false ; 
        ObjUpsert.sales_channel__c='Direct' ; 
        ObjUpsert.no_won_opportunity_in_sf__c=false ; 
        ObjUpsert.quantity_offer_number_of_employees__c=500 ; 
        ObjUpsert.activity_related__c=false ; 
        ObjUpsert.customize_token_field_text_in_portal__c=false ; 
        ObjUpsert.engagement_journey_completed__c=true ; 
        ObjUpsert.launch_plan_presented_and_approved__c=false ; 
        ObjUpsert.send_a_welcome_email__c=false ; 
        ObjUpsert.specialist_approved__c=true ; 
        ObjUpsert.openinghours_related__c=false ; 
        ObjUpsert.distribution_email__c='gft.laerte.kimura@ext.gympass.com' ; 
        ObjUpsert.vendor_portal__c='Yes' ; 
        ObjUpsert.portal_address__c='http://www.google.com' ; 
        return ObjUpsert; 
    }
    
    public static Offer_Queue__c newOfferQueue(Id oppId, Id accountId){
        Offer_Queue__c offerQueue = new Offer_Queue__c();
        offerQueue.Type_of_Request__c = 'SMB Retention';
        offerQueue.Account_ID__c = accountId;
        offerQueue.Opportunity_ID__c = oppId;
        offerQueue.Start_Date__c = System.today();
        offerQueue.End_Date__c = System.today().addDays(10);
        offerQueue.Close_Date__c = System.today().addDays(5);
        offerQueue.Number_of_Employees__c = 80;
        offerQueue.Reference_Sales_Price_per_Eligible__c = 1;        
        offerQueue.ES_Payment_Frequency__c = 'Monthly';
        offerQueue.ES_Billing_Percentage__c = 100;
        offerQueue.ES_Payment_Method__c = 'Boleto';
        offerQueue.Stage__c = 'Offer Sent';
        offerQueue.Adjusted_Probability__c = 60;
        offerQueue.Eligible_List_Registration_Method__c = 'HR Portal';
        offerQueue.Unique_Identifier__c	 = 'Company ID';
        offerQueue.Membership_fee_Due_Dates__c = '15 days';
        offerQueue.Membership_fee_Billing_Day__c = '10';
        offerQueue.Membership_fee_Payment_Method__c = 'Payroll';
        offerQueue.Waiver_Start_Date__c = System.today();
        offerQueue.Waiver_End_Date__c = System.today().addDays(10);
        offerQueue.Waiver_Percentage__c = 100;
        offerQueue.Temporary_Discount_1_Start_Date__c	 = System.today();
        offerQueue.Temporary_Discount_1_End_Date__c = System.today().addDays(10);
        offerQueue.Temporary_Discount_1_Percentage__c = 11;
        offerQueue.Temporary_Discount_2_Start_Date__c	 = System.today();
        offerQueue.Temporary_Discount_2_End_Date__c = System.today().addDays(20);
        offerQueue.Temporary_Discount_2_Percentage__c = 22;
        offerQueue.Temporary_Discount_3_Start_Date__c	 = System.today();
        offerQueue.Temporary_Discount_3_End_Date__c = System.today().addDays(30);
        offerQueue.Temporary_Discount_3_Percentage__c = 33;
        offerQueue.Temporary_Discount_4_Start_Date__c	 = System.today();
        offerQueue.Temporary_Discount_4_End_Date__c = System.today().addDays(40);
        offerQueue.Temporary_Discount_4_Percentage__c = 44;
        offerQueue.Allowlist_Enabler__c = 'No';
        offerQueue.Free_Plan__c = 'No';
        offerQueue.Family_Member__c = 'Yes';
        offerQueue.Price_index__c = 'IPCA';
        offerQueue.Full_Launch__c = 'Yes';
        offerQueue.Exclusivity_clause__c = 'Yes';
        return offerQueue;
    }
    
    public static Pricebook2 newPricebook(String name, String businessModel){
        Pricebook2 pricebook = new Pricebook2(
            Name = name,
            Business_Model__c = businessModel,
            IsActive = true
        );
        return pricebook;
    } 
    
    public static Assert_Data__c newAssertData(Id oppId, String ObjectName){
        Assert_Data__c assertData = new Assert_Data__c();
        assertData.Opportunity__c = oppId;
        assertData.Object_Name__c = ObjectName;
        assertData.Old_Value__c = '';
        assertData.New_Value__c = '';
        return assertData;
    }
    
    public static Comission_Price__c newComissionPrice(Integer comissionPercent, Decimal price, Id productId, String currencyIsoCode){
        Comission_Price__c comissionPrice = new Comission_Price__c();
        comissionPrice.Comission_Percent__c = comissionPercent;
        comissionPrice.Price__c = price;
        comissionPrice.Product__c = productId;
        comissionPrice.CurrencyIsoCode = currencyIsoCode;
        return comissionPrice;
    }
    
    public static Account newAccountReseller(){
        ID accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Reseller').getRecordTypeId();
        Account acc = new Account();
        acc.RecordTypeId = accRecordTypeId;
        acc.Name = 'Account 1';
        acc.BillingCountry = 'Brazil';
        acc.BillingState = 'São Paulo';
        acc.NumberOfEmployees = 2000;
        acc.Id_Company__c = '81.700.429/0001-93';
        return acc;
    }  
    
    public static List<Approval_Item__c> newApprovalItems(Case cs){
        List<Approval_Item__c> approvalItems = new List<Approval_Item__c>();
        
        if(cs.RecordType.DeveloperName == 'Deal_Desk_Approval'){
            Approval_Item__c itemES_PaymentFrequency = new Approval_Item__c(
                Name = 'Enterprise Subscription payment frequency is custom',
                Value__c = 'Monthly',
                Approval_Status__c = 'Open',
                Case__c = cs.Id
            );
            approvalItems.add(itemES_PaymentFrequency);
            
            Approval_Item__c itemPS_SetupFee = new Approval_Item__c(
                Name = 'Professional Service Setup Fee price out of standard range',
                Value__c = '0.00',
                Approval_Status__c = 'Open',
                Case__c = cs.Id
            );
            approvalItems.add(itemPS_SetupFee);
            
            
        }else if(cs.RecordType.DeveloperName == 'Deal_Desk_Operational'){
            Approval_Item__c itemMFBillingDay = new Approval_Item__c(
                Name = 'MF Billing Day',
                Value__c = 'Custom - 605',
                Approval_Status__c = 'Open',
                Case__c = cs.Id
            );
            approvalItems.add(itemMFBillingDay);
            
            Approval_Item__c itemESBillingDay = new Approval_Item__c(
                Name = 'Enterprise Subscription Billing Day',
                Value__c = 'Custom - 67',
                Approval_Status__c = 'Open',
                Case__c = cs.Id
            );
            approvalItems.add(itemESBillingDay);
            
        }
        return approvalItems;
        
    }
    
    public static List<EventConfiguration__c> newEventConfigurations(Integer numberOfRecords){
        List<EventConfiguration__c> listEventConfigurations = new List<EventConfiguration__c>();
        for(Integer i = 0; i < numberOfRecords; i++){
            EventConfiguration__c eventConfig = new EventConfiguration__c(
                AuthType__c = 'TestAuthType' + i,
                CommandClassName__c = 'TestCommandClass' + i,
                DisableDispatcher__c = false,
                EndPointUrl__c = 'https://www.endpointUrl.com/' + i,
                Method__c = 'Post',
                Name = 'Endpoint Name' + i,
                OauthName__c = 'OAuth' + i,
                Username__c = 'TestUsername' + i,
                Password__c = 'Password' + i
            );
            
            listEventConfigurations.add(eventConfig);
        }
        
        return listEventConfigurations;
    }
    
    public static SKU_Price__c newSKUPrice(Id prodId, Opportunity opp, Account acc, Integer minQuantity, Decimal unitPrice){
        SKU_Price__c skuPrice = new SKU_Price__c();
        skuPrice.Product__c = prodId;
        skuPrice.Start_Date__c = Date.today();
        skuPrice.End_Date__c = Date.today().addYears(1);
        skuPrice.Minimum_Quantity__c = minQuantity;
        skuPrice.CountrySKU__c = acc.BillingCountry;
        skuPrice.CurrencyIsoCode = opp.CurrencyIsoCode;
        skuPrice.Unit_Price__c = unitPrice;
        return skuPrice;
    }
    
    public static Legacy_Partner_Price__c newLegacyPartnerPrice(String name, String currencyIsoCode, Id idAccount, String businessModel){
        Legacy_Partner_Price__c legacyPartnerPrice = new Legacy_Partner_Price__c();
		legacyPartnerPrice.Name = name;
        legacyPartnerPrice.CurrencyIsoCode = currencyIsoCode;
        legacyPartnerPrice.Account__c = idAccount;
        legacyPartnerPrice.Business_Model__c = businessModel;
        legacyPartnerPrice.Legacy_Price__c = 10;
        legacyPartnerPrice.isActive__c = true;
        return legacyPartnerPrice;
    }
    
}