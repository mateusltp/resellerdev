public class SubManagedClausesTriggerHelper {

    public void setLengthOfRichTextField(List<SubManagedClauses__c> SubManagedClauseList ){
        for(SubManagedClauses__c SubClause : SubManagedClauseList){
            SubClause.LengthOfText__c = 0;
            if (SubClause.Text__c != null ) {
                SubClause.LengthOfText__c = SubClause.Text__c.length();
            } 
        }
    }
}