public without sharing class OpportunityTriggerCommercialConditions {
    
    private List<Opportunity> m_lstDataNew;
    private Map<Id, Opportunity> m_mapDataOld;   
    private Map<Id, Opportunity> m_mapObjFiltered = new Map<Id, Opportunity>();
    public static Id OPPORTUNITY_RECORD_TYPE_NEW_BUS_INDIRECT_CHANNEL = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Indirect_Channel_New_Business').getRecordTypeId();
    
    QuoteRepository quoteRepository = new QuoteRepository();
    QuoteLineItemRepository quoteLineItemRepository = new QuoteLineItemRepository();
    //ResellerSKUEditController resellerSKUEditController = new ResellerSKUEditController();
    
    public OpportunityTriggerCommercialConditions(List<Opportunity> lstDataNew, Map<Id, Opportunity> mapDataOld) {
        this.m_lstDataNew = lstDataNew;
        this.m_mapDataOld = mapDataOld;
    }
    
    private void filter() {
        for(Opportunity itemNew : this.m_lstDataNew) {
            Opportunity itemOld = this.m_mapDataOld.get(itemNew.Id);
            if(itemNew.RecordTypeId == OPPORTUNITY_RECORD_TYPE_NEW_BUS_INDIRECT_CHANNEL &&
                itemOld.FastTrackStage__c == 'Offer Creation' &&
                itemNew.FastTrackStage__c == 'Offer Review'
            ){
                m_mapObjFiltered.put(itemNew.Id, itemNew);
            }

        }
    } 
    
    public void run(){
        
        filter();
        
        if(!m_mapObjFiltered.isEmpty()){
            updateWaiverPlanBuilder(m_mapObjFiltered);
            new ResellerSkuOfferReviewController().evaluateEnablers(m_mapObjFiltered);
        }
        
    }
    
    public void updateWaiverPlanBuilder(Map<Id, Opportunity> m_mapObjFiltered){
        
        String quoteId;
        String oppId;
        
        for(Opportunity opp : m_mapObjFiltered.values()){           
            quoteId = opp.SyncedQuoteId;
            oppId = opp.id;                
        }

        List<Waiver__c> lWaiver = [ SELECT Id,Approval_Status_Formula__c, Percentage__c, Duration__c,Approval_Status__c,Quote_Line_Item__r.Quote.Waiver_Approval_Info__c,Quote_Line_Item__r.Quote.OpportunityId FROM Waiver__c where Quote_Line_Item__r.QuoteId =: quoteId];
        Quote quote = quoteRepository.lastForOpportunity(oppId); 
        if(lWaiver.size() > 0){
            for(Waiver__c item : lWaiver){
                if(item.Approval_Status__c != 'Approved'){
                    quote.Waiver_approved__c = false;
                    break;
                }else{
                    quote.Waiver_approved__c = true;
                }              
            }
        }else{
            quote.Waiver_approved__c = true;
        }
        system.debug('##### '+quote.Waiver_approved__c);
        update quote;
    }
}