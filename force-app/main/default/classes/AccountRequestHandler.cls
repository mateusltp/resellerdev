public with sharing class AccountRequestHandler extends TriggerHandler {

    private AccountRequestBO bo{get;set;}
    private ResellerAccount account{get;set;}
    private ResellerCreateOpportunityGO accountRequests{get;set;}
    
    public AccountRequestHandler(){
        this.bo = new AccountRequestBO();
        this.account = new ResellerAccount();
        this.accountRequests = new ResellerCreateOpportunityGO();
    }

    public override void beforeInsert(){
        account.run(Trigger.new, false);
    }
    
    public override void afterInsert(){}

    public override void beforeUpdate(){
        bo.runGoNoGoSetAccount();   
        account.run(Trigger.new, true);
        accountRequests.run(Trigger.new);
    }
    
    public override void afterUpdate(){
    	
    }
    
}