/**
 * @description       :
 * @Test Classes      : ProductAssignmentServiceTest
 * @author            : alysson.mota@gympass.com
 * @group             : 
 * @last modified on  : 04-07-2022
 * @last modified by  : alysson.mota@gympass.com
**/
public with sharing class CommercialConditionService {
    private static Id CapCommConditionRecType = Schema.SObjectType.Commercial_Condition__c.getRecordTypeInfosByDeveloperName().get('CAP').getRecordTypeId();
    String className = CommercialConditionService.class.getName();
    PS_Constants constants = PS_Constants.getInstance();

    /**
    * Runs on trigger after update event and calls all the necessary actions 
    * @author alysson.mota@gympass.com | 02-22-2022 
    * @param idToNewCommCondition 
    * @param idToOldCommCondition 
    **/
    public void evaluateAndSyncCommConditions(Map<Id, Commercial_Condition__c> idToNewCommCondition, Map<Id, Commercial_Condition__c> idToOldCommCondition) {
        fflib_ISObjectUnitOfWork uow = Application.UnitOfWork.newInstance();
        
        try {
            checkAndSyncCommConditions(uow, idToNewCommCondition, idToOldCommCondition);

            uow.commitWork();
        } catch(Exception e) {
            createLog(e, 'evaluateAndSyncCommConditions');
        }
    }

    /**
    * Check and sync commercial conditions with the cap commercial condition if necessary
    * @author alysson.mota@gympass.com | 02-22-2022 
    * @param uow 
    * @param idToNewCommCondition 
    * @param idToOldCommCondition 
    **/
    private void checkAndSyncCommConditions(fflib_ISObjectUnitOfWork uow, Map<Id, Commercial_Condition__c> idToNewCommCondition, Map<Id, Commercial_Condition__c> idToOldCommCondition) {
        Map<Id, Commercial_Condition__c> auxIdToNewCapCommCondition = new Map<Id, Commercial_Condition__c>();
        Map<Id, Commercial_Condition__c> auxIdToNewNonCapCommCondition = new Map<Id, Commercial_Condition__c>();
        
        for (Commercial_Condition__c newCommCondition : idToNewCommCondition.values()) {
            Commercial_Condition__c oldCommCondition = idToOldCommCondition.get(newCommCondition.Id);

            if (newCommCondition.RecordTypeId == CapCommConditionRecType) {
                if (newCommCondition.Visits_to_CAP__c   != oldCommCondition.Visits_to_CAP__c   
                    || newCommCondition.CAP_Discount__c != oldCommCondition.CAP_Discount__c) {
                    auxIdToNewCapCommCondition.put(newCommCondition.Id, newCommCondition);
                }
            } else {
                if (newCommCondition.Fee_Percentage__c  != oldCommCondition.Fee_Percentage__c) {
                    auxIdToNewNonCapCommCondition.put(newCommCondition.Id, newCommCondition);
                }
            }
        }

        if (auxIdToNewCapCommCondition.size() > 0) {
            syncNonCapWithCap(uow, auxIdToNewCapCommCondition);
            syncCapFromChildProducts(uow, auxIdToNewCapCommCondition);
        }

        if (auxIdToNewNonCapCommCondition.size() > 0) {
            syncNonCapFromChildProducts(uow, auxIdToNewNonCapCommCondition);
        }
    }

    /**
    * Copy values from Comm Conditions from the Parent Product to the Comm Conditions that have the child products
    * @author alysson.mota@gympass.com | 02-22-2022 
    * @param uow 
    * @param idToNewCommCondition 
    **/
    public void syncNonCapFromChildProducts(fflib_ISObjectUnitOfWork uow, Map<Id, Commercial_Condition__c> idToNewCommCondition) {
        Set<Id> prodIdSet       = new Set<Id>();
        Set<Id> commCondIdSet   = new Set<Id>();

        Map<Id, Product_Assignment__c> parentProdIdToParentPa               = new Map<Id, Product_Assignment__c>();
        Map<Id, List<Product_Assignment__c>> parentProdIdToChildCapPaLst    = new Map<Id, List<Product_Assignment__c>>();

        CommercialConditionSelector commCondSelector    = new CommercialConditionSelector();
        ProductAssignmentSelector paSelector            = new ProductAssignmentSelector();

        List<Product_Assignment__c> paLst = paSelector.selectByCommConditionId(idToNewCommCondition.keySet());

        for (Product_Assignment__c pa : paLst) {
            prodIdSet.add(pa.ProductId__c);
            parentProdIdToParentPa.put(pa.ProductId__c, pa);
        }

        List<Product_Assignment__c> childPaLst = paSelector.selectNonCapByParentProdId(parentProdIdToParentPa.keySet());

        for (Product_Assignment__c pa : childPaLst) {
            commCondIdSet.add(pa.CommercialConditionId__c);

            if (parentProdIdToChildCapPaLst.containsKey(pa.ProductId__r.Parent_Product__c)) {
                parentProdIdToChildCapPaLst.get(pa.ProductId__r.Parent_Product__c).add(pa);
            } else {
                parentProdIdToChildCapPaLst.put(pa.ProductId__r.Parent_Product__c, new List<Product_Assignment__c>{pa});
            }
        }

        Map<Id, Commercial_Condition__c> idToChildComms = new Map<Id, Commercial_Condition__c>(
            commCondSelector.selectCommConditionByID(commCondIdSet)
        );

        for (Product_Assignment__c pa : paLst) {
            Commercial_Condition__c parentCommCond = idToNewCommCondition.get(pa.CommercialConditionId__c);
            List<Product_Assignment__c> auxChildPaLst = parentProdIdToChildCapPaLst.get(pa.ProductId__c);

            if (parentCommCond != null && auxChildPaLst != null) {
                for (Product_Assignment__c childPa : auxChildPaLst) {
                    Commercial_Condition__c childCommCond = idToChildComms.get(childPa.CommercialConditionId__c);

                    if (childCommCond != null && (childCommCond.RecordTypeId == parentCommCond.RecordTypeId)) {
                        childCommCond.Fee_Percentage__c = parentCommCond.Fee_Percentage__c;
                        uow.registerDirty(childCommCond);
                    }
                }
            }
        }
    }
    
    /**
    * Copy values from CAP Comm Conditions from the Parent Product to the CAP Comm Conditions that have the child productsCopy values that 
    * @author alysson.mota@gympass.com | 02-22-2022 
    * @param uow 
    * @param idToNewCommCondition 
    **/
    public void syncCapFromChildProducts(fflib_ISObjectUnitOfWork uow, Map<Id, Commercial_Condition__c> idToNewCommCondition) {
        Set<Id> prodIdSet       = new Set<Id>();
        Set<Id> commCondIdSet   = new Set<Id>();

        Map<Id, Product_Assignment__c> parentProdIdToParentCapPa            = new Map<Id, Product_Assignment__c>();
        Map<Id, List<Product_Assignment__c>> parentProdIdToChildCapPaLst    = new Map<Id, List<Product_Assignment__c>>();

        CommercialConditionSelector commCondSelector    = new CommercialConditionSelector();
        ProductAssignmentSelector paSelector            = new ProductAssignmentSelector();

        List<Product_Assignment__c> paLst = paSelector.selectByCommConditionId(idToNewCommCondition.keySet());

        for (Product_Assignment__c pa : paLst) {
            prodIdSet.add(pa.ProductId__c);
            parentProdIdToParentCapPa.put(pa.ProductId__c, pa);
        }

        List<Product_Assignment__c> childPaLst = paSelector.selectCapByParentProdId(parentProdIdToParentCapPa.keySet());

        for (Product_Assignment__c pa : childPaLst) {
            commCondIdSet.add(pa.CommercialConditionId__c);

            if (parentProdIdToChildCapPaLst.containsKey(pa.ProductId__r.Parent_Product__c)) {
                parentProdIdToChildCapPaLst.get(pa.ProductId__r.Parent_Product__c).add(pa);
            } else {
                parentProdIdToChildCapPaLst.put(pa.ProductId__r.Parent_Product__c, new List<Product_Assignment__c>{pa});
            }
        }

        Map<Id, Commercial_Condition__c> idToChildComms = commCondSelector.selectCommConditionByIdAsMap(commCondIdSet);

        for (Product_Assignment__c pa : paLst) {
            Commercial_Condition__c parentCommCond = idToNewCommCondition.get(pa.CommercialConditionId__c);
            List<Product_Assignment__c> auxChildPaLst = parentProdIdToChildCapPaLst.get(pa.ProductId__c);

            if (parentCommCond != null && auxChildPaLst != null) {
                for (Product_Assignment__c childPa : auxChildPaLst) {
                    Commercial_Condition__c childCommCond = idToChildComms.get(childPa.CommercialConditionId__c);

                    if (childCommCond != null) {
                        childCommCond.Visits_to_CAP__c = parentCommCond.Visits_to_CAP__c;
                        childCommCond.CAP_Discount__c = parentCommCond.CAP_Discount__c;
                        uow.registerDirty(childCommCond);
                    }
                }
            }
        }
    }

    /**
    * Copy values from CAP Comm Conditions to Non CAP Comm conditions (Late Cancel, No Show)
    * @author alysson.mota@gympass.com | 02-22-2022 
    * @param uow 
    * @param idToNewCommCondition 
    **/
    public void syncNonCapWithCap(fflib_ISObjectUnitOfWork uow, Map<Id, Commercial_Condition__c> idToNewCommCondition) {
        Set<Id> productIdSet    = new Set<Id>();
        Set<Id> commCondIdSet   = new Set<Id>();

        CommercialConditionSelector commCondSelector    = new CommercialConditionSelector();
        ProductAssignmentSelector paSelector            = new ProductAssignmentSelector();

        Map<Id, Commercial_Condition__c>        prodIdToCapCommCond     = new Map<Id, Commercial_Condition__c>();
        Map<Id, List<Commercial_Condition__c>>  prodIdToNonCapCommCond  = new Map<Id, List<Commercial_Condition__c>>();
        Map<Id, List<Product_Assignment__c>>    prodIdToNonCapPa        = new Map<Id, List<Product_Assignment__c>>();

        List<Product_Assignment__c> capPaLst = paSelector.selectByCommConditionId(idToNewCommCondition.keySet());
        
        for (Product_Assignment__c pa : capPaLst) {
            Commercial_Condition__c capCommCond = idToNewCommCondition.get(pa.CommercialConditionId__c);
            if (capCommCond != null) {
                prodIdToCapCommCond.put(pa.ProductId__c, capCommCond);
                productIdSet.add(pa.ProductId__c);
            } 
        }

        Map<Id, Product_Assignment__c> nonCapProdAssignments = paSelector.selectNonCapByProdId(productIdSet);
        for (Product_Assignment__c nonCapPa : nonCapProdAssignments.values()) {
            if (prodIdToNonCapPa.containsKey(nonCapPa.ProductId__c)) {
                prodIdToNonCapPa.get(nonCapPa.ProductId__c).add(nonCapPa);
            } else {
                prodIdToNonCapPa.put(nonCapPa.ProductId__c, new List<Product_Assignment__c>{nonCapPa});
            }
        }

        for (Product_Assignment__c pa : nonCapProdAssignments.values()) {
            commCondIdSet.add(pa.CommercialConditionId__c);
        }
        
        Map<Id, Commercial_Condition__c> nonCapCommCondMap = new Map<Id, Commercial_Condition__c>(
            commCondSelector.selectCommConditionByID(commCondIdSet)
        );
        
        for (Id prodId : prodIdToNonCapPa.keySet()) {
            Commercial_Condition__c capCommCond = prodIdToCapCommCond.get(prodId);
            List<Product_Assignment__c> nonCapPaLst = prodIdToNonCapPa.get(prodId);

            if (capCommCond != null && nonCapPaLst != null) {
                for (Product_Assignment__c pa : nonCapPaLst) {
                    Commercial_Condition__c nonCapCommCond = nonCapCommCondMap.get(pa.CommercialConditionId__c);
                    if (nonCapCommCond != null) {
                        nonCapCommCond.Visits_to_CAP__c = capCommCond.Visits_to_CAP__c;
                        nonCapCommCond.CAP_Discount__c  = capCommCond.CAP_Discount__c;
                        uow.registerDirty(nonCapCommCond);
                    }
                }
            }
        }
    }

    private void createLog(Exception e, String origin) {
        DebugLog__c log = new DebugLog__c(
            Origin__c = '['+className+']['+origin+']',
            LogType__c = constants.LOGTYPE_ERROR,
            RelatedToSquad__c = constants.LOGTYPE_SQUAD_PARTNERS,
            ExceptionMessage__c = e.getMessage(),
            StackTrace__c = e.getStackTraceString()
        );
        Logger.createLog(log);
    }
}