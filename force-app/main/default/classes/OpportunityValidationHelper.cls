/* Contains validation rules for Opportunity object */
public without sharing class OpportunityValidationHelper {
    // Record Types Id
    Id CliSalesNewBusinessRtId  = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
    Id CliSuccessRenegRtId      = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Success_Renegotiation').getRecordTypeId();
    Id SmbNewBusinessRtId       = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SMB_New_Business').getRecordTypeId();
    Id SmbSuccessRenegRtId      = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SMB_Success_Renegotiation').getRecordTypeId();

    // Related data
    Map<Id, Account> idToAcc = null;

    // Error Messages
    public static String smbNewBusinessInsertError = 'You are trying to create a Opportunity of type SMB New Business for a Account with more than 1000 employees. Please, create an Opportunity of record type Client Sales New Business';
    public static String smbRenegInsertError = 'You are trying to create a Opportunity of type SMB Success Renegotiation for a Account with more than 1000 employees. Please, create an Opportunity of record type Client Success Renegotiation';
    public static String smbNewBusinessUpdateError = 'It is not possible to assign to the field Quantity (Offer Number of Employees) a value higher than 1000 for a SMB New Business Opportunity. Please, create an Opportunity of record type Client Sales New Business for a negotiation for more than 1000 employees';
    public static String smbRenegUpdateError = 'It is not possible to assign to the field Quantity (Offer Number of Employees) a value higher than 1000 for a SMB Success Renegotiation. Please, create an Opportunity of record type Client Success Renegotiation for a renegotiation for more than 1000 employees';


    public void doValidationsOnBeforeInsert(List<Opportunity> oppList) {
        retrieveRelatedData(oppList);
        validateSmbOppCreation(oppList);        
    }

    public void doValidationsOnBeforeUpdate(List<Opportunity> oppList) {
        validateSmbQuantityOfferEmployees(oppList);
    }

    private void validateSmbOppCreation(List<Opportunity> oppList) {
        for (Opportunity opp : oppList) {
            Account acc = idToAcc.get(opp.AccountId);

            if (opp.RecordTypeId == SmbNewBusinessRtId && acc.NumberOfEmployees > 1000) {
                opp.addError(smbNewBusinessInsertError);
            }

            if (opp.RecordTypeId == SmbSuccessRenegRtId && acc.NumberOfEmployees > 1000) {
                opp.addError(smbRenegInsertError);       
            }
        }
    }

    private void retrieveRelatedData(List<Opportunity> oppList) {
        retrieveAccounts(oppList);
    }

    private void retrieveAccounts(List<Opportunity> oppList) {
        Set<Id> accIdSet = new Set<Id>();
        
        for (Opportunity opp : oppList)
            accIdSet.add(opp.AccountId);
        
        idToAcc = new AccountRepository().getMapByIds(accIdSet); 
    }

    private void validateSmbQuantityOfferEmployees(List<Opportunity> oppList) {
        for (Opportunity newOpp : oppList) {
            if (newOpp.RecordTypeId == SmbNewBusinessRtId && newOpp.Quantity_Offer_Number_of_Employees__c > 1000) {
                newOpp.addError(smbNewBusinessUpdateError);
            }

            if (newOpp.RecordTypeId == SmbSuccessRenegRtId && newOpp.Quantity_Offer_Number_of_Employees__c > 1000) {
                newOpp.addError(smbRenegUpdateError);       
            }
        }
    }
}