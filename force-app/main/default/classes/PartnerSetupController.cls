/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 04-28-2022
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/

public with sharing class PartnerSetupController {
    
    private static final PS_Constants constants = PS_Constants.getInstance(); 
    private static final String className = PartnerSetupController.class.getName();


    @AuraEnabled(cacheable=false)
    public static void createOpsFormForContractAccounts(Id opportunityId){      
        try {
            AccountContractAgreementRelSelector accountContractSelector = (AccountContractAgreementRelSelector)Application.Selector.newInstance(Account_Contract_Agreement_Relationship__c.sobjectType); 
            Map<Id, List<Account_Contract_Agreement_Relationship__c>> accsInContract = accountContractSelector.selectByOpportunityId(new Set<Id>{opportunityId});
            if(!accsInContract.isEmpty()){
                Set<id> accountIds = new Set<Id>();
                for (Account_Contract_Agreement_Relationship__c iAcc : accsInContract.get( opportunityId )  ){
                    if(iacc.ContractAgreementId__c != null){
                        accountIds.add(iAcc.AccountId__r.Id);
                    }                    
                }
                System.debug('::accountIds ' + accountIds);
                OpsSetupFormService.createOpsFormForAccounts(opportunityId, accountIds, constants.OPS_SETUP_FORM_RT_PARTNER);
            }

        } catch (Exception e) {
            System.debug('>>> error >>>> ' + e.getStackTraceString());        
            throw new PartnerSetupControllerException( e.getMessage() );
        }
    }

    @AuraEnabled(cacheable=true)
    public static List<SetupPartner> getAccountsFromContract(Id opportunityId){
        try{
            AccountContractAgreementRelSelector accountContractSelector = (AccountContractAgreementRelSelector)Application.Selector.newInstance(Account_Contract_Agreement_Relationship__c.sobjectType); 
            FileObjectSelector fileObjSelector = (FileObjectSelector)Application.Selector.newInstance(FIleObject__c.sobjectType);
            AccountBankAccountSelector bankAccountRelSelector = (AccountBankAccountSelector)Application.Selector.newInstance(Acount_Bank_Account_Relationship__c.sobjectType);
            OpeningHourSelector openingHoursSlc = (OpeningHourSelector)Application.selector.newInstance(Opening_Hours__c.sObjectType);  
            Map<Id, List<Account_Contract_Agreement_Relationship__c>> accsInContract = accountContractSelector.selectByOpportunityId(new Set<Id>{opportunityId});
            List<SetupPartner> accSetupLst = new List<SetupPartner>();
            Map<String, String> mapFileParamValues = new Map<String, String>();
            for (File_Setting__mdt fileSetting : [SELECT Id, Parameter_Name__c, Parameter_Value__c  FROM   File_Setting__mdt WHERE MasterLabel LIKE '%Partner%']){
                mapFileParamValues.put(fileSetting.Parameter_Name__c, fileSetting.Parameter_Value__c);
            }
            System.debug(':: accsInContract ' + accsInContract);
            if(!accsInContract.isEmpty()){
                Set<id> accountIds = new Set<Id>();
                for (Account_Contract_Agreement_Relationship__c iAcc : accsInContract.get( opportunityId )  ){
                    accountIds.add(iAcc.AccountId__r.Id);
                }

                Map<Id, List<FIleObject__c>> fileLst = fileObjSelector.selectByAccountId( accountIds );
                Map<Id, Acount_Bank_Account_Relationship__c> bankAccountByAccountLst = bankAccountRelSelector.selectBankAccountByAccountId(accountIds);
                Map<Id, Opening_Hours__c> openHoursLst = openingHoursSlc.selectAllByAccountId(accountIds);
                for ( Account_Contract_Agreement_Relationship__c iAcc : accsInContract.get(opportunityId)  ){
                    List<FIleObject__c> fileObjLst = fileLst.get(iAcc.AccountId__r.Id) != null ? fileLst.get(iAcc.AccountId__r.Id) : new List<FIleObject__c>();
                    Acount_Bank_Account_Relationship__c bankAccount = bankAccountByAccountLst.get(iAcc.AccountId__r.Id) != null ? bankAccountByAccountLst.get(iAcc.AccountId__r.Id) : new Acount_Bank_Account_Relationship__c();
                    Opening_Hours__c openHour = openHoursLst.get(iAcc.AccountId__r.Id) != null ? openHoursLst.get(iAcc.AccountId__r.Id) : new Opening_Hours__c();
                    accSetupLst.add(new SetupPartner(iAcc, fileObjLst, bankAccount, openHour, mapFileParamValues));
                }
            }      
            return accSetupLst;
        } catch(Exception e){            
            throw new PartnerSetupControllerException( e.getMessage() );
        }
     
    }

    @AuraEnabled(cacheable=false)
    public static void saveAccountRecords(String partnerRecords){
        fflib_ISObjectUnitOfWork uow = Application.UnitOfWork.newInstance(); 
        try{
            List<Account> accs2UpdLst = new List<Account>();
            for(SetupPartner iPartner: (List<SetupPartner>) JSON.deserialize(partnerRecords, List<SetupPartner>.class)){
                Account acc = new Account();
                acc.Id = iPartner.accountId;
                acc.Gym_Title_on_Gympass_App__c = iPartner.partnerName;
                acc.Website = iPartner.partnerWebsite;
                acc.Gym_Email__c = iPartner.partnerEmail;
                acc.Phone = iPartner.partnerPhone;
                acc.Validation_type__c = iPartner.partnerValidationType;
                accs2UpdLst.add(acc);
            }
            uow.registerDirty(accs2UpdLst);
            uow.commitWork();
        } catch(Exception e){
            DebugLog__c log = new DebugLog__c(
                Origin__c = '['+className+'][saveAccountRecords]',
                LogType__c = constants.LOGTYPE_ERROR,
                RelatedToSquad__c = constants.LOGTYPE_SQUAD_PARTNERS,
                ExceptionMessage__c = e.getMessage(),
                StackTrace__c = e.getStackTraceString(),
                DataScope__c = JSON.serialize(partnerRecords)
            );
            Logger.createLog(log);
            throw new PartnerSetupControllerException(e.getMessage());
        }
      
    }

    @AuraEnabled(cacheable=false)
    public static void deleteFiles( String files, Boolean deleteOldFiles ){
        fflib_ISObjectUnitOfWork uow = Application.UnitOfWork.newInstance(); 
        try{
            List<ContentDocument> newContentDocLst = new List<ContentDocument>();
            Set<Id> oldContentVersionIds = new Set<Id>();
            Set<Id> accountIds = new Set<Id>();
            for(SetupFile iFile : (List<SetupFile>) JSON.deserialize(files, List<SetupFile>.class)){
                System.debug(':: iFile ' + iFile);
                ContentDocument contDoc = new ContentDocument();
                contDoc.Id = iFile.documentId;
                if(contDoc.Id != null){
                    newContentDocLst.add(contDoc);                
                } else if(deleteOldFiles){
                    oldContentVersionIds.add(iFile.contentVersionId);
                    accountIds.add(iFile.entityId);
                }
            }
            if(!oldContentVersionIds.isEmpty()){
                ContentVersionSelector contentVersionSlc = (ContentVersionSelector)Application.selector.newInstance(ContentVersion.sObjectType);
                Set<Id> contentDocIds = new Set<Id>();
                for(ContentVersion cv : contentVersionSlc.selectById(oldContentVersionIds)){
                    contentDocIds.add(cv.ContentDocumentId);
                }
                if(!contentDocIds.isEmpty()){
                    ContentDocumentLinkSelector contentLinkSlc = (ContentDocumentLinkSelector)Application.selector.newInstance(ContentDocumentLink.sObjectType);
                    List<ContentDocumentLink> contentDocumentLinkLst = new List<ContentDocumentLink>();
                    for(ContentDocumentLink ctdLink : contentLinkSlc.selectByContentDocumentId( contentDocIds )){
                        if( ctdLink.ShareType == 'V' ){
                            if(accountIds.contains(ctdLink.LinkedEntityId) && contentDocIds.contains(ctdLink.ContentDocumentId)){
                                contentDocumentLinkLst.add(ctdLink);
                            }                           
                        }
                    }
                   uow.registerDeleted(contentDocumentLinkLst); 
                } 
            }    

            if(!newContentDocLst.isEmpty()){
                uow.registerDeleted(newContentDocLst);   
            }         
            uow.commitWork();
        } catch(Exception e){
            DebugLog__c log = new DebugLog__c(
                Origin__c = '['+className+'][deleteFiles]',
                LogType__c = constants.LOGTYPE_ERROR,
                RelatedToSquad__c = constants.LOGTYPE_SQUAD_PARTNERS,
                ExceptionMessage__c = e.getMessage(),
                StackTrace__c = e.getStackTraceString(),
                DataScope__c = JSON.serialize(files)
            );
            Logger.createLog(log);
            System.debug('::delete files ' + e.getStackTraceString());
            throw new PartnerSetupControllerException(e.getMessage());
        }
    }

    @AuraEnabled(cacheable=false)
    public static void updatePartnerFromSetup(String partnersToUpdate, List<Id> selectedRecords){
        fflib_ISObjectUnitOfWork uow = Application.UnitOfWork.newInstance(); 
        System.debug('::partnersToUpdate ' + partnersToUpdate);
        try{
            UpdatedPartner aPartner = (UpdatedPartner) JSON.deserialize(partnersToUpdate, UpdatedPartner.class);
            System.debug( '::aPartner ' + aPartner );
            if(aPartner.partner != null){ updateParnerAccount(aPartner.partner, selectedRecords, uow); }
            if(aPartner.bankAccount != null){ updateBankAccount(aPartner.bankAccount, selectedRecords, uow); }
            if(aPartner.openingHours != null){ updateOpeningHours(aPartner.openingHours, selectedRecords, uow); }
            if(!aPartner.files.isEmpty()){ updateFiles(aPartner.files, selectedRecords, uow); }

            TriggerHandler.bypass('ContentDocumentLinkTriggerHandler');
            TriggerHandler.bypass('ContentVersionTriggerHandler');   
            TriggerHandler.bypass('ContentDocumentFileTriggerHandler');      
            uow.commitWork();
            TriggerHandler.clearAllBypasses();

        } catch(Exception e){
            DebugLog__c log = new DebugLog__c(
                Origin__c = '['+className+'][updatePartnerFromSetup]',
                LogType__c = constants.LOGTYPE_ERROR,
                RelatedToSquad__c = constants.LOGTYPE_SQUAD_PARTNERS,
                ExceptionMessage__c = e.getMessage(),
                StackTrace__c = e.getStackTraceString(),
                DataScope__c = JSON.serialize(partnersToUpdate)
            );
            Logger.createLog(log);
            System.debug('e.getStackTraceString() ' + e.getStackTraceString());
            throw new PartnerSetupControllerException( e.getMessage() );
        }
      
    }

    private static void updateParnerAccount(PartnerDetail partnerDetail, List<Id> partnersToUpdate, fflib_ISObjectUnitOfWork uow){
        List<Account> accountLst = new List<Account>();         
        if(!partnersToUpdate.isEmpty()){
            for(Id accountId : partnersToUpdate){              
                Account partnerAccount = partnerDetail.fields.clone(false, false, false, false); 
                partnerAccount.Id = accountId;
                accountLst.add(partnerAccount);
            }
        } else{
            Account partnerAccount = partnerDetail.fields;
            accountLst.add(partnerAccount);
        }
        uow.registerDirty(accountLst);
    }

    private static void updateBankAccount(BankAccountDetail bankAccountDetail, List<Id> partnersToUpdate, fflib_ISObjectUnitOfWork uow){
        Id bankAccountRtId =  Schema.SObjectType.Bank_Account__c.getRecordTypeInfosByDeveloperName().get(constants.BANK_ACCOUNT_PARTNER_RT).getRecordTypeId();
        Id bankAccountRelRtId =  Schema.SObjectType.Acount_Bank_Account_Relationship__c.getRecordTypeInfosByDeveloperName().get(constants.BANK_ACCOUNT_REL_PARTNER_RT).getRecordTypeId();
        AccountBankAccountSelector bankAccountRelSelector = (AccountBankAccountSelector)Application.Selector.newInstance(Acount_Bank_Account_Relationship__c.sobjectType);
        Set<Id> accountIds = new Set<Id>();
        accountIds.addAll(partnersToUpdate);


        if( bankAccountDetail.fields.Bank_Account_Number_or_IBAN__c == null || String.isEmpty(bankAccountDetail.fields.Bank_Account_Number_or_IBAN__c) || 
            bankAccountDetail.fields.Bank_Account_Ownership__c == null || String.isEmpty(bankAccountDetail.fields.Bank_Account_Ownership__c ) ){
            return;
        }


        if( bankAccountDetail.accountId != null && !String.isEmpty(bankAccountDetail.accountId)){
            accountIds.add(Id.valueOf(bankAccountDetail.accountId));
        }
       
        Map<Id, Acount_Bank_Account_Relationship__c> mapBankAccount = new Map<Id, Acount_Bank_Account_Relationship__c>();
        mapBankAccount = bankAccountRelSelector.selectBankAccountByAccountId(accountIds);
        Bank_Account__c aBankAccount = new Bank_Account__c();
        Set<Id> bankAccountUpdated = new Set<Id>();

        for(Id iPartner : accountIds){
            Acount_Bank_Account_Relationship__c bankAccountRel = mapBankAccount.get(iPartner);      
            if(bankAccountRel != null){
              
                aBankAccount = bankAccountDetail.fields.clone( false, false, false, false );
                aBankAccount.RecordTypeId = bankAccountRtId;                
                aBankAccount.Id = bankAccountRel.Bank_Account__c;
                System.debug('aBankAccount.Id ' + aBankAccount.Id);
                bankAccountUpdated.add(aBankAccount.Id);
                System.debug('bankAccountUpdated ' + bankAccountUpdated);    
                uow.registerDirty(aBankAccount);                                         
              
            } else {
                System.debug(':: ENTROU NO ELSE DO BANK');
                aBankAccount = bankAccountDetail.fields.clone( false, false, false, false );
                aBankAccount.RecordTypeId = bankAccountRtId;
                uow.registerNew(aBankAccount);
                bankAccountRel = new Acount_Bank_Account_Relationship__c();          
                bankAccountRel.Account__c = iPartner;
                bankAccountRel.RecordTypeId = bankAccountRelRtId;
                System.debug(bankAccountRel);
                uow.registerNew(bankAccountRel);
                uow.registerRelationship(bankAccountRel, Acount_Bank_Account_Relationship__c.Bank_Account__c, aBankAccount);
             
            }
        }

    }

    private static void updateOpeningHours(OpeningHoursDetail openingHoursDetail, List<Id> partnersToUpdate, fflib_ISObjectUnitOfWork uow){
        Id openinHoursRt =  Schema.SObjectType.Opening_Hours__c.getRecordTypeInfosByDeveloperName().get(constants.OPENING_HOURS_RT).getRecordTypeId();

        List<Opening_Hours__c> openHoursToDelete = new List<Opening_Hours__c>(); 
        List<Opening_Hours__c> openHoursToInsertLst = new List<Opening_Hours__c>();
        OpeningHourSelector openingHoursSlc = (OpeningHourSelector)Application.selector.newInstance(Opening_Hours__c.sObjectType);
        if(openingHoursDetail.fields.Days__c == null){
            return;
        }

        if(!partnersToUpdate.isEmpty()){
            openHoursToDelete = openingHoursSlc.selectAllById(new Set<Id>(partnersToUpdate));
            uow.registerDeleted(openHoursToDelete);
            for(Id accountId : partnersToUpdate){
                Opening_Hours__c nOpenHour = openingHoursDetail.fields.clone( false, false, false, false );
                nOpenHour.RecordTypeId = openinHoursRt;
                nOpenHour.Account_Related__c = accountId;
                openHoursToInsertLst.add( nOpenHour );
            }
            if(!openHoursToInsertLst.isEmpty()){
                uow.registerNew( openHoursToInsertLst );
            }
        } else {
            Opening_Hours__c nOpenHour = openingHoursDetail.fields.clone( false, false, false, false );
            nOpenHour.RecordTypeId = openinHoursRt;
            nOpenHour.Account_Related__c = openingHoursDetail.fields.Account_Related__c;
         
            uow.registerNew( nOpenHour );
            if( openingHoursDetail.fields.Id != null && !String.isEmpty( openingHoursDetail.fields.Id ) ){
                uow.registerDeleted( openingHoursDetail.fields );
            }
        }
    }

    private static void updateFiles(List<SetupFile> files, List<Id> partnersToUpdate, fflib_ISObjectUnitOfWork uow){      
        Id fileObjRt =  Schema.SObjectType.FileObject__c.getRecordTypeInfosByDeveloperName().get(constants.FILEOBJECT_PARTNER_RT).getRecordTypeId();
        Id contentVersionRt =  Schema.SObjectType.ContentVersion.getRecordTypeInfosByDeveloperName().get(constants.CONTENTVERSION_PARTNER_RT).getRecordTypeId(); 
        List<ContentDocumentLink> contDocLinkLst = new List<ContentDocumentLink>();        
       

        if( !partnersToUpdate.isEmpty() ){ 
            deleteFilesBeforeInsert(files, partnersToUpdate, uow);
            for( Id aPartnerId : partnersToUpdate ){     
                for( SetupFile file : files ) {
                    ContentDocumentLink contDocLink = new ContentDocumentLink();
                    contDocLink.ContentDocumentId = file.documentId;
                    contDocLink.LinkedEntityId = aPartnerId;
                    contDocLinkLst.add( contDocLink );
                }
            }
        } else {
            validateFilesBeforeInsert(files, partnersToUpdate);
            for( SetupFile file : files ) {  
                if(file.isInsert){
                    ContentDocumentLink contDocLink = new ContentDocumentLink();
                    contDocLink.ContentDocumentId = file.documentId;
                    contDocLink.LinkedEntityId = file.entityId;
                    contDocLinkLst.add( contDocLink );
                }       
            }
        }
        uow.registerNew( contDocLinkLst );
    }

    private static void deleteFilesBeforeInsert(List<SetupFile> files, List<Id> partnersToUpdate, fflib_ISObjectUnitOfWork uow){
        List<ContentDocumentLink> oldContentDocLks = new List<ContentDocumentLink>();
        Set<String> fileTypes = new Set<String>();
        for(SetupFile file : files){
            if( file.fileType != constants.FILEOBJECT_FILETYPE_PHOTO ){
                fileTypes.add(file.fileType);
            }
        }
        if(!fileTypes.isEmpty() && !partnersToUpdate.isEmpty()){
            ContentDocumentLinkSelector contentLinkSlc = (ContentDocumentLinkSelector)Application.selector.newInstance(ContentDocumentLink.sObjectType);
            for(ContentDocumentLink cdl : contentLinkSlc.selectByLinkedEntityId(new Set<Id>(partnersToUpdate))){
                if(fileTypes.contains( cdl.ContentDocument.LatestPublishedVersion.Type_Files_fileupload__c )){
                    oldContentDocLks.add( cdl );
                }
            }
            if( !oldContentDocLks.isEmpty() ){
                uow.registerDeleted( oldContentDocLks );
            }
        }
        
    }

    private static void validateFilesBeforeInsert(List<SetupFile> files, List<Id> partnersToUpdate){
        Integer photoQuantity = 0;
        Integer w9Quantity = 0;
        Integer logoQuantity = 0;
        Set<Id> contVersionIds = new Set<Id>();
        Set<Id> accountIds = new Set<Id>();
        List<String> fileTypeLst = new List<String>();

        for(SetupFile file : files){
            if(file.isInsert){
                contVersionIds.add(Id.valueOf(file.contentVersionId));
                if(file.entityId != null && !String.isBlank(file.entityId)){
                    accountIds.add(Id.valueOf(file.entityId));
                }    
            }
        }

        Map<String, String> mapFileParamValues = new Map<String, String>();
        for (File_Setting__mdt fileSetting : [SELECT Id, Parameter_Name__c, Parameter_Value__c  FROM   File_Setting__mdt WHERE MasterLabel LIKE '%Partner%']){
            mapFileParamValues.put(fileSetting.Parameter_Name__c, fileSetting.Parameter_Value__c);
        }
       
        ContentVersionSelector contVersionSelector = (ContentVersionSelector)Application.Selector.newInstance(ContentVersion.sobjectType);
        for(ContentVersion cv : contVersionSelector.selectById(contVersionIds)){
            if(cv.ContentSize > Decimal.valueof(mapFileParamValues.get(constants.FILESETTING_PARTNERS_FILE_SIZE_LIMIT))){
                throw new PartnerSetupControllerException(constants.ERROR_FILE_SIZE);
            } 
            fileTypeLst.add(cv.Type_Files_fileupload__c);
        }

        FileObjectSelector fileObjSelector = (FileObjectSelector)Application.Selector.newInstance(FIleObject__c.sobjectType);
        Map<Id, List<FIleObject__c>> fileMap = new Map<Id, List<FileObject__c>>();
        if(!partnersToUpdate.isEmpty()){
            fileMap = fileObjSelector.selectByAccountId(new Set<Id>(partnersToUpdate));           
        } else {
            fileMap = fileObjSelector.selectByAccountId(accountIds);
        }

        if(!fileMap.isEmpty()){
            for(Id iPartner : fileMap.keySet()){
                for(FIleObject__c fileObj : fileMap.get(iPartner)){
                //   if(fileObj.File_Type__c == constants.FILEOBJECT_FILETYPE_PHOTO){
                //     photoQuantity++;
                //     if( photoQuantity >= Integer.valueOf(mapFileParamValues.get(constants.FILESETTING_PARTNERS_PHOTO_QTY_LIMIT))){                  
                //         if( fileTypeLst.contains(constants.FILEOBJECT_FILETYPE_PHOTO)){
                //             throw new PartnerSetupControllerException(fileObj.Account__r.Name + ' - ' + constants.ERROR_PHOTOS_QUANTITY);
                //         }
                //     }
                //   } else 
                  if(fileObj.File_Type__c == constants.FILEOBJECT_FILETYPE_LOGO){
                    logoQuantity++;
                    if( logoQuantity >= Integer.valueOf(mapFileParamValues.get(constants.FILESETTING_PARTNERS_LOGO_QTY_LIMIT))){                
                        if(fileTypeLst.contains(constants.FILEOBJECT_FILETYPE_LOGO)){
                            throw new PartnerSetupControllerException(fileObj.Account__r.Name + ' - ' + constants.ERROR_LOGO_QUANTITY);
                        }
                    }    
                  } else if(fileObj.File_Type__c == constants.FILEOBJECT_FILETYPE_W9){
                    w9Quantity++;
                    if( w9Quantity >=  Integer.valueOf(mapFileParamValues.get(constants.FILESETTING_PARTNERS_W9_QTY_LIMIT))){  
                        if(fileTypeLst.contains(constants.FILEOBJECT_FILETYPE_W9)){
                            throw new PartnerSetupControllerException(fileObj.Account__r.Name + ' - ' + constants.ERROR_W9_QUANTITY);
                        }
                    }
                  }
                }
            }
        }
    }

    public class SetupPartner{
        @AuraEnabled
        public String accountId {get; private set;}

        @AuraEnabled
        public String partnerName {get; private set;}

        @AuraEnabled
        public String partnerWebsite {get; private set;}

        @AuraEnabled
        public String partnerEmail {get; private set;}

        @AuraEnabled
        public String partnerPhone {get; private set;}  

        @AuraEnabled 
        public String partnerValidationType {get; private set;}

        @AuraEnabled
        public Boolean partnerHasBankAccount {get; private set;}
        @AuraEnabled
        public String partnerBankAccountId {get; private set;}        

        @AuraEnabled
        public Boolean partnerHasOpeningHours {get; private set;}
        @AuraEnabled
        public String partnerOpeningHoursId {get; private set;}    

        @AuraEnabled
        public Boolean partnerHasDescription {get; private set;}

        @AuraEnabled
        public Boolean partnerHasPhotos {get; private set;}
        @AuraEnabled
        public List<SetupFile> partnerPhotos{get; private set;}

        @AuraEnabled
        public Boolean partnerHasLogo {get; private set;}
        @AuraEnabled
        public List<SetupFile> partnerLogo{get; private set;}

        @AuraEnabled
        public Boolean partnerHasW9 {get; private set;}
        @AuraEnabled
        public List<SetupFile> partnerW9{get; private set;}

        @AuraEnabled
        public String score {get; private set;}

        @AuraEnabled
        public String countryCode {get;private set;}
        
        public SetupPartner ( Account_Contract_Agreement_Relationship__c accountInContract, List<FIleObject__c> fileObjLst, Acount_Bank_Account_Relationship__c bankAccount, Opening_Hours__c openHour, Map<String, String> mapFileParamValues) {
            Integer auxScore = 0;
            this.accountId = accountInContract.AccountId__r.Id;
            this.partnerName = accountInContract.AccountId__r.Gym_Title_on_Gympass_App__c;
            
            this.partnerWebsite = accountInContract.AccountId__r.Website;
            auxScore += this.partnerWebsite != null ? 1 : 0;
             
            this.partnerEmail = accountInContract.AccountId__r.Gym_Email__c;
            auxScore += this.partnerEmail != null ? 1 : 0;

            this.partnerPhone = accountInContract.AccountId__r.Phone;
            auxScore += this.partnerPhone != null ? 1 : 0;

            this.partnerValidationType = accountInContract.AccountId__r.Validation_type__c;
            auxScore += this.partnerValidationType != null ? 1 : 0;

            this.partnerHasDescription =  accountInContract.AccountId__r.Description != null ? true : false;
            auxScore += this.partnerHasDescription != false ? 1 : 0;           
                     
            this.partnerHasBankAccount = bankAccount.Id != null ? true : false;
            this.partnerBankAccountId = bankAccount.Bank_Account__c; 
            auxScore += this.partnerHasBankAccount != false ? 1 : 0;            
         
            this.partnerHasOpeningHours = openHour.Id != null ? true : false;
            this.partnerOpeningHoursId = openHour.Id;
            auxScore += this.partnerHasOpeningHours != false ? 1 : 0;            

            this.partnerPhotos = setFiles(fileObjLst, constants.FILEOBJECT_FILETYPE_PHOTO);
            this.partnerHasPhotos = Integer.valueOf(mapFileParamValues.get(constants.FILESETTING_PARTNERS_PHOTO_QTY_LIMIT)) <= this.partnerPhotos.size() ? true : false;
            auxScore += this.partnerHasPhotos != false ? 1 : 0;

            this.partnerLogo = setFiles(fileObjLst, constants.FILEOBJECT_FILETYPE_LOGO);
            this.partnerHasLogo = partnerLogo.size() >= 1 ? true : false;
            auxScore += this.partnerHasLogo != false ? 1 : 0;

            this.countryCode = accountInContract.AccountId__r.BillingCountryCode;
            if(this.countryCode != null && this.countryCode.equals('US')){  
                this.partnerW9 = setFiles(fileObjLst, constants.FILEOBJECT_FILETYPE_W9);
                this.partnerHasW9 = partnerW9.size() >= 1 ? true : false;
                auxScore += this.partnerHasW9 != false ? 1 : 0;  
            }

            this.score = String.valueOf(auxScore);
        }

        // private Boolean checkFileType(List<FIleObject__c> fileObjLst, String fieldTypeToCompare, Map<String, String> mapFileParamValues ){
        //     Integer photosMin = Integer.valueOf(mapFileParamValues.get(constants.FILESETTING_PARTNERS_PHOTO_QTY_LIMIT));
        //     Integer photosQtd = 0;

        //     for( FIleObject__c fileObject : fileObjLst){           
        //         if(fileObject.File_Type__c == fieldTypeToCompare && fieldTypeToCompare == constants.FILEOBJECT_FILETYPE_PHOTO)     {
        //             photosQtd++;
        //             if(photosQtd >= photosMin){
        //                 return true;
        //             }
        //         } else if(fileObject.File_Type__c == fieldTypeToCompare) {
        //             return true; 
        //         }
        //     }
        //     return false;
        // }

        private List<SetupFile> setFiles (List<FIleObject__c> fileObjLst, String constantType){
            List<SetupFile> files = new List<SetupFile>();     
            System.debug(':: constantType.constantType ' + constantType);       
            for( FIleObject__c fileObject : fileObjLst){   
          
                System.debug(':: fileObject.File_Type__c ' + fileObject.File_Type__c);
                if(fileObject.File_Type__c == constantType){
                    SetupFile cFile = new SetupFile();
                    cFile.name = fileObject.File_Name__c;
                    cFile.contentVersionId = Id.valueOf(fileObject.conversionId__c);
                    cFile.fileType = fileObject.File_Type__c;
                    cFile.mimeType = fileObject.Format__c;
                    cFile.entityId = fileObject.Account__c;
                    cFile.isInsert = false;
                    files.add(cFile);
                }
            }
            System.debug(':: files ' + files);
            return files;
        }
    }


    public class SetupFile{
        @AuraEnabled
        public String name {get; set;}

        @AuraEnabled
        public String documentId {get; set;}

        @AuraEnabled
        public String contentVersionId {get; set;}

        @AuraEnabled
        public String contentBodyId {get; set;}

        @AuraEnabled
        public String mimeType {get; set;}

        @AuraEnabled
        public String fileType {get;  set;}

        @AuraEnabled
        public String entityId {get; set;}

        @AuraEnabled
        public Boolean isInsert {get; set;}
                        
        public SetupFile(){

        }

        public SetupFile (String name, String documentId, String contentVersionId, String contentBodyId, String mimeType, String fileType, Boolean isInsert) {
            this.name = name;
            this.documentId = documentId;
            this.contentVersionId = contentVersionId;
            this.contentBodyId = contentBodyId;
            this.mimeType = mimeType;
            this.fileType = fileType;
            this.entityId = entityId;
            this.isInsert = isInsert;
        }
    }

    public class UpdatedPartner {
        @AuraEnabled
        public PartnerDetail partner;
        public BankAccountDetail bankAccount;
        public OpeningHoursDetail openingHours;
        public List<SetupFile> files;

    }

    public class PartnerDetail{    
        @AuraEnabled
        public String apiName;
        @AuraEnabled
        public Account fields;

    }

    public class BankAccountDetail{      
        public String apiName;
        public String accountId;
        public Bank_Account__c fields;
    }

    public class OpeningHoursDetail{      
        public String apiName;    
        public Opening_Hours__c fields;
    }


    @TestVisible
    private class PartnerSetupControllerException extends Exception {}
}