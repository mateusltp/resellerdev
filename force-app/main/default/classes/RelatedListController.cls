/**
 * @description       : 
 * @author            : tiago.ribeiro@gympass.com
 * @group             : 
 * @last modified on  : 05-13-2022
 * @last modified by  : tiago.ribeiro@gympass.com
**/
public with sharing class RelatedListController {
    public static final String FIELDS_PARAM = 'fields';
    public static final String NUMBER_OF_RECORDS_PARAM = 'numberOfRecords';
    public static final String RECORD_ID_PARAM = 'recordId';
    public static final String SOBJECT_API_NAME_PARAM = 'sobjectApiName';
    public static final String SOBJECT_LABEL_PARAM = 'sobjectLabel';
    public static final String SOBJECT_LABEL_PLURAL_PARAM = 'sobjectLabelPlural';
    public static final String PARENT_RELATIONSHIP_API_NAME_PARAM = 'parentRelationshipApiName';
    public static final String RELATED_FIELD_API_NAME_PARAM = 'relatedFieldApiName';
    public static final String SORTED_DIRECTION_PARAM = 'sortedDirection';
    public static final String SORTED_BY_PARAM = 'sortedBy';
    public static final String RECORDS_PARAM = 'records';
    public static final String ICON_NAME_PARAM = 'iconName';
    public static final String RELATED_FROM_CHILD_OBJECT = 'relatedFromChildObject';
    public static final String RELATED_CHILD_OBJECT_API_NAME = 'relatedChildObjectApiName';
    public static final String RELATED_CHILD_OBJECT_FIELD_API_NAME = 'relatedChildObjectFieldApiName';
    public static final String RETRIEVE_WAIVERS_PARAM = 'retrieveWaivers';

    @AuraEnabled
    public static String initData(String jsonData){
        Map<String, Object> requestMap = (Map<String, Object>)JSON.deserializeUntyped(jsonData);  
        Map<String, Object> responseMap = new Map<String, Object>();
        responseMap.put(RECORDS_PARAM, getRecords(jsonData));
        
        Boolean relatedFromChildObject = (Boolean)requestMap.get(RELATED_FROM_CHILD_OBJECT);
        Id recordId = (Id)requestMap.get(RECORD_ID_PARAM);
        String sObjectApiName = (String)requestMap.get(SOBJECT_API_NAME_PARAM);
        String objectApiName = '';
        String relatedFieldName = '';
        Schema.DescribeSObjectResult descrRes = null;
        if(relatedFromChildObject){
            objectApiName = (String)requestMap.get(RELATED_CHILD_OBJECT_API_NAME);
            relatedFieldName = (String)requestMap.get(RELATED_CHILD_OBJECT_FIELD_API_NAME);
            descrRes = Schema.getGlobalDescribe().get(sObjectApiName).getDescribe();
        }else{
            objectApiName = sObjectApiName;
            relatedFieldName = (String)requestMap.get(RELATED_FIELD_API_NAME_PARAM);
            descrRes = recordId.getSObjectType().getDescribe();
        }

        responseMap.put(PARENT_RELATIONSHIP_API_NAME_PARAM, getParentRelationshipName(descrRes, objectApiName, relatedFieldName));

        Schema.SObjectType sobjectType = Schema.getGlobalDescribe().get(objectApiName);
        Schema.DescribeSObjectResult sobjectDescribe = sobjectType.getDescribe();
        responseMap.put(SOBJECT_LABEL_PARAM, sobjectDescribe.getLabel());
        responseMap.put(SOBJECT_LABEL_PLURAL_PARAM, sobjectDescribe.getLabelPlural());
        return JSON.serialize(responseMap);
    }
    
	@AuraEnabled
    public static List<Sobject> getRecords(String jsonData){
		Map<String, Object> root = (Map<String, Object>)JSON.deserializeUntyped(jsonData);  
        String fields = (String)root.get(FIELDS_PARAM);
        String recordId = (String)root.get(RECORD_ID_PARAM);
		String relatedFieldApiName = (String)root.get(RELATED_FIELD_API_NAME_PARAM);
		String sobjectApiName = (String)root.get(SOBJECT_API_NAME_PARAM);        
		String orderBy = (String)root.get(SORTED_BY_PARAM) + ' ' + (String)root.get(SORTED_DIRECTION_PARAM);
        String relatedChildObjectApiName = (String)root.get(RELATED_CHILD_OBJECT_API_NAME);
        String relatedChildObjectFieldApiName = (String)root.get(RELATED_CHILD_OBJECT_FIELD_API_NAME);
        Boolean relatedFromChildObject = (Boolean)root.get(RELATED_FROM_CHILD_OBJECT);
        Boolean retrieveWaivers = (Boolean)root.get(RETRIEVE_WAIVERS_PARAM);
        Integer numberOfRecords = (Integer)root.get(NUMBER_OF_RECORDS_PARAM);
        String query = '';
        
        if(relatedFromChildObject){
            query = 'SELECT ' + fields + ', ' + relatedChildObjectFieldApiName  + concatenateWaivers(retrieveWaivers) + ' FROM ' + relatedChildObjectApiName + ' WHERE ' + relatedChildObjectFieldApiName + ' IN ' 
                    + '(SELECT Id FROM ' + sobjectApiName + ' WHERE ' + relatedFieldApiName + '= :recordId)'; 
        }else{
            query = 'SELECT ' + fields + ' FROM ' + sobjectApiName + ' WHERE ' + relatedFieldApiName + '= :recordId';
        }
        query += ' ORDER BY ' + orderBy + ' LIMIT :numberOfRecords';
        return Database.query(String.escapeSingleQuotes(query));
	}
        
    private static String getParentRelationshipName(Schema.DescribeSObjectResult descrRes, String childSobjectApiName, String relatedFieldApiName){
        String name;
        for (Schema.ChildRelationship cr: descrRes.getChildRelationships()){
            if(cr.getChildSObject().getDescribe().getName() == childSobjectApiName && cr.getField().getDescribe().getName() == relatedFieldApiName){
          	 	name = cr.getRelationshipName();
                break;
            }
        }     
        return name;
    }

    private static String concatenateWaivers(Boolean needWaivers){
        String queryWaivers = '';
        if(needWaivers){
            queryWaivers = ', (SELECT Id, Percentage__c, Duration__c FROM Waivers__r ORDER BY Position__c ASC)';
        }

        return queryWaivers;
    }
    
}