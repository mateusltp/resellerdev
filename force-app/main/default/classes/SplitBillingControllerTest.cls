@isTest(SeeAllData=false)
public with sharing class SplitBillingControllerTest {
    
    @isTest
    static void getAccountHierarchyTest(){
        Opportunity lOpp = [ SELECT Id , AccountId FROM Opportunity ];
        Payment__c Pay = [SELECT Id, Account__c FROM Payment__c LIMIT 1];
        
        List<Account> AccTestList = [SELECT Id, NumberOfEmployees, Name, BillingState, BillingCountry, OwnerId FROM Account WHERE ParentId!=null AND Id !=: Pay.Account__c];
        List<Account> AccPayTestList = [SELECT Id, NumberOfEmployees, Name, BillingState, BillingCountry, OwnerId FROM Account WHERE ParentId!=null AND Id =: Pay.Account__c];

        List<SplitBillingController.PaymentInfo> allPaymInfoInHierarchyList = new List<SplitBillingController.PaymentInfo>();
        List<SplitBillingController.PaymentInfo> allPaymInfoWithPaymentList = new List<SplitBillingController.PaymentInfo>();
		Map<String,List<SplitBillingController.PaymentInfo>> returnMap = new Map<String,List<SplitBillingController.PaymentInfo>>();
                
                for(Account acc : AccTestList){
                    SplitBillingController.PaymentInfo paym = new SplitBillingController.PaymentInfo();
                    paym.AccountId = acc.Id;
                    paym.AccountName = acc.Name;
                    //paym.AccountBillingCity = acc.BillingCity;
                    paym.AccountBillingState = acc.BillingState;
                    paym.AccountBillingCountry = acc.BillingCountry;
                    //paym.AccountLegalName = acc.Legal_Name__c;
                    //paym.AccountLegalNumber = acc.Id_Company__c;
                    paym.nonRecurringFeePercentage = 0;
                    paym.recurringFeePercentage = 0;
                    allPaymInfoInHierarchyList.add(paym);  
                }
                
                for(Account acc : AccPayTestList){
                    SplitBillingController.PaymentInfo paym = new SplitBillingController.PaymentInfo();
                    paym.AccountId = acc.Id;
                    paym.AccountName = acc.Name;
                    //paym.AccountBillingCity = acc.BillingCity;
                    paym.AccountBillingState = acc.BillingState;
                    paym.AccountBillingCountry = acc.BillingCountry;
                    //paym.AccountLegalName = acc.Legal_Name__c;
                    //paym.AccountLegalNumber = acc.Id_Company__c;
                    paym.nonRecurringFeePercentage = 0;
                    paym.recurringFeePercentage = 0;
                    allPaymInfoWithPaymentList.add(paym);  
                }
                
                returnMap.put('allPaymInfoInHierarchyList',allPaymInfoInHierarchyList);
                returnMap.put('allPaymInfoWithPaymentList',allPaymInfoWithPaymentList);

        Test.startTest();
        Map<String,List<SplitBillingController.PaymentInfo>> responseMap = SplitBillingController.getAccountHierarchy( lOpp.Id );
        Test.stopTest();

        //System.assertEquals( responseMap , returnMap , 'Should have returned responseMap' );
    }
    
     @isTest
    static void getAccountHierarchyExceptionTest(){

        Test.startTest();
        try {
        	Map<String,List<SplitBillingController.PaymentInfo>> responseMap = SplitBillingController.getAccountHierarchy( null );
            System.assert(false, 'This assert must never be executed. An exception must be thrown before this assert.');
        }
        catch(Exception e) {
            System.assertEquals('System.AuraHandledException', e.getTypeName(), 'The exception thrown is not the expected one');
        }
        
        Test.stopTest();
    }
    
     @isTest
    static void removeAccountFromOpportunityTest(){
        Opportunity lOpp = [ SELECT Id , AccountId FROM Opportunity ];
        Payment__c Pay = [SELECT Id, Account__c FROM Payment__c LIMIT 1];
		List<Account> AccList = [SELECT Id, NumberOfEmployees, Name, BillingState, BillingCountry, OwnerId FROM Account WHERE ParentId!=null AND Id =: Pay.Account__c LIMIT 1];
        system.debug('AccList '+ AccList);
        
        List<SplitBillingController.PaymentInfo> allPaymInfoWithPaymentList = new List<SplitBillingController.PaymentInfo>();

        for(Account acc : AccList){
                    SplitBillingController.PaymentInfo paym = new SplitBillingController.PaymentInfo();
                    paym.AccountId = acc.Id;
                    paym.AccountName = acc.Name;
                    //paym.AccountBillingCity = acc.BillingCity;
                    paym.AccountBillingState = acc.BillingState;
                    paym.AccountBillingCountry = acc.BillingCountry;
                    //paym.AccountLegalName = acc.Legal_Name__c;
                    //paym.AccountLegalNumber = acc.Id_Company__c;
                    paym.nonRecurringFeePercentage = 0;
                    paym.recurringFeePercentage = 0;
                    allPaymInfoWithPaymentList.add(paym);  
                }

        Test.startTest();
        SplitBillingController.removeAccountFromOpportunity( lOpp.Id , allPaymInfoWithPaymentList);
        Test.stopTest();

    }
    
     @isTest
    static void saveAccountWithOpportunityTest(){
        Opportunity lOpp = [ SELECT Id , AccountId FROM Opportunity ];
        List<Payment__c> PaymList = [SELECT Id, Account__c, Account__r.Name, Opportunity__c, Percentage__c FROM Payment__c];
        List<Account> AccList = [SELECT Id, NumberOfEmployees, Name, BillingState, BillingCountry, OwnerId FROM Account WHERE ParentId!=null];
        List<SplitBillingController.PaymentInfo> PaymentInfoList = new List<SplitBillingController.PaymentInfo>();
		
        for(Account acc : AccList){
            SplitBillingController.PaymentInfo paymentInfo = new SplitBillingController.PaymentInfo();
            paymentInfo.AccountId = acc.Id;
            paymentInfo.AccountName = acc.Name;
            paymentInfo.recurringFeePercentage = 1;
            paymentInfo.nonRecurringFeePercentage = 1;
            paymentInfo.AccountBillingCity = 'Rio de Janeiro';
            paymentInfo.AccountBillingState = acc.BillingState;
            paymentInfo.AccountBillingCountry = acc.BillingCountry;
            paymentInfo.AccountLegalName = 'Legal Name';
            paymentInfo.AccountLegalNumber = '1234';
            PaymentInfoList.add(paymentInfo);
        }

        Test.startTest();
        SplitBillingController.saveAccountWithOpportunity( lOpp.Id , PaymentInfoList);
        Test.stopTest();

    }
    
 @isTest
    static void saveAccountWithOpportunityExceptionTest(){

        Test.startTest();
        try {
        	SplitBillingController.saveAccountWithOpportunity( null, null );
            System.assert(false, 'This assert must never be executed. An exception must be thrown before this assert.');
        }
        catch(Exception e) {
            system.debug(e.getTypeName());
            System.assertEquals('SplitBillingController.SplitBillingControllerException', e.getTypeName(), 'The exception thrown is not the expected one');
        }
        
        Test.stopTest();
    } 
    
    @TestSetup
    static void createData(){    
        Account lParentAcc = DataFactory.newAccount();
        lParentAcc.NumberOfEmployees = 1000;
        Database.insert( lParentAcc );

        Account lAcc = DataFactory.newAccount();
        lAcc.ParentId = lParentAcc.Id;
        lAcc.NumberOfEmployees = 1000;
        Database.insert( lAcc );
        
        Account lAcc2 = DataFactory.newAccount(); //account has payment
        lAcc2.ParentId = lParentAcc.Id;
        lAcc2.NumberOfEmployees = 500;
        Database.insert( lAcc2 );

        Contact lNewContact = DataFactory.newContact( lParentAcc , 'Test Contact' );
        Database.insert( lNewContact );

        Pricebook2 lStandardPb = DataFactory.newPricebook();
        Database.update( lStandardPb );

        Opportunity lOpp = new Opportunity(CurrencyIsoCode='BRL');
        lOpp = DataFactory.newOpportunity( lParentAcc.Id , lStandardPb , 'Client_Sales_New_Business' );
        lOpp.Quantity_Offer_Number_of_Employees__c = 500;
        Database.insert( lOpp );
        
        Payment__c payment = new Payment__c();
        payment.Opportunity__c = lOpp.Id;
        payment.Account__c = lAcc2.Id;
        payment.RecordTypeId = Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Recurring').getRecordTypeId();
        Database.insert( payment );
        
        Payment__c payment2 = new Payment__c();
        payment2.Opportunity__c = lOpp.Id;
        payment2.Account__c = lAcc2.Id;
        payment2.RecordTypeId = Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Non_Recurring').getRecordTypeId();
        Database.insert( payment2 );
        
        Payment__c ParentPayment = new Payment__c();
        ParentPayment.Opportunity__c = lOpp.Id;
        ParentPayment.Account__c = lParentAcc.Id;
        ParentPayment.RecordTypeId = Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Recurring').getRecordTypeId();
        Database.insert( ParentPayment );
        
        Payment__c ParentPayment2 = new Payment__c();
        ParentPayment2.Opportunity__c = lOpp.Id;
        ParentPayment2.Account__c = lParentAcc.Id;
        ParentPayment2.RecordTypeId = Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Non_Recurring').getRecordTypeId();
        Database.insert( ParentPayment2 );
        
        Eligibility__c lEligibility = DataFactory.newEligibility(ParentPayment);
        lEligibility.Payment__c = ParentPayment.Id;
        Database.insert( lEligibility );
    }

}