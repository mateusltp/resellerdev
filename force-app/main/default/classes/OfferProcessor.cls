/**
 * @author vncferraz
 * 
 * Facade implementation class to provide Offer_Queue__c processing logic layer for consumer
 */
public virtual class OfferProcessor extends OfferQueueProcessor{
    
    protected Map<String , String> currencyByCountry;

    public OfferProcessor(List<Offer_Queue__c> offers){
        super(offers);
        init();
    }

    override
    protected virtual void init(){   
        preLoadCurrencyByCountry();     
        this.builder = new OfferWrapperBuilder();
    }   

    override
    protected virtual BaseOfferWrapper buildWrapper(Offer_Queue__c offer){
        return (BaseOfferWrapper) builder.withCurrencyByCountry(currencyByCountry)
                        .build( offer );        
    }

    private void preLoadCurrencyByCountry(){
        this.currencyByCountry = new Map<String , String>();
        for( CurrencyByCountry__mdt iCurrencyByContry : [ SELECT Country__c, Currency__c, Code__c FROM CurrencyByCountry__mdt ] ){
            currencyByCountry.put(iCurrencyByContry.Country__c, iCurrencyByContry.Currency__c);
        }
    }
}