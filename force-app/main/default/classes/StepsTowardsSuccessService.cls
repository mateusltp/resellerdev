/**
 * @description       : 
 * @author            : alysson.mota@gympass.com
 * @group             : 
 * @last modified on  : 04-08-2022
 * @last modified by  : alysson.mota@gympass.com
**/
public with sharing class StepsTowardsSuccessService {
    private static final PS_Constants constants = PS_Constants.getInstance();
    private static final String className = StepsTowardsSuccessService.class.getName();
    
    public Map<Id, List<Step_Towards_Success_Partner__c>> getOppNewFlowPartnerSteps(Set<Id> oppIdSet) {
        try {
            return partnerService(constants.PARTNER_FLOW_OPP_STEP_RT).getOppNewFlowPartnerSteps(oppIdSet);
        } catch(Exception e) {
            // Alysson TODO: Improve exception flow 
            createLog(e.getMessage(), e.getStackTraceString(), 'getOppNewFlowPartnerSteps', null, null);
            System.debug(e.getMessage());
            System.debug(e.getStackTraceString());
            return new Map<Id, List<Step_Towards_Success_Partner__c>>();
        }
    }

    private static String getOppPartFlowRecTypeAndSObjName (String recordTypeDevName){
        return constants.STEP_TOWARDS_SUCCESS_PARTNER_OBJECT + '.' + recordTypeDevName;
    }
    
    private static IPartnerStepsTowardsSuccessService partnerService(String recordTypeDevName) {
        try{
            return (IPartnerStepsTowardsSuccessService) Application.ServiceByRecordType.newInstanceByRecordType(getOppPartFlowRecTypeAndSObjName(recordTypeDevName));
        }catch (Exception e){
            createLog(e.getMessage(), e.getStackTraceString(), 'partnerService', null, null);
            throw new StepsTowardsSuccessServiceException(e.getMessage());
        }  
    }

    @TestVisible
    private static void createLog(String exceptionMsg, String exceptionStackTrace, String methodName, String dataScope, Id objectId) {
        DebugLog__c log = new DebugLog__c(
            Origin__c = '['+className+']['+methodName+']',
            LogType__c = constants.LOGTYPE_ERROR,
            RelatedToSquad__c = constants.LOGTYPE_SQUAD_PARTNERS,
            ExceptionMessage__c = exceptionMsg,
            StackTrace__c = exceptionStackTrace,
            DataScope__c = dataScope, // test to verify how the uow would look like at this point
            ObjectId__c = objectId
        );
        Logger.createLog(log);
    }

    public class StepsTowardsSuccessServiceException extends Exception {}
}