@isTest
public class ResellerActivityTest {

    @IsTest 
    public static void lastAccountActivityTest() {
      
	  Account account = new Account();  
        
      account.RecordTypeId = '01241000000xyotAAA';
      account.Name = 'teste account';
      account.Legal_Document_Type__c = 'CNPJ';
      account.Id_Company__c = '56947401000155';
      account.Razao_Social__c = 'TESTE';
      account.Website = 'teste.com';
      account.Industry = 'Airlines'; 						
      account.BillingCountry = 'Brazil';
      account.Type = 'Prospect';
      account.Industry = 'Government or Public Management';
 
      insert account;
       
      
      DateTime DATE_NOW = DateTime.Now();  
      DateTime DATE_TODAY_LESS_60 = DATE_NOW.addDays(-60);
  
      Event event = new Event();
        
      event.Subject = 'Call';     
      event.CreatedDate = DATE_TODAY_LESS_60;     
      event.WhatId = account.Id;    
      event.DurationInMinutes = 20;
      event.ActivityDateTime = DATE_NOW;
        
      insert event;
        
      Task task = new Task(
          Subject = 'Test Task Created',
          WhatId = account.Id
      );
      insert task;
   
      Opportunity opportunity = new Opportunity();
      Id oppRtId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
      opportunity.recordTypeId = oppRtId;
      opportunity.AccountId = account.id;
      opportunity.Name = 'teste opp';      
      opportunity.CloseDate = Date.today();
      opportunity.StageName = 'Contract';
      opportunity.FastTrackStage__c = 'Contract';
      opportunity.Sales_Channel__c = 'Direct';
      opportunity.Type = 'New partnership';     
      opportunity.Probability = 20;
      //opportunity.Pricebook2Id = pricebook.Id;
      insert opportunity;
        
      Event event2 = new Event();
        
      event2.Subject = 'Call';     
      event2.CreatedDate = DATE_TODAY_LESS_60;     
      event2.WhatId = opportunity.Id;    
      event2.DurationInMinutes = 20;
      event2.ActivityDateTime = DATE_NOW;
        
      insert event2;
        
        Task task2 = new Task(
            Subject = 'Test Task Created',
            WhatId = opportunity.Id
        );
        
        insert task2;
      
      Account_Request__c request = new Account_Request__c();  
         
      request.Name = 'teste';
      request.Unique_Identifier_Type__c = 'CNPJ';
      request.Unique_Identifier__c = '56947401000155';
      request.Website__c = 'testerequeste.com';
      request.Billing_Country__c = 'Brazil';
      request.AccountId__c = account.Id;
      request.Created_Date_Opportunity__c = 10;
      request.Last_Modified_Opportunity__c = 35;
      request.Last_Interaction_Opportunity_Event__c = 16;
      request.OpportunityId__c = opportunity.Id;
      insert request;
        
      Boolean isInsert = true;

      Test.startTest();
      
      	ResellerActivity resellerActivity = new ResellerActivity(new Set<Id> {request.Id});
        
        ResellerActivity resellerActivityList = new ResellerActivity(new List<Account_Request__c> {request});
      
      	List<Account_Request__c> requestList = resellerActivity.run();
        
        resellerActivity.run(isInsert);
        
        resellerActivity.lastOpportunityActivity(request.Id);
        
        System.assertEquals(false, requestList.isEmpty());
    
      Test.stopTest(); 
	} 
    
    @IsTest 
    public static void lastAccountActivityEmptyTest() {
      
	  Account account = new Account();  
        
      account.RecordTypeId = '01241000000xyotAAA';
      account.Name = 'teste account';
      account.Legal_Document_Type__c = 'CNPJ';
      account.Id_Company__c = '56947401000155';
      account.Razao_Social__c = 'TESTE';
      account.Website = 'teste.com';
      account.Industry = 'Airlines'; 						
      account.BillingCountry = 'Brazil';
      account.Type = 'Prospect';
      account.Industry = 'Government or Public Management';
 
      insert account;
       
      
      DateTime DATE_NOW = DateTime.Now();  
      DateTime DATE_TODAY_LESS_60 = DATE_NOW.addDays(-60);
       
      Opportunity opportunity = new Opportunity();
        Id oppRtId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        opportunity.recordTypeId = oppRtId;
        opportunity.AccountId = account.id;
        opportunity.Name = 'teste opp';      
        opportunity.CloseDate = Date.today();
        opportunity.StageName = 'Contract';
        opportunity.FastTrackStage__c = 'Contract';
        opportunity.Sales_Channel__c = 'Direct';
        opportunity.Type = 'New partnership';     
        opportunity.Probability = 20;
        insert opportunity;
        
      Account_Request__c request = new Account_Request__c();  
         
      request.Name = 'teste';
      request.Unique_Identifier_Type__c = 'CNPJ';
      request.Unique_Identifier__c = '56947401000155';
      request.Website__c = 'testerequeste.com';
      request.Billing_Country__c = 'Brazil';
      request.AccountId__c = account.Id;
      request.Created_Date_Opportunity__c = null;
      request.Last_Modified_Opportunity__c = null;
      request.Last_Interaction_Opportunity_Event__c = null;

      insert request;
        
      Boolean isInsert = true;

      Test.startTest();
      
      	ResellerActivity resellerActivity = new ResellerActivity(new Set<Id> {request.Id});
        
        ResellerActivity resellerActivityList = new ResellerActivity(new List<Account_Request__c> {request});
      
      	List<Account_Request__c> requestList = resellerActivity.run();
        
        resellerActivity.run(isInsert);
        
        resellerActivity.lastOpportunityActivity(request.Id);
        
        System.assertEquals(false, requestList.isEmpty());

    
      Test.stopTest(); 
	} 
    
    @isTest
    public static void testLastActivityLessEqual30(){
        Account_Request__c req1 = new Account_Request__c(
            Last_Interaction_Account_Event__c = 31,
            Last_Modified_Opportunity__c = 31,
            Last_Interaction_Opportunity_Event__c = 30
        );
        
        Account_Request__c req2 = new Account_Request__c(
            Last_Interaction_Account_Event__c = null,
            Last_Modified_Opportunity__c = null,
            Last_Interaction_Opportunity_Event__c = null
        );
        
        Test.startTest();
        System.assertEquals(true, ResellerActivity.lastActivityLessEqual30(req1));
        System.assertEquals(false, ResellerActivity.lastActivityLessEqual30(req2));
        Test.stopTest();
    }
    
    @isTest
    public static void testLastActivityLessEqual60(){
        Account_Request__c req1 = new Account_Request__c(
            Last_Interaction_Account_Event__c = 61,
            Last_Modified_Opportunity__c = 61,
            Last_Interaction_Opportunity_Event__c = 60
        );
        
        Account_Request__c req2 = new Account_Request__c(
            Last_Interaction_Account_Event__c = null,
            Last_Modified_Opportunity__c = null,
            Last_Interaction_Opportunity_Event__c = null
        );
        
        Test.startTest();
        System.assertEquals(true, ResellerActivity.lastActivityLessEqual60(req1));
        System.assertEquals(false, ResellerActivity.lastActivityLessEqual60(req2));
        Test.stopTest();
    }
    
    @isTest
    public static void testLastActivityLessEqual90(){
        Account_Request__c req1 = new Account_Request__c(
            Last_Interaction_Account_Event__c = 91,
            Last_Modified_Opportunity__c = 91,
            Last_Interaction_Opportunity_Event__c = 60
        );
        
        Account_Request__c req2 = new Account_Request__c(
            Last_Interaction_Account_Event__c = null,
            Last_Modified_Opportunity__c = null,
            Last_Interaction_Opportunity_Event__c = null
        );
        
        Test.startTest();
        System.assertEquals(true, ResellerActivity.lastActivityLessEqual90(req1));
        System.assertEquals(false, ResellerActivity.lastActivityLessEqual90(req2));
        Test.stopTest();
    }
    
    @isTest
    public static void testLastActivityMore30(){
        Account_Request__c req1 = new Account_Request__c(
            Last_Interaction_Account_Event__c = 29,
            Last_Modified_Opportunity__c = 29,
            Last_Interaction_Opportunity_Event__c = 31        
        );
        
        Account_Request__c req2 = new Account_Request__c(
            Last_Interaction_Account_Event__c = null,
            Last_Modified_Opportunity__c = null,
            Last_Interaction_Opportunity_Event__c = null
        );
        
        Test.startTest();
        System.assertEquals(true, ResellerActivity.lastActivityMore30(req1));
        System.assertEquals(false, ResellerActivity.lastActivityMore30(req2));
        Test.stopTest();
    }
    
    @isTest
    public static void testLastActivityMore60(){
        Account_Request__c req1 = new Account_Request__c(
            Last_Interaction_Account_Event__c = 1,
            Last_Modified_Opportunity__c = 1,
            Last_Interaction_Opportunity_Event__c = 100
        );
        
        Account_Request__c req2 = new Account_Request__c(
            Last_Interaction_Account_Event__c = null,
            Last_Modified_Opportunity__c = null,
            Last_Interaction_Opportunity_Event__c = null
        );
        
        Test.startTest();
        System.assertEquals(true, ResellerActivity.lastActivityMore60(req1));
        System.assertEquals(false, ResellerActivity.lastActivityMore60(req2));
        Test.stopTest();
    }
    
    @isTest
    public static void testLastActivityMorel90(){
        Account_Request__c req1 = new Account_Request__c(
            Last_Interaction_Account_Event__c = 1,
            Last_Modified_Opportunity__c = 1,
            Last_Interaction_Opportunity_Event__c = 120
        );
        
        Account_Request__c req2 = new Account_Request__c(
            Last_Interaction_Account_Event__c = null,
            Last_Modified_Opportunity__c = null,
            Last_Interaction_Opportunity_Event__c = null
        );
        
        Test.startTest();
        System.assertEquals(true, ResellerActivity.lastActivityMore90(req1));
        System.assertEquals(false, ResellerActivity.lastActivityMore90(req2));
        Test.stopTest();
    }
    
}