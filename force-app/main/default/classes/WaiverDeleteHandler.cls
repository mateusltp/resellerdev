/**
 * @description       :
 * @author            : AlyssonMota
 * @group             :
 * @last modified on  : 05-12-2021
 * @last modified by  : Alysson Mota
 * Modifications Log
 * Ver   Date         Author         Modification
 * 1.0   05-12-2021   Alysson Mota   Initial Version
 **/
public with sharing class WaiverDeleteHandler {
  public void evaluateOppEnablers(List<Waiver__c> oldWaivers) {
    List<Id> sObjectIdList = new List<Id>();

    for (Waiver__c waiver : oldWaivers) {
      if (waiver.Payment__c != null) {
        sObjectIdList.add(waiver.Payment__c);
      }
    }

    EnablersHelper.updateStepsTowardsSuccess(sObjectIdList);
  }
}