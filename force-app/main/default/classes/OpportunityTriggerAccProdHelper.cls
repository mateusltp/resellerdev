/**
 * @File Name          : OpportunityTriggerAccProdHelper.cls
 * @Description        : 
 * @Author             : Samuel Silva - GFT (slml@gft.com)
 * @Group              : 
 * @Last Modified By   : Samuel Silva - GFT (slml@gft.com)
 * @Last Modified On   : 18/06/2020 10:57:53
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    17/06/2020   Samuel Silva - GFT (slml@gft.com)     Initial Version
**/
public with sharing class OpportunityTriggerAccProdHelper {

    Id recordTypeIdSmall = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Gyms_Small_Medium_Partner').getRecordTypeId();
    Id recordTypeIdWishList = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Gyms_Wishlist_Partner').getRecordTypeId();
    Id recordTypeIdProduct = Schema.SObjectType.Product_Item__c.getRecordTypeInfosByDeveloperName().get('Gyms_Product_Item_Partner').getRecordTypeId();
           
    
    public void updateGymWithProductClasses(){  
        List<Opportunity> lstNew = trigger.new;
        List<Opportunity> oppLst = new List<Opportunity>();        
        Set<Id> oppIds = new Set<Id>();
        Map<Id, String> accWithClassesMap = new Map<Id, String>();        
        for(Opportunity opp : lstNew){             
            if((opp.RecordTypeId == recordTypeIdSmall || opp.RecordTypeId == recordTypeIdWishList) && 
                (opp.AccountId != null ) && (opp.StageName == 'Lançado/Ganho') ){         
                    oppIds.add(opp.Id);
            }
        }

        if(!oppIds.isEmpty()){
            
            oppLst = [SELECT ID, AccountId FROM Opportunity WHERE Id IN: OppIds];
           
            List<Product_Item__c> prodLst = 
                        [SELECT Id, Opportunity__r.AccountId, Gym_Classes__c 
                        FROM Product_Item__c 
                        WHERE Opportunity__c IN: oppIds 
                        AND Gym_Classes__c != null 
                        AND Opportunity__r.AccountId != null];
                        
            Set<String> classes = new Set<String>();
            for(Opportunity opp : oppLst){
                Set<String> classesProd = new Set<String>();
                for(Product_Item__c prod : prodLst){                
                    if(prod.Opportunity__r.AccountId == opp.AccountId){                        
                        classesProd.addAll(prod.Gym_Classes__c.split(';'));
                    }
                }
                if(!classesProd.isEmpty()) {
                    classes.addAll(classesProd);
                    String accountClasses = String.join((Iterable<String>)classes, ';');                 
                    accWithClassesMap.put(opp.AccountId, accountClasses);
                }
            }

            if(!accWithClassesMap.isEmpty()){
                List<Account> accLst = [SELECT ID, Gym_Classes__c FROM Account WHERE Id IN: accWithClassesMap.keySet()];
                for(Account acc : accLst){
                    if(acc.Gym_Classes__c != null){                      
                        Set<String> patchClasses = new Set<String>();   
                        patchClasses.addAll(acc.Gym_Classes__c.split(';'));                      
                        patchClasses.addAll(accWithClassesMap.get(acc.id).split(';'));                  
                        acc.Gym_Classes__c = null;             
                        acc.Gym_Classes__c = String.join((Iterable<String>)patchClasses, ';');
                    } else { 
                        acc.Gym_Classes__c = accWithClassesMap.get(acc.id);
                    }
                }
                UPDATE accLst;
            }
        }
    }
    
   
}