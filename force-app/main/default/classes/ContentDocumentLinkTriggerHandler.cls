/**
* @author Conga Services, ahafez
* @date 20180914
* @version 1.00
* @description ContentDocumentLinkTriggerHandler called from ContentDocumentLinkAfterInsert Trigger
**/
public with sharing class ContentDocumentLinkTriggerHandler extends TriggerHandler{
    /**
    * @author Conga Services, ahafez 
    * @date 20180914
    * @version 1.00
    * @description Checks if LinkedEntity is syncing across Conga Platform and updates IsCongaFile__c field to true if syncing
    */

    public override void afterinsert() {
        CheckForSync(trigger.new);
    }

    public override void afterupdate() {
        CheckForSync(trigger.new);
    }

    private static void CheckForSync(List<ContentDocumentLink> cdlList){
        System.debug('::CheckForSync');
        Set<Id> contentDocumentIds = new Set<Id>();
        List<ContentVersion> cvList = new List<ContentVersion>();
        for(ContentDocumentLink cdl : cdlList){
            if(cdl.LinkedEntity.Type != 'User'){
                contentDocumentIds.add(cdl.ContentDocumentId);
            }
        }
        
        if(contentDocumentIds.size() > 0){
            cvList = [SELECT Id, ContentDocumentId, IsCongaFile__c FROM ContentVersion WHERE ContentDocumentId IN :contentDocumentIds];
        }
        List<ContentVersion> cvToUpdate = new List<ContentVersion>();
        if(cvList.size() >= 0){
            for(ContentDocumentLink cdl : cdlList){
                for(ContentVersion cv : cvList){
                    if(cdl.ContentDocumentId == cv.ContentDocumentId && cv.IsCongaFile__c == false){
                        String linkedEntityPrefix = APXT_Redlining__Contract_Agreement__c.getSObjectType().getDescribe().getKeyPrefix();
                        //if(APXT_Redlining.PlatformDataService.isRecordExcluded(cdl.LinkedEntityId) && 
                          if(((String)cdl.LinkedEntityId).left(3) == linkedEntityPrefix){
                            cvToUpdate.add(new ContentVersion(Id = cv.Id, IsCongaFile__c = true));
                        }
                    }       
                }
            }
            if(cvToUpdate.size()>0){
                //update cvToUpdate;
            }
        }
    }
}