/**
 * @description       : 
 * @author            : David Mantovani - DDMA@gft.com
 * @group             : 
 * @last modified on  : 08-12-2020 
 * @last modified by  : David Mantovani - DDMA@gft.com
 * Modifications Log 
 * Ver   Date         Author                              Modification
 * 1.0   07-16-2020   Samuel Silva - GFT (slml@gft.com)   Initial Version
**/
@isTest
public without sharing class LeadTriggerScoringTest {
  
    @TestSetup
    static void setupData(){        
        Id recordTypeIdAccount =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gym_Partner').getRecordTypeId();
        Id rtIdScoreData = Schema.SObjectType.Score_Data__c.getRecordTypeInfosByDeveloperName().get('Partners').getRecordTypeId();
        Id rtIdOwnerLead = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Partner_Owner_Referral_Lead').getRecordTypeId();

        List<Account> accLst = new List<Account>();        
        Account acc = new Account(
            Name='Company Test', 
            Website='www.acctest.com',  
            RecordTypeId = recordTypeIdAccount, 
            ShippingCountry = 'Brazil', 
            ShippingState = 'Minas Gerais',
            ShippingStreet = 'Bermudas 123', 
            ShippingCity = 'Sorocaba',                       
            Types_of_ownership__c = Label.franchisePicklist,
            Gym_Email__c = 'gymemail@apex.com',
            Phone = '3222123123' 
        );
        accLst.add(acc);
        INSERT accLst;

        List<Account> accLst2 = new List<Account>();        
        Account acc2 = new Account(
            Name='Company Test 02', 
            Website='www.acctest02.com',  
            RecordTypeId = recordTypeIdAccount, 
            ShippingCountry = 'Brazil', 
            ShippingState = 'São Paulo',
            ShippingStreet = 'Roupa 321', 
            ShippingCity = 'São Paulo',                       
            Types_of_ownership__c = Label.franchisePicklist,
            Gym_Email__c = 'gymemail02@apex.com',
            Phone = '2312332221' 
        );
        accLst2.add(acc2);
        INSERT accLst2;

        List<Account> accLst3 = new List<Account>();        
        Account acc3 = new Account(
            Name='Company Test 03', 
            Website='www.acctest03.com',  
            RecordTypeId = recordTypeIdAccount, 
            ShippingCountry = 'Brazil', 
            ShippingState = 'São Paulo',
            ShippingStreet = 'Shorts 123', 
            ShippingCity = 'Campinas',                       
            Types_of_ownership__c = Label.franchisePicklist,
            Gym_Email__c = 'gymemail02@apex.com',
            Phone = '2322123321' 
        );
        accLst3.add(acc3);
        INSERT accLst3;

        List<Account> accLst4 = new List<Account>();        
        Account acc4 = new Account(
            Name='Company Test 04 ARG', 
            Website='www.acctest04arg.com',  
            RecordTypeId = recordTypeIdAccount, 
            ShippingCountry = 'Argentina', 
            ShippingState = 'Buenos Aires',
            ShippingStreet = 'Avenue Messi 10', 
            ShippingCity = 'Almirante Brown',                       
            Types_of_ownership__c = Label.franchisePicklist,
            Gym_Email__c = 'gymemail02@apex.com',
            Phone = '2332223211' 
        );
        accLst4.add(acc4);
        INSERT accLst4;

        List<Account> accLst5 = new List<Account>();        
        Account acc5 = new Account(
            Name='Company Test 05 ARG', 
            Website='www.acctest05arg.com',  
            RecordTypeId = recordTypeIdAccount, 
            ShippingCountry = 'Argentina', 
            ShippingState = 'Buenos Aires',
            ShippingStreet = 'Pijama 123', 
            ShippingCity = 'Arrecifes',                       
            Types_of_ownership__c = Label.franchisePicklist,
            Gym_Email__c = 'gymemail02@apex.com',
            Phone = '2332223211' 
        );
        accLst5.add(acc5);
        INSERT accLst5;

        List<Account> accLst6 = new List<Account>();        
        Account acc6 = new Account(
            Name='Company Test 06', 
            Website='www.acctest06arg.com',  
            RecordTypeId = recordTypeIdAccount, 
            ShippingCountry = 'Argentina', 
            ShippingState = 'Buenos Aires',
            ShippingStreet = 'Camiseta Longa 2002', 
            ShippingCity = 'Azul',                       
            Types_of_ownership__c = Label.franchisePicklist,
            Gym_Email__c = 'gymemail02@apex.com',
            Phone = '2888223999' 
        );
        accLst6.add(acc6);
        INSERT accLst6;

        List<Account> accLst7 = new List<Account>();        
        Account acc7 = new Account(
            Name='Update Lead', 
            Website='www.uplead.com',  
            RecordTypeId = recordTypeIdAccount, 
            ShippingCountry = 'Brazil', 
            ShippingState = 'São Paulo',
            ShippingStreet = 'Av Paulista 283', 
            ShippingCity = 'Sorocaba',                       
            Types_of_ownership__c = Label.franchisePicklist,
            Gym_Email__c = 'upleadgmail@apex.com',
            Phone = '2823999882' 
        );
        accLst7.add(acc7);
        INSERT accLst7;

        Score_Data__c standard = new Score_Data__c();
            standard.RecordTypeId = rtIdScoreData;
            standard.Country_Score_Data__c = 'Standard';
            standard.State_Score_Data__c = 'Standard';
            standard.City_Score_Data__c = 'Standard';
            standard.Number_Of_Active_Users__c = 0;
            standard.Number_Of_Sign_Ups__c = 0;
        INSERT standard;
        System.Debug('Score Data ----- >  Standard ------------  >>> ' + standard);

        Score_Data__c sdt = new Score_Data__c();
            sdt.RecordTypeId = rtIdScoreData;
            sdt.Country_Score_Data__c = 'Brazil';
            sdt.State_Score_Data__c = 'São Paulo';
            sdt.City_Score_Data__c = 'Sorocaba';
            sdt.Number_Of_Active_Users__c = 0;
            sdt.Number_Of_Sign_Ups__c = 0;
        INSERT sdt;
        System.Debug('Score Data ----- >  Sorocaba ------------  >>> ' + sdt);

        Score_Data__c sdtUp = new Score_Data__c();
            sdtUp.RecordTypeId = rtIdScoreData;
            sdtUp.Country_Score_Data__c = 'Brazil';
            sdtUp.State_Score_Data__c = 'São Paulo';
            sdtUp.City_Score_Data__c = 'São Paulo';
            sdtUp.Number_Of_Active_Users__c = 29;
            sdtUp.Number_Of_Sign_Ups__c = 3001;
        INSERT sdtUp;
        System.Debug('Score Data ----- >  São Paulo ------------  >>> ' + sdtUp);

        Score_Data__c sdtUpAct = new Score_Data__c();
            sdtUpAct.RecordTypeId = rtIdScoreData;
            sdtUpAct.Country_Score_Data__c = 'Brazil';
            sdtUpAct.State_Score_Data__c = 'São Paulo';
            sdtUpAct.City_Score_Data__c = 'Campinas';
            sdtUpAct.Number_Of_Active_Users__c = 1001;
            sdtUpAct.Number_Of_Sign_Ups__c = 3001;
        INSERT sdtUpAct;
        System.Debug('Score Data ----- >  Campinas ------------  >>> ' + sdtUpAct);

        Score_Data__c sdtARGSignUp = new Score_Data__c();
            sdtARGSignUp.RecordTypeId = rtIdScoreData;
            sdtARGSignUp.Country_Score_Data__c = 'Argentina';
            sdtARGSignUp.State_Score_Data__c = 'Buenos Aires';
            sdtARGSignUp.City_Score_Data__c = 'Almirante Brown';
            sdtARGSignUp.Number_Of_Active_Users__c = 101;
            sdtARGSignUp.Number_Of_Sign_Ups__c = 101;
        INSERT sdtARGSignUp ;
        System.Debug('Score Data ----- >  Argentina - Almirante Brown  ------------  >>> ' + sdtARGSignUp );

        Score_Data__c sdtARGActive = new Score_Data__c();
            sdtARGActive.RecordTypeId = rtIdScoreData;
            sdtARGActive.Country_Score_Data__c = 'Argentina';
            sdtARGActive.State_Score_Data__c = 'Buenos Aires';
            sdtARGActive.City_Score_Data__c = 'Arrecifes';
            sdtARGActive.Number_Of_Active_Users__c = 51;
            sdtARGActive.Number_Of_Sign_Ups__c = 51;
        INSERT sdtARGActive ;
        System.Debug('Score Data ----- >  Argentina - Arrecifes ------------  >>> ' + sdtARGActive );

        Score_Data__c sdtARGAAzul = new Score_Data__c();
            sdtARGAAzul.RecordTypeId = rtIdScoreData;
            sdtARGAAzul.Country_Score_Data__c = 'Argentina';
            sdtARGAAzul.State_Score_Data__c = 'Buenos Aires';
            sdtARGAAzul.City_Score_Data__c = 'Azul';
            sdtARGAAzul.Number_Of_Active_Users__c = 51;
            sdtARGAAzul.Number_Of_Sign_Ups__c = 51;
    INSERT sdtARGAAzul ;
    System.Debug('Score Data ----- >  Argentina - Azul ------------  >>> ' + sdtARGAAzul );
	
        List<Lead> lstLd = new List<Lead>();
        Lead ldnew = new Lead(
            LastName='Lead GYM',
            RecordTypeId = rtIdOwnerLead, 
            Country = 'Brazil',
            State = 'São Paulo',
            Street = 'Av Paulista 283',
            City = 'Sorocaba',                 
            Phone = '(011)3098-1423',
            Email = 'updatelead@abdc.com',
            Company = 'Update Lead',
            Type_of_Allocation__c = 'Automatic'
            );
        lstLd.add(ldnew);
        INSERT lstLd; 
    }

    @isTest 
    static void testUpdateLead() {
        System.Debug('testUpdateLead ------------  >>> Update Lead');    
    

        Lead leadToUpdate = [SELECT Id, Referral_Score__c, Referral_Account__c , Street , Company FROM LEAD WHERE Email = 'updatelead@abdc.com'];        

        Test.startTest();
        leadToUpdate.Street = 'rua update 01';
        leadToUpdate.Company = 'UpdateGym';

        UPDATE leadToUpdate;
        Test.stopTest(); 

        List<Lead> leadWithScore = [SELECT Id, Referral_Score__c, Referral_Account__c FROM LEAD WHERE Company = 'UpdateGym'];
        
        
        //System.assertEquals(10, leadWithScore.get(0).Referral_Score__c);
        //System.assertEquals(null, leadWithScore.get(0).Referral_Account__c);
    }

    @isTest 
    static void insertLeadsFalseCity() {
        System.Debug('insertLeadsFalseCity ------------  >>> False City ');
        Id rtIdOwnerLead = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Partner_Owner_Referral_Lead').getRecordTypeId();    
        

        Test.startTest();
            Lead ld = new Lead();
            ld.LastName='False City'; 
            ld.RecordTypeId = rtIdOwnerLead; 
            ld.Country = 'Peru';
            ld.State = '';
            ld.Street = 'Perauvamaçãbanana'; 
            ld.City = 'JaboticaMelancia';                 
            ld.Phone = '(011)3098-1423';
            ld.Email = 'feiradafrutad@abdc.com';
            ld.Company = 'CompanyFeria';
            ld.Type_of_Allocation__c = 'Automatic';
              
        INSERT ld;
        Test.stopTest(); 

        List<Lead> leadWithScore = [SELECT Id, Referral_Score__c, Referral_Account__c FROM LEAD WHERE Company = 'CompanyFeria'];
        
        //System.assertEquals(0, leadWithScore.get(0).Referral_Score__c);
        //System.assertEquals(null, leadWithScore.get(0).Referral_Account__c);
    }

    @isTest 
    static void insertLeadsNoDupListScore10Percent() {
        System.Debug('insertLeadsNoDupListScore10Percent ------------  >>> 10%');
        Id rtIdOwnerLead = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Partner_Owner_Referral_Lead').getRecordTypeId();    
        

        Test.startTest();
            Lead ld = new Lead();
            ld.LastName='LeadTest'; 
            ld.RecordTypeId = rtIdOwnerLead; 
            ld.Country = 'Brazil';
            ld.State = 'São Paulo';
            ld.Street = 'Bermudas 123'; 
            ld.City = 'Sorocaba';                 
            ld.Phone = '(011)2345-2345';
            ld.Email = 'testlead@abdc.com';
            ld.Company = 'Company Test No Dup List';
            ld.Type_of_Allocation__c = 'Automatic';
              
        INSERT ld;
        Test.stopTest(); 

        List<Lead> leadWithScore = [SELECT Id, Referral_Score__c, Referral_Account__c FROM LEAD WHERE Company = 'Company Test No Dup List'];
        
       // System.assertEquals(10, leadWithScore.get(0).Referral_Score__c);
       // System.assertEquals(null, leadWithScore.get(0).Referral_Account__c);
    }

    @isTest 
    static void insertLeadsToScore10Percent() {
        System.Debug('insertLeadsToScore10Percent ------------  >>> 10% ');
        Id rtIdOwnerLead = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Partner_Owner_Referral_Lead').getRecordTypeId();    
        List<Lead> leadLst = new List<Lead>();

        Test.startTest();
        for(Integer i = 0; i<5; i++) {
            Lead ld = new Lead(
                LastName='LeadTest', 
                RecordTypeId = rtIdOwnerLead, 
                Country = 'Brazil', 
                State = 'São Paulo',
                Street = 'Bermudas 123', 
                City = 'Sorocaba',                       
                Phone = '(011)2345-2345',
                Email = 'testlead@abdc.com',
                Company = 'Company Test',
                Type_of_Allocation__c = 'Automatic'
                );
            leadLst.add(ld);
        }      
        INSERT leadLst;
        Test.stopTest(); 

        List<Lead> leadWithScore = [SELECT Id, Referral_Score__c, Referral_Account__c FROM LEAD WHERE Company = 'Company Test'];
        Account acc = [SELECT Id, Referral_Score__c FROM Account WHERE Name='Company Test'];
        
       // System.assertEquals(10, leadWithScore.get(0).Referral_Score__c);
        //System.assertEquals(acc.Id, leadWithScore.get(0).Referral_Account__c);
    }
    
    @isTest 
    static void insertLeadsToScore20Percent() {
        System.Debug('insertLeadsToScore20Percent ------------  >>> 20% ');
        Id rtIdOwnerLead = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Partner_Owner_Referral_Lead').getRecordTypeId();
        List<Lead> leadLst = new List<Lead>();

        Test.startTest();
        for(Integer i = 0; i<6; i++) {
            Lead ld = new Lead(
                LastName='LeadTest', 
                RecordTypeId = rtIdOwnerLead, 
                Country = 'Brazil', 
                State = 'São Paulo',
                Street = 'Bermudas 123', 
                City = 'Sorocaba',                       
                Phone = '(011)2345-2345',
                Email = 'testlead@abdc.com',
                Company = 'Company Test',
                Type_of_Allocation__c = 'Automatic'
                );
            leadLst.add(ld);
        }      
        INSERT leadLst;
        Test.stopTest(); 

        List<Lead> leadWithScore = [SELECT Id, Referral_Score__c, Referral_Account__c FROM LEAD WHERE Company = 'Company Test'];
        Account acc = [SELECT Id, Referral_Score__c FROM Account WHERE Name='Company Test'];

        //System.assertEquals(20, leadWithScore.get(0).Referral_Score__c);
        //System.assertEquals(acc.Id, leadWithScore.get(0).Referral_Account__c);
      
    }
    
    @isTest 
    static void insertLeadsToScore30Percent() {
        System.Debug('insertLeadsToScore30Percent ------------  >>> 30% ');
        Id rtIdOwnerLead = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Partner_Owner_Referral_Lead').getRecordTypeId();
        List<Lead> leadLst = new List<Lead>();

        Test.startTest();
        for(Integer i = 0; i<7; i++) {
            Lead ld = new Lead(
                LastName='LeadTest', 
                RecordTypeId = rtIdOwnerLead, 
                Country = 'Brazil', 
                State = 'São Paulo',
                Street = 'Bermudas 123', 
                City = 'Sorocaba',                       
                Phone = '(011)2345-2345',
                Email = 'testlead@abdc.com',
                Company = 'Company Test',
                Type_of_Allocation__c = 'Automatic'
                );
            leadLst.add(ld);
        }      
        INSERT leadLst;
        Test.stopTest(); 

        List<Lead> leadWithScore = [SELECT Id, Referral_Score__c, Referral_Account__c FROM LEAD WHERE Company = 'Company Test'];
        Account acc = [SELECT Id, Referral_Score__c FROM Account WHERE Name='Company Test'];
        
        //System.assertEquals(30, leadWithScore.get(0).Referral_Score__c);
        //System.assertEquals(acc.Id, leadWithScore.get(0).Referral_Account__c);
        
    }

    @isTest 
    static void insertLeadsToScore40Percent() {
        System.Debug('insertLeadsToScore40Percent ------------  >>> 40% ');
        Id rtIdOwnerLead = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Partner_Owner_Referral_Lead').getRecordTypeId();
        List<Lead> leadLst = new List<Lead>();

        Test.startTest();
        for(Integer i = 0; i<9; i++) {
            Lead ld = new Lead(
                LastName='LeadTest', 
                RecordTypeId = rtIdOwnerLead, 
                Country = 'Brazil', 
                State = 'São Paulo',
                Street = 'Bermudas 123', 
                City = 'Sorocaba',                       
                Phone = '(011)2345-2345',
                Email = 'testlead@abdc.com',
                Company = 'Company Test',
                Type_of_Allocation__c = 'Automatic'
                );
            leadLst.add(ld);
        }      
        INSERT leadLst;
        Test.stopTest(); 

        List<Lead> leadWithScore = [SELECT Id, Referral_Score__c, Referral_Account__c FROM LEAD WHERE Company = 'Company Test'];
        Account acc = [SELECT Id, Referral_Score__c FROM Account WHERE Name='Company Test'];

        //System.assertEquals(40, leadWithScore.get(0).Referral_Score__c);
        //System.assertEquals(acc.Id, leadWithScore.get(0).Referral_Account__c);
    }

    @isTest 
    static void insertLeadsToScore50Percent() {
        System.Debug('insertLeadsToScore50Percent ------------  >>> 50% ');
        Id rtIdOwnerLead = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Partner_Owner_Referral_Lead').getRecordTypeId();
        List<Lead> leadLst = new List<Lead>();

        Test.startTest();
        for(Integer i = 0; i<11; i++) {
            Lead ld = new Lead(
                LastName='LeadTest', 
                RecordTypeId = rtIdOwnerLead, 
                Country = 'Brazil', 
                State = 'São Paulo',
                Street = 'Bermudas 123', 
                City = 'Sorocaba',                       
                Phone = '(011)2345-2345',
                Email = 'testlead@abdc.com',
                Company = 'Company Test',
                Type_of_Allocation__c = 'Automatic'
                );
            leadLst.add(ld);
        }      
        INSERT leadLst;
        Test.stopTest(); 

        List<Lead> leadWithScore = [SELECT Id, Referral_Score__c, Referral_Account__c FROM LEAD WHERE Company = 'Company Test'];
        Account acc = [SELECT Id, Referral_Score__c FROM Account WHERE Name='Company Test'];

        //System.assertEquals(50, leadWithScore.get(0).Referral_Score__c);
        //System.assertEquals(acc.Id, leadWithScore.get(0).Referral_Account__c);
    }



    @isTest 
    static void insertLeadsToScore70Percent() {
        System.Debug('insertLeadsToScore70Percent ------------  >>> 70% ');
        Id rtIdOwnerLead = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Partner_Owner_Referral_Lead').getRecordTypeId();
        List<Lead> leadLst = new List<Lead>();

        Test.startTest();
        for(Integer i = 0; i<4; i++) {
            Lead ld = new Lead(
                LastName='LeadTest', 
                RecordTypeId = rtIdOwnerLead, 
                Country = 'Argentina', 
                State = 'Buenos Aires',
                Street = 'Calça 123', 
                City = 'Azul',                       
                Phone = '(011)3455-3455',
                Email = 'testlead01@abdc.com',
                Company = 'Company Test 06',
                Type_of_Allocation__c = 'Automatic'
                );
            leadLst.add(ld);
        }      
        INSERT leadLst;
        Test.stopTest(); 

        List<Lead> leadWithScore = [SELECT Id, Referral_Score__c, Referral_Account__c FROM LEAD WHERE Company = 'Company Test 06'];
        Account acc = [SELECT Id, Referral_Score__c FROM Account WHERE Name='Company Test 06'];

       // System.assertEquals(70, leadWithScore.get(0).Referral_Score__c);
    }

    @isTest 
    static void insertLeadsToScore90Percent() {
        System.Debug('insertLeadsToScore90Percent ------------  >>> 90% ');
        Id rtIdOwnerLead = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Partner_Owner_Referral_Lead').getRecordTypeId();
        List<Lead> leadLst = new List<Lead>();

        Test.startTest();
        for(Integer i = 0; i<3; i++) {
            Lead ld = new Lead(
                LastName='LeadTest', 
                RecordTypeId = rtIdOwnerLead, 
                Country = 'Argentina', 
                State = 'Buenos Aires',
                Street = 'Avenue Messi 10', 
                City = 'Almirante Brown',                       
                Phone = '(023) 9855-3455',
                Email = 'tesargenina@abdc.com',
                Company = 'Company Test 04 ARG',
                Type_of_Allocation__c = 'Automatic'
                );
            leadLst.add(ld);
        }      
        INSERT leadLst;
        Test.stopTest(); 

        List<Lead> leadWithScore = [SELECT Id, Referral_Score__c, Referral_Account__c FROM LEAD WHERE Company = 'Company Test 04 ARG'];
        Account acc = [SELECT Id, Referral_Score__c FROM Account WHERE Name='Company Test 04 ARG'];

        //System.assertEquals(90, leadWithScore.get(0).Referral_Score__c);
        //System.assertEquals(acc.Id, leadWithScore.get(0).Referral_Account__c);
    }


    @isTest 
    static void insertLeadsToScore100Percent() {
        System.Debug('insertLeadsToScore100Percent ------------  >>> 100% ');
        Id rtIdOwnerLead = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Partner_Owner_Referral_Lead').getRecordTypeId();
        List<Lead> leadLst = new List<Lead>();

        Test.startTest();
        for(Integer i = 0; i<14; i++) {
            Lead ld = new Lead(
                LastName='LeadTest', 
                RecordTypeId = rtIdOwnerLead, 
                Country = 'Brazil', 
                State = 'São Paulo',
                Street = 'Shorts 123', 
                City = 'Campinas',                       
                Phone = '(011)3455-3455',
                Email = 'testlead01@abdc.com',
                Company = 'Company Test 03',
                Type_of_Allocation__c = 'Automatic'
                );
            leadLst.add(ld);
        }      
        INSERT leadLst;
        Test.stopTest(); 

        List<Lead> leadWithScore = [SELECT Id, Referral_Score__c, Referral_Account__c FROM LEAD WHERE Company = 'Company Test 03'];
        Account acc = [SELECT Id, Referral_Score__c FROM Account WHERE Name='Company Test 03'];

       // System.assertEquals(100, leadWithScore.get(0).Referral_Score__c);
        //System.assertEquals(acc.Id, leadWithScore.get(0).Referral_Account__c);
    }

}