/**
 * @description       : 
 * @author            : tiago.ribeiro@gympass.com
 * @group             : 
 * @last modified on  : 07-04-2022
 * @last modified by  : tiago.ribeiro@gympass.com
**/
public with sharing class SKUWaiverHandler {
    public void updateWaiversOnLaunch(List<Opportunity> newOpps, Map<Id,Opportunity> oldMap){
        Id clientNewBusinessSKU = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_SKU_New_Business').getRecordTypeId();
        Id clientRenegotiationSKU = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Success_SKU_Renegotiation').getRecordTypeId();
        Id smbNewBusinessSKU = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SMB_SKU_New_Business').getRecordTypeId();
        Id smbRenegotiationSKU = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SMB_Success_SKU_Renegotiation').getRecordTypeId();
        
        Set<Id> setIdQuotes = new Set<Id>();
        for (Opportunity newOpp : newOpps) {
            Opportunity oldOpp = oldMap.get(newOpp.Id);
            if ((newOpp.RecordTypeId == clientNewBusinessSKU || newOpp.RecordTypeId == clientRenegotiationSKU ||
                    newOpp.RecordTypeId == smbNewBusinessSKU || newOpp.RecordTypeId == smbRenegotiationSKU) 
                    && oldOpp.StageName != 'Lançado/Ganho' && newOpp.StageName == 'Lançado/Ganho')
                setIdQuotes.add(newOpp.SyncedQuoteId);
        }

        handleSKUWaivers(setIdQuotes);
    }

    public void handleSKUWaivers(Set<Id> setIdQuotes){
        List<Waiver__c> listWaiversToUpdate = new List<Waiver__c>();
        
        List<Waiver__c> listWaivers = new WaiverRepository().getWaiversByQuote(setIdQuotes);
        String startDateField = 'Start_Date__c';

        Waiver__c previousWaiver = new Waiver__c();
        for(Waiver__c waiver : listWaivers){
            if(waiverCantBeProcessed(startDateField, waiver)){
                continue;
            }
            waiver.Start_Date__c = assignWaiverStartDate(startDateField, waiver, previousWaiver);
            waiver.End_Date__c = waiver.Start_Date__c.addMonths(Integer.valueOf(waiver.Duration__c)).addDays(-1);
            previousWaiver = waiver;
            listWaiversToUpdate.add(waiver);
        }

        handDmlStatements(listWaiversToUpdate);
    }

    public Boolean waiverCantBeProcessed(String startDateField, Waiver__c waiver){
        return String.isBlank(startDateField) || waiver.getSObject('Quote_Line_Item__r').getSObject('Quote').get(startDateField) == null || waiver.Duration__c == null || waiver.Position__c == null;
    }

    public Date assignWaiverStartDate(String startDateField, Waiver__c waiver, Waiver__c previousWaiver){
        if (waiver.OrderWaiver_Start_Date__c != null)
            return waiver.OrderWaiver_Start_Date__c;
        
        Date startDate = Date.today();
        
        if(previousWaiver.Position__c != null && waiver.Position__c == (previousWaiver.Position__c + 1)){
            startDate = previousWaiver.End_Date__c.addDays(1);
        }else{
            startDate = Date.valueOf(waiver.getSObject('Quote_Line_Item__r').getSObject('Quote').get(startDateField));
        }

        return startDate;
    }

    public void handDmlStatements(List<Waiver__c> listWaiversToUpdate){
        update listWaiversToUpdate;
    }
}