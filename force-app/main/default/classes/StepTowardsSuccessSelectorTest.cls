/**
 * @description       : 
 * @author            : gft.jorge.stevaux@ext.gympass.com
 * @group             : 
 * @last modified on  : 04-14-2022
 * @last modified by  : gft.jorge.stevaux@ext.gympass.com
**/
@isTest(seeAllData=false)

private class StepTowardsSuccessSelectorTest {

    @TestSetup
    static void createData(){

        Account lAcc = PartnerDataFactory.newAccount();              
        Database.insert( lAcc );

        Contact lContact = PartnerDataFactory.newContact(lAcc.Id);
        Database.insert(lContact);

        Bank_Account__c lBankAcct = PartnerDataFactory.newBankAcct();
        Database.insert(lBankAcct);

        Acount_Bank_Account_Relationship__c lBankAcctRel = PartnerDataFactory.newBankAcctRel(lAcc.Id, lBankAcct);
        Database.insert(lBankAcctRel);
                                  
        Opportunity lOpp = PartnerDataFactory.newOpportunity( lAcc.Id, 'Partner_Wishlist_Renegotiation' ); 
        Database.insert( lOpp );

        Product_Item__c lProd = PartnerDataFactory.newProduct(lOpp);
        Database.insert(lProd);

        Step_Towards_Success_Partner__c lSteps = PartnerDataFactory.newStepTowardsSuccess(lOpp.id, lAcc.Id );
        lSteps.RecordTypeId = Schema.SObjectType.Step_Towards_Success_Partner__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Opportunity_Step').getRecordTypeId();
        Database.insert(lSteps);
    }

    @isTest
    static void getSObjectType_Test(){

        Test.startTest();
        System.assertEquals(Step_Towards_Success_Partner__c.sObjectType, new StepTowardsSuccessSelector().getSObjectType());
        Test.stopTest();                           
    }

    @isTest
    static void getSObjectFieldList_Test(){

        List<Schema.SObjectField> expectedReturn = new List<Schema.SObjectField> {
			Step_Towards_Success_Partner__c.Id
		};

        Test.startTest();
        System.assertEquals(expectedReturn, new StepTowardsSuccessSelector().getSObjectFieldList());
        Test.stopTest();
    }

    @isTest
    static void oppPartnerStepByOppId_Test(){

        List<Opportunity> oppLst = [SELECT Id, Name FROM Opportunity LIMIT 1];
        Set<Id> oppIds = (new Map<Id,Opportunity>(oppLst)).keySet();

        List<Step_Towards_Success_Partner__c> stepLst = [SELECT Id, Name FROM Step_Towards_Success_Partner__c WHERE Related_Opportunity__c IN : oppIds LIMIT 1 ];


        Test.startTest();
        System.assertEquals(stepLst.size(), new StepTowardsSuccessSelector().oppPartnerStepByOppId(oppIds).size());
        Test.stopTest();
    }

   
    
}