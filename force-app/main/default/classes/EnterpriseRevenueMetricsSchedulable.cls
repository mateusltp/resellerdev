global class EnterpriseRevenueMetricsSchedulable implements Schedulable{
    global void execute(SchedulableContext sc){
        EnterpriseRevenueMetricsBatchable b = new EnterpriseRevenueMetricsBatchable();
        database.executebatch(b,10);
    }
   
}