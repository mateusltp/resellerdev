public with sharing class DealDeskOperationApproveSameOpportunity {   

    List< Assert_Data__c > lstAssertDataToUpdt = new List< Assert_Data__c >();
    private Id gDealDeskOperationalRecTypeId = 
        Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Deal_Desk_Operational').getRecordTypeId();
    Map< String, List<Assert_Data__c> > previousCaseInAssertData = new Map< String, List<Assert_Data__c> >();

    public void approveAtTheSameOpportunity( Case dealDeskOpCase  ){                       
        
        populateMapAssertData(dealDeskOpCase);

        if( dealDeskOpCase.RecordTypeId == gDealDeskOperationalRecTypeId ){ 
            updateAssertData(dealDeskOpCase);
        }
    }

    public List<Assert_Data__c> approveAtTheSameOpportunity( Case dealDeskOpCase, List<Assert_Data__c> assertDatas, Map<String, Approval_Item__c> itemsRejectedByCaseFieldName){                       

        List<Assert_Data__c> assertDataToUpsert = new List <Assert_Data__c>();  
        previousCaseInAssertData.put(dealDeskOpCase.OpportunityId__c, new List<Assert_Data__c>());

        for ( Assert_Data__c assertData : assertDatas ){
            if(assertData.Object_Name__c == 'case' &&  assertData.Opportunity__c == dealDeskOpCase.OpportunityId__c){
                previousCaseInAssertData.get(assertData.Opportunity__c).add(assertData);            
            }
        }

        if( previousCaseInAssertData.containsKey(dealDeskOpCase.OpportunityId__c) && !previousCaseInAssertData.get(dealDeskOpCase.OpportunityId__c).isEmpty() ){                
            assertDataToUpsert = updatePreviousCaseInAssertData(dealDeskOpCase, itemsRejectedByCaseFieldName);                
        }else{
            assertDataToUpsert= insertCaseInAssertData(dealDeskOpCase, itemsRejectedByCaseFieldName);
        }
        return assertDataToUpsert;
         
    }

    private void updateAssertData( Case dealDeskOpCase ){
        List<Assert_Data__c> assertDataToUpdate = new List<Assert_Data__c>();
        populateMapAssertData(dealDeskOpCase);

        if( previousCaseInAssertData.containsKey(dealDeskOpCase.OpportunityId__c) && !previousCaseInAssertData.get(dealDeskOpCase.OpportunityId__c).isEmpty() ){                
            assertDataToUpdate = updatePreviousCaseInAssertData(dealDeskOpCase);  
        }else{                
            assertDataToUpdate = insertCaseInAssertData(dealDeskOpCase);
        }   

        if(assertDataToUpdate.size()>0)
            Database.upsert(assertDataToUpdate);              
    
    }

    private List<Assert_Data__c> updatePreviousCaseInAssertData (Case dealDeskOpCase){        
        List<Assert_Data__c> assertDataToUpdate = new List <Assert_Data__c>();        

        for( Assert_Data__c caseInAssertData : previousCaseInAssertData.get(dealDeskOpCase.OpportunityId__c) ){
            if (dealDeskOpCase.get(caseInAssertData.Field_Name__c) != caseInAssertData.Old_Value__c ){
                caseInAssertData.Old_Value__c = string.valueOf(dealDeskOpCase.get(caseInAssertData.Field_Name__c));
                assertDataToUpdate.add(caseInAssertData);
            }
        }
        return assertDataToUpdate;
    }

    private List<Assert_Data__c> updatePreviousCaseInAssertData (Case dealDeskOpCase, Map<String, Approval_Item__c> itemsRejectedByCaseFieldName){        

        List<Assert_Data__c> assertDataToUpdate = new List <Assert_Data__c>();        
         for( Assert_Data__c caseInAssertData : previousCaseInAssertData.get(dealDeskOpCase.OpportunityId__c) ){

            if (dealDeskOpCase.get(caseInAssertData.Field_Name__c) != caseInAssertData.Old_Value__c && 
                itemsRejectedByCaseFieldName.get(caseInAssertData.Field_Name__c) == null ){
                    caseInAssertData.Old_Value__c = string.valueOf(dealDeskOpCase.get(caseInAssertData.Field_Name__c));
                    assertDataToUpdate.add(caseInAssertData);
            }
        }
        return assertDataToUpdate;
    }

    private List<Assert_Data__c> insertCaseInAssertData(Case dealDeskOpCase, Map<String, Approval_Item__c> itemsRejectedByCaseFieldName){
        List<Assert_Data_Objects__mdt> caseFields = new List<Assert_Data_Objects__mdt>();

        Map<String, Assert_Data_Objects__mdt> assertDatasMdtByName = Assert_Data_Objects__mdt.getAll();
        for(Assert_Data_Objects__mdt mdt :assertDatasMdtByName.values() ){
            if(mdt.Opportunity__c == True &&  mdt.Object_Name__c == 'case'){
                caseFields.add(mdt);
            }
        }
        
        List<Assert_Data__c> assertDataToUpdate = new List <Assert_Data__c>();

        for(Assert_Data_Objects__mdt caseField : caseFields){
            String cField = caseField.Field_Name__c;
                    Assert_Data__c newCaseInAssertData = new Assert_Data__c(
                        Opportunity__c = String.valueOf(dealDeskOpCase.OpportunityId__c),
                        Object_Name__c = caseField.Object_Name__c.toLowerCase(),
                        Field_Name__c = caseField.Field_Name__c.toLowerCase(),
                        Pre_Approved_Condition__c = false
                    );

                    if(itemsRejectedByCaseFieldName.get(cField.toLowerCase()) == null ){
                        newCaseInAssertData.Old_Value__c = String.valueOf(dealDeskOpCase.get(cField));
                    }
                    assertDataToUpdate.add(newCaseInAssertData);
               system.debug('tassert: '+assertDataToUpdate); 
        }
        return assertDataToUpdate;
    }


    private List<Assert_Data__c> insertCaseInAssertData(Case dealDeskOpCase){
        List<Assert_Data_Objects__mdt> caseFields = new List<Assert_Data_Objects__mdt>();

        Map<String, Assert_Data_Objects__mdt> assertDatasMdtByName = Assert_Data_Objects__mdt.getAll();
        for(Assert_Data_Objects__mdt mdt :assertDatasMdtByName.values() ){
            if(mdt.Opportunity__c == True &&  mdt.Object_Name__c == 'case'){
                caseFields.add(mdt);
            }
        }
        
        List<Assert_Data__c> assertDataToUpdate = new List <Assert_Data__c>();

        for(Assert_Data_Objects__mdt caseField : caseFields){
            String cField = caseField.Field_Name__c;
                Assert_Data__c newCaseInAssertData = new Assert_Data__c(
                    Opportunity__c = String.valueOf(dealDeskOpCase.OpportunityId__c),
                    Object_Name__c = caseField.Object_Name__c.toLowerCase(),
                    Field_Name__c = caseField.Field_Name__c.toLowerCase(),
                    Old_Value__c = String.valueOf(dealDeskOpCase.get(cField))
                );
                assertDataToUpdate.add(newCaseInAssertData);
        }
        return assertDataToUpdate;
    }
    
    
    private void populateMapAssertData( Case dealDeskOpCase ){
        previousCaseInAssertData.put(dealDeskOpCase.OpportunityId__c, new List<Assert_Data__c>());

        for ( Assert_Data__c assertData : [SELECT Opportunity__c, Old_Value__c, Field_Name__c, Fee_Type__c, Object_Name__c FROM Assert_Data__c WHERE Object_Name__c = 'case' AND Opportunity__c = :dealDeskOpCase.OpportunityId__c ] ){
                previousCaseInAssertData.get(assertData.Opportunity__c).add(assertData);            
        }
    }
}