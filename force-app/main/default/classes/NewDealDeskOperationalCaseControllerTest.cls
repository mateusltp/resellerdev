@isTest
public class NewDealDeskOperationalCaseControllerTest {
    
    
    @TestSetup
    static void createData(){
        Pricebook2 lStandardPricebook = DataFactory.newPricebook();
        Database.update( lStandardPricebook );
        
        Account lAcc = DataFactory.newAccount();
        Database.insert(lAcc);
        
        Opportunity OldNewBusinessOpp = DataFactory.newOpportunity( lAcc.Id, lStandardPricebook, 'Client_Sales_New_Business',true );  
        
        OldNewBusinessOpp.fasttrackstage__c='Qualification';                
        Database.insert(OldNewBusinessOpp);
        
        Quote oldQuote = DataFactory.newQuote(OldNewBusinessOpp, '1', 'Gympass_Plus');
        oldQuote.Employee_Corporate_Email__c =false;
        oldQuote.end_date__c = Date.today();
        oldQuote.contact_permission__c = 'Sign-up only';
        oldQuote.ExpirationDate = date.valueOf('2021-08-16');
        
        oldQuote.autonomous_marketplace_contract__c = 'Yes';
        oldQuote.Discount_Approval_Level__c = 4;
        oldQuote.Free_Trial_Days__c = 7;                
        Database.insert(oldQuote);
        
        OldNewBusinessOpp.Name = 'New Business';
        OldNewBusinessOpp.SyncedQuoteId = oldQuote.Id;
        OldNewBusinessOpp.StageName = 'Lançado/Ganho';
        OldNewBusinessOpp.FastTrackStage__c = 'Offer Approved';                
        Database.update(OldNewBusinessOpp);                                
        
        Product2 accessFeeProduct = DataFactory.newProduct('Enterprise Subscription', false, 'BRL');
        Product2 setupFeeProduct = DataFactory.newProduct('Setup Fee', false, 'BRL');
        Product2 professionalServicesMaintenanceFeeProduct = DataFactory.newProduct('Professional Services Maintenance Fee', false, 'BRL');
        Product2 professionalServicesSetupFeeProduct = DataFactory.newProduct('Professional Services Setup Fee', false, 'BRL');                
        Database.insert(new List<Product2>{accessFeeProduct, setupFeeProduct, professionalServicesMaintenanceFeeProduct, professionalServicesSetupFeeProduct});                
        
        PricebookEntry accessFeeEntry = DataFactory.newPricebookEntry(lStandardPricebook, accessFeeProduct, OldNewBusinessOpp);
        PricebookEntry setupFeeEntry = DataFactory.newPricebookEntry(lStandardPricebook, setupFeeProduct, OldNewBusinessOpp);
        PricebookEntry professionalServicesMaintenanceFeeEntry = DataFactory.newPricebookEntry(lStandardPricebook, professionalServicesMaintenanceFeeProduct, OldNewBusinessOpp);
        PricebookEntry professionalServicesSetupFeeEntry = DataFactory.newPricebookEntry(lStandardPricebook, professionalServicesSetupFeeProduct, OldNewBusinessOpp);                
        Database.insert(new List<PricebookEntry>{accessFeeEntry, setupFeeEntry, professionalServicesMaintenanceFeeEntry, professionalServicesSetupFeeEntry});               
     
    }
    
    @isTest
    public static void getCaseRecordTypeIdTest() {
        String caseRecordTypeName = 'Deal Desk Operational'; 
        
        RecordType caseRecordType = [SELECT Id FROM RecordType WHERE Name = :caseRecordTypeName];
        Test.startTest();
        String dealDeskOperationalId = NewDealDeskOperationalCaseController.getCaseRecordTypeId(caseRecordTypeName);
        Test.stopTest();
                        
        System.assertEquals( dealDeskOperationalId , caseRecordType.Id ,'The getCaseRecordType is not getting the correct Id value.');
    }


    @isTest
    public static void getPreviousDealDeskOperationalCaseTest() {

        Test.startTest();
        Map<String,String> valueOfLastChangesOfPreviousCaseByFieldName = new Map<String,String>();
        List<Opportunity> lOpp = [ SELECT Id, Name, CreatedDate, AccountId, SyncedQuote.ExpirationDate, Achieved_Steps_Towards_Success_Quantity__c  FROM Opportunity ];
        Pricebook2 pricebook = [SELECT Id from Pricebook2 Limit 1];
        List<Opportunity> listOppToRenegociation = new List<Opportunity>();
        Id quoteToCreateCase;                
        
        Case lOperationalCase = DataFactory.newCase(lOpp[0].id, quoteToCreateCase, 'Deal_Desk_Operational');
        Database.insert( lOperationalCase ); 
        lOperationalCase.Status = 'Approved';

        Database.update(lOperationalCase);            

        Opportunity lRenegociationOpp = DataFactory.newOpportunity( lOpp[0].AccountId, pricebook, 'Client_Success_Renegotiation',true );  
        Database.insert(lRenegociationOpp);
        
        List<Assert_Data__c> changesOnPreviousCase = [SELECT Id, Opportunity__c, Object_Name__c, Field_Name__c, Old_Value__c, New_Value__c, Fee_Type__c, Approved_Value__c
                                                   FROM Assert_Data__c WHERE Opportunity__c =: lRenegociationOpp.id AND Object_Name__c = 'Case' ];

        for(Assert_Data__c oneChange :changesOnPreviousCase){
            String fieldNameOneChange = oneChange.Field_Name__c;
                valueOfLastChangesOfPreviousCaseByFieldName.put(fieldNameOneChange, oneChange.Old_Value__c);
        }

        Map<String,String> defaultValuesByFieldName = NewDealDeskOperationalCaseController.getPreviousDealDeskOperationalCase(lRenegociationOpp.Id);

        System.assertEquals(defaultValuesByFieldName.get('eligible_list_registration_method__c'), valueOfLastChangesOfPreviousCaseByFieldName.get('eligible_list_registration_method__c') ); 
        System.assertEquals(defaultValuesByFieldName.size(), valueOfLastChangesOfPreviousCaseByFieldName.size(), 'The size of Assert Data related to the Object Case is not correct.' ); 

        Test.stopTest();

    }


    @isTest
    public static void getQueueIdTest() {
      
        String queueType = 'Queue';
        String queueDeveloperName = 'Deal_Desk_Operational';
        List<Group> queueDetails = [SELECT Id FROM Group WHERE TYPE =:queueType AND DeveloperName = :queueDeveloperName];

        Test.startTest();
        String queueId = NewDealDeskOperationalCaseController.getQueueId(queueDeveloperName);
        Test.stopTest();
                        
        System.assertEquals( queueId , queueDetails.get(0).Id, 'The getQueueId is not returning the correct value.' );
    }


    @isTest
    public static void getDefaultQuoteIdTest() {
      
        Quote newQuote = [ SELECT Id, OpportunityId FROM Quote WHERE Opportunity.RecordType.DeveloperName ='Client_Sales_New_Business' LIMIT 1]; 
        Quote newOppQuote = [SELECT Id FROM Quote WHERE contact_permission__c = 'Sign-up only' LIMIT 1];

        Test.startTest();

        String newQuoteId = NewDealDeskOperationalCaseController.getDefaultQuoteId(newQuote.OpportunityId);
        System.assertEquals(newOppQuote.Id,newQuoteId,'Default Quote is not correct.');

        Test.stopTest();
                        
    }

    @isTest
    public static void getQuoteLineItemsTest() {
      
        Opportunity opp = [ SELECT Id FROM Opportunity LIMIT 1]; 
        Test.startTest();
        NewDealDeskOperationalCaseController.getQuoteLineItems(opp.Id);
        Test.stopTest();                    
    }

    @isTest
    public static void getMembershipFeePaymentMethodTest() {
      
        Opportunity opp = [ SELECT Id FROM Opportunity LIMIT 1]; 
        Test.startTest();
        NewDealDeskOperationalCaseController.getMembershipFeePaymentMethod(opp.Id);
        Test.stopTest();                   
    }

}