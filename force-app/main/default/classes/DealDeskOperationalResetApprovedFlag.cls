/**
 * @description       : 
 * @author            : roei@gft.com
 * @group             : 
 * @last modified on  : 12-16-2020
 * @last modified by  : roei@gft.com
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   12-15-2020   roei@gft.com   Initial Version
**/
public without sharing class DealDeskOperationalResetApprovedFlag {
	private Id gDealDeskOperationalRecTypeId = 
        Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Deal_Desk_Operational').getRecordTypeId();
    
    public void resetSpecialistApprovedFromOpp( List< Case > aLstNewCase ){
        Set< Id > lSetOppIdToResetFlag = new Set< Id >();
        
    	for( Case iNewCase : aLstNewCase ){
            if( iNewCase.RecordTypeId == gDealDeskOperationalRecTypeId && iNewCase.Status == 'New' && 
               	String.isNotBlank( iNewCase.OpportunityId__c ) ){
                lSetOppIdToResetFlag.add( iNewCase.OpportunityId__c );
            }
        }
        
        resetOppFlag( lSetOppIdToResetFlag );
    }
    
    private void resetOppFlag( Set< Id > aSetOppIdToResetFlag ){
    	if( aSetOppIdToResetFlag.isEmpty() ){ return; }
        
        List< Opportunity > lLstOppToReset = new List< Opportunity >();
        
        for( Id iOppId : aSetOppIdToResetFlag ){
            lLstOppToReset.add(
                new Opportunity( Id = iOppId , Specialist_Approved__c = false )
            );
        }
        
        try{
            Database.update( lLstOppToReset );
        } catch( Exception aException ) {
            System.debug('Error while resetting Specialist approved');
			System.debug( aException.getMessage() );
        }
    }
}