public with sharing class ResellerEJFlowContactLookupController {
    
    @AuraEnabled(cacheable=true)
    public static List <sObject> fetchPreviouslySelectedValues(String objectName, String conditions, String orderBy) {
        System.debug('### INTO THE APEX fetchPreviouslySelectedValues ###');
        System.debug('### objectName ### ' + objectName);
        System.debug('### conditions ### ' + conditions);
        System.debug('### orderBy ### ' + orderBy);

        List < sObject > returnList = new List < sObject > ();
          
        String sQuery =  'SELECT Id, Name';
        sQuery += ' FROM ' + objectName;
        sQuery += (String.isBlank(conditions) ? '' : ' WHERE (' + conditions + ') ');
        sQuery += (String.isBlank(orderBy) ? '' : ' ORDER BY ' + orderBy);
        System.debug('### PreviouslySelectedValues ### sQuery --> ' + sQuery);
        
        try {
            returnList = Database.query(sQuery);
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
        return returnList;
   }  
}