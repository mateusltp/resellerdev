@isTest
public class SmbSkuUpdateProductsTest {
    
    @TestSetup
    static void makeData(){

        Account gympassEntity = AccountMock.getGympassEntity();
        gympassEntity.CurrencyIsoCode = 'BRL';
        insert gympassEntity;
    
        Account account = AccountMock.getStandard('Empresas');
        account.CurrencyIsoCode = 'BRL';
        account.UUID__c = SelfCheckoutRequestMock.ACCOUNT_UUID;
    
        insert account;
    
        Contact contact = ContactMock.getStandard(account);
    
        insert contact;
    
        account.Attention__c = contact.Id;
    
        update account;
    
        Pricebook2 pricebook = new Pricebook2(
          CurrencyIsoCode = 'BRL',
          Id = Test.getStandardPricebookId(),
          IsActive = true
        );
    
        update pricebook;
    
        List<Product2> products = new List<Product2>();
    
        Product2 setupFee = ProductMock.getSetupFee();
        setupFee.Minimum_Number_of_Employees__c = 0;
        setupFee.Maximum_Number_of_Employees__c = 10000;
        setupFee.IsActive = true;
        setupFee.Family = 'Enterprise Subscription';
        setupFee.Copay2__c = false;
        setupFee.Family_Member_Included__c = false;
        products.add(setupFee);
    
        Product2 accessFee = ProductMock.getAccessFee();
        accessFee.Minimum_Number_of_Employees__c = 0;
        accessFee.Maximum_Number_of_Employees__c = 10000;
        accessFee.IsActive = true;
        accessFee.Family = 'Enterprise Subscription';
        accessFee.Copay2__c = false;
        accessFee.Family_Member_Included__c = false;
        products.add(accessFee);
    
        insert products;
    
        List<PricebookEntry> pricebookEntries = new List<PricebookEntry>();
    
        PricebookEntry setupFeePricebookEntry = PricebookMock.getPricebookEntry(
          pricebook,
          setupFee
        );
        setupFeePricebookEntry.UnitPrice = 10;
        pricebookEntries.add(setupFeePricebookEntry);
    
        PricebookEntry accessFeePricebookEntry = PricebookMock.getPricebookEntry(
          pricebook,
          accessFee
        );
        accessFeePricebookEntry.UnitPrice = 10;
        pricebookEntries.add(accessFeePricebookEntry);
    
        insert pricebookEntries;
    
        Opportunity opp = OpportunityMock.getNewBusiness(account, pricebook);
        opp.CurrencyIsoCode = 'BRL';
        opp.StageName = 'Lançado/Ganho';
        opp.Current_Billing_Day__c = 15;
        opp.Current_Order_End_Date__c = Date.today().addMonths(3);
        opp.Quantity_Offer_Number_of_Employees__c = 10;
    
        insert opp;
    
        Quote qt = QuoteMock.getStandard(opp);
        RecordType qtRt = [SELECT Id, Name FROM RecordType WHERE SobjectType='Quote' AND Name = 'SMB Success - SKU Renegotiation' LIMIT 1];
        qt.recordTypeId = qtRt.Id;
        insert qt;
        
        opp.SyncedQuoteId = qt.Id;
		update opp;
    
        QuoteLineItem qtItem1 = QuoteLineItemMock.getSetupFee(qt,setupFeePricebookEntry);
        QuoteLineItem qtItem2 = QuoteLineItemMock.getSetupFee(qt,setupFeePricebookEntry);
        
        insert new List<QuoteLineItem> {qtItem1, qtItem2};
        
    }
    
    @isTest
    static void executeTest() {
        Test.startTest();
        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
        opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName()
            .get('SMB_Success_SKU_Renegotiation')
            .getRecordTypeId();
        opp.Quantity_Offer_Number_of_Employees__c = 20;
        update opp;
        Test.stopTest();
        
        List<QuoteLineItem> products = [SELECT Id, Quantity FROM QuoteLineItem];
        
        for(QuoteLineItem product: products) {
            System.assertEquals(20, product.Quantity, 'The quantity of each quote line item was not updated correctly.');
        }
        
    }

}