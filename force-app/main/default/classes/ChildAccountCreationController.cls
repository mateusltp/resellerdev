/**
 * @File Name          : ChildAccountCreationController.cls
 * @Description        : 
 * @Author             : Samuel Silva - GFT (slml@gft.com)
 * @Group              : 
 * @Last Modified By   : Samuel Silva - GFT (slml@gft.com)
 * @Last Modified On   : 13/05/2020 15:26:10
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    23/03/2020   Samuel Silva - GFT (slml@gft.com)     Initial Version
**/
public without sharing class ChildAccountCreationController {
    
@AuraEnabled
    public static Map<String,String> cloneAccount(Object formData){        
       return ChildAccountCreationModel.cloneParentAccount(formData);
    }
    
}