/**
 * @description       : 
 * @author            : tiago.ribeiro@gympass.com
 * @group             : 
 * @last modified on  : 06-08-2022
 * @last modified by  : tiago.ribeiro@gympass.com
**/
public with sharing class SKUQuoteLineItemHandler {
    public void preventDefaultQuoteItemRemoval(List<QuoteLineItem> listQuoteLineItems) {
        for(QuoteLineItem quoteItem : listQuoteLineItems){
            Set<String> quoteRecordTypes = new Set<String>{
                'Client_Sales_SKU_New_Business',
                'Client_Success_SKU_Renegotiation'
            };

            if(quoteRecordTypes.contains(quoteItem.Quote_RecordType__c) && quoteItem.is_Default__c){
                quoteItem.addError(Label.errorDeleteSKUMessage);
            }
        }
    }
}