/**
 * @author ercaval
 */
public class ScheduleHelper {
  public static Integer jobCountForTest = 0;

  public ScheduleHelper() {
  }

  public void scheduleIntoMinutesInterval(
    Schedulable service,
    Integer minuteInterval
  ) {
    String cronExpression = ' * * * ?';
    Integer i = 0;
    String executionMinute = '';
    while (i < 60) {
      if (i < 10)
        executionMinute = '0';

      executionMinute += i;

      System.schedule(
        getServiceName(service) +
        '  ' +
        jobCountForTest +
        executionMinute,
        '0 ' +
        executionMinute +
        cronExpression,
        service
      );

      i += minuteInterval;

      executionMinute = '';
    }
  }

  public void scheduleIntoNextMinutes(Schedulable service, Integer minutes) {
    String baseExpression = ' ?';

    Datetime nextScheduleTime = System.now().addMinutes(minutes);

    String cron =
      '0 ' +
      nextScheduleTime.minute() +
      ' ' +
      nextScheduleTime.hour() +
      ' ' +
      nextScheduleTime.day() +
      ' ' +
      nextScheduleTime.month() +
      baseExpression;

    scheduleForExpression(service, cron);
  }

  public void scheduleIntoNextSeconds(Schedulable service, Integer seconds) {
    Datetime nextScheduleTime = System.now().addSeconds(seconds);

    String cron =
      nextScheduleTime.second() +
      ' ' +
      nextScheduleTime.minute() +
      ' ' +
      nextScheduleTime.hour() +
      ' ' +
      nextScheduleTime.day() +
      ' ' +
      nextScheduleTime.month() +
      ' ? ' +
      nextScheduleTime.year();

    scheduleForExpression(service, cron);
  }

  public void scheduleIntoEveryHours(Schedulable service, Integer hours) {
    Datetime nextScheduleTime = System.now().addHours(hours);

    String cron =
      nextScheduleTime.second() +
      ' ' +
      nextScheduleTime.minute() +
      ' ' +
      nextScheduleTime.hour() +
      ' * ' +
      ' * ' +
      ' ? ' +
      '*';

    scheduleForExpression(service, cron);
  }

  public void scheduleAtHour(Schedulable service, Integer hour) {
    String cronExpression = ' * * * ?';
    Integer i = 0;
    String executionMinute = '00';

    System.schedule(
      getServiceName(service) +
      '  ' +
      jobCountForTest +
      executionMinute,
      '0 ' +
      executionMinute +
      cronExpression,
      service
    );
  }

  public void scheduleForExpression(Schedulable service, String expression) {
    System.schedule(
      new Uuid().getValue() +
      '  ' +
      getServiceName(service) +
      '  ' +
      jobCountForTest +
      expression,
      expression,
      service
    );
  }

  public void abort(String jobName) {
    for (CronTrigger cron : findJobsByName(jobName)) {
      System.abortJob(cron.id);
    }
  }

  public List<CronTrigger> findJobsByName(String jobName) {
    return [
      SELECT Id, CronJobDetail.Name
      FROM CronTrigger
      WHERE CronJobDetail.Name LIKE :(jobName + '%')
    ];
  }

  @TestVisible
  private String getServiceName(Schedulable service) {
    return String.valueOf(service)
      .substring(0, String.valueOf(service).indexOf(':'));
  }
}