@RestResource(urlMapping='/self-termination/*')
global with sharing class SelfTerminationRoute {
    @HttpPost
    global static SelfTerminationResponse post() {
      RestResponse restResponse = new SelfTerminationController().post();
  
      return new SelfTerminationResponse(restResponse);
    }

    global class SelfTerminationResponse {
        private Integer statusCode;
        private String errorMessage;
    
        private SelfTerminationResponse(RestResponse restResponse) {
          this.statusCode = restResponse.statusCode;
          this.errorMessage = restResponse?.responseBody?.toString();
        }
    
        public Integer getStatusCode() {
          return this.statusCode;
        }
    
        public String getErrorMessage() {
          return this.errorMessage;
        }
      }
}