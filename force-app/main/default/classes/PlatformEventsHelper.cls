/**
 * @description       : 
 * @author            : AlyssonMota
 * @group             : 
 * @last modified on  : 11-23-2020
 * @last modified by  : GEPI@GFT.com
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   10-09-2020   Alysson Mota   Initial Version
**/
public with sharing class PlatformEventsHelper {
    public PlatformEventsHelper() {

    }

    public static void publishStandardOfferCreationEvent(List<Opportunity> opps) {
        String oppId=null;
        String oppAccId=null;
        for (Opportunity opp : opps){
            oppId = opp.Id;
            oppAccId = opp.AccountId;
        }
        if ( oppId==null )
            return;
       
        List<Clients_Standard_Offer_Creation__e> events = new List<Clients_Standard_Offer_Creation__e>();
        events.add(new Clients_Standard_Offer_Creation__e(OpportunityId__c=oppId));
 
        List<Database.SaveResult> results = EventBus.publish(events);                   
    }

    public static void publishGenerateSalesOrderEvent(Map<Id, Opportunity> oldMap, Map<Id, Opportunity> newMap) {
        Id clientSalesNewBusinessRecTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        Id clientSuccessRenegotiationRecTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Success_Renegotiation').getRecordTypeId();
        Map<Id,  Opportunity> oldIdToOpportunityMap = new Map<Id, Opportunity>();
        List<Generate_Sales_Order__e> events = new List<Generate_Sales_Order__e>();

        for (Opportunity newOpp : newMap.values()) {
            Opportunity oldOpp = oldMap.get(newOpp.Id);                                            

            if (newOpp.RecordTypeId == clientSalesNewBusinessRecTypeId ||  (newOpp.RecordTypeId == clientSuccessRenegotiationRecTypeId && newOpp.No_Won_Opportunity_in_SF__c == true)) {
                system.debug('clientSuccessRenegotiationRecTypeId: ' + clientSuccessRenegotiationRecTypeId);
                system.debug('clientSuccessRenegotiaNo_Won_Opportunity_in_SF__ctionRecTypeId: ' + newOpp.No_Won_Opportunity_in_SF__c);

                if (oldOpp.StageName == 'Signed Contract' && newOpp.StageName == 'Launched/Won') {
                    events.add(new Generate_Sales_Order__e(Opportunity_Id__c=newOpp.Id));     
                }
            }
        }

        List<Database.SaveResult> results = EventBus.publish(events);
    }
}