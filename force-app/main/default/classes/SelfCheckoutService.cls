public without sharing class SelfCheckoutService {
  private AccountDTO accountDTO;
  private OpportunityDTO opportunityDTO;

  private Account account;
  private Opportunity opportunity;
  private Quote quote;
  private PricebookEntry pricebookEntry;

  public SelfCheckoutService(SelfCheckoutRequest selfCheckoutRequest) {
    this.accountDTO = selfCheckoutRequest.getAccountDTO();
    this.opportunityDTO = selfCheckoutRequest.getOpportunityDTO();

    this.account = accountDTO.parseToSAccount();
    this.opportunity = opportunityDTO.parseToSOpportunity();
    this.pricebookEntry = getPricebookEntry();
  }

  public void execute() {
    setSObjectsId();

    deleteChilds();

    upsertAccount();

    upsertOpportunity();
	
    if ( ! Test.isRunningTest() )
     calculateEnterpriseMetrics();

    handleSMBHistory();
  }

  private void setSObjectsId() {
    Set<String> contactEmails = new Set<String>();

    for (AccountDTO.ContactDTO contactDTO : accountDTO.getContacts()) {
      Contact contact = contactDTO.parseToSContact();
      contactEmails.add(contact.Email);
    }

    Set<Id> accountIds = new Set<Id>();

    for (Contact contact : [
      SELECT Id, AccountId
      FROM Contact
      WHERE Email IN :contactEmails
    ]) {
      accountIds.add(contact.AccountId);
    }

    List<Account> accounts = [
      SELECT Id
      FROM Account
      WHERE UUID__c = :this.account.UUID__c OR Id IN :accountIds
      WITH SECURITY_ENFORCED
    ];

    if (accounts.isEmpty()) {
      this.account.Id = null;
    } else {
      this.account.Id = accounts[0].Id;
    }

    List<Opportunity> opportunities = [
      SELECT Id, Name
      FROM Opportunity
      WHERE UUID__c = :this.opportunity.UUID__c
      WITH SECURITY_ENFORCED
    ];

    if (opportunities.isEmpty()) {
      this.opportunity.Id = null;
    } else {
      this.opportunity.Id = opportunities[0].Id;
      this.opportunity.Name = opportunities[0].Name;
    }
  }

  private void deleteChilds() {
    //deleteContacts();

    deleteSplitBilling();

    deleteFees();
  }

  private void deleteContacts() {
    List<Contact> contacts = [
      SELECT Id
      FROM Contact
      WHERE AccountId = :this.account.Id AND AccountId != NULL
      WITH SECURITY_ENFORCED
    ];

    if (!contacts.isEmpty()) {
      Database.delete(contacts);
    }
  }

  private void deleteSplitBilling() {
    List<Account_Opportunity_Relationship__c> spliBillings = [
      SELECT Id
      FROM Account_Opportunity_Relationship__c
      WHERE
        Account__c = :account.Id
        AND Account__c != NULL
        AND Opportunity__c = :opportunity.Id
        AND Opportunity__c != NULL
      WITH SECURITY_ENFORCED
    ];

    if (!spliBillings.isEmpty()) {
      Database.delete(spliBillings);
    }
  }

  private void deleteFees() {
    List<SObject> sObjects = new List<SObject>();

    List<Quote> quotes = [
      SELECT Id
      FROM Quote
      WHERE OpportunityId = :this.opportunity.Id AND OpportunityId != NULL
      WITH SECURITY_ENFORCED
    ];

    if (!quotes.isEmpty()) {
      List<QuoteLineItem> quoteLineItems = [
        SELECT Id
        FROM QuoteLineItem
        WHERE QuoteId IN :quotes
        WITH SECURITY_ENFORCED
      ];

      if (!quoteLineItems.isEmpty()) {
        List<Payment__c> payments = [
          SELECT Id
          FROM Payment__c
          WHERE Quote_Line_Item__c IN :quoteLineItems
          WITH SECURITY_ENFORCED
        ];

        if (!payments.isEmpty()) {
          List<Waiver__c> waivers = [
            SELECT Id
            FROM Waiver__c
            WHERE Payment__c IN :payments
            WITH SECURITY_ENFORCED
          ];

          List<Eligibility__c> eligibles = [
            SELECT Id
            FROM Eligibility__c
            WHERE Payment__c IN :payments
            WITH SECURITY_ENFORCED
          ];

          if (!waivers.isEmpty()) {
            sObjects.addAll(waivers);
          }

          sObjects.addAll(payments);

          if (!eligibles.isEmpty()) {
            sObjects.addAll(eligibles);
          }
        }

        sObjects.addAll(quoteLineItems);
      }

      sObjects.addAll(quotes);

      Database.delete(sObjects);
    }
  }

  private void upsertAccount() {
    Contact attention = this.account.Attention__r;

    this.account.Attention__r = null;
    this.account.NumberOfEmployees = this.opportunityDTO.number_of_employees;
    this.account.Potential__c =
      this.pricebookEntry.UnitPrice * this.opportunityDTO.number_of_employees;

    this.account.Is_Account_On_Tagus__c = this.opportunity.StageName.equals(
      'Lançado/Ganho'
    );

    Database.upsert(this.account);

    List<Contact> contacts = new List<Contact>();

    for (AccountDTO.ContactDTO contactDTO : this.accountDTO.getContacts()) {
      Contact contact = contactDTO.parseToSContact();
      contact.AccountId = this.account.Id;

      contacts.add(contact);
    }

    Database.upsert(contacts, Contact.Email);

    this.account.Attention__r = attention;

    Database.update(this.account);
  }

  private void upsertOpportunity() {
    this.opportunity.Name =
      this.account.Name +
      ' From SMB Portal In ' +
      Date.today().format();
    this.opportunity.CurrencyIsoCode = this.pricebookEntry.CurrencyIsoCode;
    this.opportunity.AccountId = this.account.Id;
    this.opportunity.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName()
      .get('SMB_New_Business')
      .getRecordTypeId();

    if (!this.opportunity.StageName.equals('Lançado/Ganho')) {
      this.opportunity.Quantity_Offer_Number_of_Employees__c = this.opportunityDTO.number_of_employees;
    }

    Database.upsert(this.opportunity);

    this.quote = new Quote(
      Name = 'Proposal - ' + this.opportunity.Name,
      CurrencyIsoCode = this.pricebookEntry.CurrencyIsoCode,
      OpportunityId = this.opportunity.Id,
      Start_Date__c = this.opportunity.Data_do_Lancamento__c,
      End_Date__c = this.opportunity.CloseDate,
      Pricebook2Id = this.pricebookEntry.Pricebook2Id,
      License_Fee_Waiver__c = 'No',
      Employee_Registration_Method__c = 'Eligible file',
      Contact_Permission__c = 'Allowlist',
      Unique_Identifier__c = 'Corporate E-mail'
    );

    Database.upsert(quote);

    OpportunityDTO.ItemsDTO itemsDTO = this.opportunityDTO.getItems();
    List<FeeItemDTO> feesDTO = new List<FeeItemDTO>();

    if (itemsDTO != null && !itemsDTO.getOneTimeFees().isEmpty()) {
      feesDTO.addAll(itemsDTO.getOneTimeFees());
    }

    if (itemsDTO != null && !itemsDTO.getRecurringFeeDTOs().isEmpty()) {
      feesDTO.addAll(itemsDTO.getRecurringFeeDTOs());
    }

    if (feesDTO.isEmpty()) {
      return;
    }

    insertFees(quote, feesDTO);

    Account_Opportunity_Relationship__c splitBilling = new Account_Opportunity_Relationship__c(
      Account__c = account.Id,
      Opportunity__c = opportunity.Id
    );

    for (OneTimeFeeDTO oneTimeFee : itemsDTO.getOneTimeFees()) {
      if (oneTimeFee.getFeeType() == OneTimeFeeDTO.FeeType.SETUP_FEE) {
        if (
          oneTimeFee.getFeeType() ==
          OneTimeFeeDTO.FeeType.PROFESSIONAL_SETUP_FEE
        ) {
          splitBilling.Prof_Serv_Setup_Fee_Billing_Percentage__c = 100;
        } else {
          splitBilling.Setup_Fee_Billing_Percentage__c = 100;
        }
      }
    }

    for (RecurringFeeDTO recurringFee : itemsDTO.getRecurringFeeDTOs()) {
      if (recurringFee.getFeeType() == RecurringFeeDTO.FeeType.ACCESS_FEE) {
        if (
          recurringFee.getFeeType() ==
          RecurringFeeDTO.FeeType.PROFESSIONAL_MAINTENANCE_FEE
        ) {
          splitBilling.Maintenance_Fee_Billing_Percentage__c = 100;
        } else {
          splitBilling.Billing_Percentage__c = 100;
        }
      }
    }

    Database.insert(splitBilling);
  }

  private PricebookEntry getPricebookEntry() {
    List<PricebookEntry> pricebookEntries = [
      SELECT Id, CurrencyIsoCode, Pricebook2Id, Product2Id, UnitPrice
      FROM PricebookEntry
      WHERE
        IsActive = TRUE
        AND Product2.IsActive = TRUE
        AND Product2.Family = 'Enterprise Subscription'
        AND Pricebook2.IsStandard = TRUE
        AND Product2.Copay2__c = FALSE
        AND Product2.Family_Member_Included__c = FALSE
        AND Product2.Minimum_Number_of_Employees__c <= :this.opportunity.Quantity_Offer_Number_of_Employees__c
        AND Product2.Maximum_Number_of_Employees__c >= :this.opportunity.Quantity_Offer_Number_of_Employees__c
        AND CurrencyIsoCode = :this.account.CurrencyIsoCode
      WITH SECURITY_ENFORCED
      LIMIT 1
    ];

    if (pricebookEntries.isEmpty()) {
      throw new IntegrationException(
        'No pricebook found based in the number of employees and the currency id'
      );
    }

    return pricebookEntries[0];
  }

  private void insertFees(Quote quote, List<FeeItemDTO> feesDTO) {
    List<SObject> sObjects = new List<SObject>();

    for (FeeItemDTO feeDTO : feesDTO) {
      QuoteLineItem quoteLineItem = feeDTO.parseToQuoteLineItem();
      quoteLineItem.PricebookEntryId = this.pricebookEntry.Id;
      quoteLineItem.Product2Id = this.pricebookEntry.Product2Id;

      if (String.isBlank(quoteLineItem.Fee_Contract_Type__c)) {
        quoteLineItem.Fee_Contract_Type__c = 'Flat Fee';
      }

      quoteLineItem.QuoteId = quote.Id;

      sObjects.add(quoteLineItem);

      for (PaymentDTO paymentDTO : feeDTO.getPayments()) {
        Payment__c payment = paymentDTO.parseToSPayment();
        payment.Quote_Line_Item__r = new QuoteLineItem(
          UUID__c = quoteLineItem.UUID__c
        );
        payment.Recurring_Billing_Period__c = 'Monthly';
        setPaymentDueDay(payment, quoteLineItem);
        setBillingDay(payment);

        sObjects.add(payment);

        for (EligibilityDTO eligibilityDTO : paymentDTO.getEligibilities()) {
          Eligibility__c eligibility = eligibilityDTO.parseToSEligibility();
          eligibility.Payment__r = new Payment__c(UUID__c = payment.UUID__c);

          sObjects.add(eligibility);
        }

        for (WaiverDTO waiverDTO : paymentDTO.getWaivers()) {
          Waiver__c waiver = waiverDTO.parseToSWaiver();
          waiver.Payment__r = new Payment__c(UUID__c = payment.UUID__c);

          sObjects.add(waiver);
        }
      }
    }

    if (!sObjects.isEmpty()) {
      Database.upsert(sObjects);
    }
  }

  private void setPaymentDueDay(
    Payment__c payment,
    QuoteLineItem quoteLinetItem
  ) {
    Integer paymentDueDays = (Integer) quoteLinetItem.Payment_Due_Days__c;

    switch on paymentDueDays {
      when 10 {
        payment.Payment_Due_Days__c = '10 days';
      }
      when 15 {
        payment.Payment_Due_Days__c = '15 days';
      }
      when 30 {
        payment.Payment_Due_Days__c = '30 days';
      }
      when 60 {
        payment.Payment_Due_Days__c = '30 days';
      }
      when else {
        payment.Payment_Due_Days__c = 'Custom';
        payment.Custom_Payment_Due_Days__c = quoteLinetItem.Payment_Due_Days__c;
      }
    }
  }

  private void handleSMBHistory() {
    List<Opportunity> opps = [
      SELECT Id
      FROM Opportunity
      WHERE
        RecordType.Name = 'SMB'
        AND IsClosed = FALSE
        AND AccountId = :this.account.Id
    ];
    for (Opportunity opp : opps) {
      opp.StageName = 'Perdido';
      opp.Loss_Reason__c = 'Other';
    }
    Database.update(opps, false);
  }

  private void setBillingDay(Payment__c payment) {
    Integer billingDay = Date.today().day();

    switch on billingDay {
      when 1 {
        payment.Billing_Day__c = '01';
      }
      when 5 {
        payment.Billing_Day__c = '05';
      }
      when 15 {
        payment.Billing_Day__c = '15';
      }
      when else {
        payment.Billing_Day__c = 'Custom';
        payment.Custom_Billing_Day__c = billingDay;
      }
    }
  }

  private void calculateEnterpriseMetrics() {
    List<Sobject> sObjects = new List<Sobject>();

    EnterpriseRevenueMetrics enterpriseRevenueMetrics = new EnterpriseRevenueMetrics();
    enterpriseRevenueMetrics.refreshQuoteRevenues(this.quote.Id);

    this.quote.AER_Revenue__c = enterpriseRevenueMetrics.getAerRevenue();
    this.quote.AES_Revenue__c = enterpriseRevenueMetrics.getAesRevenue();
    this.quote.ER_Revenue__c = enterpriseRevenueMetrics.getErRevenue();
    this.quote.ES_Revenue__c = enterpriseRevenueMetrics.getEsRevenue();

    sObjects.add(this.quote);

    this.opportunity.SyncedQuoteId = quote.Id;

    sObjects.add(opportunity);

    update sObjects;

    if (this.opportunity.StageName.equals('Lançado/Ganho')) {
      List<EnterpriseDealBO> enterpriseDeals = EnterpriseDealDAO.getEnterpriseDealsByOpportunities(
        new List<Opportunity>{ this.opportunity }
      );

      List<ClientDealBO> clientDeals = new List<ClientDealBO>();

      for (EnterpriseDealBO enterpriseDeal : enterpriseDeals) {
        ClientDealBO clientDeal = new ParseEnterpriseDealToClientDealService(
            enterpriseDeal
          )
          .execute();

        clientDeals.add(clientDeal);

        this.opportunity = enterpriseDeal.getOpportunity();
      }

      ClientDealDAO.upsertClientDeals(clientDeals);

      new UpsertClientOrderService(new List<Opportunity>{ this.opportunity })
        .execute();
    }
  }
}