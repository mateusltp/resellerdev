@IsTest
public class AccountMock {
  public static Account getStandard(String recordTypeName) {
    Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName()
      .get(recordTypeName)
      .getRecordTypeId();

    return new Account(
      Name = 'Kauê e Natália Casa Noturna Ltda',
      BillingCity = 'Itu',
      BillingCountry = 'Brazil',
      BillingCountryCode = 'BR',
      BillingState = 'São Paulo',
      BillingStreet = 'Praça Conselho do Povo',
      Email__c = 'knltda@apex.test',
      Id_Company__c = '27.812.981/0001-28',
      Legal_Document_Type__c = 'CNPJ',
      Razao_Social__c = 'Kauê e Natália Casa Noturna Ltda'
    );
  }

  public static Account getGympassEntity() {
    Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName()
      .get('Gympass_Entity')
      .getRecordTypeId();

    return new Account(
      BillingCity = 'Itu',
      BillingCountry = 'Brazil',
      BillingState = 'São Paulo',
      BillingStreet = 'Praça Conselho do Povo',
      Name = 'Theo e Eliane Alimentos Ltda',
      Phone = '3222123123',
      RecordTypeId = recordTypeId,
      Website = 'testing@tesapex.com',
      Can_use_logo__c = 'Yes',
      CAP_Value__c = 120,
      Email__c = 'gymemail@apex.com',
      Exclusivity__c = 'Yes',
      Exclusivity_End_Date__c = Date.today().addYears(1),
      Exclusivity_Partnership__c = 'Full Exclusivity',
      Exclusivity_Restrictions__c = 'No',
      GP_Status__c = 'Active',
      Gym_Email__c = 'gymemail@apex.com',
      Gyms_Identification_Document__c = 'CNPJ',
      Gym_Type__c = 'Studios',
      Gym_Classes__c = 'Cardio',
      Has_market_cannibalization__c = 'No',
      Id_Company__c = '39.450.055/0001-39',
      Legal_Registration__c = 12123,
      Legal_Document_Type__c = 'CNPJ',
      Legal_Title__c = 'Title LTDA',
      Razao_Social__c = 'Theo e Eliane Alimentos Ltda',
      Subscription_Period__c = 'Monthy value',
      Subscription_Type__c = 'Value per class',
      Subscription_Type_Estimated_Price__c = 100,
      Types_of_ownership__c = Label.franchisePicklist
    );
  }
}