/**
 * @description       : 
 * @author            : roei@gft.com
 * @group             : 
 * @last modified on  : 11-10-2021
 * @last modified by  : roei@gft.com
**/
@isTest(SeeAllData=false)
public with sharing class ApprovalRevampWaiverTest {
    
    @isTest 
    private static void shouldCreateAssertData(){
        Integer lWaiverMonths = 10;
        Payment__c lPayment = [ SELECT Id , Quote_Line_Item__r.Quote.OpportunityId FROM Payment__c LIMIT 1 ];
        String lOppId = lPayment.Quote_Line_Item__r.Quote.OpportunityId;

        Waiver__c iWaiver = DataFactory.newWaiver( lPayment );
        iWaiver.Number_of_Months_after_Launch__c = lWaiverMonths;
        Database.insert( iWaiver );

        Test.startTest();

        iWaiver.Approved_By_Approval_Process__c = true;
        Database.update( iWaiver );

        Test.stopTest();

        List< Assert_Data__c > lLstAssertData = [ SELECT Id, Old_Value__c FROM Assert_Data__c WHERE Opportunity__c =: lOppId AND 
          Fee_Type__c = 'Enterprise Subscription' AND Field_Name__c = 'number_of_months_after_launch__c' AND Object_Name__c = 'waiver__c' ];

        System.assertEquals( false , lLstAssertData.isEmpty() );
        System.assertEquals( 1 , lLstAssertData.size() );
        System.assertEquals( String.valueOf( lWaiverMonths ) , lLstAssertData[0].Old_Value__c );
    }

    @TestSetup
    private static void createData(){    
        Account lParentAcc = DataFactory.newAccount();
        lParentAcc.NumberOfEmployees = 1000;
        Database.insert( lParentAcc );

        Pricebook2 lStandardPb = DataFactory.newPricebook();

        Opportunity lOpp = DataFactory.newOpportunity( lParentAcc.Id , lStandardPb , 'Client_Sales_New_Business' );
        lOpp.Quantity_Offer_Number_of_Employees__c = 500;
        Database.insert( lOpp );

        Quote lProposal = DataFactory.newQuote( lOpp , 'Quote Test', 'Client_Sales_New_Business' );
        Database.insert( lProposal );

        Product2 lAcessFee = DataFactory.newProduct( 'Enterprise Subscription' , false , 'BRL' );
        Database.insert( lAcessFee );

        PricebookEntry lAccessFeeEntry = DataFactory.newPricebookEntry( lStandardPb , lAcessFee , lOpp );
        Database.insert( lAccessFeeEntry );

        QuoteLineItem lQuoteAccessFee = DataFactory.newQuoteLineItem( lProposal , lAccessFeeEntry );
        lQuoteAccessFee.Discount__c = 50;
        Database.insert( lQuoteAccessFee );

        Payment__c lAccessFeePayment = DataFactory.newPayment( lQuoteAccessFee );
        Database.insert( lAccessFeePayment );
    }
}