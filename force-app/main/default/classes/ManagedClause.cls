public class ManagedClause implements Comparable {
    
    private List<SubManagedClause> SubClauses;
    private ManagedClause SwapClause ;
    private string pClauseText;
    public Id ClauseId; 
    public string Name;
    public decimal Order;
    public string Category;

    
    public List<SubManagedClause> getSubClauseList(){
        return SubClauses;
    }
    
    public Integer compareTo(Object compareTo) {
        ManagedClause compareToManagedClause = (ManagedClause)compareTo;
        if (Order == compareToManagedClause.Order) return 0;
        if (Order > compareToManagedClause.Order) return 1;
        return -1;        
    }
    
    public string ClauseText {
        get { return this.SwapClause != null ? this.SwapClause.ClauseText() : this.pClauseText; }
        set { this.pClauseText = value ; }
    }
    
    public string ClauseText(){
        return this.SwapClause != null ? this.SwapClause.ClauseText() : this.pClauseText;
    }
    
    
    
    public void addSwapClause(ManagedClause swapClause){
        if (this.SwapClause == null){
            this.SwapClause = new ManagedClause();
            this.SwapClause = swapClause;
        }else
        {
            this.SwapClause = swapClause;
        }
    }
    
    public ManagedClause(){
        SubClauses = new List<SubManagedClause>();
    }
    
    public void cleanClauseSwap(){
        this.SwapClause = null;
    }  
    
    public void addSubClause(SubManagedClause subClause){
        this.SubClauses.add(subClause);
        this.SubClauses.sort();
    }
    
    public void addSubClause(List<SubManagedClause> subClauseList){
        for (SubManagedClause subclause : subClauseList){
            this.addSubClause(subclause);
        }
    }
    
    public List<SubManagedClause> getSubClauses(){
        return this.SubClauses;
    }
    
    public integer getSubClausesLength(){
        return this.SubClauses.size();
    }
    
}