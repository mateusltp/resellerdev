@isTest
public class FileUploaderTest
{
    @testSetup 
    static void createData()
    {
        Campaign campaign = new Campaign();
        campaign.Name = 'teste';
        insert campaign;
        
        Event evento = new Event();
        evento.DurationInMinutes = 10;
        evento.Type = 'Virtual meeting';
        evento.Subject = 'Teste';
        evento.ActivityDateTime = System.now();
        evento.Campaign__c = campaign.Id;
        insert evento;
        
        String csvFile = 'Id;Site_Feasible_For_Solar__c; class; run; name; age; gender; hair; legs \n';
        csvFile += '"fhdsuf4h98f";"Yes";"1";"1";"1";"1";"1";1;"33.545.613/0001-20";"Albania";"DBLQT " \n';
        
        ContentVersion contentVersionInsert = new ContentVersion(
            Title = 'Modelo',
            PathOnClient = 'Modelo.csv',
            VersionData = Blob.valueOf(csvFile)
        );
        
        insert contentVersionInsert;
    }
    
    @isTest
    public static void testUploadFile()
    {        
        Event evento = [SELECT ID FROM Event LIMIT 1];
        
        FileUploader  EWS = new FileUploader();
        EWS.nameFile = 'test.csv';
            
        String csvFile = 'Id;Site_Feasible_For_Solar__c; class; run; name; age; gender; hair; legs \n';
        csvFile += evento.Id + ';Yes;1;Analyst;Legal;teste@teste.com;1;1;33.545.613/0001-20;Brazil;São Paulo\n';
        
        Blob csvBlob = Blob.valueOf(csvFile );
        
        list<String> listCNPJ = new list<String>();
        list<String> listEmail = new list<String>();
        
        map<String,Account> mapAccount = new map<String,Account>();
        map<String,Contact> mapContact = new map<String,Contact>();
        list<Account> listNewAccount = new list<Account>();
        list<Contact> listNewContacts = new list<Contact>();
        list<EventRelation> listNewEventRelation = new list<EventRelation>();
        Account acc = new Account();
        Contact cont = new Contact();
        
        acc.Id_Company__c = '33.969.613/0001-99';
        acc.Industry = 'Education';
        acc.NumberOfEmployees = 50;
        acc.BillingCountry = 'Brazil';
        acc.BillingState = 'São Paulo';
        acc.Name = 'Padaria';
        mapAccount.put(acc.Id_Company__c, acc);
        
        insert acc;
        list<Account> accQuery = [SELECT Id,Id_Company__c FROM Account WHERE Id_Company__c IN :listCNPJ];
        
        cont.LastName = 'Carlos';
        cont.AccountId= acc.Id;
        cont.cargo__c = 'Analyst';
        cont.Area_Setor__c = 'Legal';
        cont.Email = 'carlos@teste.com';
        insert cont;
        list<Contact> contQuery = [SELECT Id, Email FROM Contact WHERE Email IN :listEmail];
        
        listCNPJ.add('33.969.613/0001-99 ;');
        listCNPJ.add('33.919.613/0001-79 ;');
        listCNPJ.add('33.929.613/0001-79 ;');
        listCNPJ.add('33.939.613/0001-79 ;');
        listCNPJ.add('33.959.613/0001-79 ;');
        listCNPJ.add('33.949.613/0001-79 ;');
        listCNPJ.add('33.979.613/0001-79 ;');
        listCNPJ.add('33.989.613/0001-79 ;');
        listCNPJ.add('33.700.613/0001-79 ;');

        
        PageReference pageRef = Page.FileUploader;
        Test.setCurrentPage(pageRef);
        
        
        Test.startTest();
        EWS.contentFile =csvBlob;
        system.debug('CSV'+ csvFile);
        //EWS.parseCSV(EWS.contentFile.toString(), true); 
        EWS.ReadFile();
        Test.stopTest();
    }
}