@isTest
public class AutoAssignAccountContactOwnerTest {
	
    @isTest
    public static void testAssignAccountContactOwner() {
        Id accountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Direct Channel').getRecordTypeId();

        Account a = new Account();
        a.RecordTypeId = accountRecordTypeId;
        a.Name = 'Test Account';
        a.BillingCountry = 'Brazil';
        a.BillingState = 'São Paulo';
        
        insert a;
        
        List<Contact> contactsToInsert = new List<Contact>();
        
        for (Integer i = 0; i < 3; i++) {
			Contact c = new Contact();
            c.FirstName = 'Contact';
            c.LastName = ' ' + i;
            c.Email = 'contact' + i + '@test.com';
            c.AccountId = a.Id;
            
            contactsToInsert.add(c);
        }
        
        insert contactsToInsert;
        
        List<Profile> profiles = [Select Id From Profile Where Name='BDR'];
        
        User bdrUser = new User();
        bdrUser.Username = 'bdrTestUser@test.com'; 
       	bdrUser.LastName = 'TestUser';
        bdrUser.Email = 'bdrTestUser@test.com'; 
        bdrUser.Alias = 'bdrTest'; 
        bdrUser.TimeZoneSidKey = 'America/Sao_Paulo';
        bdrUser.LocaleSidKey = 'en_GB';
        bdrUser.EmailEncodingKey =  'ISO-8859-1'; 
        bdrUser.ProfileId =  profiles.get(0).Id;
        bdrUser.LanguageLocaleKey = 'en_US';
        
        insert bdrUser;
        
        Test.startTest();
        
        a.BDR__c = bdrUser.Id;
        
        update a;
        
        a.BDR__c = null;
        
        update a;
        
        Test.stopTest();
    }
}