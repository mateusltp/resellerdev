/**
 * @description       : 
 * @author            : roei@gft.com
 * @group             : 
 * @last modified on  : 02-08-2020
 * @last modified by  : roei@gft.com
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   02-08-2020   roei@gft.com   Initial Version
**/
@isTest(seeAllData=false)
public class DealDeskOperationalManualShareTest {
    
	@TestSetup
    static void createData(){      
        Pricebook2 lStandardPricebook = new Pricebook2(
            Id = Test.getStandardPricebookId(),
            IsActive = true
        );
        Database.update( lStandardPricebook );
        
        Account lAcc = generateAccount();              
        Database.insert( lAcc );
                     
        Product2 lAccessFee = generateProduct('Enterprise Subscription');
        Database.insert( lAccessFee );
        Product2 lSetupFee = generateProduct('Setup Fee');
        Database.insert( lSetupFee );    
        
        Opportunity lNewBusinessOpp = generateNewBusinessOpp( lAcc.Id, lStandardPricebook );     
        Database.insert( lNewBusinessOpp );
        
        Quote lQuote = generateQuote( lNewBusinessOpp , ' 1' );
        Database.insert( lQuote );
        
        lNewBusinessOpp.SyncedQuoteId = lQuote.Id;
        lNewBusinessOpp.StageName = 'Proposta Aprovada';
        lNewBusinessOpp.FastTrackStage__c = 'Offer Approved';
        Database.update( lNewBusinessOpp );
        
        List< Profile > lLstRelationshipProfile = [
            SELECT Id
            FROM Profile
            WHERE Name = 'Standard User'
        ];
        
        User lUser1 = new User();
        lUser1.Email = 'user.test@gympass.com';
        lUser1.Username = 'user.test@gympass.com.test';
        lUser1.LastName = 'Test';
        lUser1.Alias = 'ustst';
        lUser1.TimeZoneSidKey = 'America/Sao_Paulo';
        lUser1.LocaleSidKey = 'pt_BR';
        lUser1.EmailEncodingKey = 'ISO-8859-1';
        lUser1.LanguageLocaleKey = 'pt_BR';
        lUser1.ProfileId = lLstRelationshipProfile[0].Id;
        
        Database.insert( lUser1 );
        
        PermissionSet lClientSuccessPermissionSet = [ SELECT Id FROM PermissionSet WHERE Name = 'Clients_Success_PermissionSet' ];
        PermissionSet lStandardAdminPermissionSet = [ SELECT Id FROM PermissionSet WHERE Name = 'Standard_Admin' ];
        
        List< PermissionSetAssignment > lLstPermissionSetAssign = new List< PermissionSetAssignment >();
        
        PermissionSetAssignment lPsAssignment1 = new PermissionSetAssignment();
        lPsAssignment1.AssigneeId = lUser1.Id;
        lPsAssignment1.PermissionSetId = lClientSuccessPermissionSet.Id;
        lLstPermissionSetAssign.add( lPsAssignment1 );
        
        PermissionSetAssignment lPsAssignment2 = new PermissionSetAssignment();
        lPsAssignment1.AssigneeId = lUser1.Id;
        lPsAssignment1.PermissionSetId = lStandardAdminPermissionSet.Id;
        lLstPermissionSetAssign.add( lPsAssignment2 );
        
        User lAdminUser = [ SELECT Id FROM User WHERE isActive = true AND Profile.Name = 'System Administrator' LIMIT 1 ];
        System.runAs( lAdminUser ){
            Database.SaveResult[] lLstSaveResult = Database.insert( lLstPermissionSetAssign , false );
        }
    }
    
    @isTest
    public static void shouldShareCaseAccessWithUser() {
    	Opportunity lOpp = [ SELECT Id FROM Opportunity WHERE Name = 'Renegotiation' ];
        List< User > lLstUsers = [ SELECT Id FROM User WHERE Email = 'user.test@gympass.com' ];
        Id lOperationalRtId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Deal_Desk_Operational').getRecordTypeId();
        
        Case lOperationalCase = new Case();
        lOperationalCase.OwnerId = [ SELECT Id FROM Group WHERE DeveloperName = 'Deal_Desk_Operational' ].Id;
		lOperationalCase.Description = 'isTest';
		lOperationalCase.OpportunityId__c = lOpp.Id;
        lOperationalCase.RecordTypeId = lOperationalRtId;
        lOperationalCase.Justificate_No_Compliant_Topics__c = 'Justification Test';
		lOperationalCase.Subject = 'Deal Desk Operational Approval Request';
        
        Test.startTest();
            System.runAs( lLstUsers[0] ){
                Database.insert( lOperationalCase );
            }
        Test.stopTest();
        
        UserRecordAccess lUserAccess = [ SELECT RecordId, HasReadAccess FROM UserRecordAccess WHERE UserId =: lLstUsers[0].Id AND RecordId =: lOperationalCase.Id ];
        
        System.assert( lUserAccess.HasReadAccess , 'User should have access to Case' );
    }
    
    private static Account generateAccount(){
        ID accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
        Account acc = new Account();
        acc.RecordTypeId = accRecordTypeId;
        acc.Name = 'Account 1';
        acc.BillingCountry = 'Brazil';
        acc.BillingState = 'São Paulo';
        acc.NumberOfEmployees = 2000;
        return acc;
    }
	
	private static Product2 generateProduct(String ProductName){
        Product2 product = new Product2();
        product.Name = ProductName;
        product.Minimum_Number_of_Employees__c = 0;
        product.Maximum_Number_of_Employees__c = 900;
        product.Family = ProductName;
        product.IsActive = true;
        return product;
    }    
    
    private static Opportunity generateNewBusinessOpp(Id aAccId, Pricebook2 aPriceBook){
        Id lNewBusinessRtId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        Opportunity lOpp = new Opportunity();
        lOpp.CurrencyIsoCode='BRL';
        lOpp.recordTypeId = lNewBusinessRtId;
        lOpp.StageName='Qualification';
        lOpp.AccountId = aAccId;
        lOpp.CloseDate = Date.today();
        lOpp.Engagement_Journey_Completed__c = true;
        lOpp.Name='Renegotiation'; 
        lOpp.Pricebook2Id = aPriceBook.Id;
        return lOpp;
    }
    
    private static Quote generateQuote(Opportunity opp, String name){
        Quote qt = new Quote();  
        qt.RecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Gympass_Plus').getRecordTypeId();
        qt.Name = opp.Name+name;
        qt.OpportunityId = opp.Id;   
        qt.License_Fee_Waiver__c = 'No';  
        qt.CurrencyIsoCode = opp.CurrencyIsoCode;
        return qt;
    }
}