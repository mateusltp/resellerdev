public with sharing class AccountRequestBO {

    public AccountRequestBO() {

    }

    public void runGoNoGoSetAccount(){

        ResellerEngineGoNoGO engineGoNoG = new ResellerEngineGoNoGO(Trigger.new);
        engineGoNoG.runSearchAccounts(false);
    }

}