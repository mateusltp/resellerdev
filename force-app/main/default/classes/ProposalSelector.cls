/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 09-03-2021
 * @last modified by  : gft.samuel.silva@ext.gympass.com
 * Modifications Log 
 * Ver   Date         Author                             Modification
 * 1.0   07-21-2021   gft.samuel.silva@ext.gympass.com   Initial Version
**/
public with sharing class ProposalSelector extends ApplicationSelector {
    
    public List<Schema.SObjectField> getSObjectFieldList() {
		return new List<Schema.SObjectField> {
            Quote.Id,
            Quote.Name
		};
	}

    public Schema.SObjectType getSObjectType() {
		return 	Quote.sObjectType;
	}

    public List<Quote> selectById(Set<Id> ids) {
		return (List<Quote>) super.selectSObjectsById(ids);
	}

    /** tags: clone, cloning, to clone, allFields
     * toClone means all fields are retrieving
    */
    public List<Quote> selectByOppIdToClone( Set<Id> aOppIdSet ) {
	
        return (List<Quote>) Database.query(
									newQueryFactory().
										selectFields(getAllFieldsFromProductItem()).
										setCondition('OpportunityId IN: aOppIdSet').
										toSOQL());
	}

    public Map<Id, Quote> selectQuoteByOppId ( List<Opportunity> aOppLst ) {
	
        Map<Id, Quote> mQuoteByOpp = new Map<Id, Quote>();

        for( Quote iProposal : Database.query( newQueryFactory().
										selectField(Quote.OpportunityId). 
                                        selectField('Opportunity.AccountId'). 
                                        selectField('Opportunity.CurrencyIsoCode').
                                        selectField(Quote.Start_Date__c).
                                        selectField(Quote.Signed__c).
                                        selectField(Quote.End_Date__c).                                    
										setCondition('OpportunityId IN: aOppLst').
										toSOQL()) ) {
            mQuoteByOpp.put(iProposal.OpportunityId, iProposal);
        }
        return mQuoteByOpp;
	}

    private Set<String> getAllFieldsFromProductItem(){
		Map<String, Schema.SObjectField> quoteFields = Schema.getGlobalDescribe().get('Quote').getDescribe().fields.getMap();
        return quoteFields.keySet();
    }
}