public class ApprovalRequestCommentsController {

    // ID of the record whose most recent approval process comments to retrieve
    public ID targetObjectId { get; set; }
    
    // The most recent approval process comments
    // Could show in visualforce email template, for example
    public String comments {
        get {
            if ( comments == null ) {
            	ProcessInstanceStep lastStep = getFirstApprovalStep();
            	comments = ( lastStep != null ) ? lastStep.comments : '';
            }
            return comments;
        }
        private set;
    }
    
    // The most recent approval process comments
    // Could show in visualforce email template, for example
    public String lastSubmitter {
        get {
            if ( lastSubmitter == null ) {
            	ProcessInstanceStep lastStep = getLastApprovalStep();
            	lastSubmitter = ( lastStep != null ) ? lastStep.Actor.Name : '';
            }
            return lastSubmitter;
        }
        private set;
    }
    
    // The approver name
    public String approverName {
        get {
            if ( approverName == null ) {
            	ProcessInstanceWorkItem approver = getApproverName();
            	approverName = ( approver != null ) ? approver.Actor.Name : '';
            }
            return approverName;
        }
        private set;
    }
    
    public ApprovalRequestCommentsController() {}
    
    // Queries the most recent approval process step for the target record
    private ProcessInstanceStep getLastApprovalStep() {
        List<ProcessInstanceStep> steps = new List<ProcessInstanceStep>([
            SELECT
            	Comments, Actor.Name
            FROM
            	ProcessInstanceStep
            WHERE
            	ProcessInstance.TargetObjectId = :targetObjectId
            ORDER BY
            	SystemModStamp DESC
            LIMIT
            	1
        ]);
        return ( steps.size() > 0 ) ? steps[0] : null;
    }
    
    // Queries the last recent approval process step for the target record
    private ProcessInstanceStep getFirstApprovalStep() {
        List<ProcessInstanceStep> steps = new List<ProcessInstanceStep>([
            SELECT
            	Comments, Actor.Name
            FROM
            	ProcessInstanceStep
            WHERE
            	ProcessInstance.TargetObjectId = :targetObjectId AND StepStatus = 'Started'
            ORDER BY
            	SystemModStamp DESC
            LIMIT
            	1
        ]);
        return ( steps.size() > 0 ) ? steps[0] : null;
    }
    
    // Queries the pending approval step for the target record
    private ProcessInstanceWorkItem getApproverName() {
        List<ProcessInstanceWorkItem> steps = new List<ProcessInstanceWorkItem>([
            SELECT
            	Actor.Name
            FROM
            	ProcessInstanceWorkItem
            WHERE
            	ProcessInstance.TargetObjectId = :targetObjectId
            ORDER BY
            	SystemModStamp DESC
            LIMIT
            	1
        ]);
        return ( steps.size() > 0 ) ? steps[0] : null;
    }
}