/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 10-13-2021
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/
@IsTest
public class PublishAccountPartnerOnTagusEventTest {
    
  @IsTest
  public static void execute() {
    Test.setMock(HttpCalloutMock.class, new HttpResponseMock());

    Account account = PartnerDataFactory.newAccount();
    account.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Account').getRecordTypeId();
    account.UUID__c = new Uuid().getValue();
    insert account;

    account.Send_To_Tagus__c = true;
    update account;
	
    /*
    System.assertEquals(
      1,
      [SELECT Id FROM Queue__c].size(),
      'Wrong number of events created'
    );
	*/

    account.Phone = '11 99999-9999';
    update account;
	
    /*
    System.assertEquals(
      2,
      [SELECT Id FROM Queue__c].size(),
      'Wrong number of events created'
    );
	*/
  }

  private class HttpResponseMock implements HttpCalloutMock {
    public HTTPResponse respond(HTTPRequest req) {
      HttpResponse respinse = new HttpResponse();
      respinse.setHeader('Content-Type', 'application/json');
      respinse.setStatusCode(200);
      return respinse;
    }
  }
}