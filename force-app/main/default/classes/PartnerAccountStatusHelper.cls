/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 05-31-2021
 * @last modified by  : gft.samuel.silva@ext.gympass.com
 * Modifications Log 
 * Ver   Date         Author                             Modification
 * 1.0   05-25-2021   gft.samuel.silva@ext.gympass.com   Initial Version
**/
public with sharing class PartnerAccountStatusHelper {
    private final Id gRtLongTail = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Partner_Small_and_Medium_Renegotiation').getRecordTypeId();
    private final Id gRtWishList = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Partner_Wishlist_Renegotiation').getRecordTypeId();
    private static Map<Id, Opportunity> gMapOppId = new Map<Id, Opportunity>();

    public PartnerAccountStatusHelper() {}

    public void addCancelationReasonAndBlockList(List<Opportunity> newOpportunities, Map<Id,Opportunity> oldOpportunities){
        for ( Opportunity opp : newOpportunities ){   
            
            if(Utils.isSemaphored('addCancelationReasonAndBlockList', opp.Id)){
                continue;
            } 

            if ( (  opp.RecordTypeId == gRtLongTail || opp.RecordTypeId == gRtWishList  ) && ( opp.AccountId != null ) &&
                opp.get('StageName')!=oldOpportunities.get(opp.Id).get('StageName') && opp.StageName == 'Perdido' && opp.Sub_Type__c == 'Retention')   {
                    gMapOppId.put(opp.Id, opp);
            }            
        }        
        if(!gMapOppId.isEmpty()){
            setCancelationReasonAndBlockList();
        }
    }

    private void setCancelationReasonAndBlockList(){       
        Map<Id, Set<Id>>  lRelatedAccounts = getRelatedAccounts();
        List<Account> lAccountLst = new List<Account>();
        if( !lRelatedAccounts.isEmpty() ){
            for( Id iOpp : lRelatedAccounts.keySet() ){
               for( Id iAcc : lRelatedAccounts.get(iOpp)){
                   Account aAcc = new Account();
                   aAcc.Id = iAcc;
                   aAcc.GP_Status__c = 'Cancelled';
                   aAcc.Cancellation_Reason__c = gMapOppId.get(iOpp).Cancellation_Reason__c; 
                   aAcc.Cancellation_Details__c = gMapOppId.get(iOpp).Cancellation_Reason_subcategory__c; 
                   aAcc.GP_Blacklist__c = (gMapOppId.get(iOpp).Sub_Type__c == 'Retention' && gMapOppId.get(iOpp).Cancellation_Reason__c == 'Fraud')? true : false;
                   lAccountLst.add(aAcc);                  
               }
            }
            try {
                Database.update(lAccountLst);
            }catch ( Exception e ){
                new IntegrityError().doAction( e.getMessage() );
            }
        }
        
    }

    private Map<Id, Set<Id>> getRelatedAccounts(){
        
        //Account Opportunity Relationship 
        Map<Id, Set<Id>> lRelatedAccounts = new Map<Id, Set<Id>>();
        for(Account_Opportunity_Relationship__c aor : [ SELECT Id,Account__c, Opportunity__c	
                                                        FROM Account_Opportunity_Relationship__c
                                                        WHERE Opportunity__c IN: gMapOppId.keySet()]){

            if(!lRelatedAccounts.containsKey(aor.Opportunity__c)){
                lRelatedAccounts.put(aor.Opportunity__c, new Set<Id>{aor.Account__c});
            } else {
                lRelatedAccounts.get(aor.Opportunity__c).add(aor.Account__c);
            }
        }

        //Opp AccountId 
        for(Opportunity iOpp : gMapOppId.values()){
            if(!lRelatedAccounts.containsKey(iOpp.Id)){
                lRelatedAccounts.put(iOpp.Id, new Set<Id>{iOpp.AccountId});
            } else {
                lRelatedAccounts.get(iOpp.Id).add(iOpp.AccountId);
            }
        }
        return lRelatedAccounts;
    }

    public class IntegrityError {
        public IntegrityError() {}

        public void doAction(String eMessage) {
            // Trigger.new is valid here
            SObject[] sobjects = Trigger.new;
            for (Sobject sobj : sobjects) {
                sobj.addError('Can\'t update account and related accounts. Please contact admin. Error: ' + eMessage);
            }
        }
    }

}