@IsTest
public class SelfCheckoutRequestMock {
  public static final String ACCOUNT_UUID = 'aa47c899-7e59-47ad-9ad7-7d0f6310f4ea';
  public static final String GYMPASS_ENTITY_UUID = 'cf37978d-a2ea-489c-903e-4f3eeacef5b3';
  public static final String OPPORTUNITY_UUID = 'a8556e98-a66e-41c7-8dbc-588479b03345';

  public static SelfCheckoutRequest getMock() {
    String requestJson =
      '{' +
      '"account": {' +
      '"id": "' +
      ACCOUNT_UUID +
      '",' +
      '"trade_name": "intuitive strategize bandwidth",' +
      '"phone": "+55 (44) 0324-2915",' +
      '"legal_name": "Nolan, Spinka and Stoltenberg",' +
      '"legal_document_type": "CNPJ",' +
      '"legal_document": "09.866.432/0001-07",' +
      '"currency_id": "BRL",' +
      '"contacts": [' +
      '{' +
      '"id": "c0a117bb-f1f7-4aff-829b-82c11c9270a3",' +
      '"portal_roles": [' +
      '"admin"' +
      '],' +
      '"phone": "+55 (06) 5198-1234",' +
      '"first_name": "Haylie",' +
      '"last_name": "Hoppe",' +
      '"email": "Marco21@example.com",' +
      '"address": {' +
      '"street": "Carvalho Viela",' +
      '"state": "Mato Grosso do Sul",' +
      '"postal_code": "27122",' +
      '"country_id": "BR",' +
      '"city": "Grande Isabela"' +
      '}' +
      '}' +
      '],' +
      '"business_unit": "SMB",' +
      '"billing_email": "Heaven48@example.net",' +
      '"addresses": {' +
      '"mailing": null,' +
      '"billing": {' +
      '"street": "Franco Marginal",' +
      '"state": "Maranhão",' +
      '"postal_code": "95991-516",' +
      '"country_id": "BR",' +
      '"city": "Bragade Nossa Senhora",' +
      '"attention": "c0a117bb-f1f7-4aff-829b-82c11c9270a3"' +
      '}' +
      '}' +
      '},' +
      '"opportunity": {' +
      '"id": "' +
      OPPORTUNITY_UUID +
      '",' +
      '"stage": "CLOSED_WON",' +
      '"number_of_employees": 500,' +
      '"items": {' +
      '"recurring_fees": [' +
      '{' +
      '"id": "9e258d9f-6330-480f-9c55-5131ead78f94",' +
      '"payments": [' +
      '{' +
      '"id": "77a66eb7-5766-4890-a6c1-87a3c763481b",' +
      '"waivers": [' +
      '{' +
      '"id":"8751e542-bd28-4b8b-a2e2-5320b5ffd394",' +
      '"begin_date":"2021-04-01T04:00:00.000Z",' +
      '"end_date":"2021-05-01T04:00:00.000Z",' +
      '"waived_percentage":100.0' +
      '}' +
      '],' +
      '"split_bill_percentage": 100,' +
      '"eligibles": [' +
      '{' +
      '"id": "d4b84207-743a-4894-804f-21a997e8176c",' +
      '"welcome_email_permitted": false,' +
      '"payroll_deduction_type": "AUTO_APPROVAL",' +
      '"payment_method_type": "STANDARD_METHODS",' +
      '"paid_by_client_id": "' +
      ACCOUNT_UUID +
      '",' +
      '"max_dependents": 1,' +
      '"managed_by_client_id": "' +
      ACCOUNT_UUID +
      '",' +
      '"name": "Headquarters",' +
      '"identifier_type": "EMAIL",' +
      '"exchange_type": "FILE_UPLOAD",' +
      '"is_default_item": true,' +
      '"communication_permitted_type": "ELIGIBLES"' +
      '}' +
      '],' +
      '"client_id": "aa47c899-7e59-47ad-9ad7-7d0f6310f4ea"' +
      '}' +
      '],' +
      '"payment_due_days": 30,' +
      '"currency_id": "BRL",' +
      '"variable_price_unit_increase": null,' +
      '"variable_price_floor": null,' +
      '"variable_price_ceiling": null,' +
      '"inflation_adjustment_type": null,' +
      '"inflation_adjustment_period": null,' +
      '"flat_unit_price": 19.04,' +
      '"flat_total_price": 190400,' +
      '"flat_discount_percentage": null,' +
      '"flat_baseline_quantity": 500,' +
      '"flat_baseline_adjustment": false,' +
      '"fee_type": "ACCESS_FEE",' +
      '"fee_price_type": "FLAT_PRICE_ELIGIBLE",' +
      '"discount_price_unit_decrease": null,' +
      '"discount_price_floor": null,' +
      '"discount_price_ceiling": null,' +
      '"cutoff_day": 1,' +
      '"billing_period": "YEAR",' +
      '"billing_day": 1' +
      '}' +
      '],' +
      '"one_time_fees": [' +
      '{' +
      '"id": "900606f8-9c86-4952-97b9-51559826a3fe",' +
      '"payments": [' +
      '{' +
      '"id": "f6c6be07-7bed-48d0-8f8c-eae29d2426b8",' +
      '"waivers": [],' +
      '"split_bill_percentage": 100,' +
      '"eligibles": [' +
      '{' +
      '"id": "199b9b0a-0e37-4df7-84ef-de1718ae48c7",' +
      '"welcome_email_permitted": false,' +
      '"payroll_deduction_type": "AUTO_APPROVAL",' +
      '"payment_method_type": "STANDARD_METHODS",' +
      '"paid_by_client_id": "' +
      ACCOUNT_UUID +
      '",' +
      '"max_dependents": 1,' +
      '"managed_by_client_id": "' +
      ACCOUNT_UUID +
      '",' +
      '"name": "Headquarters",' +
      '"identifier_type": "EMAIL",' +
      '"exchange_type": "FILE_UPLOAD",' +
      '"is_default_item": true,' +
      '"communication_permitted_type": "ELIGIBLES"' +
      '}' +
      '],' +
      '"client_id": "aa47c899-7e59-47ad-9ad7-7d0f6310f4ea"' +
      '}' +
      '],' +
      '"payment_due_days": 30,' +
      '"currency_id": "BRL",' +
      '"unit_price": 3.04,' +
      '"total_price": 30400,' +
      '"fee_type": "SETUP_FEE",' +
      '"discount_percentage": null,' +
      '"billing_date": "2021-04-01T04:00:00.000Z",' +
      '"baseline_quantity": 500' +
      '}' +
      ']' +
      '},' +
      '"gympass_entity_id": "' +
      GYMPASS_ENTITY_UUID +
      '",' +
      '"end_date": "2022-05-01T00:00:00.000Z",' +
      '"begin_date": "2021-04-30T00:00:00.000Z"' +
      '}' +
      '}';

    return (SelfCheckoutRequest) JSON.deserialize(
      requestJson,
      SelfCheckoutRequest.class
    );
  }

  public static SelfCheckoutRequest getMockWithIllegalDocumentType() {
    String requestJson =
      '{' +
      '"account": {' +
      '"id": "' +
      ACCOUNT_UUID +
      '",' +
      '"trade_name": "intuitive strategize bandwidth",' +
      '"phone": "+55 (44) 0324-2915",' +
      '"legal_name": "Nolan, Spinka and Stoltenberg",' +
      '"legal_document_type": "ERROR",' +
      '"legal_document": "09.866.432/0001-07",' +
      '"currency_id": "ERROR",' +
      '"contacts": [' +
      '{' +
      '"id": "c0a117bb-f1f7-4aff-829b-82c11c9270a3",' +
      '"portal_roles": [' +
      '"admin"' +
      '],' +
      '"phone": "+55 (06) 5198-1234",' +
      '"first_name": "Haylie",' +
      '"last_name": "Hoppe",' +
      '"email": "Marco21@example.com",' +
      '"address": {' +
      '"street": "Carvalho Viela",' +
      '"state": "Mato Grosso do Sul",' +
      '"postal_code": "27122",' +
      '"country_id": "BR",' +
      '"city": "Grande Isabela"' +
      '}' +
      '}' +
      '],' +
      '"business_unit": "SMB",' +
      '"billing_email": "Heaven48@example.net",' +
      '"addresses": {' +
      '"mailing": null,' +
      '"billing": {' +
      '"street": "Franco Marginal",' +
      '"state": "Maranhão",' +
      '"postal_code": "95991-516",' +
      '"country_id": "BR",' +
      '"city": "Bragade Nossa Senhora",' +
      '"attention": "c0a117bb-f1f7-4aff-829b-82c11c9270a3"' +
      '}' +
      '}' +
      '},' +
      '"opportunity": {' +
      '"id": "' +
      OPPORTUNITY_UUID +
      '",' +
      '"stage": "CLOSED_WON",' +
      '"number_of_employees": 500,' +
      '"items": {' +
      '"recurring_fees": [' +
      '{' +
      '"id": "9e258d9f-6330-480f-9c55-5131ead78f94",' +
      '"payments": [' +
      '{' +
      '"id": "77a66eb7-5766-4890-a6c1-87a3c763481b",' +
      '"waivers": [' +
      '{' +
      '"id":"8751e542-bd28-4b8b-a2e2-5320b5ffd394",' +
      '"begin_date":"2021-04-01T04:00:00.000Z",' +
      '"end_date":"2021-05-01T04:00:00.000Z",' +
      '"waived_percentage":100.0' +
      '}' +
      '],' +
      '"split_bill_percentage": 100,' +
      '"eligibles": [' +
      '{' +
      '"id": "d4b84207-743a-4894-804f-21a997e8176c",' +
      '"welcome_email_permitted": false,' +
      '"payroll_deduction_type": "AUTO_APPROVAL",' +
      '"payment_method_type": "STANDARD_METHODS",' +
      '"paid_by_client_id": "' +
      ACCOUNT_UUID +
      '",' +
      '"max_dependents": 1,' +
      '"managed_by_client_id": "' +
      ACCOUNT_UUID +
      '",' +
      '"name": "Headquarters",' +
      '"identifier_type": "EMAIL",' +
      '"exchange_type": "FILE_UPLOAD",' +
      '"is_default_item": true,' +
      '"communication_permitted_type": "ELIGIBLES"' +
      '}' +
      '],' +
      '"client_id": "aa47c899-7e59-47ad-9ad7-7d0f6310f4ea"' +
      '}' +
      '],' +
      '"payment_due_days": 30,' +
      '"currency_id": "BRL",' +
      '"variable_price_unit_increase": null,' +
      '"variable_price_floor": null,' +
      '"variable_price_ceiling": null,' +
      '"inflation_adjustment_type": null,' +
      '"inflation_adjustment_period": null,' +
      '"flat_unit_price": 19.04,' +
      '"flat_total_price": 190400,' +
      '"flat_discount_percentage": null,' +
      '"flat_baseline_quantity": 500,' +
      '"flat_baseline_adjustment": false,' +
      '"fee_type": "ACCESS_FEE",' +
      '"fee_price_type": "FLAT_PRICE_ELIGIBLE",' +
      '"discount_price_unit_decrease": null,' +
      '"discount_price_floor": null,' +
      '"discount_price_ceiling": null,' +
      '"cutoff_day": 1,' +
      '"billing_period": "YEAR",' +
      '"billing_day": 1' +
      '}' +
      '],' +
      '"one_time_fees": [' +
      '{' +
      '"id": "900606f8-9c86-4952-97b9-51559826a3fe",' +
      '"payments": [' +
      '{' +
      '"id": "f6c6be07-7bed-48d0-8f8c-eae29d2426b8",' +
      '"waivers": [],' +
      '"split_bill_percentage": 100,' +
      '"eligibles": [' +
      '{' +
      '"id": "199b9b0a-0e37-4df7-84ef-de1718ae48c7",' +
      '"welcome_email_permitted": false,' +
      '"payroll_deduction_type": "AUTO_APPROVAL",' +
      '"payment_method_type": "STANDARD_METHODS",' +
      '"paid_by_client_id": "' +
      ACCOUNT_UUID +
      '",' +
      '"max_dependents": 1,' +
      '"managed_by_client_id": "' +
      ACCOUNT_UUID +
      '",' +
      '"name": "Headquarters",' +
      '"identifier_type": "EMAIL",' +
      '"exchange_type": "FILE_UPLOAD",' +
      '"is_default_item": true,' +
      '"communication_permitted_type": "ELIGIBLES"' +
      '}' +
      '],' +
      '"client_id": "aa47c899-7e59-47ad-9ad7-7d0f6310f4ea"' +
      '}' +
      '],' +
      '"payment_due_days": 30,' +
      '"currency_id": "BRL",' +
      '"unit_price": 3.04,' +
      '"total_price": 30400,' +
      '"fee_type": "SETUP_FEE",' +
      '"discount_percentage": null,' +
      '"billing_date": "2021-04-01T04:00:00.000Z",' +
      '"baseline_quantity": 500' +
      '}' +
      ']' +
      '},' +
      '"gympass_entity_id": "' +
      GYMPASS_ENTITY_UUID +
      '",' +
      '"end_date": "2022-05-01T00:00:00.000Z",' +
      '"begin_date": "2021-04-30T00:00:00.000Z"' +
      '}' +
      '}';

    return (SelfCheckoutRequest) JSON.deserialize(
      requestJson,
      SelfCheckoutRequest.class
    );
  }

  public static SelfCheckoutRequest getMockWithAccountAndOpportunityEmpty() {
    String requestJson = '{"account": {}, "opportunity": {}}';

    return (SelfCheckoutRequest) JSON.deserialize(
      requestJson,
      SelfCheckoutRequest.class
    );
  }
}