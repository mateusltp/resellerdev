/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 09-14-2021
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/
@isTest(seeAllData=false)
public  class OrderSelectorTest {
   
    @TestSetup
    static void setupData(){
        String recordTypePartnerFlow = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Account').getRecordTypeId();
        Account partnerAcc = PartnerDataFactory.newAccount();
        partnerAcc.RecordTypeId = recordTypePartnerFlow;
        partnerAcc.CRM_ID__c = '1231';
        partnerAcc.UUID__c = new Uuid().getValue();
        INSERT partnerAcc;
             
        String recordTypePartnerContactFlow = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Contact').getRecordTypeId();
        Contact partnerContact = PartnerDataFactory.newContact(partnerAcc.Id);
        partnerContact.RecordTypeId = recordTypePartnerContactFlow;        
        partnerContact.Type_of_Contact__c = 'Admin';
        partnerContact.UUID__c = new Uuid().getValue();
        INSERT partnerContact;      

        Opportunity aOpp = PartnerDataFactory.newOpportunity( partnerAcc.Id, 'Partner_Flow_Opportunity'); 
        aOpp.UUID__c = new Uuid().getValue();
        INSERT aOpp;

        Quote proposal = PartnerDataFactory.newQuote( aOpp );
        proposal.RecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Proposal').getRecordTypeId();
        proposal.Signed__c = Date.Today() - 100;
        proposal.End_Date__c = Date.Today();
        proposal.UUID__c = new Uuid().getValue();
        INSERT proposal;

        Account_Opportunity_Relationship__c oppMember = PartnerDataFactory.newAcctOppRel(partnerAcc.Id, aOpp.Id);
        oppMember.RecordTypeId = Schema.SObjectType.Account_Opportunity_Relationship__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Opportunity_Member').getRecordTypeId();
        INSERT oppMember;

        Product_Item__c aProd = PartnerDataFactory.newProduct( aOpp );
        aProd.RecordTypeId = Schema.SObjectType.Product_Item__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product').getRecordTypeId();
        aProd.Opportunity__c = null;   
        aProd.UUID__c = new Uuid().getValue();    
        INSERT aProd;

        Commercial_Condition__c aComm = new Commercial_Condition__c();
        aComm.RecordTypeId = Schema.SObjectType.Commercial_Condition__c.getRecordTypeInfosByDeveloperName().get('CAP').getRecordTypeId();
        //aComm.Name = 'CAP';
        INSERT aComm;

        Product_Assignment__c prodAssign = new Product_Assignment__c();
        prodAssign.RecordTypeId = Schema.SObjectType.Product_Assignment__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product_Assignment').getRecordTypeId();
        prodAssign.ProductId__c = aProd.Id;
        prodAssign.CommercialConditionId__c = aComm.Id;
        INSERT prodAssign;

        Product_Opportunity_Assignment__c prodOppAssign = new Product_Opportunity_Assignment__c();
        prodOppAssign.RecordTypeId = Schema.SObjectType.Product_Opportunity_Assignment__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product_Opportunity_Assignment').getRecordTypeId();
        prodOppAssign.OpportunityMemberId__c = oppMember.Id;
        prodOppAssign.ProductAssignmentId__c = prodAssign.Id;
        INSERT prodOppAssign;
        
        Order aOrder =  new Order ();
        aOrder.AccountId = partnerAcc.Id;
        aOrder.QuoteId = proposal.Id;
        aOrder.CurrencyIsoCode = 'BRL';
        aOrder.EffectiveDate = Date.TODAY()-100;
        aOrder.EndDate = Date.TODAY()+460;
        aOrder.OpportunityId = aOpp.Id;
        aOrder.Status = 'Inactivated';
        aOrder.Type = 'Partner';
        aOrder.UUID__c = new Uuid().getValue();
        INSERT aOrder;

    }

    @isTest
    static void getSObjectFieldList_Test(){
        Test.startTest(); 
            System.assert(new OrderSelector().getSObjectFieldList().contains(Order.Id)); 
        Test.stopTest();
    }
   
    @isTest
    static void getSObjectType_Test(){
        Test.startTest(); 
            System.assertEquals(Order.sObjectType , new OrderSelector().getSObjectType()); 
        Test.stopTest();
    }

    @isTest
    static void gselectById_Test(){
        Order aOrder = [SELECT ID from Order LIMIT 1];
        Test.startTest(); 
            System.assertEquals( aOrder.Id , new OrderSelector().selectById(new Set<Id>{aOrder.Id})[0].Id); 
        Test.stopTest();
    }

    @isTest
    static void selectOrderFieldsToTagusParse_Test(){
        List<Order> aOrderLst = [SELECT ID from Order];
        Test.startTest(); 
            System.assertEquals( aOrderLst[0].Id , new OrderSelector().selectOrderFieldsToTagusParse(aOrderLst)[0].Id); 
        Test.stopTest();
    }    
}