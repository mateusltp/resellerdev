@isTest
public class SKUEditControllerTest {

    @TestSetup
    static void makeData(){

        Account gympassEntity = AccountMock.getGympassEntity();
        gympassEntity.CurrencyIsoCode = 'BRL';
        insert gympassEntity;
    
        Account account = AccountMock.getStandard('Empresas');
        account.CurrencyIsoCode = 'BRL';
        account.UUID__c = SelfCheckoutRequestMock.ACCOUNT_UUID;
    
        insert account;
    
        Contact contact = ContactMock.getStandard(account);
    
        insert contact;
    
        account.Attention__c = contact.Id;
    
        update account;
    
        Pricebook2 pricebook = new Pricebook2(
          CurrencyIsoCode = 'BRL',
          Id = Test.getStandardPricebookId(),
          IsActive = true
        );
    
        update pricebook;
    
        List<Product2> products = new List<Product2>();
    
        Product2 setupFee = ProductMock.getSetupFee();
        setupFee.Minimum_Number_of_Employees__c = 0;
        setupFee.Maximum_Number_of_Employees__c = 10000;
        setupFee.IsActive = true;
        setupFee.Family = 'Enterprise Subscription';
        setupFee.Copay2__c = false;
        setupFee.Family_Member_Included__c = false;
        products.add(setupFee);
    
        Product2 accessFee = ProductMock.getAccessFee();
        accessFee.Minimum_Number_of_Employees__c = 0;
        accessFee.Maximum_Number_of_Employees__c = 10000;
        accessFee.IsActive = true;
        accessFee.Family = 'Enterprise Subscription';
        accessFee.Copay2__c = false;
        accessFee.Family_Member_Included__c = false;
        products.add(accessFee);
    
        insert products;
    
        List<PricebookEntry> pricebookEntries = new List<PricebookEntry>();
    
        PricebookEntry setupFeePricebookEntry = PricebookMock.getPricebookEntry(
          pricebook,
          setupFee
        );
        setupFeePricebookEntry.UnitPrice = 10;
        pricebookEntries.add(setupFeePricebookEntry);
    
        PricebookEntry accessFeePricebookEntry = PricebookMock.getPricebookEntry(
          pricebook,
          accessFee
        );
        accessFeePricebookEntry.UnitPrice = 10;
        pricebookEntries.add(accessFeePricebookEntry);
    
        insert pricebookEntries;
    
        Opportunity opp = OpportunityMock.getNewBusiness(account, pricebook);
        opp.CurrencyIsoCode = 'BRL';
        opp.StageName = 'Lançado/Ganho';
    
        insert opp;
    
        Quote qt = QuoteMock.getStandard(opp);
        insert qt;
    
        QuoteLineItem qtItem = QuoteLineItemMock.getSetupFee(qt,setupFeePricebookEntry);
        insert qtItem;

        RecordType rt = [SELECT Id, Name FROM RecordType WHERE SobjectType='Waiver__c' AND Name = 'Compelling Waiver' LIMIT 1];
        List<Waiver__c> waivers = new List<Waiver__c>();
        Waiver__c waiver1 = WaiverMock.getStandard(qtItem);
        waiver1.RecordTypeId = rt.Id;
        waiver1.position__c = 1;
        Waiver__c waiver2 = WaiverMock.getStandard(qtItem);
        waiver2.RecordTypeId = rt.Id;
        waiver2.position__c = 2;
        Waiver__c waiver3 = WaiverMock.getStandard(qtItem);
        waiver3.RecordTypeId = rt.Id;
        waiver3.position__c = 3;
        waivers.add(waiver1);
        waivers.add(waiver2);
        waivers.add(waiver3);
        insert waivers;
        
    }

    @isTest
    static void updateProductAndWaiversTest(){

        QuoteLineItem qtItem = [SELECT Id FROM QuoteLineItem LIMIT 1];
        List<Waiver__c> waiversToUpdate = [SELECT Id FROM Waiver__c WHERE Position__c <= 2];
        waiversToUpdate[0].Duration__c = 1;
        waiversToUpdate[1].Duration__c = 2;

        List<Waiver__c> waiversToDelete = [SELECT Id FROM Waiver__c WHERE Position__c = 3];
        
        Test.startTest();
        SKUEditController.updateProductAndWaivers(qtItem, waiversToUpdate, waiversToDelete);
        Test.stopTest();

        List<Waiver__c> waivers = [SELECT Id, Duration__c FROM Waiver__c ORDER BY Position__c];

        System.assertEquals(2, waivers.size(), 'The number of expected waivers is not correct.');
        System.assertEquals(1, waivers[0].Duration__c, 'The duration__c field update is not correct.');
        System.assertEquals(2, waivers[1].Duration__c, 'The duration__c field update is not correct.');
        
    }

    @isTest
    static void updateProductAndWaiversExceptionTest(){
        
        Test.startTest();
        try {
            SKUEditController.updateProductAndWaivers(null, null, null);
            System.assert(false, 'This assert must never be executed. An exception must be thrown before this assert.');
        }
        catch(Exception e) {
            System.assertEquals('System.AuraHandledException', e.getTypeName(), 'The exception thrown is not the expected one');
        }
        
        Test.stopTest();
        
    }
    
}