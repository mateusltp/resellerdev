@IsTest
public class PricebookMock {
  public static Pricebook2 getByCountry(String countryName, String currencyIsoCode) {
    return new Pricebook2(
      Name = countryName,
      CurrencyIsoCode = currencyIsoCode,
      IsActive = true,
      Country__c = countryName
    );
  }

  public static PricebookEntry getPricebookEntry(Pricebook2 pricebook,Product2 product) {
    return new PricebookEntry(
      CurrencyIsoCode = pricebook.CurrencyIsoCode,
      IsActive = true,
      Pricebook2Id = pricebook.Id,
      Product2Id = product.Id,
      UnitPrice = 100
    );
  }
}