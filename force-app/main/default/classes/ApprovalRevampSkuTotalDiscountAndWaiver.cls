/**
 * @description       : 
 * @author            : roei@gft.com
 * @group             : 
 * @last modified on  : 05-26-2022
 * @last modified by  : roei@gft.com
**/
public without sharing class ApprovalRevampSkuTotalDiscountAndWaiver {
    List< Id > gLstOppId;
    Map< Id , Quote > gMapIdOldQuote;
    List< Quote > gLstNewQuote;
    List< Quote > gLstApprovedQuote;

    Set< String > gSetQuoteRecordtypeId = new Set< String >{
        Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Client_Sales_SKU_New_Business').getRecordTypeId(),
        Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Client_Success_SKU_Renegotiation').getRecordTypeId(),
        Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('SMB_SKU_New_Business').getRecordTypeId(),
        Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('SMB_Success_SKU_Renegotiation').getRecordTypeId()
    };

    public ApprovalRevampSkuTotalDiscountAndWaiver(){}

    public void saveFeesApprovedDiscountAndWaiver( Map< Id , Quote > aMapIdOldQuote , List< Quote > aLstNewQuote ){
        gLstNewQuote = aLstNewQuote;
        gMapIdOldQuote = aMapIdOldQuote;
        List< Assert_Data__c > lLstAssertData = saveTotalDiscountAndMaxWaiverApproved();

        if( !lLstAssertData.isEmpty() ){
            Database.upsert( lLstAssertData );
        }
    }

    private List< Assert_Data__c > saveTotalDiscountAndMaxWaiverApproved(){
        List< Assert_Data__c > lLstAssertData = new List< Assert_Data__c >();

        List< Id > lLstQuoteId = getApprovedQuoteId();

        if( lLstQuoteId.isEmpty() ){ return lLstAssertData; }
        
        lLstAssertData = setAssertData();

        return lLstAssertData;
    }

    private List< Assert_Data__c > setAssertData(){
        Set< String > lSetFieldPathApproval = new Set< String >{ 'quote_discount' , 'quote_waiver_max_number_of_months__c' };

        List< Assert_Data__c > lLstAssertData = 
            [ SELECT Id, Field_Name__c, Object_Name__c, Fee_Type__c,  Old_Value__c, New_Value__c, Pre_Approved_Condition__c, Full_Path_Data__c
              FROM Assert_Data__c WHERE Opportunity__c =: gLstOppId AND Full_Path_Data__c =: lSetFieldPathApproval ];
        
        if( lLstAssertData.size() < 2 ){
            for( Assert_Data__c iAssertData : lLstAssertData ){
                lSetFieldPathApproval.remove( iAssertData.Full_Path_Data__c );
            }

            for( String iFieldPath : lSetFieldPathApproval ){
                if( iFieldPath.contains( 'discount') ){
                    lLstAssertData.add( 
                        new Assert_Data__c( Opportunity__c = gLstOppId[0],
                            Field_Name__c = 'discount',
                            Object_Name__c = 'quote' ) );
                } else {
                    lLstAssertData.add( 
                        new Assert_Data__c( Opportunity__c = gLstOppId[0],
                            Field_Name__c = 'waiver_max_number_of_months__c',
                            Object_Name__c = 'quote' ) );
                }
            }
        }

        Decimal lNewTotalDiscount = gLstApprovedQuote[0].Discount;
        Decimal lNewMaxWaiver = gLstApprovedQuote[0].waiver_max_number_of_months__c;

        for( Assert_Data__c iAssertData : lLstAssertData ){
            if( iAssertData.Field_Name__c == 'discount' ){

                iAssertData.Old_Value__c = !String.isBlank( iAssertData.Old_Value__c ) && 
                    Decimal.valueOf( iAssertData.Old_Value__c ) >= lNewTotalDiscount ?
                    iAssertData.Old_Value__c : String.valueOf( lNewTotalDiscount );
            } else {

                iAssertData.Old_Value__c = !String.isBlank( iAssertData.Old_Value__c ) && 
                    Decimal.valueOf( iAssertData.Old_Value__c ) >= lNewMaxWaiver ?
                    iAssertData.Old_Value__c : String.valueOf( lNewMaxWaiver );
            }
        }

        return lLstAssertData;
    }

    private List< Id > getApprovedQuoteId(){
        gLstOppId = new List< Id >();
        List< Id > lLstQuoteId = new List< Id >();
        gLstApprovedQuote = new List< Quote >();
        
        for( Quote iQuote : gLstNewQuote ){
            if( gSetQuoteRecordtypeId.contains( iQuote.RecordTypeId ) && !gMapIdOldQuote.get( iQuote.Id ).Total_Discount_approved__c && iQuote.Total_Discount_approved__c ){
                lLstQuoteId.add( iQuote.Id );
                gLstOppId.add( iQuote.OpportunityId );
                gLstApprovedQuote.add( iQuote );
            }
        }

        return lLstQuoteId;
    }
}