/**
 * @description       : 
 * @author            : roei@gft.com
 * @group             : 
 * @last modified on  : 12-16-2020
 * @last modified by  : roei@gft.com
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   12-16-2020   roei@gft.com   Initial Version
**/
@isTest
public class DealDeskOperationalResetApprovedFlagTest {
	@TestSetup
    static void createData(){      
        Pricebook2 lStandardPricebook = new Pricebook2(
            Id = Test.getStandardPricebookId(),
            IsActive = true
        );
        Database.update( lStandardPricebook );
        
        Account lAcc = generateAccount();              
        Database.insert( lAcc );
                     
        Product2 lAccessFee = generateProduct('Enterprise Subscription');
        Database.insert( lAccessFee );
        Product2 lSetupFee = generateProduct('Setup Fee');
        Database.insert( lSetupFee );    
        
        Opportunity lNewBusinessOpp = generateNewBusinessOpp( lAcc.Id, lStandardPricebook );     
        Database.insert( lNewBusinessOpp );
        
        Quote lQuote = generateQuote( lNewBusinessOpp , ' 1' );
        Database.insert( lQuote );
        
        lNewBusinessOpp.SyncedQuoteId = lQuote.Id;
        lNewBusinessOpp.StageName = 'Proposta Aprovada';
        lNewBusinessOpp.FastTrackStage__c = 'Offer Approved';
        Database.update( lNewBusinessOpp );
    }
    
    @isTest
    public static void shouldResetSpecialistApprovedFromOpp() {
        Opportunity lOpp = [ SELECT Id FROM Opportunity WHERE Name = 'Renegotiation' ];
        Id lOperationalRtId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Deal_Desk_Operational').getRecordTypeId();
        
        Case lOperationalCase = new Case();
        lOperationalCase.OwnerId = [ SELECT Id FROM Group WHERE Name = 'Deal Desk Operational' LIMIT 1 ].Id;
        lOperationalCase.Description = 'Do not auto-approve';
		lOperationalCase.OpportunityId__c = lOpp.Id;
        lOperationalCase.RecordTypeId = lOperationalRtId;
        lOperationalCase.Justificate_No_Compliant_Topics__c = 'Justification Test';
		lOperationalCase.Subject = 'Deal Desk Operational Approval Request';
        Database.insert( lOperationalCase );
        
        lOpp.Specialist_Approved__c = true;
        
        lOperationalCase.Deal_Desk_Evaluation__c = 'Approved';
        lOperationalCase.Status = 'Approved';
        
        Database.update( lOperationalCase );
        Database.update( lOpp );
        
        Test.startTest();
        	lOperationalCase.Id = null;
        	lOperationalCase.Status = null;
        	Database.insert( lOperationalCase );
        Test.stopTest();
        
        lOpp = [ SELECT Id, Specialist_Approved__c FROM Opportunity WHERE Name = 'Renegotiation' ];
        lOperationalCase = [ SELECT Id, Status FROM Case WHERE Id =: lOperationalCase.Id ];
        
        System.assertEquals( 'New' , lOperationalCase.Status );
        System.assertEquals( false , lOpp.Specialist_Approved__c );
    }
    
    private static Account generateAccount(){
        ID accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
        Account acc = new Account();
        acc.RecordTypeId = accRecordTypeId;
        acc.Name = 'Account 1';
        acc.BillingCountry = 'Brazil';
        acc.BillingState = 'São Paulo';
        acc.NumberOfEmployees = 2000;
        return acc;
    }
	
	private static Product2 generateProduct(String ProductName){
        Product2 product = new Product2();
        product.Name = ProductName;
        product.Minimum_Number_of_Employees__c = 0;
        product.Maximum_Number_of_Employees__c = 900;
        product.Family = ProductName;
        product.IsActive = true;
        return product;
    }    
    
    private static Opportunity generateNewBusinessOpp(Id aAccId, Pricebook2 aPriceBook){
        Id lNewBusinessRtId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        Opportunity lOpp = new Opportunity();
        lOpp.CurrencyIsoCode='BRL';
        lOpp.recordTypeId = lNewBusinessRtId;
        lOpp.StageName='Qualification';
        lOpp.AccountId = aAccId;
        lOpp.CloseDate = Date.today();
        lOpp.Engagement_Journey_Completed__c = true;
        lOpp.Name='Renegotiation'; 
        lOpp.Pricebook2Id = aPriceBook.Id;
        return lOpp;
    }
    
    private static Quote generateQuote(Opportunity opp, String name){
        Quote qt = new Quote();  
        qt.RecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Gympass_Plus').getRecordTypeId();
        qt.Name = opp.Name+name;
        qt.OpportunityId = opp.Id;   
        qt.License_Fee_Waiver__c = 'No';  
        qt.CurrencyIsoCode = opp.CurrencyIsoCode;
        return qt;
    }
}