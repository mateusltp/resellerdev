public without sharing class SelfTerminationRequestValidator {
  
    private SelfTerminationRequest request;
    private Order order;

    private final String CANCEL_REQUESTED= 'Cancel Requested';
    private final String ACTIVATED = 'Activated';

    public SelfTerminationRequestValidator(SelfTerminationRequest request) {
      this.request = request;
    }
  
    public void validatePostRequest() {
      List<String> errorMessages = getAllErrors();
  
      if (!errorMessages.isEmpty()) {
        IntegrationException.setMessages(errorMessages);
  
        throw new IntegrationException(errorMessages.toString());
      }

      validateOrder();

    }
  
    private List<String> getAllErrors() {
      List<String> errorMessages = new List<String>();
  
      if (request == null) {
        errorMessages.add('Request body cannot be empty');
        return errorMessages;
      }
    
      for(String missingRequiredField : this.request.getMissingRequiredFields()) {
        errorMessages.add('The payload required field ' + missingRequiredField + ' is missing');
      }
  
      return errorMessages;
    }

    private void validateOrder() { 

      List<Order> orders = [
        SELECT Id, Status , Cancelation_Date__c, Cancelation_Reason__c, Cancelation_Description__c, OpportunityId, AccountId
        FROM Order
        WHERE UUID__c = :this.request.getClientOrderId()
        AND Account.UUID__c = : this.request.getClientId()
        //WITH SECURITY_ENFORCED 
        LIMIT 1
      ];
      if(orders.size() > 0 ){
        this.order = orders.get(0);
      }
      if (this.order == null) {
          // Exception to 404 - Not found
          throw new QueryException('Not possible to find an order related to client_order_id that is tied to an Account related to client_id');
  
      } else if(this.request.getAction() == 'Cancel' && this.order.Status != ACTIVATED) {
          // Exception to 409 - Conflict
          throw new HandledException('Action equals to CANCEL and the target Order is not on status ACTIVATED.');
  
      }else if(this.request.getAction() == 'Undo' && this.order.Status != CANCEL_REQUESTED ) {
          // Exception to 409 - Conflict
          throw new HandledException('Action equals to UNDO and the target Order is not on status CANCEL_REQUESTED.');
      }
    }

}