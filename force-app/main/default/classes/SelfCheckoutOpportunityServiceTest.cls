@IsTest
public class SelfCheckoutOpportunityServiceTest {
    
    @IsTest
    public static void execute() {
        
        Account gympassEntity = AccountMock.getGympassEntity();
        gympassEntity.CurrencyIsoCode = 'BRL';
        gympassEntity.UUID__c = SelfCheckoutOpportunityRequestMock.GYMPASS_ENTITY_UUID;
        insert gympassEntity;
        
        Account account = AccountMock.getStandard('Empresas');
        account.CurrencyIsoCode = 'BRL';
        
        insert account;
        
        Contact contact = ContactMock.getStandard(account);
        
        insert contact;
        
        Pricebook2 pricebook = new Pricebook2(
            CurrencyIsoCode = 'BRL',
            Id = Test.getStandardPricebookId(),
            IsActive = true
        );
        
        update pricebook;
        
        List<Product2> products = new List<Product2>();
        
        Product2 setupFee = ProductMock.getSetupFee();
        setupFee.Minimum_Number_of_Employees__c = 0;
        setupFee.Maximum_Number_of_Employees__c = 10000;
        setupFee.IsActive = true;
        setupFee.Family = 'Enterprise Subscription';
        setupFee.Copay2__c = false;
        setupFee.Family_Member_Included__c = false;
        products.add(setupFee);
        
        Product2 accessFee = ProductMock.getAccessFee();
        accessFee.Minimum_Number_of_Employees__c = 0;
        accessFee.Maximum_Number_of_Employees__c = 10000;
        accessFee.IsActive = true;
        accessFee.Family = 'Enterprise Subscription';
        accessFee.Copay2__c = false;
        accessFee.Family_Member_Included__c = false;
        products.add(accessFee);
        
        insert products;
        
        List<PricebookEntry> pricebookEntries = new List<PricebookEntry>();
        
        PricebookEntry setupFeePricebookEntry = PricebookMock.getPricebookEntry(pricebook,setupFee);
        setupFeePricebookEntry.UnitPrice = 10;
        pricebookEntries.add(setupFeePricebookEntry);
        
        PricebookEntry accessFeePricebookEntry = PricebookMock.getPricebookEntry(pricebook,accessFee);
        accessFeePricebookEntry.UnitPrice = 10;
        pricebookEntries.add(accessFeePricebookEntry);
        
        insert pricebookEntries;
        
        User integrationSMBJamorUser = [
            SELECT Id
            FROM User
            WHERE Name = 'Integration SMB Jamor'
            LIMIT 1
        ];
        
        System.runAs(integrationSMBJamorUser) {
            SelfCheckoutOpportunityService service =
                new SelfCheckoutOpportunityService(SelfCheckoutOpportunityRequestMock.getMock());
            service.leadConversion();
            Test.startTest();
            service.createOpportunity();
            Test.stopTest();
            service.calculateEnterpriseMetrics();
            service.handleSMBHistory();
        }
        
        Opportunity opportunity = [SELECT Id, StageName FROM Opportunity LIMIT 1];
        System.assertEquals(
            'Lançado/Ganho',
            opportunity.StageName,
            'The opportunity is not closed'
        );
    } 
    
    @IsTest
    public static void executeWithContactDiffBillingContact() {
        
        Account gympassEntity = AccountMock.getGympassEntity();
        gympassEntity.CurrencyIsoCode = 'BRL';
        gympassEntity.UUID__c = SelfCheckoutOpportunityRequestMock.GYMPASS_ENTITY_UUID;
        insert gympassEntity;
        
        Account account = AccountMock.getStandard('Empresas');
        account.CurrencyIsoCode = 'BRL';
        
        insert account;
        
        Contact contact = ContactMock.getStandard(account);
        
        insert contact;
        
        Pricebook2 pricebook = new Pricebook2(
            CurrencyIsoCode = 'BRL',
            Id = Test.getStandardPricebookId(),
            IsActive = true
        );
        
        update pricebook;
        
        List<Product2> products = new List<Product2>();
        
        Product2 setupFee = ProductMock.getSetupFee();
        setupFee.Minimum_Number_of_Employees__c = 0;
        setupFee.Maximum_Number_of_Employees__c = 10000;
        setupFee.IsActive = true;
        setupFee.Family = 'Enterprise Subscription';
        setupFee.Copay2__c = false;
        setupFee.Family_Member_Included__c = false;
        products.add(setupFee);
        
        Product2 accessFee = ProductMock.getAccessFee();
        accessFee.Minimum_Number_of_Employees__c = 0;
        accessFee.Maximum_Number_of_Employees__c = 10000;
        accessFee.IsActive = true;
        accessFee.Family = 'Enterprise Subscription';
        accessFee.Copay2__c = false;
        accessFee.Family_Member_Included__c = false;
        products.add(accessFee);
        
        insert products;
        
        List<PricebookEntry> pricebookEntries = new List<PricebookEntry>();
        
        PricebookEntry setupFeePricebookEntry = PricebookMock.getPricebookEntry(pricebook,setupFee);
        setupFeePricebookEntry.UnitPrice = 10;
        pricebookEntries.add(setupFeePricebookEntry);
        
        PricebookEntry accessFeePricebookEntry = PricebookMock.getPricebookEntry(pricebook,accessFee);
        accessFeePricebookEntry.UnitPrice = 10;
        pricebookEntries.add(accessFeePricebookEntry);
        
        insert pricebookEntries;
        
        User integrationSMBJamorUser = [
            SELECT Id
            FROM User
            WHERE Name = 'Integration SMB Jamor'
            LIMIT 1
        ];
        
        System.runAs(integrationSMBJamorUser) {
            SelfCheckoutOpportunityService service =
                new SelfCheckoutOpportunityService(SelfCheckoutOpportunityRequestMock.getMockWithDifferentContacts());
            service.leadConversion();
            Test.startTest();
            service.createOpportunity();
            Test.stopTest();
            service.calculateEnterpriseMetrics();
            service.handleSMBHistory();
        }
        
        Opportunity opportunity = [SELECT Id, StageName FROM Opportunity LIMIT 1];
        System.assertEquals(
            'Lançado/Ganho',
            opportunity.StageName,
            'The opportunity is not closed'
        );
    } 
    
    
    @IsTest
    public static void noMatchingLeadNoAccountNoContactTest(){
        
        User integrationSMBJamorUser = [
            SELECT Id
            FROM User
            WHERE Name = 'Integration SMB Jamor'
            LIMIT 1
        ];
        
        Test.startTest();
        
        System.runAs(integrationSMBJamorUser) {
            new SelfCheckoutOpportunityService(SelfCheckoutOpportunityRequestMock.getMock()).leadConversion();
        }
        
        Test.stopTest();
        
        Lead lead = [SELECT Id, IsConverted, RecordTypeId FROM Lead LIMIT 1];
        
        System.assertEquals(
            true,
            lead.IsConverted,
            'The lead is not converted'
        );
        
        Id recordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('SMB Direct Channel').getRecordTypeId();
        System.assertEquals(
            recordTypeId,
            lead.RecordTypeId,
            'The lead\'s record type is wrong'
        );
        
        Account account = [SELECT Id, Name, Send_To_Tagus__c FROM Account LIMIT 1];
        
        System.assertEquals(
            'trade name test',
            account.Name,
            'The account is not correct'
        );
        
        System.assertEquals(
            true,
            account.Send_To_Tagus__c,
            'The account\'s field Send_To_Tagus__c must be true'
        );
        
        Contact contact = [SELECT Id, Name, AccountId FROM contact LIMIT 1];
        
        System.assertEquals(
            account.Id,
            contact.AccountId,
            'The AccountId of Contact is not correct'
        );
        
    }
    
    @IsTest
    public static void matchingLeadNoAccountNoContactTest(){
        
        Lead lead = LeadMock.getSMBLead();
        lead.Email = 'Boyd_Harvey@example.com';
        
        insert lead;
        
        User integrationSMBJamorUser = [
            SELECT Id
            FROM User
            WHERE Name = 'Integration SMB Jamor'
            LIMIT 1
        ];
        
        Test.startTest();
        
        System.runAs(integrationSMBJamorUser) {
            new SelfCheckoutOpportunityService(SelfCheckoutOpportunityRequestMock.getMock()).leadConversion();
        }
        
        Test.stopTest();
        
        Lead leadResult = [SELECT Id, IsConverted, RecordTypeId FROM Lead LIMIT 1];
        
        System.assertEquals(
            true,
            leadResult.IsConverted,
            'The lead is not converted'
        );
        
        Id recordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('SMB Direct Channel').getRecordTypeId();
        System.assertEquals(
            recordTypeId,
            leadResult.RecordTypeId,
            'The lead\'s record type is wrong'
        );
        
        Account account = [SELECT Id, Name FROM Account LIMIT 1];
        
        System.assertEquals(
            'trade name test',
            account.Name,
            'The account is not correct'
        );
        
        Contact contact = [SELECT Id, Name, AccountId FROM contact LIMIT 1];
        
        System.assertEquals(
            account.Id,
            contact.AccountId,
            'The AccountId of Contact is not correct'
        );
        
    }
    
    @IsTest
    public static void matchingAccountNoContactTest(){
        
        Account account = AccountMock.getStandard('Empresas');
        account.Id_Company__c = '98.755.321/0001-09';
        
        insert account;
        
        User integrationSMBJamorUser = [
            SELECT Id
            FROM User
            WHERE Name = 'Integration SMB Jamor'
            LIMIT 1
        ];
        
        Test.startTest();
        
        System.runAs(integrationSMBJamorUser) {
            new SelfCheckoutOpportunityService(SelfCheckoutOpportunityRequestMock.getMock()).leadConversion();
        }
        
        Test.stopTest();
        
        Lead leadResult = [SELECT Id, IsConverted, RecordTypeId FROM Lead LIMIT 1];
        
        System.assertEquals(
            true,
            leadResult.IsConverted,
            'The lead is not converted'
        );
        
        Account accountResult = [SELECT Id, Name FROM Account LIMIT 1];
        
        System.assertEquals(
            'trade name test',
            accountResult.Name,
            'The account is not correct'
        );
        
        Contact contact = [SELECT Id, Name, AccountId FROM contact LIMIT 1];
        
        System.assertEquals(
            accountResult.Id,
            contact.AccountId,
            'The AccountId of Contact is not correct'
        );
        
    }
    
    @IsTest
    public static void matchingAccountMatchingContactTest(){
        
        Account account = AccountMock.getStandard('Empresas');
        account.Id_Company__c = '98.755.321/0001-09';
        insert account;
        
        Contact contact = ContactMock.getStandard(account);
        contact.Email = 'Boyd_Harvey@example.com';
        insert contact;
        
        User integrationSMBJamorUser = [
            SELECT Id
            FROM User
            WHERE Name = 'Integration SMB Jamor'
            LIMIT 1
        ];
        
        Test.startTest();
        
        System.runAs(integrationSMBJamorUser) {
            new SelfCheckoutOpportunityService(SelfCheckoutOpportunityRequestMock.getMock()).leadConversion();
        }
        
        Test.stopTest();
        
        Lead leadResult = [SELECT Id, IsConverted, RecordTypeId FROM Lead LIMIT 1];
        
        System.assertEquals(
            true,
            leadResult.IsConverted,
            'The lead is not converted'
        );
        
        Account accountResult = [SELECT Id, Name FROM Account LIMIT 1];
        
        System.assertEquals(
            'trade name test',
            accountResult.Name,
            'The account is not correct'
        );
        
        List<Contact> contacts = [SELECT Id, Name, AccountId, Finance_Contact__c FROM Contact];
        
        System.assertEquals(
            1,
            contacts.size(),
            'It must exists only 1 contact'
        );
        
        Contact contactResult = contacts[0];
        
        System.assertEquals(
            'Gene Mosciski',
            contactResult.Name,
            'The Contact name is not correct'
        );
        
        System.assertEquals(
            accountResult.Id,
            contactResult.AccountId,
            'The AccountId of Contact is not correct'
        );
        
        System.assertEquals(
            true,
            contactResult.Finance_Contact__c,
            'The Contact must be considered as finance contact'
        );
        
        
    }
    
    @IsTest
    public static void noMatchingAccountMatchingContactTest(){
        
        Account account = AccountMock.getStandard('Empresas');
        account.Id_Company__c = 'test tax id'; //no matching acount
        insert account;
        
        Contact contact = ContactMock.getStandard(account);
        contact.Email = 'Boyd_Harvey@example.com';
        insert contact;
        
        User integrationSMBJamorUser = [
            SELECT Id
            FROM User
            WHERE Name = 'Integration SMB Jamor'
            LIMIT 1
        ];
        
        Test.startTest();
        
        System.runAs(integrationSMBJamorUser) {
            new SelfCheckoutOpportunityService(SelfCheckoutOpportunityRequestMock.getMock()).leadConversion();
        }
        
        Test.stopTest();
        
        Lead leadResult = [SELECT Id, IsConverted, RecordTypeId FROM Lead LIMIT 1];
        
        System.assertEquals(
            true,
            leadResult.IsConverted,
            'The lead is not converted'
        );
        
        List<Account> accounts = [SELECT Id, Name FROM Account];
        
        System.assertEquals(
            2,
            accounts.size(),
            'It must exists 2 accounts'
        );
        
        List<Contact> contacts = [SELECT Id, Name, AccountId, Finance_Contact__c FROM Contact];
        
        System.assertEquals(
            1,
            contacts.size(),
            'It must exists only 1 contact'
        );
        
        Contact contactResult = contacts[0];
        
        System.assertEquals(
            'Gene Mosciski',
            contactResult.Name,
            'The Contact name is not correct'
        );
        
        List<AccountContactRelation> accountContactRelations = [SELECT AccountId, ContactId FROM AccountContactRelation];
        
        System.assertEquals(
            2,
            accountContactRelations.size(),
            'It must exists only 2 account contact relations'
        );
        
    }
    
    @IsTest
    public static void contactIsBillingContactTest() {
        
        User integrationSMBJamorUser = [
            SELECT Id
            FROM User
            WHERE Name = 'Integration SMB Jamor'
            LIMIT 1
        ];
        
        Test.startTest();
        
        System.runAs(integrationSMBJamorUser) {
            new SelfCheckoutOpportunityService(SelfCheckoutOpportunityRequestMock.getMock()).leadConversion();
        }
        
        Test.stopTest();
        
        List<Contact> contacts = [SELECT Id, Name, Finance_Contact__c FROM Contact];
        
        System.assertEquals(1, contacts.size(), 'It must exists only 1 contact');
        
        System.assertEquals(true, contacts[0].Finance_Contact__c, 'It must exists a contact considered as finance contact');
        
        
    }
    
    @IsTest
    public static void contactIsNotBillingContact() {
        
        User integrationSMBJamorUser = [
            SELECT Id
            FROM User
            WHERE Name = 'Integration SMB Jamor'
            LIMIT 1
        ];
        
        Test.startTest();
        
        System.runAs(integrationSMBJamorUser) {
            new SelfCheckoutOpportunityService(SelfCheckoutOpportunityRequestMock.getMockWithDifferentContacts()).leadConversion();
        }
        
        Test.stopTest();
        
        Contact contact = [SELECT Id, Name, AccountId FROM Contact WHERE Finance_Contact__c = TRUE LIMIT 1];
        
        System.assert(contact != null, 'It must exists a contact considered as finance contact');
        
        contact = [SELECT Id, Name, AccountId FROM Contact WHERE Finance_Contact__c = FALSE LIMIT 1];
        
        System.assert(contact != null, 'It must exists a contact not considered as finance contact');
        
    }
    
    @IsTest
    public static void matchingBillingContactFromOtherAccountTest() {
        
        Account account = AccountMock.getStandard('Empresas');
        insert account;
        
        Contact contact = ContactMock.getStandard(account);
        contact.Email = 'Boyd_Harvey_other@example.com';
        insert contact;
        
        User integrationSMBJamorUser = [
            SELECT Id
            FROM User
            WHERE Name = 'Integration SMB Jamor'
            LIMIT 1
        ];
        
        Test.startTest();
        
        System.runAs(integrationSMBJamorUser) {
            new SelfCheckoutOpportunityService(SelfCheckoutOpportunityRequestMock.getMockWithDifferentContacts()).leadConversion();
        }
        
        Test.stopTest(); 
        
        Contact billingContact = [SELECT Id, Name, AccountId, Finance_Contact__c FROM Contact WHERE Finance_Contact__c = TRUE LIMIT 1];
        
        Contact contactResult = [SELECT Id, Name, AccountId, Finance_Contact__c FROM Contact WHERE Finance_Contact__c = FALSE LIMIT 1];
        
        Account accountResult = [SELECT Id FROM Account WHERE Name = 'trade name test'];
        
        System.assertEquals(accountResult.Id, billingContact.AccountId, 'The billing contact\'s account is wrong');
        
        System.assertEquals(accountResult.Id, contactResult.AccountId, 'The contact\'s account is wrong');
        
        List<AccountContactRelation> accountContactRelations = [SELECT Id, AccountId, ContactId FROM AccountContactRelation WHERE ContactId = :billingContact.Id];
        
        System.assertEquals(2, accountContactRelations.size(), 'The billing contact must be related with 2 accounts');
        
    }
    
    
}