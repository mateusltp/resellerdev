public class SubManagedClause implements Comparable {
    
    private List<SubManagedClause> SubClauses;
    
    private ManagedClause SwapClause ;
    
    private string pClauseText;
    
    public Id ClauseId;  
    
    public string Name;
    
    public decimal Order;
    
    public Id ParentId;
    
    private string getSubClauseText(){
        string strSubClauseText = '';
        for (SubManagedClause subclause : this.SubClauses){
            strSubClauseText +=  subclause.ClauseText ; 
        }
        return strSubClauseText != '' ? strSubClauseText  : '' ;
    }
    
    public Integer compareTo(Object compareTo) {
        SubManagedClause compareToSubManagedClause = (SubManagedClause)compareTo;
        if (Order == compareToSubManagedClause.Order) return 0;
        if (Order > compareToSubManagedClause.Order) return 1;
        return -1;        
    }
    
    public string ClauseText {
        get { return this.SwapClause != null ? this.SwapClause.ClauseText() + this.getSubClauseText(): this.pClauseText + this.getSubClauseText();  }
    	set { this.pClauseText = value ; }
    }
    
    public string ClauseText(){
        return this.SwapClause != null ? this.SwapClause.ClauseText() + this.getSubClauseText() : this.pClauseText + this.getSubClauseText();
    }
    
   
    
    public void addSwapClause(ManagedClause swapClause){
        if (this.SwapClause == null){
            this.SwapClause = new ManagedClause();
            this.SwapClause = swapClause;
        }else
        {
            this.SwapClause = swapClause;
        }
    }
    
    public SubManagedClause(){
        SubClauses = new List<SubManagedClause>();
    }
    
    public void addSubClause(SubManagedClause subClause){
        this.SubClauses.add(subClause);
        this.SubClauses.sort();
    }
    
    public void addSubClause(List<SubManagedClause> subClauseList){
        for (SubManagedClause subclause : subClauseList){
            this.addSubClause(subclause);
        }
    }
    
    public List<SubManagedClause> getSubClauses(){
        return this.SubClauses;
    }
    
    public integer getSubClausesLength(){
        return this.SubClauses.size();
    }
}