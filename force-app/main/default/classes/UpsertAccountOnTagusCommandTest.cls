@IsTest
public class UpsertAccountOnTagusCommandTest {
  @TestSetup
  public static void makeData() {
    TriggerHandler.bypass('AccountTriggerHandler');
    TriggerHandler.bypass('ContactTriggerHandler');

    Account parentAccount = new Account(
      Name = 'João e Brenda Publicidade e Propaganda Ltda',
      BillingCountry = 'Brazil',
      BillingState = 'São Paulo'
    );
    insert parentAccount;

    Account account = new Account(
      Name = 'Theo e Eliane Alimentos Ltda',
      BillingCountry = 'Brazil',
      BillingState = 'São Paulo',
      ParentId = parentAccount.Id
    );
    insert account;

    Contact contact = new Contact(
      FirstName = 'Emanuel',
      LastName = 'Yago Costa',
      AccountId = account.Id,
      Email = 'emanuelyagocosta-87@bemarius.example',
      Role__c = 'FINANCE'
    );
    insert contact;
  }

  @IsTest
  public static void execute() {
    Test.setMock(HttpCalloutMock.class, new HttpResponseMock());

    EventQueue event = new EventBuilder()
      .createEventFor('CREATE_ACCOUNT_ON_TAGUS')
      .withSender('SALESFORCE')
      .withReceiver('TAGUS')
      .buildEvent();

    EventQueueFixtureFactory.createEventConfigForEvent(
      event.getEventName(),
      'UpsertAccountOnTagusCommand'
    );

    List<Account> accounts = [SELECT Id FROM Account];

    Map<Id, Account> accountMap = AccountTagusDTO.getAccountMapWithFieldsToParse(
      accounts
    );

    event.addPayload(event.getEventName(), JSON.serialize(accountMap));

    Test.startTest();

    event.save();

    Test.stopTest();

    System.assertEquals(
      2,
      [SELECT UUID__c FROM Account WHERE UUID__c != NULL].size(),
      'Accounts not integrated'
    );

    System.assertEquals(
      1,
      [SELECT UUID__c FROM Contact WHERE UUID__c != NULL].size(),
      'Contacts not integrated'
    );
  }

  @IsTest
  public static void executeWithNoPermission() {
    Test.setMock(HttpCalloutMock.class, new HttpResponseMock());

    EventQueue event = new EventBuilder()
      .createEventFor('UPDATE_ACCOUNT_ON_TAGUS')
      .withSender('SALESFORCE')
      .withReceiver('TAGUS')
      .buildEvent();

    EventQueueFixtureFactory.createEventConfigForEvent(
      event.getEventName(),
      'UpsertAccountOnTagusCommand'
    );

    List<Account> accounts = [SELECT Id FROM Account];

    Map<Id, Account> accountMap = AccountTagusDTO.getAccountMapWithFieldsToParse(
      accounts
    );

    event.addPayload(event.getEventName(), JSON.serialize(accountMap));

    event.save();

    Profile profileReadOnly = [SELECT Id FROM Profile WHERE Name = 'Read Only'];

    User userReadOnly = new User(
      FirstName = 'Isabell',
      LastName = 'Renata Nogueira',
      ProfileId = profileReadOnly.Id,
      Email = 'isabellarenatanogueira..isabellarenatanogueira@afsn.example',
      Username = 'isabellarenatanogueira..isabellarenatanogueira@afsn.example',
      Alias = 'IsaEx',
      EmailEncodingKey = 'UTF-8',
      LanguageLocaleKey = 'en_US',
      LocaleSidKey = 'en_US',
      TimeZoneSidKey = 'America/Los_Angeles'
    );

    System.runAs(userReadOnly) {
      Test.startTest();

      event.process();

      Test.stopTest();
    }

    Queue__c eventResult = [
      SELECT Id, Status__c
      FROM Queue__c
      WHERE Id = :event.getEventId()
    ];

    System.assertEquals(
      'ERROR',
      eventResult.Status__c,
      'Should have occured an error in the event'
    );
  }

  private class HttpResponseMock implements HttpCalloutMock {
    public HTTPResponse respond(HTTPRequest req) {
      HttpResponse res = new HttpResponse();
      res.setHeader('Content-Type', 'application/json');
      res.setStatusCode(200);
      return res;
    }
  }
}