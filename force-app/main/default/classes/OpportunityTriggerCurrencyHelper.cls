/**
 * @description       : This class is responsible for setting the correct currency for 'Client Sales - New Business' opportunities
 * @author            : roei@gft.com
 * @group             : 
 * @last modified on  : 06-10-2021
 * @last modified by  : roei@gft.com
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   09-11-2020   roei@gft.com   Initial Version
**/
public class OpportunityTriggerCurrencyHelper {

    private Map<String , String> mapCurrencyByCountry;
    private Set< String > gSetRtIdToSetCurrency = new Set< String >{
        Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId(),
        Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Success_Renegotiation').getRecordTypeId(),
        Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SMB_New_Business').getRecordTypeId(),
        Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SMB_Success_Renegotiation').getRecordTypeId(),
        Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Gyms_Small_Medium_Partner').getRecordTypeId(),
        Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Partner_Small_and_Medium_Renegotiation').getRecordTypeId(),
        Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Gyms_Wishlist_Partner').getRecordTypeId(),
        Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Partner_Wishlist_Renegotiation').getRecordTypeId(),
        Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Opportunity').getRecordTypeId(),
        Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Child_Opportunity').getRecordTypeId()
    };

    Id accountPartnerOldFlowRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gym_Partner').getRecordTypeId();

    public OpportunityTriggerCurrencyHelper(){
        setMapCurrencyByCountry();
    }

    public void setCurrencyFieldFromMetadata(){
        setCurrencyOnBeforeInsertOpportunities();
    }

    public void setCurrencyOnBeforeInsertOpportunities(){
        Set<Id> accIds = new Set<Id>();

        List<Opportunity> oppsToUpdateCurrency = new List<Opportunity>();

        for( Opportunity iOpp : (List<Opportunity>)Trigger.new ){
            if( String.isNotBlank( iOpp.AccountId ) && gSetRtIdToSetCurrency.contains( iOpp.RecordTypeId )){
                accIds.add(iOpp.AccountId);
                oppsToUpdateCurrency.add(iOpp);
            }
        }

        if( accIds.isEmpty() ){ return; }

        Map<Id , String> mapAccIdsCountries = new Map<Id , String>();

        for( Account iAcc : [SELECT Id, RecordTypeId, BillingCountry, ShippingCountry FROM Account WHERE Id = :accIds ]){
            if (iAcc.RecordTypeId == accountPartnerOldFlowRecordTypeId) {
                if( String.isBlank(iAcc.ShippingCountry) ){ continue; }

                mapAccIdsCountries.put(iAcc.Id, iAcc.ShippingCountry);
            } else {
                if( String.isBlank(iAcc.BillingCountry) ){ continue; }

                mapAccIdsCountries.put(iAcc.Id, iAcc.BillingCountry);
            }
        }

        if( mapAccIdsCountries.isEmpty() ){ return; }

        for( Opportunity iOpp : oppsToUpdateCurrency ){
            String relatedAccCountry = mapAccIdsCountries.get( iOpp.AccountId );
            String oppCurrency = mapCurrencyByCountry.get( relatedAccCountry );

            if( String.isBlank( oppCurrency ) ){ continue; }

            iOpp.CurrencyIsoCode = oppCurrency;
        }
    }

    private void setMapCurrencyByCountry(){
        mapCurrencyByCountry = new Map<String , String>();

        for( CurrencyByCountry__mdt iCurrencyByContry : [ SELECT Country__c, Currency__c, Code__c FROM CurrencyByCountry__mdt ] ){
            mapCurrencyByCountry.put(iCurrencyByContry.Country__c, iCurrencyByContry.Currency__c);
        }
    }
}