/**
 * @description       : 
 * @author            : gft.jorge.stevaux@ext.gympass.com
 * @group             : 
 * @last modified on  : 05-03-2022
 * @last modified by  : gepi@gft.com
**/
public without sharing class CloneObject {
    
    
    
    public CloneObject() {
        
    }
    
    private Map<String, String> mapFieldsMatchName(){
        Map<String, String> mapReturn =new Map<String, String>();
        mapReturn.put('APXT_Amendment_Version__c'                               ,'AmendmentVersion__c');
        mapReturn.put('Conga_Sign_Status__c'                                    ,'SignStatus__c');
        mapReturn.put('Include_full_T_C_s__c'                                   ,'IncludeFullTCs__c');
        mapReturn.put('Include_signature__c'                                    ,'IncludeSignature__c');
        mapReturn.put('Legal_Contract_Status__c'                                ,'ContractStatus__c');
        mapReturn.put('APXT_Redlining__Legal_Entity__c'                         ,'LegalEntity__c');
        mapReturn.put('Opportunity__c'                                          ,'Opportunity__c');
        mapReturn.put('OwnerId'                                                 ,'OwnerId');
        //mapReturn.put('APXT_Redlining__Contract_Agreement_Family_Parent__c'     ,'ParentDocument__c');
        mapReturn.put('Parent_Opportunity__c'                                   ,'ParentOpportunity__c');
        mapReturn.put('Parent_Proposal__c'                                      ,'ParentProposal__c');
        mapReturn.put('Proposal__c'                                             ,'Proposal__c');
        mapReturn.put('APXT_Renegotiation__c'                                   ,'Renegotiation__c');
        mapReturn.put('APXT_Redlining__Status__c'                               ,'Status__c');
        mapReturn.put('Template_Selection__c'                                   ,'TemplateSelection__c');
        mapReturn.put('CurrencyIsoCode'                                         ,'CurrencyIsoCode');
        mapReturn.put('SignedMethod__c'                                         ,'SignedMethod__c');
        mapReturn.put('SignedDate__c'                                           ,'SignedDate__c');
        mapReturn.put('Type_of_contract__c'                                     ,'Type_of_contract__c');
        return mapReturn;
        

    }




    public void CloneDataFromContractAgreement(List<APXT_Redlining__Contract_Agreement__c> listObjectsToMatch){
        
        Set<Id> IdsOfTargetObjects = (new Map<Id,SObject>(listObjectsToMatch)).keySet();

        Map<APXT_Redlining__Contract_Agreement__c, Document__c> mapSourceTarget = new  Map<APXT_Redlining__Contract_Agreement__c, Document__c>();

        List<Document__c> listTargetObjects = new List<Document__c>();
        system.debug('IdsOfTargetObjects : ' + IdsOfTargetObjects);

        listTargetObjects = [   SELECT Id, ContractAgreement__c, Type_of_contract__c, AmendmentVersion__c , SignStatus__c, IncludeFullTCs__c , IncludeSignature__c , ContractStatus__c , LegalEntity__c , Opportunity__c , OwnerId , ParentDocument__c, ParentOpportunity__c,  ParentProposal__c , Proposal__c ,Renegotiation__c, Status__c , TemplateSelection__c , CurrencyIsoCode, SignedMethod__c, SignedDate__c
                                FROM  Document__c 
                                WHERE ContractAgreement__c IN : IdsOfTargetObjects ];
                          
        
        if (!listTargetObjects.isEmpty()){
            for (APXT_Redlining__Contract_Agreement__c sourceObj : listObjectsToMatch){
                for (Document__c targetObj : listTargetObjects){
                    if (targetObj.ContractAgreement__c == sourceObj.Id ){
                        mapSourceTarget.put(sourceObj, targetObj);
                    }
                }
            }
        }

        for (APXT_Redlining__Contract_Agreement__c sourceObj : listObjectsToMatch){
            if (!mapSourceTarget.containsKey(sourceObj)){
                mapSourceTarget.put(sourceObj, null);
                system.debug('Add new doc');
            }
        }

        for (APXT_Redlining__Contract_Agreement__c sourceObj : mapSourceTarget.keySet()){
            if (mapSourceTarget.get(sourceObj) != null){
                this.MatchObjects(sourceObj, mapSourceTarget.get(sourceObj));
            }else{
                Document__c newDoc = new Document__c();
                newDoc.RecordTypeId = Schema.SObjectType.Document__c.getRecordTypeInfosByName().get('Contract').getRecordTypeId();
                 
                System.debug(sourceObj);
                system.debug('!!!!!!! ' + sourceObj.recordtypeid);
                
                string recordtypename = Schema.SObjectType.APXT_Redlining__Contract_Agreement__c.getRecordTypeInfosById().get(sourceObj.recordtypeid).getname();
                
                system.debug('!!! ' + recordtypename);

                switch on recordtypename{
                    when 'B2B Contract' {		
                        newDoc.Type__c = 'B2B';
                    }	
                    when 'Partner Contract' {
                        newDoc.Type__c = 'Partner';
                    }
                    when 'Indirect Contract' {		
                        newDoc.Type__c = 'B2B';
                    }
                    when else {		  
                    }
                }  

                newDoc.ContractAgreement__c = sourceObj.Id;
                mapSourceTarget.put(sourceObj, newDoc);
                this.MatchObjects(sourceObj, newDoc);
            }
        }

        //system.debug('mapSourceTarget >> ' + mapSourceTarget.values());

        if (!mapSourceTarget.values().isEmpty()){
            upsert mapSourceTarget.values();
        }

    }

 
    public void CloneDataFromDocument(List<Document__c> listObjectsToMatch){
        
        Set<Id> IdsOfTargetObjects = new Set<Id>();

        for (Document__c doc : listObjectsToMatch){
            IdsOfTargetObjects.add(doc.ContractAgreement__c);
        }

        Map<Document__c, APXT_Redlining__Contract_Agreement__c> mapSourceTarget = new  Map<Document__c, APXT_Redlining__Contract_Agreement__c>();

        List<APXT_Redlining__Contract_Agreement__c> listTargetObjects = new List<APXT_Redlining__Contract_Agreement__c>();

        listTargetObjects = [   SELECT  Id                                                      , 
                                        APXT_Amendment_Version__c                               ,
                                        Conga_Sign_Status__c                                    ,
                                        Include_full_T_C_s__c                                   ,
                                        Include_signature__c                                    ,
                                        Legal_Contract_Status__c                                ,
                                        APXT_Redlining__Legal_Entity__c                         ,
                                        Opportunity__c                                          ,
                                        OwnerId                                                 ,        
                                        APXT_Redlining__Contract_Agreement_Family_Parent__c     ,
                                        Parent_Opportunity__c                                   ,
                                        Parent_Proposal__c                                      ,    
                                        Proposal__c                                             ,
                                        APXT_Renegotiation__c                                   ,
                                        APXT_Redlining__Status__c                               ,
                                        Template_Selection__c                                   ,
                                        CurrencyIsoCode          								,
                             			SignedMethod__c                                         ,
                                        Type_of_contract__c	                                    ,
                                        SignedDate__c
                                FROM  APXT_Redlining__Contract_Agreement__c 
                                WHERE Id IN : IdsOfTargetObjects ];

        system.debug('listTargetObjects ' + listTargetObjects);                                
        
        if (!listTargetObjects.isEmpty()){
            for (Document__c sourceObj : listObjectsToMatch){
                for (APXT_Redlining__Contract_Agreement__c targetObj : listTargetObjects){
                    if (targetObj.Id == sourceObj.ContractAgreement__c ){
                        mapSourceTarget.put(sourceObj, targetObj);
                    }
                }
            }
        }

       
        for (Document__c sourceObj : mapSourceTarget.keySet()){
            this.MatchDocument(sourceObj, mapSourceTarget.get(sourceObj));
        }

        //system.debug('mapSourceTarget >> ' + mapSourceTarget.values());

        if (!mapSourceTarget.values().isEmpty()){
            upsert mapSourceTarget.values();
        }

    }


    
    public void MatchObjects(sObject fromObject, sObject toObject){

        Map<String, String> mapFieldsToMatch = this.mapFieldsMatchName();

        for (String fieldName : mapFieldsToMatch.keyset()){
            if (fromObject.get(fieldName) != toObject.get(mapFieldsToMatch.get(fieldName))){
                system.debug('!!! diference = ' + mapFieldsToMatch.get(fieldName) + '  -  ' + fromObject.get(fieldName) + ' - ' + toObject.get(mapFieldsToMatch.get(fieldName)) );
                toObject.put(mapFieldsToMatch.get(fieldName), fromObject.get(fieldName) );
            }

        } 
    }


    public void MatchDocument(sObject fromObject, sObject toObject){

        Map<String, String> mapFieldsToMatch = this.mapFieldsMatchName();

        for (String fieldName : mapFieldsToMatch.keyset()){
            if (toObject.get(fieldName) != fromObject.get(mapFieldsToMatch.get(fieldName))){
                system.debug('!!! diference = ' + mapFieldsToMatch.get(fieldName) + '  -  ' + toObject.get(fieldName) + ' - ' + fromObject.get(mapFieldsToMatch.get(fieldName)) );
                toObject.put(fieldName , fromObject.get(mapFieldsToMatch.get(fieldName)) );
            }

        } 
    }
}