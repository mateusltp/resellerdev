global class AutoConvertLeadsBatchSch Implements Schedulable {
    global void execute(SchedulableContext sc) {
        scheduleAutoConvertLeadsBatch();
    }
    
    public void scheduleAutoConvertLeadsBatch() {
        Database.executeBatch(new AutoConvertLeadsBatch(),1);
    }
}