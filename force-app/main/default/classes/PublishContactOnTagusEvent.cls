public without sharing class PublishContactOnTagusEvent {
  public void execute() {
    if (UserInfo.getName().equals('Integration SMB Jamor')) {
      return;
    }

    Set<Id> accountIds = new Set<Id>();

    for (Contact newContact : (List<Contact>) Trigger.new) {
      if (isToPublishContact(newContact)) {
        accountIds.add(newContact.AccountId);
      }
    }

    List<Account> accounts = queryAccounts(accountIds);

    if (!accounts.isEmpty()) {
      new TagusAccountOutboundPublisher(accounts).runUpdate();
    }
  }

  private Boolean isToPublishContact(Contact newContact) {
    Map<Id, Contact> oldContactMap = (Map<Id, Contact>) Trigger.oldMap;

    Contact oldContact = oldContactMap?.get(newContact.Id);

    return newContact.Status_do_contato__c.equals('Ativo') &&
      (oldContact == null ||
      oldContact.Status_do_contato__c != newContact.Status_do_contato__c ||
      oldContact.LastName != newContact.LastName ||
      oldContact.Email != newContact.Email ||
      oldContact.Role__c != newContact.Role__c ||
      oldContact.FirstName != newContact.FirstName ||
      oldContact.MailingStreet != newContact.MailingStreet ||
      oldContact.MailingCity != newContact.MailingCity ||
      oldContact.MailingCountry != newContact.MailingCountry ||
      oldContact.MailingState != newContact.MailingState ||
      oldContact.MailingPostalCode != newContact.MailingPostalCode ||
      oldContact.Phone != newContact.Phone);
  }

  private List<Account> queryAccounts(Set<Id> accountIds) {
    return [
      SELECT Id, UUID__c
      FROM Account
      WHERE
        Id IN :accountIds
        AND UUID__c != NULL
        AND Is_Account_On_Tagus__c = TRUE
      WITH SECURITY_ENFORCED
    ];
  }
}