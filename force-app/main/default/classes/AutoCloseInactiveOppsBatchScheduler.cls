/**
 * @description		Scheduler for the batchable AutoCloseInactiveOppsBatch.
 * @date			08-07-2022
 */
public class AutoCloseInactiveOppsBatchScheduler implements Schedulable {
    public void execute(SchedulableContext sc) {
        Id jobId = Database.executeBatch(new AutoCloseInactiveOppsBatch(), 200);
    }
}