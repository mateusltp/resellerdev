/**
 * @description       : 
 * @author            : AlyssonMota
 * @group             : 
 * @last modified on  : 11-26-2020
 * @last modified by  : Alysson Mota
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   11-26-2020   Alysson Mota   Initial Version
**/
public with sharing class ClientsEnablersRelatedListController {
    @AuraEnabled
    public static List<Enabler> getEnablers(String recordId){
        List<Enabler> enablers = new List<Enabler>();

        List<Step_Towards_Success1__c> steps = [
            SELECT Id, Name, Achieved__c
            FROM Step_Towards_Success1__c
            WHERE Related_Account__c =: recordId
            AND Manual_changes_allowed__c = true
        ];

        for (Step_Towards_Success1__c step : steps) {
            Enabler enabler = new Enabler();
            enabler.Name = step.Name;
            enabler.Id = step.Id;
            enabler.Achieved = (step.Achieved__c == 'Yes' ? true : false);

            enablers.add(enabler);
        }

        return enablers;
    }

    @AuraEnabled
    public static UpdateEnablerResponse updateEnablerApx(String recordId, Boolean achieved){
        UpdateEnablerResponse response = new UpdateEnablerResponse();
        try {
            Step_Towards_Success1__c step = [
                SELECT Id, Achieved__c
                FROM Step_Towards_Success1__c
                WHERE Id =: recordId
            ];

            if (achieved == true) {
                step.Achieved__c = 'Yes';
            }
            else if (achieved == false) {
                step.Achieved__c = 'No';
            }

            update step;

            response.status = 'SUCCESS';
            response.message = 'The enabler has been updated';
            
        } catch (Exception e) {
            response.status = 'ERROR';
            response.message = e.getMessage();
        }

        return response;
    }

    public class Enabler {
        @AuraEnabled public String Id {get;set;}
        @AuraEnabled public String Name {get;set;}
        @AuraEnabled public Boolean Achieved {get;set;}
    }

    public class UpdateEnablerResponse {
        @AuraEnabled public String status {get;set;}
        @AuraEnabled public String message {get;set;}
    }
}