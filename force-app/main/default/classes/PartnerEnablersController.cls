/**
 * @description       : 
 * @author            : alysson.mota@gympass.com
 * @group             : 
 * @last modified on  : 04-08-2022
 * @last modified by  : alysson.mota@gympass.com
**/
public with sharing class PartnerEnablersController {

    /**
    * @description 
    * @author alysson.mota@gympass.com | 04-04-2022 
    * @param oppId 
    * @return StepsTowardsSuccessCtrl.Response 
    **/
    public static StepsTowardsSuccessCtrl.Response getResponse(Id oppId) {
        StepsTowardsSuccessCtrl.Response response = new StepsTowardsSuccessCtrl.Response();
        List<StepsTowardsSuccessCtrl.StepsTowardsSuccessWrapper> wrapperLst = new List<StepsTowardsSuccessCtrl.StepsTowardsSuccessWrapper>();

        List<Step_Towards_Success_Partner__c> steps = getOppSteps(oppId);

        for (Step_Towards_Success_Partner__c step : steps) {
            StepsTowardsSuccessCtrl.StepsTowardsSuccessWrapper wrapper = new StepsTowardsSuccessCtrl.StepsTowardsSuccessWrapper();
            wrapper.label = step.Name;
            wrapper.option = (step.Achieved_New_Flow__c ? 'positive' : 'negative');
            wrapperLst.add(wrapper);
        }

        response.stepsTowardsSuccessWrapper = wrapperLst;

        return response;
    }

    /**
    * @description 
    * @author alysson.mota@gympass.com | 04-04-2022 
    * @param oppId 
    * @return List<Step_Towards_Success_Partner__c> 
    **/
    private static List<Step_Towards_Success_Partner__c> getOppSteps(Id oppId) {
        StepsTowardsSuccessService service = new StepsTowardsSuccessService();
        Map<Id, List<Step_Towards_Success_Partner__c>> oppIdToSteps = service.getOppNewFlowPartnerSteps(new Set<Id>{oppId});
        List<Step_Towards_Success_Partner__c> steps = oppIdToSteps.get(oppId);
        
        return steps;
    }
}