@IsTest
public class OrderMock {
  public static Order getStandard(Opportunity opportunity, Quote quote) {
    return new Order(
      AccountId = opportunity.AccountId,
      ContractId = quote.ContractId,
      CurrencyIsoCode = opportunity.CurrencyIsoCode,
      EffectiveDate = quote.Start_Date__c,
      EndDate = quote.End_Date__c,
      Cancelation_Date__c = opportunity.cancellation_date__c,
      Cancelation_Reason__c = opportunity.Cancellation_Reason__c,
      OpportunityId = quote.OpportunityId,
      Pricebook2Id = opportunity.Pricebook2Id,
      QuoteId = quote.Id,
      Status = 'Draft',
      Type = opportunity.Type,
      Contact_Permission__c = quote.Contact_Permission__c,
      Employee_Registration_Method__c = quote.Employee_Registration_Method__c,
      Gympass_Entity__c = opportunity.Gympass_Entity__c,
      Unique_Identifier__c = quote.Unique_Identifier__c
    );
  }
}