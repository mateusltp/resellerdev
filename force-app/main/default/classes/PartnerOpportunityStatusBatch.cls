/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 07-29-2021
 * @last modified by  : Jorge Stevaux - JZRX@gft.com
 * Modifications Log 
 * Ver   Date         Author                             Modification
 * 1.0   05-31-2021   gft.samuel.silva@ext.gympass.com   Initial Version
**/
global without sharing class PartnerOpportunityStatusBatch implements Database.Batchable<sObject>, Schedulable {
    
      //Seconds Minutes Hours Day_of_month Month Day_of_week Optional_year 
      private static String gStdSchedTime = '0 0 0 * * ?';  //Every Day at Midnight 
      

      global PartnerOpportunityStatusBatch() {}
  
      global Database.QueryLocator start(Database.BatchableContext BC) {

            return ((OpportunitySelector) Application.Selector.NewInstance(Opportunity.SObjectType)).selectPartnerToLostQueryLocator();
// teste
      }


      global void execute(Database.BatchableContext BC, List<Opportunity> oppScopeLst) {  

            OpportunityService.updateStageToLost(oppScopeLst); 
        
      }
  
      global void execute(SchedulableContext SC) {
          Database.executebatch(new PartnerOpportunityStatusBatch(), 10);
      }
  
   
      global void finish(Database.BatchableContext BC) {}   
  
      global static void start(String schedTime){
        gStdSchedTime = schedTime == null || String.isBlank(schedTime) ?  '0 0 0 * * ?' : schedTime;      
        String jobName =  Test.isRunningTest() ? 'Partner Opportunity Status Test' : 'Partner Opportunity Status';
        System.Schedule(jobName, gStdSchedTime, new PartnerOpportunityStatusBatch());
      }
}