/**
 * @File Name          : ApprovalProcessHandler.cls
 * @Description        : 
 * @Author             : GEPI@GFT.com
 * @Group              : 
 * @Last Modified By   : alysson.mota@gympass.com
 * @Last Modified On   : 04-08-2022
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    05/05/2020   Samuel Silva - GFT (slml@gft.com)     Initial Version
**/
public without sharing class ApprovalProcessHandler {
  
    public void submitApproval(SObject obj) {
        String sObjectType = obj.getSobjectType().getDescribe().getName();        
        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();     
        if(sObjectType == 'Account') {                
            Account acc = (Account)obj; 
            req1.setComments('Submitting new account for approval: ' + acc.Creation_Reason_Details__c);
            req1.setObjectId(acc.id);            
        }        
        Approval.ProcessResult result = Approval.process(req1);     
        
    }

    public Boolean validateStepsTowardsSuccessApproval(Opportunity opp) {
      
        ID stepRecordType = Schema.SObjectType.Step_Towards_Success_Partner__c.getRecordTypeInfosByName().get('Gym Partner - Opportunity Step').getRecordTypeId();
        ID recordTypeIdSmall = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Gyms_Small_Medium_Partner').getRecordTypeId();
        ID recordTypeIdWishList = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Gyms_Wishlist_Partner').getRecordTypeId();
        ID recordTypeIdNewFlow = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Gyms_Wishlist_Partner').getRecordTypeId();

        // Set<Integer> wishLst = new Set<Integer>{3,5,6,8,10};        
        //Set<Integer> smLst = new Set<Integer>{1,2,3,4,5};   
        Set<Integer> wishLst = new Set<Integer>{2,5,7,9};        
        Set<Integer> smLst = new Set<Integer>{0,1,2};  
        
        // Integer smIntegration = 4;
        List<Step_Towards_Success_Partner__c> stepLst = [
            SELECT Id, Step_Number__c, Achieved__c
            FROM Step_Towards_Success_Partner__c
            WHERE RecordTypeId =: stepRecordType
            AND Related_Opportunity__c =: opp.Id
        ];
        
        if(opp.RecordTypeId == recordTypeIdSmall){
            for(Integer i : smLst){       
                if(stepLst[i].Achieved__c == 'No') {
                    return false;
                } 
            } 
            return true;        
        } else if(opp.RecordTypeId == recordTypeIdWishList){
            for(Integer i : wishLst){
                if(stepLst[i].Achieved__c == 'No') {
                     return false;
                }                
            }
            return true;
        }
        return false;

    }

    public Boolean isEnablersApprovalNewFlowsValid(Account acc, List<Step_Towards_Success_Partner__c> steps) {
        Set<String> wishLstNewFlow = new Set<String>{'Partner_Volume_Discount', 'Partner_C_Level', 'Partner_JBP', 'Partner_Content'};
        Set<String> smLstNewFlow = new Set<String>{'Partner_Cap_Discount', 'Partner_Visits_Cap', 'Partner_Volume_Discount'};
        
        if(acc.Wishlist__c){
            for(Step_Towards_Success_Partner__c step : steps){       
                if(wishLstNewFlow.contains(step.Metadata_Developer_Name__c) && !step.Achieved_New_Flow__c) {
                    return false;
                }
            }
            return true;        
        } else {
            for(Step_Towards_Success_Partner__c step : steps){       
                if(smLstNewFlow.contains(step.Metadata_Developer_Name__c) && !step.Achieved_New_Flow__c) {
                    return false;
                }
            }
            return true;
        }
    }
}