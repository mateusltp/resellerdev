/**
* @author vinicius.ferraz
* @description PricebookRepository unit test 
*/
@isTest(seeAllData=false)
public class PricebookRepositoryTest {
    
    @TestSetup
    static void createData(){   
        Pricebook2 pb = generatePricebook();
        insert pb;
        
        Pricebook2 standardPricebook = new Pricebook2(
            Id = Test.getStandardPricebookId(),
            IsActive = true
        );
        update standardPricebook;

        Product2 product = generateAccessFeeProduct();
        insert product;
        
        insert generateAccessFeePricebookEntry(pb, product);

        Product2 product500 = new Product2();
        product500.Name = 'Enterprise Subscription';
        product500.Minimum_Number_of_Employees__c = 0;
        product500.Maximum_Number_of_Employees__c = 400;
        product500.Family = 'Enterprise Subscription';
        insert product500;

        insert generateAccessFeePricebookEntry(pb, product500);

        //Reseller TESTE 

        Pricebook2 exclusivePricebook = new Pricebook2( Name = 'Exclusive', Country__c = 'Brazil', Business_Model__c = 'Exclusive', IsActive = true);
        insert exclusivePricebook;

        Pricebook2 resellerPricebook = new Pricebook2( Name = 'Reseller', Country__c = 'Brazil', Business_Model__c = 'Subsidy', IsActive = true);
        insert resellerPricebook;

        Pricebook2 intermediationPricebook = new Pricebook2( Name = 'Intermediation', Country__c = 'Brazil', Business_Model__c = 'Intermediation', IsActive = true);
        insert intermediationPricebook;

        Pricebook2 reseller5kPricebook = new Pricebook2( Name = 'Reseller 5k', Country__c = 'Brazil', Business_Model__c = 'Subsidy 5k', IsActive = true);
        insert reseller5kPricebook;

        Pricebook2 reseller15kPricebook = new Pricebook2( Name = 'Reseller 15k', Country__c = 'Brazil', Business_Model__c = 'Subsidy 15k', IsActive = true);
        insert reseller15kPricebook;

        Product2 productReseller = new Product2();
        productReseller.Name = 'Enterprise Subscription2';
        productReseller.Minimum_Number_of_Employees__c = 0;
        productReseller.Maximum_Number_of_Employees__c = 800;
        productReseller.Family = 'Enterprise Subscription2';
        insert productReseller;

        PricebookEntry pbEntry = new PricebookEntry();
        pbEntry.Product2Id = productReseller.Id;
        pbEntry.Pricebook2Id = Test.getStandardPricebookId();
        pbEntry.UnitPrice = 1200;
        pbEntry.CurrencyISOCode = 'USD';
        insert pbEntry;
        

        Set<Id> pbIds = new Set<Id>{ exclusivePricebook.Id, intermediationPricebook.Id, resellerPricebook.Id, reseller5kPricebook.Id, reseller15kPricebook.Id };
        List<PricebookEntry> customEntry = new List<PricebookEntry>();

        for(Id id : pbIds)
            customEntry.add(new PricebookEntry( Product2Id = productReseller.Id,
                                                Pricebook2Id = id,
                                                UnitPrice = 100,
                                                CurrencyISOCode = 'USD'));

        insert customEntry;

        Id IndirectRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
        
        Account account = new Account();  
        account.Name = 'teste acc';
        account.Legal_Document_Type__c = 'CNPJ';
        account.Id_Company__c = '56947401000108';
        account.Razao_Social__c = 'TESTE';
        account.Website = 'testeaccount.com';
        account.Industry = 'Airlines';
        account.BillingCountry = 'Brazil';
        account.RecordTypeId = IndirectRecordTypeId;    
        insert account;


    }

    @isTest
    static void testById(){
        Pricebook2 pricebook = [select Id,Name from Pricebook2 limit 1];
        PricebookRepository pbs = new PricebookRepository();
        Pricebook2 pb = null;
        try{
            pb = pbs.byId(pricebook.Id);
        }catch(System.QueryException e){
            System.assert(false);
        }
        System.assertEquals(pb.Id,pricebook.Id);
    }

    @isTest
    static void testForCountry(){
        Pricebook2 pricebook = [select Id,Name, Country__c from Pricebook2 where Country__c = 'Brazil' limit 1];
        PricebookRepository pbs = new PricebookRepository();
        Pricebook2 pb = null;
        try{
            pb = pbs.forCountry('Brazil');
        }catch(System.QueryException e){
            System.assert(false);
        }
        System.assertEquals(pb.Id,pricebook.Id);
    }

    @isTest
    static void testForAccessFeeEligibleRange(){
        Pricebook2 pricebook = [select Id,Name, Business_Model__c from Pricebook2 limit 1];
        PricebookRepository pbs = new PricebookRepository();
        PricebookEntry pbEntry = null;
        try{
            pbEntry = pbs.accessFee(pricebook, 1000, false, false, 'USD');
        }catch(System.QueryException e){
            System.assert(true);
        }
        System.assertEquals(pbEntry,null);

        try{
            pbEntry = pbs.accessFee(pricebook, 500, false, false, 'USD');
        }catch(System.QueryException e){
            System.assert(true);
        }
        //System.assertNotEquals(pbEntry.Id,null);
    }

    private static Pricebook2 generatePricebook(){        
        Pricebook2 pb = new Pricebook2();
        pb.Name = 'Br';
        pb.Country__c = 'Brazil';
        pb.IsActive = true;
        return pb;
    }

    private static Product2 generateAccessFeeProduct(){
        Product2 product = new Product2();
        product.Name = 'Enterprise Subscription';
        product.Minimum_Number_of_Employees__c = 0;
        product.Maximum_Number_of_Employees__c = 900;
        product.Family = 'Enterprise Subscription';
        return product;
    }

    private static PricebookEntry generateAccessFeePricebookEntry(Pricebook2 pb, Product2 product){        
        PricebookEntry pbEntry = new PricebookEntry();
        pbEntry.Product2Id = product.Id;
        pbEntry.Pricebook2Id = Test.getStandardPricebookId();
        pbEntry.UnitPrice = 100;
        pbEntry.CurrencyISOCode = 'USD';
        insert pbEntry;
       
        PricebookEntry custompbEntry = new PricebookEntry();
        custompbEntry.Product2Id = product.Id;
        custompbEntry.Pricebook2Id = pb.Id;
        custompbEntry.UnitPrice = 100;
        custompbEntry.CurrencyISOCode = 'USD';
        return custompbEntry;
    }

    @isTest
    public static void testGetIndirectChannelPricebookMap(){

        Test.startTest();
        PricebookRepository rep = new PricebookRepository();
        Map<String, Pricebook2> priceMap = rep.getIndirectChannelPricebookMap();
        System.assertNotEquals(true, priceMap.isEmpty());
        Test.stopTest();
    }

    @isTest
    public static void testRetrievePricebookEntries(){

        Pricebook2 pb = [ SELECT Id FROM Pricebook2 LIMIT 1 ];

        Test.startTest();
        List<PricebookEntry> entry =  PricebookRepository.retrievePricebookEntries(new Set<Id>{pb.Id});
        System.assertNotEquals(false, entry.isEmpty());
        Test.stopTest();

    }

    @isTest
    public static void testGetInstanceByOppId(){

        Account acc = [ SELECT Id FROM Account WHERE Id_Company__c = '56947401000108' LIMIT 1];
        Opportunity newOpp = new Opportunity(
            Name = 'Indirect Channel', 
            StageName = 'Creation',
            Probability = 10,
            AccountId = acc.Id,
            RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Indirect_Channel_New_Business').getRecordTypeId(),
            CloseDate = Date.Today().addDays(90),
            Pricebook2Id = Test.getStandardPricebookId(),
            TotalOpportunityQuantity = 900,
            Quantity_Offer_Number_of_Employees__c= 900,
            B_M__c = 'Total Subsidy',
            CurrencyIsoCode = 'USD',
            OwnerId = UserInfo.getUserId());

            insert newOpp;
        
        Test.startTest();
        PricebookRepository rep = PricebookRepository.getInstanceByOppId(newOpp.Id);
        System.assertNotEquals(null, rep);
        Test.stopTest();
    }

    @isTest
    public static void testGetPricebook(){

        Test.startTest();
        PricebookRepository rep = new PricebookRepository();
        Pricebook2 pb = rep.getPricebook();
        System.assertNotEquals(null, pb);
        Test.stopTest();
    }

    @isTest
    public static void testStandard(){

        Test.startTest();
        PricebookRepository rep = new PricebookRepository();
        Pricebook2 pb = rep.standard('Reseller 15k');
        System.assertNotEquals(null, pb);
        Test.stopTest();
    }

    @isTest
    public static void testGetPricebook2(){

        Test.startTest();
        PricebookRepository rep = new PricebookRepository();
        Pricebook2 pb = rep.getPricebook('Reseller 15k');
        System.assertNotEquals(null, pb);
        Test.stopTest();
    }

    @isTest
    public static void testForNames(){

        Test.startTest();
        PricebookRepository rep = new PricebookRepository();
        List<Pricebook2> pb = rep.forNames(new Set<String>{'Reseller 15k'});
        System.assertNotEquals(null, pb);
        Test.stopTest();
    }

    @isTest
    public static void testSetupFee(){
        Pricebook2 pb = [ SELECT Id FROM Pricebook2 WHERE Name = 'Reseller 15k'];

        Product2 productReseller = new Product2();
        productReseller.Name = 'Setup Fee';
        productReseller.Minimum_Number_of_Employees__c = 0;
        productReseller.Maximum_Number_of_Employees__c = 800;
        productReseller.Family = 'Setup Fee';
        insert productReseller;

        PricebookEntry pbEntry = new PricebookEntry();
        pbEntry.Product2Id = productReseller.Id;
        pbEntry.Pricebook2Id = Test.getStandardPricebookId();
        pbEntry.UnitPrice = 1200;
        pbEntry.CurrencyISOCode = 'USD';
        pbEntry.IsActive = true;
        insert pbEntry;

        PricebookEntry pbEntry2 = new PricebookEntry();
        pbEntry2.Product2Id = productReseller.Id;
        pbEntry2.Pricebook2Id = pb.Id;
        pbEntry2.UnitPrice = 1200;
        pbEntry2.CurrencyISOCode = 'USD';
        pbEntry2.IsActive = true;
        insert pbEntry2;

        Test.startTest();
        PricebookRepository rep = new PricebookRepository();
        PricebookEntry entry = rep.setupFee(pb, null, null, 'USD');
        System.assertNotEquals(null, entry);
        Test.stopTest();

    }

    @isTest
    public static void testProServicesOneFee(){
        Pricebook2 pb = [ SELECT Id FROM Pricebook2 WHERE Name = 'Reseller 15k'];

        Product2 productReseller = new Product2();
        productReseller.Name = 'Professional Services Setup Fee';
        productReseller.Minimum_Number_of_Employees__c = 0;
        productReseller.Maximum_Number_of_Employees__c = 800;
        productReseller.Family = 'Professional Services Setup Fee';
        insert productReseller;

        PricebookEntry pbEntry = new PricebookEntry();
        pbEntry.Product2Id = productReseller.Id;
        pbEntry.Pricebook2Id = Test.getStandardPricebookId();
        pbEntry.UnitPrice = 1200;
        pbEntry.CurrencyISOCode = 'USD';
        pbEntry.IsActive = true;
        insert pbEntry;

        PricebookEntry pbEntry2 = new PricebookEntry();
        pbEntry2.Product2Id = productReseller.Id;
        pbEntry2.Pricebook2Id = pb.Id;
        pbEntry2.UnitPrice = 1200;
        pbEntry2.CurrencyISOCode = 'USD';
        pbEntry2.IsActive = true;
        insert pbEntry2;

        Test.startTest();
        PricebookRepository rep = new PricebookRepository();
        PricebookEntry entry = rep.proServicesOneFee(pb, null, null, 'USD');
        System.assertNotEquals(null, entry);
        Test.stopTest();

    }

    @isTest
    public static void testProServicesMainFee(){
        Pricebook2 pb = [ SELECT Id FROM Pricebook2 WHERE Name = 'Reseller 15k'];

        Product2 productReseller = new Product2();
        productReseller.Name = 'Professional Services Maintenance Fee';
        productReseller.Minimum_Number_of_Employees__c = 0;
        productReseller.Maximum_Number_of_Employees__c = 800;
        productReseller.Family = 'Professional Services Maintenance Fee';
        insert productReseller;

        PricebookEntry pbEntry = new PricebookEntry();
        pbEntry.Product2Id = productReseller.Id;
        pbEntry.Pricebook2Id = Test.getStandardPricebookId();
        pbEntry.UnitPrice = 1200;
        pbEntry.CurrencyISOCode = 'USD';
        pbEntry.IsActive = true;
        insert pbEntry;

        PricebookEntry pbEntry2 = new PricebookEntry();
        pbEntry2.Product2Id = productReseller.Id;
        pbEntry2.Pricebook2Id = pb.Id;
        pbEntry2.UnitPrice = 1200;
        pbEntry2.CurrencyISOCode = 'USD';
        pbEntry2.IsActive = true;
        insert pbEntry2;

        Test.startTest();
        PricebookRepository rep = new PricebookRepository();
        PricebookEntry entry = rep.proServicesMainFee(pb, null, null, 'USD');

        System.assertNotEquals(null, entry);
        Test.stopTest();

    }

    
    @isTest
    public static void testAllPriceBookByPricebook2Id(){
        Pricebook2 pb = [ SELECT Id FROM Pricebook2 WHERE Name = 'Reseller 15k'];

        Product2 productReseller = new Product2();
        productReseller.Name = 'Professional Services Maintenance Fee';
        productReseller.Minimum_Number_of_Employees__c = 0;
        productReseller.Maximum_Number_of_Employees__c = 800;
        productReseller.Family = 'Professional Services Maintenance Fee';
        insert productReseller;

        PricebookEntry pbEntry = new PricebookEntry();
        pbEntry.Product2Id = productReseller.Id;
        pbEntry.Pricebook2Id = Test.getStandardPricebookId();
        pbEntry.UnitPrice = 1200;
        pbEntry.CurrencyISOCode = 'USD';
        pbEntry.IsActive = true;
        insert pbEntry;

        PricebookEntry pbEntry2 = new PricebookEntry();
        pbEntry2.Product2Id = productReseller.Id;
        pbEntry2.Pricebook2Id = pb.Id;
        pbEntry2.UnitPrice = 1200;
        pbEntry2.CurrencyISOCode = 'USD';
        pbEntry2.IsActive = true;
        insert pbEntry2;

        Test.startTest();
        PricebookRepository rep = new PricebookRepository();
        List<PricebookEntry> entry = rep.AllPriceBookByPricebook2Id(pb, null, null,null, 'USD');

        System.assertNotEquals(null, entry);
        Test.stopTest();

    }

}