/**
 * @description       : 
 * @author            : GEPI@GFT.com
 * @group             : 
 * @last modified on  : 04-06-2022
 * @last modified by  : gft.samuel.silva@ext.gympass.com
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   07-09-2020   GEPI@GFT.com   Initial Version
**/
public class ContentDocumentFileTriggerHandler extends TriggerHandler{

    public override void beforeDelete() {
        system.debug('delete');
        new ContentDocumentFileHelper().deleteFileType();
    }

    public override void beforeUpdate() {
        system.debug('::update');
        new ContentDocumentFileHelper().updateFileType();
    }
}