/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 06-28-2022
 * @last modified by  : alysson.mota@gympass.com
 * Modifications Log 
 * Ver   Date         Author                             Modification
 * 1.0   07-22-2021   gft.samuel.silva@ext.gympass.com   Initial Version
**/
public with sharing class ContactService {
    private static final PS_Constants constants = PS_Constants.getInstance();
    private static final String className = ContactService.class.getName();

    @AuraEnabled
    public static void updateEmailAddress ( List<Contact> contactList, String aRecordTypeDevName ){
        try{
            service(aRecordTypeDevName).updateEmailAddress(contactList);       
        } catch (Exception e) {
            throw new ContactServiceException('Error updating email address ' + e.getMessage() );
        }
        
    }

    @AuraEnabled
    public static Id getRecordTypeId ( String aRecordTypeDevName ){
        try{
            return service(aRecordTypeDevName).getRecordTypeId( aRecordTypeDevName );       
        } catch (Exception e) {
            throw new ContactServiceException('Error getting record type id ' + e.getMessage() );
        }
    }

    public void updateAccountLegalRepresentative (List<Contact> contacts) {
       
        try{
            String recordTypeDevName;
            Id recordTypeId = contacts.get(0).recordTypeId;
            if(recordTypeId != null){			
				recordTypeDevName = Schema.SObjectType.Contact.getRecordTypeInfosById().get(recordTypeId).getDeveloperName();
			}
            if(recordTypeDevName == constants.CONTACT_RT_PARTNER_FLOW_CONTACT || recordTypeDevName == constants.CONTACT_RT_GYMS_PARTNER ){
                service(recordTypeDevName).updateAccountLegalRepresentative(contacts);
            }
        }catch (Exception e){
            createLog(e.getMessage(), e.getStackTraceString(), '['+className+'][updateAccountLegalRepresentative]', JSON.serialize(contacts),null);
            if(e.getMessage().contains(constants.ERROR_NO_IMPLEMENTATION_FOR_RT)){
                System.debug(constants.ERROR_NO_IMPLEMENTATION_FOR_RT  );
            } else {
                throw new ContactServiceException(e.getMessage() + ' - ' + e.getLineNumber());
            }
        }
    }

    public void updateAccountLegalRepresentative (List<Contact> contacts, Map<Id, Contact> oldContactsByIds) {
       
        try{
            String recordTypeDevName;
            Id recordTypeId = contacts.get(0).recordTypeId;
            if(recordTypeId != null){			
				recordTypeDevName = Schema.SObjectType.Contact.getRecordTypeInfosById().get(recordTypeId).getDeveloperName();
			}
            if(recordTypeDevName == constants.CONTACT_RT_PARTNER_FLOW_CONTACT || recordTypeDevName == constants.CONTACT_RT_GYMS_PARTNER ){
                service(recordTypeDevName).updateAccountLegalRepresentative(contacts, oldContactsByIds);
            }
        }catch (Exception e){
            createLog(e.getMessage(), e.getStackTraceString(), '['+className+'][updateAccountLegalRepresentative]', JSON.serialize(contacts),null);
            if(e.getMessage().contains(constants.ERROR_NO_IMPLEMENTATION_FOR_RT)){
                System.debug(constants.ERROR_NO_IMPLEMENTATION_FOR_RT );
            } else {
                throw new ContactServiceException(e.getMessage() + ' - ' + e.getLineNumber());
            }
        }
    }

    public void updateRelationshipRole (List<Contact> contacts) {
     
        try{
            String recordTypeDevName;
            Id recordTypeId = contacts.get(0).recordTypeId;
            if(recordTypeId != null){			
				recordTypeDevName = Schema.SObjectType.Contact.getRecordTypeInfosById().get(recordTypeId).getDeveloperName();
			}
            if(recordTypeDevName == constants.CONTACT_RT_PARTNER_FLOW_CONTACT || recordTypeDevName == constants.CONTACT_RT_GYMS_PARTNER ){
                service(recordTypeDevName).updateRelationshipRole(contacts);
            }
        }catch (Exception e){
            createLog(e.getMessage(), e.getStackTraceString(), '['+className+'][updateRelationshipRole]', JSON.serialize(contacts),null);
            if(e.getMessage().contains(constants.ERROR_NO_IMPLEMENTATION_FOR_RT)){
                System.debug(constants.ERROR_NO_IMPLEMENTATION_FOR_RT  );
            } else {
                throw new ContactServiceException(e.getMessage() + ' - ' + e.getLineNumber());
            }
        }
    }

    public void updateRelationshipRole (List<Contact> contacts, Map<Id, Contact> oldContactsByIds) {
        
        try{
            String recordTypeDevName;
            Id recordTypeId = contacts.get(0).recordTypeId;
            if(recordTypeId != null){			
				recordTypeDevName = Schema.SObjectType.Contact.getRecordTypeInfosById().get(recordTypeId).getDeveloperName();
			}
            if(recordTypeDevName == constants.CONTACT_RT_PARTNER_FLOW_CONTACT || recordTypeDevName == constants.CONTACT_RT_GYMS_PARTNER ){
                service(recordTypeDevName).updateRelationshipRole(contacts, oldContactsByIds);
            }
        }catch (Exception e){
            createLog(e.getMessage(), e.getStackTraceString(), '['+className+'][updateRelationshipRole]', JSON.serialize(contacts),null);
            if(e.getMessage().contains(constants.ERROR_NO_IMPLEMENTATION_FOR_RT)){
                System.debug(constants.ERROR_NO_IMPLEMENTATION_FOR_RT );
            } else {
                throw new ContactServiceException(e.getMessage() + ' - ' + e.getLineNumber());
            }
        }
    }

    public static void updateAccountsWithExistingLegalRepresentative (List<Id> accountIds, Id contactId, String recordTypeDevName) {
        try{
            if(recordTypeDevName == constants.CONTACT_RT_PARTNER_FLOW_CONTACT || recordTypeDevName == constants.CONTACT_RT_GYMS_PARTNER ){
                service(recordTypeDevName).updateAccountsWithExistingLegalRepresentative(accountIds, contactId);
            }
        }catch (Exception e){
            DebugLog__c log = new DebugLog__c(
                    Origin__c = '['+className+'][updateAccountsWithExistingLegalRepresentative]',
                    LogType__c = constants.LOGTYPE_ERROR,
                    RelatedToSquad__c = constants.LOGTYPE_SQUAD_PARTNERS,
                    ExceptionMessage__c = e.getMessage(),
                    StackTrace__c = e.getStackTraceString(),
                    DataScope__c = JSON.serialize(accountIds)
            );
            Logger.createLog(log);
            if(e.getMessage().contains(constants.ERROR_NO_IMPLEMENTATION_FOR_RT)){
                System.debug(constants.ERROR_NO_IMPLEMENTATION_FOR_RT + ': ' + recordTypeDevName);
            } else {
                throw new ContactServiceException(e.getMessage() + ' - ' + e.getLineNumber());
            }
        }
    }

    public static void updateAccountsWithNewLegalRepresentative (List<Id> accountIds, String contactSerialized, String recordTypeDevName) {
        try{
            if(recordTypeDevName == constants.CONTACT_RT_PARTNER_FLOW_CONTACT || recordTypeDevName == constants.CONTACT_RT_GYMS_PARTNER ){
                service(recordTypeDevName).updateAccountsWithNewLegalRepresentative(accountIds, contactSerialized);
            }
        }catch (Exception e){
            createLog(e.getMessage(), e.getStackTraceString(), '['+className+'][updateAccountsWithNewLegalRepresentative]', JSON.serialize(accountIds),null);
            if(e.getMessage().contains(constants.ERROR_NO_IMPLEMENTATION_FOR_RT)){
                System.debug(constants.ERROR_NO_IMPLEMENTATION_FOR_RT + ': ' + recordTypeDevName);
            } else {
                throw new ContactServiceException(e.getMessage() + ' - ' + e.getLineNumber());
            }
        }
    }


    private static IContactService service(String recordTypeDevName) {	
        try {
            return (IContactService) Application.ServiceByRecordType.newInstanceByRecordType(getRecordTypeAndSObjectName(recordTypeDevName));
        } catch(Exception e){
            throw new ContactServiceException(e.getMessage());
        }       
    }
    
    private static String getRecordTypeAndSObjectName (String recordTypeDevName){
        return 'Contact.'+recordTypeDevName;
    }
    
    @TestVisible
    private static void createLog(String exceptionMsg, String exceptionStackTrace, String methodName, String dataScope, Id objectId) {
        DebugLog__c log = new DebugLog__c(
                Origin__c = '['+className+']['+methodName+']',
                LogType__c = constants.LOGTYPE_ERROR,
                RelatedToSquad__c = constants.LOGTYPE_SQUAD_PARTNERS,
                ExceptionMessage__c = exceptionMsg,
                StackTrace__c = exceptionStackTrace,
                DataScope__c = dataScope,
                ObjectId__c = objectId
        );
        Logger.createLog(log);
    }

    public class ContactServiceException extends Exception {} 	

}