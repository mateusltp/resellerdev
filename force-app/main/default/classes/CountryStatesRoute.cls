@RestResource(urlMapping='/country-states-cities/*')
global with sharing class CountryStatesRoute {
   
  @HttpGet
  global static CountryStatesResponse get() {
    RestResponse restResponse = new CountryStatesController().get();
    return new CountryStatesResponse(restResponse);
  }

  global class CountryStatesResponse {
    private Integer statusCode;
    private String errorMessage;
    private List<CountryStatesPicklistDTO> countries;

    private CountryStatesResponse(RestResponse restResponse) {
      this.statusCode = restResponse.statusCode;
      if(this.statusCode == 200){
        this.errorMessage = '';
        this.countries = (List<CountryStatesPicklistDTO>) JSON.deserialize(restResponse.responseBody.toString(), List<CountryStatesPicklistDTO>.class);
      }else{
       this.errorMessage = restResponse?.responseBody?.toString();  
      }
    }

    public Integer getStatusCode() {
      return this.statusCode;
    }

    public String getErrorMessage() {
      return this.errorMessage;
    }

  }

}