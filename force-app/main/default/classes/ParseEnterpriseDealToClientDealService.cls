public without sharing class ParseEnterpriseDealToClientDealService {
  private EnterpriseDealBO enterpriseDeal;

  public ParseEnterpriseDealToClientDealService(
    EnterpriseDealBO enterpriseDeal
  ) {
    this.enterpriseDeal = enterpriseDeal;
  }

  public ClientDealBO execute() {
    Opportunity opportunity = enterpriseDeal.getOpportunity();
    Quote quote = enterpriseDeal.getQuote();

    ClientDealBO clientDeal = new ClientDealBO();

    for (
      EnterpriseDealBO.SalesItem salesItem : enterpriseDeal.getSalesItems()
    ) {
      QuoteLineItem quoteLineItem = salesItem.getQuoteLineItem();
      List<Waiver__c> waivers = new List<Waiver__c>();

      for (
        EnterpriseDealBO.PaymentItem paymentItem : salesItem.getPaymentItems()
      ) {
        for (
          Account_Opportunity_Relationship__c splitBilling : enterpriseDeal.getSplitBillings()
        ) {
          Payment__c payment = paymentItem.getPayment().clone(true, true);
          payment.Account__c = splitBilling.Account__c;
          payment.Opportunity__c = opportunity.Id;

          for (Eligibility__c eligibility : payment.Eligibility__r) {
            eligibility.Paid_By__c = splitBilling.Account__c;
            eligibility.Managed_By__c = splitBilling.Account__c;
          }

          List<Eligibility__c> newEligibilities = new List<Eligibility__c>();

          if (opportunity.AccountId != splitBilling.Account__c) {
            payment.Id = null;
            payment.UUID__c = new Uuid().getValue();

            for (Eligibility__c eligibility : payment.Eligibility__r) {
              Eligibility__c newEligibility = eligibility.clone(true, true);
              newEligibility.Id = null;
              newEligibility.UUID__c = new Uuid().getValue();
              newEligibilities.add(newEligibility);
            }
          }

          if (quoteLineItem.Fee_Type__c.equals('Enterprise Subscription')) {
            opportunity.Payment_Due_Days__c = getPaymentDueDays(payment);
            opportunity.Billing_Period__c = payment.Frequency__c;
            opportunity.Billing_Day__c = getBillingDay(payment);
            opportunity.Cutoff_Day__c = getBillingDay(payment);

            payment.Percentage__c = splitBilling.Billing_Percentage__c;
            payment.RecordTypeId = Schema.SObjectType.Payment__c
              .getRecordTypeInfosByDeveloperName()
              .get('Recurring')
              .getRecordTypeId();

            if (newEligibilities.isEmpty()) {
              clientDeal.addPaymentItem(payment, payment.Eligibility__r);
            } else {
              clientDeal.addPaymentItem(payment, newEligibilities);
            }
          }

          if (quoteLineItem.Fee_Type__c.equals('Setup Fee')) {
            payment.Percentage__c = splitBilling.Setup_Fee_Billing_Percentage__c;
            payment.RecordTypeId = Schema.SObjectType.Payment__c
              .getRecordTypeInfosByDeveloperName()
              .get('Non_Recurring')
              .getRecordTypeId();

            if (newEligibilities.isEmpty()) {
              clientDeal.addPaymentItem(payment, payment.Eligibility__r);
            } else {
              clientDeal.addPaymentItem(payment, newEligibilities);
            }
          }
        }
        for (Waiver__c waiver : paymentItem.getWaivers()) {
          waiver.Quote_Line_Item__c = quoteLineItem.Id;
          waivers.add(waiver);
        }
      }
      clientDeal.addSaleItem(quoteLineItem, waivers);
    }

    opportunity.RecordTypeId = getRecordTypeId(opportunity);

    clientDeal.setOpportunity(opportunity);
    clientDeal.setQuote(quote);

    return clientDeal;
  }

  private Double getPaymentDueDays(Payment__c payment) {
    switch on payment.Payment_Due_Days__c {
      when '10 days' {
        return 10;
      }
      when '15 days' {
        return 15;
      }
      when '30 days' {
        return 30;
      }
      when '60 days' {
        return 60;
      }
      when 'Custom' {
        return payment.Custom_Payment_Due_Days__c;
      }
      when else {
        return null;
      }
    }
  }

  private Double getBillingDay(Payment__c payment) {
    switch on payment.Billing_Day__c {
      when '01' {
        return 1;
      }
      when '05' {
        return 5;
      }
      when '15' {
        return 15;
      }
      when 'Custom' {
        return payment.Custom_Billing_Day__c;
      }
      when else {
        return null;
      }
    }
  }

  private Id getRecordTypeId(Opportunity opportunity) {
    switch on opportunity.RecordType.DeveloperName {
      when 'Client_Sales_New_Business' {
        return Schema.SObjectType.Opportunity
          .getRecordTypeInfosByDeveloperName()
          .get('Client_Sales_SKU_New_Business')
          .getRecordTypeId();
      }
      when 'Client_Success_Renegotiation' {
        return Schema.SObjectType.Opportunity
          .getRecordTypeInfosByDeveloperName()
          .get('Client_Success_SKU_Renegotiation')
          .getRecordTypeId();
      }
      when 'SMB_New_Business' {
        return Schema.SObjectType.Opportunity
          .getRecordTypeInfosByDeveloperName()
          .get('SMB_SKU_New_Business')
          .getRecordTypeId();
      }
      when 'SMB_Success_Renegotiation' {
        return Schema.SObjectType.Opportunity
          .getRecordTypeInfosByDeveloperName()
          .get('SMB_Success_SKU_Renegotiation')
          .getRecordTypeId();
      }
      when else {
        return null;
      }
    }
  }
}