public with sharing class SMB_ApprovalInfo {
    
    public SMB_ApprovalInfo(){}
    
     @InvocableMethod
    public static List<FlowOutput> getApprovalInfo( List<String> aQuoteIds ){
        
        Quote flowQuote =  getQuote( aQuoteIds.get(0) )[0];
    	List<FlowOutput> flowOutputList = new List<FlowOutput>();
        List<String> DealDeskApprovalList = new List<String>();
        String DealDeskApproval;
        List<String> CommercialApprovalList = new List<String>();
        String CommercialApproval; 
        
        //String teste = '[{"isApproved":true,"conditionCode":"CustomPayFrequency","approvalCondition":"Payment frequency is custom"},{"isApproved":true,"conditionCode":"NoAutonomousMarketPlace","approvalCondition":"Autonomous Market Place is not defined for your Offer"},{"isApproved":true,"conditionCode":"CustomPayMethod","approvalCondition":"Eligibility/MembershipFee with Custom payment settings or Credit Card method"}]';
        List<DealDeskApprovalCondition> DealDeskConditionsList = new List<DealDeskApprovalCondition>();
        if(string.isNotBlank(flowQuote.Deal_Desk_Approved_Conditions__c))
        DealDeskConditionsList = (List<DealDeskApprovalCondition>)JSON.deserialize(flowQuote.Deal_Desk_Approved_Conditions__c /*teste*/, List<DealDeskApprovalCondition>.class);
        
        
        
        if(flowQuote.Waiver_Approval_Info__c == 'Pending Approval'){
            CommercialApprovalList.add('Waivers need approval');
        }
        if(flowQuote.Discount_Approval_Info__c == 'Pending Approval'){
            CommercialApprovalList.add('Discount out of range');
        }
        if(DealDeskConditionsList.size()>0){
            for(DealDeskApprovalCondition ddCondition : DealDeskConditionsList){
                DealDeskApprovalList.add(ddCondition.conditionCode);
            }
        }
        
        DealDeskApproval = String.join(DealDeskApprovalList, ',');
        CommercialApproval = String.join(CommercialApprovalList, ','); 
        
        system.debug('DealDeskApproval '+DealDeskApproval);
        system.debug('CommercialApproval '+CommercialApproval);
        
        FlowOutput fOutput = new FlowOutput();
        fOutput.listOfCommercialApprovals = CommercialApproval;
        fOutput.listOfDealDeskApprovals = DealDeskApproval;
        flowOutputList.add(fOutput);
        return flowOutputList;
    }
    
    private static List<Quote> getQuote(Id aQuoteId){

        List<Quote> lineItems = [SELECT Id, Deal_Desk_Approved_Conditions__c, Waiver_Approval_Info__c, Discount_Approval_Info__c
                                        FROM Quote 
                                        WHERE Id = : aQuoteId];
        return lineItems;
    }
    
    public class FlowOutput{
    @InvocableVariable()
    public String listOfDealDeskApprovals;
   	@InvocableVariable()
    public String listOfCommercialApprovals;
}
    
    public class DealDeskApprovalCondition{
        public Boolean isApproved {get; set;}
        public String conditionCode {get; set;}
        public String approvalCondition {get; set;}
    }

}