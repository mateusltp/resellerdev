public class UpdateOpportunityActivityBatch implements Database.Batchable<sObject>, Database.Stateful, Schedulable {
    
    public Integer recordsProcessed = 0;
    
    public static String CRON_EXP = '0 0 2 * * ?';
    
    public Database.QueryLocator start(Database.BatchableContext bc) {
        
        String recordType = 'SMB - SKU New Business';
        String ownerName = '%Integration%';
        String offerSent = 'Offer Sent';
        String offerCreation = 'Offer Creation';
        
        return Database.getQueryLocator(
            'SELECT Id, DaysWithoutActivity__c, LastActivityDate ' +
            'FROM Opportunity ' +
            'WHERE RecordType.Name =: recordType ' +
            'AND LastActivityDate != Null ' +
            'AND (NOT Owner.Name LIKE : ownerName) ' + 
            'AND (FastTrackStage__c = : offerCreation OR FastTrackStage__c = : offerSent) '
        );
    }
    
    public void execute(Database.BatchableContext bc, List<Opportunity> scope){
        try{
            for(Opportunity opp : scope){
                Integer dt = (Date.today()).daysBetween(Date.valueOf(opp.LastActivityDate));
                opp.DaysWithoutActivity__c = dt;
                recordsProcessed = recordsProcessed + 1;
            }
            update scope;
        }
        catch(Exception e){
            System.debug('An exception occurred: ' + e.getMessage());
        }  
    }
    
    public void finish(Database.BatchableContext bc){
        System.debug(recordsProcessed + ' records processed!');
    }
    
    public void execute(SchedulableContext sc){
        UpdateOpportunityActivityBatch uoab = new UpdateOpportunityActivityBatch();
        Database.executeBatch(uoab);
    }
}