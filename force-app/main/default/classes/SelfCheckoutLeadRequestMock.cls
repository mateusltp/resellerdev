@IsTest
public class SelfCheckoutLeadRequestMock {
    
    public static SelfCheckoutLeadRequest getMock() {
        String requestJson = '{' +
            '"client": {' +
                '"client_id": "aa47c899-7e59-47ad-9ad7-7d0f6310f4ea",'+
                '"trade_name": "EEEEE",'+
                '"legal_name": "Macejkovic, Lang and Huel",'+
                '"lead_source": "inbound",'+
                '"lead_subsource_sf": "self-checkout",'+
                '"legal_document_type": "CNPJ",'+
                '"legal_document": "98.755.321/0001-09",'+
                '"phone": "(74) 9335-4814",'+
                '"billing_firstname":"Gene",'+
                '"billing_lastname":"Mosciski",'+
                '"billing_email": "aaaaa@example.com",'+
                '"business_unit": "SMB",'+
                '"currency_id": "BRL",'+
                '"billing_id": "123456",'+
                '"number_of_employees": 10,'+
                '"number_of_employees_rounded": "20",'+
                '"Language": "br",'+
                '"funnel_stage": "company_info",'+
                '"UTM_campaign": "One",'+
                '"UTM_source": "Two",'+
                '"utm_url": "Three",'+
                '"utm_medium": "Four",'+
                '"utm_content": "Five",'+
                '"utm_term": "Six",'+
                '"total_value_w_discount": "1",'+
                '"total_value": "300",'+
                '"sales_flow": "New",'+
                '"sku_selected": "SETUP_SERVICE",'+
                '"contact": {'+
                    '"first_name": "Gene",'+
                    '"last_name": "Mosciski",'+
                    '"email": "eeeee@example.com",'+
                    '"phone": "+55 (42) 1930-7525",'+
                    '"job_title": "CTO",'+
                    '"contact_id": "c0a117bb-f1f7-4aff-829b-82c11c9270a3"'+
                '},'+
                '"address": {'+
                    '"street": "Barros Avenida",'+
                    '"state": "Bahia",'+
                    '"postal_code": "63795-087",'+
                    '"country_id": "BR",'+
                    '"city": "Moraesdo Descoberto",'+
                    '"district": "aaaa",' +
                    '"complements": "AAAA"' +
                '}'+
            '}'+
        '}';
        return (SelfCheckoutLeadRequest) JSON.deserialize(requestJson, SelfCheckoutLeadRequest.class);
    }

    public static SelfCheckoutLeadRequest getMockWithLeadEmpty() {
        String requestJson = '{"client": {}}';
    
        return (SelfCheckoutLeadRequest) JSON.deserialize(requestJson, SelfCheckoutLeadRequest.class);
    }

    public static SelfCheckoutLeadRequest getMockWithContactEmpty() {
        String requestJson = '{' +
            '"client": {' +
                '"client_id": "aa47c899-7e59-47ad-9ad7-7d0f6310f4ea",'+
                '"trade_name": "EEEEE",'+
                '"legal_name": "Macejkovic, Lang and Huel",'+
                '"lead_source": "inbound",'+
                '"lead_subsource_sf": "self-checkout",'+
                '"legal_document_type": "CNPJ",'+
                '"legal_document": "98.755.321/0001-09",'+
                '"phone": "(74) 9335-4814",'+
                '"billing_firstname":"Gene",'+
                '"billing_lastname":"Mosciski",'+
                '"billing_email": "aaaaa@example.com",'+
                '"business_unit": "SMB",'+
                '"currency_id": "BRL",'+
                '"billing_id": "123456",'+
                '"number_of_employees": 10,'+
                '"number_of_employees_rounded": "20",'+
                '"Language": "br",'+
                '"funnel_stage": "company_info",'+
                '"UTM_campaign": "One",'+
                '"UTM_source": "Two",'+
                '"utm_url": "Three",'+
                '"utm_medium": "Four",'+
                '"utm_content": "Five",'+
                '"utm_term": "Six",'+
                '"total_value_w_discount": "1",'+
                '"total_value": "300",'+
                '"sales_flow": "New",'+
                '"sku_selected": "GYMPASS_CORE; FAMILY_MEMBER",'+
                '"contact": {'+
                    '"phone": "+55 (42) 1930-7525",'+
                    '"job_title": "CTO"'+
                '},'+
                '"address": {'+
                    '"street": "Barros Avenida",'+
                    '"state": "Bahia",'+
                    '"postal_code": "63795-087",'+
                    '"country_id": "BR",'+
                    '"city": "Moraesdo Descoberto",'+
                    '"district": "aaaa",' +
                    '"complements": "AAAA"' +
                '}'+
            '}'+
        '}';

        return (SelfCheckoutLeadRequest) JSON.deserialize(requestJson, SelfCheckoutLeadRequest.class);
    }

    public static SelfCheckoutLeadRequest getMockUpdatedLead(){
        Lead lead = new Lead();
        lead.Email = 'eeeee@example.com';
        lead.LastName = 'Teste';
        lead.Company = 'Teste';
        lead.Country = 'Brazil';
        lead.Status = 'Waiting for Qualification';
        lead.UTM_Campaign__c = 'One';
        lead.UTM_Source__c = 'One';
        lead.UTM_Medium__c = 'One';
        lead.UTM_URL__c = 'One';
        lead.UTM_Content__c = 'One';
        lead.UTM_Term__c = 'One';
        INSERT lead;
        
        String requestJson = '{' +
            '"client": {' +
                '"client_id": "aa47c899-7e59-47ad-9ad7-7d0f6310f4ea",'+
                '"trade_name": "EEEEE",'+
                '"legal_name": "Macejkovic, Lang and Huel",'+
                '"lead_source": "inbound",'+
                '"lead_subsource_sf": "self-checkout",'+
                '"legal_document_type": "CNPJ",'+
                '"legal_document": "98.755.321/0001-09",'+
                '"phone": "(74) 9335-4814",'+
                '"billing_firstname":"Gene",'+
                '"billing_lastname":"Mosciski",'+
                '"billing_email": "aaaaa@example.com",'+
                '"business_unit": "SMB",'+
                '"currency_id": "BRL",'+
                '"billing_id": "123456",'+
                '"number_of_employees": 10,'+
                '"number_of_employees_rounded": "20",'+
                '"Language": "br",'+
                '"funnel_stage": "company_info",'+
                '"UTM_campaign": null,'+
                '"UTM_source": null,'+
                '"utm_url": null,'+
                '"utm_medium": null,'+
                '"utm_content": null,'+
                '"utm_term": null,'+
                '"total_value_w_discount": "1",'+
                '"total_value": "300",'+
                '"sales_flow": "New",'+
                '"sku_selected": "SETUP_SERVICE",'+
                '"contact": {'+
                    '"first_name": "Gene",'+
                    '"last_name": "Mosciski",'+
                    '"email": "eeeee@example.com",'+
                    '"phone": "+55 (42) 1930-7525",'+
                    '"job_title": "CTO",'+
                    '"contact_id": "c0a117bb-f1f7-4aff-829b-82c11c9270a3"'+
                '},'+
                '"address": {'+
                    '"street": "Barros Avenida",'+
                    '"state": "Bahia",'+
                    '"postal_code": "63795-087",'+
                    '"country_id": "BR",'+
                    '"city": "Moraesdo Descoberto",'+
                    '"district": "aaaa",' +
                    '"complements": "AAAA"' +
                '}'+
            '}'+
        '}';
    
        return (SelfCheckoutLeadRequest) JSON.deserialize(requestJson, SelfCheckoutLeadRequest.class);
    }

    public static SelfCheckoutLeadRequest getMockWithCountryEmpty() {
        String requestJson = '{' +
            '"client": {' +
                '"client_id": "aa47c899-7e59-47ad-9ad7-7d0f6310f4ea",'+
                '"trade_name": "EEEEE",'+
                '"legal_name": "Macejkovic, Lang and Huel",'+
                '"lead_source": "inbound",'+
                '"lead_subsource_sf": "self-checkout",'+
                '"legal_document_type": "CNPJ",'+
                '"legal_document": "98.755.321/0001-09",'+
                '"phone": "(74) 9335-4814",'+
                '"billing_firstname":"Gene",'+
                '"billing_lastname":"Mosciski",'+
                '"billing_email": "aaaaa@example.com",'+
                '"business_unit": "SMB",'+
                '"currency_id": "BRL",'+
                '"billing_id": "123456",'+
                '"number_of_employees": 10,'+
                '"number_of_employees_rounded": "20",'+
                '"Language": "br",'+
                '"funnel_stage": "company_info",'+
                '"UTM_campaign": "One",'+
                '"UTM_source": "Two",'+
                '"utm_url": "Three",'+
                '"utm_medium": "Four",'+
                '"utm_content": "Five",'+
                '"utm_term": "Six",'+
                '"total_value_w_discount": "1",'+
                '"total_value": "300",'+
                '"sales_flow": "New",'+
                '"sku_selected": "GYMPASS_CORE; FAMILY_MEMBER",'+
                '"contact": {'+
                    '"first_name": "Gene",'+
                    '"last_name": "Mosciski",'+
                    '"email": "eeeee@example.com",'+
                    '"phone": "+55 (42) 1930-7525",'+
                    '"job_title": "CTO",'+
                    '"contact_id": "c0a117bb-f1f7-4aff-829b-82c11c9270a3"'+
                '},'+
                '"address": {'+
                    '"street": "Barros Avenida",'+
                    '"state": "Bahia",'+
                    '"postal_code": "63795-087",'+
                    '"city": "Moraesdo Descoberto",'+
                    '"district": "aaaa",' +
                    '"complements": "AAAA"' +
                '}'+
            '}'+
        '}';
        return (SelfCheckoutLeadRequest) JSON.deserialize(requestJson, SelfCheckoutLeadRequest.class);
    }

    public static SelfCheckoutLeadRequest getMockUTMBiggerThanMaxValue(){
        String nameCampaign = 'Test';

        Lead lead = new Lead();
        lead.Email = 'eeeee@example.com';
        lead.LastName = 'Teste';
        lead.Company = 'Teste';
        lead.Country = 'Brazil';
        lead.Status = 'Waiting for Qualification';
        lead.UTM_Campaign__c = nameCampaign.repeat(32750);

        INSERT lead;
        
        String requestJson = '{' +
            '"client": {' +
                '"client_id": "aa47c899-7e59-47ad-9ad7-7d0f6310f4ea",'+
                '"trade_name": "EEEEE",'+
                '"legal_name": "Macejkovic, Lang and Huel",'+
                '"lead_source": "inbound",'+
                '"lead_subsource_sf": "self-checkout",'+
                '"legal_document_type": "CNPJ",'+
                '"legal_document": "98.755.321/0001-09",'+
                '"phone": "(74) 9335-4814",'+
                '"billing_firstname":"Gene",'+
                '"billing_lastname":"Mosciski",'+
                '"billing_email": "aaaaa@example.com",'+
                '"business_unit": "SMB",'+
                '"currency_id": "BRL",'+
                '"billing_id": "123456",'+
                '"number_of_employees": 10,'+
                '"number_of_employees_rounded": "20",'+
                '"Language": "br",'+
                '"funnel_stage": "company_info",'+
                '"UTM_campaign": "One",'+
                '"UTM_source": "Two",'+
                '"utm_url": "Three",'+
                '"utm_medium": "Four",'+
                '"utm_content": "Five",'+
                '"utm_term": "Six",'+
                '"total_value_w_discount": "1",'+
                '"total_value": "300",'+
                '"sales_flow": "New",'+
                '"sku_selected": "GYMPASS_CORE; FAMILY_MEMBER",'+
                '"contact": {'+
                    '"first_name": "Gene",'+
                    '"last_name": "Mosciski",'+
                    '"email": "eeeee@example.com",'+
                    '"phone": "+55 (42) 1930-7525",'+
                    '"job_title": "CTO",'+
                    '"contact_id": "c0a117bb-f1f7-4aff-829b-82c11c9270a3"'+
                '},'+
                '"address": {'+
                    '"street": "Barros Avenida",'+
                    '"state": "Bahia",'+
                    '"postal_code": "63795-087",'+
                    '"country_id": "BR",'+
                    '"city": "Moraesdo Descoberto",'+
                    '"district": "aaaa",' +
                    '"complements": "AAAA"' +
                '}'+
            '}'+
        '}';
    
        return (SelfCheckoutLeadRequest) JSON.deserialize(requestJson, SelfCheckoutLeadRequest.class);
    }

}