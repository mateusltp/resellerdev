public without sharing class SelfTerminationController {
    private RestRequest request;
    private RestResponse response;
  
    private List<BadRequest> badRequests;
  
    public SelfTerminationController() {
      this.request = RestContext.request;
      this.response = RestContext.response;
      this.badRequests = new List<BadRequest>();
    }
  
    public RestResponse post() {
     // Savepoint savepoint = Database.setSavepoint();
  
      try {
        SelfTerminationRequest selfTerminationRequest = getRequest();
  
        new SelfTerminationRequestValidator(selfTerminationRequest)
          .validatePostRequest();
  
        if (selfTerminationRequest.getAction() == 'Cancel') {
          new SelfTerminationPublisher(selfTerminationRequest).runCancel();
        } else if (selfTerminationRequest.getAction() == 'Undo') {
          new SelfTerminationPublisher(selfTerminationRequest).runUndo();
        }
  
        response.statusCode = 204;
      } catch (JSONException error) {
        response.statusCode = 400;
        BadRequest badRequest = new BadRequest();
        badRequest.status_code = 'JSON_EXCEPTION';
        badRequest.messages.add('The payload is in the wrong format');
        badRequests.add(badRequest);
      } catch (QueryException error) {
        response.statusCode = 404;
        BadRequest badRequest = new BadRequest();
        badRequest.status_code = 'QUERY EXCEPTION';
        badRequest.messages = new List<String>{ error.getMessage() };
        badRequests.add(badRequest);
      } catch (HandledException error) {
        response.statusCode = 409;
        BadRequest badRequest = new BadRequest();
        badRequest.status_code = 'HANDLE EXCEPTION';
        badRequest.messages = new List<String>{ error.getMessage() };
        badRequests.add(badRequest);
      } catch (IntegrationException error) {
        response.statusCode = 400;
        BadRequest badRequest = new BadRequest();
        badRequest.status_code = 'INTEGRATION_EXCEPTION';
        badRequest.messages = IntegrationException.getMessages();
        badRequests.add(badRequest);
      }
  
      if (!badRequests.isEmpty()) {
      //  Database.rollback(savepoint);
        response.responseBody = Blob.valueOf(JSON.serialize(badRequests));
        String requestBodyJson = this.request?.requestBody?.toString();
        SelfTerminationRequest requestSelfTermination = (SelfTerminationRequest) JSON.deserialize(
          requestBodyJson,
          SelfTerminationRequest.class);
        new SelfTerminationPublisher(
          requestSelfTermination,
            JSON.serialize(badRequests)
          )
          .runCancel();
      }
  
      return response;
    }
  
    private SelfTerminationRequest getRequest() {
      String requestBodyJson = this.request?.requestBody?.toString();
  
      if (requestBodyJson == null) {
        return null;
      }
  
      return (SelfTerminationRequest) JSON.deserialize(
        requestBodyJson,
        SelfTerminationRequest.class
      );
    }
  
    private class BadRequest {
      private List<String> messages;
      private String status_code;
  
      private BadRequest() {
        this.messages = new List<String>();
      }
    }
  }