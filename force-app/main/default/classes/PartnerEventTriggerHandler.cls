/**
 * @description       : 
 * @author            : bruno.mendes@gympass.com
 * @group             : 
 * @last modified on  : 25/05/2022
 * @last modified by  : bruno.mendes@gympass.com
**/

public without sharing class PartnerEventTriggerHandler extends TriggerHandler {
    public override void afterInsert() {
        PartnerEventTriggerHelper.handleEvents(Trigger.new);
    }
}