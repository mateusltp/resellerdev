/**
 * @description       : 
 * @author            : bruno.mendes@gympass.com
 * @group             : 
 * @last modified on  : 18/05/2022
 * @last modified by  : bruno.mendes@gympass.com
**/

@IsTest
private class NetworkBuilderApprovalControllerTest {

    @TestSetup
    static void setup() {
        PS_Constants constants = PS_Constants.getInstance();
        ID accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(constants.ACCOUNT_RT_PARTNER_FLOW_ACCOUNT).getRecordTypeId();

        Account account = PartnerDataFactory.newAccount();
        account.RecordTypeId = accRecordTypeId;
        account.Approval_Status_Exclusivity__c = null;
        insert account;

    }

    @IsTest
    static void sendAccountsForExclusivityApproval_Sent_EmptyStatus() {
        Account account = [SELECT Id, Approval_Status_Exclusivity__c FROM Account LIMIT 1];
        Test.startTest();

        List<AccountPartnerServiceImpl.Response> responses = NetworkBuilderApprovalController.sendAccountsForExclusivityApproval(new List<Id>{account.Id}, 'test comments');

        Test.stopTest();

        // error message is empty meaning that the approval was successfully processed. Access to index 0 means we have data in the response.
        System.assertEquals(null, responses.get(0).errorMessage);
    }

    @IsTest
    static void sendAccountsForExclusivityApproval_Sent_RejectedStatus() {
        Account account = [SELECT Id, Approval_Status_Exclusivity__c FROM Account LIMIT 1];
        account.Approval_Status_Exclusivity__c = 'Rejected';
        update account;

        Test.startTest();

        List<AccountPartnerServiceImpl.Response> responses = NetworkBuilderApprovalController.sendAccountsForExclusivityApproval(new List<Id>{account.Id}, 'test comments');

        Test.stopTest();

        // error message is empty meaning that the approval was successfully processed. Access to index 0 means we have data in the response.
        System.assertEquals(null, responses.get(0).errorMessage);
    }

    @IsTest
    static void sendAccountsForExclusivityApproval_NotSent_PendingStatus() {
        PS_Constants constants = PS_Constants.getInstance();

        Account account = [SELECT Id, Approval_Status_Exclusivity__c FROM Account LIMIT 1];
        account.Approval_Status_Exclusivity__c = 'Pending';
        update account;

        Test.startTest();

        List<AccountPartnerServiceImpl.Response> responses = NetworkBuilderApprovalController.sendAccountsForExclusivityApproval(new List<Id>{account.Id}, 'test comments');

        Test.stopTest();

        // error message with value meaning that the approval was not processed. Access to index 0 means we have data in the response.
        System.assertEquals(constants.ACCOUNT_APPROVED_OR_PENDING, responses.get(0).errorMessage);
    }

    @IsTest
    static void sendAccountsForExclusivityApproval_NotSent_ApprovedStatus() {
        PS_Constants constants = PS_Constants.getInstance();

        Account account = [SELECT Id, Approval_Status_Exclusivity__c FROM Account LIMIT 1];
        account.Approval_Status_Exclusivity__c = 'Approved';
        update account;


        Test.startTest();

        List<AccountPartnerServiceImpl.Response> responses = NetworkBuilderApprovalController.sendAccountsForExclusivityApproval(new List<Id>{account.Id}, 'test comments');

        Test.stopTest();

        // error message meaning that the account is either approved or pending, and was not send for processing. Access to index 0 means we have data in the response.
        System.assertEquals(constants.ACCOUNT_APPROVED_OR_PENDING, responses.get(0).errorMessage);
    }

    @IsTest
    static void sendAccountsForExclusivityApproval_NotSent_DifferentOwner() {
        PS_Constants constants = PS_Constants.getInstance();

        Profile profileGyms = [
                SELECT Id, Name
                FROM Profile
                WHERE Name = 'Gyms'
        ].get(0);

        User testUser = new User();
        testUser.Username = 'user@gympass.com111';
        testUser.LastName = 'Gympass';
        testUser.Email = 'user@gympass.com111';
        testUser.Alias = 'user';
        testUser.TimeZoneSidKey = 'America/Sao_Paulo';
        testUser.LocaleSidKey = 'en_US';
        testUser.EmailEncodingKey = 'ISO-8859-1';
        testUser.ProfileId = profileGyms.Id;
        testUser.LanguageLocaleKey = 'en_US';
        insert testUser;

        Account account = [SELECT Id FROM Account LIMIT 1];



        System.runAs(testUser) {
            List<AccountPartnerServiceImpl.Response> responses = NetworkBuilderApprovalController.sendAccountsForExclusivityApproval(new List<Id>{account.Id}, 'test comments');
            // error message meaning that the user is sending an account for approval that he is not owner or manager, not sent for approval. Access to index 0 means we have data in the response.
            System.assertEquals(constants.ACCOUNT_NOT_SENT_FOR_APPROVAL, responses.get(0).errorMessage);
        }
    }

    // teste com id invalido para apanhar a exception
    @IsTest
    static void sendAccountsForExclusivityApproval_Exception() {
        try {
            Id opportunityId = fflib_IDGenerator.generate(Opportunity.SObjectType);
            List<AccountPartnerServiceImpl.Response> responses = NetworkBuilderApprovalController.sendAccountsForExclusivityApproval(new List<Id>{opportunityId}, 'test comments');
        } catch (Exception e) {
            system.debug('## exception: '+e.getMessage());
        }
    }
}