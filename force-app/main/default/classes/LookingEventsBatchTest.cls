@isTest
public class LookingEventsBatchTest { 
    
    @isTest
    public static void LookingEventsBatchTest() {
        
         Massive_Account_Request__c massReq = new Massive_Account_Request__c(
            name='test_loadMassiveAccountRequest',
            Status__c=ResellerEnumRepository.MassiveAccountRequestStatus.LookingForEvents.name()   
        );
  
        insert massReq;
        
        Account account = new Account();
        account.Name = 'Account';
        account.BillingCountry = 'Brazil';
        account.BillingState = 'São Paulo';
        account.Id_Company__c = '82.898.430/0001-37';
        insert account;
        
        Account_Request__c request = new Account_Request__c();  
        request.Name = 'teste7';
        request.Unique_Identifier_Type__c = 'CNPJ';
        request.Unique_Identifier__c = '73054510000115';
        request.Billing_Country__c = 'Brazil';
        request.Massive_Account_Request__c = massReq.Id;
        request.AccountId__c = account.Id;
        request.Bulk_Operation__c = true;
        request.Massive_Status__c = ResellerEnumRepository.MassiveAccountRequestStatus.LookingForEvents.name();
        insert request;
        
        Test.startTest();	
         	 LookingEventsBatch bte = new LookingEventsBatch();
             bte.massiveId = massreq.Id;
             ID idEvent = DataBase.executeBatch(bte);
        Test.stopTest();
        
       System.assertEquals(massReq.Status__c, 'LookingForEvents'); 
    }
    
     @isTest
    public static void RunEngineBatchTest() {
         Massive_Account_Request__c massReq2 = new Massive_Account_Request__c(
            name='test_loadMassiveAccountRequest2',
            Status__c=ResellerEnumRepository.MassiveAccountRequestStatus.RunEngine.name()   
        );
  
        insert massReq2;
        
        Account account2 = new Account();
        account2.Name = 'Account';
        account2.BillingCountry = 'Brazil';
        account2.BillingState = 'São Paulo';
        account2.Id_Company__c = '56.071.923/0001-99';
        insert account2;
        
        Account_Request__c request2 = new Account_Request__c();  
        request2.Name = 'teste7';
        request2.Unique_Identifier_Type__c = 'CNPJ';
        request2.Unique_Identifier__c = '56071923000199';
        request2.Billing_Country__c = 'Brazil';
        request2.Massive_Account_Request__c = massReq2.Id;
        request2.AccountId__c = account2.Id;
        request2.Bulk_Operation__c = true;
        request2.Massive_Status__c = ResellerEnumRepository.MassiveAccountRequestStatus.RunEngine.name();
        insert request2;

        Test.startTest();
         	RunEngineBatch bt = new RunEngineBatch();
            bt.massiveId = massreq2.Id;
            ID idOpp = Database.executeBatch(bt);
        Test.stopTest();
       
        System.assertEquals(massReq2.Status__c, 'RunEngine');

    }
    
}