@istest
public class CaseAssignmentHelperTest {
	@istest
    public static void CaseAssignmentHelper_assignCases(){
        
        Account lAcc = DataFactory.newAccount();
        Database.insert( lAcc );
        
        Pricebook2 lStandardPB = DataFactory.newPricebook();
        
        Opportunity lOpp = DataFactory.newOpportunity( lAcc.Id , lStandardPB , 'Client_Sales_New_Business');
        Database.insert( lOpp );

        List<Case> casesForTest = new List<Case>();
        Case caso = new Case();
        caso.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('RTContractReview').getRecordTypeId();
        //caso.AccountId ='0014100001AwoIdAAJ';
        //caso.ContractAgreement__c = 'a3e6C000000R7EeQAK';
        //caso.Document__c = 'a406C0000007lOaQAI';
        caso.Status='Created';
        caso.OpportunityId__c = lOpp.Id;
        

       	casesForTest.add(caso);
       
        insert casesForTest;
        
        Test.startTest();
        Database.DMLOptions prevDMLOptions = casesForTest[0].getOptions();
        
        CaseAssignmentHelper.assignCases(casesForTest);
        System.assert(prevDMLOptions != casesForTest[0].getOptions());
        
        Test.stopTest();
    }
}