/**
 * @description       : 
 * @author            : bruno.mendes@gympass.com
 * @group             : 
 * @last modified on  : 28/04/2022
 * @last modified by  : bruno.mendes@gympass.com
**/
@IsTest
private class OpsSetupFormSelectorTest {

    @TestSetup
    static void setup() {
        Account account = PartnerDataFactory.newAccount();
        insert account;

        Opportunity opportunity = PartnerDataFactory.newOpportunity(account.Id, 'Partner_Flow_Opportunity');
        insert opportunity;

        Ops_Setup_Validation_Form__c opsSetupValidationForm = PartnerDataFactory.newOpsSetupValidationForm(opportunity, account.Id, 'Partner_Ops_Setup_Validation');
        insert opsSetupValidationForm;
    }

    @isTest
    static void getSObjectFieldList_Test(){

        List<Schema.SObjectField> schSObjLst = new List<Schema.SObjectField> {
                Ops_Setup_Validation_Form__c.Id,
                Ops_Setup_Validation_Form__c.Name
        };

        Test.startTest();
        System.assertEquals(schSObjLst, new OpsSetupFormSelector().getSObjectFieldList());
        Test.stopTest();
    }

    @isTest
    static void getSObjectType_Test(){

        Test.startTest();
        System.assertEquals(Ops_Setup_Validation_Form__c.sObjectType, new OpsSetupFormSelector().getSObjectType());
        Test.stopTest();
    }

    @isTest
    static void selectById_Test(){

        List<Ops_Setup_Validation_Form__c> opsSetupValidationForms = [SELECT Id FROM Ops_Setup_Validation_Form__c LIMIT 1];

        Test.startTest();
        system.assertEquals(opsSetupValidationForms[0].Id, new OpsSetupFormSelector().selectById(new Set<Id> {opsSetupValidationForms[0].Id}).get(0).id);
        Test.stopTest();
    }

    @IsTest
    static void selectOpsSetupFormForPartner_Test() {
        Id accountId = [SELECT Id FROM Account LIMIT 1].Id;
        Id opportunityId = [SELECT Id FROM Opportunity LIMIT 1].Id;
        Id oppSetupValidationFormId = [SELECT Id FROM Ops_Setup_Validation_Form__c LIMIT 1].Id;

        Test.startTest();

        System.assertEquals(oppSetupValidationFormId, new OpsSetupFormSelector().selectOpsSetupFormForPartner(opportunityId, new Set<Id> {accountId}).values()[0].Id);

        Test.stopTest();
    }

}