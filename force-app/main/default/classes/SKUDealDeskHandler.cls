/**
 * @description       : 
 * @author            : roei@gft.com
 * @group             : 
 * @last modified on  : 06-08-2022
 * @last modified by  : roei@gft.com
**/
public without sharing class SKUDealDeskHandler {
    public SKUDealDeskHandler(){}

    private static final String gSTANDARD_AUTONOMOUS_MARKETPLACE = 'Yes';
    private String gSTANDARD_PAY_FREQUENCY = 'Yearly';
    private Map< String , String > gMapCodeDealDeskCondition = new Map< String , String >{
        'CustomPayFrequency' => 'Payment frequency is custom',
        'NoAutonomousMarketPlace' => 'Autonomous Market Place is not defined for your Offer',
        'CustomPayMethod' => 'Eligibility/MembershipFee with Custom payment settings or Credit Card method'
    };
    private Integer gIndexDealDeskCondition;
    private Boolean gIsStandardDealDeskCondition = false;

    //Opportunity
    public void checkDealDeskFieldsApproved( Map< Id , Opportunity > aMapOldOpp , List< Opportunity > aLstNewOpp ){
        Set< String > gSetOppRecordtypeId = new Set< String >{
            Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_SKU_New_Business').getRecordTypeId(),
            Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Success_SKU_Renegotiation').getRecordTypeId(),
            Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SMB_SKU_New_Business').getRecordTypeId(),
            Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SMB_Success_SKU_Renegotiation').getRecordTypeId()
        };

        List< Opportunity > lLstOppChanged = new List < Opportunity >();

        for( Opportunity iOpp : aLstNewOpp ){
            if( !gSetOppRecordtypeId.contains( iOpp.RecordTypeId ) ){ continue; }

            if( aMapOldOpp.get( iOpp.Id ).Billing_Period__c != iOpp.Billing_Period__c ){
                lLstOppChanged.add( iOpp );
            }
        }

        checkDealDeskConditions( lLstOppChanged );
    }
    
    public void checkDealDeskConditions( List< Opportunity > lLstOppChanged ){
        if( lLstOppChanged.isEmpty() ){ return; }

        List< String > lLstOppId = new List< String >();
        List< String > lLstQuoteId = new List< String >();
        
        for( Opportunity iOpp : lLstOppChanged ){ 
            lLstOppId.add( iOpp.Id ); 
            lLstQuoteId.add( iOpp.SyncedQuoteId );
        }

        Map< String , Map< String , Assert_Data__c > > lMapOppIdMapFieldNameAssertData = loadMapOppAssertData( lLstOppId );
        Map< String , Map< String , Integer > > lMapObjFieldMapTermQualified = getTermsQualifiedMap();

        List< Quote > lLstQuoteToUpdate = new List< Quote >();
        
        Map< String , List< DealDeskConditions > > lMapQuoteIdDealDeskApprovedConditions = getMapQuoteIdDealDeskApprovedConditions( lLstQuoteId );

        for( Opportunity iOpp : lLstOppChanged ){
            Quote lQuote = new Quote();
            lQuote.Id = iOpp.SyncedQuoteId;

            List< DealDeskConditions > lLstDealDeskApprovedConditions = lMapQuoteIdDealDeskApprovedConditions.get( lQuote.Id );

            DealDeskConditions lFrequencyCondition = 
                getDealDeskConditionByCode( 'CustomPayFrequency' , lLstDealDeskApprovedConditions ); 

            if( lFrequencyCondition == null ){
                lFrequencyCondition = new DealDeskConditions( 'CustomPayFrequency' , gMapCodeDealDeskCondition.get( 'CustomPayFrequency' ) , false );
                lFrequencyCondition.isApproved = 
                    isFrequencyApproved( lMapOppIdMapFieldNameAssertData.get( iOpp.Id ) , iOpp.Billing_Period__c , lMapObjFieldMapTermQualified );

                if( gIsStandardDealDeskCondition == false ){
                    lLstDealDeskApprovedConditions.add( lFrequencyCondition );
                }
            } else {
                lFrequencyCondition.isApproved = 
                    isFrequencyApproved( lMapOppIdMapFieldNameAssertData.get( iOpp.Id ) , iOpp.Billing_Period__c , lMapObjFieldMapTermQualified );

                if( gIsStandardDealDeskCondition == true && gIndexDealDeskCondition != null ){
                    lLstDealDeskApprovedConditions.remove( gIndexDealDeskCondition );
                }
            }

            lQuote.Deal_Desk_Approved_Conditions__c = JSON.serialize( lLstDealDeskApprovedConditions );
            
            lLstQuoteToUpdate.add( lQuote );
        }

        Database.update( lLstQuoteToUpdate );
    }

    public DealDeskConditions getDealDeskConditionByCode( String aDealDeskConditionCode , List< DealDeskConditions > aLstDealDeskConditions ){
        DealDeskConditions lCondition;

        for( Integer i = 0 ; i < aLstDealDeskConditions.size() ; i++ ){
            if( aLstDealDeskConditions[i].conditionCode == aDealDeskConditionCode ){
                lCondition = aLstDealDeskConditions[i];
                gIndexDealDeskCondition = i;
                break;
            }
        }

        return lCondition;
    }

    public Boolean isFrequencyApproved( Map< String , Assert_Data__c > gMapFieldNameAssertData , String aPaymentFrequency,
            Map< String , Map< String , Integer > > aMapObjFieldMapTermQualified ) {
        if( aPaymentFrequency == null || gSTANDARD_PAY_FREQUENCY.contains( aPaymentFrequency ) ){ 
            gIsStandardDealDeskCondition = true;
            return true; 
        }

        Assert_Data__c lPaymentFrequency = gMapFieldNameAssertData?.get( 'billing_period__c' );

        String lPayFrequencyApproved = lPaymentFrequency?.Old_Value__c;

        return !String.isBlank( lPayFrequencyApproved ) &&
            aMapObjFieldMapTermQualified.get( 'opportunity.billing_period__c' ).get( lPayFrequencyApproved ) 
            >= aMapObjFieldMapTermQualified.get( 'opportunity.billing_period__c' ).get( aPaymentFrequency );
    }

    public Map< String , Map< String , Assert_Data__c > > loadMapOppAssertData( List< String > aLstOppId ){
        Map< String , Map< String , Assert_Data__c > > lMapOppIdMapFieldNameAssertData = getMapAssertData( aLstOppId , 'opportunity' );

        return lMapOppIdMapFieldNameAssertData;
    }

    public Map< String , Map< String , Integer > > getTermsQualifiedMap(){
        Map< String , Map< String , Integer > > lMapObjFieldMapTermQualified = new Map< String , Map< String , Integer > >();

        for( Approval_Revamp_Terms_Qualified__mdt iTermsQualifiedMdt : 
            [ SELECT Id, Object_Name__c, Field_Developer_Name__c, Field_Values__c
              FROM Approval_Revamp_Terms_Qualified__mdt ] ){
            
            Map< String , Integer > lMapFieldValueQualification = new Map< String , Integer >();
            List< String > lFieldValues = iTermsQualifiedMdt.Field_Values__c.split(';');

            for( String iFieldValue : lFieldValues ){
                List< String > lFieldValueAndPosition = iFieldValue.split('->');
                
                lMapFieldValueQualification.put( lFieldValueAndPosition[0] , Integer.valueOf( lFieldValueAndPosition[1] ) );
            }

            lMapObjFieldMapTermQualified.put( 
                iTermsQualifiedMdt.Object_Name__c + '.' + iTermsQualifiedMdt.Field_Developer_Name__c , 
                lMapFieldValueQualification );
        }

        return lMapObjFieldMapTermQualified;
    }

    public Map< String , List< DealDeskConditions > > getMapQuoteIdDealDeskApprovedConditions( List< String > aLstQuoteId ){
        Map< String , List< DealDeskConditions > > lMapQuoteIdApprovedConditions = new Map< String , List< DealDeskConditions > >();

        for( Quote iQuote : [ SELECT Id, Deal_Desk_Approved_Conditions__c FROM Quote WHERE Id =: aLstQuoteId ]){
            List< DealDeskConditions > lLstDealDeskApprovedConditions = 
                String.isBlank( iQuote.Deal_Desk_Approved_Conditions__c ) || iQuote.Deal_Desk_Approved_Conditions__c == '[]' ? 
                    new List< DealDeskConditions >{ new DealDeskConditions( 'CustomPayFrequency' , gMapCodeDealDeskCondition.get( 'CustomPayFrequency' ) , false ) } :
                    (List< DealDeskConditions >)JSON.deserialize( iQuote.Deal_Desk_Approved_Conditions__c , List< DealDeskConditions >.class );

            lMapQuoteIdApprovedConditions.put( iQuote.Id , lLstDealDeskApprovedConditions );
        }

        return lMapQuoteIdApprovedConditions;
    }

    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------

    //Quote
    public void checkDealDeskFieldsApproved( Map< Id , Quote > aMapIdOldQuote , List< Quote > aLstNewQuote ){
        Set< String > gSetQuoteRecordtypeId = new Set< String >{
            Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Client_Sales_SKU_New_Business').getRecordTypeId(),
            Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Client_Success_SKU_Renegotiation').getRecordTypeId(),
            Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('SMB_SKU_New_Business').getRecordTypeId(),
            Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('SMB_Success_SKU_Renegotiation').getRecordTypeId()
        };

        List< Quote > lLstQuoteChanged = new List < Quote >();

        for( Quote iQuote : aLstNewQuote ){
            if( !gSetQuoteRecordtypeId.contains( iQuote.RecordTypeId ) ){ continue; }

            if( aMapIdOldQuote.get( iQuote.Id ).autonomous_marketplace_contract__c != iQuote.autonomous_marketplace_contract__c ){
                lLstQuoteChanged.add( iQuote ); 
            }
        }

        checkDealDeskConditions( lLstQuoteChanged );
    }

    public void checkDealDeskConditions( List< Quote > aLstQuoteChanged ){
        if( aLstQuoteChanged.isEmpty() ){ return; }

        Map< String , Map< String , Assert_Data__c > > lMapOppIdMapFieldNameAssertData = loadMapOppAssertData( aLstQuoteChanged );

        Map< String , List< DealDeskConditions > > lMapQuoteIdDealDeskApprovedConditions = getMapQuoteIdDealDeskApprovedConditions( aLstQuoteChanged , 'NoAutonomousMarketPlace' );

        for( Quote iQuote : aLstQuoteChanged ){
            List< DealDeskConditions > lLstDealDeskApprovedConditions = lMapQuoteIdDealDeskApprovedConditions.get( iQuote.Id );

            DealDeskConditions lAutonomousMarketCondition = 
                getDealDeskConditionByCode( 'NoAutonomousMarketPlace' , lLstDealDeskApprovedConditions ); 

            if( lAutonomousMarketCondition == null ){
                lAutonomousMarketCondition = new DealDeskConditions( 'NoAutonomousMarketPlace' , gMapCodeDealDeskCondition.get( 'NoAutonomousMarketPlace' ) , false );
                lAutonomousMarketCondition.isApproved = 
                    autoMarketApproved( lMapOppIdMapFieldNameAssertData.get( iQuote.OpportunityId ) , iQuote.autonomous_marketplace_contract__c );

                if( gIsStandardDealDeskCondition == false ){
                    lLstDealDeskApprovedConditions.add( lAutonomousMarketCondition );
                }
            } else {
                lAutonomousMarketCondition.isApproved = 
                    autoMarketApproved( lMapOppIdMapFieldNameAssertData.get( iQuote.OpportunityId ) , iQuote.autonomous_marketplace_contract__c );

                if( gIsStandardDealDeskCondition == true && gIndexDealDeskCondition != null ){
                    lLstDealDeskApprovedConditions.remove( gIndexDealDeskCondition );
                }
            }

            iQuote.Deal_Desk_Approved_Conditions__c = JSON.serialize( lLstDealDeskApprovedConditions );
        }
    }

    public Map< String , Map< String , Assert_Data__c > > loadMapOppAssertData( List< Quote > aLstQuoteChanged ){
        List< String > lLstOppId = new List< String >();
        
        for( Quote iQuote : aLstQuoteChanged ){ lLstOppId.add( iQuote.OpportunityId ); }

        Map< String , Map< String , Assert_Data__c > > lMapOppIdMapFieldNameAssertData = getMapAssertData( lLstOppId , 'quote' );

        return lMapOppIdMapFieldNameAssertData;
    }

    private Map< String , Map< String , Assert_Data__c > > getMapAssertData( List< String > aLstOppId , String aObjName ){
        Map< String , Map< String , Assert_Data__c > > lMapOppIdMapFieldNameAssertData = new Map< String , Map< String , Assert_Data__c > >();

        for( Assert_Data__c iAssertData : 
            [ SELECT Id, Object_Name__c, Field_Name__c, Fee_Type__c, Old_Value__c, Approved_Value__c, New_Value__c, Pre_Approved_Condition__c, Opportunity__c
              FROM Assert_Data__c WHERE Opportunity__c =: aLstOppId AND Object_Name__c =: aObjName ] ){
            
            Map< String , Assert_Data__c > lMapFieldNameAssertData = 
                lMapOppIdMapFieldNameAssertData.get( iAssertData.Opportunity__c );

            if( lMapFieldNameAssertData == null ){
                lMapFieldNameAssertData = new Map< String , Assert_Data__c >();
            }

            lMapFieldNameAssertData.put( iAssertData.Field_Name__c , iAssertData );

            lMapOppIdMapFieldNameAssertData.put( iAssertData.Opportunity__c , lMapFieldNameAssertData );
        }

        return lMapOppIdMapFieldNameAssertData;
    }

    public Boolean autoMarketApproved( Map< String , Assert_Data__c > gMapFieldNameAssertData , String aAutonomousMarketPlace ) {
        if( aAutonomousMarketPlace == null || aAutonomousMarketPlace == gSTANDARD_AUTONOMOUS_MARKETPLACE ){ 
            gIsStandardDealDeskCondition = true;
            return true; 
        }

        Assert_Data__c lAutoMarketPlace = gMapFieldNameAssertData?.get( 'autonomous_marketplace_contract__c' );

        String lAutonomousMarketPlaceApproved = lAutoMarketPlace?.Old_Value__c;

        return !String.isBlank( lAutonomousMarketPlaceApproved ) &&
            aAutonomousMarketPlace == lAutonomousMarketPlaceApproved;
    }

    public Map< String , List< DealDeskConditions > > getMapQuoteIdDealDeskApprovedConditions( List< Quote > aLstQuote , String aConditionCode ){
        Map< String , List< DealDeskConditions > > lMapQuoteIdApprovedConditions = new Map< String , List< DealDeskConditions > >();

        for( Quote iQuote : aLstQuote ){
            List< DealDeskConditions > lLstDealDeskApprovedConditions = 
                String.isBlank( iQuote.Deal_Desk_Approved_Conditions__c ) || iQuote.Deal_Desk_Approved_Conditions__c == '[]' ?
                    new List< DealDeskConditions >{ new DealDeskConditions( aConditionCode , gMapCodeDealDeskCondition.get( aConditionCode ) , false ) } :
                    (List< DealDeskConditions >)JSON.deserialize( iQuote.Deal_Desk_Approved_Conditions__c , List< DealDeskConditions >.class );

            lMapQuoteIdApprovedConditions.put( iQuote.Id , lLstDealDeskApprovedConditions );
        }

        return lMapQuoteIdApprovedConditions;
    }

    //----------------------------------------------------------------------------------------------------------------------------------------

    //Eligibility
    public void checkDealDeskFieldsApproved( Map< Id , Eligibility__c > aMapIdOldEligibility , List< Eligibility__c > aLstNewEligibility ){
        //definir melhor o filtro dessa classe

        List< Eligibility__c > lLstEligibilityChanged = new List < Eligibility__c >();

        for( Eligibility__c iEligibility : aLstNewEligibility ){
            if( aMapIdOldEligibility.get( iEligibility.Id ).Payment_Method__c != iEligibility.Payment_Method__c ){
                lLstEligibilityChanged.add( iEligibility ); 
            }
        }

        checkDealDeskConditions( lLstEligibilityChanged );
    }

    public void checkDealDeskConditions( List< Eligibility__c > aLstEligibilityChanged ){
        if( aLstEligibilityChanged.isEmpty() ){ return; }

        Map< String , Map< String , Assert_Data__c > > lMapOppIdMapFieldNameAssertData = loadMapOppAssertData( aLstEligibilityChanged );

        Map< String , String > lMapOppIdPayMethod = getMapOppIdPayMethod( aLstEligibilityChanged );

        Map< String , Quote > lMapOppIdQuote = getMapOppIdQuote( lMapOppIdPayMethod.keySet() );

        List< Quote > lLstQuoteToUpdate = new List< Quote >();

        Map< String , List< DealDeskConditions > > lMapQuoteIdDealDeskApprovedConditions = getMapQuoteIdDealDeskApprovedConditions( lMapOppIdQuote.values() , 'CustomPayMethod' );

        for( String iOppId : lMapOppIdQuote.keySet() ){
            Quote lQuote = lMapOppIdQuote.get( iOppId );

            List< DealDeskConditions > lLstDealDeskApprovedConditions = lMapQuoteIdDealDeskApprovedConditions.get( lQuote.Id );

            DealDeskConditions lPayMethodCondition = 
                getDealDeskConditionByCode( 'CustomPayMethod' , lLstDealDeskApprovedConditions ); 

            if( lPayMethodCondition == null ){
                lPayMethodCondition = new DealDeskConditions( 'CustomPayMethod' , gMapCodeDealDeskCondition.get( 'CustomPayMethod' ) , false );
                lPayMethodCondition.isApproved = 
                    payMethodApproved( lMapOppIdMapFieldNameAssertData.get( iOppId ) , lMapOppIdPayMethod.get( iOppId ) );

                if( gIsStandardDealDeskCondition == false ){
                    lLstDealDeskApprovedConditions.add( lPayMethodCondition );
                }
            } else {
                lPayMethodCondition.isApproved = 
                    payMethodApproved( lMapOppIdMapFieldNameAssertData.get( iOppId ) , lMapOppIdPayMethod.get( iOppId ) );

                if( gIsStandardDealDeskCondition == true && gIndexDealDeskCondition != null ){
                    lLstDealDeskApprovedConditions.remove( gIndexDealDeskCondition );
                }
            }
            
            lQuote.Deal_Desk_Approved_Conditions__c = JSON.serialize( lLstDealDeskApprovedConditions );

            lLstQuoteToUpdate.add( lQuote );
        }

        Database.update( lLstQuoteToUpdate );
    }

    public Boolean payMethodApproved( Map< String , Assert_Data__c > gMapFieldNameAssertData , String aPayMethod ) {
        if( aPayMethod == null || aPayMethod != 'Credit Card' ){ 
            gIsStandardDealDeskCondition = true;
            return true; 
        }

        Assert_Data__c lPaymentMethod = gMapFieldNameAssertData?.get( 'payment_method__c' );

        String lPaymentMethodApproved = lPaymentMethod?.Old_Value__c;

        return !String.isBlank( lPaymentMethodApproved ) &&
            aPayMethod == lPaymentMethodApproved;
    }

    public Map< String , Map< String , Assert_Data__c > > loadMapOppAssertData( List< Eligibility__c > aLstEligibilityChanged ){
        Set< String > lSetOppId = new Set< String >();
        
        for( Eligibility__c iEligibility : aLstEligibilityChanged ){ lSetOppId.add( iEligibility.Opportunity_Id__c ); }

        Map< String , Map< String , Assert_Data__c > > lMapOppIdMapFieldNameAssertData = getMapAssertData( lSetOppId , 'eligibility__c' );

        return lMapOppIdMapFieldNameAssertData;
    }

    private Map< String , Map< String , Assert_Data__c > > getMapAssertData( Set< String > aSetOppId , String aObjName ){
        Map< String , Map< String , Assert_Data__c > > lMapOppIdMapFieldNameAssertData = new Map< String , Map< String , Assert_Data__c > >();

        for( Assert_Data__c iAssertData : 
            [ SELECT Id, Object_Name__c, Field_Name__c, Fee_Type__c, Old_Value__c, Approved_Value__c, New_Value__c, Pre_Approved_Condition__c, Opportunity__c
              FROM Assert_Data__c WHERE Opportunity__c =: aSetOppId AND Object_Name__c =: aObjName ] ){
            
            Map< String , Assert_Data__c > lMapFieldNameAssertData = 
                lMapOppIdMapFieldNameAssertData.get( iAssertData.Opportunity__c );

            if( lMapFieldNameAssertData == null ){
                lMapFieldNameAssertData = new Map< String , Assert_Data__c >();
            }

            lMapFieldNameAssertData.put( iAssertData.Field_Name__c , iAssertData );

            lMapOppIdMapFieldNameAssertData.put( iAssertData.Opportunity__c , lMapFieldNameAssertData );
        }

        return lMapOppIdMapFieldNameAssertData;
    }

    public Map< String , Quote > getMapOppIdQuote( Set< String > aSetOppId ){
        Map< String , Quote > lMapOppIdQuote = new Map< String , Quote >();

        for( Quote iQuote : [ SELECT Id, OpportunityId, Deal_Desk_Approved_Conditions__c FROM Quote WHERE OpportunityId =: aSetOppId ] ){
            lMapOppIdQuote.put( iQuote.OpportunityId , iQuote );
        }

        return lMapOppIdQuote;
    }

    public Map< String , String > getMapOppIdPayMethod( List< Eligibility__c > aLstEligibilityChanged ){
        Map< String , String > lMapOppIdPayMethod = new Map< String , String >();

        for( Eligibility__c iEligibility : aLstEligibilityChanged ){
            lMapOppIdPayMethod.put( iEligibility.Opportunity_Id__c , iEligibility.Payment_Method__c );
        }

        return lMapOppIdPayMethod;
    }

    public class DealDeskConditions {
        public String conditionCode {get; set;}
        public String approvalCondition {get; set;}
        public Boolean isApproved {get; set;}

        public DealDeskConditions( String aConditionCode , String aApprovalCondition , Boolean aIsApproved ){
            conditionCode = aConditionCode;
            approvalCondition = aApprovalCondition;
            isApproved = aIsApproved;
        }
    }

    //----------------------------------------------------------------------------------------------------------------------------------------

    //Case
    public void updateAssertDataWhenCaseApproved( Map< Id , Case > aMapIdOldCase , List< Case > aLstNewCase ){
        Set< String > lSetCaseRecordtypeId = new Set< String >{
            Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Deal_Desk_Approval').getRecordTypeId()
        };

        Set< String > lSetOppRecordtypeName = new Set< String >{
            'Client Sales - SKU New Business',
            'Client Success - SKU Renegotiation',
            'SMB - SKU New Business',
            'SMB Success - SKU Renegotiation'
        };

        List< Case > lLstClosedCases = new List < Case >();

        for( Case iCase : aLstNewCase ){
            if( lSetCaseRecordtypeId.contains( iCase.RecordTypeId ) &&
                lSetOppRecordtypeName.contains( iCase.Opportunity_Record_Type__c ) &&
                aMapIdOldCase.get( iCase.Id ).Status != iCase.Status && ( iCase.Status == 'Approved' || iCase.Status == 'Rejected' ) ){
                lLstClosedCases.add( iCase ); 
            }
        }
        
        updateAssertData( lLstClosedCases );
    }

    public void updateAssertData( List< Case > aLstClosedCases ){
        if( aLstClosedCases.isEmpty() ){ return; }
        
        List< String > lLstCaseId = new List< String >();

        for( Case iCase : aLstClosedCases ){ lLstCaseId.add( iCase.Id ); }

        List< Approval_Item__c > lLstCaseApprovedItems = getCaseApprovedItems( lLstCaseId );
        
        if( lLstCaseApprovedItems.isEmpty() ){ return; }
        
        Map< String , Assert_Data__c > lMapDealDeskConditionAssertData = getMapConditionAssertData( lLstCaseApprovedItems );
        
        List< Assert_Data__c > lLstAssertData = new List< Assert_Data__c >();

        for( Approval_Item__c iApprovalItem : lLstCaseApprovedItems ){
            Assert_Data__c lAssertData = lMapDealDeskConditionAssertData.get( iApprovalItem.Name );
            if( lAssertData == null ){ continue; }

            lAssertData.Old_Value__c = iApprovalItem.Value__c;

            if( String.isBlank( lAssertData.Opportunity__c ) ){ 
                lAssertData.Opportunity__c = iApprovalItem.Case__r.OpportunityId__c; 
            }
         
            lLstAssertData.add( lAssertData );
        }

        if( lLstAssertData.isEmpty() ){ return; }
        
        Database.upsert( lLstAssertData );
    }

    public Map< String , Assert_Data__c > getMapConditionAssertData( List< Approval_Item__c > aLstApprovalItem ){
        Map< String , Assert_Data__c > lMapDealDeskConditionAssertData = new Map< String , Assert_Data__c >{
            'Payment frequency is custom' => new Assert_Data__c( Object_Name__c = 'opportunity' , Field_Name__c = 'billing_period__c' ),
            'Autonomous Market Place is not defined for your Offer' => new Assert_Data__c( Object_Name__c = 'quote' , Field_Name__c = 'autonomous_marketplace_contract__c' ),
            'Eligibility/MembershipFee with Custom payment settings or Credit Card method' => new Assert_Data__c( Object_Name__c = 'eligibility__c' , Field_Name__c = 'payment_method__c' )
        };

        List< String > lLstAssertDataToRetrieve = 
            new List< String >{ 'opportunity_billing_period__c' , 'quote_autonomous_marketplace_contract__c' , 'eligibility__c_payment_method__c' };

        Map< String , String > lMapAssertDataFielCondition = new Map< String , String >{
            'opportunity_billing_period__c' => 'Payment frequency is custom',
            'quote_autonomous_marketplace_contract__c' => 'Autonomous Market Place is not defined for your Offer' ,
            'eligibility__c_payment_method__c' => 'Eligibility/MembershipFee with Custom payment settings or Credit Card method'
        };

        List< String > lLstOppId = new List< String >();
            
        for( Approval_Item__c iApprovalItem : aLstApprovalItem ){
            lLstOppId.add( iApprovalItem.Case__r.OpportunityId__c );
        }

        for( Assert_Data__c iAssertData : [ SELECT Id, Full_Path_Data__c, Object_Name__c, Field_Name__c, Opportunity__c FROM Assert_Data__c WHERE Opportunity__c =: lLstOppId AND Full_Path_Data__c =: lLstAssertDataToRetrieve ]){
            lMapDealDeskConditionAssertData.put( lMapAssertDataFielCondition.get( iAssertData.Full_Path_Data__c ) , iAssertData );
        }

        return lMapDealDeskConditionAssertData;
    }

    public List< Approval_Item__c > getCaseApprovedItems( List< String > aLstCaseId ){
        List< Approval_Item__c > lLstApprovalItems = [ SELECT Name, Value__c, Case__r.OpportunityId__c FROM Approval_Item__c WHERE Case__c =: aLstCaseId AND Approval_Status__c = 'Approved' ];

        return lLstApprovalItems;
    }
}