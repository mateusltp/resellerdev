public without sharing class DocumentTriggerHandler extends TriggerHandler{

    

    public override void afterUpdate(){ 
         /* Used to keep a clone from contract agreement until Conga app be removed */
         TriggerHandler.bypass('ContractAgreementHandler');
         new CloneObject().CloneDataFromDocument(Trigger.new);
    }
}