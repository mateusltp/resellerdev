public class GymActivityTriggerHandler extends triggerhandler {
    
    public override void beforeInsert() {
        System.debug('B4Insert');
        
    }
    
    public override void afterInsert(){
        System.debug('AfterInsert');
        new GymActivityTriggerHelper().afterInsertMethod(); 
        
    }

    
    public override void AfterDelete(){
        System.debug('AfterDelete');
        new GymActivityTriggerHelper().afterdeleteMethod(); 
    }
    
}