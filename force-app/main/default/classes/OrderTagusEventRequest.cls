public without sharing class OrderTagusEventRequest {
  private Order order;
  private List<ItemRequest> items;

  public OrderTagusEventRequest(Order order) {
    this.order = order;
    this.items = new List<ItemRequest>();
  }

  public Order getOrder() {
    return this.order;
  }

  public List<ItemRequest> getItems() {
    return this.items;
  }

  public void addItem(OrderItem orderItem, List<Payment__c> payments) {
    this.items.add(new ItemRequest(orderItem, payments));
  }

  public class ItemRequest {
    OrderItem item;
    private List<Payment__c> payments;

    private ItemRequest(OrderItem item, List<Payment__c> payments) {
      this.item = item;
      this.payments = payments;
    }

    public OrderItem getItem() {
      return this.item;
    }

    public List<Payment__c> getPayments() {
      return this.payments;
    }
  }
}