/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 05-11-2022
 * @last modified by  : gft.samuel.silva@ext.gympass.com
 * Modifications Log 
 * Ver   Date         Author                             Modification
 * 1.0   07-20-2021   gft.samuel.silva@ext.gympass.com   Initial Version
**/
public with sharing class ProductItemSelector extends ApplicationSelector {
   
    public List<Schema.SObjectField> getSObjectFieldList() {
		return new List<Schema.SObjectField> {
			Product_Item__c.Id,
			Product_Item__c.Name
		};
	}

    public Schema.SObjectType getSObjectType() {
		return Product_Item__c.sObjectType;
	}

    public List<Product_Item__c> selectById(Set<Id> ids) {
		return (List<Product_Item__c>) super.selectSObjectsById(ids);
	}

    public List<Product_Item__c> selectByIdToClone( Set<Id> Ids ) {
	
        return (List<Product_Item__c>) Database.query(
									newQueryFactory().
										selectFields(getAllFieldsFromProductItem()).
										setCondition('Id IN: Ids' ).
										toSOQL());
	}
    
    /** tags: clone, cloning, to clone, allFields
     * toClone means all fields are retrieving
    */
    public List<Product_Item__c> selectByOppIdToClone( Set<Id> aOppIdSet ) {
	
        return (List<Product_Item__c>) Database.query(
									newQueryFactory().
										selectFields(getAllFieldsFromProductItem()).
										setCondition('Opportunity__c IN: aOppIdSet' ).
										toSOQL());
	}

    public List<Product_Item__c> selectAllByParentId( Set<Id> parentIds ) {

        return (List<Product_Item__c>) Database.query(
                newQueryFactory().
                        selectFields(getAllFieldsFromProductItem()).
                        setCondition('Parent_Product__c IN: parentIds' ).
                        toSOQL());
    }

	
    public List<Product_Item__c> selectProductByID(Set<Id> ids) {
        return new List<Product_Item__c>( (List<Product_Item__c>)Database.query(
                                        newQueryFactory().
                                        selectField(Product_Item__c.Id).
                                        selectField(Product_Item__c.CurrencyIsoCode).
                                        selectField(Product_Item__c.Auto_Product_Assignment__c).
                                        selectField(Product_Item__c.Package_Type__c).
                                        selectField(Product_Item__c.PR_Activity__c).
                                        selectField(Product_Item__c.Type__c).
                                        selectField(Product_Item__c.Max_Monthly_Visit_Per_User__c).
                                        selectField(Product_Item__c.Price_Visits_Month_Package_Selected__c).
                                        selectField(Product_Item__c.Late_Cancellation_Percent__c).
                                        selectField(Product_Item__c.No_Show_Fee_Percent__c).
                                        selectField(Product_Item__c.Parent_Product__c).
                                        selectField(Product_Item__c.RecordTypeId).
                                        setCondition('Id in: ids').
                                        toSOQL()
                                        ));									
    }

    public List<Product_Item__c> selectProductByParentID(Set<Id> ids) {
        return new List<Product_Item__c>( (List<Product_Item__c>)Database.query(
                                        newQueryFactory().
                                        selectField(Product_Item__c.Id).
                                        selectField(Product_Item__c.CurrencyIsoCode).
                                        selectField(Product_Item__c.No_Show_Fee_Percent__c).
                                        selectField(Product_Item__c.Parent_Product__c).
                                        setCondition('Parent_Product__c in: ids').
                                        toSOQL()
                                        ));									
    }

    public List<Product_Item__c> selectProductForSetUp(Id oppId) {
        return new List<Product_Item__c>( (List<Product_Item__c>)Database.query(
                                        newQueryFactory().
                                        selectField(Product_Item__c.Id).
                                        selectField(Product_Item__c.Opportunity__c).
                                        setCondition('Opportunity__c =: oppId').
                                        toSOQL()
                                        ));									
    }

    public Map<Id, Product_Item__c> selectProductByIdAsMap(Set<Id> ids) {
        Map<Id, Product_Item__c> idToProdItem = new Map<Id, Product_Item__c>();

        List<Product_Item__c> prodItemLst = selectProductByID(ids);

        for (Product_Item__c prodItem : prodItemLst) {
            idToProdItem.put(prodItem.Id, prodItem);
        }

        return idToProdItem;
    }

    public Map<Id, Product_Item__c> selectProductByIdToClone ( Set<Id> prodItemIds ) {
	
		return new Map<Id, Product_Item__c> ( ( List<Product_Item__c> ) Database.query(
									newQueryFactory().
										selectFields( getAllFieldsFromProductItem() ).
										setCondition('Id in : prodItemIds').
										toSOQL()
									));
	}

    private Set<String> getAllFieldsFromProductItem(){
		Map<String, Schema.SObjectField> productFields = Schema.getGlobalDescribe().get('Product_Item__c').getDescribe().fields.getMap();
        return productFields.keySet();
    }
}