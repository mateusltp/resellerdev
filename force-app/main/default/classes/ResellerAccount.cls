public without sharing class ResellerAccount {

    private List<Account_Request__c> accountRequestListNew = new List<Account_Request__c>();
    private Map<Id, Account_Request__c> accountRequestOldMap = (Map<Id, Account_Request__c>) Trigger.oldMap;
    private Map<Id, Account> accountId_account_map = new Map<Id, Account>();
    private Map<Id, Account> userId_account_map = new Map<Id, Account>();
    private Map<Id, Id> partner_account_map = new Map<Id, Id>();
    private List<Account> partnerAccount = new List<Account>();
    private List<Contact> contact = new List<Contact>();
    private List<Id> newAccounts = new List<Id>();
    Set<Id> ownerAccountRequest = new Set<Id>();
    private Boolean hasPermission = FeatureManagement.checkPermission('Reseller_Admin');
    
    public ResellerAccount(){}    
    
    public void run(List<Account_Request__c> accountRequestListNew, Boolean isUpdate){
        this.accountRequestListNew = accountRequestListNew;

        for(Account_Request__c request : this.accountRequestListNew){
            ownerAccountRequest.add(request.OwnerId);
        } 

        this.getContactCurrentUser();
        this.getContactAccountRequest(ownerAccountRequest);

        // --- Atualiza o owner de novas contas com o owner da conta do reseller ---
        if(isUpdate){ 
            this.filterGO();
            if(this.newAccounts != null){
                this.setAccountOwner();
            }  
        }

        // --- Seta o contato na account request ---
        else{ 
            for(Account_Request__c request : this.accountRequestListNew){
                this.setContact(request);   
            }
        }     
    }

    public void filterGO(){
        for(Account_Request__c request : this.accountRequestListNew){
            Account_Request__c requestOld = new Account_Request__c();
            requestOld = this.accountRequestOldMap.get(request.Id);
            
            //Incluindo Id da conta na lista de IDs que irão receber o Owner da Partner
            if(requestOld.Search_Account_Info__c == false && requestOld.AccountId__c != null){
                if(requestOld.IsNewAccount__c){
                    this.newAccounts.add(request.AccountId__c); 
                    request.Search_Account_Info__c = true;
                }  
            } 

            // --- Seta o contato na account request ---
            if(request.ContactId__c == null){
                this.setContact(request); 
            }
        }
    }

    // --- Seta o contato na account request ---
    public Id setContact(Account_Request__c request){
        if(this.contact.size() > 0){ 
            if(request.Use_My_Own_Data__c == true || request.Bulk_Operation__c == true){  request.ContactId__c = this.contact[0].Id;}
            
            if(request.Bulk_Operation__c == false){ 
                request.Partner_Account__c = this.partnerAccount[0].Id; 
                request.Partner_Account_Owner__c = this.partnerAccount[0].OwnerId; 
                request.Indirect_Channel_Pricebook__c = this.partnerAccount[0].Indirect_Channel_Pricebook__c;
            }
            request.Engine_Log__c += ' -|- ' + ResellerEnumRepository.MassiveLog.INFO_CURRENT_USER_FOUND.name() + ' ' + String.valueOf(System.now());
        
        }else if(this.userId_account_map.containsKey(request.OwnerId)){
            Account acc = this.userId_account_map.get(request.OwnerId);
            request.ContactId__c = acc.Contacts[0].Id;
            request.Partner_Account__c = acc.Id;
            request.Partner_Account_Owner__c = acc.OwnerId;
            request.Indirect_Channel_Pricebook__c = acc.Indirect_Channel_Pricebook__c;

            this.partner_account_map.put(request.AccountId__c, acc.OwnerId);   
            request.Engine_Log__c += ' -|- ' + ResellerEnumRepository.MassiveLog.INFO_OWNER_ACCOUNT_REQUEST_FOUND.name() + ' ' + String.valueOf(System.now()); 

        }else{
            request.Engine_Log__c += ' -|- ' + ResellerEnumRepository.MassiveLog.INFO_NOT_FOUND.name() + ' ' + String.valueOf(System.now());   
        }
        
        return null;
    }

    // --- Pega o contato do owner da account request ---
    public void getContactAccountRequest(Set<Id> ownerId){
        Set<Id> contactId = new Set<Id>();
        Set<Id> accountId = new Set<Id>();
       
        List<User> listUser = [SELECT Id, Contact.AccountId, ContactId FROM User WHERE Id IN : ownerId];
        for(User user : listUser){
            contactId.add(user.ContactId);
            accountId.add(user.Contact.AccountId);
        }

        this.accountId_account_map = new Map<Id, Account>(
            [ SELECT Id, OwnerId, Indirect_Channel_Pricebook__c,
                ( SELECT Id, AccountId from Contacts WHERE Id IN : contactId LIMIT 1) 
              FROM Account 
              WHERE Id IN : accountId
            ]
        ); 
             
        for(User user : listUser){
            if(this.accountId_account_map.containsKey(user.Contact.AccountId)){
                Account accMap = this.accountId_account_map.get(user.contact.AccountId);    this.userId_account_map.put(user.Id, accMap);
               
            }
        }
    }

    // --- Pega o contato do current user ---
    public void getContactCurrentUser(){
        List<User> userOwner = [SELECT Id, Contact.AccountId FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];

        if(userOwner.size() > 0){
            this.contact = [SELECT Id, AccountId from Contact WHERE Id =: userOwner[0].ContactId LIMIT 1];    
            if(this.contact.size() > 0){
                this.partnerAccount = [SELECT Id, Indirect_Channel_Pricebook__c, OwnerId FROM Account WHERE id =: contact[0].accountId limit 1];                      
            } 
        }
    } 

    // --- Alterando o Owner das contas novas ---
    public void setAccountOwner(){
        List<Account> indirectExecutives = [SELECT OwnerId, Reseller__c FROM Account WHERE Id IN : this.newAccounts];
        for(Account acc : indirectExecutives){

            acc.OwnerId = this.accountRequestListNew[0].Partner_Account_Owner__c; 
            acc.Reseller__c = this.accountRequestListNew[0].Partner_Account__c;  
        }
        update indirectExecutives;
    }
}