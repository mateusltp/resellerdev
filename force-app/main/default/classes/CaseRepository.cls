/**
 * @description       : 
 * @author            : Samuel Silva - GFT (slml@gft.com)
 * @group             : 
 * @last modified on  : 12-03-2020
 * @last modified by  : Samuel Silva - GFT (slml@gft.com)
 * Modifications Log 
 * Ver   Date         Author                              Modification
 * 1.0   09-14-2020   Samuel Silva - GFT (slml@gft.com)   Initial Version
**/
public without sharing class CaseRepository {

    private ID dealDeskRT = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Deal_Desk_Approval').getRecordTypeId();
    
    public CaseRepository() {

    }

    public virtual Case byId(String recordId){
        return [SELECT Id
                ,Status
                ,Cancellation_Date__c
                ,OpportunityId__c
                FROM Case
                WHERE Id = :recordId];
    }

    public virtual Case getOpenCasebyQuoteId(String recordId){
        return [  
                SELECT Id, status
                    FROM Case
                    WHERE QuoteId__c =: recordId AND
                    Status NOT in ('Approved', 'Rejected') AND
                    RecordType.DeveloperName = 'Deal_Desk_Approval'
            ];
    }

    public virtual List<Case> getCaseByOppIdAndRTDevName(String oppId, String rtDevName){
        System.debug('####################### HERE ###############');
        return [  
                SELECT Id, Status, IsClosed
                FROM Case
                WHERE OpportunityId__c =: oppId AND RecordType.DeveloperName =: rtDevName
                ORDER BY CreatedDate DESC
                ];
    }

    public virtual Case createDealDeskCaseForOpp(String oppId, String quoteId, List<String> dealDeskDescription){
        
        String caseDescription = String.join(dealDeskDescription, ',');
        return new Case (   Subject = 'Deal Desk Approval Request', 
                            recordTypeId = dealDeskRT, 
                            OpportunityId__c = oppId,
                            Description = caseDescription,
                            QuoteId__c = quoteId,
                            Status = 'New'
                        );
    }

    public virtual Case add(Case record, Boolean isDealDeskCase){

        if(isDealDeskCase){
            Database.DMLOptions dmlOpts = setDealDeskAssignment();
            record.setOptions(dmlOpts);
        }
        Database.insert(record, true);
        return record;
    }

    public virtual Case edit(Case record){
        Database.update(record, true);
        return record;
    }

    private virtual AssignmentRule getDeskAssignmentRule (){
        AssignmentRule AR = new AssignmentRule();
        try {
            AR = [select id from AssignmentRule where SobjectType = 'Case' and Name='Default Assignment Rule' and Active = true limit 1];
        } catch(System.DmlException e) {
            //create new assigment rule? TBD
        }
        return AR;
    }    
    
    private virtual Database.DMLOptions setDealDeskAssignment (){
        AssignmentRule AR = getDeskAssignmentRule();
        Database.DMLOptions dmlOpts = new Database.DMLOptions();
        dmlOpts.assignmentRuleHeader.assignmentRuleId= AR.id;
        dmlOpts.EmailHeader.TriggerUserEmail = true;
        return dmlOpts;
    }
    
    public boolean manualShareRead(Id recordId, Id userOrGroupId){
        CaseShare caseShr = new CaseShare();
        caseShr.CaseId = recordId;      
        caseShr.UserOrGroupId = userOrGroupId;
        caseShr.CaseAccessLevel = 'Read';
        caseShr.RowCause = Schema.CaseShare.RowCause.Manual;
        Database.SaveResult sr = Database.insert(caseShr,false);
        if(sr.isSuccess()){
           return true;
        }
        else {
           Database.Error err = sr.getErrors()[0];         
           if(err.getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION  &&  
                    err.getMessage().contains('AccessLevel')){         
              return true;
           }
           else{
              return false;
           }
         }
     }
}