/**
 * @author vinicius.ferraz
 */
public with sharing class WaiverTriggerHandler extends triggerhandler {

    public override void beforeUpdate() {
        //new OfferChangesForApprovalHandler().waiverHandler( (List<Waiver__c>) trigger.new, (Map<Id,Waiver__c>) trigger.oldMap);
    }
    
    public override void beforeDelete() {
    }
    
    public override void afterUpdate() {
        new ApprovalRevampWaiver()
            .saveApprovedWaiver( (Map<Id,Waiver__c>) trigger.oldMap , (List<Waiver__c>) trigger.new );
        new WaiverApprovalHandler()
            .checkNeedApproval( (Map<Id,Waiver__c>) trigger.oldMap , (List<Waiver__c>) trigger.new );
        new SmbSkuWaiverApprovalHandler()
            .checkNeedApproval( (Map<Id,Waiver__c>) trigger.oldMap , (List<Waiver__c>) trigger.new );
        //new SMBRevenueMetricsUpdate().calculateRevenueMetrics((List<Waiver__c>) trigger.new);
        
        //FOR INDIRECT
        WaiverApprovalUpdateQuote waiverApprovalUpdateQuote = new WaiverApprovalUpdateQuote((List<Waiver__c>) trigger.new, (Map<Id,Waiver__c>) trigger.oldMap);
        waiverApprovalUpdateQuote.run();
    }

    public override void beforeInsert() {
        
    }

    public override void afterInsert() {
        new WaiverApprovalHandler()
            .checkNeedApproval( (Map<Id,Waiver__c>) trigger.oldMap , (List<Waiver__c>) trigger.new );
        new SmbSkuWaiverApprovalHandler()
            .checkNeedApproval( (Map<Id,Waiver__c>) trigger.oldMap , (List<Waiver__c>) trigger.new );
        //new SMBRevenueMetricsUpdate().calculateRevenueMetrics((List<Waiver__c>) trigger.new);
    }

    public override void afterDelete() {
        new WaiverDeleteHandler().evaluateOppEnablers((List<Waiver__c>) trigger.old);    
        new WaiverApprovalHandler()
            .checkNeedApproval( (Map<Id,Waiver__c>) trigger.oldMap , (List<Waiver__c>) trigger.new );
        new SmbSkuWaiverApprovalHandler()
            .checkNeedApproval( (Map<Id,Waiver__c>) trigger.oldMap , (List<Waiver__c>) trigger.new );
        //new SMBRevenueMetricsUpdate().calculateRevenueMetrics((List<Waiver__c>) trigger.old);
    }
}