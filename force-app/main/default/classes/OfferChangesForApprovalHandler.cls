/**
 * @author vinicius.ferraz
 */
public without sharing class OfferChangesForApprovalHandler {
    
    public void quoteHandler(List<Quote> newQuotes, Map<Id,Quote> oldQuotes){
        Set<Id> quoteIds = new Set<Id>();
        for (Quote newQuote:newQuotes )
            quoteIds.add(newQuote.Id);

        Map<Id,Quote> retrievedQuotes = new Map<Id,Quote>([select Id,Opportunity.RecordType.DeveloperName from Quote where Id in :quoteIds]);
        for ( Quote quote:newQuotes ){
            if (quote.get('Are_Family_Members_Included2__c')!=oldQuotes.get(quote.Id).get('Are_Family_Members_Included2__c') ||
                enablersHasRelevantChanges(quote, oldQuotes, retrievedQuotes)
                ){
                    quote.Has_Relevant_Changes_for_Offer_Approval__c = true;
                }            
                    
            if  (quote.get('ExpirationDate')!=oldQuotes.get(quote.Id).get('ExpirationDate') ||
                 quote.get('Free_Trial_Days__c')!=oldQuotes.get(quote.Id).get('Free_Trial_Days__c')  ||
                 quote.get('autonomous_marketplace_contract__c')!=oldQuotes.get(quote.Id).get('autonomous_marketplace_contract__c')  ||
                 quote.get('Custom_Integrations_Required__c')!=oldQuotes.get(quote.Id).get('Custom_Integrations_Required__c')){
                    quote.Has_Relevant_Changes_for_Offer_Deal_Desk__c = true;
                 }

            System.debug('NEW ' + quote.get('Custom_Integrations_Required__c'));
            System.debug('OLD ' + oldQuotes.get(quote.Id).get('Custom_Integrations_Required__c'));

        }
    }

    public void itemHandler(List<QuoteLineItem> newItems, Map<Id,QuoteLineItem> oldItems){
        Set<Id> itemsIds = new Set<Id>();
        Set<Id> priceEntriesIds = new Set<Id>();
        for (QuoteLineItem item : newItems ){
            if(Utils.isSemaphored('itemHandler', item.Id)){
                continue;
            }
            itemsIds.add(item.Id);
            if(item.PricebookEntryId != null){
                priceEntriesIds.add(item.PricebookEntryId);
            }
        }
        
        if(itemsIds.isEmpty()) return;
        
        Map<Id,QuoteLineItem> retrievedItems = new Map<Id,QuoteLineItem>([SELECT Id, ListPrice, Quote.Opportunity.RecordType.Name FROM QuoteLineItem WHERE Id in :itemsIds]);
        Map<Id, PricebookEntry> mapIdPricebookEntry = new Map<Id, PricebookEntry>((List<PricebookEntry>)PricebookRepository.retrievePricebookEntries(priceEntriesIds));
        
        for ( QuoteLineItem item:newItems ){
            if(retrievedItems.get(item.Id) == null){
                continue;
            }

            String lRecordtypeName = retrievedItems.get(item.Id).Quote.Opportunity.RecordType.Name;
            if ( lRecordtypeName <> 'Client Sales - New Business'  &&
                 lRecordtypeName <> 'Client Success - Renegotiation' && 
                 lRecordtypeName <> 'SMB - New Business' &&
                 lRecordtypeName <> 'SMB Success - Renegotiation' )
                break;
            
            Decimal lPreviousDiscount;
            
            if(item.get('Total_Price__c') != oldItems.get(item.id).get('Total_Price__c')){
                item.UnitPrice = item.Total_Price__c / item.Quantity;  
            }else if(item.get('UnitPrice') != oldItems.get(item.id).get('UnitPrice')){
                    item.Total_Price__c =  (item.UnitPrice * item.Quantity).setScale(2);
            }
            
             if ( item.get('UnitPrice') != oldItems.get(item.Id).get('UnitPrice') &&
                 retrievedItems.get(item.Id).ListPrice.setScale(2) <= item.UnitPrice.setScale(2) ){
                item.Discount__c = 0;
            } 
            
            if (mapIdPricebookEntry.containsKey(item.PricebookEntryId) && mapIdPricebookEntry.get(item.PricebookEntryId).Has_Minimum_Quantity__c){
                PricebookEntry priceEntry = mapIdPricebookEntry.get(item.PricebookEntryId);
                Decimal fullPrice = priceEntry.Product2.Maximum_Number_of_Employees__c * priceEntry.UnitPrice;
                if(fullPrice.setScale(2) > item.Total_Price__c.setScale(2)){
                    item.Discount__c = (((fullPrice - item.Total_Price__c) / fullPrice) * 100).setScale(2);
                }
            }else if ( retrievedItems.get(item.Id).ListPrice.setScale(2) > item.UnitPrice.setScale(2)){ 
                lPreviousDiscount = item.Discount__c;                
                item.Discount__c = (((retrievedItems.get(item.Id).ListPrice - item.UnitPrice) / retrievedItems.get(item.Id).ListPrice)*100).setScale(2);
            }

            if (
                !( ((String) item.get('Fee_Type__c')).contains('Professional Services') ) &&
                !( lRecordtypeName == 'SMB - New Business' && ((String) item.get('Fee_Type__c')).contains('Setup Fee') ) &&
                ( item.get('Fee_Type__c')!=oldItems.get(item.Id).get('Fee_Type__c') ||
                item.get('Discount__c')!=oldItems.get(item.Id).get('Discount__c') ||
                item.get('TotalPrice')!=oldItems.get(item.Id).get('TotalPrice') ) ){
                item.Has_Relevant_Changes_for_Offer_Approval__c = true;
            }
            
            if( lRecordtypeName.contains( 'Renegotiation' ) && lPreviousDiscount != null && 
                !( ((String) item.get('Fee_Type__c')).contains('Professional Services') ) &&
                item.Approval_Status__c == 'Approved' ){
                item.Has_Relevant_Changes_for_Offer_Approval__c = 
                    item.Discount__c > lPreviousDiscount;
            }
                        
            if( item.get('Fee_Contract_Type__c')!=null && (!((String) item.get('Fee_Contract_Type__c')).equals((String) oldItems.get(item.Id).get('Fee_Contract_Type__c')))){                
                item.Has_Relevant_Changes_for_Offer_Deal_Desk__c = true;
            }

            if ( item.get('Fee_Contract_Type__c')!=null && (((String) item.get('Fee_Type__c')).contains('Professional Services') &&
                item.get('TotalPrice')!=oldItems.get(item.Id).get('TotalPrice') ) )
                item.Has_Relevant_Changes_for_Offer_Deal_Desk__c = true;   
            }
        }

    public void waiverHandler(List<Waiver__c> newWaivers, Map<Id,Waiver__c> oldWaivers){
        for ( Waiver__c waiver:newWaivers ){
            if (oldWaivers.get(waiver.Id).get('Number_of_Months_after_Launch__c')!= null && 
                waiver.get('Number_of_Months_after_Launch__c')!=oldWaivers.get(waiver.Id).get('Number_of_Months_after_Launch__c') )
                waiver.Approval_Status__c = 'New';
        }        
    }

    public void eligibilityHandler(List<Eligibility__c> newEligibilities, Map<Id,Eligibility__c> oldEligibilities){
        for ( Eligibility__c eligibility:newEligibilities ){
            if (eligibility.get('Payment_Method__c')!=oldEligibilities.get(eligibility.Id).get('Payment_Method__c') ||                
                eligibility.get('Recurring_Billing_Period__c')!=oldEligibilities.get(eligibility.Id).get('Recurring_Billing_Period__c') )                
                eligibility.Has_Relevant_Changes_for_Offer_Approval__c = true;
            
            if (eligibility.get('Payment_Due_Days__c')!=oldEligibilities.get(eligibility.Id).get('Payment_Due_Days__c') ||
                eligibility.get('Billing_Day__c')!=oldEligibilities.get(eligibility.Id).get('Billing_Day__c') )
                eligibility.Has_Relevant_Changes_for_Offer_Deal_Desk__c = true;
        }
    }

    public void billingHandler(List<Payment__c> newBillingSettings, Map<Id,Payment__c> oldBillingSettings){
        for ( Payment__c payment:newBillingSettings ){
            if (payment.get('Frequency__c')!=oldBillingSettings.get(payment.Id).get('Frequency__c') ||        
                payment.get('Payment_Method__c')!=oldBillingSettings.get(payment.Id).get('Payment_Method__c') ||
                payment.get('Payment_Due_Days__c')!=oldBillingSettings.get(payment.Id).get('Payment_Due_Days__c')  )                
                payment.Has_Relevant_Changes_for_Offer_Deal_Desk__c = true;
        }
    }

    private Boolean enablersHasRelevantChanges(Quote quote, Map<Id,Quote> oldQuotes, Map<Id,Quote> retrievedQuotes) {
        String oppRecType = retrievedQuotes.get(quote.Id).Opportunity.RecordType.DeveloperName;
        if (oppRecType != 'SMB_New_Business' && oppRecType != 'SMB_Success_Renegotiation') {
            return false;
        }

        if (quote.get('Contact_Permission__c')!=oldQuotes.get(quote.Id).get('Contact_Permission__c')                        ||
            (quote.get('Expansion_100_of_the_eligible__c')!=oldQuotes.get(quote.Id).get('Expansion_100_of_the_eligible__c') &&  (! ((boolean) quote.get('Expansion_100_of_the_eligible__c'))) ) ||
            (quote.get('Exclusivity__c')!=oldQuotes.get(quote.Id).get('Exclusivity__c') &&  (! ((boolean) quote.get('Exclusivity__c'))) )  ||
            (quote.get('Employee_Corporate_Email__c')!=oldQuotes.get(quote.Id).get('Employee_Corporate_Email__c')  &&  (! ((boolean) quote.get('Employee_Corporate_Email__c')))   )
        ) {
            return true;
        } else {
            return false;
        }
    }
    public void populateCustomTotalPrice(List<QuoteLineItem> newItems){
        for (QuoteLineItem item : newItems){
            if(item.TotalPrice != null){
            item.Total_Price__c = item.TotalPrice.setScale(2);
            }
        }
    }
}