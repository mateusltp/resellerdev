/**
 * @description       : 
 * @author            : roei@gft.com
 * @group             : 
 * @last modified on  : 12-16-2021
 * @last modified by  : roei@gft.com
**/
@IsTest
public class UpsertOrderOnTagusCommandTest {
  /* @IsTest
  public static void execute() {
    Test.setMock(HttpCalloutMock.class, new HttpResponseMock());

    Account account = AccountMock.getGympassEntity();
    account.Send_To_Tagus__c = true;
    account.UUID__c = new Uuid().getValue();
    insert account;

    Contact contact = ContactMock.getStandard(account);
    insert contact;

    account.Attention__c = contact.Id;
    update account;

    Pricebook2 pricebook = new Pricebook2(
      CurrencyIsoCode='BRL',
      Id = Test.getStandardPricebookId(),
      IsActive = true
    );

    List<Product2> products = new List<Product2>();

    Product2 setupFee = ProductMock.getSetupFee();
    products.add(setupFee);

    Product2 accessFee = ProductMock.getAccessFee();
    products.add(accessFee);

    insert products;

    List<PricebookEntry> pricebookEntries = new List<PricebookEntry>();

    PricebookEntry setupFeePricebookEntry = PricebookMock.getPricebookEntry(pricebook, setupFee);
    pricebookEntries.add(setupFeePricebookEntry);

    PricebookEntry accessFeePricebookEntry = PricebookMock.getPricebookEntry(pricebook, accessFee);
    pricebookEntries.add(accessFeePricebookEntry);

    insert pricebookEntries;

    Opportunity opportunity = OpportunityMock.getNewBusiness(account, pricebook);
    opportunity.Gympass_Entity__c = account.Id;
    insert opportunity;

    Account_Opportunity_Relationship__c splitBilling = new Account_Opportunity_Relationship__c(
      Account__c = account.Id,
      Billing_Percentage__c = 100,
      Maintenance_Fee_Billing_Percentage__c = 100,
      Opportunity__c = opportunity.Id,
      Prof_Serv_Setup_Fee_Billing_Percentage__c = 100,
      Setup_Fee_Billing_Percentage__c = 100
    );
    insert splitBilling;

    Quote quote = QuoteMock.getStandard(opportunity);
    insert quote;

    List<QuoteLineItem> quoteLineItems = new List<QuoteLineItem>();

    QuoteLineItem setupFeeLine = QuoteLineItemMock.getSetupFee(
      quote,
      setupFeePricebookEntry
    );
    quoteLineItems.add(setupFeeLine);

    QuoteLineItem accessFeeLine = QuoteLineItemMock.getEnterpriseSubscription(
      quote,
      accessFeePricebookEntry
    );
    quoteLineItems.add(accessFeeLine);

    insert quoteLineItems;

    List<Payment__c> payments = new List<Payment__c>();

    Payment__c setupFeePayment = PaymentMock.getStandard(setupFeeLine);
    payments.add(setupFeePayment);

    Payment__c accessFeePayment = PaymentMock.getStandard(accessFeeLine);
    payments.add(accessFeePayment);

    insert payments;

    List<Eligibility__c> eligibles = new List<Eligibility__c>();

    Eligibility__c setupFeeEligibility = EligibilityMock.getStandard(setupFeePayment);
    eligibles.add(setupFeeEligibility);

    Eligibility__c accessFeeEligibility = EligibilityMock.getStandard(accessFeePayment);
    eligibles.add(accessFeeEligibility);

    insert eligibles;

    List<Waiver__c> waivers = new List<Waiver__c>();

    Waiver__c setupFeeWaiver = WaiverMock.getStandard(setupFeePayment);
    waivers.add(setupFeeWaiver);

    Waiver__c accessFeeWaiver = WaiverMock.getStandard(accessFeePayment);
    waivers.add(accessFeeWaiver);

    insert waivers;

    Test.startTest();

    opportunity.StageName = 'Lançado/Ganho';
    update opportunity;

    EventQueueFixtureFactory.createEventConfigForEvent(
      'CREATE_ORDER_ON_TAGUS',
      'UpsertOrderOnTagusCommand'
    );

    List<Order> orders = [SELECT Id FROM Order];

    EventQueue event = new TagusOrderOutboundPublisher(orders).runCreate();

    Test.stopTest();

    Order order = [SELECT Id, UUID__c FROM Order LIMIT 1];

    System.assert(order.UUID__c != null, 'Order is not integrated');
  }
  */
  private class HttpResponseMock implements HttpCalloutMock {
    public HTTPResponse respond(HTTPRequest req) {
      HttpResponse res = new HttpResponse();
      res.setHeader('Content-Type', 'application/json');
      res.setStatusCode(200);
      return res;
    }
  }
}