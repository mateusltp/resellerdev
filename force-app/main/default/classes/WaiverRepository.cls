/**
 * @description       : 
 * @author            : AlyssonMota
 * @group             : 
 * @last modified on  : 06-02-2022
 * @last modified by  : tiago.ribeiro@gympass.com
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   09-11-2020   Alysson Mota   Initial Version
**/
public with sharing class WaiverRepository {
	
    public List<Waiver__c> allForItems(Set<Id> quoteItemsIds){
        return [
            SELECT Id,
            Payment__r.Quote_Line_Item__r.PricebookEntry.Product2.Family,Payment__r.Quote_Line_Item__r.PricebookEntry.Product2.Name,
            Name,
            Rationale__c,
            Approval_Status_Formula__c,
            Number_of_Months_after_Launch__c,
            End_Date__c,
            Payment__c,
            Percentage__c,
            Start_Date__c,
            Approval_Status__c
            FROM Waiver__c
            WHERE Payment__r.Quote_Line_Item__c in :quoteItemsIds
        ];
    }
    
    public Map<Id, List<Waiver__c>> getWaiversByPayments(List<Payment__c> payments) {
        Map<Id, List<Waiver__c>> mapPaymentIdWaiverList = new Map<Id, List<Waiver__c>>();
        Set<Id> paymentIdSet = new Set<Id>();

        for (Payment__c p : payments) {
            if (p.Id != null) {
                paymentIdSet.add(p.Id);
            }
        }

        List<Waiver__c> waivers = [
            SELECT Id, Name,Rationale__c, Approval_Status_Formula__c,Number_of_Months_after_Launch__c, End_Date__c, Payment__c, Percentage__c, Start_Date__c, Approval_Status__c
            FROM Waiver__c
            WHERE Payment__c IN :paymentIdSet
        ];

        for (Waiver__c w : waivers) {
            if (mapPaymentIdWaiverList.containsKey(w.Payment__c)) {
                mapPaymentIdWaiverList.get(w.Payment__c).add(w);
            } else {
                mapPaymentIdWaiverList.put(w.Payment__c, new List<Waiver__c>{w});
            }
        }

        return mapPaymentIdWaiverList;
    }


    public List<Waiver__c> getListWaiversForPayments(List<Payment__c> payments) {     
        Set<Id> paymentIdSet = new Set<Id>();
        for (Payment__c p : payments) {
            if (p.Id != null) {
                paymentIdSet.add(p.Id);
            }
        }

        return [
            SELECT Id, Approval_Status__c,Rationale__c,Has_Relevant_Changes_for_Offer_Approval__c,Approval_Status_Formula__c,Number_of_Months_after_Launch__c, Name, End_Date__c, Payment__c, Percentage__c, Start_Date__c
            FROM Waiver__c
            WHERE Payment__c IN :paymentIdSet
        ];
    }

    public List<Waiver__c>  edit(List<Waiver__c> waiverList ){
        Database.update(waiverList); 
        return waiverList;
    }
    
    public List<Waiver__c> byIdList(List<Id> idList) {
        return [
            SELECT Id, Payment__r.Quote_Line_Item__r.Quote.OpportunityId
            FROM Waiver__c
            WHERE Id IN :idList
        ];
    }
    
    public Map<Id, List<Waiver__c>> getOppIdToWaivers(List<Id> waiverIdList) {
        Map<Id, List<Waiver__c>> oppIdToWaivers = new Map<Id, List<Waiver__c>>();
        List<Waiver__c> waivers = byIdList(waiverIdList);
        
        for (Waiver__c waiver : waivers) {
            if (oppIdToWaivers.containsKey(waiver.Payment__r.Quote_Line_Item__r.Quote.OpportunityId)) {
            	oppIdToWaivers.get(waiver.Payment__r.Quote_Line_Item__r.Quote.OpportunityId).add(waiver);
            } else {
                oppIdToWaivers.put(waiver.Payment__r.Quote_Line_Item__r.Quote.OpportunityId, new List<Waiver__c>{waiver});
            }
        }
        
        return oppIdToWaivers;
    }

    public List<Waiver__c> getWaiversByQuote(Set<Id> setQuoteIds){
        return [SELECT Id, Duration__c, Position__c, Quote_Line_Item__r.Waivers_Start_From__c, Quote_Line_Item__r.Quote.Start_Date__c, Quote_Line_Item__r.Quote.Offer_Sent_Date__c, OrderWaiver_Start_Date__c
                    FROM Waiver__c 
                    WHERE Quote_Line_Item__r.QuoteId IN :setQuoteIds
                    ORDER BY Quote_Line_Item__c, Position__c];
    }

    @AuraEnabled
    public static ID getRecordTypeIdByDeveloperName (String devName){
        try { 
            return Schema.SObjectType.Waiver__c.getRecordTypeInfosByDeveloperName().get(devName).getRecordTypeId();
        }catch (Exception e) {
            throw new AuraHandledException('Something wrong! Waiver RecordType was not found: ' + devName + ' Error: ' + e.getMessage());
        }
    }
}