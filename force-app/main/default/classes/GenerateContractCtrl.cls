/*
* @Author: Bruno Pinho
* @Date: March/2019
* @Description: Controller to Generate Contracts
*/
public class GenerateContractCtrl
{
    @AuraEnabled
    public static List<ApexPage> getAvailableContracts() {
        List<ApexPage> listApexPages = new List<ApexPage>();
        
        /*listApexPages = [SELECT 
                            Id, Name, MasterLabel
                         FROM 
                            ApexPage
                         WHERE 
                            MasterLabel LIKE '%Contract%'
                         ORDER BY
                            MasterLabel ASC];*/
        
        listApexPages = [SELECT
                            Id, Name, MasterLabel
                         FROM
                            ApexPage
                         WHERE
                            Name LIKE '%ContractBrazilPortuguese' OR Name = 'ContractUSEnglish'
                         ORDER BY 
                            MasterLabel ASC];
        
        return listApexPages;        
    }
    
    @AuraEnabled
    public static Boolean validateContractGeneration(Id recordId) {
        
        List<Quote> listQuote = [SELECT 
                                    Id, Pending_Exceptions__c
                                 FROM
                                    Quote
                                 WHERE
                                    Id =: recordId
                                 LIMIT
                                    1];
        
        if (listQuote.size() > 0) {
            if (listQuote[0].Pending_Exceptions__c > 0)
                return false;
            else
                return true;
        } else {
            return false;
        }
    }
}