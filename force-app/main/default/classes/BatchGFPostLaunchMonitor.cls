/**
* @author vncferraz
*
*   Generate records to provide information to business which contacts will receive which survey on getFeedback
**/
global class BatchGFPostLaunchMonitor implements Database.Batchable<sObject> {
    
    global Integer daysToAdd;

    global BatchGFPostLaunchMonitor(Integer daysToAdd){
        this.daysToAdd = daysToAdd;
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        Date ed = (Date.today().addDays(this.daysToAdd)).addDays(-90);
        return Database.getQueryLocator([
            SELECT Id, Data_do_Lancamento__c, AccountId, Account.RecordTypeId, RecordTypeId, StageName
            FROM Opportunity
            WHERE Data_do_Lancamento__c =: ed
        ]);
    }
    
    global void execute(Database.BatchableContext bc, List<Opportunity> scope) {
        
        List<Account> lstAccNew = new List<Account>();
        List<Get_Feedback_Survey_Schedule__c> schedules = new List<Get_Feedback_Survey_Schedule__c>();
        List<Contact> lstUpdateConNew = new List<Contact>();
        
        Map<Id,Contact> lMapContactEvent = new Map<Id,Contact>();
        
        Id rtOppClientNew = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        Id rtOppSMBNew = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SMB_New_Business').getRecordTypeId();
        
        Id rtAcc = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
        
        for(Opportunity o: scope){
            if((o.RecordTypeId == rtOppClientNew || o.RecordTypeId == rtOppSMBNew) && o.StageName == 'Lançado/Ganho' && o.Account.RecordTypeId == rtAcc){                
                   lstAccNew.add(o.Account);
               }
        }
        if(!lstAccNew.isEmpty()){
            for(Contact con: [Select Id, Status_do_contato__c, GFSendSurvey__c, Email, Primary_HR_Contact__c,DecisionMaker__c, Role__c FROM Contact WHERE AccountID IN :lstAccNew]){
                if(con.Status_do_contato__c == 'Ativo' && con.Email != null && con.Primary_HR_Contact__c == TRUE){
                    schedules.add( 
                        new Get_Feedback_Survey_Schedule__c(Contact__c=con.Id,
                                                             Survey_Name__c='Client Launch/Post Launch',
                                                             Scheduled_Date__c=Date.today().addDays(this.daysToAdd))
                    );
                   
                }
                else if (con.Status_do_contato__c == 'Ativo' && con.Email != null && con.DecisionMaker__c == 'Yes'){
                    
                    con.GFSendSurvey__c = 'Client Launch/Post Launch';
                    lMapContactEvent.put(con.Id, con);
                }
            }
        }
        
        if(!lMapContactEvent.isEmpty()){
            for(Event ev : [SELECT Id, WhoId, Who.Name,Realizado__c, RecordType.Name FROM Event Where Subject='Meeting' AND WhoId IN: lMapContactEvent.KeySet() AND Realizado__c = 'Yes' AND ActivityDate <= LAST_N_DAYS:90]){
                schedules.add( 
                        new Get_Feedback_Survey_Schedule__c(Contact__c=ev.whoId,
                                                             Survey_Name__c='Client Launch/Post Launch',
                                                             Scheduled_Date__c=Date.today().addDays(this.daysToAdd))
                    );
               
            }
        }
        
        if(!schedules.isEmpty()){
            insert schedules;
        }
    }
    
    global void finish(Database.BatchableContext bc) {
        
    }
}