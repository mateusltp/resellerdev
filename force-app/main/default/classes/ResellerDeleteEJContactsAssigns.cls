public without sharing class ResellerDeleteEJContactsAssigns {
    
    @InvocableMethod(label='[RESELLER][EJ] Delete Previous Contacts Assignments')
    public static void deleteFormContactsAssignments(List<String> engagementFormIdLst) {

        if(engagementFormIdLst.isEmpty()) return;

        List<Engagement_Journey_Contact_Assignment__c> assignmentsToDeleteLst = 
            [SELECT Id 
             FROM Engagement_Journey_Contact_Assignment__c 
             WHERE Engagement_Journey_Form__c = :engagementFormIdLst[0]];
        
        System.debug('### INTO THE deleteFormContactsAssignments ###');
        System.debug(assignmentsToDeleteLst);
        if(assignmentsToDeleteLst.isEmpty()) return;
        
        Database.delete(assignmentsToDeleteLst);
    }
}