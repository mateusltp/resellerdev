public with sharing class AccountController {
    @AuraEnabled(cacheable=true)
    public static List<Account> getAccounts(String getCnpj) {
        return [
            SELECT Name, BillingCountry, Website
            FROM Account WHERE Id_Company__c = :getCnpj
            
            ORDER BY Name
        ];
    }
}