public class SelfCheckoutOpportunityRequest {
    private LeadDTO client;
    private OpportunityDTO opportunity;
  
    public LeadDTO getLeadDTO() {
      return this.client;
    }
  
    public OpportunityDTO getOpportunityDTO() {
      return this.opportunity;
    }
}