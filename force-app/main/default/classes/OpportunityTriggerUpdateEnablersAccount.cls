/**
* @description       : 
* @author            : Mateus Augusto - GFT (moes@gft.com)
* @group             : 
* @last modified on  : 05-30-2022
* @last modified by  : Mateus Augusto - GFT (moes@gft.com)
**/
public without sharing class OpportunityTriggerUpdateEnablersAccount {
    
    private List<Opportunity> m_lstDataNew;
    private Map<Id, Opportunity> m_mapDataOld;   
    private Map<Id, Opportunity> m_mapObjFiltered = new Map<Id, Opportunity>();
    public static Id OPPORTUNITY_RECORD_TYPE_NEW_BUS_INDIRECT_CHANNEL = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Indirect_Channel_New_Business').getRecordTypeId();
    
    EnablersRepository enablersRepository = new EnablersRepository();
    
    public OpportunityTriggerUpdateEnablersAccount(List<Opportunity> lstDataNew, Map<Id, Opportunity> mapDataOld) {
        this.m_lstDataNew = lstDataNew;
        this.m_mapDataOld = mapDataOld;
    }
    
    private void filter() {
        
        for(Opportunity itemNew : this.m_lstDataNew) {
            Opportunity itemOld = this.m_mapDataOld.get(itemNew.Id);
            
            if (
                itemNew.RecordTypeId == OPPORTUNITY_RECORD_TYPE_NEW_BUS_INDIRECT_CHANNEL &&
                itemNew.StageName != itemOld.StageName &&
                (itemNew.StageName == 'Perdido' || itemNew.StageName == 'Lançado/Ganho')
                //itemNew.isClosed //Fechado/Ganho ou Fechado/Perdido
            )
                m_mapObjFiltered.put(itemNew.Id, itemNew);
        }
    } 
    
    public void run(){
        
        filter();
        
        if(!m_mapObjFiltered.isEmpty()){
            
            updateEnablersAccount(m_mapObjFiltered);
            
        }
        
    }
        
    public void updateEnablersAccount(Map<Id, Opportunity> m_mapObjFiltered){
        
        Set<Id> oppIds = new Set<Id>(m_mapObjFiltered.keySet());        
        Set<Id> accIds = new Set<Id>();
        
        for(Opportunity opp : m_mapObjFiltered.values()){           
            accIds.add(opp.AccountId);                         
        }
        
        List<Step_Towards_Success1__c> oppEnablers = enablersRepository.getOpportunityEnablersByOppId(oppIds);
        List<Step_Towards_Success1__c> accEnablers = enablersRepository.getAccountEnablersByAccId(accIds);
        //system.debug('@@oppEnablers '+oppEnablers.size());
        //system.debug('@@accEnablers '+accEnablers);
        
        Map<String, Step_Towards_Success1__c> mapOppEnablers = new Map<String, Step_Towards_Success1__c>();
        for(Step_Towards_Success1__c item : oppEnablers){
            
            mapOppEnablers.put(item.Name, item);
            //system.debug('mapOppEnablers '+mapOppEnablers.keySet());
            //system.debug('mapOppEnablers '+mapOppEnablers.values());           
        }
        
        Map<String, Step_Towards_Success1__c> mapAccEnablers = new Map<String, Step_Towards_Success1__c>();
        for(Step_Towards_Success1__c item : accEnablers){
            
            mapAccEnablers.put(item.Name, item);              
            //system.debug('mapAccEnablers '+mapAccEnablers.keySet());
            //system.debug('mapAccEnablers '+mapAccEnablers.values());
        }
        
        for(String step : mapAccEnablers.keySet()){
            Step_Towards_Success1__c itemAcc = mapAccEnablers.get(step);
            Step_Towards_Success1__c itemOpp = mapOppEnablers.get(step);
                
            if(itemAcc != null && itemOpp != null) {
                itemAcc.Achieved__c = itemOpp.Achieved__c;
            }
            
        }
               
        upsert mapAccEnablers.values();
       
    }
}