/*
 * @author Bruno Pinho
 * @date January/2019
 * @description Test To BatchEventAllHistoryDecisionMakers
 */

@isTest(SeeAllData=true)
private class BatchEventAllHistoryDecisionMakersTest
{
	static BatchEventAllHistoryDecisionMakers instance = null;
	static void SetUp(){
		test.StartTest();
		instance = new BatchEventAllHistoryDecisionMakers();
	}
    
	static void TearDown(){
		test.StopTest();
	}
    
	public static testmethod void Test_start(){
		SetUp();
		Database.BatchableContext bc;
		System.Assert(instance.start(bc) != null);
		TearDown();
	}
    
	public static testmethod void Test_execute(){
		SetUp();
		Database.BatchableContext bc;
		List<Event> scope = [SELECT Id, IsChild, AccountId, ActivityDate, ActivityDateTime, /*Campaign__c,*/ Comentarios__c, Created_By_Role_Name__c, CurrencyIsoCode, DB_Activity_Type__c, Description, EndDateTime, EventSubtype, Executive_in_Meeting__c, GroupEventType, Gympass_Event__c, Head_or_Above_Gympass_del__c, IsAllDayEvent, IsRecurrence, IsReminderSet, Is_there_a_Decision_Maker__c, Is_there_a_gympass_leader_present__c, LID__Date_Sent__c, LID__URL__c, Location, Motivo__c, Oportunidade_de_Negocio__c, Origem__c, OwnerId, Realizado__c, RecurrenceActivityId, RecurrenceDayOfMonth, RecurrenceDayOfWeekMask, RecurrenceEndDateOnly, RecurrenceInstance, RecurrenceInterval, RecurrenceMonthOfYear, RecurrenceStartDateTime, RecurrenceTimeZoneSidKey, RecurrenceType, ReminderDateTime, ShowAs, StartDateTime, Subject, TipoTE__c, Type, WhatId, WhoId
                                             FROM Event
                                             WHERE IsDeleted = FALSE
                                             LIMIT 50];
		instance.execute(bc, scope);
		TearDown();
	}
    
	public static testmethod void Test_finish(){
		SetUp();
		Database.BatchableContext bc;
		instance.finish(bc);
		TearDown();
	}
}