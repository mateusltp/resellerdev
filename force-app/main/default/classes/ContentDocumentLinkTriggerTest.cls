/**
 * @description       : 
 * @author            : GEPI@GFT.com
 * @group             : 
 * @last modified on  : 04-07-2022
 * @last modified by  : gft.samuel.silva@ext.gympass.com
 * Modifications Log 
 * Ver   Date          Author                           Modification
 * 1.0   07-09-2020    David Mantovani - DDMA@gft.com   Initial Version
**/
@isTest
public class ContentDocumentLinkTriggerTest {
    @TestSetup
    static void setup(){
      
        List<Account> accList = new List<Account>();
        Id recordTypePartner = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gym_Partner').getRecordTypeId();

        Account fileAccount = new Account();

        fileAccount.Name = 'Partner_Upload';
        fileAccount.RecordTypeId = recordTypePartner;
        fileAccount.Types_of_ownership__c = Label.privateOwnerPicklist;
        fileAccount.GP_Status__c = 'Pending Approval';
        fileAccount.ShippingState = 'Minas Gerais';
        fileAccount.ShippingStreet = 'RUX ASSS';
        fileAccount.ShippingCity = 'CITY CITY';
        fileAccount.ShippingCountry = 'Brazil';
        fileAccount.Gym_Type__c = 'Studios';
        fileAccount.Subscription_Type__c = 'Value per class';
        fileAccount.Subscription_Period__c = 'Monthy value';
        fileAccount.Subscription_Type_Estimated_Price__c	= 100;
        fileAccount.Has_market_cannibalization__c = 'No';
        fileAccount.Exclusivity__c = 'Yes';
        fileAccount.Exclusivity_End_Date__c = Date.today().addYears(1);
        fileAccount.Exclusivity_Partnership__c = 'Full Exclusivity';
        fileAccount.Exclusivity_Restrictions__c= 'No';
        fileAccount.Website = 'testing@tesapex.com';
        fileAccount.Gym_Email__c = 'gymemail@apex.com';
        fileAccount.Phone = '3222123123';
        accList.add(fileAccount);
        insert fileAccount;

        //Create Opportunity by RecordType
        List<Opportunity> oppList = new List<Opportunity>();
        Id recordTypeIdSmall = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Gyms_Small_Medium_Partner').getRecordTypeId();
        Id recordTypeIdWishList = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Gyms_Wishlist_Partner').getRecordTypeId();
        
        Opportunity accOpp = new Opportunity();
        accOpp.recordTypeId = recordTypeIdSmall;
        accOpp.AccountId = fileAccount.id;
        accOpp.Name = 'Upload_Test_Small';      
        accOpp.Gym_agreed_to_an_API_integration__c	= 'Yes';
        accOpp.Integration_Fee_Deduction__c = 'No';
        accOpp.CloseDate = Date.today();
        accOpp.Success_Look_Like__c = 'Yes';
        accOpp.Success_Look_Like_Description__c	= 'Money money';
        accOpp.StageName = 'Qualification';
        accOpp.Type	= 'Expansion';     
        accOpp.CurrencyIsoCode = 'BRL';
        accOpp.Gympass_Plus__c = 'Yes';
        accOpp.Partner_Opp_File_url__c = 'Small_OppFile.jpg';
        INSERT accOpp;

        Opportunity accOpp2 = new Opportunity();
        accOpp2.recordTypeId = recordTypeIdSmall;
        accOpp2.AccountId = fileAccount.id;
        accOpp2.Name = 'Upload_Test_Whishlist';
        accOpp2.Gym_agreed_to_an_API_integration__c	= 'Yes';
        accOpp2.Integration_Fee_Deduction__c = 'No';
        accOpp2.CloseDate = Date.today();
        accOpp2.Success_Look_Like__c = 'Yes';
        accOpp2.Success_Look_Like_Description__c	= 'Money money';
        accOpp2.StageName = 'Qualification';
        accOpp2.Type	= 'Expansion';
        accOpp2.CurrencyIsoCode = 'BRL';
        accOpp2.Gympass_Plus__c = 'Yes';
        accOpp2.Partner_Opp_File_url__c = 'Whishlist_OppFile.jpg';
        INSERT accOpp2;


        

        Id prodRtId = Schema.SObjectType.Product_Item__c.getRecordTypeInfosByDeveloperName().get('Gyms_Product_Item_Partner').getRecordTypeId();
       
        Product_Item__c prod = new Product_Item__c();    
        prod.recordTypeId = prodRtId;
        prod.CurrencyIsoCode = 'BRL';
        prod.CAP_Value__c = 120;
        prod.Do_You_Have_A_No_Show_Fee__c = 'No';
        prod.Has_Late_Cancellation_Fee__c = 'No';
        prod.Late_Cancellation_Percent__c = '0';
        prod.Maximum_Live_Class_Per_Month__c = 0;
        prod.No_Show_Fee_Percent__c = '0';
        prod.Net_Transfer_Price__c	=10;
        prod.Product_Definition__c	='Dança';
        prod.Product_Restriction__c	= 'None';
        prod.Name = 'Dança';
        
        INSERT prod;
    }
    @isTest static void uploadNewFileInAccount(){
        List<Account> listFileAccount = [SELECT Id, Name FROM Account WHERE Name = 'Partner_Upload'  LIMIT 1];
        
        if (!listFileAccount.isEmpty()) {
            Account fileAccount = listFileAccount[0];
            
            Test.startTest();
            Blob bodyBlob=Blob.valueOf('Unit Test ContentVersion Body');
            ContentVersion contentVersion_1 = new ContentVersion(
                Title = 'Photo Penguins',
                PathOnClient = '/Penguins.png',
                VersionData = bodyBlob,
                origin = 'H',
                Type_Files_fileupload__c = 'Photo'
              );
            Insert  contentVersion_1;
            
            ContentDocumentLink ContentDLAcc = new ContentDocumentLink();
            ContentDLAcc.ContentDocumentId = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: contentVersion_1.Id].ContentDocumentId;
            ContentDLAcc.LinkedEntityId = fileAccount.Id;
            Insert ContentDLAcc;
    
            list<ContentDistribution> lstDistribution = [SELECT id, Name, DistributionPublicUrl FROM ContentDistribution where Name =: 'File ' + contentVersion_1.Id];
    
            system.assertEquals(lstDistribution.size() > 0, true);
    
            Test.stopTest();    
        }
    }

    @isTest static void uploadNewFileInOpportunity(){
        List<Opportunity> listAccOpp = [SELECT Id, Name FROM Opportunity WHERE Name = 'Upload_Test_Small' LIMIT 1];
        
        if (!listAccOpp.isEmpty()) {
              Opportunity accOpp = listAccOpp[0];
                    
              Test.startTest();
              Blob bodyBlob=Blob.valueOf('Unit Test ContentVersion Body');
              
              ContentVersion contentVersion_2 = new ContentVersion(
                  Title = 'Penguins3',
                  PathOnClient = '/Penguins3.jpg',
                  VersionData = bodyBlob,
                  origin = 'H'
                );
              Insert  contentVersion_2;
        
              ContentDocumentLink ContentDLAcc1 = new ContentDocumentLink();
              ContentDLAcc1.ContentDocumentId = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: contentVersion_2.Id].ContentDocumentId;
              ContentDLAcc1.LinkedEntityId = accOpp.Id;
              Insert ContentDLAcc1;
        
              list<ContentDistribution> lstDistribution2 = [SELECT id, Name, DistributionPublicUrl FROM ContentDistribution where Name =: 'File ' + contentVersion_2.Id];
        
              system.assertEquals(lstDistribution2.size() > 0, true);
        
              Test.stopTest(); 
        }
    }
        
    @isTest static void deleteNewFileContentDocumentLink(){
        List<Opportunity> listAccOpp = [SELECT Id, Name FROM Opportunity WHERE Name = 'Upload_Test_Small' LIMIT 1];
        
        if (!listAccOpp.isEmpty()) {
            Opportunity accOpp = listAccOpp[0];
            
          Blob bodyBlob=Blob.valueOf('Unit Test ContentVersion Body');
    
          ContentVersion contentVersion_2 = new ContentVersion(
              Title = 'Penguins2',
              PathOnClient = '/Penguins2.png',
              VersionData = bodyBlob,
              origin = 'H'
            );
          Insert  contentVersion_2;
    
          ContentDocumentLink ContentDLAcc1 = new ContentDocumentLink();
          ContentDLAcc1.ContentDocumentId = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: contentVersion_2.Id].ContentDocumentId;
          ContentDLAcc1.LinkedEntityId = accOpp.Id;
          Insert ContentDLAcc1;
    
    
          Test.startTest();  
          DELETE ContentDLAcc1;     
          List<FileObject__c>  fileObjLst= [SELECT Id FROM FileObject__c WHERE conversionId__c =: contentVersion_2.Id];
          system.assertEquals(fileObjLst.size() == 0, true);
          Test.stopTest(); 
        }
    }

    @isTest static void deleteNewFileContentDocument(){
        List<Opportunity> listAccOpp = [SELECT Id, Name FROM Opportunity WHERE Name = 'Upload_Test_Small' LIMIT 1];
        
        if (!listAccOpp.isEmpty()) {
            Opportunity accOpp = listAccOpp[0];
          Blob bodyBlob=Blob.valueOf('Unit Test ContentVersion Body');
    
          ContentVersion contentVersion_2 = new ContentVersion(
              Title = 'Penguins1',
              PathOnClient = '/Penguins1.png',
              VersionData = bodyBlob,
              origin = 'H'
            );
          Insert  contentVersion_2;
    
      
          ContentDocumentLink ContentDLAcc1 = new ContentDocumentLink();
          ContentDLAcc1.ContentDocumentId = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: contentVersion_2.Id].ContentDocumentId;
          ContentDLAcc1.LinkedEntityId = accOpp.Id;
          Insert ContentDLAcc1;
    
    
          Test.startTest();  
             
          List<ContentDocument>  contDocumentLst = [SELECT Id FROM ContentDocument WHERE Id =: ContentDLAcc1.ContentDocumentId];
          DELETE contDocumentLst;
          List<FileObject__c>  fileObjLst= [SELECT Id FROM FileObject__c WHERE conversionId__c =: contentVersion_2.Id];
          system.assertEquals(fileObjLst.size() == 0, true);
          Test.stopTest(); 
        }
    }
}