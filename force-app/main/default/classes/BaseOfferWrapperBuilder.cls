/**
 * @author vncferraz
 * 
 * Provide abstraction layer for OfferWrapperBuilder to OfferProcessor
 */
public abstract class BaseOfferWrapperBuilder {
    
    protected BaseOfferWrapper wrapper;
    protected AccountRepository accounts;
    protected Account account;
    protected Map<String , String> currencyByCountry;
    protected Id gympassEntityId;
    

    public BaseOfferWrapperBuilder() {
        init();
    }

    public virtual void init(){        
        this.accounts = new AccountRepository();  
    }

    protected void addError(String message){
        this.wrapper.addError(message);
    }

    public BaseOfferWrapperBuilder withCurrencyByCountry(Map<String , String> currencyByCountry){
        this.currencyByCountry = currencyByCountry;
        return this;
    }

    protected virtual void defineGympassEntity(Offer_Queue__c offer){
        Map<String,Id> entitiesByExternalId = accounts.gympassEntitiesByExternalId();
        if ( this.account.Standard_Gympass_Entity__c == null || !entitiesByExternalId.containsKey( account.Standard_Gympass_Entity__c)){
            addError('Please check Account Standard Gympass Entity for '+this.account.Name);
            return;
        }        
        this.gympassEntityId = entitiesByExternalId.get( account.Standard_Gympass_Entity__c);
    }

    protected virtual void preLoad(Offer_Queue__c offer){
        try{
            this.account = this.accounts.byId(offer.Account_ID__c);        
        }catch(System.QueryException e){
            addError('Account not found for ID '+offer.Account_ID__c);
            return;
        }
    }

    public virtual BaseOfferWrapper build( Offer_Queue__c offer){
        
        preLoad(offer);

        if ( !this.wrapper.hasError() )
            defineGympassEntity(offer);
        
        if ( !this.wrapper.hasError() )
            postLoad(offer);
        
        return this.wrapper;
    }

    public abstract BaseOfferWrapper postLoad(Offer_Queue__c offer);

    
}