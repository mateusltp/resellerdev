/**
 * @description       : 
 * @author            : 
 * @group             : 
 * @last modified on  : 09-15-2022
 * @last modified by  : Eurikesse Ferreira - (eurikesse.ferreira@gympass.com)
**/
public without sharing class ResellerFormController {
    
    public static final String ACCOUNT_RECORDTYPEID_RESELLER = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
    public static final String CONTACT_RECORDTYPEID_RESELLER = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Direct_Channel').getRecordTypeId();
    public static final String OPPORTUNITY_RECORDTYPEID_RESELLER = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Indirect_Channel_New_Business').getRecordTypeId();
    public static final String OPPORTUNITY_RECORDTYPEID_INDIRECT = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Indirect_Channel').getRecordTypeId();

    
    @AuraEnabled
    public static void createContact(Account_Request__c account_request){ // String contact
    
        Boolean isUpdate = false;
        Contact contactUpdate;
        List<Account> account = [SELECT Id, (SELECT Id, Email FROM Contacts) FROM Account WHERE Id = :account_request.AccountId__c LIMIT 1];
      
        for(Contact contacts : account[0].Contacts){
            if(contacts.Email == account_request.Email__c){
                isUpdate = true;
                contactUpdate = contacts;
                //account_request.ContactId__c = contacts.Id;
            }       
        }

        if(isUpdate == true){
            contactUpdate.LastName = account_request.Contact_Name__c;
            contactUpdate.Email = account_request.Email__c;
            contactUpdate.Department = account_request.Department__c;
            contactUpdate.Cargo__c = account_request.Role__c;

            update contactUpdate;
            account_request.ContactId__c = contactUpdate.Id;
        }
        else{
            Contact contact = new Contact();
            contact.LastName = account_request.Contact_Name__c;
            contact.Email = account_request.Email__c;
            contact.AccountId = account_request.AccountId__c;
            contact.RecordTypeId = CONTACT_RECORDTYPEID_RESELLER;
            contact.Department = account_request.Department__c;
            contact.Cargo__c = account_request.Role__c;
            
            insert contact;
            account_request.ContactId__c = contact.Id;   
        }
    }

    @AuraEnabled
    public static String getAccounts(String accountRequestId){
        List<ResellerLogDomain> domainsLogs_list = new List<ResellerLogDomain>();
        Map<String, AccountSearch.AccountSearchWrapper> accountSearchWrapper_map = new Map<String,AccountSearch.AccountSearchWrapper>();

        try{
            List<Account_Request__c> accountRequest_list = [ SELECT Id, Unique_Identifier__c, Billing_Country__c, Name, Website__c FROM Account_Request__c WHERE Id =: accountRequestId LIMIT 1 ];    
            AccountSearch accountSearch = new AccountSearch(accountRequest_list);
            accountSearchWrapper_map = accountSearch.getAccounts();

            if(accountSearchWrapper_map == null)return null;
            if(accountSearchWrapper_map.isEmpty())return null;

        } catch (Exception e) {
            domainsLogs_list.add(new ResellerLogDomain(ResellerEnumRepository.ResellerLogActions.CLICK_GET_ACCOUNTS.name(),
                            e.getMessage(), e.getStackTraceString(), e.getTypeName(), String.valueOf(e.getLineNumber()), accountRequestId, 'Account_Request__c'));                  
        }finally{
            if(!domainsLogs_list.isEmpty()){
                ResellerLogService.publishLogs(domainsLogs_list);
                throw new AuraHandledException(domainsLogs_list[0].message);
            }
        }
        return JSON.serialize(accountSearchWrapper_map.values());

        //acc_wrapper.account_list = AccountSearch.uniqueIdentifierSearch(new List<String>{acc_request[0].Unique_Identifier__c}, acc_request[0].Billing_Country__c, acc_request[0].Name, acc_request[0].Website__c, 'single');
        // for(Account acc : acc_wrapper.account_list)
        //     if(acc.Id_Company__c == acc_request[0].Unique_Identifier__c && (String.isNotBlank(acc.Id_Company__c) && String.isNotBlank(acc_request[0].Unique_Identifier__c)))
        //         acc_wrapper.uniqueKey = true;

        //return JSON.serialize(acc_wrapper);
    }
    
    
     //@last modified on  : 09-30-2022
    //@last modified by  : (eurikesse.ferreira@gympass.com)
    @AuraEnabled
    public static void setAccountInAccountRequest(String accountId, String accountRequestId, Boolean noneAbove, String resellerId, String userId ){
        List<ResellerLogDomain> domainsLogs_list = new List<ResellerLogDomain>();
        List<Account_Request__c> account_request = [ SELECT Id, AccountId__c, Reseller__c, Name, Website__c, Billing_Country__c, Unique_Identifier_Type__c, Unique_Identifier__c, None_of_Above__c, Existing_In_SalesForce__c, Total_number_of_employees__c FROM Account_Request__c WHERE Id = : accountRequestId LIMIT 1];
        
        List<Account> account = [ SELECT Id, Name, Razao_Social__c, Website, BillingCountry, Legal_Document_Type__c, Id_Company__c, NumberOfEmployees FROM Account WHERE Id = : accountId LIMIT 1];
        if(noneAbove)account_request[0].None_of_Above__c = true;
        else{

            String unique_Identifier = account_request[0].Unique_Identifier__c != null ? account_request[0].Unique_Identifier__c : '';
            String website = account_request[0].Website__c != null ? account_request[0].Website__c : '';
            String acc_unique_Identifier = account[0].Id_Company__c != null ? account[0].Id_Company__c : null;
            String acc_website = account[0].Website != null ? account[0].Website : null;
            String acc_name = account[0].Name;
            Integer maxSizeName = 80;
            
            //Se a conta estiver com o website ou identifier null mantem a account request com o input do usuário 
            account_request[0].Website__c = acc_website != null ? acc_website : website;
            account_request[0].Unique_Identifier__c = acc_unique_Identifier != null ? acc_unique_Identifier : unique_Identifier; 

            //Depois altera os valores de website ou identifier na conta
            account[0].Id_Company__c = acc_unique_Identifier != null ? acc_unique_Identifier : unique_Identifier;
            account[0].Website = acc_website != null ? acc_website : website;
            
            if(acc_name.length() > maxSizeName ){ 
                account_request[0].Name = acc_name.substring(0, maxSizeName); account[0].Name = acc_name.substring(0, maxSizeName);
            
            }else{ account_request[0].Name = acc_name;}
            
            account_request[0].AccountId__c = accountId;
            //account_request[0].Name = account[0].name;
            account_request[0].Business_Name__c = account[0].Razao_Social__c;
            account_request[0].Billing_Country__c = account[0].BillingCountry;
            account_request[0].Existing_In_SalesForce__c = true;
        }
        
        if(resellerId == '' || resellerId == null){
             List<User> user = [SELECT Id, AccountId FROM User WHERE Id =: userId LIMIT 1];
              account_request[0].Reseller__c = user[0].AccountId;
        }else{
             account_request[0].Reseller__c = resellerId;
        }

        try {
            update account_request;
            update account;

        } catch (Exception e) {
            domainsLogs_list.add(new ResellerLogDomain(ResellerEnumRepository.ResellerLogActions.CLICK_SET_ACCOUNT_IN_ACCOUNT_REQUEST.name(),
                            e.getMessage(), e.getStackTraceString(), e.getTypeName(), String.valueOf(e.getLineNumber()), accountRequestId, 'Account_Request__c'));
        }finally{
            if(!domainsLogs_list.isEmpty()){
            ResellerLogService.publishLogs(domainsLogs_list); throw new AuraHandledException(domainsLogs_list[0].message);
            }
        }
        
    }

    @AuraEnabled
    public static void createNewAccount(Account_Request__c account_request){
       
            system.debug('Criando account ');             
            Account account = new Account();  
            account.Name = account_request.Name;
            account.Legal_Document_Type__c = account_request.Unique_Identifier_Type__c;
            account.Id_Company__c = account_request.Unique_Identifier__c;
            account.Razao_Social__c = account_request.Name;
            account.Website = account_request.Website__c;
            account.Industry = 'Airlines';                      //incluir o sector no account request
            account.BillingCountry = account_request.Billing_Country__c;
            account.BillingState = account_request.Billing_Country__c == 'Brazil' ? 'São Paulo' : '';
            account.NumberOfEmployees = Integer.valueOf(account_request.Total_number_of_employees__c);
            account.RecordTypeId = ACCOUNT_RECORDTYPEID_RESELLER;    //account.RecordType.Name = 'Indirect Channel'; 

            
            insert account;  
            system.debug('Criando account '+account.OwnerId);      
            account_request.IsNewAccount__c = true;
            account_request.AccountId__c = account.Id;

            
            
            update account_request;
      
              
    }
    
    @AuraEnabled
    public static String getInfo(String requestId){

        
        Account_Request__c request = new Account_Request__c();
        GoNoGoResultWrapper wrapper = new GoNoGoResultWrapper();     
       
        request = [ SELECT Id, Name, Website__c, Billing_Country__c FROM Account_Request__c WHERE Id =: requestId LIMIT 1 ];

        wrapper.nameAccountRequest = request.Name;
        wrapper.website = request.Website__c;
        wrapper.billingCountry = request.Billing_Country__c;
    
        return JSON.serialize(wrapper);

    }

    @AuraEnabled
    public static String getResellerResource(String developerName){

        Reseller_Resource__mdt resource = new Reseller_Resource__mdt();
        GoNoGoResultWrapper wrapper = new GoNoGoResultWrapper();  
        
        resource = [SELECT Id, Static_Resource__c FROM Reseller_Resource__mdt WHERE DeveloperName = :developerName LIMIT 1];

        wrapper.staticResource = resource.Static_Resource__c;
        return JSON.serialize(wrapper);
    }

    @AuraEnabled
    public static String runGoNoGO(String requestId){

        Account_Request__c request = new Account_Request__c();
        List<ResellerLogDomain> domainsLogs_list = new List<ResellerLogDomain>();

        try {
            
            request = AccountRequestRepository.getById(requestId);
            
            ResellerEngineGoNoGO engine = new ResellerEngineGoNoGO(new Set<Id>{requestId});

            if(request.None_of_Above__c == true)createNewAccount(request);
            else{
                engine.runSearchOpportunity(true);

                ResellerActivity reseller = new ResellerActivity(new Set<Id>{requestId});
                reseller.run(true);
                reseller.lastOpportunityActivity(requestId);
            }
            engine = new ResellerEngineGoNoGO(new Set<Id>{requestId});
            engine.noGO();

            request = AccountRequestRepository.getById(requestId);
            
            if(request.Engine_Status__c == 'GO'){
                List<Account> account = [ SELECT Id, NumberOfEmployees FROM Account WHERE Id = :request.AccountId__c LIMIT 1];
                account[0].NumberOfEmployees = integer.valueOf(request.Total_number_of_employees__c);  
                update account;
            }
        } catch (Exception e) {
            system.debug(e);
            domainsLogs_list.add(new ResellerLogDomain(ResellerEnumRepository.ResellerLogActions.CLICK_RUN_GO_NOGO.name(),
                                   e.getMessage(), e.getStackTraceString(), e.getTypeName(), String.valueOf(e.getLineNumber()), requestId, 'Account_Request__c'));
        }finally{
            if(!domainsLogs_list.isEmpty()){
                        system.debug(domainsLogs_list[0].message);
               ResellerLogService.publishLogs(domainsLogs_list);
                throw new AuraHandledException(domainsLogs_list[0].message);  
            }
                
        }

        return request.Engine_Status__c;

    }

    @AuraEnabled
    public static String setTotalPrice(Id requestId){
        GoNoGoResultWrapper wrapper = new GoNoGoResultWrapper();
        Account_Request__c request = new Account_Request__c();
        List<ResellerLogDomain> domainsLogs_list = new List<ResellerLogDomain>();
        List<Opportunity> opportunity_list = new List<Opportunity>();

        try{

            
            request = AccountRequestRepository.getById(requestId);
            wrapper.reasonNoGo = request.Reason_NoGo__c;

            if(request.Engine_Status__c != 'GO'){
                wrapper.go = false;

                new SendCustomNotification().sendNotificationAccountExecutive(request, 'Account_Request_No_Go_Denied', request.Partner_Account__r.OwnerId);

                return JSON.serialize(wrapper);
            }

            wrapper.go = true;

            opportunity_list = [ SELECT Id, Contact__c,Amount FROM Opportunity WHERE Id = :request.OpportunityId__c ];
            if(request.Use_My_Own_Data__c == false){
                createContact(request);
                opportunity_list[0].Contact__c = request.ContactId__c;
            }

            if(opportunity_list.isEmpty())return JSON.serialize(wrapper);

            List<PriceEngineItem__c> priceEngineItem = PriceEngineService.getPriceResellerHub(request);

            //ResellerPriceResponse response = ResellerGetValuesFromOppController.getPriceValues(request.OpportunityId__c);
    
            if(priceEngineItem.isEmpty())return JSON.serialize(wrapper);
            
            wrapper.oppId = request.OpportunityId__c;
            wrapper.totalPrice = priceEngineItem[0].Absolute_Value__c * request.Total_number_of_employees__c; //response.totalPrice;
            wrapper.pricePerElegible = priceEngineItem[0].Absolute_Value__c;//response.pricePerElegible;

            //opportunity_list[0].Amount = response.totalPrice;
            opportunity_list[0].Contact__c = request.ContactId__c;

            request.Total_Price_Request__c = priceEngineItem[0].Absolute_Value__c * request.Total_number_of_employees__c; //response.totalPrice;
            request.Price_per_Eligible__c = priceEngineItem[0].Absolute_Value__c;//response.pricePerElegible;

            update opportunity_list;
            update request;

            new SendCustomNotification().sendNotificationAccountExecutive(request, 'Account_Request_Go_Indirect_Channel', request.Partner_Account__r.OwnerId);
            
            if(request.Joint_Effort__c == true){
                new SendCustomNotification().sendNotificationAccountExecutive(request, 'Account_Request_Go_Direct_Channel', request.AccountId__r.OwnerId);
            }

        } catch (Exception e) {
            domainsLogs_list.add(new ResellerLogDomain(ResellerEnumRepository.ResellerLogActions.CLICK_RUN_GO_NOGO_GET_PRICE.name(),
                               e.getMessage(), e.getStackTraceString(), e.getTypeName(), String.valueOf(e.getLineNumber()), requestId, 'Account_Request__c'));
        }finally{
            if(!domainsLogs_list.isEmpty()){
               ResellerLogService.publishLogs(domainsLogs_list);
                throw new AuraHandledException(domainsLogs_list[0].message);
            }
        }

        return JSON.serialize(wrapper);

    }

    @AuraEnabled
    public static void createLogError(String requestId, String error){
        ResellerLogDomain logDomain = new ResellerLogDomain(ResellerEnumRepository.ResellerLogActions.JS_RUN_GO_NOGO_GET_PRICE.name(),
                                        error, error, 'JS ERROR', null, requestId, 'Account_Request__c');
        ResellerLogService.publishLogs(new List<ResellerLogDomain>{logDomain});                                        
    }

    @AuraEnabled
    public static void setStatusLoading(String massiveRequestId) {
        MassiveAccReqLoad.loadMassiveAccountRequest(massiveRequestId);
        try{
            
            //SendNotifications('This is Test Body', '', userinfo.getUserId(), 'This is Test Message', '0ML1k0000008WEZGA2', new set<String>{UserInfo.getUserId()});
            update new Massive_Account_Request__c(Id = massiveRequestId, Status__c = 'Loading');
        } catch(Exception ex){

            throw new AuraHandledException('Error setStatusLoading loadMassiveAccountRequest');
        }
    }
    //@last modified on  : 09-28-2022
    //@last modified by  : (eurikesse.ferreira@gympass.com)
    @AuraEnabled (cacheable=true)
    public static List<Account> getResellerList(){
        return [SELECT Id, Name
        FROM Account
        WHERE RecordType.Name = 'Indirect Channel'
        AND Type = 'Channel Partner'
        AND IsPartner = true ORDER BY Name ASC];
    }

    @AuraEnabled
    public static String createMassiveAccountRequest(String resellerId, String userId){
        Massive_Account_Request__c massive = new Massive_Account_Request__c();
        if(resellerId == '' || resellerId == null){
            List<User> user = [SELECT Id, AccountId FROM User WHERE Id =: userId LIMIT 1];
            massive.Reseller__c = user[0].AccountId;
        }
        else{
            massive.Reseller__c = resellerId;
        }
        insert massive;
        return massive.Id;
    }

    public class GoNoGoResultWrapper{
        public Boolean go{get;set;}
        public String reasonNoGo{get;set;}
        public String oppId{get;set;}
        public String nameAccountRequest{get;set;}
        public String website{get;set;}
        public String billingCountry{get;set;}
        public String staticResource{get;set;}
        public Decimal totalPrice;
        public Decimal pricePerElegible;
    }

    public class AccountList{
        public List<Account> account_list{get;set;}
        public Boolean uniqueKey{get;set;}
    }

    /*public static void SendNotifications(String strBody, String strSenderId, String strTargetId, String strTitle, String strNotificationId, set<String> setUserIds) {
       
        Messaging.CustomNotification notification = new Messaging.CustomNotification();
        notification.setNotificationTypeId(strNotificationId);
        notification.setTargetId(strTargetId);
        notification.setTitle(strTitle);
        notification.setBody(strBody);
        notification.send(setUserIds);
    }*/

}