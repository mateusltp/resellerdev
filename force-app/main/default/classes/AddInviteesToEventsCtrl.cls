/*
* @Author: Bruno Pinho
* @Date: December/2018
* @Description: Controller to add invitees to Events
*/
public class AddInviteesToEventsCtrl
{
    @AuraEnabled
    public static List<Gympass_Leader_Association__c> getInvitees(String recordId)
    {
        List<Gympass_Leader_Association__c> listGympassLeaderAssociation = [SELECT Id, Gympass_Leader__r.Name, Gympass_Leader__r.Country, Gympass_Leader__r.UserRole.Name, Type__c
                                                                            FROM Gympass_Leader_Association__c 
                                                                            WHERE Gympass_Event__r.Event_Id__c =: recordId
                                                                            ORDER BY Gympass_Leader__r.Name ASC];
        
        return listGympassLeaderAssociation;
    }
    
    @AuraEnabled
    public static String getOwnerId(String recordId)
    {
        List<Event> listMasterEvents = [SELECT Id, OwnerId
                                        FROM Event
                                        WHERE Id =: recordId];
        
        if (listMasterEvents.size() > 0)
            return listMasterEvents[0].OwnerId;
        else
            return null;
    }    
    
    @AuraEnabled
    public static String deleteEventAssociation(String recordId)
    {
        try
        {
            Gympass_Leader_Association__c gympassLeaderAssociation = new Gympass_Leader_Association__c(Id = recordId);
            DELETE gympassLeaderAssociation;
            
            return 'The invitee was deleted';
        } catch (Exception e) {
            if (!Test.isRunningTest())
                throw new AuraHandledException('An error has ocurred while trying to delete the record. Please try it again later!');
            else
                return null;
        }
    }
    
    @AuraEnabled
    public static String addInvitee(String objectName, List<SObject> users, String recordId, String assigneeId)
    {        
        try 
        {
            //List to get data from the master record
            List<Event> listMasterEvents = new List<Event>();
            
            //Lists to set data to insert
            List<Gympass_Event__c> listGympassEventsToInsert = new List<Gympass_Event__c>();
            List<Gympass_Leader_Association__c> listGympassLeaderAssociationToInsert = new List<Gympass_Leader_Association__c>();
            
            //Trying to get data from existing cloned records
            List<Gympass_Event__c> listGympassEvents = [SELECT Id FROM Gympass_Event__c WHERE Event_Id__c =: recordId];
            List<Gympass_Leader_Association__c> listGympassLeaderAssociation = [SELECT Id, Gympass_Leader__c 
                                                                                FROM Gympass_Leader_Association__c 
                                                                                WHERE Gympass_Event__r.Event_Id__c =: recordId];
            
            //According to the records existance
            if (listGympassEvents.size() == 0)
            {
                System.debug('@@ Cloned record found: '+listGympassEvents);
                //Getting data from the standard master record
                listMasterEvents = [SELECT Id, Subject, IsAllDayEvent, OwnerId, Realizado__c, EndDateTime, AccountId, StartDateTime, Type
                                    FROM Event
                                    WHERE Id =: recordId
                                    LIMIT 1];
                                    
                if (listMasterEvents.size() > 0)
                {
                    System.debug('@@ Master record was found: '+listMasterEvents);
                    //Create the records to be inserted
                    listGympassEventsToInsert = generateGympassEvent(listMasterEvents[0], recordId);
                    listGympassLeaderAssociationToInsert = generateGympassLeaderAssociation(users, assigneeId);
                    
                    INSERT listGympassEventsToInsert;
                    INSERT updateGympassLeaderAssociation(listGympassLeaderAssociationToInsert, listGympassEventsToInsert[0].Id);
                    
                    listMasterEvents[0].Gympass_Event__c = listGympassEventsToInsert[0].Id;
                    UPDATE listMasterEvents[0];
                    
                    return 'The invitees were added!';
                } else {
                    System.debug('@@ Master record was not found: '+listMasterEvents);
                    return 'We couldn\'t collect the event data. Please try it again later!';
                }
            } else {
                if (listGympassLeaderAssociation.size() == 0)
                {
                    System.debug('@@ Association records were not found: '+listGympassLeaderAssociation);
                    listGympassLeaderAssociationToInsert = generateGympassLeaderAssociation(users, assigneeId);
                    INSERT updateGympassLeaderAssociation(listGympassLeaderAssociationToInsert, listGympassEvents[0].Id);
                    
                    return 'The invitees were added';
                } else {
                    System.debug('@@ Association records were found: '+listGympassLeaderAssociation);
                    listGympassLeaderAssociationToInsert = handleGympassLeaderAssociation(listGympassLeaderAssociation, users, assigneeId, listGympassEvents[0].Id);
                    INSERT updateGympassLeaderAssociation(listGympassLeaderAssociationToInsert, listGympassEvents[0].Id);
                    
                    return 'The invitee already exists!';
                }
            
            }
        } catch (Exception e) {
            throw new AuraHandledException('Unhandled exception.');
        }
    }
    
    public static List<Gympass_Event__c> generateGympassEvent(Event event, String recordId)
    {
        List<Gympass_Event__c> listGympassEventsToInsert = new List<Gympass_Event__c>();
        
        Gympass_Event__c gympassEvent = new Gympass_Event__c();
        gympassEvent.Name = event.Subject;
        gympassEvent.All_Day_Event__c = event.IsAllDayEvent;
        gympassEvent.Assigned_To__c = event.OwnerId;
        gympassEvent.Done__c = event.Realizado__c;
        gympassEvent.End__c = event.EndDateTime;
        gympassEvent.Related_Account__c = event.AccountId;
        gympassEvent.Start__c = event.StartDateTime;
        gympassEvent.Type__c = event.Type;
        gympassEvent.Event_Id__c = recordId;
        
        listGympassEventsToInsert.add(gympassEvent);
        
        return listGympassEventsToInsert;
    }
    
    public static List<Gympass_Leader_Association__c> generateGympassLeaderAssociation(List<SObject> listUsers, String assigneeId)
    {
        List<Gympass_Leader_Association__c> listGympassLeaderAssociationToInsert = new List<Gympass_Leader_Association__c>();
        
        if (assigneeId != null)
        {
            for (SObject sobj: listUsers)
            {
                if (sobj.Id != assigneeId)
                {
                    Gympass_Leader_Association__c gympassLeaderAssociation = new Gympass_Leader_Association__c();
                    gympassLeaderAssociation.Gympass_Leader__c = sobj.Id;
                    listGympassLeaderAssociationToInsert.add(gympassLeaderAssociation);
                }
            }
                           
            Gympass_Leader_Association__c gympassLeaderAssociation = new Gympass_Leader_Association__c();
            gympassLeaderAssociation.Gympass_Leader__c = assigneeId;
            listGympassLeaderAssociationToInsert.add(gympassLeaderAssociation);
        }
        
        return listGympassLeaderAssociationToInsert;
    }
    
    public static List<Gympass_Leader_Association__c> updateGympassLeaderAssociation(List<Gympass_Leader_Association__c> listGympassLeaderAssociationToInsert, String masterId)
    {
        List<Gympass_Leader_Association__c> listUpdatedGympassLeaderAssociationToInsert = new List<Gympass_Leader_Association__c>();
        
        for (Gympass_Leader_Association__c gla: listGympassLeaderAssociationToInsert)
        {
            gla.Gympass_Event__c = masterId;
            listUpdatedGympassLeaderAssociationToInsert.add(gla);
        }
        
        return listUpdatedGympassLeaderAssociationToInsert;
    }
    
    public static List<Gympass_Leader_Association__c> handleGympassLeaderAssociation(List<Gympass_Leader_Association__c> listGympassLeaderAssociation, List<sObject> listUsers, String assigneeId, string masterGympassEventId)
    {
        for (Gympass_Leader_Association__c gla: listGympassLeaderAssociation)
        {
            for (Integer i=0; i<listUsers.size(); i++)
            {
                if (gla.Gympass_Leader__c == listUsers[i].Id)
                    listUsers.remove(i);
            }
        }
        
        List<Gympass_Leader_Association__c> listGympassLeaderAssociationToInsert = new List<Gympass_Leader_Association__c>();
        
        if (assigneeId != null)
        {
            for (SObject sobj: listUsers)
            {
                if (sobj.Id != assigneeId)
                {
                    Gympass_Leader_Association__c gympassLeaderAssociation = new Gympass_Leader_Association__c();
                    gympassLeaderAssociation.Gympass_Leader__c = sobj.Id;
                    listGympassLeaderAssociationToInsert.add(gympassLeaderAssociation);
                }
            }
        }
        
        return listGympassLeaderAssociationToInsert;
    }
}