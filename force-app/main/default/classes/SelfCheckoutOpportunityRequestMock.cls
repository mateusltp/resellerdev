@IsTest
public class SelfCheckoutOpportunityRequestMock {
    
    public static final String ACCOUNT_UUID = 'aa47c899-7e59-47ad-9ad7-7d0f6310f4ea';
    public static final String GYMPASS_ENTITY_UUID = 'cf37978d-a2ea-489c-903e-4f3eeacef5b3';
    public static final String OPPORTUNITY_UUID = 'b6e45d9d-8f83-4d30-889a-d019f84c7181';

    public static String getRequestBody() {
        return
            '{'+
            '  "client": {'+
            '    "client_id": "' + ACCOUNT_UUID + '",' +
            '    "trade_name": "trade name test",'+
            '    "legal_name": "Macejkovic, Lang and Huel",'+
            '    "lead_source": "inbound",'+
            '    "lead_subsource_sf": "self-checkout",'+
            '    "sales_flow": "new",'+
            '    "legal_document_type": "CNPJ",'+
            '    "legal_document": "98.755.321/0001-09",'+
            '    "phone": "(74) 9335-4814",'+
            '    "billing_firstname":"Gene",'+
            '    "billing_lastname":"Mosciski",'+
            '    "billing_email": "Boyd_Harvey@example.com",'+
            '    "billing_id":"123e4567-e89b-12d3-a456-426655440000",'+
            '    "business_unit": "SMB",'+
            '    "currency_id": "BRL",'+
            '    "number_of_employees": 7,'+
            '    "number_of_employees_rounded": 10,'+
            '    "UTM_url": "",'+
            '    "UTM_campaign": "",'+
            '    "UTM_source": "",'+
            '    "UTM_medium": "",'+
            '    "UTM_content": "",'+
            '    "UTM_term": "",'+
            '    "language": "br",'+
            '    "funnel_stage": "company_info",'+
            '    "coupon_code": "VERAO2022",'+
            '    "coupon_expiration": "2021-04-01T04:00:00.000Z",'+
            '    "coupon_amount":"100.00",'+
            '    "total_value":"500.00",'+
            '    "total_value_w_discount":"400.00",'+
            '    "sku_selected":"",'+
            '    "contact": {'+
            '        "contact_id": "c0a117bb-f1f7-4aff-829b-82c11c9270a3",'+
            '        "first_name": "Gene",'+
            '        "last_name": "Mosciski",'+
            '        "email": "Boyd_Harvey@example.com",'+
            '        "phone": "+55 (42) 1930-7525",'+
            '        "job_title": "CTO"'+
            '        },'+
            '    "address": {'+
            '        "street": "Barros Avenida",'+
            '        "state": "Bahia",'+
            '        "postal_code": "63795-087",'+
            '        "country_id": "BR",'+
            '        "district": "Jardim Tiete",'+
            '        "complements": "Apt.1",'+
            '        "city": "Moraesdo Descoberto"'+
            '    }'+
            ' },'+
            '  "opportunity": {'+
            '    "id": "' + OPPORTUNITY_UUID + '",' +
            '    "stage": "CLOSED_WON",'+
            '    "number_of_employees": 500,'+
            '    "items": {'+
            '      "recurring_fees": ['+
            '        {'+
            '          "id": "aba28ddc-30e4-48f4-beeb-1ac0d354ffee",'+
            '          "payments": ['+
            '            {'+
            '              "id": "f4d6d964-f670-453b-8c97-46e027d90563",'+
            '              "waivers": [],'+
            '              "split_bill_percentage": 100,'+
            '              "eligibles": ['+
            '                {'+
            '                  "id": "94e90575-499b-4bb7-9d27-1fe5af29c2bd",'+
            '                  "welcome_email_permitted": false,'+
            '                  "payroll_deduction_type": "AUTO_APPROVAL",'+
            '                  "payment_method_type": "STANDARD_METHODS",'+
            '                  "paid_by_client_id": "' + ACCOUNT_UUID + '",' +
            '                  "max_dependents": 1,'+
            '                  "managed_by_client_id": "' + ACCOUNT_UUID + '",' +
            '                  "name": "Headquarters",'+
            '                  "identifier_type": "EMAIL",'+
            '                  "exchange_type": "FILE_UPLOAD",'+
            '                  "is_default_item": true,'+
            '                  "communication_permitted_type": "ELIGIBLES"'+
            '                }'+
            '              ],'+
            '              "client_id": "aa47c899-7e59-47ad-9ad7-7d0f6310f4ea"'+
            '            }'+
            '          ],'+
            '          "payment_due_days": 30,'+
            '          "currency_id": "BRL",'+
            '          "variable_price_unit_increase": null,'+
            '          "variable_price_floor": null,'+
            '          "variable_price_ceiling": null,'+
            '          "inflation_adjustment_type": null,'+
            '          "inflation_adjustment_period": null,'+
            '          "flat_unit_price": 19.04,'+
            '          "flat_total_price": 190400,'+
            '          "flat_discount_percentage": null,'+
            '          "flat_baseline_quantity": 500,'+
            '          "flat_baseline_adjustment": false,'+
            '          "fee_type": "ACCESS_FEE",'+
            '          "fee_price_type": "FLAT_PRICE_ELIGIBLE",'+
            '          "discount_price_unit_decrease": null,'+
            '          "discount_price_floor": null,'+
            '          "discount_price_ceiling": null,'+
            '          "cutoff_day": 1,'+
            '          "billing_period": "YEAR",'+
            '          "billing_day": 1'+
            '        }'+
            '      ],'+
            '      "one_time_fees": ['+
            '        {'+
            '          "id": "0b3f9318-b995-4b47-acdd-7a3ce20d2062",'+
            '          "payments": ['+
            '            {'+
            '              "id": "b205a63e-6914-4e32-8dce-36bb8f938c76",'+
            '              "waivers": [],'+
            '              "split_bill_percentage": 100,'+
            '              "eligibles": ['+
            '                {'+
            '                  "id": "ac992450-941c-4cab-b20e-5411b735b114",'+
            '                  "welcome_email_permitted": false,'+
            '                  "payroll_deduction_type": "AUTO_APPROVAL",'+
            '                  "payment_method_type": "STANDARD_METHODS",'+
            '                  "paid_by_client_id": "' + ACCOUNT_UUID + '",' +
            '                  "max_dependents": 1,'+
            '                  "managed_by_client_id": "' + ACCOUNT_UUID + '",' +
            '                  "name": "Headquarters",'+
            '                  "identifier_type": "EMAIL",'+
            '                  "exchange_type": "FILE_UPLOAD",'+
            '                  "is_default_item": true,'+
            '                  "communication_permitted_type": "ELIGIBLES"'+
            '                }'+
            '              ],'+
            '              "client_id": "aa47c899-7e59-47ad-9ad7-7d0f6310f4ea"'+
            '            }'+
            '          ],'+
            '          "payment_due_days": 30,'+
            '          "currency_id": "BRL",'+
            '          "unit_price": 3.04,'+
            '          "total_price": 30400,'+
            '          "fee_type": "SETUP_FEE",'+
            '          "discount_percentage": null,'+
            '          "billing_date": "2021-04-01T04:00:00.000Z",'+
            '          "baseline_quantity": 500'+
            '        }'+
            '      ]'+
            '    },'+
            '    "gympass_entity_id": "' + GYMPASS_ENTITY_UUID + '",' +
            '    "end_date": "2022-05-01T00:00:00.000Z",'+
            '    "begin_date": "2021-04-30T00:00:00.000Z"'+
            '  }'+
            '}';
    }

    public static SelfCheckoutOpportunityRequest getMock() {
        return (SelfCheckoutOpportunityRequest) JSON.deserialize(getRequestBody(), SelfCheckoutOpportunityRequest.class);
    }

    public static String getRequestBodyWithContactWithNoFields() {
        return
        '{'+
        '  "client": {'+
        '    "client_id": "' + ACCOUNT_UUID + '",' +
        '    "trade_name": "trade name test",'+
        '    "legal_name": "Macejkovic, Lang and Huel",'+
        '    "lead_source": "inbound",'+
        '    "lead_subsource_sf": "self-checkout",'+
        '    "sales_flow": "new",'+
        '    "legal_document_type": "CNPJ",'+
        '    "legal_document": "98.755.321/0001-09",'+
        '    "phone": "(74) 9335-4814",'+
        '    "billing_firstname":"Gene",'+
        '    "billing_lastname":"Mosciski",'+
        '    "billing_email": "Boyd_Harvey@example.com",'+
        '    "billing_id":"123e4567-e89b-12d3-a456-426655440000",'+
        '    "business_unit": "SMB",'+
        '    "currency_id": "BRL",'+
        '    "number_of_employees": 7,'+
        '    "number_of_employees_rounded": 10,'+
        '    "UTM_url": "",'+
        '    "UTM_campaign": "",'+
        '    "UTM_source": "",'+
        '    "UTM_medium": "",'+
        '    "UTM_content": "",'+
        '    "UTM_term": "",'+
        '    "language": "br",'+
        '    "funnel_stage": "company_info",'+
        '    "coupon_code": "VERAO2022",'+
        '    "coupon_expiration": "2021-04-01T04:00:00.000Z",'+
        '    "coupon_amount":"100.00",'+
        '    "total_value":"500.00",'+
        '    "total_value_w_discount":"400.00",'+
        '    "sku_selected":"",'+
        '    "contact": {'+
        '        },'+
        '    "address": {'+
        '        "street": "Barros Avenida",'+
        '        "state": "Bahia",'+
        '        "postal_code": "63795-087",'+
        '        "country_id": "BR",'+
        '        "district": "Jardim Tiete",'+
        '        "complements": "Apt.1",'+
        '        "city": "Moraesdo Descoberto"'+
        '    }'+
        ' },'+
        '  "opportunity": {'+
        '    "id": "' + OPPORTUNITY_UUID + '",' +
        '    "stage": "CLOSED_WON",'+
        '    "number_of_employees": 500,'+
        '    "items": {'+
        '      "recurring_fees": ['+
        '        {'+
        '          "id": "aba28ddc-30e4-48f4-beeb-1ac0d354ffee",'+
        '          "payments": ['+
        '            {'+
        '              "id": "f4d6d964-f670-453b-8c97-46e027d90563",'+
        '              "waivers": [],'+
        '              "split_bill_percentage": 100,'+
        '              "eligibles": ['+
        '                {'+
        '                  "id": "94e90575-499b-4bb7-9d27-1fe5af29c2bd",'+
        '                  "welcome_email_permitted": false,'+
        '                  "payroll_deduction_type": "AUTO_APPROVAL",'+
        '                  "payment_method_type": "STANDARD_METHODS",'+
        '                  "paid_by_client_id": "' + ACCOUNT_UUID + '",' +
        '                  "max_dependents": 1,'+
        '                  "managed_by_client_id": "' + ACCOUNT_UUID + '",' +
        '                  "name": "Headquarters",'+
        '                  "identifier_type": "EMAIL",'+
        '                  "exchange_type": "FILE_UPLOAD",'+
        '                  "is_default_item": true,'+
        '                  "communication_permitted_type": "ELIGIBLES"'+
        '                }'+
        '              ],'+
        '              "client_id": "aa47c899-7e59-47ad-9ad7-7d0f6310f4ea"'+
        '            }'+
        '          ],'+
        '          "payment_due_days": 30,'+
        '          "currency_id": "BRL",'+
        '          "variable_price_unit_increase": null,'+
        '          "variable_price_floor": null,'+
        '          "variable_price_ceiling": null,'+
        '          "inflation_adjustment_type": null,'+
        '          "inflation_adjustment_period": null,'+
        '          "flat_unit_price": 19.04,'+
        '          "flat_total_price": 190400,'+
        '          "flat_discount_percentage": null,'+
        '          "flat_baseline_quantity": 500,'+
        '          "flat_baseline_adjustment": false,'+
        '          "fee_type": "ACCESS_FEE",'+
        '          "fee_price_type": "FLAT_PRICE_ELIGIBLE",'+
        '          "discount_price_unit_decrease": null,'+
        '          "discount_price_floor": null,'+
        '          "discount_price_ceiling": null,'+
        '          "cutoff_day": 1,'+
        '          "billing_period": "YEAR",'+
        '          "billing_day": 1'+
        '        }'+
        '      ],'+
        '      "one_time_fees": ['+
        '        {'+
        '          "id": "0b3f9318-b995-4b47-acdd-7a3ce20d2062",'+
        '          "payments": ['+
        '            {'+
        '              "id": "b205a63e-6914-4e32-8dce-36bb8f938c76",'+
        '              "waivers": [],'+
        '              "split_bill_percentage": 100,'+
        '              "eligibles": ['+
        '                {'+
        '                  "id": "ac992450-941c-4cab-b20e-5411b735b114",'+
        '                  "welcome_email_permitted": false,'+
        '                  "payroll_deduction_type": "AUTO_APPROVAL",'+
        '                  "payment_method_type": "STANDARD_METHODS",'+
        '                  "paid_by_client_id": "' + ACCOUNT_UUID + '",' +
        '                  "max_dependents": 1,'+
        '                  "managed_by_client_id": "' + ACCOUNT_UUID + '",' +
        '                  "name": "Headquarters",'+
        '                  "identifier_type": "EMAIL",'+
        '                  "exchange_type": "FILE_UPLOAD",'+
        '                  "is_default_item": true,'+
        '                  "communication_permitted_type": "ELIGIBLES"'+
        '                }'+
        '              ],'+
        '              "client_id": "aa47c899-7e59-47ad-9ad7-7d0f6310f4ea"'+
        '            }'+
        '          ],'+
        '          "payment_due_days": 30,'+
        '          "currency_id": "BRL",'+
        '          "unit_price": 3.04,'+
        '          "total_price": 30400,'+
        '          "fee_type": "SETUP_FEE",'+
        '          "discount_percentage": null,'+
        '          "billing_date": "2021-04-01T04:00:00.000Z",'+
        '          "baseline_quantity": 500'+
        '        }'+
        '      ]'+
        '    },'+
        '    "gympass_entity_id": "' + GYMPASS_ENTITY_UUID + '",' +
        '    "end_date": "2022-05-01T00:00:00.000Z",'+
        '    "begin_date": "2021-04-30T00:00:00.000Z"'+
        '  }'+
        '}';
    }

    public static SelfCheckoutOpportunityRequest getMockWithContactWithNoFields() {
        return (SelfCheckoutOpportunityRequest) JSON.deserialize(getRequestBodyWithContactWithNoFields(), SelfCheckoutOpportunityRequest.class);
    }
    
    public static String getRequestBodyWithClientWithoutTradeName() {
        return
        '{'+
        '  "client": {'+
        '    "client_id": "' + ACCOUNT_UUID + '",' +
        '    "trade_name": "",'+
        '    "legal_name": "Macejkovic, Lang and Huel",'+
        '    "lead_source": "inbound",'+
        '    "lead_subsource_sf": "self-checkout",'+
        '    "sales_flow": "new",'+
        '    "legal_document_type": "CNPJ",'+
        '    "legal_document": "98.755.321/0001-09",'+
        '    "phone": "(74) 9335-4814",'+
        '    "billing_firstname":"Gene",'+
        '    "billing_lastname":"Mosciski",'+
        '    "billing_email": "Boyd_Harvey@example.com",'+
        '    "billing_id":"123e4567-e89b-12d3-a456-426655440000",'+
        '    "business_unit": "SMB",'+
        '    "currency_id": "BRL",'+
        '    "number_of_employees": 7,'+
        '    "number_of_employees_rounded": 10,'+
        '    "UTM_url": "",'+
        '    "UTM_campaign": "",'+
        '    "UTM_source": "",'+
        '    "UTM_medium": "",'+
        '    "UTM_content": "",'+
        '    "UTM_term": "",'+
        '    "language": "br",'+
        '    "funnel_stage": "company_info",'+
        '    "coupon_code": "VERAO2022",'+
        '    "coupon_expiration": "2021-04-01T04:00:00.000Z",'+
        '    "coupon_amount":"100.00",'+
        '    "total_value":"500.00",'+
        '    "total_value_w_discount":"400.00",'+
        '    "sku_selected":"",'+
        '    "contact": {'+
        '        "contact_id": "c0a117bb-f1f7-4aff-829b-82c11c9270a3",'+
        '        "first_name": "Gene",'+
        '        "last_name": "Mosciski",'+
        '        "email": "Boyd_Harvey@example.com",'+
        '        "phone": "+55 (42) 1930-7525",'+
        '        "job_title": "CTO"'+
        '        },'+
        '    "address": {'+
        '        "street": "Barros Avenida",'+
        '        "state": "Bahia",'+
        '        "postal_code": "63795-087",'+
        '        "country_id": "BR",'+
        '        "district": "Jardim Tiete",'+
        '        "complements": "Apt.1",'+
        '        "city": "Moraesdo Descoberto"'+
        '    }'+
        ' },'+
        '  "opportunity": {'+
        '    "id": "' + OPPORTUNITY_UUID + '",' +
        '    "stage": "CLOSED_WON",'+
        '    "number_of_employees": 500,'+
        '    "items": {'+
        '      "recurring_fees": ['+
        '        {'+
        '          "id": "aba28ddc-30e4-48f4-beeb-1ac0d354ffee",'+
        '          "payments": ['+
        '            {'+
        '              "id": "f4d6d964-f670-453b-8c97-46e027d90563",'+
        '              "waivers": [],'+
        '              "split_bill_percentage": 100,'+
        '              "eligibles": ['+
        '                {'+
        '                  "id": "94e90575-499b-4bb7-9d27-1fe5af29c2bd",'+
        '                  "welcome_email_permitted": false,'+
        '                  "payroll_deduction_type": "AUTO_APPROVAL",'+
        '                  "payment_method_type": "STANDARD_METHODS",'+
        '                  "paid_by_client_id": "' + ACCOUNT_UUID + '",' +
        '                  "max_dependents": 1,'+
        '                  "managed_by_client_id": "' + ACCOUNT_UUID + '",' +
        '                  "name": "Headquarters",'+
        '                  "identifier_type": "EMAIL",'+
        '                  "exchange_type": "FILE_UPLOAD",'+
        '                  "is_default_item": true,'+
        '                  "communication_permitted_type": "ELIGIBLES"'+
        '                }'+
        '              ],'+
        '              "client_id": "aa47c899-7e59-47ad-9ad7-7d0f6310f4ea"'+
        '            }'+
        '          ],'+
        '          "payment_due_days": 30,'+
        '          "currency_id": "BRL",'+
        '          "variable_price_unit_increase": null,'+
        '          "variable_price_floor": null,'+
        '          "variable_price_ceiling": null,'+
        '          "inflation_adjustment_type": null,'+
        '          "inflation_adjustment_period": null,'+
        '          "flat_unit_price": 19.04,'+
        '          "flat_total_price": 190400,'+
        '          "flat_discount_percentage": null,'+
        '          "flat_baseline_quantity": 500,'+
        '          "flat_baseline_adjustment": false,'+
        '          "fee_type": "ACCESS_FEE",'+
        '          "fee_price_type": "FLAT_PRICE_ELIGIBLE",'+
        '          "discount_price_unit_decrease": null,'+
        '          "discount_price_floor": null,'+
        '          "discount_price_ceiling": null,'+
        '          "cutoff_day": 1,'+
        '          "billing_period": "YEAR",'+
        '          "billing_day": 1'+
        '        }'+
        '      ],'+
        '      "one_time_fees": ['+
        '        {'+
        '          "id": "0b3f9318-b995-4b47-acdd-7a3ce20d2062",'+
        '          "payments": ['+
        '            {'+
        '              "id": "b205a63e-6914-4e32-8dce-36bb8f938c76",'+
        '              "waivers": [],'+
        '              "split_bill_percentage": 100,'+
        '              "eligibles": ['+
        '                {'+
        '                  "id": "ac992450-941c-4cab-b20e-5411b735b114",'+
        '                  "welcome_email_permitted": false,'+
        '                  "payroll_deduction_type": "AUTO_APPROVAL",'+
        '                  "payment_method_type": "STANDARD_METHODS",'+
        '                  "paid_by_client_id": "' + ACCOUNT_UUID + '",' +
        '                  "max_dependents": 1,'+
        '                  "managed_by_client_id": "' + ACCOUNT_UUID + '",' +
        '                  "name": "Headquarters",'+
        '                  "identifier_type": "EMAIL",'+
        '                  "exchange_type": "FILE_UPLOAD",'+
        '                  "is_default_item": true,'+
        '                  "communication_permitted_type": "ELIGIBLES"'+
        '                }'+
        '              ],'+
        '              "client_id": "aa47c899-7e59-47ad-9ad7-7d0f6310f4ea"'+
        '            }'+
        '          ],'+
        '          "payment_due_days": 30,'+
        '          "currency_id": "BRL",'+
        '          "unit_price": 3.04,'+
        '          "total_price": 30400,'+
        '          "fee_type": "SETUP_FEE",'+
        '          "discount_percentage": null,'+
        '          "billing_date": "2021-04-01T04:00:00.000Z",'+
        '          "baseline_quantity": 500'+
        '        }'+
        '      ]'+
        '    },'+
        '    "gympass_entity_id": "' + GYMPASS_ENTITY_UUID + '",' +
        '    "end_date": "2022-05-01T00:00:00.000Z",'+
        '    "begin_date": "2021-04-30T00:00:00.000Z"'+
        '  }'+
        '}';
    }
    
    public static SelfCheckoutOpportunityRequest getMockWithClientWithoutTradeName() {
        return (SelfCheckoutOpportunityRequest) JSON.deserialize(getRequestBodyWithClientWithoutTradeName(), SelfCheckoutOpportunityRequest.class);
    }

    public static String getRequestBodyWithDifferentContacts() {

        return 
        '{'+
        '  "client": {'+
        '    "client_id": "' + ACCOUNT_UUID + '",' +
        '    "trade_name": "trade name test",'+
        '    "legal_name": "Macejkovic, Lang and Huel",'+
        '    "lead_source": "inbound",'+
        '    "lead_subsource_sf": "self-checkout",'+
        '    "sales_flow": "new",'+
        '    "legal_document_type": "CNPJ",'+
        '    "legal_document": "98.755.321/0001-09",'+
        '    "phone": "(74) 9335-4814",'+
        '    "billing_firstname":"Gene_other",'+
        '    "billing_lastname":"Mosciski",'+
        '    "billing_email": "Boyd_Harvey_other@example.com",'+
        '    "billing_id":"123e4567-e89b-12d3-a456-426655440000",'+
        '    "business_unit": "SMB",'+
        '    "currency_id": "BRL",'+
        '    "number_of_employees": 7,'+
        '    "number_of_employees_rounded": 10,'+
        '    "UTM_url": "",'+
        '    "UTM_campaign": "",'+
        '    "UTM_source": "",'+
        '    "UTM_medium": "",'+
        '    "UTM_content": "",'+
        '    "UTM_term": "",'+
        '    "language": "br",'+
        '    "funnel_stage": "company_info",'+
        '    "coupon_code": "VERAO2022",'+
        '    "coupon_expiration": "2021-04-01T04:00:00.000Z",'+
        '    "coupon_amount":"100.00",'+
        '    "total_value":"500.00",'+
        '    "total_value_w_discount":"400.00",'+
        '    "sku_selected":"",'+
        '    "contact": {'+
        '        "contact_id": "c0a117bb-f1f7-4aff-829b-82c11c9270a3",'+
        '        "first_name": "Gene",'+
        '        "last_name": "Mosciski",'+
        '        "email": "Boyd_Harvey@example.com",'+
        '        "phone": "+55 (42) 1930-7525",'+
        '        "job_title": "CTO"'+
        '        },'+
        '    "address": {'+
        '        "street": "Barros Avenida",'+
        '        "state": "Bahia",'+
        '        "postal_code": "63795-087",'+
        '        "country_id": "BR",'+
        '        "district": "Jardim Tiete",'+
        '        "complements": "Apt.1",'+
        '        "city": "Moraesdo Descoberto"'+
        '    }'+
        ' },'+
        '  "opportunity": {'+
        '    "id": "' + OPPORTUNITY_UUID + '",' +
        '    "stage": "CLOSED_WON",'+
        '    "number_of_employees": 500,'+
        '    "items": {'+
        '      "recurring_fees": ['+
        '        {'+
        '          "id": "aba28ddc-30e4-48f4-beeb-1ac0d354ffee",'+
        '          "payments": ['+
        '            {'+
        '              "id": "f4d6d964-f670-453b-8c97-46e027d90563",'+
        '              "waivers": [],'+
        '              "split_bill_percentage": 100,'+
        '              "eligibles": ['+
        '                {'+
        '                  "id": "94e90575-499b-4bb7-9d27-1fe5af29c2bd",'+
        '                  "welcome_email_permitted": false,'+
        '                  "payroll_deduction_type": "AUTO_APPROVAL",'+
        '                  "payment_method_type": "STANDARD_METHODS",'+
        '                  "paid_by_client_id": "' + ACCOUNT_UUID + '",' +
        '                  "max_dependents": 1,'+
        '                  "managed_by_client_id": "' + ACCOUNT_UUID + '",' +
        '                  "name": "Headquarters",'+
        '                  "identifier_type": "EMAIL",'+
        '                  "exchange_type": "FILE_UPLOAD",'+
        '                  "is_default_item": true,'+
        '                  "communication_permitted_type": "ELIGIBLES"'+
        '                }'+
        '              ],'+
        '              "client_id": "aa47c899-7e59-47ad-9ad7-7d0f6310f4ea"'+
        '            }'+
        '          ],'+
        '          "payment_due_days": 30,'+
        '          "currency_id": "BRL",'+
        '          "variable_price_unit_increase": null,'+
        '          "variable_price_floor": null,'+
        '          "variable_price_ceiling": null,'+
        '          "inflation_adjustment_type": null,'+
        '          "inflation_adjustment_period": null,'+
        '          "flat_unit_price": 19.04,'+
        '          "flat_total_price": 190400,'+
        '          "flat_discount_percentage": null,'+
        '          "flat_baseline_quantity": 500,'+
        '          "flat_baseline_adjustment": false,'+
        '          "fee_type": "ACCESS_FEE",'+
        '          "fee_price_type": "FLAT_PRICE_ELIGIBLE",'+
        '          "discount_price_unit_decrease": null,'+
        '          "discount_price_floor": null,'+
        '          "discount_price_ceiling": null,'+
        '          "cutoff_day": 1,'+
        '          "billing_period": "YEAR",'+
        '          "billing_day": 1'+
        '        }'+
        '      ],'+
        '      "one_time_fees": ['+
        '        {'+
        '          "id": "0b3f9318-b995-4b47-acdd-7a3ce20d2062",'+
        '          "payments": ['+
        '            {'+
        '              "id": "b205a63e-6914-4e32-8dce-36bb8f938c76",'+
        '              "waivers": [],'+
        '              "split_bill_percentage": 100,'+
        '              "eligibles": ['+
        '                {'+
        '                  "id": "ac992450-941c-4cab-b20e-5411b735b114",'+
        '                  "welcome_email_permitted": false,'+
        '                  "payroll_deduction_type": "AUTO_APPROVAL",'+
        '                  "payment_method_type": "STANDARD_METHODS",'+
        '                  "paid_by_client_id": "' + ACCOUNT_UUID + '",' +
        '                  "max_dependents": 1,'+
        '                  "managed_by_client_id": "' + ACCOUNT_UUID + '",' +
        '                  "name": "Headquarters",'+
        '                  "identifier_type": "EMAIL",'+
        '                  "exchange_type": "FILE_UPLOAD",'+
        '                  "is_default_item": true,'+
        '                  "communication_permitted_type": "ELIGIBLES"'+
        '                }'+
        '              ],'+
        '              "client_id": "aa47c899-7e59-47ad-9ad7-7d0f6310f4ea"'+
        '            }'+
        '          ],'+
        '          "payment_due_days": 30,'+
        '          "currency_id": "BRL",'+
        '          "unit_price": 3.04,'+
        '          "total_price": 30400,'+
        '          "fee_type": "SETUP_FEE",'+
        '          "discount_percentage": null,'+
        '          "billing_date": "2021-04-01T04:00:00.000Z",'+
        '          "baseline_quantity": 500'+
        '        }'+
        '      ]'+
        '    },'+
        '    "gympass_entity_id": "' + GYMPASS_ENTITY_UUID + '",' +
        '    "end_date": "2022-05-01T00:00:00.000Z",'+
        '    "begin_date": "2021-04-30T00:00:00.000Z"'+
        '  }'+
        '}';

    }

    public static SelfCheckoutOpportunityRequest getMockWithDifferentContacts() {
        return (SelfCheckoutOpportunityRequest) JSON.deserialize(getRequestBodyWithDifferentContacts(), SelfCheckoutOpportunityRequest.class);
    }

}