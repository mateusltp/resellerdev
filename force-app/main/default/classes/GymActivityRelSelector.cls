/**
 * @description       : 
 * @author            : gft.jorge.stevaux@ext.gympass.com
 * @group             : 
 * @last modified on  : 01-03-2022
 * @last modified by  : gft.jorge.stevaux@ext.gympass.com
**/
public inherited sharing class GymActivityRelSelector extends ApplicationSelector{

    public List<Schema.SObjectField> getSObjectFieldList() {
		return new List<Schema.SObjectField> {
			Gym_Activity_Relationship__c.Id,
			Gym_Activity_Relationship__c.Name
		};
	}

	public Schema.SObjectType getSObjectType() {
		return Gym_Activity_Relationship__c.sObjectType;
	}

    public List<Gym_Activity_Relationship__c> selectById(Set<Id> ids) {
		return (List<Gym_Activity_Relationship__c>) super.selectSObjectsById(ids);
	}

	public List<Gym_Activity_Relationship__c> selectAllGymActivitiesRel (){

		return new List<Gym_Activity_Relationship__c>( (List<Gym_Activity_Relationship__c>)Database.query(
			newQueryFactory().
				selectField(Gym_Activity_Relationship__c.Id).
				selectField(Gym_Activity_Relationship__c.Name).
				setLimit(500).
				toSOQL()));
	}

}