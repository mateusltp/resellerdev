/**
 * @description       : 
 * @author            : ext.gft.pedro.oliveira@gympass.com
 * @group             : 
 * @last modified on  : 04-08-2022
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/
public with sharing class OpsSetupFormSelector extends ApplicationSelector {
    public List<Schema.SObjectField> getSObjectFieldList() {
		return new List<Schema.SObjectField> {
            Ops_Setup_Validation_Form__c.Id,
            Ops_Setup_Validation_Form__c.Name
		};
	}

    public Schema.SObjectType getSObjectType() {
		return 	Ops_Setup_Validation_Form__c.sObjectType;
	}

    public List<Ops_Setup_Validation_Form__c> selectById(Set<Id> ids) {
		return (List<Ops_Setup_Validation_Form__c>) super.selectSObjectsById(ids);
	}
	
	public Map<Id,Ops_Setup_Validation_Form__c> selectOpsSetupFormForPartner (Id aOpportunityId, Set<Id> accountsId ){
		
		Map<Id,Ops_Setup_Validation_Form__c> lMapIdOpsSetup = new Map<Id,Ops_Setup_Validation_Form__c>();
		for(Ops_Setup_Validation_Form__c opsForm : (List<Ops_Setup_Validation_Form__c>) Database.query(
																						newQueryFactory(false).
																						selectField(Ops_Setup_Validation_Form__c.Id).
																						selectField(Ops_Setup_Validation_Form__c.RecordTypeId).
																						selectField(Ops_Setup_Validation_Form__c.Opportunity__c).
																						selectField(Ops_Setup_Validation_Form__c.Account_Information__c).
																						selectField(Ops_Setup_Validation_Form__c.Name).
																						selectField(Ops_Setup_Validation_Form__c.OwnerId).
																						setCondition('Account_Information__c IN: accountsId AND Opportunity__c =: aOpportunityId ').
																						toSOQL())){

			lMapIdOpsSetup.put(opsForm.Account_Information__c,opsForm);
		}
		return lMapIdOpsSetup;
		}
	}