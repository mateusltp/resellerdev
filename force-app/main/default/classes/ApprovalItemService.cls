/**
 * @description       : Approval Item Object methods logic
 * @author            : Tiago Ribeiro
 * @group             : Corporate
 * @last modified on  : 02-14-2022
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                              Modification
 * 1.0   01-31-2022   Tiago Ribeiro                       Initial Version
**/
public without sharing class ApprovalItemService {

    public List<Approval_Item__c> addApprovalItem(Id caseId, Map<String, String> mapApprovalItemValueField, Map<String, String> mapApprovedItems){
        List<Approval_Item__c> listApprovalItems = new List<Approval_Item__c>();
		
        if(mapApprovedItems == null) return listApprovalItems;
 		   
        for(String key : mapApprovedItems.keySet()){
            Boolean isApproved = Boolean.valueOf(mapApprovedItems.get(key));
            if(mapApprovalItemValueField.containsKey(key) && !isApproved){
                String value = mapApprovalItemValueField.get(key);
                listApprovalItems.add(generateApprovalItem(caseId, key, value, 'Open'));   
            }
        }
        
        return listApprovalItems;
    }

    public Approval_Item__c generateApprovalItem(Id caseId, String name, String value, String approvalStatus){
        Approval_Item__c approvalItem = new Approval_Item__c(
            Name = name,
            Case__c = caseId,
            Approval_Status__c = approvalStatus,
            Value__c = value
        );
        return approvalItem;
    }

    public void createApprovalItems(List<Approval_Item__c> listApprovalItems){
        try{
            insert listApprovalItems;
        }catch(Exception e){
            throw e;
        }
        
    }
}