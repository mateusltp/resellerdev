/**
 * @File Name          : OpportunityTriggerHandler.cls
 * @Description        :
 * @Author             : GEPI@GFT.com
 * @Group              :
 * @Last Modified By   : gft.samuel.silva@ext.gympass.com
 * @Last Modified On   : 08-22-2022
 * @Modification Log   :
 * Ver       Date            Author              Modification
 * 1.0    15/04/2020   GEPI@GFT.com     Initial Version
 **/
public without sharing class OpportunityTriggerHandler extends TriggerHandler {
  public override void beforeUpdate() {
    new ValidateOpportunityAccount().execute();

    OpportunityTriggerValOppRelHelper oppRelHelper = new OpportunityTriggerValOppRelHelper();
    oppRelHelper.updateOppWithRelated();
    oppRelHelper.validateStepsTowardsSuccessForPartners();
    oppRelHelper.validateEnablersNewFlow(
      (Map<Id, Opportunity>) Trigger.newMap,
      (Map<Id, Opportunity>) Trigger.oldMap
    );

    //new OpportunityTriggerCurrencyHelper().setCurrencyFieldFromMetadata();
     //OpportunityService.validateOpportunityForTagus((List<Opportunity>) Trigger.new, (Map<Id, Opportunity>) Trigger.oldMap); 

    OpportunityService.validateW9Form(
      (List<Opportunity>) Trigger.new,
      (Map<Id, Opportunity>) Trigger.oldMap
    );
	
    OpportunityService.validate5Photos(
      (List<Opportunity>) Trigger.new,
      (Map<Id, Opportunity>) Trigger.oldMap
    );

    OpportunityService.validateOpportunityStage(
      (Map<Id, Opportunity>) Trigger.newMap,
      (Map<Id, Opportunity>) Trigger.oldMap
    );

    /*Eric Torres
     remove the process from process builder and put in a class/trigger */
    FastTrackPBCreateNewOppUpdateAcc.beforeUpdate(
      Trigger.new,
      (Map<Id, Opportunity>) Trigger.oldMap
    );
  }

  public override void beforeDelete() {
    new OpportunityTriggerAccOppRelHelper().deleteOppRelatedToAccounts();
  }

  public override void afterUpdate() {

    new PublishPipefyCreateCardTA().execute();
    new OpportunityTriggerAccProdHelper().updateGymWithProductClasses();

    new OpsFormValidationHelper().opsFormValidation();
    new PartnerAccountStatusHelper()
      .addCancelationReasonAndBlockList(
        (List<Opportunity>) Trigger.new,
        (Map<Id, Opportunity>) Trigger.oldMap
      );

    PlatformEventsHelper.publishGenerateSalesOrderEvent(
      (Map<Id, Opportunity>) Trigger.oldMap,
      (Map<Id, Opportunity>) Trigger.newMap
    );

    new OpportunityTriggerContractCancellation()
      .createContractCancellationCase();
      
    new DealHierarchyStatusCtrl().validateGpStatus(Trigger.new, Trigger.oldMap);

    new CreateSalesPartnerOrder().execute();

    new OpportunityTriggerSmbHelper()
      .fillRelatedObjsFields(
        (Map<Id, Opportunity>) Trigger.oldMap,
        (Map<Id, Opportunity>) Trigger.newMap
      );
    OpportunityAccountTeamHelper opportunityAccountTeamHelper = new OpportunityAccountTeamHelper();
    opportunityAccountTeamHelper.changeAccountOwnerToCsUser(
      (Map<Id, Opportunity>) Trigger.oldMap,
      (Map<Id, Opportunity>) Trigger.newMap
    );
    opportunityAccountTeamHelper.assignUserAccountTeamMembersOnLaunch(
      (Map<Id, Opportunity>) Trigger.oldMap,
      (Map<Id, Opportunity>) Trigger.newMap
    );

    OpportunityService.syncProductItemsCurrency((Map<Id, Opportunity>) Trigger.newMap, (Map<Id,Opportunity>) Trigger.oldMap);
    OpportunityService.createSetups((List<Opportunity>)trigger.new, (Map<Id,Opportunity>) trigger.oldMap);
    new SKUDealDeskHandler()
      .checkDealDeskFieldsApproved(
        (Map<Id, Opportunity>) Trigger.oldMap,
        (List<Opportunity>) Trigger.new
      );

    new SKUDealDeskHandler()
      .checkEnablersNeedApproval(
        (Map<Id, Opportunity>) Trigger.oldMap,
        (List<Opportunity>) Trigger.new
      );

    new CreateSalesOrder().execute();

    /*Eric Torres
     remove the process from process builder and put in a class/trigger */
    FastTrackPBCreateNewOppUpdateAcc.afterUpdate(
      Trigger.new,
      (Map<Id, Opportunity>) Trigger.oldMap
    );

    OpportunityTriggerUpdateEnablersAccount updatingAllEnablersOnAccount = new OpportunityTriggerUpdateEnablersAccount(
      (List<Opportunity>) Trigger.new,
      (Map<Id, Opportunity>) Trigger.oldMap
    );
    updatingAllEnablersOnAccount.run();

    new SmbSkuUpdateProducts()
      .execute(
        (Map<Id, Opportunity>) Trigger.oldMap,
        (List<Opportunity>) Trigger.new
      );

    new SKUQuantityUpdateHandler()
      .updateQuantityAndPrices(
        (List<Opportunity>) Trigger.new,
        (Map<Id, Opportunity>) Trigger.oldMap
      );

    new SKUWaiverHandler()
      .updateWaiversOnLaunch(
        (List<Opportunity>) Trigger.new,
        (Map<Id, Opportunity>) Trigger.oldMap
      );
      
    new ParseEnterpriseDealsToClientDealsTA().execute();
    new UpsertClientOrderTA().execute();
    
    //-- Reseller-specific record updates/actions
    ResellerSKUUpdateProducts.updateQuantityAndPrices((Map<Id, Opportunity>) Trigger.oldMap, Trigger.new);

    OpportunityTriggerCommercialConditions updateCommercialConditions = new OpportunityTriggerCommercialConditions((List<Opportunity>) trigger.new, (Map<Id,Opportunity>) trigger.oldMap);
    updateCommercialConditions.run();
  }

  public override void beforeInsert() {
    new OpportunityTriggerUserManagerHelper().setUserManagerInOpp();

    new OpportunityTriggerCurrencyHelper().setCurrencyFieldFromMetadata();
    new OpportunityTriggerPricebookHelper().setPricebookFromAccountCountry();
    OpportunityService.setOpportunityTypeForOpportunitiesFromConvertedLeads(Trigger.new);
    FastTrackPBCreateNewOppUpdateAcc.beforeInsert(Trigger.new);
  }

  public override void afterInsert() {
    new OpportunityTriggerAccOppRelHelper()
      .createAccountOpportunityRelationship();
    new OpportunityTriggerOppRenegotiationHelper(Trigger.New);
    FastTrackPBCreateNewOppUpdateAcc.AfterInsert(Trigger.new);
  }
}