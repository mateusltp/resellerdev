/**
 * @author vncferraz
 * 
 * Batchable to provide processing batchs for the Offer_Queue__c massive upload 
 */
global class OfferQueueBatchable implements Database.Batchable<sObject>{
   
    private List<Offer_Queue__c> processedScope = new List<Offer_Queue__c>();

    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator('select Id,Account_ID__c,Comments__c,Reseller__c,Percentage_Reseller_Sales_Executive__c,Business_Model__c,Corporate_Email__c,Permission_Contact__c,OwnerEmail__c,Identification_Number__c,Free_Trial_End_User__c,Name_of_the_Benefit_Admin__c,ES_Due_Days__c,ES_Billing_Day__c,ES_Payment_Method__c, Membership_fee_Payment_Method__c,Membership_fee_Due_Dates__c,Close_Date__c,Membership_fee_Billing_Day__c, Unique_Identifier__c,Eligible_List_Registration_Method__c,Temporary_Discount_1_End_Date__c,Temporary_Discount_1_Percentage__c,Temporary_Discount_1_Start_Date__c,Temporary_Discount_2_End_Date__c,Temporary_Discount_2_Percentage__c,Temporary_Discount_2_Start_Date__c,Temporary_Discount_3_End_Date__c,Temporary_Discount_3_Percentage__c,Temporary_Discount_3_Start_Date__c,Temporary_Discount_4_End_Date__c,Temporary_Discount_4_Percentage__c,Temporary_Discount_4_Start_Date__c,Allowlist_Enabler__c,Full_Launch__c,Adjusted_Probability__c,Exclusivity_clause__c,ES_Billing_Percentage__c,ES_Payment_Frequency__c,Family_Member__c,Free_Plan__c,Number_of_Employees__c,Opportunity_ID__c,Parent_Account_ID__c,Reference_Sales_Price_per_Eligible__c,Stage__c,Status__c,Status_Detail__c,Type_of_Request__c,Waiver_End_Date__c,Waiver_Percentage__c,Waiver_Start_Date__c,Start_Date__c,End_Date__c,Price_Index__c,Price_Index_Description__c,CreatedById from Offer_Queue__c where Status__c = \'Pending\' and Type_of_Request__c <> \'Subsidiary\' and Type_of_Request__c <> \'Close\'  ');
    }

    public void execute(Database.BatchableContext BC, List<Offer_Queue__c> scope){

        OfferQueueProcessor processor = new OfferProcessor(scope);

        processedScope.addAll( 

            processor.run() 
        );

        update this.processedScope;
    }
    
    public void finish(Database.BatchableContext BC){
        
    }
    
}