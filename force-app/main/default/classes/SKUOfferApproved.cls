/**
 * @description       : 
 * @author            : tania.fartaria@gympass.com
 * @group             : 
 * @last modified on  : 05-17-2022
 * @last modified by  : tania.fartaria@gympass.com
**/
public with sharing class SKUOfferApproved {

    public static List<Eligibility__c> listEligiblesToUpdate = new List<Eligibility__c>();
    public static List<Payment__c> listPaymentsToUpdate = new List<Payment__c>();

    public static Map<Id, Opportunity> mapOppByPaymentId = new Map<Id, Opportunity>();
    public static Map<Id, Quote> mapOppIdByQuote = new Map<Id, Quote>();
    public static List<Payment__c> listPayments = new List<Payment__c>();
    public static Map<Id, Payment__c> mapMainPaymentRecurringByOppId = new Map<Id, Payment__c>();
    public static Map<Id, Payment__c> mapMainPaymentOneTimeByOppId = new Map<Id, Payment__c>();

    @InvocableMethod(label='Update Payments and Eligibilities' description='Updates Payments and Eligibities on SKU Offer Approved process')
    public static void updatePaymentsAndEligibilities(List<Opportunity> listOpps){
        if(listOpps == null || listOpps.size() == 0) return;

        Map<Id, Opportunity> mapIdOpp = mapOpportunity(listOpps);
        mapOppIdByQuote = mapOpportunityIdByQuote(mapIdOpp.keySet());
        mapOppByPaymentId = mapOpportunityByPaymentId(mapIdOpp);
        manageCurrentPayments(mapIdOpp);
        manageCurrentEligibilities(mapIdOpp);

        handleDMLStatements();
    }

    private static Map<Id, Opportunity> mapOpportunity(List<Opportunity> listOpps){
        Map<Id, Opportunity> mapIdOpp = new Map<Id, Opportunity>();
        for(Opportunity opp : listOpps){
            mapIdOpp.put(opp.Id, opp);
        }
        return mapIdOpp;
    }

    private static Map<Id, Quote> mapOpportunityIdByQuote(Set<Id> setOppIds){
        List<Id> oppIds = new List<Id>();
        oppIds.addAll(setOppIds);
        QuoteRepository repository = new QuoteRepository();
        Map<Id, Quote> mapOppIdByQuote = repository.getLastForOpportunitiesAsOppIdToQuoteMap(oppIds);
        return mapOppIdByQuote;
    }

    private static Map<Id, Opportunity> mapOpportunityByPaymentId(Map<Id, Opportunity> mapIdOpp){
        Id RecurringPaymentRecordTypeId =  Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Recurring').getRecordTypeId();
        Id OneTimePaymentRecordTypeId = Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Non_Recurring').getRecordTypeId();
        

        listPayments = new PaymentRepository().getListPaymentsBySetOpps(mapIdOpp.keySet());
        Map<Id, Opportunity> mapOppByPaymentId = new Map<Id, Opportunity>();

        for (Payment__c payment : listPayments ){
           if(mapIdOpp.get(payment.Opportunity__c) != null){
                Opportunity opp = mapIdOpp.get(payment.Opportunity__c);
                mapOppByPaymentId.put(payment.Id ,opp);
                if(payment.Account__c == opp.accountId && payment.RecordTypeId == RecurringPaymentRecordTypeId){
                    mapMainPaymentRecurringByOppId.put(opp.Id, payment);
                } else if(payment.Account__c == opp.accountId && payment.RecordTypeId == OneTimePaymentRecordTypeId){
                    mapMainPaymentOneTimeByOppId.put(opp.Id, payment);
                }
           }
        }
        return mapOppByPaymentId;
       
    }

    private static void manageCurrentPayments(Map<Id, Opportunity> mapIdOpp ){
     
        for(Payment__c pay : listPayments){
          if(mapMainPaymentRecurringByOppId.get(pay.Opportunity__c) != null && 
            pay.RecordTypeId == mapMainPaymentRecurringByOppId.get(pay.Opportunity__c).RecordTypeId ){
            
            Payment__c mainRecurringPayment = mapMainPaymentRecurringByOppId.get(pay.Opportunity__c);
            pay.Payment_Method__c = mainRecurringPayment.Payment_Method__c;
            pay.PO_Required__c = mainRecurringPayment.PO_Required__c;
            listPaymentsToUpdate.add(pay);

          }else if(mapMainPaymentOneTimeByOppId.get(pay.Opportunity__c) != null && 
            pay.RecordTypeId == mapMainPaymentOneTimeByOppId.get(pay.Opportunity__c).RecordTypeId ){
            
            Payment__c mainOneTimePayment = mapMainPaymentOneTimeByOppId.get(pay.Opportunity__c);
            pay.Payment_Method__c = mainOneTimePayment.Payment_Method__c;
            pay.PO_Required__c = mainOneTimePayment.PO_Required__c;
            listPaymentsToUpdate.add(pay);
         }
        }
    }

    private static void manageCurrentEligibilities(Map<Id, Opportunity> mapIdOpp ){
     
        Map<Id, List<Eligibility__c>> mapPaymentIdEligibilityList = new EligibilityRepository().getEligibilitiesByPayments(listPayments);

        for(List<Eligibility__c> eligibilities : mapPaymentIdEligibilityList.values()){
            for(Eligibility__c eli : eligibilities)
                setValuesOnEligibility(eli);
        }
    }

    private static void setValuesOnEligibility(Eligibility__c eli){
        
        if(mapOppByPaymentId.get(eli.Payment__c) != null){
            Opportunity opp = mapOppByPaymentId.get(eli.Payment__c);
            eli.Purchase_Order_number_is_needed__c = opp.Purchase_Order_number_is_needed__c;
            eli.Membership_Fee_PO_Duration__c = opp.Membership_Fee_PO_Duration__c;
            if(mapOppIdByQuote.get(opp.Id)!=null){
                Quote qt = mapOppIdByQuote.get(opp.Id);
                eli.Unique_Identifier__c = qt.Unique_Identifier__c;
            }
            listEligiblesToUpdate.add(eli);
        }
    }

    private static void handleDMLStatements(){
        database.update(listEligiblesToUpdate);
        database.update(listPaymentsToUpdate);

    }
}