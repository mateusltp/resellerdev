public without sharing class SelfCheckoutLeadService {
    private LeadDTO leadDTO;
    private Lead lead;

    public SelfCheckoutLeadService(SelfCheckoutLeadRequest selfCheckoutLeadRequest) {
        this.leadDTO = selfCheckoutLeadRequest.getLeadDTO();
        this.lead = leadDTO.parseToSLead();
    }

    public void execute() {
        String businessDocument = lead.firstname + ' ' + lead.LastName;

        List<Queue__c> events = [SELECT Id, Status__c, businessDocument__c, createdDAte 
                                 FROM Queue__c 
                                 WHERE Status__c = 'QUEUED' AND businessDocument__c = :businessDocument
                                 FOR UPDATE]; // this is just to execution wait until the previous event finishes processing, this way duplicated lead creation is avoided.
        
        List<Lead> leadOldInfo = [ SELECT Id, isConverted, Status
                                FROM Lead
                                WHERE Email =: this.lead.Email
                                WITH SECURITY_ENFORCED
                                ORDER BY CreatedDate desc
                                LIMIT 1];
        if( (leadOldInfo != null && !leadOldInfo.isEmpty())  && leadOldInfo[0].isConverted == false ){
            updateLead(leadOldInfo);
        }else{
            insertLead();
        }
        
    }

    private void updateLead(List<Lead> leadOld) {
        this.lead.Id = leadOld[0].Id;
        this.lead.Status = leadOld[0].Status;
        this.lead.Lead_has_Contact__c = hasLeadContact(this.lead.Email);

        Database.update(this.lead);
    }

    private void insertLead() {
        this.lead.Status = 'Waiting for Qualification';
        this.lead.LeadSource = 'Inbound';
        this.lead.Lead_Sub__c = 'Hands Up';
        this.lead.Lead_has_Contact__c = hasLeadContact(this.lead.Email);

        Database.insert(this.lead);
    }
    
    private Boolean hasLeadContact(String leadEmail){
        List<Contact> contact = [ SELECT Id, Email
                                    FROM Contact
                                    WHERE Email =: leadEmail
                                    WITH SECURITY_ENFORCED
                                    ORDER BY CreatedDate desc
                                    LIMIT 1];
        return !contact.isEmpty();
    }
}