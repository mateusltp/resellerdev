/**
 * @description       : 
 * @author            : gft.jorge.stevaux@ext.gympass.com
 * @group             : 
 * @last modified on  : 08-12-2021
 * @last modified by  : gft.jorge.stevaux@ext.gympass.com
**/
@isTest(seeAllData=false)
private class CustomNotificationTypeSelectorTest {

    @TestSetup
    static void makeData(){

        Account lAcc = PartnerDataFactory.newAccount();  
        lAcc.Name = 'Parent Acct';   
        lAcc.Send_To_Tagus__c = true;   
        lAcc.UUID__c = '123456';      
        Database.insert( lAcc );

        Account lChildAcct = PartnerDataFactory.newChildAccount(lAcc.Id);
        lChildAcct.Name = 'Child Acct';
        Database.insert( lChildAcct );

        Contact lContact = PartnerDataFactory.newContact(lAcc.Id);
        Database.insert(lContact);

        Bank_Account__c lBankAcct = PartnerDataFactory.newBankAcct();
        Database.insert(lBankAcct);

        Acount_Bank_Account_Relationship__c lBankAcctRel = PartnerDataFactory.newBankAcctRel(lAcc.Id, lBankAcct);
        Database.insert(lBankAcctRel);
                                  
        Opportunity lOpp = PartnerDataFactory.newOpportunity( lAcc.Id, 'Partner_Wishlist_Renegotiation' );     
        Database.insert( lOpp );

        
               
    }

    @isTest 
    static void getSObjectFieldList_Test(){

        List<Schema.SObjectField> schSObjLst = new List<Schema.SObjectField> {
            CustomNotificationType.Id,
            CustomNotificationType.DeveloperName
		};

        Test.startTest();
        System.assertEquals(schSObjLst, new CustomNotificationTypeSelector().getSObjectFieldList());
        Test.stopTest();
    }

    @isTest 
    static void getSObjectType_Test(){

        Test.startTest();
        System.assertEquals(CustomNotificationType.sObjectType, new CustomNotificationTypeSelector().getSObjectType());
        Test.stopTest();
    }

    @isTest 
    static void selectById_Test(){

        List<CustomNotificationType> custNotTypeLst = [SELECT Id FROM CustomNotificationType LIMIT 1];
        Set<Id> custNotTypeIds = (new Map<Id, CustomNotificationType>(custNotTypeLst)).keySet();

        Test.startTest();
        System.assertEquals(custNotTypeLst.get(0).id, new CustomNotificationTypeSelector().selectById(custNotTypeIds).get(0).id);
        Test.stopTest();
    }

    @isTest 
    static void selectCustomNotificationTypeByDeveloperName_Test(){

        CustomNotificationType type = [SELECT Id FROM CustomNotificationType WHERE DeveloperName =  'Waiting_Cancelation_Notify' LIMIT 1];
        
        String customNotTypeRecordTypeDevName = 'Waiting_Cancelation_Notify';

        Test.startTest();
        System.assertEquals(type.id, new CustomNotificationTypeSelector().selectCustomNotificationTypeByDeveloperName(customNotTypeRecordTypeDevName).id);
        Test.stopTest();
    }
    
}