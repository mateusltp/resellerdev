@istest
public class CloneObject_test {
    
    
    @istest
    public static void Clone_CloneDataFromContractAgreement_CreateDocumentCloned(){
        
       Pricebook2 lStandardPricebook = DataFactory.newPricebook();
        Database.update( lStandardPricebook );
        
        Account lAcc = DataFactory.newAccount();              
        Database.insert( lAcc );
        
        //Create the opportunities
        Opportunity lNewBusinessOpp = DataFactory.newOpportunity( lAcc.Id, lStandardPricebook, 'Client_Sales_New_Business',true ); 
        
        Database.insert(new List<Opportunity>{lNewBusinessOpp} );
        
        CloneObject clone = new CloneObject();
        
        APXT_Redlining__Contract_Agreement__c objContractAgreement = new APXT_Redlining__Contract_Agreement__c();
        
        objContractAgreement.Opportunity__c = lNewBusinessOpp.id;
        objContractAgreement.APXT_Amendment_Version__c = 0;
        objContractAgreement.Conga_Sign_Status__c = 'DRAFT';
        objContractAgreement.Include_full_T_C_s__c = true;
        objContractAgreement.Include_signature__c = false;
        objContractAgreement.Legal_Contract_Status__c= 'Contract Draft';
        //objContractAgreement.OwnerId = '0051L00000Dn9Q7QAJ';
        //objContractAgreement.Parent_Opportunity__c = '0061L0000174UN0QAM';
        //objContractAgreement.SignedMethod__c = 'Signed with SF';
        //objContractAgreement.Parent_Proposal__c = '0Q06C000000PgXaSAK';
        //objContractAgreement.Proposal__c= '0Q06C000000PgXaSAK';
        objContractAgreement.APXT_Renegotiation__c = false;
        objContractAgreement.APXT_Redlining__Status__c = 'In Process';
        objContractAgreement.Template_Selection__c = 'USA';
        objContractAgreement.RecordTypeId = Schema.SObjectType.APXT_Redlining__Contract_Agreement__c.getRecordTypeInfosByDeveloperName().get('B2B_Contract').getRecordTypeId();
        
        
        
        insert objContractAgreement;
        
        
        List<APXT_Redlining__Contract_Agreement__c> listContractAgreement = [ SELECT    Id                                                  ,
                                                                             Opportunity__c                                      ,
                                                                             APXT_Amendment_Version__c                           ,
                                                                             Conga_Sign_Status__c                                ,
                                                                             Include_full_T_C_s__c                               ,
                                                                             Include_signature__c                                ,
                                                                             Legal_Contract_Status__c                            ,
                                                                             APXT_Redlining__Legal_Entity__c                     ,
                                                                             OwnerId                                             ,
                                                                             APXT_Redlining__Contract_Agreement_Family_Parent__c ,
                                                                             Parent_Opportunity__c                               ,
                                                                             Parent_Proposal__c                                  ,
                                                                             Proposal__c                                         ,
                                                                             APXT_Renegotiation__c                               ,
                                                                             APXT_Redlining__Status__c                           ,
                                                                             Template_Selection__c 								 ,
                                                                             SignedMethod__c
                                                                             FROM        
                                                                             APXT_Redlining__Contract_Agreement__c
                                                                            ];
        
        
        Test.StartTest();
        
        List<Document__c> listDocument =  [SELECT   Id, 
                                           AmendmentVersion__c ,
                                           SignStatus__c       ,
                                           IncludeFullTCs__c   ,
                                           IncludeSignature__c ,
                                           ContractStatus__c   ,
                                           LegalEntity__c      ,
                                           Opportunity__c      ,
                                           OwnerId             ,
                                           ParentDocument__c   ,
                                           ParentOpportunity__c,
                                           ParentProposal__c   ,
                                           Proposal__c         ,
                                           Renegotiation__c    ,
                                           Status__c           ,
                                           TemplateSelection__c,
                                           SignedMethod__c
                                           FROM 
                                           Document__c
                                           WHERE   ContractAgreement__c = : objContractAgreement.Id 
                                           LIMIT   1 ];
        
        
        
        System.assertequals(objContractAgreement.APXT_Amendment_Version__c,listDocument[0].AmendmentVersion__c);
        System.assertequals(objContractAgreement.Include_full_T_C_s__c,listDocument[0].IncludeFullTCs__c);
        System.assertequals(objContractAgreement.Include_signature__c,listDocument[0].IncludeSignature__c);
        System.assertequals(objContractAgreement.Legal_Contract_Status__c,listDocument[0].ContractStatus__c);
        System.assertequals(objContractAgreement.APXT_Redlining__Legal_Entity__c,listDocument[0].LegalEntity__c);
        System.assertequals(objContractAgreement.Opportunity__c,listDocument[0].Opportunity__c);
        System.assertequals(objContractAgreement.Parent_Opportunity__c,listDocument[0].ParentOpportunity__c);
        System.assertequals(objContractAgreement.Parent_Proposal__c,listDocument[0].ParentProposal__c);
        System.assertequals(objContractAgreement.Proposal__c,listDocument[0].Proposal__c);
        System.assertequals(objContractAgreement.APXT_Renegotiation__c,listDocument[0].Renegotiation__c);
        System.assertequals(objContractAgreement.APXT_Redlining__Status__c,listDocument[0].Status__c);
        System.assertequals(objContractAgreement.Template_Selection__c,listDocument[0].TemplateSelection__c);
        System.assertequals(objContractAgreement.SignedMethod__c,listDocument[0].SignedMethod__c);
        
        
        //Testing if contract changed
        objContractAgreement.Include_full_T_C_s__c = false;
        
        update objContractAgreement;
        
        
        listDocument =  [SELECT   Id, 
                         AmendmentVersion__c ,
                         SignStatus__c       ,
                         IncludeFullTCs__c   ,
                         IncludeSignature__c ,
                         ContractStatus__c   ,
                         LegalEntity__c      ,
                         Opportunity__c      ,
                         OwnerId             ,
                         ParentDocument__c   ,
                         ParentOpportunity__c,
                         ParentProposal__c   ,
                         Proposal__c         ,
                         Renegotiation__c    ,
                         Status__c           ,
                         TemplateSelection__c,
                         SignedMethod__c
                         FROM 
                         Document__c
                         WHERE   ContractAgreement__c = : objContractAgreement.Id 
                         LIMIT   1 ];
        
        System.assertequals(objContractAgreement.APXT_Amendment_Version__c,listDocument[0].AmendmentVersion__c);
        System.assertequals(objContractAgreement.Include_full_T_C_s__c,listDocument[0].IncludeFullTCs__c);
        System.assertequals(objContractAgreement.Include_signature__c,listDocument[0].IncludeSignature__c);
        System.assertequals(objContractAgreement.Legal_Contract_Status__c,listDocument[0].ContractStatus__c);
        System.assertequals(objContractAgreement.APXT_Redlining__Legal_Entity__c,listDocument[0].LegalEntity__c);
        System.assertequals(objContractAgreement.Opportunity__c,listDocument[0].Opportunity__c);
        System.assertequals(objContractAgreement.Parent_Opportunity__c,listDocument[0].ParentOpportunity__c);
        System.assertequals(objContractAgreement.Parent_Proposal__c,listDocument[0].ParentProposal__c);
        System.assertequals(objContractAgreement.Proposal__c,listDocument[0].Proposal__c);
        System.assertequals(objContractAgreement.APXT_Renegotiation__c,listDocument[0].Renegotiation__c);
        System.assertequals(objContractAgreement.APXT_Redlining__Status__c,listDocument[0].Status__c);
        System.assertequals(objContractAgreement.Template_Selection__c,listDocument[0].TemplateSelection__c);
        System.assertequals(objContractAgreement.SignedMethod__c,listDocument[0].SignedMethod__c);
        
        
        //Testing if change the document
        
        listDocument[0].AmendmentVersion__c = 100;
        update listDocument[0];
        
        objContractAgreement = [ SELECT    Id                                                  ,
                                Opportunity__c                                      ,
                                APXT_Amendment_Version__c                           ,
                                Conga_Sign_Status__c                                ,
                                Include_full_T_C_s__c                               ,
                                Include_signature__c                                ,
                                Legal_Contract_Status__c                            ,
                                APXT_Redlining__Legal_Entity__c                     ,
                                OwnerId                                             ,
                                APXT_Redlining__Contract_Agreement_Family_Parent__c ,
                                Parent_Opportunity__c                               ,
                                Parent_Proposal__c                                  ,
                                Proposal__c                                         ,
                                APXT_Renegotiation__c                               ,
                                APXT_Redlining__Status__c                           ,
                                Template_Selection__c 								,
                                SignedMethod__c
                                FROM        
                                APXT_Redlining__Contract_Agreement__c
                                LIMIT 1
                               ];
        
        System.assertequals(objContractAgreement.APXT_Amendment_Version__c,listDocument[0].AmendmentVersion__c);
        System.assertequals(objContractAgreement.Include_full_T_C_s__c,listDocument[0].IncludeFullTCs__c);
        System.assertequals(objContractAgreement.Include_signature__c,listDocument[0].IncludeSignature__c);
        System.assertequals(objContractAgreement.Legal_Contract_Status__c,listDocument[0].ContractStatus__c);
        System.assertequals(objContractAgreement.APXT_Redlining__Legal_Entity__c,listDocument[0].LegalEntity__c);
        System.assertequals(objContractAgreement.Opportunity__c,listDocument[0].Opportunity__c);
        System.assertequals(objContractAgreement.Parent_Opportunity__c,listDocument[0].ParentOpportunity__c);
        System.assertequals(objContractAgreement.Parent_Proposal__c,listDocument[0].ParentProposal__c);
        System.assertequals(objContractAgreement.Proposal__c,listDocument[0].Proposal__c);
        System.assertequals(objContractAgreement.APXT_Renegotiation__c,listDocument[0].Renegotiation__c);
        System.assertequals(objContractAgreement.APXT_Redlining__Status__c,listDocument[0].Status__c);
        System.assertequals(objContractAgreement.Template_Selection__c,listDocument[0].TemplateSelection__c);
        System.assertequals(objContractAgreement.SignedMethod__c,listDocument[0].SignedMethod__c);
        
        Test.StopTest();
    }
    
}