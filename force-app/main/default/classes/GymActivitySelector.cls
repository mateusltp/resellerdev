/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 07-21-2021
 * @last modified by  : gft.samuel.silva@ext.gympass.com
 * Modifications Log 
 * Ver   Date         Author                             Modification
 * 1.0   07-21-2021   gft.samuel.silva@ext.gympass.com   Initial Version
**/
public with sharing class GymActivitySelector extends ApplicationSelector{


     public List<Schema.SObjectField> getSObjectFieldList() {
		return new List<Schema.SObjectField> {
            Gym_Activity__c.Id,
            Gym_Activity__c.Name
		};
	}

    public Schema.SObjectType getSObjectType() {
		return 	Gym_Activity__c.sObjectType;
	}

    public List<Gym_Activity__c> selectById(Set<Id> ids) {
		return (List<Gym_Activity__c>) super.selectSObjectsById(ids);
	}

    /** tags: clone, cloning, to clone, allFields
     * toClone means all fields are retrieving
    */
    public List<Gym_Activity__c> selectByProductIdToClone( Set<Id> aProductItemIdSet ) {
	
        return (List<Gym_Activity__c>) Database.query(
									newQueryFactory().
										selectFields(getAllFieldsFromProductItem()).
										setCondition('Product_Item__c IN: aProductItemIdSet').
										toSOQL());
	}


    private Set<String> getAllFieldsFromProductItem(){
		Map<String, Schema.SObjectField> gymActivityFields = Schema.getGlobalDescribe().get('Gym_Activity__c').getDescribe().fields.getMap();
        return gymActivityFields.keySet();
    }

}