/**
 * @File Name          : ProductItemTriggerCAPHelper.cls
 * @Description        : 
 * @Author             : GEPI@GFT.com
 * @Group              : 
 * @Last Modified By   : gft.samuel.silva@ext.gympass.com
 * @Last Modified On   : 04-26-2022
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    18/05/2020   GEPI@GFT.com     Initial Version
**/
public class ProductItemTriggerCAPHelper {

    //Get RecordTypeId for Gym Partner Account
    Id recordTypeId = Schema.SObjectType.Product_Item__c.getRecordTypeInfosByDeveloperName().get('Gyms_Product_Item_Partner').getRecordTypeId();

    public void populateCAPNetworkInProduct(){
        List<Product_Item__c> lstNew = trigger.new;
        //New Record
        if(trigger.old == null){
            populateOnAfterInsertCAPNetworkInProduct(lstNew);
        }
        else{
            populateOnBeforeUpdateCAPNetworkInProduct(lstNew);
        }
    }

    public void populateOnAfterInsertCAPNetworkInProduct(List<Product_Item__c> lstNew){
        Set<id> triggerIds = new Set<id>();
        List<Product_Item__c> lstPrd = new List<Product_Item__c>();
        
        for(Product_Item__c prdItem:lstNew)
        {
            if( prdItem.RecordTypeId == recordTypeId ){
                triggerIds.add(prdItem.Id);  
            }
             
        }
        if(!triggerIds.isEmpty()){           
            for(Product_Item__c prd : [SELECT Id, Opportunity__c,Opportunity__r.Account.CAP_Value__c,Opportunity__r.Account.Types_of_ownership__c,RecordTypeId,Is_Network_CAP__c FROM Product_Item__c where id IN :triggerIds AND RecordTypeId =: recordTypeId]){
                if(prd.Opportunity__c!=null){
                    if(prd.RecordTypeId == recordTypeId ){
                        System.debug('::entrou populateOnAfterInsertCAPNetworkInProduct ');
                        if(prd.Is_Network_CAP__c == 'Yes' && prd.Opportunity__r.Account.Types_of_ownership__c != Label.franchisePicklist){
                            (new CapNullError()).doAction('You can not use network CAP value for non franchise account hierarchy type.');
                            return;
                        }

                        if(prd.Is_Network_CAP__c == 'Yes' && prd.Opportunity__r.Account.Types_of_ownership__c == Label.franchisePicklist && prd.Opportunity__r.Account.CAP_Value__c == null){
                            System.debug('entrou no IF');
                            (new CapNullError()).doAction('CAP value in account is blank. Please, enter a value before saving this product.'); 
                            return;                      
                        }

                        if(prd.Is_Network_CAP__c == 'Yes' && prd.Opportunity__r.Account.Types_of_ownership__c == Label.franchisePicklist && prd.Opportunity__r.Account.CAP_Value__c != null){
                            prd.CAP_Value__c = prd.Opportunity__r.Account.CAP_Value__c;
                            lstPrd.add(prd);
                        }
                    }
                }    
            }
        }

        if(!lstPrd.isempty()){
            upsert lstPrd;
        }
    }

    public void populateOnBeforeUpdateCAPNetworkInProduct(List<Product_Item__c> lstNew){
        Set<id> triggerIds = new Set<id>();
        Decimal capValue;
        String typeOfOwnership = null;
        for(Product_Item__c prdItem:lstNew)
        {
            if( prdItem.RecordTypeId == recordTypeId ){
                triggerIds.add(prdItem.Id);  
            }
        }
        if(triggerIds.isEmpty()){

            for(Product_Item__c prd : [SELECT Opportunity__c,Opportunity__r.Account.CAP_Value__c,Opportunity__r.Account.Types_of_ownership__c,RecordTypeId,Is_Network_CAP__c FROM Product_Item__c where id IN :triggerIds AND recordTypeId =: recordTypeId]){
                
                if(prd.Opportunity__c!=null){
                    capValue = prd.Opportunity__r.Account.CAP_Value__c;
                    typeOfOwnership = prd.Opportunity__r.Account.Types_of_ownership__c;
                }    
            }

            for(Product_Item__c prd : lstNew){
                if(prd.RecordTypeId == recordTypeId ){
                    
                    if(prd.Opportunity__c!=null && prd.Is_Network_CAP__c == 'Yes' && typeOfOwnership != Label.franchisePicklist){
                        (new CapNullError()).doAction('You can not use network CAP value for non franchise account hierarchy type.');
                        return;
                    } 
                    
                    system.debug('### ' + prd.Is_Network_CAP__c);
                    system.debug('### ' + typeOfOwnership);
                    system.debug('### ' + capValue);

                    
                    if(prd.Opportunity__c!=null && prd.Is_Network_CAP__c == 'Yes' && typeOfOwnership == Label.franchisePicklist && capValue== null){
                        System.debug('Entrou no IF UPDATE');
                        (new CapNullError()).doAction('CAP value in account is blank. Please, enter a value before saving this product.');
                        return;
                    } 

                    if(prd.Is_Network_CAP__c == 'Yes' && capValue != null && typeOfOwnership == Label.franchisePicklist ){
                        prd.CAP_Value__c = capValue;
                    }
                }
            }
        }
    }

    private class CapNullError {
        public CapNullError() {}

        public void doAction(String msg) {
            // Trigger.new is valid here
            SObject[] sobjects = Trigger.new;
            for (Sobject sobj : sobjects) {
                sobj.addError(msg);
            }
        }
    }

}