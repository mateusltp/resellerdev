/**
* @File Name          : LeadTriggerHandler.cls
* @Description        : 
* @Author             : Samuel Silva - GFT (slml@gft.com)
* @Group              : 
* @Last Modified By   : gft.samuel.silva@ext.gympass.com
* @Last Modified On   : 03-16-2022
* @Modification Log   : 
* Ver       Date            Author      		    Modification
* 1.0    27/04/2020   MLNC@GFT.com     Initial Version
**/
public class LeadTriggerHandler extends triggerhandler {
    

    public override void beforeInsert() {
        new ResellerLeadAllocationController().updateToIndirectChannelRecordType();
        new LeadTriggerAllocationHelper().LeadUpdUser(); 
    }

    public override void afterInsert(){
        new LeadTriggerScoringController().executeJob();
        new ClientSalesLeadAllocation().allocateSalesLead();
    }
    
    public override void beforeUpdate(){
        new LeadTriggerAllocationHelper().LeadUpdUser();
        new ConvertLeadInMass().convertToBeQualifiedLeads();
        new ConvertLeadInMass().convertRejectedLeads();  
        new PartnerLeadsConversion().updateReferralAccountInLeads();  
    }
    
    public override void afterUpdate(){
        new LeadTriggerScoringController().executeJob();   
        new PartnerLeadsConversion.PartnerLeadsConversionWithoutSharing().updateDuplicatedLeads();
    }
    
}