/**
 * Created by bruno on 31/01/2022.
 */

/**
 * @description       :
 * @author            : bruno.mendes@gympass.com
 * @group             :
 * @last modified on  : 05-31-2022
 * @last modified by  : gft.jorge.stevaux@ext.gympass.com
**/
public with sharing class ProductActivityRelationshipSelector extends ApplicationSelector {

    private static final PS_Constants constants = PS_Constants.getInstance();

    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
                Product_Activity_Relationship__c.Id
        };
    }

    public Schema.SObjectType getSObjectType() {
        return Product_Activity_Relationship__c.sObjectType;
    }

    public List<Product_Activity_Relationship__c> selectById(Set<Id> ids) {
        return (List<Product_Activity_Relationship__c>) super.selectSObjectsById(ids);
    }

    public List<Product_Activity_Relationship__c> selectActivityByProductId(Set<Id> productIds)
    {
        return (List<Product_Activity_Relationship__c>) Database.query(
                newQueryFactory().
                        selectField(Product_Activity_Relationship__c.Id).
                        selectField(Product_Activity_Relationship__c.Gym_Activity_Relationship__c).
                        selectField(Product_Activity_Relationship__c.Product__c).
                        selectField('Gym_Activity_Relationship__r.Name').
                        selectField('Gym_Activity_Relationship__r.Name_AR__c').
                        selectField('Gym_Activity_Relationship__r.Name_BR__c').
                        selectField('Gym_Activity_Relationship__r.Name_CL__c').
                        selectField('Gym_Activity_Relationship__r.Name_DE__c').
                        selectField('Gym_Activity_Relationship__r.Name_IT__c').
                        selectField('Gym_Activity_Relationship__r.Name_MX__c').
                        selectField('Gym_Activity_Relationship__r.Name_ES__c').
                        selectField('Gym_Activity_Relationship__r.Name_UK__c').
                        selectField('Gym_Activity_Relationship__r.Name_PT__c').
                        selectField('Gym_Activity_Relationship__r.Name_FR__c').
                        selectField('Gym_Activity_Relationship__r.Name_NL__c').
                        setCondition('Product__c IN :productIds').toSOQL()
        );
    }

    public List<Product_Activity_Relationship__c> selectAllByProductIds(Set<Id> productIds) {

        return (List<Product_Activity_Relationship__c>) Database.query(
                newQueryFactory().
                        selectFields(getAllFieldsFromProductActivityRelationship()).
                        setCondition('Product__c IN: productIds AND RecordType.DeveloperName != \''+constants.PRODUCT_ITEM_CHILD_RT_PARTNER_FLOW+'\'').
                        toSOQL());
                        
    }

    private Set<String> getAllFieldsFromProductActivityRelationship(){
        Map<String, Schema.SObjectField> productFields = Schema.getGlobalDescribe().get('Product_Activity_Relationship__c').getDescribe().fields.getMap();
        return productFields.keySet();
    }
}