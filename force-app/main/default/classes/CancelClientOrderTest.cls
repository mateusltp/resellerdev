@IsTest
public class CancelClientOrderTest {
  @TestSetup
  public static void makeData() {
    EventQueueFixtureFactory.createEventConfigForEvent(
      'CLIENT_ORDER_CANCELATION',
      'CancelClientOrderCommand'
    );

    Account account = AccountMock.getStandard('Empresas');
    account.Send_To_Tagus__c = true;
    insert account;

    Contact contact = ContactMock.getStandard(account);
    insert contact;

    account.Attention__c = contact.Id;
    update account;

    Pricebook2 pricebook = new Pricebook2(
      CurrencyIsoCode = 'BRL',
      Id = Test.getStandardPricebookId(),
      IsActive = true
    );
    upsert pricebook;

    List<Product2> products = new List<Product2>();

    Product2 setupFee = ProductMock.getSetupFee();
    products.add(setupFee);

    Product2 accessFee = ProductMock.getAccessFee();
    products.add(accessFee);

    insert products;

    List<PricebookEntry> pricebookEntries = new List<PricebookEntry>();

    PricebookEntry setupFeePricebookEntry = PricebookMock.getPricebookEntry(
      pricebook,
      setupFee
    );
    pricebookEntries.add(setupFeePricebookEntry);

    PricebookEntry accessFeePricebookEntry = PricebookMock.getPricebookEntry(
      pricebook,
      accessFee
    );
    pricebookEntries.add(accessFeePricebookEntry);

    insert pricebookEntries;

    Opportunity newBusiness = OpportunityMock.getClientSalesSkuNewBusiness(
      account,
      pricebook
    );
    newBusiness.Gympass_Entity__c = account.Id;
    insert newBusiness;

    Payment__c newBusinessPayment = PaymentMock.getStandard(
      newBusiness,
      account
    );

    insert newBusinessPayment;

    Eligibility__c newBusinessEligibility = EligibilityMock.getStandard(
      newBusinessPayment
    );

    insert newBusinessEligibility;

    Quote newBusinessQuote = QuoteMock.getStandard(newBusiness);
    insert newBusinessQuote;

    List<QuoteLineItem> newBusinessQuoteLineItems = new List<QuoteLineItem>();

    QuoteLineItem newBusinessSetupFeeLine = QuoteLineItemMock.getSetupFee(
      newBusinessQuote,
      setupFeePricebookEntry
    );
    newBusinessQuoteLineItems.add(newBusinessSetupFeeLine);

    QuoteLineItem newBusinessAccessFeeLine = QuoteLineItemMock.getEnterpriseSubscription(
      newBusinessQuote,
      accessFeePricebookEntry
    );
    newBusinessAccessFeeLine.UUID__c = new Uuid().getValue();
    newBusinessQuoteLineItems.add(newBusinessAccessFeeLine);

    insert newBusinessQuoteLineItems;

    Waiver__c newBusinessWaiver = WaiverMock.getStandard(
      newBusinessAccessFeeLine
    );

    insert newBusinessWaiver;

    Order order = OrderMock.getStandard(newBusiness, newBusinessQuote);
    order.Cancelation_Date__c = Date.today();
    insert order;
  }

  @IsTest
  public static void execute() {
    Test.startTest();

    new CancelClientOrderSchedule().execute(null);

    Test.stopTest();

    Queue__c clientCancelationEvent = [
      SELECT Id
      FROM Queue__c
      WHERE EventName__c = 'CLIENT_ORDER_CANCELATION'
    ];

    System.assert(
      clientCancelationEvent != null,
      'The cancelation order event should be created'
    );

    Order order = [SELECT Id, Status FROM Order LIMIT 1];

    System.assertEquals(
      'Canceled',
      order.Status,
      'The order status should be \'Canceled\''
    );
  }
}