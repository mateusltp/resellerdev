/**
 * @description       : 
 * @author            : roei@gft.com
 * @group             : 
 * @last modified on  : 11-25-2021
 * @last modified by  : roei@gft.com
**/
public without sharing class ApprovalRevampWaiver {
    List< Id > gLstOppId;
    Map< Id , Waiver__c > gMapIdOldWaiver;
    List< Waiver__c > gLstNewWaiver;

    public ApprovalRevampWaiver(){}

    public void saveApprovedWaiver( Map< Id , Waiver__c > aMapIdOldWaiver , List< Waiver__c > aLstNewWaiver ){
        gLstNewWaiver = aLstNewWaiver;
        gMapIdOldWaiver = aMapIdOldWaiver;
        List< Assert_Data__c > lLstApprovedWaiverAssertData = getApprovedWaiverAssertData();

        if( !lLstApprovedWaiverAssertData.isEmpty() ){
            Database.upsert( lLstApprovedWaiverAssertData );
        }
    }

    private List< Assert_Data__c > getApprovedWaiverAssertData(){
        List< Assert_Data__c > lLstApprovedWaiverAssertData = new List< Assert_Data__c >();

        List< Id > lLstWaiverId = getApprovedWaiverId();

        if( lLstWaiverId.isEmpty() ){ return lLstApprovedWaiverAssertData; } 

        Map< Id , List< Waiver__c >> lMapOppIdWaivers = getMapOppIdWaivers( lLstWaiverId );

        Map< Id , Decimal > lMapOppIdNumMonthWaiver = getMapOppIdNumMonthWaiver( lMapOppIdWaivers );
        
        lLstApprovedWaiverAssertData = getLstAssertDataTotalNumMonthsWaiver( lMapOppIdNumMonthWaiver );

        return lLstApprovedWaiverAssertData;
    }

    private List< Assert_Data__c > getLstAssertDataTotalNumMonthsWaiver( Map< Id , Decimal > aMapOppIdNumMonthWaiver ){
        Set< Id > lSetOppId = aMapOppIdNumMonthWaiver.keySet();
        Map< Id , Assert_Data__c > lMapOppIdAssertData = new Map< Id , Assert_Data__c >();
        List< Assert_Data__c > lLstEndDateAssertData = new List< Assert_Data__c >();
        Map< String , Assert_Data__c > lMapOppIdEndDateAssertData = getMapOppIdEndDateAssertData( lSetOppId );

        List< Assert_Data__c > lLstAssertData = 
            [ SELECT Id, Field_Name__c, Object_Name__c, Fee_Type__c,  Old_Value__c, New_Value__c, Opportunity__c
              FROM Assert_Data__c WHERE Opportunity__c =: lSetOppId AND 
              Fee_Type__c = 'Enterprise Subscription' AND Object_Name__c = 'waiver__c' AND Field_Name__c = 'number_of_months_after_launch__c' ];
        
        if( lLstAssertData.size() != lSetOppId.size() ){
            for( Assert_Data__c iAssertData : lLstAssertData ){
                lMapOppIdAssertData.put( iAssertData.Opportunity__c , iAssertData );
            }

            for( Id iOppId : lSetOppId ){
                Assert_Data__c lAssertData = lMapOppIdAssertData.get( iOppId );
                if( lAssertData != null ){ continue; }

                lLstAssertData.add( new Assert_Data__c(
                    Opportunity__c = iOppId,
                    Fee_Type__c = 'Enterprise Subscription',
                    Object_Name__c = 'waiver__c',
                    Field_Name__c = 'number_of_months_after_launch__c'
                ));
            }
        }

        for( Assert_Data__c iAssertData : lLstAssertData ){
            Decimal lTotalWaiverMonths = aMapOppIdNumMonthWaiver.get( iAssertData.Opportunity__c );
            if( lTotalWaiverMonths == null ){ lTotalWaiverMonths = 0; }

            Assert_Data__c lEndDateAssertData = lMapOppIdEndDateAssertData.get( iAssertData.Opportunity__c );

            if( lEndDateAssertData == null || lEndDateAssertData.New_Value__c != 'Approved' ){
                lEndDateAssertData = new Assert_Data__c(
                    Opportunity__c = iAssertData.Opportunity__c,
                    Object_Name__c = 'quote',
                    Field_Name__c = 'end_date__c',
                    Old_Value__c = lEndDateAssertData?.Old_Value__c,
                    New_Value__c = 'Approved'
                );
                iAssertData.Old_Value__c = String.valueOf( lTotalWaiverMonths );

                lLstEndDateAssertData.add( lEndDateAssertData );
            } else {
                iAssertData.Old_Value__c = !String.isBlank( iAssertData.Old_Value__c ) && 
                    Decimal.valueOf( iAssertData.Old_Value__c ) > lTotalWaiverMonths ?
                    iAssertData.Old_Value__c : String.valueOf( lTotalWaiverMonths );
            }
        }

        lLstAssertData.addAll( lLstEndDateAssertData );

        return lLstAssertData;
    }
    
    private Map< String , Assert_Data__c > getMapOppIdEndDateAssertData( Set< Id > aSetOppId ){
        Map< String , Assert_Data__c > lMapOppIdEndDateAssertData = new Map< String , Assert_Data__c >();

        for( Assert_Data__c iAssertData : 
            [ SELECT Id, Field_Name__c, Object_Name__c, Fee_Type__c,  Old_Value__c, New_Value__c, Opportunity__c
             FROM Assert_Data__c WHERE Opportunity__c =: aSetOppId AND 
             Object_Name__c = 'quote' AND Field_Name__c = 'end_date__c' ] ){
            lMapOppIdEndDateAssertData.put( iAssertData.Opportunity__c , iAssertData );
        }

        return lMapOppIdEndDateAssertData;
    }

    private List< Id > getApprovedWaiverId(){
        List< Id > lLstWaiverId = new List< Id >();

        for( Waiver__c iWaiver : gLstNewWaiver ){
            if( !gMapIdOldWaiver.get( iWaiver.Id ).Approved_By_Approval_Process__c && iWaiver.Approved_By_Approval_Process__c ){
                lLstWaiverId.add( iWaiver.Id );
            }
        }

        return lLstWaiverId;
    }
    
    private Map< Id , List< Waiver__c >> getMapOppIdWaivers( List< Id > aLstWaiverId ){
        Map< Id , List< Waiver__c >> lMapOppIdWaivers = new Map< Id , List< Waiver__c >>();
        Set< Id > lSetQuoteId = new Set< Id >();

        for( Waiver__c iWaiver : [ SELECT Payment__r.Quote_Line_Item__r.Quote.OpportunityId FROM Waiver__c WHERE Id =: aLstWaiverId ]){
            if( iWaiver.Payment__r?.Quote_Line_Item__r?.Quote.OpportunityId == null ){ continue; }

            lSetQuoteId.add( iWaiver.Payment__r.Quote_Line_Item__r.Quote.OpportunityId );
        }

        if( lSetQuoteId.isEmpty() ){ return lMapOppIdWaivers; }
        
        for( Waiver__c iWaiver : [ SELECT Id, Number_of_Months_after_Launch__c, Payment__r.Quote_Line_Item__r.Quote.OpportunityId 
                                   FROM Waiver__c
                                   WHERE Payment__r.Quote_Line_Item__r.Quote.OpportunityId =: lSetQuoteId ]){
            String lOppId = iWaiver.Payment__r.Quote_Line_Item__r.Quote.OpportunityId;
            
            if( lMapOppIdWaivers.get( lOppId ) == null ){
                lMapOppIdWaivers.put( lOppId , new List< Waiver__c >{ iWaiver } );
            }else{
                lMapOppIdWaivers.get( lOppId ).add( iWaiver );
            }
        }

        return lMapOppIdWaivers;
    }

    private Map< Id , Decimal > getMapOppIdNumMonthWaiver( Map< Id , List< Waiver__c >> aMapOppIdWaivers ){
        Map< Id , Decimal > lMapOppIdNumMonthWaiver = new Map< Id , Decimal >();

        for( Id iOppId : aMapOppIdWaivers.keySet() ){
            Decimal lTotalWaiverMonths = 0;

            for( Waiver__c iWaiver : aMapOppIdWaivers.get( iOppId ) ){
                lTotalWaiverMonths += 
                    ( iWaiver.Number_of_Months_after_Launch__c == null ? 0 : iWaiver.Number_of_Months_after_Launch__c );
            }
            lMapOppIdNumMonthWaiver.put( iOppId , lTotalWaiverMonths );
        }

        return lMapOppIdNumMonthWaiver;
    }

}