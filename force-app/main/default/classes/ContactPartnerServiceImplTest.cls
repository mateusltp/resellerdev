/**
 * Created by gympasser on 16/02/2022.
 */

@IsTest
private class ContactPartnerServiceImplTest {
    @TestSetup
    static void setup() {
        List<Account> accounts = new List<Account>();
        // Account without a contact (therefore also no Legal Representative)
        Account account = new Account(
                Name = 'Test Account 1',
                GP_Status__c = 'Active',
                BillingCity = 'Lisboa',
                BillingCountry = 'Portugal'
        );
        accounts.add(account);

        // Account with a contact that will be defined as its legal representative
        Account account2 = new Account(
                Name = 'Test Account 2',
                GP_Status__c = 'Active',
                BillingCity = 'Lisboa',
                BillingCountry = 'Portugal'
        );
        accounts.add(account2);

        // Account with a contact that will not be defined as its legal representative
        Account account3 = new Account(
                Name = 'Test Account 3',
                GP_Status__c = 'Active',
                BillingCity = 'Lisboa',
                BillingCountry = 'Portugal'
        );
        accounts.add(account3);
        insert accounts;

        List<Contact> contacts = new List<Contact>();

        Contact contact = new Contact(
                FirstName = 'Johnny',
                LastName = ' Does',
                Type_of_contact__c = 'Legal Representative',
                AccountId = account2.Id,
                Email = 'johnny@email.com',
                RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Gyms_Partner').getRecordTypeId()

        );
        contacts.add(contact);
        Contact contact2 = new Contact(
                FirstName = 'Jamie',
                LastName = ' Doesty',
                Type_of_contact__c = 'Decision Maker',
                AccountId = account3.Id,
                Email = 'jamie@email.com',
                RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Gyms_Partner').getRecordTypeId()

        );
        contacts.add(contact2);
        insert contacts;
    }

    @IsTest
    static void testUpdateAccountLegalRepresentative_onInsert() {
        ID recordTypePartnerID = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Gyms_Partner').getRecordTypeId();

        Account account = [SELECT Id, Legal_Representative__c FROM Account WHERE Name = 'Test Account 1' LIMIT 1];

        Contact contact = new Contact(
                FirstName = 'Janesh',
                LastName = ' Doesy',
                Type_of_contact__c = 'Legal Representative',
                AccountId = account.Id,
                Email = 'janeshy@email.com',
                RecordTypeId = recordTypePartnerID
        );
        insert contact;
        account = [SELECT Legal_Representative__c FROM Account WHERE Name = 'Test Account 1' LIMIT 1];
        // Account legal representative is the newly created contact
        System.assertEquals(contact.Id, account.Legal_Representative__c);
    }

    @IsTest
    static void testUpdateAccountLegalRepresentative_onUpdateClear() {
        Contact contact = [SELECT Id, Type_of_contact__c FROM Contact WHERE Account.Name = 'Test Account 2' LIMIT 1];

        contact.Type_of_contact__c = 'Decision Maker';
        update contact;

        Account account = [SELECT Legal_Representative__c FROM Account WHERE Name = 'Test Account 2' LIMIT 1];
        // Account doesn't have a legal representative anymore after contact type of Legal Representative is removed
        System.assertEquals(null, account.Legal_Representative__c);
    }

    @IsTest
    static void testUpdateAccountLegalRepresentative_onUpdateFill() {
        Contact contact = [SELECT Id, Type_of_contact__c FROM Contact WHERE Account.Name = 'Test Account 3' LIMIT 1];

        contact.Type_of_contact__c += ';Legal Representative';
        update contact;

        Account account = [SELECT Legal_Representative__c FROM Account WHERE Name = 'Test Account 3' LIMIT 1];
        // Account Legal Representative is filled in again after updating the contact
        System.assertEquals(contact.Id, account.Legal_Representative__c);
    }

    @IsTest
    static void testUpdateRelationshipRole_onInsert() {
        ID recordTypePartnerID = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Gyms_Partner').getRecordTypeId();

        Account account = [SELECT Id, Legal_Representative__c FROM Account WHERE Name = 'Test Account 1' LIMIT 1];

        Contact contact = new Contact(
                FirstName = 'Janesh',
                LastName = ' Doesy',
                Type_of_contact__c = 'Legal Representative',
                AccountId = account.Id,
                Email = 'janeshy@email.com',
                RecordTypeId = recordTypePartnerID
        );
        insert contact;
        List<AccountContactRelation> accountContactRelations = [SELECT Id, Type_of_Contact__c FROM AccountContactRelation WHERE ContactId = :contact.Id AND AccountId = :account
        .Id];
        // Account Contact Relation type of contact is the same as the newly created contact
        System.assertEquals(contact.Type_of_contact__c, accountContactRelations.get(0).Type_of_Contact__c);
    }

    @IsTest
    static void testUpdateRelationshipRole_onUpdate() {
        Contact contact = [SELECT Id, Type_of_contact__c FROM Contact WHERE Account.Name = 'Test Account 3' LIMIT 1];
        List<AccountContactRelation> accountContactRelations = [SELECT Id, Type_of_Contact__c FROM AccountContactRelation WHERE ContactId = :contact.Id];
        system.debug('## accountContactRelations: '+accountContactRelations);
        contact.Type_of_contact__c += ';Legal Representative';
        update contact;

        accountContactRelations = [SELECT Id, Type_of_Contact__c FROM AccountContactRelation WHERE ContactId = :contact.Id];
        // Account Contact Relation type of contact is the same as the newly created contact
        System.assert(accountContactRelations.get(0).Type_of_Contact__c.contains('Legal Representative'));
    }
}