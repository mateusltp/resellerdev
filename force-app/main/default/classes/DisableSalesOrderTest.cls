/**
 * @description       : 
 * @author            : roei@gft.com
 * @group             : 
 * @last modified on  : 12-16-2021
 * @last modified by  : roei@gft.com
**/
@IsTest
public class DisableSalesOrderTest {
  /*@IsTest
  public static void execute() {
    Account account = AccountMock.getGympassEntity();
    account.Send_To_Tagus__c = true;
    account.UUID__c = new Uuid().getValue();
    insert account;

    Contact contact = ContactMock.getStandard(account);
    insert contact;

    account.Attention__c = contact.Id;
    update account;

    Pricebook2 pricebook = new Pricebook2(
      CurrencyIsoCode='BRL',
      Id = Test.getStandardPricebookId(),
      IsActive = true
    );

    Product2 product = ProductMock.getSetupFee();
    insert product;

    PricebookEntry priceBookEntry = PricebookMock.getPricebookEntry(pricebook, product);
    insert priceBookEntry;

    Opportunity opportunity = OpportunityMock.getNewBusiness(account, pricebook);
    opportunity.Gympass_Entity__c = account.Id;
    opportunity.Type = 'New Business';
    insert opportunity;

    Quote quote = QuoteMock.getStandard(opportunity);
    insert quote;

    QuoteLineItem quoteLineItem = QuoteLineItemMock.getSetupFee(
      quote,
      priceBookEntry
    );
    insert quoteLineItem;

    Order order = OrderMock.getStandard(opportunity, quote);
    order.AccountId = opportunity.AccountId;
    insert order;

    OrderItem orderItem = OrderItemMock.getStandard(order, quoteLineItem);
    orderItem.Type__c = 'Setup Fee';
    insert orderItem;

    Test.startTest();

    order.Status = 'Activated';
    update order;

    opportunity.StageName = 'Contrato Assinado';
    update opportunity;

    order = [
      SELECT Id,
      Status
      FROM Order
      WHERE OpportunityId = :opportunity.Id
      LIMIT 1
    ];

    System.assertEquals(
      'Inactivated',
      order.Status,
      'Order is not inactivated'
    );

    Test.stopTest();
  }
  */
}