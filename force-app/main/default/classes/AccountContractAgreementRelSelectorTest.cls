/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 04-28-2022
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/

@IsTest
public with sharing class AccountContractAgreementRelSelectorTest {
    

    
    @TestSetup
    static void setupData(){
        PS_Constants constants = PS_Constants.getInstance(); 
        String recordTypePartnerFlow = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(constants.ACCOUNT_RT_PARTNER_FLOW_ACCOUNT).getRecordTypeId();
        Account partnerAcc = PartnerDataFactory.newAccount();
        partnerAcc.RecordTypeId = recordTypePartnerFlow;
        partnerAcc.CRM_ID__c = '1231';
        partnerAcc.Legal_Document_Type__c = 'CNPJ';
        partnerAcc.Legal_Title__c = 'AccountContractTest';
        partnerAcc.BillingCountryCode = 'US';
        partnerAcc.BillingCountry = 'United States';
        partnerAcc.BillingState = 'Arizona';
        partnerAcc.BillingPostalCode = '1223';
        partnerAcc.ShippingCountryCode = 'US';
        partnerAcc.ShippingCountry = 'United States';
        partnerAcc.ShippingState = 'Arizona';
        partnerAcc.CurrencyIsoCode = 'USD';
        partnerAcc.Description = 'dasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasda';
        INSERT partnerAcc;
        

        Opportunity aOpp = PartnerDataFactory.newOpportunity( partnerAcc.Id, 'Partner_Flow_Opportunity'); 
        aOpp.Name = 'AccountContractTest';
        aOpp.CurrencyIsoCode = 'USD';
        INSERT aOpp;
  
        APXT_Redlining__Contract_Agreement__c contractAgreementPartner = PartnerDataFactory.newContractAgreement(aOpp,  partnerAcc, 'Hybrid Contract');
        contractAgreementPartner.APXT_Redlining__Status__c = 'Active';
        contractAgreementPartner.Opportunity__c = aOpp.Id;
        contractAgreementPartner.APXT_Amendment_Version__c = 0;
        contractAgreementPartner.Conga_Sign_Status__c = 'DRAFT';
        contractAgreementPartner.Include_full_T_C_s__c = true;
        contractAgreementPartner.Include_signature__c = false;
        contractAgreementPartner.Legal_Contract_Status__c= 'Contract Draft';
        contractAgreementPartner.APXT_Renegotiation__c = false;
        contractAgreementPartner.APXT_Redlining__Status__c = 'In Process';
        contractAgreementPartner.Template_Selection__c = 'USA';
        contractAgreementPartner.RecordTypeId = Schema.SObjectType.APXT_Redlining__Contract_Agreement__c.getRecordTypeInfosByDeveloperName().get('Partner_Contract').getRecordTypeId();
        
        INSERT contractAgreementPartner;
        
        Account_Contract_Agreement_Relationship__c accContractAgreemRel = new Account_Contract_Agreement_Relationship__c();
        accContractAgreemRel.AccountId__c = partnerAcc.Id;
        accContractAgreemRel.ContractAgreementId__c = contractAgreementPartner.Id;

        INSERT accContractAgreemRel;
    }

    @IsTest
    private static void getSObjectFieldList_Success(){
   
        Test.startTest();
            List<Schema.SObjectField> schemaSobjFieldLst = new AccountContractAgreementRelSelector().getSObjectFieldList();
            System.debug('::schemaSobjFieldLst ' + schemaSobjFieldLst);
            System.assert(schemaSobjFieldLst.size() > 0 );
        Test.stopTest();
    }

    @IsTest
    private static void getSObjectType_Success(){
   
        Test.startTest();
            System.assertEquals(Account_Contract_Agreement_Relationship__c.sObjectType, new AccountContractAgreementRelSelector().getSObjectType());
        Test.stopTest();
    }


    @IsTest
    private static void byOppId_Success(){
        
        Test.startTest();

            Opportunity opp = [SELECT ID FROM Opportunity WHERE Name LIKE '%AccountContractTest' LIMIT 1];
            List<Account_Contract_Agreement_Relationship__c> result = new AccountContractAgreementRelSelector().byOppId( new set<Id>{opp.Id} );
            System.assert( result.size() > 0 );
            System.assertEquals( result[0].ContractAgreementId__r.Opportunity__c, opp.Id );

        Test.stopTest();
    }

    @IsTest
    private static void byOppIdAsMap_Success(){
        
        Test.startTest();

            Opportunity opp = [SELECT ID FROM Opportunity WHERE Name LIKE '%AccountContractTest' LIMIT 1];
            Map<Id, List<Account_Contract_Agreement_Relationship__c>> result = new AccountContractAgreementRelSelector().selectByOpportunityId( new set<Id>{opp.Id} );
            System.assert( result.size() > 0 );
            System.assertEquals( result.get(opp.Id)[0].ContractAgreementId__r.Opportunity__c, opp.Id );

        Test.stopTest();
    }
    
}