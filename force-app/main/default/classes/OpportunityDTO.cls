public without sharing class OpportunityDTO {
  private String id;
  private OpportunityStage stage;
  private String gympass_entity_id;
  private Datetime end_date;
  private Datetime begin_date;
  public Integer number_of_employees;
  private ItemsDTO items;

  public ItemsDTO getItems() {
    return this.items;
  }

  public Opportunity parseToSOpportunity() {
    Account gympassEntity = new Account(UUID__c = this.gympass_entity_id);

    return new Opportunity(
      UUID__c = this.id,
      Name = 'From SMB Portal',
      StageName = parseOpportunityStage(this),
      FastTrackStage__c = parseOpportunityFastrackStage(this),
      Gympass_Entity__r = gympassEntity,
      CloseDate = this.end_date?.Date(),
      Data_do_Lancamento__c = this.begin_date?.Date(),
      Quantity_Offer_Number_of_Employees__c = this.number_of_employees
    );
  }

  private String parseOpportunityStage(OpportunityDTO opportunityDTO) {
    switch on opportunityDTO.stage {
      when VALIDATED {
        return 'Validated';
      }
      when OFFER_SENT {
        return 'Proposta Enviada';
      }
      when CLOSED_WON {
        return 'Lançado/Ganho';
      }
      when else {
        return null;
      }
    }
  }

  private String parseOpportunityFastrackStage(OpportunityDTO opportunityDTO) {
    switch on opportunityDTO.stage {
      when VALIDATED {
        return 'Validated';
      }
      when OFFER_SENT {
        return 'Offer Sent';
      }
      when CLOSED_WON {
        return 'Launched/Won';
      }
      when else {
        return null;
      }
    }
  }

  public class ItemsDTO {
    private List<OneTimeFeeDTO> one_time_fees;
    private List<RecurringFeeDTO> recurring_fees;

    public List<OneTimeFeeDTO> getOneTimeFees() {
      if(this.one_time_fees == null) {
        return new List<OneTimeFeeDTO>();
      }

      return this.one_time_fees;
    }

    public List<RecurringFeeDTO> getRecurringFeeDTOs() {
      if(this.recurring_fees == null) {
        return new List<RecurringFeeDTO>();
      }

      return this.recurring_fees;
    }
  }

  private enum OpportunityStage {
    VALIDATED,
    OFFER_SENT,
    CLOSED_WON
  }
}