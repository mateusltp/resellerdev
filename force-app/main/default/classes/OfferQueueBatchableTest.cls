@isTest
public with sharing class OfferQueueBatchableTest {

   @TestSetup
    public static void createData(){        
        Account acc = DataFactory.newAccount();           
        Database.insert( acc );
    
        Pricebook2 lStandardPricebook = DataFactory.newPricebook();
        Database.update( lStandardPricebook );
       
        Opportunity opp = DataFactory.newOpportunity( acc.Id, lStandardPricebook, 'Client_Sales_New_Business',true ); 
        Database.insert( opp );

        Offer_Queue__c offerQueue = DataFactory.newOfferQueue(opp.Id, acc.Id);
        Database.insert( offerQueue );

    }
    
    @isTest
    public static void testExecuteBatch() {   
    	Test.startTest();
        Id lBatchJobId = Database.executeBatch( new OfferQueueBatchable() , 200 );
        Id lBatchJobIdClose = Database.executeBatch( new OffersToCloseBatchable(), 200 );
        new OfferQueueSchedulable().execute(null);
        Test.stopTest();
        
        System.assert( lBatchJobId != null );
    }

    @isTest
    public static void testSchedule() {   
    	Test.startTest();
        OfferQueueSchedulable offerSchedulable = new OfferQueueSchedulable();
        String scheduleTime = '0 0 23 * * ?'; 
        system.schedule('Testing Reassign Class.', scheduleTime, offerSchedulable); 
        Test.stopTest();
        
    }
    
}