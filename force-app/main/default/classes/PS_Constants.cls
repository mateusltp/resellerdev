/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 07-08-2022
 * @last modified by  : alysson.mota@gympass.com
**/
public with sharing class PS_Constants
{

    private static PS_Constants instance = new PS_Constants();

    private PS_Constants()
    {
    }

    public static PS_Constants getInstance()
    {
        if (instance == null)
        {
            instance = new PS_Constants();
        }
        return instance;
    }

    public final String PARTNER = 'Partner';
    public final String PARTNER_LEVEL_LOCATION = 'Location';
    public final String PARTNER_LEVEL_SINGLE_PARTNER = 'Single Partner';    
    /* Tagus Constants */
    public final String TAGUS_CREATE = 'CREATE';

    /* Debug Log Constants */
    public final String LOGTYPE_ERROR           = 'Error';
    public final String LOGTYPE_INFO            = 'Info';
    public final String LOGTYPE_SQUAD_PARTNERS  = 'Partners';


    /*Lead Constants */
    public final string LEAD_OBJECT                         = 'Lead';
    public final string LEAD_RT_OWNER_REFERRAL              = 'Partner_Owner_Referral_Lead';
    public final string LEAD_STATUS_CONVERTED               = 'Convertido';
    public final string LEAD_STATUS_SQL                     = 'SQL';
    public final string LEAD_STATUS_INPROGRESS              = 'In Progress';
    public final string LEAD_STATUS_ABERTO                  = 'Em aberto';
    public final string LEAD_STATUS_QUALIFICACAO            = 'Em qualificação';
    public final string LEAD_STATUS_QUALIFICADO             = 'Qualificado';
    public final string LEAD_STATUS_DESCARTADO              = 'Descartado';
    public final string LEAD_STATUS_CONVERTIDO              = 'Convertido';
    public final string LEAD_STATUS_WAITQUALIFICATION       = 'Waiting for Qualification';
    public final string LEAD_STATUS_QUALIFY                 = 'Qualify';
    public final string LEAD_STATUS_READYCONVERT            = 'Ready to Convert';    
    public final string LEAD_STATUS_BLOCKLIST               = 'Blocklist';
    public final string LEAD_ALLOCATION_MANUAL              = 'Manually';
    public final string LEAD_ALLOCATION_AUTOMATIC           = 'Automatic';
    public final String LEAD_DUPLICATED_ERROR               = 'Your are creating a duplicated referral. Please refer another gym.';

    /* Account Constants */
    public final String ACCOUNT_OBJECT                      = 'Account';
    public final String ACCOUNT_RT_GYM_PARTNER              = 'Gym_Partner';
    public final String ACCOUNT_RT_PARTNER_FLOW_ACCOUNT     = 'Partner_Flow_Account';
    public final String ACCOUNT_TIER_T1                     = 'T1';
    public final String ACCOUNT_TIER_T2                     = 'T2';
    public final String ACCOUNT_TIER_T3                     = 'T3';
    public final String ACCOUNT_TIER_T4                     = 'T4';
    public final String ACCOUNT_TYPE_PROSPECT               = 'Prospect';
    public final String ACCOUNT_ACTIVE                      = 'Active';
    public final String ACCOUNT_FROZEN_3M                   = 'Frozen Period 3m';
    public final String ACCOUNT_FROZEN_6M                   = 'Frozen Period 6m';
    public final String ACCOUNT_FROZEN_9M                   = 'Frozen Period 9m';
    public final String ACCOUNT_INACTIVE                    = 'Inactive';
    public final String ACCOUNT_MCC                         = 'MCC';
    public final String ACCOUNT_PREMIUM                     = 'Premium';
    public final String ACCOUNT_FROZEN                      = 'Frozen';
    public final String ACCOUNT_WISHLIST                    = 'Wishlist';
    public final String ACCOUNT_CANCELLED                   = 'Cancelled';
    public final String ACCOUNT_BLOCKLIST                   = 'Blacklist';
    public final String ACCOUNT_NONVALID                    = 'Nonvalid';
    public final String ACCOUNT_REJECT                      = 'Reject';



    /* Contact Constants */
    public final String CONTACT_OBJECT                      = 'Contact';
    public final String CONTACT_TYPE_LEGAL_REPRESENTATIVE   = 'Legal Representative';
    public final String CONTACT_RT_PARTNER_FLOW_CONTACT     = 'Partner_Flow_Contact';
    public final String CONTACT_RT_GYMS_PARTNER             = 'Gyms_Partner';

    /* Opportunity Constants */
    public final String OPPORTUNITY_OBJECT                      = 'Opportunity';
    public final String OPPORTUNITY_RT_PARTNER_SM_RENEG         = 'Partner_Small_and_Medium_Renegotiation';
    public final String OPPORTUNITY_RT_PARTNER_WISHLIST_RENEG   = 'Partner_Wishlist_Renegotiation';
    public final String OPPORTUNITY_RT_GYMS_SMP                 = 'Gyms_Small_Medium_Partner';
    public final String OPPORTUNITY_RT_GYMS_WISHLIST            = 'Gyms_Wishlist_Partner';
    public final String OPPORTUNITY_RT_PARTNER_FLOW_OPP         = 'Partner_Flow_Opportunity';
    public final String OPPORTUNITY_RT_PARTNER_FLOW_CHILD_OPP   = 'Partner_Flow_Child_Opportunity';
    public final String OPPORTUNITY_RT_PARTNER_SUCCESS_FLOW     = 'Partner_Success_Flow';
    public final String OPPORTUNITY_STAGE_QUALIFICACAO          = 'Qualificação';
    public final String OPPORTUNITY_STAGE_PERDIDO               = 'Perdido';
    public final String OPPORTUNITY_STAGE_REVIEW_CONTRACT       = 'Review Contract';
    public final String OPPORTUNITY_STAGE_LANCADO_GANHO         = 'Lançado/Ganho';
    public final String OPPORTUNITY_TYPE_RENEGOTIATION          = 'Renegotiation';
    public final String OPPORTUNITY_LOSS_REASON_OTHER           = 'Other';
    public final String OPPORTUNITY_SIGNED_CONTRACT             = 'Contrato Assinado';
    public final String OPPORTUNITY_SET_UP_VALIDATION           = 'Set Up Validation';
    public final String OPPORTUNITY_STAGE_OPORTUNIDADE_VALIDADA = 'Oportunidade Validada Após 1ª Reunião';
    public final String OPPORTUNITY_STAGE_REVIEW_OPPORTUNITY    = 'Review Opportunity';
    public final String OPPORTUNITY_STAGE_PROPOSTA_ENVIADA      = 'Proposta Enviada';
    public final String OPPORTUNITY_STAGE_PROPOSTA_APROVADA     = 'Proposta Aprovada';
	
    /*Product_Item__c Constants */
    public final String PRODUCT_ITEM_OBJECT                     = 'Product_Item__c';
    public final String PRODUCT_ITEM_RT_PARTNER_FLOW            = 'Partner_Flow_Product';
    public final String PRODUCT_ITEM_CHILD_RT_PARTNER_FLOW      = 'Partner_Child_Product';
    public final String PRODUCT_ITEM_NO_OPP_ASSIGNMENTS_ERROR   = 'No Product Opportunity Assignments found. Please contact your admin.';

    /*Commercial_Condition__c Constants */
    public final String COMMERCIAL_CONDITION_OBJECT                         = 'Commercial_Condition__c';
    public final String COMMERCIAL_CONDITION_RT_ADVANCED_PAYMENT            = 'Advanced_Payment';
    public final String COMMERCIAL_CONDITION_RT_CANNIBALIZATION             = 'Cannibalization';
    public final String COMMERCIAL_CONDITION_RT_CAP                         = 'CAP';
    public final String COMMERCIAL_CONDITION_RT_EXCLUSIVITY                 = 'Exclusivity';
    public final String COMMERCIAL_CONDITION_RT_INTEGRATION_FEE_DEDUCTION   = 'Integration_Fee_Deduction';
    public final String COMMERCIAL_CONDITION_RT_LATE_CANCELLATION           = 'Late_Cancellation';
    public final String COMMERCIAL_CONDITION_RT_MINIMUM_GUARANTEED          = 'Minimum_Guaranteed';
    public final String COMMERCIAL_CONDITION_RT_NO_SHOW                     = 'No_Show';
    public final String COMMERCIAL_CONDITION_RT_VOLUME_DISCOUNT             = 'Volume_Discount';

    /*Account_Opportunity_Relationship__c Constants*/
    public final String OPPORTUNITY_MEMBER_OBJECT                           = 'Account_Opportunity_Relationship__c';
    public final String OPPORTUNITY_MEMBER_RT_PARTNER_FLOW                  = 'Partner_Flow_Opportunity_Member';

    /*Threshold__c Constants*/
    public final String THRESHOLD_OBJECT                                    = 'Threshold__c';
    public final String THRESHOLD_RT_PARTNER_FLOW                           = 'Partner_Flow_Treshold';

     /*Product_Threshold_Member__c Constants*/
     public final String PRODUCT_THRESHOLD_MEMBER_OBJECT                    = 'Product_Threshold_Member__c';
     public final String PRODUCT_THRESHOLD_MEMBER_RT_PARTNER_FLOW           = 'Partner_Flow_Treshold_Member'; 

     /*Product_Assignment__c Constants*/
    public final String PRODUCT_ASSIGNMENT_OBJECT                           = 'Product_Assignment__c';
    public final String PRODUCT_ASSIGNMEN_RT_PARTNER_FLOW                   = 'Partner_Flow_Product_Assignment';


     /*Product_Opportunity_Assignment__c Constants*/
     public final String PRODUCT_OPPORTUNITY_ASSIGNMENT_OBJECT              = 'Product_Opportunity_Assignment__c';
     public final String PRODUCT_OPPORTUNITY_ASSIGNMEN_RT_PARTNER_FLOW      = 'Partner_Flow_Product_Opportunity_Assignment';


    /*Product_Activity_Relationship__c Constants*/
    public final String PRODUCT_ACTIVITY_OBJECT                             = 'Product_Activity_Relationship__c';
    public final String PRODUCT_ACTIVITY_RT_PARTNER_FLOW                    = 'Partner_Flow_Product_Activity';

  /* Contract Constants */
    public final String INTENTION_CONTRACT                                  = 'Intention Contract';

     /* Ops Set-Up Validation Form RT Constants */
     public final String OPS_SETUP_FORM_OBJECT                              = 'Ops_Setup_Validation_Form__c';
     public final String OPS_SETUP_FORM_RT_PARTNER                          = 'Partner_Flow_Ops_Form';
     public final String OPS_SETUP_NOT_NEEDED                               = 'Not needed';
     public final String OPS_SETUP_IN_PROGRESS                              = 'In progress';
     public final String OPS_SETUP_COMPLETED                                = 'Completed';
     public final String OPS_SETUP_ERROR_STATUS                             = 'Please, complete opportunity setup before proceeding.';
     public final String OPS_SETUP_STATUS_FIELD                             = 'Setup_Status__c';
     
    /* Order Constants */
    public final String ORDER_STATUS_INACTIVATED = 'Inactivated';

    /* Account_Opportunity_Relationship__c Constants */
    public final String ACC_OPP_REL_RT_PARTNERS_AOR = 'Partners_AOR';

    /* FileObject__c Constants */
    public final String FILEOBJECT_OBJECT =  'FIleObject__c';
    public final String FILEOBJECT_PARTNER_RT = 'Partner';
    public final String FILEOBJECT_FILETYPE_W9      = 'W9';
    public final String FILEOBJECT_FILETYPE_PHOTO   = 'Photo';
    public final String FILEOBJECT_FILETYPE_LOGO    = 'Logo';
    public final String FILEOBJECT_FILETYPE_JBP     = 'JBP';

    
    /* ContentVersion Constants */
    public final String CONTENTVERSION_OBJECT = 'ContentVersion';
    public final String CONTENTVERSION_PARTNER_RT = 'Partner_Flow_File';

    /* File_Setting__mdt Constants */
    public final String FILESETTING_PARTNERS_FILE_TYPE          = 'Partners_File_Type';
    public final String FILESETTING_PARTNERS_FILE_SIZE_LIMIT    = 'Partners_File_Size_Limit';
    public final String FILESETTING_PARTNERS_FILE_QTY_LIMIT     = 'Partners_File_Quantity_Limit';
    public final String FILESETTING_PARTNERS_PHOTO_QTY_LIMIT    = 'Partners_Photos_Quantity';
    public final String FILESETTING_PARTNERS_W9_QTY_LIMIT       = 'Partners_W9_Quantity';
    public final String FILESETTING_PARTNERS_LOGO_QTY_LIMIT     = 'Partners_Logo_Quantity';

    /* Payment__c Constants */
    public final String PAYMENT_OBJECT = 'Payment__c';


    /* Bank_Account__c Constants */
    public final String BANK_ACCOUNT_OBJECT = 'Bank_Account__c';
    public final String BANK_ACCOUNT_PARTNER_RT = 'Partner_Flow_Bank_Account_Detail';


    /* Acount_Bank_Account_Relationship__c Constants */
    public final String BANK_ACCOUNT_REL_OBJECT = 'Acount_Bank_Account_Relationship__c';
    public final String BANK_ACCOUNT_REL_PARTNER_RT = 'Partner_Flow_Bank_Account';

    /* Opening_Hours__c Constants */
    public final String OPENING_HOURS_OBJECT = 'Opening_Hours__c';
    public final String OPENING_HOURS_RT = 'Partner';

    /* Step_Towards_Success_Partner__c Constants */
    public final String STEP_TOWARDS_SUCCESS_PARTNER_OBJECT = 'Step_Towards_Success_Partner__c';
    public final String PARTNER_FLOW_OPP_STEP_RT = 'Partner_Flow_Opportunity_Step';
    public final String PARTNER_FLOW_ACC_STEP_RT = 'Partner_Flow_Account_Step';
    
    /* Email Template Constants */
    public final String ET_WAITING_CANCELATION_NOTIFICATION = 'Waiting Cancelation Notification';

    /* Notification Constants */
    public final String NOTIF_NAME_WAITING_CANCELATION_NOTIFY   = 'Waiting_Cancelation_Notify';
    public final String NOTIF_TITLE_WAITING_CANCELATION_NOTIFY  = 'Waiting Cancellation Notify';
    public final String NOTIF_BODY_WAITING_CANCELATION_NOTIFY   = 'You have an account about to be cancelled. Please check your emapublicil for more details.';
    public final String NOTIF_EXCEPTION_MESSAGE                 = 'Problem sending notification: ';

    /* Message Constants */
    public final String ERROR_UPDATING_NETWORK                  = 'Error updating network: ';
    public final String ERROR_ACCOUNT_CREATING_CHILD_ACCOUNT    = 'Error creating child account: ';
    public final String ERROR_ACCOUNT_WISHLIST_CANNOT_BE_T4     = 'A wishlist gym cannot be T4, please change the wishlist field or the tier field';
    public final String ERROR_ACCOUNT_LONGTAIL_T4_OR_BLANK      = 'A longtail gym can only be T4 or blank, please change the wishlist field or the tier field';
    public final String ERROR_ACCOUNT_DUPLICATE_NAME            = 'You cannot create an account with the selected name as there is already another account with this name on Salesforce: ';
    public final String ERROR_ACCOUNT_DUPLICATE_NETWORKID       = 'You cannot create an account with the selected networkId since there\'s another account with this network Id. Please, place those two accounts in the same network or change the network Id: ';
    public final String ERROR_OPP_OPENING_RENEGOTIATION         = 'Error opening renegotiation: '; 
    public final String ERROR_OPP_UPDATING_STAGE_LOST           = 'Error updating to Lost Stage: ';
    public final String ERROR_NO_IMPLEMENTATION_FOR_RT          = 'No Implementation registered for record type';
    public final String ERROR_VALID_TYPE                        = 'Please select a file type';
    public final String ERROR_VALID_NAME                        = 'You can only upload files with names that STARTS WITH photo, logo, JBP or w9 (US only)';
    public final String ERROR_FILE_SIZE                         = 'File size exceeded. Please select a different file.';
    public final String ERROR_PHOTOS_QUANTITY                   = 'This partner already has 4 photos. You can upload only 4 photos by partner.';
    public final String ERROR_W9_QUANTITY                       = 'This partner already has a W9 Form. You can upload only 1 W9 form by partner.';
    public final String ERROR_LOGO_QUANTITY                     = 'This partner already has a logo. You can upload only 1 logo by partner.';
    public final String ERROR_CREATING_ORDER_PARTNER            = 'Error creating Partner Order. Error message: ';
    public final String ERROR_CONTACT_ACCOUNT_UPDATE_PERM       = 'You should have update permission in contact and account sobjects to execute this event';

    /* Opportunity Validation Error Messages */
    public final String OPPORTUNITY_FIELDS_MISSING      = 'Required fields are missing for this stage. Verify the following fields: ';
    public final String OPPORTUNITY_NO_PRODUCTS         = 'Some information is missing. Add products to the opportunity to continue.';
    public final String OPPORTUNITY_NO_CONTRACTS        = 'Some information is missing. Choose a contract for the opportunity to continue.';
    public final String PRODUCTS_FIELDS_MISSING         = 'Required fields on related Product Items are missing for this stage. Verify the following fields: ';
    public final String OPPORTUNITY_NO_CHILD_PRODUCTS   = 'Opportunity products do not have child products to associate market prices yet. Stage could not be updated.';
    public final String PRODUCTS_NO_MARKET_PRICE        = 'Some information is missing. Enter the market price for each location to continue.';
    public final String ACCOUNT_FIELDS_MISSING          = 'Some information is missing. Fill out the following fields for all accounts to continue: ';
    public final String CONTACTS_FIELDS_MISSING         = 'Required fields on related Contacts are missing for this stage. Verify the following fields: ';
    public final String PRODUCTS_NO_ACTIVITIES          = 'Related products do not have activities associated: ';
    public final String PAYMENT_FIELDS_MISSING          = 'Required fields on the related Payment are missing for this stage. Verify the following fields: ';
    public final String ACCOUNT_NO_BANK_ACCOUNTS        = 'Related Account doesn\'t have Bank Account records associated. Stage could not be updated.';
    public final String ACCOUNT_NO_OPENING_HOURS        = 'Related Account doesn\'t have Opening Hour records associated. Stage could not be updated.';
    public final String ACCOUNT_NO_FILES                = 'Account does not have any required files associated. Stage could not be updated.';
    public final String ACCOUNT_FILES_MISSING           = 'Account required files missing: ';
    public final String ACCOUNT_NO_CONTACTS             = 'Related Accounts doesn\'t have Contact records associated. Stage could not be updated.';
    public final String ACCOUNT_EXCLUSIVITY_APPROVAL    = 'The following accounts require exclusivity approval before you can move to the next stage: ';
    public final String OPPORTUNITY_REQUIRES_APPROVAL   = 'Please, submit this opportunity to approval before proceeding.';
    public final String NO_LEGAL_REPRESENTATIVE         = 'Please, add a legal representative for the account:';

    /* Approval Constants */
    public final String APPROVAL_PENDING = 'Pending';
    public final String APPROVAL_REJECTED = 'Rejected';
    public final String APPROVAL_APPROVED = 'Approved';
    public final String ACCOUNT_APPROVED_OR_PENDING = 'Account was already approved or is part of an active approval process.';
    public final String NO_APPLICABLE_APPROVAL = 'No applicable approval process was found.';
    public final String ACCOUNT_NOT_SENT_FOR_APPROVAL = 'The account could not be sent for approval. Verify if you are the owner, the manager or if any data fitting the criteria is missing. ';

    /* Partner Custom Metadata */
    public final String EXCLUSIVITY_APPROVAL_PROCESS_DEV_NAME   = 'Exclusivity_Approval_Process_API_Name';
    public final String EXCLUSIVITY_APPROVAL_COUNTRIES          = 'Exclusivity_Approval_Countries';

    /* Partner Event Platform Event */
    public final String UPDATE_REQUIRES_CMS_APPROVAL_TRUE   = 'UPDATE_REQUIRES_CMS_APPROVAL_TRUE';
    public final String UPDATE_REQUIRES_CMS_APPROVAL_FALSE  = 'UPDATE_REQUIRES_CMS_APPROVAL_FALSE';

    /* Checkbox as picklist constants */
    public final String YES = 'Yes';
    public final String NO = 'No';

}