@isTest
public class GymActivityTriggerHelperTest {
    @isTest public static void Metodo1(){
        Product_Item__c prod1 = new Product_Item__c();
        prod1.Name = 'Prod Test';
        insert prod1;
        
        Gym_Activity__c gym1 = new Gym_Activity__c();
        gym1.Product_Item__c = prod1.Id;
        insert gym1;
        
        Product_Item__c prodQuery = [SELECT Id, Name FROM Product_Item__c WHERE Name = 'Prod Test'];
        prodQuery.Gym_Activity_Concat__c = 'Activities List Test';
        try{
            update prodQuery;
        }catch(DmlException e){
            Trigger.New[0].addError(e.getDmlMessage(0));  // This will display your custom validation rule error messages
        }
        
        GymActivityTriggerHelper helperTest = new GymActivityTriggerHelper();
        
    }
    
    @isTest public static void Metodo2(){
        List<Gym_Activity__c> glist = new List<Gym_Activity__c>();
        Set<ID> accIds = new Set<ID>();
        Set<String> con = new Set<String>();
        
        Product_Item__c prod1 = new Product_Item__c();
        prod1.Name = 'Prod Test';
        insert prod1;
        
        Gym_Activity__c gym1 = new Gym_Activity__c();
        gym1.Name = 'Gym 1';
        gym1.Product_Item__c = prod1.Id;
        gList.add(gym1);
        insert gym1;
        
        Gym_Activity__c gym2 = new Gym_Activity__c();
        gym2.Name = 'Gym 2';
        gym2.Product_Item__c = prod1.Id;
        gList.add(gym2);
        insert gym2;
        
        Gym_Activity__c gQuery = [SELECT Id, Name, Product_Item__c FROM Gym_Activity__c WHERE Name ='Gym 2'];
        gQuery.Name = 'Gym 3';
        
        delete gQuery;
        
        for (Gym_Activity__c ca : gList){
            if (ca.Product_Item__c != null) {
                accIds.add(ca.Product_Item__c);
                con.add(ca.Product_Item__c);
            }
        }
        
        Product_Item__c prodQuery = [SELECT Id, Name FROM Product_Item__c WHERE Name = 'Prod Test'];
        prodQuery.Gym_Activity_Concat__c = 'Activities List Test';
        try{
            update prodQuery;
        }catch(DmlException e){
            Trigger.New[0].addError(e.getDmlMessage(0));  // This will display your custom validation rule error messages
        }
        GymActivityTriggerHelper helperTest = new GymActivityTriggerHelper();
        
    }
    
}