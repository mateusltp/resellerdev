public with sharing class ResellerCreateM0FormHelper {

    @InvocableMethod(label='[RESELLER] Create M0 Form' description='Generates the Engagement Journey form M0.')
    public static void createM0ForOppsIds(List<Id> oppIdList) {
        System.debug('### Into createM0ForOppsIds ###');
        System.debug('### oppIdList ### ' + oppIdList);
        StepsTowardsSuccessRepository stepsRepo = new StepsTowardsSuccessRepository();
        List<Id> oppIds = getResellerNewBusinessOpp(oppIdList);
        Map<Id, List<Step_Towards_Success1__c>> oppIdToStepsList = stepsRepo.getStepsTowardsSuccessFromOppsAsMap(oppIds);
        createM0FormForOpps(oppIdToStepsList, oppIds);
    }

    private static List<Id> getResellerNewBusinessOpp (List<Id> oppIdList) {
        List<Id> newOppIdLst = new List<Id>();
        for(Opportunity iOpp : [SELECT Id FROM Opportunity 
                                WHERE Id IN:oppIdList 
                                AND RecordType.DeveloperName = 'Indirect_Channel_New_Business']) {
            newOppIdLst.add(iOpp.Id);
        }
        return newOppIdLst;
    }

    private static void createM0FormForOpps(Map<Id, List<Step_Towards_Success1__c>> oppIdToStepsList, List<Id> oppIdList) {
        System.debug('### oppIdToStepsList ### ' + oppIdToStepsList);
        System.debug('### oppIdList ### ' + oppIdList);
        FormRepository formRepo = new FormRepository();
        QuoteLineItemRepository quoteLineItemRepo = new QuoteLineItemRepository();
        QuoteRepository quoteRepo = new QuoteRepository();
        OpportunityRepository oppRepo = new OpportunityRepository();
        List<Form__c> formsM0ToInsert = new List<Form__c>();

        Id m0recordTypeId = Schema.SObjectType.Form__c.getRecordTypeInfosByDeveloperName().get('RESELLER_Engagement_Journey_M0').getRecordTypeId();

        Map<Id, Form__c> oppIdToM0Form = formRepo.getM0ForOppsAsMapOppIdToForm(oppIdList);
        Map<Id, QuoteLineItem> oppIdToEnterpriseSubscription = quoteLineItemRepo.getOppIdToAccessFeeItem(oppIdList);
        Map<Id, Quote> oppIdToQuoteMap = quoteRepo.getLastForOpportunitiesAsOppIdToQuoteMap(oppIdList);
        Map<Id, Opportunity> oppIdToOpp = oppRepo.getIdToOppByIds(oppIdList);

        for (Id oppId : oppIdToStepsList.keySet()) {
            List<Step_Towards_Success1__c> steps = oppIdToStepsList.get(oppId);
            Form__c currentM0Form = oppIdToM0Form.get(oppId);
            Quote proposal = oppIdToQuoteMap.get(oppId);
            QuoteLineItem enterpriseSubscription = oppIdToEnterpriseSubscription.get(oppId);
            Opportunity opp = oppIdToOpp.get(oppId);
            System.debug('### currentM0Form == null ### ' + currentM0Form == null);
            if (currentM0Form == null) { // Opp doesn't have an M0 yet
                Form__c newM0 = new Form__c();
                newM0.Opportunity__c = oppId;
                newM0.RecordTypeId = m0recordTypeId;

                if (steps != null && steps.size() == 12) {
                    fillFieldsFromFormWithEnablers(newM0, steps);
                }

                fillFieldsFromFormWithQuoteLineItemInfo(newM0, enterpriseSubscription);

                fillFieldsFromFormWithQuoteInfo(newM0, proposal);

                fillFieldsFromFormWithOppInfo(newM0, opp);

                formsM0ToInsert.add(newM0);
            }
        }
        System.debug('### formsM0ToInsert ### ' + formsM0ToInsert);
        insert formsM0ToInsert;
    }
    
    private static void fillFieldsFromFormWithQuoteLineItemInfo(Form__c newM0, QuoteLineItem enterpriseSubscription) {
        if (enterpriseSubscription != null) {
            if(enterpriseSubscription.Fee_Contract_Type__c != null ) {
                newM0.Contract_Type__c = enterpriseSubscription.Fee_Contract_Type__c;
            } else {
                newM0.Contract_Type__c = 'Custom';
            }            
        } else {
            newM0.Contract_Type__c = 'Custom';
        }
    }

    private static void fillFieldsFromFormWithQuoteInfo(Form__c newM0, Quote proposal) {
        if (proposal != null) {
            newM0.Number_of_FTEs_in_the_contract__c = proposal.Total_Number_of_Employees__c;
        }
    }

    private static void fillFieldsFromFormWithOppInfo(Form__c newM0, Opportunity opp) {
        // System.debug('### Into the M0 fillFieldsFromFormWithOppInfo ###');
        // System.debug('### opp ###' + opp);
        // System.debug('### opp.OwnerId ###' + opp.OwnerId);
        if (opp != null) {
            newM0.Is_there_an_estimated_date_for_launch__c = opp.Data_do_Lancamento__c;
            newM0.Client_Sales_Executive__c = opp.OwnerId;
            newM0.Client_Success_Executive__c = opp.Responsavel_Gympass_Relacionamento__c;
        }
    }

    private static void fillFieldsFromFormWithEnablers(Form__c newM0, List<Step_Towards_Success1__c> steps) {
       for (Step_Towards_Success1__c step : steps) {
            if (step.Step_Number__c == 1) {
                newM0.Standard_Membership_Fee_GL__c = step.Achieved__c;
                continue;
            }
            
            if (step.Step_Number__c == 2) {
                newM0.Expansion_100_of_the_eligible_GL__c = step.Achieved__c;
                continue;
            }
            
            if (step.Step_Number__c == 3) {
                newM0.Minimum_Access_Fee_GL__c = step.Achieved__c;
                continue;
            }
            
            if (step.Step_Number__c == 4) {
                newM0.Engagement_with_C_Level_GL__c = step.Achieved__c;
                newM0.Have_you_had_interactions_with_c_level__c = step.Achieved__c; 
                continue;
            }
            
            if (step.Step_Number__c == 5) {
                newM0.Payroll_GL__c = step.Achieved__c;
                continue;
            }
            
            if (step.Step_Number__c == 6) {
                newM0.Database_Email_GL__c = step.Achieved__c;
                continue;
            }
            
            if (step.Step_Number__c == 7) {
                newM0.Whitelist_GL__c = step.Achieved__c;
                continue;
            }
            
            if (step.Step_Number__c == 8) {
                newM0.New_Hires_GL__c = step.Achieved__c;
                continue;
            }
            
            if (step.Step_Number__c == 9) {
                newM0.HR_Communication_GL__c = step.Achieved__c;
                continue;
            }
            
            if (step.Step_Number__c == 10) {
                newM0.Exclusivity_clause_GL__c = step.Achieved__c;
                continue;
            }
            
            if (step.Step_Number__c == 11) {
                newM0.PR_in_contract_GL__c = step.Achieved__c;
                continue;
            }
            
            if (step.Step_Number__c == 12) {
                newM0.What_does_success_looks_like_Joint_BP_GL__c = step.Achieved__c;
                continue;
            }
        }
    }
}