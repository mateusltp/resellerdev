/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 09-14-2021
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/
public with sharing class TagusOrderPartnerOutboundPublisher {
    private final String CREATE_EVENT = 'CREATE_PARTNER_ORDER_ON_TAGUS';
    private final String UPDATE_EVENT = 'UPDATE_PARTNER_ORDER_ON_TAGUS';    
    private List<Order> orders;

    public TagusOrderPartnerOutboundPublisher(List<Order> orders) {
        this.orders = orders;
    }

    public EventQueue runCreate() {
        System.debug('RUN CREATE ');
        return publishEvent(CREATE_EVENT);
    }
    
    public EventQueue runUpdate() {
        
        System.debug('RUN UPDATE ');
        return publishEvent(UPDATE_EVENT);
    }

    private EventQueue publishEvent(String eventName) {
        EventQueue event = new EventBuilder()
          .createEventFor(eventName)
          .withSender('SALESFORCE')
          .withReceiver('TAGUS')
          .buildEvent();
    
        List<OrderPartnerTagusDTO> orderPartnerRequest = getEventRequest();
        event.addPayload(eventName, JSON.serialize(orderPartnerRequest));
        event.save();
        return event;
    }

    private List<OrderPartnerTagusDTO> getEventRequest() { 
        List<OrderPartnerTagusDTO> orderPartnerRequestLst = new List<OrderPartnerTagusDTO>();
        this.orders = ((OrderSelector)Application.Selector.newInstance(Order.SObjectType)).selectOrderFieldsToTagusParse(this.orders);
        Set<Id> aOppSet = new Set<Id>();
        for(Order iOrder : this.orders){
            aOppSet.add(iOrder.OpportunityId);
        }
        if(!aOppSet.isEmpty()){
            Map<Id, List<Product_Opportunity_Assignment__c>> mProdByOpp = ((AccountOpportunitySelector)Application.Selector.newInstance(Account_Opportunity_Relationship__c.SObjectType)).selectProductAssignmentToTagus( aOppSet );
            for(Order iOrder : this.orders){
                OrderPartnerTagusDTO orderPartnerRequest = new OrderPartnerTagusDTO(iOrder, mProdByOpp.get(iOrder.OpportunityId));
                orderPartnerRequestLst.add(orderPartnerRequest);
            }              
        }   
        return orderPartnerRequestLst;
      }

    
}