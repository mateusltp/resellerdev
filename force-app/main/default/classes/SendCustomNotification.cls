public without sharing class SendCustomNotification {
    
    public static void notificationsStructure(String body, String senderId, String targetId, String title, String notificationId, set<String> userIds) {
       
        Messaging.CustomNotification obj = new Messaging.CustomNotification();
        obj.setBody(body);
        obj.setTargetId(targetId);
        obj.setTitle(title);
        obj.setNotificationTypeId(notificationId);
        obj.send(userIds);
    }

    public static void emailStruture(String template, String massiveOwner, String targetId){
        List<OrgWideEmailAddress> lstEmailAddress = [select Id from OrgWideEmailAddress WHERE Address='no-reply@gympass.com'];

        Messaging.SingleEmailMessage email = Messaging.renderStoredEmailTemplate(template, massiveOwner, targetId);

        String emailSubject = email.getSubject();
        String emailTextBody = email.getPlainTextBody();
        
        email.setTargetObjectId(massiveOwner);
        email.setSubject(emailSubject);
        email.setPlainTextBody(emailTextBody);
        email.setOrgWideEmailAddressId(lstEmailAddress[0].Id);
        email.saveAsActivity = false;

        Messaging.sendEmail(new List<Messaging.Email>{email});  
    }

    public void sendNotifications() {
        CustomNotificationType notification = [SELECT Id
                                                FROM CustomNotificationType
                                                WHERE DeveloperName =: 'Massive_Account_Request'
                                                LIMIT 1];
        List<EmailTemplate> emailTemplates = [SELECT Id
                                                FROM EmailTemplate
                                                WHERE DeveloperName =: 'Massive_Request_Completed'
                                                LIMIT 1];

        for(Massive_Account_Request__c massive : (List<Massive_Account_Request__c>)Trigger.New ){
            Massive_Account_Request__c oldMassive = (Massive_Account_Request__c)Trigger.oldMap.get(massive.Id);
            
            if(massive.Status__c != oldMassive.Status__c && massive.Status__c == 'Loading'){
                notificationsStructure(System.Label.Community_Request_Progress,'', massive.Id, massive.Name, notification.Id, new set<String>{UserInfo.getUserId()});
                
            }else if(massive.Status__c != oldMassive.Status__c && massive.Status__c == 'Finished'){
                notificationsStructure(System.Label.Community_Request_Complete,'', massive.Id, massive.Name, notification.Id, new set<String>{UserInfo.getUserId()});
                emailStruture(emailTemplates[0].Id, massive.OwnerId, massive.Id);
            }
        }
    }

    public void sendNotificationError(Massive_Account_Request__c massive) {
        CustomNotificationType notification = [SELECT Id
                                                FROM CustomNotificationType
                                                WHERE DeveloperName =: 'Massive_Account_Request'
                                                LIMIT 1];
     
        List<EmailTemplate> emailTemplatesError = [SELECT Id
                                                FROM EmailTemplate
                                                WHERE DeveloperName =: 'Massive_Request_Not_Completed'
                                                LIMIT 1];

       
        notificationsStructure(System.Label.Community_Request_Not_Created,'', massive.Id, massive.Name, notification.Id, new set<String>{UserInfo.getUserId()});
        emailStruture(emailTemplatesError[0].Id, massive.OwnerId, massive.Id);

    }

    public void sendNotificationAccountExecutive(Account_Request__c request, String developerName, String ownerId) {
     
        List<EmailTemplate> emailTemplates = [SELECT Id
                                                FROM EmailTemplate
                                                WHERE DeveloperName =: developerName
                                                LIMIT 1];

        emailStruture(emailTemplates[0].Id, ownerId, request.Id);
    }

}