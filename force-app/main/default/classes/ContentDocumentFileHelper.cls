/**
 * @description       : 
 * @author            : GEPI@GFT.com
 * @group             : 
 * @last modified on  : 04-07-2022
 * @last modified by  : gft.samuel.silva@ext.gympass.com
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   07-09-2020   GEPI@GFT.com   Initial Version
**/
public with sharing class ContentDocumentFileHelper {

    List<ContentDocument> lstOld = trigger.old;
    List<ContentDocument> lstNew = trigger.new;

    Id recordTypePartner = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gym_Partner').getRecordTypeId();
    Id recordTypePartnerFlow = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Account').getRecordTypeId();
    static boolean hasRun = false;

    public void updateFileType(){
        updateFileType(lstNew);
    }

    public void updateFileType(List<ContentDocument> documents) {
      

        Set<ID> contentDocumentLinkIds = new Set<ID> ();
        Set<ID> contentVersionIds = new Set<ID> ();
        Map<ID, ContentVersion> mapIdContentVersion = new Map<ID, ContentVersion>();
        Map<ID, FileObject__c> mapIdFileObject = new Map<ID, FileObject__c>();
        List<FileObject__c> fileObjLst = new List<FileObject__c>();
        List<ContentVersion> contentVersionLst = new List<ContentVersion>();

            for (ContentDocument contentDocLink : documents) {  
                contentDocumentLinkIds.add(contentDocLink.Id);
            }


            for (ContentVersion contentVesion : [SELECT id, Type_Files_fileupload__c FROM contentversion WHERE contentdocumentid IN :contentDocumentLinkIds]) {  
                contentVersionIds.add(contentVesion.Id);
                contentVersionLst.add(contentVesion);
                mapIdContentVersion.put(contentVesion.Id, contentVesion);
            }

            fileObjLst= [SELECT Id, File_Type__c, Account__c, Account__r.Name, Account__r.RecordType.DeveloperName FROM FileObject__c WHERE conversionId__c IN:contentVersionIds];
            List<FileObject__c> updFileObjLst = new List<FileObject__c>();
            for(ContentVersion contentItem : contentVersionLst) {
                for(FileObject__c fileItem : fileObjLst){    
                    if(contentItem.Type_Files_fileupload__c != fileItem.File_Type__c && (fileItem.Account__r.RecordTypeId == recordTypePartner || fileItem.Account__r.RecordTypeId == recordTypePartnerFlow) ){
                        fileItem.File_Type__c = contentItem.Type_Files_fileupload__c;
                        updFileObjLst.add(fileItem);
                    }
                }

            }

            if(!updFileObjLst.isEmpty()){
                update updFileObjLst;
            }
 
    }

    public void deleteFileType(){
        new DeleteContentDocumentFileHelper().deleteimage(lstOld);
    }

    private without sharing class DeleteContentDocumentFileHelper {
    
        public void deleteimage(List<ContentDocument> lstOld){
            deleteFileType(lstOld);
        }
    
        private void deleteFileType(List<ContentDocument> documents) {
            Set<ID> contentDocumentLinkIds = new Set<ID> ();
            Set<ID> contentVersionIds = new Set<ID> ();
            List<FileObject__c> fileObjLst = new List<FileObject__c>();
    
                for (ContentDocument contentDocLink : documents) {  
                    contentDocumentLinkIds.add(contentDocLink.Id);
                }
    
                for (ContentVersion contentVesion :[SELECT id FROM contentversion WHERE contentdocumentid IN :contentDocumentLinkIds]) {  
                    contentVersionIds.add(contentVesion.Id);
                }
                fileObjLst= [SELECT Id FROM FileObject__c WHERE conversionId__c IN:contentVersionIds];
                delete fileObjLst;
            
        }
    }
    }