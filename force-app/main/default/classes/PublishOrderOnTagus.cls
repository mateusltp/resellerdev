public without sharing class PublishOrderOnTagus implements Schedulable {
  private List<Order> newOrders;
  private Map<Id, Order> oldOrderMap;

  public void execute() {
    if (UserInfo.getName().equals('Integration SMB Jamor')) {
      return;
    }

    this.newOrders = (List<Order>) Trigger.new;
    this.oldOrderMap = (Map<Id, Order>) Trigger.oldMap;

    List<Account> accounts = new List<Account>();

    for (Order order : newOrders) {
      if (!order.Is_Account_On_Tagus__c && order.Type.equals('Clients')) {
        accounts.add(new Account(Id = order.AccountId));
      }
    }

    if (accounts.isEmpty()) {
      if (System.isFuture() || System.isBatch()) {
        new ScheduleHelper().scheduleIntoNextSeconds(this, 2);
      } else {
        publishOrder();
      }
    } else {
      new TagusAccountOutboundPublisher(accounts).runCreate();
      new ScheduleHelper().scheduleIntoNextSeconds(this, 2);
    }
  }

  public void execute(SchedulableContext context) {
    publishOrder();
  }

  private void publishOrder() {
    List<Order> ordersToCreateOnTagus = new List<Order>();
    List<Order> ordersToUpdateOnTagus = new List<Order>();

    for (Order order : this.newOrders) {
      if (isToPublishOrder(order)) {
        if (order.Is_Order_On_Tagus__c) {
          ordersToUpdateOnTagus.add(order);
        } else {
          ordersToCreateOnTagus.add(order);
        }
      }
    }

    if (!ordersToCreateOnTagus.isEmpty()) {
      new TagusOrderOutboundPublisher(ordersToCreateOnTagus).runCreate();
    }

    if (!ordersToUpdateOnTagus.isEmpty()) {
      new TagusOrderOutboundPublisher(ordersToUpdateOnTagus).runUpdate();
    }
  }

  private Boolean isToPublishOrder(Order newOrder) {
    Order oldOrder = oldOrderMap.get(newOrder.Id);

    return oldOrder.Is_Order_On_Tagus__c == newOrder.Is_Order_On_Tagus__c &&
      newOrder.Type.equals('Clients') &&
      (newOrder.Status.equals('Activated') ||
      newOrder.Status.equals('Canceled') ||
      newOrder.Status.equals('Cancel Requested'));
  }
}