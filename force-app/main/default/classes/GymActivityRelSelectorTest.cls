/**
 * @description       : 
 * @author            : gft.jorge.stevaux@ext.gympass.com
 * @group             : 
 * @last modified on  : 01-03-2022
 * @last modified by  : gft.jorge.stevaux@ext.gympass.com
**/

@isTest(seeAllData=false)

private class GymActivityRelSelectorTest {

    @TestSetup
    static void createData(){

        Gym_Activity_Relationship__c lGymActivityRel = PartnerDataFactory.newGymActivityRel();
        Database.insert(lGymActivityRel);
    }

    @isTest
     static void getSObjectFieldList_Test(){

        List<Schema.SObjectField> expectedReturn = new List<Schema.SObjectField> {
			Gym_Activity_Relationship__c.Id,
			Gym_Activity_Relationship__c.Name
		};

        Test.startTest();
        System.assertEquals(expectedReturn, new GymActivityRelSelector().getSObjectFieldList());
        Test.stopTest();
    }

    @isTest 
    static void getSObjectType_Test(){

       Test.startTest();
       System.assertEquals(Gym_Activity_Relationship__c.sObjectType, new GymActivityRelSelector().getSObjectType());
       Test.stopTest();
   }

   @isTest 
     static void selectById_Test(){

        List<Gym_Activity_Relationship__c> gymActivitiesLst = [SELECT Id, Name FROM Gym_Activity_Relationship__c LIMIT 1];
        Set<Id> activitiesIds = (new Map<Id, Gym_Activity_Relationship__c>(gymActivitiesLst)).keySet();

        Test.startTest();
        system.assertEquals(gymActivitiesLst.get(0).id, new GymActivityRelSelector().selectById(activitiesIds).get(0).id);
        Test.stopTest();
    }

   @isTest 
     static void selectAllGymActivitiesRel_Test(){

        List<Gym_Activity_Relationship__c> gymActivitiesLst = [SELECT Id, Name FROM Gym_Activity_Relationship__c LIMIT 500];

        Test.startTest();
        system.assertEquals(gymActivitiesLst.get(0).id, new GymActivityRelSelector().selectAllGymActivitiesRel().get(0).id);
        Test.stopTest();
    }


}