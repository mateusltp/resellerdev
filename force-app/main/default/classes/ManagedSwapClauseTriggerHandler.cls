public class ManagedSwapClauseTriggerHandler extends TriggerHandler {
    public override void AfterInsert() {
        new ManagedSwapClauseTriggerHelper().createManagedClauses(trigger.newMap);
    }

    public override void AfterUpdate() {
        new ManagedSwapClauseTriggerHelper().createManagedClauses(trigger.newMap);
    }

    public override void AfterDelete() {
        new ManagedSwapClauseTriggerHelper().createManagedClauses(trigger.oldMap); 
    }
}