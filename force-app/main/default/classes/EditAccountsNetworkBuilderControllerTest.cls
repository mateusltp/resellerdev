/**
 * @description       : 
 * @author            : gepi@gft.com
 * @group             : 
 * @last modified on  : 04-08-2022
 * @last modified by  : gepi@gft.com
**/
@isTest
public with sharing class EditAccountsNetworkBuilderControllerTest {
    @TestSetup
    static void makeData(){
        Id recTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Account').getRecordTypeId();

        Account parentAccount = new Account(
            RecordTypeId = recTypeId
            ,Name = 'Parent Account',
            BillingCountry = 'Brazil',
            BillingState = 'São Paulo',
            BillingCity = 'São Paulo',
            BillingStreet = 'Casa do Ator'
            //,Partner_Level__c = 'Brand'
        );
        insert parentAccount;

        Account childAccount = new Account(
            RecordTypeId = recTypeId
            ,ParentId = parentAccount.Id
            ,Name = 'Child Account'
            ,BillingCountry = 'Brazil'
            ,BillingState = 'São Paulo'
            ,BillingCity = 'São Paulo'
            ,BillingStreet = 'Casa do Ator'
            //,Partner_Level__c = 'Location'
        );

        insert childAccount;


    }
    
    @isTest
    public static void testGetAccountList() {
        List<Account> accList = [
            SELECT Id FROM Account
        ];

        List<Id> accIdList = new List<Id>();

        for (Account acc : accList) {
            accIdList.add(acc.Id);
        }

        EditAccountsNetworkBuilderController.getAccountList(accIdList);
    }

    @isTest
    public static void testUpdateAccounts() {
        List<EditAccountsNetworkBuilderController.AccountWrapper> accWrapperLst = new List<EditAccountsNetworkBuilderController.AccountWrapper>();

        for (Account acc : [SELECT Id, Name, Partner_Level__c FROM Account]) {
            EditAccountsNetworkBuilderController.AccountWrapper wrapper = new EditAccountsNetworkBuilderController.AccountWrapper();
            wrapper.Id = acc.Id;
            wrapper.PartnerLevel = (acc.Name.contains('Child') ? 'Location' : 'Brand');
            wrapper.Name = acc.Name;

            accWrapperLst.add(wrapper);
        }
        
        Test.startTest();
        EditAccountsNetworkBuilderController.updateAccounts(
            JSON.deserializeUntyped(JSON.serialize(accWrapperLst))
        );
        Test.stopTest();

        for (Account acc : [SELECT Id, Name, Partner_Level__c FROM Account]) {
            if (acc.Name == 'Parent Account') {
                System.assertEquals('Brand', acc.Partner_Level__c);
            }

            if(acc.Name == 'Child Account') {
                System.assertEquals('Location', acc.Partner_Level__c);
            }
        }
    }

    @isTest
    public static void testUpdateAccountsError() {
        List<EditAccountsNetworkBuilderController.AccountWrapper> accWrapperLst = new List<EditAccountsNetworkBuilderController.AccountWrapper>();

        for (Account acc : [SELECT Id, Name, Partner_Level__c FROM Account]) {
            EditAccountsNetworkBuilderController.AccountWrapper wrapper = new EditAccountsNetworkBuilderController.AccountWrapper();
            wrapper.Id = acc.Id;
            wrapper.PartnerLevel = 'Location';
            wrapper.Name = acc.Name;

            accWrapperLst.add(wrapper);
        }
        
        Test.startTest();
        try {
            EditAccountsNetworkBuilderController.updateAccounts(
                JSON.deserializeUntyped(JSON.serialize(accWrapperLst))
            );
        } catch(Exception e) {
            System.debug(e.getMessage());
        }
        
        Test.stopTest();
    }
}