/**
 * @author vncferraz
 * Provide Unit Test for OfferProcessor orchestration and methods
 */
@isTest(seeAllData=false)
public class OfferProcessorTest {
    
    @testSetup
    private static void createData(){
        Pricebook2 lStandardPricebook = DataFactory.newPricebook();
        Database.update( lStandardPricebook );

        Account lAccEntity = DataFactory.newGympassEntity( 'Gympass' );
        lAccEntity.Id_Company__c = '15.664.649/0001-84';
        Database.insert( lAccEntity );
        
        Account lAcc = DataFactory.newAccount();              
        Database.insert( lAcc );

        Contact contact = DataFactory.newContact(lAcc, '1');
        contact.Primary_HR_Contact__c = true;
        Database.insert( contact );
                     
        Product2 lAccessFee = DataFactory.newProduct('Enterprise Subscription', false, 'BRL');
        lAccessFee.Minimum_Number_of_Employees__c =0;
        lAccessFee.Maximum_Number_of_Employees__c =99999;
        lAccessFee.Family = 'Enterprise Subscription';
        lAccessFee.Family_Member_Included__c = true;
        Database.insert( lAccessFee );
        
        Product2 lSetupFee = DataFactory.newProduct('Setup Fee', false, 'BRL');
        Database.insert( lSetupFee ); 
        
       // Opportunity lNewBusinessOpp = DataFactory.newOpportunity( lAcc.Id, lStandardPricebook, 'Client_Sales_New_Business' ); 
       // lNewBusinessOpp.CurrencyIsoCode = 'BRL';    
       // Database.insert( lNewBusinessOpp );

        PricebookEntry lAccessFeeEntry = DataFactory.newPricebookEntry(lStandardPricebook, lAccessFee, new Opportunity(CurrencyIsoCode='BRL'));
        lAccessFeeEntry.IsActive = true;
        Database.insert(lAccessFeeEntry);
        
        Offer_Queue__c offerQueue1 = new Offer_Queue__c();

        offerQueue1.Account_ID__c = lAcc.Id;
        offerQueue1.Adjusted_Probability__c = null;
        offerQueue1.Allowlist_Enabler__c = 'Yes';        
        offerQueue1.CurrencyIsoCode = 'BRL';
        offerQueue1.End_Date__c = System.today().addDays(365);
        offerQueue1.ES_Billing_Percentage__c = 100;
        offerQueue1.ES_Payment_Frequency__c = 'Monthly';
        offerQueue1.Family_Member__c = 'Yes';
        offerQueue1.Free_Plan__c = 'No';
        offerQueue1.Number_of_Employees__c = 900;
        offerQueue1.Close_Date__c = System.today();
        //offerQueue1.Opportunity_ID__c = lNewBusinessOpp.Id;
        offerQueue1.Parent_Account_ID__c = null;
        offerQueue1.Price_index__c = 'IPCA';
        offerQueue1.Price_index_description__c = '';
        offerQueue1.Reference_Sales_Price_per_Eligible__c = 4.45;
        offerQueue1.Stage__c = 'Launched/Won';
        offerQueue1.Start_Date__c = System.today();
        offerQueue1.Status__c = 'Pending';
        offerQueue1.Status_Detail__c = '';
        offerQueue1.Comments__c = '[expansion]';
        offerQueue1.Type_of_Request__c = 'SMB Sales';
        offerQueue1.Waiver_End_Date__c = System.today().addDays(90);
        offerQueue1.Waiver_Percentage__c = 100;
        offerQueue1.Waiver_Start_Date__c = System.today().addDays(30);
        offerQueue1.Full_Launch__c = 'Yes';
        offerQueue1.Exclusivity_clause__c = 'Yes';
        insert offerQueue1;

        //Offer_Queue__c offerQueue2 = new Offer_Queue__c();

        //offerQueue2.ES_Billing_Percentage__c = 100;
        //offerQueue2.Opportunity_ID__c = lNewBusinessOpp.Id;
        //offerQueue2.Stage__c = 'Launched/Won';
        //offerQueue2.Type_Of_Request__c = 'Close';
        //insert offerQueue2;
    }
    @isTest
    private static void testRenegotiationUpdate(){
        Offer_Queue__c offerQueue = [select Id,Status__c,Status_Detail__c,Opportunity_ID__c,Account_ID__c from Offer_Queue__c where Type_of_request__c <> 'Close' limit 1];
        System.assertEquals( offerQueue.Status__c, 'Pending' );
       
        OfferQueueBatchable batchable = new OfferQueueBatchable();
        Test.startTest();
        Database.executeBatch(batchable);
        Test.stopTest();

        offerQueue = [select Id,Status__c,Status_Detail__c,Opportunity_ID__c,Account_ID__c from Offer_Queue__c limit 1];
        System.debug( ' offerQueue = ' + JSON.serialize(offerQueue) );

       
        Opportunity opp = [select Id ,StageName,Name from Opportunity where AccountId = :offerQueue.Account_ID__c ];
        System.assertEquals( opp.StageName, 'Lançado/Ganho');
        
        List<Quote> proposals = [select Id from Quote where Opportunity.AccountId = :offerQueue.Account_ID__c];
        System.assert(!proposals.isEmpty());
        

        QuoteLineItem enterpriseSubscription = [select Id from QuoteLineItem where QuoteId = :proposals.get(0).Id limit 1];
        System.assertNotEquals(enterpriseSubscription.Id,null);

        Payment__c billingSettings = [select Id from Payment__c where Quote_Line_Item__c = :enterpriseSubscription.Id limit 1];
        System.assertNotEquals(billingSettings.Id,null);

        Eligibility__c eligibility = [select Id from Eligibility__c where Payment__c = :billingSettings.Id limit 1];
        System.assertNotEquals(eligibility.Id,null);

        Waiver__c waiver = [select Id from Waiver__c where Payment__c = :billingSettings.Id limit 1];
        System.assertNotEquals(waiver.Id,null);


    }

    /*@isTest
    private static void testOfferClose(){
        Offer_Queue__c offerQueue = [select Id,Status__c,Status_Detail__c,Opportunity_ID__c from Offer_Queue__c where Type_of_Request__c = 'Close' limit 1];
        System.assertEquals( offerQueue.Status__c, 'Pending' );
        
        Opportunity opp = [select Id ,Name, StageName from Opportunity where Id = :offerQueue.Opportunity_ID__c ];
        System.assertNotEquals( opp.Id, null);
        
        OffersToCloseBatchable batchable = new OffersToCloseBatchable();
        Test.startTest();
        Database.executeBatch(batchable);
        Test.stopTest();

        offerQueue = [select Id,Status__c,Status_Detail__c,Opportunity_ID__c from Offer_Queue__c where Type_of_Request__c = 'Close' limit 1];
        System.debug( ' offerQueue = ' + JSON.serialize(offerQueue) );

        opp = [select Id ,Name,StageName from Opportunity where Id = :offerQueue.Opportunity_ID__c ];
        System.assertEquals( opp.StageName, 'Lançado/Ganho');        

    }*/
}