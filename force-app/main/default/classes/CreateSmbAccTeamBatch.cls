/**
 * @description       : 
 * @author            : AlyssonMota
 * @group             : 
 * @last modified on  : 07-15-2021
 * @last modified by  : Alysson Mota
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   07-14-2021   Alysson Mota   Initial Version
**/
global with sharing class CreateSmbAccTeamBatch implements Database.Batchable<sObject>, Database.Stateful {
    
    private User_For_SMB_Allocation__mdt smbUserOwnerMtd;
    
    public CreateSmbAccTeamBatch() {
        this.smbUserOwnerMtd = getSmbUserMtd();
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
    	String smbUserEmailPrefix = '';
        
        if (this.smbUserOwnerMtd != null) {
            smbUserEmailPrefix = this.smbuserOwnerMtd.User_email__c.split('@')[0];
        }

       	String query = 'SELECT Id, (Select Id, UserId From AccountTeamMembers) FROM Account WHERE Owner.Email LIKE \'' + smbUserEmailPrefix + '%\'';
        
       	return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext bc, List<Account> scope){
        List<AccountTeamMember> atmToInsert = new List<AccountTeamMember>();
        List<AccountTeamMember> atmToRemove = new List<AccountTeamMember>();

        User smbUser = getUser(this.smbUserOwnerMtd.User_email__c);

        Map<Id, UserAccountTeamMember> userIdToUserAccountTeamMember = new Map<Id, UserAccountTeamMember>();
        if (smbUserOwnerMtd != null) {
            userIdToUserAccountTeamMember = getUserAccountTeamMembersAsMap(smbUser.Id);
        }

        for (Account acc : scope) {
            if (acc.AccountTeamMembers.size() == 0) {
                atmToInsert.addAll(createAccountTeamInstances(acc, userIdToUserAccountTeamMember.values())); 
            } else if (acc.AccountTeamMembers.size() != userIdToUserAccountTeamMember.values().size()) {
                atmToRemove.addAll(acc.AccountTeamMembers);
                atmToInsert.addAll(createAccountTeamInstances(acc, userIdToUserAccountTeamMember.values()));
            } 
        }

        // Remove ATM
        Database.delete(atmToRemove, false);

        // Insert ATM
        Database.insert(atmToInsert, false);
    }

    private List<AccountTeamMember> createAccountTeamInstances(Account acc, List<UserAccountTeamMember> uatmList) {
        System.debug('createAccountTeamInstances');
        List<AccountTeamMember> accountTeamMembers = new List<AccountTeamMember>();

        for (UserAccountTeamMember uatm : uatmList) {
            AccountTeamMember atm = new AccountTeamMember();
            atm.UserId = uatm.UserId;
            atm.AccountId = acc.Id;
            atm.TeamMemberRole = uatm.TeamMemberRole;
            atm.AccountAccessLevel = uatm.AccountAccessLevel;
            atm.OpportunityAccessLevel = uatm.OpportunityAccessLevel;
            atm.CaseAccessLevel = uatm.CaseAccessLevel;
            
            accountTeamMembers.add(atm);
        }

        return accountTeamMembers;
    }

    global void finish(Database.BatchableContext bc){
    
    }

    private Map<Id, UserAccountTeamMember> getUserAccountTeamMembersAsMap(Id userId) {
        Map<Id, UserAccountTeamMember> userIdToUserAccountTeamMember = new Map<Id, UserAccountTeamMember>();
        List<UserAccountTeamMember> userAccTeamMemberList = getUserAccountTeamMembersByEmail(userId);

        for (UserAccountTeamMember uatm : userAccTeamMemberList) {
            userIdToUserAccountTeamMember.put(uatm.UserId, uatm);
        }

        return userIdToUserAccountTeamMember;
    }

    private List<UserAccountTeamMember> getUserAccountTeamMembersByEmail(Id userId) {
        List<UserAccountTeamMember> userAccTeamMemberList = [
            SELECT Id, TeamMemberRole, UserId, OwnerId, User.Name, AccountAccessLevel, OpportunityAccessLevel, CaseAccessLevel
            FROM UserAccountTeamMember
            WHERE OwnerId =: userId
        ];

        return userAccTeamMemberList;
    }

    private User_For_SMB_Allocation__mdt getSmbUserMtd() {
        return [
            SELECT Id, User_email__c
            FROM User_For_SMB_Allocation__mdt
            WHERE User_for_Account_Team__c = true
            LIMIT 1
        ];
    }

    private User getUser(String email) {
        String emailPrefix = email.split('@')[0] + '%';
        
        return [
            SELECT Id
            FROM User
            WHERE Email LIKE :emailPrefix
            LIMIT 1
        ];
    }
}