/**
 * @author vncferraz
 * 
 * Batchable to provide processing batchs for the Offer_Queue__c massive upload for Subsidiary Type of Request
 */
global class OfferQueueSubsidiaryBatchable implements Database.Batchable<sObject>{
   
    private List<Offer_Queue__c> processedScope = new List<Offer_Queue__c>();

    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator('select Id,Account_ID__c,Comments__c,Allowlist_Enabler__c,Full_Launch__c,Exclusivity_clause__c,ES_Billing_Percentage__c,ES_Payment_Frequency__c,Family_Member__c,Free_Plan__c,Number_of_Employees__c,Opportunity_ID__c,Parent_Account_ID__c,Reference_Sales_Price_per_Eligible__c,Stage__c,Status__c,Status_Detail__c,Type_of_Request__c,Waiver_End_Date__c,Waiver_Percentage__c,Waiver_Start_Date__c,Start_Date__c,End_Date__c,Price_Index__c,Price_Index_Description__c,CreatedById from Offer_Queue__c where Status__c = \'Pending\' and Type_of_Request__c = \'Subsidiary\'  ');
    }

    public void execute(Database.BatchableContext BC, List<Offer_Queue__c> scope){

        OfferQueueProcessor processor = new OfferSubsidiaryProcessor(scope);

        processedScope.addAll( 

            processor.run() 
        );

        update this.processedScope;
    }
    
    public void finish(Database.BatchableContext BC){
        
    }
    
}