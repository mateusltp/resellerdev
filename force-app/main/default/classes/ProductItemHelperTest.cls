/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 05-17-2022
 * @last modified by  : alysson.mota@gympass.com
 * Modifications Log 
 * Ver   Date         Author                             Modification
 * 1.0   07-12-2021   gft.samuel.silva@ext.gympass.com   Initial Version
**/
@isTest(isParallel=true)
public with sharing class ProductItemHelperTest {

   
    @TestSetup
    static void SetupData(){
        fflib_ISObjectUnitOfWork uow = Application.UnitOfWork.newInstance();

        Account acc = getPartnerAccount();
        insert acc;

        Opportunity opp = getPartnerOpportunity(acc.Id);
        insert opp;
        
        Product_Item__c prodOldGymFlow = getPartnerProduct(opp.Id);
        insert prodOldGymFlow;

        Product_Item__c prodNewPartnerFlow = getNewFlowPartnerProduct('Product parent');
        insert prodNewPartnerFlow;

        Product_Item__c prodNewPartnerFlowChild = getNewFlowPartnerProduct('Product child');
        prodNewPartnerFlowChild.Parent_Product__c = prodNewPartnerFlow.Id;
        insert prodNewPartnerFlowChild;
    }

    @isTest
    static void insertDuplicatedProduct() {
               
        Opportunity opp = [SELECT Id FROM Opportunity WHERE Name = 'academiaBrasilCompanyOpp'];
        
        List<Product_Item__c> prodLst = new List<Product_Item__c>();
        Product_Item__c dupProductItem = getPartnerProduct(opp.Id);
        dupProductItem.Name = 'DupProd';
        Product_Item__c dupProductItem2 = getPartnerProduct(opp.Id);
        dupProductItem2.Name = 'DupProd';

        prodLst.add(dupProductItem);
        prodLst.add(dupProductItem2);

        Test.startTest();
            try{
                INSERT prodLst;
            }catch(Exception e) {
                String lErrorMessage = 'Duplicated product found';
                System.Assert( e.getMessage().contains(lErrorMessage) );
            }        
        Test.stopTest();
    }


    @isTest
    static void insertNewProduct() {
               
        Opportunity opp = [SELECT Id FROM Opportunity WHERE Name = 'academiaBrasilCompanyOpp'];
        
        List<Product_Item__c> prodLst = new List<Product_Item__c>();
        Product_Item__c newProductItem = getPartnerProduct(opp.Id);
        newProductItem.Name = 'NewProduct';
        Product_Item__c newProductItem2 = getPartnerProduct(opp.Id);
        newProductItem2.Name = 'NewProduct2';
        prodLst.add(newProductItem);
        prodLst.add(newProductItem2);
        Test.startTest();       
            INSERT prodLst;
            prodLst.clear();
            prodLst = [SELECT Id FROM Product_Item__c WHERE Opportunity__c =: opp.id];
            System.assertEquals(3, prodLst.size());
        Test.stopTest();
    }

    @isTest
    static void updateChildProductsTest() {
        Id prodRt = Schema.SObjectType.Product_Item__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product').getRecordTypeId();    
        Product_Item__c parentProduct = [SELECT Id, No_Show_Fee_Percent__c FROM Product_Item__c WHERE RecordTypeId =: prodRt AND Name = 'Product parent' LIMIT 1];
        
        Test.startTest();       
            parentProduct.No_Show_Fee_Percent__c = '20';
            update parentProduct;
            
            Product_Item__c childProduct = [SELECT Id, No_Show_Fee_Percent__c FROM Product_Item__c WHERE RecordTypeId =: prodRt AND Parent_Product__c = :parentProduct.Id LIMIT 1];
            System.assertEquals('20', childProduct.No_Show_Fee_Percent__c);
        Test.stopTest();
    }

    private static Product_Item__c getPartnerProduct(Id oppId){
        Id prodRt = Schema.SObjectType.Product_Item__c.getRecordTypeInfosByDeveloperName().get('Gyms_Product_Item_Partner').getRecordTypeId();    
        Product_Item__c lProductItem = new Product_Item__c();    
        lProductItem.recordTypeId = prodRt;
        lProductItem.Product_Type__c = 'In person';
        lProductItem.Is_Network_CAP__c = 'Yes';
        lProductItem.CurrencyIsoCode = 'BRL';
        lProductItem.Do_You_Have_A_No_Show_Fee__c = 'No';
        lProductItem.Has_Late_Cancellation_Fee__c = 'No';
        lProductItem.Late_Cancellation_Percent__c = '0';
        lProductItem.Maximum_Live_Class_Per_Month__c = 0;
        lProductItem.No_Show_Fee_Percent__c = '0';
        lProductItem.Net_Transfer_Price__c  =10;
        lProductItem.Product_Definition__c  ='Dança';
        lProductItem.Product_Restriction__c = 'None';
        lProductItem.Name = 'DupProd';
        lProductItem.Opportunity__c = oppId;    
        lProductItem.Max_Monthly_Visit_Per_User__c = 12;
        lProductItem.Reference_Price_Value_Unlimited__c = 12;
        lProductItem.Price_Visits_Month_Package_Selected__c = 400;
        return lProductItem;
    }


    private static Product_Item__c getNewFlowPartnerProduct(String productName){
        Id prodRt = Schema.SObjectType.Product_Item__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product').getRecordTypeId();    
        Product_Item__c lProductItem = new Product_Item__c();    
        lProductItem.Name = productName;
        lProductItem.recordTypeId = prodRt;
        lProductItem.Package_Type__c = 'Value per class';
        lProductItem.Type__c = 'In person';
        return lProductItem;
    }

    private static Account getPartnerAccount(){
        Id accRt =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gym_Partner').getRecordTypeId();
        Account acc = new Account();
        acc.name='AcademiaBrasilCompanyPai';
        acc.RecordTypeId = accRt;
        acc.GP_Status__c = 'Pending Approval';
        acc.ShippingState = 'Minas Gerais';
        acc.CAP_Value__c = 120;
        acc.ShippingCity = 'CityAcademiaBrasil';
        acc.ShippingStreet = 'Rua academiabrasilpai';
        acc.ShippingCountry = 'Brazil';
        acc.Gym_Type__c = 'Studios';
        acc.Types_of_ownership__c = Label.franchisePicklist;
        acc.Subscription_Type__c = 'Value per class';
        acc.Subscription_Period__c = 'Monthy value';
        acc.Subscription_Type_Estimated_Price__c    = 100;
        acc.Has_market_cannibalization__c = 'No';
        acc.Exclusivity__c = 'Yes';
        acc.Exclusivity_End_Date__c = Date.today().addYears(1);
        acc.Exclusivity_Partnership__c = 'Full Exclusivity';
        acc.Exclusivity_Restrictions__c= 'No';
        acc.Website = 'testing@tesapex.com';
        acc.Gym_Email__c = 'gymemail@apex.com';
        acc.Phone = '3222123123';
        return acc;
    }


    private static Opportunity getPartnerOpportunity(Id accId){
        Id oppRt = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Gyms_Small_Medium_Partner').getRecordTypeId();
        Opportunity opp = new Opportunity();
        opp.recordTypeId = oppRt;
        opp.AccountId =accId;
        opp.Name = 'academiaBrasilCompanyOpp'; 
        opp.CMS_Used__c = 'Yes';     
        opp.Gym_agreed_to_an_API_integration__c  = 'Yes';
        opp.Club_Management_System__c = 'ABC Financial';
        opp.Integration_Fee_Deduction__c = 'No';
        opp.CloseDate = Date.today();
        opp.Success_Look_Like__c = 'Yes';
        opp.Success_Look_Like_Description__c = 'Money money';
        opp.StageName = 'Qualificação';
        opp.Type = 'Expansion';     
        opp.CurrencyIsoCode = 'BRL';
        opp.Gympass_Plus__c = 'Yes';
        opp.Standard_Payment__c = 'Yes';
        return opp;
    }
    
}