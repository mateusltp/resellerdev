/**
 * @description       : 
 * @author            : bruno.mendes@gympass.com
 * @group             : 
 * @last modified on  : 28/04/2022
 * @last modified by  : bruno.mendes@gympass.com
**/

@IsTest
private class ContentVersionSelectorTest {
    @TestSetup
    static void setup() {
        Account account = PartnerDataFactory.newAccount();
        insert account;

        ContentVersion cv = new ContentVersion(
                Title = 'photo_test',
                PathOnClient = 'photo_test.jpg',
                VersionData = Blob.valueOf('jpgfile'),
                Type_Files_fileupload__c = 'Photo',
                FirstPublishLocationId = account.Id
        );
        insert cv;
    }

    @isTest
    static void getSObjectFieldList_Test(){

        List<Schema.SObjectField> schSObjLst = new List<Schema.SObjectField> {
                ContentVersion.Id,
                ContentVersion.Type_Files_fileupload__c
        };

        Test.startTest();
        System.assertEquals(schSObjLst, new ContentVersionSelector().getSObjectFieldList());
        Test.stopTest();
    }

    @isTest
    static void getSObjectType_Test(){

        Test.startTest();
        System.assertEquals(ContentVersion.sObjectType, new ContentVersionSelector().getSObjectType());
        Test.stopTest();
    }

    @isTest
    static void selectById_Test(){

        List<ContentVersion> contentVersions = [SELECT Id FROM ContentVersion LIMIT 1];

        Test.startTest();
        system.assertEquals(contentVersions[0].Id, new ContentVersionSelector().selectById(new Set<Id> {contentVersions[0].Id}).get(0).id);
        Test.stopTest();
    }

    @isTest
    static void selectConventVersionById_Test(){

        List<ContentVersion> contentVersions = [SELECT Id FROM ContentVersion LIMIT 1];

        Test.startTest();
        system.assertEquals(contentVersions[0].Id, new ContentVersionSelector().selectConventVersionById(new Set<Id> {contentVersions[0].Id}).values().get(0).id);
        Test.stopTest();
    }

    @isTest
    static void selectByContentDocumentId_Test(){

        List<ContentVersion> contentVersions = [SELECT Id, ContentDocumentId FROM ContentVersion LIMIT 1];

        Test.startTest();
        system.assertEquals(contentVersions[0].Id, new ContentVersionSelector().selectByContentDocumentId(new Set<Id> {contentVersions[0].ContentDocumentId}).get(0).id);
        Test.stopTest();
    }

    @isTest
    static void selectByFirstPublishLocations_Test(){

        List<ContentVersion> contentVersions = [SELECT Id, FirstPublishLocationId FROM ContentVersion LIMIT 1];

        Test.startTest();
        system.assertEquals(contentVersions[0].Id, new ContentVersionSelector().selectByFirstPublishLocations(new Set<Id> {contentVersions[0].FirstPublishLocationId}).get(0).id);
        Test.stopTest();
    }
}