public with sharing class ResellerListViewMultipleController {

   
    @AuraEnabled(cacheable=true)
    public static List<Account_Request__c> getSearchValue(String massAccReqId, String searchKey, String engineStatus) {

        if(searchKey != ''){
        Set<Id> ids = new Set<Id>();
             
        String searchStr = ''+searchKey+'';
        String searchQuery = 'FIND \'' + searchStr + '\' IN Name FIELDS RETURNING Account_Request__c(Name, Id WHERE Massive_Account_Request__c = :massAccReqId)';
        
        List<List<Account_Request__c>> searchList = search.query(searchQuery);
        Account_Request__c [] accountRequest = ((List<Account_Request__c>)searchList[0]);
        

        if(accountRequest != null){
       
            for(Account_Request__c request : accountRequest){
                
                ids.add(request.Id);
            }
            
            List<Account_Request__c> accountRequestList = [SELECT Massive_Account_Request__c, Id, Name, Website__c, Unique_Identifier_Type__c, Unique_Identifier__c, toLabel(Partner_Model__c),Total_number_of_employees__c,toLabel(Billing_Country__c),Status__c, Error_message__c, CreatedDate, Engine_Status__c
            FROM Account_Request__c 
            WHERE Engine_Status__c = :engineStatus AND Id = :ids];
            
            return accountRequestList;   
        }
    }

    else {

        return getAccountRequest(massAccReqId, engineStatus);
    }
        
        return null;
    }

    @AuraEnabled(cacheable=true)
    public static List<Account_Request__c> getAccountRequest(String massAccReqId, String engineStatus) {
        return [
            SELECT Massive_Account_Request__c, Id, Name, Website__c, Unique_Identifier_Type__c, Unique_Identifier__c, toLabel(Partner_Model__c),Total_number_of_employees__c,toLabel(Billing_Country__c), CreatedDate, Status__c, Error_message__c
            FROM Account_Request__c
            WHERE Massive_Account_Request__c = :massAccReqId AND Engine_Status__c = :engineStatus
        ];
    }

    @AuraEnabled(cacheable=true)
    public static string getPicklistValuesByApiName(String fieldApiName){
        return ResellerUtil.getPicklistValuesByObjectApiNameAndFieldApiName('Account_Request__c', fieldApiName);
    }


    @AuraEnabled
    public static string updateAccountRequests(Object data) {

        List<Account_Request__c> accountRequestsForUpdate = (List<Account_Request__c>) JSON.deserialize(
            JSON.serialize(data),
            List<Account_Request__c>.class
        );

        Map<String, String> billingCountry = ResellerUtil.getPicklistValuesMap('Account_Request__c', 'Billing_Country__c');
        Map<String, String> partnerModel = ResellerUtil.getPicklistValuesMap('Account_Request__c', 'Partner_Model__c');

        for(Account_Request__c request : accountRequestsForUpdate){
            if(request.Billing_Country__c == '')throw new AuraHandledException(System.Label.Community_MassiveValidation_CountryEmpty);
            if(request.Billing_Country__c != null){
                if(billingCountry.containsKey(request.Billing_Country__c)){
                    request.Billing_Country__c = billingCountry.get(request.Billing_Country__c);
                    break;
                }
                else{
                    throw new AuraHandledException(System.Label.Community_MassiveValidation_CountryMisstyped);
                }
            }
            
        }

        for(Account_Request__c request : accountRequestsForUpdate){
            if(request.Partner_Model__c == '')throw new AuraHandledException(System.Label.Community_MassiveValidation_BusinessModelEmpty);
            if(request.Partner_Model__c != null){
                if(partnerModel.containsKey(request.Partner_Model__c)){
                    request.Partner_Model__c = partnerModel.get(request.Partner_Model__c);
                    break;
                }
                else{
                    throw new AuraHandledException(System.Label.Community_MassiveValidation_BusinessModelMisstyped);
                }
            }
        }

        try {
            
            update accountRequestsForUpdate;
            return 'Success: Account Requests updated successfully';
        }
        catch (Exception e) {
            throw new AuraHandledException('The following exception has occurred: ' + e.getMessage());
        }
    }

   

}