/**
 * @description       : 
 * @author            : roei@gft.com
 * @group             : 
 * @last modified on  : 09-14-2021
 * @last modified by  : roei@gft.com
 * Modifications Log 
 * Ver   Date         Author                              Modification
 * 1.0   09-09-2021   Rafael Reis - GFT (roei@gft.com)    Initial Version
**/
@isTest(SeeAllData=false)
public with sharing class FastTrackSmbDealDeskHandlerTest {

    @isTest 
    private static void shouldRequestDealDeskApproval() {

        FastTrackProposalCreationTO lProposalTo = createMockData();

        lProposalTo.achievedEnablersQuantity = 1;
        lProposalTo.autonomousMarketPlace = 'No';
        lProposalTo.orderExpirationDays = FastTrackDealDeskHandler.ORDER_EXPIRATION_DATE + 1;
        lProposalTo.freeTrialDays = FastTrackDealDeskHandler.FREE_TRIAL + 1;

        Test.startTest();

        FastTrackDealDeskHandler lDealDeskHandler = FastTrackDealDeskHandler.getInstanceByOppId( lProposalTo.opportunityId , lProposalTo );
        lDealDeskHandler.dealDeskEvaluation();

        Test.stopTest();

        System.assertEquals( true , lProposalTo.proposal.dealDeskApprovalNeeded , 'Deal desk doesnt need to approve' );
    }

    private static FastTrackProposalCreationTO createMockData(){    
        Account lParentAcc = DataFactory.newAccount();
        lParentAcc.NumberOfEmployees = 1000;
        Database.insert( lParentAcc );

        Contact lNewContact = DataFactory.newContact( lParentAcc , 'Test Contact' );

        Pricebook2 lStandardPb = DataFactory.newPricebook();

        Opportunity lOpp = new Opportunity(CurrencyIsoCode='BRL');

        lOpp = DataFactory.newOpportunity( lParentAcc.Id , lStandardPb , 'SMB_New_Business' );
        lOpp.Quantity_Offer_Number_of_Employees__c = 500;
        Database.insert( lOpp );

        Product2 lAcessFee = DataFactory.newProduct( 'Enterprise Subscription' , false , 'BRL' );

        PricebookEntry lAccessFeeEntry = DataFactory.newPricebookEntry( lStandardPb , lAcessFee , lOpp );

        Quote lProposal = DataFactory.newQuote( lOpp , 'Quote Test', 'Client_Sales_New_Business' );
        Database.insert( lProposal );

        QuoteLineItem lQuoteAccessFee = DataFactory.newQuoteLineItem( lProposal , lAccessFeeEntry );
        lQuoteAccessFee.Fee_Contract_Type__c = 'Copay 2';

        Payment__c lBillingSetting = DataFactory.newPayment( lQuoteAccessFee );
        lBillingSetting.Frequency__c = 'Yearly';
        lBillingSetting.Billing_Day__c = 'Custom';

        FastTrackProposalCreationTO.Payment lToPayment = new FastTrackProposalCreationTO.Payment();
        lToPayment.sObj = lBillingSetting;

        Eligibility__c lEligibility = DataFactory.newEligibility( lBillingSetting );
        lEligibility.Billing_Day__c = 'Custom';

        FastTrackProposalCreationTO.Eligibility lToEligibility = new FastTrackProposalCreationTO.Eligibility();
        lToEligibility.sObj = lEligibility;

        lToPayment.eligibilities = new List< FastTrackProposalCreationTO.Eligibility >{ lToEligibility };

        Product2 lProfServicesOneFee = DataFactory.newProduct( 'Professional Services Setup Fee' , false , 'BRL' );
        Database.insert( lProfServicesOneFee );
        
        PricebookEntry lProfServicesOneFeeEntry = DataFactory.newPricebookEntry( lStandardPb , lProfServicesOneFee , lOpp );
        Database.insert( lProfServicesOneFeeEntry );

        QuoteLineItem lQuotePsOneFee = DataFactory.newQuoteLineItem( lProposal , lProfServicesOneFeeEntry );
        lQuotePsOneFee.Fee_Contract_Type__c = 'Copay 2';
        Database.insert( lQuotePsOneFee );

        FastTrackProposalCreationTO lProposalTo = new FastTrackProposalCreationTO();
        lProposalTo.opportunityId = lOpp.Id;
        lProposalTo.oppRecTypeDevName = 'SMB_New_Business';
        lProposalTo.proposal = new FastTrackProposalCreationTO.Proposal();
        lProposalTo.proposal.sObj = lProposal;
        lProposalTo.achievedEnablersQuantity = FastTrackDealDeskHandler.ENABLERS_THRESHOLD;
        lProposalTo.autonomousMarketPlace = 'Yes';
        lProposalTo.orderExpirationDays = FastTrackDealDeskHandler.ORDER_EXPIRATION_DATE;
        lProposalTo.freeTrialDays = FastTrackDealDeskHandler.FREE_TRIAL;
        lProposalTo.proposal.accessFee = new FastTrackProposalCreationTO.AccessFee();
        lProposalTo.proposal.accessFee.sObj = lQuoteAccessFee;
        lProposalTo.proposal.accessFee.payments = new List< FastTrackProposalCreationTO.Payment >{ lToPayment };
        lProposalTo.proposal.proServicesOneFee = new FastTrackProposalCreationTO.ProServicesOneFee();
        lProposalTo.proposal.proServicesOneFee.sObj = lQuotePsOneFee;
        lProposalTo.proposal.proServicesOneFee.payments = new List< FastTrackProposalCreationTO.Payment >{ lToPayment };

        return lProposalTo;
    }
}