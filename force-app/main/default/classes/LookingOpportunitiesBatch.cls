global class LookingOpportunitiesBatch implements Database.Batchable<SObject>, Database.stateful {
    
    String errors = '';
    String targetId = '';
    public String massiveId{get;set;}

    global Database.QueryLocator start(Database.BatchableContext BC){
        String status = ResellerEnumRepository.MassiveAccountRequestStatus.LookingForOpportunities.name();
        //List<String> listaStatus = new List<String>{'Analyze', 'Invalid'};
        String query = 'SELECT Id, Name, Massive_Account_Request__c FROM Account_Request__c WHERE Massive_Account_Request__c =:massiveId AND Bulk_Operation__c = true AND Massive_Status__c =:status';
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<Account_Request__c> records) {
        
        try{
            Set<Id> AccountRequestIds = new Set<Id>();
            //Set<Id> massiveAccountRequestId_set = new Set<Id>();
        
            for(Account_Request__c ar : records){
            
                AccountRequestIds.add(ar.Id);
                //massiveAccountRequestId_set.add(ar.Massive_Account_Request__c);
                ar.Massive_Status__c = ResellerEnumRepository.MassiveAccountRequestStatus.LookingForEvents.name();
        }
        
            ResellerEngineGoNoGO engine = new ResellerEngineGoNoGO(AccountRequestIds);
            engine.runSearchOpportunity(true);
        
        
            update records;      
            update new Massive_Account_Request__c( Id = records[0].Massive_Account_Request__c, Status__c = ResellerEnumRepository.MassiveAccountRequestStatus.LookingForEvents.name());
        }
            catch( Exception e){
            errors = errors + e.getMessage() + ' --- ';
            targetId = records[0].Massive_Account_Request__c;
    }
        
    }
    global void finish(Database.BatchableContext BC) {
        if(String.isNotBlank(errors) || Test.isRunningTest()) {

            Set<String> recipientsIds = new Set<String>();
            recipientsIds.add(UserInfo.getUserId());

            CustomNotificationType notificationType = 
            [SELECT Id, DeveloperName 
             FROM CustomNotificationType 
             WHERE DeveloperName='Massive_Account_Request_Batch'];

            Messaging.CustomNotification notification = new Messaging.CustomNotification();
            notification.setTitle('Massive Account Request Error');
            notification.setBody('LookingOpportunitiesBatch Error in your email');
            notification.setNotificationTypeId(notificationType.Id);
            notification.setTargetId(targetId);
            
            try {
                notification.send(recipientsIds);
            }
            catch (Exception e) {
                System.debug('Problem sending notification: ' + e.getMessage());
            }


            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setSubject('Errors occurred during batch process.');
            mail.setTargetObjectId(UserInfo.getUserId());
            mail.setSaveAsActivity(false);
            mail.setPlainTextBody(errors);
            Messaging.sendEmail(new Messaging.Email[] { mail });
        }
        else{

            LookingEventsBatch bt = new LookingEventsBatch();
            bt.massiveId = massiveId;
            ID idOpp = Database.executeBatch(bt);

        }
        
               
        
    }
    
    
    
}