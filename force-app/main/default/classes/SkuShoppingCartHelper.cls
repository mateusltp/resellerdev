/**
 * @description       : 
 * @author            : roei@gft.com
 * @group             : 
 * @last modified on  : 07-18-2022
 * @last modified by  : tiago.ribeiro@gympass.com
**/
global without sharing class SkuShoppingCartHelper {
    global SkuShoppingCartHelper() {}

    @InvocableMethod
    global static List< List< QuoteLineItem > > getSkuProducts( OppRecord[] aLstOpp ) {
         System.debug('aLstOpp' + aLstOpp);
        OppRecord lOpp = aLstOpp.get(0);

        List< SKU_Price__c > lLstSkuPrice = getSkuPrice( lOpp.syncedQuote , lOpp.currencyCode );
        System.debug('---lLstSkuPrice' + lLstSkuPrice);

        Map< String , String > lMapSkuProductIdPbEntryId = getMapSkuProductIdPbEntryId( lLstSkuPrice , lOpp.currencyCode , lOpp.pricebookId);
        System.debug('---lMapSkuProductIdPbEntryId' + lLstSkuPrice);
        List< QuoteLineItem > lLstQuoteLineItem = new List< QuoteLineItem >();

        for( SKU_Price__c iSkuPrice : lLstSkuPrice ){
            QuoteLineItem lQuoteLineItem = new QuoteLineItem();
            lQuoteLineItem.QuoteId = lOpp.syncedQuote;
            lQuoteLineItem.Product2Id = iSkuPrice.Product__c;
            lQuoteLineItem.Plan_Name__c = iSkuPrice.Product_Name__c;
            lQuoteLineItem.PricebookEntryId = lMapSkuProductIdPbEntryId.get( iSkuPrice.Product__c );
            lQuoteLineItem.Quantity = iSkuPrice.Product__r.Has_Fixed_Quantity__c ? 1 : lOpp.quantity;
            lQuoteLineItem.UnitPrice = iSkuPrice.Unit_Price__c;
            lQuoteLineItem.Total_Price__c = lOpp.quantity * iSkuPrice.Unit_Price__c;
            lQuoteLineItem.Discount = 0;
            lQuoteLineItem.List_Price__c = iSkuPrice.Unit_Price__c;
            if(iSkuPrice.Product__r.Professional_services_types__c != null && iSkuPrice.Product__r.Professional_Services_Type_Selection__c != null){
                lQuoteLineItem.Product2 = new Product2(
                    Professional_services_types__c = iSkuPrice.Product__r.Professional_services_types__c,
                    Professional_Services_Type_Selection__c = iSkuPrice.Product__r.Professional_Services_Type_Selection__c
                );
            }
            System.debug('---lQuoteLineItem' + lQuoteLineItem);
            lLstQuoteLineItem.add( lQuoteLineItem );
        }

        return new List< List< QuoteLineItem > >{ lLstQuoteLineItem };
    }
    
    private static List< SKU_Price__c > getSkuPrice( String aQuoteId , String aCurrencyCode ) {
        Set< String > lSetProducts = new Set< String >();

        Quote proposal = [SELECT Id, opportunity.TotalOpportunityQuantity, CreatedDate, Account.BillingCountry FROM Quote WHERE Id = :aQuoteId];
        for( QuoteLineItem iQuoteLineItem : [ SELECT Id, Product2Id FROM QuoteLineItem WHERE QuoteId =: aQuoteId ]){
            lSetProducts.add( iQuoteLineItem.Product2Id );
        }

        Date createdDate = date.newinstance(proposal.CreatedDate.year(), proposal.CreatedDate.month(), proposal.CreatedDate.day());
        Map< String , SKU_Price__c > lMapProductSkuPrice = new Map< String , SKU_Price__c >();
        
        for( SKU_Price__c iSkuPrice : 
            [ SELECT Id, Unit_Price__c, Product_Name__c, Product__c, Minimum_Quantity__c, Product__r.Professional_services_types__c, 
                    Product__r.Has_Fixed_Quantity__c, Product__r.Professional_Services_Type_Selection__c
              FROM SKU_Price__c 
              WHERE CurrencyIsoCode =: aCurrencyCode AND 
                    Product__c != null AND 
                    Product__r.IsActive = true AND
                    Product__c NOT IN : lSetProducts AND
                    Minimum_Quantity__c <= :proposal.opportunity.TotalOpportunityQuantity AND
                    Start_Date__c <= :createdDate AND
                    (End_Date__c = null OR End_Date__c >= :createdDate) AND
                    CountrySKU__c = :proposal.Account.BillingCountry
              ORDER BY Minimum_Quantity__c DESC ] ){
            if( lMapProductSkuPrice.get( iSkuPrice.Product__c ) == null ){
                lMapProductSkuPrice.put( iSkuPrice.Product__c , iSkuPrice );
            }
        }

        return lMapProductSkuPrice.values();
    }
    
    private static Map< String , String > getMapSkuProductIdPbEntryId( List< SKU_Price__c > aLstSkuPrice , String aCurrencyCode , String pricebookId) {
        Map< String , String > lMapSkuProductIdPbEntryId = new Map< String , String >();
        List< String > lLstPbEntryId = new List< String >();

        for( SKU_Price__c iSkuPrice : aLstSkuPrice ){ lLstPbEntryId.add( iSkuPrice.Product__c ); }

        for( PricebookEntry iPbEntry : [ SELECT Id, Product2Id FROM PricebookEntry WHERE Product2Id =: lLstPbEntryId AND Pricebook2Id = :pricebookId AND CurrencyIsoCode =: aCurrencyCode] ){
            lMapSkuProductIdPbEntryId.put( iPbEntry.Product2Id , iPbEntry.Id );
        }

        return lMapSkuProductIdPbEntryId;
    }
    
    global class OppRecord {
        @InvocableVariable
        public String recordId;

        @InvocableVariable
        public String currencyCode;

        @InvocableVariable
        public String syncedQuote;

        @InvocableVariable
        public Integer quantity;

        @InvocableVariable
        public String pricebookId;
    }
}