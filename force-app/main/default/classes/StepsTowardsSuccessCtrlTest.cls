/**
 * @description       : 
 * @author            : AlyssonMota
 * @group             : 
 * @last modified on  : 01-22-2021
 * @last modified by  : Alysson Mota
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   12-01-2020   Alysson Mota   Initial Version
**/
@isTest
public class StepsTowardsSuccessCtrlTest {

    @TestSetup
    static void setup() {
        Id pricebookId = Test.getStandardPricebookId();
        Pricebook2 standardPricebook = new Pricebook2(
                Id = pricebookId,
                IsActive = true
        );
        update standardPricebook;
    }


    @isTest
    public static void testGetResponseForOppB2B() {
        Id accountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Direct Channel').getRecordTypeId();
        Id opportunityRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Client Sales - New Business').getRecordTypeId();
        Id quoteRecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByName().get('Direct Channel').getRecordTypeId();
        
        Account acc = new Account();
        acc.Name = 'Acc Test';
        acc.BillingCountry = 'Brazil';
        acc.BillingState = 'São Paulo';
        acc.RecordTypeId = accountRecordTypeId;
        insert acc;
        
        Contact c = new Contact();
        c.AccountId = acc.Id;
        c.Email = 'contact@test.com';
        c.LastName = 'Test';
        c.Cargo__c = 'CEO';
        insert c;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Opp Test'; 
        opp.StageName = 'Qualificação'; 
        opp.CloseDate = System.now().addDays(30).date();
        opp.RecordTypeId = opportunityRecordTypeId;
        opp.AccountId = acc.Id;
        opp.A_Authority_Opp_Val__c = 'No';
		opp.CH_Challenges_Opp_Val__c = 'No';
		opp.M_Money_Opp_Val__c = 'No';
		opp.P_Priority_Opp_Val__c = 'No';
        insert opp;
        
        Event e = new Event();
        e.DurationInMinutes = 120;
        e.WhatId = opp.Id;
        e.WhoId = c.Id;
        e.ActivityDateTime = datetime.now().addMonths(-1);
        insert e;
        
        opp.StageName = 'Oportunidade Validada Após 1ª Reunião';
        update opp;
        
        Copay_Plan__c membershipFee = new Copay_Plan__c();
        membershipFee.Name = 'Brasil Padrão';
        insert membershipFee;
        
        Quote q = new Quote();
        q.RecordTypeId = quoteRecordTypeId;
        q.Name = 'Proposal Test';
        q.OpportunityId = opp.Id;
        q.Expansion_100_of_the_eligible__c = true;
        q.Payment_method__c = 'Payroll';
        q.Employee_Corporate_Email__c = true;
        q.Employee_Corporate_Email__c = true;
        q.New_Hires__c = true;
        q.HR_Communication__c = true;
        q.Exclusivity_clause__c = true;
        q.PR_in_contract__c = true;
        q.What_does_success_looks_like_Joint_BP__c = true;
        q.Gym__c = membershipFee.Id;
        q.License_Fee_Waiver__c = 'No';
        insert q;
        
        opp.SyncedQuoteId = q.Id;
        
        Test.startTest();
        update opp;
        Test.stopTest();
        
        StepsTowardsSuccessCtrl.getResponse(opp.Id);
        
    }

    @isTest
    public static void testGetResponseForOppSMB() {
        Id accountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Direct Channel').getRecordTypeId();
        Id opportunityRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('SMB').getRecordTypeId();
        Id quoteRecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByName().get('Direct Channel').getRecordTypeId();
        
        Account acc = new Account();
        acc.Name = 'Acc Test';
        acc.BillingCountry = 'Brazil';
        acc.BillingState = 'São Paulo';
        acc.RecordTypeId = accountRecordTypeId;
        insert acc;
        
        Contact c = new Contact();
        c.AccountId = acc.Id;
        c.Email = 'contact@test.com';
        c.LastName = 'Test';
        c.Cargo__c = 'CEO';
        insert c;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Opp Test'; 
        opp.StageName = 'Qualificação'; 
        opp.CloseDate = System.now().addDays(30).date();
        opp.RecordTypeId = opportunityRecordTypeId;
        opp.AccountId = acc.Id;
        opp.A_Authority_Opp_Val__c = 'No';
		opp.CH_Challenges_Opp_Val__c = 'No';
		opp.M_Money_Opp_Val__c = 'No';
		opp.P_Priority_Opp_Val__c = 'No';
        insert opp;
        
        Event e = new Event();
        e.DurationInMinutes = 120;
        e.WhatId = opp.Id;
        e.WhoId = c.Id;
        e.ActivityDateTime = datetime.now().addMonths(-1);
        insert e;
        
        opp.StageName = 'Oportunidade Validada Após 1ª Reunião';
        update opp;
        
        Copay_Plan__c membershipFee = new Copay_Plan__c();
        membershipFee.Name = 'Brasil Padrão';
        insert membershipFee;
        
        Quote q = new Quote();
        q.RecordTypeId = quoteRecordTypeId;
        q.Name = 'Proposal Test';
        q.OpportunityId = opp.Id;
        q.Expansion_100_of_the_eligible__c = true;
        q.Payment_method__c = 'Payroll';
        q.Employee_Corporate_Email__c = true;
        q.Employee_Corporate_Email__c = true;
        q.New_Hires__c = true;
        q.HR_Communication__c = true;
        q.Exclusivity_clause__c = true;
        q.PR_in_contract__c = true;
        q.What_does_success_looks_like_Joint_BP__c = true;
        q.Gym__c = membershipFee.Id;
        q.License_Fee_Waiver__c = 'No';
        insert q;
        
        opp.SyncedQuoteId = q.Id;
        
        Test.startTest();
        update opp;
        Test.stopTest();
        
        StepsTowardsSuccessCtrl.getResponse(opp.Id);
        
    }
    
    @isTest
    public static void testGetResponseForAccB2B() {
        Id accountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Direct Channel').getRecordTypeId();
		
        Account acc = new Account();
        acc.Name = 'Acc Test';
        acc.BillingCountry = 'Brazil';
        acc.BillingState = 'São Paulo';
        acc.RecordTypeId = accountRecordTypeId;
        insert acc;
        
        StepsTowardsSuccessCtrl.getResponse(acc.Id);
    }
    
    @isTest
    public static void testGetResponseForAccRelationshipB2B() {
        Id accountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Direct Channel').getRecordTypeId();
		Id opportunityRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('SMB').getRecordTypeId();
        Id quoteRecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByName().get('Direct Channel').getRecordTypeId();
        
        List<Profile> profiles = [Select Id From Profile Where Name='Relationship'];
        
        User user = new User();
        user.Username = 'r.user@test.com'; 
        user.LastName = 'user'; 
        user.Email = 'r.user@test.com'; 
        user.Alias = 'tuser'; 
        user.TimeZoneSidKey = 'America/Los_Angeles'; 
        user.LocaleSidKey = 'en_US'; 
        user.EmailEncodingKey = 'UTF-8';
        user.ProfileId = profiles.get(0).id;
        user.LanguageLocaleKey = 'en_US';
        
        insert user;
        
        Account acc = new Account();
        acc.Name = 'Acc Test';
        acc.BillingCountry = 'Brazil';
        acc.BillingState = 'São Paulo';
        acc.RecordTypeId = accountRecordTypeId;
        acc.OwnerId = user.Id;
        insert acc;
        
        Contact c = new Contact();
        c.AccountId = acc.Id;
        c.Email = 'contact@test.com';
        c.LastName = 'Test';
        c.Cargo__c = 'CEO';
        insert c;
        
        Event e = new Event();
        e.DurationInMinutes = 120;
        e.WhatId = acc.Id;
        e.WhoId = c.Id;
        e.ActivityDateTime = datetime.now().addMonths(-1);
        insert e;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Opp Test'; 
        opp.StageName = 'Qualificação'; 
        opp.CloseDate = System.now().addDays(30).date();
        opp.RecordTypeId = opportunityRecordTypeId;
        opp.AccountId = acc.Id;
        opp.A_Authority_Opp_Val__c = 'No';
		opp.CH_Challenges_Opp_Val__c = 'No';
		opp.M_Money_Opp_Val__c = 'No';
		opp.P_Priority_Opp_Val__c = 'No';
        opp.Responsavel_Gympass_Relacionamento__c = user.Id;
        insert opp;
        
        opp.StageName = 'Oportunidade Validada Após 1ª Reunião';
        update opp;
        
        Copay_Plan__c membershipFee = new Copay_Plan__c();
        membershipFee.Name = 'Brasil Padrão';
        insert membershipFee;
        
        Quote q = new Quote();
        q.RecordTypeId = quoteRecordTypeId;
        q.Name = 'Proposal Test';
        q.OpportunityId = opp.Id;
        q.Expansion_100_of_the_eligible__c = true;
        q.Payment_method__c = 'Payroll';
        q.Employee_Corporate_Email__c = true;
        q.Employee_Corporate_Email__c = true;
        q.New_Hires__c = true;
        q.HR_Communication__c = true;
        q.Exclusivity_clause__c = true;
        q.PR_in_contract__c = true;
        q.What_does_success_looks_like_Joint_BP__c = true;
        q.Gym__c = membershipFee.Id;
        q.License_Fee_Waiver__c = 'No';
        insert q;
        
        q.Status = 'Sent Decision Maker';
        update q;
        
        Test.startTest();
        opp.StageName = 'Proposta Enviada';
        opp.SyncedQuoteId = q.Id;
        opp.Are_you_using_the_Standard_Pricing_plan__c = 'Yes';
        opp.Does_it_have_unique_technical_request__c = 'No';
        opp.Are_there_License_Fee_changes__c = 'No';
        update opp;
        
        q.Status = 'Decision Maker OK';
        update q;
        
        q.Status = 'Ganho';
        //update q;
        
        opp.StageName = 'Proposta Aprovada';
        opp.Country_Manager_Approval__c = true;
        opp.CloseDate = System.today().addDays(20);
        opp.Data_do_Lancamento__c = System.today().addDays(21);
        //update opp;
        
        opp.StageName = 'Contrato Assinado';
        //update opp;
        
        opp.StageName = 'Lançado/Ganho';
        //update opp;
       
        createSteps(acc, opp, 'Opp');
        
        StepsTowardsSuccessCtrl.getResponse(acc.Id);
        //callStepsTowardsSuccessCtrl(acc.Id);
        Test.stopTest();
    }


     @isTest
    public static void testGetResponseForAccSmbRelationshipB2B() {
        Id accountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Direct Channel').getRecordTypeId();
		Id opportunityRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Client Sales - New Business').getRecordTypeId();
        Id quoteRecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByName().get('Direct Channel').getRecordTypeId();
        
        List<Profile> profiles = [Select Id From Profile Where Name='Relationship'];
        
        User user = new User();
        user.Username = 'r.user@test.com'; 
        user.LastName = 'user'; 
        user.Email = 'r.user@test.com'; 
        user.Alias = 'tuser'; 
        user.TimeZoneSidKey = 'America/Los_Angeles'; 
        user.LocaleSidKey = 'en_US'; 
        user.EmailEncodingKey = 'UTF-8';
        user.ProfileId = profiles.get(0).id;
        user.LanguageLocaleKey = 'en_US';
        
        insert user;
        
        Account acc = new Account();
        acc.Name = 'Acc Test';
        acc.BillingCountry = 'Brazil';
        acc.BillingState = 'São Paulo';
        acc.RecordTypeId = accountRecordTypeId;
        acc.OwnerId = user.Id;
        acc.Business_Unit__c = 'SMB';
        insert acc;
        
        Contact c = new Contact();
        c.AccountId = acc.Id;
        c.Email = 'contact@test.com';
        c.LastName = 'Test';
        c.Cargo__c = 'CEO';
        insert c;
        
        Event e = new Event();
        e.DurationInMinutes = 120;
        e.WhatId = acc.Id;
        e.WhoId = c.Id;
        e.ActivityDateTime = datetime.now().addMonths(-1);
        insert e;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Opp Test'; 
        opp.StageName = 'Qualificação'; 
        opp.CloseDate = System.now().addDays(30).date();
        opp.RecordTypeId = opportunityRecordTypeId;
        opp.AccountId = acc.Id;
        opp.A_Authority_Opp_Val__c = 'No';
		opp.CH_Challenges_Opp_Val__c = 'No';
		opp.M_Money_Opp_Val__c = 'No';
		opp.P_Priority_Opp_Val__c = 'No';
        opp.Responsavel_Gympass_Relacionamento__c = user.Id;
        insert opp;
        
        opp.StageName = 'Oportunidade Validada Após 1ª Reunião';
        update opp;
        
        Copay_Plan__c membershipFee = new Copay_Plan__c();
        membershipFee.Name = 'Brasil Padrão';
        insert membershipFee;
        
        Quote q = new Quote();
        q.RecordTypeId = quoteRecordTypeId;
        q.Name = 'Proposal Test';
        q.OpportunityId = opp.Id;
        q.Expansion_100_of_the_eligible__c = true;
        q.Payment_method__c = 'Payroll';
        q.Employee_Corporate_Email__c = true;
        q.Employee_Corporate_Email__c = true;
        q.New_Hires__c = true;
        q.HR_Communication__c = true;
        q.Exclusivity_clause__c = true;
        q.PR_in_contract__c = true;
        q.What_does_success_looks_like_Joint_BP__c = true;
        q.Gym__c = membershipFee.Id;
        q.License_Fee_Waiver__c = 'No';
        insert q;
        
        q.Status = 'Sent Decision Maker';
        update q;
        
        Test.startTest();
        opp.StageName = 'Proposta Enviada';
        opp.SyncedQuoteId = q.Id;
        opp.Are_you_using_the_Standard_Pricing_plan__c = 'Yes';
        opp.Does_it_have_unique_technical_request__c = 'No';
        opp.Are_there_License_Fee_changes__c = 'No';
        update opp;
        
        q.Status = 'Decision Maker OK';
        update q;
        
        q.Status = 'Ganho';
        //update q;
        
        opp.StageName = 'Proposta Aprovada';
        opp.Country_Manager_Approval__c = true;
        opp.CloseDate = System.today().addDays(20);
        opp.Data_do_Lancamento__c = System.today().addDays(21);
        //update opp;
        
        opp.StageName = 'Contrato Assinado';
        //update opp;
        
        opp.StageName = 'Lançado/Ganho';
        //update opp;
       
        createSteps(acc, opp, 'Opp');
        
        StepsTowardsSuccessCtrl.getResponse(acc.Id);
        //callStepsTowardsSuccessCtrl(acc.Id);
        Test.stopTest();
    }
    
    @future
    public static void callStepsTowardsSuccessCtrl(Id accId) {
        StepsTowardsSuccessCtrl.getResponse(accId);
    }
    
    private static void createSteps(Account acc, Opportunity opp, String objType) {
		Map<Decimal, Step_Towards_Success_Definition__mdt> stepNumbeToStepTowardsSuccessDefinition = getMap12StepsTowardsSucessDefinition();
        List<Step_Towards_Success1__c> stepsToInsert = new List<Step_Towards_Success1__c>();
        
        for (Integer i=1; i<=12; i++) {
       		Step_Towards_Success1__c step = new Step_Towards_Success1__c();
            step.Step_Number__c = i;
            step.Name = stepNumbeToStepTowardsSuccessDefinition.get(i).MasterLabel;
            if (objType == 'Acc') {
            	step.Related_Account__c = acc.Id;
                step.Master_Account__c = acc.Id;
            } else {
                step.Related_Opportunity__c = opp.Id;
                step.Master_Account__c = opp.AccountId;
            }
            
            stepsToInsert.add(step);
        }
        
        insert stepsToInsert;
   	}    
        
    private static Map<Decimal, Step_Towards_Success_Definition__mdt> getMap12StepsTowardsSucessDefinition() {
		Map<Decimal, Step_Towards_Success_Definition__mdt> stepNumbeToStepTowardsSuccessDefinition = new Map<Decimal, Step_Towards_Success_Definition__mdt>(); 
        
        List<Step_Towards_Success_Definition__mdt> listStepsTowardsSuccessDefinitions = [
            SELECT DeveloperName , MasterLabel, Category__c, Description__c, Field_API_Name__c, Field_Expected_Value__c, How_are_we_going_to_track_it__c, Object_API_Name__c, Step__c
            FROM Step_Towards_Success_Definition__mdt
            ORDER BY Step__c ASC
        ];
        
        for (Step_Towards_Success_Definition__mdt step : listStepsTowardsSuccessDefinitions) {
            stepNumbeToStepTowardsSuccessDefinition.put((Integer)step.Step__c, step);
        }
        
        return stepNumbeToStepTowardsSuccessDefinition;
    }
}