/**
 * @description       : 
 * @author            : marcus.silva@gft.com
 * @group             : 
 * @last modified on  : 11-12-2021
 * @last modified by  : roei@gft.com
 * Modifications Log 
 * Ver   Date         Author         			Modification
 * 1.0   20-05-2021   marcus.silva@gft.com   Initial Version
**/
@isTest
public class OpportunityTriggerRenegotiationTest {    
    @TestSetup
    static void createData(){
        Pricebook2 lStandardPricebook = DataFactory.newPricebook();
        Database.update( lStandardPricebook );
        
        Account lAcc = DataFactory.newAccount();
        Database.insert(lAcc);
        
        Opportunity OldNewBusinessOpp = DataFactory.newOpportunity( lAcc.Id, lStandardPricebook, 'Client_Sales_New_Business',true );  
        
        OldNewBusinessOpp.fasttrackstage__c='Qualification';                
        Database.insert(OldNewBusinessOpp);

        system.debug('Opp after insert: ' + OldNewBusinessOpp);
        
        Quote oldQuote = DataFactory.newQuote(OldNewBusinessOpp, '1', 'Gympass_Plus');
        oldQuote.Employee_Corporate_Email__c =false;
        oldQuote.end_date__c = Date.today();
        oldQuote.contact_permission__c = 'Sign-up only';
        oldQuote.ExpirationDate = date.valueOf('2021-08-16');
        
        oldQuote.autonomous_marketplace_contract__c = 'Yes';
        oldQuote.Discount_Approval_Level__c = 4;
        oldQuote.Free_Trial_Days__c = 7;                
        Database.insert(oldQuote);
        
        OldNewBusinessOpp.Name = 'New Business';
        OldNewBusinessOpp.SyncedQuoteId = oldQuote.Id;
        OldNewBusinessOpp.StageName = 'Lançado/Ganho';
        OldNewBusinessOpp.FastTrackStage__c = 'Offer Approved';                
        Database.update(OldNewBusinessOpp);                                
        
        system.debug('Opp after update: ' + OldNewBusinessOpp);
        Product2 accessFeeProduct = DataFactory.newProduct('Enterprise Subscription', false, 'BRL');
        Product2 setupFeeProduct = DataFactory.newProduct('Setup Fee', false, 'BRL');
        Product2 professionalServicesMaintenanceFeeProduct = DataFactory.newProduct('Professional Services Maintenance Fee', false, 'BRL');
        Product2 professionalServicesSetupFeeProduct = DataFactory.newProduct('Professional Services Setup Fee', false, 'BRL');                
        Database.insert(new List<Product2>{accessFeeProduct, setupFeeProduct, professionalServicesMaintenanceFeeProduct, professionalServicesSetupFeeProduct});                
        
        PricebookEntry accessFeeEntry = DataFactory.newPricebookEntry(lStandardPricebook, accessFeeProduct, OldNewBusinessOpp);
        PricebookEntry setupFeeEntry = DataFactory.newPricebookEntry(lStandardPricebook, setupFeeProduct, OldNewBusinessOpp);
        PricebookEntry professionalServicesMaintenanceFeeEntry = DataFactory.newPricebookEntry(lStandardPricebook, professionalServicesMaintenanceFeeProduct, OldNewBusinessOpp);
        PricebookEntry professionalServicesSetupFeeEntry = DataFactory.newPricebookEntry(lStandardPricebook, professionalServicesSetupFeeProduct, OldNewBusinessOpp);                
        Database.insert(new List<PricebookEntry>{accessFeeEntry, setupFeeEntry, professionalServicesMaintenanceFeeEntry, professionalServicesSetupFeeEntry});
                        
        QuoteLineItem oldAccessFee = DataFactory.newQuoteLineItem(oldQuote, accessFeeEntry, 100);
        QuoteLineItem oldSetupFee = DataFactory.newQuoteLineItem(oldQuote, setupFeeEntry, 200);
        QuoteLineItem oldProfessionalServicesMaintenanceFee = DataFactory.newQuoteLineItem(oldQuote, professionalServicesMaintenanceFeeEntry, 300);
        QuoteLineItem ProfessionalServicesSetupFee = DataFactory.newQuoteLineItem(oldQuote, professionalServicesSetupFeeEntry, 400);

        oldAccessFee.Fee_Contract_Type__c = 'Flat Fee';                
        oldSetupFee.Fee_Contract_Type__c = 'Copay 2	';                
        oldProfessionalServicesMaintenanceFee.Fee_Contract_Type__c = 'Flat Fee';                
        ProfessionalServicesSetupFee.Fee_Contract_Type__c = 'Copay 2';
        

        oldAccessFee.Description = 'Enterprise Subscription';
        oldSetupFee.Description = 'Setup Fee';
        oldProfessionalServicesMaintenanceFee.Description = 'Professional Services Maintenance Fee';
        ProfessionalServicesSetupFee.Description = 'Professional Services Setup Fee';                   
        Database.insert(new List<QuoteLineItem>{oldAccessFee, oldSetupFee, oldProfessionalServicesMaintenanceFee, ProfessionalServicesSetupFee} );        
        
        Payment__c payForoldAccessFee = DataFactory.newPayment(oldAccessFee);
        Payment__c payForoldSetupFee = DataFactory.newPayment(oldSetupFee);
        Payment__c payForoldProfessionalServicesMaintenanceFee = DataFactory.newPayment(oldProfessionalServicesMaintenanceFee);
        Payment__c payForoldProfessionalServicesSetupFee = DataFactory.newPayment(ProfessionalServicesSetupFee);

        payForoldAccessFee.Frequency__c = 'Monthly';
        payForoldSetupFee.Frequency__c = 'Yearly';
        payForoldProfessionalServicesMaintenanceFee.Frequency__c = 'Quarterly';
        payForoldProfessionalServicesSetupFee.Frequency__c = 'Biannually';                
        Database.insert(new List<Payment__c>{payForoldAccessFee, payForoldSetupFee, payForoldProfessionalServicesMaintenanceFee, payForoldProfessionalServicesSetupFee});  
        
        Eligibility__c eliAccessFee = DataFactory.newEligibility(payForoldAccessFee, true);
        Eligibility__c eliSetupFee = DataFactory.newEligibility(payForoldSetupFee, true);
        Eligibility__c eliProfessionalServicesMaintenanceFee = DataFactory.newEligibility(payForoldProfessionalServicesMaintenanceFee, true);
        Eligibility__c eliProfessionalServicesSetupFee = DataFactory.newEligibility(payForoldProfessionalServicesSetupFee, true);

        eliAccessFee.Payment_Method__c = 'Credit Card';
        eliSetupFee.Payment_Method__c = 'Payroll';
        eliProfessionalServicesMaintenanceFee.Payment_Method__c = 'Payroll + Credit Card';
        eliProfessionalServicesSetupFee.Payment_Method__c = 'Credit Card';                
        Database.insert(new List<Eligibility__c>{eliAccessFee, eliSetupFee, eliProfessionalServicesMaintenanceFee, eliProfessionalServicesSetupFee});                
    }
            
    @isTest
    public static void createOpportunity() {
        Test.startTest();
        
        List<Opportunity> lOpp = [ SELECT Id, Name, CreatedDate, AccountId, SyncedQuote.ExpirationDate, Achieved_Steps_Towards_Success_Quantity__c  FROM Opportunity ];
        Pricebook2 pricebook = [SELECT Id from Pricebook2 Limit 1];
        Opportunity lRenegociationOpp = DataFactory.newOpportunity( lOpp[0].AccountId, pricebook, 'Client_Success_Renegotiation',true );  
        Database.insert(lRenegociationOpp);
        system.debug('Last opp Achieved_Steps_Towards_Success_Quantity__c: ' + lOpp[0].Achieved_Steps_Towards_Success_Quantity__c);
        system.debug('lRenegociationOpp' + lRenegociationOpp);
        
        List<Opportunity> listOppToUpdate = new List<Opportunity>();
        List<Opportunity> listOppToRenegociation = new List<Opportunity>();
        Id quoteToCreateCase;                
        
        Case lOperationalCase = DataFactory.newCase(lOpp[0].id, quoteToCreateCase, 'Deal_Desk_Operational');
        Database.insert( lOperationalCase ); 
        Database.update(listOppToUpdate);                
        
        
        List<Assert_Data__c> listContractData = [SELECT Id, Opportunity__c, Object_Name__c, Field_Name__c, Old_Value__c, New_Value__c, Fee_Type__c, Approved_Value__c
                                                    FROM Assert_Data__c WHERE Opportunity__c =: lRenegociationOpp.id  ];
        system.debug('listContractData.size(): ' + listContractData.size());
        
        Test.stopTest();
        System.debug('Old opp: ' + lOpp[0]);
        System.assert(listContractData.size() > 0);
        
        integer countAllrecordsVerified = 0;
        
        for( Assert_Data__c contractData : listContractData){            
            switch on contractData.Object_Name__c{
                when 'opportunity' {
                    if (contractData.Field_Name__c == 'achieved_steps_towards_success_quantity__c'){                                
                        System.debug('achieved_steps_towards_success_quantity__c: ' + contractData.Old_Value__c);
                        //System.assertEquals(contractData.Old_Value__c, string.valueof(10));
                        countAllrecordsVerified ++;
                    }
                }
                when 'quote' {
                    for(Opportunity opp : lOpp){
                        if (contractData.Field_Name__c == 'orderExpirationDays'){
                            System.assertEquals(contractData.Old_Value__c, opp.SyncedQuote.ExpirationDate==null?String.valueOf(90):String.valueOf(opp.CreatedDate.date().daysBetween(opp.SyncedQuote.ExpirationDate)) );                        
                            countAllrecordsVerified ++;
                        }
                    }                            
                    if (contractData.Field_Name__c == 'discount'){                                
                        System.assertEquals(contractData.Old_Value__c, string.valueof(0.00));                        
                        countAllrecordsVerified ++;
                    }
                    if (contractData.Field_Name__c == 'autonomous_marketplace_contract__c'){
                        System.assertEquals(contractData.Old_Value__c, 'Yes');                        
                        countAllrecordsVerified ++;
                    }
                    if (contractData.Field_Name__c == 'discount_approval_level__c'){
                        System.assertEquals(contractData.Approved_Value__c, string.valueof(4));                        
                        countAllrecordsVerified ++;
                    }
                    if (contractData.Field_Name__c == 'free_trial_days__c'){
                        System.assertEquals(contractData.Old_Value__c, string.valueof(7));                        
                        countAllrecordsVerified ++;
                    }
                }
                when 'quotelineitem' {
                    if (contractData.Field_Name__c == 'unitprice' && contractData.Fee_Type__c == 'Enterprise Subscription'){
                        System.assertEquals(contractData.Old_Value__c, string.valueof(100.00));                        
                        countAllrecordsVerified ++;
                    }
                    if (contractData.Field_Name__c == 'unitprice' && contractData.Fee_Type__c == 'Setup Fee'){
                        System.assertEquals(contractData.Old_Value__c, string.valueof(200.00));                        
                        countAllrecordsVerified ++;
                    }
                    if (contractData.Field_Name__c == 'unitprice' && contractData.Fee_Type__c == 'Professional Services Maintenance Fee'){
                        System.assertEquals(contractData.Old_Value__c, string.valueof(300.00));                        
                        countAllrecordsVerified ++;
                    }
                    if (contractData.Field_Name__c == 'unitprice' && contractData.Fee_Type__c == 'Professional Services Setup Fee'){
                        System.assertEquals(contractData.Old_Value__c, string.valueof(400.00));                        
                        countAllrecordsVerified ++;
                    }
                    if (contractData.Field_Name__c == 'fee_contract_type__c' && contractData.Fee_Type__c == 'Enterprise Subscription'){
                        System.assertEquals(contractData.Old_Value__c, 'Flat Fee');                        
                        countAllrecordsVerified ++;
                    }
                    if (contractData.Field_Name__c == 'fee_contract_type__c' && contractData.Fee_Type__c == 'Setup Fee'){
                        System.assertEquals(contractData.Old_Value__c, 'Copay 2');                        
                        countAllrecordsVerified ++;
                    }
                    if (contractData.Field_Name__c == 'fee_contract_type__c' && contractData.Fee_Type__c == 'Professional Services Maintenance Fee'){
                        System.assertEquals(contractData.Old_Value__c, 'Flat Fee');                        
                        countAllrecordsVerified ++;
                    }
                    if (contractData.Field_Name__c == 'fee_contract_type__c' && contractData.Fee_Type__c == 'Professional Services Setup Fee'){
                        System.assertEquals(contractData.Old_Value__c, 'Copay 2');                        
                        countAllrecordsVerified ++;
                    }
                }
                when 'payment__c' {
                    if (contractData.Field_Name__c == 'frequency__c' && contractData.Fee_Type__c == 'Enterprise Subscription'){
                        System.assertEquals(contractData.Old_Value__c, 'Monthly');
                        countAllrecordsVerified ++;
                    }
                    if (contractData.Field_Name__c == 'frequency__c' && contractData.Fee_Type__c == 'Setup Fee'){
                        System.assertEquals(contractData.Old_Value__c, 'Yearly');
                        countAllrecordsVerified ++;
                    }
                    if (contractData.Field_Name__c == 'frequency__c' && contractData.Fee_Type__c =='Professional Services Maintenance Fee'){
                        System.assertEquals(contractData.Old_Value__c, 'Quarterly');
                        countAllrecordsVerified ++;
                    }
                    if (contractData.Field_Name__c == 'frequency__c' && contractData.Fee_Type__c == 'Professional Services Setup Fee'){
                        System.assertEquals(contractData.Old_Value__c, 'Biannually');
                        countAllrecordsVerified ++;
                    }
                }
                when 'eligibility__c' {
                    if (contractData.Field_Name__c == 'payment_method__c' && contractData.Fee_Type__c == 'Enterprise Subscription'){
                        System.assertEquals(contractData.Old_Value__c, 'Credit Card');                       
                        countAllrecordsVerified ++;
                    }
                    if (contractData.Field_Name__c == 'payment_method__c' && contractData.Fee_Type__c == 'Setup Fee'){
                        System.assertEquals(contractData.Old_Value__c, 'Payroll');                       
                        countAllrecordsVerified ++;
                        }
                        if (contractData.Field_Name__c == 'payment_method__c' && contractData.Fee_Type__c == 'Professional Services Maintenance Fee'){
                        System.assertEquals(contractData.Old_Value__c, 'Payroll + Credit Card');                       
                        countAllrecordsVerified ++;
                        }
                        if (contractData.Field_Name__c == 'payment_method__c' && contractData.Fee_Type__c == 'Professional Services Setup Fee'){
                        System.assertEquals(contractData.Old_Value__c, 'Credit Card');                       
                        countAllrecordsVerified ++;
                        }
                }
            }
            
        }                                
        //System.assert(countAllrecordsVerified == 21);
    }
}