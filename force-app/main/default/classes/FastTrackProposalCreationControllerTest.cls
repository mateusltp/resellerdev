/**
* @author vinicius.ferraz
* @description Provide unit test for fast track proposal controller 
*/
@isTest(SeeAllData=false)
public with sharing class FastTrackProposalCreationControllerTest {
    
    @isTest
    static void getPaymentTest(){
        
        List<Opportunity> lOpp = [ SELECT Id , AccountId FROM Opportunity ];    
        String payment = '';
        Test.startTest();
        try{
            FastTrackProposalCreationController.getPayment(lOpp[0].id);
        }catch(System.QueryException e){
            System.assert(false);
        }
        System.assertEquals(payment != null, true);
        Test.stopTest();
    }
    
    @isTest
    static void resellerCreateProposalTest(){
        
        List<Opportunity> lOpp = [ SELECT Id , AccountId FROM Opportunity ];    
        system.debug('mateus lOpp '+lOpp);
        Test.startTest();
        try{
            FastTrackProposalCreationController.resellerCreateProposal(lOpp[0].id);
        }catch(System.QueryException e){
            System.assert(true);
        }
        Test.stopTest();
    }
   
    @isTest
    static void findComissionValueTest(){
        
        Product2 product = [ SELECT Id FROM Product2 WHERE Name = 'Setup Fee' LIMIT 1 ];
        String currencyIsoCode = 'BRL';
        List<Comission_Price__c> listComissionPrice = new List<Comission_Price__c>();
        
        Test.startTest();
        try{
            listComissionPrice = FastTrackProposalCreationController.findComissionValue(product.id, currencyIsoCode);
        }catch(System.QueryException e){
            System.assert(true);
        }
        System.assertEquals(listComissionPrice.size() > 0, false);
        Test.stopTest();
    }
    
    @isTest
    static void setModalTest(){
        
        List<Opportunity> lOpp = [ SELECT Id , AccountId FROM Opportunity ];    
        Quote quote = [SELECT Id,OpportunityId,Quote.Pricebook2Id,CurrencyIsoCode FROM Quote];
        QuoteLineItem accessFee = [SELECT Id,UnitPrice,ListPrice,QuoteLineItem.Product2Id FROM QuoteLineItem LIMIT 1];
        Payment__c payment = [SELECT Id FROM Payment__c LIMIT 1];
        
        String quoteJson = JSON.serialize(quote);
        String accessFeeJson = JSON.serialize(accessFee);
        String paymentJson = JSON.serialize(payment);
        
        Test.startTest();
        try{
            FastTrackProposalCreationController.setModal(accessFeeJson, paymentJson, quoteJson);
        }catch(System.QueryException e){
            System.assert(true);
        }
        Test.stopTest();
    }
  
    @isTest
    static void getAccOppSubsidyWithResellerTest(){
        
        List<Opportunity> lOpp = [ SELECT Id , AccountId FROM Opportunity ];    
        List<Account_Opportunity_Relationship__c> listAccountOpportunityRelationship = new List<Account_Opportunity_Relationship__c>();
        
        Test.startTest();
        try{
            listAccountOpportunityRelationship = FastTrackProposalCreationController.getAccOppSubsidyWithReseller(lOpp[0].id);
        }catch(System.QueryException e){
            System.assert(false);
        }
        System.assertEquals(listAccountOpportunityRelationship.size() > 0, true);
        Test.stopTest();
    }
    
    @isTest
    static void getAccOppSubsidyTest(){
        
        List<Opportunity> lOpp = [ SELECT Id , AccountId FROM Opportunity ];    
        List<Account_Opportunity_Relationship__c> listAccountOpportunityRelationship = new List<Account_Opportunity_Relationship__c>();
        
        Test.startTest();
        try{
            listAccountOpportunityRelationship = FastTrackProposalCreationController.getAccOppSubsidy(lOpp[0].id);
        }catch(System.QueryException e){
            System.assert(false);
        }
        System.assertEquals(listAccountOpportunityRelationship.size() > 0, true);
        Test.stopTest();
    }
    
    @isTest
    static void removeAccountRelationshipFromOpportunityTest(){
        
        List<Opportunity> lOpp = [ SELECT Id , AccountId FROM Opportunity ];       
        Account_Opportunity_Relationship__c lAccountOpportunityRelationship = new Account_Opportunity_Relationship__c(
            Opportunity__c = lOpp[0].Id,
            Account__c = lOpp[0].AccountId,
            RecordTypeId = Schema.SObjectType.Account_Opportunity_Relationship__c.getRecordTypeInfosByDeveloperName().get('Client_Sales').getRecordTypeId(),
            Main_Order__c = true,
            Billing_Percentage__c = 0
        );
        Database.insert( lAccountOpportunityRelationship );
        List<Account_Opportunity_Relationship__c> listAccountOpportunityRelationship = [SELECT Id,Opportunity__c from Account_Opportunity_Relationship__c];
        
        Test.startTest();
        try{
            FastTrackProposalCreationController.removeAccountRelationshipFromOpportunity(lOpp[0].id, listAccountOpportunityRelationship);
        }catch(System.QueryException e){
            System.assert(false);
        }
        System.assertEquals(lOpp[0].Id, listAccountOpportunityRelationship[0].Opportunity__c);
        Test.stopTest();
    }
    
    @isTest
    static void getStageCmpMappingMetadataTest(){
        List<Opportunity> lOpp = [ SELECT Id , AccountId FROM Opportunity ];
        
        FastTrackStageCmpMapping__mdt[] lLstFastTrackStageMapping;
        
        Test.startTest();
        try{
            lLstFastTrackStageMapping = FastTrackProposalCreationController.getStageCmpMappingMetadata(lOpp[0].id);
        }catch(System.QueryException e){
            System.assert(false);
        }
        System.assertEquals(lLstFastTrackStageMapping.size() > 0, true);
        Test.stopTest();
    }
    
    @isTest
    static void shouldBuildToObject(){
        Opportunity lOpp = [ SELECT Id , AccountId FROM Opportunity ];
        
        Test.startTest();
        FastTrackProposalCreationTO lProposalTo = FastTrackProposalCreationController.find( lOpp.Id , 1 , new List<Id>{ lOpp.AccountId } );
        Test.stopTest();
        
        System.assertEquals( lProposalTo.opportunityId , lOpp.Id ,
                            'Should have returned TO instance' );
    }
    
    @isTest
    static void shouldFindGympassEntities(){      
        Test.startTest();          
        List< Account > lLstGympassEntities = FastTrackProposalCreationController.findGympassEntities();
        Test.stopTest();
        
        System.assertEquals( lLstGympassEntities.size() , 1 ,
                            'Should have find a Gympass Entity' );
    }
    
    @isTest
    static void shouldReturnAccContacts(){ 
        Opportunity lOpp = [ SELECT Id FROM Opportunity ];
        
        Test.startTest();
        List< Contact > lLstContacts = FastTrackProposalCreationController.findClientManagers( lOpp.Id );  
        Test.stopTest();
        
        System.assertEquals( lLstContacts.size() , 1 ,
                            'Contacts should have returned');
    } 
  /*  
    @isTest
    static void shouldSaveProposalInfo(){
        Opportunity lOpp = [ SELECT Id , AccountId FROM Opportunity ];
        
        FastTrackProposalCreationTO lProposalTo = 
            FastTrackProposalCreationController.find( lOpp.Id , 1 , new List< Id >{ lOpp.AccountId } );
        
        Test.startTest();
        FastTrackProposalCreationTO lProposalToResult = 
            FastTrackProposalCreationController.saveProposal( JSON.serialize( lProposalTo ) );  
        Test.stopTest();
        
        System.assert( [ SELECT Id FROM Quote WHERE OpportunityId =: lOpp.Id LIMIT 1 ].Id != null ,
                      'Quote should be created');
    }*/
 
    @isTest
    static void shouldCreateAccOppRelantionship(){
        Opportunity lOpp = [ SELECT Id , AccountId FROM Opportunity ];
        List< Account > lLstAcc = [ SELECT Id FROM Account WHERE Recordtype.DeveloperName = 'Empresas' ];
        
        Test.startTest();
        FastTrackProposalCreationController.saveAccountWithOpportunity( lOpp.Id , lLstAcc );  
        Test.stopTest();
        
        System.assertEquals( 2 , [ SELECT Id FROM Account_Opportunity_Relationship__c ].size() ,
                            'Account Opportunity Relationship should have been created' );
    }
    
    @isTest
    static void shouldRemoveAccOppRelantionship(){
        Opportunity lOpp = [ SELECT Id , AccountId FROM Opportunity ];
        List< Account > lLstAcc = [ SELECT Id FROM Account WHERE Recordtype.DeveloperName = 'Empresas' ];
        
        FastTrackProposalCreationController.saveAccountWithOpportunity( lOpp.Id , lLstAcc );
        
        Test.startTest();
        FastTrackProposalCreationController.removeAccountFromOpportunity( lOpp.Id , lLstAcc );
        Test.stopTest();
        
        System.assertEquals( 0 , [ SELECT Id FROM Account_Opportunity_Relationship__c ].size() ,
                            'Account Opportunity Relationship should have been deleted' );
    }
    
    @isTest
    static void shouldSetUpDealHierarchy(){
        Opportunity lOpp = [ SELECT Id , AccountId FROM Opportunity ];
        List< Account > lLstAcc = [ SELECT Id FROM Account WHERE Recordtype.DeveloperName = 'Empresas' ];
        
        FastTrackProposalCreationController.saveAccountWithOpportunity( lOpp.Id , lLstAcc );
        
        System.debug( [ SELECT Id FROM Account_Opportunity_Relationship__c ].size() );
        
        Test.startTest();
        Map< String , Object > lMap = (Map< String , Object >) FastTrackProposalCreationController.getSetUpDealHierarchy( lOpp.Id );
        Test.stopTest();
        
        System.assertEquals( 2000 , (Integer)lMap.get('numberOfEmployees') ,
                            'Accounts in the hierarchy not set' );
    }
    
    @isTest
    static void shouldSetOppFastTrackStage(){
        Opportunity lOpp = [ SELECT Id , AccountId , Name , CurrencyIsoCode , FastTrackStage__c  FROM Opportunity ];
        
        Quote lProposal = DataFactory.newQuote( lOpp , 'Quote Test', 'Client_Sales_New_Business' );
        Database.insert( lProposal );
        
        FastTrackProposalCreationTO lProposalTo = 
            FastTrackProposalCreationController.find( lOpp.Id , 1 , new List< Id >{ lOpp.AccountId } );
        
        Test.startTest();
        FastTrackProposalCreationTO lReturnedProposalTo =
            FastTrackProposalCreationController.setStage( JSON.serialize( lProposalTo ) , 4 );
        Test.stopTest();
        
        System.assert( lOpp.FastTrackStage__c != lReturnedProposalTo.fastTrackStage , 
                      'Did not set new Opportunity fast track stage' );
    }
    
    @isTest
    static void shouldApproveDealDeskCase(){
        Opportunity lOpp = [ SELECT Id , Name , AccountId , CurrencyIsoCode  FROM Opportunity ];
        
        Quote lProposal = DataFactory.newQuote( lOpp , 'Quote Test', 'Client_Sales_New_Business' );
        Database.insert( lProposal );
        
        Case lDealDeskCase = DataFactory.newCase( lOpp , lProposal , 'Deal_Desk_Approval' );
        lDealDeskCase.Deal_Desk_Evaluation__c = 'Approved';
        Database.insert( lDealDeskCase );
        
        Test.startTest();
        FastTrackProposalCreationController.approveOrRejectDealDesk( lOpp.Id , lDealDeskCase.Id , true, 'teste');
        Test.stopTest();
        
        lDealDeskCase = [ SELECT Status FROM Case WHERE Id =: lDealDeskCase.Id ];
        
        System.assertEquals( 'New' , lDealDeskCase.Status ,'Case was not approved' );
    }
 
    @isTest
    static void shouldEvaluateProposal(){
        Opportunity lOpp = [ SELECT Id , Name , AccountId , CurrencyIsoCode  FROM Opportunity ];
        
        Quote lProposal = DataFactory.newQuote( lOpp , 'Quote Test', 'Client_Sales_New_Business' );
        Database.insert( lProposal );
        
        Test.startTest();
        FastTrackProposalCreationTO lReturnedProposalTo =
            FastTrackProposalCreationController.proposalEvaluation( lOpp.Id );
        Test.stopTest();
        
        System.assert( lReturnedProposalTo.proposal.dealDeskApprovalNeeded != null ,
                      'Did not evaluate proposal');
    }
    
    @isTest
    static void shouldSubmitProposalAndCaseToApproval(){
        Opportunity lOpp = [ SELECT Id , Name , AccountId , CurrencyIsoCode FROM Opportunity ];
        
        Quote lProposal = DataFactory.newQuote( lOpp , 'Quote Test', 'Client_Sales_New_Business' );
        lProposal.Deal_Desk_Approval_Needed__c = true;
        lProposal.Deal_Desk_Approved__c = false;
        Database.insert( lProposal );
        
        Case lDealDeskCase = DataFactory.newCase( lOpp , lProposal , 'Deal_Desk_Approval' );
        lDealDeskCase.Deal_Desk_Evaluation__c = 'Approved';
        Database.insert( lDealDeskCase );
        
        Test.startTest();
        FastTrackProposalCreationTO lProposalTo = 
        FastTrackProposalCreationController.find( lOpp.Id , 1 , new List< Id >{ lOpp.AccountId } );
         
        FastTrackProposalCreationTO lReturnedProposalTo =
        FastTrackProposalCreationController.submitForApproval( JSON.serialize( lProposalTo ) );
        Test.stopTest();
    }
/*    
    @isTest
    static void shouldSubmitProposalAndCaseToApprovalSmb(){
        Account lAcc = DataFactory.newAccount();
        lAcc.NumberOfEmployees = 500;
        Database.insert( lAcc );
        
        Pricebook2 lSmbPricebook = new Pricebook2(
            Name = 'SMB Price Book',
            IsActive = true
        );
        Database.insert( lSmbPricebook );
        
        Opportunity lOpp = DataFactory.newOpportunity( lAcc.Id , lSmbPricebook , 'SMB_New_Business' );
        lOpp.Quantity_Offer_Number_of_Employees__c = 500;
        Database.insert( lOpp );  
        
        Product2 lAccessFee = [ SELECT Id FROM Product2 WHERE Name = 'Enterprise Subscription' LIMIT 1 ];
        
        PricebookEntry lAccessFeeEntry = DataFactory.newPricebookEntry( lSmbPricebook , lAccessFee , lOpp );
        Database.insert( lAccessFeeEntry );
        
        Quote lProposal = DataFactory.newQuote( lOpp , 'Quote Test', 'Client_Sales_New_Business' );
        lProposal.Deal_Desk_Approval_Needed__c = true;
        lProposal.Deal_Desk_Approved__c = false;
        Database.insert( lProposal );
        
        Case lDealDeskCase = DataFactory.newCase( lOpp , lProposal , 'Deal_Desk_Approval' );
        lDealDeskCase.Deal_Desk_Evaluation__c = 'Approved';
        Database.insert( lDealDeskCase );
        
        Test.startTest();
        FastTrackProposalCreationTO lProposalTo = 
            FastTrackProposalCreationController.find( lOpp.Id , 1 , new List< Id >{ lOpp.AccountId } );
        
        FastTrackProposalCreationTO lReturnedProposalTo =
            FastTrackProposalCreationController.submitForApproval( JSON.serialize( lProposalTo ) );
        Test.stopTest();
    }
*/    
    @isTest
    static void shouldResetCommercialConditions(){
        Opportunity lOpp = [ SELECT Id , Name , AccountId , CurrencyIsoCode  FROM Opportunity ];
        
        Quote lProposal = DataFactory.newQuote( lOpp , 'Quote Test', 'Client_Sales_New_Business' );
        Database.insert( lProposal );
        
        PricebookEntry lAccessFeeEntry = [ SELECT Id, Product2Id, UnitPrice FROM PricebookEntry 
                                          WHERE Product2.Family = 'Enterprise Subscription' AND Product2.Family_Member_Included__c = false ];
        
        QuoteLineItem lQuoteLineItem = DataFactory.newQuoteLineItem( lProposal , lAccessFeeEntry );
        Database.insert( lQuoteLineItem );
        
        Payment__c lPayForLineItem = DataFactory.newPayment( lQuoteLineItem );
        Database.insert( lPayForLineItem );
       	
        Test.startTest();
        Case lDealDeskCase = DataFactory.newCase( lOpp , lProposal , 'Deal_Desk_Approval' );
        lDealDeskCase.Deal_Desk_Evaluation__c = 'Approved';
        Database.insert( lDealDeskCase );
        
        
        FastTrackProposalCreationTO lProposalTo = 
            FastTrackProposalCreationController.find( lOpp.Id , 1 , new List< Id >{ lOpp.AccountId } );
        
        FastTrackProposalCreationController.resetCommercialConditions( lProposalTo );
        Test.stopTest();
    }
    
    @isTest
    static void shouldGetAccOppRelationship(){
        Opportunity lOpp = [ SELECT Id , AccountId FROM Opportunity ];
        List< Account > lLstAcc = [ SELECT Id FROM Account WHERE Recordtype.DeveloperName = 'Empresas' ];
        
        FastTrackProposalCreationController.saveAccountWithOpportunity( lOpp.Id , lLstAcc ); 
        
        Test.startTest();
        List< Account_Opportunity_Relationship__c > lLstAccOppRelantionship =
            FastTrackProposalCreationController.getAccOppRelationships( lOpp.Id );
        Test.stopTest();
        
        System.assert( lLstAccOppRelantionship.size() > 0 ,
                      'Could get Account Opportunity relantionship' );
    }
    
    @isTest
    static void shouldSaveAccOppRelationship(){
        Opportunity lOpp = [ SELECT Id , AccountId FROM Opportunity ];
        List< Account > lLstAcc = [ SELECT Id FROM Account WHERE Recordtype.DeveloperName = 'Empresas' ];
        
        Account_Opportunity_Relationship__c lAccountOpportunityRelationship = new Account_Opportunity_Relationship__c(
            Opportunity__c = lOpp.Id,
            Account__c = lLstAcc[0].Id,
            RecordTypeId = Schema.SObjectType.Account_Opportunity_Relationship__c.getRecordTypeInfosByDeveloperName().get('Client_Sales').getRecordTypeId(),
            Main_Order__c = true,
            Billing_Percentage__c = 0
        );
        Database.insert( lAccountOpportunityRelationship );
        
        lAccountOpportunityRelationship.Billing_Percentage__c = 100;
        
        Test.startTest();
        FastTrackProposalCreationController.saveAccOppRelationships( new List < Account_Opportunity_Relationship__c >{ 
            lAccountOpportunityRelationship
                } , JSON.serialize( lLstAcc ) );
        Test.stopTest();
        
        lAccountOpportunityRelationship = [ SELECT Billing_Percentage__c FROM Account_Opportunity_Relationship__c 
                                           WHERE Id =: lAccountOpportunityRelationship.Id ];
        
        System.assertEquals( 100 , lAccountOpportunityRelationship.Billing_Percentage__c ,
                            'Account Opportunity Relationship did not save' );
    }
    
    @isTest
    static void shouldGetDealDeskOperationalCase(){
        Opportunity lOpp = [ SELECT Id FROM Opportunity ];
        
        Test.startTest();
        Case lOperationalCase = FastTrackProposalCreationController.getDealDeskOperationalCase( lOpp.Id );
        Test.stopTest();
        
        System.assertNotEquals( null , lOperationalCase.Subject ,
                               'Didnt populate Subject' );
        System.assertNotEquals( null , lOperationalCase.OwnerId ,
                               'Didnt find Deal Desk Operational Queue' );
    }
    
    @isTest
    static void shouldRemoveQuoteItems(){
        Opportunity lOpp = [ SELECT Id , Name , AccountId , CurrencyIsoCode FROM Opportunity ];
        
        Quote lProposal = DataFactory.newQuote( lOpp , 'Quote Test', 'Client_Sales_New_Business' );
        Database.insert( lProposal );
        
        PricebookEntry lAccessFeeEntry = [ SELECT Id, Product2Id, UnitPrice FROM PricebookEntry 
                                          WHERE Product2.Family = 'Enterprise Subscription' AND Product2.Family_Member_Included__c = false ];
        
        QuoteLineItem lQuoteLineItem = DataFactory.newQuoteLineItem( lProposal , lAccessFeeEntry);
        Database.insert( lQuoteLineItem );
        
        Payment__c lPayForLineItem = DataFactory.newPayment( lQuoteLineItem );
        Database.insert( lPayForLineItem );
        
        FastTrackProposalCreationTO lProposalTo = new FastTrackProposalCreationTO();
        lProposalTo.opportunityId = lOpp.Id;
        lProposalTo.quoteId = lOpp.Id;
        
        Test.startTest();
        FastTrackProposalCreationController.removeItems( JSON.serialize( lProposalTo ) );
        Test.stopTest();
        
        System.assertEquals( 0 , [ SELECT Id FROM QuoteLineItem WHERE Quote.OpportunityId =: lOpp.Id ].size() ,
                            'Quote line items havent been removed' );
    }
      
    @TestSetup
    static void createData(){    
        Account lParentAcc = DataFactory.newAccount();
        lParentAcc.NumberOfEmployees = 1000;
        Database.insert( lParentAcc );
        
        Account lAcc = DataFactory.newAccount();
        lAcc.ParentId = lParentAcc.Id;
        lAcc.NumberOfEmployees = 1000;
        Database.insert( lAcc );
        
        Contact lNewContact = DataFactory.newContact( lParentAcc , 'Test Contact' );
        Database.insert( lNewContact );
        
        Pricebook2 lStandardPb = DataFactory.newPricebook();
        Database.update( lStandardPb );
        
        Account accReseller = DataFactory.newAccountReseller();
        Database.insert(accReseller);
        
        Opportunity lOpp = new Opportunity(CurrencyIsoCode='BRL');
        lOpp.Reseller_del__c = accReseller.id;
        
        Product2 lAcessFee = DataFactory.newProduct( 'Enterprise Subscription' , false , 'BRL' );
        Database.insert( lAcessFee );
        
        Product2 lAccessFeeFamilyMember = DataFactory.newProduct( 'Enterprise Subscription' , true , 'BRL' );
        Database.insert( lAccessFeeFamilyMember );
        
        PricebookEntry lAccessFeeEntry = DataFactory.newPricebookEntry( lStandardPb , lAcessFee , lOpp );
        Database.insert( lAccessFeeEntry );
        
        PricebookEntry lAccessFeeFamilyMemberEntry = DataFactory.newPricebookEntry( lStandardPb , lAccessFeeFamilyMember , lOpp );
        Database.insert( lAccessFeeFamilyMemberEntry );
        
        Product2 lSetupFee = DataFactory.newProduct( 'Setup Fee' , false , 'BRL' );
        Database.insert( lSetupFee );
        
        PricebookEntry lSetupFeeEntry = DataFactory.newPricebookEntry( lStandardPb , lSetupFee , lOpp );
        Database.insert( lSetupFeeEntry );
        
        Product2 lProfServicesOneFee = DataFactory.newProduct( 'Professional Services Setup Fee' , false , 'BRL' );
        Database.insert( lProfServicesOneFee );
        
        PricebookEntry lProfServicesOneFeeEntry = DataFactory.newPricebookEntry( lStandardPb , lProfServicesOneFee , lOpp );
        Database.insert( lProfServicesOneFeeEntry );
        
        Product2 lProfServicesMainFee = DataFactory.newProduct( 'Professional Services Maintenance Fee' , false , 'BRL' );
        Database.insert( lProfServicesMainFee );
        
        PricebookEntry lProfServicesMainFeeEntry = DataFactory.newPricebookEntry( lStandardPb , lProfServicesMainFee , lOpp );
        Database.insert( lProfServicesMainFeeEntry );
        
        lOpp = DataFactory.newOpportunity( lParentAcc.Id , lStandardPb , 'Client_Sales_New_Business' );
        lOpp.Quantity_Offer_Number_of_Employees__c = 500;
        Database.insert( lOpp );
        
        Quote quote = DataFactory.newQuote(lOpp, 'testeMateus', 'Client_Sales_New_Business');
        Database.insert(quote);
        
        QuoteLineItem accessFee = DataFactory.newQuoteLineItem(quote, lSetupFeeEntry);
        Database.insert(accessFee);
        
        Payment__c payment = DataFactory.newPayment(accessFee);
        Database.insert(payment);
        
        Comission_Price__c comissionPrice = DataFactory.newComissionPrice(30, 30, lSetupFee.id, 'BRL');
        Database.insert(comissionPrice);
        
        Eligibility__c eligibility = DataFactory.newEligibility(payment);
        eligibility.Payment__c = payment.id;
        eligibility.Payment_Method__c = 'Credit Card';
        Database.insert(eligibility);
        
        Database.insert( DataFactory.newGympassEntity( 'Gympass' ) );
    }
}