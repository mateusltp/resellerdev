/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 05-20-2022
 * @last modified by  : alysson.mota@gympass.com
**/

@IsTest
public with sharing class AccountServiceTest {


    @TestSetup
    static void setupData(){
        PS_Constants constants = PS_Constants.getInstance();
        String recordTypePartnerFlow = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(constants.ACCOUNT_RT_PARTNER_FLOW_ACCOUNT).getRecordTypeId();
        Account partnerAcc = PartnerDataFactory.newAccount();
        partnerAcc.RecordTypeId = recordTypePartnerFlow;
        partnerAcc.CRM_ID__c = '1231';
        partnerAcc.Network_ID__c = '12345';
        partnerAcc.Legal_Document_Type__c = 'CNPJ';
        partnerAcc.Legal_Title__c = 'AccountServiceTest';
        partnerAcc.BillingCountryCode = 'US';
        partnerAcc.BillingCountry = 'United States';
        partnerAcc.BillingState = 'Arizona';
        partnerAcc.BillingPostalCode = '1223';
        partnerAcc.ShippingCountryCode = 'US';
        partnerAcc.ShippingCountry = 'United States';
        partnerAcc.ShippingState = 'Arizona';
        partnerAcc.CurrencyIsoCode = 'USD';
        partnerAcc.Description = 'dasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasda';
        INSERT partnerAcc;

        Account childAcc = PartnerDataFactory.newChildAccount(partnerAcc.Id);
        childAcc.RecordTypeId = recordTypePartnerFlow;
        childAcc.CRM_ID__c = '4455';
        childAcc.Legal_Document_Type__c = 'CNPJ';
        childAcc.Partner_Level__c = 'Location';
        childAcc.Legal_Title__c = 'AccountServiceTestChild1';
        childAcc.BillingCountryCode = 'US';
        childAcc.BillingCountry = 'United States';
        childAcc.BillingState = 'Arizona';
        childAcc.BillingPostalCode = '1223';
        childAcc.ShippingCountryCode = 'US';
        childAcc.ShippingCountry = 'United States';
        childAcc.ShippingState = 'Arizona';
        childAcc.CurrencyIsoCode = 'USD';
        childAcc.Description = 'dasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasda';

        List<Account> childs = new List<Account>();
        childs.add(childAcc);

        Account childAcc2 = PartnerDataFactory.newChildAccount(partnerAcc.Id);
        childAcc2.RecordTypeId = recordTypePartnerFlow;
        childAcc2.Name = 'Child2';
        childAcc2.CRM_ID__c = '3213';
        childAcc2.Network_ID__c = '12345';
        childAcc2.Legal_Document_Type__c = 'CNPJ';
        childAcc2.Partner_Level__c = 'Location';
        childAcc2.Legal_Title__c = 'AccountServiceTestChild2';
        childAcc2.BillingCountryCode = 'US';
        childAcc2.BillingCountry = 'United States';
        childAcc2.BillingState = 'Arizona';
        childAcc2.BillingPostalCode = '3144';
        childAcc2.ShippingCountryCode = 'US';
        childAcc2.ShippingCountry = 'United States';
        childAcc2.ShippingState = 'Arizona';
        childAcc2.CurrencyIsoCode = 'USD';
        childAcc2.Description = 'dasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasda';

        childs.add(childAcc2);
        INSERT childs;


        String recordTypePartnerContactFlow = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Contact').getRecordTypeId();
        Contact partnerContact = PartnerDataFactory.newContact(partnerAcc.Id);
        partnerContact.RecordTypeId = recordTypePartnerContactFlow;
        partnerContact.Type_of_Contact__c = 'Decision Maker';
        partnerContact.FirstName = 'Admin';
        partnerContact.UUID__c = new Uuid().getValue();
        INSERT partnerContact;


        Opportunity aOpp = PartnerDataFactory.newOpportunity( partnerAcc.Id, 'Partner_Flow_Opportunity');
        aOpp.Name = 'AccountServiceNetwork';
        aOpp.CurrencyIsoCode = 'USD';
        INSERT aOpp;

        APXT_Redlining__Contract_Agreement__c contractAgreementPartner = PartnerDataFactory.newContractAgreement(aOpp,  partnerAcc, 'Hybrid Contract');
        contractAgreementPartner.APXT_Redlining__Status__c = 'Active';
        contractAgreementPartner.Opportunity__c = aOpp.Id;
        contractAgreementPartner.APXT_Amendment_Version__c = 0;
        contractAgreementPartner.Conga_Sign_Status__c = 'DRAFT';
        contractAgreementPartner.Include_full_T_C_s__c = true;
        contractAgreementPartner.Include_signature__c = false;
        contractAgreementPartner.Legal_Contract_Status__c= 'Contract Draft';
        contractAgreementPartner.APXT_Renegotiation__c = false;
        contractAgreementPartner.APXT_Redlining__Status__c = 'In Process';
        contractAgreementPartner.Template_Selection__c = 'USA';
        contractAgreementPartner.RecordTypeId = Schema.SObjectType.APXT_Redlining__Contract_Agreement__c.getRecordTypeInfosByDeveloperName().get('Partner_Contract').getRecordTypeId();

        INSERT contractAgreementPartner;

        Account_Contract_Agreement_Relationship__c accContractAgreemRel = new Account_Contract_Agreement_Relationship__c();
        accContractAgreemRel.AccountId__c = partnerAcc.Id;
        accContractAgreemRel.ContractAgreementId__c = contractAgreementPartner.Id;

        Account_Contract_Agreement_Relationship__c accContractAgreemRelChild = new Account_Contract_Agreement_Relationship__c();
        accContractAgreemRelChild.AccountId__c = childAcc.Id;
        accContractAgreemRelChild.ContractAgreementId__c = contractAgreementPartner.Id;

        Account_Contract_Agreement_Relationship__c accContractAgreemRelChild2 = new Account_Contract_Agreement_Relationship__c();
        accContractAgreemRelChild2.AccountId__c = childAcc2.Id;
        accContractAgreemRelChild2.ContractAgreementId__c = contractAgreementPartner.Id;

        List<Account_Contract_Agreement_Relationship__c> accContractRelLst = new List<Account_Contract_Agreement_Relationship__c>();
        accContractRelLst.add(accContractAgreemRel);
        accContractRelLst.add(accContractAgreemRelChild);
        accContractRelLst.add(accContractAgreemRelChild2);
        INSERT accContractRelLst;

        Account_Opportunity_Relationship__c oppMember = PartnerDataFactory.newAcctOppRel(partnerAcc.Id, aOpp.Id);
        oppMember.RecordTypeId = Schema.SObjectType.Account_Opportunity_Relationship__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Opportunity_Member').getRecordTypeId();
        INSERT oppMember;

        Account_Opportunity_Relationship__c oppMemberChild = PartnerDataFactory.newAcctOppRel(childAcc.Id, aOpp.Id);
        oppMemberChild.RecordTypeId = Schema.SObjectType.Account_Opportunity_Relationship__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Opportunity_Member').getRecordTypeId();
        INSERT oppMemberChild;

    }

    @IsTest
    Private static void wishListAndTierDependency_Partner_UnitTest(){
        Id rtAccId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gym_Partner').getRecordTypeId();
        fflib_ApexMocks mocks = new fflib_ApexMocks();
        fflib_SObjectUnitOfWork mockUOW = (fflib_SObjectUnitOfWork)mocks.mock(fflib_SObjectUnitOfWork.class);
        AccountSelector mockAccountSelector = (AccountSelector) mocks.mock(AccountSelector.class);
        Id mockParentAccountId = fflib_IDGenerator.generate(Account.SObjectType);

        List<Account> mockAccountLst = new List<Account> { new Account (
                parentId = mockParentAccountId,
                name = 'formAccountName',
                shippingStateCode = 'São Paulo',
                shippingCountryCode = 'BR',
                shippingStreet= 'Rua SOC Test',
                shippingPostalCode = '111111',
                shippingCity = 'City',
                Types_of_ownership__c = 'Franchise',
                Partner_Level__c = 'Location',
                recordTypeId = rtAccId,
                Wishlist__c = true,
                Tier__c = 'T4'
        )};

        mocks.startStubbing();
        mocks.when(mockAccountSelector.sobjectType()).thenReturn(Account.sObjectType);
        ((fflib_SObjectUnitOfWork)mocks.doThrowWhen(new AccountService.AccountServiceException(), mockUOW)).commitWork();
        mocks.stopStubbing();

        Application.UnitOfWork.setMock(mockUOW);
        Application.Selector.setMock(mockAccountSelector);

        try {
            Test.startTest();
            new AccountService().wishListAndTierDependency(mockAccountLst);
            Test.stopTest();
        } catch(Exception e){
            System.assert(e instanceof AccountService.AccountServiceException);
        }
    }

    @IsTest
    Private static void wishListAndTierDependency_Partner_UnitTest2(){
        Id rtAccId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gym_Partner').getRecordTypeId();
        fflib_ApexMocks mocks = new fflib_ApexMocks();
        fflib_SObjectUnitOfWork mockUOW = (fflib_SObjectUnitOfWork)mocks.mock(fflib_SObjectUnitOfWork.class);
        AccountSelector mockAccountSelector = (AccountSelector) mocks.mock(AccountSelector.class);
        Id mockParentAccountId = fflib_IDGenerator.generate(Account.SObjectType);

        List<Account> mockAccountLst = new List<Account> {new Account (
                parentId = mockParentAccountId,
                name = 'formAccountName',
                shippingStateCode = 'São Paulo',
                shippingCountryCode = 'BR',
                shippingStreet= 'Rua SOC Test',
                shippingPostalCode = '111111',
                shippingCity = 'City',
                Types_of_ownership__c = 'Franchise',
                Partner_Level__c = 'Location',
                recordTypeId = rtAccId,
                Wishlist__c = false,
                Tier__c = 'T3'
        )};

        mocks.startStubbing();
        mocks.when(mockAccountSelector.sobjectType()).thenReturn(Account.sObjectType);
        ((fflib_SObjectUnitOfWork)mocks.doThrowWhen(new AccountService.AccountServiceException(), mockUOW)).commitWork();
        mocks.stopStubbing();

        Application.UnitOfWork.setMock(mockUOW);
        Application.Selector.setMock(mockAccountSelector);

        try {
            Test.startTest();
            new AccountService().wishListAndTierDependency(mockAccountLst);
            Test.stopTest();
        } catch(Exception e){
            System.assert(e instanceof AccountService.AccountServiceException);
        }
    }

    @IsTest
    private static void avoidDuplicatedName_Partner_UnitTest(){
        Id rtAccId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gym_Partner').getRecordTypeId();
        fflib_ApexMocks mocks = new fflib_ApexMocks();
        fflib_SObjectUnitOfWork mockUOW = (fflib_SObjectUnitOfWork)mocks.mock(fflib_SObjectUnitOfWork.class);
        AccountSelector mockAccountSelector = (AccountSelector) mocks.mock(AccountSelector.class);

        Id mockParentAccountId = fflib_IDGenerator.generate(Account.SObjectType);

        List<Account> mockFormAccountLst = new List<Account> { new Account (
                parentId = mockParentAccountId,
                name = 'formAccountName',
                shippingStateCode = 'São Paulo',
                shippingCountryCode = 'BR',
                shippingStreet= 'Rua SOC Test',
                shippingPostalCode = '111111',
                shippingCity = 'City',
                Types_of_ownership__c = 'Franchise',
                Partner_Level__c = 'Location',
                recordTypeId = rtAccId

        )};

        Map<Id, Account> accountResponseToClone = new Map<Id, Account>{ mockParentAccountId => new Account (
                id= mockParentAccountId,
                name = 'formAccountName',
                shippingStateCode = 'Minas Gerais',
                shippingCountryCode = 'BR',
                shippingStreet= 'Rua Parent',
                shippingPostalCode = '111111',
                shippingCity = 'City',
                Types_of_ownership__c = 'Franchise',
                Partner_Level__c = 'Franchisor',
                recordTypeId = rtAccId
        )};
        Map<String, Account> responseAccountNameMap = new Map<String, Account>{'formAccountName'=>mockFormAccountLst[0]};

        mocks.startStubbing();
        mocks.when(mockAccountSelector.sobjectType()).thenReturn(Account.sObjectType);
        mocks.when(mockAccountSelector.selectAccountNamePartners(mockFormAccountLst)).thenReturn(responseAccountNameMap);
        ((fflib_SObjectUnitOfWork)mocks.doThrowWhen(new AccountService.AccountServiceException(), mockUOW)).commitWork();
        mocks.stopStubbing();

        Application.UnitOfWork.setMock(mockUOW);
        Application.Selector.setMock(mockAccountSelector);

        try {
            Test.startTest();
            new AccountService().avoidDuplicateAccountName(mockFormAccountLst);
            Test.stopTest();
        } catch(Exception e){
            System.assert(e instanceof AccountService.AccountServiceException);
        }
    }

    @IsTest
    private static void avoidDuplicatedNameError_Partner_UnitTest(){

        Id rtAccId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Digital_Partner').getRecordTypeId();

        fflib_ApexMocks mocks = new fflib_ApexMocks();
        fflib_SObjectUnitOfWork mockUOW = (fflib_SObjectUnitOfWork)mocks.mock(fflib_SObjectUnitOfWork.class);
        AccountSelector mockAccountSelector = (AccountSelector) mocks.mock(AccountSelector.class);


        Id mockParentAccountId = fflib_IDGenerator.generate(Account.SObjectType);

        List<Account> mockFormAccountLst = new List<Account> { new Account (
                parentId = mockParentAccountId,
                name = 'formAccountName',
                shippingStateCode = 'São Paulo',
                shippingCountryCode = 'BR',
                shippingStreet= 'Rua SOC Test',
                shippingPostalCode = '111111',
                shippingCity = 'City',
                Types_of_ownership__c = 'Franchise',
                Partner_Level__c = 'Location',
                recordTypeId = rtAccId
        )};

        Map<String, Account> responseAccountNameMap = new Map<String, Account>{'formAccountName'=>mockFormAccountLst[0]};

        mocks.startStubbing();
        mocks.when(mockAccountSelector.sobjectType()).thenReturn(Account.sObjectType);
        //  mocks.when(mockAccountSelector.selectAccountNamePartners(mockFormAccountLst)).thenReturn(responseAccountNameMap);
        ((AccountSelector)mocks.doThrowWhen(new AccountService.AccountServiceException(), mockAccountSelector)).selectAccountNamePartners( mockFormAccountLst );
        mocks.stopStubbing();

        Application.UnitOfWork.setMock(mockUOW);
        Application.Selector.setMock(mockAccountSelector);

        try {
            Test.startTest();
            new AccountService().avoidDuplicateAccountName(mockFormAccountLst);
            Test.stopTest();
        } catch(Exception e){
            System.assert(e instanceof AccountService.AccountServiceException);
        }
    }


    @IsTest
    private static void buildNetwork_Success(){
        Test.startTest();
        Account acc = [SELECT ID, RecordType.DeveloperName FROM Account WHERE Legal_Title__c = 'AccountServiceTest' Limit 1];
        AccountService.buildNetwork(acc.Id, acc.RecordType.DeveloperName);
        Test.stopTest();

    }

    @IsTest
    private static void buildNetworkForContracts_Success(){

        Test.startTest();
        Account acc = [SELECT ID, RecordType.DeveloperName FROM Account WHERE Legal_Title__c = 'AccountServiceTest' Limit 1];
        Opportunity opp = [SELECT ID, RecordType.DeveloperName FROM Opportunity WHERE Name Like '%AccountServiceNetwork' Limit 1];
        AccountService.buildNetworkForContracts(acc.Id, opp.Id,acc.RecordType.DeveloperName);
        Test.stopTest();

    }

    @IsTest
    private static void buildNetworkForContracts_Error(){
        Test.startTest();
        try {
            Contact contact = [SELECT ID  FROM Contact WHERE FirstName = 'Admin'  Limit 1];
            AccountService.buildNetworkForContracts(contact.Id, contact.Id, 'INVALID RECORD TYPE');
        } catch(Exception e){
            System.assert(e instanceof AccountService.AccountServiceException);
        }
        Test.stopTest();
    }


    @IsTest
    private static void getAccountWishlistFromParent_Success(){

        Test.startTest();
        List<Account> accLst = [SELECT ID,RecordTypeId, ParentId, Wishlist__c,Tier__c FROM Account WHERE Legal_Title__c LIKE 'AccountServiceTest%'];
        new AccountService().getAccountWishlistFromParent(accLst);
        Test.stopTest();

    }

    @IsTest
    private static void setAccountTypeProspect_Success(){
        Test.startTest();
        List<Account> accLst = [SELECT ID,RecordTypeId, Type FROM Account WHERE Legal_Title__c LIKE 'AccountServiceTest%'];
        new AccountService().setAccountTypeProspect(accLst);
        Test.stopTest();
    }

    @IsTest
    private static void setAccountTypeProspect_Error(){
        Test.startTest();
        try {
            List<Account> accLst = [SELECT ID, RecordTypeId  FROM Account WHERE Legal_Title__c LIKE 'AccountServiceTest%'];
            new AccountService().setAccountTypeProspect(accLst);
        } catch(Exception e){
            system.debug('## error msg: '+e.getMessage());
            System.assert(e instanceof AccountService.AccountServiceException);
        }
        Test.stopTest();
    }


    @IsTest
    private static void getAccountWishlistFromParent_Error(){

        Test.startTest();
        try {
            List<Account> accLst = [SELECT ID, RecordTypeId  FROM Account WHERE Legal_Title__c LIKE 'AccountServiceTest%'];
            new AccountService().getAccountWishlistFromParent(accLst);
        } catch(Exception e){
            system.debug('## error msg: '+e.getMessage());
            System.assert(e instanceof AccountService.AccountServiceException);
        }
        Test.stopTest();
    }

    @IsTest
    private static void avoidDuplicatedNetworkIds_Success(){

        Test.startTest();
        List<Account> accLst = [SELECT ID,RecordTypeId, Network_ID__c, Name,Ultimate_Parent__c FROM Account WHERE Legal_Title__c LIKE 'AccountServiceTest%'];
        new AccountService().avoidDuplicatedNetworkIds(accLst);
        Test.stopTest();

    }

    @IsTest
    private static void avoidDuplicatedNetworkIdsOnUpdate_Success(){

        Test.startTest();
        Account acc = [SELECT ID,RecordTypeId, Network_ID__c, Name,Ultimate_Parent__c FROM Account WHERE Legal_Title__c = 'AccountServiceTest' LIMIT 1];

        acc.Network_ID__c =  '748596';

        update acc;
        Test.stopTest();
    }

    @IsTest
    private static void avoidDuplicatedNetworkIds_Error(){

        Test.startTest();
        try {
            List<Account> accLst = [SELECT ID,RecordTypeId  FROM Account WHERE Legal_Title__c LIKE 'AccountServiceTest%'];
            new AccountService().avoidDuplicatedNetworkIds(accLst);
        } catch(Exception e){
            System.assert(e instanceof AccountService.AccountServiceException);
        }
        Test.stopTest();
    }


    @IsTest
    private static void getContactRelations_Success(){

        Test.startTest();
        Account acc = [SELECT ID, RecordType.DeveloperName FROM Account WHERE Legal_Title__c = 'AccountServiceTest' Limit 1];
        List<AccountContactRelation> resultLst = AccountService.getContactRelations(new Set<Id>{acc.Id}, acc.RecordType.DeveloperName);
        Test.stopTest();

    }

    @IsTest
    private static void getContactRelations_Error(){
        Test.startTest();
        try {
            Contact contact = [SELECT ID  FROM Contact WHERE FirstName = 'Admin'  Limit 1];
            List<AccountContactRelation> resultLst = AccountService.getContactRelations(new Set<Id>{contact.Id}, 'INVALID RECORD TYPE');
        } catch(Exception e){
            System.assert(e instanceof AccountService.AccountServiceException);
        }
        Test.stopTest();
    }

    @IsTest
    private static void getContactsFromParentThatAreNotRelatedToChild_Success(){

        Test.startTest();
        Account aChildAcc = [SELECT ID,ParentId, RecordType.DeveloperName FROM Account WHERE Legal_Title__c = 'AccountServiceTestChild1' Limit 1];
        List<AccountContactRelation> resultLst = AccountService.getContactsFromParentThatAreNotRelatedToChild(aChildAcc.Id, aChildAcc.ParentId, aChildAcc.RecordType.DeveloperName);
        Test.stopTest();

    }

    @IsTest
    private static void getContactsFromParentThatAreNotRelatedToChild_Error(){

        Test.startTest();
        try {
            Contact contact = [SELECT ID  FROM Contact WHERE FirstName = 'Admin'  Limit 1];
            List<AccountContactRelation> resultLst = AccountService.getContactsFromParentThatAreNotRelatedToChild(contact.Id, contact.Id, 'INVALID RECORD TYPE');
        } catch(Exception e){
            System.assert(e instanceof AccountService.AccountServiceException);
        }
        Test.stopTest();

    }

    @IsTest
    private static void getContactsFromParent_Success(){

        Test.startTest();
        Account aChildAcc = [SELECT ID,ParentId, RecordType.DeveloperName FROM Account WHERE Legal_Title__c = 'AccountServiceTestChild1' Limit 1];
        List<AccountContactRelation> resultLst = AccountService.getContactsFromParent(aChildAcc.Id, aChildAcc.RecordType.DeveloperName);
        Test.stopTest();

    }

    @IsTest
    private static void getContactsFromParent_Error(){
        Test.startTest();
        try {
            Contact contact = [SELECT ID  FROM Contact WHERE FirstName = 'Admin'  Limit 1];
            List<AccountContactRelation> resultLst = AccountService.getContactsFromParent(contact.Id, 'DeveloperName');
        } catch(Exception e){
            System.assert(e instanceof AccountService.AccountServiceException);
        }
        Test.stopTest();
    }


}