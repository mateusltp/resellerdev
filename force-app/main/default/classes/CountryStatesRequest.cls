public without sharing class CountryStatesRequest {
    public List<String> countriesIsoCodes;
  
    public List<String> getCountriesIsoCodes() {
      return this.countriesIsoCodes;
    }

  }