public with sharing class MassiveAccountRequestSelector {
    

    public static String getMassiveAccountRequestByMassiveStatusAndMassiveId(List<String> massiveStatus, Id massiveId){
        String query = 'SELECT Id, Name, Massive_Account_Request__c, Unique_Identifier__c, Website__c, Unique_Identifier_Type__c, Billing_Country__c, Engine_Status__c, Massive_Status__c ';
        query += ' FROM Account_Request__c ';
        query += ' WHERE Massive_Account_Request__c =:massiveId AND Bulk_Operation__c = true AND Massive_Status__c IN:massiveStatus ';
        return query;
    }
}