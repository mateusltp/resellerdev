public class QualtricsContactDTO {
    
    public QualtricsContactDTO(){}
    
    public class Contacts {
		public String firstName;
		public String lastName;
		public String email;
		public String extRef;
		public EmbeddedData embeddedData;
		public String language;
		public Boolean unsubscribed;
	}

	public class EmbeddedData {
		public String LaunchDate;
		public String ContractSignatureDate;
		public String ContractEndDate;
		public String MobilePhone;
		public String Role;
		public String AccountID;
		public String AccountRecordType;
		public String BillingCountry;
		public String TotalNofEmployees;
		public String GympassCRMID;
		public String Size;
		public String AccountOwnerFullName;
		public String Tagus;
		public String BusinessUnit;
		public String NumberofDependents;
		public String PaymentMethod;
		public String AccountName;
	}

	public List<Contacts> contacts;

	
	//public static QualtricsContactDTO parse(String json) {
		//return (QualtricsContactDTO) System.JSON.deserialize(json, QualtricsContactDTO.class);
	//}
}