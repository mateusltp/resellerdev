/**
 * @description       : 
 * @author            : gft.jorge.stevaux@ext.gympass.com
 * @group             : 
 * @last modified on  : 09-13-2021
 * @last modified by  : gft.jorge.stevaux@ext.gympass.com
**/
global with sharing class AccountDataProvider implements sortablegrid.sdgIDataProvider {
    
    global static Boolean isUserSelectable()
    {
        return true;
    }
       
    global sortablegrid.SDGResult getData(sortablegrid.SDG coreSDG,  sortablegrid.SDGRequest request)
    {

        System.debug('request'+request);
        sortablegrid.SDGResult result = new sortablegrid.SDGResult();
        System.debug('result >>>'+result);
                
        result.data = new List<Sobject>();  
        System.debug('result data'+result.data);

        Set<Id> aAccIdSet = new Set<Id> {request.ParentRecordID};
        System.debug('Parent Id Set >>>'+aAccIdSet);
        
        AccountOpportunitySelector accOppSelector = (AccountOpportunitySelector)Application.Selector.newInstance(Account_Opportunity_Relationship__c.sobjectType);
        Map<Id, List<Product_Opportunity_Assignment__c>> mProductOppAssignByOpp = accOppSelector.selectProductAssignmentToSDGGrid(aAccIdSet);
        System.debug('Map Query >>>' +mProductOppAssignByOpp);

        List<Product_Opportunity_Assignment__c> prodOppAssLst = mProductOppAssignByOpp.get(request.ParentRecordID);
        System.debug('List with Product Values >>>'+prodOppAssLst);

        result.data.addAll(prodOppAssLst);
        result.FullQueryCount = result.data.size();
        result.pagecount = 1;
        result.isError = false;
        result.ErrorMessage = '';
        return result;  
    }
    
    global sortablegrid.SDG LoadSDG(String SDGTag, String ParentRecordId)
    {
        sortablegrid.SDG CoreSDG = new sortablegrid.SDG( 'CustomDataProviderExample' );
        CoreSDG.SDGFields = GetFields();
        return CoreSDG;
    }
    
    private List<sortablegrid.SDGField> GetFields()
    {        
        List<sortablegrid.SDGField> fields = new List<sortablegrid.SDGField>();
        
        fields.add( new sortablegrid.SDGField('1', 'PROD NAME', 'ProductAssignmentId__r.ProductId__r.Name', 'STRING', '', false, false, null, 1));
        fields.add( new sortablegrid.SDGField('3', 'TYPE', 'ProductAssignmentId__r.ProductId__r.Type__c', 'STRING', '', false, false, null, 3));
        fields.add( new sortablegrid.SDGField('4', 'NET TRANSFER PRICE', 'ProductAssignmentId__r.ProductId__r.Net_Transfer_Price__c', 'DOUBLE', '', false, false, null, 4));
        fields.add( new sortablegrid.SDGField('5', 'CURRENCY ISO CODE', 'ProductAssignmentId__r.ProductId__r.CurrencyIsoCode', 'STRING', '', false, false, null, 5));
        fields.add( new sortablegrid.SDGField('6', 'CC NAME', 'ProductAssignmentId__r.CommercialConditionId__r.Name', 'STRING', '', false, false, null, 6));
        fields.add( new sortablegrid.SDGField('8', 'CC VALUE', 'ProductAssignmentId__r.CommercialConditionId__r.Value__c', 'DOUBLE', '', false, false, null, 8));
        fields.add( new sortablegrid.SDGField('9', 'CC RECORD TYPE', 'ProductAssignmentId__r.CommercialConditionId__r.RecordType.DeveloperName', 'STRING', '', false, false, null, 9));
        
        return fields;
    }
    
}