/**
* @File Name          : ScheduleGetFeedbackTest.cls
* @Description        : 
* @Author             : JRDL@GFT.com
* @Group              :
* @Last Modified By   : 
* @Last Modified On   : 
* @Modification Log   :
* Ver       Date            Author      		    Modification
* 1.0    10/06/2021   JRDL@GFT.com     Initial Version
**/
@isTest
public class ScheduleGetFeedbackTest {
	public static String CRON_EXP = '0 0 13 * * ?';
    
    @isTest
    public static void scheduleGFPreRenewal() {
    	Test.startTest();
        String jobId = System.schedule('ScheduledApexTest',
            CRON_EXP, 
            new ScheduleGFPreRenewal());         
        Test.stopTest();
    }
    
    @isTest
    public static void scheduleGFPostLaunch() {
    	Test.startTest();
        String jobId = System.schedule('ScheduledApexTest',
            CRON_EXP, 
            new ScheduleGFPostLaunch());         
        Test.stopTest();
    }
    
    @isTest
    public static void scheduleGFProspectingSales() {
    	Test.startTest();
        String jobId = System.schedule('ScheduledApexTest',
            CRON_EXP, 
            new ScheduleGFProspectingSales());         
        Test.stopTest();
    }
    
    @isTest
    public static void scheduleGFServiceUtilization() {
    	Test.startTest();
        String jobId = System.schedule('ScheduledApexTest',
            CRON_EXP, 
            new ScheduleGFServiceUtilization());         
        Test.stopTest();
    }
    
    @isTest
    public static void scheduleGFPartnerExperience() {
    	Test.startTest();
        String jobId = System.schedule('ScheduledApexTest',
            CRON_EXP, 
            new ScheduleGFPartnerExperience());         
        Test.stopTest();
    }
}