/**
* @File Name          : StepsTowardsSuccessForGymsTest.cls
* @Description        : 
* @Author             : Samuel Silva - GFT (slml@gft.com)
* @Group              : 
* @Last Modified By   : gft.samuel.silva@ext.gympass.com
* @Last Modified On   : 08-13-2021
* @Modification Log   : 
* Ver       Date            Author                 Modification
* 1.0    17/04/2020   Samuel Silva - GFT (slml@gft.com)     Initial Version
**/
@isTest
public with sharing class StepsTowardsSuccessForGymsTest {
    
    @TestSetup
    static void generateGenericDataFor10Steps(){
        
        /** Account - Generic Fake results (no opp closed as won for this acc)*/
        Id rtId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gym_Partner').getRecordTypeId();
        Account fakeAcc = new Account(name='fakeAcc', website='www.fakeAcc.com',  Legal_Title__c = 'title',
                                      Id_Company__c = '11111',
                                      RecordTypeId = rtId,ShippingStreet = 'Rua fakeAcc', ShippingCountry = 'Brazil', ShippingCity = 'FakeCity', ShippingState = 'Maranhão', GP_Status__c = 'Pending Approval',
                                      Types_of_ownership__c = Label.franchisePicklist);
        INSERT fakeAcc;        
        
        Account fakeChild = new Account(name='fakeChild', website='www.fakeChild.com',  Legal_Title__c = 'title',
                                        Id_Company__c = '11111',
                                        RecordTypeId = rtId,ShippingStreet='rua fakechild',ShippingCity ='FakeChildCity', ShippingCountry = 'Brazil', ParentId =fakeAcc.id, 
                                        ShippingState = 'Rio de Janeiro', Types_of_ownership__c = Label.franchisePicklist);
        INSERT fakeChild;
        
        /**Account*/
        Account acc = new Account();
        
        acc.name='AcademiaBrasilCompanyPai';
        acc.RecordTypeId = rtId;
        acc.GP_Status__c = 'Active';
        acc.Legal_Title__c = 'title';
        acc.Id_Company__c = '11111';
        acc.ShippingState = 'Minas Gerais';
        acc.CAP_Value__c = 120;
        acc.ShippingCity = 'CityAcademiaBrasil';
        acc.ShippingStreet = 'Rua academiabrasilpai';
        acc.ShippingCountry = 'Brazil';
        acc.Gym_Type__c = 'Studios';
        acc.Gym_Classes__c = 'Cardio';
        acc.Types_of_ownership__c = Label.franchisePicklist;
        acc.Subscription_Type__c = 'Value per class';
        acc.Subscription_Period__c = 'Monthy value';
        acc.Subscription_Type_Estimated_Price__c    = 100;
        acc.Has_market_cannibalization__c = 'No';
        acc.Exclusivity__c = 'Yes';
        acc.Exclusivity_End_Date__c = Date.today().addYears(1);
        acc.Exclusivity_Partnership__c = 'Full Exclusivity';
        acc.Exclusivity_Restrictions__c= 'No';
        acc.Website = 'testing@tesapex.com';
        acc.Gym_Email__c = 'gymemail@apex.com';
        acc.Phone = '3222123123';
        acc.Can_use_logo__c = 'Yes';
        acc.Legal_Registration__c = 12123;
        acc.Legal_Title__c = 'Title LTDA';
        acc.Gyms_Identification_Document__c = 'CNPJ'; 
        INSERT acc;
        
        Account child = new Account();
        child.name='academiaBrasilCompanyChild1'; 
        child.ParentId = acc.Id;
        child.Legal_Title__c = 'title';
        child.Id_Company__c = '11111';
        child.GP_Status__c = 'Pending Approval';
        child.website='www.ChildOne.com';
        child.CAP_Value__c = 120;
        child.RecordTypeId = rtId; 
        child.Gym_Type__c ='Studios';     
        child.Gym_Classes__c = 'Cardio';   
        child.ShippingStreet = 'Rua BrasilChild';
        child.ShippingCity = 'BrasilChild';
        child.ShippingCountry = 'Brazil'; 
        child.ShippingState = 'Roraima';
        child.Types_of_ownership__c = Label.franchisePicklist;
        child.Subscription_Type__c = 'Value per class';
        child.Subscription_Period__c = 'Monthy value';
        child.Subscription_Type_Estimated_Price__c = 100;
        child.Exclusivity__c = 'Yes';
        child.Exclusivity_End_Date__c = Date.today().addYears(1);
        child.Exclusivity_Partnership__c = 'Full Exclusivity';
        child.Exclusivity_Restrictions__c= 'No';
        child.Gym_Email__c = 'gymemail@apex.com';
        child.Phone = '3222123123';
        
        INSERT child; 
        
        /**Account with Negative results*/
        Account accNegative = new Account();
        accNegative.name='accNegative';
        accNegative.Legal_Title__c = 'title';
        accNegative.Id_Company__c = '11111';
        accNegative.website='www.accNegative.com';
        accNegative.GP_Status__c = 'Pending Approval';
        accNegative.RecordTypeId = rtId;
        accNegative.CAP_Value__c = 120;
        accNegative.Gym_Type__c  = 'Studios';
        accNegative.Gym_Classes__c = 'Cardio';
        accNegative.ShippingStreet = 'Rua CityNEgative';
        accNegative.ShippingCity = 'CityNEgative';
        accNegative.ShippingCountry = 'Brazil';
        accNegative.ShippingState = 'Mato Grosso';
        accNegative.Types_of_ownership__c = Label.franchisePicklist;
        accNegative.Subscription_Type__c = 'Value per class';
        accNegative.Subscription_Period__c = 'Monthy value';
        accNegative.Subscription_Type_Estimated_Price__c = 100;
        accNegative.Has_market_cannibalization__c = 'No';
        accNegative.Exclusivity__c = 'No';
        accNegative.Gym_Email__c = 'accNegative@apex.com';
        accNegative.Phone = 'accNegative'; 
        accNegative.Can_use_logo__c = 'Yes';     
        accNegative.Legal_Registration__c = 12123;
        accNegative.Legal_Title__c = 'Title LTDA';
        accNegative.Gyms_Identification_Document__c = 'CNPJ';   
        
        INSERT accNegative;  
        
        Account childNegative = new Account();
        childNegative.name='childNegative';
        childNegative.ParentId = accNegative.Id;
        childNegative.GP_Status__c = 'Pending Approval';
        childNegative.website='www.childNegative.com';
        childNegative.RecordTypeId = rtId;
        childNegative.Legal_Title__c = 'title';
        childNegative.Id_Company__c = '11111';
        child.CAP_Value__c = 120;
        childNegative.ShippingStreet='Rua ChildNEgative';
        childNegative.ShippingCity = 'ChildNEgativeCity';
        childNegative.ShippingCountry = 'Brazil';
        childNegative.Gym_Type__c = 'Studios';
        childNegative.Gym_Classes__c = 'Cardio';
        childNegative.ShippingState = 'Pará';
        childNegative.Types_of_ownership__c = Label.franchisePicklist;
        childNegative.Subscription_Type__c = 'Value per class';
        childNegative.Subscription_Period__c = 'Monthy value';
        childNegative.Subscription_Type_Estimated_Price__c  = 100;
        childNegative.Has_market_cannibalization__c = 'No';
        childNegative.Exclusivity__c = 'No';
        childNegative.Website = 'testing@tesapex.com';
        childNegative.Gym_Email__c = 'accNegative@apex.com';
        childNegative.Phone = 'accNegative';       
        
        INSERT childNegative; 
        
        
        /**Opportunity*/                
        List<Opportunity> oppToInsert = new List<Opportunity>();        
        Id oppRtId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Gyms_Small_Medium_Partner').getRecordTypeId();
        
        Opportunity accOpp = new Opportunity();
        accOpp.recordTypeId = oppRtId;
        accOpp.AccountId = acc.id;
        accOpp.Name = 'academiaBrasilCompanyOpp'; 
        accOpp.CMS_Used__c = 'Yes';     
        accOpp.Should_OPS_team_send_the_training__c = 'Standard Training';
        accOpp.Gym_agreed_to_an_API_integration__c  = 'Yes';
        accOpp.Club_Management_System__c = 'Companhia Athletica';
        accOpp.Integration_Fee_Deduction__c = 'No';
        accOpp.CloseDate = Date.today();
        accOpp.Success_Look_Like__c = 'Yes';
        accOpp.Success_Look_Like_Description__c = 'Money money';
        accOpp.Product_Related__c = true;
        accOpp.StageName = 'Qualificação';
        accOpp.Type = 'Expansion';  
        accOpp.Country_Manager_Approval__c = true;
        accOpp.Payment_approved__c = true;   
        accOpp.CurrencyIsoCode = 'BRL';
        accOpp.Gympass_Plus__c = 'Yes';
        accOpp.Standard_Payment__c = 'Yes';
        accOpp.Request_for_self_checkin__c = 'Yes';
        oppToInsert.add(accOpp);
        
        Opportunity negativeOpp = new Opportunity();
        negativeOpp.recordTypeId = oppRtId;
        negativeOpp.AccountId = accNegative.id;
        negativeOpp.Name = 'negativeOpp';      
        negativeOpp.Gym_agreed_to_an_API_integration__c = 'No';
        negativeOpp.CMS_Used__c = 'No';
        negativeOpp.Club_Management_System__c = 'ABC Financial';
        negativeOpp.Integration_Fee_Deduction__c = 'No';
        negativeOpp.CloseDate = Date.today();
        negativeOpp.Success_Look_Like__c = 'No';
        negativeOpp.Success_Look_Like_Description__c = 'None';
        negativeOpp.Approval_Needed__c = 'No';
        negativeOpp.Product_Related__c = true;
        negativeOpp.StageName = 'Qualificação';
        negativeOpp.Type = 'Expansion';     
        negativeOpp.CurrencyIsoCode = 'BRL';
        negativeOpp.Gympass_Plus__c = 'Yes';    
        negativeOpp.Should_OPS_team_send_the_training__c = 'Standard Training';    
        negativeOpp.Standard_Payment__c = 'No';        
        negativeOpp.Justification_Payment__c = 'Testing';          
        negativeOpp.Country_Manager_Approval__c = true;
        negativeOpp.Payment_approved__c = true;         
        negativeOpp.Request_for_self_checkin__c = 'Yes';
        oppToInsert.add(negativeOpp);
        
        INSERT oppToInsert;   


     

        Account_Opportunity_Relationship__c aor = new Account_Opportunity_Relationship__c();
        aor.Account__c = child.Id;
        aor.Opportunity__c = accOpp.id;       
        INSERT aor;
        
        /**Contact*/
        Id ctcRtId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Gyms_Partner').getRecordTypeId();
        List<Contact> contactToInsert = new List<Contact>();
        
        Contact contactAcc = new Contact(
            FirstName = 'Fernando',
            LastName = 'Souza',
            Phone = '233333',
            Email = 'fernandoTestSouze@mail.com',
            recordTypeId = ctcRtId,
            MailingCountry = 'Brazil',
            Type_of_contact__c = 'Point of Contact;Legal Representative',
            Cargo__c = 'CEO',           
            accountId = acc.id
        );
        contactToInsert.add(contactAcc);
        
        Contact contactNegative = new Contact(
            FirstName = 'Murilo',
            LastName = 'Silva',
            Phone = '233333',
            Email = 'murilotest@mail.com',
            recordTypeId = ctcRtId,
            MailingCountry = 'Brazil',
            Type_of_contact__c = 'Point of Contact;Legal Representative',
            Cargo__c = 'Analyst',           
            accountId = accNegative.id
        );
        contactToInsert.add(contactNegative);
        
        INSERT contactToInsert;        
        
        /**Event CLevel btw Contact and Opp*/
        Id evtRtId = Schema.SObjectType.Event.getRecordTypeInfosByDeveloperName().get('Gyms_Event_Partner').getRecordTypeId();
        List<Event> evtsToInsert = new List<Event>();
        
        Event evt = new Event();
        evt.recordTypeId = evtRtId;
        evt.whatId = accOpp.Id;
        evt.whoId = contactAcc.Id;
        evt.Subject = 'Email'; 
        Integer year = Datetime.now().year();
        Integer month = Datetime.now().month();
        Integer day = Datetime.now().day();
        Integer hour = Datetime.now().hour();
        Integer minute = Datetime.now().minute() - 3;
        Integer second = 0;
        string stringDate = year + '-' + month    + '-' + day + ' ' + hour + ':' +    minute + ':' + second;
        evt.StartDateTime = Datetime.valueOf(stringDate); 
        evt.EndDateTime = DateTime.now();
        evt.DurationInMinutes = 3; 
        evtsToInsert.add(evt);   
        
        Event evtNegative = new Event();
        evtNegative.recordTypeId = evtRtId;
        evtNegative.whatId = negativeOpp.Id;
        evtNegative.whoId = contactNegative.Id;
        evtNegative.StartDateTime = evt.startDateTime;
        evtNegative.EndDateTime = evt.EndDateTime;
        evtNegative.DurationInMinutes = evt.DurationInMinutes;
        evtsToInsert.add(evtNegative); 
        
        INSERT evtsToInsert;
        
        
        
        
        /**Bank Account*/       
        Id bkaccRtId = Schema.SObjectType.Bank_Account__c.getRecordTypeInfosByDeveloperName().get('Gyms_Bank_Account').getRecordTypeId();
        
        List<Bank_Account__c> bankAccountLst = new List<Bank_Account__c>();
        
        Bank_Account__c bankAccount = new Bank_Account__c();
        bankAccount.recordTypeId = bkaccRtId;
        bankAccount.Bank_Account_Number_or_IBAN__c = 'abc';
        bankAccount.Bank_Account_Ownership__c = 'sadsa';
        bankAccount.Bank_Name__c = 'ItauFake';
        bankAccount.BIC_or_Routing_Number_or_Sort_Code__c = 'abc';
        bankAccount.Routing_Number__c = 1234;
        
        bankAccount.VAT_Number_or_UTR_number__c = 'tttt1231';
        
        bankAccountLst.add(bankAccount);
        
        Bank_Account__c bankAccountNegative = new Bank_Account__c();
        bankAccountNegative.recordTypeId = bkaccRtId;
        bankAccountNegative.Bank_Account_Number_or_IBAN__c = 'abc';
        bankAccountNegative.Bank_Account_Ownership__c  = 'contactNegative.id;';
        bankAccountNegative.Bank_Name__c = 'BradescoFake';
        bankAccountNegative.BIC_or_Routing_Number_or_Sort_Code__c = 'abc';
        bankAccountNegative.Routing_Number__c = 1234;
        bankAccountNegative.VAT_Number_or_UTR_number__c = 'VVVT1233';
        
        bankAccountLst.add(bankAccountNegative);
        
        INSERT bankAccountLst;
        
        
        Id abrRtId = Schema.SObjectType.Acount_Bank_Account_Relationship__c.getRecordTypeInfosByDeveloperName().get('Gyms_Account_Bank_Account_Partner').getRecordTypeId();
        List<Acount_Bank_Account_Relationship__c> abrLst = new List<Acount_Bank_Account_Relationship__c>();
        Acount_Bank_Account_Relationship__c abr = new Acount_Bank_Account_Relationship__c();
        abr.RecordTypeId = abrRtId;
        abr.Account__c = acc.Id;
        abr.Bank_Account__c = bankAccount.id;
        abrLst.add(abr);
        
        Acount_Bank_Account_Relationship__c abrNegative = new Acount_Bank_Account_Relationship__c();
        abrNegative.RecordTypeId = abrRtId;
        abrNegative.Account__c = accNegative.Id;
        abrNegative.Bank_Account__c = bankAccountNegative.id;
        abrLst.add(abrNegative);   
        
        INSERT abrLst;
        
        /** Payment */
        Id payRtId = Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Gyms_Payment').getRecordTypeId();
        List<Payment__c> payLst = new List<Payment__c>();
        
        Payment__c payment = new Payment__c();
        payment.recordTypeId = payRtId;
        payment.Payment_Start_Date__c = Date.today();
        payment.Payment_Method__c = 'TED';
        payment.Payment_Frequency__c = 'Every_3_months';  
        
        payLst.add(payment);
        
        Payment__c paymentNegative = new Payment__c();
        paymentNegative.recordTypeId = payRtId;
        paymentNegative.Payment_Start_Date__c = Date.today();
        paymentNegative.Payment_Method__c = 'TED';
        paymentNegative.Payment_Frequency__c = 'Every_3_months';  
        
        payLst.add(paymentNegative);
        
        INSERT payLst;
        
        /** Proposals/Quote */
        Id propRtId = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Gyms_Quote_Partner').getRecordTypeId();
        List<Quote> propLst = new List<Quote>();
        
        Quote proposal = new Quote();
        proposal.recordTypeId= propRtId;
        proposal.Name = 'academiaBrasilCompanyQuote';
        proposal.Status = 'Ganho';
        proposal.Signed__c = Date.today();
        proposal.Start_Date__c  = Date.today();
        proposal.Final_Date__c =  Date.today().addYears(4);
        proposal.Priority_Tier__c = 'Tier 1';
        proposal.PR_Activity__c  = 'Yes';
        proposal.discounts_usage_volume_range__c = 'Yes';
        proposal.Volume_Discount_Type__c = 'By single user volume';
        proposal.OpportunityId = accOpp.id;  
        proposal.Threshold_Value_1__c= 50;
        proposal.Threshold_Value_2__c= 100;
        proposal.Threshold_Value_3__c= 150;
        proposal.Threshold_Value_4__c= 200;
        proposal.First_Discount_Range__c=5;
        proposal.Second_Discount_Range__c=10;
        proposal.Third_Discount_Range__c=15;  
        proposal.Fourth_Discount_Range__c=20;  
        propLst.add(proposal);
        
        Quote proposalNegative = new Quote();
        proposalNegative.recordTypeId= propRtId;
        proposalNegative.Name = 'proposalNegative';
        proposalNegative.Status = 'Ganho';
        proposalNegative.Signed__c = Date.today();
        proposalNegative.Start_Date__c  = Date.today();
        proposalNegative.Final_Date__c =  Date.today().addYears(1);
        proposalNegative.Priority_Tier__c = 'Tier 1';
        proposalNegative.PR_Activity__c  = 'No';        
        proposalNegative.discounts_usage_volume_range__c = 'No';
        proposalNegative.Threshold_Value_1__c = 12;
        proposalNegative.Threshold_Value_2__c = 32;                         
        proposalNegative.Threshold_Value_3__c = 42;                
        proposalNegative.Threshold_Value_4__c = 48;                               
        proposalNegative.First_Discount_Range__c = 20;
        proposalNegative.Second_Discount_Range__c = 24; 
        proposalNegative.Third_Discount_Range__c = 56;
        proposalNegative.Fourth_Discount_Range__c = 70;
        proposalNegative.Volume_Discount_Type__c = 'By single user volume';
        proposalNegative.OpportunityId = negativeOpp.id; 
        
        propLst.add(proposalNegative);
        
        INSERT propLst;        
        
        /** Plans/Products */     
        
        List<Product_Item__c> prodLst = new List<Product_Item__c>();
        Id prodRtId = Schema.SObjectType.Product_Item__c.getRecordTypeInfosByDeveloperName().get('Gyms_Product_Item_Partner').getRecordTypeId();
        
        Product_Item__c prod = new Product_Item__c();    
        prod.recordTypeId = prodRtId;
        prod.Product_Type__c = 'In person';
        prod.Is_Network_CAP__c = 'No';
        prod.CAP_Value__c = 100;
        prod.CurrencyIsoCode = 'BRL';
        prod.Do_You_Have_A_No_Show_Fee__c = 'No';
        prod.Has_Late_Cancellation_Fee__c = 'No';
        prod.Late_Cancellation_Percent__c = '0';
        prod.Maximum_Live_Class_Per_Month__c = 0;
        prod.No_Show_Fee_Percent__c = '0';
        prod.Product_Definition__c  ='Dança';
        prod.Product_Restriction__c = 'None';
        prod.Name = 'Dança';
        prod.Opportunity__c = accOpp.Id;
        prod.Max_Monthly_Visit_Per_User__c = 12;
        prod.Reference_Price_Value_Unlimited__c = 400;
        prod.Net_Transfer_Price__c  = 1; //7
        prod.Price_Visits_Month_Package_Selected__c = 400;
        
        prodLst.add(prod);
        
        Product_Item__c prodNegative = new Product_Item__c();    
        prodNegative.recordTypeId = prodRtId;
        prodNegative.Product_Type__c = 'In person';
        prodNegative.CurrencyIsoCode = 'BRL';
        prodNegative.CAP_Value__c = 300;
        prodNegative.Do_You_Have_A_No_Show_Fee__c = 'No';
        prodNegative.Has_Late_Cancellation_Fee__c = 'No';
        prodNegative.Late_Cancellation_Percent__c = '0';
        prodNegative.Maximum_Live_Class_Per_Month__c = 0;
        prodNegative.No_Show_Fee_Percent__c = '0';
        prodNegative.Product_Definition__c  ='Forro';
        prodNegative.Product_Restriction__c = 'None';
        prodNegative.Name = 'Forro';
        prodNegative.Opportunity__c = negativeOpp.Id;
        prodNegative.Max_Monthly_Visit_Per_User__c = 1;
        prodNegative.Reference_Price_Value_Unlimited__c = 180;
        prodNegative.Net_Transfer_Price__c  = 30;
        prodNegative.Price_Visits_Month_Package_Selected__c = 180;
        prodLst.add(prodNegative);
        
        INSERT prodLst;
        
        List<Opportunity> oppToUpdate = new List<Opportunity>();
        accOpp.Legal_representative__c = contactAcc.id;            
        accOpp.Payment__c = payment.id; 
        negativeOpp.Legal_representative__c = contactNegative.id;             
        negativeOpp.Payment__c = paymentNegative.id; 
        
        List<Ops_Setup_Validation_Form__c> opsForms = new List<Ops_Setup_Validation_Form__c>();
        Id recordTypePartnerForm = Schema.SObjectType.Ops_Setup_Validation_Form__c.getRecordTypeInfosByDeveloperName().get('Partner_Ops_Setup_Validation').getRecordTypeId();
        Ops_Setup_Validation_Form__c accOppForm = new Ops_Setup_Validation_Form__c (
            recordTypeId = recordTypePartnerForm,
            Name = accOpp.Name + ' - OPS Form',
            Status__c = 'Approved',
            Did_you_send_the_training_email__c = 'Yes',
            Opportunity__c = accOpp.Id );  
        opsForms.add(accOppForm);
        
        Ops_Setup_Validation_Form__c negativeOppForm = new Ops_Setup_Validation_Form__c (
            recordTypeId = recordTypePartnerForm,
            Name = negativeOpp.Name + ' - OPS Form',
            Status__c = 'Approved',
            Did_you_send_the_training_email__c = 'Yes',
            Opportunity__c = negativeOpp.Id ); 
        
        opsForms.add(negativeOppForm);
        
        INSERT opsForms;
        
        accOpp.StageName = 'Lançado/Ganho';       
        negativeOpp.StageName = 'Lançado/Ganho';
        accOpp.Product_Related__c  = true; 
        negativeOpp.Product_Related__c = true;
        
        UPDATE oppToUpdate;
        

        Pricebook2 standardPricebook = new Pricebook2(
            Id = Test.getStandardPricebookId(),
            IsActive = true
        );
        update standardPricebook;
        Pricebook2 pb = standardPricebook;                      
        Product2 accessFee = generateProduct('Enterprise Subscription');
        insert accessFee;
        PricebookEntry accessFeeEntry = generatePricebookEntry(pb, accessFee, accOpp);
        insert accessFeeEntry;
        OpportunityLineItem item  = new OpportunityLineItem(OpportunityId=accOpp.Id, PricebookEntryId=accessFeeEntry.Id,Quantity=100,UnitPrice=1);
        insert item;
    }
    
    
    
    @isTest static void responseFromOpportunityTest() {
        
        Opportunity opp = [SELECT id,RecordType.DeveloperName, Number_of_Locations_Opportunity__c, accountId FROM Opportunity WHERE Name = 'academiaBrasilCompanyOpp'];
        Account accChild = [SELECT id FROM Account WHERE Name = 'academiaBrasilCompanyChild1'];      
        
        List<Account_Opportunity_Relationship__c> aorLst = [SELECT Id FROM Account_Opportunity_Relationship__c WHERE Opportunity__c =: opp.id];
        
        if(opp.Number_of_Locations_Opportunity__c != aorLst.size()){
            opp.Number_of_Locations_Opportunity__c = aorLst.size();            
        }
        UPDATE opp;
        
        Integer expectedScore = 10;
        Integer actualScore = 0;
        Test.startTest();
        StepsTowardsSuccessCtrl.Response resp = new StepsTowardsSuccessCtrl.Response();
        resp = StepsTowardsSuccessCtrl.getResponse(opp.Id);
        List<StepsTowardsSuccessCtrl.StepsTowardsSuccessWrapper> lstResponse = resp.StepsTowardsSuccessWrapper;
        for(StepsTowardsSuccessCtrl.StepsTowardsSuccessWrapper r : lstResponse){
            if(r.option == 'positive')
                actualScore++;
            else{
                System.debug('responseFromOpportunityTest');
                System.debug(r.label);
            }
        }             
        System.assertEquals(expectedScore, actualScore, 'responseFromOpportunityTest');
        Test.stopTest();               
    } 
    
    
    @isTest static void responseFromAccountTest() {
        
        Account academiaBrasilCompany = [SELECT id,Name,RecordType.DeveloperName, Number_Of_Locations__c FROM Account WHERE Name = 'AcademiaBrasilCompanyPai'];
        Contact ceoContact = [SELECT Id FROM Contact WHERE Cargo__c = 'CEO' LIMIT 1];
        
        Opportunity opp = [SELECT id,Name, StageName,Number_of_Locations_Opportunity__c, accountId FROM Opportunity WHERE Name = 'academiaBrasilCompanyOpp'];
        Account accChild = [SELECT id, Name FROM Account WHERE Name = 'academiaBrasilCompanyChild1'];  
        opp.Product_Related__c = true;
        opp.StageName = 'Lançado/Ganho';
        opp.Bank_Account_Related__c = true;
        opp.Legal_representative__c = ceoContact.Id;
        opp.Proposal_Related__c = true;
        
        List<Account_Opportunity_Relationship__c> aorLst = [SELECT Id FROM Account_Opportunity_Relationship__c WHERE Opportunity__c =: opp.id];
        
        if(opp.Number_of_Locations_Opportunity__c != aorLst.size()){
            opp.Number_of_Locations_Opportunity__c = aorLst.size();            
        }
        
        //try {
            UPDATE opp;
        //} catch(DmlException e) {
         //   System.debug('The following exception has occurred: ' + e.getMessage());
        //}
        
        System.debug('Account ' + academiaBrasilCompany.Name);
        System.debug('opp ' + opp.Name + ' '+ opp.StageName);
        System.debug('accChild ' + accChild.Name);
        
        Integer expectedScore = 10;
        Integer actualScore = 0;        
        Test.startTest();
        StepsTowardsSuccessCtrl.Response resp = new StepsTowardsSuccessCtrl.Response();
        resp = StepsTowardsSuccessCtrl.getResponse(academiaBrasilCompany.Id);
        List<StepsTowardsSuccessCtrl.StepsTowardsSuccessWrapper> lstResponse = resp.StepsTowardsSuccessWrapper;
        for(StepsTowardsSuccessCtrl.StepsTowardsSuccessWrapper r : lstResponse){
            if(r.option == 'positive')
                actualScore++;
            else{
                System.debug('responseFromAccountTest');
                System.debug(r.label);
            }
        }
        System.assertEquals(expectedScore, actualScore, 'responseFromAccountTest');
        Test.stopTest();               
    }
    
    @isTest static void responseFakeAccountResults() {
        Account fakeAcc = [SELECT id,RecordType.DeveloperName FROM Account WHERE Name = 'fakeAcc'];
        
        Integer expectedScore = 0;
        Integer actualScore = 0; 
        Test.startTest();
        StepsTowardsSuccessCtrl.Response resp = new StepsTowardsSuccessCtrl.Response();
        resp = StepsTowardsSuccessCtrl.getResponse(fakeAcc.Id);
        List<StepsTowardsSuccessCtrl.StepsTowardsSuccessWrapper> lstResponse = resp.StepsTowardsSuccessWrapper;
        for(StepsTowardsSuccessCtrl.StepsTowardsSuccessWrapper r : lstResponse){
            if(r.option == 'positive') {
                actualScore++;
                System.debug('responseFakeAccountResults');
                System.debug(r.label);
            }
        }
        System.assertEquals(expectedScore, actualScore);
        Test.stopTest();               
    } 
    
    
    @isTest static void responseFromNegativeOpportunityTest() {
        Opportunity negativeOpp = [SELECT id,RecordType.DeveloperName,Number_of_Locations_Opportunity__c, AccountId FROM Opportunity WHERE Name = 'negativeOpp'];
        Integer expectedScore = 0;
        Integer actualScore = 0; 
        
        List<Account_Opportunity_Relationship__c> aorLst = [SELECT Id FROM Account_Opportunity_Relationship__c WHERE Opportunity__c =: negativeOpp.id];
        
        if(negativeOpp.Number_of_Locations_Opportunity__c != aorLst.size()){
            negativeOpp.Number_of_Locations_Opportunity__c = aorLst.size();            
        }
        UPDATE negativeOpp;
        
        
        Test.startTest();
        StepsTowardsSuccessCtrl.Response resp = new StepsTowardsSuccessCtrl.Response();
        resp = StepsTowardsSuccessCtrl.getResponse(negativeOpp.Id);
        List<StepsTowardsSuccessCtrl.StepsTowardsSuccessWrapper> lstResponse = resp.StepsTowardsSuccessWrapper;
        for(StepsTowardsSuccessCtrl.StepsTowardsSuccessWrapper r : lstResponse){
            if(r.option == 'positive') {
                actualScore++;
                System.debug('responseFromNegativeOpportunityTest');
                System.debug(r.label);
            }
            
        }           
        System.assertEquals(expectedScore, actualScore);
        Test.stopTest();               
    }
    
    
    @isTest static void responseFromNegativeAccountTest() {
        Account accNegative = [SELECT id FROM Account WHERE Name = 'accNegative'];
        Integer expectedScore = 0;
        Integer actualScore = 0;
        
        Opportunity opp = [SELECT id,RecordType.DeveloperName,Number_of_Locations_Opportunity__c, accountId FROM Opportunity WHERE Name = 'negativeOpp'];   
        
        List<Account_Opportunity_Relationship__c> aorLst = [SELECT Id FROM Account_Opportunity_Relationship__c WHERE Opportunity__c =: opp.id];
        
        if(opp.Number_of_Locations_Opportunity__c != aorLst.size()){
            opp.Number_of_Locations_Opportunity__c = aorLst.size();            
        }
        UPDATE opp;
        
        Test.startTest();
        StepsTowardsSuccessCtrl.Response resp = new StepsTowardsSuccessCtrl.Response();
        resp = StepsTowardsSuccessCtrl.getResponse(accNegative.Id);
        List<StepsTowardsSuccessCtrl.StepsTowardsSuccessWrapper> lstResponse = resp.StepsTowardsSuccessWrapper;
        for(StepsTowardsSuccessCtrl.StepsTowardsSuccessWrapper r : lstResponse){
            if(r.option == 'positive') {
                actualScore++;
                System.debug('responseFromNegativeAccountTest');
                System.debug(r.label);
            }
        }
        System.assertEquals(expectedScore, actualScore);
        System.assertEquals(expectedScore, actualScore);
        Test.stopTest();               
    }

    private static Pricebook2 generatePricebook(){        
        Pricebook2 pb = new Pricebook2();
        pb.Name = 'Br';
        pb.Country__c = 'Brazil';
        pb.IsActive = true;
        return pb;
    }

    private static Product2 generateProduct(String ProductName){
        Product2 product = new Product2();
        product.Name = ProductName;
        product.Minimum_Number_of_Employees__c = 0;
        product.Maximum_Number_of_Employees__c = 900;
        product.Family = ProductName;
        product.IsActive = true;
        return product;
    }

    private static PricebookEntry generatePricebookEntry(Pricebook2 pb, Product2 product, Opportunity opp){        
        PricebookEntry pbEntry = new PricebookEntry();
        pbEntry.Product2Id = product.Id;
        pbEntry.Pricebook2Id = Test.getStandardPricebookId();
        pbEntry.UnitPrice = 100;
        pbEntry.IsActive = true;
        pbEntry.CurrencyIsoCode = opp.CurrencyIsoCode;
        return pbEntry;
    }
    
    
}