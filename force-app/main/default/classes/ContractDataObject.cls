/**
* @description       : Part of Contract data (amendment) process - holds previous data and actual data to compare and log to be used in Conga contract
* author            : Laerte Kimura - leka@gft.com
* @group             : 
* @last modified on  : 19-05-2021
* @last modified by  : Laerte Kimura - leka@gft.com
* Modifications Log 
* Ver   Date         Author                         Modification
* 1.0   19-05-2021   Laerte Kimura - leka@gft.com   Created 
* 1.1   01-06-2021   Laerte Kimura - leka@gft.com   Removed waivers - no longer needed 
* 1.2   06-08-2021   Laerte Kimura - leka@gft.com   Added autonomous_marketplace_contract__c,pr_in_contract__c,exclusivity_clause__c
**/

public class ContractDataObject{
    
    public class ContractDataObjectException extends Exception {}
    
    public id ContractId;
    public id ParentContractId;
    
    public id OpportunityId;
    public id ParentOpportunityId;
    
    public id MainPaymentId;
    public id ParentPaymentId;
    
    public id QuoteId;  
    public id ParentQuoteId;
    
    public List<Opportunity> OpportunityList;
    public List<Quote> ProposalList;
    public List<QuoteLineItem> EnterpriseSubscriptionList;
    public List<Payment__c> PaymentList;
    public List<Eligibility__c> EligibilityList;
    
    public List<Assert_Data__c> ContractDataRecords;
    
    
    /* Contructor */
    public ContractDataObject(){
        
        OpportunityList = new List<Opportunity>();
        ProposalList = new List<Quote>();
        EnterpriseSubscriptionList = new List<QuoteLineItem>();
        PaymentList = new List<Payment__c>();
        EligibilityList = new List<Eligibility__c>();
        ContractDataRecords = new List<Assert_Data__c>(); /* Holds the Assert_Data__c to be saved or updated */
    }
    
    public List<Assert_Data__c> getAllContractDataRecords(){
        return this.ContractDataRecords;
    }
    
    public void createRecord(Map<string, List<String>> MapObjectAndFieldsToTrack){
        for (Payment__c payment: this.PaymentList){
            if (payment.Quote_Line_Item__r.QuoteId == this.QuoteId){
                This.MainPaymentId = payment.id;
            }else {
                this.ParentPaymentId = payment.id;
            }
        }
        /* After received a list of objects to track, separates each record (old and new), and compare the value of mapped fields if changed */
        for(string objectName : MapObjectAndFieldsToTrack.keyset() ){
            switch on objectName.toLowerCase(){
                when 'quote' {
                    Quote MainQuote = new Quote();
                    Quote ParentQuote = new Quote();
                    List<string> FieldsToQueryList = MapObjectAndFieldsToTrack.get(objectName);
                    for (Quote proposta: this.ProposalList){
                        if (proposta.id == this.QuoteId){
                            MainQuote = proposta;
                        }else {
                            ParentQuote = proposta;
                        }
                    }
                    for(string fieldname: FieldsToQueryList)  {
                        if(MainQuote.get(fieldname) != ParentQuote.get(fieldname)){
                            this.addContractDataObject( objectName.toLowerCase(),fieldname ,String.valueOf(ParentQuote.get(fieldname)),  String.valueOf(MainQuote.get(fieldname)), '' ); 
                        }
                        
                    }
                    
                }
                
                when 'opportunity'{
                    Opportunity MainOpportunity = new Opportunity();
                    Opportunity ParentOpportunity = new Opportunity();
                    List<string> FieldsToQueryList = MapObjectAndFieldsToTrack.get(objectName);
                    for (Opportunity opportunity: this.OpportunityList){
                        if (opportunity.id == this.OpportunityId){
                            MainOpportunity = opportunity;
                        }else {
                            ParentOpportunity = opportunity;
                        }
                    }
                    for(string fieldname: FieldsToQueryList)  {
                        
                        if(MainOpportunity.get(fieldname) != ParentOpportunity.get(fieldname)){
                            this.addContractDataObject( objectName.toLowerCase(), fieldname ,String.valueOf(ParentOpportunity.get(fieldname)),  String.valueOf(MainOpportunity.get(fieldname)), '' );                  
                        }
                        
                    }
                }
                
                when 'quotelineitem'{
                    Quotelineitem MainEnterpriseSubscrition = new Quotelineitem();
                    Quotelineitem ParentEnterpriseSubscrition = new Quotelineitem();
                    List<string> FieldsToQueryList = MapObjectAndFieldsToTrack.get(objectName);
                    for (Quotelineitem enterpriseSubscrition: this.EnterpriseSubscriptionList){
                        if (enterpriseSubscrition.QuoteId == this.QuoteId){
                            MainEnterpriseSubscrition = enterpriseSubscrition;
                        }else {
                            ParentEnterpriseSubscrition = enterpriseSubscrition;
                        }
                    }
                    for(string fieldname: FieldsToQueryList)  {
                        if(MainEnterpriseSubscrition.get(fieldname) != ParentEnterpriseSubscrition.get(fieldname)){
                            this.addContractDataObject( objectName.toLowerCase(),fieldname ,String.valueOf(ParentEnterpriseSubscrition.get(fieldname)),  String.valueOf(MainEnterpriseSubscrition.get(fieldname)), 'Enterprise Subscription' );                            
                        }
                        
                    }
                }
                when 'payment__c'{
                    Payment__c MainPayment = new Payment__c();
                    Payment__c ParentPayment = new Payment__c();
                    List<string> FieldsToQueryList = MapObjectAndFieldsToTrack.get(objectName);
                    for (Payment__c payment: this.PaymentList){
                        if (payment.Quote_Line_Item__r.QuoteId == this.QuoteId){
                            MainPayment = payment;
                        }else {
                            ParentPayment = payment;
                        }
                    }
                    for(string fieldname: FieldsToQueryList)  {
                        if(MainPayment.get(fieldname) != ParentPayment.get(fieldname)){
                            this.addContractDataObject( objectName.toLowerCase(),fieldname ,String.valueOf(ParentPayment.get(fieldname)),  String.valueOf(MainPayment.get(fieldname)),'' );
                        }
                        
                    }
                }
                when 'eligibility__c'{
                    Eligibility__c MainEligibility = new Eligibility__c();
                    Eligibility__c ParentEligibility = new Eligibility__c();
                    List<string> FieldsToQueryList = MapObjectAndFieldsToTrack.get(objectName);
                    for (Eligibility__c eligibility: this.EligibilityList){
                        system.debug('MainPaymentId : ' + MainPaymentId);
                        if (eligibility.payment__c == this.MainPaymentId){
                            MainEligibility = eligibility;
                        }else {
                            ParentEligibility = eligibility;
                        }
                    }
                    for(string fieldname: FieldsToQueryList)  {
                        if(MainEligibility.get(fieldname) != ParentEligibility.get(fieldname)){
                            this.addContractDataObject( objectName.toLowerCase(),fieldname ,String.valueOf(ParentEligibility.get(fieldname)),  String.valueOf(MainEligibility.get(fieldname)), '' );
                        }
                        
                    }
                }
            }
            
        }
    }
    
   private void addContractDataObject(string objectName, string fieldName, string oldValue, string newValue, string feeType){
        /* Update the old value and new value if the contract data exists otherwise add the record to the list */
        try {
            /* Rule for Free product - only record if the change was from NO to YES */
            if ((objectName == 'quote') && (fieldName =='will_this_company_have_the_free_product__c') && (oldValue == 'Yes') && (newValue == 'No')){
                return;
            }
            /* Rule for Contact Permission - only record if the change is from ANY to Allowlist */
            if ((objectName == 'quote') && (fieldName =='employee_corporate_email__c') && (oldValue == 'true') && (newValue == 'false')){
                return;
            }
             /* Rule for Exclusivity - only record if the change is from false to true */
             if ((objectName == 'quote') && (fieldName =='exclusivity_clause__c') && (oldValue == 'true') && (newValue == 'false')){
                return;
            }
             /* Rule for Press release - only record if the change is from false to true */
             if ((objectName == 'quote') && (fieldName =='pr_in_contract__c') && (oldValue == 'true') && (newValue == 'false')){
                return;
            }
            /* Rule for Membership Fee Condition - only record if the change is from No to Yes */
             if ((objectName == 'quote') && (fieldName =='autonomous_marketplace_contract__c') && (oldValue == 'Yes') && (newValue == 'No')){
                return;
             }
            Boolean isFound =false;
            for (Assert_Data__c contractData : this.ContractDataRecords){
                if (objectName == 'quotelineitem'){
                    if ((contractData.Object_Name__c == objectName.toLowerCase()) 
                        && (contractData.Field_Name__c == fieldname)
                        && (contractData.Fee_Type__c == feeType)   
                       ){
                           //system.debug('new field found to update => ' + objectName + ' ' + fieldName);
                           contractData.Old_Value__c =  (oldValue);
                           contractData.New_Value__c =  (newValue);
                           contractData.Fee_Type__c  =  (feeType);
                           isFound = true;
                           break;
                       }}else
                       {
                           if ((contractData.Object_Name__c == objectName.toLowerCase()) 
                               && (contractData.Field_Name__c == fieldname)){
                                  //system.debug('new field found to update => ' + objectName + ' ' + fieldName);
                                  contractData.Old_Value__c =  (oldValue);
                                  contractData.New_Value__c =  (newValue);
                                  contractData.Fee_Type__c  =  (feeType);
                                  isFound = true;
                                  break;
                              }
                       }}
                if (!isFound){
                    Assert_Data__c newContractDataRecord = new Assert_Data__c();
                    newContractDataRecord.Opportunity__c = this.OpportunityId;
                    newContractDataRecord.Object_Name__c = objectName.toLowerCase();
                    newContractDataRecord.Field_Name__c = fieldname;
                    newContractDataRecord.Old_Value__c =  (oldValue);
                    newContractDataRecord.New_Value__c =  (newValue);
                    newContractDataRecord.Fee_Type__c  =  (feeType);
                    //system.debug('new field  => ' + objectName + ' ' + fieldName);
                    this.ContractDataRecords.add(newContractDataRecord);
                }
            }
            catch (ContractDataObjectException cEx){
                throw new ContractDataObjectException('Unable to Add Contract Data records in Contract Data List in Opportunity Id ' + OpportunityId);
            }     
        }
        
}