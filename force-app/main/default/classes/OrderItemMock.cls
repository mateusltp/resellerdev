@IsTest
public class OrderItemMock {
  public static OrderItem getStandard(Order order, QuoteLineItem quoteLineItem) {
    return new OrderItem(
      OrderId = order.Id,
      ListPrice = quoteLineItem.ListPrice,
      PricebookEntryId = quoteLineItem.PricebookEntryId,
      Product2Id = quoteLineItem.Product2Id,
      Quantity = quoteLineItem.Quantity,
      QuoteLineItemId = quoteLineItem.Id,
      UnitPrice = quoteLineItem.Discount != null
        ? quoteLineItem.UnitPrice * (1 - quoteLineItem.Discount / 100)
        : quoteLineItem.UnitPrice,
      Contract_Type__c = quoteLineItem.Fee_Contract_Type__c,
      Discount__c = quoteLineItem.Discount,
      Discount_Price_Ceiling__c = quoteLineItem.Discount_Price_Ceiling__c,
      Discount_Price_Floor__c = quoteLineItem.Discount_Price_Floor__c,
      Discount_Price_Unit_Decrease__c = quoteLineItem.Discount_Price_Unit_Decrease__c,
      Flat_Baseline_Adjustment__c = quoteLineItem.Flat_Baseline_Adjustment__c,
      Flat_Baseline_Quantity__c = quoteLineItem.Quantity,
      Inflation_Adjustment_Index__c = quoteLineItem.Inflation_Adjustment_Index__c,
      Inflation_Adjustment_Period__c = quoteLineItem.Inflation_Adjustment_Period__c,
      Recurring_Billing_Period__c = quoteLineItem.Recurring_Billing_Period__c,
      Variable_Price_Ceiling__c = quoteLineItem.Variable_Price_Ceiling__c,
      Variable_Price_Floor__c = quoteLineItem.Variable_Price_Floor__c,
      Variable_Price_Unit_Increase__c = quoteLineItem.Variable_Price_Unit_Increase__c,
      Type__c = quoteLineItem.Fee_Type__c
    );
  }
}