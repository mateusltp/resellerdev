public without sharing class SmbSkuSaveProductsController {
    
    @AuraEnabled
    public static List<QuoteLineItem> saveProducts(List<QuoteLineItem> products){
        try {

            insert products;

            List<QuoteLineItem> resultProducts = [SELECT Id, Plan_Name__c, Subtotal, Discount, CurrencyIsoCode, Allow_Waivers__c, Product2Id,
                                                Quantity, List_Price__c, Total_List_Price__c, UnitPrice, Product_Fee_type__c
                                                FROM QuoteLineItem 
                                                WHERE Id IN :products];

            return resultProducts;
            
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

}