/**
 * @description       : Engagement Journey Form Repository 
 * @author            : AlyssonMota
 * @group             : 
 * @last modified on  : 06-11-2021
 * @last modified by  : roei@gft.com
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   11-12-2020   Alysson Mota   Initial Version
**/
public with sharing class FormRepository {
    public FormRepository() {
    }

    public Map<Id, Form__c> getM0ForOppsAsMapOppIdToForm(List<Id> oppIdList) {
        Map<Id, Form__c> oppIdToForm = new Map<Id, Form__c>();
        List<Form__c> m0FormIdList = getM0ForOppsWithIdInTheList(oppIdList);

        for (Form__c form : m0FormIdList) {
            oppIdToForm.put(form.Opportunity__c, form);
        }
        return oppIdToForm;
    }

    public List<Form__c> getM0ForOppsWithIdInTheList(List<Id> oppIdList) {
        Id m0recordTypeId = Schema.SObjectType.Form__c.getRecordTypeInfosByDeveloperName()
            .get('M0_12_Steps').getRecordTypeId();
        
        //-- record type for reseller channel
        Id resellerM0recordTypeId = Schema.SObjectType.Form__c.getRecordTypeInfosByDeveloperName()
            .get('RESELLER_Engagement_Journey_M0').getRecordTypeId();

        List<Form__c> m0FormList = [
            SELECT Id, Opportunity__c,Client_Sales_Executive__c	,
            Standard_Membership_Fee_GL_justify__c,
            Expansion_100_of_the_eligible_GL_justify__c,
            Minimum_Access_Fee_GL_justify__c,
            Engagement_with_C_Level_GL_justify__c,
            Payroll_GL_justify__c,
            Database_Email_GL_justify__c,
            Whitelist_GL_justify__c,
            New_Hires_GL_justify__c,
            HR_Communication_GL_justify__c,
            Exclusivity_clause_GL_justify__c,
            PR_in_contract_GL_justify__c,
            What_does_success_looks_like_GL_justify__c,
            Approval_Status__c,
            SMB_CS_success_executive__c,
            Form_Status__c,
            Client_Success_Approval__c,
            Head_of_client_success__c,
            Country_Manager__c,
            Client_Success_Executive__c,
            Additional_Information__c,
            Internal_Communication_Responsible__c
            FROM Form__c
            WHERE (RecordTypeId = :m0recordTypeId OR RecordTypeId = :resellerM0recordTypeId)
        ];

        return m0FormList;
    }

    public Map<Id, Form__c> getM1ForOppsAsMapOppIdToForm(List<Id> oppIdList) {
        Map<Id, Form__c> oppIdToForm = new Map<Id, Form__c>();
        List<Form__c> m1FormIdList = getM1ForOppsWithIdInTheList(oppIdList);

        for (Form__c form : m1FormIdList) {
            oppIdToForm.put(form.Opportunity__c, form);
        }
        return oppIdToForm;
    }

    public List<Form__c> getM1ForOppsWithIdInTheList(List<Id> oppIdList) {
        Id m1recordTypeId = Schema.SObjectType.Form__c.getRecordTypeInfosByDeveloperName()
            .get('M1_12_Steps').getRecordTypeId();
        
        //-- record type for reseller channel
        Id resellerM1recordTypeId = Schema.SObjectType.Form__c.getRecordTypeInfosByDeveloperName()
            .get('RESELLER_Engagement_Journey_M1').getRecordTypeId();

        List<Form__c> m1FormList = [
            SELECT Id, Opportunity__c,Client_Success_Executive__c
            FROM Form__c
            WHERE (RecordTypeId = :m1recordTypeId OR RecordTypeId = :resellerM1recordTypeId)
        ];

        return m1FormList;
    }


    public Form__c byId(Id recordId) {
        return [SELECT Id, 
                Opportunity__c,
                Country_Manager__c,
                Standard_Membership_Fee_GL__c, Standard_Membership_Fee_GL_justify__c,
                Expansion_100_of_the_eligible_GL__c,Expansion_100_of_the_eligible_GL_justify__c,
                Minimum_Access_Fee_GL__c, Minimum_Access_Fee_GL_justify__c,
                Engagement_with_C_Level_GL__c, Engagement_with_C_Level_GL_justify__c,
                Payroll_GL__c, Payroll_GL_justify__c,
                Database_Email_GL__c, Database_Email_GL_justify__c,
                Whitelist_GL__c, Whitelist_GL_justify__c,
                New_Hires_GL__c, New_Hires_GL_justify__c,
                HR_Communication_GL__c, HR_Communication_GL_justify__c,
                Exclusivity_clause_GL__c, Exclusivity_clause_GL_justify__c,
                PR_in_contract_GL__c, PR_in_contract_GL_justify__c,
                What_does_success_looks_like_GL_justify__c, What_does_success_looks_like_Joint_BP_GL__c,
                Approval_Status__c, Form_Status__c,
                SMB_CS_success_executive__c, Client_Success_Approval__c,
                Head_of_client_success__c,
                Client_Success_Executive__c,
                Opportunity__r.Account.BillingCountry,
                Additional_Information__c,
                Internal_Communication_Responsible__c
                FROM Form__c WHERE Id =: recordId];
    }

    @AuraEnabled
    public static String getRecordTypeNameByRecordId(ID recordId){
        Form__c m = [SELECT RecordType.Name FROM Form__c WHERE ID=:recordId];
        return m.RecordType.Name;
    }

    public Form__c byIdM0WithRequiredFields(Id recordId){

        return [SELECT Id, Number_of_FTEs_in_the_contract__c,Where_does_the_c_level_sit__c,
                Registration_type__c,How_engaged_are_they__c,Do_you_have_the_list_of_FTEs_per_unit__c,
                Contract_automated_renovation__c,Automatic_increase_in_ES_at_renewal__c,
                Engagement_with_C_Level_GL__c,Interacted_with_the_HR_director__c,
                Where_does_the_HR_Director_sit__c,Please_confirm_their_contacts_c_level__c,
                Please_confirm_their_contact_info_HR__c,where_does_the_main_contact_sit__c,
                Please_confirm_our_focal_point_contactHR__c,Responsible_for_signing_the_contract__c,
                Biggest_challenge_and_or_difficulties__c,Why_are_they_implementing_Gympass__c, Approval_Status__c,
                SMB_CS_success_executive__c, Form_Status__c, Client_Success_Approval__c,
                Head_of_client_success__c,
                Country_Manager__c,
                Client_Success_Executive__c,
                Additional_Information__c,
                Internal_Communication_Responsible__c
                FROM Form__c 
                WHERE Id =: recordId];
        
    }

    public List<Form__c> byOppsIdM0WithRequiredFields(List<Id> oppsRecordId){
        Id m0recordTypeId = Schema.SObjectType.Form__c.getRecordTypeInfosByDeveloperName()
            .get('M0_12_Steps').getRecordTypeId();
        
        //-- record type for reseller channel
        Id resellerM0recordTypeId = Schema.SObjectType.Form__c.getRecordTypeInfosByDeveloperName()
            .get('RESELLER_Engagement_Journey_M0').getRecordTypeId();

        return [SELECT Id, Number_of_FTEs_in_the_contract__c,Where_does_the_c_level_sit__c,
                Registration_type__c,How_engaged_are_they__c,Do_you_have_the_list_of_FTEs_per_unit__c,
                Contract_automated_renovation__c,Automatic_increase_in_ES_at_renewal__c,
                Engagement_with_C_Level_GL__c,Interacted_with_the_HR_director__c,
                Where_does_the_HR_Director_sit__c,Please_confirm_their_contacts_c_level__c,
                Please_confirm_their_contact_info_HR__c,where_does_the_main_contact_sit__c,
                Please_confirm_our_focal_point_contactHR__c,Responsible_for_signing_the_contract__c,
                Biggest_challenge_and_or_difficulties__c,Why_are_they_implementing_Gympass__c, Approval_Status__c,
                SMB_CS_success_executive__c, Form_Status__c, Client_Success_Approval__c, Opportunity__c,
                Head_of_client_success__c,
                Country_Manager__c,
                Client_Success_Executive__c,
                Additional_Information__c,
                Internal_Communication_Responsible__c
                FROM Form__c 
                WHERE Opportunity__c =: oppsRecordId
                    AND (RecordTypeId =: m0recordTypeId OR RecordTypeId = :resellerM0recordTypeId)];
    }

    public Map<Id, Form__c> getOppIdToM0Map(List<Id> oppsRecordId) {
        Map<Id, Form__c> oppIdToM0Map = new Map<Id, Form__c>(); 
        List<Form__c> m0List = byOppsIdM0WithRequiredFields(oppsRecordId);
        
        for (Form__c m0 : m0List) {
            oppIdToM0Map.put(m0.Opportunity__c, m0);
        }

        return oppIdToM0Map;
    }
}