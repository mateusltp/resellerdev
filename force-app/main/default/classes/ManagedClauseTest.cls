@istest
public class ManagedClauseTest {
    
    @testSetup 
    static void setupClauses() {
        
        String litBrazil = 'BRAZIL';
        String litB2B    = 'B2B';
        
        List<Clause__c> ClauseList =  new List<Clause__c>();

        Clause__c clause1 = new Clause__c();
        clause1.Name = 'BR 1.1 - Definitions';
        clause1.Text__c = 'Texto clausula BR 1.1';
        clause1.Category__c = 'DEFINITIONS';
        clause1.Country__c = litBrazil;
        clause1.Type__c = litB2B;
        clause1.Order__c = 1001;

        insert clause1;
        ClauseList.add(clause1);

        Clause__c clause2 = new Clause__c();
        clause2.Name = 'BR 1.2 - Definitions';
        clause2.Text__c = 'Texto clausula BR 1.2';
        clause2.Category__c = 'DEFINITIONS';
        clause2.Country__c = litBrazil;
        clause2.Type__c = litB2B;
        clause2.Order__c = 1002;

        insert clause2;
        ClauseList.add(clause2);

        Clause__c clause3 = new Clause__c();
        clause3.Name = 'BR 1.3 - Definitions';
        clause3.Text__c = 'Texto clausula BR 1.3';
        clause3.Category__c = 'DEFINITIONS';
        clause3.Country__c = litBrazil;
        clause3.Type__c = litB2B;
        clause3.Order__c = 1003;

        insert clause3;
        ClauseList.add(clause3);

        Clause__c clause4 = new Clause__c();
        clause4.Name = 'BR 2.1 - Gympass Responsibilities';
        clause4.Text__c = 'Texto clausula BR 2.1';
        clause4.Category__c = 'GYMPASS RESPONSIBILITIES';
        clause4.Country__c = litBrazil;
        clause4.Type__c = litB2B;
        clause4.Order__c = 2001;

        insert clause4;
        ClauseList.add(clause4);


            Subclause__c subclause1 = new Subclause__c();
            subclause1.Name = 'BR 2.1.1 - Gympass Responsibilities';
            subclause1.Text__c = 'Texto clausula BR 2.1.1';
            subclause1.Order__c = 1000;
            subclause1.clause__c = clause4.id;


            insert subclause1;

                subSubclause__c subsubclause1 = new subSubclause__c();
                subsubclause1.Name = 'BR 2.1.1.1 - Gympass Responsibilities';
                subsubclause1.Text__c = 'Texto clausula BR 2.1.1.1';
                subsubclause1.Order__c = 1000;
                subsubclause1.subclause__c = subclause1.id;


                insert subsubclause1;

                subSubclause__c subsubclause2 = new subSubclause__c();
                subsubclause2.Name = 'BR 2.1.1.2 - Gympass Responsibilities';
                subsubclause2.Text__c = 'Texto clausula BR 2.1.1.2';
                subsubclause2.Order__c = 2000;
                subsubclause2.subclause__c = subclause1.id;


                insert subsubclause2;

                subSubclause__c subsubclause3 = new subSubclause__c();
                subsubclause3.Name = 'BR 2.1.1.2 - Gympass Responsibilities';
                subsubclause3.Text__c = '';
                subsubclause3.Order__c = 3000;
                subsubclause3.subclause__c = subclause1.id;


                insert subsubclause3;

            Subclause__c subclause2 = new Subclause__c();
            subclause2.Name = 'BR 2.1.2 - Gympass Responsibilities';
            subclause2.Text__c = 'Texto clausula BR 2.1.2';
            subclause2.Order__c = 2000;
            subclause2.clause__c = clause4.id;

            insert subclause2;
            

        Clause__c clause5 = new Clause__c();
        clause5.Name = 'BR 2.2 - Gympass Responsibilities';
        clause5.Text__c = 'Texto clausula BR 2.2';
        clause5.Category__c = 'GYMPASS RESPONSIBILITIES';
        clause5.Country__c = litBrazil;
        clause5.Type__c = litB2B;
        clause5.Order__c = 2002;

        insert clause5;
        ClauseList.add(clause5);

        Clause__c clause6 = new Clause__c();
        clause6.Name = 'BR 2.3 - Gympass Responsibilities';
        clause6.Text__c = '';
        clause6.Category__c = 'GYMPASS RESPONSIBILITIES';
        clause6.Country__c = litBrazil;
        clause6.Type__c = litB2B;
        clause6.Order__c = 2003;

        insert clause6;
        ClauseList.add(clause6);


        List<swapClause__c> swapListToInsert = new List<swapClause__c>();

        SwapClause__c swap1 = new swapClause__c();
        swap1.Name  =  'BR 2.3 - Gympass Responsibilities Option 1';
        swap1.Text__c = 'Opção 1 para 2.3';
        swap1.clause__c = clause6.id;

        SwapClause__c swap2 = new swapClause__c();
        swap2.Name  =  'BR 2.3 - Gympass Responsibilities Option 2';
        swap2.Text__c = 'Opção 2 para 2.3';
        swap2.clause__c = clause6.id;

        SwapClause__c swap3 = new swapClause__c();
        swap3.Name  =  'BR 2.3 - Gympass Responsibilities Option 3';
        swap3.Text__c = 'Opção 3 para 2.3';
        swap3.clause__c = clause6.id;


        swapListToInsert.add(swap1);
        swapListToInsert.add(swap2);
        swapListToInsert.add(swap3);



        SwapClause__c swap4 = new swapClause__c();
        swap4.Name  =  'BR 2.1.1 - Gympass Responsibilities Option 1';
        swap4.Text__c = 'Opção 1 para 2.1.1';
        swap4.Subclause__c = subclause1.id;

        SwapClause__c swap5 = new swapClause__c();
        swap5.Name  =  'BR 2.1.1 - Gympass Responsibilities Option 2';
        swap5.Text__c = 'Opção 2 para 2.1.1';
        swap5.Subclause__c = subclause1.id;

        swapListToInsert.add(swap4);
        swapListToInsert.add(swap5);

        SwapClause__c swap6 = new swapClause__c();
        swap6.Name  =  'BR 2.1.1.2 - Gympass Responsibilities Option 1';
        swap6.Text__c = 'Opção 1 para 2.1.1.2';
        swap6.SubSubclause__c = subsubclause2.id;

        swapListToInsert.add(swap6);

        
        SwapClause__c swap7 = new swapClause__c();
        swap7.Name  =  'BR 2.1.1.2 - Gympass Responsibilities Option 2';
        swap7.Text__c = 'Opção 2 para 2.1.1.2';
        swap7.SubSubclause__c = subsubclause2.id;

        swapListToInsert.add(swap7);

        Database.insert(swapListToInsert);
      

    }

    @isTest
    static void ManagedClause_addSwapClause(){
        ManagedClause managedClause = new ManagedClause();
        //managedClause.ClauseId = 'a3e1L000000c4EeQAI';
        managedClause.ClauseText = 'Texto standard';
        
        ManagedClause subManagedClause = new ManagedClause();
        //subManagedClause.ClauseId = 'a3e1L000000c4EeQAI';
        subManagedClause.ClauseText = 'Texto Swap';
        
        managedClause.addSwapClause(subManagedClause);
        
        System.assertequals('Texto Swap', (String)managedClause.ClauseText);
        System.assertequals('Texto Swap', (String)managedClause.ClauseText());
    }
    
    @isTest
    static void ManagedClause_ClauseTextWhenAddedASwapAndRemoved(){
        ManagedClause managedClause = new ManagedClause();
        //managedClause.ClauseId = 'a3e1L000000c4EeQAI';
        managedClause.ClauseText = 'Texto standard';
        
        SubManagedClause subManagedClause = new SubManagedClause();
        //subManagedClause.ClauseId = 'a3e1L000000c4EeQAI';
        subManagedClause.ClauseText = 'Texto Swap';
        
        SubManagedClause subManagedClause2 = new SubManagedClause();
        //subManagedClause2.ClauseId = 'a3e1L000000c4EeQAI';
        subManagedClause2.ClauseText = 'Texto Swap 2';
        
        managedClause.addSubClause(subManagedClause);
        managedClause.addSubClause(subManagedClause2);
        
        managedClause.cleanClauseSwap();
        System.assertequals(2, managedClause.getSubClausesLength());
        System.assertequals('Texto standard', managedClause.ClauseText());
        System.assertequals('Texto standard', managedClause.ClauseText);
    }
    
    @isTest
    static void ManagedClause_SortingManagedClauseClass(){
        List<ManagedClause> listManagedClauses = new list<ManagedClause>();
        
        ManagedClause m1 = new ManagedClause();
        //m1.ClauseId = 'a3e1L000000c4EeQAI';
        m1.ClauseText = 'Clausula 9';
        m1.Order = 9;
        
        ManagedClause m2 = new ManagedClause();
        //m2.ClauseId = 'a3e1L000000c4EeQAI';
        m2.ClauseText = 'Clausula 5';
        m2.Order = 5;
        
        ManagedClause m3 = new ManagedClause();
        //m3.ClauseId = 'a3e1L000000c4EeQAI';
        m3.ClauseText = 'Clausula 20';
        m3.Order = 20;
        
        ManagedClause m4 = new ManagedClause();
        //m4.ClauseId = 'a3e1L000000c4EeQAI';
        m4.ClauseText = 'Clausula 2';
        m4.Order = 2;
        
        listManagedClauses.add(m1);
        listManagedClauses.add(m2);
        
        listManagedClauses.sort();
        System.assertequals('Clausula 5',listManagedClauses.get(0).ClauseText);
        System.assertequals('Clausula 9',listManagedClauses.get(1).ClauseText);
        
        listManagedClauses.add(m3);
        listManagedClauses.add(m4);
        
        listManagedClauses.sort();
        
        
        System.assertequals('Clausula 2',listManagedClauses.get(0).ClauseText);
        System.assertequals('Clausula 5',listManagedClauses.get(1).ClauseText);
        System.assertequals('Clausula 9',listManagedClauses.get(2).ClauseText);
        System.assertequals('Clausula 20',listManagedClauses.get(3).ClauseText);
        
    }
    
    @istest
    static void ManagedClause_SubClauseTextWithSubSubClauseIn(){
        ManagedClause managedClause = new ManagedClause();
        //managedClause.ClauseId = 'a3e1L000000c4EeQAI';
        managedClause.ClauseText = 'Texto standard';
        
        
        SubManagedClause subsubmanagedClause1 = new SubManagedClause();
        //subsubmanagedClause1.ClauseId = 'a3e1L000000c4EeQAI';
        subsubmanagedClause1.ClauseText = 'Texto da subsub clause 2';
        subsubmanagedClause1.Order = 2;
        
        SubManagedClause subsubmanagedClause2 = new SubManagedClause();
        //subsubmanagedClause2.ClauseId = 'a3e1L000000c4EeQAI';
        subsubmanagedClause2.ClauseText = 'Texto da subsub clause 1';
        subsubmanagedClause2.Order = 1;
        
        
        SubManagedClause subManagedClause1 = new SubManagedClause();
        //subManagedClause1.ClauseId = 'a3e1L000000c4EeQAI';
        subManagedClause1.ClauseText = 'Texto SUBCLAUSE 14';
        subManagedClause1.Order = 14;
        
        SubManagedClause subManagedClause2 = new SubManagedClause();
        //subManagedClause2.ClauseId = 'a3e1L000000c4EeQAI';
        subManagedClause2.ClauseText = 'Texto SUBCLAUSE 9';
        subManagedClause2.Order = 9;
        
        subManagedClause2.addSubClause(subsubmanagedClause2);
        subManagedClause2.addSubClause(subsubmanagedClause1);
        
        managedClause.addSubClause(subManagedClause2);
        managedClause.addSubClause(subManagedClause1);


        //Testing the addSubClause of list
        ManagedClause managedClause2 = new ManagedClause();
        //managedClause2.ClauseId = 'a3e1L000000c4EeQAF';
        managedClause2.ClauseText = 'Texto standard 2';

        SubManagedClause subManagedClause01 = new SubManagedClause();
        //subManagedClause01.ClauseId = 'a3e1L000000c4EeQAI';
        subManagedClause01.ClauseText = 'Texto SUBCLAUSE 1';
        subManagedClause01.Order = 1 ;

        SubManagedClause subManagedClause02 = new SubManagedClause();
        //subManagedClause02.ClauseId = 'a3e1L000000c4EeQAI';
        subManagedClause02.ClauseText = 'Texto SUBCLAUSE 2';
        subManagedClause02.Order = 2;

        List<SubManagedClause> listOfSubManagedClause = new List<SubManagedClause>{subManagedClause01,subManagedClause02};
        managedClause2.addSubClause(listOfSubManagedClause);

        
        System.assertequals('Texto standard', managedClause.ClauseText);
        System.assertequals('Texto SUBCLAUSE 9Texto da subsub clause 1Texto da subsub clause 2',  managedClause.getSubClauseList().get(0).ClauseText);
        System.assertequals('Texto SUBCLAUSE 14',  managedClause.getSubClauseList().get(1).ClauseText);
    }
    

    
    @isTest
    static void ManageClauseCreator_getDefaultClauses(){
        ManagedClauseCreator mnCreator = new ManagedClauseCreator('Brazil','B2B');
        
        mnCreator.createManagedClauses();
        List<ManagedClause> ClausesList = mnCreator.getManagedClauses();
        
        system.assertNotEquals(0, ClausesList[3].getSubClauseList().size());
        system.assertNotEquals(ClausesList[0], ClausesList[1]);

        system.assertEquals('BR 1.1 - Definitions', ClausesList[0].Name);
        system.assertEquals('BR 1.2 - Definitions', ClausesList[1].Name);
        system.assertEquals('BR 1.3 - Definitions', ClausesList[2].Name);
        system.assertEquals(0, ClausesList[2].getSubClausesLength());

        system.assertEquals('BR 2.1 - Gympass Responsibilities', ClausesList[3].Name);
        system.assertEquals('Texto clausula BR 2.1', ClausesList[3].ClauseText()); // check if getting text
        system.assertEquals('Texto clausula BR 2.1', ClausesList[3].ClauseText); // check if getting text
        system.assertEquals(3, ClausesList[3].getSubClauses()[0].getSubClausesLength()); //check size of subclause
        system.assertEquals(2, ClausesList[3].getSubClauses().size());
        system.assertEquals('BR 2.1.1 - Gympass Responsibilities', ClausesList[3].getSubClauses()[0].Name);
        system.assertEquals('BR 2.1.1.1 - Gympass Responsibilities', ClausesList[3].getSubClauses()[0].getSubClauses()[0].Name); //check level 3
        system.assertEquals(0, ClausesList[3].getSubClauses()[0].getSubClauses()[0].getSubClauses().size()); //no more levels then 3
        system.assertEquals('Texto clausula BR 2.1.1.1', ClausesList[3].getSubClauses()[0].getSubClauses()[0].ClauseText()); // check if getting text
        system.assertEquals('Texto clausula BR 2.1.1.1', ClausesList[3].getSubClauses()[0].getSubClauses()[0].ClauseText); // check if getting text
        system.assertEquals(0, ClausesList[3].getSubClauses()[0].getSubClauses()[0].getSubClausesLength()); //check size of subclause

        system.assertEquals('BR 2.1.1.2 - Gympass Responsibilities', ClausesList[3].getSubClauses()[0].getSubClauses()[1].Name);
        system.assertEquals('BR 2.1.2 - Gympass Responsibilities', ClausesList[3].getSubClauses()[1].Name);

        system.assertEquals('BR 2.2 - Gympass Responsibilities', ClausesList[4].Name);

        system.assertEquals('BR 2.3 - Gympass Responsibilities', ClausesList[5].Name);

        system.assertEquals(6, ClausesList.size()); //check how many clauses was built

      
        
    }

    @istest
    static void ManagedClauseHelper_ManagedClauseTypeWrapper(){
        ManagedClauseHelper.ManagedClauseTypeWrapper type1 = new ManagedClauseHelper.ManagedClauseTypeWrapper();
        type1.TemplateType = 'B2B';
        type1.Country = 'Brazil';


        ManagedClauseHelper.ManagedClauseTypeWrapper type2 = new ManagedClauseHelper.ManagedClauseTypeWrapper();
        type2.TemplateType = 'Gym';
        type2.Country = 'Brazil';

        System.assertNotEquals(type1, type2);
        System.assertNotEquals(type1.hashCode(), type2.hashCode());
        System.assertNotEquals(type1, null);

    }
     
    @isTest
    static void ManagedClauseHelper_findUniqueTypeOfContracts(){

        String litBrazil = 'BRAZIL';
    	
        List<APXT_Redlining__Contract_Agreement__c> ContractList = new List<APXT_Redlining__Contract_Agreement__c>();

        APXT_Redlining__Contract_Agreement__c contrato = new APXT_Redlining__Contract_Agreement__c(); // B2B Order Form
        ///contrato.Id = 'a3e1L000000GIShQAO';
        contrato.Include_signature__c = False ;
        contrato.APXT_Renegotiation__c =False;
        contrato.RecordTypeId = Schema.SObjectType.APXT_Redlining__Contract_Agreement__c.getRecordTypeInfosByDeveloperName().get('B2B_Contract').getRecordTypeId();
        contrato.Include_full_T_C_s__c = true;
        contrato.Template_Selection__c = litBrazil;

        ContractList.add(contrato);

        APXT_Redlining__Contract_Agreement__c contrato2 = new APXT_Redlining__Contract_Agreement__c();// B2B
        //contrato2.Id = 'a3e1L000000GIbtQAG';
        contrato2.Include_signature__c = False ;
        contrato2.APXT_Renegotiation__c =False;
        contrato2.RecordTypeId =Schema.SObjectType.APXT_Redlining__Contract_Agreement__c.getRecordTypeInfosByDeveloperName().get('B2B_Contract').getRecordTypeId();
        contrato.Include_full_T_C_s__c = true;
        contrato2.Template_Selection__c = litBrazil;

        ContractList.add(contrato2);
        APXT_Redlining__Contract_Agreement__c contrato3 = new APXT_Redlining__Contract_Agreement__c();// B2B
        //contrato3.Id = 'a3e1L000000NkIPQA0';
        contrato3.Include_signature__c = False ;
        contrato3.APXT_Renegotiation__c =False;
        contrato3.RecordTypeId = Schema.SObjectType.APXT_Redlining__Contract_Agreement__c.getRecordTypeInfosByDeveloperName().get('B2B_Contract').getRecordTypeId();
        contrato.Include_full_T_C_s__c = true;
        contrato3.Template_Selection__c = litBrazil;

        ContractList.add(contrato3);

        APXT_Redlining__Contract_Agreement__c contrato4 = new APXT_Redlining__Contract_Agreement__c();// B2B
        //contrato4.Id = 'a3e1L000000NkIUQA0';
        contrato4.Include_signature__c = False ;
        contrato4.APXT_Renegotiation__c =False;
        contrato4.RecordTypeId = Schema.SObjectType.APXT_Redlining__Contract_Agreement__c.getRecordTypeInfosByDeveloperName().get('B2B_Contract').getRecordTypeId();
        contrato.Include_full_T_C_s__c = true;
        contrato4.Template_Selection__c = 'USA';

        ContractList.add(contrato4);

        APXT_Redlining__Contract_Agreement__c contrato5 = new APXT_Redlining__Contract_Agreement__c();//Partner
        //contrato5.Id = 'a3e1L000000NkIZQA0';
        contrato5.Include_signature__c = False ;
        contrato5.APXT_Renegotiation__c =False; 
        contrato5.RecordTypeId = Schema.SObjectType.APXT_Redlining__Contract_Agreement__c.getRecordTypeInfosByDeveloperName().get('B2B_Contract').getRecordTypeId();
        contrato.Include_full_T_C_s__c = true;
        contrato5.Template_Selection__c = 'UK';

        ContractList.add(contrato5);

        
        APXT_Redlining__Contract_Agreement__c contrato6 = new APXT_Redlining__Contract_Agreement__c();// B2B
        //contrato6.Id = 'a3e1L000000NkIjQAK';
        contrato6.Include_signature__c = False ;
        contrato6.APXT_Renegotiation__c =False;
        contrato6.RecordTypeId = Schema.SObjectType.APXT_Redlining__Contract_Agreement__c.getRecordTypeInfosByDeveloperName().get('B2B_Contract').getRecordTypeId();
        contrato6.Template_Selection__c = 'Italy';

        ContractList.add(contrato6);
        Formula.recalculateFormulas(ContractList);


        insert ContractList;

        ManagedClauseHelper mnHelper = new ManagedClauseHelper(ContractList);

        //system.assert(mnHelper.ClausesTypes[0] !=  mnHelper.ClausesTypes[1]);

        system.assertEquals('1', string.valueOf(mnHelper.ClausesTypes.size()));

    }



    @istest
    static void ClauseManagementController_getSwapClauses_UsingContractAgreementId_WithSwappedClause(){

        //Create a contract for test 
        APXT_Redlining__Contract_Agreement__c contrato = new APXT_Redlining__Contract_Agreement__c(); 
        contrato.Include_signature__c = False ;
        contrato.APXT_Renegotiation__c =False;
        contrato.RecordTypeId = Schema.SObjectType.APXT_Redlining__Contract_Agreement__c.getRecordTypeInfosByDeveloperName().get('B2B_Contract').getRecordTypeId();
        contrato.Include_full_T_C_s__c = true;
        contrato.Template_Selection__c = 'Brazil';

        insert contrato;

        //Get the contract to use the Id
        APXT_Redlining__Contract_Agreement__c contractAgreement = [SELECT Id, 
                                                                Template_Group__c, 
                                                                Template_Selection__c
                                                                FROM
                                                                APXT_Redlining__Contract_Agreement__c
                                                                LIMIT 1];

        //Get the clauses and the swaps to each one to create the ManagedSwapClause__c                                                              
        Clause__c ClauseItem = [SELECT Id, Name FROM Clause__c WHERE Name ='BR 2.3 - Gympass Responsibilities' LIMIT 1];

        SwapClause__c swapClauseItem = [Select Id, name from SwapClause__c Where Name = 'BR 2.3 - Gympass Responsibilities Option 2' LIMIT 1];

        SubClause__c subClauseItem = [Select Id, Name FROM SubClause__c where Name = 'BR 2.1.1 - Gympass Responsibilities' LIMIT 1];

        SwapClause__c swapSubClauseItem = [Select Id, name from SwapClause__c Where Name = 'BR 2.1.1 - Gympass Responsibilities Option 1' LIMIT 1];

        SubSubClause__c subsubClauseItem = [Select Id, Name FROM SubSubClause__c where Name = 'BR 2.1.1.2 - Gympass Responsibilities' LIMIT 1];

        SwapClause__c swapSubSubClauseItem = [Select Id, name from SwapClause__c Where Name = 'BR 2.1.1.2 - Gympass Responsibilities Option 2' LIMIT 1];
        
        List<ManagedSwapClause__c> ManagedSwapClauseListToInsert = new  List<ManagedSwapClause__c>();
        ManagedSwapClause__c managedswapClause = new ManagedSwapClause__c();
        managedswapClause.Clause__c =  ClauseItem.Id;
        managedswapClause.SwapClause__c = swapClauseItem.Id;       
        managedswapClause.ContractAgreement__c = contractAgreement.Id;
        ManagedSwapClauseListToInsert.add(managedswapClause);


        ManagedSwapClause__c managedswap = new ManagedSwapClause__c();
        managedswap.SubClause__c =  subClauseItem.Id;
        managedswap.SwapClause__c = swapSubClauseItem.Id;       
        managedswap.ContractAgreement__c = contractAgreement.Id;
        ManagedSwapClauseListToInsert.add(managedswap);

        ManagedSwapClause__c managedsubsubswap = new ManagedSwapClause__c();
        managedsubsubswap.SubSubClause__c =  subsubClauseItem.Id;
        managedsubsubswap.SwapClause__c = swapSubSubClauseItem.Id;       
        managedsubsubswap.ContractAgreement__c = contractAgreement.Id;
        ManagedSwapClauseListToInsert.add(managedsubsubswap);
        
        insert ManagedSwapClauseListToInsert;

        update ManagedSwapClauseListToInsert; //cover update of managed swap Clauses

        //create the class responsible to create the managed clauses
        ManagedClauseCreator clauseCreator = new ManagedClauseCreator('Brazil', 'B2B');
        //Add the list of SwappedClauses to the class
        clauseCreator.addSwapList(ManagedSwapClauseListToInsert);
        //let the class create the managed clauses
        clauseCreator.createManagedClauses();
        //Get the list of managed clauses created
        List<ManagedClause> listOfManagedClauses =  clauseCreator.getManagedClauses();

        //testing if the clause text was changed to swap clause text
        system.assertEquals('BR 2.3 - Gympass Responsibilities', listOfManagedClauses[5].Name);
        system.assertEquals('Opção 2 para 2.3', listOfManagedClauses[5].ClauseText);

        //testing if the Subclause text AND the SubSubClause was changed to swap clause text
        system.assertEquals('BR 2.1.1 - Gympass Responsibilities', listOfManagedClauses[3].getSubClauses()[0].Name);
        system.assertEquals('Opção 1 para 2.1.1Texto clausula BR 2.1.1.1Opção 2 para 2.1.1.2null', listOfManagedClauses[3].getSubClauses()[0].ClauseText);

        List<ManagedClause__c> managedClauseList = [SELECT Name, Order__c, Text__c FROM ManagedClause__c WHERE ContractAgreement__c =: contractAgreement.Id];
        System.debug('managedClauseList ' + managedClauseList);
        Set<Id> ManagedClauseSetId = new Set<Id>();
        for (ManagedClause__c managedClause : managedClauseList){
            ManagedClauseSetId.add(managedClause.id);
        }

        update managedClauseList; //cover update of managed Clauses

        List<SubManagedClauses__c> managedSubClauseList = [SELECT Name, Order__c, Text__c FROM SubManagedClauses__c WHERE ManagedClause__c =: ManagedClauseSetId];
        System.debug('managedSubClauseList ' + managedSubClauseList);

        update managedSubClauseList; //cover update of sub managed Clauses
    }

    @istest
    static void ClauseManagementController_getSwapClauses_UsingContractAgreementId(){

        APXT_Redlining__Contract_Agreement__c contrato = new APXT_Redlining__Contract_Agreement__c(); // B2B Order Form
        contrato.Include_signature__c = False ;
        contrato.APXT_Renegotiation__c =False;
        contrato.RecordTypeId = Schema.SObjectType.APXT_Redlining__Contract_Agreement__c.getRecordTypeInfosByDeveloperName().get('B2B_Contract').getRecordTypeId();
        contrato.Include_full_T_C_s__c = true;
        contrato.Template_Selection__c = 'Brazil';

        insert contrato;

        APXT_Redlining__Contract_Agreement__c contractAgreement = [SELECT Id, 
                                                                Template_Group__c, 
                                                                Template_Selection__c
                                                                FROM
                                                                APXT_Redlining__Contract_Agreement__c
                                                                LIMIT 1];


        List<SwapClauseWrapper> swapList = ClauseManagementController.getSwapClausesOptions(contractAgreement.Id);

        string stringCreatedToAura =  ClauseManagementController.getSwapClauses(contractAgreement.Id);
        System.assertEquals(3,swapList.size());

        System.assertEquals(2,  swapList[0].SwapOptions.size() );

        System.assertEquals(2,  swapList[1].SwapOptions.size() );

        System.assertEquals(3,  swapList[2].SwapOptions.size() );

        System.assertNotEquals(null, stringCreatedToAura);
    }


    @istest
    static void ClauseManagementController_getActiveSwapClauses(){


        Clause__c clause = [SELECT Id, Name FROM Clause__c WHERE Name LIKE '%BR 1.1 - Definitions%' LIMIT 1];

        SubClause__c Subclause = [SELECT Id, Name FROM SubClause__c WHERE Name LIKE '%BR 2.1.1 - Gympass Responsibilities%' LIMIT 1];

        SubSubClause__c SubSubclause = [SELECT Id, Name FROM SubSubClause__c WHERE Name LIKE '%BR 2.1.1.1 - Gympass Responsibilities%' LIMIT 1];

        SwapClause__c swap  = [SELECT Id, Name FROM SwapClause__c WHERE Name LIKE '%BR 2.1.1 - Gympass Responsibilities Option 1%' LIMIT 1];

        //system.debug('SwapClause__c => ' + swap );

        List<APXT_Redlining__Contract_Agreement__c> ContractList = new List<APXT_Redlining__Contract_Agreement__c>();

        APXT_Redlining__Contract_Agreement__c contrato = new APXT_Redlining__Contract_Agreement__c(); 
        contrato.Include_signature__c = False ;
        contrato.APXT_Renegotiation__c =False;
        contrato.RecordTypeId = Schema.SObjectType.APXT_Redlining__Contract_Agreement__c.getRecordTypeInfosByDeveloperName().get('B2B_Contract').getRecordTypeId();
       // contrato.Include_full_T_C_s__c = true;
        contrato.Template_Selection__c = 'Brazil';

        ContractList.add(contrato);

        insert ContractList;



        List<ManagedSwapClause__c> MnSwapClauseList = new List<ManagedSwapClause__c>();
        ManagedSwapClause__c mnClause1 = new ManagedSwapClause__c();
        mnClause1.Clause__c = clause.id;
        mnClause1.SwapClause__c = swap.id;
        mnClause1.Status__c = 'Alteração criada com sucesso';
        mnClause1.ContractAgreement__c = contrato.id;

        MnSwapClauseList.add(mnClause1) ;

        ManagedSwapClause__c mnClause2 = new ManagedSwapClause__c();
        mnClause2.SubClause__c = subclause.id;
        mnClause2.SwapClause__c = swap.id;
        mnClause2.Status__c = 'Alteração criada com sucesso';
        mnClause2.ContractAgreement__c = contrato.id;

        MnSwapClauseList.add(mnClause2) ;

        ManagedSwapClause__c mnClause3 = new ManagedSwapClause__c();
        mnClause3.SubSubClause__c = Subsubclause.id;
        mnClause3.SwapClause__c = swap.id;
        mnClause3.Status__c = 'Alteração criada com sucesso';
        mnClause3.ContractAgreement__c = contrato.id;

        MnSwapClauseList.add(mnClause3) ;

        insert MnSwapClauseList;

        List<ManagedSwapClauseWrapper> listTest = ClauseManagementController.getActiveSwapClauses(contrato.id);
        string stringCreatedToAura = ClauseManagementController.getSwappedClauses(contrato.id);

        System.assertEquals(listTest.size(), 3);

        System.assertNotEquals(null, stringCreatedToAura);

    }


    @istest
    static void ClauseManagementController_saveSwappedClauses_AND_deleteSwapClauses(){


        Clause__c clause = [SELECT Id, Name FROM Clause__c WHERE Name LIKE '%BR 1.1 - Definitions%' LIMIT 1];

        SubClause__c Subclause = [SELECT Id, Name FROM SubClause__c WHERE Name LIKE '%BR 2.1.1 - Gympass Responsibilities%' LIMIT 1];

        SubSubClause__c SubSubclause = [SELECT Id, Name FROM SubSubClause__c WHERE Name LIKE '%BR 2.1.1.1 - Gympass Responsibilities%' LIMIT 1];

        SwapClause__c swap  = [SELECT Id, Name FROM SwapClause__c WHERE Name LIKE '%BR 2.1.1 - Gympass Responsibilities Option 1%' LIMIT 1];

        
        List<APXT_Redlining__Contract_Agreement__c> ContractList = new List<APXT_Redlining__Contract_Agreement__c>();

        APXT_Redlining__Contract_Agreement__c contrato = new APXT_Redlining__Contract_Agreement__c(); 
        contrato.Include_signature__c = False ;
        contrato.APXT_Renegotiation__c =False;
        contrato.RecordTypeId = Schema.SObjectType.APXT_Redlining__Contract_Agreement__c.getRecordTypeInfosByDeveloperName().get('B2B_Contract').getRecordTypeId();
       // contrato.Include_full_T_C_s__c = true;
        contrato.Template_Selection__c = 'Brazil';

        ContractList.add(contrato);

        insert ContractList;



        List<ManagedSwapClauseWrapper> swappedClauseList = new List<ManagedSwapClauseWrapper>();

        ManagedSwapClauseWrapper mnSwappClause1 = new ManagedSwapClauseWrapper();
         
        mnSwappClause1.ContractAgreementId = contrato.id;
        mnSwappClause1.StandardId = clause.Id;
        mnSwappClause1.Type = 'Clause';
        mnSwappClause1.SwapId = swap.id;
        mnSwappClause1.Status = 'Teste 1';     
        swappedClauseList.add(mnSwappClause1);

        ManagedSwapClauseWrapper mnSwappClause2 = new ManagedSwapClauseWrapper();
         
        mnSwappClause2.ContractAgreementId = contrato.id;
        mnSwappClause2.StandardId =Subclause.Id;
        mnSwappClause2.Type = 'SubClause';
        mnSwappClause2.SwapId = swap.id;
        mnSwappClause2.Status = 'Teste 2';     
        swappedClauseList.add(mnSwappClause2);

        ManagedSwapClauseWrapper mnSwappClause3 = new ManagedSwapClauseWrapper();
         
        mnSwappClause3.ContractAgreementId = contrato.id;
        mnSwappClause3.StandardId = SubSubclause.Id;
        mnSwappClause3.Type = 'SubSubClause';
        mnSwappClause3.SwapId = swap.id;
        mnSwappClause3.Status = 'Teste 2';     
        swappedClauseList.add(mnSwappClause3);

        //ClauseManagementController.saveSwappedClauses(swappedClauseList);
        string swappedClauseListInJSON = json.serialize(swappedClauseList);
        ClauseManagementController.saveSwapClauses(swappedClauseListInJSON);
        //ClauseManagementController.saveSwapClauses('[{"ObjectId": "","StandardId": "a3t6C000000DvxTQAS","StandardClause": "BR 3.4.1 - Company Responsibilities","SwapId": "a3v6C0000010LhhQAE","NewClause": "BR 3.4.1 EXCLUSIVITY - CONTRACT TERM ONLY - 20% INCREASE","CreatedDateTime": 1631727877059,"Status": "Alternative clause applied", "Type":"Test", "Status":"OK"}]');

        Test.startTest();

        List<ManagedSwapClause__c> managedSwapClauseList = [SELECT Id, SwapClause__c , clause__c, subClause__c, subsubClause__c ,ContractAgreement__c
                                                            FROM ManagedSwapClause__c 
                                                            WHERE ContractAgreement__c =: contrato.Id];

        system.assertEquals(3, managedSwapClauseList.size());


        List<ManagedSwapClauseWrapper> swappedClauseListToDelete = new List<ManagedSwapClauseWrapper>();

        for (ManagedSwapClause__c managedSwapClause: managedSwapClauseList ){
           
            for (ManagedSwapClauseWrapper mnSwappClause : swappedClauseList){
                if (mnSwappClause.StandardId ==  managedSwapClause.clause__c){
                    mnSwappClause.ObjectId = managedSwapClause.Id;
                }else if (mnSwappClause.StandardId ==  managedSwapClause.subClause__c){
                    mnSwappClause.ObjectId = managedSwapClause.Id;
                }else if (mnSwappClause.StandardId ==  managedSwapClause.subsubClause__c){
                    mnSwappClause.ObjectId = managedSwapClause.Id;
                }
                System.debug('test ' +  mnSwappClause);
            }

        }

        string swappedClauseListToDeleteInJSON = json.serialize(swappedClauseList);

        System.debug('json ' +  swappedClauseListToDeleteInJSON);

        ClauseManagementController.deleteSwapClauses(swappedClauseListToDeleteInJSON);

        List<ManagedSwapClause__c> managedSwapClauseListAfterDelete = [SELECT Id, SwapClause__c , clause__c, subClause__c, subsubClause__c ,ContractAgreement__c
        FROM ManagedSwapClause__c 
        WHERE ContractAgreement__c =: contrato.Id];

        system.assertEquals(0, managedSwapClauseListAfterDelete.size());

        Test.stopTest();
    }


}