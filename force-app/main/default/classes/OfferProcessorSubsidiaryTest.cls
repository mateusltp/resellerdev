/**
 * @author vncferraz
 * Provide Unit Test for OfferSubsidiaryProcessor orchestration and methods
 */
@isTest(seeAllData=false)
public class OfferProcessorSubsidiaryTest {
    
    @testSetup
    private static void createData(){
        Pricebook2 lStandardPricebook = DataFactory.newPricebook();
        Database.update( lStandardPricebook );

        Account lAccEntity = DataFactory.newGympassEntity( 'Gympass' );
        lAccEntity.Id_Company__c = '15.664.649/0001-84';
        Database.insert( lAccEntity );
        
        Account lAcc = DataFactory.newAccount();  
        lAcc.NumberOfEmployees = 500;            
        Database.insert( lAcc );

        Account lAccSubsidiary = DataFactory.newAccount();              
        lAccSubsidiary.Id_Company__c = '2';
        lAccSubsidiary.ParentId = lAcc.Id;
        Database.insert( lAccSubsidiary );


        Contact contact = DataFactory.newContact(lAcc, '1');
        contact.Primary_HR_Contact__c = true;
        Database.insert( contact );
                     
        Product2 lAccessFee = DataFactory.newProduct('Enterprise Subscription', false, 'BRL');
        lAccessFee.Minimum_Number_of_Employees__c =0;
        lAccessFee.Maximum_Number_of_Employees__c =99999;
        lAccessFee.Family = 'Enterprise Subscription';
        lAccessFee.Family_Member_Included__c = true;
        Database.insert( lAccessFee );
        
        Product2 lSetupFee = DataFactory.newProduct('Setup Fee', false, 'BRL');
        Database.insert( lSetupFee ); 
        
        Opportunity lNewBusinessOpp = DataFactory.newOpportunity( lAcc.Id, lStandardPricebook, 'SMB_New_Business' ); 
        lNewBusinessOpp.CurrencyIsoCode = 'BRL';    
        Database.insert( lNewBusinessOpp );
        System.debug( 'lNewBusinessOpp acc id = ' + lNewBusinessOpp.AccountId );

        PricebookEntry lAccessFeeEntry = DataFactory.newPricebookEntry(lStandardPricebook, lAccessFee, lNewBusinessOpp);
        lAccessFeeEntry.IsActive = true;
        Database.insert(lAccessFeeEntry);
        
        Offer_Queue__c offerQueue1 = new Offer_Queue__c();
        offerQueue1.Account_ID__c = lAcc.Id;
        offerQueue1.Adjusted_Probability__c = null;
        offerQueue1.Allowlist_Enabler__c = 'Yes';        
        offerQueue1.CurrencyIsoCode = 'BRL';
        offerQueue1.End_Date__c = System.today().addDays(365);
        offerQueue1.ES_Billing_Percentage__c = 50;
        offerQueue1.ES_Payment_Frequency__c = 'Monthly';
        offerQueue1.Family_Member__c = 'Yes';
        offerQueue1.Free_Plan__c = 'No';
        offerQueue1.Number_of_Employees__c = 900;
        offerQueue1.Opportunity_ID__c = lNewBusinessOpp.Id;
        offerQueue1.Parent_Account_ID__c = null;
        offerQueue1.Price_index__c = 'IPCA';
        offerQueue1.Price_index_description__c = '';
        offerQueue1.Reference_Sales_Price_per_Eligible__c = 4.45;
        offerQueue1.Stage__c = 'Offer Sent';
        offerQueue1.Start_Date__c = System.today();
        offerQueue1.Status__c = 'Pending';
        offerQueue1.Status_Detail__c = '';
        offerQueue1.Type_of_Request__c = 'SMB Renegotiation';
        offerQueue1.Waiver_End_Date__c = System.today().addDays(90);
        offerQueue1.Waiver_Percentage__c = 100;
        offerQueue1.Waiver_Start_Date__c = System.today().addDays(30);
        offerQueue1.Full_Launch__c = 'Yes';
        offerQueue1.Exclusivity_clause__c = 'Yes';
        insert offerQueue1;

        Offer_Queue__c offerQueue2 = new Offer_Queue__c();
        offerQueue2.Account_ID__c = lAccSubsidiary.Id;
        offerQueue2.Opportunity_ID__c = lNewBusinessOpp.Id;
        offerQueue2.ES_Billing_Percentage__c = 50;
        offerQueue2.Parent_Account_ID__c = lAcc.Id;
        offerQueue2.Type_of_Request__c = 'Subsidiary';
        offerQueue2.Status__c = 'Pending';
        offerQueue2.Status_Detail__c = '';
        insert offerQueue2;
    }
    @isTest
    private static void testSubsidiaryCreation(){
        Offer_Queue__c offerQueue = [select Id,Status__c,Status_Detail__c,Account_ID__c,Parent_Account_ID__c,Opportunity_ID__c from Offer_Queue__c where Type_of_Request__c= 'Subsidiary' limit 1];
        System.assertEquals( offerQueue.Status__c, 'Pending' );
        
        Opportunity opp = [select Id ,Name from Opportunity where AccountId = :offerQueue.Parent_Account_ID__c ];
        System.assertNotEquals( opp.Id, null);
        
        OfferQueueSubsidiaryBatchable sbatchable = new OfferQueueSubsidiaryBatchable();
        Test.startTest();
        Database.executeBatch(sbatchable);
        Test.stopTest();

        offerQueue = [select Id,Status__c,Status_Detail__c,Parent_Account_ID__c,Opportunity_ID__c from Offer_Queue__c where Type_of_Request__c = 'Subsidiary' limit 1];
        System.debug( ' offerQueue = ' + JSON.serialize(offerQueue) );

        List<Account_Opportunity_Relationship__c> accountsInOpportunity = [select Id from Account_Opportunity_Relationship__c where Opportunity__r.AccountId = :offerQueue.Parent_Account_ID__c and Main_Order__c = false];
        System.assertEquals(accountsInOpportunity.size(),1);
    }
}