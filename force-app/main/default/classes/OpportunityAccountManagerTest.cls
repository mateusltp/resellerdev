/**
 * @File Name          : OpportunityAccountManagerTest.cls
 * @Description        : 
 * @Author             : Samuel Silva - GFT (slml@gft.com)
 * @Group              : 
 * @Last Modified By   : Samuel Silva - GFT (slml@gft.com)
 * @Last Modified On   : 07-24-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    26/03/2020   Samuel Silva - GFT (slml@gft.com)     Initial Version
**/

@isTest 
public class OpportunityAccountManagerTest {
    
    @TestSetup
    static void generateGenericDataForOppManagerTest(){
       
        Id rtId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gym_Partner').getRecordTypeId();
       
        Account acc = new Account();

        acc.name='AcademiaBrasilCompanyPai';
        acc.RecordTypeId = rtId;
        acc.Types_of_ownership__c = Label.privateOwnerPicklist;
        acc.GP_Status__c = 'Pending Approval';
        acc.ShippingState = 'Minas Gerais';
        acc.ShippingStreet = 'RUX ASSS';
        acc.ShippingCity = 'CITY CITY';
        acc.ShippingCountry = 'Brazil';
        acc.Gym_Type__c = 'Studios';
        acc.Subscription_Type__c = 'Value per class';
        acc.Subscription_Period__c = 'Monthy value';
        acc.Subscription_Type_Estimated_Price__c	= 100;
        acc.Has_market_cannibalization__c = 'No';
        acc.Exclusivity__c = 'Yes';
        acc.Exclusivity_End_Date__c = Date.today().addYears(1);
        acc.Exclusivity_Partnership__c = 'Full Exclusivity';
        acc.Exclusivity_Restrictions__c= 'No';
        acc.Website = 'testing@tesapex.com';
        acc.Gym_Email__c = 'gymemail@apex.com';
        acc.Phone = '3222123123';
        INSERT acc;
      
        Account child = new Account();
        child.name='child1'; 
        child.ParentId = acc.Id;
        child.ShippingStreet = 'RUX AS333';
        child.Types_of_ownership__c = Label.privateOwnerPicklist;
        child.GP_Status__c = 'Pending Approval';
        child.website='www.ChildOne.com';
        child.RecordTypeId = rtId; 
        child.Gym_Type__c ='Studios';
        child.ShippingCountry = 'Brazil'; 
        child.ShippingCity ='ddddd';
        child.ShippingState = 'Roraima';
        child.Subscription_Type__c = 'Value per class';
        child.Subscription_Period__c = 'Monthy value';
        child.Subscription_Type_Estimated_Price__c = 100;
        child.Exclusivity__c = 'Yes';
        child.Exclusivity_End_Date__c = Date.today().addYears(1);
        child.Exclusivity_Partnership__c = 'Full Exclusivity';
        child.Exclusivity_Restrictions__c= 'No';
        child.Gym_Email__c = 'gymemail@apex.com';
        child.Phone = '3222123123';
        
        INSERT child; 


        Account child2 = new Account();
        child2.name='child2'; 
        child2.ParentId = acc.Id;
        child2.Types_of_ownership__c = Label.privateOwnerPicklist;
        child2.GP_Status__c = 'Pending Approval';
        child2.ShippingCity = 'AAAA';
        child2.ShippingStreet = 'RUXDDDD ASSS33d';
        child2.website='www.ChildOne.com';
        child2.RecordTypeId = rtId; 
        child2.Gym_Type__c ='Studios';
        child2.ShippingCountry = 'Brazil'; 
        child2.ShippingState = 'São Paulo';
        child2.Subscription_Type__c = 'Value per class';
        child2.Subscription_Period__c = 'Monthy value';
        child2.Subscription_Type_Estimated_Price__c = 100;
        child2.Exclusivity__c = 'Yes';
        child2.Exclusivity_End_Date__c = Date.today().addYears(1);
        child2.Exclusivity_Partnership__c = 'Full Exclusivity';
        child2.Exclusivity_Restrictions__c= 'No';
        child2.Gym_Email__c = 'gymemail@apex.com';
        child2.Phone = '1111111';
        
        INSERT child2; 

        Account outraConta = new Account();

        outraConta.name='outraConta';
        outraConta.ParentId = acc.Id;
        outraConta.RecordTypeId = rtId;
        outraConta.GP_Status__c = 'Pending Approval';
        outraConta.ShippingState = 'Mato Grosso';
        outraConta.ShippingCountry = 'Brazil';
        outraConta.ShippingCity = 'JJJJJ';
        outraConta.ShippingStreet = 'RUEEEXXSS';
        outraConta.Gym_Type__c = 'Studios';
        outraConta.Types_of_ownership__c = Label.franchisePicklist;
        outraConta.Subscription_Type__c = 'Value per class';
        outraConta.Subscription_Period__c = 'Monthy value';
        outraConta.Subscription_Type_Estimated_Price__c	= 100;
        outraConta.Has_market_cannibalization__c = 'No';
        outraConta.Exclusivity__c = 'Yes';
        outraConta.Exclusivity_End_Date__c = Date.today().addYears(1);
        outraConta.Exclusivity_Partnership__c = 'Full Exclusivity';
        outraConta.Exclusivity_Restrictions__c= 'No';
        outraConta.Website = 'testing@tesapex.com';
        outraConta.Gym_Email__c = 'gymemail@apex.com';
        outraConta.Phone = '3222123123';
        INSERT outraConta;
        
        List<Opportunity> oppToInsert = new List<Opportunity>();
        
        Id oppRtId =   Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Gyms_Small_Medium_Partner').getRecordTypeId();
        
        Opportunity accOpp = new Opportunity();
        accOpp.recordTypeId = oppRtId;
        accOpp.AccountId = acc.id;
        accOpp.Name = 'academiaBrasilCompanyOpp'; 
        accOpp.CMS_Used__c = 'Yes';     
        accOpp.Gym_agreed_to_an_API_integration__c	= 'Yes';
        accOpp.Club_Management_System__c = 'ABC Financial';
        accOpp.Integration_Fee_Deduction__c = 'No';
        accOpp.Should_OPS_team_send_the_training__c = 'Standard Training';
        accOpp.CloseDate = Date.today();
        accOpp.Success_Look_Like__c = 'Yes';
        accOpp.Success_Look_Like_Description__c	= 'Money money';
        accOpp.StageName = 'Qualificação';
        accOpp.Type	= 'Expansion';     
        accOpp.CurrencyIsoCode = 'BRL';
        accOpp.Gympass_Plus__c = 'Yes';
        oppToInsert.add(accOpp);
        
        INSERT accOpp;
        
        List<Account_Opportunity_Relationship__c> aorsToInsert= new List<Account_Opportunity_Relationship__c>();
        
        Account_Opportunity_Relationship__c aor = new Account_Opportunity_Relationship__c();
        aor.Account__c = child.Id;
        aor.Opportunity__c = accOpp.Id;
        aorsToInsert.add(aor);

        Account_Opportunity_Relationship__c aorOther = new Account_Opportunity_Relationship__c();
        aorOther.Account__c = child2.Id;
        aorOther.Opportunity__c = accOpp.Id;        
        aorsToInsert.add(aorOther);
        
        INSERT aorsToInsert;
    }

    
    @isTest static void getRelatedAccountsTest() {
        
        Opportunity opTest = [SELECT id, AccountId FROM Opportunity WHERE Name = 'academiaBrasilCompanyOpp'];
     
        Object result = null;
        Test.startTest();
            result = OpportunityAccountManagerController.getRelatedAccounts(opTest.Id);
            Map<String, List<Object>> m = (Map<String, List<Object>>) result;
            List<Object> objRelated = new List<Object>();
            objRelated.addAll(m.get('AccWithOpp'));
            objRelated.addAll(m.get('AccWithNoOpp'));
            System.assertEquals(3, objRelated.size());           
        Test.stopTest();               
    }

   
    @isTest static void setRelationshipAccountWithOppTest() {
        Opportunity opTest = [SELECT id, AccountId FROM Opportunity WHERE Name = 'academiaBrasilCompanyOpp'];
        List<Account> accLst = [SELECT Id FROM Account WHERE Name = 'outraConta'];
        
        Set<Id> relatedAccounts = (new Map<Id,Account>(accLst)).keySet();
        Test.startTest();            
            OpportunityAccountManagerController.setRelationshipAccountWithOpp(accLst, opTest.Id);
            List<Account_Opportunity_Relationship__c> aorLst = new List<Account_Opportunity_Relationship__c>();
            aorLst = [SELECT Id, Opportunity__c, Account__c FROM Account_Opportunity_Relationship__c WHERE Opportunity__c =: opTest.Id AND Account__c IN: relatedAccounts];  
          
            System.assertEquals(accLst.size(), aorLst.size());
        Test.stopTest();               
    } 

    @isTest static void breakRelationshipAccountWithOppTest() {
        Opportunity opTest = [SELECT id, AccountId FROM Opportunity WHERE Name = 'academiaBrasilCompanyOpp'];
        List<Account> accLst = [SELECT Id FROM Account WHERE Name LIKE '%child%'];
        System.debug('accLst' + accLst);
        Test.startTest();            
            OpportunityAccountManagerController.breakRelationshipAccountWithOpp(accLst, opTest.Id);
            List<Account_Opportunity_Relationship__c> aorLst = new List<Account_Opportunity_Relationship__c>();
            aorLst = [SELECT Id, Opportunity__c, Account__c FROM Account_Opportunity_Relationship__c WHERE Opportunity__c =: opTest.Id];  
            System.debug('aorLst' + aorLst);
            System.assertEquals(0, aorLst.size());
        Test.stopTest();               
    } 
}