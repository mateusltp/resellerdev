public with sharing class FastTrackPBCreateNewOppUpdateAcc {
    
    public static Id recordTypeIdIndirectChannel = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Indirect_Channel').getRecordTypeId();
    public static Id recordTypeIdIndirectChannelNewBusiness = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Indirect_Channel_New_Business').getRecordTypeId();
    public static Id recordTypeIdGymsWishlistPartner = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Gyms_Wishlist_Partner').getRecordTypeId();
    public static Id recordTypeIdGymsSmallMediumPartner = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Gyms_Small_Medium_Partner').getRecordTypeId();
    class MyException extends Exception{}

    //set the name before insert 
    public static void beforeInsert(List<Opportunity> NewOpportunity) {

        System.debug('@@ INDIRETOS inicio processo before insert'+' DATE/TIME:'+Datetime.now());

        Set<id> accIds = new Set<id>();
       

        //Get accounts ids
        for(Opportunity opp :  NewOpportunity)
        {
            if(opp.RecordTypeId != recordTypeIdGymsWishlistPartner && opp.RecordTypeId != recordTypeIdGymsSmallMediumPartner)
            {
                accIds.add(opp.AccountId);
            }
        }

        //Account Map
        Map<id, account> accsMap = new Map<id, account>([SELECT id, name, Business_Unit__c FROM account WHERE Id IN :accIds]);

        for(Opportunity opp :  NewOpportunity)
        {

            if(opp.RecordTypeId == recordTypeIdIndirectChannelNewBusiness)
            {
                opp.Probability = 10;
                opp.StageName = 'Validated';
                
            }

            if(opp.RecordTypeId != recordTypeIdGymsWishlistPartner && opp.RecordTypeId != recordTypeIdGymsSmallMediumPartner)
            {
                account acc = accsMap.get(opp.Accountid);
                if(acc != null)
                {
                    //opp.name = IsNew(opp, acc);
                }
            }
           
        }

        System.debug('@@ INDIRETOS fim processo before insert'+' DATE/TIME:'+Datetime.now());

    }

    //Call the  EnablersHelper.updateStepsTowardsSucces (InvocableMethod) the same at PB but only recordTypeIdIndirectChannel
    public static void AfterInsert(List<Opportunity> NewOpportunity) 
    {
        //System.debug('@@ INDIRETOS inicio processo after insert'+' DATE/TIME:'+Datetime.now());
        List<id> newOppIds = new List<id>();
        for(Opportunity opp :  NewOpportunity)
        {
            if(opp.RecordTypeId == recordTypeIdIndirectChannelNewBusiness)
            {
                newOppIds.add(opp.id);
            }
        }

        if(newOppIds.size()>0)
        {
            IsNewIndirectChannelNewBusinessEnablersHelper(newOppIds);
        }

        //System.debug('@@ INDIRETOS fim processo after insert'+' DATE/TIME:'+Datetime.now());
    }

    public static void beforeUpdate(List<Opportunity> NewOpportunity, Map<Id, Opportunity> OldOpportunity) {
        //System.debug('@@ INDIRETOS inicio processo before update'+' DATE/TIME:'+Datetime.now());
        
        for(Opportunity opp :  NewOpportunity)
        {
            if(opp.RecordTypeId == recordTypeIdIndirectChannelNewBusiness){
                Opportunity oldOpp  = OldOpportunity.get(opp.Id);
                opp.Opportunity_Validated_Date__c = OpportunityValidated(opp, oldOpp);                
            }
        }
        
        //System.debug('@@ INDIRETOS fim processo before update'+' DATE/TIME:'+Datetime.now());
    }

    public static void afterUpdate(List<Opportunity> NewOpportunity, Map<Id, Opportunity> OldOpportunity) {
       
        //System.debug('@@ INDIRETOS inicio processo after update'+' DATE/TIME:'+Datetime.now());
        List<Opportunity> lsNewOpportunitys = new List<Opportunity>(); 
        List<Account> lsAccountUpdate = new List<Account>();
        Set<id> usersIds = new Set<id>();
        Set<id> accIds = new Set<id>();

        for(Opportunity opp :  NewOpportunity)
        {
            usersIds.add(opp.OwnerId);
            accIds.add(opp.AccountId);
        }

        //Users Maps
        Map<id, User> usersMap = new Map<id, User>([SELECT id, name, Email FROM user WHERE Id IN :usersIds]);
        
        //Account Map
        Map<id, account> accsMap = new Map<id, account>([SELECT id, name, Business_Unit__c, Type FROM account WHERE Id IN :accIds]);
        
        //Map<id, List<account>> ChildrensAccsMap = new Map<id, List<account>>();
        
        //List with chindrens acc
        List<account> lsAccChildrens = [SELECT ParentId, id, name, Business_Unit__c, Type FROM account WHERE ParentId IN :accIds];

         //capturar as contas filhos de cada conta pai, repassar para o metodo e quando a conta pai for alterada a  acc.Type = 'Client'; altera as filhas tbm
         //na filha tem o parent account que aponta pra pai
         //qa - aulas de biologia e padaria do arthur aguiar
         //select id, name from account where ParentId = '00174000008OrCOAA0' or id = '00174000008OrCOAA0'

         
        for(Opportunity opp :  NewOpportunity)
        {

            user userData = usersMap.get(opp.OwnerId);
            account acc = accsMap.get(opp.Accountid);//Get father acc

            Opportunity oldOpp  = OldOpportunity.get(opp.Id);
            
            //Rules to create news oppotunits from update
            Opportunity OpportunityWonNewBusiness = OpportunityWonNewBusiness(opp, oldOpp);
            Opportunity OpportunityWonDirectIndirectPayroll = OpportunityWonDirectIndirectPayroll(opp, oldOpp);
            Opportunity OpportunityWonDirectIndirectContactPermission = OpportunityWonDirectIndirectContactPermission(opp, oldOpp);

            if(OpportunityWonNewBusiness.name != null)
            {

                lsNewOpportunitys.add(OpportunityWonNewBusiness);
            }

            if(OpportunityWonDirectIndirectPayroll.name != null)
            {

                lsNewOpportunitys.add(OpportunityWonDirectIndirectPayroll);
            }

            if(OpportunityWonDirectIndirectContactPermission.name != null)
            {

                lsNewOpportunitys.add(OpportunityWonDirectIndirectContactPermission);
            }


            //Rules to update account
            if(acc != null && opp.StageName == 'Lançado/Ganho') 
            {
                //string oldAccBusinessUnit = acc.Business_Unit__c;
                string oldAccType = acc.type;

                acc = OppWonNewBusinessSMBOwnerOrNot(opp, oldOpp, userData, acc);

                system.debug('@@ ACC ' + acc);
                system.debug('@@ lsAccChildrens '+  lsAccChildrens);

                //verify if acc has change value and add to update 
                if(oldAccType != acc.type /*|| oldAccBusinessUnit != acc.Business_Unit__c*/)
                {
                    //Add the father account to update and set the children too
                    lsAccountUpdate.add(acc);

                    //Logic to get the children acc and change the type when father acc was changed
                    if(lsAccChildrens != null && lsAccChildrens.size()>0)
                    {
                        for(account childrenAcc :  lsAccChildrens)
                        {
                            //When chindrens account (parentId) = Father acccount (id)
                            if(childrenAcc.ParentId == acc.id)
                            {
                                childrenAcc.Type = 'Client';
                                lsAccountUpdate.add(childrenAcc);
                            }
                        }
                    } 

                }
            }
            
            
        }

        if(lsNewOpportunitys.size()> 0)
        {
            upsert lsNewOpportunitys;
        }

        if(lsAccountUpdate.size()> 0)
        {
           upsert lsAccountUpdate;
        }

        //System.debug('@@ INDIRETOS fim processo after update'+' DATE/TIME:'+Datetime.now());

    }

    //New - set the specific name / before insert 
    private static String IsNew (Opportunity NewOpportunity, account acc)
    {
        String formatName;


                formatName = String.ValueOf(
                String.ValueOf(DateTime.Now().Month())
                +'/'
                +String.ValueOf(DateTime.Now().Year()) 
                +'/'
                +acc.Name
                +'/'
                +NewOpportunity.Name
                );


                if(formatName.Length()>=120)
                {
                    formatName = formatName.substring(0,120); 
                }

        Return formatName;
    }

    //New  Is New Indirect Channe - New Business / call EnablersHelper - affter insert, need opportunity ids
    //Only Indirect_Channel_New_Business
    private static void IsNewIndirectChannelNewBusinessEnablersHelper(List<id> NewOpportunityID)
    {
            EnablersHelper.updateStepsTowardsSuccess(NewOpportunityID);
    }

    //Opportunity Validated - before update 
    //para todos os recordTypes menos Gyms_Small_Medium_Partner e Gyms_Wishlist_Partner
    //Add no pb pra não rodar com nosso recordType
    public static Date OpportunityValidated(Opportunity NewOpportunity, Opportunity OldOpportunity)
    {
        Date oppDateValidate;

            if(NewOpportunity.StageName == 'Validated' && OldOpportunity.StageName != NewOpportunity.StageName
            && NewOpportunity.Opportunity_Validated_Date__c == null )
            {
                //"Opportunity Validated" Date???
                //NewOpportunity.Opportunity_Validated_Date__c = Date.today();
                oppDateValidate = Date.today();
            }

        return oppDateValidate;

    }

    //Opportunity Won - New Business - after update / create a new opportunity
    public static Opportunity OpportunityWonNewBusiness(Opportunity NewOpportunity, Opportunity OldOpportunity)
    {

        Opportunity newOpp = new Opportunity();

            if((NewOpportunity.StageName == 'Launched/Won' || NewOpportunity.StageName == 'Lançado/Ganho')  && OldOpportunity.StageName != NewOpportunity.StageName
            && NewOpportunity.Type == 'New Business'
            && newOpportunity.RecordTypeId == recordTypeIdIndirectChannel )
            {
                
                newOpp.name = 'Price Renegociation';
                newOpp.CloseDate = NewOpportunity.CloseDate.addMonths(12); //DATE(YEAR(ADDMONTHS([Opportunity].CloseDate,12) ),MONTH([Opportunity].CloseDate) ,DAY([Opportunity].CloseDate)) 
                newOpp.StageName = 'Opportunity validated';
                newOpp.accountId = OldOpportunity.accountId;
                newOpp.CurrencyIsoCode = OldOpportunity.CurrencyIsoCode;
                if(OldOpportunity.Responsavel_Gympass_Relacionamento__c != null)
                {
                    newOpp.OwnerId = OldOpportunity.Responsavel_Gympass_Relacionamento__c;
                }
                newOpp.Pricebook2Id = OldOpportunity.Pricebook2Id;
                newOpp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Price_Renegotiation').getRecordTypeId(); //Price_Renegotiation 0121L000001NQxvQAG
                newOpp.TotalOpportunityQuantity = OldOpportunity.TotalOpportunityQuantity;
                newOpp.Type = 'Renewal';
            }


        return newOpp;
    }

    //Opp Won - New Business - SMB Owner - only IndirectChannel - after update / update account value
    public static account OppWonNewBusinessSMBOwnerOrNot(Opportunity newOpportunity, Opportunity oldOpportunity, user userData, account acc)
    {

            if((newOpportunity.StageName == 'Launched/Won' ||  newOpportunity.StageName == 'Lançado/Ganho' ) && oldOpportunity.StageName != newOpportunity.StageName)
            {

                if(newOpportunity.RecordTypeId == recordTypeIdIndirectChannel || newOpportunity.RecordTypeId == recordTypeIdIndirectChannelNewBusiness)
                {
                    acc.Type = 'Client';

                    //set Business_Unit__c
                    if(newOpportunity.Type == 'New Business' && newOpportunity.RecordTypeId == recordTypeIdIndirectChannel)
                    {
                        if(userData.Email != null && 
                        (
                            
                            userData.Email == 'thais.machado@gympass.com'||
                            userData.Email == 'joseane.costa@gympass.com'||
                            userData.Email == 'vitor.honda@gympass.com'||
                            userData.Email == 'lucas.batista@gympass.com'||
                            userData.Email == 'rafael.souza@gympass.com'||
                            userData.Email == 'fellipe.garcia@gympass.com'||
                            userData.Email == 'aline.campos@gympass.com'||
                            userData.Email == 'vinicius.cardoso@gympass.com'||
                            userData.Email == 'gustavo.matavelli@gympass.com'
                            //userData.Email == 'ext.eric.torres@gympass.com'
                        ))
                        {
                            acc.Business_Unit__c = 'SMB';
                        }
                        else if(userData.Email != 'thais.machado@gympass.com'
                        && userData.Email != 'joseane.costa@gympass.com'
                        && userData.Email != 'vitor.honda@gympass.com'
                        && userData.Email != 'lucas.batista@gympass.com'
                        && userData.Email != 'rafael.souza@gympass.com'
                        && userData.Email != 'fellipe.garcia@gympass.com'
                        && userData.Email != 'aline.campos@gympass.com'
                        && userData.Email != 'vinicius.cardoso@gympass.com'
                        && userData.Email != 'gustavo.matavelli@gympass.com')
                        {
                            acc.Business_Unit__c = null;
                        }

                    }
                }
                
                
            }

        return acc;
    }

    //Opportunity Won - Direct Indirect Payroll - Update / create a new opportunity
    public static Opportunity OpportunityWonDirectIndirectPayroll(Opportunity newOpportunity, Opportunity oldOpportunity)
    {

        Opportunity newOpp = new Opportunity();

            if( (newOpportunity.RecordTypeId == recordTypeIdIndirectChannel 
            || newOpportunity.RecordTypeId == recordTypeIdIndirectChannelNewBusiness)
            && oldOpportunity.StageName != newOpportunity.StageName
            && (NewOpportunity.StageName == 'Launched/Won' || NewOpportunity.StageName == 'Lançado/Ganho') 
            && newOpportunity.Type == 'New Business'
            && (newOpportunity.Proposal_Payment_Method__c == 'Credit Card' || newOpportunity.Proposal_Payment_Method__c == 'Bank Charge'))
            {
                newOpp.name = 'CS - Enrollment Enabler/Payroll';
                newOpp.CloseDate = NewOpportunity.CloseDate.addMonths(12); //DATE(YEAR(ADDMONTHS([Opportunity].CloseDate ,12) ),MONTH([Opportunity].CloseDate),DAY([Opportunity].CloseDate)) 
                newOpp.StageName = 'Opportunity validated';
                newOpp.accountId = OldOpportunity.accountId;
                newOpp.CurrencyIsoCode = OldOpportunity.CurrencyIsoCode;
                if(OldOpportunity.Responsavel_Gympass_Relacionamento__c != null)
                {
                    newOpp.OwnerId = OldOpportunity.Responsavel_Gympass_Relacionamento__c;
                }
                newOpp.Pricebook2Id = OldOpportunity.Pricebook2Id;
                newOpp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Relationship').getRecordTypeId(); // '0121L000001QRupQAG' Relationship
                newOpp.Type = 'Payroll';
            }


        return newOpp;
    }

    //Opportunity Won - Direct Indirect Contact Permission - after Update / create new opp
    public static Opportunity OpportunityWonDirectIndirectContactPermission(Opportunity newOpportunity, Opportunity oldOpportunity)
    {

        Opportunity newOpp = new Opportunity();

            if((newOpportunity.RecordTypeId == recordTypeIdIndirectChannel 
            || newOpportunity.RecordTypeId == recordTypeIdIndirectChannelNewBusiness)
            && (NewOpportunity.StageName == 'Launched/Won' || NewOpportunity.StageName == 'Lançado/Ganho') 
            && oldOpportunity.StageName != newOpportunity.StageName
            && newOpportunity.Type == 'New Business'
            && newOpportunity.Proposal_Contact_Permission__c != 'Whitelist')
            {
                newOpp.name = 'CS - Enrollment Enabler/Whitelist';
                newOpp.CloseDate = NewOpportunity.CloseDate.addMonths(12); //DATE(YEAR(ADDMONTHS([Opportunity].CloseDate ,12) ),MONTH([Opportunity].CloseDate),DAY([Opportunity].CloseDate)) ;
                newOpp.StageName = 'Opportunity validated';
                newOpp.accountId = OldOpportunity.accountId;
                newOpp.CurrencyIsoCode = OldOpportunity.CurrencyIsoCode;
                if(OldOpportunity.Responsavel_Gympass_Relacionamento__c != null)
                {
                    newOpp.OwnerId = OldOpportunity.Responsavel_Gympass_Relacionamento__c;
                }
                newOpp.Pricebook2Id = OldOpportunity.Pricebook2Id;
                newOpp.RecordTypeId =  Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Relationship').getRecordTypeId(); //'0121L000001QRupQAG' Relationship
                newOpp.Type = 'Allowlist';


            }

        return newOpp;
    }
}