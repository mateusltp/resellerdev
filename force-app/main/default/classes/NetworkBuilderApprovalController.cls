/**
 * @description       : 
 * @author            : bruno.mendes@gympass.com
 * @group             : 
 * @last modified on  : 10/05/2022
 * @last modified by  : bruno.mendes@gympass.com
**/

public with sharing class NetworkBuilderApprovalController {
    private static final PS_Constants constants = PS_Constants.getInstance();

    @AuraEnabled
    public static List<AccountPartnerServiceImpl.Response> sendAccountsForExclusivityApproval(List<Id> accountIds, String comments) {
            return AccountService.sendAccountsForExclusivityApproval(accountIds, comments, constants.ACCOUNT_RT_PARTNER_FLOW_ACCOUNT);
    }
}