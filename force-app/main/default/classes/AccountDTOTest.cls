@IsTest
public class AccountDTOTest {
  @IsTest
  public static void execute() {
    new AccountDTO().parseToSAccount();

    List<String> expectedAccountErrorMessages = new List<String>();
    expectedAccountErrorMessages.add('id');
    expectedAccountErrorMessages.add('trade_name');
    expectedAccountErrorMessages.add('currency_id');
    expectedAccountErrorMessages.add('contacts');
    expectedAccountErrorMessages.add('addresses');

    List<String> actualAccountErrorMessages = new AccountDTO().getMissingRequiredFields();

    System.assertEquals(
      expectedAccountErrorMessages,
      actualAccountErrorMessages,
      'Not all validations have been done to the account'
    );

    new AccountDTO.ContactDTO().parseToSContact();

    List<String> expectedContactErrorMessages = new List<String>();
    expectedContactErrorMessages.add('id');
    expectedContactErrorMessages.add('first_name');
    expectedContactErrorMessages.add('last_name');
    expectedContactErrorMessages.add('email');

    List<String> actualContactErrorMessages = new AccountDTO.ContactDTO().getMissingRequiredFields();

    System.assertEquals(
      expectedContactErrorMessages,
      actualContactErrorMessages,
      'Not all validations have been done to the contact'
    );
  }
}