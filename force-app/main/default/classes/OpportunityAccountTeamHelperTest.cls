/**
 * @description       : 
 * @author            : AlyssonMota
 * @group             : 
 * @last modified on  : 06-21-2021
 * @last modified by  : Alysson Mota
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   05-11-2021   Alysson Mota   Initial Version
**/
@isTest
public with sharing class OpportunityAccountTeamHelperTest {

    @TestSetup
    static void createData(){ 
        System.debug('createData');
        //User us = getClientSuccessExecutive();
        
        Account acc = getAccount();
        insert acc;
        
        Opportunity opp = getClientOpp(acc);
        insert opp;
    }
    
    @isTest
    private static void testAssignTeamMembers(){
        Opportunity opp = [SELECT Id, StageName, Name FROM Opportunity LIMIT 1];

        User user = [SELECT Id, Name FROM User WHERE Email LIKE 'vitor.honda%' LIMIT 1];

        Form__c m0 = getM0(opp);
        insert m0;

        updateOpp(opp.Id, user.Id);
    }
    
    private static Account getAccount(){
        Account acc = new Account();
        acc.Name = 'Test Name';
        acc.Razao_Social__c = 'Test Legal Name';
        acc.BillingCity = 'Test Billig City';
        acc.BillingCountry = 'Brazil';
        acc.BillingCountryCode = 'BR';
        acc.BillingPostalCode = '00000-000';
        acc.BillingStreet = 'Test Street 999';
        acc.Id_Company__c = '26.556.823/0001-91';
        acc.NumberOfEmployees = 800;
        
        return acc;
    }
     
    private static Opportunity getClientOpp(Account acc){
        Opportunity opp = new Opportunity();
        opp.AccountId = acc.Id;
        opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        opp.Name = 'Opp Test Name B';
        opp.CloseDate = system.today().addDays(30);
        opp.StageName = 'Contrato Assinado';
        opp.CurrencyIsoCode = 'BRL';
        opp.Engagement_Journey_Completed__c = false;
        
        return opp;
    }
    
    private static Form__c getM0(Opportunity opp){
        Form__c m0 = new  Form__c();
        m0.Opportunity__c = opp.Id;
        m0.RecordTypeId = Schema.SObjectType.Form__c.getRecordTypeInfosByDeveloperName().get('M0_12_Steps').getRecordTypeId();
        return m0;
    }

    @future
    public static void updateOpp(Id oppId, Id userId) {   
        Opportunity opp = [SELECT Id, StageName, Name  FROM Opportunity WHERE Id =: oppId];

        opp.StageName = 'Lançado/Ganho';
        opp.Responsavel_Gympass_Relacionamento__c = userId;
        update opp;
    }

    private static User getClientSuccessExecutive(){
        return [ SELECT Id, FirstName, LastName, ProfileId, Email FROM User WHERE Profile.Name = 'Client Success Executive' AND Email != null and IsActive = true LIMIT 1];
    }
}