/**
 * @File Name          : ContentDocumentTriggerHandler.cls
 * @Description        : 
 * @Author             : David Mantovani - DDMA@gft.com
 * @Group              : 
 * @Last Modified By   : gft.samuel.silva@ext.gympass.com
 * @Last Modified On   : 04-20-2022
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    07/07/2020   GEPI@GFT.com     Initial Version
**/
public class ContentDocumentTriggerHandler extends TriggerHandler {

    public override void afterInsert() {        
        new ContentFileValidationHelper().validateContentDocument();
        //new ContentFileHelper().generatePublickLink();        
    }

    public override void beforeInsert(){
        //new ContentFileValidationHelper().validateContentDocument();
       new ContentFileHelper().generatePublickLink();
    }

    public override void afterDelete(){
        //new ContentFileValidationHelper().validateContentDocument();
        new ContentDocumentLinkHelper().deleteRelatedRecords();
    }
}