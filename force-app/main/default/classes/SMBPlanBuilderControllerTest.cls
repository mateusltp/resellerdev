@isTest
public class SMBPlanBuilderControllerTest {
    
    @TestSetup
    static void makeData(){
        
        Account gympassEntity = AccountMock.getGympassEntity();
        gympassEntity.CurrencyIsoCode = 'BRL';
        insert gympassEntity;
        
        Account account = AccountMock.getStandard('Empresas');
        account.CurrencyIsoCode = 'BRL';
        account.UUID__c = SelfCheckoutRequestMock.ACCOUNT_UUID;
        
        insert account;
        
        Contact contact = ContactMock.getStandard(account);
        
        insert contact;
        
        account.Attention__c = contact.Id;
        
        update account;
        
        Pricebook2 pricebook = new Pricebook2(
            CurrencyIsoCode = 'BRL',
            Id = Test.getStandardPricebookId(),
            IsActive = true
        );
        
        update pricebook;
        
        List<Product2> products = new List<Product2>();
        
        Product2 setupFee = ProductMock.getSetupFee();
        setupFee.Minimum_Number_of_Employees__c = 0;
        setupFee.Maximum_Number_of_Employees__c = 10000;
        setupFee.IsActive = true;
        setupFee.Family = 'Enterprise Subscription';
        setupFee.Copay2__c = false;
        setupFee.Family_Member_Included__c = false;
        products.add(setupFee);
        
        Product2 accessFee = ProductMock.getAccessFee();
        accessFee.Minimum_Number_of_Employees__c = 0;
        accessFee.Maximum_Number_of_Employees__c = 10000;
        accessFee.IsActive = true;
        accessFee.Family = 'Enterprise Subscription';
        accessFee.Copay2__c = false;
        accessFee.Family_Member_Included__c = false;
        products.add(accessFee);
        
        insert products;
        
        
        List<PricebookEntry> pricebookEntries = new List<PricebookEntry>();
        
        PricebookEntry setupFeePricebookEntry = PricebookMock.getPricebookEntry(
            pricebook,
            setupFee
        );
        setupFeePricebookEntry.UnitPrice = 10;
        pricebookEntries.add(setupFeePricebookEntry);
        
        PricebookEntry accessFeePricebookEntry = PricebookMock.getPricebookEntry(
            pricebook,
            accessFee
        );
        accessFeePricebookEntry.UnitPrice = 10;
        pricebookEntries.add(accessFeePricebookEntry);
        
        insert pricebookEntries;
        
        Opportunity opp = OpportunityMock.getNewBusiness(account, pricebook);
        opp.CurrencyIsoCode = 'BRL';
        opp.Current_Billing_Day__c = 15;
        opp.Current_Order_End_Date__c = Date.today().addMonths(3);
        opp.Quantity_Offer_Number_of_Employees__c = 50;
        
        insert opp;
        
        Quote qt = QuoteMock.getStandard(opp);
        insert qt;
        
        opp.SyncedQuoteId = qt.Id;
        update opp;
        
        QuoteLineItem qtItemSetup = QuoteLineItemMock.getSetupFee(qt,setupFeePricebookEntry);
        QuoteLineItem qtItemAccess = QuoteLineItemMock.getSetupFee(qt,accessFeePricebookEntry);
        insert new List<QuoteLineItem> { qtItemSetup, qtItemAccess };

    }
    
    @isTest
    static void getDataTest() {
        Test.startTest();        
        Opportunity opp = [SELECT Id, SyncedQuoteId, CurrencyIsoCode FROM Opportunity LIMIT 1];
        SMBPlanBuilderController.DataWrapper data = SMBPlanBuilderController.getData(opp.Id);
        System.assert(data.proposalId == opp.SyncedQuoteId, 'The proposal Id is not correct.');
        System.assertEquals(2, data.products.size(), 'The number of products retrieved is not correct.');
        System.assert(data.currencyIsoCode == opp.CurrencyIsoCode, 'The currencyIsoCode is not correct.');
        Test.stopTest();
    }
    
    @isTest
    static void getDataExceptionTest() {        
        try {
            Test.startTest();
            SMBPlanBuilderController.getData(null);
            System.assert(false, 'This assert must never be executed. An exception must be thrown before this assert.');
            Test.stopTest();
        }
        catch(Exception e) {
            System.assertEquals('System.AuraHandledException', e.getTypeName(), 'The exception thrown is not the expected one');
        }
    }
    
    
}