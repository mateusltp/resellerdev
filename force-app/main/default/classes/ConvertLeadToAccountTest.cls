/**
* @description       : 
* @author            : GEPI@GFT.com
* @group             : 
* @last modified on  : 11-16-2020
* @last modified by  : Jorge Stevaux - JZRX@gft.com
* Modifications Log 
* Ver   Date          Author                           Modification
* 1.0   07-15-2020    Bruno Carneiro - BOSR@gft.com   Initial Version
**/
@Istest
public class ConvertLeadToAccountTest {
    
    @TestSetup
    static void Setup(){   
        Id recordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gym_Partner').getRecordTypeId();
        Id rtIdOwnerReferral = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Partner_Owner_Referral_Lead').getRecordTypeId();
        Id rtIdContact = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Gyms_Partner').getRecordTypeId();
        
        /*SCORE*/
        Score_Data__c standard = new Score_Data__c();
        standard.Country_Score_Data__c = 'Standard';
        standard.State_Score_Data__c = 'Standard';
        standard.City_Score_Data__c = 'Standard';
        standard.Number_Of_Active_Users__c = 0;
        standard.Number_Of_Sign_Ups__c = 0;
        INSERT standard;
        System.Debug('Score Data ----- >  Standard -----------  >>> ' + standard);
        
        Score_Data__c sdt = new Score_Data__c();
        sdt.Country_Score_Data__c = 'Brazil';
        sdt.State_Score_Data__c = 'São Paulo';
        sdt.City_Score_Data__c = 'Sorocaba';
        sdt.Number_Of_Active_Users__c = 0;
        sdt.Number_Of_Sign_Ups__c = 0;
        INSERT sdt;
        System.Debug('Score Data ----- >  Sorocaba ------------  >>> ' + sdt);
        
        Score_Data__c sdtUp = new Score_Data__c();
        sdtUp.Country_Score_Data__c = 'Brazil';
        sdtUp.State_Score_Data__c = 'São Paulo';
        sdtUp.City_Score_Data__c = 'São Paulo';
        sdtUp.Number_Of_Active_Users__c = 29;
        sdtUp.Number_Of_Sign_Ups__c = 3001;
        INSERT sdtUp;
        System.Debug('Score Data ----- >  São Paulo ------------  >>> ' + sdtUp);
        
        Score_Data__c sdtUpAct = new Score_Data__c();
        sdtUpAct.Country_Score_Data__c = 'Brazil';
        sdtUpAct.State_Score_Data__c = 'São Paulo';
        sdtUpAct.City_Score_Data__c = 'Campinas';
        sdtUpAct.Number_Of_Active_Users__c = 1001;
        sdtUpAct.Number_Of_Sign_Ups__c = 3001;
        INSERT sdtUpAct;
        System.Debug('Score Data ----- >  Campinas ------------  >>> ' + sdtUpAct);
        
        Score_Data__c sdtARGSignUp = new Score_Data__c();
        sdtARGSignUp.Country_Score_Data__c = 'Argentina';
        sdtARGSignUp.State_Score_Data__c = 'Buenos Aires';
        sdtARGSignUp.City_Score_Data__c = 'Almirante Brown';
        sdtARGSignUp.Number_Of_Active_Users__c = 49;
        sdtARGSignUp.Number_Of_Sign_Ups__c = 51;
        INSERT sdtARGSignUp ;
        System.Debug('Score Data ----- >  Argentina - Almirante Brown  ------------  >>> ' + sdtARGSignUp );
        
        Score_Data__c sdtARGActive = new Score_Data__c();
        sdtARGActive.Country_Score_Data__c = 'Argentina';
        sdtARGActive.State_Score_Data__c = 'Buenos Aires';
        sdtARGActive.City_Score_Data__c = 'Arrecifes';
        sdtARGActive.Number_Of_Active_Users__c = 51;
        sdtARGActive.Number_Of_Sign_Ups__c = 51;
        INSERT sdtARGActive ;
        System.Debug('Score Data ----- >  Argentina - Arrecifes ------------  >>> ' + sdtARGActive );
        
        Score_Data__c sdtARGAAzul = new Score_Data__c();
        sdtARGAAzul.Country_Score_Data__c = 'Argentina';
        sdtARGAAzul.State_Score_Data__c = 'Buenos Aires';
        sdtARGAAzul.City_Score_Data__c = 'Azul';
        sdtARGAAzul.Number_Of_Active_Users__c = 101;
        sdtARGAAzul.Number_Of_Sign_Ups__c = 101;
        INSERT sdtARGAAzul ;
        System.Debug('Score Data ----- >  Argentina - Azul ------------  >>> ' + sdtARGAAzul );
        
        Lead ld1 = new Lead(
            LastName='Gympass Lead', RecordTypeId = rtIdOwnerReferral, 
            Country = 'Brazil', State = 'São Paulo',
            Street = 'Ipanema', City = 'Sorocaba',                       
            Phone = '(015)99835-3692',
            Company = 'Test lead',
            Status = 'em aberto',
            PostalCode = '18071801',
            Is_User_Owner__c = 'Yes',
            Types_of_ownership__c = 'Private Owner');
        
        INSERT ld1;
        
        Lead ld2 = new Lead(
            LastName='Gympass Lead', RecordTypeId = rtIdOwnerReferral, 
            Country = 'Brazil', State = 'São Paulo',
            Street = 'Ipanema', City = 'Sorocaba',                       
            Phone = '(015)99835-3692',
            Company = 'Test lead',
            Status = 'em aberto',
            PostalCode = '18031230',
            Is_User_Owner__c = 'Yes',
            Types_of_ownership__c = 'Private Owner');
        
        INSERT ld2;
        
        Account acct2 = new Account();
        acct2.RecordTypeId = recordTypeIdAccount;
        acct2.ParentId = ld2.Parent_Account_Related__c;
        acct2.Name = ld2.Company;
        acct2.phone = ld2.Phone;
        acct2.Email__c = ld2.Email;
        acct2.ShippingStreet = ld2.Street;
        acct2.ShippingCity = ld2.City;
        acct2.ShippingStateCode = ld2.StateCode;
        acct2.ShippingCountryCode = ld2.CountryCode;
        acct2.Referral_Score__c = ld2.Number_of_Referrals__c;
        acct2.AccountSource = ld2.LeadSource;
        acct2.Types_of_ownership__c = ld2.Types_of_ownership__c;
        acct2.BillingStreet = 'Rua 21 de Abril';
        acct2.BillingCity = 'São Paulo';
        acct2.BillingCountry = 'Brazil';
        try{
            insert acct2;
        }catch (System.Exception ex){
            
        }
    }
    
    
    @isTest static void updateLeadWithoutReferral(){
        Lead lead1 = [SELECT Id, status, Types_of_ownership__c, RecordTypeId FROM Lead WHERE PostalCode ='18071801' LIMIT 1];
        lead1.status = 'Convertido';
        
        Test.startTest();
        UPDATE lead1;
        Test.stopTest();             
    }
    
    @isTest static void updateToBeQualified(){
        Lead lead1 = [SELECT Id, status, Types_of_ownership__c, RecordTypeId FROM Lead WHERE PostalCode ='18071801' LIMIT 1];
        lead1.status = 'Em qualificação';
        
        Test.startTest();
        UPDATE lead1;
        Test.stopTest();             
    }
    
    @isTest static void updateToRejected(){
        Lead lead1 = [SELECT Id, status, Types_of_ownership__c, RecordTypeId FROM Lead WHERE PostalCode ='18071801' LIMIT 1];
        lead1.status = 'Descartado';
        
        Test.startTest();
        UPDATE lead1;
        Test.stopTest();             
    }
    
    @isTest static void insertAcct(){
        Id recordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gym_Partner').getRecordTypeId();
        
        Lead lead1 = [SELECT status, Parent_Account_Related__c, Company,
                      Phone, Email, Street, City, StateCode, CountryCode,
                      Number_of_Referrals__c, LeadSource, Types_of_ownership__c
                      FROM Lead WHERE PostalCode ='18071801'];
        
        Account acct = new Account();
        acct.RecordTypeId = recordTypeIdAccount;
        acct.ParentId = lead1.Parent_Account_Related__c;
        acct.Name = lead1.Company;
        acct.phone = lead1.Phone;
        acct.Email__c = lead1.Email;
        acct.ShippingStreet = lead1.Street;
        acct.ShippingCity = lead1.City;
        acct.ShippingStateCode = lead1.StateCode;
        acct.ShippingCountryCode = lead1.CountryCode;
        acct.Referral_Score__c = lead1.Number_of_Referrals__c;
        acct.AccountSource = lead1.LeadSource;
        acct.Types_of_ownership__c = lead1.Types_of_ownership__c;
        
        Test.startTest();
        INSERT acct;
        Test.stopTest();                
    }
    @isTest static void updateLeadWithReferral(){
        Lead lead2 = [SELECT Id, status, Referral_Account__c, Types_of_ownership__c, RecordTypeId FROM Lead WHERE PostalCode ='18031230' LIMIT 1];
        lead2.status = 'Convertido';
        
        Test.startTest();
        UPDATE lead2;
        Test.stopTest();             
    }
}