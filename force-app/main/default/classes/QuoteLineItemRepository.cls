/**
* @author vinicius.ferraz
* @description Provide mockable repository data layer for QuoteLineItem domain
*/
public with sharing virtual class QuoteLineItemRepository {
    public QuoteLineItemRepository() {
        
    }

    /**
     * throwable System.QueryException
     */
    public virtual QuoteLineItem byId(String recordId){
        return [select Id, Quantity, Has_Relevant_Changes_for_Offer_Approval__c,Description, QuoteId, pricebookEntryId, UnitPrice, TotalPrice, Discount__c,Approval_Status__c,ListPrice,Agency_fee_frequency__c,List_Price__c from QuoteLineItem where Id = :recordId];
    }

    public virtual QuoteLineItem accessFeeAndPaymentsForQuote(String quoteId){
        return [
            select  Id, Fee_Subtype__c,Has_Relevant_Changes_for_Offer_Deal_Desk__c, Has_Relevant_Changes_for_Offer_Approval__c,Approval_Status__c,Fee_Contract_Type__c,Fee_Type__c,Discount_Approval_Needed__c,Discount_Approved__c, Discount, Discount__c, Flat_Baseline_Adjustment__c, Flat_Baseline_Quantity__c, Flat_Unit_Price__c, Inflation_Adjustment_Index__c, Inflation_Adjustment_Period__c, Description, LineNumber, ListPrice, Non_Recurring_Billing_Date__c,
            Product2Id, Quantity, PricebookEntryId,QuoteId, UnitPrice, Subtotal, Subtotal__c, TotalPrice, Variable_Price_Ceiling__c, Variable_Price_Floor__c, Variable_Price_Unit_Increase__c, Quote.Copay__c,
            minInvestment__c, monthlyCap__c, priceperEnrolled__c, PricebookEntry.Has_Minimum_Quantity__c,Agency_fee_frequency__c,List_Price__c,
                    ( SELECT Id,Inflation_Adjustment_Index__c, Inflation_Adjustment_Period__c,Non_Recurring_Billing_Date__c,
                    Payment_Due_Days__c,Billing_Day__c,Custom_Billing_Day__c,Custom_Payment_Due_Days__c, Quote_Line_Item__c, Has_Relevant_Changes_for_Offer_Approval__c,Has_Relevant_Changes_for_Offer_Deal_Desk__c,Payment_Method__c,Percentage__c,Frequency__c,Cutoff_Day__c,Recurring_Cutoff_Day__c,Recurring_Billing_Period__c,Billing_Due_Date__c,Account__c FROM Payments__r ),( SELECT Id, Approval_Status__c,Number_of_Months_after_Launch__c, Name, End_Date__c, Payment__c, Percentage__c, Start_Date__c FROM Waivers__r )
            from QuoteLineItem 
            where QuoteId = :quoteId and PricebookEntry.Product2.Family = 'Enterprise Subscription' 
            order by CreatedDate desc limit 1
        ];    
    }

    public virtual Map<Id, QuoteLineItem> getAccessFeesForQuotes(List<Id> lQuoteLst){
        Map<Id, QuoteLineItem> lMapQuoteItem = new Map<Id, QuoteLineItem>();
        List <QuoteLineItem> lESLst = [
            select  Id, Fee_Subtype__c, Has_Relevant_Changes_for_Offer_Deal_Desk__c,Has_Relevant_Changes_for_Offer_Approval__c,Approval_Status__c,Fee_Contract_Type__c,Fee_Type__c, Discount_Approval_Needed__c,Discount_Approved__c, Discount, Discount__c, Flat_Baseline_Adjustment__c, Flat_Baseline_Quantity__c, Flat_Unit_Price__c, Inflation_Adjustment_Index__c, Inflation_Adjustment_Period__c, Description, LineNumber, ListPrice, Non_Recurring_Billing_Date__c,
            Product2Id, Quantity, PricebookEntryId,QuoteId, UnitPrice, Subtotal, Subtotal__c, TotalPrice, Variable_Price_Ceiling__c, Variable_Price_Floor__c, Variable_Price_Unit_Increase__c,
            minInvestment__c, monthlyCap__c, priceperEnrolled__c, PricebookEntry.Has_Minimum_Quantity__c,Agency_fee_frequency__c,List_Price__c
            from QuoteLineItem 
            where QuoteId IN: lQuoteLst and PricebookEntry.Product2.Family = 'Enterprise Subscription'
        ];    

        for(QuoteLineItem iItem : lESLst){
            lMapQuoteItem.put(iItem.QuoteId, iItem);
        }
        return lMapQuoteItem;
    }

    //dup?
    public virtual QuoteLineItem accessFeeForQuote(String quoteId){
        return [
            select  Id, Fee_Subtype__c, Has_Relevant_Changes_for_Offer_Deal_Desk__c,Has_Relevant_Changes_for_Offer_Approval__c,Approval_Status__c,Fee_Contract_Type__c,Fee_Type__c, Discount_Approval_Needed__c,Discount_Approved__c, Discount, Discount__c, Flat_Baseline_Adjustment__c, Flat_Baseline_Quantity__c, Flat_Unit_Price__c, Inflation_Adjustment_Index__c, Inflation_Adjustment_Period__c, Description, LineNumber, ListPrice, Non_Recurring_Billing_Date__c,
            Product2Id, Quantity, PricebookEntryId,QuoteId, UnitPrice, Subtotal, Subtotal__c, TotalPrice, Variable_Price_Ceiling__c, Variable_Price_Floor__c, Variable_Price_Unit_Increase__c,
            minInvestment__c, monthlyCap__c, priceperEnrolled__c, PricebookEntry.Product2.Maximum_Number_of_Employees__c, PricebookEntry.Has_Minimum_Quantity__c,Is_there_an_agency_fee__c,Agency_fee_frequency__c,Percentage_of_agency_fee__c,List_Price__c
            from QuoteLineItem 
            where QuoteId = :quoteId and PricebookEntry.Product2.Family = 'Enterprise Subscription' 
            order by CreatedDate desc limit 1
        ];    
    }
    
    //dup?
    public virtual List<QuoteLineItem> accessFeesForQuote(String quoteId){
        return [
            select  Id, Fee_Subtype__c,Has_Relevant_Changes_for_Offer_Deal_Desk__c,Has_Relevant_Changes_for_Offer_Approval__c,Approval_Status__c,Fee_Contract_Type__c, Fee_Type__c,Discount_Approval_Needed__c,Discount_Approved__c, Discount, Discount__c, Flat_Baseline_Adjustment__c, Flat_Baseline_Quantity__c, Flat_Unit_Price__c, Inflation_Adjustment_Index__c, Inflation_Adjustment_Period__c, Description, LineNumber, ListPrice, Non_Recurring_Billing_Date__c,
                   Product2Id, Quantity, PricebookEntryId,QuoteId, UnitPrice, Subtotal, Subtotal__c, TotalPrice, Variable_Price_Ceiling__c, Variable_Price_Floor__c, Variable_Price_Unit_Increase__c, PricebookEntry.Product2.Maximum_Number_of_Employees__c, PricebookEntry.Has_Minimum_Quantity__c,Agency_fee_frequency__c,List_Price__c
            from QuoteLineItem 
            where QuoteId = :quoteId and PricebookEntry.Product2.Family = 'Enterprise Subscription' 
            order by CreatedDate desc
        ];
    }

    public virtual Map<Id, QuoteLineItem> getOppIdToAccessFeeItem(List<Id> oppIdList) {
        Map<Id, QuoteLineItem> oppIdToAccessFeeItem = new Map<Id, QuoteLineItem>();
        List<QuoteLineItem> accessFeeList = accessFeeForOpportunities(oppIdList);

        for (QuoteLineItem accessFee : accessFeeList) {
            oppIdToAccessFeeItem.put(accessFee.Quote.OpportunityId, accessFee);
        }

        return oppIdToAccessFeeItem;
    }

    public virtual List<QuoteLineItem> accessFeeForOpportunities(List<Id> oppIdList) {
        return [
            select  Id, Fee_Subtype__c,Approval_Status__c, PricebookEntry.Has_Minimum_Quantity__c,Fee_Contract_Type__c,Fee_Type__c, Discount_Approval_Needed__c,Discount_Approved__c, Discount, Discount__c, Flat_Baseline_Adjustment__c, Flat_Baseline_Quantity__c, Flat_Unit_Price__c, Inflation_Adjustment_Index__c, Inflation_Adjustment_Period__c, Description, LineNumber, ListPrice,
                    Quote.OpportunityId, Product2Id, Quantity, PricebookEntryId,QuoteId, UnitPrice, Subtotal, Subtotal__c, TotalPrice, Variable_Price_Ceiling__c, Variable_Price_Floor__c, Variable_Price_Unit_Increase__c,Agency_fee_frequency__c,List_Price__c
            from QuoteLineItem 
            where Quote.OpportunityId = :oppIdList and PricebookEntry.Product2.Family = 'Enterprise Subscription' 
        ];  
    }

    public virtual QuoteLineItem setupFeeAndPaymentsForQuote(String quoteId){
        return [
            select  Id, Fee_Subtype__c,Has_Relevant_Changes_for_Offer_Deal_Desk__c,Has_Relevant_Changes_for_Offer_Approval__c,Approval_Status__c, Fee_Contract_Type__c,Fee_Type__c,Discount_Approval_Needed__c,Discount_Approved__c, Discount, Discount__c, Flat_Baseline_Adjustment__c, Flat_Baseline_Quantity__c, Flat_Unit_Price__c, Inflation_Adjustment_Index__c, Inflation_Adjustment_Period__c, Description, LineNumber, ListPrice, Non_Recurring_Billing_Date__c,
                    Product2Id, Quantity, PricebookEntryId,QuoteId, UnitPrice, Subtotal, Subtotal__c, TotalPrice, Variable_Price_Ceiling__c, Variable_Price_Floor__c, Variable_Price_Unit_Increase__c,Agency_fee_frequency__c,List_Price__c,
                    Quote.Opportunity.RecordType.DeveloperName,
                    ( SELECT Id,Inflation_Adjustment_Index__c,Percentage__c, Frequency__c,Inflation_Adjustment_Period__c,Non_Recurring_Billing_Date__c,
                    Payment_Due_Days__c,Billing_Day__c,Custom_Billing_Day__c,Custom_Payment_Due_Days__c,Quote_Line_Item__c,Has_Relevant_Changes_for_Offer_Approval__c,Has_Relevant_Changes_for_Offer_Deal_Desk__c, Payment_Method__c,Recurring_Cutoff_Day__c,Cutoff_Day__c,Recurring_Billing_Period__c,Billing_Due_Date__c,Account__c FROM Payments__r ), ( SELECT Id FROM Eligibilities__r ), ( SELECT Id, Approval_Status__c, Approval_level__c FROM Waivers__r )  
            from QuoteLineItem 
            where QuoteId = :quoteId and PricebookEntry.Product2.Family = 'Setup Fee' 
            order by CreatedDate desc limit 1
        ];
    }

    public virtual QuoteLineItem setupFeeForQuote(String quoteId){
        return [
            select  Id, Fee_Subtype__c,Has_Relevant_Changes_for_Offer_Deal_Desk__c,Has_Relevant_Changes_for_Offer_Approval__c,Approval_Status__c,Fee_Contract_Type__c, Fee_Type__c,Discount_Approval_Needed__c,Discount_Approved__c, Discount, Discount__c, Flat_Baseline_Adjustment__c, Flat_Baseline_Quantity__c, Flat_Unit_Price__c, Inflation_Adjustment_Index__c, Inflation_Adjustment_Period__c, Description, LineNumber, ListPrice, Non_Recurring_Billing_Date__c,
                   Product2Id, Quantity, PricebookEntryId,QuoteId, UnitPrice, Subtotal, Subtotal__c, TotalPrice, Variable_Price_Ceiling__c, Variable_Price_Floor__c, Variable_Price_Unit_Increase__c,Agency_fee_frequency__c,List_Price__c,
                   Quote.Opportunity.RecordType.DeveloperName
            from QuoteLineItem 
            where QuoteId = :quoteId and PricebookEntry.Product2.Family = 'Setup Fee' 
            order by CreatedDate desc limit 1
        ];
    }

    public virtual List<QuoteLineItem> setupFeesForQuote(String quoteId){
        return [
            select  Id, Fee_Subtype__c,Has_Relevant_Changes_for_Offer_Deal_Desk__c,Has_Relevant_Changes_for_Offer_Approval__c,Approval_Status__c,Fee_Contract_Type__c, Fee_Type__c,Discount_Approval_Needed__c,Discount_Approved__c, Discount, Discount__c, Flat_Baseline_Adjustment__c, Flat_Baseline_Quantity__c, Flat_Unit_Price__c, Inflation_Adjustment_Index__c, Inflation_Adjustment_Period__c, Description, LineNumber, ListPrice, Non_Recurring_Billing_Date__c,
                   Product2Id, Quantity, PricebookEntryId,QuoteId, UnitPrice, Subtotal, Subtotal__c, TotalPrice, Variable_Price_Ceiling__c, Variable_Price_Floor__c, Variable_Price_Unit_Increase__c,Agency_fee_frequency__c,List_Price__c
            from QuoteLineItem 
            where QuoteId = :quoteId and PricebookEntry.Product2.Family = 'Setup Fee' 
            order by CreatedDate desc
        ];
    }

    public virtual QuoteLineItem proServicesOneFeeAndPaymentsForQuote(String quoteId){
        return [
            select  Id, Fee_Subtype__c,Has_Relevant_Changes_for_Offer_Deal_Desk__c,Has_Relevant_Changes_for_Offer_Approval__c,Approval_Status__c, Fee_Contract_Type__c,Fee_Type__c, Discount, Discount__c,Discount_Approval_Needed__c,Discount_Approved__c, Flat_Baseline_Adjustment__c, Flat_Baseline_Quantity__c, Flat_Unit_Price__c, Inflation_Adjustment_Index__c, Inflation_Adjustment_Period__c, Description, LineNumber, ListPrice, Non_Recurring_Billing_Date__c,
                    Product2Id, Quantity,PricebookEntryId, QuoteId, UnitPrice, Subtotal, Subtotal__c, TotalPrice, Variable_Price_Ceiling__c, Variable_Price_Floor__c, Variable_Price_Unit_Increase__c,Agency_fee_frequency__c,List_Price__c,
                    ( SELECT Id,Inflation_Adjustment_Index__c,Percentage__c,Frequency__c, Inflation_Adjustment_Period__c,Non_Recurring_Billing_Date__c,
                    Payment_Due_Days__c,Billing_Day__c,Custom_Billing_Day__c,Custom_Payment_Due_Days__c,Quote_Line_Item__c,Has_Relevant_Changes_for_Offer_Approval__c,Has_Relevant_Changes_for_Offer_Deal_Desk__c, Payment_Method__c,Recurring_Cutoff_Day__c,Cutoff_Day__c,Recurring_Billing_Period__c,Billing_Due_Date__c,Account__c FROM Payments__r ), ( SELECT Id FROM Eligibilities__r ), ( SELECT Id, Approval_Status__c, Approval_level__c FROM Waivers__r ) 
            from QuoteLineItem 
            where QuoteId = :quoteId and PricebookEntry.Product2.Name = 'Professional Services Setup Fee'  
            order by CreatedDate desc limit 1
        ];
    }

    public virtual QuoteLineItem proServicesOneFeeForQuote(String quoteId){
        return [
            select  Id, Fee_Subtype__c, Has_Relevant_Changes_for_Offer_Deal_Desk__c,Has_Relevant_Changes_for_Offer_Approval__c,Approval_Status__c,Fee_Contract_Type__c,Fee_Type__c, Discount, Discount__c, Flat_Baseline_Adjustment__c, Discount_Approval_Needed__c,Discount_Approved__c, Flat_Baseline_Quantity__c, Flat_Unit_Price__c, Inflation_Adjustment_Index__c, Inflation_Adjustment_Period__c, Description, LineNumber, ListPrice, Non_Recurring_Billing_Date__c,
                    Product2Id, Quantity,PricebookEntryId, QuoteId, UnitPrice, Subtotal, Subtotal__c, TotalPrice, Variable_Price_Ceiling__c, Variable_Price_Floor__c, Variable_Price_Unit_Increase__c,Agency_fee_frequency__c,List_Price__c
            from QuoteLineItem 
            where QuoteId = :quoteId and PricebookEntry.Product2.Name = 'Professional Services Setup Fee'  
            order by CreatedDate desc limit 1
        ];
    }

    public virtual QuoteLineItem proServicesMainFeeAndPaymentsForQuote(String quoteId){
        return [
            select  Id, Fee_Subtype__c,Has_Relevant_Changes_for_Offer_Deal_Desk__c, Has_Relevant_Changes_for_Offer_Approval__c,Approval_Status__c,Fee_Contract_Type__c,Fee_Type__c, Discount, Discount__c,Discount_Approval_Needed__c,Discount_Approved__c, Flat_Baseline_Adjustment__c, Flat_Baseline_Quantity__c, Flat_Unit_Price__c, Inflation_Adjustment_Index__c, Inflation_Adjustment_Period__c, Description, LineNumber, ListPrice, Non_Recurring_Billing_Date__c,
                    Product2Id, Quantity, PricebookEntryId,QuoteId, UnitPrice, Subtotal, Subtotal__c, TotalPrice, Variable_Price_Ceiling__c, Variable_Price_Floor__c, Variable_Price_Unit_Increase__c,Agency_fee_frequency__c,List_Price__c,
                    ( SELECT Id,Inflation_Adjustment_Index__c, Percentage__c,Frequency__c,Inflation_Adjustment_Period__c,Non_Recurring_Billing_Date__c,
                    Payment_Due_Days__c,Billing_Day__c,Custom_Billing_Day__c,Custom_Payment_Due_Days__c,Quote_Line_Item__c, Payment_Method__c,Recurring_Cutoff_Day__c,Cutoff_Day__c,Recurring_Billing_Period__c,Billing_Due_Date__c,Account__c,Has_Relevant_Changes_for_Offer_Approval__c,Has_Relevant_Changes_for_Offer_Deal_Desk__c FROM Payments__r ), ( SELECT Id FROM Eligibilities__r ), ( SELECT Id, Approval_Status__c, Approval_level__c FROM Waivers__r ) 
            from QuoteLineItem 
            where QuoteId = :quoteId and PricebookEntry.Product2.Name = 'Professional Services Maintenance Fee' 
            order by CreatedDate desc limit 1
        ];
    }

    public virtual QuoteLineItem proServicesMainFeeForQuote(String quoteId){
        return [
            select  Id, Fee_Subtype__c,Has_Relevant_Changes_for_Offer_Deal_Desk__c,Has_Relevant_Changes_for_Offer_Approval__c,Approval_Status__c,Fee_Contract_Type__c, Fee_Type__c, Discount, Discount__c,Discount_Approval_Needed__c,Discount_Approved__c, Flat_Baseline_Adjustment__c, Flat_Baseline_Quantity__c, Flat_Unit_Price__c, Inflation_Adjustment_Index__c, Inflation_Adjustment_Period__c, Description, LineNumber, ListPrice, Non_Recurring_Billing_Date__c,
                    Product2Id, Quantity, PricebookEntryId,QuoteId, UnitPrice, Subtotal, Subtotal__c, TotalPrice, Variable_Price_Ceiling__c, Variable_Price_Floor__c, Variable_Price_Unit_Increase__c,Agency_fee_frequency__c,List_Price__c
            from QuoteLineItem 
            where QuoteId = :quoteId and PricebookEntry.Product2.Name = 'Professional Services Maintenance Fee' 
            order by CreatedDate desc limit 1
        ];
    }

     public virtual List<QuoteLineItem> all(String quoteId){
        return [select  Id, Fee_Subtype__c,Has_Relevant_Changes_for_Offer_Deal_Desk__c,Has_Relevant_Changes_for_Offer_Approval__c,Approval_Status__c,Fee_Contract_Type__c, Fee_Type__c, Discount, Discount__c,Discount_Approval_Needed__c,Discount_Approved__c, Flat_Baseline_Adjustment__c, Flat_Baseline_Quantity__c, Flat_Unit_Price__c, Inflation_Adjustment_Index__c, Inflation_Adjustment_Period__c, Description, LineNumber, ListPrice, Non_Recurring_Billing_Date__c,
                    Product2Id, Quantity, PricebookEntry.Product2.Family,PricebookEntry.Product2.Name,PricebookEntryId,QuoteId, UnitPrice, Subtotal, Subtotal__c, TotalPrice, Variable_Price_Ceiling__c, Variable_Price_Floor__c, Variable_Price_Unit_Increase__c,List_Price__c
            from QuoteLineItem 
            where QuoteId = :quoteId order by CreatedDate desc];
    }

    public virtual List<QuoteLineItem> allForOpportunity(String opportunityId){
        List<QuoteLineItem> allQuotes = new List<QuoteLineItem>();
        allQuotes = [SELECT  Id, Fee_Type__c
                    FROM  QuoteLineItem 
                    WHERE Quote.OpportunityId  = :opportunityId ORDER BY CreatedDate desc];
        
        return allQuotes;
    }

    public static List<QuoteLineItem> feesPaymentsForQuote(List<Id> ltQuoteIds){
        return [
            SELECT  Id, PricebookEntry.Product2.Name, PricebookEntry.Product2.Family, QuoteId,Agency_fee_frequency__c,Quote.OpportunityId,
                    ( SELECT Id,Quote_Line_Item__r.Quote.OpportunityId,Inflation_Adjustment_Index__c,Inflation_Adjustment_Period__c,Non_Recurring_Billing_Date__c,
                    Payment_Due_Days__c,Billing_Day__c,Custom_Billing_Day__c,Custom_Payment_Due_Days__c, Quote_Line_Item__c, Has_Relevant_Changes_for_Offer_Approval__c,Has_Relevant_Changes_for_Offer_Deal_Desk__c,Payment_Method__c,Percentage__c,Frequency__c,Cutoff_Day__c,Recurring_Cutoff_Day__c,Recurring_Billing_Period__c,Billing_Due_Date__c,Account__c FROM Payments__r )
            FROM QuoteLineItem 
            WHERE QuoteId IN :ltQuoteIds 
                AND (PricebookEntry.Product2.Name IN ('Setup Fee', 'Professional Services Setup Fee', 'Professional Services Maintenance Fee') 
                    OR PricebookEntry.Product2.Family IN ('Enterprise Subscription'))
            ORDER BY CreatedDate desc
        ];    
    }
    
    public static List<QuoteLineItem> feesPaymentsForOpp(List<Id> oppIds){
        return [
            SELECT  Id, PricebookEntry.Product2.Name, PricebookEntry.Product2.Family, QuoteId,Quote.OpportunityId,
                    ( SELECT Id,Quote_Line_Item__r.Quote.OpportunityId,Inflation_Adjustment_Index__c,Inflation_Adjustment_Period__c,Non_Recurring_Billing_Date__c,
                    Payment_Due_Days__c,Billing_Day__c,Custom_Billing_Day__c,Custom_Payment_Due_Days__c, Quote_Line_Item__c, Has_Relevant_Changes_for_Offer_Approval__c,Has_Relevant_Changes_for_Offer_Deal_Desk__c,Payment_Method__c,Percentage__c,Frequency__c,Cutoff_Day__c,Recurring_Cutoff_Day__c,Recurring_Billing_Period__c,Billing_Due_Date__c,Account__c FROM Payments__r )
            FROM QuoteLineItem 
            WHERE Quote.OpportunityId IN :oppIds 
                AND (PricebookEntry.Product2.Name IN ('Setup Fee', 'Professional Services Setup Fee', 'Professional Services Maintenance Fee') 
                    OR PricebookEntry.Product2.Family IN ('Enterprise Subscription'))
            ORDER BY CreatedDate desc
        ];    
    }

    public List<QuoteLineItem> getQuoteItemsBySetOpps(Set<Id> setOppIds){
        return [SELECT Id, Product_Fee_type__c, Quote.OpportunityId, Product2.Has_Fixed_Quantity__c, Product2Id, Quantity FROM QuoteLineItem WHERE Quote.OpportunityId IN :setOppIds];
    }
    
      /**
     * throwable System.DMLException
     */
    public virtual QuoteLineItem add(QuoteLineItem record){
        Database.insert(record);
        return record;
    }

    /**
     * throwable System.DMLException
     */
    public virtual QuoteLineItem edit(QuoteLineItem record){
        return edit(new List<QuoteLineItem>{record}).get(0);
    }

    public virtual List<QuoteLineItem> edit(List<QuoteLineItem> recordLst){
        Database.update(recordLst);
        return recordLst;
    }


    /**
     * throwable System.DMLException
     */
    public virtual QuoteLineItem addOrEdit(QuoteLineItem record){
        Database.upsert(record, QuoteLineItem.Id, true);
        return record;
    }
    
    //Method used to save QuoteLineItem list /* Eric Torres 24/05/2021 */
    public virtual void addOrEditList(List<QuoteLineItem> lsRecord){
        Database.upsert(lsRecord, true);
        //return lsRecord;
    }

    public virtual void clearItemsForQuote(String oppId){
        delete [select Id from QuoteLineItem where Quote.OpportunityId=:oppId];
    }


}