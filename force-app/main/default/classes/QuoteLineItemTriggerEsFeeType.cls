public class QuoteLineItemTriggerEsFeeType {
    
    private List<QuoteLineItem> m_lstDataNew;
    private Map<Id, QuoteLineItem> m_mapDataOld;   
    private Map<Id, QuoteLineItem> m_mapObjFiltered = new Map<Id, QuoteLineItem>();
    public static Id OPPORTUNITY_RECORD_TYPE_NEW_BUS_INDIRECT_CHANNEL = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Indirect_Channel_New_Business').getRecordTypeId();
    
    QuoteRepository quoteRepository = new QuoteRepository();
    QuoteLineItemRepository quoteLineItemRepository = new QuoteLineItemRepository();
    OpportunityRepository opportunityRepository = new OpportunityRepository();
    
    public QuoteLineItemTriggerEsFeeType(List<QuoteLineItem> lstDataNew, Map<Id, QuoteLineItem> mapDataOld) {
        this.m_lstDataNew = lstDataNew;
        this.m_mapDataOld = mapDataOld;
    }
    
    private void filter() {
        String QuoteId;
        for(QuoteLineItem itemNew : this.m_lstDataNew) {
            QuoteId = itemNew.QuoteId;
            break;
        }
        Quote quote = quoteRepository.byId(QuoteId); 
        Opportunity opp = opportunityRepository.byId(quote.OpportunityId);
        for(QuoteLineItem itemNew : this.m_lstDataNew) {
            if(opp.RecordType.DeveloperName == 'Indirect_Channel_New_Business'){
                if(itemNew.Fee_Contract_Type__c == null){
                    if(opp.B_M__c == 'Intermediation'){
                        itemNew.Fee_Contract_Type__c = 'Flat Fee';                      
                    }else{
                        itemNew.Fee_Contract_Type__c = 'Variable per eligible';
                    }
                    m_mapObjFiltered.put(itemNew.Id, itemNew);
                }
            }
            
        }
    } 
    
    public void run(){
        
        filter();
        
        if(!m_mapObjFiltered.isEmpty()){
            updateEsFeeType(m_mapObjFiltered);
        }
        
    }
    
    public void updateEsFeeType(Map<Id, QuoteLineItem> m_mapObjFiltered){
        
        List<QuoteLineItem> listQuoteLineItem = new List<QuoteLineItem>();
        for(QuoteLineItem item : m_mapObjFiltered.values()){ 
            listQuoteLineItem.add(item);  
        }

    }
}