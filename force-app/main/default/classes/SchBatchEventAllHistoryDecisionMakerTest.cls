/*
* @Author: Bruno Pinho
* @Date: January/2019
* @Description: Test class for SchBatchEventAllHistoryDecisionMaker
*/
@isTest(SeeAllData=true)
private class SchBatchEventAllHistoryDecisionMakerTest
{    
    static void SetUp()
    {
        test.StartTest();
    }
    
    static void TearDown()
    {
        test.StopTest();
    }
    
    public static testmethod void Test_execute()
    {
        SetUp();
        Datetime sysTime = System.now().addDays(1);
        String chron_exp = '' + sysTime.second() + ' ' + sysTime.minute() + ' ' + sysTime.hour() + ' ' + sysTime.day() + ' ' + sysTime.month() + ' ? ' + sysTime.year();
        String strName = 'TestSchBatchEventAllHistoryDecisionMakers' + sysTime.day() + '/' + sysTime.month() + '/' + sysTime.year() + ' - '+ sysTime.hour() + ':' + sysTime.minute() + ':' + sysTime.second();
        System.schedule(strName, chron_exp, new SchBatchEventAllHistoryDecisionMakers());
        TearDown();
    }
}