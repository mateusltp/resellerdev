public with sharing class TaxIdBrazilWrapper {

    public List<AtividadePrincipal> atividade_principal;
    public String data_situacao;//ok
    public String complemento;//ok
    public String tipo;//ok
    public String nome;//ok
    public String uf;//ok
    public String telefone;//ok
    public String email;//ok
    public List<AtividadesSecundaria> atividades_secundarias;
    public List<Qsa> qsa;
    public String situacao;//ok
    public String bairro;//ok
    public String logradouro;//ok
    public String numero;//ok
    public String cep;//ok
    public String municipio;//ok
    public String porte;//ok
    public String abertura;//ok
    public String natureza_juridica;//ok
    public String fantasia;//ok
    public String cnpj;//ok
    //public Date ultima_atualizacao;//ok
    public String status;//ok
    public String efr;
    public String motivo_situacao;//ok
    public String situacao_especial;//ok
    public String data_situacao_especial;//ok
    public String capital_social;
    public Billing billing;
    
    public class AtividadePrincipal{
        public String text;
        public String code;
    }
    
    public class AtividadesSecundaria{
        public String text;
        public String code;
    }
    
    public class Qsa{
        public String qual;
        public String nome;
    }
    
    public class Billing{
        public boolean free;
        public boolean database;
    }
}