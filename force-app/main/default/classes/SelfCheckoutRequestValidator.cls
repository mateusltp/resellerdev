public without sharing class SelfCheckoutRequestValidator {
  private SelfCheckoutRequest request;

  public SelfCheckoutRequestValidator(SelfCheckoutRequest request) {
    this.request = request;
  }

  public void validatePostRequest() {
    List<String> errorMessages = getAllErrors();

    if (!errorMessages.isEmpty()) {
      IntegrationException.setMessages(errorMessages);

      throw new IntegrationException(errorMessages.toString());
    }
  }

  private List<String> getAllErrors() {
    List<String> errorMessages = new List<String>();

    if (request == null) {
      errorMessages.add('Request body cannot be empty');
      return errorMessages;
    }

    errorMessages.addAll(getAllAccountErrors());

    errorMessages.addAll(getAllOpportunityErrors());

    return errorMessages;
  }

  private List<String> getAllAccountErrors() {
    List<String> errorMessages = new List<String>();

    AccountDTO accountDTO = request.getAccountDTO();

    if (accountDTO == null) {
      errorMessages.add('account field is requidred');
      return errorMessages;
    }

    for(String missingRequiredField : accountDTO.getMissingRequiredFields()) {
      errorMessages.add('The account required field ' + missingRequiredField + ' is missing');
    }

    if (accountDTO.getContacts() != null) {
      for(AccountDTO.ContactDTO contactDTO : accountDTO.getContacts()) {
        for(String missingRequiredField : contactDTO.getMissingRequiredFields()) {
          errorMessages.add('The contact required field ' + missingRequiredField + ' is missing');
        }
      }
    }

    return errorMessages;
  }

  private List<String> getAllOpportunityErrors() {
    List<String> errorMessages = new List<String>();

    if (request.getOpportunityDTO() == null) {
      errorMessages.add('opportunity field is requidred');
      return errorMessages;
    }

    return errorMessages;
  }
}