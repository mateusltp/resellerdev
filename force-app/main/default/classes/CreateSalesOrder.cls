/**
 * @description       : partnerqa
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 10-13-2021
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/
public without sharing class CreateSalesOrder {

  private List<Opportunity> partnersOppLst = new List<Opportunity>();
  
  public void execute() {
    List<Opportunity> opportunities = new List<Opportunity>();

    for (Opportunity newOpportunity : (List<Opportunity>) Trigger.new) {   
      if(isToCreateOrder(newOpportunity)){      
          opportunities.add(newOpportunity);
        } 
    }   
   
    if (opportunities.isEmpty()) {
      return;
    }

    List<Quote> quotes = queryQuotes(opportunities);

    if (quotes.isEmpty()) {
      return;
    }

    List<Account_Opportunity_Relationship__c> splitBillings = querySplitBillings(quotes);

    List<QuoteLineItem> quoteLineItems = queryQuoteLineItems(quotes);

    List<Payment__c> quoteLinePayments = queryPayments(quoteLineItems);

    List<Eligibility__c> quoteLineEligibles = queryEligibles(quoteLinePayments);

    Map<Id, List<Account_Opportunity_Relationship__c>> spliBillingsByOpportunityId = (
      getMapSplitBillingsByOpportunityId(splitBillings)
    );

    Map<Id, List<Eligibility__c>> eligiblesByPaymentId = getMapEligiblesByPaymentId(quoteLineEligibles);

    List<Waiver__c> quoteLineWaivers = queryWaivers(quoteLinePayments);

    Map<Id, List<QuoteLineItem>> quoteItemsByQuoteId = getMapQuoteItemsByQuoteId(
      quoteLineItems
    );

    Map<Id, List<Payment__c>> paymentsByQuoteLineItemId = getMapPaymentsByQuoteLineItemId(
      quoteLinePayments
    );

    Map<Id, List<Waiver__c>> waiversByPaymentId = getMapWaiversByPaymentId(quoteLineWaivers);

    List<Order> orders = new List<Order>();
    List<OrderItem> orderItems = new List<OrderItem>();
    List<Payment__c> orderPayments = new List<Payment__c>();
    List<Eligibility__c> orderEligibles = new List<Eligibility__c>();
    List<Waiver__c> orderWaivers = new List<Waiver__c>();

    for (Quote quote : quotes) {
      Order order = new Order(
        AccountId = quote.Opportunity.AccountId,
        ContractId = quote.ContractId,
        CurrencyIsoCode = quote.CurrencyIsoCode,
        EffectiveDate = quote.Start_Date__c,
        EndDate = quote.End_Date__c,
        OpportunityId = quote.OpportunityId,
        Pricebook2Id = quote.Pricebook2Id,
        QuoteId = quote.Id,
        Status = 'Inactivated',
        Type = quote.Opportunity.Type,
        Contact_Permission__c = quote.Contact_Permission__c,
        Employee_Registration_Method__c = quote.Employee_Registration_Method__c,
        Gympass_Entity__c = quote.Opportunity.Gympass_Entity__c,
        Unique_Identifier__c = quote.Unique_Identifier__c
      );
      orders.add(order);
    }

    Database.insert(orders);

    for (Order order : orders) {
      List<QuoteLineItem> quoteItems = quoteItemsByQuoteId.get(
        order.QuoteId
      );

      for (QuoteLineItem quoteLineItem : quoteItems) {
        OrderItem orderItem = new OrderItem(
          OrderId = order.Id,
          ListPrice = quoteLineItem.ListPrice,
          PricebookEntryId = quoteLineItem.PricebookEntryId,
          Product2Id = quoteLineItem.Product2Id,
          Quantity = quoteLineItem.Quantity,
          QuoteLineItemId = quoteLineItem.Id,
          UnitPrice = quoteLineItem.Discount__c != null
            ? quoteLineItem.UnitPrice * (1 - quoteLineItem.Discount__c/100)
            : quoteLineItem.UnitPrice,
          Contract_Type__c = quoteLineItem.Fee_Contract_Type__c,
          Discount__c = quoteLineItem.Discount__c,
          Discount_Price_Ceiling__c = quoteLineItem.Discount_Price_Ceiling__c,
          Discount_Price_Floor__c = quoteLineItem.Discount_Price_Floor__c,
          Discount_Price_Unit_Decrease__c = quoteLineItem.Discount_Price_Unit_Decrease__c,
          Flat_Baseline_Adjustment__c = quoteLineItem.Flat_Baseline_Adjustment__c,
          Flat_Baseline_Quantity__c = quoteLineItem.Quantity,
          Inflation_Adjustment_Index__c = quoteLineItem.Inflation_Adjustment_Index__c,
          Inflation_Adjustment_Period__c = quoteLineItem.Inflation_Adjustment_Period__c,
          Recurring_Billing_Period__c = quoteLineItem.Recurring_Billing_Period__c,
          Variable_Price_Ceiling__c = quoteLineItem.Variable_Price_Ceiling__c,
          Variable_Price_Floor__c = quoteLineItem.Variable_Price_Floor__c,
          Variable_Price_Unit_Increase__c = quoteLineItem.Variable_Price_Unit_Increase__c,
          Type__c = quoteLineItem.Fee_Type__c
        );

        orderItems.add(orderItem);
      }
    }

    if (!orderItems.isEmpty()) {
      Database.insert(orderItems);
    }

    for (OrderItem orderItem : orderItems) {
      List<Payment__c> quotePayments = paymentsByQuoteLineItemId.get(
        orderItem.QuoteLineItemId
      );

      if (quotePayments[0].Payment_Due_Days__c.equals('Custom')) {
        orderItem.Payment_Due_Days__c = (Integer) quotePayments[0].Custom_Payment_Due_Days__c;
      } else {
        orderItem.Payment_Due_Days__c = Integer.valueOf(
          quotePayments[0].Payment_Due_Days__c.remove(' days')
        );
      }

      if (quotePayments[0].Billing_Day__c.equals('Custom')) {
        orderItem.Cutoff_Day__c = (Integer) quotePayments[0].Custom_Billing_Day__c;
        orderItem.Billing_Day__c = (Integer) quotePayments[0].Custom_Billing_Day__c;

      } else {
        orderItem.Cutoff_Day__c = Integer.valueOf(quotePayments[0].Billing_Day__c);
        orderItem.Billing_Day__c = Integer.valueOf(quotePayments[0].Billing_Day__c);
      }
      orderItem.Recurring_Billing_Period__c = quotePayments[0].Frequency__c;

      for (Payment__c quotePayment : quotePayments) {
        List<Account_Opportunity_Relationship__c> quoteSplitBillings = (
          spliBillingsByOpportunityId.get(quotePayment.Quote_Line_Item__r.Quote.OpportunityId)
        );
        for(Account_Opportunity_Relationship__c qupteSpliBilling : quoteSplitBillings) {
          Payment__c orderPayment = quotePayment.clone(false, true);
          orderPayment.Order_Item__c = orderItem.Id;
          orderPayment.Account__c = qupteSpliBilling.Account__c;
          orderPayment.Opportunity_Payment__c = quotePayment.Id;

          if (orderItem.Type__c.contains('Setup Fee')) {
            if (orderItem.Type__c.contains('Professional Services')) {
              orderPayment.Percentage__c = qupteSpliBilling.Prof_Serv_Setup_Fee_Billing_Percentage__c;
            } else {
              orderPayment.Percentage__c = qupteSpliBilling.Setup_Fee_Billing_Percentage__c;
            }
          }

          if (orderItem.Type__c.contains('Enterprise Subscription')) {
            if (orderItem.Type__c.contains('Professional Services')) {
              orderPayment.Percentage__c = qupteSpliBilling.Maintenance_Fee_Billing_Percentage__c;
            } else {
              orderPayment.Percentage__c = qupteSpliBilling.Billing_Percentage__c;
            }
          }

          orderPayment.Quote_Line_Item__c = null;

          orderPayments.add(orderPayment);
        }
      }
    }

    Database.update(orderItems);

    Database.insert(orderPayments);

    for(Payment__c orderPayment : orderPayments) {
      if (!eligiblesByPaymentId.containsKey(orderPayment.Opportunity_Payment__c)) {
        continue;
      }

      List<Eligibility__c> quoteEligibles = eligiblesByPaymentId.get(orderPayment.Opportunity_Payment__c);

      for(Eligibility__c quoteEligible : quoteEligibles) {
        Eligibility__c orderEligible = quoteEligible.clone(false, true);
        orderEligible.Payment__c = orderPayment.Id;

        orderEligibles.add(orderEligible);
      }

      if (!waiversByPaymentId.containsKey(orderPayment.Opportunity_Payment__c)) {
        continue;
      }

      List<Waiver__c> quoteWaivers = waiversByPaymentId.get(orderPayment.Opportunity_Payment__c);

      for (Waiver__c quoteWaiver : quoteWaivers) {
        Waiver__c orderWaiver = quoteWaiver.clone(false, true);
        orderWaiver.Payment__c = orderPayment.Id;

        orderWaivers.add(orderWaiver);
      }
    }

    Database.insert(orderEligibles);
    Database.insert(orderWaivers);

    for(Order order : orders) {
      order.Status = 'Activated';
    }

    Database.update(orders);
  }

  private List<Quote> queryQuotes(List<Opportunity> opportunities) {
    return [
      SELECT
        Id,
        ContractId,
        CurrencyIsoCode,
        Pricebook2Id,
        OpportunityId,
        Opportunity.AccountId,
        Opportunity.Type,
        Opportunity.Gympass_Entity__c,
        Contact_Permission__c,
        Employee_Registration_Method__c,
        End_Date__c,
        Start_Date__c,
        Unique_Identifier__c
      FROM Quote
      WHERE
        OpportunityId IN :opportunities
        AND Account.Send_To_Tagus__c = true
        AND Opportunity.StageName = 'Lançado/Ganho'
        AND Opportunity.RecordType.DeveloperName IN ('Client_Sales_New_Business','Client_Success_Renegotiation','SMB_New_Business','SMB_Success_Renegotiation')
      WITH SECURITY_ENFORCED
    ];
  }

  private Boolean isToCreateOrder(Opportunity newOpportunity) {
    Map<Id, Opportunity> oldOpportunityMap = (Map<Id, Opportunity>) Trigger.oldMap;
    Opportunity oldOpportunity = oldOpportunityMap.get(newOpportunity.Id);

    return (
      newOpportunity.StageName.equals('Lançado/Ganho') &&
      oldOpportunity.StageName != newOpportunity.StageName
    );
  }

  private List<QuoteLineItem> queryQuoteLineItems(List<Quote> quotes) {
    return [
      SELECT
        Id,
        Discount__c,
        ListPrice,
        Product2Id,
        PricebookEntryId,
        Quantity,
        Quote.OpportunityId,
        UnitPrice,
        Fee_Contract_Type__c,
        Discount_Price_Ceiling__c,
        Discount_Price_Floor__c,
        Discount_Price_Unit_Decrease__c,
        Flat_Baseline_Adjustment__c,
        Fee_Type__c,
        Inflation_Adjustment_Index__c,
        Inflation_Adjustment_Period__c,
        Recurring_Billing_Period__c,
        Variable_Price_Ceiling__c,
        Variable_Price_Floor__c,
        Variable_Price_Unit_Increase__c
      FROM QuoteLineItem
      WHERE QuoteId IN :quotes
      WITH SECURITY_ENFORCED
    ];
  }

  private Map<Id, List<QuoteLineItem>> getMapQuoteItemsByQuoteId(
    List<QuoteLineItem> items
  ) {
    Map<Id, List<QuoteLineItem>> itemsByQuote = new Map<Id, List<QuoteLineItem>>();

    for (QuoteLineItem quoteLineItem : items) {
      if (itemsByQuote.containsKey(quoteLineItem.QuoteId)) {
        itemsByQuote.get(quoteLineItem.QuoteId)
          .add(quoteLineItem);
      } else {
        itemsByQuote.put(
          quoteLineItem.QuoteId,
          new List<QuoteLineItem>{ quoteLineItem }
        );
      }
    }

    return itemsByQuote;
  }

  private List<Payment__c> queryPayments(List<QuoteLineItem> quoteLineItems) {
    return [
      SELECT
        Id,
        Billing_Day__c,
        Custom_Billing_Day__c,
        Custom_Payment_Due_Days__c,
        Frequency__c,
        Opportunity_Payment__c,
        Payment_Due_Days__c,
        Quote_Line_Item__c,
        Quote_Line_Item__r.Quote.OpportunityId,
        Recurring_Billing_Period__c,
        Recurring_Cutoff_Day__c,
        UUID__c
      FROM Payment__c
      WHERE Quote_Line_Item__c IN :quoteLineItems
      WITH SECURITY_ENFORCED
    ];
  }

  private Map<Id, List<Payment__c>> getMapPaymentsByQuoteLineItemId(
    List<Payment__c> payments
  ) {
    Map<Id, List<Payment__c>> paymentsByQuoteLineItemId = new Map<Id, List<Payment__c>>();

    for (Payment__c payment : payments) {
      if (paymentsByQuoteLineItemId.containsKey(payment.Quote_Line_Item__c)) {
        paymentsByQuoteLineItemId.get(payment.Quote_Line_Item__c).add(payment);
      } else {
        paymentsByQuoteLineItemId.put(
          payment.Quote_Line_Item__c,
          new List<Payment__c>{ payment }
        );
      }
    }

    return paymentsByQuoteLineItemId;
  }

  private Map<Id, List<Eligibility__c>> getMapEligiblesByPaymentId(List<Eligibility__c> eligibles) {
    Map<Id, List<Eligibility__c>> eligiblesByPaymentId = new Map<Id, List<Eligibility__c>>();

    for(Eligibility__c eligible : eligibles) {
      if (eligiblesByPaymentId.containsKey(eligible.Payment__c)) {
        eligiblesByPaymentId.get(eligible.Payment__c).add(eligible);
      } else {
        eligiblesByPaymentId.put(eligible.Payment__c, new List<Eligibility__c>{eligible});
      }
    }

    return eligiblesByPaymentId;
  }

  private List<Eligibility__c> queryEligibles(List<Payment__c> payments) {
    return [
      SELECT Id,
      Name,
      Communication_Restriction__c,
      Contact_Permission__c,
      Exchange_Type__c,
      Identifier_Type__c,
      Is_Default__c,
      Max_Dependents__c,
      Payment_Method__c,
      Payment__c,
      Payroll_Deduction__c,
      Recurring_Billing_Period__c
      FROM Eligibility__c
      WHERE Payment__c IN :payments
      WITH SECURITY_ENFORCED
    ];
  }

  private Map<Id, List<Waiver__c>> getMapWaiversByPaymentId(List<Waiver__c> waivers) {
    Map<Id, List<Waiver__c>> waiversByPaymentId = new Map<Id, List<Waiver__c>>();

    for (Waiver__c waiver : waivers) {
      if (waiversByPaymentId.containsKey(waiver.Payment__c)) {
        waiversByPaymentId.get(waiver.Payment__c).add(waiver);
      } else {
        waiversByPaymentId.put(
          waiver.Payment__c,
          new List<Waiver__c>{ waiver }
        );
      }
    }

    return waiversByPaymentId;
  }

  private List<Waiver__c> queryWaivers(List<Payment__c> payments) {
    return [
      SELECT Id,
      End_Date__c,
      Payment__c,
      Percentage__c,
      Start_Date__c
      FROM Waiver__c
      WHERE Payment__c IN :payments
      WITH SECURITY_ENFORCED
    ];
  }

  private Map<Id, List<Account_Opportunity_Relationship__c>> getMapSplitBillingsByOpportunityId(
    List<Account_Opportunity_Relationship__c> splitBillings
  ) {
    Map<Id, List<Account_Opportunity_Relationship__c>> splitBillingsByOpportunityId = (
      new Map<Id, List<Account_Opportunity_Relationship__c>>()
    );

    for(Account_Opportunity_Relationship__c splitBilling : splitBillings) {
      if (splitBillingsByOpportunityId.containsKey(splitBilling.Opportunity__c)) {
        splitBillingsByOpportunityId.get(splitBilling.Opportunity__c).add(splitBilling);
      } else {
        splitBillingsByOpportunityId.put(
          splitBilling.Opportunity__c,
          new List<Account_Opportunity_Relationship__c>{splitBilling}
        );
      }
    }

    return splitBillingsByOpportunityId;
  }

  private List<Account_Opportunity_Relationship__c> querySplitBillings(List<Quote> quotes) {
    Set<Id> opportunityIds = new Set<Id>();

    for(Quote proposal : quotes) {
      opportunityIds.add(proposal.OpportunityId);
    }

    return [
      SELECT Id,
      Account__c,
      Billing_Percentage__c,
      Maintenance_Fee_Billing_Percentage__c,
      Opportunity__c,
      Prof_Serv_Setup_Fee_Billing_Percentage__c,
      Setup_Fee_Billing_Percentage__c
      FROM Account_Opportunity_Relationship__c
      WHERE Opportunity__c IN: opportunityIds
      WITH SECURITY_ENFORCED
    ];
  }
}