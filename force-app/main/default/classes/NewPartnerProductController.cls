/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 05-02-2022
 * @last modified by  : gepi@gft.com
**/
public with sharing class NewPartnerProductController {

    private static final PS_Constants constants = PS_Constants.getInstance();
    private static final String className = NewPartnerProductController.class.getName();


    @AuraEnabled(cacheable=true)
    public static List<Gym_Activity_Relationship__c> showActivities(){
      
        List<Gym_Activity_Relationship__c> activitiesLst = ((GymActivityRelSelector)Application.Selector.newInstance(Gym_Activity_Relationship__c.sobjectType)).selectAllGymActivitiesRel();
        return activitiesLst;
    }


    @AuraEnabled(cacheable=true)
    public static RecordTypesPartnerProduct getRecordTypeIdsFormPartnerProductFlow(){
        RecordTypesPartnerProduct response = new RecordTypesPartnerProduct();
        return response;
    }


    @AuraEnabled(cacheable=true)
    public static OpportunityDataResponse getOpportunityData( String recordId ){
        Opportunity aOpp = ((OpportunitySelector)Application.Selector.newInstance(Opportunity.sobjectType)).selectOpportunityForPartnerProductFlow(new Set<Id> { recordId })[0];
        return new OpportunityDataResponse( aOpp );
    }

    @AuraEnabled(cacheable=false)
    public static String saveProduct( String partnerProductRecord ){
        PartnerProductFlow partnerProduct = (PartnerProductFlow)JSON.deserialize(partnerProductRecord, PartnerProductFlow.class);
        Savepoint sp;
        Product_Item__c aProdItem = partnerProduct.getProduct();
        Set<Id> selectedActivities = new Set<Id>(partnerProduct.getSelectedGymActivities());
        List<Commercial_Condition__c> commercialLst = partnerProduct.getCommercialDetails();
        List<Threshold__c> thresholdLst = partnerProduct.getThresholds();
        Map<String, List<Commercial_Condition__c>> conditionsByDML = new Map<String, List<Commercial_Condition__c>>();
        sp = Database.setSavepoint();
        try{
            if (!String.isBlank(partnerProduct.currentProductId)) {
                aProdItem.Id = partnerProduct.currentProductId;
                conditionsByDML = ProductItemService.deleteProductRelationships(aProdItem, commercialLst);
            } else {
                conditionsByDML.put('INSERT', commercialLst);
            }
            Product_Item__c prodResult = ProductItemService.createNewProduct(partnerProduct.currentOpportunityId,partnerProduct.currentAccountId,aProdItem, selectedActivities, conditionsByDML, thresholdLst);
            return prodResult.Id;

        } catch (Exception e){
            Database.rollback(sp);
            system.debug(e.getMessage() + ' - ' + e.getLineNumber()+ ' - ' + e.getStackTraceString());
            throw new NewPartnerProductControllerException(e.getMessage() + ' - ' + e.getStackTraceString());
          }    
    }


    @AuraEnabled(cacheable=false)
    public static PartnerProductFlow getProductData(String recordId) {
        System.debug('::recordID ' + recordId);
        try {
            return getData(recordId);
        } catch (Exception e) {
            createLog(e.getMessage(), e.getStackTraceString(), 'getData', null, (id)recordId);
            throw new NewPartnerProductControllerException(e.getMessage());
        }

    }

    
    // @AuraEnabled(cacheable=true)
    // public static Boolean replicateEntireThreshold (String recordId){
    //     Set<Id> lSetThresholdIds = new Set<Id>();
    //     Set<Id> lSetProdItemId = new Set<Id>();
    //     ProductOpportunityAssignmentSelector prodOppAssignment = (ProductOpportunityAssignmentSelector)Application.Selector.newInstance(Product_Opportunity_Assignment__c.sobjectType);
    //     List<Product_Opportunity_Assignment__c> lLstProdOppAssignment = prodOppAssignment.selectByOpportunityWithProduct(recordId);
        
    //     if(lLstProdOppAssignment == NULL || lLstProdOppAssignment.isEmpty()){return false;}
    //     for (Product_Opportunity_Assignment__c prodItem : lLstProdOppAssignment){
    //         lSetProdItemId.add(prodItem.ProductAssignmentId__r.ProductId__c);
    //     }

    //     ProductThresholdMemberSelector productThresholdMemberSelector = new ProductThresholdMemberSelector();
    //     List<Product_Threshold_Member__c> productThresholdMembers = productThresholdMemberSelector.selectThresholdByProductId(lSetProdItemId);
        
    //     if(productThresholdMembers == NULL || productThresholdMembers.isEmpty()){return false;}
    //     for (Product_Threshold_Member__c prodThreshold : productThresholdMembers){
    //         if(prodThreshold.Threshold__r.Replicate_Discount__c == TRUE)
    //             {return true;}
    //         }
    // return false;}    

    // @AuraEnabled
    // public static void createThresholdReplicated (String recordId, String productId){
    //     List<Product_Threshold_Member__c> lLstProdThresholdMember = new List<Product_Threshold_Member__c>();
    //     Set<Id> lSetThresholdIds = new Set<Id>();
    //     Set<Id> lSetProdItemId = new Set<Id>();
    //     ProductOpportunityAssignmentSelector prodOppAssignment = (ProductOpportunityAssignmentSelector)Application.Selector.newInstance(Product_Opportunity_Assignment__c.sobjectType);
    //     List<Product_Opportunity_Assignment__c> lLstProdOppAssignment = prodOppAssignment.selectByOpportunityWithProduct(recordId);
        
    //     if(lLstProdOppAssignment == NULL || lLstProdOppAssignment.isEmpty()){return;}
    //     for (Product_Opportunity_Assignment__c prodItem : lLstProdOppAssignment){
    //         lSetProdItemId.add(prodItem.ProductAssignmentId__r.ProductId__c);
    //     }

    //     ProductThresholdMemberSelector productThresholdMemberSelector = new ProductThresholdMemberSelector();
    //     List<Product_Threshold_Member__c> productThresholdMembers = productThresholdMemberSelector.selectThresholdByProductId(lSetProdItemId);
    //     if(productThresholdMembers == NULL || productThresholdMembers.isEmpty()){return;}

    //     for (Product_Threshold_Member__c prodThreshold : productThresholdMembers){
    //             if(prodThreshold.Threshold__r.Replicate_Discount__c == TRUE){
    //                 Product_Threshold_Member__c prodThresholdMember = new Product_Threshold_Member__c(
    //                     Threshold__c  = prodThreshold.Threshold__c,
    //                     Product__c = productId);
    //                     lLstProdThresholdMember.add(prodThresholdMember);
    //             }
    //     }
    //     if(!lLstProdThresholdMember.isEmpty()){
    //         insert lLstProdThresholdMember;
    //     }
    // }

    // @AuraEnabled
    // public static void recreateThresholdEntireNetwork (String recordId, String productId){
    //     Set<Id> lSetThresholdIds = new Set<Id>();
    //     Set<Id> lSetProdItemId = new Set<Id>();
    //     List<Product_Threshold_Member__c> lLstProdThresholdMember = new List<Product_Threshold_Member__c>();
    //     List<Product_Threshold_Member__c> lLstThresholdMemberToDelete = new List<Product_Threshold_Member__c>();
    //     List<Threshold__c> lLstThresholdIdsToDelete = new List<Threshold__c>();
    //     List<Id> lLstThresholdsForReplicate = new List<Id>();
    //     Id aThresholdIdToRecreate;
    //     id aProductIdToRecreate;

    //     ProductOpportunityAssignmentSelector prodOppAssignment = (ProductOpportunityAssignmentSelector)Application.Selector.newInstance(Product_Opportunity_Assignment__c.sobjectType);
    //     List<Product_Opportunity_Assignment__c> lLstProdOppAssignment = prodOppAssignment.selectByOpportunityWithProduct(recordId);
        
    //     if(lLstProdOppAssignment == NULL || lLstProdOppAssignment.isEmpty()){return;}
    //     for (Product_Opportunity_Assignment__c prodItem : lLstProdOppAssignment){
    //         lSetProdItemId.add(prodItem.ProductAssignmentId__r.ProductId__c);
    //     }
    //     ProductThresholdMemberSelector productThresholdMemberSelector = new ProductThresholdMemberSelector();
    //     List<Product_Threshold_Member__c> productThresholdMembers = productThresholdMemberSelector.selectThresholdByProductId(lSetProdItemId);
        
    //     if(productThresholdMembers == NULL || productThresholdMembers.isEmpty()){return;}
    //     for (Product_Threshold_Member__c prodThreshold : productThresholdMembers){
    //         if(prodThreshold.Threshold__r.Replicate_Discount__c == true){
    //                 aThresholdIdToRecreate = prodThreshold.Threshold__r.Id;
    //                 aProductIdToRecreate = prodThreshold.Product__c;
    //         }
    //         if(prodThreshold.Threshold__r.Replicate_Discount__c == false){
    //             lLstThresholdMemberToDelete.add(prodThreshold);
    //             lLstThresholdIdsToDelete.add(prodThreshold.Threshold__r);
    //         }
    //     }
    //     List<Product_Threshold_Member__c> thresholdReplicate = productThresholdMemberSelector.selectThresholdToReplicate(aProductIdToRecreate);
    //     for (Product_Threshold_Member__c threshold : thresholdReplicate){
    //         lLstThresholdsForReplicate.add(threshold.Threshold__r.id);
    //     }        
    //     for (Product_Threshold_Member__c thresholdMember : lLstThresholdMemberToDelete){
    //         for (Id thresholdIds : lLstThresholdsForReplicate){
    //             Product_Threshold_Member__c prodThresholdMember = new Product_Threshold_Member__c(
    //             Threshold__c  = thresholdIds,
    //             Product__c = thresholdMember.Product__c);
    //             lLstProdThresholdMember.add(prodThresholdMember);
    //         }
    //     }

    //     delete(lLstThresholdMemberToDelete);
    //     delete(lLstThresholdIdsToDelete);
    //     insert(lLstProdThresholdMember);
    // }

    @AuraEnabled(cacheable=false)
    public static StandardDiscountsWrapper getStandardDiscount(String accountId){
        Set<String> europe = new Set<String>{'AR','CL','CO','CR','FR','DE','IE','IT','NL','PE','PT','ES','GB','US'};
        Set<String> brazil = new Set<String>{'BR'};
        Set<String> mexico = new Set<String>{'MX'};
        StandardDiscountsWrapper wrapper = new StandardDiscountsWrapper();
        AccountSelector accountSelector = (AccountSelector)Application.Selector.newInstance(Account.sobjectType); 
        Account lAccount = accountSelector.selectContryAccount(accountId);

        List<Standard_Volume_Discount__mdt> StaVolDisList = [SELECT MasterLabel, X1st_Discount_Range1__c, X2nd_Discount_Range2__c,
        X3rd_Discount_Range3__c, X4th_Discount_Range4__c,
        Threshold_Value_1__c, Threshold_Value_2__c,
        Threshold_Value_3__c, Threshold_Value_4__c 
        FROM Standard_Volume_Discount__mdt];
        Map<String, Standard_Volume_Discount__mdt> StaVolDisMap = new Map<String, Standard_Volume_Discount__mdt>();
        for (Standard_Volume_Discount__mdt  sdv : StaVolDisList ) {
             StaVolDisMap.put(sdv.MasterLabel,sdv);
        }

        if(lAccount != NULL){            
            Standard_Volume_Discount__mdt standardDiscount = new Standard_Volume_Discount__mdt();
            if(europe.contains(lAccount.BillingCountryCode)) {
                standardDiscount = StaVolDisMap.get('Europe_South_Cone_US');
            } else if(brazil.contains(lAccount.BillingCountryCode)){                
                standardDiscount = StaVolDisMap.get('Brazil');
            } else if(mexico.contains(lAccount.BillingCountryCode)) {            
                standardDiscount = StaVolDisMap.get('Mexico');
            } else {
                standardDiscount = StaVolDisMap.get('Standard');
            }
            System.debug('Mapa' + standardDiscount);
            wrapper.thresholdStart1 = standardDiscount.Threshold_Value_1__c;
            wrapper.thresholdStart2 = standardDiscount.Threshold_Value_2__c;
            wrapper.thresholdStart3 = standardDiscount.Threshold_Value_3__c;
            wrapper.thresholdStart4 = standardDiscount.Threshold_Value_4__c;
            wrapper.discount1 = standardDiscount.X1st_Discount_Range1__c;
            wrapper.discount2 = standardDiscount.X2nd_Discount_Range2__c;
            wrapper.discount3 = standardDiscount.X3rd_Discount_Range3__c;
            wrapper.discount4 = standardDiscount.X4th_Discount_Range4__c;
        }
         return wrapper;
    }


    private static PartnerProductFlow getData(String recordId) {
        PartnerProductFlow partnerProductFlow = new PartnerProductFlow();

        ProductOpportunityAssignmentSelector productOpportunityAssignmentSelector = new ProductOpportunityAssignmentSelector();
        List<Product_Opportunity_Assignment__c> productOpportunityAssignments = productOpportunityAssignmentSelector.selectAllCommercialConditionsByProductId(recordId);

        if(productOpportunityAssignments.isEmpty()) {
            throw new NewPartnerProductControllerException((constants.PRODUCT_ITEM_NO_OPP_ASSIGNMENTS_ERROR));
        }

        partnerProductFlow.currentAccountId = productOpportunityAssignments[0].OpportunityMemberId__r.Account__c;
        partnerProductFlow.currentOpportunityId = productOpportunityAssignments[0].OpportunityMemberId__r.Opportunity__c;
        partnerProductFlow.currentProductId = productOpportunityAssignments[0].ProductAssignmentId__r.ProductId__c;
        partnerProductFlow.isWishlist = productOpportunityAssignments[0].OpportunityMemberId__r.Account__r.Wishlist__c;

        ProductDetails productDetails = new ProductDetails();
        productDetails.apiName = constants.PRODUCT_ITEM_OBJECT;
        productDetails.fields = new Product_Item__c(
                Id = productOpportunityAssignments[0].ProductAssignmentId__r.ProductId__c,
                Type__c = productOpportunityAssignments[0].ProductAssignmentId__r.ProductId__r.Type__c,
                Price_Visits_Month_Package_Selected__c = productOpportunityAssignments[0].ProductAssignmentId__r.ProductId__r.Price_Visits_Month_Package_Selected__c,
                PR_Activity__c =  productOpportunityAssignments[0].ProductAssignmentId__r.ProductId__r.PR_Activity__c,
                Package_Type__c = productOpportunityAssignments[0].ProductAssignmentId__r.ProductId__r.Package_Type__c,
                Max_Monthly_Visit_Per_User__c = productOpportunityAssignments[0].ProductAssignmentId__r.ProductId__r.Max_Monthly_Visit_Per_User__c,
                CurrencyIsoCode = productOpportunityAssignments[0].ProductAssignmentId__r.ProductId__r.CurrencyIsoCode,
                Name = productOpportunityAssignments[0].ProductAssignmentId__r.ProductId__r.Name,
                Max_Weekly_Times__c = productOpportunityAssignments[0].ProductAssignmentId__r.ProductId__r.Max_Weekly_Times__c
        );
        partnerProductFlow.productDetails = productDetails;

        List<CommercialDetails> commercialDetailsList = new List<CommercialDetails>();
        for (Product_Opportunity_Assignment__c productOpportunityAssignment : productOpportunityAssignments) {
            CommercialDetail commercialDetail = new CommercialDetail();
            commercialDetail.apiName = constants.COMMERCIAL_CONDITION_OBJECT;
            commercialDetail.fields = new Commercial_Condition__c(
                    Id = productOpportunityAssignment.ProductAssignmentId__r.CommercialConditionId__c,
                    Amount__c = productOpportunityAssignment.ProductAssignmentId__r.CommercialConditionId__r.Amount__c,
                    CAP_Discount__c = productOpportunityAssignment.ProductAssignmentId__r.CommercialConditionId__r.CAP_Discount__c,
                    Discount__c = productOpportunityAssignment.ProductAssignmentId__r.CommercialConditionId__r.Discount__c,
                    Discount1__c = productOpportunityAssignment.ProductAssignmentId__r.CommercialConditionId__r.Discount1__c,
                    Discount2__c = productOpportunityAssignment.ProductAssignmentId__r.CommercialConditionId__r.Discount2__c,
                    Discount3__c = productOpportunityAssignment.ProductAssignmentId__r.CommercialConditionId__r.Discount3__c,
                    Discount4__c = productOpportunityAssignment.ProductAssignmentId__r.CommercialConditionId__r.Discount4__c,
                    End_Date__c = productOpportunityAssignment.ProductAssignmentId__r.CommercialConditionId__r.End_Date__c,
                    Exclusivity__c = productOpportunityAssignment.ProductAssignmentId__r.CommercialConditionId__r.Exclusivity__c,
                    Exclusivity_Fee__c = productOpportunityAssignment.ProductAssignmentId__r.CommercialConditionId__r.Exclusivity_Fee__c,
                    Fee_Percentage__c = productOpportunityAssignment.ProductAssignmentId__r.CommercialConditionId__r.Fee_Percentage__c,
                    Frequency__c = productOpportunityAssignment.ProductAssignmentId__r.CommercialConditionId__r.Frequency__c,
                    Installments__c = productOpportunityAssignment.ProductAssignmentId__r.CommercialConditionId__r.Installments__c,
                    Integration_Fee_Deduction__c = productOpportunityAssignment.ProductAssignmentId__r.CommercialConditionId__r.Integration_Fee_Deduction__c,
                    Is_It_Network_Applied__c = productOpportunityAssignment.ProductAssignmentId__r.CommercialConditionId__r.Is_It_Network_Applied__c,
                    Late_cancel__c = productOpportunityAssignment.ProductAssignmentId__r.CommercialConditionId__r.Late_cancel__c,
                    Membership_Type__c = productOpportunityAssignment.ProductAssignmentId__r.CommercialConditionId__r.Membership_Type__c,
                    Network_Value__c = productOpportunityAssignment.ProductAssignmentId__r.CommercialConditionId__r.Network_Value__c,
                    No_show_fee__c = productOpportunityAssignment.ProductAssignmentId__r.CommercialConditionId__r.No_show_fee__c,
                    Number_of_Thresholds__c = productOpportunityAssignment.ProductAssignmentId__r.CommercialConditionId__r.Number_of_Thresholds__c,
                    Restriction__c = productOpportunityAssignment.ProductAssignmentId__r.CommercialConditionId__r.Restriction__c,
                    Start_Date__c = productOpportunityAssignment.ProductAssignmentId__r.CommercialConditionId__r.Start_Date__c,
                    Value__c = productOpportunityAssignment.ProductAssignmentId__r.CommercialConditionId__r.Value__c,
                    Value1__c = productOpportunityAssignment.ProductAssignmentId__r.CommercialConditionId__r.Value1__c,
                    Value2__c = productOpportunityAssignment.ProductAssignmentId__r.CommercialConditionId__r.Value2__c,
                    Value3__c = productOpportunityAssignment.ProductAssignmentId__r.CommercialConditionId__r.Value3__c,
                    Value4__c = productOpportunityAssignment.ProductAssignmentId__r.CommercialConditionId__r.Value4__c,
                    Visits_to_CAP__c = productOpportunityAssignment.ProductAssignmentId__r.CommercialConditionId__r.Visits_to_CAP__c
            );
            CommercialDetails commercialDetails = new CommercialDetails();
            commercialDetails.key = productOpportunityAssignment.ProductAssignmentId__r.CommercialConditionId__r.RecordType.DeveloperName;
            commercialDetails.value = commercialDetail;
            commercialDetailsList.add(commercialDetails);
        }
        partnerProductFlow.commercialDetails = commercialDetailsList;

        ProductThresholdMemberSelector productThresholdMemberSelector = new ProductThresholdMemberSelector();
        List<Product_Threshold_Member__c> productThresholdMembers = productThresholdMemberSelector.selectThresholdByProductId(new Set<Id> {recordId});
        List<Threshold> thresholds = new List<Threshold>();

        for (Product_Threshold_Member__c thMember : productThresholdMembers) {
            Threshold th = new Threshold();
            th.apiName = constants.THRESHOLD_OBJECT;
            th.fields = new Threshold__c(
                    Id = thMember.Threshold__r.Id,
                    Volume_discount_date__c = thMember.Threshold__r.Volume_discount_date__c,
                    Discount__c = thMember.Threshold__r.Discount__c,
                    Threshold_value_start__c = thMember.Threshold__r.Threshold_value_start__c,
                    Volume_Discount_Type__c = thMember.Threshold__r.Volume_Discount_Type__c
            );
            thresholds.add(th);
        }
        partnerProductFlow.thresholds = thresholds;
        ProductActivityRelationshipSelector productActivityRelationshipSelector = new ProductActivityRelationshipSelector();
        List<Product_Activity_Relationship__c> productActivityRelationships = productActivityRelationshipSelector.selectActivityByProductId(new Set<Id> {recordId});

        Set<Id> gymActivityIds = new Set<Id>();
        for (Product_Activity_Relationship__c par : productActivityRelationships) {
            gymActivityIds.add(par.Gym_Activity_Relationship__c);
        }
        partnerProductFlow.selectedGymActivities = new List<Id>(gymActivityIds);
        return partnerProductFlow;
    }

    /**
     *
     * @description Retrieves custom metadata rows from the Partner cmdt from type Regex to validate product fields on the LWC
     * @task IT-2594
     * @version 1.0 - Bruno Mendes 1-June-2022 - created
     */
    @AuraEnabled
    public static List<RegexConfig> getRegexCustomMetadata() {
        List<RegexConfig> regexConfigs = new List<RegexConfig>();
        try {
            for (Partner__mdt partnerMdt : [SELECT QualifiedApiName, Value__c, Message__c FROM Partner__mdt WHERE Type__c = 'Regex']) {
                RegexConfig regexConfig = new RegexConfig();
                regexConfig.apiName = partnerMdt.QualifiedApiName + '__c';
                regexConfig.regex = partnerMdt.Value__c;
                regexConfig.message = partnerMdt.Message__c;

                regexConfigs.add(regexConfig);
            }
            return regexConfigs;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    public class RegexConfig {
        @AuraEnabled
        public String apiName {get; set;}

        @AuraEnabled
        public String regex {get; set;}

        @AuraEnabled
        public String message {get; set;}

    }


    public class PartnerProductFlow {

        @AuraEnabled
        public Id currentAccountId;

        @AuraEnabled
        public Id currentOpportunityId;

        @AuraEnabled
        public Id currentProductId;

        @AuraEnabled
        public ProductDetails productDetails;

        @AuraEnabled
        public List<Id> selectedGymActivities = new List<Id>();

        @AuraEnabled
        public List<CommercialDetails> commercialDetails =  new List<CommercialDetails>();

        @AuraEnabled
        public List<Threshold> thresholds = new List<Threshold>();

        @AuraEnabled
        public Boolean isWishlist;

        public Product_Item__c getProduct(){
            return productDetails.fields;
        }

        public  List<Commercial_Condition__c> getCommercialDetails(){
            List<Commercial_Condition__c> commercialLst = new List<Commercial_Condition__c>();
            for(CommercialDetails commDetail : CommercialDetails){
                commercialLst.add( commDetail.value.fields );
            }
            return commercialLst;
        }

        public List<Id> getSelectedGymActivities(){
            if(this.selectedGymActivities == null ){
                return new List<Id>();
            } else {
                return this.selectedGymActivities;
            }
        }

        public List<Threshold__c> getThresholds(){
            List<Threshold__c> threholdLst = new List<Threshold__c>();
            for(Threshold th : thresholds){
                threholdLst.add( th.fields);
            }
            return threholdLst;
        }
    }

    public class ProductDetails{
        @AuraEnabled
        public String apiName;

        @AuraEnabled
        public Product_Item__c fields;
    }

    public class CommercialDetails{
        @AuraEnabled
        public String key;

        @AuraEnabled
        public CommercialDetail value;
    }

    public class CommercialDetail{
        @AuraEnabled
        public String apiName;

        @AuraEnabled
        public Commercial_Condition__c fields;

        /*@AuraEnabled
        public Decimal marketPrice;

        @AuraEnabled
        public Decimal marketPrice;

        @AuraEnabled
        public Decimal marketPrice;

        @AuraEnabled
        public Decimal marketPrice;*/

    }

    public class Threshold {
        @AuraEnabled
        public String apiName;

        @AuraEnabled
        public Threshold__c fields;
    }



    public class OpportunityDataResponse {

        @AuraEnabled
        public Id accountId {get; set;} 

        @AuraEnabled
        public Boolean isWishlistPartner {get; set;} 

        public OpportunityDataResponse( Opportunity opp ){
            this.accountId = opp.AccountId;
            this.isWishlistPartner = opp.Account.Wishlist__c;
        }
    }

    public class RecordTypesPartnerProduct {
        @AuraEnabled
        public Id productFlowRecordTypeId {get; set;} 

        @AuraEnabled
        public Id advancedPaymentRecordTypeId {get; set;} 

        @AuraEnabled
        public String advancedPaymentRecordTypeName {get; set;} 

        @AuraEnabled    
        public Id cannibalizationRecordTypeId{get; set;} 
        
        @AuraEnabled
        public String cannibalizationRecordTypeName{get; set;}    

        @AuraEnabled
        public Id capRecordTypeId{get;set;} 

        @AuraEnabled
        public String capRecordTypeName{get; set;} 

        @AuraEnabled
        public Id exclusivityRecordTypeId {get; set;} 

        @AuraEnabled
        public String exclusivityRecordTypeName{get; set;} 

        @AuraEnabled
        public Id integrationFeeDeductionRecordTypeId {get; set;} 

        @AuraEnabled
        public String integrationFeeDeductionRecordTypeName {get; set;} 

        @AuraEnabled
        public Id lateCancellationRecordTypeId{get; set;} 

        @AuraEnabled
        public String lateCancellationRecordTypeName{get; set;} 

        @AuraEnabled
        public Id minimumGuaranteedRecordTypeId {get; set;} 

        @AuraEnabled
        public String minimumGuaranteedRecordTypeName{get; set;} 

        @AuraEnabled
        public Id noShowRecordTypeId{get; set;} 

        @AuraEnabled
        public String noShowRecordTypeName {get; set;} 

        @AuraEnabled
        public Id volumeDiscountRecordTypeId {get; set;} 

        @AuraEnabled
        public String volumeDiscountRecordTypeName {get; set;}
        
        @AuraEnabled
        public String tresholdPartnerRecordTypeId {get; set;}

        @AuraEnabled
        public String tresholdPartnerRecordTypeName {get; set;}

        public RecordTypesPartnerProduct(){
            PS_Constants aPSConst = PS_Constants.getInstance();
            this.productFlowRecordTypeId = Schema.SObjectType.Product_Item__c.getRecordTypeInfosByDeveloperName().get(aPSConst.PRODUCT_ITEM_RT_PARTNER_FLOW).getRecordTypeId();
            
            this.advancedPaymentRecordTypeName = aPSConst.COMMERCIAL_CONDITION_RT_ADVANCED_PAYMENT;
            this.advancedPaymentRecordTypeId = Schema.SObjectType.Commercial_Condition__c.getRecordTypeInfosByDeveloperName().get(aPSConst.COMMERCIAL_CONDITION_RT_ADVANCED_PAYMENT).getRecordTypeId();
            
            this.cannibalizationRecordTypeName = aPSConst.COMMERCIAL_CONDITION_RT_CANNIBALIZATION;
            this.cannibalizationRecordTypeId = Schema.SObjectType.Commercial_Condition__c.getRecordTypeInfosByDeveloperName().get(aPSConst.COMMERCIAL_CONDITION_RT_CANNIBALIZATION).getRecordTypeId();
            
            this.capRecordTypeName = aPSConst.COMMERCIAL_CONDITION_RT_CAP;
            this.capRecordTypeId = Schema.SObjectType.Commercial_Condition__c.getRecordTypeInfosByDeveloperName().get(aPSConst.COMMERCIAL_CONDITION_RT_CAP).getRecordTypeId();
            
            this.exclusivityRecordTypeName = aPSConst.COMMERCIAL_CONDITION_RT_EXCLUSIVITY;
            this.exclusivityRecordTypeId = Schema.SObjectType.Commercial_Condition__c.getRecordTypeInfosByDeveloperName().get(aPSConst.COMMERCIAL_CONDITION_RT_EXCLUSIVITY).getRecordTypeId();
            
            this.integrationFeeDeductionRecordTypeName = aPSConst.COMMERCIAL_CONDITION_RT_INTEGRATION_FEE_DEDUCTION;
            this.integrationFeeDeductionRecordTypeId = Schema.SObjectType.Commercial_Condition__c.getRecordTypeInfosByDeveloperName().get(aPSConst.COMMERCIAL_CONDITION_RT_INTEGRATION_FEE_DEDUCTION).getRecordTypeId();
            
            this.lateCancellationRecordTypeName = aPSConst.COMMERCIAL_CONDITION_RT_LATE_CANCELLATION;
            this.lateCancellationRecordTypeId = Schema.SObjectType.Commercial_Condition__c.getRecordTypeInfosByDeveloperName().get(aPSConst.COMMERCIAL_CONDITION_RT_LATE_CANCELLATION).getRecordTypeId();
            
            this.minimumGuaranteedRecordTypeName = aPSConst.COMMERCIAL_CONDITION_RT_MINIMUM_GUARANTEED;
            this.minimumGuaranteedRecordTypeId = Schema.SObjectType.Commercial_Condition__c.getRecordTypeInfosByDeveloperName().get(aPSConst.COMMERCIAL_CONDITION_RT_MINIMUM_GUARANTEED).getRecordTypeId();
            
            this.noShowRecordTypeName = aPSConst.COMMERCIAL_CONDITION_RT_NO_SHOW;
            this.noShowRecordTypeId = Schema.SObjectType.Commercial_Condition__c.getRecordTypeInfosByDeveloperName().get(aPSConst.COMMERCIAL_CONDITION_RT_NO_SHOW).getRecordTypeId();
            
            this.volumeDiscountRecordTypeName = aPSConst.COMMERCIAL_CONDITION_RT_VOLUME_DISCOUNT;
            this.volumeDiscountRecordTypeId = Schema.SObjectType.Commercial_Condition__c.getRecordTypeInfosByDeveloperName().get(aPSConst.COMMERCIAL_CONDITION_RT_VOLUME_DISCOUNT).getRecordTypeId();
        
            this.tresholdPartnerRecordTypeId = Schema.SObjectType.Threshold__c.getRecordTypeInfosByDeveloperName().get(aPSConst.THRESHOLD_RT_PARTNER_FLOW).getRecordTypeId();
            this.tresholdPartnerRecordTypeName = aPSConst.THRESHOLD_RT_PARTNER_FLOW;
        }
    }

    private class StandardDiscountsWrapper {
        @AuraEnabled public Decimal thresholdStart1 {get; set;}
        @AuraEnabled public Decimal thresholdStart2 {get; set;}
        @AuraEnabled public Decimal thresholdStart3 {get; set;}
        @AuraEnabled public Decimal thresholdStart4 {get; set;}
        @AuraEnabled public Decimal discount1 {get; set;}
        @AuraEnabled public Decimal discount2 {get; set;}
        @AuraEnabled public Decimal discount3 {get; set;}
        @AuraEnabled public Decimal discount4 {get; set;}
    }


    @TestVisible
    private static void createLog(String exceptionMsg, String exceptionStackTrace, String methodName, String dataScope, Id objectId) {
        DebugLog__c log = new DebugLog__c(
                Origin__c = '['+className+']['+methodName+']',
                LogType__c = constants.LOGTYPE_ERROR,
                RelatedToSquad__c = constants.LOGTYPE_SQUAD_PARTNERS,
                ExceptionMessage__c = exceptionMsg,
                StackTrace__c = exceptionStackTrace,
                DataScope__c = dataScope,
                ObjectId__c = objectId
        );
        Logger.createLog(log);
    }

    public class NewPartnerProductControllerException extends Exception {} 
   
    
}