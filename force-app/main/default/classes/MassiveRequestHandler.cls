public with sharing class MassiveRequestHandler extends TriggerHandler {

    public MassiveRequestHandler(){}
    
    public override void beforeInsert(){}
    
    public override void afterInsert(){
        new MassiveTriggerName().massiveAddName();
    }
    
    public override void beforeUpdate(){}
    
    public override void afterUpdate(){
        new SendCustomNotification().sendNotifications();
    }
    
}