/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 04-18-2022
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/
public with sharing class PartnerChildOpportunityManagerController {
    
    private static final PS_Constants constants = PS_Constants.getInstance();
    private static final String className = NewPartnerProductController.class.getName();
    private static Boolean showButton = true;

    @AuraEnabled(cacheable=true)
    public static List<List<Account_Item>> getLocationsByLegalGroup(String opportunityId ){ 

        try {
            AccountOpportunitySelector accOppSelector = (AccountOpportunitySelector)Application.Selector.newInstance(Account_Opportunity_Relationship__c.sobjectType);
            Map<Id,Account_Opportunity_Relationship__c> oppMemberByAccountId = new Map<Id,Account_Opportunity_Relationship__c>();
            for(Account_Opportunity_Relationship__c iOppMember: accOppSelector.selectOppMemberByOppID(new Set<Id> {opportunityId})){
                oppMemberByAccountId.put(iOppMember.Account__c, iOppMember);
            }        
            Map<Id, Opportunity> opportunitiesById = ((OpportunitySelector)Application.Selector.newInstance(Opportunity.SObjectType)).selectChildOpportunitiesByMasterOppId(new Set<Id> { opportunityId });
            
            for(Account_Opportunity_Relationship__c iChildOppMember : accOppSelector.selectOppMemberByOppID(opportunitiesById.keySet())){
                System.debug(':: iChildOppMember Acc ' + iChildOppMember.Account__c + ' iChildOppMember Opp ' + iChildOppMember.Opportunity__c);
                oppMemberByAccountId.remove(iChildOppMember.Account__c);
                System.debug(':: oppMemberByAccountId ' + oppMemberByAccountId);
            }

            AccountContractAgreementRelSelector accountContractSelector = (AccountContractAgreementRelSelector)Application.Selector.newInstance(Account_Contract_Agreement_Relationship__c.sobjectType); 
            Map<Id, List<Account_Contract_Agreement_Relationship__c>> accsInContract = accountContractSelector.selectByOpportunityId(new Set<Id>{opportunityId});
            if(!accsInContract.isEmpty()){
                for( Account_Contract_Agreement_Relationship__c iAcc : accsInContract.get( opportunityId )  ){
                    oppMemberByAccountId.remove(iAcc.AccountId__r.Id);
                }
            }
            
            Map<Id, List<Account_Opportunity_Relationship__c>> oppMemberMap = new Map<Id, List<Account_Opportunity_Relationship__c>>();
            for(Account_Opportunity_Relationship__c oppMember: oppMemberByAccountId.values() ){
                if( oppMember.Account__r.Partner_Level__c == constants.PARTNER_LEVEL_LOCATION ){
                    if(oppMemberMap.containsKey(oppMember.Account__r.Legal_Representative__c)){
                        oppMemberMap.get(oppMember.Account__r.Legal_Representative__c).add(oppMember);
                     } else {
                         oppMemberMap.put(oppMember.Account__r.Legal_Representative__c, new List<Account_Opportunity_Relationship__c>{oppMember});
                     }
                }                              
            }
            List<List<Account_Item>> listResponse = new List<List<Account_Item>>();
            if(!oppMemberMap.isEmpty()){
                for(Id oppMemberId : oppMemberMap.keySet()){
                    List<Account_Item> accountItemLst = new List<Account_Item>();
                    for(Account_Opportunity_Relationship__c oppMember : oppMemberMap.get(oppMemberId)){
                        Account_Item iAccount = new Account_Item(oppMember.Account__c, oppMember.Account__r.Name, 
                                                                oppMember.Account__r.Partner_Level__c, oppMember.Account__r.Legal_Representative__c,
                                                                oppMember.Account__r.Legal_Representative__r.Name);
                        accountItemLst.add(iAccount);
                    }
                    listResponse.add(accountItemLst);
                }
            }
            return listResponse;
        } catch(Exception e){
            DebugLog__c log = new DebugLog__c(
                Origin__c = '['+className+'][getLocationsByLegalGroup]',
                LogType__c = constants.LOGTYPE_ERROR,
                RelatedToSquad__c = constants.LOGTYPE_SQUAD_PARTNERS,
                ExceptionMessage__c = e.getMessage(),
                StackTrace__c = e.getStackTraceString(),
                ObjectId__c = opportunityId);
                Logger.createLog(log);
                throw new PartnerChildOpportunityManagerControllerException(e.getMessage());
        }
    }
    

    @AuraEnabled(cacheable=false)
    public static String createChildOpportunity (String name, Date closeDate, String ownerId, String accountId, String masterOpportunityId, String[] accountInOppIds ){  
        
        try {
            Opportunity childOppCreated = OpportunityService.createChildOpportunity (name, closeDate, ownerId, accountId, masterOpportunityId, accountInOppIds, constants.OPPORTUNITY_RT_PARTNER_FLOW_OPP);
            return childOppCreated.Name;
        } catch (Exception e) {
                DebugLog__c log = new DebugLog__c(
                        Origin__c = '['+className+'][createChildOpportunity]',
                        LogType__c = constants.LOGTYPE_ERROR,
                        RelatedToSquad__c = constants.LOGTYPE_SQUAD_PARTNERS,
                        ExceptionMessage__c = e.getMessage(),
                        StackTrace__c = e.getStackTraceString(),
                        ObjectId__c = masterOpportunityId);
                Logger.createLog(log);
                System.debug( ' e.getStackTraceString() ' + e.getStackTraceString() );
                throw new PartnerChildOpportunityManagerControllerException(e.getMessage());
        }
    }

       
    @AuraEnabled(cacheable=true)
    public static Id getMainOpportunityMember(Id opportunityId, Id accountId){
        try {
            Map<Id,Account_Opportunity_Relationship__c> oppMemberMap = ((AccountOpportunitySelector)Application.Selector.newInstance(Account_Opportunity_Relationship__c.sobjectType)).selectOpportunityMemberByAccountIdAndOpportunityId(new Set<Id>{accountId}, new Set<Id>{opportunityId});
            if(!oppMemberMap.isEmpty()){
                return oppMemberMap.values()[0].Id;
            } 
            throw new PartnerChildOpportunityManagerControllerException('No opportunity member found. Please, contact admin.');
        } catch (Exception e) {
            DebugLog__c log = new DebugLog__c(
                        Origin__c = '['+className+'][getMainOpportunityMember]',
                        LogType__c = constants.LOGTYPE_ERROR,
                        RelatedToSquad__c = constants.LOGTYPE_SQUAD_PARTNERS,
                        ExceptionMessage__c = e.getMessage(),
                        StackTrace__c = e.getStackTraceString(),
                        ObjectId__c = opportunityId);
                Logger.createLog(log);
                throw new PartnerChildOpportunityManagerControllerException(e.getMessage());
        }
    }

    public static void setShowButton(Boolean hasAccountToDisplay){
        showButton = hasAccountToDisplay;
    }

    @AuraEnabled(cacheable=true)
    public static Boolean getShowButton(){
        return showButton;
    }



    
    public class PartnerChildOpportunityManagerControllerException extends Exception {}

	public class Account_Item {
        @AuraEnabled
		public String accountId;
        @AuraEnabled
		public String accountName;
        @AuraEnabled
        public String partnerLevel;
        @AuraEnabled
        public String legalRepresentativeId;
        @AuraEnabled
        public String legalRepresentativeName;

        public Account_Item(String Id, String Name, String partnerLevel, String legalRepresentativeId, String legalRepresentativeName){
            this.accountId = '/'+Id;
            this.accountName = Name;
            this.partnerLevel = partnerLevel;
            this.legalRepresentativeId = legalRepresentativeName != null ? '/'+legalRepresentativeId : '';
            this.legalRepresentativeName = legalRepresentativeName;
        }
    }

}