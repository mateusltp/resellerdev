@isTest
private class GymActivitiesControllerTest {

    @isTest static void fetchAccountTest() {
        
        Gym_Activity__c gymActivity1 = new Gym_Activity__c(
            Name='Activity Test');
        insert gymActivity1;                               
        GymActivitiesController.fetchAccount('Activity Test');
        
    }
    
    @isTest static void getContactsTest() {
        
        Gym_Activity__c gymActivity2 = new Gym_Activity__c(
            Name='Activity Test 2');
        insert gymActivity2; 
        Gym_Activity__c queryActivity = [SELECT id, name FROM Gym_Activity__c];
        GymActivitiesController.getContacts();
        
    }
    
    @isTest static void addParentAccountTest() {
        Product_Item__c prod1 = new Product_Item__c();
        prod1.CAP_Value__c = 80;
        prod1.Do_You_Have_A_No_Show_Fee__c =  'No';
        prod1.Gym_Classes__c = 'Weight training';
        prod1.Has_Late_Cancellation_Fee__c = 'No';
        prod1.Is_Network_CAP__c = 'No';
        prod1.Max_Monthly_Visit_Per_User__c = 0;
        prod1.Name = 'Prod Test';
        prod1.Net_Transfer_Price__c = 8.17;
        prod1.CAP_Value__c = 80;
        
        insert prod1;
        
        Product_Item__c prodQuery = [Select id, name FROM Product_Item__c
                                    WHERE Name = 'Prod Test' LIMIT 1];

        GymActivities__c gym1 = new GymActivities__c(
            Name='Parent 1',
        Label__c='Label Test');
        insert gym1;
        
        List<String>lstOfContactIds = new List<String>();
        lstOfContactIds.add(gym1.id);
        String ParentId;
        
        list<Gym_Activity__c> lstContactstoInsert = new list<Gym_Activity__c>();
        List<GymActivities__c> gymActivitiesQuery = [SELECT id, Label__c FROM GymActivities__c where Label__c='Label Test'];
        for(GymActivities__c gymActivity : gymActivitiesQuery){
            Gym_Activity__c oContact = new Gym_Activity__c();
            oContact.Name = 'Test Activity';
            oContact.Product_Item__c = prodQuery.Id;
            lstContactstoInsert.add(oContact);
            Test.startTest();
        }
       try{
            insert lstContactstoInsert;
        }catch (System.Exception ex){
            
        }
         GymActivitiesController.addParentAccount(prodQuery.Id, lstOfContactIds);
        Test.stopTest();
    }
}