/**
 * @description       : 
 * @author            : Samuel Silva - GFT (slml@gft.com)
 * @group             : 
 * @last modified on  : 12-18-2020
 * @last modified by  : Samuel Silva - GFT (slml@gft.com)
 * Modifications Log 
 * Ver   Date         Author                              Modification
 * 1.0   12-07-2020   Samuel Silva - GFT (slml@gft.com)   Initial Version
**/
public without sharing class DealHierarchyStatusCtrl {
    private List<Case> caseLst = new List<Case>();
    private List<Opportunity> oppLst = new List<Opportunity>();
    private Map<Id, SObject> recordMapOld2 = new Map<Id, SObject>();

    public void validateGpStatus(List<SObject> recordLst, Map<Id, SObject> recordMapOld){ 
        recordMapOld2.putAll(recordMapOld);
        for(SObject obj : recordLst){
            if (obj.id.getSobjectType() == Schema.Opportunity.SObjectType) {
                oppLst.add((Opportunity)obj);
            } else if(obj.id.getSobjectType() == Schema.Case.SObjectType){
                caseLst.add((Case)obj);
            }
        }
        if(!oppLst.isEmpty()){
            cancelByOppLst();
        }

        if(!caseLst.isEmpty()){
            cancelByCaseLst();
        }
    }

    private void cancelByOppLst(){
        List<Opportunity> renOppList = new List<Opportunity>();
        List<Opportunity> nbOppList = new List<Opportunity>();
        Opportunity oldValue = new Opportunity();
        
        Id rtIdRen = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Success_Renegotiation').getRecordTypeId();
        Id rtIdNb =  Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        for(Opportunity opp : oppLst){
            if(opp.StageName == 'Lançado/Ganho' && opp.Sub_Type__c != 'Retention' && oldValue.StageName != opp.StageName){
                if(opp.RecordTypeId == rtIdRen){
                    renOppList.add(opp);
                } else if(opp.RecordTypeId == rtIdNb){
                    nbOppList.add(opp);              
                } 
            }
        }

        if(!renOppList.isEmpty()){
            getNewPreviousDealHierarchy(renOppList);
        }
        if(!nbOppList.isEmpty()){
            getNewAccsInDeal(nbOppList);
        }
    }

    private void getNewAccsInDeal(List<Opportunity> oppLst){
        Set<Id> oppIds = new Set<Id>();
        Set<Id> accIds = new Set<Id>();
        for(Opportunity opp : oppLst){
            oppIds.add(opp.Id);
        }
        accIds = getAccountsInDeal(oppIds);
        if(!accIds.isEmpty()){
            changeStatus(accIds, true);
        }
    }

    private void getNewPreviousDealHierarchy(List<Opportunity> oppLst){   
        Set<Id> accIds = new Set<Id>();
        Set<Id> oppIds = new Set<Id>();
        for(Opportunity opp : oppLst){
            oppIds.add(opp.Id);
            accIds.add(opp.AccountId);
        }
        Set<Id> previousOppIds = (new Map<id, SObject>([    SELECT ID
                                                FROM Opportunity 
                                                WHERE AccountId IN: accIds
                                                AND StageName = 'Lançado/Ganho'
                                                AND ID NOT IN: oppIds
                                            ])).keySet();

        Set<Id> pAccountIds = getAccountsInDeal(previousOppIds); 
        Set<Id> nAccountIds = getAccountsInDeal(oppIds);         
        pAccountIds.removeAll(nAccountIds);
        if(!pAccountIds.isEmpty()) {
            changeStatus(pAccountIds, false);
        }
        if(!nAccountIds.isEmpty()){
            changeStatus(nAccountIds, true);
        }
        
    }
   

    private void cancelByCaseLst(){
        Id rtId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Contract_Cancellation').getRecordTypeId();
        Set<Id> oppIds = new Set<Id>();
        for(Case c : caseLst){
            if(c.RecordTypeId == rtId && c.Status == 'Confirmed' ){
                oppIds.add(c.OpportunityId__c);
            }
        }
        if(!oppIds.isEmpty()){
            List<Account> accsToUpdate = getDealHiearchyWithCancellationReason(oppIds);
            if(!accsToUpdate.isEmpty()){
                UPDATE accsToUpdate;
            }
        }
    }

    private void changeStatus(Set<Id> accIds, Boolean status){
        List<Account> accstToUpdate = new List<Account>();
        String newStatus = status ? 'Active' : 'Inactive';
        String newType = status ? 'Client' : 'Cancel';
        String cReason = status ? '' : 'Other';
        for(ID i : accIds){
            Account a = new Account(id=i, GP_Status__c = newStatus, Type = newType, cancellation_reason__c = cReason);
            accstToUpdate.add(a);
        }
        update accstToUpdate;
    }

    private Set<Id> getAccountsInDeal(Set<Id> oppIds){
        Set<Id> accIds = new Set<Id>();
        for(Account_Opportunity_Relationship__c aor : [ SELECT Account__c 
                                                        FROM Account_Opportunity_Relationship__c 
                                                        WHERE Opportunity__c IN: oppIds]){            
            accIds.add(aor.Account__c);                                            
        }
        return accIds;
    }

    private List<Account> getDealHiearchyWithCancellationReason(Set<Id> oppIds){
        Map<Id, String> accsWithCancelReason = new Map<Id, String>();
        List<Account> accLst = new List<Account>();
        for(Account_Opportunity_Relationship__c aor : [SELECT Account__c,Opportunity__r.Loss_Reason__c
                                                        FROM Account_Opportunity_Relationship__c 
                                                        WHERE Opportunity__c IN: oppIds]){ 
            accsWithCancelReason.put(aor.Account__c, aor.Opportunity__r.Loss_Reason__c);
        }
        
        for(Id iAcc : accsWithCancelReason.keySet()){
            Account a = new Account(
                            id=iAcc, 
                            GP_Status__c = 'Inactive', 
                            Type = 'Cancel', 
                            cancellation_reason__c = accsWithCancelReason.get(iAcc));
            accLst.add(a);
        }       
        return accLst;
    }
}