/**
 * @description       : 
 * @author            : Samuel Silva - GFT (slml@gft.com)
 * @group             : 
 * @last modified on  : 06-11-2021
 * @last modified by  : roei@gft.com
 * Modifications Log 
 * Ver   Date         Author                              Modification
 * 1.0   11-13-2020   Samuel Silva - GFT (slml@gft.com)   Initial Version
**/
public without sharing class EngagementJourneyFormHelper {

    private enum EngagementJourneyIndex {M0, M1, M2, M3}
    private Map<Id, List<Form__c>> mapOppWithForms;
    FormRepository formRepo;  
    List<Opportunity> opps;
    OpportunityRepository oppRepo;

    @InvocableMethod
    public static void createM1ForOppsWithIds(List<Id> oppIdList) {        
        findOppWithoutExistingM1(oppIdList);
    }
  
    private static void findOppWithoutExistingM1(List<Id> oppIdList){
        Set<Id> oppIdSet = new Set<Id>(oppIdList);
        FormRepository formRepo = new FormRepository();
        Map<Id, Form__c> oppToM1Form = formRepo.getM1ForOppsAsMapOppIdToForm(oppIdList);
        for(Id oppId : oppToM1Form.keySet()){
            oppIdSet.remove(oppId);
        }
        if(!oppIdSet.isEmpty()){
            Map<Id, Form__c> previousMForms = formRepo.getM0ForOppsAsMapOppIdToForm(new List<Id>(oppIdSet));
            EngagementJourneyIndex m1Index = EngagementJourneyIndex.M1;
            createFormForOpps(oppIdSet, m1Index, previousMForms);
        }
    }


    private static void createFormForOpps(Set<Id> oppIdSet, EngagementJourneyIndex mIndex, Map<Id, Form__c> previousMForms){
        Id recordTypeId = setRecordType(mIndex);
        List<Form__c> ejFormToInsertLst = new List<Form__c>();
        Map<Id, List<Step_Towards_Success1__c>> oppIdToStepsList = getStepsTowardsSuccessFromOpp(new List<Id>(oppIdSet));
        for(ID oppId : oppIdSet){
            Form__c newEjForm = new Form__c();
            newEjForm.recordTypeId = recordTypeId;
            newEjForm.Opportunity__c = oppId;
            if(mIndex == EngagementJourneyIndex.M1){
                Form__c m0 = previousMForms.get(oppId);
                if(m0 != null){
                    populateFieldsFromPreviousForm(newEjForm, m0);  
                }                           
            }
            try {
                populateFormForOppsWithEnablers(newEjForm, oppIdToStepsList.get(oppId));
                ejFormToInsertLst.add(newEjForm);
            } catch(Exception e) {
                throw new AuraException(e.getMessage());
            }       
        }
        if(!ejFormToInsertLst.isEmpty()){
            Database.insert(ejFormToInsertLst);
        }
    }

    private static void populateFieldsFromPreviousForm(Form__c newEjForm, Form__c previousForm){
        
        if(previousForm.Client_Success_Executive__c != null){
            newEjForm.Client_Success_Executive__c = previousForm.Client_Success_Executive__c;
        }
        if(previousForm.Client_Sales_Executive__c != null){
            newEjForm.Client_Sales_Executive__c = previousForm.Client_Sales_Executive__c;
        }
        if(previousForm.Approval_Status__c == 'Approved By Regional CEO') {
            newEjForm.Approval_Status__c = previousForm.Approval_Status__c;
        }

        if (previousForm.SMB_CS_success_executive__c) {
            newEjForm.Client_Success_Approval__c = previousForm.Client_Success_Approval__c;
            newEjForm.SMB_CS_success_executive__c = previousForm.SMB_CS_success_executive__c;
            newEjForm.Form_Status__c = previousForm.Form_Status__c;
            newEjForm.Approval_Status__c = previousForm.Approval_Status__c;
            newEjForm.Head_of_client_success__c = previousForm.Head_of_client_success__c;
            newEjForm.Country_Manager__c = previousForm.Country_Manager__c;
            newEjForm.Client_Success_Executive__c = previousForm.Client_Success_Executive__c;
        }
        newEjForm.Standard_Membership_Fee_GL_justify__c = previousForm.Standard_Membership_Fee_GL_justify__c;
        newEjForm.Expansion_100_of_the_eligible_GL_justify__c = previousForm.Expansion_100_of_the_eligible_GL_justify__c;
        newEjForm.Minimum_Access_Fee_GL_justify__c=previousForm.Minimum_Access_Fee_GL_justify__c;
        newEjForm.Engagement_with_C_Level_GL_justify__c=previousForm.Engagement_with_C_Level_GL_justify__c;
        newEjForm.Payroll_GL_justify__c = previousForm.Payroll_GL_justify__c;
        newEjForm.Database_Email_GL_justify__c = previousForm.Database_Email_GL_justify__c;
        newEjForm.Whitelist_GL_justify__c=previousForm.Whitelist_GL_justify__c;
        newEjForm.New_Hires_GL_justify__c=previousForm.New_Hires_GL_justify__c;
        newEjForm.HR_Communication_GL_justify__c=previousForm.HR_Communication_GL_justify__c;
        newEjForm.Exclusivity_clause_GL_justify__c=previousForm.Exclusivity_clause_GL_justify__c;
        newEjForm.PR_in_contract_GL_justify__c=previousForm.PR_in_contract_GL_justify__c;
        newEjForm.What_does_success_looks_like_GL_justify__c=previousForm.What_does_success_looks_like_GL_justify__c;      
    }

    private static void populateFormForOppsWithEnablers(Form__c ejForm, List<Step_Towards_Success1__c> steps){
        if (steps == null) return;

        for (Step_Towards_Success1__c step : steps) {
            if (step.Step_Number__c == 1) {
                ejForm.Standard_Membership_Fee_GL__c = step.Achieved__c;
                continue;
            }
            
            if (step.Step_Number__c == 2) {
                ejForm.Expansion_100_of_the_eligible_GL__c = step.Achieved__c;
                continue;
            }
            
            if (step.Step_Number__c == 3) {
                ejForm.Minimum_Access_Fee_GL__c = step.Achieved__c;
                continue;
            }
            
            if (step.Step_Number__c == 4) {
                ejForm.Engagement_with_C_Level_GL__c = step.Achieved__c;
                continue;
            }
            
            if (step.Step_Number__c == 5) {
                ejForm.Payroll_GL__c = step.Achieved__c;
                continue;
            }
            
            if (step.Step_Number__c == 6) {
                ejForm.Database_Email_GL__c = step.Achieved__c;
                continue;
            }
            
            if (step.Step_Number__c == 7) {
                ejForm.Whitelist_GL__c = step.Achieved__c;
                continue;
            }
            
            if (step.Step_Number__c == 8) {
                ejForm.New_Hires_GL__c = step.Achieved__c;
                continue;
            }
            
            if (step.Step_Number__c == 9) {
                ejForm.HR_Communication_GL__c = step.Achieved__c;
                continue;
            }
            
            if (step.Step_Number__c == 10) {
                ejForm.Exclusivity_clause_GL__c = step.Achieved__c;
                continue;
            }
            
            if (step.Step_Number__c == 11) {
                ejForm.PR_in_contract_GL__c = step.Achieved__c;
                continue;
            }
            
            if (step.Step_Number__c == 12) {
                ejForm.What_does_success_looks_like_Joint_BP_GL__c = step.Achieved__c;
                continue;
            }
        }
    }
  

    private static ID setRecordType(EngagementJourneyIndex mIndex){
       Id recordTypeId;
        switch on mIndex {
            when M0 {
                recordTypeId = Schema.SObjectType.Form__c.getRecordTypeInfosByDeveloperName().get('M0_12_Steps').getRecordTypeId();
            }            
            when M1 {
                recordTypeId = Schema.SObjectType.Form__c.getRecordTypeInfosByDeveloperName().get('M1_12_Steps').getRecordTypeId();
            }
        }

        return recordTypeId;
    }

    private static Map<Id, List<Step_Towards_Success1__c>> getStepsTowardsSuccessFromOpp (List<Id> oppIdList){
        StepsTowardsSuccessRepository stepsRepo = new StepsTowardsSuccessRepository();
        Map<Id, List<Step_Towards_Success1__c>> oppIdToStepsList = stepsRepo.getStepsTowardsSuccessFromOppsAsMap(oppIdList);
        return oppIdToStepsList;
    }

}