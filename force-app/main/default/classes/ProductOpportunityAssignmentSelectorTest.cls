/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 05-23-2022
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/
@isTest(seeAllData=false)
public class ProductOpportunityAssignmentSelectorTest {  

    @TestSetup
    static void setupData(){
        String recordTypePartnerFlow = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Account').getRecordTypeId();
        Account partnerAcc = PartnerDataFactory.newAccount();
        partnerAcc.RecordTypeId = recordTypePartnerFlow;
        partnerAcc.CRM_ID__c = '1231';
        partnerAcc.UUID__c = new Uuid().getValue();
        INSERT partnerAcc;

        /* Single Partner Account*/ 
        Account singlePartnerAcc = PartnerDataFactory.newAccount();
        singlePartnerAcc.RecordTypeId = recordTypePartnerFlow;
        singlePartnerAcc.Partner_Level__c = 'Single Partner';
        singlePartnerAcc.Name = 'Single Partner';
        singlePartnerAcc.UUID__c = new Uuid().getValue();
        INSERT singlePartnerAcc;
             
        String recordTypePartnerContactFlow = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Contact').getRecordTypeId();
        Contact partnerContact = PartnerDataFactory.newContact(partnerAcc.Id);
        partnerContact.RecordTypeId = recordTypePartnerContactFlow;        
        partnerContact.Type_of_Contact__c = 'Decision Maker';
        partnerContact.UUID__c = new Uuid().getValue();
        INSERT partnerContact;      

        Opportunity aOpp = PartnerDataFactory.newOpportunity( partnerAcc.Id, 'Partner_Flow_Opportunity'); 
        aOpp.UUID__c = new Uuid().getValue();
        INSERT aOpp;

          /* Single Partner Opportunity */ 
        Opportunity aSingleOpp = PartnerDataFactory.newOpportunity( singlePartnerAcc.Id, 'Partner_Flow_Opportunity'); 
        aSingleOpp.UUID__c = new Uuid().getValue();
        INSERT aSingleOpp;

        Quote proposal = PartnerDataFactory.newQuote( aOpp );
        proposal.RecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Proposal').getRecordTypeId();
        proposal.Signed__c = Date.Today() - 100;
        proposal.End_Date__c = Date.Today();
        proposal.UUID__c = new Uuid().getValue();
        INSERT proposal;

        Account_Opportunity_Relationship__c oppMember = PartnerDataFactory.newAcctOppRel(partnerAcc.Id, aOpp.Id);
        oppMember.RecordTypeId = Schema.SObjectType.Account_Opportunity_Relationship__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Opportunity_Member').getRecordTypeId();
        INSERT oppMember;

         /* Single OppMember */ 
        Account_Opportunity_Relationship__c singleOppMember = PartnerDataFactory.newAcctOppRel(singlePartnerAcc.Id, aSingleOpp.Id);
        singleOppMember.RecordTypeId = Schema.SObjectType.Account_Opportunity_Relationship__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Opportunity_Member').getRecordTypeId();
        INSERT singleOppMember;

        Product_Item__c aProd = PartnerDataFactory.newProduct( aOpp );
        aProd.RecordTypeId = Schema.SObjectType.Product_Item__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product').getRecordTypeId();
        aProd.Opportunity__c = null;   
        aProd.UUID__c = new Uuid().getValue();    
        INSERT aProd;

         /* Single Partner Prod */ 
        Product_Item__c aSingleProd = PartnerDataFactory.newProduct( aOpp );
        aSingleProd.RecordTypeId = Schema.SObjectType.Product_Item__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product').getRecordTypeId();
        aSingleProd.Opportunity__c = null;   
        aSingleProd.Name = 'Single Partner';
        aSingleProd.UUID__c = new Uuid().getValue();    
        INSERT aSingleProd;

        Commercial_Condition__c aComm = new Commercial_Condition__c();
        aComm.RecordTypeId = Schema.SObjectType.Commercial_Condition__c.getRecordTypeInfosByDeveloperName().get('CAP').getRecordTypeId();
        INSERT aComm;

         /* Single Partner Prod */ 
        Commercial_Condition__c aSingleComm = new Commercial_Condition__c();
        aSingleComm.RecordTypeId = Schema.SObjectType.Commercial_Condition__c.getRecordTypeInfosByDeveloperName().get('CAP').getRecordTypeId();
        INSERT aSingleComm;
 
        Product_Assignment__c prodAssign = new Product_Assignment__c();
        prodAssign.RecordTypeId = Schema.SObjectType.Product_Assignment__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product_Assignment').getRecordTypeId();
        prodAssign.ProductId__c = aProd.Id;
        prodAssign.CommercialConditionId__c = aComm.Id;
        INSERT prodAssign;

        /* Single Partner Product Assignment */ 
        Product_Assignment__c singleProdAssign = new Product_Assignment__c();
        singleProdAssign.RecordTypeId = Schema.SObjectType.Product_Assignment__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product_Assignment').getRecordTypeId();
        singleProdAssign.ProductId__c = aSingleProd.Id;
        singleProdAssign.CommercialConditionId__c = aSingleComm.Id;
        INSERT singleProdAssign;

        Product_Opportunity_Assignment__c prodOppAssign = new Product_Opportunity_Assignment__c();
        prodOppAssign.RecordTypeId = Schema.SObjectType.Product_Opportunity_Assignment__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product_Opportunity_Assignment').getRecordTypeId();
        prodOppAssign.OpportunityMemberId__c = oppMember.Id;
        prodOppAssign.ProductAssignmentId__c = prodAssign.Id;
        INSERT prodOppAssign;

        /* Single Partner Product Opportunity Assignment */        
        Product_Opportunity_Assignment__c singleProdOppAssign = new Product_Opportunity_Assignment__c();
        singleProdOppAssign.RecordTypeId = Schema.SObjectType.Product_Opportunity_Assignment__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product_Opportunity_Assignment').getRecordTypeId();
        singleProdOppAssign.OpportunityMemberId__c = singleOppMember.Id;
        singleProdOppAssign.ProductAssignmentId__c = singleProdAssign.Id;
        INSERT singleProdOppAssign;        

        Product_Item__c childProd = PartnerDataFactory.newProduct( aOpp );
        childProd.RecordTypeId = Schema.SObjectType.Product_Item__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product').getRecordTypeId();
        childProd.Parent_Product__c = aProd.Id;   
        childProd.UUID__c = new Uuid().getValue();    
        INSERT childProd;

        Commercial_Condition__c childComm = new Commercial_Condition__c();
        childComm.RecordTypeId = Schema.SObjectType.Commercial_Condition__c.getRecordTypeInfosByDeveloperName().get('CAP').getRecordTypeId();
        INSERT childComm;

        Product_Assignment__c childProdAssign = new Product_Assignment__c();
        childProdAssign.RecordTypeId = Schema.SObjectType.Product_Assignment__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product_Assignment').getRecordTypeId();
        childProdAssign.ProductId__c = childProd.Id;
        childProdAssign.CommercialConditionId__c = childComm.Id;
        INSERT childProdAssign;

        Product_Opportunity_Assignment__c childProdOppAssign = new Product_Opportunity_Assignment__c();
        childProdOppAssign.RecordTypeId = Schema.SObjectType.Product_Opportunity_Assignment__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product_Opportunity_Assignment').getRecordTypeId();
        childProdOppAssign.OpportunityMemberId__c = oppMember.Id;
        childProdOppAssign.ProductAssignmentId__c = childProdAssign.Id;
        INSERT childProdOppAssign;
    }

    @isTest
    static void getSObjectFieldList_Test(){
        Test.startTest(); 
            System.assert(new ProductOpportunityAssignmentSelector().getSObjectFieldList().contains(Product_Opportunity_Assignment__c.Id)); 
        Test.stopTest();
    }

    @isTest
    static void getSObjectType_Test(){
        Test.startTest(); 
            System.assertEquals(Product_Opportunity_Assignment__c.sObjectType , new ProductOpportunityAssignmentSelector().getSObjectType()); 
        Test.stopTest();
    }

    @isTest
    static void gselectById_Test(){
        Product_Opportunity_Assignment__c aProdOppAssign = [SELECT ID from Product_Opportunity_Assignment__c LIMIT 1];
        Test.startTest(); 
            System.assertEquals( aProdOppAssign.Id , new ProductOpportunityAssignmentSelector().selectById(new Set<Id>{aProdOppAssign.Id})[0].Id); 
        Test.stopTest();
    }

    @isTest
    static void gselectCapByProductId_Test(){
        Product_Opportunity_Assignment__c aProdOppAssign = [SELECT ID, ProductAssignmentId__r.ProductId__c FROM Product_Opportunity_Assignment__c WHERE ProductAssignmentId__r.CommercialConditionId__r.RecordType.DeveloperName = 'CAP' LIMIT 1];
        Test.startTest(); 
            System.assertEquals( aProdOppAssign.Id , new ProductOpportunityAssignmentSelector().selectCapByProductId(new Set<Id>{aProdOppAssign.ProductAssignmentId__r.ProductId__c})[0].Id); 
        Test.stopTest();
    }

    @isTest
    static void gselectProductIdWithCommConditions_Test(){
        Product_Opportunity_Assignment__c aProdOppAssign = [SELECT ID, ProductAssignmentId__r.ProductId__c FROM Product_Opportunity_Assignment__c WHERE ProductAssignmentId__r.CommercialConditionId__r.RecordType.DeveloperName = 'CAP' LIMIT 1];
        Test.startTest(); 
            System.assertEquals( aProdOppAssign.Id , new ProductOpportunityAssignmentSelector().selectProductIdWithCommConditions(new Set<Id>{aProdOppAssign.ProductAssignmentId__r.ProductId__c})[0].Id); 
        Test.stopTest();
    }

    @isTest
    static void gselectCapByAccountId_Test(){
        Product_Opportunity_Assignment__c aProdOppAssign = [SELECT ID, OpportunityMemberId__r.Account__c FROM Product_Opportunity_Assignment__c WHERE ProductAssignmentId__r.CommercialConditionId__r.RecordType.DeveloperName = 'CAP' LIMIT 1];
        Test.startTest(); 
            System.assertEquals( aProdOppAssign.Id , new ProductOpportunityAssignmentSelector().selectCapByAccountId(new Set<Id>{aProdOppAssign.OpportunityMemberId__r.Account__c})[0].Id); 
        Test.stopTest();
    }

    @isTest
    static void gselectAllCommercialConditionsByProductId_Test(){
        Product_Opportunity_Assignment__c aProdOppAssign = [SELECT ID, ProductAssignmentId__r.ProductId__c FROM Product_Opportunity_Assignment__c WHERE ProductAssignmentId__r.CommercialConditionId__r.RecordType.DeveloperName = 'CAP' LIMIT 1];
        Test.startTest(); 
            System.assertEquals( aProdOppAssign.Id , new ProductOpportunityAssignmentSelector().selectAllCommercialConditionsByProductId(aProdOppAssign.ProductAssignmentId__r.ProductId__c)[0].Id); 
        Test.stopTest();
    }

    @isTest
    static void gselectNonCapByProductId_Test(){
        Product_Opportunity_Assignment__c aProdOppAssign = [SELECT ID, ProductAssignmentId__r.ProductId__c FROM Product_Opportunity_Assignment__c WHERE ProductAssignmentId__r.CommercialConditionId__r.RecordType.DeveloperName = 'CAP' LIMIT 1];
        Test.startTest(); 
            System.assertEquals( 0 , new ProductOpportunityAssignmentSelector().selectNonCapByProductId(new Set<Id>{aProdOppAssign.ProductAssignmentId__r.ProductId__c}).size()); 
        Test.stopTest();
    }

    @isTest
    static void gselectByParentProductId_Test(){
        Product_Opportunity_Assignment__c aProdOppAssign = [SELECT ID, ProductAssignmentId__r.ProductId__r.Parent_Product__c FROM Product_Opportunity_Assignment__c WHERE ProductAssignmentId__r.ProductId__r.Parent_Product__c != null LIMIT 1];
        Test.startTest(); 
            System.assertEquals( aProdOppAssign.Id  , new ProductOpportunityAssignmentSelector().selectByParentProductId(aProdOppAssign.ProductAssignmentId__r.ProductId__r.Parent_Product__c)[0].Id); 
        Test.stopTest();
    }

    @isTest
    static void gselectSinglePartnerById_Test(){
        Product_Opportunity_Assignment__c aProdOppAssign = [SELECT ID, ProductAssignmentId__r.ProductId__r.Parent_Product__c FROM Product_Opportunity_Assignment__c WHERE ProductAssignmentId__r.ProductId__r.Name = 'Single Partner' LIMIT 1];
        Test.startTest(); 
            System.assertEquals( aProdOppAssign.Id  , new ProductOpportunityAssignmentSelector().selectSinglePartnerById(aProdOppAssign.ProductAssignmentId__r.ProductId__c)[0].Id); 
        Test.stopTest();
    }

    @isTest
    static void gselectByParentProductIdSet_Test(){
        Product_Opportunity_Assignment__c aProdOppAssign = [SELECT ID, ProductAssignmentId__r.ProductId__r.Parent_Product__c FROM Product_Opportunity_Assignment__c WHERE ProductAssignmentId__r.ProductId__r.Parent_Product__c != null LIMIT 1];
        Test.startTest(); 
            System.assertEquals(aProdOppAssign.Id  , new ProductOpportunityAssignmentSelector().selectByParentProductIdSet(new Set<Id>{aProdOppAssign.ProductAssignmentId__r.ProductId__r.Parent_Product__c})[0].Id); 
        Test.stopTest();
    }

    @isTest
    static void gselectByProductAssignmentIds_Test(){
        Product_Opportunity_Assignment__c aProdOppAssign = [SELECT ID, ProductAssignmentId__c FROM Product_Opportunity_Assignment__c WHERE ProductAssignmentId__r.ProductId__r.Parent_Product__c != null LIMIT 1];
        Test.startTest(); 
            System.assertEquals(aProdOppAssign.Id  , new ProductOpportunityAssignmentSelector().selectByProductAssignmentIds(new Set<Id>{aProdOppAssign.ProductAssignmentId__c})[0].Id); 
        Test.stopTest();
    }

    @isTest
    static void gselectByProductAssignmentIdsWithCap_Test(){
        Product_Opportunity_Assignment__c aProdOppAssign = [SELECT ID, ProductAssignmentId__c FROM Product_Opportunity_Assignment__c WHERE ProductAssignmentId__r.ProductId__r.Parent_Product__c != null LIMIT 1];
        Test.startTest(); 
            System.assertEquals(aProdOppAssign.Id  , new ProductOpportunityAssignmentSelector().selectByProductAssignmentIdsWithCap(new Set<Id>{aProdOppAssign.ProductAssignmentId__c})[0].Id); 
        Test.stopTest();
    }

    @isTest
    static void gselectByAccountAndOpportunity_Test(){
        Account aAcc = [SELECT ID  FROM Account WHERE Name='Single Partner' LIMIT 1];
        Opportunity opp = [SELECT ID FROM Opportunity WHERE AccountId =: aAcc.Id LIMIT 1];
        Test.startTest(); 
            System.assert( (new ProductOpportunityAssignmentSelector().selectByAccountAndOpportunity(aAcc.Id, opp.Id )).size() > 0 ); 
        Test.stopTest();
    }

    @isTest
    static void gselectByAccountsAndOpportunity_Test(){
        Account aAcc = [SELECT ID  FROM Account WHERE Name='Single Partner' LIMIT 1];
        Opportunity opp = [SELECT ID FROM Opportunity WHERE AccountId =: aAcc.Id LIMIT 1];
        Test.startTest(); 
            System.assert( (new ProductOpportunityAssignmentSelector().selectByAccountsAndOpportunity(new Set<Id>{aAcc.Id}, opp.Id )).size() > 0 ); 
        Test.stopTest();
    }


    @isTest
    static void gselectByOpportunityWithProduct_Test(){
        Account aAcc = [SELECT ID  FROM Account WHERE Name='Single Partner' LIMIT 1];
        Opportunity opp = [SELECT ID FROM Opportunity WHERE AccountId =: aAcc.Id LIMIT 1];
        Test.startTest(); 
            System.assert( (new ProductOpportunityAssignmentSelector().selectByOpportunityWithProduct( opp.Id )).size() > 0 ); 
        Test.stopTest();
    }

    @isTest
    static void gbyOppIdSetWithProduct_Test(){
        Account aAcc = [SELECT ID  FROM Account WHERE Name='Single Partner' LIMIT 1];
        Opportunity opp = [SELECT ID FROM Opportunity WHERE AccountId =: aAcc.Id LIMIT 1];
        Test.startTest(); 
            System.assert( (new ProductOpportunityAssignmentSelector().byOppIdSetWithProduct( new Set<Id>{opp.Id} )).size() > 0 ); 
        Test.stopTest();
    }
 

    @isTest
    static void gselectOpportunityByProductId_Test(){
        Product_Opportunity_Assignment__c aProdOppAssign = [SELECT ID, ProductAssignmentId__r.ProductId__c FROM Product_Opportunity_Assignment__c WHERE ProductAssignmentId__r.ProductId__r.Parent_Product__c != null LIMIT 1];
        Test.startTest(); 
            System.assertEquals(aProdOppAssign.Id  , new ProductOpportunityAssignmentSelector().selectOpportunityByProductId(aProdOppAssign.ProductAssignmentId__r.ProductId__c)[0].Id); 
        Test.stopTest();
    }

    @isTest
    static void gselectByOpportunitybyProductId_Test(){
        Product_Opportunity_Assignment__c aProdOppAssign = [SELECT ID, ProductAssignmentId__r.ProductId__c FROM Product_Opportunity_Assignment__c WHERE ProductAssignmentId__r.ProductId__r.Parent_Product__c != null LIMIT 1];
        Test.startTest(); 
            System.assertEquals(aProdOppAssign.Id  , new ProductOpportunityAssignmentSelector().selectByOpportunitybyProductId(new Set<Id>{aProdOppAssign.ProductAssignmentId__r.ProductId__c})[0].Id); 
        Test.stopTest();
    }
 
}