public with sharing class CancelClientOrderCommand extends AbstractCommand {
  override public void execute() {
    List<Order> orders = (List<Order>) event.getPayloadFromJson(
      List<Order>.class
    );

    for (Order order : orders) {
      order.Status = 'Canceled';
    }

    if (!orders.isEmpty()) {
      Database.update(orders);
    }
  }
}