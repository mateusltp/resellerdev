/*
 * @author Bruno Pinho
 * @date January/2019
 * @description Batch to update the field "Is there a decision maker" in all events
 */
global class BatchEventDecisionMakers implements Database.Batchable<sObject>
{
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        if (!Test.isRunningTest())
            return Database.getQueryLocator([SELECT Id, IsChild, AccountId, ActivityDate, ActivityDateTime, /*Campaign__c,*/ Comentarios__c, Created_By_Role_Name__c, CurrencyIsoCode, DB_Activity_Type__c, Description, EndDateTime, EventSubtype, Executive_in_Meeting__c, GroupEventType, Gympass_Event__c, Head_or_Above_Gympass_del__c, IsAllDayEvent, IsRecurrence, IsReminderSet, Is_there_a_Decision_Maker__c, Is_there_a_gympass_leader_present__c, LID__Date_Sent__c, LID__URL__c, Location, Motivo__c, Oportunidade_de_Negocio__c, Origem__c, OwnerId, Realizado__c, RecurrenceActivityId, RecurrenceDayOfMonth, RecurrenceDayOfWeekMask, RecurrenceEndDateOnly, RecurrenceInstance, RecurrenceInterval, RecurrenceMonthOfYear, RecurrenceStartDateTime, RecurrenceTimeZoneSidKey, RecurrenceType, ReminderDateTime, ShowAs, StartDateTime, Subject, TipoTE__c, Type, WhatId, WhoId
                                             FROM Event
                                             WHERE LastModifiedDate = TODAY
                                             AND IsDeleted = FALSE]);
        else
            return Database.getQueryLocator([SELECT Id, IsChild, AccountId, ActivityDate, ActivityDateTime, /*Campaign__c,*/ Comentarios__c, Created_By_Role_Name__c, CurrencyIsoCode, DB_Activity_Type__c, Description, EndDateTime, EventSubtype, Executive_in_Meeting__c, GroupEventType, Gympass_Event__c, Head_or_Above_Gympass_del__c, IsAllDayEvent, IsRecurrence, IsReminderSet, Is_there_a_Decision_Maker__c, Is_there_a_gympass_leader_present__c, LID__Date_Sent__c, LID__URL__c, Location, Motivo__c, Oportunidade_de_Negocio__c, Origem__c, OwnerId, Realizado__c, RecurrenceActivityId, RecurrenceDayOfMonth, RecurrenceDayOfWeekMask, RecurrenceEndDateOnly, RecurrenceInstance, RecurrenceInterval, RecurrenceMonthOfYear, RecurrenceStartDateTime, RecurrenceTimeZoneSidKey, RecurrenceType, ReminderDateTime, ShowAs, StartDateTime, Subject, TipoTE__c, Type, WhatId, WhoId
                                             FROM Event
                                             WHERE IsDeleted = FALSE
                                             LIMIT 50]);
        
    }

    global void execute(Database.BatchableContext BC, List<Event> scope)
    {
        Set<String> setAllContactIds = new Set<String>();
        Set<String> setDecisionMakerContacts = new Set<String>();
        Set<String> setEventsWithDecisionMaker = new Set<String>();
        Set<String> setActiveUsers = new Set<String>();
        
        List<Contact> listDecisionMakerContacts = new List<Contact>();
        List<Event> listEventsToDelete = new List<Event>();
        List<Event> listEventsToInsert = new List<Event>();
        List<EventWhoRelation> listEventWhoRelations = new List<EventWhoRelation>();
        List<User> listActiveUsers = new List<User>();
        
        Map<Id, Event> mapEventsToUpdate = new Map<Id, Event>();
        Map<Id, Event> mapEventsToDelete = new Map<Id, Event>();
        
        listActiveUsers = [SELECT Id
                           FROM User
                           WHERE IsActive = TRUE];
                           
        for (User u: listActiveUsers)
            setActiveUsers.add(u.Id);
        
        listEventWhoRelations = [SELECT EventId, RelationId
                                 FROM EventWhoRelation
                                 WHERE EventId IN: scope
                                 ORDER BY EventId ASC];
            
        for (EventWhoRelation ewr: listEventWhoRelations)
            setAllContactIds.add(ewr.RelationId);   
            
        listDecisionMakerContacts = [SELECT Id
                                     FROM Contact
                                     WHERE Id IN: setAllContactIds
                                     AND DecisionMaker__c = 'Yes'];
                                                   
        for (Contact c: listDecisionMakerContacts)
            setDecisionMakerContacts.add(c.Id);
                                                
        for (EventWhoRelation ewr: listEventWhoRelations)
        {
            if (setDecisionMakerContacts.contains(ewr.RelationId) == TRUE)
                setEventsWithDecisionMaker.add(ewr.EventId);
        }
                                                    
        for (Event evt: scope)
        {
            Boolean evaluateConditions = FALSE;
            
            if (Test.isRunningTest() == TRUE)
                evaluateConditions = FALSE;
            else if (evt.IsChild == FALSE)
                evaluateConditions = TRUE;
                
            if (evaluateConditions)
            {
                if (evt.Is_there_a_Decision_Maker__c == FALSE && setEventsWithDecisionMaker.contains(evt.Id))
                {
                    evt.Is_there_a_Decision_Maker__c = TRUE;
                    mapEventsToUpdate.put(evt.Id, evt);
                } 
                else if (evt.Is_there_a_Decision_Maker__c == TRUE && setEventsWithDecisionMaker.contains(evt.Id) == FALSE)
                {
                    evt.Is_there_a_Decision_Maker__c = FALSE;
                    mapEventsToUpdate.put(evt.Id, evt);
                }
            } else {
                Event event = new Event();
                event.ActivityDate = evt.ActivityDate;
                event.ActivityDateTime = evt.ActivityDateTime;
                //event.Campaign__c = evt.Campaign__c;
                event.Comentarios__c = evt.Comentarios__c;
                event.CurrencyIsoCode = evt.CurrencyIsoCode;
                event.Description = evt.Description;
                event.EndDateTime = evt.EndDateTime;
                event.EventSubtype = evt.EventSubtype;
                event.Executive_in_Meeting__c = evt.Executive_in_Meeting__c;
                event.Gympass_Event__c = evt.Gympass_Event__c;
                event.Head_or_Above_Gympass_del__c = evt.Head_or_Above_Gympass_del__c;
                event.IsAllDayEvent = evt.IsAllDayEvent;
                event.IsRecurrence = evt.IsRecurrence;
                event.IsReminderSet = evt.IsReminderSet;
                event.Is_there_a_Decision_Maker__c = evt.Is_there_a_Decision_Maker__c;
                event.Is_there_a_gympass_leader_present__c = evt.Is_there_a_gympass_leader_present__c;
                event.LID__Date_Sent__c = evt.LID__Date_Sent__c;
                event.LID__URL__c = evt.LID__URL__c;
                event.Location = evt.Location;
                event.Motivo__c = evt.Motivo__c;
                event.Oportunidade_de_Negocio__c = evt.Oportunidade_de_Negocio__c;
                event.Origem__c = evt.Origem__c;
                event.Realizado__c = evt.Realizado__c;
                event.RecurrenceDayOfMonth = evt.RecurrenceDayOfMonth;
                event.RecurrenceDayOfWeekMask = evt.RecurrenceDayOfWeekMask;
                event.RecurrenceEndDateOnly = evt.RecurrenceEndDateOnly;
                event.RecurrenceInstance = evt.RecurrenceInstance;
                event.RecurrenceInterval = evt.RecurrenceInterval;
                event.RecurrenceMonthOfYear = evt.RecurrenceMonthOfYear;
                event.RecurrenceStartDateTime = evt.RecurrenceStartDateTime;
                event.RecurrenceTimeZoneSidKey = evt.RecurrenceTimeZoneSidKey;
                event.RecurrenceType = evt.RecurrenceType;
                event.ReminderDateTime = evt.ReminderDateTime;
                event.ShowAs = evt.ShowAs;
                event.StartDateTime = evt.StartDateTime;
                event.Subject = evt.Subject;
                event.TipoTE__c = evt.TipoTE__c;
                event.Type = evt.Type;
                event.WhatId = evt.WhatId;
                event.WhoId = evt.WhoId;
                
                if (setActiveUsers.contains(evt.OwnerId))
                    event.OwnerId = evt.OwnerId;
                else
                    event.OwnerId = UserInfo.getUserId();
                
                listEventsToInsert.add(event);
                //listEventsToDelete.add(evt);
                mapEventsToDelete.put(evt.Id, evt);
            }
        }
        
        if (mapEventsToUpdate.size() > 0)
        {
            UPDATE mapEventsToUpdate.values();
        }
            
        if (listEventsToDelete.size() > 0)
        {
            DELETE mapEventsToDelete.values();
            
            if (listEventsToInsert.size() > 0 && !Test.isRunningTest())
                INSERT listEventsToInsert;
        }
    }

    global void finish(Database.BatchableContext BC)
    {
        
        if (!Test.isRunningTest())
        {
            Datetime sysTime = System.now().addHours(1);
            String chron_exp = '' + sysTime.second() + ' ' + sysTime.minute() + ' ' + sysTime.hour() + ' ' + sysTime.day() + ' ' + sysTime.month() + ' ? ' + sysTime.year();
            String strName = 'BatchEventHourlyDecisionMakers ' + sysTime.day() + '/' + sysTime.month() + '/' + sysTime.year() + ' - '+ sysTime.hour() + ':' + sysTime.minute() + ':' + sysTime.second();
            System.schedule(strName, chron_exp, new SchBatchEventDecisionMakers());
        }
    }
}