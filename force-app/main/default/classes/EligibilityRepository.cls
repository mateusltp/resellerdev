/**
 * @description       :
 * @author            : AlyssonMota
 * @group             :
 * @last modified on  : 02-27-2021
 * @last modified by  : Alysson Mota
 * Modifications Log
 * Ver   Date         Author         Modification
 * 1.0   09-11-2020   Alysson Mota   Initial Version
**/
public with sharing class EligibilityRepository {

    public Map<Id, List<Eligibility__c>> getEligibilitiesByPayments(List<Payment__c> payments) {
        Map<Id, List<Eligibility__c>> mapPaymentIdEligibilityList = new Map<Id, List<Eligibility__c>>();
        Set<Id> paymentIdSet = new Set<Id>();

        for (Payment__c p : payments) {
            if (p.Id != null) {
                paymentIdSet.add(p.Id);
            }
        }

        List<Eligibility__c> eligibilities = [
            SELECT Id
            ,Name
            ,Communication_Restriction__c
            ,Exchange_Type__c
            ,Frequency__c
            ,Group_Name__c
            ,Identifier_Type__c
            ,Is_Default__c
            ,Launch_Date__c
            ,Max_Dependents__c
            ,Payment__c
            ,Payment_Method__c
            ,Payroll_Deduction__c
            ,Has_Relevant_Changes_for_Offer_Approval__c
            ,Has_Relevant_Changes_for_Offer_Deal_Desk__c
            ,Recurring_Billing_Period__c
            ,Registration_Method__c
            ,Billing_Day__c
            ,Custom_Billing_Day__c
            ,Payment_Due_Days__c
            ,Custom_Payment_Due_Days__c
            ,Cutoff_Day__c
            FROM Eligibility__c
            WHERE Payment__c IN :paymentIdSet
        ];

        for (Eligibility__c e : eligibilities) {
            if (mapPaymentIdEligibilityList.containsKey(e.Payment__c)) {
                mapPaymentIdEligibilityList.get(e.Payment__c).add(e);
            } else {
                mapPaymentIdEligibilityList.put(e.Payment__c, new List<Eligibility__c>{e});
            }
        }

        return mapPaymentIdEligibilityList;
    }

    public List<Eligibility__c> getEligibilitiesByPaymentId(List<Id> paymentIdList) {
        List<Eligibility__c> eligibilities = [
            SELECT Id
            ,Name
            ,Communication_Restriction__c
            ,Exchange_Type__c
            ,Frequency__c
            ,Group_Name__c
            ,Identifier_Type__c
            ,Is_Default__c
            ,Launch_Date__c
            ,Max_Dependents__c
            ,Payment__c
            ,Payment_Method__c
            ,Payroll_Deduction__c
            ,Has_Relevant_Changes_for_Offer_Approval__c
            ,Has_Relevant_Changes_for_Offer_Deal_Desk__c
            ,Recurring_Billing_Period__c
            ,Registration_Method__c
            ,Billing_Day__c
            ,Custom_Billing_Day__c
            ,Payment_Due_Days__c
            ,Custom_Payment_Due_Days__c
            ,Cutoff_Day__c
            FROM Eligibility__c
            WHERE Payment__c IN :paymentIdList
        ];

        return eligibilities;
    }

    public List<Eligibility__c> getEligibilitiesForPayments(List<Payment__c> payments) {
        Set<Id> paymentIdSet = new Set<Id>();

        for (Payment__c p : payments) {
            if (p.Id != null) {
                paymentIdSet.add(p.Id);
            }
        }

        return  [
            SELECT Id
            ,Name
            ,Communication_Restriction__c
            ,Exchange_Type__c
            ,Frequency__c
            ,Group_Name__c
            ,Identifier_Type__c
            ,Is_Default__c
            ,Launch_Date__c
            ,Max_Dependents__c
            ,Payment__c
            ,Payment_Method__c
            ,Payroll_Deduction__c
            ,Has_Relevant_Changes_for_Offer_Approval__c
            ,Has_Relevant_Changes_for_Offer_Deal_Desk__c
            ,Recurring_Billing_Period__c
            ,Registration_Method__c
            ,Billing_Day__c
            ,Custom_Billing_Day__c
            ,Payment_Due_Days__c
            ,Custom_Payment_Due_Days__c
            ,Cutoff_Day__c
            FROM Eligibility__c
            WHERE Payment__c IN :paymentIdSet
        ];
    }


    public List<Eligibility__c>  edit(List<Eligibility__c> eligibilities ){
        Database.update(eligibilities);
        return eligibilities;
    }


    public List<Eligibility__c> getEligibilitiesForOpportunity (String oppId){
        List<Eligibility__c> eligibilities = [
            SELECT  Id, 
                    Payment__r.Quote_Line_Item__r.Fee_Type__c, 
                    Payment_Method__c 
            FROM  eligibility__c
            WHERE Payment__r.Quote_Line_Item__r.Quote.OpportunityId =: oppId ];
            return eligibilities;
    }
}