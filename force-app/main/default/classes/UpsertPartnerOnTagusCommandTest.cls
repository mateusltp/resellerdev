/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 09-15-2021
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/
@isTest(seeAllData=false)
public class UpsertPartnerOnTagusCommandTest {

    @TestSetup
    static void setupData(){
        String recordTypePartnerFlow = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Account').getRecordTypeId();
        Account partnerAcc = PartnerDataFactory.newAccount();
        partnerAcc.RecordTypeId = recordTypePartnerFlow;
        partnerAcc.CRM_ID__c = '1231';
        INSERT partnerAcc;
             
        String recordTypePartnerContactFlow = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Contact').getRecordTypeId();
        Contact partnerContact = PartnerDataFactory.newContact(partnerAcc.Id);
        partnerContact.RecordTypeId = recordTypePartnerContactFlow;        
        partnerContact.Type_of_Contact__c = 'Admin';
        INSERT partnerContact; 
    }

    @IsTest
        public static void execute() {

        Test.setMock(HttpCalloutMock.class, new HttpResponseMock());

        EventQueue event = new EventBuilder()
        .createEventFor('CREATE_PARTNER_ON_TAGUS')
        .withSender('SALESFORCE')
        .withReceiver('TAGUS')
        .buildEvent();

        EventQueueFixtureFactory.createEventConfigForEvent( event.getEventName(),'UpsertPartnerOnTagusCommand');

        List<Account> partners = [SELECT Id FROM Account];

        Map<Id, Account> partnersMap = AccountPartnerTagusDTO.getAccountMapWithFieldsToParse(  partners  );

        event.addPayload(event.getEventName(), JSON.serialize(partnersMap));

        Test.startTest();

        event.save();

        Test.stopTest();

        System.assertEquals(   1,   [SELECT UUID__c FROM Account WHERE UUID__c != NULL].size(),  'Accounts not integrated' );
        System.assertEquals(   1,    [SELECT UUID__c FROM Contact WHERE UUID__c != NULL].size(),   'Contacts not integrated' );
    }


  private class HttpResponseMock implements HttpCalloutMock {
    public HTTPResponse respond(HTTPRequest req) {
      HttpResponse res = new HttpResponse();
      res.setHeader('Content-Type', 'application/json');
      res.setStatusCode(200);
      return res;
    }
  }

}