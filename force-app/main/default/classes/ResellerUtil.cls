/*******************************************************************************************
 * @description       : Methods used and useful for the reseller squad.
 * @author            : Rafael - GFT (dhdl@gft.com)
 * @group             : 
 * @last modified on  : 24-03-2022
 * @last modified by  : Rafael - GFT (dhdl@gft.com)
 * Modifications Log 
 * Ver   Date         Author                              Modification
 * 1.0   24-03-2021   Rafael - GFT (dhdl@gft.com)         Initial Version
******************************************************************************************/
public with sharing class ResellerUtil {
    
    /**
     * @author: Rafael - GFT (dhdl@gft.com)
     * @description: return any active picklist values ​​based on the submitted object and field name.
     * @return: String object.
     * @Date: 24-03-2021
     */
    public static string getPicklistValuesByObjectApiNameAndFieldApiName(String objectName, String fieldApiName){

        List<Picklist> pickListValuesList= new List<Picklist>();
        Schema.SObjectType schemaSObjectType = Schema.getGlobalDescribe().get(objectName);
        Schema.DescribeSObjectResult schemaSObjectDescribeResult = schemaSObjectType.getDescribe();
        Map<String,Schema.SObjectField> fields = schemaSObjectDescribeResult.fields.getMap();
        Schema.DescribeFieldResult fieldResult = fields.get(fieldApiName).getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		for( Schema.PicklistEntry pickListVal : ple)
            if(pickListVal.isActive())
			    pickListValuesList.add(new Picklist(pickListVal.getLabel(), pickListVal.getValue()));
		   
		return JSON.serialize(pickListValuesList);
    }

    public static Map<String, String> getPicklistValuesMap(String objectName, String fieldApiName){

        Map<String, String> pickListValuesMap= new Map<String, String>();
        Schema.SObjectType schemaSObjectType = Schema.getGlobalDescribe().get(objectName);
        Schema.DescribeSObjectResult schemaSObjectDescribeResult = schemaSObjectType.getDescribe();
        Map<String,Schema.SObjectField> fields = schemaSObjectDescribeResult.fields.getMap();
        Schema.DescribeFieldResult fieldResult = fields.get(fieldApiName).getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		for( Schema.PicklistEntry pickListVal : ple)
            if(pickListVal.isActive())
                pickListValuesMap.put(pickListVal.getLabel(), pickListVal.getValue());
		   
		return pickListValuesMap;
    }


    public static String getQueryFieldsByFieldSetName(String objectName, String fieldSetName){
        String fields = ' Id';
        List<Schema.FieldSetMember> fieldMember_list = ResellerUtil.getFieldMemberByFieldSetName(objectName, fieldSetName);
        for(Schema.FieldSetMember member : fieldMember_list)
            fields += ', ' + member.getFieldPath() + '';
        return fields;
    }

    public static List<Schema.FieldSetMember> getFieldMemberByFieldSetName(String objectName, String fieldSetName){
        Map<String, Schema.SObjectType> globalDescribeMap = Schema.getGlobalDescribe();
        Schema.SObjectType sObjectTypeObj = GlobalDescribeMap.get(objectName);
        Schema.DescribeSObjectResult describeSObjectResultObj = sObjectTypeObj.getDescribe();
        return describeSObjectResultObj.FieldSets.getMap().get(fieldSetName).getFields();
    }


    public class Picklist{
        public String label{get;set;}
        public String value{get;set;}
        public Picklist(String label, String value){
            this.label = label;
            this.value = value;
        }
    }

}