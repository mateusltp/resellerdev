public without sharing class TagusIntegrationController {
	
    public static TagusIntegrationCtlrResponse startIntegrationRequestCreation(List<Id> sObjectIdList, String source) {    
        List<Id> accountIdList = new List<Id>();
        List<Id> orderIdList = new List<Id>();
        Map<String, List<Id>> objecTypeToIdListMap = new Map<String, List<Id>>();
        List<Integration_Request__c> integrationRequests = new List<Integration_Request__c>();
        List<Integration_Request__c> integrationRequestToUpdate = new List<Integration_Request__c>(); 
        
        for (Id sObjectId : sObjectIdList) {
			String sObjectType = sObjectId.getSObjectType().getDescribe().getName();
            
            if (objecTypeToIdListMap.containsKey(sObjectType)) {
           		objecTypeToIdListMap.get(sObjectType).add(sObjectId);
            } else {
                objecTypeToIdListMap.put(sObjectType, new List<Id>{sObjectId});
            }
        }
        
        // This can cause too many queries if it is invoked with a list
        // that contains IDs from different types of objects
        for (String sObjectType : objecTypeToIdListMap.keySet()) {
        	List<sObject> sObjectList = getSObjectsWithTheIdsAndType(objecTypeToIdListMap.get(sObjectType), sObjectType);
 
            IntegrationRequestFactory factory = new IntegrationRequestFactory(source, sObjectType, 'smb', 'salesforce_company', 'CREATE');
            List<Integration_Request__c> createdIntegrationRequests = factory.getIntegrationRequests(sObjectList, sObjectType);
            
            integrationRequests.addAll(createdIntegrationRequests);
        }
        
        Exception responseException = null;
        try {
			insert integrationRequests;
        } catch(Exception e) {
            responseException = e;
            System.debug('Error while trying to insert Integration Request');
        }
        
        try {
        	// Set Integration Requests as Pending Integration
            for (Integration_Request__c request : integrationRequests) {
        		configureIntegrationRequest(request);
            }    
            update integrationRequests;
        } catch(Exception e) {
            responseException = e;
            System.debug('Error while trying to insert Integration Request');
        }
        
        TagusIntegrationCtlrResponse response = new TagusIntegrationCtlrResponse();
        if (responseException == null) {
        	response.status = 'SUCCESS';
            response.integrationRequests = integrationRequests;
        } else {
        	response.status = 'ERROR';
            response.responseException = responseException;
        }
        
        return response;
    }
    
    private static void configureIntegrationRequest(Integration_Request__c request) {
    	request.Integration_Request_Status__c = 'Pending Integration';
        request.Payload__c = request.Payload__c.replace('@integration_request_id', request.Id); 
    }
    
    private static List<sObject> getSObjectsWithTheIdsAndType(List<Id> sObjectIdList, String sObjectType) {
        SObjectType accountType = Schema.getGlobalDescribe().get(sObjectType);
		Map<String,Schema.SObjectField> mfields = accountType.getDescribe().fields.getMap();
		List<String> accountFields = new List<String>();
        List<sObject> sObjectList = new List<sObject>();
        
        List<Tagus_Integration_Data__mdt> integrationData = getIntegrationDataForSObjectType(sObjectType);
        
        for (Tagus_Integration_Data__mdt data : integrationData) {
       		accountFields.add(data.Field_API_Name_Or_Related_API_Path__c); 
        }

		String query = 'SELECT ';

		for (Integer i=0; i<accountFields.size(); i++) {
    		if (i == (accountFields.size() - 1)) {
  				query += accountFields.get(i);
    		} else {
        		query += accountFields.get(i) + ',';
    		}
		}

		query += ' FROM ' + sObjectType + ' WHERE Id IN :sObjectIdList'; 

		sObjectList = Database.query(query);
        
        return sObjectList;
    }
    
    private static List<Tagus_Integration_Data__mdt> getIntegrationDataForSObjectType(String sObjectType) {
        List<Tagus_Integration_Data__mdt> integrationData = [
        	SELECT Field_API_Name_Or_Related_API_Path__c, Is_Relationship__c, Name__c
            FROM Tagus_Integration_Data__mdt
            WHERE Object_API_Name__c =: sObjectType
        ];
        
        return integrationData;
    }
    
    public class TagusIntegrationCtlrResponse {
        public List<Integration_Request__c> integrationRequests;
        public String status;
        public Exception responseException;
    }
}