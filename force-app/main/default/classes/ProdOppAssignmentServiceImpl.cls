public with sharing class ProdOppAssignmentServiceImpl implements IProdOppAssignmentService{

    private static final PS_Constants constants = PS_Constants.getInstance();
    private static final String className = ProdOppAssignmentServiceImpl.class.getName();

    public void insertCurrencyProdOppAssignment(List<Product_Opportunity_Assignment__c> prodOppAssignmentLst) {
        AccountOpportunitySelector accOppSelector = (AccountOpportunitySelector)Application.Selector.newInstance(Account_Opportunity_Relationship__c.sobjectType);
        Set<Id> accOppIdSet = new Set<Id>();

        for (Product_Opportunity_Assignment__c prodOpp : prodOppAssignmentLst)
            if (prodOpp.OpportunityMemberId__c != null)
                accOppIdSet.add(prodOpp.OpportunityMemberId__c);
        Map<Id, Account_Opportunity_Relationship__c> accOppRelMap = accOppSelector.byIdAsMap(accOppIdSet);
        for (Product_Opportunity_Assignment__c prodOpp : prodOppAssignmentLst) {
            Account_Opportunity_Relationship__c accOppRel = accOppRelMap.get(prodOpp.OpportunityMemberId__c);
            prodOpp.CurrencyIsoCode = accOppRel.Account__r.CurrencyIsoCode;
        }
    }    

    public void updateCurrencyRelatedPartnerData(Map<Id, Product_Opportunity_Assignment__c> prodOppAssignmentMap) {
        fflib_ISObjectUnitOfWork uow = Application.UnitOfWork.newInstance();
        Set<Id> prodAssignmentIdSet = new Set<Id>();
        ProductAssignmentSelector prodAssingmentSelector = (ProductAssignmentSelector)Application.Selector.newInstance(Product_Assignment__c.sobjectType);
        
        for (Product_Opportunity_Assignment__c prodOppAssgnment : prodOppAssignmentMap.values()) {
            prodAssignmentIdSet.add(prodOppAssgnment.ProductAssignmentId__c);
        }

        Map<Id, Product_Assignment__c> prodAssignmentMap = prodAssingmentSelector.selectProductAssignmentByIdAsMap(prodAssignmentIdSet);
        
        for (Product_Opportunity_Assignment__c prodOppAssgnment : prodOppAssignmentMap.values()) {
            Product_Assignment__c prodAssignment = prodAssignmentMap.get(prodOppAssgnment.ProductAssignmentId__c);

            if (prodAssignment != null) {
                prodAssignment.CurrencyIsoCode = prodOppAssgnment.CurrencyIsoCode;
                uow.registerDirty(prodAssignment);
            }
        }

        setCurrencyCommConditionsAndProdItems(uow, prodAssignmentMap, prodAssignmentIdSet);

        try{            
            uow.commitWork();   
        } catch( Exception e){
            DebugLog__c log = new DebugLog__c(
                    Origin__c = '['+className+'][updateCurrencyRelatedPartnerData]',
                    LogType__c = constants.LOGTYPE_ERROR,
                    RelatedToSquad__c = constants.LOGTYPE_SQUAD_PARTNERS,
                    ExceptionMessage__c = e.getMessage(),
                    StackTrace__c = e.getStackTraceString(),
                    DataScope__c = JSON.serialize(uow) // test to verify how the uow would look like at this point
            );
            Logger.createLog(log);
            throw new ProdOppAssignmentServiceImplException(constants.ERROR_UPDATING_NETWORK + e.getMessage());
        }  
    }

    public void updateCurrencyRelatedPartnerData(Map<Id, Product_Opportunity_Assignment__c> prodOppAssignmentNewMap, Map<Id, Product_Opportunity_Assignment__c> prodOppAssignmentOldMap) {
        Map<Id, Product_Opportunity_Assignment__c> prodOppAssignmentMap = new Map<Id, Product_Opportunity_Assignment__c>();
        
        for (Product_Opportunity_Assignment__c newProdOppAssignment : prodOppAssignmentNewMap.values()) {
            Product_Opportunity_Assignment__c oldProdOppAssignment = prodOppAssignmentOldMap.get(newProdOppAssignment.Id);

            if (newProdOppAssignment.ProductAssignmentId__c != oldProdOppAssignment.ProductAssignmentId__c) {
                prodOppAssignmentMap.put(newProdOppAssignment.Id, newProdOppAssignment);
            }
        }

        updateCurrencyRelatedPartnerData(prodOppAssignmentMap);
    }

    private void setCurrencyCommConditionsAndProdItems(fflib_ISObjectUnitOfWork uow, Map<Id, Product_Assignment__c> prodAssignmentMap, Set<Id> prodAssignmentIdSet) {
        Set<Id> commConditionIdSet = new Set<Id>();
        Set<Id> prodIdSet = new Set<Id>();

        ProductItemSelector prodItemSelector = new ProductItemSelector();
        CommercialConditionSelector commConditionSelector = new CommercialConditionSelector();
        
        for (Product_Assignment__c prodAssignment : prodAssignmentMap.values()) {
            commConditionIdSet.add(prodAssignment.CommercialConditionId__c);
            prodIdSet.add(prodAssignment.ProductId__c);
        }
 
        Map<Id, Commercial_Condition__c> commConditionMap = commConditionSelector.selectCommConditionByIdAsMap(commConditionIdSet);
        Map<Id, Product_Item__c> prodItemMap = prodItemSelector.selectProductByIdAsMap(prodIdSet);

        for (Product_Assignment__c prodAssignment : prodAssignmentMap.values()) {
            Commercial_Condition__c commCondition = commConditionMap.get(prodAssignment.CommercialConditionId__c);
            Product_Item__c prodItem = prodItemMap.get(prodAssignment.ProductId__c);

            if (commCondition != null) {
                commCondition.CurrencyIsoCode = prodAssignment.CurrencyIsoCode;
                uow.registerDirty(commCondition);
            }

            if (prodItem != null) {
                prodItem.CurrencyIsoCode = prodAssignment.CurrencyIsoCode;
                uow.registerDirty(prodItem);
            }
        }
    }

    private class ProdOppAssignmentServiceImplException extends Exception {

    }
}