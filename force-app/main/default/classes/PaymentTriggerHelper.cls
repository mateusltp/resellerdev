public with sharing class PaymentTriggerHelper {   

    public void checkIfFrequencyHaschanged(List<Payment__c> newBillingSettings, Map<Id,Payment__c> oldBillingSettings){
        List<Payment__c> lstPayment = new List<Payment__c>();
        for ( Payment__c payment : newBillingSettings ){
            if(payment.Payment_Code__c != null){
                if ( payment.Payment_Code__c.contains('Enterprise_Subscription') && payment.get('Frequency__c') != oldBillingSettings.get(payment.Id).get('Frequency__c') ){
                    lstPayment.add(payment);                
                }
            }
        }
        if(lstPayment.size() > 0){
            changeFlagSpecialistApprovedToFalse(lstPayment);
        }
        
    }
    public void changeFlagSpecialistApprovedToFalse(List<Payment__c> lstPayment){        
        Set<Id> qtLineItem = new Set<Id>();
        Set<Id> qt = new Set<Id>();
        List<Opportunity> oppsToUpddate = new List<Opportunity>(); 
        
        for (Payment__c pay : lstPayment){
            qtLineItem.add(pay.Quote_Line_Item__c);
        }
        List<QuoteLineItem> lstQuoteLineItem = [SELECT QuoteId FROM QuoteLineItem WHERE Id =: qtLineItem];        

        for (QuoteLineItem iQuoteLineItem : lstQuoteLineItem ){
            qt.add(iQuoteLineItem.QuoteId);
        }
        List<Opportunity> lstOpp = [SELECT Id FROM Opportunity WHERE SyncedQuoteId IN :qt];       

        for(Opportunity opp : lstOpp){
            opp.Specialist_Approved__c = false;
            oppsToUpddate.add(opp);                
        }
        
        if(oppsToUpddate.size() > 0){
            Database.update(oppsToUpddate);            
        }
    }
}