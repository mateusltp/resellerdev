/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 03-07-2022
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/
@IsTest
public with sharing class PartnerChildOpportunityManagerTest {

    private static final PS_Constants constants = PS_Constants.getInstance();

    @TestSetup
    static void setupData(){
        
        String recordTypePartnerFlow = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Account').getRecordTypeId();
        Account partnerAcc = PartnerDataFactory.newAccount();
        partnerAcc.RecordTypeId = recordTypePartnerFlow;
        partnerAcc.CRM_ID__c = '1231';
        partnerAcc.Legal_Document_Type__c = 'CNPJ';
        //partnerAcc.Id_Company__c = 'XX.XXX.XXX/XXXX-XX';
        partnerAcc.Legal_Title__c = 'SAA';
        partnerAcc.BillingCountryCode = 'BR';
        partnerAcc.BillingPostalCode = '1223';
        partnerAcc.ShippingCountryCode = 'US';
        partnerAcc.ShippingCountry = 'United States';
        partnerAcc.ShippingState = 'Washington';
        partnerAcc.Description = 'dasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasda';
        INSERT partnerAcc;
                
        Account childAcc = PartnerDataFactory.newChildAccount(partnerAcc.Id);
        childAcc.RecordTypeId = recordTypePartnerFlow;
        childAcc.CRM_ID__c = '4455';
        childAcc.Legal_Document_Type__c = 'CNPJ';
        childAcc.Partner_Level__c = 'Location';
        childAcc.Legal_Title__c = 'SAA11';
        childAcc.BillingCountryCode = 'BR';
        childAcc.BillingPostalCode = '1223';
        childAcc.ShippingCountryCode = 'BR';
        childAcc.ShippingCountry = 'Brazil';
        childAcc.ShippingState = 'São Paulo';
        childAcc.Description = 'dasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasdadasdxzxcsdasda';
        INSERT childAcc;
             
        String recordTypePartnerContactFlow = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Contact').getRecordTypeId();
        Contact partnerContact = PartnerDataFactory.newContact(partnerAcc.Id);
        partnerContact.RecordTypeId = recordTypePartnerContactFlow;        
        partnerContact.Type_of_Contact__c = 'Decision Maker';
        partnerContact.FirstName = 'Admin';
        partnerContact.UUID__c = new Uuid().getValue();
        INSERT partnerContact;    

        partnerAcc.Legal_Representative__c = partnerContact.Id;
        UPDATE partnerContact;

        Opportunity aOpp = PartnerDataFactory.newOpportunity( partnerAcc.Id, 'Partner_Flow_Opportunity'); 
        INSERT aOpp;

        Quote proposal = PartnerDataFactory.newQuote( aOpp );
        proposal.RecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Proposal').getRecordTypeId();
        proposal.Signed__c = Date.Today() - 100;
        proposal.End_Date__c = Date.Today();
        proposal.Start_Date__c = Date.Today() - 100;
        INSERT proposal;


        Account_Opportunity_Relationship__c oppMember = PartnerDataFactory.newAcctOppRel(partnerAcc.Id, aOpp.Id);
        oppMember.RecordTypeId = Schema.SObjectType.Account_Opportunity_Relationship__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Opportunity_Member').getRecordTypeId();
        INSERT oppMember;

        Account_Opportunity_Relationship__c oppMemberChild = PartnerDataFactory.newAcctOppRel(childAcc.Id, aOpp.Id);
        oppMemberChild.RecordTypeId = Schema.SObjectType.Account_Opportunity_Relationship__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Opportunity_Member').getRecordTypeId();
        INSERT oppMemberChild; 


        Product_Item__c aProd = PartnerDataFactory.newProduct( aOpp );
        aProd.Type__c = 'Live';
        aProd.Net_Transfer_Price__c = 120;
        aProd.RecordTypeId = Schema.SObjectType.Product_Item__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product').getRecordTypeId();
        aProd.Opportunity__c = null;       
        INSERT aProd;

        Commercial_Condition__c aComm = new Commercial_Condition__c();
        aComm.RecordTypeId = Schema.SObjectType.Commercial_Condition__c.getRecordTypeInfosByDeveloperName().get('CAP').getRecordTypeId();
        aComm.Amount__c = 120;
        INSERT aComm;

        Product_Assignment__c prodAssign = new Product_Assignment__c();
        prodAssign.RecordTypeId = Schema.SObjectType.Product_Assignment__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product_Assignment').getRecordTypeId();
        prodAssign.ProductId__c = aProd.Id;
        prodAssign.CommercialConditionId__c = aComm.Id;
        INSERT prodAssign;


        Product_Opportunity_Assignment__c prodOppAssign = new Product_Opportunity_Assignment__c();
        prodOppAssign.RecordTypeId = Schema.SObjectType.Product_Opportunity_Assignment__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product_Opportunity_Assignment').getRecordTypeId();
        prodOppAssign.OpportunityMemberId__c = oppMember.Id;
        prodOppAssign.ProductAssignmentId__c = prodAssign.Id;
        INSERT prodOppAssign;

        Threshold__c threshold = new Threshold__c();
        threshold.recordTypeId = Schema.SObjectType.Threshold__c.getRecordTypeInfosByDeveloperName().get(constants.THRESHOLD_RT_PARTNER_FLOW).getRecordTypeId();
        threshold.Threshold_value_start__c = 10;
        threshold.Threshold_value_end__c = 20;
        threshold.Volume_discount_date__c = Date.today();
        INSERT threshold;

        Product_Threshold_Member__c prodThresholdMember = new Product_Threshold_Member__c();
        prodThresholdMember.recordTypeId = Schema.SObjectType.Product_Threshold_Member__c.getRecordTypeInfosByDeveloperName().get(constants.PRODUCT_THRESHOLD_MEMBER_RT_PARTNER_FLOW).getRecordTypeId();
        prodThresholdMember.Product__c = aProd.Id;
        prodThresholdMember.Threshold__c = threshold.Id;
        INSERT prodThresholdMember;

        Gym_Activity_Relationship__c gymAct = new Gym_Activity_Relationship__c();
        gymAct.Name = 'Test Act';
        INSERT gymAct;

        Product_Activity_Relationship__c prodActRel = new Product_Activity_Relationship__c();
        prodActRel.recordTypeId = Schema.SObjectType.Product_Activity_Relationship__c.getRecordTypeInfosByDeveloperName().get(constants.PRODUCT_ACTIVITY_RT_PARTNER_FLOW).getRecordTypeId();
        prodActRel.Gym_Activity_Relationship__c= gymAct.Id;
        prodActRel.Product__c = aProd.Id;
        INSERT prodActRel;

    }

    @IsTest
    private static void getLocationsByLegalGroup(){

        Opportunity opp = [SELECT Id FROM Opportunity WHERE Name LIKE '%Test Opp' LIMIT 1];
        Account acc = [SELECT ID FROM Account WHERE Partner_Level__c = 'Location' LIMIT 1];
        String idChild = '/'+acc.Id;
        Test.startTest();

            List<List<PartnerChildOpportunityManagerController.Account_Item>> response = PartnerChildOpportunityManagerController.getLocationsByLegalGroup(opp.Id);
            System.assert(response.size()>0);
            System.assertEquals(response.get(0).get(0).accountId, idChild);
            
        Test.stopTest();
    }

    @IsTest
    private static void createChildOpportunity(){

        Opportunity opp = [SELECT Id FROM Opportunity WHERE Name LIKE '%Test Opp' LIMIT 1];
        Account acc = [SELECT ID FROM Account WHERE ParentId = null LIMIT 1];
        List<String> accountsInOpp = new List<String>();

        for(Account_Opportunity_Relationship__c iOppMember: [SELECT ID, Account__c, Opportunity__c FROM Account_Opportunity_Relationship__c WHERE Opportunity__c =: opp.Id]){
            
            accountsInOpp.add(iOppMember.Account__c);
        }
   
        Test.startTest();
            String response = PartnerChildOpportunityManagerController.createChildOpportunity('Child Opp', Date.today(), UserInfo.getUserId(), acc.Id, opp.Id, accountsInOpp );
            Opportunity oppChild = [SELECT Id, MasterOpportunity__c FROM Opportunity WHERE RecordType.DeveloperName =: constants.OPPORTUNITY_RT_PARTNER_FLOW_CHILD_OPP LIMIT 1];
            System.debug('oppChild.MasterOpportunity__c ' + oppChild.MasterOpportunity__c);
            System.debug('oppChild.MasterOpportunity__c 2 ' + opp.Id);
            
            System.assertEquals(oppChild.MasterOpportunity__c, opp.Id);
        Test.stopTest();

    }

    @IsTest
    private static void createChildOpportunity_ERROR(){

        Opportunity opp = [SELECT Id FROM Opportunity WHERE Name LIKE '%Test Opp' LIMIT 1];
        Account acc = [SELECT ID FROM Account WHERE ParentId = null LIMIT 1];
        List<String> accountsInOpp = new List<String>();
    
        Test.startTest();
             try{
                String response = PartnerChildOpportunityManagerController.createChildOpportunity('Child Opp', Date.today(), '111111', acc.Id, opp.Id, accountsInOpp );
          
            } catch (Exception e){
                System.assert(e instanceOf PartnerChildOpportunityManagerController.PartnerChildOpportunityManagerControllerException);
            } 
        Test.stopTest();
    }

    @IsTest
    private static void getMainOpportunityMember(){

        Opportunity opp = [SELECT Id FROM Opportunity WHERE Name LIKE '%Test Opp' LIMIT 1];
        Account acc = [SELECT ID FROM Account WHERE ParentId = null LIMIT 1];
        Account_Opportunity_Relationship__c oppMember = [SELECT Id, Account__c, Opportunity__c FROM Account_Opportunity_Relationship__c WHERE Account__c =: acc.Id AND Opportunity__c =:opp.Id LIMIT 1];
        Test.startTest();
            Id response = PartnerChildOpportunityManagerController.getMainOpportunityMember(opp.Id, acc.Id);
            System.assertEquals(response, oppMember.Id);
        Test.stopTest();

    }

    @IsTest
    private static void getMainOpportunityMember_ERROR(){
        Id fakeAccId;
        Id fakeOppId;

        Test.startTest();
            try{
                Id response = PartnerChildOpportunityManagerController.getMainOpportunityMember(fakeOppId , fakeAccId);
            } catch (Exception e){
                System.assert(e instanceOf PartnerChildOpportunityManagerController.PartnerChildOpportunityManagerControllerException);
            } 
        Test.stopTest();
    }

   

}