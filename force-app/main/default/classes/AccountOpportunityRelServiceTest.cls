@IsTest
public with sharing class AccountOpportunityRelServiceTest {
    
    @IsTest
    private static void createAccountOpportunityRelationship_UnitTest() {  

        Id mockAccId = fflib_IDGenerator.generate(Account.SObjectType);
        Id mockOppId = fflib_IDGenerator.generate(Opportunity.SObjectType);
        Map<Id, Id> mockAccountOppIdsMap = new Map<Id, Id>{mockAccId => mockOppId};

        fflib_ApexMocks mocks = new fflib_ApexMocks();
        fflib_SObjectUnitOfWork mockUOW = (fflib_SObjectUnitOfWork)mocks.mock(fflib_SObjectUnitOfWork.class);

        mocks.startStubbing();
            ((fflib_SObjectUnitOfWork)mocks.doThrowWhen(new AccountOpportunityRelPartnerServiceImpl.AccountOpportunityRelPartnerServiceImplException(), mockUOW)).commitWork();
        mocks.stopStubbing();

        Application.UnitOfWork.setMock(mockUOW);

        try {
            Test.startTest();
            AccountOpportunityRelationshipService.createAccountOpportunityRelationship(mockAccountOppIdsMap, 'Account_Opportunity_Relationship__c.Partners_AOR');
            Test.stopTest();
        } catch (Exception e){
            system.debug(e.getMessage());
            System.assert(e instanceof AccountOpportunityRelPartnerServiceImpl.AccountOpportunityRelPartnerServiceImplException);
        }
    }
}