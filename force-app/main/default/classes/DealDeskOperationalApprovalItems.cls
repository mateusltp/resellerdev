/**
 * @description       : Deal Desk Operational Approval Items management
 * @author            : Tiago Ribeiro
 * @group             : Corporate
 * @last modified on  :
 * @last modified by  :
 * Modifications Log 
 * Ver   Date         Author                              Modification
 * 1.0   01-31-2022   Tiago Ribeiro                       Initial Version
**/
public with sharing class DealDeskOperationalApprovalItems {
    public void createApprovalItems(List<Case> listNewCases){
        Id ddOperationalRecType = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Deal_Desk_Operational').getRecordTypeId();
        List<Approval_Item__c> listApprovalItems = new List<Approval_Item__c>();
        ApprovalItemService approvalItemServ = new ApprovalItemService();

        for(Case newCase : listNewCases){
            if(newCase.RecordTypeId == ddOperationalRecType && newCase.Description != null){
                Map<String, String> mapItemNameFieldValue = new Map<String, String>();
                List<String> listApprovalItemsToCreate = newCase.Description.split(',');
                for(String approvalItem : listApprovalItemsToCreate){
                    List<String> listApprovalItemAndValue = approvalItem.split(':');
                    if(!listApprovalItemAndValue.isEmpty()){
                        String approvalItemName = listApprovalItemAndValue[0];
                        String approvalItemFieldValue = listApprovalItemAndValue.size() > 1 ? listApprovalItemAndValue[1] : '';
                        listApprovalItems.add(approvalItemServ.generateApprovalItem(newCase.Id, approvalItemName, approvalItemFieldValue, 'Open'));
                    }
                }
            }
        }

        approvalItemServ.createApprovalItems(listApprovalItems);
    }
}