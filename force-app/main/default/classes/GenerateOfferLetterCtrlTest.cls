@isTest
public class GenerateOfferLetterCtrlTest {
    
    @isTest
    public static void functionalTest() {
        Account acc = new Account();
        acc.Name = 'Acc Teste';
        acc.BillingState = 'São Paulo';
        acc.BillingCountry = 'Brazil';
        insert acc;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Opp Test';
        opp.StageName = 'Qualificação';
        opp.CloseDate = Date.today();
        opp.AccountId = acc.Id;
        insert opp;

        GenerateOfferLetterCtrl.generateOfferLetter(opp.Id);
    }
}