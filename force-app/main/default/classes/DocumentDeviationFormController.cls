public with sharing class DocumentDeviationFormController {    
    
    @AuraEnabled
    public static List<DocumentDeviationFormTO> getDocumentDeviation(String recordId){        
        Map<String, String> labelsOfFields = new Map<String,String>();        
        List<DocumentDeviationFormTO> documentDeviationFormTO = new List<DocumentDeviationFormTO>();
        List<String> documentFieldsApiName = new List<String>();                        
        List<Document_Deviation_Form__mdt> docDeviationForm = [SELECT FieldApiName__c, AccordionSection__c, Template__c, FieldOtherDeviation__c, TextAboutFieldLabelB2B__c, TextAboutFieldLabelPartner__c FROM Document_Deviation_Form__mdt];
        
        Map<String, String> mpFieldAndSection = DocumentDeviationForm.getMpFieldAndSection(docDeviationForm);
        Map<String, String> mpFieldAndTemplate = DocumentDeviationForm.getMpFieldAndTemplate(docDeviationForm);
        Map<String, String> mpFieldAndOtherField = DocumentDeviationForm.getMpFieldAndOtherField(docDeviationForm);
        Map<String, String> mpFieldAndTextB2B = DocumentDeviationForm.getMpFieldAndTextB2B(docDeviationForm);                
        Map<String, String> mpFieldAndTextPartner = DocumentDeviationForm.getMpFieldAndTextPartner(docDeviationForm);
        
        List<DocumentDeviation__c> documentDeviation = DocumentDeviationForm.getDocumentDeviationQuery(recordId);
        documentFieldsApiName = DocumentDeviationForm.getFieldOfFieldSets(recordId);
        labelsOfFields = DocumentDeviationForm.getLabelsOfFields(documentFieldsApiName);
        String businessUnit = getBusinessUnit(recordId);            
            
            for (String docFields : documentFieldsApiName) {
                if( !docFields.contains('Other__c') ){                                
                    documentDeviationFormTO.add( 
                        new DocumentDeviationFormTO(
                            mpFieldAndSection.get(docFields),
                            labelsOfFields.get(docFields),
                            DocumentDeviationForm.getPickListValues(docFields),
                            docFields,
                            businessUnit.contains('B2B') || businessUnit.contains('Indirect') ? mpFieldAndTextB2B.get(docFields) : mpFieldAndTextPartner.get(docFields),
                            mpFieldAndOtherField.get(docFields),
                            mpFieldAndTemplate.get(docFields),
                            documentDeviation.size() > 0 ? (String)documentDeviation[0].get(docFields) : DocumentDeviationForm.getDefaultValuePickList(docFields),
                            documentDeviation.size() > 0 ? (String)documentDeviation[0].get(mpFieldAndOtherField.get(docFields)) :  ''
                        ));                        
                }
            }
            return documentDeviationFormTO;            
    }    

    @AuraEnabled
    public static String getAnyOtherDetailField( String recordId ){
        List<DocumentDeviation__c> documentDeviation = DocumentDeviationForm.getDocumentDeviationQuery(recordId);        
        if(documentDeviation.size() > 0){
            return documentDeviation[0].AnyOtherDetail__c;
        }else{
            return '';
        }        
    }    

    @AuraEnabled
    public static String getBusinessUnit(String recordId){        
        return [SELECT BusinessUnit__c FROM Case WHERE Id = :recordId].BusinessUnit__c;
    } 

    @AuraEnabled
    public static void saveDocumentDeviationForm(String documentDeviationFormJSON, String recordId, String anyOtherDetail){        
        
        Map<String, String> mpDeviation = new Map<String, String>();        
        
        List<DocumentDeviationFormTO> lstDocumentDeviation = (List<DocumentDeviationFormTO>) JSON.deserialize(documentDeviationFormJSON, List<DocumentDeviationFormTO>.class);
        
        for(DocumentDeviationFormTO doc : lstDocumentDeviation){        
            mpDeviation.put(doc.fieldApiName, doc.value);
            mpDeviation.put(doc.otherDeviationField, doc.otherDeviationValue);         
        }

        DocumentDeviation__c documentDeviation = new DocumentDeviation__c(); 

        for (String deviation : mpDeviation.KeySet()){
            if( !String.isEmpty(mpDeviation.get(deviation)) ){
            documentDeviation.put(deviation, mpDeviation.get(deviation));
            }
        }       
        
            documentDeviation.Document__c = DocumentDeviationForm.getDocumentId(recordId);
            documentDeviation.Case__c= recordId;            
            documentDeviation.AnyOtherDetail__c = anyOtherDetail;
            List<DocumentDeviation__c> documentDeviationId = [SELECT Id FROM DocumentDeviation__c WHERE Case__c = :recordId];

            if( documentDeviationId.size() > 0){
                documentDeviation.Id = documentDeviationId[0].Id;
            }            
            upsert documentDeviation;
    }

    public class DocumentDeviationFormTO {

        @AuraEnabled public String accordionSection; 
        @AuraEnabled public String labelRadioGroup; 
        @AuraEnabled public List<RadioOptions> optionsRadioGroup;         
        @AuraEnabled public String fieldApiName;
        @AuraEnabled public String textAboutLabelRadioGroup; 
        @AuraEnabled public String otherDeviationField;
        @AuraEnabled public String template;
        @AuraEnabled public String value;
        @AuraEnabled public String otherDeviationValue;

        public DocumentDeviationFormTO (String accordionSection,
                                        String labelRadioGroup,
                                        List<RadioOptions> optionsRadioGroup,
                                        String fieldApiName,
                                        String textAboutLabelRadioGroup,
                                        String otherDeviationField,
                                        String template,
                                        String value,
                                        String otherDeviationValue){
            this.accordionSection = accordionSection;
            this.labelRadioGroup = labelRadioGroup;
            this.optionsRadioGroup = optionsRadioGroup;
            this.fieldApiName = fieldApiName;
            this.textAboutLabelRadioGroup = textAboutLabelRadioGroup;
            this.otherDeviationField = otherDeviationField;
            this.template = template;
            this.value = value;
            this.otherDeviationValue = otherDeviationValue; 
        }
        public DocumentDeviationFormTO(){}
    }

    public class RadioOptions{
        @AuraEnabled public String label; 
        @AuraEnabled public String value; 

        public RadioOptions(String label, String value){
            this.label = label;
            this.value = value;
        }
        public RadioOptions(){}
    }
}