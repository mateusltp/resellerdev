/**
 * @description       : 
 * @author            : Mateus Augusto - GFT (moes@gft.com)
 * @group             : 
 * @last modified on  : 05-12-2022
 * @last modified by  : Mateus Augusto - GFT (moes@gft.com)
**/
public without sharing class OpportunityTriggerPricebookHelper {

    public static Id OPPORTUNITY_RECORD_TYPE_NEW_BUS_INDIRECT_CHANNEL = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Indirect_Channel_New_Business').getRecordTypeId();
    public static Id OPPORTUNITY_RECORD_TYPE_INDIRECT_CHANNEL = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Indirect_Channel').getRecordTypeId();

    public OpportunityTriggerPricebookHelper() {}
    Map< String , String > gMapRTDevNameRtId = new Map< String , String >{
        'Client_Sales_New_Business' => Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId(),
        'Client_Success_Renegotiation' => Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Success_Renegotiation').getRecordTypeId(),
        'SMB_New_Business' => Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SMB_New_Business').getRecordTypeId(),
        'SMB_Success_Renegotiation' => Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SMB_Success_Renegotiation').getRecordTypeId()
    };
    public void setPricebookFromAccountCountry() {
        Set<Id> accountIdSet = new Set<Id>();
        Set<Id> accountResellerIdSet = new Set<Id>();
        Set<String> pricebookNameSet = new Set<String>();

        List<Pricebook2> pricebookList = new List<Pricebook2>();
        Map<Id, Account> idToAccountMap = new Map<Id, Account>();
        Map<Id, Account> accountIdReseller_map = new Map<Id, Account>();
        Map<Id, Opportunity> accountIdToOpportunityMap = new Map<Id, Opportunity>();
        Map<String, Pricebook2> countryNameToPricebookMap = new Map<String, Pricebook2>();
        Map<String, Pricebook2> indirectChannelPricebook_map = new Map<String, Pricebook2>();
        List< String > lLstOppRt = gMapRTDevNameRTId.values();

        Id oppRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        Id renegotiationRecTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Success_Renegotiation').getRecordTypeId();

        for (Opportunity opp : (List<Opportunity>)Trigger.new) {
            if (opp.AccountId != null && lLstOppRt.contains( opp.RecordTypeId ) )  {
                accountIdSet.add(opp.AccountId);
                accountIdToOpportunityMap.put(opp.AccountId, opp);
            }
            if (opp.AccountId != null && (opp.RecordTypeId == OPPORTUNITY_RECORD_TYPE_NEW_BUS_INDIRECT_CHANNEL || opp.RecordTypeId == OPPORTUNITY_RECORD_TYPE_INDIRECT_CHANNEL))  {
                accountIdSet.add(opp.AccountId);
                accountResellerIdSet.add(opp.Reseller_del__c);
                accountIdToOpportunityMap.put(opp.AccountId, opp);
            }
        }
		if(accountIdSet.isEmpty()) return;
        // Get Accounts
        //idToAccountMap = new AccountRepository().getMapByIds(accountIdSet);
        accountIdReseller_map = new AccountRepository().getMapByIds(accountResellerIdSet);
        indirectChannelPricebook_map = new PricebookRepository().getIndirectChannelPricebookMap();

        // Get PricebookNames
        // pricebookNameSet = getPricebookNamesFromMetadata();
        // Get Pricebooks
        // pricebookList = new PricebookRepository().forNames(pricebookNameSet);
        // Get Pricebook Map by country name
        //countryNameToPricebookMap = getCountryToPricebookMap(pricebookList);
        //assignPricebookToOpps(countryNameToPricebookMap, idToAccountMap, accountIdToOpportunityMap);
        // Get Standart Pricebook
        //Pricebook2 standardPricebook = new PricebookRepository().standard();
        
        try{
            for (Opportunity opp : (List<Opportunity>)Trigger.new) {
                if (opp.AccountId != null && (opp.RecordTypeId == OPPORTUNITY_RECORD_TYPE_NEW_BUS_INDIRECT_CHANNEL || opp.RecordTypeId == OPPORTUNITY_RECORD_TYPE_INDIRECT_CHANNEL))  {
                    //assignStandartPricebookToOpps(accountIdToOpportunityMap, standardPricebook);
                    assignIndirectChannelPricebookToOpps(accountIdToOpportunityMap, accountIdReseller_map, indirectChannelPricebook_map);
                   
            }else{
            // Get Map Pricebooks Pricebook
            Map< String , String > lMapRtIdPricebookId = getMapClassInstanceByRTId();
            assignStandartPricebookToOpps(accountIdToOpportunityMap, lMapRtIdPricebookId);
            }
        }
        }catch(System.QueryException e){
            //system.debug('ERRO'+e.getMessage());
           // standardPricebook = [select Id From Pricebook2 limit 1];
        }
        //assignStandartPricebookToOpps(accountIdToOpportunityMap, standardPricebook);
    }
    @TestVisible
    private Map< String , String > getMapClassInstanceByRTId() {
        Pricebook2 lClientsPricebook = new ClientsPricebookRepository().getPricebook();
        Pricebook2 lSMBPricebook = new SMBPricebookRepository().getPricebook();
        Map< String , String > lMapRtIdPricebookId = new Map< String , String >{
            gMapRTDevNameRtId.get('Client_Sales_New_Business') => lClientsPricebook.Id,
            gMapRTDevNameRtId.get('Client_Success_Renegotiation') => lClientsPricebook.Id,
            gMapRTDevNameRtId.get('SMB_New_Business') => lClientsPricebook.Id,
            gMapRTDevNameRtId.get('SMB_Success_Renegotiation') => lClientsPricebook.Id
        };
        return lMapRtIdPricebookId;
    }
    private void assignStandartPricebookToOpps(Map<Id, Opportunity> accountIdToOpportunityMap, Map< String , String > aMapRtIdPricebookId) {
        for (Opportunity opp : accountIdToOpportunityMap.values()) {
            opp.Pricebook2Id = aMapRtIdPricebookId.get( opp.RecordTypeId );
        }
    }

    private void assignIndirectChannelPricebookToOpps(Map<Id, Opportunity> accountIdToOpportunityMap, Map<Id, Account> accountIdReseller_map, Map<String, Pricebook2> indirectChannelPricebook_map){
        for(Opportunity opp : (List<Opportunity>)Trigger.new){
            if(accountIdReseller_map.containsKey(opp.Reseller_del__c) && (opp.RecordTypeId == OPPORTUNITY_RECORD_TYPE_NEW_BUS_INDIRECT_CHANNEL || opp.RecordTypeId == OPPORTUNITY_RECORD_TYPE_INDIRECT_CHANNEL)){
                Account opp_ref_account = accountIdReseller_map.get(opp.Reseller_del__c);
                if(opp.B_M__c == 'Total Subsidy'){
                    if(opp_ref_account.Indirect_Channel_Pricebook__c == 'Legacy Partners' || opp_ref_account.Indirect_Channel_Pricebook__c == 'Standard'){
                        opp.Pricebook2Id = String.valueOf(indirectChannelPricebook_map.get('Subsidy').Id);
                    }
                    else if(opp_ref_account.Indirect_Channel_Pricebook__c == '5k FTEs'){
                        opp.Pricebook2Id = String.valueOf(indirectChannelPricebook_map.get('Subsidy 5k').Id);
                    }
                    else if(opp_ref_account.Indirect_Channel_Pricebook__c == '15k FTEs'){
                        opp.Pricebook2Id = String.valueOf(indirectChannelPricebook_map.get('Subsidy 15k').Id);
                    }
                }
                else if(opp.B_M__c == 'Intermediation'){
                    opp.Pricebook2Id = String.valueOf(indirectChannelPricebook_map.get('Intermediation').Id);
                }
                else if(opp.B_M__c == 'Exclusive'){
                    opp.Pricebook2Id = String.valueOf(indirectChannelPricebook_map.get('Exclusive').Id);
                }
                
            }
        }
    }
    @TestVisible
    private Set<String> getPricebookNamesFromMetadata() {
        Set<String> pricebookNames = new Set<String>();
        List<Country_Pricebook__mdt> countryPricebooks = [
            SELECT Country_Name__c, Pricebook_Name__c
            FROM Country_Pricebook__mdt
        ];
        for (Country_Pricebook__mdt countryPricebook : countryPricebooks)
            pricebookNames.add(countryPricebook.Pricebook_Name__c);
        return pricebookNames;
    }
    @TestVisible
    private Map<String, Pricebook2> getCountryToPricebookMap(List<Pricebook2> pricebookList) {
        Map<String, Pricebook2> countryNameToPricebookMap = new Map<String, Pricebook2>();
        for (Pricebook2 pb : pricebookList) {
            countryNameToPricebookMap.put(pb.Country__c, pb);
        }
        return countryNameToPricebookMap;
    }
    @TestVisible
    private void assignPricebookToOpps(Map<String, Pricebook2> countryNameToPricebookMap, Map<Id, Account> idToAccountMap, Map<Id, Opportunity> accountIdToOpportunityMap) {
        for (Opportunity opp : accountIdToOpportunityMap.values()) {
            Account acc = idToAccountMap.get(opp.AccountId);
            Pricebook2 pb = countryNameToPricebookMap.get(acc.BillingCountry);

            opp.Pricebook2Id = (pb != null ? pb.Id : null);
        }
    }
}