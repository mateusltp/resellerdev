public abstract without sharing class ClientDealDAO {
  public static void upsertClientDeals(List<ClientDealBO> clientDeals) {
    List<Opportunity> opportunities = new List<Opportunity>();
    List<Quote> quotes = new List<Quote>();
    List<QuoteLineItem> quoteLineItems = new List<QuoteLineItem>();
    List<Waiver__c> waivers = new List<Waiver__c>();
    List<Payment__c> payments = new List<Payment__c>();
    List<Eligibility__c> eligibilities = new List<Eligibility__c>();

    for (ClientDealBO clientDeal : clientDeals) {
      if (
        clientDeal.getOpportunity() == null ||
        clientDeal.getQuote() == null
      ) {
        continue;
      }

      opportunities.add(clientDeal.getOpportunity());
      quotes.add(clientDeal.getQuote());

      for (ClientDealBO.SalesItem salesItem : clientDeal.getSalesItems()) {
        quoteLineItems.add(salesItem.getQuoteLineItem());
        waivers.addAll(salesItem.getWaivers());
      }

      for (
        ClientDealBO.PaymentItem paymentItem : clientDeal.getPaymentItems()
      ) {
        payments.add(paymentItem.getPayment());
        eligibilities.addAll(paymentItem.getEligibilities());
      }
    }

    if (!opportunities.isEmpty()) {
      Database.upsert(opportunities);
    }

    if (!quotes.isEmpty()) {
      Database.upsert(quotes);
    }

    if (!quoteLineItems.isEmpty()) {
      Database.upsert(quoteLineItems);
    }

    if (!waivers.isEmpty()) {
      Database.upsert(waivers);
    }

    if (!payments.isEmpty()) {
      Database.upsert(payments);
    }

    if (!eligibilities.isEmpty()) {
      Database.upsert(eligibilities);
    }
  }

  public static List<ClientDealBO> getClientDealsByOpportunities(
    List<Opportunity> opportunities
  ) {
    if (opportunities == null || opportunities.isEmpty()) {
      return new List<ClientDealBO>();
    }

    List<Quote> quotes = queryQuotesByOpportunities(opportunities);

    SearchEngine searchEngine = new SearchEngine();
    searchEngine.setOpportunities(opportunities);
    searchEngine.setOrders(queryOrdersByOpportunities(opportunities));
    searchEngine.setPayments(queryPaymentsByOpportunities(opportunities));
    searchEngine.setQuotes(quotes);
    searchEngine.setQuoteLineItems(queryQuoteLineItemsByQuotes(quotes));

    return getClientDeals(searchEngine);
  }

  private static List<Order> queryOrdersByOpportunities(
    List<Opportunity> opportunities
  ) {
    Map<String, Object> orderFields = (Map<String, Object>) Schema.SObjectType.Order.fields.getMap();

    String soqlOrderFields = '';

    for (String orderFieldName : orderFields.keySet()) {
      soqlOrderFields += (orderFieldName + ',');
    }

    List<Id> accountIds = new List<Id>();

    for (Opportunity opportunity : opportunities) {
      accountIds.add(opportunity.AccountId);
    }

    String soqlQuery =
      'SELECT ' +
      soqlOrderFields.removeEnd(',') +
      ' FROM Order WHERE Status != \'Draft\' AND AccountId IN :accountIds';

    return (List<Order>) Database.query(soqlQuery.escapeJava());
  }

  private static List<Quote> queryQuotesByOpportunities(
    List<Opportunity> opportunities
  ) {
    Map<String, Object> quoteFields = (Map<String, Object>) Schema.SObjectType.Quote.fields.getMap();

    String soqlQuoteFields = '';

    for (String quoteFieldName : quoteFields.keySet()) {
      soqlQuoteFields += (quoteFieldName + ',');
    }

    String soqlQuery =
      'SELECT ' +
      soqlQuoteFields.removeEnd(',') +
      ' FROM Quote WHERE OpportunityId IN :opportunities';

    return (List<Quote>) Database.query(soqlQuery.escapeJava());
  }

  private static List<QuoteLineItem> queryQuoteLineItemsByQuotes(
    List<Quote> quotes
  ) {
    Map<String, Object> quoteLineItemFields = (Map<String, Object>) Schema.SObjectType.QuoteLineItem.fields.getMap();
    Map<String, Object> waiverFields = (Map<String, Object>) Schema.SObjectType.Waiver__c.fields.getMap();

    String soqlQuoteLineItemFields = '';

    for (String quoteLineItemFieldName : quoteLineItemFields.keySet()) {
      soqlQuoteLineItemFields += (quoteLineItemFieldName + ',');
    }

    String soqlQuery = 'SELECT ' + soqlQuoteLineItemFields + '(SELECT ';

    String soqlWaiverFields = '';

    for (String waiverFieldName : waiverFields.keySet()) {
      soqlWaiverFields += (waiverFieldName + ',');
    }

    soqlQuery +=
      soqlWaiverFields.removeEnd(',') +
      ' FROM Waivers__r)' +
      ' FROM QuoteLineItem WHERE QuoteId IN :quotes';

    return (List<QuoteLineItem>) Database.query(soqlQuery.escapeJava());
  }

  private static List<Payment__c> queryPaymentsByOpportunities(
    List<Opportunity> opportunities
  ) {
    Map<String, Object> paymentFields = (Map<String, Object>) Schema.SObjectType.Payment__c.fields.getMap();
    Map<String, Object> eligibilityFields = (Map<String, Object>) Schema.SObjectType.Eligibility__c.fields.getMap();

    String soqlPaymentFields = '';

    for (String paymentFieldName : paymentFields.keySet()) {
      soqlpaymentFields += (paymentFieldName + ',');
    }

    String soqlQuery =
      'SELECT ' +
      soqlpaymentFields +
      '(SELECT Paid_By__r.UUID__c,Managed_By__r.UUID__c,';

    String soqlEligibilityFields = '';

    for (String eligibilityFieldName : eligibilityFields.keySet()) {
      soqlEligibilityFields += (eligibilityFieldName + ',');
    }

    soqlQuery +=
      soqlEligibilityFields.removeEnd(',') +
      ' FROM Eligibility__r)' +
      ' FROM Payment__c WHERE Opportunity__c IN :opportunities';

    return (List<Payment__c>) Database.query(soqlQuery.escapeJava());
  }

  private static List<ClientDealBO> getClientDeals(SearchEngine searchEngine) {
    List<ClientDealBO> clientDeals = new List<ClientDealBO>();

    for (Opportunity opportunity : searchEngine.opportunities) {
      if (!searchEngine.quotesByOpportunityId.containsKey(opportunity.Id)) {
        continue;
      }

      for (
        Quote quote : searchEngine.quotesByOpportunityId.get(opportunity.Id)
      ) {
        ClientDealBO clientDeal = new ClientDealBO(opportunity, quote);

        if (searchEngine.orderByAccountId.containsKey(opportunity.AccountId)) {
          Order order = searchEngine.orderByAccountId.get(
            opportunity.AccountId
          );

          clientDeal.setOrder(order);
        }

        if (searchEngine.quoteLineItemsByQuoteId.containsKey(quote.Id)) {
          for (
            QuoteLineItem quoteLineItem : searchEngine.quoteLineItemsByQuoteId.get(
              quote.Id
            )
          ) {
            clientDeal.addSaleItem(quoteLineItem, quoteLineItem.Waivers__r);
          }
        }

        if (
          searchEngine.paymentsByOpportunityId.containsKey(quote.OpportunityId)
        ) {
          for (
            Payment__c payment : searchEngine.paymentsByOpportunityId.get(
              quote.OpportunityId
            )
          ) {
            clientDeal.addPaymentItem(payment, payment.Eligibility__r);
          }
        }

        clientDeals.add(clientDeal);
      }
    }

    return clientDeals;
  }

  private class SearchEngine {
    private List<Opportunity> opportunities;
    private Map<Id, Order> orderByAccountId;
    private Map<Id, List<Quote>> quotesByOpportunityId;
    private Map<Id, List<QuoteLineItem>> quoteLineItemsByQuoteId;
    private Map<Id, List<Payment__c>> paymentsByOpportunityId;

    private SearchEngine() {
      this.opportunities = new List<Opportunity>();
      this.orderByAccountId = new Map<Id, Order>();
      this.quotesByOpportunityId = new Map<Id, List<Quote>>();
      this.quoteLineItemsByQuoteId = new Map<Id, List<QuoteLineItem>>();
      this.paymentsByOpportunityId = new Map<Id, List<Payment__c>>();
    }

    private void setOpportunities(List<Opportunity> opportunities) {
      this.opportunities = opportunities;
    }

    private void setOrders(List<Order> orders) {
      this.orderByAccountId = getMapOrderByAccountId(orders);
    }

    private Map<Id, Order> getMapOrderByAccountId(List<Order> orders) {
      Map<Id, Order> orderByAccountId = new Map<Id, Order>();

      for (Order order : orders) {
        orderByAccountId.put(order.AccountId, order);
      }

      return orderByAccountId;
    }

    private void setQuotes(List<Quote> quotes) {
      this.quotesByOpportunityId = getMapQuotesByOpportunityId(quotes);
    }

    private Map<Id, List<Quote>> getMapQuotesByOpportunityId(
      List<Quote> quotes
    ) {
      Map<Id, List<Quote>> quotesByOpportunityId = new Map<Id, List<Quote>>();

      for (Quote quote : quotes) {
        if (quotesByOpportunityId.containsKey(quote.OpportunityId)) {
          quotesByOpportunityId.get(quote.OpportunityId).add(quote);
        } else {
          quotesByOpportunityId.put(
            quote.OpportunityId,
            new List<Quote>{ quote }
          );
        }
      }

      return quotesByOpportunityId;
    }

    private void setQuoteLineItems(List<QuoteLineItem> quoteLineItems) {
      this.quoteLineItemsByQuoteId = getMapQuoteLineItemsByQuoteId(
        quoteLineItems
      );
    }

    private Map<Id, List<QuoteLineItem>> getMapQuoteLineItemsByQuoteId(
      List<QuoteLineItem> quoteLineItems
    ) {
      Map<Id, List<QuoteLineItem>> quoteLineItemsByQuoteId = new Map<Id, List<QuoteLineItem>>();

      for (QuoteLineItem quoteLineItem : quoteLineItems) {
        if (quoteLineItemsByQuoteId.containsKey(quoteLineItem.QuoteId)) {
          quoteLineItemsByQuoteId.get(quoteLineItem.QuoteId).add(quoteLineItem);
        } else {
          quoteLineItemsByQuoteId.put(
            quoteLineItem.QuoteId,
            new List<QuoteLineItem>{ quoteLineItem }
          );
        }
      }

      return quoteLineItemsByQuoteId;
    }

    private void setPayments(List<Payment__c> payments) {
      this.paymentsByOpportunityId = getMapPaymentsByOpportunityId(payments);
    }

    private Map<Id, List<Payment__c>> getMapPaymentsByOpportunityId(
      List<Payment__c> payments
    ) {
      Map<Id, List<Payment__c>> paymentsByOpportunityId = new Map<Id, List<Payment__c>>();

      for (Payment__c payment : payments) {
        if (paymentsByOpportunityId.containsKey(payment.Opportunity__c)) {
          paymentsByOpportunityId.get(payment.Opportunity__c).add(payment);
        } else {
          paymentsByOpportunityId.put(
            payment.Opportunity__c,
            new List<Payment__c>{ payment }
          );
        }
      }

      return paymentsByOpportunityId;
    }
  }
}