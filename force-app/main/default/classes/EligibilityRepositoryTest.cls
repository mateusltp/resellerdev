/**
 * @description       : 
 * @author            : AlyssonMota
 * @group             : 
 * @last modified on  : 02-27-2021
 * @last modified by  : Alysson Mota
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   02-26-2021   Alysson Mota   Initial Version
**/
@isTest
public inherited sharing class EligibilityRepositoryTest {
    
    @TestSetup
    public static void setup() {
        Account acc = getAccount();
        insert acc;
        
        Pricebook2 standardPricebook = new Pricebook2(
            Id = Test.getStandardPricebookId(),
            IsActive = true
        );
        update standardPricebook;
        Pricebook2 pb = standardPricebook;
        
        Product2 accessFeeProduct = getProduct('Enterprise Subscription');
        insert accessFeeProduct;
        
        Opportunity opp = new Opportunity(CurrencyIsoCode='BRL');

        PricebookEntry accessFeeEntry = getPricebookEntry(pb, accessFeeProduct, opp);
        insert accessFeeEntry;
        
        opp = getOpp(acc, pb);
        insert opp;
        
        Quote qt = getQuote(opp);
        insert qt;
        
        //access fee
        QuoteLineItem accessFee = getAccessFee(qt, accessFeeEntry);
        insert accessFee;
        
        Payment__c payForAccessFee = getPaymentForFee(accessFee, acc);
        insert payForAccessFee;
        
        Eligibility__c eli = getEligibilityForPayment(payForAccessFee);
        insert eli;        
    }

    @isTest
    public static void testGetEligibilitiesByPayments() {
        EligibilityRepository repo = new EligibilityRepository();

        Payment__c payment = [SELECT Id FROM Payment__c LIMIT 1];
        
        Eligibility__c eli = getEligibilityForPayment(payment);
        insert eli;

        Test.startTest();
        repo.getEligibilitiesByPayments(new List<Payment__c>{payment});
        Test.stopTest();
    }

    @isTest
    public static void testGetEligibilitiesByPaymentId() {
        EligibilityRepository repo = new EligibilityRepository();

        Payment__c payment = [SELECT Id FROM Payment__c LIMIT 1];
        
        Test.startTest();
        repo.getEligibilitiesByPaymentId(new List<Id>{payment.Id});
        Test.stopTest();
    }

    @isTest
    public static void testGetEligibilitiesForPayments() {
        EligibilityRepository repo = new EligibilityRepository();

        Payment__c payment = [SELECT Id FROM Payment__c LIMIT 1];

        Test.startTest();
        repo.getEligibilitiesForPayments(new List<Payment__c>{payment});
        Test.stopTest();
    }


    @isTest
    public static void testGetEligibilitiesForOpportunity() {
        EligibilityRepository repo = new EligibilityRepository();

        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];

        Test.startTest();
        repo.getEligibilitiesForOpportunity(opp.Id);
        Test.stopTest();
    }


    private static Account getAccount(){
        Account acc = new Account();
        acc.Name = 'Test Name';
        acc.Razao_Social__c = 'Test Legal Name';
        acc.BillingCity = 'Test Billig City';
        acc.BillingCountry = 'Brazil';
        acc.BillingCountryCode = 'BR';
        acc.BillingPostalCode = '00000-000';
        acc.BillingStreet = 'Test Street 999';
        acc.Id_Company__c = '26.556.823/0001-91';
        
        return acc;
    }
    
    private static Product2 getProduct(String ProductName){
        Product2 product = new Product2();
        product.Name = ProductName;
        product.Minimum_Number_of_Employees__c = 0;
        product.Maximum_Number_of_Employees__c = 800;
        product.Family = ProductName;
        product.IsActive = true;
        return product;
    }
    
    private static PricebookEntry getPricebookEntry(Pricebook2 pb, Product2 prd, Opportunity opp){        
        PricebookEntry pbEntry = new PricebookEntry();
        pbEntry.Product2Id = prd.Id;
        pbEntry.Pricebook2Id = Test.getStandardPricebookId();
        pbEntry.UnitPrice = 200;
        pbEntry.IsActive = true;
        pbEntry.CurrencyIsoCode = opp.CurrencyIsoCode;
        return pbEntry;
    }
    
    private static Opportunity getOpp(Account acc, Pricebook2 pb){
        Opportunity opp = new Opportunity();
        opp.AccountId = acc.Id;
        opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        opp.Name = 'Opp Test Name Renew';
        opp.CloseDate = system.today().addDays(30);
        opp.FastTrackStage__c = 'Qualification';
        opp.StageName = 'Qualification';
        opp.Pricebook2Id = pb.Id;
        opp.Type = 'Expansion';  
        opp.Country_Manager_Approval__c = true;
        opp.Payment_approved__c = true;   
        opp.CurrencyIsoCode = 'BRL';
        opp.Gympass_Plus__c = 'Yes';
        opp.Standard_Payment__c = 'Yes';
        opp.Request_for_self_checkin__c = 'Yes';
        return opp;
    }
    
    private static Quote getQuote(Opportunity opp){
        Quote qt = new Quote();
        qt.Name = 'Test';
        qt.OpportunityId = opp.Id;
        qt.License_Fee_Waiver__c = 'Yes';
        qt.Waiver_Termination_Date__c = system.today().addDays(10);
        return qt;
    }
    
    private static QuoteLineItem getAccessFee(Quote qt, PricebookEntry pbEntry){
        QuoteLineItem accessFee = new QuoteLineItem();
        accessFee.QuoteId = qt.Id;
        accessFee.PricebookEntryId = pbEntry.Id;
        accessFee.Description = '';
        accessFee.Quantity = 100;
        accessFee.UnitPrice = pbEntry.UnitPrice;  
        
        return accessFee;
    }
    
    private static Payment__c getPaymentForFee(QuoteLineItem qli, Account acc){
        Payment__c afp = new Payment__c();
        afp.RecordTypeId = Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Payment').getRecordTypeId();
        afp.Quote_Line_Item__c = qli.Id;
        afp.Percentage__c = 100;
        afp.Payment_Method__c = 'Wire Transfer';
        afp.PO_Required__c = 'Yes';
        afp.Frequency__c = 'Monthly';
        afp.Cutoff_Day__c = 1;   
        afp.Account__c =  acc.Id;
        return afp;
    }
    
    private static Eligibility__c getEligibilityForPayment(Payment__c payment){
        Eligibility__c eli = new Eligibility__c();
        eli.Name = 'Headquarters';
        eli.Communication_Restriction__c = '3. Accept communications with employees after sign up';
        eli.Is_Default__c = true;
        eli.Group_Name__c = 'Main';
        eli.Launch_Date__c = system.today().addDays(10);
        eli.Payment__c = payment.Id;
        return eli;
    }

}