/**
 * @description       : 
 * @author            : roei@gft.com
 * @group             : 
 * @last modified on  : 01-22-2021
 * @last modified by  : roei@gft.com
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   01-22-2021   roei@gft.com   Initial Version
**/
public without sharing class LeadAssignToMeController {
    @AuraEnabled
    public static void assingToMe( Id aLeadId ) {
        Lead lLeadToUpdt = new Lead( 
            Id = aLeadId , 
            OwnerId = UserInfo.getUserId() );

        try{
            Database.update( lLeadToUpdt );
        } catch( Exception aException ){ throw new AuraHandledException( aException.getMessage() ); }
    }
}