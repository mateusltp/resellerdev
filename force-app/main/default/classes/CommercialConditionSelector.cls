/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 03-02-2022
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/
public with sharing class CommercialConditionSelector  extends ApplicationSelector {

    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
            Commercial_Condition__c.Id,
            Commercial_Condition__c.Name
        };
    }

    public Schema.SObjectType getSObjectType() {
        return Commercial_Condition__c.sObjectType;
    }

    public List<Commercial_Condition__c> selectById(Set<Id> ids) {
        return (List<Commercial_Condition__c>) super.selectSObjectsById(ids);
    }

    public List<Commercial_Condition__c> selectCommConditionByID(Set<Id> ids) {
        return new List<Commercial_Condition__c>( (List<Commercial_Condition__c>)Database.query(
                                        newQueryFactory().
                                        selectField(Commercial_Condition__c.Id).
                                        selectField(Commercial_Condition__c.Amount__c).
                                        selectField(Commercial_Condition__c.Exclusivity__c).
                                        selectField(Commercial_Condition__c.Fee_Percentage__c).
                                        selectField(Commercial_Condition__c.Frequency__c).
                                        selectField(Commercial_Condition__c.CurrencyIsoCode).
                                        selectField(Commercial_Condition__c.RecordTypeId).
                                        selectField(Commercial_Condition__c.CAP_Discount__c).
                                        selectField(Commercial_Condition__c.Visits_to_CAP__c).
                                        setCondition('Id in: ids').
                                        toSOQL()
                                        ));									
    }

    public Map<Id, Commercial_Condition__c> selectCommConditionByIdAsMap(Set<Id> ids) {
        Map<Id, Commercial_Condition__c> idToCommCondition = new Map<Id, Commercial_Condition__c>();

        List<Commercial_Condition__c> commConditionLst = selectCommConditionByID(ids);

        for (Commercial_Condition__c  commCondition : commConditionLst) {
            idToCommCondition.put(commCondition.Id, commCondition);
        }

        return idToCommCondition;						
    }

    public List<Commercial_Condition__c> selectByIdToClone(Set<Id> ids) {
        return new List<Commercial_Condition__c>( (List<Commercial_Condition__c>)Database.query(
                                        newQueryFactory().
                                        selectFields(getAllFieldsFromCommercialConditions()).
                                        setCondition('Id in: ids').
                                        toSOQL()
                                        ));									
    }

    private Set<String> getAllFieldsFromCommercialConditions(){
		Map<String, Schema.SObjectField> commercialConditionFields = Schema.getGlobalDescribe().get('Commercial_Condition__c').getDescribe().fields.getMap();
        return commercialConditionFields.keySet();
    }
                                        
}