/**
 * @description       : 
 * @author            : Samuel Silva - GFT (slml@gft.com)
 * @group             : 
 * @last modified on  : 06-21-2021
 * @last modified by  : Alysson Mota
 * Modifications Log 
 * Ver   Date         Author                              Modification
 * 1.0   11-19-2020   Samuel Silva - GFT (slml@gft.com)   Initial Version
**/
@isTest(seeAllData=false)
public with sharing class EngagementJourneyFormHelperTest {

    @TestSetup
    static void createData(){        
    
        Pricebook2 pb = generatePricebook();  
        insert pb; 
        Account acc = generateClientsAccount();
        insert acc;
        Opportunity opp = generateClientNewBusinessOpp(acc.Id,pb);
        insert opp;
        Quote quote = generateQuote(opp,'1');
        insert quote;                
        Product2 p = generateAccessFeeProduct();
        opp.SyncedQuoteId = quote.Id;
        update opp;
        insert p;
        PricebookEntry accessFee = generateAccessFeePricebookEntry(pb, p, opp);
        insert accessFee;
        insert generateQuoteLineItem(quote,accessFee);        
        Form__c m0 = generateM0(opp.Id);
        insert m0;      
    }

//need one M0 in data setup to test other methods, but need to run CreateM0FormHelper in order to get it covered;
    @isTest
    public static void testCreateM0() {
        Opportunity myOpp = [SELECT Id from Opportunity WHERE Name Like '%Opp Test%' Limit 1];     
        Test.startTest();           
            List<Form__c> m0 = [SELECT Id, Name FROM Form__c WHERE Opportunity__c =: myOpp.id AND RecordType.DeveloperName = 'M0_12_Steps' ];
            delete m0;          
            CreateM0FormHelper.createM0ForOppsWithIds(new List<Id>{myOpp.id});
            m0 = [SELECT Id, Name FROM Form__c WHERE Opportunity__c =: myOpp.id AND RecordType.DeveloperName = 'M0_12_Steps' ];
            //System.assertEquals(1, m0.size());
        Test.stopTest();
    }


    @isTest
    public static void testCreateM1() {
        Opportunity myOpp = [SELECT Id from Opportunity WHERE Name Like '%Opp Test%' Limit 1];        
       // CreateM0FormHelper.createM0ForOppsWithIds(new List<Id>{myOpp.id});     
        Test.startTest();
            EngagementJourneyFormHelper.createM1ForOppsWithIds(new List<Id>{myOpp.id});            
            List<Form__c> m1 = [SELECT Id, Name FROM Form__c WHERE Opportunity__c =: myOpp.id AND RecordType.DeveloperName = 'M1_12_Steps' ];
            System.assertEquals(1, m1.size());
        Test.stopTest();
    }

    @isTest
    public static void testSubmitGreenLightForApprovalEnablers1() {
        Form__c mForm = [SELECT Id, Name, Standard_Membership_Fee_GL__c,
                                Approval_Status__c FROM Form__c]; 
        mForm.Approval_Status__c = 'Needs Approval';
        mForm.Country_Manager__c = UserInfo.getUserId();
        update mForm;
        Test.startTest();   
            try{
                SubmitGreenLightForApprovalController.submitFormForApproval(mForm.Id, 'Test');
            }   catch(Exception e){                
                System.Assert(e.getMessage().contains('Please enter (1)'));
            }            
        Test.stopTest();
    }

    @isTest
    public static void testSubmitGreenLightForApprovalEnablers2() {
        Form__c mForm = [SELECT Id, Name, Standard_Membership_Fee_GL__c,
                                Expansion_100_of_the_eligible_GL__c,
                                Approval_Status__c FROM Form__c]; 
        mForm.Country_Manager__c = UserInfo.getUserId();                        
        mForm.Approval_Status__c = 'Needs Approval';
        mForm.Standard_Membership_Fee_GL__c = 'Yes';
        update mForm;
        Test.startTest();   
            try{
                SubmitGreenLightForApprovalController.submitFormForApproval(mForm.Id, 'Test');
            }   catch(Exception e){            
                System.Assert(e.getMessage().contains('Please enter (2)'));
            }            
        Test.stopTest();
    }
    @isTest
    public static void testSubmitGreenLightForApprovalEnablers3() {
        Form__c mForm = [SELECT Id, Name, Standard_Membership_Fee_GL__c,
                                Expansion_100_of_the_eligible_GL__c,
                                Minimum_Access_Fee_GL__c,Contract_Type__c,
                                Approval_Status__c FROM Form__c]; 
        mForm.Approval_Status__c = 'Needs Approval';
        mForm.Country_Manager__c = UserInfo.getUserId(); 
        mForm.Standard_Membership_Fee_GL__c = 'Yes';
        mForm.Expansion_100_of_the_eligible_GL__c = 'Yes';
        update mForm;
        Test.startTest();   
            try{
                SubmitGreenLightForApprovalController.submitFormForApproval(mForm.Id, 'Test');
            }   catch(Exception e){
                System.Assert(e.getMessage().contains('Please enter (3)'));
            }            
        Test.stopTest();
    }
    @isTest
    public static void testSubmitGreenLightForApprovalEnablers4() {
        Form__c mForm = [SELECT Id, Name, Standard_Membership_Fee_GL__c,
                                Expansion_100_of_the_eligible_GL__c,
                                Minimum_Access_Fee_GL__c,Engagement_with_C_Level_GL__c,  
                                Contract_Type__c,                              
                                Approval_Status__c FROM Form__c]; 
        mForm.Approval_Status__c = 'Needs Approval';
        mForm.Country_Manager__c = UserInfo.getUserId(); 
        mForm.Standard_Membership_Fee_GL__c = 'Yes';
        mForm.Expansion_100_of_the_eligible_GL__c = 'Yes';
        mForm.Minimum_Access_Fee_GL__c = 'Yes';
               
        update mForm;
        Test.startTest();   
            try{
                SubmitGreenLightForApprovalController.submitFormForApproval(mForm.Id, 'Test');
            }   catch(Exception e){
                System.Assert(e.getMessage().contains('Please enter (4)'));
            }            
        Test.stopTest();
    }

    @isTest
    public static void testSubmitGreenLightForApprovalEnablers5() {
        Form__c mForm = [SELECT Id, Name, Standard_Membership_Fee_GL__c,
                                Expansion_100_of_the_eligible_GL__c,
                                Minimum_Access_Fee_GL__c,Engagement_with_C_Level_GL__c,  
                                Payroll_GL__c,Contract_Type__c,                              
                                Approval_Status__c FROM Form__c]; 
        mForm.Approval_Status__c = 'Needs Approval';
        mForm.Country_Manager__c = UserInfo.getUserId(); 
        mForm.Standard_Membership_Fee_GL__c = 'Yes';
        mForm.Expansion_100_of_the_eligible_GL__c = 'Yes';
        mForm.Minimum_Access_Fee_GL__c = 'Yes';
        mForm.Engagement_with_C_Level_GL__c = 'Yes';
           
        update mForm;
        Test.startTest();   
            try{
                SubmitGreenLightForApprovalController.submitFormForApproval(mForm.Id, 'Test');
            }   catch(Exception e){
                System.Assert(e.getMessage().contains('Please enter (5)'));
            }            
        Test.stopTest();
    }

    @isTest
    public static void testSubmitGreenLightForApprovalEnablers6() {
        Form__c mForm = [SELECT Id, Name, Standard_Membership_Fee_GL__c,
                                Expansion_100_of_the_eligible_GL__c,
                                Minimum_Access_Fee_GL__c,Engagement_with_C_Level_GL__c,  
                                Payroll_GL__c,Contract_Type__c,Database_Email_GL__c,                              
                                Approval_Status__c FROM Form__c]; 
        mForm.Approval_Status__c = 'Needs Approval';
        mForm.Country_Manager__c = UserInfo.getUserId(); 
        mForm.Standard_Membership_Fee_GL__c = 'Yes';
        mForm.Expansion_100_of_the_eligible_GL__c = 'Yes';
        mForm.Minimum_Access_Fee_GL__c = 'Yes';
        mForm.Engagement_with_C_Level_GL__c = 'Yes';
         
        mForm.Payroll_GL__c = 'Yes';  
        update mForm;
        Test.startTest();   
            try{
                SubmitGreenLightForApprovalController.submitFormForApproval(mForm.Id, 'Test');
            }   catch(Exception e){
                System.Assert(e.getMessage().contains('Please enter (6)'));
            }            
        Test.stopTest();
    }

    @isTest
    public static void testSubmitGreenLightForApprovalEnablers7() {
        Form__c mForm = [SELECT Id, Name, Standard_Membership_Fee_GL__c,
                                Expansion_100_of_the_eligible_GL__c,
                                Minimum_Access_Fee_GL__c,Engagement_with_C_Level_GL__c,  
                                Payroll_GL__c,Contract_Type__c,Whitelist_GL__c,                              
                                Approval_Status__c FROM Form__c]; 
        mForm.Approval_Status__c = 'Needs Approval';
        mForm.Country_Manager__c = UserInfo.getUserId(); 
        mForm.Standard_Membership_Fee_GL__c = 'Yes';
        mForm.Expansion_100_of_the_eligible_GL__c = 'Yes';
        mForm.Minimum_Access_Fee_GL__c = 'Yes';
        mForm.Engagement_with_C_Level_GL__c = 'Yes';
        
        mForm.Payroll_GL__c = 'Yes';   
        mForm.Database_Email_GL__c = 'Yes';
        update mForm;
        Test.startTest();   
            try{
                SubmitGreenLightForApprovalController.submitFormForApproval(mForm.Id, 'Test');
            }   catch(Exception e){
                System.Assert(e.getMessage().contains('Please enter (7)'));
            }            
        Test.stopTest();
    }

    @isTest
    public static void testSubmitGreenLightForApprovalEnablers8() {
        Form__c mForm = [SELECT Id, Name, Standard_Membership_Fee_GL__c,
                        Expansion_100_of_the_eligible_GL__c,
                        Minimum_Access_Fee_GL__c,Engagement_with_C_Level_GL__c,  
                        Payroll_GL__c,Contract_Type__c,Whitelist_GL__c,                              
                        Approval_Status__c FROM Form__c]; 
        mForm.Approval_Status__c = 'Needs Approval';
        mForm.Country_Manager__c = UserInfo.getUserId(); 
        mForm.Standard_Membership_Fee_GL__c = 'Yes';
        mForm.Expansion_100_of_the_eligible_GL__c = 'Yes';
        mForm.Minimum_Access_Fee_GL__c = 'Yes';
        mForm.Engagement_with_C_Level_GL__c = 'Yes';
        
        mForm.Payroll_GL__c = 'Yes';   
        mForm.Database_Email_GL__c = 'Yes';
        mForm.Whitelist_GL__c = 'Yes';
        update mForm;
        Test.startTest();   
            try{
                SubmitGreenLightForApprovalController.submitFormForApproval(mForm.Id, 'Test');
            }   catch(Exception e){
                System.Assert(e.getMessage().contains('Please enter (8)'));
            }            
        Test.stopTest();
    }

    @isTest
    public static void testSubmitGreenLightForApprovalEnablers9() {
        Form__c mForm = [SELECT Id, Name, Standard_Membership_Fee_GL__c,
                        Expansion_100_of_the_eligible_GL__c,
                        Minimum_Access_Fee_GL__c,Engagement_with_C_Level_GL__c,  
                        Payroll_GL__c,Contract_Type__c,Whitelist_GL__c, New_Hires_GL__c,                             
                        Approval_Status__c FROM Form__c]; 
            mForm.Approval_Status__c = 'Needs Approval';
            mForm.Country_Manager__c = UserInfo.getUserId(); 
            mForm.Standard_Membership_Fee_GL__c = 'Yes';
            mForm.Expansion_100_of_the_eligible_GL__c = 'Yes';
            mForm.Minimum_Access_Fee_GL__c = 'Yes';
            mForm.Engagement_with_C_Level_GL__c = 'Yes';
            
            mForm.Payroll_GL__c = 'Yes';   
            mForm.Database_Email_GL__c = 'Yes';
            mForm.Whitelist_GL__c = 'Yes';
            mForm.New_Hires_GL__c = 'Yes';
        update mForm;
        Test.startTest();   
            try{
                SubmitGreenLightForApprovalController.submitFormForApproval(mForm.Id, 'Test');
            }   catch(Exception e){
                System.Assert(e.getMessage().contains('Please enter (9)'));
            }            
        Test.stopTest();
    }

    @isTest
    public static void testSubmitGreenLightForApprovalEnablers10() {
        Form__c mForm = [SELECT Id, Name, Standard_Membership_Fee_GL__c,
                        Expansion_100_of_the_eligible_GL__c,
                        Minimum_Access_Fee_GL__c,Engagement_with_C_Level_GL__c,  
                        Payroll_GL__c,Contract_Type__c,Whitelist_GL__c, New_Hires_GL__c,                             
                        Approval_Status__c FROM Form__c]; 
            mForm.Approval_Status__c = 'Needs Approval';
            mForm.Country_Manager__c = UserInfo.getUserId(); 
            mForm.Standard_Membership_Fee_GL__c = 'Yes';
            mForm.Expansion_100_of_the_eligible_GL__c = 'Yes';
            mForm.Minimum_Access_Fee_GL__c = 'Yes';
            mForm.Engagement_with_C_Level_GL__c = 'Yes';
            
            mForm.Payroll_GL__c = 'Yes';   
            mForm.Database_Email_GL__c = 'Yes';
            mForm.Whitelist_GL__c = 'Yes';
            mForm.New_Hires_GL__c = 'Yes';
            mForm.HR_Communication_GL__c = 'Yes';
        update mForm;
        Test.startTest();   
            try{
                SubmitGreenLightForApprovalController.submitFormForApproval(mForm.Id, 'Test');
            }   catch(Exception e){
                System.Assert(e.getMessage().contains('Please enter (10)'));
            }            
        Test.stopTest();
    }

    @isTest
    public static void testSubmitGreenLightForApprovalEnablers11() {
        Form__c mForm = [SELECT Id, Name, Standard_Membership_Fee_GL__c,
                        Expansion_100_of_the_eligible_GL__c,
                        Minimum_Access_Fee_GL__c,Engagement_with_C_Level_GL__c,  
                        Payroll_GL__c,Contract_Type__c,Whitelist_GL__c, New_Hires_GL__c, 
                        Exclusivity_clause_GL__c,PR_in_contract_GL__c,                            
                        Approval_Status__c FROM Form__c]; 
            mForm.Approval_Status__c = 'Needs Approval';
            mForm.Country_Manager__c = UserInfo.getUserId(); 
            mForm.Standard_Membership_Fee_GL__c = 'Yes';
            mForm.Expansion_100_of_the_eligible_GL__c = 'Yes';
            mForm.Minimum_Access_Fee_GL__c = 'Yes';
            mForm.Engagement_with_C_Level_GL__c = 'Yes';
            
            mForm.Payroll_GL__c = 'Yes';   
            mForm.Database_Email_GL__c = 'Yes';
            mForm.Whitelist_GL__c = 'Yes';
            mForm.New_Hires_GL__c = 'Yes';
            mForm.HR_Communication_GL__c = 'Yes';
            mForm.Exclusivity_clause_GL__c = 'Yes';   
        update mForm;
        Test.startTest();   
            try{
                SubmitGreenLightForApprovalController.submitFormForApproval(mForm.Id, 'Test');
            }   catch(Exception e){
                System.Assert(e.getMessage().contains('Please enter (11)'));
            }            
        Test.stopTest();
    }

    @isTest
    public static void testSubmitGreenLightForApprovalEnablers12() {
        Form__c mForm = [SELECT Id, Name,Standard_Membership_Fee_GL__c,
                        Expansion_100_of_the_eligible_GL__c,Database_Email_GL__c,
                        Minimum_Access_Fee_GL__c,Engagement_with_C_Level_GL__c,  
                        Payroll_GL__c,Contract_Type__c,Whitelist_GL__c, New_Hires_GL__c ,
                        Exclusivity_clause_GL__c,PR_in_contract_GL__c,HR_Communication_GL__c,
                        What_does_success_looks_like_Joint_BP_GL__c,                         
                        Approval_Status__c FROM Form__c]; 
        mForm.Approval_Status__c = 'Needs Approval';
        mForm.Country_Manager__c = UserInfo.getUserId(); 
        mForm.Standard_Membership_Fee_GL__c = 'Yes';
        mForm.Expansion_100_of_the_eligible_GL__c = 'Yes';
        mForm.Minimum_Access_Fee_GL__c = 'Yes';
        mForm.Engagement_with_C_Level_GL__c = 'Yes';
        
        mForm.Payroll_GL__c = 'Yes';   
        mForm.Database_Email_GL__c = 'Yes';
        mForm.Whitelist_GL__c = 'Yes';
        mForm.New_Hires_GL__c = 'Yes';
        mForm.HR_Communication_GL__c = 'Yes';
        mForm.Exclusivity_clause_GL__c = 'Yes';     
        mForm.PR_in_contract_GL__c = 'Yes';
        update mForm;
        Test.startTest();   
            try{
                SubmitGreenLightForApprovalController.submitFormForApproval(mForm.Id, 'Test');
            }   catch(Exception e){
                System.Assert(e.getMessage().contains('Please enter (12)'));
            }            
        Test.stopTest();
    }

    @isTest
    public static void testApprovalSuccess() {
        Form__c mForm = [SELECT Id, Name, Standard_Membership_Fee_GL__c,
                        Expansion_100_of_the_eligible_GL__c,Database_Email_GL__c,
                        Minimum_Access_Fee_GL__c,Engagement_with_C_Level_GL__c,  
                        Payroll_GL__c,Contract_Type__c,Whitelist_GL__c, New_Hires_GL__c, 
                        Exclusivity_clause_GL__c,PR_in_contract_GL__c,HR_Communication_GL__c,
                        What_does_success_looks_like_Joint_BP_GL__c,                          
                        Approval_Status__c FROM Form__c]; 
        mForm.Approval_Status__c = 'Needs Approval';
        mForm.Country_Manager__c = UserInfo.getUserId(); 
        mForm.Standard_Membership_Fee_GL__c = 'Yes';
        mForm.Expansion_100_of_the_eligible_GL__c = 'Yes';
        mForm.Minimum_Access_Fee_GL__c = 'Yes';
        mForm.Engagement_with_C_Level_GL__c = 'Yes';
        
        mForm.Payroll_GL__c = 'Yes';   
        mForm.Database_Email_GL__c = 'Yes';
        mForm.Whitelist_GL__c = 'Yes';
        mForm.New_Hires_GL__c = 'Yes';
        mForm.HR_Communication_GL__c = 'Yes';
        mForm.Exclusivity_clause_GL__c = 'Yes';     
        mForm.PR_in_contract_GL__c = 'Yes';
        mForm.What_does_success_looks_like_Joint_BP_GL__c = 'Yes';
        update mForm;
        Test.startTest();   
            SubmitGreenLightForApprovalController.submitFormForApproval(mForm.Id, 'Test');         
        Test.stopTest();
    }

    private static Account generateClientsAccount(){
        ID accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
        Account acc = new Account();
        acc.RecordTypeId = accRecordTypeId;
        acc.Name = 'Test Account';
        acc.BillingCountry = 'Brazil';
        acc.BillingState = 'São Paulo';
        acc.NumberOfEmployees = 900;
        return acc;
    }

    
    private static Opportunity generateClientNewBusinessOpp(Id accId, Pricebook2 pb){
        Id oppRtId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        Opportunity opp = new Opportunity();
        opp.recordTypeId = oppRtId;
        opp.StageName='Qualification';
        opp.AccountId = accId;
        opp.CloseDate = Date.today();
        opp.Name='Opp Test'; 
        opp.CurrencyIsoCode='BRL';
        opp.Pricebook2Id = pb.Id;
        return opp;
    }

    private static Product2 generateAccessFeeProduct(){
        Product2 product = new Product2();
        product.Name = 'Enterprise Subscription';
        product.Minimum_Number_of_Employees__c = 0;
        product.Maximum_Number_of_Employees__c = 900;
        product.Family = 'Enterprise Subscription';
        product.IsActive = true;
        return product;
    }

    private static PricebookEntry generateAccessFeePricebookEntry(Pricebook2 pb, Product2 product, Opportunity opp){        
        PricebookEntry pbEntry = new PricebookEntry();
        pbEntry.Product2Id = product.Id;
        pbEntry.Pricebook2Id = Test.getStandardPricebookId();
        pbEntry.UnitPrice = 100;
        pbEntry.IsActive = true;
        pbEntry.CurrencyIsoCode = opp.CurrencyIsoCode;
        insert pbEntry;
       
        PricebookEntry custompbEntry = new PricebookEntry();
        custompbEntry.Product2Id = product.Id;
        custompbEntry.Pricebook2Id = pb.Id;
        custompbEntry.UnitPrice = 200;
        custompbEntry.IsActive = true;
        custompbEntry.CurrencyIsoCode = opp.CurrencyIsoCode;
        return custompbEntry;
    }

    
    private static Quote generateQuote(Opportunity opp, String name){
        Quote qt = new Quote();  
        qt.RecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Gympass_Plus').getRecordTypeId();
        qt.Name = opp.Name+name;
        qt.OpportunityId = opp.Id;   
        qt.License_Fee_Waiver__c = 'No';  
        qt.CurrencyIsoCode = opp.CurrencyIsoCode;
        return qt;
    }

    private static QuoteLineItem generateQuoteLineItem(Quote quote, PricebookEntry accessFee){
        QuoteLineItem qt = new QuoteLineItem();
        qt.QuoteId = quote.Id;   
        qt.PricebookEntryId = accessFee.Id;
        qt.Product2Id = accessFee.Product2Id;
        qt.Quantity = 10;
        qt.UnitPrice =100;
        qt.Fee_Contract_Type__c = 'Flat Fee';
        return qt;
    }

    private static Form__c generateM0(ID oppId){
        ID rtId = Schema.SObjectType.Form__c.getRecordTypeInfosByDeveloperName().get('M0_12_Steps').getRecordTypeId();
        Form__c m0 = new Form__c();
        m0.RecordTypeId = rtId;
        m0.Opportunity__c= oppId;
        m0.SMB_CS_success_executive__c = true;
        return m0;
    }

    private static Pricebook2 generatePricebook(){        
        Pricebook2 pb = new Pricebook2();
        pb.Name = 'Br';
        pb.Country__c = 'Brazil';
        pb.IsActive = true;
        return pb;
    }
}