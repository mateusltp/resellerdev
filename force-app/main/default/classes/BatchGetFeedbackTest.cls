/**
* @description       : 
* @author            : JRDL@GFT.com
* @group             : 
* @last modified on  : 
* @last modified by  : 
* Modifications Log 
* Ver   Date         Author                              Modification
* 1.0   10-06-2021   jrdl@gft.com   Initial Version
**/
@isTest
public class BatchGetFeedbackTest {
    
    @isTest
    public static void testBatchGFPreRenewal() {
        Id rtId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
        Account acc = new Account();
        acc.name='Test ';
        acc.RecordTypeId = rtId;
        acc.GP_Status__c = 'Active';
        acc.Type = 'Prospect';
        acc.billingState = 'São Paulo';
        acc.BillingCity = 'São Paulo';
        acc.billingStreet = 'Rua Test';
        acc.billingCountry = 'Brazil';        
        acc.NumberOfEmployees = 520;
        insert acc;
        
        Id rtCtt = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Direct_Channel').getRecordTypeId();
        Contact accCtt = new Contact();
        accCtt.recordTypeId = rtCtt;
        accCtt.AccountId = acc.id;
        accCtt.Email = 'test@test.com';
        accCtt.FirstName = 'Contato';
        accCtt.LastName = 'Teste';
        accCtt.DecisionMaker__c = 'Yes';
        accCtt.GFSendSurvey__c = 'Client Sales PreRenewal';
        accCtt.Primary_HR_Contact__c = TRUE;
        insert accCtt;
        
        Id rtOpp = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        Opportunity opp = new Opportunity();
        opp.AccountId = acc.Id;
        opp.RecordTypeId = rtOpp;
        opp.Name = 'test';
        opp.StageName = 'Lançado/Ganho';
        opp.Probability = 100;
        opp.CurrencyIsoCode = 'BRL';
        opp.CloseDate = Date.today().addDays(90);
        insert opp;
        
        Id rtQ = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        Quote q = new Quote();
        q.Name = 'test';
        q.OpportunityId = opp.Id;
        q.RecordTypeId = rtQ;
        q.Submission_Date__c = Date.today().addDays(-90);
        q.End_Date__c = Date.today().addDays(90);
        q.License_Fee_Waiver__c = 'No';
        insert q;
        
        opp.SyncedQuoteId = q.Id;
        opp.StageName = 'Lançado/Ganho';
        upsert opp;
                
        Test.startTest();
        Database.executeBatch(new BatchGFPreRenewal());
        Test.stopTest();
    }
    
    @isTest
    public static void testBatchGFPostLaunch() {
        Id rtId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
        Account acc = new Account();
        acc.name='Test ';
        acc.RecordTypeId = rtId;
        acc.GP_Status__c = 'Active';
        acc.Type = 'Prospect';
        acc.billingState = 'São Paulo';
        acc.BillingCity = 'São Paulo';
        acc.billingStreet = 'Rua Test';
        acc.billingCountry = 'Brazil';        
        acc.NumberOfEmployees = 520;
        insert acc;
        
        Id rtCtt = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Direct_Channel').getRecordTypeId();
        Contact accCtt = new Contact();
        accCtt.recordTypeId = rtCtt;
        accCtt.AccountId = acc.id;
        accCtt.Email = 'test@test.com';
        accCtt.FirstName = 'Contato';
        accCtt.LastName = 'Teste';
        accCtt.DecisionMaker__c = 'Yes';
        accCtt.GFSendSurvey__c = 'Client Launch/Post Launch';
        accCtt.Primary_HR_Contact__c = TRUE;
        insert accCtt;
        
        Id rtOpp = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        Opportunity opp = new Opportunity();
        opp.AccountId = acc.Id;
        opp.RecordTypeId = rtOpp;
        opp.Name = 'test';
        opp.StageName = 'Lançado/Ganho';
        opp.Probability = 100;
        opp.CurrencyIsoCode = 'BRL';
        opp.CloseDate = Date.today().addDays(-90);
        opp.Data_do_Lancamento__c = Date.today().addDays(-90);
        insert opp;
        
        Id rtQ = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        Quote q = new Quote();
        q.Name = 'test';
        q.OpportunityId = opp.Id;
        q.RecordTypeId = rtQ;
        q.Submission_Date__c = Date.today().addDays(-90);
        q.End_Date__c = Date.today().addDays(90);
        q.License_Fee_Waiver__c = 'No';
        insert q;
        
        opp.StageName = 'Lançado/Ganho';
        update opp;
        
        Test.startTest();
        Database.executeBatch(new BatchGFPostLaunch());
        Test.stopTest();
        
    }
    
    @isTest
    public static void testBatchGFPartnerExperience() {
        Id rtId2 =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gym_Partner').getRecordTypeId();
        Account acc2 = new Account();
        acc2.name='Test ';
        acc2.RecordTypeId = rtId2;
        acc2.GP_Status__c = 'Active';
        acc2.Type = 'Prospect';
        acc2.Types_of_ownership__c = 'Private owner';
        acc2.billingState = 'São Paulo';
        acc2.BillingCity = 'São Paulo';
        acc2.billingStreet = 'Rua Test';
        acc2.billingCountry = 'Brazil';
        acc2.ShippingStreet = 'rua';
        acc2.ShippingCity = 'SP';
        acc2.ShippingCountry = 'Brazil';
        acc2.NumberOfEmployees = 520;
        insert acc2;
        
        Id rtCtt2 = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Gyms_Partner').getRecordTypeId();
        Contact accCtt2 = new Contact();
        accCtt2.recordTypeId = rtCtt2;
        accCtt2.AccountId = acc2.id;
        accCtt2.Email = 'test@test.com';
        accCtt2.FirstName = 'Contato';
        accCtt2.LastName = 'Teste';
        accCtt2.Primary_HR_Contact__c = TRUE;
        accCTT2.DecisionMaker__c = 'Yes';
        accCtt2.GFSendSurvey__c = 'Clients CSAT - Pre-Renewal';
        insert accCtt2;
        
        
        Id rtOpp2 = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        Opportunity opp2 = new Opportunity();
        opp2.AccountId = acc2.Id;
        opp2.RecordTypeId = rtOpp2;
        opp2.Name = 'test';
        opp2.StageName = 'Lançado/Ganho';
        opp2.Probability = 100;
        opp2.CurrencyIsoCode = 'BRL';
        opp2.CloseDate = Date.today().addDays(-90);
        opp2.Data_do_Lancamento__c = Date.today().addMonths(-1);
        insert opp2;
        
        Test.startTest();
        Database.executeBatch(new BatchGFPartnerExperience());
        Test.stopTest();
    }
    
    @isTest
    public static void testBatchGFProspectingSales() {
        List<Account> lstAcc = new List<Account>();
        List<Opportunity> lstOpp = new List<Opportunity>();
        Id rtId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
        Account acc = new Account();
        acc.name='Test ';
        acc.RecordTypeId = rtId;
        acc.GP_Status__c = 'Active';
        acc.Type = 'Prospect';
        acc.billingState = 'São Paulo';
        acc.BillingCity = 'São Paulo';
        acc.billingStreet = 'Rua Test';
        acc.billingCountry = 'Brazil';        
        acc.NumberOfEmployees = 520;
        lstAcc.add(acc);
        
        Account acc2 = new Account();
        acc2.name='gft';
        acc2.RecordTypeId = rtId;
        acc2.GP_Status__c = 'Active';
        acc2.Type = 'Prospect';
        acc2.billingState = 'São Paulo';
        acc2.BillingCity = 'São Paulo';
        acc2.billingStreet = 'Rua Test';
        acc2.billingCountry = 'Brazil';        
        acc2.NumberOfEmployees = 520;
        lstAcc.add(acc2);
        
        insert lstAcc;
        
        Id rtCtt = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Direct_Channel').getRecordTypeId();
        Contact accCtt = new Contact();
        accCtt.recordTypeId = rtCtt;
        accCtt.AccountId = acc.id;
        accCtt.Email = 'test@test.com';
        accCtt.FirstName = 'Contato';
        accCtt.LastName = 'Teste';
        accCtt.DecisionMaker__c = 'Yes';
        accCtt.GFSendSurvey__c = 'Client Sales Won';
        accCtt.Primary_HR_Contact__c = TRUE;
        insert accCtt;
        
        Contact accCtt2 = new Contact();
        accCtt2.recordTypeId = rtCtt;
        accCtt2.AccountId = acc2.id;
        accCtt2.Email = 'gft@test.com';
        accCtt2.FirstName = 'Contato';
        accCtt2.LastName = 'Teste';
        accCtt2.DecisionMaker__c = 'Yes';
        accCtt2.GFSendSurvey__c = 'Client Sales PostRenewal';
        accCtt2.Primary_HR_Contact__c = TRUE;
        insert accCtt2;
        
        Id rtOpp = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        Opportunity opp = new Opportunity();
        opp.AccountId = acc.Id;
        opp.RecordTypeId = rtOpp;
        opp.Name = 'test';
        opp.StageName = 'Lançado/Ganho';
        opp.Probability = 100;
        opp.CurrencyIsoCode = 'BRL';
        opp.CloseDate = Date.today().addDays(-1);
        opp.Data_do_Lancamento__c = Date.today().addDays(-1);
        lstOpp.add(opp);
        
        Id rtOppClientReneg = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Success_Renegotiation').getRecordTypeId();
        Opportunity opp2 = new Opportunity();
        opp2.AccountId = acc2.Id;
        opp2.RecordTypeId = rtOpp;
        opp2.Name = 'test';
        opp2.StageName = 'Perdido';
        opp2.Loss_Reason__c = 'Gym Network';
        opp2.Probability = 0;
        opp2.CurrencyIsoCode = 'BRL';
        opp2.CloseDate = Date.today().addDays(-1);
        opp2.Opportunity_Lost_Date__c = Date.today().addDays(-1);
        lstOpp.add(opp2);
        
        insert lstOpp;
        
        opp.StageName = 'Lançado/Ganho';
        update opp;
        
        Test.startTest();
        Database.executeBatch(new BatchGFProspectingSales());        
        Test.stopTest();
        
    }
    
    @isTest
    public static void testBatchGFServiceUtilization() {
        List<Account> lstAcc = new List<Account>();
        List<Opportunity> lstOpp = new List<Opportunity>();
        
        Id rtId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
        Account acc = new Account();
        acc.name='Test ';
        acc.RecordTypeId = rtId;
        acc.GP_Status__c = 'Active';
        acc.Type = 'Prospect';
        acc.billingState = 'São Paulo';
        acc.BillingCity = 'São Paulo';
        acc.billingStreet = 'Rua Test';
        acc.billingCountry = 'Brazil';        
        acc.NumberOfEmployees = 520;
        lstAcc.add(acc);
        insert lstAcc;
        
        Id rtCtt = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Direct_Channel').getRecordTypeId();
        Contact accCtt = new Contact();
        accCtt.recordTypeId = rtCtt;
        accCtt.AccountId = acc.id;
        accCtt.Email = 'test@test.com';
        accCtt.FirstName = 'Contato';
        accCtt.LastName = 'Teste';
        accCtt.DecisionMaker__c = 'Yes';
        accCtt.GFSendSurvey__c = 'Client Service Utilization';
        accCtt.Primary_HR_Contact__c = TRUE;
        insert accCtt;
        
        
        Id rtOpp = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        Opportunity opp = new Opportunity();
        opp.AccountId = acc.Id;
        opp.RecordTypeId = rtOpp;
        opp.Name = 'test';
        opp.StageName = 'Lançado/Ganho';
        opp.Probability = 100;
        opp.CurrencyIsoCode = 'BRL';
        opp.CloseDate = Date.today().addDays(-120);
        opp.Data_do_Lancamento__c = Date.today().addDays(-120);
        insert opp;
        
        Id rtQ = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        Quote q = new Quote();
        q.Name = 'test';
        q.OpportunityId = opp.Id;
        q.RecordTypeId = rtQ;
        q.Submission_Date__c = Date.today().addDays(-120);
        q.End_Date__c = Date.today().addDays(360);
        q.License_Fee_Waiver__c = 'No';
        insert q;
        
        Test.startTest();
        Database.executeBatch(new BatchGFServiceUtilization());
        Test.stopTest();
    }
    
    @isTest
    public static void testBatchGFServiceUtilization2() {
        List<Account> lstAcc = new List<Account>();
        List<Opportunity> lstOpp = new List<Opportunity>();
        Id rtId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
        
        Account acc2 = new Account();
        acc2.name='gft';
        acc2.RecordTypeId = rtId;
        acc2.GP_Status__c = 'Active';
        acc2.Type = 'Prospect';
        acc2.billingState = 'São Paulo';
        acc2.BillingCity = 'São Paulo';
        acc2.billingStreet = 'Rua Test';
        acc2.billingCountry = 'Brazil';
        acc2.EnvioServiceUtilization__c = Date.today().addDays(-30);        
        acc2.NumberOfEmployees = 520;
        lstAcc.add(acc2);
        insert lstAcc;
        
        Id rtCtt = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Direct_Channel').getRecordTypeId();
        Contact accCtt2 = new Contact();
        accCtt2.recordTypeId = rtCtt;
        accCtt2.AccountId = acc2.id;
        accCtt2.Email = 'gft@test.com';
        accCtt2.FirstName = 'Contato';
        accCtt2.LastName = 'Teste';
        accCtt2.DecisionMaker__c = 'Yes';
        accCtt2.GFSendSurvey__c = 'Client Service Utilization';
        accCtt2.Primary_HR_Contact__c = TRUE;
        insert accCtt2;
        
        Id rtOppClientReneg = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Success_Renegotiation').getRecordTypeId();
        Opportunity opp2 = new Opportunity();
        opp2.AccountId = acc2.Id;
        opp2.RecordTypeId = rtOppClientReneg;
        opp2.Name = 'test';
        opp2.StageName = 'Lançado/Ganho';
        opp2.Probability = 100;
        opp2.CurrencyIsoCode = 'BRL';
        opp2.Sub_Type__c = 'Renegotiation';
        opp2.CloseDate = Date.today().addDays(-150);
        opp2.Data_do_Lancamento__c = Date.today().addDays(-150);
        lstOpp.add(opp2);
        insert lstOpp;
        
        Id rtQ2 = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Direct_Channel').getRecordTypeId();
        Quote q2 = new Quote();
        q2.Name = 'test';
        q2.OpportunityId = opp2.Id;
        q2.RecordTypeId = rtQ2;
        q2.Status = 'Ganho';
        q2.Submission_Date__c = Date.today().addDays(-150);
        q2.End_Date__c = Date.today().addDays(150);
        q2.License_Fee_Waiver__c = 'No';
        insert q2;
        
        q2.Status = 'Ganho';
        update q2;
        opp2.SyncedQuoteId = q2.Id;
        opp2.StageName = 'Lançado/Ganho';
        update lstOpp;
        
        Test.startTest();
        Database.executeBatch(new BatchGFServiceUtilization());
        Test.stopTest();
    }
    
    @isTest
    public static void testContactDTO() {
        QualtricsContactDTO objContactDTO = new QualtricsContactDTO();
    }
    @isTest
    public static void testQualtrics() {
        List<Contact> lstUpdateConNew = new List<Contact>();
        Map<String, Account> mapAcc = new Map<String, Account>();
        
        Id rtId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
        Account acc2 = new Account();
        acc2.name='gft';
        acc2.RecordTypeId = rtId;
        acc2.GP_Status__c = 'Active';
        acc2.Type = 'Prospect';
        acc2.billingState = 'São Paulo';
        acc2.BillingCity = 'São Paulo';
        acc2.billingStreet = 'Rua Test';
        acc2.billingCountry = 'Brazil';
        acc2.EnvioServiceUtilization__c = Date.today().addDays(-30);        
        acc2.NumberOfEmployees = 520;
        insert acc2;
        mapAcc.put(acc2.Id, acc2);
        
        Id rtCtt = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Direct_Channel').getRecordTypeId();
        Contact accCtt2 = new Contact();
        accCtt2.recordTypeId = rtCtt;
        accCtt2.AccountId = acc2.id;
        accCtt2.Email = 'gft@test.com';
        accCtt2.FirstName = 'Contato';
        accCtt2.LastName = 'Teste';
        accCtt2.DecisionMaker__c = 'Yes';
        accCtt2.GFSendSurvey__c = 'Client Service Utilization';
        accCtt2.Primary_HR_Contact__c = TRUE;
        insert accCtt2;
        lstUpdateConNew.add(accCtt2);
        
        Qualtrics.createMailingList('Client Sales Won',lstUpdateConNew, mapAcc);
    } 
    
}