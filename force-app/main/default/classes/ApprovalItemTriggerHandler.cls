public class ApprovalItemTriggerHandler extends TriggerHandler {
    
    public override void afterUpdate() {
        new ApprovalItemTriggerHelper().checkAllItemsAreApprovedOrRejected(Trigger.new);
    }
}