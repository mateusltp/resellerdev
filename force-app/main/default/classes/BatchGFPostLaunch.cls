/**
* @File Name          : BatchGFPostLaunch.cls
* @Description        :
* @Author             : JRDL@GFT.com
* @Group              :
* @Last Modified By   : 
* @Last Modified On   : 
* @Modification Log   :
* Ver       Date            Author      		    Modification
* 1.0    04/06/2021   JRDL@GFT.com     Initial Version
**/
global class BatchGFPostLaunch implements Database.Batchable<sObject>, Database.AllowsCallouts {
    global Database.QueryLocator start(Database.BatchableContext bc) {
        Date ed = Date.today().addDays(-90);
        return Database.getQueryLocator([
            SELECT Id, Data_do_Lancamento__c, AccountId, Account.RecordTypeId, RecordTypeId, StageName,
            Account.Name,Account.Payment_method__c,Account.GP_NumberOfDependents__c,Account.Business_Unit__c,Account.Tagus_Based__c,Account.Size__c,
            Account.Owner.Name,Account.ID_Gympass_CRM__c,Account.NumberOfEmployees,Account.BillingCountry,Account.RecordType.DeveloperName,
            Account.Proposal_End_Date__c,Account.Contract_Signed_At__c,Account.Launch_date__c
            FROM Opportunity
            WHERE Data_do_Lancamento__c =: ed
        ]);
    }
    
    global void execute(Database.BatchableContext bc, List<Opportunity> scope) {
        
        List<Account> lstAccNew = new List<Account>();
        
        List<Contact> lstUpdateConNew = new List<Contact>();
        
        Map<Id,Contact> lMapContactEvent = new Map<Id,Contact>();
        Map<String,Account> mapAcc = new Map<String,Account>();
        
        Id rtOppClientNew = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        Id rtOppSMBNew = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SMB_New_Business').getRecordTypeId();
        
        Id rtAcc = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
        
        for(Opportunity o: scope){
            if((o.RecordTypeId == rtOppClientNew || o.RecordTypeId == rtOppSMBNew) && o.StageName == 'Lançado/Ganho' && o.Account.RecordTypeId == rtAcc){                
                   lstAccNew.add(o.Account);
                mapAcc.put(o.AccountId, o.Account);
               }
        }
        if(!lstAccNew.isEmpty()){
            for(Contact con: [Select Id, Status_do_contato__c, GFSendSurvey__c, Email, Primary_HR_Contact__c,DecisionMaker__c, Role__c, AccountId,MobilePhone,FirstName,LastName,Language_Code__c,HasOptedOutOfEmail FROM Contact WHERE AccountID IN :lstAccNew]){
                if(con.Status_do_contato__c == 'Ativo' && con.Email != null && con.Primary_HR_Contact__c == TRUE){
                    con.GFSendSurvey__c = 'Client Launch/Post Launch';
                    lstUpdateConNew.add(con); 
                }
                else if (con.Status_do_contato__c == 'Ativo' && con.Email != null && con.DecisionMaker__c == 'Yes'){
                    con.GFSendSurvey__c = 'Client Launch/Post Launch';
                    lMapContactEvent.put(con.Id, con);
                }
            }
        }
        
        if(!lMapContactEvent.isEmpty()){
            for(Event ev : [SELECT Id, WhoId, Who.Name,Realizado__c, RecordType.Name FROM Event Where Subject='Meeting' AND WhoId IN: lMapContactEvent.KeySet() AND Realizado__c = 'Yes' AND ActivityDate <= LAST_N_DAYS:90]){
                lstUpdateConNew.add(lMapContactEvent.get(ev.whoId));
            }
        }
        
        Qualtrics__mdt mdtQualtrics = Qualtrics__mdt.getInstance('Client_Launch_Post_Launch');
        if(!lstAccNew.isEmpty() && mdtQualtrics.Active__c == true){
            Qualtrics.createMailingList('Client Launch Post Launch',lstUpdateConNew, mapAcc);
        }
        
        if(!lstUpdateConNew.isEmpty()){
            update lstUpdateConNew;
        }
    }
    
    global void finish(Database.BatchableContext bc) {
        
    }
}