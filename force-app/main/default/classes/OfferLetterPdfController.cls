public without sharing class OfferLetterPdfController {
    public String companyName {get; set;}
    public String offerDate {get;set;}
    public String offerExpirationDate {get;set;}
    public String numberOfEligibles {get;set;}
    public String numberOfDependents {get;set;}

    public OfferLetterPdfController(Apexpages.StandardController controller) {
        System.debug('OfferLetterPdfController');
        String id = ApexPages.currentPage().getParameters().get('id');
        system.debug(id);

        if (id != null) {
            Opportunity opp = getOpportunityWithId(id);
            setOfferLetterParamsForOpportunity(opp);

            Quote syncedProposal = getSyncedProposalForOpp(opp);
            if (syncedProposal != null) {
                setOfferLetterParamsForProposal(syncedProposal);
            }
        }
    }

    private Opportunity getOpportunityWithId(String id) {
        Opportunity opp = [
            SELECT Id, Name, Account.Name, Account.NumberOfEmployees
            FROM Opportunity
            WHERE Id =: id
        ];

        return opp;
    }

    private void setOfferLetterParamsForOpportunity(Opportunity opp) {
        companyName = opp.Account.Name;
        numberOfEligibles = String.valueOf(opp.Account.NumberOfEmployees);
        numberOfDependents = '5';
        System.debug(numberOfDependents);
    }

    private Quote getSyncedProposalForOpp(Opportunity opp) {
        List<Quote> proposalList = [
            SELECT Id, Submission_Date__c, Proposal_End_Date__c
            FROM Quote
            WHERE OpportunityId =: opp.Id
            AND IsSyncing = TRUE
        ];
        
        Quote syncedProposal = (proposalList.size() > 0 ? proposalList.get(0) : null);

        return syncedProposal;
    }

    private void setOfferLetterParamsForProposal(Quote syncedProposal) {
        // TODO: Format the dates according to the account country,not to the user location
        
        if (syncedProposal.Submission_Date__c != null)
        	offerDate = syncedProposal.Submission_Date__c.format();
        
        if (syncedProposal.Proposal_End_Date__c != null) 
        	offerExpirationDate = syncedProposal.Proposal_End_Date__c.format();
    }
}