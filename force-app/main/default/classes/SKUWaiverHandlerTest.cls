/**
 * @description       : 
 * @author            : tiago.ribeiro@gympass.com
 * @group             : 
 * @last modified on  : 07-05-2022
 * @last modified by  : tiago.ribeiro@gympass.com
**/
@isTest
public with sharing class SKUWaiverHandlerTest {
    @TestSetup
    static void setupData(){
        Account acc = DataFactory.newAccount();   
        insert acc;
        
        Pricebook2 standardPricebook = new Pricebook2(
            Id = Test.getStandardPricebookId(),
            IsActive = true
        );
        update standardPricebook;

        Product2 lAcessFee = DataFactory.newProduct( 'Enterprise Subscription' , false , 'BRL' );
        lAcessFee.Payment_Type__c = 'Recurring fee';
        Database.insert( lAcessFee );

        Opportunity opp = DataFactory.newOpportunity( acc.Id, standardPricebook, 'Client_Sales_SKU_New_Business' );
        opp.Billing_Period__c = 'Monthly';
        opp.TotalOpportunityQuantity = 1000;
        
        PricebookEntry lAcessFeeESEntry = DataFactory.newPricebookEntry( standardPricebook , lAcessFee, opp);
        insert lAcessFeeESEntry;

        SKU_Price__c skuPrice = DataFactory.newSKUPrice(lAcessFee.Id, opp, acc, 1, 50);
        insert skuPrice;
        
        insert opp;

        List<QuoteLineItem> listQuoteLineItems = [SELECT Id FROM QuoteLineItem WHERE Quote.OpportunityId = :opp.Id];
        if(listQuoteLineItems.size() > 0){
            listQuoteLineItems[0].Waivers_Start_From__c = 'Offer sent month';
            update listQuoteLineItems[0];

            List<Waiver__c> listWaivers = DataFactory.createWaivers(listQuoteLineItems[0], 2);
            insert listWaivers;
        }
    }
    
    @isTest
    static void testWaiversUpdateOnLaunched(){
        Test.startTest();
        Quote quote = [SELECT Id FROM Quote LIMIT 1];
        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
        opp.SyncedQuoteId = quote.Id;
        opp.StageName = 'Lançado/Ganho';
        
        quote.Start_Date__c = Date.today().addDays(10);
        quote.End_Date__c = quote.Start_Date__c.addDays(10).addYears(1);
        update quote;
        update opp;
        Test.stopTest();

        List<Waiver__c> listWaivers = [SELECT Id, Duration__c, Start_Date__c, End_Date__c FROM Waiver__c WHERE Quote_Line_Item__r.QuoteId = :quote.Id ORDER BY Quote_Line_Item__c,Position__c];
        Date startDate = quote.Start_Date__c;
        for(Waiver__c waiver : listWaivers){
            System.assertEquals(waiver.Start_Date__c, startDate, 'Incorrect Waiver Start Date');
            System.assertEquals(waiver.End_Date__c, startDate.addMonths(Integer.valueOf(waiver.Duration__c)).addDays(-1), 'Incorrect Waiver End Date');
            startDate = waiver.End_Date__c.addDays(1);
        }
    }
}