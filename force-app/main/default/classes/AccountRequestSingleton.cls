public with sharing class AccountRequestSingleton {
    private static AccountRequestSingleton instance;
    private Account_Request__c accountRequest = new Account_Request__c();

    private AccountRequestSingleton(Id requestId){
        this.accountRequest = [ SELECT Id, Name, Website__c, Unique_Identifier_Type__c, Unique_Identifier__c, Engine_Status__c, Billing_Country__c, Business_Name__c, Reason_NoGo__c, IsNewAccount__c, None_of_Above__c, OpportunityId__c, AccountId__c, Existing_In_SalesForce__c, Contact_Name__c, Email__c, ContactId__c, Use_My_Own_Data__c, Department__c, Role__c, Total_Price_Request__c, Price_per_Eligible__c FROM Account_Request__c WHERE Id =: requestId LIMIT 1 ];
    }

    public static AccountRequestSingleton getInstance(Id requestId){
        if(instance == null)
            instance = new AccountRequestSingleton(requestId);
        return instance;
    }

    public void dataUpdate(){
        update accountRequest;
    }

    public String getName(){
        return this.accountRequest.Name;
    }

    public String getBusinessName(){
        return this.accountRequest.Business_Name__c;
    }

    public String getUniqueIdentifierType(){
        return this.accountRequest.Unique_Identifier_Type__c;
    }

    public String getUniqueIdentifier(){
        return this.accountRequest.Unique_Identifier__c;
    }

    public String getWebsite(){
        return this.accountRequest.Website__c;
    }

    public String getBillingCountry(){
        return this.accountRequest.Billing_Country__c;
    }

    public String getAccountId(){
        return this.accountRequest.AccountId__c;
    }

    public Boolean getExistingInSalesForce(){
        return this.accountRequest.Existing_In_SalesForce__c;
    }

    public Boolean getNoneOfAbove(){
        return this.accountRequest.None_of_Above__c;
    }

    public String getEngineStatus(){
        return this.accountRequest.Engine_Status__c;
    }

    public String getContactId(){
        return this.accountRequest.ContactId__c;
    }

    public Boolean getUseMyOwnData(){
        return this.accountRequest.Use_My_Own_Data__c;
    }

    public String getOpportunityId(){
        return this.accountRequest.OpportunityId__c;
    }

    public String getReasonNoGo(){
        return this.accountRequest.Reason_NoGo__c;
    }

    public String getContactName(){
        return this.accountRequest.Contact_Name__c;
    }

    public String getEmail(){
        return this.accountRequest.Email__c;
    }
    
    public String getDepartment(){
        return this.accountRequest.Department__c;
    }

    public String getRole(){
        return this.accountRequest.Role__c;
    }

    public void setIsNewAccount(Boolean isNewAccount){
        this.accountRequest.IsNewAccount__c = IsNewAccount;
    }

    public void setAccountId(String accountId){
        this.accountRequest.AccountId__c = accountId;
    }

    public void setTotalPriceRequest(Decimal totalPriceRequest){
        this.accountRequest.Total_Price_Request__c = totalPriceRequest;
    }

    public void setPricePerEligible(Decimal pricePerEligible){
        this.accountRequest.Price_per_Eligible__c = pricePerEligible;
    }

    public void setContactId(String contactId){
        this.accountRequest.ContactId__c = contactId;
    }
}