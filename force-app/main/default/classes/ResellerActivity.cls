public without sharing class ResellerActivity {

    private Boolean isSandBox = [ SELECT isSandBox FROM Organization LIMIT 1 ].isSandBox;

    /**
     * Constant Setting for the day intervals between engine events.
     */
    public static final DateTime DATE_NOW = DateTime.Now();
    public static final DateTime DATE_TODAY = DateTime.Now();
    //public static final DateTime DATE_TODAY_LESS_30 = DATE_NOW.addDays(-30);
    //public static final DateTime DATE_TODAY_LESS_60 = DATE_NOW.addDays(-60);
    //public static final DateTime DATE_TODAY_LESS_90 = DATE_NOW.addDays(-90);
   
        
    public static final DateTime DATE_TODAY_LESS_30 = DATE_NOW.addMinutes(-30);
    public static final DateTime DATE_TODAY_LESS_60 = DATE_NOW.addMinutes(-60);
    public static final DateTime DATE_TODAY_LESS_90 = DATE_NOW.addMinutes(-90);

    private static final List<String> OPPORTUNITY_FASTTRACK_STAGE = new List<String>{'Qualification', 'Validated', 'Offer Sent', 'Signed Contract', 'Offer Approved', 'Contract'};
    private static final List<String> OPPORTUNITY_STAGE_NAME = new List<String>{'Proposta Aprovada','Offer Approved', 'Contract', 'Contrato Assinado','Signed Contract', 'Proposta Enviada','Offer sent', 'Validated', 'Qualification', 'Qualificação', 'Oportunidade Validada Após 1ª Reunião'};
    /**
     * Control variables for execution.
     */
    private List<Account_Request__c> account_request_list = new List<Account_Request__c>();
    private Map<Id, Account_Request__c> accountId_AccountRequest_map = new Map<Id, Account_Request__c>();
    private Map<Id, Account> account_map = new Map<Id,Account>();
    private Set<Id> account_set = new Set<Id>();
    private Boolean isInsert = false;

    public ResellerActivity(){}

    /**
     * Constructor to execute based on account request set.
     */
    public ResellerActivity(Set<Id> account_request_set){
        this.account_request_list = [ SELECT Id, Last_Interaction_Account_Event__c, Last_Interaction_Opportunity_Event__c, Created_Date_Opportunity__c, AccountId__c, Engine_Log__c FROM Account_Request__c WHERE Id IN : account_request_set ];
        for(Account_Request__c ar : this.account_request_list){
            account_set.add(ar.AccountId__c);
            accountId_AccountRequest_map.put(ar.AccountId__c, ar);
        }
            
    }

    /**
     * Constructor to execute based on account request List.
     */
    public ResellerActivity(List<Account_Request__c> account_request_list){
        this.account_request_list = account_request_list;
        for(Account_Request__c ar : account_request_list){
            account_set.add(ar.AccountId__c);
            accountId_AccountRequest_map.put(ar.AccountId__c, ar);
        }
    }

    /**
     * Run Validation with Insert Option.
     */
    public void run(Boolean isInsert){
        this.isInsert = isInsert;
        this.lastAccountActivity();
    }

    /**
     * Run Validation without Insert Option.
     */
    public List<Account_Request__c> run(){
        this.lastAccountActivity();
        return this.accountId_AccountRequest_map.values();
    }

    private void lastAccountActivity(){
        this.account_map = new Map<Id, Account>([ SELECT Id,    ( SELECT Id, CreatedDate FROM Events ORDER BY CreatedDate DESC LIMIT 1 ), 
                                            ( SELECT Id, CreatedDate FROM Tasks ORDER BY CreatedDate DESC LIMIT 1 ) 
                              FROM Account 
                              WHERE Id IN : this.account_set ]);
        
        for( Account_Request__c request : accountId_AccountRequest_map.values() ){
            if(this.account_map.containsKey(request.AccountId__c)){
                
                Account acc = this.account_map.get(request.AccountId__c);
                DateTime last_Interaction;

                if(!acc.Events.isEmpty())
                    if(request.Last_Interaction_Account_Event__c == null){
                        request.Last_Interaction_Account_Event__c = this.getLastmodifiedDays(acc.Events[0].CreatedDate);
                        request.Engine_Log__c += ' -|- ' + ResellerEnumRepository.MassiveLog.ACCOUNT_EVENT_FOUND.name() + ' ' + String.valueOf(System.now());
                    }
                
                if(!acc.Tasks.isEmpty())
                    if(request.Last_Interaction_Account_Event__c == null || request.Last_Interaction_Account_Event__c > this.getLastmodifiedDays(acc.Tasks[0].CreatedDate)){
                        request.Last_Interaction_Account_Event__c = this.getLastmodifiedDays(acc.Tasks[0].CreatedDate);
                        request.Engine_Log__c += ' -|- ' + ResellerEnumRepository.MassiveLog.ACCOUNT_TASK_FOUND.name() + ' ' + String.valueOf(System.now());
                    }
                
                if(request.Last_Interaction_Account_Event__c == null)
                    request.Engine_Log__c += ' -|- ' + ResellerEnumRepository.MassiveLog.ACCOUNT_ACTIVITY_NOT_FOUND.name() + ' ' + String.valueOf(System.now());
               
            }
        }

        if(this.isInsert)update accountId_AccountRequest_map.values();
    }

    public void lastOpportunityActivity(Id accountRequestId){

        Account_Request__c accountrequest = [SELECT Id, AccountId__c, Created_Date_Opportunity__c, Last_Modified_Opportunity__c, Last_Interaction_Opportunity_Event__c, Queried_Opportunity__c FROM Account_Request__c WHERE Id =:accountRequestId LIMIT 1];
        
        List<Opportunity> opportunity_list = new List<Opportunity>();

        if(accountrequest.Queried_Opportunity__c != null)
            opportunity_list = [    SELECT Id, CreatedDate, LastModifiedDate,StageName,
                                                    ( SELECT Id, CreatedDate FROM Events ORDER BY CreatedDate DESC LIMIT 1 ),
                                                    ( SELECT Id, CreatedDate FROM Tasks ORDER BY CreatedDate DESC LIMIT 1 )
                                                    FROM Opportunity WHERE Id=: accountrequest.Queried_Opportunity__c ]; //AccountId =: accountrequest.AccountId__c AND StageName IN: OPPORTUNITY_STAGE_NAME AND isClosed = false ];
        
        for(Opportunity opp : opportunity_list){
            System.debug('OPP : ' + opp);

            if(accountrequest.Created_Date_Opportunity__c != null){
                if(accountrequest.Created_Date_Opportunity__c == 0 || accountrequest.Created_Date_Opportunity__c > this.getLastmodifiedDays(opp.CreatedDate))
                    accountrequest.Created_Date_Opportunity__c = this.getLastmodifiedDays(opp.CreatedDate);
            }else{
                accountrequest.Created_Date_Opportunity__c = this.getLastmodifiedDays(opp.CreatedDate);
            }

            if(accountrequest.Last_Modified_Opportunity__c != null){
                if(accountrequest.Last_Modified_Opportunity__c == 0 || accountrequest.Last_Modified_Opportunity__c > this.getLastmodifiedDays(opp.LastModifiedDate))
                    accountrequest.Last_Modified_Opportunity__c = this.getLastmodifiedDays(opp.LastModifiedDate);
            }else{
                accountrequest.Last_Modified_Opportunity__c = this.getLastmodifiedDays(opp.LastModifiedDate);
            }

            if(!opp.Events.isEmpty()){
                if(accountrequest.Last_Interaction_Opportunity_Event__c == null)
                    accountrequest.Last_Interaction_Opportunity_Event__c = this.getLastmodifiedDays(opp.Events[0].CreatedDate);
                else
                    if(accountrequest.Last_Interaction_Opportunity_Event__c > this.getLastmodifiedDays(opp.Events[0].CreatedDate)) 
                        accountrequest.Last_Interaction_Opportunity_Event__c = this.getLastmodifiedDays(opp.Events[0].CreatedDate);
            }
                
            
            if(!opp.Tasks.isEmpty())
                if(accountrequest.Last_Interaction_Opportunity_Event__c > this.getLastmodifiedDays(opp.Tasks[0].CreatedDate)) 
                    accountrequest.Last_Interaction_Opportunity_Event__c = this.getLastmodifiedDays(opp.Tasks[0].CreatedDate);
            
        }
        update accountrequest;
    }

    public Integer getLastmodifiedDays(DateTime genericDateTime){
        Date oppLastModifiedDate = Date.newInstance(genericDateTime.year(), genericDateTime.month(), genericDateTime.day());
        return oppLastModifiedDate.daysBetween(System.today());
    }

    /**
     * @Author: Rafael Carvalho
     * @description: defines what counts as the last engine modification within 30 days.
     */
    public static Boolean lastActivityLessEqual30(Account_Request__c account_Request){
        if(account_Request.Last_Interaction_Account_Event__c <= 30)return true;
        else if(account_Request.Last_Modified_Opportunity__c <= 30)return true;
        else if(account_Request.Last_Interaction_Opportunity_Event__c <= 30)return true;
        else if(account_Request.Last_Modified_Opportunity__c == null && account_Request.Last_Interaction_Account_Event__c == null && account_Request.Last_Interaction_Opportunity_Event__c == null)return false;
        else return false;
    }

    public static Boolean lastActivityLessEqual60(Account_Request__c account_Request){
        if(account_Request.Last_Interaction_Account_Event__c <=60 )return true;
        else if(account_Request.Last_Modified_Opportunity__c <= 60)return true;
        else if(account_Request.Last_Interaction_Opportunity_Event__c <= 60)return true;
        else if(account_Request.Last_Modified_Opportunity__c == null && account_Request.Last_Interaction_Account_Event__c == null && account_Request.Last_Interaction_Opportunity_Event__c == null)return false;
        else return false;
    }

    public static Boolean lastActivityLessEqual90(Account_Request__c account_Request){
        if(account_Request.Last_Interaction_Account_Event__c <= 90)return true;
        else if(account_Request.Last_Modified_Opportunity__c <= 90)return true;
        else if(account_Request.Last_Interaction_Opportunity_Event__c <= 90)return true;
        else if(account_Request.Last_Modified_Opportunity__c == null && account_Request.Last_Interaction_Account_Event__c == null && account_Request.Last_Interaction_Opportunity_Event__c == null)return false;
        else return false;
    }

    public static Boolean lastActivityMore30(Account_Request__c account_Request){
        if(account_Request.Last_Interaction_Account_Event__c > 30)return true;
        else if(account_Request.Last_Modified_Opportunity__c > 30)return true;
        else if(account_Request.Last_Interaction_Opportunity_Event__c > 30)return true;
        else if(account_Request.Last_Modified_Opportunity__c == null && account_Request.Last_Interaction_Account_Event__c == null && account_Request.Last_Interaction_Opportunity_Event__c == null)return false;
        else return false;
    }

    public static Boolean lastActivityMore60(Account_Request__c account_Request){
        if(account_Request.Last_Interaction_Account_Event__c >60 )return true;
        else if(account_Request.Last_Modified_Opportunity__c > 60)return true;
        else if(account_Request.Last_Interaction_Opportunity_Event__c > 60)return true;
        else if(account_Request.Last_Modified_Opportunity__c == null && account_Request.Last_Interaction_Account_Event__c == null && account_Request.Last_Interaction_Opportunity_Event__c == null)return false;
        else return false;
    }

    public static Boolean lastActivityMore90(Account_Request__c account_Request){
        if(account_Request.Last_Interaction_Account_Event__c > 90)return true;
        else if(account_Request.Last_Modified_Opportunity__c > 90)return true;
        else if(account_Request.Last_Interaction_Opportunity_Event__c > 90)return true;
        else if(account_Request.Last_Modified_Opportunity__c == null && account_Request.Last_Interaction_Account_Event__c == null && account_Request.Last_Interaction_Opportunity_Event__c == null)return false;
        else return false;
    }
    
}