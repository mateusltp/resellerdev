public with sharing class SmbSkuEditController {

    @AuraEnabled
    public static void updateProductAndWaivers(QuoteLineItem product, List<Waiver__c> waiversToUpsert, List<Waiver__c> waiversToDelete){
        try {

            update product;

            delete waiversToDelete;

            SmbSkuWaiverHelper.populateEndDateAndDuration(product.Id, waiversToUpsert);

            upsert waiversToUpsert;  
            
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

}