/**
* @author laerte.kimura
* @description Provide access to contract process in Fasttrack cmp
*/

public with sharing class FastTrackContract {
	public FastTrackContract() {}
    
    @AuraEnabled
    Public static boolean isContractSignedForOpportunity(id opportunityId){
        boolean bolSignedContractFound = false;
        string strContractSignedStatus = 'COMPLETE';
        
        integer count = database.countQuery('SELECT count() from APXT_Redlining__Contract_Agreement__c  where Opportunity__c = :opportunityId and conga_sign_status__c = :strContractSignedStatus');

        if (count > 0){
            bolSignedContractFound = true;
        }
        
        return bolSignedContractFound;

    }
}