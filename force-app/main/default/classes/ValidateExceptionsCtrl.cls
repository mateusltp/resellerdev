public class ValidateExceptionsCtrl {
    
    @AuraEnabled
    public static Boolean submitForApproval(Id recordId, String jsonListExceptions) {
        Boolean success = true;
        
        List<Exception__c> listExceptions = new List<Exception__c>();
        List<Exception__c> listExceptionsToSubmit = new List<Exception__c>();
        
        Type idArrType = Type.forName('List<Exception__c>');
        listExceptions = (List<Exception__c>) JSON.deserialize(jsonListExceptions, idArrType);
        
        if (listExceptions != null) {
            if (listExceptions.size() > 0) {
                for (Exception__c b2bMenuException : listExceptions) {
                    if (b2bMenuException.Status__c != 'Approved' && b2bMenuException.Status__c != 'Pending Approval')
                        listExceptionsToSubmit.add(b2bMenuException);
                }
                
                if (listExceptionsToSubmit.size() > 0) {
                    
                    Approval.ProcessSubmitRequest [] requestList = new Approval.ProcessSubmitRequest []{};
                        
                    for (Exception__c b2bMenuException : listExceptionsToSubmit) {
                        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
                        req1.setComments('Submitting request for approval.');
                        req1.setObjectId(b2bMenuException.Id);
                        requestList.add(req1);
                    }
                    
                    Approval.ProcessResult[] result = new Approval.ProcessResult[]{};
                    
                    try {
                        UPDATE listExceptionsToSubmit;
                        
                        if (requestList.size()>0)
                            result = Approval.process(requestList);
                    
                        for (Approval.ProcessResult processResult : result)
                            if (!processResult.isSuccess())
                                success = false;
                        
                        return success;
                    } catch (Exception ex) {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return success;
            }
        } else {
            return false;
        }
    }
    
    @AuraEnabled
    public static void setValidateExceptions(Id recordId) {
        Quote quote = new Quote();
        quote.Id = recordId;
        quote.Validate_Exceptions__c = TRUE;
        
        try {
            UPDATE quote;   
            
            quote.Validate_Exceptions__c = FALSE;
            
            UPDATE quote;
        } catch (Exception e) {
            System.debug('An unexpected exception has occurred. Ex: ' + e);
        }
    }
    
    @AuraEnabled
    public static void deleteOldExceptions(Id recordId) {
        List<Exception__c> listExceptions = [SELECT
                                            	Id
                                             FROM
                                            	Exception__c
                                             WHERE
                                            	Proposal__c =: recordId
                                             AND 
                                             	Status__c = 'Open'];
        
        try {
            if (listExceptions.size() > 0)
            	DELETE listExceptions;
        } catch (Exception e) {
            System.debug('An unexpected exception has occurred. Ex: ' + e);
        }
    }
    
    @AuraEnabled
    public static List<Exception__c> getExceptions(Id recordId) {
        List<Exception__c> listExceptions = new List<Exception__c>();
        
        listExceptions = [SELECT
                          Id, Name, Type__c, Subtype__c, toLabel(Status__c), CFO__c, Country_Manager__c, Global_Head_of_B2B_Operations__c, Global_Head_of_Strategy_Operations__c,
                          Global_Head_of_User_Growth__c, Local_Operation_Leader__c,  Rationale__c, Regional_Finance_Controller__c, Regional_Manager__c, What_the_Company_Wants__c, Can_be_modified__c
                          FROM
                          	Exception__c
                          WHERE
                          	Proposal__c =: recordId];
        
        return listExceptions;
    }

    @AuraEnabled
    public static boolean updateExceptions(List<Exception__c> editedExceptionList){
        try{
            UPDATE editedExceptionList;
            return true;
        } catch(Exception e){
            return false;
        }
    }
}