public without sharing class SKUResellerRevenueMetrics {
    
    @InvocableMethod
    public static void calculateResellerMetrics(List<String> aQuoteIds) {

        List< QuoteLineItem > lLstQuoteLineItems = getQuoteItems( aQuoteIds.get(0) );

        if(lLstQuoteLineItems.size() > 0) {    
            
            Map<String, List<QuoteLineItem>> gMapFeeTypeLstItem = setMapFeeTypeLstItem(lLstQuoteLineItems);
            Map<String, List<Waiver__c>> gMapItemIdLstWaiver = setMapItemIdLstWaiver(lLstQuoteLineItems);
            Decimal enterpriseMonthly = getEnterpriseSubscription( gMapFeeTypeLstItem.get('Recurring fee'));

            Decimal gEsRevenue = calculateEs( gMapFeeTypeLstItem.get('Recurring fee'));
            Decimal gAesRevenue = calculateAes( gMapFeeTypeLstItem.get('Recurring fee'), gMapItemIdLstWaiver );
            Decimal gErRevenue = calculateEr( enterpriseMonthly, gMapFeeTypeLstItem.get('One time fee' ));
            Decimal gAerRevenue = calculateAer(gAesRevenue, gMapFeeTypeLstItem.get('One time fee'), gMapItemIdLstWaiver );

            //-- recalculate potential
            Decimal potentialSku = calculatePotential( lLstQuoteLineItems );
            
            Quote quoteToUpdate = new Quote (
                Id = aQuoteIds.get(0),
                ES_Revenue__c = gEsRevenue,
                AES_Revenue__c = gAesRevenue,
                ER_Revenue__c = gErRevenue,
                AER_Revenue__c = gAerRevenue,
                Total_Sales_Discount__c = potentialSku - lLstQuoteLineItems.get(0).Quote.Subtotal
            );
            Database.update(quoteToUpdate);
            
            Opportunity oppToUpdate = new Opportunity (
                Id = lLstQuoteLineItems.get(0).Quote.OpportunityId,
                PotentialSKU__c = potentialSku
            );
            Database.update(oppToUpdate);
        }
    }

    /**
     *  Calculates the product potential
     *  IMPORTANT:  this calculation requires de Quantity field to be correctly updated when the opportunity field 
     *              Quantity_Offer_Number_of_Employees__c changes. 
     */
    public static decimal calculatePotential( List<QuoteLineItem> allQuoteLineItems ) {
        Decimal potential = 0;
        for(QuoteLineItem item : allQuoteLineItems) {
            potential += item?.List_Price__c == null ? 0 : item.List_Price__c * item.Quantity;
        }
        return potential;
    }

    //-- returns the Subtotal value for the Enterprise Subscription QuoteLineItem
    public static decimal getEnterpriseSubscription(List<QuoteLineItem> aLstRecurringItem) {
        
        Decimal enterpriseMonthly = 0;
        if(aLstRecurringItem == null) return enterpriseMonthly;
        for(QuoteLineItem item : aLstRecurringItem) {
            if(item.Product_Name__c == 'Enterprise Subscription') {
                enterpriseMonthly= item.Subtotal;
                break;
            }
        }
        return enterpriseMonthly;
    }

    //-- calculates ES Revenue - The amount of monthly recurring expenses
    public static decimal calculateEs( List<QuoteLineItem> aLstRecurringItem ) {
        Decimal lSumMonthlyRevenue = 0;
        if(aLstRecurringItem == null) return lSumMonthlyRevenue;

        for( QuoteLineItem item : aLstRecurringItem ){
            lSumMonthlyRevenue += item?.Subtotal == null ? 0 : item.Subtotal;
        }
        return lSumMonthlyRevenue;
    }

    //-- calculates the AES Revenue - The annual amount of recurring expenses,  minus waivers
    public static decimal calculateAes(List<QuoteLineItem> aLstRecurringItem, Map<String, List<Waiver__c>> gMapItemIdLstWaiver) {
        Decimal lSumAes = 0;
        if(aLstRecurringItem == null) return lSumAes;

        for(QuoteLineItem item : aLstRecurringItem) {
            List<Waiver__c> lLstWaiver = gMapItemIdLstWaiver.get(item.Id);
            Decimal sumWaiverItem = 0;
            Decimal waiverMonth = 0;
            Decimal itemAes = 0;

            if(lLstWaiver == null) lLstWaiver = new List< Waiver__c >(); 

            for(Waiver__c iWaiver : lLstWaiver) {
                waiverMonth = (iWaiver.Duration__c == null ? 0  : iWaiver.Duration__c);
                
                if (waiverMonth > 12) waiverMonth = 12;
        
                sumWaiverItem += item.Subtotal * iWaiver.Percentage__c/100 * waiverMonth;
            }

            itemAes = ((12 * item.Subtotal) - (sumWaiverItem));
            lSumAes += itemAES;
        }
        return lSumAes;
    }

    //-- calculates the ER Revenue - The amount of recurring monthly expenses plus one-time fees (12 installments)
    public static decimal calculateEr(Decimal enterpriseMonthly, List<QuoteLineItem> aLstOneTimeFee) {
        Decimal lSumOneTime = 0;
        if(aLstOneTimeFee == null) return enterpriseMonthly + lSumOneTime;
            
        for(QuoteLineItem iItem : aLstOneTimeFee) {
            lSumOneTime += iItem?.Subtotal == null ? 0 : iItem.Subtotal;
        }

        return enterpriseMonthly + lSumOneTime;
    }

    //-- calculates the AER Revenue - The annual amount of recurring expenses and one-time fees, minus waivers
    public static decimal calculateAer(Decimal AESValue, List<QuoteLineItem> aLstOneTimeFee, Map<String, List< Waiver__c>> gMapItemIdLstWaiver ){
        Decimal sumAerOneTime = 0;
        if(aLstOneTimeFee == null) { return AESValue; }
        
        for(QuoteLineItem item : aLstOneTimeFee) {
            List< Waiver__c > lLstWaiver = gMapItemIdLstWaiver.get(Item.Id);
            
            Decimal sumWaiverItem = 0;
            Decimal waiverMonth = 0;
            Decimal itemAes = 0;

            if(lLstWaiver == null) { lLstWaiver = new List<Waiver__c>(); }

            for(Waiver__c iWaiver : lLstWaiver) {
                waiverMonth = (iWaiver.Duration__c == null ? 0 : iWaiver.Duration__c);
                
                if (waiverMonth > 12) { waiverMonth = 12; }

                sumWaiverItem += item.Subtotal * iWaiver.Percentage__c / 100 * waiverMonth;
            }
			
            itemAes = (((Decimal) item.Subtotal) - (sumWaiverItem));
            sumAerOneTime  += itemAES;
        }
        return AESValue + sumAerOneTime;
    }

    //-- returns a list of QuoteLineItems and waivers related to the QuoteId
    private static List<QuoteLineItem> getQuoteItems(Id aQuoteId) {
        List<QuoteLineItem> lineItems = [SELECT Id, 
                                                TotalPrice, 
                                                List_Price__c,
                                                Subtotal,
                                                Quantity,
                                                Type__c, 
                                                Fee_Type__c, 
                                                Product_Name__c,
                                                Product2.Payment_Type__c,
                                                (SELECT Id, Duration__c,Percentage__c
                                                 FROM Waivers__r), 
                                                quote.OpportunityId,
                                     			quote.Subtotal
                                         FROM QuoteLineItem 
                                         WHERE quoteId = :aQuoteId];
        return lineItems;
    }

    //-- builds up a map of QuoteLineItems by fee type 'Recurring fee' / 'One time fee'
    private static Map<String, List<QuoteLineItem>> setMapFeeTypeLstItem(List<QuoteLineItem> lLstQuoteLineItems) {
        Map<String, List<QuoteLineItem>> gMapFeeTypeLstItem = new Map<String, List<QuoteLineItem>>();
        
        for(QuoteLineItem item : lLstQuoteLineItems) {
            if(item.Product2.Payment_Type__c == 'Recurring fee') {
                if(gMapFeeTypeLstItem.get('Recurring fee') == null ) {
                    gMapFeeTypeLstItem.put('Recurring fee', new List<QuoteLineItem>{item});
                } else {
                    List<QuoteLineItem> items = gMapFeeTypeLstItem.get('Recurring fee');
                    items.add(item);
                    gMapFeeTypeLstItem.put('Recurring fee', items);
                } 
            }
            else if(item.Product2.Payment_Type__c == 'One time fee'){
                if(gMapFeeTypeLstItem.get('One time fee') == null ){
                    gMapFeeTypeLstItem.put('One time fee', new List<QuoteLineItem>{item});
                } else {
                    List<QuoteLineItem> items = gMapFeeTypeLstItem.get('One time fee');
                    items.add(item);
                    gMapFeeTypeLstItem.put('One time fee', items);
                }
            }     
        }
        return gMapFeeTypeLstItem;
    }

    //-- builds up a map of waivers by QuoteLineItem Id
    private static Map<String, List<Waiver__c>> setMapItemIdLstWaiver(List<QuoteLineItem> lLstQuoteLineItems) {
        Map<String, List<Waiver__c>> gMapItemIdLstWaiver = new Map<String, List<Waiver__c>>();
        for(QuoteLineItem item : lLstQuoteLineItems) {
            gMapItemIdLstWaiver.put(item.Id, item.Waivers__r);
        }
        return gMapItemIdLstWaiver;
    }

}