/**
 * @description       : 
 * @author            : Jorge Stevaux - JZRX@gft.com
 * @group             : 
 * @last modified on  : 07-30-2021
 * @last modified by  : Jorge Stevaux - JZRX@gft.com
**/
public with sharing class EmailTemplateSelector extends ApplicationSelector {

    public List<Schema.SObjectField> getSObjectFieldList() {
		return new List<Schema.SObjectField> {
			EmailTemplate.Id
		};
	}

    public Schema.SObjectType getSObjectType() {
		return EmailTemplate.sObjectType;
	}

    public List<EmailTemplate> selectById(Set<Id> ids) {
		return (List<EmailTemplate>) super.selectSObjectsById(ids);
	}
    
    public EmailTemplate selectEmailTemplateByName(String aTemplateName) {
		
		return ((EmailTemplate)Database.query(newQueryFactory().
	 									setCondition('name =: aTemplateName').
	 									toSOQL()
     								));			
	}

	
}