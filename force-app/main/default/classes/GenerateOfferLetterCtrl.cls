public without sharing class GenerateOfferLetterCtrl {

    @AuraEnabled
    public static Response generateOfferLetter(String oppId){
        System.debug('generateOfferLetter');
        Response response = new Response();
        String exceptionMessage = '';
        String status = '';
        Boolean exceptionError = false;

        try {
            Opportunity opp = getOpportunity(oppId);

            PageReference page = new Pagereference('/apex/OfferLetterPdf');
            page.getParameters().put('Id', oppId);
            Blob pageData;
            
            if (!Test.isRunningTest()) {
                pageData = page.getContentAsPDF();
            } else {
                pageData = Blob.valueOf('Pdf content');
            }
            
            Attachment attach = new Attachment();
            attach.Body = pageData;
            attach.Name = 'OfferLetter' + opp.Account.Name + '.pdf';
            attach.ParentId = oppId;

            insert attach;   
        } catch(Exception e) {
            exceptionError = true;
            exceptionMessage = e.getMessage();
        }

        if (exceptionError) {
            response.responseMessage = exceptionMessage;
            response.status = 'ERROR';
        } else {
            response.responseMessage = 'The Offer Letter has been saved as an Opportunity\'s attachment';
            response.status = 'SUCCESS';
        }

        return response;
    }

    private static Opportunity getOpportunity(String oppId) {
        Opportunity opp = [
            SELECT Id, Account.Name
            FROM Opportunity
            WHERE Id =: oppId
        ];

        return opp;
    }

    public class Response {
        @auraEnabled public String status {get; set;}
        @auraEnabled public String responseMessage {get; set;}
    }
}