/**
 * @File Name          : OpportunityAccountManagerController.cls
 * @Description        : 
 * @Author             : Samuel Silva - GFT (slml@gft.com)
 * @Group              : 
 * @Last Modified By   : Samuel Silva - GFT (slml@gft.com)
 * @Last Modified On   : 08-25-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    25/03/2020   Samuel Silva - GFT (slml@gft.com)     Initial Version
**/
public with sharing class OpportunityAccountManagerController {
    

@AuraEnabled
    public static Object getRelatedAccounts(String oppId){
        System.debug('OPP ID getRelatedAccounts ' + oppId);
        String rtName = getOppRecordTypeName(oppId);
        System.debug('rtName ' + rtName);
        if(rtName.contains('Client')) {
            System.debug('Entrou no fluxo de clients');
            return new OpportunityAccountManagerForClients(oppId).getRelatedAccounts();
        }
            
        return new OpportunityAccountManagerModel(oppId).getRelatedAccounts();
    }
    

@AuraEnabled
    public static void setRelationshipAccountWithOpp(List<Account> accLst, String oppId){
        new OpportunityAccountManagerModel(oppId).setRelationshipAccountWithOpp(accLst);
    }

@AuraEnabled
    public static void breakRelationshipAccountWithOpp(List<Account> accLst, String oppId){
        new OpportunityAccountManagerModel(oppId).breakRelationshipAccountWithOpp(accLst);
    }
    
@AuraEnabled
    public static String getOppRecordTypeName(String oppId){        
       Opportunity oppRtName = [SELECT RecordType.Name FROM Opportunity WHERE ID =: oppId];
       return oppRtName.RecordType.Name;
    }
}