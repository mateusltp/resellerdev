/**
 * @description       : 
 * @author            : tiago.ribeiro@gympass.com
 * @group             : 
 * @last modified on  : 05-17-2022
 * @last modified by  : tiago.ribeiro@gympass.com
**/
public with sharing class SKUOfferCreation {
    public static final String RECURRING_PAYMENT_TYPE = 'Recurring fee';
    public static final String NON_RECURRING_PAYMENT_TYPE = 'One time fee';
    public static Map<Id, Set<Id>> mapOppPaymentsToCreateByRecordType = new Map<Id, Set<Id>>();
    public static List<Payment__c> listPaymentsToDelete = new List<Payment__c>();
    public static List<Payment__c> listPaymentsToCreate = new List<Payment__c>();

    @InvocableMethod(label='Create Payments' description='Generates Payments on SKU Offer Creation process')
    public static void createPayments(List<Opportunity> listOpps){
        if(listOpps == null || listOpps.size() == 0) return;

        Map<Id, Opportunity> mapIdOpp = mapOpportunity(listOpps);
        checkFeeTypesNeededForOpps(mapIdOpp.keySet());
        manageCurrentPaymentsForOpps(mapIdOpp.keySet());
        createPayments(mapIdOpp);
        handleDMLStatements();
    }

    private static Map<Id, Opportunity> mapOpportunity(List<Opportunity> listOpps){
        Map<Id, Opportunity> mapIdOpp = new Map<Id, Opportunity>();
        for(Opportunity opp : listOpps){
            mapIdOpp.put(opp.Id, opp);
        }

        return mapIdOpp;
    }

    private static void checkFeeTypesNeededForOpps(Set<Id> setOppIds){
        List<QuoteLineItem> listQuoteLineItems = new QuoteLineItemRepository().getQuoteItemsBySetOpps(setOppIds);
        for(QuoteLineItem quoteItem : listQuoteLineItems){
            Set<Id> setPaymentRecordTypeIds = storePaymentsNeededForOpp(quoteItem);
            mapOppPaymentsToCreateByRecordType.put(quoteItem.Quote.OpportunityId, setPaymentRecordTypeIds);
        }
    }

    private static Set<Id> storePaymentsNeededForOpp(QuoteLineItem quoteItem){
        Map<String, Id> mapPaymentTypeRecordTypeId = new Map<String,Id>{
            RECURRING_PAYMENT_TYPE => Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Recurring').getRecordTypeId(),
            NON_RECURRING_PAYMENT_TYPE => Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Non_Recurring').getRecordTypeId()
        };
        Set <Id> setPaymentRecordTypeIds = new Set<Id>();

        if(mapOppPaymentsToCreateByRecordType.containsKey(quoteItem.Quote.OpportunityId)){
            setPaymentRecordTypeIds = mapOppPaymentsToCreateByRecordType.get(quoteItem.Quote.OpportunityId);
        }
        if(mapPaymentTypeRecordTypeId.containsKey(quoteItem.Product_Fee_type__c)){
            setPaymentRecordTypeIds.add(mapPaymentTypeRecordTypeId.get(quoteItem.Product_Fee_type__c));
        }
        
        return setPaymentRecordTypeIds;
    }

    private static void manageCurrentPaymentsForOpps(Set<Id> setOppIds){
        List<Payment__c> listPayments = new PaymentRepository().getListPaymentsBySetOpps(setOppIds);
        for(Payment__c payment : listPayments){
            if(mapOppPaymentsToCreateByRecordType.containsKey(payment.Opportunity__c)){
                manageExistingPaymentsForOpp(payment);
            }
        }
    }

    private static void manageExistingPaymentsForOpp(Payment__c payment){
        Set<Id> setRecordTypeIds = mapOppPaymentsToCreateByRecordType.get(payment.Opportunity__c);
        //If Payment exists no need to create a new one
        //Else it means the Payment should no longer exist, so we need to delete it
        if(setRecordTypeIds.contains(payment.RecordTypeId)){
            removeExistingPaymentFromCreation(payment);
        }else{
            listPaymentsToDelete.add(payment);
        }
    }

    private static void removeExistingPaymentFromCreation(Payment__c payment){
        mapOppPaymentsToCreateByRecordType.get(payment.Opportunity__c).remove(payment.RecordTypeId);
    }

    private static void createPayments(Map<Id, Opportunity> mapIdOpp){
        for(Id oppId : mapOppPaymentsToCreateByRecordType.keySet()){
            for(Id paymentRecordType : mapOppPaymentsToCreateByRecordType.get(oppId)){
                if(mapIdOpp.containsKey(oppId)){
                    Opportunity opp = mapIdOpp.get(oppId);
                    Payment__c newPayment = new Payment__c(
                        Opportunity__c = oppId,
                        Account__c = opp.AccountId,
                        RecordTypeId = paymentRecordType,
                        Frequency__c = opp.Billing_Period__c
                    );

                    listPaymentsToCreate.add(newPayment);
                }                
            }
        }
    }

    private static void handleDMLStatements(){
        delete listPaymentsToDelete;
        insert listPaymentsToCreate;
    }
}