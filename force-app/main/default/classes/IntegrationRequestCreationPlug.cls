public class IntegrationRequestCreationPlug {

    @InvocableMethod
    public static void createIntegrationRequest (List<Id> objIdLIst) {
    	TagusIntegrationController.startIntegrationRequestCreation(objIdLIst, 'Process Builder');
    }
}