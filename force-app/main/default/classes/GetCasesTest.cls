@istest
public class GetCasesTest {
    
    @TestSetup
    static void setupCases(){
        
        Account lAcc = DataFactory.newAccount();
        Database.insert( lAcc );
        
        Pricebook2 lStandardPB = DataFactory.newPricebook();
        
        Opportunity lOpp = DataFactory.newOpportunity( lAcc.Id , lStandardPB , 'Client_Sales_New_Business');
        Database.insert( lOpp );
        
        Quote lQuote = DataFactory.newQuote(lOpp, ' Quote Test' , 'Client_Sales_New_Business');
        Database.insert( lQuote );
        
        APXT_Redlining__Contract_Agreement__c lCttAgreem =  DataFactory.newContractAgreement(lOpp.Id , lQuote.Id, lAcc.Id);        
        lCttAgreem.Template_Selection__c = 'Brazil';   
        lCttAgreem.RecordTypeId = Schema.SObjectType.APXT_Redlining__Contract_Agreement__c.getRecordTypeInfosByDeveloperName().get('B2B_Contract').getRecordTypeId();
        Database.insert( lCttAgreem );
        
        
        List< Profile > lLstRelationshipProfile = [
            SELECT Id
            FROM Profile
            WHERE Name = 'Global'
        ];
        
        User lUser1 = new User();
        lUser1.Email = 'user.test@gympass.com';
        lUser1.Username = 'user.test@gympass.com.test';
        lUser1.LastName = 'Test';
        lUser1.Alias = 'ustst';
        lUser1.TimeZoneSidKey = 'America/Sao_Paulo';
        lUser1.LocaleSidKey = 'pt_BR';
        lUser1.EmailEncodingKey = 'ISO-8859-1';
        lUser1.LanguageLocaleKey = 'pt_BR';
        lUser1.ProfileId = lLstRelationshipProfile[0].Id;
        lUser1.Region__c = 'EU;LATAM;US';
        
        Database.insert( lUser1 );
        
        PermissionSet lClientSuccessPermissionSet = [ SELECT Id FROM PermissionSet WHERE Name = 'Clients_Success_PermissionSet' ];
        PermissionSet lStandardAdminPermissionSet = [ SELECT Id FROM PermissionSet WHERE Name = 'Standard_Admin' ];
        
        List< PermissionSetAssignment > lLstPermissionSetAssign = new List< PermissionSetAssignment >();
        
        PermissionSetAssignment lPsAssignment1 = new PermissionSetAssignment();
        lPsAssignment1.AssigneeId = lUser1.Id;
        lPsAssignment1.PermissionSetId = lClientSuccessPermissionSet.Id;
        lLstPermissionSetAssign.add( lPsAssignment1 );
        
        PermissionSetAssignment lPsAssignment2 = new PermissionSetAssignment();
        lPsAssignment1.AssigneeId = lUser1.Id;
        lPsAssignment1.PermissionSetId = lStandardAdminPermissionSet.Id;
        lLstPermissionSetAssign.add( lPsAssignment2 );
        
        User lAdminUser = [ SELECT Id FROM User WHERE isActive = true AND Profile.Name = 'System Administrator' LIMIT 1 ];
        System.runAs( lAdminUser ){
            Database.SaveResult[] lLstSaveResult = Database.insert( lLstPermissionSetAssign , false );
        }
        
        Case lCase = New Case();
        System.runAs( lUser1 ){
            
            lCase.AccountId = [ SELECT Id FROM Account LIMIT 1 ].Id;
            lCase.Type = '3rd Party Paper';
            lCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('RTContractReview').getRecordTypeId();
            lCase.Status = 'Created';
            lCase.Subject = 'Subject test case';
            lCase.Priority = 'High';
            lCase.Description = 'Description test case';
            lCase.CurrencyIsoCode = 'USD';
            lCase.ContractAgreement__c = lCttAgreem.Id;
            Database.insert( lCase );  
        }
      
        CaseAssignmentHelper.assignCases(new List<case>{lCase});
             
        
    }
    
    @isTest
    static void GetCasesTest() {
        List< User > lLstUsers = [ SELECT Id FROM User WHERE Email = 'user.test@gympass.com' ];
        
        List<Case> listCases = new List<Case>();
        
        Test.startTest();
        System.runAs( lLstUsers[0] ){
            listCases = GetCases.getCases();
        }
        Test.stopTest();
        
//        System.assert(!listCases.isEmpty());
        
    }
}