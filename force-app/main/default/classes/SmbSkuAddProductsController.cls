public without sharing class SmbSkuAddProductsController {

    private class GenericException extends Exception{

    }

    @AuraEnabled
    public static List<QuoteLineItem> getSkuProducts(Id opportunityId){
        try {
            
            Opportunity opp = [SELECT Id, Pricebook2Id, CurrencyIsoCode, Quantity_Offer_Number_of_Employees__c, (SELECT Id, IsSyncing 
                                                                                                    FROM Quotes 
                                                                                                    WHERE IsSyncing = TRUE) 
                                FROM Opportunity 
                                WHERE Id = :opportunityId LIMIT 1];
                                
            Id pricebookId = opp.Pricebook2Id;
            Id quoteId = !opp.Quotes.isEmpty() ? opp.Quotes[0].Id : null;

            if(pricebookId == null) {
                throw new GenericException('This opportunity has no pricebook.');
            }
            if(quoteId == null) {
                throw new GenericException('This opportunity has no syncing quote.');
            }

            SMBSkuGetProductsService.ProductsRequest productRequest = new SMBSkuGetProductsService.ProductsRequest();
            productRequest.recordId = opp.Id;
            productRequest.currencyCode = opp.CurrencyIsoCode;
            productRequest.syncedQuote = quoteId;
            productRequest.quantity = opp.Quantity_Offer_Number_of_Employees__c != null? opp.Quantity_Offer_Number_of_Employees__c.intValue() : 0;
            productRequest.pricebookId = pricebookId;

            List<List<QuoteLineItem>> result = SMBSkuGetProductsService.getSkuProducts(new List<SMBSkuGetProductsService.ProductsRequest> {productRequest});
            return result[0];

        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

}