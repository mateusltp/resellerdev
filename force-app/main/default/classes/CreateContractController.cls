/**
 * @description       : 
 * @author            : ext.gft.marcus.silva@gympass.com
 * @group             : 
 * @last modified on  : 02-17-2022
 * @last modified by  : ext.gft.marcus.silva@gympass.com
**/
public with sharing class CreateContractController {
    @AuraEnabled
    public static List<APXT_Redlining__Contract_Agreement__c> getContractAgreement(String recordId){
        return [SELECT Name                 ,
                APXT_Redlining__Status__c ,
                SignedMethod__c           ,
                Include_full_T_C_s__c     ,
                APXT_Renegotiation__c     ,
                CreatedBy.Name               ,
                CreatedDate  
                    FROM APXT_Redlining__Contract_Agreement__c 
                    WHERE Opportunity__c = :recordId
                    ORDER BY CreatedDate DESC];       
    }

    @AuraEnabled
    public static List<Case> getCase(String recordId){
        Id recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('RTContractReview').getRecordTypeId();
        return [SELECT CaseNumber, 
                Owner.Name              ,
                ContractAgreement__r.Name ,        
                Status               , 
                CreatedBy.Name       ,
                CreatedDate
                    FROM Case
                    WHERE OpportunityId__c =: recordId AND RecordType.Id =: recordTypeId
                    ORDER BY CreatedDate DESC];
    }    

    @AuraEnabled
    public static string getProposalId(String recordId){
        return [SELECT SyncedQuoteId FROM Opportunity WHERE Id = :recordId].SyncedQuoteId;
    }
}