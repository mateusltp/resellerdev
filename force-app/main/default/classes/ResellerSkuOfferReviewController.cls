public without sharing class ResellerSkuOfferReviewController {
    public ResellerSkuOfferReviewController(){}

    public static Map< String , String > gMapDealDeskApprovedConditions;
    
    @AuraEnabled(cacheable=false)
    public static void sendToApproval( String recordId , String aRationale , Decimal aTotalDiscount , String aQuoteId , String aDealDeskRationale,
        Boolean needQuoteApproval, Boolean needDealDeskApproval, Boolean regionalManagerApproval){
        
        try{
            // if( needQuoteApproval ){    
            //     Approval.ProcessSubmitRequest quoteReq = new Approval.ProcessSubmitRequest();
            //     //quoteReq.setComments( 'Rationale:' + aRationale + '\nTotal Discount: ' + aTotalDiscount + '%\n' + lWaivers );
            //     quoteReq.setObjectId( aQuoteId );
            //     quoteReq.setSubmitterId( UserInfo.getUserId() );
            //     quoteReq.setNextApproverIds( getInicialApprover( aQuoteId ) );
            //     quoteReq.setProcessDefinitionNameOrId( 'SKU_Quote_Discount_Approval' );
            
            //     Approval.ProcessResult result = Approval.process( quoteReq );
            // }
            
            if( needDealDeskApproval ){
                sendDealDeskCaseToApproval( recordId , aQuoteId , aDealDeskRationale );
            }
            if( regionalManagerApproval ){
                sendEnterpriseToApproval( recordId , aQuoteId , aDealDeskRationale );
            }

        } catch( Exception aException ){
            if( !aException.getMessage().contains( 'NO_APPLICABLE_PROCESS' ) ){
                throw new CustomException( aException.getMessage() );
            }
        }
    }
    
    public static void sendDealDeskCaseToApproval( String aOppId , String aQuoteId , String aRationale ){
        String lCaseDescription = getDealDeskDescription( aQuoteId );

        Case lDeakDeskCase = 
            new Case (   
                Subject = 'Deal Desk Approval Request', 
                RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Deal_Desk_Approval').getRecordTypeId(), 
                OpportunityId__c = aOppId,
                Description = lCaseDescription + '\nRationale: ' + aRationale,
                QuoteId__c = aQuoteId,
                Status = 'New'
                );
        
        Database.SaveResult lCaseInsertResult = Database.insert( lDeakDeskCase );

        if(lCaseInsertResult.isSuccess()){
            Boolean lIsShared = new CaseRepository().manualShareRead( lDeakDeskCase.Id , UserInfo.getUserId() );
            createApprovalItems( lDeakDeskCase.Id , aOppId );
        }
    }
    
    public static void createApprovalItems( String aDealDeskCaseId , String aOppId ){
        Map< String , String > lMapApprovalItemValueField = getMapApprovalItemValueField( aOppId );

        ApprovalItemService lApprovalService = new ApprovalItemService();

        List< Approval_Item__c > lLstApprovalItem = 
            lApprovalService.addApprovalItem( aDealDeskCaseId , lMapApprovalItemValueField , gMapDealDeskApprovedConditions );

        lApprovalService.createApprovalItems( lLstApprovalItem );
    }

    @AuraEnabled(cacheable=false)
    public static void updateQuote( String aQuoteId, String rationaleDealDeskApproval, String rationaleCommercialConditions ){
        Quote lQuote = [ SELECT Deal_Desk_Comments__c, Rationale__c FROM Quote WHERE Id =: aQuoteId ];
        
        lQuote.Deal_Desk_Comments__c = rationaleDealDeskApproval;
        lQuote.Rationale__c = rationaleCommercialConditions;

        update lQuote;
    }

    @AuraEnabled(cacheable=false)
    public static Quote getQuote( String aQuoteId ){
        Quote lQuote = [ SELECT Deal_Desk_Comments__c, Rationale__c FROM Quote WHERE Id =: aQuoteId ];

        return lQuote;
    }

    public static Map< String , String > getMapApprovalItemValueField( String aOppId ){
        Map< String , String > lMapSobjNFieldNameFieldValue = getMapSobjNFieldNameFieldValue( aOppId );

        Map< String , String > lMapApprovalItemValueField = 
        new Map< String , String >{
            'ES fee type out of standard' => lMapSobjNFieldNameFieldValue.get( 'quotelineitem.fee_contract_type__c' ),
            'PS setup fee out of standard' => lMapSobjNFieldNameFieldValue.get( 'quotelineitem.unitprice' ),
            'PS monthly fee out of standard' => lMapSobjNFieldNameFieldValue.get( 'quotelineitem.unitprice' ),
            'ES Custom payment frequency' => lMapSobjNFieldNameFieldValue.get( 'payment__c.frequency__c' ),
            'PS custom payment frequency' => lMapSobjNFieldNameFieldValue.get( 'payment__c.frequency__c' ),
            'Credit card selected' => lMapSobjNFieldNameFieldValue.get( 'eligibility__c.payment_method__c' ),
            'No autonomous marketplace' => lMapSobjNFieldNameFieldValue.get( 'quote.autonomous_marketplace_contract__c' ),
            'Membership fee subsidized' => lMapSobjNFieldNameFieldValue.get( 'quote.Will_the_MF_be_subsidized__c' )
        };

        return lMapApprovalItemValueField;
    }

    public static Map< String , String > getMapSobjNFieldNameFieldValue( String aOppId ){
        Map< String , String > lMapSobjNFieldNameFieldValue = new Map< String , String >();

        Set< String > lSetQuoteFieldsToKeep = new Set< String >{ 'autonomous_marketplace_contract__c', 'Will_the_MF_be_subsidized__c' };
        Set< String > lSetEligibilityFieldsToKeep = new Set< String >{ 'payment_method__c' };
        Set< String > lSetQuoteLineItemFieldsToKeep = new Set< String >{ 'unitprice', 'fee_contract_type__c' };
        Set< String > lSetPaymentFieldsToKeep = new Set< String >{ 'frequency__c' };
        

        Quote lQuote = [ SELECT id, autonomous_marketplace_contract__c, Will_the_MF_be_subsidized__c FROM Quote WHERE OpportunityId =: aOppId LIMIT 1];
        Eligibility__c lEligibility = [ SELECT Payment_Method__c FROM Eligibility__c WHERE Opportunity_Id__c =: aOppId LIMIT 1];

        QuoteLineItem lQuoteLineItem = [ SELECT id, unitprice, fee_contract_type__c FROM QuoteLineItem WHERE QuoteId =: lQuote.Id LIMIT 1];
        Payment__c lPayment = [ SELECT frequency__c FROM Payment__c WHERE Opportunity__c =: aOppId LIMIT 1];

        for( String iFieldName : lSetQuoteFieldsToKeep ){
            lMapSobjNFieldNameFieldValue.put( 'quote.' + iFieldName , String.valueOf( lQuote.get( iFieldName ) ) );
        }

        for( String iFieldName : lSetEligibilityFieldsToKeep ){
            lMapSobjNFieldNameFieldValue.put( 'eligibility__c.' + iFieldName , String.valueOf( lEligibility.get( iFieldName ) ) );
        }

        for( String iFieldName : lSetQuoteLineItemFieldsToKeep ){
            lMapSobjNFieldNameFieldValue.put( 'quotelineitem.' + iFieldName , String.valueOf( lQuoteLineItem.get( iFieldName ) ) );
        }

        for( String iFieldName : lSetPaymentFieldsToKeep ){
            lMapSobjNFieldNameFieldValue.put( 'payment__c.' + iFieldName , String.valueOf( lPayment.get( iFieldName ) ) );
        }

        return lMapSobjNFieldNameFieldValue;
    }

    public static String getDealDeskDescription( String aQuoteId ){
        String lCaseDealDeskDescription = '';
        gMapDealDeskApprovedConditions = new Map< String , String >();

        Quote lQuote = [ SELECT Deal_Desk_Approved_Conditions__c FROM Quote WHERE Id =: aQuoteId LIMIT 1 ];

        List< ResellerSKUDealDeskHandler.DealDeskConditions > lLstDealDeskConditions =
            (List< ResellerSKUDealDeskHandler.DealDeskConditions >) JSON.deserialize( lQuote.Deal_Desk_Approved_Conditions__c , List< ResellerSKUDealDeskHandler.DealDeskConditions >.class );

        for( ResellerSKUDealDeskHandler.DealDeskConditions iCondition : lLstDealDeskConditions ){
            lCaseDealDeskDescription += iCondition.approvalCondition + ', ';
            gMapDealDeskApprovedConditions.put( iCondition.approvalCondition , String.valueOf( iCondition.isApproved ) );
        }

        return lCaseDealDeskDescription;
    }

    public static List<Waiver__c> getWaiversToApproval( String aQuoteId ){
        
        /**METODO ANTIGO*/
        /*
        String lAllWaivers = '';
        Map< String , String > lMapItemWaivers = new Map< String , String >();

        for( Waiver__c iWaiver : [ SELECT Id, Quote_Line_Item__r.Product_Name__c, Percentage__c, Duration__c FROM Waiver__c WHERE Quote_Line_Item__r.QuoteId =: aQuoteId ] ){
            if( lMapItemWaivers.get( iWaiver?.Quote_Line_Item__r?.Product_Name__c ) == null ){
                lMapItemWaivers.put( iWaiver.Quote_Line_Item__r.Product_Name__c , ( iWaiver.Percentage__c + '% / ' + iWaiver.Duration__c + ' months' ) );
            } else {
                String lNewWaiver = lMapItemWaivers.get( iWaiver.Quote_Line_Item__r.Product_Name__c );
                lNewWaiver += ( ' - ' + iWaiver.Percentage__c + '% / ' + iWaiver.Duration__c + ' months' );
                lMapItemWaivers.put( iWaiver.Quote_Line_Item__r.Product_Name__c , lNewWaiver );
            }
        }

        for( String iQuoteLineItem : lMapItemWaivers.keySet() ){
            lAllWaivers += (( iQuoteLineItem + ': ' + lMapItemWaivers.get( iQuoteLineItem ) ) + '\n' );
        }

        return lAllWaivers;
*/
        
        List<Waiver__c> listWaivers = [SELECT Id, Quote_Line_Item__r.Product_Name__c, Percentage__c, Duration__c,Approval_Status__c,Approval_Status_Formula__c FROM Waiver__c WHERE Quote_Line_Item__r.QuoteId =: aQuoteId];
		List<Waiver__c> listWaiversToApproval = new List<Waiver__c>();
        
        for(Waiver__c waiver : listWaivers){
            if(waiver.Approval_Status_Formula__c == 'Needs Approval'){
                listWaiversToApproval.add(waiver);                
    }
        }
        return listWaiversToApproval;
    }

    public static Id[] getInicialApprover( String aQuoteId ){
        Quote lQuote = [ SELECT Discount_Approval_Level__c, Waiver_Approval_Level__c FROM Quote WHERE Id =: aQuoteId ];
        User lUser = [ SELECT ManagerId, Manager.ManagerId FROM User WHERE Id =: UserInfo.getUserId() ];

        Decimal aTotalDiscountLevel = lQuote.Discount_Approval_Level__c;
        Decimal aWaiverApprovalLevel = lQuote.Waiver_Approval_Level__c;

        return new Id[]{ ( lQuote.Discount_Approval_Level__c == 0 && lQuote.Waiver_Approval_Level__c > 0 ? lUser.Manager.ManagerId : lUser.ManagerId ) };
    }
    
    public static void sendEnterpriseToApproval( String aOppId , String aQuoteId , String aRationale ){
        
        QuoteRepository quoteRepository = new QuoteRepository();
        Quote quote = quoteRepository.lastForOpportunity(aOppId);
        
        if(quote.Enablers_Approval_Needed__c || quote.Enterprise_Discount_Approval_Needed__c){
            if(quote.Enablers_Approval_Needed__c){
                quote.Approval_Level__c = 1;
                if(quote.Enterprise_Discount_Approval_Needed__c){
                    quote.Approval_Level__c = quote.Approval_Level__c + quote.User_Discount_Approval_Level__c;
                }
            }else if(quote.Enterprise_Discount_Approval_Needed__c){
                quote.Approval_Level__c = 3 + quote.User_Discount_Approval_Level__c;
            }
            update quote;
            submitIndirectCommercialApproval(quote);
        }

        if(!quote.Waiver_approved__c){
            submitItemWaiversToApproval(getWaiversToApproval(quote.id));
        }       

    }

    public static void submitIndirectCommercialApproval(Quote quote){
        try{
            Approval.ProcessSubmitRequest quoteReq = new Approval.ProcessSubmitRequest();
            quoteReq.setComments('Submitting request for approval. ' + quote.Rationale__c);
            quoteReq.setObjectId(quote.Id);
            quoteReq.setSubmitterId(UserInfo.getUserId());
            quoteReq.setProcessDefinitionNameOrId('Indirect_Approval_v9');
            Approval.ProcessResult result = Approval.process( quoteReq );
            system.debug('### '+result);
        } catch( Exception aException ){
            if( !aException.getMessage().contains( 'NO_APPLICABLE_PROCESS' ) ){
                throw new CustomException( aException.getMessage() + 'Indirect_Approval' );
            }
        }
    }  
    
     /* public void checkEnablersFieldsApproved(Map< Id , Quote > aMapIdOldQuote , List< Quote > aLstNewQuote) {
        Set< String > gSetOppRecordtypeId = new Set< String >{
            Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Indirect_Channel_New_Business').getRecordTypeId()};

        List< Quote > lLstQuoteChanged = new List < Quote >();
        List< Opportunity > lLstOpportunity = new List < Opportunity >();
        Set< Id > lSetOpportunity = new Set< Id >();

        for( Quote iQuote : aLstNewQuote ){
            lSetOpportunity.add(iQuote.OpportunityId);
        }
        Map< Id , Opportunity > lMapOpportunityId = getMapOpportunityId( lSetOpportunity );
		Map< Id , Opportunity > lMapOpportunityIdEnabler = new Map< Id , Opportunity >();
        for( Quote iQuote : aLstNewQuote ){
            Opportunity opp = lMapOpportunityId.get(iQuote.OpportunityId);

            if( !gSetOppRecordtypeId.contains( opp.RecordTypeId ) ){ continue; }

            else if( aMapIdOldQuote.get( iQuote.Id ).Contact_Permission__c != iQuote.Contact_Permission__c 
             || aMapIdOldQuote.get( iQuote.Id ).Expansion_100_of_the_eligible__c != iQuote.Expansion_100_of_the_eligible__c
             || aMapIdOldQuote.get( iQuote.Id ).Exclusivity_clause__c != iQuote.Exclusivity_clause__c
             || aMapIdOldQuote.get( iQuote.Id ).Employee_Corporate_Email__c != iQuote.Employee_Corporate_Email__c
            
            ){ lMapOpportunityIdEnabler.put(opp.Id, opp);} 
        }

       
        if(lMapOpportunityIdEnabler.size() > 0){
            evaluateEnablers(lMapOpportunityId, aLstNewQuote, lSetOpportunity);
        }
    }*/

    public void evaluateEnablers(Map< Id , Opportunity > aMapOpportunityId ) {  // List< Quote > aLstNewQuote, Set<Id> aSetOpportunity
        Set<String> enablersNames = new Set<String>{'Allowlist', 'Full launch | Expansion', 'Exclusivity', 'Email Database'};
        String enablersOutOfStandardCondition = '';
        Set< Id > lSetOpportunity = new Set< Id >();
        Set< Id > lSetQuote = new Set< Id >();
        List< Quote > lLstQuoteChanged = new List < Quote >();

        for( Opportunity opp : aMapOpportunityId.values() ){
            lSetQuote.add(opp.SyncedQuoteId);
            lSetOpportunity.add(opp.id);
        }

        Map<Id, List<Step_Towards_Success1__c>> oppIdToStepsList = getStepsForOpportunities(lSetOpportunity); 
        
        List< Quote > lLstQuote = [SELECT Id, Enablers_out_of_standard_condition__c, Enablers_Approval_Needed__c, Enablers_Approved__c, OpportunityId FROM Quote WHERE Id IN :lSetQuote];

        for( Quote iQuote : lLstQuote ){
            if(aMapOpportunityId.get(iQuote.OpportunityId) != null){
                Opportunity opp = aMapOpportunityId.get(iQuote.OpportunityId);

                List<Step_Towards_Success1__c> steps = oppIdToStepsList.get(opp.Id);
                //List<Step_Towards_Success1__c> steps = opp.Steps_Towards_Success1__r;

                if(steps.size() > 0){

                    for (Step_Towards_Success1__c step : steps) {
                        if (enablersNames.contains(step.Name) && step.Achieved__c == 'No') {
                            
                            if (opp.Account.BillingCountry == 'Brazil' && step.Step_Number__c == 6) {
                                enablersOutOfStandardCondition += (enablersOutOfStandardCondition == '' ? step.Name : '; ' + step.name) ;
                            } else if (step.Step_Number__c != 6) {
                                enablersOutOfStandardCondition += (enablersOutOfStandardCondition == '' ? step.Name : '; ' + step.name);
                            }
                        }
                    }
                
                    if(iQuote.Enablers_out_of_standard_condition__c != enablersOutOfStandardCondition){
                        iQuote.Enablers_Approval_Needed__c = true;
                        iQuote.Enablers_Approved__c = false;                
                    }
                    if(enablersOutOfStandardCondition == ''){
                        iQuote.Enablers_Approval_Needed__c = false;
                        iQuote.Enablers_Approved__c = true;
                    }
                    iQuote.Enablers_out_of_standard_condition__c = enablersOutOfStandardCondition;
                    lLstQuoteChanged.add(iQuote);
                }  
            } 
        } update lLstQuoteChanged;
    }

    private static Map<Id, List<Step_Towards_Success1__c>> getStepsForOpportunities(Set<Id> oppIdSet) {
        Map<Id, List<Step_Towards_Success1__c>> oppIdToStepsList = new Map<Id, List<Step_Towards_Success1__c>>();
        EnablersRepository enablers = new EnablersRepository();
        List<Step_Towards_Success1__c> steps = enablers.getOpportunityEnablersByOppId(oppIdSet);

        for (Step_Towards_Success1__c step : steps) {
            if (oppIdToStepsList.containsKey(step.Related_Opportunity__c)) {
                oppIdToStepsList.get(step.Related_Opportunity__c).add(step);
            } else {
                oppIdToStepsList.put(step.Related_Opportunity__c, new List<Step_Towards_Success1__c>{step});
            }        
        }

        return oppIdToStepsList;
    }
    

    public Map< Id , Opportunity > getMapOpportunityId( Set< Id > lSetOppId ){
        Map< Id , Opportunity > lMapOpportunityId = new Map< Id , Opportunity >();
       
        for( Opportunity iOpportunity : [ SELECT Id, RecordTypeId, RecordType.DeveloperName, CloseDate, Account.BillingCountry, Account.Proposal_End_Date__c,B_M__c FROM Opportunity WHERE Id =: lSetOppId ] ){
            lMapOpportunityId.put( iOpportunity.Id , iOpportunity );
        }
       
        return lMapOpportunityId;
    }
    
    @testVisible
    public static void submitItemWaiversToApproval(List<Waiver__c> waivers){
        if( waivers.isEmpty() ){ return; }
        List<Approval.ProcessSubmitRequest> approvalRequests = new List<Approval.ProcessSubmitRequest>();
        User lCurrentUser = [ SELECT ManagerId FROM User WHERE Id =: UserInfo.getUserId() ];

        for( Waiver__c iWaiver : waivers ){
            Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
            req1.setComments('Submitting request for approval. ');
            req1.setObjectId( iWaiver.Id );
            req1.setSubmitterId( UserInfo.getUserId() );
            req1.setNextApproverIds(new Id[] { getWaiverInitialApproverId() });
            req1.setProcessDefinitionNameOrId('Waiver_approval_2');

            approvalRequests.add( req1 );
            
            //iWaiver.Rationale__c = to.proposal.accessFee.waiversJustification;
        }
        new WaiverRepository().edit(waivers);

        try{
            Approval.ProcessResult[] results = Approval.process(approvalRequests);
        } catch (Exception e){
            if(!e.getMessage().contains( 'NO_APPLICABLE_PROCESS' )){
                throw new CustomException(e.getMessage());
            }
        }
    }

    public static Id getWaiverInitialApproverId(){
        User lCurrentUser;

            lCurrentUser = [ SELECT ManagerId FROM User WHERE Id =: UserInfo.getUserId() ];
            return lCurrentUser.ManagerId;
    }


}