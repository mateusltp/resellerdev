/**
 * @description       : 
 * @author            : bruno.mendes@gympass.com
 * @group             : 
 * @last modified on  : 28/04/2022
 * @last modified by  : bruno.mendes@gympass.com
**/

@IsTest
private class ProductThresholdMemberSelectorTest {

    @TestSetup
    static void setup() {
        Account account = PartnerDataFactory.newAccount();
        insert account;

        Opportunity opportunity = PartnerDataFactory.newOpportunity(account.Id, 'Partner_Flow_Opportunity');
        insert opportunity;

        Product_Item__c product = PartnerDataFactory.newProduct(opportunity);
        insert product;

        Threshold__c threshold = PartnerDataFactory.newThreshold();
        insert threshold;

        Product_Threshold_Member__c productThresholdMember = PartnerDataFactory.newThresholdMember(product.Id, threshold.Id);
        insert productThresholdMember;
    }

    @isTest
    static void getSObjectFieldList_Test(){

        List<Schema.SObjectField> schSObjLst = new List<Schema.SObjectField> {
                Product_Threshold_Member__c.Id
        };

        Test.startTest();
        System.assertEquals(schSObjLst, new ProductThresholdMemberSelector().getSObjectFieldList());
        Test.stopTest();
    }

    @isTest
    static void getSObjectType_Test(){

        Test.startTest();
        System.assertEquals(Product_Threshold_Member__c.sObjectType, new ProductThresholdMemberSelector().getSObjectType());
        Test.stopTest();
    }

    @isTest
    static void selectById_Test(){

        List<Product_Threshold_Member__c> productThresholdMembers = [SELECT Id FROM Product_Threshold_Member__c LIMIT 1];

        Test.startTest();
        system.assertEquals(productThresholdMembers[0].Id, new ProductThresholdMemberSelector().selectById(new Set<Id> {productThresholdMembers[0].Id}).get(0).id);
        Test.stopTest();
    }

    @IsTest
    static void selectThresholdByProductId_Test() {
        Id productId = [SELECT Id FROM Product_Item__c LIMIT 1].Id;
        Id productThresholdMemberId = [SELECT Id FROM Product_Threshold_Member__c LIMIT 1].Id;

        Test.startTest();

        System.assertEquals(productThresholdMemberId, new ProductThresholdMemberSelector().selectThresholdByProductId(new Set<Id>{productId})[0].Id);

        Test.stopTest();
    }

    @IsTest
    static void selectThresholdToReplicate_Test() {
        Id productId = [SELECT Id FROM Product_Item__c LIMIT 1].Id;
        Id productThresholdMemberId = [SELECT Id FROM Product_Threshold_Member__c LIMIT 1].Id;

        Test.startTest();

        System.assertEquals(productThresholdMemberId, new ProductThresholdMemberSelector().selectThresholdToReplicate(productId)[0].Id);

        Test.stopTest();
    }

    @IsTest
    static void selectThresholdsByProdId_Test() {
        Id productId = [SELECT Id FROM Product_Item__c LIMIT 1].Id;
        Id productThresholdMemberId = [SELECT Id FROM Product_Threshold_Member__c LIMIT 1].Id;

        Test.startTest();

        System.assertEquals(productThresholdMemberId, new ProductThresholdMemberSelector().selectThresholdsByProdId(new Set<Id>{productId})[0].Id);

        Test.stopTest();
    }

    @IsTest
    static void selectThresholdMapByProductId_Test() {
        Id productId = [SELECT Id FROM Product_Item__c LIMIT 1].Id;
        Id productThresholdMemberId = [SELECT Id FROM Product_Threshold_Member__c LIMIT 1].Id;

        Test.startTest();

        System.assertEquals(productThresholdMemberId, new ProductThresholdMemberSelector().selectThresholdMapByProductId(new Set<Id>{productId}).values()[0].Id);

        Test.stopTest();
    }

}