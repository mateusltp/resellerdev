/**
 * @description       :
 * @author            : roei@gft.com
 * @group             :
 * @last modified on  : 06-18-2021
 * @last modified by  : roei@gft.com
 * Modifications Log
 * Ver   Date         Author         Modification
 * 1.0   09-16-2020   roei@gft.com   Initial Version
 **/
public with sharing virtual class EnterpriseRevenueMetrics {
  private QuoteLineItemRepository items;
  private WaiverRepository waivers;

  private Decimal esRevenue;
  private Decimal aesRevenue;
  private Decimal erRevenue;
  private Decimal aerRevenue;

  public class RevenueTO {
    public QuoteLineItem accessFee;
    public QuoteLineItem setupFee;
    public QuoteLineItem proServicesOneFee;
    public QuoteLineItem proServicesMainFee;
    public List<FastTrackProposalCreationTO.Waiver> accessfeeWaivers;
    public List<FastTrackProposalCreationTO.Waiver> setupFeeWaivers;
    public List<FastTrackProposalCreationTO.Waiver> psOneFeeWaivers;
    public List<FastTrackProposalCreationTO.Waiver> psMainFeeWaivers;
    public RevenueTO(){
        accessfeeWaivers = new List<FastTrackProposalCreationTO.Waiver>();
        setupFeeWaivers = new List<FastTrackProposalCreationTO.Waiver>();
        psOneFeeWaivers = new List<FastTrackProposalCreationTO.Waiver>();
        psMainFeeWaivers = new List<FastTrackProposalCreationTO.Waiver>();
    }
  }

  protected virtual void init() {
    items = new QuoteLineItemRepository();
    waivers = new WaiverRepository();
  }

  public EnterpriseRevenueMetrics() {
    init();
  }

  public EnterpriseRevenueMetrics(
    QuoteLineItem accessFee,
    List<FastTrackProposalCreationTO.Waiver> accessFeeWaivers,
    QuoteLineItem setupFee,
    List<FastTrackProposalCreationTO.Waiver> setupFeeWaivers,
    QuoteLineItem psOneFee,
    List<FastTrackProposalCreationTO.Waiver> psOneFeeWaivers,
    QuoteLineItem psMainFee,
    List<FastTrackProposalCreationTO.Waiver> psMainFeeWaivers
  ) {
    setEsRevenue(accessFee, psMainFee);
    setAesRevenue(accessFee, accessFeeWaivers, psMainFee, psMainFeeWaivers);
    setErRevenue(setupFee, psOneFee);
    setAerRevenue();
  }

  public void refreshQuoteRevenues(Id quoteId) {
    List<QuoteLineItem> quoteItems = items.all(quoteId);
    
    Set<Id> itemsIds = new Set<Id>();
    for ( QuoteLineItem item : quoteItems )
        itemsIds.add(item.Id);
    
    List<Waiver__c> waiversForItems = waivers.allForItems(itemsIds);

    RevenueTO revenues = new RevenueTO();
    buildItemsForRevenueTO(revenues, quoteItems);
    buildWaiversForRevenueTO(revenues, waiversForItems);

    setEsRevenue(revenues.accessFee, revenues.proServicesMainFee);
    setAesRevenue(revenues.accessFee, revenues.accessFeeWaivers, revenues.proServicesMainFee, revenues.psMainFeeWaivers);
    setErRevenue(revenues.setupFee, revenues.proServicesOneFee);
    setAerRevenue(revenues.setupFee, revenues.proServicesOneFee);
  }

  public void buildItemsForRevenueTO(RevenueTO revenues, List<QuoteLineItem> items) {
    for (QuoteLineItem item : items) {
      if (item.PricebookEntry.Product2.Name == 'Professional Services Maintenance Fee')
        revenues.proServicesMainFee = item;
      else if (item.PricebookEntry.Product2.Name == 'Professional Services Setup Fee')
        revenues.proServicesOneFee = item;
      else if (item.PricebookEntry.Product2.Family == 'Setup Fee')
        revenues.setupFee = item;
      else if (item.PricebookEntry.Product2.Family == 'Enterprise Subscription' )
        revenues.accessFee = item;
    }
  }

  public void buildWaiversForRevenueTO(RevenueTO revenues, List<Waiver__c> waivers){
    for (Waiver__c waiver : waivers) {
        FastTrackProposalCreationTO.Waiver waiverTo = new FastTrackProposalCreationTO.Waiver();  
        if (waiver.Payment__r.Quote_Line_Item__r.PricebookEntry.Product2.Name == 'Professional Services Maintenance Fee'){
          waiverTo.sObj = waiver;
          revenues.psMainFeeWaivers.add(waiverTo);
        }else if (waiver.Payment__r.Quote_Line_Item__r.PricebookEntry.Product2.Name == 'Professional Services Setup Fee'){          
          waiverTo.sObj = waiver;
          revenues.psOneFeeWaivers.add(waiverTo);
        }else if (waiver.Payment__r.Quote_Line_Item__r.PricebookEntry.Product2.Family == 'Setup Fee'){          
          waiverTo.sObj = waiver;
          revenues.setupFeeWaivers.add(waiverTo);
        }else if (waiver.Payment__r.Quote_Line_Item__r.PricebookEntry.Product2.Family == 'Enterprise Subscription' ){          
          waiverTo.sObj = waiver;
          revenues.accessfeeWaivers.add(waiverTo);
        }
      }
  }

  public Decimal getEsRevenue() {
    return this.esRevenue;
  }

  public Decimal getAesRevenue() {
    return this.aesRevenue;
  }

  public Decimal getErRevenue() {
    return this.erRevenue;
  }

  public Decimal getAerRevenue() {
    return this.aerRevenue;
  }

  //ES soma valores de items AccessFee e Professional Services Main Fee (se tiver claro)
  private void setEsRevenue(QuoteLineItem accessFee, QuoteLineItem psMainFee) {
    Decimal accessFeeTotalPrice = accessFee == null ||
      accessFee.TotalPrice == null
      ? 0
      : accessFee.TotalPrice;
    Decimal psMainFeeTotalPrice = psMainFee == null ||
      psMainFee.TotalPrice == null
      ? 0
      : psMainFee.TotalPrice;

    this.esRevenue = accessFeeTotalPrice + psMainFeeTotalPrice;
  }

  /* AES  mesmo coisa que o de cima, porém varre os waivers do payment posição 0  de cada um destes quote items citados(acessfee e ps main fee), e faz para cada um: 
       (totalAccessFeeAES = ES - (totalAccessFeeWaiversMonths*accessFee price) e
       (totalPsMainFeeAES = ES - (totalPsMainFeeWaiversMonths*psMainFee price)   e fecha somando os dois
       AES = (12 * (totalAccessFee + totalPsMainFee))
    */
  private void setAesRevenue(
    QuoteLineItem accessFee,
    List<FastTrackProposalCreationTO.Waiver> accessFeeWaivers,
    QuoteLineItem psMainFee,
    List<FastTrackProposalCreationTO.Waiver> psMainFeeWaivers
  ) {
    Decimal accessFeeTotalPrice = accessFee == null ||
      accessFee.TotalPrice == null
      ? 0
      : accessFee.TotalPrice;
    Decimal psMainFeeTotalPrice = psMainFee == null ||
      psMainFee.TotalPrice == null
      ? 0
      : psMainFee.TotalPrice;
    Decimal accessFeeWaiverMonth = 0;
    Decimal accessFeeWaiverPercentage = 0;
    Decimal totalWaiverValue = 0;
    for (FastTrackProposalCreationTO.Waiver iWaiver : accessFeeWaivers) {
        accessFeeWaiverMonth = (iWaiver.sObj.Number_of_Months_after_Launch__c ==   null ? 0  : iWaiver.sObj.Number_of_Months_after_Launch__c);
        if (accessFeeWaiverMonth > 12)
            accessFeeWaiverMonth = 12;
        
        accessFeeWaiverPercentage = (iWaiver.sObj.Percentage__c ==  null ? 100 : iWaiver.sObj.Percentage__c);
        
        totalWaiverValue += (accessFeeTotalPrice * (accessFeeWaiverPercentage/100))*accessFeeWaiverMonth;

        System.debug( ' - accessFeeWaiverMonth =  ' + accessFeeWaiverMonth);
        System.debug( ' - accessFeeWaiverPercentage =  ' + accessFeeWaiverPercentage);
    }   

    System.debug( ' - accessFeeTotalPrice =  ' + accessFeeTotalPrice);
    System.debug( ' - psMainFeeTotalPrice =  ' + psMainFeeTotalPrice);
    System.debug( ' - totalWaiverValue =  ' + totalWaiverValue);

    Decimal totalAccessFeeAES = (( 12  * accessFeeTotalPrice) - (totalWaiverValue));

    this.aesRevenue = (totalAccessFeeAES + (12 * psMainFeeTotalPrice));
  }

  //ER = ES + setupFee price + psOneFee price (se existir essa ultima claro)
  private void setErRevenue(QuoteLineItem setupFee, QuoteLineItem psOneFee) {
    Decimal setupFeeTotalPrice = setupFee == null ||
      setupFee.TotalPrice == null
      ? 0
      : (setupFee.TotalPrice/12);
    Decimal psOneFeeTotalPrice = psOneFee == null ||
      psOneFee.TotalPrice == null
      ? 0
      : (psOneFee.TotalPrice/12);

    this.erRevenue = this.esRevenue + setupFeeTotalPrice + psOneFeeTotalPrice;
  }

  private void setAerRevenue(QuoteLineItem setupFee, QuoteLineItem psOneFee) {
    Decimal setupFeeTotalPrice = setupFee == null ||
      setupFee.TotalPrice == null
      ? 0
      : setupFee.TotalPrice;
    Decimal psOneFeeTotalPrice = psOneFee == null ||
      psOneFee.TotalPrice == null
      ? 0
      : psOneFee.TotalPrice;

    this.aerRevenue = this.aesRevenue +  setupFeeTotalPrice +  psOneFeeTotalPrice;
  }

  private void setAerRevenue() {
    this.aerRevenue = (12 * this.esRevenue);
  }
}