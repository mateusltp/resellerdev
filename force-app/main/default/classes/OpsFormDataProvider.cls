/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 04-08-2022
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/
global with sharing class OpsFormDataProvider implements sortablegrid.sdgIDataProvider{

    Id opportunityRT = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Child_Opportunity').getRecordTypeId();
    
    global static Boolean isUserSelectable()
    {
        return true;
    }

    global static sortablegrid.SDGResult getData(sortablegrid.SDG coreSDG,  sortablegrid.SDGRequest request) {

        OpportunitySelector opportunitySel = (OpportunitySelector)Application.Selector.newInstance(Opportunity.SObjectType);
        Id accountId = opportunitySel.selectById(new Set<Id>{request.ParentRecordID}).get(0).AccountId;

        OpsSetupFormSelector opsSetupSelector = (OpsSetupFormSelector)Application.Selector.newInstance(Ops_Setup_Validation_Form__c.SObjectType);
        AccountContractAgreementRelSelector accountContractSelector = (AccountContractAgreementRelSelector)Application.Selector.newInstance(Account_Contract_Agreement_Relationship__c.sobjectType); 
        Map<Id, List<Account_Contract_Agreement_Relationship__c>> accsInContract = accountContractSelector.selectByOpportunityId(new Set<Id>{request.ParentRecordID});

        Set<id> accountIds = new Set<Id>();
        if(!accsInContract.isEmpty()){           
            for (Account_Contract_Agreement_Relationship__c iAcc : accsInContract.get( request.ParentRecordID )  ){
                accountIds.add(iAcc.AccountId__c);
            }
        }

        Map<Id,Ops_Setup_Validation_Form__c> opsFormByAccount = opsSetupSelector.selectOpsSetupFormForPartner(request.ParentRecordID,accountIds);   
        Ops_Setup_Validation_Form__c opsFormResult = new Ops_Setup_Validation_Form__c();
        if(!opsFormByAccount.isEmpty()){
            opsFormResult = opsFormByAccount.get(accountId);
        }
        sortablegrid.SDGResult result = new sortablegrid.SDGResult();       
        result.data = new List<Sobject>();
        result.data.addAll((List<sObject>)new List<Ops_Setup_Validation_Form__c>{opsFormResult});
        result.FullQueryCount = result.data.size();
        result.pagecount = 1;
        result.isError = false;
        result.ErrorMessage = '';
        return result;
    }

    global sortablegrid.SDG LoadSDG(String SDGTag, String ParentRecordId)
    {          
        sortablegrid.SDG CoreSDG = new sortablegrid.SDG( 'teste' );
        CoreSDG.SDGFields = GetFields();
        return CoreSDG;
    }

   
    private List<sortablegrid.SDGField> GetFields()
    {
        List<sortablegrid.SDGField> fields = new List<sortablegrid.SDGField>();

        fields.add( new sortablegrid.SDGField('1', 'Name', 'Name', 'STRING', '', false, false, null, 1));      
        return fields; 
    }

   
}