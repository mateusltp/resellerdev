/**
 * @description       : IT-2185: Data provider class for CurrentValidProducts Sortable Data Grid
 * @author            : bruno.mendes@gympass.com
 * @group             :
 * @last modified on  : 10-01-2022
 * @last modified by  : bruno.mendes@gympass.com
 */

global with sharing class OpportunityWithChildOppsDataProvider implements sortablegrid.sdgIDataProvider
{
    global String className = OpportunityWithChildOppsDataProvider.class.getName();
    global PS_Constants constants = PS_Constants.getInstance();

    global static Boolean isUserSelectable()
    {
        return true;
    }

    global sortablegrid.SDGResult getData(sortablegrid.SDG coreSDG,  sortablegrid.SDGRequest request)
    {
        sortablegrid.SDGResult result = new sortablegrid.SDGResult();
        System.debug('result >>>'+result);

        result.data = new List<Sobject>();
        System.debug('result data'+result.data);

        try {
            AccountOpportunitySelector accOppSelector = (AccountOpportunitySelector)Application.Selector.newInstance(Account_Opportunity_Relationship__c.sobjectType);

            List<Account_Opportunity_Relationship__c> accountOpportunityRelationships = accOppSelector.selectById(new Set<Id> {request.ParentRecordID});

            OpportunitySelector opportunitySelector = (OpportunitySelector)Application.Selector.newInstance(Opportunity.SObjectType);

            List<Opportunity> opportunities = opportunitySelector.selectOpportunityAndChildOppsById(new Set<Id> {accountOpportunityRelationships.get(0).Opportunity__c});
            List<Opportunity> opportunitiesToReturn = new List<Opportunity>();
            opportunitiesToReturn.add(opportunities.get(0));
            for (Opportunity childOpp : opportunities.get(0).ChildOpportunities__r) {
                opportunitiesToReturn.add(childOpp);
            }
            System.debug('Opportunities to return >>>' +opportunitiesToReturn);
            result.data.addAll(opportunitiesToReturn);
            result.FullQueryCount = result.data.size();
            result.pagecount = 1;
            result.isError = false;
            result.ErrorMessage = '';
        }
        catch (Exception e) {
            DebugLog__c log = new DebugLog__c(
                    Origin__c = '['+className+'][getData]',
                    LogType__c = constants.LOGTYPE_ERROR,
                    RelatedToSquad__c = constants.LOGTYPE_SQUAD_PARTNERS,
                    ExceptionMessage__c = e.getMessage(),
                    StackTrace__c = e.getStackTraceString(),
                    ObjectId__c = request.ParentRecordID
            );
            Logger.createLog(log);
            throw new OpportunityWithChildOppsDataProviderException();
        }

        return result;
    }

    global sortablegrid.SDG LoadSDG(String SDGTag, String ParentRecordId)
    {
        sortablegrid.SDG CoreSDG = new sortablegrid.SDG( 'OpportunityWithChildOpps' );
        CoreSDG.SDGFields = GetFields();
        return CoreSDG;
    }

    private List<sortablegrid.SDGField> GetFields()
    {
        List<sortablegrid.SDGField> fields = new List<sortablegrid.SDGField>();

        fields.add( new sortablegrid.SDGField('1', 'Opportunity Name', 'Name', 'STRING', '', false, false, null, 1));
        fields.add( new sortablegrid.SDGField('2', 'Stage', 'StageName', 'STRING', '', false, false, null, 3));

        return fields;
    }

    public class OpportunityWithChildOppsDataProviderException extends Exception {}
}