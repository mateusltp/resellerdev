@IsTest
public class SelfCheckoutRequestValidatorTest {
  @IsTest
  public static void execute() {
    String errorMessage;

    try {
      new SelfCheckoutRequestValidator(null).validatePostRequest();
    } catch(IntegrationException error) {
      errorMessage = error.getMessage();
    }

    System.assertEquals(
      '(Request body cannot be empty)',
      errorMessage,
      'the body do not be null'
    );

    try {
      new SelfCheckoutRequestValidator(new SelfCheckoutRequest()).validatePostRequest();
    } catch(IntegrationException error) {
      errorMessage = error.getMessage();
    }

    System.assertEquals(
      '(account field is requidred, opportunity field is requidred)',
      errorMessage,
      'the account and the opportunity fields must be required'
    );

    SelfCheckoutRequest selfCheckoutRequest = SelfCheckoutRequestMock.getMockWithAccountAndOpportunityEmpty();

    try {
      new SelfCheckoutRequestValidator(selfCheckoutRequest).validatePostRequest();
    } catch(IntegrationException error) {
      errorMessage = error.getMessage();
    }

    List<String> expectedErrorMessages = new List<String>();
    expectedErrorMessages.add('The account required field id is missing');
    expectedErrorMessages.add('The account required field trade_name is missing');
    expectedErrorMessages.add('The account required field currency_id is missing');
    expectedErrorMessages.add('The account required field contacts is missing');
    expectedErrorMessages.add('The account required field addresses is missing');

    System.assertEquals(
      expectedErrorMessages.toString(),
      errorMessage,
      'Not all validations have been done'
    );
  }
}