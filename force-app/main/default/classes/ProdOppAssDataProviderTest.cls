/**
 * @description       : 
 * @author            : bruno.mendes@gympass.com
 * @group             : 
 * @last modified on  : 03/05/2022
 * @last modified by  : bruno.mendes@gympass.com
**/

@IsTest
private class ProdOppAssDataProviderTest {
    @IsTest
    static void getData_Test() {
        fflib_ApexMocks mocks = new fflib_ApexMocks();
        ProductOpportunityAssignmentSelector mockProdOppAssSelector = (ProductOpportunityAssignmentSelector)mocks.mock(ProductOpportunityAssignmentSelector.class);

        Id mockProductId = fflib_IDGenerator.generate(Product_Item__c.SObjectType);

        Id mockProductAssignmentId = fflib_IDGenerator.generate(Product_Assignment__c.SObjectType);
        Product_Assignment__c mockProductAssignment = new Product_Assignment__c(Id = mockProductAssignmentId, ProductId__c = mockProductId);

        Id mockProductOppAssignmentId = fflib_IDGenerator.generate(Product_Opportunity_Assignment__c.SObjectType);
        Product_Opportunity_Assignment__c mockProductOppAssignment = new Product_Opportunity_Assignment__c(Id = mockProductOppAssignmentId, ProductAssignmentId__r = mockProductAssignment);
        List<Product_Opportunity_Assignment__c> mockProductOppAssignments = new List<Product_Opportunity_Assignment__c>{mockProductOppAssignment};

        mocks.startStubbing();

        mocks.when(mockProdOppAssSelector.sObjectType()).thenReturn(Product_Opportunity_Assignment__c.SObjectType);
        mocks.when(mockProdOppAssSelector.selectOpportunityByProductId(mockProductId)).thenReturn(mockProductOppAssignments);

        mocks.stopStubbing();

        Application.Selector.setMock(mockProdOppAssSelector);

        Test.startTest();

        // instantiate class
        ProdOppAssDataProvider dataProvider = new ProdOppAssDataProvider();
        sortablegrid.SDG coreSDG = dataProvider.LoadSDG('', mockProductId);

        // create the user request
        sortablegrid.SDGRequest request = new sortablegrid.SDGRequest();
        request.ParentRecordID = mockProductId;
        request.PageID = 1;
        request.PageSize = 10;
        request.FieldSetName=null;
        request.RelationshipName=null;
        request.SDGTag='Apex:ProdOppAssDataProvider';



        sortablegrid.SDGResult result = dataProvider.getData(coreSDG, request);
        system.assertEquals(1, result.data.size());

        Test.stopTest();
    }

    @IsTest
    static void isUserSelectableTest(){

        Boolean isUserSelectable = true;
        System.assertEquals(isUserSelectable, ProdOppAssDataProvider.isUserSelectable());
    }
}