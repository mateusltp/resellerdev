/**
 * @description       : Selector for Step_Towards_Success_Partner__c SObject
 * @author            : alysson.mota@gympass.com
 * @group             : 
 * @last modified on  : 04-08-2022
 * @last modified by  : alysson.mota@gympass.com
**/
public with sharing class StepTowardsSuccessSelector extends ApplicationSelector {
    public Schema.SObjectType getSObjectType() {
		return Step_Towards_Success_Partner__c.sObjectType;
	}

    public List<Schema.SObjectField> getSObjectFieldList() {
		return new List<Schema.SObjectField> {
			Step_Towards_Success_Partner__c.Id
		};
	}

	/*
    public List<Step_Towards_Success_Partner__c> accPartnerStepByAccountId(Set<Id> ids) {
        Id accStepRecTypeId = Schema.SObjectType.Step_Towards_Success_Partner__c.getRecordTypeInfosByDeveloperName().get('Gym_Partner_Account_Step').getRecordTypeId();

        return (List<Step_Towards_Success_Partner__c>) Database.query(
				newQueryFactory().
				selectField(Step_Towards_Success_Partner__c.Id).
				selectField(Step_Towards_Success_Partner__c.Name).
				selectField(Step_Towards_Success_Partner__c.Step_Number__c).
				selectField(Step_Towards_Success_Partner__c.Achieved_New_Flow__c).
				selectField(Step_Towards_Success_Partner__c.Related_Account__c).
				setCondition('RecordTypeId = :accStepRecTypeId AND Related_Account__c IN :ids').
                setOrdering('Step_Towards_Success_Partner__c.Step_Number__c', fflib_QueryFactory.SortOrder.ASCENDING).
				toSOQL()
		);
    }
	*/

    public List<Step_Towards_Success_Partner__c> oppPartnerStepByOppId(Set<Id> ids) {
		Id oppStepRecTypeId = Schema.SObjectType.Step_Towards_Success_Partner__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Opportunity_Step').getRecordTypeId();

		return (List<Step_Towards_Success_Partner__c>) Database.query(
			newQueryFactory().
			selectField(Step_Towards_Success_Partner__c.Id).
			selectField(Step_Towards_Success_Partner__c.Name).
			selectField(Step_Towards_Success_Partner__c.Step_Number__c).
			selectField(Step_Towards_Success_Partner__c.Achieved_New_Flow__c).
			selectField(Step_Towards_Success_Partner__c.Related_Account__c).
			selectField(Step_Towards_Success_Partner__c.Related_Opportunity__c).
			selectField(Step_Towards_Success_Partner__c.Metadata_Developer_Name__c).
			setCondition('RecordTypeId = :oppStepRecTypeId AND Related_Opportunity__c IN :ids').
			setOrdering('Step_Number__c', fflib_QueryFactory.SortOrder.ASCENDING).
			toSOQL()
		);
	}
}