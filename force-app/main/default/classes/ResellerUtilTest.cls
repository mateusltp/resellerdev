@isTest
public with sharing class ResellerUtilTest {
    

    @isTest
    public static void testGetPicklistValuesByObjectApiNameAndFieldApiName(){

        Test.startTest();
        String retorno =  ResellerUtil.getPicklistValuesByObjectApiNameAndFieldApiName('Opportunity', 'StageName');
        System.assertNotEquals(null, retorno);
        Test.stopTest();

    }

    @isTest
    public static void testGetPicklistValuesMap(){

        Test.startTest();
        Map<String, String> retorno =  ResellerUtil.getPicklistValuesMap('Opportunity', 'StageName');
        System.assertNotEquals(null, retorno);
        Test.stopTest();

    }
}