/**
 * @description       : 
 * @author            : AlyssonMota
 * @group             : 
 * @last modified on  : 01-21-2021
 * @last modified by  : Alysson Mota
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   01-21-2021   Alysson Mota   Initial Version
**/
@IsTest
public class ContractCtrlTest {
    @testsetup
    static void Setup(){
        User u= new User(ProfileId = [SELECT Id FROM Profile WHERE Name LIKE '%admin%' ORDER BY CreatedDate LIMIT 1].Id,
                          LastName = 'last',
                          Email = 'puser000@amamama.com',
                          Username = 'puser000@amamama.com' + System.currentTimeMillis(),
                          CompanyName = 'TEST',
                          Title = 'title',
                          Alias = 'alias',
                          TimeZoneSidKey = 'America/Los_Angeles',
                          EmailEncodingKey = 'UTF-8',
                          LanguageLocaleKey = 'en_US',
                          LocaleSidKey = 'en_US',
                          UserRoleId = null);
        insert u;
        
        Product2 pro = new Product2(Name = 'test', Family = 'test');
        Insert pro;
        
        Pricebook2 standardPricebook = new Pricebook2(
            Id = Test.getStandardPricebookId(),
            IsActive = true
        );
        Update standardPricebook;
        
        PricebookEntry pbe = new PricebookEntry(
            Pricebook2Id = standardPricebook.Id,
            Product2Id = pro.Id,
            UnitPrice = 1020,
            IsActive = true
        );
        Insert pbe;
        
        Copay_Plan__c cp = new Copay_Plan__c();
        cp.Name = 'cp teste';
        cp.Custom__c = false;
        insert cp;
        
        Account account = new Account();
        account.Name = 'Account';
        account.BillingCountry = 'Brazil';
        account.BillingState = 'São Paulo';
        account.Id_Company__c = '82.898.430/0001-37';
        insert account;
                
        Opportunity opp = new Opportunity();
        opp.AccountId = account.Id;
        opp.Name = 'op test';
        opp.StageName = 'Qualification';
        opp.CloseDate = Date.today();
        opp.Pricebook2Id = standardPricebook.Id;
        opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Indirect Channel').getRecordTypeId();
        insert opp;
    	
        Quote quote1 = new Quote();
        quote1.Name = 'Test';
        quote1.Payment_method__c = 'Credit Card';
        quote1.Contact_Permission__c = 'Allowlist';
        quote1.Company_Tax_Reference__c = '82.898.430/0001-37';
        quote1.OpportunityId = opp.Id;
        quote1.Gym__c = cp.Id;
        quote1.Submission_Date__c = date.today();
        quote1.RecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByName().get('Direct Channel').getRecordTypeId();
        quote1.Status = 'Draft';
        quote1.License_Fee_Waiver__c = 'No';
        opp.SyncedQuoteId = quote1.Id;
        update opp;
        
        insert quote1;
        
        Opp.SyncedQuoteId = quote1.Id;
        update Opp;
        quote1.Status = 'Sent Influencer';
        update quote1;
        
        Quote quote2 = new Quote();
        quote2.Name = 'Test';
        quote2.Company_Tax_Reference__c = '82.898.430/0001-37';
        quote2.OpportunityId = opp.Id;
        quote2.Gym__c = cp.Id;
        quote2.Submission_Date__c = date.today();
        quote2.RecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByName().get('Direct Channel').getRecordTypeId();
        quote2.Status = 'Draft';
        quote2.License_Fee_Waiver__c ='No';
        //quote2.IsSyncing = true;
        insert quote2;
    }
    @IsTest
    static void Test1(){
        test.startTest();
        List<Quote> qt= [SELECT Id FROM Quote];
        ApexPages.currentPage().getParameters().put('id',qt[0].Id);
        ApexPages.currentPage().getParameters().put('fileFormat','doc');
        
        ApexPages.StandardController sc = new ApexPages.StandardController(qt[0]);
        ContractCtrl contrCtrl = new ContractCtrl(sc);
        
        contrCtrl.renderToWord();
        contrCtrl.render();
        
        ApexPages.currentPage().getParameters().put('fileFormat','pdf');
        ApexPages.StandardController sc1 = new ApexPages.StandardController(qt[1]);
        ContractCtrl contrCtrl1 = new ContractCtrl(sc1);
        
    }
    
}