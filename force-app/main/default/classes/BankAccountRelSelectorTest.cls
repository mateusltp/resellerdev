/**
 * @description       : 
 * @author            : bruno.mendes@gympass.com
 * @group             : 
 * @last modified on  : 26/04/2022
 * @last modified by  : bruno.mendes@gympass.com
**/

@IsTest
private class BankAccountRelSelectorTest {

    @TestSetup
    static void createData(){

        Account lAcc = PartnerDataFactory.newAccount();
        lAcc.Name = 'Parent Acct';
        Database.insert(lAcc);

        Bank_Account__c lBankAcct = PartnerDataFactory.newBankAcct();
        Database.insert(lBankAcct);

        Acount_Bank_Account_Relationship__c lBankAcctRel = PartnerDataFactory.newBankAcctRel(lAcc.Id, lBankAcct);
        Database.insert(lBankAcctRel);
    }

    /* Tests for BankAccountRelSelector */
    @isTest
    static void getSObjectFieldList_Test(){

        List<Schema.SObjectField> schemaLst = new List<Schema.SObjectField> {
                Acount_Bank_Account_Relationship__c.Id,
                Acount_Bank_Account_Relationship__c.Name
        };

        Test.startTest();
        System.assertEquals(schemaLst, new BankAccountRelSelector().getSObjectFieldList());
        Test.stopTest();
    }

    @isTest
    static void getSObjectType_Test(){

        Test.startTest();
        System.assertEquals(Acount_Bank_Account_Relationship__c.sObjectType, new BankAccountRelSelector().getSObjectType());
        Test.stopTest();
    }

    @IsTest
    static void selectById_Test() {
        Id bankAccountRelId = [SELECT Id FROM Acount_Bank_Account_Relationship__c LIMIT 1].Id;

        Test.startTest();
        List<Acount_Bank_Account_Relationship__c> acountBankAccountRelationships = new BankAccountRelSelector().selectById(new Set<Id> {bankAccountRelId});
        system.assertEquals(1, acountBankAccountRelationships.size());
        Test.stopTest();
    }

    @IsTest
    static void selectAccIdAndBankAccID_Test() {
        Id accountId = [SELECT Id FROM Account LIMIT 1].Id;

        Test.startTest();
        List<Acount_Bank_Account_Relationship__c> acountBankAccountRelationships = new BankAccountRelSelector().selectAccIdAndBankAccID(new Set<Id> {accountId});
        system.assertEquals(1, acountBankAccountRelationships.size());
        Test.stopTest();
    }

    /* Tests for AccountBankAccountSelector */
    @isTest
    static void getSObjectFieldList2_Test(){

        List<Schema.SObjectField> schemaLst = new List<Schema.SObjectField> {
                Acount_Bank_Account_Relationship__c.Id,
                Acount_Bank_Account_Relationship__c.Name
        };

        Test.startTest();
        System.assertEquals(schemaLst, new AccountBankAccountSelector().getSObjectFieldList());
        Test.stopTest();
    }

    @isTest
    static void getSObjectType2_Test(){

        Test.startTest();
        System.assertEquals(Acount_Bank_Account_Relationship__c.sObjectType, new AccountBankAccountSelector().getSObjectType());
        Test.stopTest();
    }

    @IsTest
    static void selectById2_Test() {
        Id bankAccountRelId = [SELECT Id FROM Acount_Bank_Account_Relationship__c LIMIT 1].Id;

        Test.startTest();
        List<Acount_Bank_Account_Relationship__c> acountBankAccountRelationships = new AccountBankAccountSelector().selectById(new Set<Id> {bankAccountRelId});
        system.assertEquals(1, acountBankAccountRelationships.size());
        Test.stopTest();
    }

    @IsTest
    static void selectBankAccToSetUp_Test() {
        Id accountId = [SELECT Id FROM Account LIMIT 1].Id;

        Test.startTest();
        List<Acount_Bank_Account_Relationship__c> acountBankAccountRelationships = new AccountBankAccountSelector().selectBankAccToSetUp(new Set<Id> {accountId});
        system.assertEquals(1, acountBankAccountRelationships.size());
        Test.stopTest();
    }

    @IsTest
    static void selectBankAccountByAccountId_Test() {
        Id accountId = [SELECT Id FROM Account LIMIT 1].Id;

        Test.startTest();
        Map<Id, Acount_Bank_Account_Relationship__c> acountBankAccountRelationships = new AccountBankAccountSelector().selectBankAccountByAccountId(new Set<Id> {accountId});
        system.assertEquals(1, acountBankAccountRelationships.size());
        Test.stopTest();
    }

    @IsTest
    static void selectAccountBankAccountRelationshipsByAccountIds_Test() {
        Id accountId = [SELECT Id FROM Account LIMIT 1].Id;

        Test.startTest();
        List<Acount_Bank_Account_Relationship__c> acountBankAccountRelationships = new AccountBankAccountSelector().selectAccountBankAccountRelationshipsByAccountIds(new Set<Id> {accountId});
        system.assertEquals(1, acountBankAccountRelationships.size());
        Test.stopTest();
    }


}