/**
 * @description       : 
 * @author            : alysson.mota@gympass.com
 * @group             : 
 * @last modified on  : 02-25-2022
 * @last modified by  : alysson.mota@gympass.com
**/
public with sharing class CommercialConditionTriggerHandler extends TriggerHandler{
    
    /**
    * Runs on after update event
    * @author alysson.mota@gympass.com | 02-25-2022 
    **/
    public override void afterUpdate(){
        new CommercialConditionService().evaluateAndSyncCommConditions((Map<Id, Commercial_Condition__c>)Trigger.newMap, (Map<Id, Commercial_Condition__c>)Trigger.oldMap);
    }
}