/**
 * @description       : 
 * @author            : roei@gft.com
 * @group             : 
 * @last modified on  : 03-19-2021
 * @last modified by  : Alysson Mota - alysson.mota@gympass.com
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   01-19-2021   roei@gft.com   Initial Version
**/
public without sharing class ClientSalesLeadAllocation {
    private Id gClientSalesRt = 
        Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Client').getRecordTypeId();
    private List< String > gLstSDRQueueName = 
        new List< String >{ 
            'LATAM_SDR' , 'US_SDR' ,
            'BR_SDR' , 'ES_SDR' , 'PT_SDR',
            'IT_SDR' , 'DE_SDR' , 'GB_SDR' };

    public void allocateSalesLead(){
        List< Lead > lLstSalesLead = new List< Lead >();

        for( Lead iLead : ( List< Lead > ) Trigger.new ){
            Boolean isSMB = isSMB(iLead);
            System.debug('isSMB: ' + isSMB);
            if(isSMB == false && iLead.RecordTypeId == gClientSalesRt){
                lLstSalesLead.add( iLead );
            }
        }

        allocateLeadToBDR( lLstSalesLead );
    }

    private void allocateLeadToBDR( List< Lead > aLstSalesLead ){
        if( aLstSalesLead.isEmpty() ){ return; }

        Map< String , Set< String > > lMapLeadFieldNValues = getLeadFieldNValues( aLstSalesLead );

        Map< String , String > lMapAccNameBDRId = getMapAccNameBDRId( lMapLeadFieldNValues.get( 'Company' ) );

        Map< String , String > lMapContactEmailBDRId = getMapContactEmailBDRId( lMapLeadFieldNValues.get( 'Email' ) );

        Map< String , String > lMapSDRQueueDevNameId = getMapSDRQueueDevNameId();
        
        Map< String , String > lMapContryCodeRegion = Utils.getMapContryCodeRegionFromMdt();

        List< Lead > lLstSalesLead = new List< Lead >();
        String lOwnerId;
        
        List<Lead> leadsToUpdate = new List<Lead>();
        
        List<Id> leadIdList = new List<Id>();
        
        for( Lead iLead : aLstSalesLead ){
            leadIdList.add(iLead.Id);
        }
        
        for( Lead iLead :  [SELECT CountryCode, Company, Email FROM Lead WHERE Id IN :leadIdList]){
            String countryCode = iLead.CountryCode != null ? iLead.CountryCode : Utils.getCountryCodeByCountry().get(Utils.capitalizeFully(iLead.Country));              
            System.debug('Country code ' + countryCode );

            lOwnerId = lMapSDRQueueDevNameId.get( countryCode + '_SDR' ); 
            if( lOwnerId == null ){ lOwnerId = lMapSDRQueueDevNameId.get( lMapContryCodeRegion.get( countryCode ) + '_SDR' ); }
            
            if( lOwnerId == null ){ continue; }
            
            iLead.OwnerId = lOwnerId;
            
            leadsToUpdate.add(iLead);
        }
        
        updateLeads(leadsToUpdate);
    }

    private Map< String , String > getMapSDRQueueDevNameId(){
        Map< String , String > lMapSDRQueueDevNameId = new Map< String , String >();

        for( Group iQueue : [ SELECT Id, DeveloperName FROM Group WHERE Type = 'Queue' AND DeveloperName =: gLstSDRQueueName ] ){
            lMapSDRQueueDevNameId.put( iQueue.DeveloperName , iQueue.Id );
        }

        lMapSDRQueueDevNameId.putAll(getCountryWithoutQueueMap( lMapSDRQueueDevNameId ));

        System.debug('map Queue ');
        System.debug(lMapSDRQueueDevNameId);

        return lMapSDRQueueDevNameId;
    }
    
    private Map< String , String > getMapAccNameBDRId( Set< String > aSetLeadCompanyName ){
        Map< String , String > lMapAccNameBDRId = new Map< String , String >();

        for( Account iAcc : [ SELECT Id, Name, BDR__c FROM Account WHERE Name =: aSetLeadCompanyName ] ){
            lMapAccNameBDRId.put( iAcc.Name , iAcc.BDR__c );
        }

        return lMapAccNameBDRId;
    }

    private Map< String , String > getMapContactEmailBDRId( Set< String > aSetLeadEmail ){
        Map< String , String > lMapContactEmailBDRId = new Map< String , String >();

        for( Contact iContact : [ SELECT Id, Email, Account.BDR__c FROM Contact WHERE Email =: aSetLeadEmail ] ){
            lMapContactEmailBDRId.put( iContact.Email , iContact.Account.BDR__c );
        }

        return lMapContactEmailBDRId;
    }
    
    private Map< String , Set< String > > getLeadFieldNValues( List< Lead > aLstSalesLead ){
        Map< String , Set< String > > lMapLeadFieldNValues = new Map< String , Set< String > >();
        Set< String > lLstLeadEmail = new Set< String >();
        Set< String > lLstLeadCompanyName = new Set< String >();
        Set< String > lLstLeadCountry = new Set< String >();

        for( Lead iSalesLead : aLstSalesLead ){
            lLstLeadEmail.add( iSalesLead.Email );
            lLstLeadCompanyName.add( iSalesLead.Company );
            lLstLeadCountry.add( iSalesLead.Country );
        }

        lLstLeadEmail.remove( null );
        lLstLeadCompanyName.remove( null );
        lLstLeadCountry.remove( null );

        lMapLeadFieldNValues.put( 'Email' , lLstLeadEmail );
        lMapLeadFieldNValues.put( 'Company' , lLstLeadCompanyName );
        lMapLeadFieldNValues.put( 'Country' , lLstLeadCountry );
        return lMapLeadFieldNValues;
    }

    private Map< String , String > getCountryWithoutQueueMap( Map< String , String > lMapSDRQueueDevNameId ){
        return new Map< String , String >{
            'IE_SDR' => lMapSDRQueueDevNameId.get( 'GB_SDR' )
        };
    }
    
    private Boolean isSMB(Lead lead) {     
        if (lead.Business_Unit__c == 'SMB' ) {
            return true;
        }
        return false;
    }
    
    private void updateLeads(List<Lead> leadsToUpdate) {
        if (!leadsToUpdate.isEmpty()) {
        	Database.SaveResult[] resultOppList = Database.update(leadsToUpdate);
            // Iterate through each returned result
            for (Database.SaveResult sr : resultOppList) {
                if (sr.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    System.debug('Successfully updated Account. Account ID: ' + sr.getId());
                }
                else {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Account fields that affected this error: ' + err.getFields());
                    }
                }
            }   
        }
    } 
}