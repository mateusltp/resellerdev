/**
 * @File Name          : ProgressBarFieldsController.cls
 * @Description        : 
 * @Author             : Samuel Silva - GFT (slml@gft.com)
 * @Group              : 
 * @Last Modified By   : Samuel Silva - GFT (slml@gft.com)
 * @Last Modified On   : 30/04/2020 15:56:27
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    30/04/2020   Samuel Silva - GFT (slml@gft.com)     Initial Version
**/
public without sharing class ProgressBarFieldsController {

    @AuraEnabled
    public static Double checkFieldsInObject(String recordId){  
        ProgressBarFieldsModel pgbr = new ProgressBarFieldsModel();
        return pgbr.checkFieldsInObject(recordId);
        
    }
}