@IsTest
public class LeadDTOTest {
    @IsTest
    public static void execute() {

      new LeadDTO().parseToSLead();
      List<String> expectedAccountErrorMessages = new List<String>();
      expectedAccountErrorMessages.add('client_id');
      expectedAccountErrorMessages.add('trade_name');
      expectedAccountErrorMessages.add('billing_id');
      expectedAccountErrorMessages.add('billing_firstname');
      expectedAccountErrorMessages.add('billing_lastname');
      expectedAccountErrorMessages.add('billing_email');
      expectedAccountErrorMessages.add('business_unit');
      expectedAccountErrorMessages.add('currency_id');
      expectedAccountErrorMessages.add('number_of_employees');
      expectedAccountErrorMessages.add('language');
      expectedAccountErrorMessages.add('contact');
      expectedAccountErrorMessages.add('sales_flow');
      expectedAccountErrorMessages.add('number_of_employees_rounded');
      List<String> actualAccountErrorMessages = new LeadDTO().getMissingRequiredFields();
      System.assertEquals(
        expectedAccountErrorMessages,
        actualAccountErrorMessages,
        'Not all validations have been done to the lead'
      );

      new LeadDTO().parseToSLead();
      List<String> expectedContactErrorMessages = new List<String>();
      expectedContactErrorMessages.add('contact_id');
      expectedContactErrorMessages.add('first_name');
      expectedContactErrorMessages.add('last_name');
      expectedContactErrorMessages.add('email');
      List<String> actualContactErrorMessages = new LeadDTO.ContactDTO().getMissingRequiredFields();
      System.assertEquals(
        expectedContactErrorMessages,
        actualContactErrorMessages,
        'Not all validations have been done to the contact'
      );

      new LeadDTO().parseToSLead();
      List<String> expectedSalesErrorMessages = new List<String>();
      expectedSalesErrorMessages.add('client_id');
      expectedSalesErrorMessages.add('trade_name');
      expectedSalesErrorMessages.add('billing_id');
      expectedSalesErrorMessages.add('billing_firstname');
      expectedSalesErrorMessages.add('billing_lastname');
      expectedSalesErrorMessages.add('billing_email');
      expectedSalesErrorMessages.add('business_unit');
      expectedSalesErrorMessages.add('currency_id');
      expectedSalesErrorMessages.add('number_of_employees');
      expectedSalesErrorMessages.add('language');
      expectedSalesErrorMessages.add('contact');
      expectedSalesErrorMessages.add('sales_flow');
      expectedSalesErrorMessages.add('number_of_employees_rounded');
      expectedSalesErrorMessages.add('legal_document');
      List<String> actualSalesErrorMessages = new LeadDTO().getMissingRequiredFieldsOnSalesCompleted();
      System.assertEquals(
        expectedSalesErrorMessages,
        actualSalesErrorMessages,
        'Not all validations have been done to the sales completed'
      );

    }
}