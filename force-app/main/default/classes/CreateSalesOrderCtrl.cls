public without sharing class CreateSalesOrderCtrl {
    @AuraEnabled
    public static Response createSalesOrder(String recordId) {
    	Id sObjectId = (Id)recordId;
        Id salesOrderId = null;
        
        Response response = new Response();
        
        if (sObjectId.getSobjectType() == Schema.Opportunity.SObjectType) {
            FieldsValidationCtrl.Response r = FieldsValidationCtrl.validateFields(recordId);
            
            for (FieldsValidationCtrl.ValidationError error : r.validationErrors) {
            	System.debug('******');
                System.debug(error.title);
            }
            
            if (r.validationErrors.size() > 0) {
               	response.status = 'ERROR';
                response.errorMessage = 'There are some missing information. Please refer to the Validation Errors section in this Opportunity page';
                return response;
            }
            
            Opportunity opp = getOpportunity(sObjectId);
            if (opp.StageName == 'Lançado/Ganho') {
           		Quote proposal = getProposal(sObjectId);
                salesOrderId = SalesOrderHelper.putSalesOrderForSMB(opp.AccountId, opp.Id, proposal.Id);
                response.status = 'SUCCESS';
                response.salesOrderId = salesOrderId;
            } else {
               	response.status = 'ERROR';
                response.errorMessage = 'Please change the Opportunity stage to Lauched/Won to create the Sales Order';
            }
        }
        
        return response;
    }
    
    private static Opportunity getOpportunity(Id oppId) {
    	Opportunity opp = [
            SELECT Id, AccountId, StageName, RecordType.DeveloperName
            FROM Opportunity
            WHERE Id =: oppId
        ];
        
        return opp;
    }
    
    private static Quote getProposal(Id oppId) {
        Quote proposal = [
        	SELECT Id
            FROM Quote
            WHERE OpportunityId =: oppId
          	AND IsSyncing = true
        ];
        
        return proposal;
    }
    
    public class Response {
        @auraEnabled public String status {get; set;}
        @auraEnabled public String salesOrderId {get; set;}
        @auraEnabled public String errorMessage {get; set;}
    }
}