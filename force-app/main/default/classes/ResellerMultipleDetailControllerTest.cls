@isTest
public class ResellerMultipleDetailControllerTest {
    @isTest
    public static void functionalTest() {

        Account account = new Account();
        account.RecordTypeId = '01241000000xyotAAA';
        account.Name = 'Reseller Test PDF';
        account.BillingState = 'São Paulo';
        account.BillingCountry = 'Brazil';
        account.Website = 'www.resellerTestPDF.com';
        account.Industry = 'Engineering';
        account.Razao_Social__c = 'Reseller Test PDF';
        account.GP_Status__c = 'Active';
        insert account;

        Massive_Account_Request__c massiveAccountRequest = new Massive_Account_Request__c();
        massiveAccountRequest.Reseller__c = account.Id;
        insert massiveAccountRequest;

        Account_Request__c accountRequest = new Account_Request__c();
        accountRequest.Name = 'New Company Test';
        accountRequest.Massive_Account_Request__c = massiveAccountRequest.Id;
        insert accountRequest;

        PageReference pageRef = Page.ResellerMultipleDetail;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id', String.valueOf(massiveAccountRequest.Id));
        ApexPages.StandardController sc = new ApexPages.StandardController(massiveAccountRequest);        

        ResellerMultipleDetailController testResellerMultipleDetail = new ResellerMultipleDetailController(sc);
        
        System.assertEquals(accountRequest.Name, 'New Company Test');


    }
}