/**
 * @description       : 
 * @author            : marcus.silva@gft.com
 * @group             : 
 * @last modified on  : 11-18-2021
 * @last modified by  : roei@gft.com
 * Modifications Log 
 * Ver   Date         Author         			Modification
 * 1.0   20-05-2021   marcus.silva@gft.com   Initial Version
**/
public with sharing class OpportunityTriggerOppRenegotiationHelper {
    
    public class ContractDataHandlerException extends Exception {}
    public class ContractDataObjectException extends Exception {}
    Set<Id> setOldProposalIds = new Set<Id>();
    Set<Id> setOldOpportunityIds = new Set<Id>();    
    Set<Id> setOldQuoteLineItemsIds = new Set<Id>();
    Set<Id> setOldEligibilityIds = new Set<Id>();
    Set<Id> setOldPaymentIds = new Set<Id>();
    Set<Id> setOldWaiverIds = new Set<Id>();
    Set<Id> setOldOperationalCaseIds = new Set<Id>();
    Set<Id> setMainOpportunityIds = new Set<Id>();
    List<Opportunity> lstMainOpportunity;
    List<Quote> allQuotes;
    Map<string, List<String>> mapObjectAndFieldsToTrack;
    List<ContractDataObject> allContractsToCompare;
    List<Assert_Data__c> allContractData;
    List<QuoteLineItem> allQuoteLineItems;
    List<Payment__c> allPayments;
    List<Eligibility__c> allEligibility;
    List<Waiver__c> allWaiver;
    List<Case> allOperationalCase;
    List<Opportunity> allOpportunity;
    
    public OpportunityTriggerOppRenegotiationHelper (List<Opportunity> lstMainOpportunity){
        
        this.lstMainOpportunity = lstMainOpportunity;
        this.mapObjectAndFieldsToTrack = this.getObjectAndFieldsToTrackChanges();
        this.loadSetIdsOfAllRelatedObjects();
        this.loadAllObjects();
        this.loadContracts();

        List<Assert_Data__c> ContractDataToSave = new List<Assert_Data__c>();
        for (ContractDataObject contrato : this.allContractsToCompare){
            contrato.createRecord(this.mapObjectAndFieldsToTrack);
            ContractDataToSave.addall(contrato.getallContractDataRecords());
        }

        insert ContractDataToSave;
    }       
    
    private Map<String, List<String>> getObjectAndFieldsToTrackChanges(){
        try {
            Map<string, List<String>> mapObjectFieldsToTrackChanges = new Map<string, List<String>>();

            List<Assert_Data_Objects__mdt> listSetupFieldsToTrackChanges = [SELECT Object_Name__c  , Field_Name__c FROM Assert_Data_Objects__mdt WHERE Opportunity__c = True];
            
            for (Assert_Data_Objects__mdt objContractDataObject: listSetupFieldsToTrackChanges){
                if (mapObjectFieldsToTrackChanges.containsKey(objContractDataObject.Object_Name__c)){
                        List<string> fieldListToTrack = mapObjectFieldsToTrackChanges.get(objContractDataObject.Object_Name__c);
                        fieldListToTrack.add(objContractDataObject.Field_Name__c.toLowerCase());
                        mapObjectFieldsToTrackChanges.put(objContractDataObject.Object_Name__c,fieldListToTrack);
                }else{
                        mapObjectFieldsToTrackChanges.put(objContractDataObject.Object_Name__c, new List<string>{objContractDataObject.Field_Name__c.toLowerCase()});
                }
            }
            if (listSetupFieldsToTrackChanges.isEmpty()){
                throw new ContractDataHandlerException('No records found to compare values in the process in function getObjectAndFieldsToTrackChanges'); 
            }else{
                return mapObjectFieldsToTrackChanges;
            }
        }
        catch(ContractDataHandlerException ex){
            throw new ContractDataHandlerException('Error to find the mapped objects and fields to process in function getObjectAndFieldsToTrackChanges.' + ex.getMessage() ); 
        }
    }

    private void LoadSetIdsOfAllRelatedObjects(){
        try{
            List< String > lLstOppRT = new List< String >{
                Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Success_Renegotiation').getRecordTypeId(),
                Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SMB_Success_Renegotiation').getRecordTypeId()
            };
            this.allContractsToCompare = new List<ContractDataObject>();
            List<Id> lstAccountId = new List<Id>();    

            for (Opportunity OppTrigger: lstMainOpportunity){
                if( lLstOppRT.contains( OppTrigger.RecordTypeId ) ){                    
                    lstAccountId.add(OppTrigger.AccountId);
                }
            }
            
            this.allOpportunity = ContractAgreementTriggerHelper.getLastClosedOppByAcc(lstAccountId);

            for (Opportunity OppTrigger: lstMainOpportunity){                
                this.setMainOpportunityIds.add(OppTrigger.Id);
                
                for (Opportunity oldOpp : allOpportunity) {
                    if(OldOpp.AccountId == OppTrigger.AccountId){
                        ContractDataObject contract = new ContractDataObject();
                        contract.opportunityId = OppTrigger.Id;
                        contract.accountId = OppTrigger.AccountId;
                        contract.oldProposalId = oldOpp.SyncedQuoteId;
                        contract.oldOpportunityId = oldOpp.Id;
                        this.allContractsToCompare.add(contract);
                    }
                }
            }

            for(Opportunity opp: allOpportunity){
                this.setOldOpportunityIds.add(opp.Id);
                this.setOldProposalIds.add(opp.SyncedQuoteId);
            }     
                   
            if (!this.AllContractsToCompare.isEmpty()){
                /* Load Eligibility , Payment and Quote_Line_Item set Ids */
                Set<Id> querySetId = this.setOldProposalIds;
                this.allEligibility = database.query(this.getsObjectQuery('eligibility__c', 'Payment__r.Quote_Line_Item__c, Quote_Line_Item__r.QuoteId, Payment__r.Quote_Line_Item__r.Fee_Type__c ', 'WHERE Payment__r.Quote_Line_Item__r.QuoteId IN : querySetId', ''));
            
                for (Eligibility__c eligibility: allEligibility){
                    this.setOldEligibilityIds.add(eligibility.Id);
                    this.setOldPaymentIds.add(eligibility.Payment__c);
                    this.setOldQuoteLineItemsIds.add(eligibility.Payment__r.Quote_Line_Item__c);
                }

                this.allWaiver = Database.query( this.getsObjectQuery( 'Waiver__c' , 'Payment__r.Quote_Line_Item__c, Quote_Line_Item__r.QuoteId, Payment__r.Quote_Line_Item__r.Fee_Type__c ' , 'WHERE Payment__r.Quote_Line_Item__r.QuoteId IN : querySetId' , '' ) );
                
                for (Waiver__c iWaiver: allWaiver){
                    this.setOldWaiverIds.add(iWaiver.Id);
                }

                this.allOperationalCase = 
                    Database.query( 'SELECT ' + Utils.getSobjectFieldsDevNameForQuery('Case') + ' FROM Case WHERE OpportunityId__c = : setOldOpportunityIds ' +
                        'AND Status = \'Approved\' AND RecordType.DeveloperName = \'Deal_Desk_Operational\' ORDER BY CreatedDate DESC LIMIT 1' );
                
                for( Case iCase : allOperationalCase ){
                    this.setOldOperationalCaseIds.add( iCase.Id );
                }
            }
        }
        catch(ContractDataHandlerException ex){
            throw new ContractDataHandlerException('Error in LoadSetIdsOfAllRelatedObjects. ' + ex.getMessage());
        }
    }

    private void loadAllObjects(){
        system.debug('mapObjectAndFieldsToTrack ' + this.mapObjectAndFieldsToTrack.keyset());
        for(String ObjectToTrack: this.mapObjectAndFieldsToTrack.keyset()){
            Set <Id> setIdsToQuery = new Set<Id>();
            try{
                switch on ObjectToTrack.toLowerCase() 
                {
                  /*  when 'eligibility__c', 'waiver' and 'opportunity__c' WAS FULLFILLED BEFORE */                      
                    when 'quote' {
                        setIdsToQuery= this.setOldProposalIds;
                        string query = this.generateQueryOfTrackObjects(ObjectToTrack.toLowerCase() , mapObjectAndFieldsToTrack.get(ObjectToTrack) ,'ExpirationDate, Opportunity.CreatedDate') ;                         
                        allQuotes = database.query(query += ': setIdsToQuery');
                        system.debug('AllOldQuotes: ' + allQuotes);
                    }
                    when 'quotelineitem' {
                        setIdsToQuery= setOldQuoteLineItemsIds;
                        string query = this.generateQueryOfTrackObjects(ObjectToTrack.toLowerCase() , MapObjectAndFieldsToTrack.get(ObjectToTrack) ,  'QuoteId, Fee_Type__c') ; 
                        system.debug('---query----: ' + query);
                        allQuoteLineItems = database.query(query += ': setIdsToQuery');
                        system.debug('AllOldQuoteLineItems: ' + allQuoteLineItems);
                    }
                    when 'payment__c' {
                        setIdsToQuery= this.setOldPaymentIds;
                        string query = this.generateQueryOfTrackObjects(ObjectToTrack.toLowerCase() , MapObjectAndFieldsToTrack.get(ObjectToTrack) ,  'Quote_Line_Item__r.QuoteId, Opportunity_Payment__c, Quote_Line_Item__r.Fee_Type__c' ) ; 
                        system.debug('qqq = '+ this.generateQueryOfTrackObjects(ObjectToTrack.toLowerCase() , MapObjectAndFieldsToTrack.get(ObjectToTrack) ,  'Quote_Line_Item__r.QuoteId, Opportunity_Payment__c'));
                        allPayments = database.query(query += ': setIdsToQuery');                        
                        system.debug('AllOldPayments = ' + allPayments);
                    }                              
                }
            }
            catch (ContractDataHandlerException ex){
                throw new ContractDataHandlerException('Error loading objects in loadAllObjects. '  + ex.getMessage());               
            }            
        }
    }
    
    private void loadContracts(){
        try{
            for (ContractDataObject contract : this.allContractsToCompare){
                for(Quote quote : allQuotes){
                    if (quote.Id == contract.oldProposalId){
                        contract.oldProposalList.add(quote);
                    }
                }

                for (QuoteLineItem quoteLineItem : this.allQuoteLineItems){
                    if ((quoteLineItem.QuoteId == contract.oldProposalId)){
                        contract.oldQuoteLineItemsList.add(quoteLineItem);
                    }
                }
                
                for(Opportunity opportunity : this.allOpportunity){
                    if (opportunity.id == contract.oldOpportunityId){
                        contract.oldOpportunityList.add(opportunity);
                    }
                }
                for(Payment__c payment : this.allPayments){
                    for (Quotelineitem quoteLineItem: contract.oldQuoteLineItemsList){
                        if (quoteLineItem.id == payment.Quote_Line_Item__c){
                            contract.oldPaymentList.add(payment);
                        }
                    }
                }
                for(Eligibility__c eligibility : this.allEligibility){
                    for (Payment__c payment:  contract.oldPaymentList){
                        if (eligibility.payment__c == payment.id ){
                            contract.oldEligibilityList.add(eligibility);
                        }
                    }
                }    
                
                for( Waiver__c iWaiver : this.allWaiver ){
                    for (Payment__c payment:  contract.oldPaymentList){
                        if (iWaiver.payment__c == payment.id ){
                            contract.oldWaiverList.add(iWaiver);
                        }
                    }
                }

                for( Case iOperationalCase : this.allOperationalCase ){
                    contract.oldOperationalCaseList.add( iOperationalCase );
                }  
            }
            if (!this.allContractsToCompare.isEmpty()){
            Set<Id> queryOppSetId = this.setMainOpportunityIds;
            this.allContractData = database.query(this.getsObjectQuery('Assert_Data__c' , 'WHERE Opportunity__c IN : queryOppSetId' ,''));   
            }
        }
        catch (ContractDataHandlerException ex){
            throw new ContractDataHandlerException('Error in loadContracts. ' + ex.getMessage());
        }        
    }

    private string generateQueryOfTrackObjects(String objectName,  List<String> fieldsToTrackChanges, string joinLink ){
      
        Map<String, Schema.SObjectType> mapSchemaObject = Schema.getGlobalDescribe();
        Schema.SObjectType schemaObject = mapSchemaObject.get(objectName);
        Schema.DescribeSObjectResult resultOfDescribe = schemaObject.getDescribe();
        Map<String,Schema.SObjectField> fieldsToAddInQuery = resultOfDescribe.fields.getMap();

        String listOfFieldsSeparatedByComma = '';
        try {
            for(String fieldFoundInObjectDescription : fieldsToAddInQuery.keyset()) 
            {                
                if (fieldsToTrackChanges.contains(fieldFoundInObjectDescription.toLowerCase())){
                    listOfFieldsSeparatedByComma += fieldFoundInObjectDescription.toLowerCase() + ' , ';
                }   
            }
            if (!String.isEmpty(listOfFieldsSeparatedByComma)){
                if (!String.isEmpty(joinLink)){
                    listOfFieldsSeparatedByComma = 'SELECT Id, ' + joinLink + ' , ' + listOfFieldsSeparatedByComma.left(listOfFieldsSeparatedByComma.length() - 2) + ' FROM ' + objectName + ' WHERE Id IN ';
                }else{
                    listOfFieldsSeparatedByComma = 'SELECT Id, ' + listOfFieldsSeparatedByComma.left(listOfFieldsSeparatedByComma.length() - 2) + ' FROM ' + objectName + ' WHERE Id IN ';
                }
            }
            return listOfFieldsSeparatedByComma;
        }
        catch(ContractDataHandlerException ex){
            throw new ContractDataHandlerException('Error generating query in generateQueryOfTrackObjects.'  + ex.getMessage());
        }
    }

    private String getsObjectQuery(string objectName, string whereClause , string ConditionClause){
        return 
            'SELECT ' + Utils.getSobjectFieldsDevNameForQuery(objectName) + 
            ' FROM ' + objectName + ' ' +
            whereClause + ' ' +
            ConditionClause ;
    }

    private String getsObjectQuery(string objectName, string relationalFields, string whereClause , string ConditionClause){        
        return 
            'SELECT ' + Utils.getSobjectFieldsDevNameForQuery(objectName) + ', ' + relationalFields + 
            ' FROM ' + objectName + ' ' +
            whereClause + ' ' +
            ConditionClause ;
    }

    public class ContractDataObject{        
        id opportunityId;
        id accountId;
        id oldProposalId;
        id oldOpportunityId;
        
        List<Opportunity> oldOpportunityList;
        List<Quote> oldProposalList;
        List<QuoteLineItem> oldQuoteLineItemsList;
        List<Payment__c> oldPaymentList;
        List<Eligibility__c> oldEligibilityList;
        List<Waiver__c> oldWaiverList;
        List<Case> oldOperationalCaseList;

        List<Assert_Data__c> ContractDataRecords;

        private List<Assert_Data__c> getallContractDataRecords(){
            return this.ContractDataRecords;
        }

        public ContractDataObject(){            
            oldOpportunityList = new List<Opportunity>();
            oldProposalList = new List<Quote>();
            oldQuoteLineItemsList = new List<QuoteLineItem>();
            oldPaymentList = new List<Payment__c>();
            oldEligibilityList = new List<Eligibility__c>();
            oldWaiverList = new List<Waiver__c>();
            oldOperationalCaseList = new List<Case>();
            ContractDataRecords = new List<Assert_Data__c>(); /* Holds the Assert_Data__c to be saved or updated */
        }
        private void createRecord(Map<string, List<String>> mapObjectAndFieldsToTrack){            
            for(string objectName : mapObjectAndFieldsToTrack.keyset() ){
                switch on objectName.toLowerCase(){
                    when 'quote' {                        
                        List<string> FieldsToQueryList = mapObjectAndFieldsToTrack.get(objectName);
                        system.debug('oldProposalList: ' + oldProposalList);
                        for (Quote proposal: this.oldProposalList){
                            for(string fieldname: FieldsToQueryList){                            
                                if(fieldName == 'Discount_Approval_Level__c'){                                    
                                    this.addContractDataObject('', objectName.toLowerCase(),fieldName ,'', String.valueOf(proposal.get(fieldName)) );
                                }else if(fieldName == 'orderExpirationDays'){
                                    this.addContractDataObject('', objectName.toLowerCase(),fieldName , proposal.ExpirationDate==null?String.valueOf(90):String.valueOf(proposal.Opportunity.CreatedDate.date().daysBetween(proposal.ExpirationDate)), ''  );
                                }else{
                                    this.addContractDataObject('', objectName.toLowerCase(),fieldName ,String.valueOf(proposal.get(fieldName)), '');
                                }
                            }
                        }
                    }                    
                    when 'opportunity'{
                        List<string> FieldsToQueryList = mapObjectAndFieldsToTrack.get(objectName);
                        for (Opportunity opportunity: this.oldOpportunityList){
                            for(string fieldName: FieldsToQueryList)  {
                                this.addContractDataObject('', objectName.toLowerCase(),fieldName ,String.valueOf(opportunity.get(fieldName)), '' );                                
                                system.debug('Object name: ' + objectName + '' + 'fieldName: ' + fieldName + '' + 'Field value: ' + String.valueOf(opportunity.get(fieldName)));                                
                            }                            
                        }                        
                    }
                    when 'quotelineitem'{
                        List<string> FieldsToQueryList = MapObjectAndFieldsToTrack.get(objectName);                        
                        for (Quotelineitem quoteLineItem: this.oldQuoteLineItemsList){
                            for(string fieldName: FieldsToQueryList)  {
                                //if(fieldName != 'Fee_Type__c'){
                                this.addContractDataObject(quoteLineItem.Fee_Type__c, objectName.toLowerCase(),fieldName ,String.valueOf(quoteLineItem.get(fieldName)),  '' );
                                //}                           
                            }
                        }
                    }
                    when 'payment__c'{
                        List<string> FieldsToQueryList = MapObjectAndFieldsToTrack.get(objectName);
                        for (Payment__c payment: this.oldPaymentList){
                            for(string fieldName: FieldsToQueryList){
                                //if(fieldName != 'Quote_Line_Item__r.Fee_Type__c'){
                                this.addContractDataObject(payment.Quote_Line_Item__r.Fee_Type__c, objectName.toLowerCase(),fieldName ,String.valueOf(payment.get(fieldName)),  '' );
                                //}
                        }
                        }                        
                    }
                    when 'eligibility__c'{
                        system.debug('oldEligibilityList: ' + oldEligibilityList);
                        List<string> FieldsToQueryList = MapObjectAndFieldsToTrack.get(objectName);
                        for (Eligibility__c eligibility: this.oldEligibilityList){
                            for(string fieldName: FieldsToQueryList)  {
                                //if(fieldName != 'Payment__r.Quote_Line_Item__r.Fee_Type__c'){
                                this.addContractDataObject(eligibility.Payment__r.Quote_Line_Item__r.Fee_Type__c, objectName.toLowerCase(),fieldName ,String.valueOf(eligibility.get(fieldName)),  '' );
                                //}
                        }    
                        }
                    }
                    when 'waiver__c'{
                        List<string> FieldsToQueryList = MapObjectAndFieldsToTrack.get(objectName);
                        
                        addWaiverMonthsContractDataObject( FieldsToQueryList );

                        for (Waiver__c iWaiver: this.oldWaiverList){
                            for(string fieldName: FieldsToQueryList){
                                this.addContractDataObject(iWaiver.Payment__r.Quote_Line_Item__r.Fee_Type__c, objectName.toLowerCase(),fieldName ,String.valueOf(iWaiver.get(fieldName)),  '' );                            
                            }    
                        }
                    }
                    when 'case' {
                        List<string> FieldsToQueryList = MapObjectAndFieldsToTrack.get(objectName);
                        for( Case iOperationalCase : this.oldOperationalCaseList ){
                            for( String fieldName : FieldsToQueryList )  {
                                this.addContractDataObject( '' , objectName.toLowerCase(), fieldName, String.valueOf( iOperationalCase.get(fieldName) ),  '' );
                            }    
                        }
                    }
                }
            }
        }

        private void addContractDataObject(string FeeType, string objectName, string fieldName, string oldValue, string approvedValue){            
            try {
                Assert_Data__c newContractDataRecord = new Assert_Data__c();
                newContractDataRecord.Opportunity__c = this.OpportunityId;
                newContractDataRecord.Object_Name__c = objectName.toLowerCase();
                newContractDataRecord.Field_Name__c = fieldName;
                newContractDataRecord.Old_Value__c =  (oldValue);
                newContractDataRecord.Approved_Value__c = (approvedValue);
                newContractDataRecord.Fee_Type__c = FeeType;

                this.ContractDataRecords.add(newContractDataRecord);
            } catch (ContractDataObjectException cEx) {
                throw new ContractDataObjectException('Unable to Add Contract Data records in Contract Data List in Opportunity Id ' + OpportunityId);
            }     
        }

        private void addWaiverMonthsContractDataObject( List< String > aLstFieldsToQuery ){
            Integer lIndexOfWaiverMonths = aLstFieldsToQuery.indexOf( 'number_of_months_after_launch__c' );

            if( lIndexOfWaiverMonths == -1 ){ return; }

            aLstFieldsToQuery.remove( lIndexOfWaiverMonths );

            Decimal lTotalWaiverMonths = 0;

            for( Waiver__c iWaiver : this.oldWaiverList ){
                lTotalWaiverMonths += ( iWaiver.Number_of_Months_after_Launch__c == null ? 0 : iWaiver.Number_of_Months_after_Launch__c );
            }

            this.addContractDataObject( 'Enterprise Subscription' , 'waiver__c' , 'number_of_months_after_launch__c' ,
                String.valueOf( lTotalWaiverMonths ) ,  '' );
        }
    }    
}