public without sharing class TagusTransactionData {
  private IntegrationRequest integration_request;

  public TagusTransactionData(SourceEventType sourceEventType) {
    this.integration_request = new IntegrationRequest(sourceEventType);
  }

  public Object getdata() {
    return this.integration_request.data;
  }

  public void setData(Object data) {
    this.integration_request.data = data;
  }

  private class IntegrationRequest {
    private Object data;
    private SourceEvent source_event;

    private IntegrationRequest(SourceEventType sourceEventType) {
      this.source_event = new SourceEvent(sourceEventType);
    }
  }

  private class SourceEvent {
    private SourceEventType type;
    private CreatedBy created_by_user;

    private SourceEvent(SourceEventType type) {
      this.type = type;
      this.created_by_user = new CreatedBy();
    }
  }

  private class CreatedBy {
    private String username;
    private String salesforce_id;
    private Datetime timestamp;

    private CreatedBy() {
      this.salesforce_id = UserInfo.getUserId();
      this.username = UserInfo.getUserName();
      this.timestamp = Datetime.now();
    }
  }

  public enum SourceEventType {
    CRUD
  }
}