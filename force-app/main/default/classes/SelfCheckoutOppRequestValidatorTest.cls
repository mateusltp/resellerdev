@IsTest
public class SelfCheckoutOppRequestValidatorTest {
    @IsTest
    public static void validatePostRequestTest() {
        String errorMessage;
        
        try {
            new SelfCheckoutOpportunityRequestValidator(null).validatePostRequest();
        } catch(IntegrationException error) {
            errorMessage = error.getMessage();
        }

        System.assertEquals(
            '(Request body cannot be empty)',
            errorMessage,
            'the body cannot be null'
        );
        
        try {
            new SelfCheckoutOpportunityRequestValidator(new SelfCheckoutOpportunityRequest()).validatePostRequest();
        } catch(IntegrationException error) {
            errorMessage = error.getMessage();
        }

        System.assertEquals(
            '(client field is required, opportunity field is required)',
            errorMessage,
            'the client and the opportunity fields must be required'
        );
        
        SelfCheckoutOpportunityRequest selfCheckoutOpportunityRequest = SelfCheckoutOpportunityRequestMock.getMockWithContactWithNoFields();
        
        try {
            new SelfCheckoutOpportunityRequestValidator(selfCheckoutOpportunityRequest).validatePostRequest();
        } catch(IntegrationException error) {
            errorMessage = error.getMessage();
        }
        
        List<String> expectedErrorMessages = new List<String>();

        expectedErrorMessages.add('The contact required field contact_id is missing');
        expectedErrorMessages.add('The contact required field first_name is missing');
        expectedErrorMessages.add('The contact required field last_name is missing');
        expectedErrorMessages.add('The contact required field email is missing');
        
        System.assertEquals(
            expectedErrorMessages.toString(),
            errorMessage,
            'Not all validations have been done'
        ); 
        
        selfCheckoutOpportunityRequest = SelfCheckoutOpportunityRequestMock.getMockWithClientWithoutTradeName();
                
        try {
            new SelfCheckoutOpportunityRequestValidator(selfCheckoutOpportunityRequest).validatePostRequest();
        } catch(IntegrationException error) {
            errorMessage = error.getMessage();
        }

        System.assertEquals(
            '(The client required field trade_name is missing)',
            errorMessage,
            'the trade_Name field on client must be required'
        );
        
    }
}