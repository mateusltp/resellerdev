public abstract without sharing class EnterpriseDealDAO {
  public static List<EnterpriseDealBO> getEnterpriseDealsByOpportunities(
    List<Opportunity> opportunities
  ) {
    if (opportunities == null || opportunities.isEmpty()) {
      return new List<EnterpriseDealBO>();
    }

    List<Quote> quotes = queryQuotesByOpportunities(opportunities);
    List<QuoteLineItem> quoteLineItems = queryQuoteLineItemsByQuotes(quotes);

    SearchEngine searchEngine = new SearchEngine();
    searchEngine.setOpportunities(
      queryOpportunitiesByOpportunities(opportunities)
    );
    searchEngine.setSplitBillings(
      querySplitBillingsByOpportunities(opportunities)
    );
    searchEngine.setQuotes(quotes);
    searchEngine.setQuoteLineItems(quoteLineItems);
    searchEngine.setPayments(queryPaymentsByQuoteLineItems(quoteLineItems));

    return getEnterpriseDeals(searchEngine);
  }

  private static List<Opportunity> queryOpportunitiesByOpportunities(
    List<Opportunity> opportunities
  ) {
    Map<String, Object> opportunityFields = (Map<String, Object>) Schema.SObjectType.Opportunity.fields.getMap();

    String soqlOpportunityFields = '';

    for (String opportunityFieldName : opportunityFields.keySet()) {
      if (opportunityFieldName == 'SyncedQuoteId') {
        continue;
      }

      soqlOpportunityFields += (opportunityFieldName + ',');
    }

    String soqlQuery =
      'SELECT RecordType.DeveloperName,' +
      soqlOpportunityFields.removeEnd(',') +
      ' FROM Opportunity WHERE Id IN :opportunities';

    return (List<Opportunity>) Database.query(soqlQuery.escapeJava());
  }

  private static List<Account_Opportunity_Relationship__c> querySplitBillingsByOpportunities(
    List<Opportunity> opportunities
  ) {
    Map<String, Object> splitBillingFields = (Map<String, Object>) Schema.SObjectType.Account_Opportunity_Relationship__c.fields.getMap();

    String soqlSplitBillingFields = '';

    for (String splitBillingFieldName : splitBillingFields.keySet()) {
      soqlSplitBillingFields += (splitBillingFieldName + ',');
    }

    String soqlQuery =
      'SELECT Account__r.UUID__c,' +
      soqlSplitBillingFields.removeEnd(',') +
      ' FROM Account_Opportunity_Relationship__c WHERE Opportunity__c IN :opportunities';

    return (List<Account_Opportunity_Relationship__c>) Database.query(
      soqlQuery.escapeJava()
    );
  }

  private static List<Quote> queryQuotesByOpportunities(
    List<Opportunity> opportunities
  ) {
    Map<String, Object> quoteFields = (Map<String, Object>) Schema.SObjectType.Quote.fields.getMap();

    String soqlQuoteFields = '';

    for (String quoteFieldName : quoteFields.keySet()) {
      soqlQuoteFields += (quoteFieldName + ',');
    }

    String soqlQuery =
      'SELECT ' +
      soqlQuoteFields.removeEnd(',') +
      ' FROM Quote WHERE OpportunityId IN :opportunities';

    return (List<Quote>) Database.query(soqlQuery.escapeJava());
  }

  private static List<QuoteLineItem> queryQuoteLineItemsByQuotes(
    List<Quote> quotes
  ) {
    Map<String, Object> quoteLineItemFields = (Map<String, Object>) Schema.SObjectType.QuoteLineItem.fields.getMap();

    String soqlQuoteLineItemFields = '';

    for (String quoteLineItemFieldName : quoteLineItemFields.keySet()) {
      soqlQuoteLineItemFields += (quoteLineItemFieldName + ',');
    }

    String soqlQuery =
      'SELECT ' +
      soqlQuoteLineItemFields.removeEnd(',') +
      ' FROM QuoteLineItem WHERE QuoteId IN :quotes';

    return (List<QuoteLineItem>) Database.query(soqlQuery.escapeJava());
  }

  private static List<Payment__c> queryPaymentsByQuoteLineItems(
    List<QuoteLineItem> quoteLineItems
  ) {
    Map<String, Object> paymentFields = (Map<String, Object>) Schema.SObjectType.Payment__c.fields.getMap();
    Map<String, Object> eligibilityFields = (Map<String, Object>) Schema.SObjectType.Eligibility__c.fields.getMap();
    Map<String, Object> waiverFields = (Map<String, Object>) Schema.SObjectType.Waiver__c.fields.getMap();

    String soqlPaymentFields = '';

    for (String paymentFieldName : paymentFields.keySet()) {
      soqlpaymentFields += (paymentFieldName + ',');
    }

    String soqlQuery =
      'SELECT ' +
      soqlpaymentFields +
      '(SELECT Paid_By__r.UUID__c,Managed_By__r.UUID__c,';

    String soqlEligibilityFields = '';

    for (String eligibilityFieldName : eligibilityFields.keySet()) {
      soqlEligibilityFields += (eligibilityFieldName + ',');
    }

    soqlQuery +=
      soqlEligibilityFields.removeEnd(',') + ' FROM Eligibility__r),(SELECT ';

    String soqlWaiverFields = '';

    for (String waiverFieldName : waiverFields.keySet()) {
      soqlWaiverFields += (waiverFieldName + ',');
    }

    soqlQuery +=
      soqlWaiverFields.removeEnd(',') +
      ' FROM Waivers__r)' +
      ' FROM Payment__c WHERE Quote_Line_Item__c IN :quoteLineItems';

    return (List<Payment__c>) Database.query(soqlQuery.escapeJava());
  }

  private static List<EnterpriseDealBO> getEnterpriseDeals(
    SearchEngine searchEngine
  ) {
    List<EnterpriseDealBO> enterpriseDeals = new List<EnterpriseDealBO>();

    for (Opportunity opportunity : searchEngine.opportunities) {
      if (!searchEngine.quotesByOpportunityId.containsKey(opportunity.Id)) {
        continue;
      }

      for (
        Quote quote : searchEngine.quotesByOpportunityId.get(opportunity.Id)
      ) {
        EnterpriseDealBO enterpriseDeal = new EnterpriseDealBO(
          opportunity,
          quote
        );

        if (
          searchEngine.splitBillingsByOpportunityId.containsKey(opportunity.Id)
        ) {
          enterpriseDeal.setSplitBilling(
            searchEngine.splitBillingsByOpportunityId.get(opportunity.Id)
          );
        }

        if (searchEngine.quoteLineItemsByQuoteId.containsKey(quote.Id)) {
          for (
            QuoteLineItem quoteLineItem : searchEngine.quoteLineItemsByQuoteId.get(
              quote.Id
            )
          ) {
            EnterpriseDealBO.SalesItem salesItem = new EnterpriseDealBO.SalesItem(
              quoteLineItem
            );

            if (
              searchEngine.paymentsByQuoteLineItemId.containsKey(
                quoteLineItem.Id
              )
            ) {
              for (
                Payment__c payment : searchEngine.paymentsByQuoteLineItemId.get(
                  quoteLineItem.Id
                )
              ) {
                salesItem.addPaymentItem(
                  payment,
                  payment.Eligibility__r,
                  payment.Waivers__r
                );
              }
            }

            enterpriseDeal.addSalesItem(salesItem);
          }
        }

        enterpriseDeals.add(enterpriseDeal);
      }
    }

    return enterpriseDeals;
  }

  private class SearchEngine {
    private List<Opportunity> opportunities;
    private Map<Id, List<Account_Opportunity_Relationship__c>> splitBillingsByOpportunityId;
    private Map<Id, List<Quote>> quotesByOpportunityId;
    private Map<Id, List<QuoteLineItem>> quoteLineItemsByQuoteId;
    private Map<Id, List<Payment__c>> paymentsByQuoteLineItemId;

    private SearchEngine() {
      this.opportunities = new List<Opportunity>();
      this.quotesByOpportunityId = new Map<Id, List<Quote>>();
      this.quoteLineItemsByQuoteId = new Map<Id, List<QuoteLineItem>>();
      this.paymentsByQuoteLineItemId = new Map<Id, List<Payment__c>>();
    }

    private void setOpportunities(List<Opportunity> opportunities) {
      this.opportunities = opportunities;
    }

    private void setSplitBillings(
      List<Account_Opportunity_Relationship__c> splitBillings
    ) {
      this.splitBillingsByOpportunityId = getMapSplitBillingsByOpportunityId(
        splitBillings
      );
    }

    private Map<Id, List<Account_Opportunity_Relationship__c>> getMapSplitBillingsByOpportunityId(
      List<Account_Opportunity_Relationship__c> splitBillings
    ) {
      Map<Id, List<Account_Opportunity_Relationship__c>> splitBillingsByOpportunityId = new Map<Id, List<Account_Opportunity_Relationship__c>>();

      for (Account_Opportunity_Relationship__c splitBilling : splitBillings) {
        if (
          splitBillingsByOpportunityId.containsKey(splitBilling.Opportunity__c)
        ) {
          splitBillingsByOpportunityId.get(splitBilling.Opportunity__c)
            .add(splitBilling);
        } else {
          splitBillingsByOpportunityId.put(
            splitBilling.Opportunity__c,
            new List<Account_Opportunity_Relationship__c>{ splitBilling }
          );
        }
      }
      return splitBillingsByOpportunityId;
    }

    private void setQuotes(List<Quote> quotes) {
      this.quotesByOpportunityId = getMapQuotesByOpportunityId(quotes);
    }

    private Map<Id, List<Quote>> getMapQuotesByOpportunityId(
      List<Quote> quotes
    ) {
      Map<Id, List<Quote>> quotesByOpportunityId = new Map<Id, List<Quote>>();

      for (Quote quote : quotes) {
        if (quotesByOpportunityId.containsKey(quote.OpportunityId)) {
          quotesByOpportunityId.get(quote.OpportunityId).add(quote);
        } else {
          quotesByOpportunityId.put(
            quote.OpportunityId,
            new List<Quote>{ quote }
          );
        }
      }

      return quotesByOpportunityId;
    }

    private void setQuoteLineItems(List<QuoteLineItem> quoteLineItems) {
      this.quoteLineItemsByQuoteId = getMapQuoteLineItemsByQuoteId(
        quoteLineItems
      );
    }

    private Map<Id, List<QuoteLineItem>> getMapQuoteLineItemsByQuoteId(
      List<QuoteLineItem> quoteLineItems
    ) {
      Map<Id, List<QuoteLineItem>> quoteLineItemsByQuoteId = new Map<Id, List<QuoteLineItem>>();

      for (QuoteLineItem quoteLineItem : quoteLineItems) {
        if (quoteLineItemsByQuoteId.containsKey(quoteLineItem.QuoteId)) {
          quoteLineItemsByQuoteId.get(quoteLineItem.QuoteId).add(quoteLineItem);
        } else {
          quoteLineItemsByQuoteId.put(
            quoteLineItem.QuoteId,
            new List<QuoteLineItem>{ quoteLineItem }
          );
        }
      }

      return quoteLineItemsByQuoteId;
    }

    private void setPayments(List<Payment__c> payments) {
      this.paymentsByQuoteLineItemId = getMapPaymentsByQuoteLineItemId(
        payments
      );
    }

    private Map<Id, List<Payment__c>> getMapPaymentsByQuoteLineItemId(
      List<Payment__c> payments
    ) {
      Map<Id, List<Payment__c>> paymentsByQuoteLineItemId = new Map<Id, List<Payment__c>>();

      for (Payment__c payment : payments) {
        if (paymentsByQuoteLineItemId.containsKey(payment.Quote_Line_Item__c)) {
          paymentsByQuoteLineItemId.get(payment.Quote_Line_Item__c)
            .add(payment);
        } else {
          paymentsByQuoteLineItemId.put(
            payment.Quote_Line_Item__c,
            new List<Payment__c>{ payment }
          );
        }
      }

      return paymentsByQuoteLineItemId;
    }
  }
}