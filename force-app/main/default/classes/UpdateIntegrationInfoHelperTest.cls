@isTest
public class UpdateIntegrationInfoHelperTest {
    
	@isTest
    public static void testSuccessIntegrationSalesOrder() {
        Sales_Order__c so = new Sales_Order__c();
        insert so;
        
        Integration_Request__c ir = new Integration_Request__c();
        ir.SObject_type__c = 'Sales_Order__c';
        ir.SObject_ID__c = String.valueOf(so.Id);
        ir.Integration_Request_Status__c = 'success';
        insert ir;
        
        Sales_Order__c soUpdated = [
        	SELECT Id, Integration_Request__c, Integration_Status__c
            FROM Sales_Order__c
            WHERE Id =: so.Id
        ];
        
        //System.assertEquals('Integrated', soUpdated.Integration_Status__c);
        //System.assertEquals(ir.Id, soUpdated.Integration_Request__c);
    }
    
    @isTest
    public static void testErrorIntegrationSalesOrder() {
        Sales_Order__c so = new Sales_Order__c();
        insert so;
        
        Integration_Request__c ir = new Integration_Request__c();
        ir.SObject_type__c = 'Sales_Order__c';
        ir.SObject_ID__c = String.valueOf(so.Id);
        ir.Integration_Request_Status__c = 'error';
        insert ir;
        
        Sales_Order__c soUpdated = [
        	SELECT Id, Integration_Request__c, Integration_Status__c
            FROM Sales_Order__c
            WHERE Id =: so.Id
        ];
        
        //System.assertEquals('Error', soUpdated.Integration_Status__c);
        //System.assertEquals(ir.Id, soUpdated.Integration_Request__c);
    }
    
   	@isTest
    public static void testMoreThanOneRecord() {
        Sales_Order__c so1 = new Sales_Order__c();
        insert so1;
        
        Sales_Order__c so2 = new Sales_Order__c();
        insert so2;
        
        List<Id> irIdList = new List<Id>();
        
        Integration_Request__c ir1 = new Integration_Request__c();
        ir1.SObject_type__c = 'Sales_Order__c';
        ir1.SObject_ID__c = String.valueOf(so1.Id);
        ir1.Integration_Request_Status__c = 'error';
        insert ir1;
        irIdList.add(ir1.Id);
        
        Integration_Request__c ir2 = new Integration_Request__c();
        ir2.SObject_type__c = 'Sales_Order__c';
        ir2.SObject_ID__c = String.valueOf(so2.Id);
        ir2.Integration_Request_Status__c = 'error';
        insert ir2;
        irIdList.add(ir2.Id);
        
    	UpdateIntegrationInfoHelper.updateIntegrationInfo(irIdList);
    }
}