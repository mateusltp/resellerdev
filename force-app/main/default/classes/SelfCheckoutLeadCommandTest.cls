@isTest
public class SelfCheckoutLeadCommandTest {
    
    @TestSetup
    static void makeData() {

        EventConfiguration__c eventConfig = new EventConfiguration__c(
          name = 'SELF_CHECKOUT_LEAD_INBOUND',
          commandClassName__c = 'SelfCheckoutLeadCommand'
        );
        insert eventConfig;
    }
    
    @isTest
    private static void executeTest() {
        Test.startTest();
        
        SelfCheckoutLeadRequest selfCheckoutLeadRequest = SelfCheckoutLeadRequestMock.getMock();
        
        EventQueue event = new SelfCheckoutLeadPublisher().publish(selfCheckoutLeadRequest);
        
        Test.stopTest();
    }
    
}