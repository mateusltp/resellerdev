/*
* @author: Patrick Goia
* @date: February/2019
* @description: Controller to all Contracts
*/
public class ContractCtrl {
    public Quote quote {get; set;}
    public Copay_Plan__c copayPlan {get; set;}
    public String renderAs {get; set;}
    public String contentType {get; set;}
    public string renderWORD{get; set;}
    public boolean show{get; set;}
    
    public void renderToWord() {
        this.show = false;
        this.renderWORD = 'application/msword#'+ quote.Account.Name +'.doc';
    }
    public void render() {
        this.renderAs = 'pdf';
    }
    
    public ContractCtrl(ApexPages.StandardController controller) {   
        quote = new Quote();
        
        if (ApexPages.currentPage().getParameters().get('id') != null) {   
            List<Quote> listQuotes = new List<Quote>();
            
            listQuotes = [SELECT Id, Name, Membership_Fee_cut_off_date__c,Waiver_Termination_Date__c,Number_of_Subsidiaries__c, Account.Id_Company__c, License_Fee_cut_off_date__c ,Account.Name,Contact.Name,
                          quote.Contact.Cargo__c,TotalPrice, BillingCountry, BillingPostalCode, BillingState, BillingCity, BillingStreet,CreatedDate,CurrencyIsoCode,Additional_Considerations__c,
                          Administrator_E_mail__c, Administrator_Name__c, Administrator_Phone__c, Approved_Legal_Validations__c, Are_Family_Members_Included2__c, CAP_Payment__c, GP_Country__c,
                          Company_Name_as_registered__c, Company_Tax_Reference__c, Contact_Permission__c, Gym__c, Employee_Company_ID__c, Employee_Corporate_Email__c, Employee_Name__c, 
                          Employee_National_ID__c, Employee_National_ID_if_applicable__c, Exclusivity__c, Finance_E_mail__c, Finance_Name__c, Finance_Phone__c, Fixed_daily_pass_payment__c, 
                          Generate_PDF__c, Grouped_Payment_method__c, Gympass_Collections_E_mail__c, Gympass_Legal_E_mail__c, Gympass_Relationship_E_mail__c, Is_Deletable__c, License_Fee__c, 
                          License_Fee_Frequency__c, License_Fee_PO_Duration__c, valor_maximo_da_mensalidade_starter_c__c, Market_Value_Gold_Plus__c,Administrator__r.Name, Administrator__r.Phone, Administrator__r.Email, Operations__r.Name, Operations__r.Phone, Operations__r.Email, Finance__r.Name, Finance__r.Phone, Finance__r.Email,
                          Membership_Fee_PO_Duration__c,Unique_Identifier__c ,Gympass_responsible_to_sign_the_contract__c, Minimum_commitment__c, 
                          Monthly_employee_contribution_Starter__c, Monthly_Fee_Gold_Plus__c, Number_of_Additional_Subsidiaries__c,Eligible_Employees__c, Number_of_Family_Members__c, 
                          Number_of_Exceptions__c,Number_of_Gyms_Black__c, Number_of_Gyms_Gold__c, Number_of_Gyms_Gold_Plus__c, Number_of_Gyms_Platinum__c, Number_of_Gyms_Silver__c, 
                          Number_of_Gyms_Starter__c, Numero_de_Vidas__c, Numero_de_academias_basicI__c, Numero_de_academias_basicII__c, Operations_E_mail__c, Operations_Name__c, 
                          Operations_Phone__c, Other_Comments__c, Other_Conditions__c, Other_Conditions_i_e_Pilot__c, Payment_Method_License_Fee__c, Payment_Structure__c, 
                          Payment_Terms_for_License_Fee__c,Payment_method__c, Payment_Terms_for_Payroll_Debit_Memo__c, Payment_Terms_for_Setup_Fee__c, Pending_Legal_Validations__c, 
                          MB_PO_Number_Required__c, LF_PO_Number_Required__c, Prepayment__c, Prepayment_commitment_amount__c, Proposal_Won__c, quote_PON__c, Region__c, Send_for_Signature__c, 
                          Send_to_Legal_Validation__c, Send_to_Prospect__c, Setup_Fee__c, Standard_gyms_payment__c, Submission_Date__c, Subsidiary_Administrator_E_mail__c, 
                          Subsidiary_Administrator_Name__c, Subsidiary_Administrator_Phone__c, Subsidiary_Finance_E_mail__c, Subsidiary_Finance_Name__c, Subsidiary_Finance_Phone__c, 
                          Subsidiary_Number_of_Eligible_Employees__c, Subsidiary_Operations_E_mail__c, Subsidiary_Operations_Name__c, Subsidiary_Operations_Phone__c, 
                          Taxa_paga_pelos_colaboradores_basicI__c, Taxa_paga_pelos_colaboradores_basicII__c, Taxa_paga_pelos_colaboradores_Black__c, Taxa_paga_pelos_colaboradores_gold__c, 
                          Taxa_paga_pelos_colaboradores_platinum__c, Taxa_paga_pelos_colaboradores_silver__c, Valor_maximo_da_mensalidade_basicI__c, Valor_maximo_da_mensalidade_basicII__c, 
                          Valor_maximo_da_mensalidades_Black__c, Valor_maximo_da_mensalidade_gold__c, Valor_maximo_da_mensalidade_platinum__c, Valor_maximo_da_mensalidade_silver__c,
                          Monthly_Fee_Family_Members_Basic_I__c, Monthly_Fee_Family_Members_Basic_II__c, Monthly_Fee_Family_Members_Black__c, Monthly_Fee_Family_Members_Gold__c, 
                          Monthly_Fee_Family_Members_Gold_Plus__c, Monthly_Fee_Family_Members_Platinum__c, Monthly_Fee_Family_Members_Silver__c, Monthly_Fee_Family_Members_Starter__c,
                          (SELECT Proposal__c , Subsidiary__r.Id, Subsidiary__r.Name, Subsidiary__r.Subsidiary_Administrator_E_mail__c, Subsidiary__r.Subsidiary_Administrator_Name__c, Subsidiary__r.Subsidiary_Administrator_Phone__c, 
                           Subsidiary__r.Subsidiary_Finance_E_mail__c, Subsidiary__r.Subsidiary_Finance_Name__c,Subsidiary__r.CNPJ__c,Subsidiary__r.Billing_Address__c, Subsidiary__r.Subsidiary_Finance_Phone__c, Subsidiary__r.Subsidiary_Number_of_Eligible_Employees__c, Subsidiary__r.Subsidiary_Operations_E_mail__c, 
                           Subsidiary__r.Subsidiary_Operations_Name__c, Subsidiary__r.Subsidiary_Operations_Phone__c FROM ProposalSubsidiaries__r
                          )
                          FROM Quote 
                          WHERE Id =: ApexPages.currentPage().getParameters().get('id')
                          LIMIT 1];
            
            if (listQuotes.size() > 0) {
                quote = listQuotes[0];
                
                if (quote != null && quote.Gym__c != null) {
                    if (ApexPages.currentPage().getParameters().get('fileFormat') == 'doc')
                        contentType = 'application/msword#'+ quote.Account.Name +'.doc';
                    else
                        renderAs = 'pdf';
                    
                    copayPlan = [SELECT Market_Value_Basic_I__c, Market_Value_Basic_II__c, Market_Value_Black__c, Market_Value_Gold__c, Market_Value_Gold_Plus__c, Market_Value_Platinum__c, Market_Value_Silver__c, Market_Value_Starter__c, Monthly_Fee_Basic_I__c, Monthly_Fee_Basic_II__c, Monthly_Fee_Black__c, Monthly_Fee_Family_Members_Basic_I__c, Monthly_Fee_Family_Members_Basic_II__c, Monthly_Fee_Family_Members_Black__c, Monthly_Fee_Family_Members_Gold__c, Monthly_Fee_Family_Members_Gold_Plus__c, Monthly_Fee_Family_Members_Platinum__c, Monthly_Fee_Family_Members_Silver__c, Monthly_Fee_Family_Members_Starter__c, Monthly_Fee_Gold__c, Monthly_Fee_Gold_Plus__c, Monthly_Fee_Platinum__c, Monthly_Fee_Silver__c, Monthly_Fee_Starter__c, Number_of_Gyms_Basic_I__c, Number_of_Gyms_Basic_II__c, Number_of_Gyms_Black__c, Number_of_Gyms_Gold__c, Number_of_Gyms_Gold_Plus__c, Number_of_Gyms_Platinum__c, Number_of_Gyms_Silver__c, Number_of_Gyms_Starter__c, Valid__c
                                 FROM Copay_Plan__c
                                 WHERE Id =: quote.Gym__c];
                }
            }
        }
    }
}