/**
* @author Guilherme Togniolo
* @date 20220330
* @version 1.00
* @description Test class for ContentDocumentLinkTriggerHandlerNew called from 
*     ContentDocumentLinkAfterInsert and ContentDocumentLinkAfterUpdate Trigger
*/

@isTest
public class ContentDocumentLinkTriggerHandlerNewTest {
    @TestSetup
    static void createData(){
        Account lAcc = DataFactory.newAccount();
        Database.insert( lAcc );
        
        Pricebook2 lStandardPB = DataFactory.newPricebook();
        
        Opportunity lOpp = DataFactory.newOpportunity( lAcc.Id , lStandardPB , 'Client_Sales_New_Business');
        Database.insert( lOpp );
        
        Quote lQuote = DataFactory.newQuote(lOpp, ' Quote Test' , 'Client_Sales_New_Business');
        Database.insert( lQuote );
        
        APXT_Redlining__Contract_Agreement__c lCttAgreem =  DataFactory.newContractAgreement(lOpp.Id , lQuote.Id, lAcc.Id);        
        lCttAgreem.Template_Selection__c = 'Brazil';   
        lCttAgreem.RecordTypeId = Schema.SObjectType.APXT_Redlining__Contract_Agreement__c.getRecordTypeInfosByDeveloperName().get('B2B_Contract').getRecordTypeId();
        Database.insert( lCttAgreem );

        Case legalCase = new Case();
            legalCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Contract Review').getRecordTypeId();
            legalCase.status = 'In legal review'; 
            legalCase.ContractAgreement__c = lCttAgreem.Id;
            legalCase.OwnerId = UserInfo.getUserId();             
        insert legalCase; 
        
        ContentVersion lCV = new ContentVersion();
        lCV.PathOnClient = 'any name.pdf';
        lCV.Title = 'any name';
        lCV.VersionData = Blob.valueOf('pdfBody');
        insert lCV;        
    }
    
    
    @isTest
    public static void ContentDocumentLinkTriggerHandlerNewTestInsert() {

        Opportunity lOpp = [SELECT Id From Opportunity Limit 1];
        Account lAcc = [SELECT Id FROM Account Limit 1];
        Quote lQuote = [SELECT Id FROM Quote Limit 1];
        APXT_Redlining__Contract_Agreement__c lCttAgreem =  [SELECT Id FROM APXT_Redlining__Contract_Agreement__c Limit 1];
        ContentVersion lCV = [SELECT ContentDocumentId FROM ContentVersion Limit 1];

        test.startTest();

        ContentDocumentLink cdl = new ContentDocumentLink();

        cdl.LinkedEntityId = lCttAgreem.Id;
        cdl.ContentDocumentId = lCV.ContentDocumentId;
        cdl.ShareType = 'V';
        insert cdl;

        List<ContentDocumentLink> result = [SELECT Id, ShareType FROM ContentDocumentLink WHERE ContentDocumentId = :lCV.ContentDocumentId Limit 1];

        System.assert(result.size() > 0);

        test.stopTest();
    }

    @isTest
    public static void ContentDocumentLinkTriggerHandlerNewTestUpdate() {

        APXT_Redlining__Contract_Agreement__c lCttAgreem =  [SELECT Id FROM APXT_Redlining__Contract_Agreement__c Limit 1];
        ContentVersion lCV = [SELECT ContentDocumentId FROM ContentVersion Limit 1];

        ContentDocumentLink cdl = new ContentDocumentLink();

        cdl.LinkedEntityId = lCttAgreem.Id;
        cdl.ContentDocumentId = lCV.ContentDocumentId;
        cdl.ShareType = 'I';
        insert cdl;
       
        test.startTest();

        update cdl;

        List<ContentDocumentLink> result = [SELECT Id, ShareType FROM ContentDocumentLink WHERE ContentDocumentId = :lCV.ContentDocumentId Limit 1];

        System.assert(result.size() > 0);

        test.stopTest();
    }
}