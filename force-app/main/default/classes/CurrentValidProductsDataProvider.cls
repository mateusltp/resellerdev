/**
 * @description       : IT-2185: Data provider class for CurrentValidProducts Sortable Data Grid
 * @author            : bruno.mendes@gympass.com
 * @group             :
 * @last modified on  : 10-01-2022
 * @last modified by  : bruno.mendes@gympass.com
 */

global with sharing class CurrentValidProductsDataProvider implements sortablegrid.sdgIDataProvider
{
    global String className = CurrentValidProductsDataProvider.class.getName();
    global PS_Constants constants = PS_Constants.getInstance();

    global static Boolean isUserSelectable()
    {
        return true;
    }

    global sortablegrid.SDGResult getData(sortablegrid.SDG coreSDG,  sortablegrid.SDGRequest request)
    {
        sortablegrid.SDGResult result = new sortablegrid.SDGResult();
        System.debug('result >>>'+result);

        result.data = new List<Sobject>();
        System.debug('result data'+result.data);

        List<Product_Opportunity_Assignment__c> prodOppAssLst = new List<Product_Opportunity_Assignment__c>();
        try {
            AccountOpportunitySelector accOppSelector = (AccountOpportunitySelector)Application.Selector.newInstance(Account_Opportunity_Relationship__c.sobjectType);

            Map<Id, Id> opportunityIdByAccountId = accOppSelector.selectLastWonOpportunityIdByAccountId(new Set<Id> {request.ParentRecordID});
            System.debug('Map opps by account >>>' +opportunityIdByAccountId);

            if (!opportunityIdByAccountId.isEmpty()) {
                Map<Id, List<Product_Opportunity_Assignment__c>> mProductOppAssignByOpp = accOppSelector.selectProductAssignmentToTagus(new Set<Id> {opportunityIdByAccountId.get(request.ParentRecordID)});
                System.debug('Map Query >>>' +mProductOppAssignByOpp);

                if(!mProductOppAssignByOpp.isEmpty()) {
                    prodOppAssLst = mProductOppAssignByOpp.get(opportunityIdByAccountId.get(request.ParentRecordID));
                }
            }

            system.debug('## prodOppAssLst: '+json.serializePretty(prodOppAssLst));
            result.data.addAll(prodOppAssLst);
            result.FullQueryCount = result.data.size();
            result.pagecount = 1;
            result.isError = false;
            result.ErrorMessage = '';
        }
        catch (Exception e) {
            DebugLog__c log = new DebugLog__c(
                    Origin__c = '['+className+'][getData]',
                    LogType__c = constants.LOGTYPE_ERROR,
                    RelatedToSquad__c = constants.LOGTYPE_SQUAD_PARTNERS,
                    ExceptionMessage__c = e.getMessage(),
                    StackTrace__c = e.getStackTraceString(),
                    ObjectId__c = request.ParentRecordID
            );
            Logger.createLog(log);
        }

        return result;
    }

    global sortablegrid.SDG LoadSDG(String SDGTag, String ParentRecordId)
    {
        sortablegrid.SDG CoreSDG = new sortablegrid.SDG( 'CurrentValidProducts' );
        CoreSDG.SDGFields = GetFields();
        return CoreSDG;
    }

    private List<sortablegrid.SDGField> GetFields()
    {
        List<sortablegrid.SDGField> fields = new List<sortablegrid.SDGField>();

        fields.add( new sortablegrid.SDGField('1', 'PROD NAME', 'ProductAssignmentId__r.ProductId__r.Name', 'STRING', '', false, false, null, 1));
        fields.add( new sortablegrid.SDGField('3', 'TYPE', 'ProductAssignmentId__r.ProductId__r.Type__c', 'STRING', '', false, false, null, 3));
        fields.add( new sortablegrid.SDGField('4', 'NET TRANSFER PRICE', 'ProductAssignmentId__r.ProductId__r.Net_Transfer_Price__c', 'DOUBLE', '', false, false, null, 4));
        fields.add( new sortablegrid.SDGField('5', 'CURRENCY', 'ProductAssignmentId__r.ProductId__r.CurrencyIsoCode', 'STRING', '', false, false, null, 5));
        fields.add( new sortablegrid.SDGField('6', 'COMMERCIAL CONDITION', 'ProductAssignmentId__r.CommercialConditionId__r.Name', 'STRING', '', false, false, null, 6));
        fields.add( new sortablegrid.SDGField('8', 'VALUE', 'ProductAssignmentId__r.CommercialConditionId__r.Value__c', 'DOUBLE', '', false, false, null, 8));
        fields.add( new sortablegrid.SDGField('9', 'RECORD TYPE', 'ProductAssignmentId__r.CommercialConditionId__r.RecordType.DeveloperName', 'STRING', '', false, false, null, 9));

        return fields;
    }

    public class CurrentValidProductsDataProviderException extends Exception {}
}