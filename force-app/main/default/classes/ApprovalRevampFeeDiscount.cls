/**
 * @description       : 
 * @author            : roei@gft.com
 * @group             : 
 * @last modified on  : 12-05-2021
 * @last modified by  : roei@gft.com
**/
public without sharing class ApprovalRevampFeeDiscount {
    List< Id > gLstOppId;
    Map< Id , Quote > gMapIdOldQuote;
    List< Quote > gLstNewQuote;

    public ApprovalRevampFeeDiscount(){}

    public void saveFeesApprovedDiscount( Map< Id , Quote > aMapIdOldQuote , List< Quote > aLstNewQuote ){
        gLstNewQuote = aLstNewQuote;
        gMapIdOldQuote = aMapIdOldQuote;
        List< Assert_Data__c > lLstFeesApprovedDiscount = saveFeesApprovedDiscount();

        if( !lLstFeesApprovedDiscount.isEmpty() ){
            Database.upsert( lLstFeesApprovedDiscount );
        }
    }

    private List< Assert_Data__c > saveFeesApprovedDiscount(){
        List< Assert_Data__c > lLstFeesApprovedDiscount = new List< Assert_Data__c >();

        List< Id > lLstQuoteId = getApprovedQuoteId();

        if( lLstQuoteId.isEmpty() ){ return lLstFeesApprovedDiscount; } 

        List< QuoteLineItem > lLstQuoteLineItem = getApprovedFees( lLstQuoteId );

        Map< String , Decimal > lMapFeeTypeApprovedDiscount = getMapFeeTypeApprovedDiscount( lLstQuoteLineItem );
        
        lLstFeesApprovedDiscount = setAssertDataFeeDiscount( lLstQuoteLineItem , lMapFeeTypeApprovedDiscount );

        return lLstFeesApprovedDiscount;
    }

    private List< Assert_Data__c > setAssertDataFeeDiscount( List< QuoteLineItem > aLstQuoteLineItem , Map< String , Decimal > aMapFeeTypeApprovedDiscount ){
        List< Assert_Data__c > lLstAssertData = 
            [ SELECT Id, Field_Name__c, Object_Name__c, Fee_Type__c,  Old_Value__c, New_Value__c, Pre_Approved_Condition__c
              FROM Assert_Data__c WHERE Opportunity__c =: gLstOppId AND Fee_Type__c IN( 'Enterprise Subscription' , 'Setup Fee' )
              AND Field_Name__c = 'discount__c' AND Object_Name__c = 'quotelineitem' ];
        
        if( lLstAssertData.isEmpty() ){
            for( QuoteLineItem iQuoteLineItem : aLstQuoteLineItem ){
                lLstAssertData.add( new Assert_Data__c(
                    Opportunity__c = gLstOppId[0],
                    Fee_Type__c = iQuoteLineItem.Fee_Type__c,
                    Object_Name__c = 'quotelineitem',
                    Field_Name__c = 'discount__c',
                    Pre_Approved_Condition__c = false
                ) );
            }
        }

        for( Assert_Data__c iAssertData : lLstAssertData ){
            Decimal lNewDiscount = aMapFeeTypeApprovedDiscount.get( iAssertData.Fee_Type__c );

            if( iAssertData.Pre_Approved_Condition__c == true ){
                iAssertData.Old_Value__c = String.valueOf( lNewDiscount );
            } else {
                iAssertData.Old_Value__c = !String.isBlank( iAssertData.Old_Value__c ) && 
                    Decimal.valueOf( iAssertData.Old_Value__c ) >= lNewDiscount ?
                    iAssertData.Old_Value__c : String.valueOf( lNewDiscount );
            }
            
            iAssertData.Pre_Approved_Condition__c = false;
        }

        return lLstAssertData;
    }

    private List< Id > getApprovedQuoteId(){
        gLstOppId = new List< Id >();
        List< Id > lLstQuoteId = new List< Id >();

        for( Quote iQuote : gLstNewQuote ){
            if( !gMapIdOldQuote.get( iQuote.Id ).Enterprise_Subscription_Approved__c && iQuote.Enterprise_Subscription_Approved__c ){
                lLstQuoteId.add( iQuote.Id );
                gLstOppId.add( iQuote.OpportunityId );
            }
        }

        return lLstQuoteId;
    }
    
    private List< QuoteLineItem > getApprovedFees( List< Id > aLstQuoteId ){
        return [ SELECT Id, Fee_Type__c, Discount__c FROM QuoteLineItem WHERE QuoteId =: aLstQuoteId 
            AND Fee_Type__c IN( 'Enterprise Subscription' , 'Setup Fee' ) ];
    }

    private Map< String , Decimal > getMapFeeTypeApprovedDiscount( List< QuoteLineItem > aLstQuoteLineItem ){
        Map< String , Decimal > lMapFeeTypeApprovedDiscount = new Map< String , Decimal >();

        for( QuoteLineItem iQuoteLineItem : aLstQuoteLineItem ){
            lMapFeeTypeApprovedDiscount.put( iQuoteLineItem.Fee_Type__c , iQuoteLineItem.Discount__c );
        }

        return lMapFeeTypeApprovedDiscount;
    }
}