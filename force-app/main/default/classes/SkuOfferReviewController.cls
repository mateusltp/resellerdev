/**
 * @description       : 
 * @author            : roei@gft.com
 * @group             : 
 * @last modified on  : 05-27-2022
 * @last modified by  : roei@gft.com
**/
public without sharing class SkuOfferReviewController {
    public SkuOfferReviewController(){}

    public static Map< String , String > gMapDealDeskApprovedConditions;

    @AuraEnabled(cacheable=false)
    public static void sendToApproval( String recordId , String aRationale , Decimal aTotalDiscount , String aQuoteId , String aDealDeskRationale,
        Boolean needQuoteApproval, Boolean needDealDeskApproval ){
        String lWaivers = getWaivers( aQuoteId );
        
        try{
            if( needQuoteApproval ){    
                Approval.ProcessSubmitRequest quoteReq = new Approval.ProcessSubmitRequest();
                quoteReq.setComments( 'Rationale:' + aRationale + '\nTotal Discount: ' + aTotalDiscount + '%\n' + lWaivers );
                quoteReq.setObjectId( aQuoteId );
                quoteReq.setSubmitterId( UserInfo.getUserId() );
                quoteReq.setNextApproverIds( getInicialApprover( aQuoteId ) );
                quoteReq.setProcessDefinitionNameOrId( 'SKU_Quote_Discount_Approval' );
            
                Approval.ProcessResult result = Approval.process( quoteReq );
            }
            
            if( needDealDeskApproval ){
                sendDealDeskCaseToApproval( recordId , aQuoteId , aDealDeskRationale );
            }
        } catch( Exception aException ){
            if( !aException.getMessage().contains( 'NO_APPLICABLE_PROCESS' ) ){
                throw new CustomException( aException.getMessage() );
            }
        }
    }
    
    public static void sendDealDeskCaseToApproval( String aOppId , String aQuoteId , String aRationale ){
        String lCaseDescription = getDealDeskDescription( aQuoteId );

        Case lDeakDeskCase = 
            new Case (   
                Subject = 'Deal Desk Approval Request', 
                RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Deal_Desk_Approval').getRecordTypeId(), 
                OpportunityId__c = aOppId,
                Description = lCaseDescription + '\nRationale: ' + aRationale,
                QuoteId__c = aQuoteId,
                Status = 'New'
                );
        
        Database.SaveResult lCaseInsertResult = Database.insert( lDeakDeskCase );

        if(lCaseInsertResult.isSuccess()){
            Boolean lIsShared = new CaseRepository().manualShareRead( lDeakDeskCase.Id , UserInfo.getUserId() );
            createApprovalItems( lDeakDeskCase.Id , aOppId );
        }
    }
    
    public static void createApprovalItems( String aDealDeskCaseId , String aOppId ){
        Map< String , String > lMapApprovalItemValueField = getMapApprovalItemValueField( aOppId );

        ApprovalItemService lApprovalService = new ApprovalItemService();

        List< Approval_Item__c > lLstApprovalItem = 
            lApprovalService.addApprovalItem( aDealDeskCaseId , lMapApprovalItemValueField , gMapDealDeskApprovedConditions );

        lApprovalService.createApprovalItems( lLstApprovalItem );
    }

    public static Map< String , String > getMapApprovalItemValueField( String aOppId ){
        Map< String , String > lMapSobjNFieldNameFieldValue = getMapSobjNFieldNameFieldValue( aOppId );

        Map< String , String > lMapApprovalItemValueField = 
            new Map< String , String >{
                'Payment frequency is custom' => lMapSobjNFieldNameFieldValue.get( 'opportunity.billing_period__c' ),
                'Autonomous Market Place is not defined for your Offer' => lMapSobjNFieldNameFieldValue.get( 'quote.autonomous_marketplace_contract__c' ),
                'Eligibility/MembershipFee with Custom payment settings or Credit Card method' => lMapSobjNFieldNameFieldValue.get( 'eligibility__c.payment_method__c' )
            };

        return lMapApprovalItemValueField;
    }

    public static Map< String , String > getMapSobjNFieldNameFieldValue( String aOppId ){
        Map< String , String > lMapSobjNFieldNameFieldValue = new Map< String , String >();

        Set< String > lSetOppFieldsToKeep = new Set< String >{ 'billing_period__c' };
        Set< String > lSetQuoteFieldsToKeep = new Set< String >{ 'autonomous_marketplace_contract__c' };
        Set< String > lSetEligibilityFieldsToKeep = new Set< String >{ 'payment_method__c' };

        Opportunity lOpp = [ SELECT billing_period__c FROM Opportunity WHERE Id =: aOppId ];
        Quote lQuote = [ SELECT autonomous_marketplace_contract__c FROM Quote WHERE OpportunityId =: aOppId LIMIT 1];
        Eligibility__c lEligibility = [ SELECT Payment_Method__c FROM Eligibility__c WHERE Opportunity_Id__c =: aOppId LIMIT 1];

        for( String iFieldName : lSetOppFieldsToKeep ){
            lMapSobjNFieldNameFieldValue.put( 'opportunity.' + iFieldName , String.valueOf( lOpp.get( iFieldName ) ) );
        }

        for( String iFieldName : lSetQuoteFieldsToKeep ){
            lMapSobjNFieldNameFieldValue.put( 'quote.' + iFieldName , String.valueOf( lQuote.get( iFieldName ) ) );
        }

        for( String iFieldName : lSetEligibilityFieldsToKeep ){
            lMapSobjNFieldNameFieldValue.put( 'eligibility__c.' + iFieldName , String.valueOf( lEligibility.get( iFieldName ) ) );
        }

        return lMapSobjNFieldNameFieldValue;
    }

    public static String getDealDeskDescription( String aQuoteId ){
        String lCaseDealDeskDescription = '';
        gMapDealDeskApprovedConditions = new Map< String , String >();

        Quote lQuote = [ SELECT Deal_Desk_Approved_Conditions__c FROM Quote WHERE Id =: aQuoteId LIMIT 1 ];

        List< SKUDealDeskHandler.DealDeskConditions > lLstDealDeskConditions =
            (List< SKUDealDeskHandler.DealDeskConditions >) JSON.deserialize( lQuote.Deal_Desk_Approved_Conditions__c , List< SKUDealDeskHandler.DealDeskConditions >.class );

        for( SKUDealDeskHandler.DealDeskConditions iCondition : lLstDealDeskConditions ){
            lCaseDealDeskDescription += iCondition.approvalCondition + ', ';
            gMapDealDeskApprovedConditions.put( iCondition.approvalCondition , String.valueOf( iCondition.isApproved ) );
        }

        return lCaseDealDeskDescription;
    }

    public static String getWaivers( String aQuoteId ){
        String lAllWaivers = '';
        Map< String , String > lMapItemWaivers = new Map< String , String >();

        for( Waiver__c iWaiver : [ SELECT Id, Quote_Line_Item__r.Product_Name__c, Percentage__c, Duration__c FROM Waiver__c WHERE Quote_Line_Item__r.QuoteId =: aQuoteId ] ){
            if( lMapItemWaivers.get( iWaiver?.Quote_Line_Item__r?.Product_Name__c ) == null ){
                lMapItemWaivers.put( iWaiver.Quote_Line_Item__r.Product_Name__c , ( iWaiver.Percentage__c + '% / ' + iWaiver.Duration__c + ' months' ) );
            } else {
                String lNewWaiver = lMapItemWaivers.get( iWaiver.Quote_Line_Item__r.Product_Name__c );
                lNewWaiver += ( ' - ' + iWaiver.Percentage__c + '% / ' + iWaiver.Duration__c + ' months' );
                lMapItemWaivers.put( iWaiver.Quote_Line_Item__r.Product_Name__c , lNewWaiver );
            }
        }

        for( String iQuoteLineItem : lMapItemWaivers.keySet() ){
            lAllWaivers += (( iQuoteLineItem + ': ' + lMapItemWaivers.get( iQuoteLineItem ) ) + '\n' );
        }

        return lAllWaivers;
    }

    public static Id[] getInicialApprover( String aQuoteId ){
        Quote lQuote = [ SELECT Discount_Approval_Level__c, Waiver_Approval_Level__c FROM Quote WHERE Id =: aQuoteId ];
        User lUser = [ SELECT ManagerId, Manager.ManagerId FROM User WHERE Id =: UserInfo.getUserId() ];

        Decimal aTotalDiscountLevel = lQuote.Discount_Approval_Level__c;
        Decimal aWaiverApprovalLevel = lQuote.Waiver_Approval_Level__c;

        return new Id[]{ ( lQuote.Discount_Approval_Level__c == 0 && lQuote.Waiver_Approval_Level__c > 0 ? lUser.Manager.ManagerId : lUser.ManagerId ) };
    }
}