/*
 * @author Bruno Pinho
 * @date January/2019
 * @description Schedulable class to run BatchEventDecisionMakers
 */
 global class SchBatchEventAllHistoryDecisionMakers Implements Schedulable
    {
        global void execute(SchedulableContext sc)
        {
            scheduleBatchEventDecisionMakers();
        }

        public void scheduleBatchEventDecisionMakers()
        {
            Database.executeBatch(new BatchEventAllHistoryDecisionMakers());
        }
    }