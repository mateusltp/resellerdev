/**
 * @description       : 
 * @author            : gft.jorge.stevaux@ext.gympass.com
 * @group             : 
 * @last modified on  : 08-12-2021
 * @last modified by  : gft.jorge.stevaux@ext.gympass.com
**/

@isTest(seeAllData=false)
private class ContactSelectorTest {

    @TestSetup
    static void makeData(){

        Account lAcc = PartnerDataFactory.newAccount();              
        Database.insert( lAcc );

        Contact lContact = PartnerDataFactory.newContact(lAcc.Id);
        Database.insert(lContact);

        Bank_Account__c lBankAcct = PartnerDataFactory.newBankAcct();
        Database.insert(lBankAcct);

        Acount_Bank_Account_Relationship__c lBankAcctRel = PartnerDataFactory.newBankAcctRel(lAcc.Id, lBankAcct);
        Database.insert(lBankAcctRel);
                                  
        Opportunity lOpp = PartnerDataFactory.newOpportunity( lAcc.Id, 'Partner_Wishlist_Renegotiation' );     
        Database.insert( lOpp );

        
    }

    @isTest 
    static void getSObjectFieldList_Test(){

        List<Schema.SObjectField> schSObjFields = new List<Schema.SObjectField> {
			Contact.Id,
			Contact.Name
		};

        Test.startTest();
        System.assertEquals(schSObjFields, new ContactSelector().getSObjectFieldList());
        Test.stopTest();
    }

    @isTest 
    static void getSObjectType_Test(){

        Test.startTest();
        System.assertEquals(Contact.sObjectType, new ContactSelector().getSObjectType());
        Test.stopTest();
    }

    @isTest 
    static void selectById_Test(){

        List<Contact> conLst = [SELECT Id, Name, CurrencyIsoCode FROM Contact LIMIT 1];
        Set<Id> conIds = (new Map<Id, Contact>(conLst)).keySet();

        Test.startTest();
        System.assertEquals(conLst, new ContactSelector().selectById(conIds));
        Test.stopTest();
    }
}