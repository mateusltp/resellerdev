@RestResource(urlMapping='/self-checkout-opportunity/*')
global with sharing class SelfCheckoutOpportunityRoute {
  @HttpPost
  global static SelfCheckoutOpportunityResponse post() {
    RestResponse restResponse = new SelfCheckoutOpportunityController().post();

    return new SelfCheckoutOpportunityResponse(restResponse);
  }

  global class SelfCheckoutOpportunityResponse {
    private Integer statusCode;
    private String errorMessage;

    private SelfCheckoutOpportunityResponse(RestResponse restResponse) {
      this.statusCode = restResponse.statusCode;
      this.errorMessage = restResponse?.responseBody?.toString();
    }

    public Integer getStatusCode() {
      return this.statusCode;
    }

    public String getErrorMessage() {
      return this.errorMessage;
    }
  }
}