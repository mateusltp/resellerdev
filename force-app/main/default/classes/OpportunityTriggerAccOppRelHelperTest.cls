/**
 * @File Name          : OpportunityTriggerAccOppRelHelperTest.cls
 * @Description        : 
 * @Author             : GEPI@GFT.com
 * @Group              : 
 * @Last Modified By   : GEPI@GFT.com
 * @Last Modified On   : 07-24-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    17/04/2020   GEPI@GFT.com     Initial Version
**/
@Istest
public class OpportunityTriggerAccOppRelHelperTest {

    @TestSetup
    static void Setup(){      
        
        Id rtId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gym_Partner').getRecordTypeId();
        Id recordTypeIdSmall = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Gyms_Small_Medium_Partner').getRecordTypeId();
        Id recordTypeIdWishList = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Gyms_Wishlist_Partner').getRecordTypeId();
		Id recordTypeIdClientNewBusiness = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
    	Id recordTypeIdClientResellerNewBusiness = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Indirect_Channel_New_Business').getRecordTypeId(); 
        
        Account acc = new Account(name='AccTest', website='www.test.com',  RecordTypeId = rtId, ShippingCountry = 'Brazil', 
            ShippingStreet = 'Bermudas', ShippingCity = 'Sorocaba' , ShippingState = 'São Paulo',  Types_of_ownership__c = Label.franchisePicklist);    
        INSERT acc;    

        Id oppRtId =   Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Gyms_Small_Medium_Partner').getRecordTypeId();
        
        
        Opportunity smbOpp = new Opportunity();
        smbOpp.recordTypeId = recordTypeIdSmall;
        smbOpp.AccountId = acc.id;
        smbOpp.Name = 'SmbOppTest';
        smbOpp.CloseDate = Date.today();
        smbOpp.StageName = 'Qualification';
        smbOpp.Type	= 'Expansion';
        INSERT smbOpp;
        
        Opportunity smbOpp2 = new Opportunity();
        smbOpp2.recordTypeId = recordTypeIdClientNewBusiness;
        smbOpp2.AccountId = acc.id;
        smbOpp2.Name = 'SmbOppTest2';
        smbOpp2.CloseDate = Date.today();
        smbOpp2.StageName = 'Qualification';
        smbOpp2.Type	= 'Expansion';
        INSERT smbOpp2;

        Account_Opportunity_Relationship__c accOppRel = new Account_Opportunity_Relationship__c();
        accOppRel.Opportunity__c = smbOpp.id;
        accOppRel.Account__c = acc.id;
        INSERT accOppRel;
        
        Account_Opportunity_Relationship__c accOppRel2 = new Account_Opportunity_Relationship__c();
        accOppRel2.Opportunity__c = smbOpp2.id;
        accOppRel2.Account__c = acc.id;
        INSERT accOppRel2;
   
    }


    @isTest static void deleteOpportunity() {
        Opportunity opp = [SELECT id, Name FROM Opportunity WHERE Name = 'SmbOppTest'];
        Test.startTest();
        delete opp;
        List<Account_Opportunity_Relationship__c> accOppRelList =  [SELECT id  FROM Account_Opportunity_Relationship__c WHERE Opportunity__c =: opp.id];
        system.debug('accOppRelList' +  accOppRelList);
        System.assertEquals(accOppRelList.isEmpty(), true);
        Test.stopTest(); 
    }
}