@IsTest
public class QuoteLineItemMock {
  public static QuoteLineItem getSetupFee(Quote quote, PricebookEntry pricebookEntry) {
    return new QuoteLineItem(
      Discount = 0,
      Product2Id = pricebookEntry.Product2Id,
      PricebookEntryId = pricebookEntry.Id,
      Quantity = 684,
      QuoteId = quote.Id,
      UnitPrice = 3.04,
      Fee_Contract_Type__c = 'Flat Fee',
      Flat_Baseline_Adjustment__c = false
    );
  }

  public static QuoteLineItem getEnterpriseSubscription(Quote quote, PricebookEntry pricebookEntry) {
    return new QuoteLineItem(
      Discount = 0,
      PricebookEntryId = pricebookEntry.Id,
      Product2Id = pricebookEntry.Product2Id,
      QuoteId = quote.Id,
      Quantity = 684,
      UnitPrice = 29.9,
      Fee_Contract_Type__c = 'Flat Fee',
      Flat_Baseline_Adjustment__c = false
    );
  }
}