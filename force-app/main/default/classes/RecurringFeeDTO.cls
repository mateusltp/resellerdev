public without sharing class RecurringFeeDTO extends FeeItemDTO {
  private String flat_baseline_adjustment;
  private Decimal flat_unit_price;
  private Decimal flat_total_price;
  private Decimal flat_discount_percentage;
  private Integer flat_baseline_quantity;
  private Integer billing_day;
  private Integer cutoff_day;
  private FeePriceType fee_price_type;
  private FeeType fee_type;
  private BillingPeriod billing_period;

  private String inflation_adjustment_period;
  private String inflation_adjustment_type;

  private Decimal variable_price_floor;
  private Decimal variable_price_ceiling;
  private Decimal variable_price_unit_increase;

  private Decimal discount_price_floor;
  private Decimal discount_price_ceiling;
  private Decimal discount_price_unit_decrease;

  public RecurringFeeDTO(OrderItem orderItem) {
    this.id = orderItem.UUID__c != null
      ? orderItem.UUID__c
      : new Uuid().getValue();
    this.salesforce_id = orderItem.Id;
    this.currency_id = orderItem.CurrencyIsoCode;
    this.fee_type = getFeeType(orderItem);
    this.fee_price_type = getFeePriceType(orderItem);
    this.billing_period = getBillingPeriod(orderItem);
    this.payment_due_days = (Integer) orderItem.Payment_Due_Days__c;
    this.billing_day = (Integer) orderItem.Billing_Day__c;
    this.cutoff_day = (Integer) orderItem.Cutoff_Day__c;

    this.flat_baseline_adjustment = String.valueOf(orderItem.Flat_Baseline_Adjustment__c);
    this.flat_unit_price = orderItem.UnitPrice;
    this.flat_total_price = orderItem.TotalPrice;
    this.flat_discount_percentage = orderItem.Discount__c;
    this.flat_baseline_quantity = (Integer) orderItem.Quantity;

    this.variable_price_floor = orderItem.Variable_Price_Floor__c;
    this.variable_price_ceiling = orderItem.Variable_Price_Ceiling__c;
    this.variable_price_unit_increase = orderItem.Variable_Price_Unit_Increase__c;

    this.discount_price_floor = orderItem.Discount_Price_Floor__c;
    this.discount_price_ceiling = orderItem.Discount_Price_Ceiling__c;
    this.discount_price_unit_decrease = orderItem.Discount_Price_Unit_Decrease__c;

    this.inflation_adjustment_type = orderItem.Inflation_Adjustment_Index__c;
    this.inflation_adjustment_period = orderItem.Inflation_Adjustment_Period__c;
  }

  public FeeType getFeeType() {
    return this.fee_type;
  }

  private FeeType getFeeType(OrderItem orderItem) {
    if (orderItem.Type__c.contains('Professional Services')) {
      return FeeType.PROFESSIONAL_MAINTENANCE_FEE;
    }

    return FeeType.ACCESS_FEE;
  }

  private FeePriceType getFeePriceType(OrderItem orderItem) {
    switch on orderItem.Contract_Type__c {
      when 'Copay 2' {
        return FeePriceType.VARIABLE_PRICE_ENROLLED;
      }
      when 'Flat Fee' {
        return FeePriceType.FLAT_PRICE_ELIGIBLE;
      }
      when else {
        return null;
      }
    }
  }

  private BillingPeriod getBillingPeriod(OrderItem orderItem) {
    switch on orderItem.Recurring_Billing_Period__c {
      when 'Monthly' {
        return BillingPeriod.MONTH;
      }
      when 'Quarterly' {
        return BillingPeriod.QUARTER;
      }
      when 'Semesterly' {
        return BillingPeriod.SEMESTER;
      }
      when 'Annually' {
        return BillingPeriod.YEAR;
      }
      when else {
        return null;
      }
    }
  }

  override
  public QuoteLineItem parseToQuoteLineItem() {
    return new QuoteLineItem(
      UUID__c = this.id,
      Fee_Contract_Type__c = getFeePriceType(this),
      Recurring_Billing_Period__c = getBillingPeriod(this),
      Payment_Due_Days__c = this.payment_due_days,
      Flat_Baseline_Adjustment__c = this.flat_baseline_adjustment != null 
        ? Boolean.valueOf(this.flat_baseline_adjustment) 
        : false,
      UnitPrice = this.flat_unit_price,
      Discount = this.flat_discount_percentage,
      Quantity = this.flat_baseline_quantity,
      Variable_Price_Floor__c = this.variable_price_floor,
      Variable_Price_Ceiling__c = this.variable_price_ceiling,
      Variable_Price_Unit_Increase__c = this.variable_price_unit_increase,
      Discount_Price_Floor__c = this.discount_price_floor,
      Discount_Price_Ceiling__c = this.discount_price_ceiling,
      Discount_Price_Unit_Decrease__c = this.discount_price_unit_decrease,
      Inflation_Adjustment_Index__c = this.inflation_adjustment_type,
      Inflation_Adjustment_Period__c = this.inflation_adjustment_period
    );
  }

  private String getFeePriceType(RecurringFeeDTO recurringFee) {
    switch on recurringFee.fee_price_type {
      when VARIABLE_PRICE_ENROLLED {
        return 'Copay 2';
      }
      when FLAT_PRICE_ELIGIBLE {
        return 'Flat Fee';
      }
      when else {
        return null;
      }
    }
  }

  private String getBillingPeriod(RecurringFeeDTO recurringFee) {
    switch on recurringFee.billing_period {
      when MONTH {
        return 'Monthly';
      }
      when QUARTER {
        return 'Quarterly';
      }
      when SEMESTER {
        return 'Semesterly';
      }
      when YEAR {
        return 'Annually';
      }
      when else {
        return null;
      }
    }
  }

  public enum FeeType {
    ACCESS_FEE,
    PROFESSIONAL_MAINTENANCE_FEE
  }

  private enum FeePriceType {
    FLAT_PRICE_ELIGIBLE,
    VARIABLE_PRICE_ELIGIBLE,
    VARIABLE_PRICE_ENROLLED,
    DISCOUNT_PRICE_ENROLLED
  }

  private enum BillingPeriod {
    MONTH,
    QUARTER,
    SEMESTER,
    YEAR
  }
}