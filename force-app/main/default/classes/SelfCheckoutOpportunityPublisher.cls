public without sharing class SelfCheckoutOpportunityPublisher {
    public final String EVENT_NAME = 'SELF_CHECKOUT_OPPORTUNITY_INBOUND';
   
    public EventQueue publish(SelfCheckoutOpportunityRequest selfCheckoutOpportunityRequest) {
      EventQueue event = new EventBuilder()
        .createEventFor(EVENT_NAME)
        .withStatus(EventQueueStatusType.QUEUED.name())
        .withSender('JAMOR')
        .withReceiver('SALESFORCE')
        .withBusinessDocumentNumber(selfCheckoutOpportunityRequest.getLeadDTO().getClientName())
        .withBusinessDocumentCorrelatedNumber(selfCheckoutOpportunityRequest.getOpportunityDTO().parseToSOpportunity().StageName)
        .buildEvent();
  
      event.addPayload(EVENT_NAME, JSON.serialize(selfCheckoutOpportunityRequest));
  
      event.save();
  
      return event;
    }
  }