@IsTest
public class CountryStatesRouteTest {

    @IsTest
    public static void execute() {

      RestRequest request = new RestRequest();
      request.requestURI = '/services/apexrest/country-states-cities/BR';
      request.httpMethod = 'GET';
  
      Test.startTest();
  
      RestContext.response = new RestResponse();
      RestContext.request = request;
      CountryStatesRoute.CountryStatesResponse response = CountryStatesRoute.get();
      
      Test.stopTest();
      System.assertEquals(
        400,
        response.getStatusCode(),
        'Error Message: ' + response.getErrorMessage()
      );

    }
  
}