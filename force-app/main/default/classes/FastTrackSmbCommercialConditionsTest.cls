@isTest
public with sharing class FastTrackSmbCommercialConditionsTest {

    @isTest
    static void testSMB(){
       
        Opportunity lOpp = [ SELECT Id , Name , AccountId , CurrencyIsoCode  FROM Opportunity ];

        Test.startTest();

        FastTrackProposalCreationTO lProposalTo = 
            FastTrackProposalCreationController.find( lOpp.Id , 1 , new List< Id >{ lOpp.AccountId } );
          
        FastTrackProposalCreationTO lReturnedProposalTo =
            FastTrackProposalCreationController.submitForApproval( JSON.serialize( lProposalTo ) );

        FastTrackProposalCreationTO lReturnedProposalEvaluation =
            FastTrackProposalCreationController.proposalEvaluation( lOpp.Id );
        Test.stopTest();
    }

    @TestSetup
    static void createData(){    
        Account lParentAcc = DataFactory.newAccount();
        lParentAcc.NumberOfEmployees = 100;
        Database.insert( lParentAcc );

        Account lAcc = DataFactory.newAccount();
        lAcc.ParentId = lParentAcc.Id;
        lAcc.NumberOfEmployees = 1000;
        Database.insert( lAcc );

        Contact lNewContact = DataFactory.newContact( lParentAcc , 'Test Contact' );
        Database.insert( lNewContact );

        Pricebook2 lStandardPb = DataFactory.newPricebook();
        Database.update( lStandardPb );

        Opportunity lOpp = new Opportunity(CurrencyIsoCode='BRL');

        Product2 lAcessFee = DataFactory.newProduct( 'Enterprise Subscription' , false , 'BRL' );
        Database.insert( lAcessFee );

        Product2 lAccessFeeFamilyMember = DataFactory.newProduct( 'Enterprise Subscription' , true , 'BRL' );
        Database.insert( lAccessFeeFamilyMember );

        PricebookEntry lAccessFeeEntry = DataFactory.newPricebookEntry( lStandardPb , lAcessFee , lOpp );
        Database.insert( lAccessFeeEntry );

        PricebookEntry lAccessFeeFamilyMemberEntry = DataFactory.newPricebookEntry( lStandardPb , lAccessFeeFamilyMember , lOpp );
        Database.insert( lAccessFeeFamilyMemberEntry );

        Product2 lSetupFee = DataFactory.newProduct( 'Setup Fee' , false , 'BRL' );
        Database.insert( lSetupFee );

        PricebookEntry lSetupFeeEntry = DataFactory.newPricebookEntry( lStandardPb , lSetupFee , lOpp );
        Database.insert( lSetupFeeEntry );

        Product2 lProfServicesOneFee = DataFactory.newProduct( 'Professional Services Setup Fee' , false , 'BRL' );
        Database.insert( lProfServicesOneFee );

        PricebookEntry lProfServicesOneFeeEntry = DataFactory.newPricebookEntry( lStandardPb , lProfServicesOneFee , lOpp );
        Database.insert( lProfServicesOneFeeEntry );

        Product2 lProfServicesMainFee = DataFactory.newProduct( 'Professional Services Maintenance Fee' , false , 'BRL' );
        Database.insert( lProfServicesMainFee );

        PricebookEntry lProfServicesMainFeeEntry = DataFactory.newPricebookEntry( lStandardPb , lProfServicesMainFee , lOpp );
        Database.insert( lProfServicesMainFeeEntry );

        lOpp = DataFactory.newOpportunity( lAcc.Id , lStandardPb , 'SMB_Success_Renegotiation' );
        lOpp.Quantity_Offer_Number_of_Employees__c = 100;
        Database.insert( lOpp );

        Quote lProposal = DataFactory.newQuote( lOpp , 'Quote Test', 'Client_Sales_New_Business' );
        lProposal.Deal_Desk_Approval_Needed__c = true;
        lProposal.Deal_Desk_Approved__c = false;
        Database.insert( lProposal );

        Case lDealDeskCase = DataFactory.newCase( lOpp , lProposal , 'Deal_Desk_Approval' );
        lDealDeskCase.Deal_Desk_Evaluation__c = 'Approved';
        Database.insert( lDealDeskCase );

        QuoteLineItem lQuoteAccessFee = DataFactory.newQuoteLineItem( lProposal , lAccessFeeEntry );
        lQuoteAccessFee.Discount__c = 50;
        Database.insert( lQuoteAccessFee );
   
        Payment__c lAccessFeePayment = DataFactory.newPayment( lQuoteAccessFee );
        Database.insert( lAccessFeePayment );

        Eligibility__c eli = DataFactory.newEligibility(lAccessFeePayment, true);
        Database.insert(eli);
        
        String lOppId = lAccessFeePayment.Quote_Line_Item__r.Quote.OpportunityId;

        Waiver__c iWaiver = DataFactory.newWaiver( lAccessFeePayment );
        iWaiver.Number_of_Months_after_Launch__c = 12;
        iWaiver.Approval_Status__c = 'New';
        Database.insert( iWaiver );

        Database.insert( DataFactory.newGympassEntity( 'Gympass' ) );
    }

}