public without sharing class UpdateSmbOppHelper {
	@InvocableMethod
    public static void updateStepsTowardsSuccess(List<Id> sObjectIdList) {
   		List<Contact> contatcs = getContacts(sObjectIdList);
        List<Opportunity> smbOpps = getSmbOppsForContacts(contatcs);
        updateSMBOpps(contatcs, smbOpps);
    }
    
    private static List<Contact> getContacts(List<Id> contactIdList) {
   		List<Contact> contacts = [
       		SELECT Id, AccountId, Business_Model_Indirect__c, Cofee_Comission_Rate__c
            FROM Contact
            WHERE Id IN :contactIdList
        ];
        
        return contacts;
    }
    
    private static List<Opportunity> getSmbOppsForContacts(List<Contact> contacts) {
		Set<Id> accountIdList = new Set<Id>();
        
        for (Contact c : contacts) {
        	accountIdList.add(c.AccountId);
		}
        
        List<Opportunity> opps = [
            SELECT Id, AccountId, B_M__c, Taxa_de_comissionamento_sobre_cofee__c
            FROM Opportunity
            WHERE RecordType.DeveloperName = 'SMB' AND StageName != 'Perdido'
            Order By CreatedDate DESC
        ];
        
        return opps;
    }
    
    private static void updateSMBOpps(List<Contact> contacts, List<Opportunity> smbOpps) {
        List<Opportunity> oppsToUpdate = new List<Opportunity>();
        Map<Id, Opportunity> accountIdToOppMap = getAccountIdToOppMap(smbOpps);
        
        for (Contact c : contacts) {
            Opportunity opp = accountIdToOppMap.get(c.AccountId);
            
            if (opp != null) {
            	opp.B_M__c = c.Business_Model_Indirect__c;
                opp.Taxa_de_comissionamento_sobre_cofee__c = c.Cofee_Comission_Rate__c;
                oppsToUpdate.add(opp);
            }
        }
        
        update oppsToUpdate;
    }
    
    private static Map<Id, Opportunity> getAccountIdToOppMap(List<Opportunity> opps) {
		Map<Id, Opportunity> accountIdToOppMap = new Map<Id, Opportunity>();
        
        for (Opportunity opp : opps) {
            if (!accountIdToOppMap.containsKey(opp.AccountId)) {
            	accountIdToOppMap.put(opp.AccountId, opp);
            }
        }
        
        return accountIdToOppMap;
    } 
}