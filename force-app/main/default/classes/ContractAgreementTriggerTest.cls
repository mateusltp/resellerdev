/**
 * @description       : 
 * @author            : gilberto.souza@gft.com
 * @group             : 
 * @last modified on  : 17-06-2021
 * @last modified by  : marcus.silva@gft.com
 * Modifications Log 
 * Ver   Date         Author         			Modification
 * 1.0   10-05-2021   gilberto.souza@gft.com    Initial Version
 * 2.0   17-06-2021   marcus.silva@gft.com      Creating ContractAgreementTriggerTest method and deleting all others.   
**/

@isTest
public with sharing class ContractAgreementTriggerTest {
    
    @TestSetup
    static void createData(){
        Pricebook2 lStandardPricebook = DataFactory.newPricebook();
        Database.update( lStandardPricebook );
        
        Account lAcc = DataFactory.newAccount();              
        Database.insert( lAcc );
        
        Opportunity lNewBusinessOpp = DataFactory.newOpportunity( lAcc.Id, lStandardPricebook, 'Client_Sales_New_Business', true );   
        Opportunity lRenegotiationOpp = DataFactory.newOpportunity( lAcc.Id, lStandardPricebook, 'Client_Success_Renegotiation', true );     
        Database.insert(new List<Opportunity>{lNewBusinessOpp, lRenegotiationOpp});

        Quote quoteNewBusiness = DataFactory.newQuote(lNewBusinessOpp, '1', 'Gympass_Plus');
        Quote quoteRenegotiation = DataFactory.newQuote(lRenegotiationOpp, '1', 'Gympass_Plus');
        Database.insert(new List<Quote>{quoteNewBusiness, quoteRenegotiation}); 
        
        /*Create operational case to launch the New business */
        Id ownerId = [ SELECT Id FROM Group WHERE Name = 'Deal Desk Operational' ].Id;

        Case OperationalCaseNB = DataFactory.newCase(lNewBusinessOpp.Id, quoteNewBusiness.Id, 'Deal_Desk_Operational');
        OperationalCaseNB.OwnerId = ownerId;

        Case OperationalCaseRN = DataFactory.newCase(lRenegotiationOpp.Id, quoteRenegotiation.Id, 'Deal_Desk_Operational');
        OperationalCaseRN.OwnerId = ownerId;

        Database.insert(new List<Case>{OperationalCaseNB, OperationalCaseRN}); 
        
        TriggerHandler.bypass('AccountTriggerHandler');

        /* create the contract to the opportunities */
        APXT_Redlining__Contract_Agreement__c contractAgreementNB = DataFactory.newContractAgreement(lNewBusinessOpp.Id, quoteNewBusiness.Id, lAcc.Id);
        contractAgreementNB.APXT_Redlining__Status__c = 'Active';
        contractAgreementNB.RecordTypeId = Schema.SObjectType.APXT_Redlining__Contract_Agreement__c.getRecordTypeInfosByDeveloperName().get('B2B_Contract').getRecordTypeId();

        APXT_Redlining__Contract_Agreement__c contractAgreementRN = DataFactory.newContractAgreement(lRenegotiationOpp.Id, quoteRenegotiation.Id, lAcc.Id);
        contractAgreementRN.APXT_Redlining__Status__c = 'Active';
        contractAgreementRN.RecordTypeId = Schema.SObjectType.APXT_Redlining__Contract_Agreement__c.getRecordTypeInfosByDeveloperName().get('B2B_Contract').getRecordTypeId();
        
        Database.insert(new List<APXT_Redlining__Contract_Agreement__c>{contractAgreementNB, contractAgreementRN});
    }

    @isTest
    public static void ContractAgreementTriggerTest(){
        Test.startTest();   
        
        List<Opportunity> oppsCreatedInSetup = [SELECT Id, RecordType.DeveloperName, StageName, SyncedQuoteId, AccountId FROM Opportunity];
        List<Quote> ltProposal = [SELECT Id, OpportunityId FROM Quote];                            
        
        Id proposalNewBusiness;
        Id oppNewBusiness;
        
        APXT_Redlining__Contract_Agreement__c newContract = new APXT_Redlining__Contract_Agreement__c();    
        newContract.APXT_Redlining__Account__c = oppsCreatedInSetup[0].AccountId;    
        newContract.RecordTypeId = Schema.SObjectType.APXT_Redlining__Contract_Agreement__c.getRecordTypeInfosByDeveloperName().get('B2B_Contract').getRecordTypeId();
        
        for(Opportunity opp : oppsCreatedInSetup){
            for(Quote proposal : ltProposal){
                if(proposal.OpportunityId == opp.Id){
                   if(opp.RecordType.DeveloperName == 'Client_Success_Renegotiation'){                
                       opp.Name = 'Renegotiation';
                       opp.SyncedQuoteId = proposal.Id;
                       opp.StageName = 'Lançado/Ganho';
                       opp.FastTrackStage__c = 'Offer Approved';
                       
                       newContract.Opportunity__c = opp.Id;
                       newContract.proposal__c = proposal.Id;
                    }else{                    
                    	opp.Name = 'New Business';
                        opp.SyncedQuoteId = proposal.Id;
                        opp.StageName = 'Lançado/Ganho';
                        opp.FastTrackStage__c = 'Offer Approved'; 
                        
                        oppNewBusiness = opp.Id;
                        proposalNewBusiness = proposal.Id;
                    }   
                }
            }                      
        }
        
        Database.update(oppsCreatedInSetup);

        APXT_Redlining__Contract_Agreement__c contractLastOppClosed = [SELECT Id FROM APXT_Redlining__Contract_Agreement__c WHERE proposal__c =: proposalNewBusiness];        
        insert newContract;        
        APXT_Redlining__Contract_Agreement__c contract = [SELECT APXT_Amendment_Version__c, APXT_Redlining__Contract_Agreement_Family_Parent__c, Parent_Opportunity__c, Parent_Proposal__c FROM APXT_Redlining__Contract_Agreement__c WHERE Id =: newContract.Id];        
        Test.stopTest();
        
        System.assertEquals(contract.APXT_Amendment_Version__c, 2);
        System.assertEquals(contract.APXT_Redlining__Contract_Agreement_Family_Parent__c, contractLastOppClosed.Id);        
        System.assertEquals(contract.Parent_Opportunity__c, oppNewBusiness);
        System.assertEquals(contract.Parent_Proposal__c, proposalNewBusiness);        
    }
}