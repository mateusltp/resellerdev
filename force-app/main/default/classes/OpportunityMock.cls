@IsTest
public class OpportunityMock {
  public static Opportunity getNewBusiness(
    Account account,
    Pricebook2 pricebook
  ) {
    Id recordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName()
      .get('Client_Sales_New_Business')
      .getRecordTypeId();

    return new Opportunity(
      AccountId = account.Id,
      CloseDate = Date.today(),
      CurrencyIsoCode = 'BRL',
      Name = account.Id,
      Pricebook2Id = pricebook.Id,
      RecordTypeId = recordTypeId,
      StageName = 'Qualification',
      Type = 'Expansion',
      Club_Management_System__c = 'Companhia Athletica',
      CMS_Used__c = 'Yes',
      Country_Manager_Approval__c = true,
      Gym_agreed_to_an_API_integration__c = 'Yes',
      Gympass_Plus__c = 'Yes',
      Integration_Fee_Deduction__c = 'No',
      Payment_approved__c = true,
      Request_for_self_checkin__c = 'Yes',
      Standard_Payment__c = 'Yes',
      Success_Look_Like__c = 'Yes',
      Success_Look_Like_Description__c = 'Money money'
    );
  }

  public static Opportunity getClientSalesSkuNewBusiness(
    Account account,
    Pricebook2 pricebook
  ) {
    Id recordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName()
      .get('Client_Sales_SKU_New_Business')
      .getRecordTypeId();

    return new Opportunity(
      AccountId = account.Id,
      CloseDate = Date.today(),
      CurrencyIsoCode = 'BRL',
      Name = account.Id + ' Client_Sales_SKU_New_Business',
      Pricebook2Id = pricebook.Id,
      RecordTypeId = recordTypeId,
      StageName = 'Qualification',
      Type = 'Expansion',
      Billing_Day__c = 1,
      Billing_Period__c = 'Monthly',
      Club_Management_System__c = 'Companhia Athletica',
      Cutoff_Day__c = 5,
      CMS_Used__c = 'Yes',
      Country_Manager_Approval__c = true,
      Gym_agreed_to_an_API_integration__c = 'Yes',
      Gympass_Plus__c = 'Yes',
      Integration_Fee_Deduction__c = 'No',
      Payment_approved__c = true,
      Payment_Due_Days__c = 5,
      Request_for_self_checkin__c = 'Yes',
      Standard_Payment__c = 'Yes',
      Success_Look_Like__c = 'Yes',
      Success_Look_Like_Description__c = 'Money money'
    );
  }

  public static Opportunity getClientSuccessSkuRenegotiation(
    Account account,
    Pricebook2 pricebook
  ) {
    Id recordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName()
      .get('Client_Success_SKU_Renegotiation')
      .getRecordTypeId();

    return new Opportunity(
      AccountId = account.Id,
      CloseDate = Date.today(),
      CurrencyIsoCode = 'BRL',
      Name = account.Id + 'Client_Success_SKU_Renegotiation',
      Pricebook2Id = pricebook.Id,
      RecordTypeId = recordTypeId,
      StageName = 'Qualification',
      Type = 'Expansion',
      Billing_Day__c = 1,
      Billing_Period__c = 'Monthly',
      Club_Management_System__c = 'Companhia Athletica',
      Cutoff_Day__c = 5,
      CMS_Used__c = 'Yes',
      Country_Manager_Approval__c = true,
      Gym_agreed_to_an_API_integration__c = 'Yes',
      Gympass_Plus__c = 'Yes',
      Integration_Fee_Deduction__c = 'No',
      Payment_approved__c = true,
      Payment_Due_Days__c = 5,
      Request_for_self_checkin__c = 'Yes',
      Standard_Payment__c = 'Yes',
      Success_Look_Like__c = 'Yes',
      Success_Look_Like_Description__c = 'Money money'
    );
  }
}