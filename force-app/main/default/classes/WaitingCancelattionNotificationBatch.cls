/**
 * @description       : 
 * @author            : ext.gft.pedro.oliveira@gympass.com
 * @group             : 
 * @last modified on  : 06-11-2021
 * @last modified by  : gft.samuel.silva@ext.gympass.com
 * Modifications Log 
 * Ver   Date         Author                             Modification
 * 1.0   05-11-2021   ext.gft.pedro.oliveira@gympass.com   Initial Version
**/ 
global without sharing class WaitingCancelattionNotificationBatch implements Database.Batchable<sObject>, Schedulable {
    private static String gSchedTime = '0 0 12 * * ?';  

    global WaitingCancelattionNotificationBatch() {}
    
    global Database.QueryLocator start(Database.BatchableContext BC) {

        return ((OpportunitySelector) Application.Selector.NewInstance(Opportunity.SObjectType)).selectPartnerToNotifyRenegotiationQueryLocator();
    }

    global void execute(Database.BatchableContext BC, List<Opportunity> oppOwners) {      	
        OpportunityService.NotifyOpportunityCancellation(oppOwners);     
    }        
                
    global void execute(SchedulableContext SC) {
        Database.executebatch(new WaitingCancelattionNotificationBatch(), 10);
    }
    
     
    global void finish(Database.BatchableContext BC) {}   
    
    global static void start(String schedTime){
        gSchedTime = schedTime == null || String.isBlank(schedTime) ?  '00 0 12 * * ?' : schedTime;      
        String jobName =  Test.isRunningTest() ? 'Waiting Cancelation Notification Test' : 'Waiting Cancelation Notification';
        System.Schedule(jobName, gSchedTime, new WaitingCancelattionNotificationBatch());
    }
}