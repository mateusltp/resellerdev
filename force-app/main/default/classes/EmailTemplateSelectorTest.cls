/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 08-10-2021
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/
@isTest(seeAllData=false)
public with sharing class EmailTemplateSelectorTest {
    
    @TestSetup
    static void createData(){            
     EmailTemplate e =PartnerDataFactory.newEmailTemplate();
     insert e; 
    }


    @isTest
    static void getSObjectFieldList_Test(){

        List<Schema.SObjectField> expectedReturn = new List<Schema.SObjectField> {
			EmailTemplate.Id
		};

        Test.startTest();
            System.assertEquals(expectedReturn, new EmailTemplateSelector().getSObjectFieldList());
        Test.stopTest();
    }

    @isTest
    static void getSObjectType_Test(){
        Test.startTest();
            System.assertEquals(EmailTemplate.sObjectType, new EmailTemplateSelector().getSObjectType());
        Test.stopTest();
    }

    @isTest
    static void selectById_Test(){

        EmailTemplate e = [SELECT id FROM EmailTemplate LIMIT 1];
        Test.startTest();
        System.assertEquals(e.Id, new EmailTemplateSelector().selectById(new Set<Id>{e.Id}).get(0).Id);
        Test.stopTest();
    }

    @isTest
    static void selectEmailTemplateByName_Test(){

        EmailTemplate e = [SELECT id, name FROM EmailTemplate LIMIT 1];
        Test.startTest();
        System.assertEquals(e.Id, new EmailTemplateSelector().selectEmailTemplateByName(e.name).id);
        Test.stopTest();
    }
}