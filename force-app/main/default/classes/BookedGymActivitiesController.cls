/**
 * @description       : 
 * @author            : alysson.mota@gympass.com
 * @group             : 
 * @last modified on  : 06-29-2022
 * @last modified by  : alysson.mota@gympass.com
**/
public without sharing class BookedGymActivitiesController {
    
    @AuraEnabled
    public static List<Gym_Activity_Relationship__c> getProdGymActivities(Id prodId) {
        ProductActivityRelationshipSelector parSelector = new ProductActivityRelationshipSelector();
        GymActivityRelSelector gaRelSelector = new GymActivityRelSelector();
        Set<Id> gaRelIdSet = new Set<Id>();
        
        List<Product_Activity_Relationship__c> parLst = parSelector.selectActivityByProductId(new Set<Id>{prodId});
        
        for (Product_Activity_Relationship__c par : parLst) {
            gaRelIdSet.add(par.Gym_Activity_Relationship__c);
        }

        return gaRelSelector.selectById(gaRelIdSet);
    }

    @AuraEnabled
    public static List<Id> getCurrentGymActivitiesBooked(Id prodId) {
        GymActivityToBeBookedSelector gymBookedSelector = new GymActivityToBeBookedSelector();
        List<Gym_Activity_To_Be_Booked__c> bookedGyms = gymBookedSelector.byProdId(new Set<Id>{prodId});
        List<Id> currentGymRelIdsBooked = new List<Id>();

        for (Gym_Activity_To_Be_Booked__c gymBooked : bookedGyms) {
            currentGymRelIdsBooked.add(gymBooked.GymActivityRelationshipId__c);
        }

        return currentGymRelIdsBooked;
    }

    @AuraEnabled
    public static void createGymActivitiesToBeBooked(Id prodId, List<Gym_Activity_Relationship__c> selectedGymActivities) {
        GymActivityToBeBookedService.createNewToProdFromGymActivityRels(prodId, selectedGymActivities);
    }
}