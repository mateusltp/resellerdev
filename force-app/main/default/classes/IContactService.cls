/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 12-15-2021
 * @last modified by  : gft.samuel.silva@ext.gympass.com
 * Modifications Log 
 * Ver   Date         Author                             Modification
 * 1.0   07-22-2021   gft.samuel.silva@ext.gympass.com   Initial Version
**/
public interface IContactService {

    void updateEmailAddress(List<Contact> aContactList );
    ID getRecordTypeId(String recordTypeDevName);
    void updateAccountLegalRepresentative(List<Contact> contacts);
    void updateAccountLegalRepresentative(List<Contact> contacts, Map<Id, Contact> oldContactsByIds);
    void updateRelationshipRole(List<Contact> contacts);
    void updateRelationshipRole(List<Contact> contacts, Map<Id, Contact> oldContactsByIds);
    void updateAccountsWithExistingLegalRepresentative(List<Id> accountIds, Id selectedLegalRepContactId);
    void updateAccountsWithNewLegalRepresentative(List<Id> accountIds, String newContactSerialized);
}