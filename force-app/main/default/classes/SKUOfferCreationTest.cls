/**
 * @description       : 
 * @author            : tiago.ribeiro@gympass.com
 * @group             : 
 * @last modified on  : 05-17-2022
 * @last modified by  : tiago.ribeiro@gympass.com
**/
@isTest
public with sharing class SKUOfferCreationTest {
    @TestSetup
    static void setupData(){
        Account acc = DataFactory.newAccount();   
        insert acc;
        
        Pricebook2 standardPricebook = new Pricebook2(
            Id = Test.getStandardPricebookId(),
            IsActive = true
        );
        update standardPricebook;
        Pricebook2 pb = standardPricebook;

        Product2 lAcessFee = DataFactory.newProduct( 'Enterprise Subscription' , false , 'BRL' );
        lAcessFee.Payment_Type__c = 'Recurring fee';
        Database.insert( lAcessFee );

        Opportunity opp = DataFactory.newOpportunity( acc.Id, standardPricebook, 'Client_Sales_SKU_New_Business' );
        opp.Billing_Period__c = 'Monthly';
        opp.TotalOpportunityQuantity = 1000;
        
        PricebookEntry lAcessFeeESEntry = DataFactory.newPricebookEntry( standardPricebook , lAcessFee, opp);
        insert lAcessFeeESEntry;

        SKU_Price__c skuPrice = DataFactory.newSKUPrice(lAcessFee.Id, opp, acc, 1, 50);
        insert skuPrice;
        
        insert opp;

        Product2 lAcessFamily = DataFactory.newProduct( 'SKU Family Subscription' , false , 'BRL' );
        lAcessFamily.Payment_Type__c = 'One time fee';
        Database.insert( lAcessFamily );

        PricebookEntry lAccessFamilyEntry = DataFactory.newPricebookEntry( standardPricebook , lAcessFamily , opp );
        insert lAccessFamilyEntry;
        
        SKU_Price__c skuPriceFamily = DataFactory.newSKUPrice(lAcessFamily.Id, opp, acc, 1, 100);
        insert skuPriceFamily;

        Quote lQuote = [SELECT Id FROM Quote WHERE OpportunityId = :opp.Id];
        
        QuoteLineItem lQuoteAccessFamilyFee = DataFactory.newQuoteLineItem( lQuote , lAccessFamilyEntry );
        Database.insert( lQuoteAccessFamilyFee );
    }

    @isTest
    static void testCreateNewPayments(){
        List<Opportunity> listOpps = [SELECT Id, AccountId, Billing_Period__c FROM Opportunity LIMIT 1];
        SKUOfferCreation.createPayments(listOpps);
        List<Payment__c> listPayments = [SELECT Id FROM Payment__c WHERE Opportunity__c = :listOpps[0].Id];

        System.assertEquals(listPayments.size(), 2, 'Payment was not created for Recurring and Non Recurring Fee');
    }

    @isTest
    static void testRemovalNonRecurring(){
        List<Opportunity> listOpps = [SELECT Id, AccountId, Billing_Period__c FROM Opportunity LIMIT 1];
        SKUOfferCreation.createPayments(listOpps);

        List<QuoteLineItem> listItemsNonRecurring = [SELECT Id FROM QuoteLineItem WHERE Quote.OpportunityId = :listOpps[0].Id AND Product2.Payment_Type__c = 'One time fee'];
        delete listItemsNonRecurring;

        SKUOfferCreation.mapOppPaymentsToCreateByRecordType = new Map<Id, Set<Id>>();
        SKUOfferCreation.listPaymentsToCreate = new List<Payment__c>();
        SKUOfferCreation.createPayments(listOpps);
        List<Payment__c> listPayments = [SELECT Id FROM Payment__c WHERE Opportunity__c = :listOpps[0].Id];

        System.assertEquals(listPayments.size(), 1, 'Payment Non Recurring fee was not removed');
    }
}