public without sharing class ResellerLogDomain {

    public String action{get;private set;}
    public String message{get;private set;}
    public String tracking{get;private set;}    
    public String type{get;private set;}
    public String lineNumber{get;private set;}
    public String referenceId{get; private set;}
    public String referenceSobject{get;private set;}
    public String userId{get;private set;}

    public ResellerLogDomain(String action, String message, String tracking, String type, String lineNumber, String referenceId, String referenceSobject){
        this.action = action;
        this.message = message;
        this.tracking = tracking;
        this.type = type;
        this.lineNumber = lineNumber;
        this.referenceId = referenceId;
        this.referenceSobject = referenceSobject;
        this.userId = UserInfo.getUserId();
    }

}