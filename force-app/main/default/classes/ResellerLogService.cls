public with sharing class ResellerLogService {

    public static void publishLogs(List<ResellerLogDomain> domains){
        List<Reseller_Log_Error__e> logs = new List<Reseller_Log_Error__e>();
        for(ResellerLogDomain domain : domains)
            logs.add(new Reseller_Log_Error__e( Action__c = domain.action, 
                                                Error_Message__c = domain.message,
                                                Error_Tracking__c = domain.tracking,
                                                Error_Type__c = domain.type,
                                                Line_Number__c = domain.lineNumber,
                                                Reference_Id__c = domain.referenceId,
                                                Reference_SObject__c = domain.referenceSobject,
                                                User_Id__c = domain.userId));
        
        EventBus.publish(logs);
    }

}