public with sharing class FastTrackResellerProposalController {
    public FastTrackResellerProposalController() {}

    @AuraEnabled
    public static FastTrackProposalCreationTO find(String recordId, Integer totalOfEmployees,  List<Id> accountsInOpp){
        FastTrackProposalCreationTO to = new FastTrackProposalCreationBuilder()
                    .withOpportunity(recordId)
                    .withLastCreatedQuoteWhenExists(recordId)
                    .withDealHierarchy(totalOfEmployees, accountsInOpp)
                    .withOpsSetupForm(recordId)
                    .build();    

        return to;
    }

    @AuraEnabled
    public static List<Account> findGympassEntities(){
        AccountRepository accounts = new AccountRepository();
        return accounts.gympassEntities();
    }

    @AuraEnabled
    public static List<Contact> findClientManagers(String recordId){
        OpportunityRepository opportunities = new OpportunityRepository();
        ContactRepository contacts = new ContactRepository();
        SObject opportunity =  opportunities.byId(recordId);
        return contacts.fromAccount( (String) opportunity.get('AccountId') );
    }

    @AuraEnabled
    public static FastTrackResellerStageCmpMapping__mdt[] getStageCmpMappingMetadata(){
        FastTrackResellerStageCmpMapping__mdt[] lLstFastTrackStageMapping = 
            [ SELECT Label, DeveloperName, ChildComponentApiName__c, ChildComponentParameters__c FROM FastTrackResellerStageCmpMapping__mdt ];
        return lLstFastTrackStageMapping;
    }

    @AuraEnabled
    public static FastTrackProposalCreationTO saveProposal(String proposalData){ 
        System.debug('saveProposal');
        FastTrackProposalCreationTO proposal = (FastTrackProposalCreationTO) JSON.deserialize(proposalData, FastTrackProposalCreationTO.class); 
        System.debug('proposal '+ proposal);
        FastTrackProposalCreation fastTrackProposalCreation = new FastTrackProposalCreation( proposal.opportunityId );
        System.debug('fastTrackProposalCreation '+ fastTrackProposalCreation);
        fastTrackProposalCreation.save(proposal);   
        System.debug('fastTrackProposalCreation.save');
        fastTrackProposalCreation.refreshRevenues(proposal); 
        System.debug('fastTrackProposalCreation.refreshRevenues');
        return proposal;
    }

    @AuraEnabled
    public static FastTrackProposalCreationTO removeItems(String proposalData){ 
        FastTrackProposalCreationTO proposal = (FastTrackProposalCreationTO) JSON.deserialize(proposalData, FastTrackProposalCreationTO.class);        
        FastTrackProposalCreation fastTrackProposalCreation = new FastTrackProposalCreation( proposal.opportunityId );
        fastTrackProposalCreation.removeCurrentItems(proposal.quoteId);      
        return proposal;
    }
        
    @AuraEnabled
    public static void saveAccountWithOpportunity(ID oppId, List<Account> acctLst){        
        FastTrackProposalCreation fastTrackProposalCreation = new FastTrackProposalCreation( oppId );
        fastTrackProposalCreation.saveAccountWithOpportunity(oppId,acctLst);
    }

    @AuraEnabled
    public static void removeAccountFromOpportunity(ID oppId, List<Account> acctLst){        
        FastTrackProposalCreation fastTrackProposalCreation = new FastTrackProposalCreation( oppId );
        fastTrackProposalCreation.removeAccountFromOpportunity(oppId, acctLst);
    }

    @AuraEnabled
    public static void removeAccountRelationshipFromOpportunity(ID oppId, List<Account_Opportunity_Relationship__c> acctLst){   
        AccountOpportunityRepository accountsInOpp = new AccountOpportunityRepository(oppId);
        accountsInOpp.deleteRelationshipAccountWithOpp(acctLst);
    }

    @AuraEnabled
    public static Object getSetUpDealHierarchy(ID oppId){   
        AccountRepository accounts = new AccountRepository();    
        OpportunityRepository opportunities = new OpportunityRepository();
        AccountOpportunityRepository accountsInOpp = new AccountOpportunityRepository(oppId);
        Opportunity opp = opportunities.byId(oppId);        
        Account accOpp = accounts.byId(opp.AccountId);

        Set<Id> accountsRelatedIds = accountsInOpp.findRelatedAccounts();

        if(!accountsRelatedIds.contains(accOpp.Id)){
            saveAccountWithOpportunity(opp.Id, new List<Account>{accOpp});
            accountsRelatedIds = accountsInOpp.findRelatedAccounts();
        }
                
        Set<Id> childAccounts = accounts.findChildAccounts(opp.AccountId);
        System.debug(childAccounts);
        Set<Account> allAccountsInHierarchy = new Set<Account>(accounts.getAccounts(childAccounts));
        //allAccountsInHierarchy.add(accOpp);
        Set<Account> accountNotAvailable = new Set<Account>();
        Integer totalEmployees = accOpp.NumberOfEmployees;
        System.debug(totalEmployees);
        Map<String, Object> setUpDealHierarchy = new Map<String, Object>(); 
        System.debug(allAccountsInHierarchy);       
        if(!allAccountsInHierarchy.isEmpty()){
            for(Account acc : allAccountsInHierarchy){
                if(accountsRelatedIds.contains(acc.Id)){
                    System.debug(acc.NumberOfEmployees);
                    if(acc.NumberOfEmployees!= null){
                        totalEmployees += acc.NumberOfEmployees;
                    }   
                    accountNotAvailable.add(acc);
                }
            }
        }       
        allAccountsInHierarchy.removeAll(accountNotAvailable);
        setUpDealHierarchy.put('accAvailable', allAccountsInHierarchy);
        setUpDealHierarchy.put('accRelatedToOpp', accountNotAvailable);
        setUpDealHierarchy.put('numberOfEmployees', totalEmployees);
        return setUpDealHierarchy;
    }

    @AuraEnabled
    public static FastTrackProposalCreationTO setStage(String newTo, integer index){
        //newTo = JSON.serialize(newTo);
        FastTrackProposalCreationTO to = (FastTrackProposalCreationTO)JSON.deserialize(newTo, FastTrackProposalCreationTO.class);
        FastTrackProposalCreation fastTrackProposalCreation = new FastTrackProposalCreation( to.opportunityId );        
        QuoteRepository quotes = new QuoteRepository();
        OpportunityRepository opportunities = new OpportunityRepository();       
        try{
            Opportunity opportunity = fastTrackProposalCreation.refreshOpportunityForNewStage(to.opportunityId, index);
            switch on opportunity.FastTrackStage__c {
                when 'Offer Review'  {
                        Quote quote = quotes.lastForOpportunity(opportunity.Id);
                    	FastTrackProposalCreationController.proposalEvaluation(opportunity.Id);
                        //opportunities.sync(quote.Id, opportunity.Id);
                    }
                // when 'Setup'{
                //     try {                 
                //         to.withOpsForm(fastTrackProposalCreation.saveClientOpsForm(to, Database.setSavepoint()));
                //     } catch (Exception e){
                //         throw new AuraHandledException('Can not save Ops Setup Form for Id: '+ to.opsSetupForm.opsFormId + '. Error: ' + e.getMessage());
                //     }
                // }            
            }          
            to.fastTrackStage = opportunity.FastTrackStage__c;
            return to;
        } catch (Exception e){
            throw new AuraHandledException(e.getMessage());
        }
    }    
    
    @AuraEnabled
    public static FastTrackProposalCreationTO approveOrRejectDealDesk(Id oppId, Id recordId, Boolean isApproved){
        AccountOpportunityRepository accOppRep = new AccountOpportunityRepository(oppId);
        FastTrackProposalCreationTO to = FastTrackProposalCreationController.find(oppId, accOppRep.totalOfEmployees, accOppRep.accountsInOpp);
        String lRecordtypeDevName = [ SELECT Id, Recordtype.DeveloperName FROM Case WHERE Id =: recordId ].Recordtype.DeveloperName;
        try {       
            FastTrackDealDeskHandler lDeadDeskHandle = FastTrackDealDeskHandler.getInstanceByOppId( oppId , to );

            return lRecordtypeDevName == 'Deal_Desk_Operational' ?
                lDeadDeskHandle.approveOrRejectDealDeskSpecialist(oppId, recordId, isApproved) :
                lDeadDeskHandle.approveOrRejectDealDeskCase(recordId, isApproved);
        }  catch (Exception e) {
            System.debug(' ERROR ' + e.getMessage());
            throw new AuraHandledException('ERROR ' + e.getMessage());
        }
    }

    @AuraEnabled
    public static FastTrackProposalCreationTO proposalEvaluation(Id oppId){   
        AccountOpportunityRepository accOppRep = new AccountOpportunityRepository(oppId);
        FastTrackProposalCreationTO to = FastTrackProposalCreationController.find(oppId, accOppRep.totalOfEmployees, accOppRep.accountsInOpp);
        System.debug('TO Pronto pra evaluation ' + to);        
        FastTrackProposalCreation fastTrackProposalCreation = new FastTrackProposalCreation( to.opportunityId );
        fastTrackProposalCreation.resetConditionToApproval(to, 1);       
        fastTrackProposalCreation.resetConditionToApproval(to, 2); 

        FastTrackDealDeskHandler lDealDeskHandler = FastTrackDealDeskHandler.getInstanceByOppId( oppId , to );
        lDealDeskHandler.dealDeskEvaluation();
        
        FastTrackCommercialConditionsHandler.getInstanceByOppId( oppId , to ).commercialConditionsEvaluation();
        return to;
    }
  
    @AuraEnabled
    public static FastTrackProposalCreationTO submitForApproval (String jsonTO){
        System.debug('jsonTO '+ jsonTO);
        FastTrackProposalCreationTO oldTo = (FastTrackProposalCreationTO)JSON.deserialize(jsonTO, FastTrackProposalCreationTO.class);
          
        FastTrackProposalCreationTO to = new FastTrackProposalCreationBuilder()
                    .withOpportunity(oldTo.opportunityId)
                    .withLastCreatedQuoteWhenExists(oldTo.opportunityId)
                    .build();       
        
        to.proposal.dealDeskDescription =  oldTo.proposal.dealDeskDescription;
        to.proposal.commercialJustification = oldTo.proposal.commercialJustification;
        to.proposal.accessFee.waiversJustification = oldTo.proposal.accessFee.waiversJustification;
        
        FastTrackDealDeskHandler dealDeskHandler = FastTrackDealDeskHandler.getInstanceByOppId( oldTo.opportunityId , to );

        if(to.proposal.sObj.Deal_Desk_Approval_Needed__c && !to.proposal.sObj.Deal_Desk_Approved__c) {
            dealDeskHandler.submitCaseForApproval();
        }
        FastTrackCommercialConditionsHandler commHandler = FastTrackCommercialConditionsHandler.getInstanceByOppId( oldTo.opportunityId , to );
        commHandler.submitForApproval();
        return to;
    }

    @AuraEnabled
    public static void resetCommercialConditions(FastTrackProposalCreationTO to){        
        to = new FastTrackProposalCreationBuilder()
                    .withOpportunity(to.opportunityId)
                    .withLastCreatedQuoteWhenExists(to.opportunityId)
                    .build();
        new FastTrackProposalCreation( to.opportunityId ).resetDealDeskAndCommercialConditions(to); 
        new FastTrackProposalCreation( to.opportunityId ).refreshRevenues(to);   

        FastTrackDealDeskHandler lDealDeskHandler = FastTrackDealDeskHandler.getInstanceByOppId( to.opportunityId , to );
        lDealDeskHandler.dealDeskEvaluation();

        FastTrackCommercialConditionsHandler.getInstanceByOppId( to.opportunityId , to ).commercialConditionsEvaluation();
    }

    @AuraEnabled
    public static List<Account_Opportunity_Relationship__c> getAccOppRelationships(Id oppId){
        return new AccountOpportunityRepository(oppId).getAccountsInOpp();
    }

    @AuraEnabled
    public static void saveAccOppRelationships( List<Account_Opportunity_Relationship__c> accOppRelationships, String ltAcc ){
        List<Account> ltAccount = (List<Account>)JSON.deserialize(ltAcc, List<Account>.class);
        try{
            new AccountOpportunityRepository().edit(accOppRelationships);
            new AccountRepository().edit(ltAccount);
        } catch(Exception except) {
            throw new AuraHandledException('There was a problem while updating. Error Message:'+ except.getMessage());
        }
    }

    @AuraEnabled
    public static Case getDealDeskOperationalCase(String oppId){
        Group lOperationalQueue;
        Case lOperationalCase;
        try{
            List<Case> lLstDealDeskOperationalCases = new CaseRepository().getCaseByOppIdAndRTDevName( oppId , 'Deal_Desk_Operational' );

            lOperationalCase = lLstDealDeskOperationalCases.isEmpty() ?
                new Case() : lLstDealDeskOperationalCases[0];

            lOperationalQueue = [ SELECT Id FROM Group WHERE Name = 'Deal Desk Operational' and Type = 'Queue' ];

            lOperationalCase.Subject = 'Deal Desk Operational Approval Request';
            lOperationalCase.OwnerId = lOperationalQueue == null ? null : lOperationalQueue.Id;
            
            return lOperationalCase;
        } catch(Exception except) {
            throw new AuraHandledException(except.getMessage());
        }
    }
}