public without sharing class TagusOrderOutboundPublisher {
  private final String CREATE_EVENT = 'CREATE_ORDER_ON_TAGUS';
  private final String UPDATE_EVENT = 'UPDATE_ORDER_ON_TAGUS';

  private List<Order> orders;

  public TagusOrderOutboundPublisher(List<Order> orders) {
    this.orders = orders;
  }

  public EventQueue runCreate() {
    return publishEvent(CREATE_EVENT);
  }

  public EventQueue runUpdate() {
    return publishEvent(UPDATE_EVENT);
  }

  private EventQueue publishEvent(String eventName) {
    EventQueue event = new EventBuilder()
      .createEventFor(eventName)
      .withSender('SALESFORCE')
      .withReceiver('TAGUS')
      .buildEvent();

    List<OrderTagusDTO> eventRequests = OrderTagusDAO.getOrderDtoByOrders(
      this.orders
    );

    event.addPayload(eventName, JSON.serialize(eventRequests));

    event.save();

    return event;
  }
}