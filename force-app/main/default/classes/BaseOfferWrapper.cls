/**
 * @author vncferraz
 * 
 * Provide abstraction layer for OfferWrapper on OfferProcessor to Offer_Queue__c
 */
public abstract class BaseOfferWrapper {
    
    public class ProcessResult {
        public String status;
        public String statusDetail;
    }

    protected Offer_Queue__c offerQueue;   
    protected boolean hasError;
    protected List<String> errors;

    public BaseOfferWrapper(){
        this.hasError = false;
        this.errors = new List<String>();
    }
    
    public void addError(String message){
        this.hasError = true;
        this.errors.add(message);
    }

    public boolean hasError(){
        return this.hasError;
    }

    public abstract void setOpportunity(Opportunity opp);

    public abstract void setProposal(Quote proposal);

    public abstract void setEnterpriseSubscription(QuoteLineItem enterpriseSubscription);

    public abstract void setBillingSettings(Payment__c billingSettings);

    public abstract void setEligibility(Eligibility__c eligibility);

    public abstract void setWaivers(List<Waiver__c> waiver);

    public abstract void setAccountInOpportunity(Account_Opportunity_Relationship__c accountInOpportunity);

    public abstract Opportunity getOpportunity();

    public abstract Quote getProposal();

    public abstract ProcessResult finish();

    public abstract void saveOpportunity();
    
    public abstract void saveProposal();

    public abstract void saveEnterpriseSubscription();

    public abstract void saveBillingSettings();

    public abstract void saveEligibility();

    public abstract void saveWaivers();

    public abstract void saveAccountInOpportunity();

    
    
}