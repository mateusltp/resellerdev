/**
 * @File Name          : StepsTowardsSuccessForGyms.cls
 * @Description        : 
 * @Author             : GEPI@GFT.com
 * @Group              : 
 * @Last Modified By   : GEPI@GFT.com
 * @Last Modified On   : 08-07-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    02/04/2020   Samuel Silva - GFT (slml@gft.com)     Initial Version
**/
public without sharing class StepsTowardsSuccessForGyms {
    
    public static StepsTowardsSuccessCtrl.Response getResponse(String recordId) {       
        StepsTowardsSuccessCtrl.Response response = null;  
        response = get10StepsForGym(recordId);	        
        return response;
    }
  
    public static StepsTowardsSuccessCtrl.Response get10StepsForGym(String recordId) {
		StepsTowardsSuccessCtrl.Response response = new StepsTowardsSuccessCtrl.Response();        
        response.title = 'Steps Towards Success';
       	response.stepsTowardsSuccessWrapper = getStepsWrapperList(recordId);
       	response.isVisible = true; 
        return response;
    }  
  
    public static List<StepsTowardsSuccessCtrl.StepsTowardsSuccessWrapper> getStepsWrapperList(String recordId) {
        
        String sObjectType = ((Id)recordId).getSobjectType().getDescribe().getName();
        Id stepRecordType = null;
        Opportunity currentOpp = null;
        
        List<Step_Towards_Success_Partner__c> stepsTowardsSuccess = null; 
               
        if(sObjectType == 'Account'){         
            stepRecordType = Schema.SObjectType.Step_Towards_Success_Partner__c.getRecordTypeInfosByName().get('Gym Partner - Account Step').getRecordTypeId();                
            stepsTowardsSuccess = [
                SELECT Id, Step_Number__c, Achieved__c, Date_Achieved__c, Name
                FROM Step_Towards_Success_Partner__c
                WHERE RecordTypeId =: stepRecordType
                AND Related_Account__c =: recordId
            ];
            
        } else {
            currentOpp = [SELECT id, Standard_Payment__c,Club_Management_System__c, StageName,RecordType.DeveloperName,accountId,Achieved_Score__c, CMS_Used__c,Success_Look_Like__c,Total_Locations__c,Number_of_Locations_Opportunity__c FROM Opportunity WHERE Id =: recordId];          
            stepRecordType = Schema.SObjectType.Step_Towards_Success_Partner__c.getRecordTypeInfosByName().get('Gym Partner - Opportunity Step').getRecordTypeId();
            updateNumberOfLocationInOpportunity(currentOpp);
            
            stepsTowardsSuccess = [
                SELECT Id, Step_Number__c, Achieved__c, Date_Achieved__c, Name
                FROM Step_Towards_Success_Partner__c
                WHERE RecordTypeId =: stepRecordType
                AND Related_Opportunity__c =: currentOpp.Id
            ];
        }
        
        if (stepsTowardsSuccess.size() != 10) {            
            Database.delete(stepsTowardsSuccess);
            stepsTowardsSuccess = createGymSteps(sObjectType, recordId);
        }  
       
        List<StepsTowardsSuccessCtrl.StepsTowardsSuccessWrapper> stepsTowardsSuccessWrapperList = verify10StepsInHierarchy(stepsTowardsSuccess, currentOpp, recordId);
        
        return stepsTowardsSuccessWrapperList;
    }
   
    
    private static List<StepsTowardsSuccessCtrl.StepsTowardsSuccessWrapper> verify10StepsInHierarchy(List<Step_Towards_Success_Partner__c> stepsTowardsSuccess, Opportunity currentOpp, String recordId) {
        
        String sObjectType = ((Id)recordId).getSobjectType().getDescribe().getName();       
        Set<Id> accsId = new Set<Id>();
        Account currentAcc = null;
        List<Account> accInHierarchy = new List<Account>();
        List<Account_Opportunity_Relationship__c> relatedObjAccToOpp =  new List<Account_Opportunity_Relationship__c>();
        List<Opportunity> oppInHierarchy = new List<Opportunity>();  
        AccountHierarchy accHiearchyObj = new AccountHierarchy();                  
        Integer tempScore = 0;
        Integer numberOfLocationsForChild = 0;
        Boolean isInheritedOpp = false;
        Boolean isHeadquarterWithNoOpp = false;
        Boolean isBrandWithNoOpp = false;
        
        if(sObjectType=='Opportunity') {
            currentAcc = [SELECT Id,Site, Achieved_Score__c, Exclusivity__c,Gym_Type__c, Number_of_Locations__c FROM Account WHERE Id =: currentOpp.accountId];
            accsId.add(currentOpp.AccountId);            
            accHiearchyObj.findlAllChildAccounts(accsId);
            accsId.addAll(accHiearchyObj.getTotalAccounts());
            accHiearchyObj.updateNumberOfLocations(currentAcc, accsId.size()-1);
        
            relatedObjAccToOpp = [
                SELECT Account__c FROM Account_Opportunity_Relationship__c
                WHERE Account__c IN: accsId AND Opportunity__c =: currentOpp.id
            ];                              
            if(relatedObjAccToOpp.size() > 0){
                Set<Id> relatedAccounts = new Set<id>();
                for(Account_Opportunity_Relationship__c aor : relatedObjAccToOpp){
                    relatedAccounts.add(aor.Account__c);
                }                       
                accInHierarchy = [SELECT Id,Site, Gym_Type__c, Exclusivity__c ,Number_of_Locations__c,Achieved_Score__c FROM Account WHERE Id IN : relatedAccounts];
            
            }            
            accInHierarchy.add(currentAcc);
            oppInHierarchy.add(currentOpp);  
                    
        } else {
            accsId.add(recordId);
            currentAcc = [SELECT Id,Site, Gym_Type__c, Exclusivity__c,Achieved_Score__c, Number_of_Locations__c FROM Account WHERE Id =: recordId];
            accHiearchyObj.findlAllChildAccounts(accsId);
            accsId.addAll(accHiearchyObj.getTotalAccounts());   
            accHiearchyObj.updateNumberOfLocations(currentAcc, accsId.size()-1);
              
            accInHierarchy = [SELECT Id,Site, Gym_Type__c, Name, Exclusivity__c,Achieved_Score__c, Number_of_Locations__c FROM Account WHERE Id IN : accsId];   
            oppInHierarchy = [SELECT Id,Club_Management_System__c,Standard_Payment__c,AccountId, Name, CMS_Used__c,Success_Look_Like__c,Total_Locations__c,Number_of_Locations_Opportunity__c FROM Opportunity WHERE AccountID =: currentAcc.Id AND StageName = 'Lançado/Ganho'];
            
            if(oppInHierarchy.size() == 0) {                
                if(currentAcc.Site == 'Franchisor'){
                    isHeadquarterWithNoOpp = true;
                } else if(currentAcc.Site == 'Brand'){
                    isBrandWithNoOpp = true;
                }
                relatedObjAccToOpp = [
                    SELECT Opportunity__c FROM Account_Opportunity_Relationship__c
                    WHERE Account__c =: currentAcc.Id                
                ]; 
            
                if(relatedObjAccToOpp.size() > 0) {
                    Set<Id> relatedOpp = new Set<id>();
                    for(Account_Opportunity_Relationship__c aor : relatedObjAccToOpp){
                        relatedOpp.add(aor.Opportunity__c);
                    } 
                    oppInHierarchy = [SELECT Id,Club_Management_System__c,Standard_Payment__c, AccountId, CMS_Used__c,Success_Look_Like__c,Total_Locations__c,Number_of_Locations_Opportunity__c FROM Opportunity WHERE Id IN: relatedOpp AND StageName = 'Lançado/Ganho'];  
                    if(oppInHierarchy.size() > 0){
                        numberOfLocationsForChild = (Integer)oppInHierarchy.get(0).Total_Locations__c;
                        isInheritedOpp = true;
                    } else  {
                        currentAcc.Achieved_Score__c = tempScore;
                        UPDATE currentAcc;
                        return generateFakeStepResults();
                    }                
                        
                } else {
                    oppInHierarchy = [SELECT Id,Club_Management_System__c,Standard_Payment__c,AccountId, CMS_Used__c,Success_Look_Like__c,Total_Locations__c,Number_of_Locations_Opportunity__c FROM Opportunity WHERE AccountId IN: accsId AND StageName = 'Lançado/Ganho'];
                    if(oppInHierarchy.size() == 0){
                        currentAcc.Achieved_Score__c = tempScore;
                        UPDATE currentAcc;
                        return generateFakeStepResults();
                    }                    
                       
                }
            }
        }
       
        Set<Id> oppIds = new Set<Id>();        
        Set<Id> allAccountsToCheck = new Set<Id>();
        
        if(isInheritedOpp){
            for(Opportunity o : oppInHierarchy){
                oppIds.add(o.Id);
                allAccountsToCheck.add(o.AccountId);
            }
        } else {
            for(Opportunity o : oppInHierarchy){
                oppIds.add(o.Id);
            }
        }
        allAccountsToCheck.addAll(accsId);
        
        List<Product_Item__c> allProductInOpp = [SELECT Id, Opportunity__c,
                                                    Opportunity__r.Account_Billing_Country__c,
                                                    Opportunity__r.Account.Gym_Type__c,
                                                    Reference_Price_Value_Unlimited__c,
                                                    Max_Monthly_Visit_Per_User__c,
                                                    CAP_Value__c, Net_Transfer_Price__c,
                                                    Price_Visits_Month_Package_Selected__c
                                                    FROM Product_Item__c 
                                                    WHERE Opportunity__c IN: oppIds];
                                                    
    
                                                    
        List<String> cLevelRoles = new List<String>{'CEO', 'CFO', 'CHRO', 'Other C - Level'};
    
        List<Event> events = [
        	SELECT Id, WhoId, WhatId
            FROM Event
            WHERE (WhatId IN: oppIds AND StartDateTime = LAST_N_DAYS:120)
            OR (WhatId IN: allAccountsToCheck AND StartDateTime = LAST_N_DAYS:90)
        ];
        
        Set<Id> contactsToCheck = new Set<Id>();

        for(Event e : events){
            contactsToCheck.add(e.whoId);
        }
        List<Contact> contacts = [
            SELECT Id
            FROM Contact
            WHERE (Id IN: contactsToCheck)
            AND Cargo__c IN :cLevelRoles
        ];

        /*
        System.debug('allAccountsToCheck' + allAccountsToCheck);
        System.debug('Contacts ' + contacts);
        System.debug('EVENTS ' + events);
        
        Map<Id, Set<Id>> objWithContact = new Map<Id, Set<Id>>();
        Set<Id> allContacts = new Set<Id>();
        for(Event e : events){
            for(Contact c : contacts) {
                System.debug('Participante do evento ' + e.whoId + '== Contact ' + c.Id);
                if(e.whoId == c.Id)
                    allContacts.add(c.Id);
            }
            if(allContacts.size()> 0){
                objWithContact.put(e.whatId, allContacts);
            }
        } 
        
        System.debug('MAPA DE CONTACTS CM OBJ ' + objWithContact);*/
        
        List<Quote> quotesInOpps = [
            SELECT id, Start_Date__c, Final_Date__c, OpportunityId,PR_Activity__c, discounts_usage_volume_range__c,
            Threshold_Value_1__c, Threshold_Value_2__c, Threshold_Value_3__c,Threshold_Value_4__c,
            First_Discount_Range__c,Second_Discount_Range__c,Third_Discount_Range__c,Fourth_Discount_Range__c, Country__c
            FROM Quote
            WHERE  OpportunityId IN: oppIds       
        ];

        //BUSCAR METADT AQUI E PASSAR PRO METODO
        List<Standard_Volume_Discount__mdt> StaVolDisList = [SELECT MasterLabel, X1st_Discount_Range1__c, X2nd_Discount_Range2__c,
         X3rd_Discount_Range3__c, X4th_Discount_Range4__c,
          Threshold_Value_1__c, Threshold_Value_2__c,
          Threshold_Value_3__c, Threshold_Value_4__c 
        FROM Standard_Volume_Discount__mdt];
        Map<String, Standard_Volume_Discount__mdt> StaVolDisMap = new Map<String, Standard_Volume_Discount__mdt>();
        for (Standard_Volume_Discount__mdt  sdv : StaVolDisList ) {
             StaVolDisMap.put(sdv.MasterLabel,sdv);
        }
        
        List<StepsTowardsSuccessCtrl.StepsTowardsSuccessWrapper> listStepsTowardsSuccessWrapper = new List<StepsTowardsSuccessCtrl.StepsTowardsSuccessWrapper>();
        List<Step_Towards_Success_Partner__c> stepsToUpdate = new List<Step_Towards_Success_Partner__c>();
        Map<Decimal, Step_Towards_Success_Partner__c> mapStepsTowardsSucess = new Map<Decimal, Step_Towards_Success_Partner__c>();
        Map<Decimal, Step_Towards_Success_Definition__mdt> stepNumbeToStepTowardsSuccessDefinition = getMap10StepsTowardsSucessDefinition();
        
        for (Step_Towards_Success_Partner__c step : stepsTowardsSuccess) {
			mapStepsTowardsSucess.put(step.Step_Number__c, step);
        }
        
        for (Integer i = 1; i <= 10; i++) {
            if (mapStepsTowardsSucess.get(i) != null) {  
                Boolean isAchieved = false;              
            	Step_Towards_Success_Partner__c stepTowardsSuccess = mapStepsTowardsSucess.get(i);
                stepTowardsSuccess.Achieved__c = 'No';                
                StepsTowardsSuccessCtrl.StepsTowardsSuccessWrapper stepsTowardsSuccessWrapper = new StepsTowardsSuccessCtrl.StepsTowardsSuccessWrapper();
                stepsTowardsSuccessWrapper.label = (i < 10 ? '0' + i : '' + i) + ' - ' + stepTowardsSuccess.Name;
           		stepsTowardsSuccessWrapper.alternativeText = stepNumbeToStepTowardsSuccessDefinition.get(i).Description__c;
                stepsTowardsSuccessWrapper.option = 'negative';
                                 
                switch on (Integer)stepTowardsSuccess.Step_Number__c {
                    
                    when 1{                        
                        isAchieved = verifyCapPercentage(allProductInOpp);
                        if(isAchieved){
                            stepsTowardsSuccessWrapper.option = 'positive';
                            stepTowardsSuccess.Achieved__c = 'Yes';
                            tempScore++;                            
                        } 

                    } when 2{                        
                        isAchieved = verifyVisitsToCap(allProductInOpp,accInHierarchy);
                        if(isAchieved){
                            stepsTowardsSuccessWrapper.option = 'positive';
                            stepTowardsSuccess.Achieved__c = 'Yes';
                            tempScore++;                            
                        } 
                                          
                    } when 3{                        
                        isAchieved = (quotesInOpps.size() > 0 ? verifyStandardDiscount(quotesInOpps,StaVolDisMap) : false);
                        if(isAchieved){
                            stepsTowardsSuccessWrapper.option = 'positive';
                            stepTowardsSuccess.Achieved__c = 'Yes';
                            tempScore++;                            
                        } 
                        
                    } when 4{
                                                                                                
                        isAchieved = verifyExclusivity(accInHierarchy);
                        if(isAchieved){
                            stepsTowardsSuccessWrapper.option = 'positive';
                            stepTowardsSuccess.Achieved__c = 'Yes';
                            tempScore++;                            
                        }
                                                    
                    } when 5{
                        isAchieved = verifyIntegration(oppInHierarchy);
                        if(isAchieved){
                            stepsTowardsSuccessWrapper.option = 'positive';
                            stepTowardsSuccess.Achieved__c = 'Yes';
                            tempScore++;
                        }

                    } when 6{
                        isAchieved = (contacts.size() > 0)? true : false;//(oppWithCLevel.size()>0 ? verifyEngagementCLevel(oppInHierarchy, objWithContact) : false);
                        if(isAchieved){
                            stepsTowardsSuccessWrapper.option = 'positive';
                            stepTowardsSuccess.Achieved__c = 'Yes';
                            tempScore++;
                        }

                    } when 7 {
                        Integer numberOfLocationsToCompare = 0; //(isInheritedOpp ? numberOfLocationsForChild : (Integer)currentAcc.Number_of_Locations__c);
                        Integer qtdLocationsInOpp = verifySignWithNetwork(oppInHierarchy);
                        
                        if(isHeadquarterWithNoOpp || isBrandWithNoOpp ){                           
                            numberOfLocationsToCompare = (Integer)currentAcc.Number_of_Locations__c;                         
                        } else if (isInheritedOpp) {
                            numberOfLocationsToCompare = numberOfLocationsForChild+1;                      
                        } else {
                           numberOfLocationsToCompare = (Integer)currentAcc.Number_of_Locations__c+oppInHierarchy.size();
                        }

                        /*System.debug('numberOfLocationsToCompare ' + numberOfLocationsToCompare);
                        System.debug('qtdLocationsInOpp ' + qtdLocationsInOpp);
                        System.debug('isHeadquarterWithNoOpp ' + isHeadquarterWithNoOpp);
                        System.debug('isInheritedOpp ' + isInheritedOpp); 
                        System.debug('isBrandWithNoOpp ' + isBrandWithNoOpp);*/
                         
                        isAchieved = (qtdLocationsInOpp >= numberOfLocationsToCompare ? true : false); 
                      
                        if(isAchieved){
                            stepsTowardsSuccessWrapper.option = 'positive';
                            stepTowardsSuccess.Achieved__c = 'Yes';
                            tempScore++;
                        }  
                        
                    } when 8{                      
                        isAchieved = (quotesInOpps.size() > 0 ? verifyPRActivity(quotesInOpps) : false);
                        if(isAchieved){
                            stepsTowardsSuccessWrapper.option = 'positive';
                            stepTowardsSuccess.Achieved__c = 'Yes';
                            tempScore++;
                        }
                        
                    } when 9{
                        isAchieved = (quotesInOpps.size() > 0 ? verifyLongTerm(quotesInOpps) : false);
                        if(isAchieved){
                            stepsTowardsSuccessWrapper.option = 'positive';
                            stepTowardsSuccess.Achieved__c = 'Yes';
                            tempScore++;
                        }
                        
                    } when 10{
                        isAchieved = verifySuccess(oppInHierarchy);
                        if(isAchieved){
                            stepsTowardsSuccessWrapper.option = 'positive';
                            stepTowardsSuccess.Achieved__c = 'Yes';
                            tempScore++;
                        }
                    } when 11{
                        isAchieved = verifyStandardPayment(oppInHierarchy);
                        if(isAchieved){
                            stepsTowardsSuccessWrapper.option = 'positive';
                            stepTowardsSuccess.Achieved__c = 'Yes';
                            tempScore++;
                        }
                    }
                    when else {
                        System.debug('Invalid step!');
                    }
                }
                stepsToUpdate.add(stepTowardsSuccess);                
       		    listStepsTowardsSuccessWrapper.add(stepsTowardsSuccessWrapper);
     		}
        } 

        
        UPDATE stepsToUpdate; 
        
        if(currentAcc != null && currentAcc.Achieved_Score__c != tempScore){
            currentAcc.Achieved_Score__c = tempScore;
            UPDATE currentAcc;
        }          
        if(currentOpp != null && currentOpp.Achieved_Score__c != tempScore){
            currentOpp.Achieved_Score__c = tempScore;
            UPDATE currentOpp;
        }
               
        return listStepsTowardsSuccessWrapper; 
    }
    
    private static List<Step_Towards_Success_Partner__c> createGymSteps(String sObjectType, String recordId){
        Id stepRecordType = null;
        Id relatedAccount = null;
        Id relatedOpp = null;
    
        if(sObjectType == 'Account'){
            Id accountStepRecordTypeId = Schema.SObjectType.Step_Towards_Success_Partner__c.getRecordTypeInfosByName().get('Gym Partner - Account Step').getRecordTypeId();
            stepRecordType = accountStepRecordTypeId;
            relatedAccount = recordId;
            relatedOpp = null;
        } else {
            Id opportunityStepRecordTypeId = Schema.SObjectType.Step_Towards_Success_Partner__c.getRecordTypeInfosByName().get('Gym Partner - Opportunity Step').getRecordTypeId();
            stepRecordType = opportunityStepRecordTypeId;
            relatedAccount = null;
            relatedOpp = recordId;
        }
        
        List<Step_Towards_Success_Partner__c> stepsToInsert = new List<Step_Towards_Success_Partner__c>();
        Map<Decimal, Step_Towards_Success_Definition__mdt> stepNumbeToStepTowardsSuccessDefinition = getMap10StepsTowardsSucessDefinition();
        
        for (Integer i=1; i<=10; i++) {
			Step_Towards_Success_Partner__c step = new Step_Towards_Success_Partner__c();
            step.Step_Number__c = i;
            step.Related_Opportunity__c = relatedOpp;
            step.Related_Account__c = relatedAccount;
            step.Name = stepNumbeToStepTowardsSuccessDefinition.get(i).MasterLabel;
            step.RecordTypeId = stepRecordType;
            step.Achieved__c = 'No';            
            stepsToInsert.add(step);
        }        
        
        insert stepsToInsert;
        return stepsToInsert;
    }
    /** Populate Map with 10 steps from MDT*/
    private static Map<Decimal, Step_Towards_Success_Definition__mdt> getMap10StepsTowardsSucessDefinition() {
		Map<Decimal, Step_Towards_Success_Definition__mdt> stepNumberToStepTowardsSuccessDefinition = new Map<Decimal, Step_Towards_Success_Definition__mdt>(); 
       
        List<Step_Towards_Success_Definition__mdt> listStepsTowardsSuccessDefinitions = [
            SELECT DeveloperName , MasterLabel, Category__c, Description__c, Field_API_Name__c, Field_Expected_Value__c, How_are_we_going_to_track_it__c, Object_API_Name__c, Step__c
            FROM Step_Towards_Success_Definition__mdt
            WHERE Category__c = '10 Steps'
            ORDER BY Step__c ASC
        ];
        
        for (Step_Towards_Success_Definition__mdt step : listStepsTowardsSuccessDefinitions) {
            stepNumberToStepTowardsSuccessDefinition.put(Integer.valueOf(step.Step__c), step);
        } 
        return stepNumberToStepTowardsSuccessDefinition;
    }


    private static Boolean verifyCapPercentage(List<Product_Item__c> allProductInOpp){

        if(allProductInOpp.size() == 0)
            return false;

                  
        Double thresholdPercentage = 0.2;
        Double thresholdLatamPercentage = 0.1;
        Double capPercentage = 0;

            for(Product_Item__c prod : allProductInOpp){     
        
                if(prod.CAP_Value__c == null || prod.Price_Visits_Month_Package_Selected__c == null)
                    return false;
                //FOR LATAM AND FULL SERVICE
                if(prod.CAP_Value__c < prod.Price_Visits_Month_Package_Selected__c  && prod.Opportunity__r.Account.Gym_Type__c == 'Full Service' && 
                (prod.Opportunity__r.Account_Billing_Country__c == 'Brazil' || prod.Opportunity__r.Account_Billing_Country__c == 'Mexico'
                || prod.Opportunity__r.Account_Billing_Country__c == 'Chile' || prod.Opportunity__r.Account_Billing_Country__c == 'Argentina')){   
                    capPercentage = Math.ABS((prod.CAP_Value__c/prod.Price_Visits_Month_Package_Selected__c) -1);
                    if(capPercentage < thresholdLatamPercentage){
                    return false;
                    }

                } else if(prod.CAP_Value__c < prod.Price_Visits_Month_Package_Selected__c ){   
                    capPercentage = Math.ABS((prod.CAP_Value__c/prod.Price_Visits_Month_Package_Selected__c) -1);
                    if(capPercentage < thresholdPercentage){
                    return false;
                    }
                } else {
                return false;  
                } 
            }  
              
       return true;

    }

    private static Boolean verifyVisitsToCap(List<Product_Item__c> allProductInOpp, List<Account> accInHierarchy){

        if(allProductInOpp.size() == 0)
            return false;
            
        Integer thresholdStudio = 12;
        Integer thresholdOthers = 10;
        
        for(Account acc : accInHierarchy){
            for(Product_Item__c prod : allProductInOpp){
                if(prod.Max_Monthly_Visit_Per_User__c == null)
                    return false;  
                if(acc.Gym_Type__c == 'Studios' && (prod.CAP_Value__c / prod.Net_Transfer_Price__c < thresholdStudio)){
                    return false;
                } else if(acc.Gym_Type__c != 'Studios' && (prod.CAP_Value__c / prod.Net_Transfer_Price__c < thresholdOthers)){
                    return false;
                }
            }
        }              
        return true;
    }
    
    private static Boolean verifyStandardDiscount(List<Quote> quotesInOpp, Map<String,Standard_Volume_Discount__mdt> StaVolDisMap){    
        Set<String> europe = new Set<String>{'France','Italy','Ireland','Netherlands','Portugal','Spain' ,'United Kingdom','United States','Germany', 'Argentina', 'Chile'};
        Set<String> brazil = new Set<String>{'Brazil'};
        Set<String> mexico = new Set<String>{'Mexico'};

        for(Quote proposal : quotesInOpp){            
            Standard_Volume_Discount__mdt standardDiscount = new Standard_Volume_Discount__mdt();
            if(europe.contains(proposal.Country__c)) {
                standardDiscount = StaVolDisMap.get('Europe_South_Cone_US');
            } else if(brazil.contains(proposal.Country__c)){                
                standardDiscount = StaVolDisMap.get('Brazil');
            } else if(mexico.contains(proposal.Country__c)) {            
                standardDiscount = StaVolDisMap.get('Mexico');
                System.debug('Entrou else if mexico');
            } else {
                standardDiscount = StaVolDisMap.get('Standard');
            }
            System.debug('Mapa' + standardDiscount);
            if(proposal.Threshold_Value_1__c != standardDiscount.Threshold_Value_1__c)            
                return false;                
            if(proposal.Threshold_Value_2__c != standardDiscount.Threshold_Value_2__c) 
                return false;            
            if(proposal.Threshold_Value_3__c != standardDiscount.Threshold_Value_3__c) 
                return false;
            if(proposal.Threshold_Value_4__c != standardDiscount.Threshold_Value_4__c ) 
                return false;              
            if(proposal.First_Discount_Range__c != standardDiscount.X1st_Discount_Range1__c)
                return false;                 
            if(proposal.Second_Discount_Range__c!= standardDiscount.X2nd_Discount_Range2__c)
                return false;
            if(proposal.Third_Discount_Range__c != standardDiscount.X3rd_Discount_Range3__c)
                return false;
            if(proposal.Fourth_Discount_Range__c !=standardDiscount.X4th_Discount_Range4__c ) 
               return false;     
        }
         return true;        
    }
    
    private static Boolean verifyExclusivity(List<Account> accInHierarchy) {        
        for(Account acc : accInHierarchy){
            if(acc.Exclusivity__c != 'Yes')
                return false;
        }
        return true;
    }

    private static Boolean verifyLongTerm(List<Quote> quotesInOpp) {        
        for(Quote proposal : quotesInOpp){
           if(proposal.Start_Date__c == null || proposal.Final_Date__c == null || proposal.Start_Date__c.monthsBetween(proposal.Final_Date__c) < 36)
                return false;
        }
        return true;
    }

    private static Boolean verifyPRActivity(List<Quote> quotesInOpp) {        
        for(Quote proposal : quotesInOpp){
            if(proposal.PR_Activity__c != 'Yes' )
                return false;
        }
        return true;
    }
    
    /*
    private static Boolean verifyEngagementCLevel(List<Opportunity> oppHierarchy, Map<id,Boolean> oppWithCLevel) {        
        for(Opportunity opp : oppHierarchy){
          if(!oppWithCLevel.containsKey(opp.Id)){
            return false;
          }   
      }
      return true;
    } */

    private static Integer verifySignWithNetwork(List<Opportunity> oppHierarchy) {        
        Integer qtd = 0;
        for(Opportunity opp : oppHierarchy){
            qtd+= (Integer)opp.Number_of_Locations_Opportunity__c;
            qtd += 1; //its own acc;
        }        
      return qtd;
    }

    private static Boolean verifyIntegration(List<Opportunity> oppHierarchy) {  
        
        List<Club_Management_System__mdt> cmsMd = [ SELECT 
                                                    MasterLabel 
                                                    FROM Club_Management_System__mdt];

        Set<String> cmsLst = new Set<String>();
        if(cmsMd.size() > 0 ){
            for(Club_Management_System__mdt cm : cmsMd){
                cmsLst.add(cm.MasterLabel);
            }
        } else {
            return false;
        }           
        
        for(Opportunity opp : oppHierarchy){
            Set<String> cmsUSed = new Set<String>();
            if(opp.Club_Management_System__c == null)
                return false;
            cmsUsed.addAll(opp.Club_Management_System__c.split(';'));
            if( opp.CMS_Used__c != 'Yes'|| (opp.CMS_Used__c == 'Yes' && !cmsLst.containsAll(cmsUSed))){
                return false;
            }
        }
        return true;     
    }

    private static Boolean verifySuccess(List<Opportunity> oppHierarchy) {  
        for(Opportunity opp : oppHierarchy){
            if(opp.Success_Look_Like__c != 'Yes')
                return false;
        }
        return true;     
    }

    private static Boolean verifyStandardPayment(List<Opportunity> oppHierarchy) {  
        for(Opportunity opp : oppHierarchy){
            if(opp.Standard_Payment__c != 'Yes')
                return false;
        }
        return true;     
    }

    private static List<StepsTowardsSuccessCtrl.StepsTowardsSuccessWrapper>  generateFakeStepResults() {
		List<StepsTowardsSuccessCtrl.StepsTowardsSuccessWrapper> stepsTowardsSuccessWrapperList = new List<StepsTowardsSuccessCtrl.StepsTowardsSuccessWrapper>(); 
        Map<Decimal, Step_Towards_Success_Definition__mdt> stepNumbeToStepTowardsSuccessDefinition = getMap10StepsTowardsSucessDefinition();
        for (Integer i = 1; i <= 10; i++) {
			StepsTowardsSuccessCtrl.StepsTowardsSuccessWrapper stepsTowardsSuccessWrapper = new StepsTowardsSuccessCtrl.StepsTowardsSuccessWrapper();
           	stepsTowardsSuccessWrapper.label = (i < 10 ? '0' + i : '' + i) + ' - ' + stepNumbeToStepTowardsSuccessDefinition.get(i).MasterLabel;
           	stepsTowardsSuccessWrapper.alternativeText = stepNumbeToStepTowardsSuccessDefinition.get(i).Description__c;
           	stepsTowardsSuccessWrapper.option = 'negative';            
			stepsTowardsSuccessWrapperList.add(stepsTowardsSuccessWrapper);            
        }        
        return stepsTowardsSuccessWrapperList;     
    }
    
    private static void updateNumberOfLocationInOpportunity(Opportunity opp){        
        List<Account_Opportunity_Relationship__c> aorLst = [SELECT Id FROM Account_Opportunity_Relationship__c WHERE Opportunity__c =: opp.id];
    
        if(aorLst.size() != opp.Number_of_Locations_Opportunity__c) {
            opp.Number_of_Locations_Opportunity__c = aorLst.size();    
            UPDATE opp;
        }
    }
}