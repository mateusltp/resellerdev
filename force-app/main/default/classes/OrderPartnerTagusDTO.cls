/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 09-23-2021
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/
public with sharing class OrderPartnerTagusDTO {


	private String partner_order_id; //Quote.UUID
	private String sales_force_transaction_id; // ????
	private DateTime signed_at;
	private String conga_contract_id;
	private Partner_order_detail partner_order_detail;
	private List<Partner_order_items> partner_order_items;

	public Id getSalesforceTransactionId(){
		return this.sales_force_transaction_id;
	}

	public String getUuId(){
		return this.partner_order_id;
	}

	public Partner_order_detail getPartnerOrderDetail(){
		return  this.partner_order_detail;
	}

	public List<Partner_order_items> getPartnerOrderItem(){
		return  this.partner_order_items;
	}
	
	public OrderPartnerTagusDTO(Order aOrder, List<Product_Opportunity_Assignment__c> aProductAssignLst ){
		setPartnerOrder(aOrder, aProductAssignLst);	
	}

	//SOBJECT Ref: QUOTE 
	private void setPartnerOrder( Order aOrder, List<Product_Opportunity_Assignment__c> aProductAssignLst ){		
		this.partner_order_id = aOrder.Quote.UUID__c != null ? aOrder.Quote.UUID__c : new Uuid().getValue();
		this.signed_at =  aOrder.EffectiveDate;
		this.sales_force_transaction_id =  aOrder.QuoteId; //aOrder.salesForceTransactionId != null ?  aOrder.salesForceTransactionId : new Uuid().getValue();
		this.partner_order_detail =  setPartnerOrderDetails( aOrder ) ;
		this.partner_order_items =  new List<Partner_order_items> (setPartnerOrderItems( aOrder, aProductAssignLst) );
		this.conga_contract_id = null;	
	}

	//SOBJECT Ref: OPPORTUNITY 
	private Partner_order_detail setPartnerOrderDetails(Order aOrder){
		Partner_order_detail partnerOrderDetail = new Partner_order_detail();
		partnerOrderDetail.id = aOrder.Opportunity.UUID__c != null ? aOrder.Opportunity.UUID__c : new Uuid().getValue();
		partnerOrderDetail.status ='SIGNED'; //aOrder.Status; //SIGNED CANCELED
		partnerOrderDetail.salesforce_id = aOrder.OpportunityId;
		partnerOrderDetail.begin_date = aOrder.EffectiveDate;
		partnerOrderDetail.end_date = aOrder.EndDate;
		partnerOrderDetail.cancelation_date = aOrder.Opportunity.cancellation_date__c;
		partnerOrderDetail.cancelation_reason = aOrder.Opportunity.cancellation_reason__c;
		partnerOrderDetail.cancelation_description = aOrder.Opportunity.Cancellation_Reason_subcategory__c;		
		return partnerOrderDetail;
	}

	//SOBJECT Ref: ORDER 
	private List<Partner_order_items> setPartnerOrderItems(Order aOrder, List<Product_Opportunity_Assignment__c> aProductAssignLst  ){
		List<Partner_order_items> partnerOrderItemLst = new List<Partner_order_items>();		
		Partner_order_items partnerOrderItem = new Partner_order_items();
		partnerOrderItem.id = aOrder.Quote.Order_Item_UUID__c != null ? aOrder.Quote.Order_Item_UUID__c : new Uuid().getValue();
		partnerOrderItem.partner_id = aOrder.Account.UUID__c;
		partnerOrderItem.salesforce_id = aOrder.Id;
		partnerOrderItem.currency_id = aOrder.Account.CurrencyIsoCode;
		partnerOrderItem.country_id = aOrder.Account.BillingCountryCode;
		partnerOrderItem.partner_products = new List<Partner_products>( setPartnerProducts( aProductAssignLst ));						
		partnerOrderItemLst.add(partnerOrderItem);	
		return partnerOrderItemLst;
	}


	//SOBJECT Ref: ProdIem
	private List<Partner_products> setPartnerProducts ( List<Product_Opportunity_Assignment__c> aProductAssignLst  ){
		
		List<Partner_products> partnerProductLst = new List<Partner_products>();
		Map<Id, Partner_products> partnerProductMap = new Map<Id, Partner_products>();

		for( Product_Opportunity_Assignment__c productAssign : aProductAssignLst ){ 
			if(!partnerProductMap.containsKey(productAssign.ProductAssignmentId__r.ProductId__r.Id) && productAssign.ProductAssignmentId__c != null){
				Partner_products partnerProduct = new Partner_products();
				partnerProduct.id = productAssign.ProductAssignmentId__r.ProductId__r.UUID__c != null ? productAssign.ProductAssignmentId__r.ProductId__r.UUID__c : new Uuid().getValue();
				partnerProduct.legacy_id = productAssign.ProductAssignmentId__r.ProductId__r.LegacyId__c != null ? Integer.valueOf(productAssign.ProductAssignmentId__r.ProductId__r.LegacyId__c) : null;
				partnerProduct.salesforce_id = productAssign.ProductAssignmentId__r.ProductId__r.Id;
				partnerProduct.partner_product_type_id = 'af5a70ae-878e-4d54-b886-eb1a7e1faff6';
				partnerProduct.currency_id = productAssign.ProductAssignmentId__r.ProductId__r.CurrencyIsoCode;	
				partnerProduct.cost_per_usage = productAssign.ProductAssignmentId__r.ProductId__r.Net_Transfer_Price__c;				
				if(productAssign.ProductAssignmentId__r.CommercialConditionId__c != null && productAssign.ProductAssignmentId__r.CommercialConditionId__r.RecordType.DeveloperName.equals('CAP')) {
					partnerProduct.cap_value = productAssign.ProductAssignmentId__r.CommercialConditionId__r.Amount__c == null ? 0.0 :  productAssign.ProductAssignmentId__r.CommercialConditionId__r.Amount__c;
				} 
				partnerProduct.description=productAssign.ProductAssignmentId__r.ProductId__r.Name;
				partnerProduct.salesforce_id = productAssign.ProductAssignmentId__r.ProductId__r.Id;
				partnerProductMap.put(productAssign.ProductAssignmentId__r.ProductId__r.Id, partnerProduct);				
			} else if( productAssign.ProductAssignmentId__c != null ){		
				if(productAssign.ProductAssignmentId__r.CommercialConditionId__c != null && productAssign.ProductAssignmentId__r.CommercialConditionId__r.RecordType.DeveloperName.equals('CAP')) {
					Partner_products partnerProduct = partnerProductMap.get(productAssign.ProductAssignmentId__r.ProductId__r.Id);
					if(partnerProduct.cap_value == null || partnerProduct.cap_value == 0.0){
						partnerProduct.cap_value = productAssign.ProductAssignmentId__r.CommercialConditionId__r.Amount__c == null ? 0.0 :  productAssign.ProductAssignmentId__r.CommercialConditionId__r.Amount__c;
					}				
				}				
			}	
		}

		return partnerProductMap.values();
	}

    public class Partner_order_detail {
		private String id;
		private String status;
		private String salesforce_id;
		private DateTime begin_date;
		private DateTime end_date;
		private DateTime cancelation_date;
		private String cancelation_reason;
		private String cancelation_description;

		public Id getSalesforceId(){
			return this.salesforce_id;
		}

		public String getUuid(){
			return this.id;
		}

	}
    
	public class Partner_order_items {
		private String id;
		private String partner_id;		
		private String salesforce_id;
		private String currency_id;
		private String country_id;
		private List<Partner_products> partner_products;


		public Id getSalesforceId(){
			return this.salesforce_id;
		}

		public String getUuid(){
			return this.id;
		}

		public List<Partner_products> getPartnerProducts(){
			return this.partner_products;
		}
	}	

	public class Partner_products {
		private String id;		
		private String salesforce_id;
		private Integer legacy_id;
		private String partner_product_type_id;
		private String currency_id;
		private Decimal cost_per_usage;
		private Decimal cap_value;
		private String description;

		public Id getSalesforceId(){
			return this.salesforce_id;
		}

		public String getUuid(){
			return this.id;
		}
	}
	
}