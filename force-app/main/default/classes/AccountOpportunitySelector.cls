/**
 * @description       : 
 * @author            : GEPI@GFT.com
 * @group             : 
 * @last modified on  : 04-25-2022
 * @last modified by  : alysson.mota@gympass.com
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   07-18-2021   GEPI@GFT.com   Initial Version
**/
public inherited sharing class AccountOpportunitySelector extends ApplicationSelector{

	private static final PS_Constants constants = PS_Constants.getInstance();
	private Object CreatedDate;

	public List<Schema.SObjectField> getSObjectFieldList() {
		return new List<Schema.SObjectField> {
				Account_Opportunity_Relationship__c.Id,
				Account_Opportunity_Relationship__c.Name,
				Account_Opportunity_Relationship__c.Opportunity__c,
				Account_Opportunity_Relationship__c.Account__c
		};
	}

	public Schema.SObjectType getSObjectType() {
		return Account_Opportunity_Relationship__c.sObjectType;
	}

	public List<Account_Opportunity_Relationship__c> selectById(Set<Id> ids) {
		return (List<Account_Opportunity_Relationship__c>) super.selectSObjectsById(ids);
	}

	/** tags: clone, cloning, to clone, allFields
    * toClone means all fields are retrieving
    */
    public List<Account_Opportunity_Relationship__c> selectByOppIdToClone( Set<Id> aOppIdSet ) {
	
        return (List<Account_Opportunity_Relationship__c>) Database.query(
									newQueryFactory()
										.selectFields(getAllFieldsFromAccountOppRelationship())
										.selectField('Account__r.Wishlist__c')
										.selectField('Account__r.Approval_Status_Exclusivity__c')
										.selectField('Account__r.Approval_Status_CMS__c')
										.selectField('Account__r.Does_this_partner_use_CMS__c')
										.selectField('Account__r.Partner_agreed_to_have_integration_API__c')
										.setCondition('Opportunity__c IN: aOppIdSet').
										toSOQL());
	}


	/** tags: TAGUS, PARTNER 
    */

	public Map<Id, List<Product_Opportunity_Assignment__c>> selectProductAssignmentToTagus( Set<Id> aOppIdSet ) {
		Map<Id, List<Product_Opportunity_Assignment__c>> mProductOppAssignByOpp = new Map<Id, List<Product_Opportunity_Assignment__c>>();
		fflib_QueryFactory opportunityMemberFactory = newQueryFactory(false);
		fflib_QueryFactory oppAssignProdFactory = new ProductOpportunityAssignmentSelector().addQueryFactorySubselect(opportunityMemberFactory);
		oppAssignProdFactory.selectField(Product_Opportunity_Assignment__c.ProductAssignmentId__c);
		oppAssignProdFactory.selectField('ProductAssignmentId__r.ProductId__c');
		oppAssignProdFactory.selectField('ProductAssignmentId__r.ProductId__r.Id');
		oppAssignProdFactory.selectField('ProductAssignmentId__r.ProductId__r.Name');
		oppAssignProdFactory.selectField('ProductAssignmentId__r.ProductId__r.Type__c');
		oppAssignProdFactory.selectField('ProductAssignmentId__r.ProductId__r.UUID__c');
		oppAssignProdFactory.selectField('ProductAssignmentId__r.ProductId__r.CurrencyIsoCode');
		oppAssignProdFactory.selectField('ProductAssignmentId__r.ProductId__r.Type__c');
		oppAssignProdFactory.selectField('ProductAssignmentId__r.ProductId__r.Net_Transfer_Price__c');
		oppAssignProdFactory.selectField('ProductAssignmentId__r.ProductId__r.LegacyId__c');
		oppAssignProdFactory.selectField('ProductAssignmentId__r.CommercialConditionId__c');
		oppAssignProdFactory.selectField('ProductAssignmentId__r.CommercialConditionId__r.Name');
		oppAssignProdFactory.selectField('ProductAssignmentId__r.CommercialConditionId__r.Value__c');
		oppAssignProdFactory.selectField('ProductAssignmentId__r.CommercialConditionId__r.Amount__c');
		oppAssignProdFactory.selectField('ProductAssignmentId__r.CommercialConditionId__r.RecordType.DeveloperName');
		oppAssignProdFactory.setCondition('ProductAssignmentId__c != null AND ProductAssignmentId__r.CommercialConditionId__r.RecordType.DeveloperName = \''+constants.COMMERCIAL_CONDITION_RT_CAP+'\''); // IT-2185: Current Valid Products grid
		for (Account_Opportunity_Relationship__c iOppMember : (List<Account_Opportunity_Relationship__c>) Database.query(
				opportunityMemberFactory.selectField(Account_Opportunity_Relationship__c.Id).
						selectField(Account_Opportunity_Relationship__c.Opportunity__c).
						setCondition('Opportunity__c in :aOppIdSet').
						toSOQL())){
			if(mProductOppAssignByOpp.containsKey(iOppMember.Opportunity__c)){
				mProductOppAssignByOpp.get(iOppMember.Opportunity__c).addAll(iOppMember.Product_Opportunity_Assignments__r);
			} else if (!String.isBlank(iOppMember.Opportunity__c)){
				mProductOppAssignByOpp.put(iOppMember.Opportunity__c, iOppMember.Product_Opportunity_Assignments__r);
			}
		}
		system.debug('## mProductOppAssignByOpp: '+mProductOppAssignByOpp);
		return mProductOppAssignByOpp;
	}

	/** tags: SDG, SDGGrid, PARTNER
    */

	public Map<Id, List<Product_Opportunity_Assignment__c>> selectProductAssignmentToOpportunitySDGGrid( Set<Id> aOppIdSet ) {
		Map<Id, List<Product_Opportunity_Assignment__c>> mProductOppAssignByOpp = new Map<Id, List<Product_Opportunity_Assignment__c>>();
		fflib_QueryFactory opportunityMemberFactory = newQueryFactory(false);
		fflib_QueryFactory oppAssignProdFactory = new ProductOpportunityAssignmentSelector().addQueryFactorySubselect(opportunityMemberFactory);
		oppAssignProdFactory.selectField(Product_Opportunity_Assignment__c.ProductAssignmentId__c);
		oppAssignProdFactory.selectField('ProductAssignmentId__r.ProductId__c');
		oppAssignProdFactory.selectField('ProductAssignmentId__r.ProductId__r.Name');
		oppAssignProdFactory.selectField('ProductAssignmentId__r.ProductId__r.RecordType.Name');
		oppAssignProdFactory.selectField('ProductAssignmentId__r.ProductId__r.RecordTypeId');
		oppAssignProdFactory.selectField('ProductAssignmentId__r.ProductId__r.Parent_Product__c');
		oppAssignProdFactory.selectField('ProductAssignmentId__r.CommercialConditionId__r.CAP_Discount__c');
		oppAssignProdFactory.selectField('OpportunityMemberId__r.Account__c');
		oppAssignProdFactory.selectField('OpportunityMemberId__r.Account__r.Name');
		oppAssignProdFactory.selectField('OpportunityMemberId__r.Opportunity__c');
		for (Account_Opportunity_Relationship__c iOppMember : (List<Account_Opportunity_Relationship__c>) Database.query(
				opportunityMemberFactory.selectField(Account_Opportunity_Relationship__c.Id).
						selectField(Account_Opportunity_Relationship__c.Opportunity__c).
						selectField(Account_Opportunity_Relationship__c.Account__c).
						setCondition('Opportunity__c in :aOppIdSet').
						toSOQL())){
			if(mProductOppAssignByOpp.containsKey(iOppMember.Opportunity__c)){
				mProductOppAssignByOpp.get(iOppMember.Opportunity__c).addAll(iOppMember.Product_Opportunity_Assignments__r);
			} else if (!String.isBlank(iOppMember.Opportunity__c)){
				mProductOppAssignByOpp.put(iOppMember.Opportunity__c, iOppMember.Product_Opportunity_Assignments__r);
			}
		}
		return mProductOppAssignByOpp;
	}


	public Map<Id, List<Product_Opportunity_Assignment__c>> selectProductAssignmentToSDGGrid( Set<Id> aAccIdSet ) {

		Map<Id, List<Product_Opportunity_Assignment__c>> mProductOppAssignByOpp = new Map<Id, List<Product_Opportunity_Assignment__c>>();       
		fflib_QueryFactory opportunityMemberFactory = newQueryFactory(false);
		fflib_QueryFactory oppAssignProdFactory = new ProductOpportunityAssignmentSelector().addQueryFactorySubselect(opportunityMemberFactory);
		oppAssignProdFactory.selectField(Product_Opportunity_Assignment__c.ProductAssignmentId__c);
		oppAssignProdFactory.selectField('ProductAssignmentId__r.ProductId__c');
		oppAssignProdFactory.selectField('ProductAssignmentId__r.ProductId__r.Name');
		oppAssignProdFactory.selectField('ProductAssignmentId__r.ProductId__r.Type__c');
		oppAssignProdFactory.selectField('ProductAssignmentId__r.ProductId__r.UUID__c');
		oppAssignProdFactory.selectField('ProductAssignmentId__r.ProductId__r.CurrencyIsoCode');
		oppAssignProdFactory.selectField('ProductAssignmentId__r.ProductId__r.Net_Transfer_Price__c');
		oppAssignProdFactory.selectField('ProductAssignmentId__r.ProductId__r.Type_Id__c');
		oppAssignProdFactory.selectField('ProductAssignmentId__r.CommercialConditionId__r.Name');
		oppAssignProdFactory.selectField('ProductAssignmentId__r.CommercialConditionId__r.Value__c');
		oppAssignProdFactory.selectField('ProductAssignmentId__r.CommercialConditionId__r.Amount__c');
		oppAssignProdFactory.selectField('ProductAssignmentId__r.CommercialConditionId__r.RecordType.DeveloperName');
		for (Account_Opportunity_Relationship__c iOppMember : (List<Account_Opportunity_Relationship__c>) Database.query( 
																opportunityMemberFactory.selectField(Account_Opportunity_Relationship__c.Id).
																selectField(Account_Opportunity_Relationship__c.Account__c).
																setCondition('Account__c in : aAccIdSet').
																toSOQL())){
			if(mProductOppAssignByOpp.containsKey(iOppMember.Account__c)){
				mProductOppAssignByOpp.get(iOppMember.Account__c).addAll(iOppMember.Product_Opportunity_Assignments__r);
			} else {
				mProductOppAssignByOpp.put(iOppMember.Account__c, iOppMember.Product_Opportunity_Assignments__r);
			}
		}
		return mProductOppAssignByOpp;
	}


	public List<Account_Opportunity_Relationship__c> selectOppMemberByAccID(Set<Id> ids) {
		return new List<Account_Opportunity_Relationship__c>( (List<Account_Opportunity_Relationship__c>)Database.query(
										newQueryFactory().
										selectField(Account_Opportunity_Relationship__c.Id).
										selectField(Account_Opportunity_Relationship__c.Account__c).
										selectField(Account_Opportunity_Relationship__c.Account__r.CurrencyIsoCode).
										selectField(Account_Opportunity_Relationship__c.Opportunity__c).
										setCondition('Account__c in: ids').
										toSOQL()
										));									
	}

	public List<Account_Opportunity_Relationship__c> selectOppMemberByOppID(Set<Id> oppIds) {
		return new List<Account_Opportunity_Relationship__c>( (List<Account_Opportunity_Relationship__c>)Database.query(
				newQueryFactory().
						selectField(Account_Opportunity_Relationship__c.Id).
						selectField(Account_Opportunity_Relationship__c.Account__c).
						selectField(Account_Opportunity_Relationship__c.From_Child_Opp_Creation_Process__c).
						selectField('Account__r.Name').
						selectField('Account__r.Legal_Representative__c').
                        selectField('Account__r.Legal_Representative__r.Name').
						selectField('Account__r.Partner_Level__c').						
						selectField(Account_Opportunity_Relationship__c.Opportunity__c).
						selectField('Opportunity__r.AccountId').
						setCondition('Opportunity__c IN :oppIds').
						setOrdering('CreatedDate', fflib_QueryFactory.SortOrder.DESCENDING).
						toSOQL()
		));
	}

	public List<Account_Opportunity_Relationship__c> byId(Set<Id> ids) {
		return new List<Account_Opportunity_Relationship__c>( (List<Account_Opportunity_Relationship__c>)Database.query(
										newQueryFactory().
										selectField(Account_Opportunity_Relationship__c.Id).
										selectField(Account_Opportunity_Relationship__c.Account__c).
										selectField(Account_Opportunity_Relationship__c.Opportunity__c).
										selectField('Account__r.CurrencyIsoCode').
										setCondition('Id in: ids').
										toSOQL()
										));
	}
	
	public Map<Id, Account_Opportunity_Relationship__c> byIdAsMap(Set<Id> ids) {
		Map<Id, Account_Opportunity_Relationship__c> idToAccOppRel = new Map<Id, Account_Opportunity_Relationship__c>();
		List<Account_Opportunity_Relationship__c> accOppRelLst = byId(ids);

		for (Account_Opportunity_Relationship__c accOppRel : accOppRelLst) {
			idToAccOppRel.put(accOppRel.Id, accOppRel);
		}

		return idToAccOppRel;
	}

	// Bruno Mendes IT-2185: Current Valid Products grid
	public Map<Id, Id> selectLastWonOpportunityIdByAccountId (Set<Id> accountIds) {
		Map<Id, Id> opportunityIdByAccountId = new Map<Id, Id>();
		List<Account_Opportunity_Relationship__c> accountOppRelList = (List<Account_Opportunity_Relationship__c>) Database.query(
				newQueryFactory()
						.selectField(Account_Opportunity_Relationship__c.Opportunity__c)
						.selectField(Account_Opportunity_Relationship__c.Account__c)
						.setCondition('Account__c IN :accountIds AND Opportunity__r.isClosed = true AND Opportunity__r.StageName = \''+constants.OPPORTUNITY_STAGE_LANCADO_GANHO+'\'')
						.setOrdering('Opportunity__r.CreatedDate', fflib_QueryFactory.SortOrder.DESCENDING)
						.toSOQL()
		);
		for (Account_Opportunity_Relationship__c aor : accountOppRelList) {
			if (!opportunityIdByAccountId.containsKey(aor.Account__c)) {
				opportunityIdByAccountId.put(aor.Account__c, aor.Opportunity__c);
			}
		}
		System.debug('## opportunityIdByAccountId: ' + json.serializePretty(opportunityIdByAccountId));
		return opportunityIdByAccountId;
	}

	

	public Map<Id, Account_Opportunity_Relationship__c> selectExistingOpportunityMemberByAccountId ( Set<Id> accountIds ) {
		Map<Id, Account_Opportunity_Relationship__c> aorIdMap = new Map<Id, Account_Opportunity_Relationship__c>();
		List<Account_Opportunity_Relationship__c> accountOppRelList = (List<Account_Opportunity_Relationship__c>) Database.query(
				newQueryFactory()
						.selectField(Account_Opportunity_Relationship__c.Opportunity__c)
						.selectField(Account_Opportunity_Relationship__c.Account__c)
						.setCondition('Account__c IN :accountIds')
						.toSOQL()
		);
		for (Account_Opportunity_Relationship__c aor : accountOppRelList) {
			if (!aorIdMap.containsKey(aor.Id)) {
				aorIdMap.put(aor.Id, aor);
			}
		}
		return aorIdMap;
	}

	public Map<Id, Account_Opportunity_Relationship__c> selectOpportunityMemberByAccountIdAndOpportunityId ( Set<Id> accountIds, Set<Id> opportunityIds ) {
		
		return new Map<Id,Account_Opportunity_Relationship__c>(  (List<Account_Opportunity_Relationship__c>) Database.query(
				newQueryFactory()
						.setCondition('Account__c IN :accountIds AND Opportunity__c IN: opportunityIds')
						.toSOQL()
		));
	}
	

	@TestVisible
	private Set<String> getAllFieldsFromAccountOppRelationship(){
		Map<String, Schema.SObjectField> accOppRelFields = Schema.getGlobalDescribe().get('Account_Opportunity_Relationship__c').getDescribe().fields.getMap();
        return accOppRelFields.keySet();
    }

}