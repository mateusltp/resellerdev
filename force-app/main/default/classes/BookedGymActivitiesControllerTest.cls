/**
 * @description       : 
 * @author            : alysson.mota@gympass.com
 * @group             : 
 * @last modified on  : 06-29-2022
 * @last modified by  : alysson.mota@gympass.com
**/
@isTest
public with sharing class BookedGymActivitiesControllerTest {

    @TestSetup
    static void makeData(){
        Product_Item__c prodItem = new Product_Item__c();
        prodItem.Name = 'Test Gym Prod';
        insert prodItem;

        List<Gym_Activity_Relationship__c> gymRelsToInsert = new List<Gym_Activity_Relationship__c>();

        Gym_Activity_Relationship__c gymRel1 = new Gym_Activity_Relationship__c();
        gymRel1.Name = 'Cardio';

        Gym_Activity_Relationship__c gymRel2 = new Gym_Activity_Relationship__c();
        gymRel2.Name = 'ABD';

        gymRelsToInsert.add(gymRel1);      
        gymRelsToInsert.add(gymRel2);
        
        insert gymRelsToInsert;

        List<Product_Activity_Relationship__c> parLstToInsert = new List<Product_Activity_Relationship__c>();

        Product_Activity_Relationship__c parGym1 = new Product_Activity_Relationship__c();
        parGym1.Product__c = prodItem.Id;
        parGym1.Gym_Activity_Relationship__c = gymRel1.Id;

        Product_Activity_Relationship__c parGym2 = new Product_Activity_Relationship__c();
        parGym2.Product__c = prodItem.Id;
        parGym2.Gym_Activity_Relationship__c = gymRel2.Id;

        parLstToInsert.add(parGym1);
        parLstToInsert.add(parGym2);

        insert parLstToInsert;

        List<Gym_Activity_To_Be_Booked__c> gymBookedLstToInsert = new List<Gym_Activity_To_Be_Booked__c>();
        
        Gym_Activity_To_Be_Booked__c gymBooked1 = new Gym_Activity_To_Be_Booked__c();
        gymBooked1.GymActivityRelationshipId__c = gymRel1.Id;
        gymBooked1.Name = gymRel1.Name;
        gymBooked1.Product_Item__c = prodItem.Id;

        Gym_Activity_To_Be_Booked__c gymBooked2 = new Gym_Activity_To_Be_Booked__c();
        gymBooked2.GymActivityRelationshipId__c = gymRel2.Id;
        gymBooked2.Name = gymRel2.Name;
        gymBooked2.Product_Item__c = prodItem.Id;

        gymBookedLstToInsert.add(gymBooked1);
        gymBookedLstToInsert.add(gymBooked2);

        insert gymBookedLstToInsert;
    }

    @isTest
    public static void getProdGymActivitiesTest() {
        Product_Item__c prodItem = [SELECT Id FROM Product_Item__c LIMIT 1];
        
        Test.startTest();
        List<Gym_Activity_Relationship__c> currentGymRels = BookedGymActivitiesController.getProdGymActivities(prodItem.Id);
        Test.stopTest();
        
        System.assertEquals(2, currentGymRels.size());
    }

    @isTest
    public static void getCurrentGymActivitiesBookedTest() {
        Product_Item__c prodItem = [SELECT Id FROM Product_Item__c LIMIT 1];
        
        Test.startTest();
        List<Id> currentBookedGymRelsIds = BookedGymActivitiesController.getCurrentGymActivitiesBooked(prodItem.Id);
        Test.stopTest();
        
        System.assertEquals(2, currentBookedGymRelsIds.size());
    }

    @isTest
    public static void createGymActivitiesToBeBookedTest() {
        Product_Item__c prodItem = [SELECT Id FROM Product_Item__c LIMIT 1];
        List<Id> gymRelIdsBefore = BookedGymActivitiesController.getCurrentGymActivitiesBooked(prodItem.Id);
        List<Product_Activity_Relationship__c> parLstBefore = [SELECT Id, Gym_Activity_Relationship__c FROM Product_Activity_Relationship__c WHERE Product__c =: prodItem.Id];
        Set<Id> gymRelIdSet = new Set<Id>();

        for (Product_Activity_Relationship__c par : parLstBefore) {
            gymRelIdSet.add(par.Gym_Activity_Relationship__c);
        }
        List<Gym_Activity_Relationship__c> gymRelLstBefore = [SELECT Id, Name FROM Gym_Activity_Relationship__c LIMIT 1];

        List<Gym_Activity_Relationship__c> newGymRelLst = new List<Gym_Activity_Relationship__c>();

        Gym_Activity_Relationship__c newGymRel1 = new Gym_Activity_Relationship__c();
        newGymRel1.Name = 'Swimming';
        newGymRelLst.add(newGymRel1);

        Gym_Activity_Relationship__c newGymRel2 = new Gym_Activity_Relationship__c();
        newGymRel2.Name = 'Dance';
        newGymRelLst.add(newGymRel2);
        
        insert newGymRelLst;
        gymRelLstBefore.addAll(newGymRelLst);

        List<Product_Activity_Relationship__c> newParLst = new List<Product_Activity_Relationship__c>();
        
        Product_Activity_Relationship__c newParGym1 = new Product_Activity_Relationship__c();
        newParGym1.Product__c = prodItem.Id;
        newParGym1.Gym_Activity_Relationship__c = newGymRel1.Id;
        newParLst.add(newParGym1);

        Product_Activity_Relationship__c newParGym2 = new Product_Activity_Relationship__c();
        newParGym2.Product__c = prodItem.Id;
        newParGym2.Gym_Activity_Relationship__c = newGymRel2.Id;
        newParLst.add(newParGym2);
        
        insert newParLst;

        Test.startTest();

        BookedGymActivitiesController.createGymActivitiesToBeBooked(prodItem.Id, gymRelLstBefore);

        List<Id> currentBookedGymRelsIds = BookedGymActivitiesController.getCurrentGymActivitiesBooked(prodItem.Id);

        Test.stopTest();

        System.assertEquals(3, currentBookedGymRelsIds.size());
    }
}