@IsTest
public class UniqueKeyCustomFieldTest {

    @isTest public static void isCNPJTest() {
        Account account = new Account();  
        account.Name = 'Test Account';
        account.Legal_Document_Type__c = 'CNPJ';
        account.Id_Company__c = '56947401000155';
        account.Razao_Social__c = 'TESTE';
        account.Industry = 'Airlines';             
        account.BillingCountry = 'Brazil';
        account.Type = 'Prospect';
        account.Industry = 'Government or Public Management';
        insert account;
        
        
        Boolean isValid;
        Test.startTest();
        isValid = UniqueKeyCustomField.isCNPJ(account.Id_Company__c);
        Test.stopTest(); 
        
         System.assertEquals(false, isValid);
    }
}