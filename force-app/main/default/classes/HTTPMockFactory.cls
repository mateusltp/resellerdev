@isTest
public class HTTPMockFactory implements HttpCalloutMock {
	protected Integer code;
  	protected String status;
  	protected String body;
  	protected Map<String, String> responseHeaders;
  	
    public HTTPMockFactory(BatchTagusIntegrationTest testClassInstance, Integer code, String status, String body, Map<String, String> responseHeaders) {
    	this.code = code;
    	this.status = status;
    	this.body = body;
    	this.responseHeaders = responseHeaders;
 	}
  
    public HTTPMockFactory(UtilsTest testClassInstance, Integer code, String status, String body, Map<String, String> responseHeaders) {
    	this.code = code;
    	this.status = status;
    	this.body = body;
    	this.responseHeaders = responseHeaders;
 	}
    
    public HTTPResponse respond(HTTPRequest req) {
        HttpResponse res = new HttpResponse();
        
        if (code == 401) {
        	CalloutException e = (CalloutException)CalloutException.class.newInstance();
        	e.setMessage('Unauthorized endpoint, please check Setup->Security->Remote site settings.');
        	throw e;
        } else {
			for (String key : this.responseHeaders.keySet()) {
      			res.setHeader(key, this.responseHeaders.get(key));
            }
            res.setBody(this.body);
            res.setStatusCode(this.code);
            res.setStatus(this.status);
        }
        
    	return res;
  	}
}