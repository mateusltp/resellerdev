@IsTest
public class SmbSkuWaiverApprovalHelperTest {
    
    @TestSetup
    static void makeData(){

        Account gympassEntity = AccountMock.getGympassEntity();
        gympassEntity.CurrencyIsoCode = 'BRL';
        insert gympassEntity;
    
        Account account = AccountMock.getStandard('Empresas');
        account.CurrencyIsoCode = 'BRL';
        account.UUID__c = SelfCheckoutRequestMock.ACCOUNT_UUID;
    
        insert account;
    
        Contact contact = ContactMock.getStandard(account);
    
        insert contact;
    
        account.Attention__c = contact.Id;
    
        update account;
    
        Pricebook2 pricebook = new Pricebook2(
          CurrencyIsoCode = 'BRL',
          Id = Test.getStandardPricebookId(),
          IsActive = true
        );
    
        update pricebook;
    
        List<Product2> products = new List<Product2>();
    
        Product2 setupFee = ProductMock.getSetupFee();
        setupFee.Minimum_Number_of_Employees__c = 0;
        setupFee.Maximum_Number_of_Employees__c = 10000;
        setupFee.IsActive = true;
        setupFee.Family = 'Enterprise Subscription';
        setupFee.Copay2__c = false;
        setupFee.Family_Member_Included__c = false;
        products.add(setupFee);
    
        Product2 accessFee = ProductMock.getAccessFee();
        accessFee.Minimum_Number_of_Employees__c = 0;
        accessFee.Maximum_Number_of_Employees__c = 10000;
        accessFee.IsActive = true;
        accessFee.Family = 'Enterprise Subscription';
        accessFee.Copay2__c = false;
        accessFee.Family_Member_Included__c = false;
        products.add(accessFee);
    
        insert products;
    
        List<PricebookEntry> pricebookEntries = new List<PricebookEntry>();
    
        PricebookEntry setupFeePricebookEntry = PricebookMock.getPricebookEntry(
          pricebook,
          setupFee
        );
        setupFeePricebookEntry.UnitPrice = 10;
        pricebookEntries.add(setupFeePricebookEntry);
    
        PricebookEntry accessFeePricebookEntry = PricebookMock.getPricebookEntry(
          pricebook,
          accessFee
        );
        accessFeePricebookEntry.UnitPrice = 10;
        pricebookEntries.add(accessFeePricebookEntry);
    
        insert pricebookEntries;
    
        Opportunity opp = OpportunityMock.getNewBusiness(account, pricebook);
        opp.CurrencyIsoCode = 'BRL';
        opp.StageName = 'Lançado/Ganho';
        opp.Current_Billing_Day__c = 15;
        opp.Current_Order_End_Date__c = Date.today().addMonths(3);
    
        insert opp;
    
        Quote qt = QuoteMock.getStandard(opp);
        RecordType qtRt = [SELECT Id, Name FROM RecordType WHERE SobjectType='Quote' AND Name = 'SMB Success - SKU Renegotiation' LIMIT 1];
        qt.recordTypeId = qtRt.Id;
        insert qt;
    
        QuoteLineItem qtItem = QuoteLineItemMock.getSetupFee(qt,setupFeePricebookEntry);
        insert qtItem;

        RecordType rt = [SELECT Id, Name FROM RecordType WHERE SobjectType='Waiver__c' AND Name = 'Months after Launch' LIMIT 1];
        Date today = Date.today();
        List<Waiver__c> waivers = new List<Waiver__c>();
        Waiver__c waiver1 = WaiverMock.getStandard(qtItem);
        waiver1.RecordTypeId = rt.Id;
        waiver1.position__c = 1;
        waiver1.End_Date__c = Date.newInstance(today.year(), today.month(), 15);
        Waiver__c waiver2 = WaiverMock.getStandard(qtItem);
        waiver2.RecordTypeId = rt.Id;
        waiver2.position__c = 2;
        waiver2.End_Date__c = waiver1.End_Date__c.addMonths(1);
        Waiver__c waiver3 = WaiverMock.getStandard(qtItem);
        waiver3.RecordTypeId = rt.Id;
        waiver3.position__c = 3;
        waiver3.End_Date__c = waiver2.End_Date__c.addMonths(1);
        waivers.add(waiver1);
        waivers.add(waiver2);
        waivers.add(waiver3);
        insert waivers;
        
    }
    
    @isTest
    static void checkApprovalNeedWithWaiversTest() {
        Test.startTest();
        
        Discount_and_Waiver_approval_parameters__mdt approvalMeta = 
            new Discount_and_Waiver_approval_parameters__mdt(
                Country__c = 'United States',
                Waiver_CCO__c = 7,
                Waiver_RM__c = 5,
                Initial_Approver_Email__c = 'test@test.com',
                Option__c = 'Waiver',
                Waiver_RM_1__c = 1,
                Initial_Approver_Queue_Name__c = 'United States - RM -1'
            );
        
		QuoteLineItem product = [SELECT Id FROM QuoteLineItem LIMIT 1];
        
        List<Waiver__c> waivers = [SELECT Id, End_Date_Text__c, Position__c, QuoteId__c, Quote_Line_Item__c, Duration__c FROM Waiver__c];
        
        SmbSkuWaiverHelper.populateEndDateAndDuration(product.Id, waivers);
        SmbSkuWaiverApprovalHelper helper = new SmbSkuWaiverApprovalHelper();
        helper.gMapOptionApprovalMetadata = new Map<String, Discount_and_Waiver_approval_parameters__mdt> { 'Waiver' => approvalMeta };
        Boolean result = helper.checkApprovalNeed(waivers, new List<Waiver__c>());
        Test.stopTest();
        System.assertEquals(true, result, 'An approval need is expected.');
    }
    
        @isTest
    static void checkApprovalNeedWithNoWaiversTest() {
        Test.startTest();
        
        Discount_and_Waiver_approval_parameters__mdt approvalMeta = 
            new Discount_and_Waiver_approval_parameters__mdt(
                Country__c = 'United States',
                Waiver_CCO__c = 7,
                Waiver_RM__c = 5,
                Initial_Approver_Email__c = 'test@test.com',
                Option__c = 'Waiver',
                Waiver_RM_1__c = 1,
                Initial_Approver_Queue_Name__c = 'United States - RM -1'
            );
        
		QuoteLineItem product = [SELECT Id FROM QuoteLineItem LIMIT 1];
        
        List<Waiver__c> waivers = [SELECT Id, End_Date_Text__c, Position__c, QuoteId__c, Quote_Line_Item__c, Duration__c FROM Waiver__c];

        SmbSkuWaiverApprovalHelper helper = new SmbSkuWaiverApprovalHelper();
        helper.gMapOptionApprovalMetadata = new Map<String, Discount_and_Waiver_approval_parameters__mdt> { 'Waiver' => approvalMeta };
        Boolean result = helper.checkApprovalNeed(new List<Waiver__c>(), waivers);
        Test.stopTest();
        System.assertEquals(false, result, 'An approval need is not expected.');
    }

}