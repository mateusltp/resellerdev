/**
 * @description       : 
 * @author            : AlyssonMota
 * @group             : 
 * @last modified on  : 12-15-2020
 * @last modified by  : Alysson Mota
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   12-15-2020   Alysson Mota   Initial Version
**/
@istest
public class ProposalExceptionsRejectedTest {
@TestSetup
    	private static void generateInfos(){
        Id rtId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gympass_Entity').getRecordTypeId();
        Account acc = new Account();
        acc.name='AcademiaBrasilCompanyPai';
        acc.RecordTypeId = rtId;
        acc.GP_Status__c = 'Active';
        acc.billingState = 'Minas Gerais';
        acc.CAP_Value__c = 120;
        acc.BillingCity = 'CityAcademiaBrasil';
        acc.billingStreet = 'Rua academiabrasilpai';
        acc.billingCountry = 'Brazil';
        acc.Gym_Type__c = 'Studios';
        acc.Gym_Classes__c = 'Cardio';
        acc.Types_of_ownership__c = Label.franchisePicklist;
        acc.Subscription_Type__c = 'Value per class';
        acc.Subscription_Period__c = 'Monthy value';
        acc.Subscription_Type_Estimated_Price__c    = 100;
        acc.Has_market_cannibalization__c = 'No';
        acc.Exclusivity__c = 'Yes';
        acc.Exclusivity_End_Date__c = Date.today().addYears(1);
        acc.Exclusivity_Partnership__c = 'Full Exclusivity';
        acc.Exclusivity_Restrictions__c= 'No';
        acc.Website = 'testing@tesapex.com';
        acc.Gym_Email__c = 'gymemail@apex.com';
        acc.Phone = '3222123123';
        acc.Can_use_logo__c = 'Yes';
        acc.Legal_Registration__c = 12123;
        acc.Legal_Title__c = 'Title LTDA';
        acc.Gyms_Identification_Document__c = 'CNPJ'; 
        insert acc;
        
        Id oppRtId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        Opportunity accOpp = new Opportunity();
        accOpp.recordTypeId = oppRtId;
        accOpp.AccountId = acc.id;
        accOpp.Name = 'Quote_NAME_TEST_1'; 
        accOpp.CMS_Used__c = 'Yes';     
        accOpp.Gym_agreed_to_an_API_integration__c  = 'Yes';
        accOpp.Club_Management_System__c = 'Companhia Athletica';
        accOpp.Integration_Fee_Deduction__c = 'No';
        accOpp.CloseDate = Date.today();
        accOpp.Success_Look_Like__c = 'Yes';
        accOpp.Success_Look_Like_Description__c = 'Money money';
        accOpp.StageName = 'Proposta Enviada';
        accOpp.Type = 'Expansion';  
        accOpp.Country_Manager_Approval__c = true;
        accOpp.Payment_approved__c = true;   
        accOpp.CurrencyIsoCode = 'BRL';
        accOpp.Gympass_Plus__c = 'Yes';
        accOpp.Standard_Payment__c = 'Yes';
        accOpp.Request_for_self_checkin__c = 'Yes';  
        Insert accOpp;
        
        Id quoteRecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Indirect_Channel').getRecordTypeId();
        Quote qt = new Quote();  
        qt.Name = 'Test Quote';
        qt.OpportunityId = accOpp.Id;   
        qt.License_Fee_Waiver__c = 'No';  
        qt.RecordTypeId = quoteRecordTypeId; 
        Insert qt;
            
        Exception__c e = new exception__c();
        e.Proposal__c = qt.Id;
        e.Status__c = 'Rejected';
        e.Type__c = 'Membership Fee';
        Insert e;
    }
    
   @isTest static void updateStatus()
    {
    test.startTest();
        
    List<Quote> qList = [SELECT Id, Name FROM Quote WHERE name = 'Test Quote'];
    List<Exception__c> eList = [SELECT id, Name, Proposal__c, status__c FROM Exception__c WHERE Proposal__c IN : qList];
    
        for(exception__c e : eList)
        {
    
        e.Status__c = 'Appoved';
         
        }
        
        update eList;
    
    test.stopTest();    
    }
}