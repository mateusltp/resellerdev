/**
 * @description       : 
 * @author            : roei@gft.com
 * @group             : 
 * @last modified on  : 06-30-2021
 * @last modified by  : Alysson Mota
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   11-25-2020   roei@gft.com   Initial Version
**/
public without sharing class OpportunityTriggerContractCancellation {
    private Id gRenegotiationRecTypeId = 
        Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Success_Renegotiation').getRecordTypeId();
    
    private Id smbRenegotiationRecTypeId = 
        Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SMB_Success_Renegotiation').getRecordTypeId();
    
    private Id gContractCancellationRecTypeId = 
        Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Contract_Cancellation').getRecordTypeId();    
	private Set< Id > gSetOppId = new Set< Id >();
    
    public OpportunityTriggerContractCancellation() {}

    public void createContractCancellationCase(){
       
        Map< Id , Opportunity > lMapOldOppIdOpp = (Map<Id,Opportunity>)Trigger.oldMap;
        List< Opportunity > lLstOppToCreateCancellationCase = new List< Opportunity >();
		System.debug('Trigger.new' +((List<Opportunity>)Trigger.new).size());
        for( Opportunity iOpp : (List<Opportunity>)Trigger.new ){

            if( (iOpp.RecordTypeId == gRenegotiationRecTypeId || iOpp.RecordTypeId == smbRenegotiationRecTypeId) && iOpp.StageName == 'Perdido' && iOpp.Sub_Type__c == 'Retention' &&
                iOpp.StageName != lMapOldOppIdOpp.get(iOpp.Id).StageName ){
                lLstOppToCreateCancellationCase.add(iOpp);
                gSetOppId.add( iOpp.Id );
                System.debug('iOpp.id ' + iOpp.id);
            }
        }
		
        createCaseByOpp(lLstOppToCreateCancellationCase);
    }

    private void createCaseByOpp( List< Opportunity > aLstOppToCreateCancellationCase ){
        if( aLstOppToCreateCancellationCase.isEmpty() ) { return; }

        List< Case > lLstCancellationCase = new List< Case >();
        Map< Id , Id > lMapOppIdContactId = getContactIdByOppId();
        Set<Id> oppsIdsWithOpenCase = getOppsIdsWithOpenCases(aLstOppToCreateCancellationCase);

        for( Opportunity iOpp : aLstOppToCreateCancellationCase ){
            if (!oppsIdsWithOpenCase.contains(iOpp.Id)) {
                lLstCancellationCase.add( 
                    new Case(
                        Subject = 'Contract Cancellation',
                        RecordTypeId = gContractCancellationRecTypeId,
                        Status = 'New',
                        Origin = 'Email',
                        OwnerId = iOpp.OwnerId,
                        OpportunityId__c = iOpp.Id,
                        Cancellation_Date__c = iOpp.cancellation_date__c,
                        Cancellation_Reason__c = iOpp.Loss_Reason__c,
                        AccountId = iOpp.AccountId,
                        ContactId = lMapOppIdContactId.get( iOpp.Id )
                ) );
            }
        }

        try{
            Database.DMLOptions lOptions = new Database.DMLOptions();
            lOptions.assignmentRuleHeader.useDefaultRule = false;

            Database.insert( lLstCancellationCase , lOptions );
        } catch( Exception lExcep ){
            System.debug('Error Message: ' + lExcep.getMessage());
            System.debug('Error Stack Trace: ' + lExcep.getStackTraceString());
        }
    }
    
    private Map< Id , Id > getContactIdByOppId(){
        Map< Id , Id > lMapOppIdContactId = new Map< Id , Id >();
        
        for( Opportunity iOpp : [ SELECT SyncedQuote.ContactId FROM Opportunity WHERE Id =: gSetOppId AND SyncedQuoteId != null ] ){
        	lMapOppIdContactId.put( iOpp.Id , iOpp.SyncedQuote.ContactId );
        }
        
        return lMapOppIdContactId;
    }

    private Set<Id> getOppsIdsWithOpenCases(List< Opportunity > aLstOppToCreateCancellationCase) {
        Set<Id> oppsIdsWithOpenCase = new Set<Id>();
        Set<Id> oppIdSet  = new Set<Id>();

        for (Opportunity opp : aLstOppToCreateCancellationCase) {
            oppIdSet.add(opp.Id);
        }

        List<Case> cases = getCases(oppIdSet);

        for (Case iCase : cases) {
            oppsIdsWithOpenCase.add(iCase.OpportunityId__c);
        }

        return oppsIdsWithOpenCase;
    }

    private List<Case> getCases(Set<Id> oppIdSet)  {
        List<String> caseStatusList = new List<String>{
            'Reverted', 'Confirmed'
        };

        return [
            SELECT Id, OpportunityId__c
            FROM Case
            WHERE OpportunityId__c IN :oppIdSet
            AND Status NOT IN :caseStatusList
            ORDER BY CreatedDate DESC
        ];
    }
}