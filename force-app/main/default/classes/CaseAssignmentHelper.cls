/**
 * @author Mauricio Copetti - GFT
 */
public class CaseAssignmentHelper {
    @InvocableMethod
    public static void assignCases(List<Case> caze){
        
        Database.DMLOptions dmo = new Database.DMLOptions();
        dmo.AssignmentRuleHeader.useDefaultRule= true;
        dmo.EmailHeader.triggerUserEmail = true;
        
        for (Case obj:caze){
            obj.setOptions(dmo);
        } 
        update caze;
    }  
}