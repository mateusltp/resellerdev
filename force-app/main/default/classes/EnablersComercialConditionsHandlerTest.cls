/**
 * @description       : 
 * @author            : AlyssonMota
 * @group             : 
 * @last modified on  : 07-02-2021
 * @last modified by  : Alysson Mota
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   11-30-2020   Alysson Mota   Initial Version
**/
@isTest
public with sharing class EnablersComercialConditionsHandlerTest {

    @TestSetup
    static void setup(){
        Pricebook2 standardPricebook = new Pricebook2(
	        Id = Test.getStandardPricebookId(),
	        IsActive = true
        );

        update standardPricebook;
    }

    @isTest
    public static void testWithWaiver() {
        Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
        Id oppRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        Id quoteRecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();

        List<QuoteLineItem> itemsToInsert = new List<QuoteLineItem>();
        List<Payment__c> paymentsToInsert = new List<Payment__c>();
        List<Eligibility__c> eligibilititesToInsert = new List<Eligibility__c>();
        List<Waiver__c> waiversToInsert = new List<Waiver__c>();

        Account acc = new Account();
        acc.Name = 'Teste';
        acc.BillingCountry = 'United States';
        acc.BillingState = 'New York';
        acc.RecordTypeId = accRecordTypeId;
        insert acc;

        Product2 plan0to100 = new Product2();
        plan0to100.Name = 'Plan 01 (0 - 100)';
        plan0to100.Family = 'Enterprise Subscription';
        plan0to100.IsActive = true;
        plan0to100.Maximum_Number_of_Employees__c = 10;
        insert plan0to100;
        
        insert new PricebookEntry(pricebook2id = Test.getStandardPricebookId(), product2id = plan0to100.id, unitprice=1.0, isActive=true, CurrencyIsoCode='USD');
        
        Pricebook2 pb = new pricebook2(name='Price List US', IsActive=true, Country__c = 'United States');
        insert pb;
        
        PricebookEntry entry1 = new PricebookEntry(pricebook2id=pb.id, product2id=plan0to100.id, unitprice=1.0, isActive=true, CurrencyIsoCode='USD');
        insert entry1;

        Opportunity opp = new Opportunity();
        opp.Name = 'Teste opp';
        opp.RecordTypeId = oppRecordTypeId;
        opp.AccountId = acc.Id;
        opp.StageName = 'Proposta Enviada';
        opp.CloseDate = System.now().date();
        insert opp;

        Quote proposal = new Quote();
        proposal.RecordTypeId = quoteRecordTypeId;
        proposal.Name = 'Teste Prop';
        proposal.OpportunityId = opp.Id;
        proposal.License_Fee_Waiver__c = 'No';
        proposal.Pricebook2Id = pb.Id;
        insert proposal;

        QuoteLineItem item = new QuoteLineItem();
        item.QuoteId = proposal.Id;
        item.UnitPrice = 0;
        item.Product2Id = plan0to100.Id;
        item.PricebookEntryId = entry1.Id;
        item.Quantity = 100;
        item.Discount__c = 65.01;
        itemsToInsert.add(item);
        
        Payment__c payment = new Payment__c();
        payment.Quote_Line_Item__c = item.Id;
        payment.Frequency__c = 'Monthly';
        paymentsToInsert.add(payment);
        
        Eligibility__c eligibility = new Eligibility__c();
        eligibility.Payment__c = payment.Id;
        eligibility.Payment_Method__c = 'Payroll';    
        eligibilititesToInsert.add(eligibility);    	
        
        Waiver__c waiver = new Waiver__c();
        waiver.Payment__c = payment.Id;
        waiver.Percentage__c = 12;
        waiversToInsert.add(waiver);

        QuoteLineItem item2 = new QuoteLineItem();
        item2.QuoteId = proposal.Id;
        item2.UnitPrice = 0;
        item2.Product2Id = plan0to100.Id;
        item2.PricebookEntryId = entry1.Id;
        item2.Quantity = 100;
        item2.Discount__c = 50;
        itemsToInsert.add(item2);
        
        Payment__c payment2 = new Payment__c();
        payment2.Quote_Line_Item__c = item.Id;
        payment2.Frequency__c = 'Monthly';
        paymentsToInsert.add(payment2);
        
        Eligibility__c eligibility2 = new Eligibility__c();
        eligibility2.Payment__c = payment.Id;
        eligibility2.Payment_Method__c = 'Payroll + Credit Card';    
        eligibilititesToInsert.add(eligibility2);
        
        Waiver__c waiver2 = new Waiver__c();
        waiver2.Payment__c = payment.Id;
        waiver2.Percentage__c = 10;
        waiversToInsert.add(waiver2);

        Test.startTest();
        insert itemsToInsert;
        insert paymentsToInsert;
        insert eligibilititesToInsert;
        insert waiversToInsert;
        Test.stopTest();

        EnablersComercialConditionsHandler handler = new EnablersComercialConditionsHandler();
        Boolean testWithWaiver = handler.areConditionsOkForES(opp, item, payment, new List<Waiver__c>{waiver}, new List<Eligibility__c>{eligibility});
        System.assertEquals(false, testWithWaiver);

        /*
        Boolean testWithGreaterThanRm = handler.areConditionsOkForES(opp, item, payment, new List<Waiver__c>{}, new List<Eligibility__c>{eligibility});
        System.assertEquals(false, testWithGreaterThanRm);
        */

        Boolean testWithAllowedDiscount = handler.areConditionsOkForES(opp, item2, payment2, new List<Waiver__c>{}, new List<Eligibility__c>{eligibility2});
        System.assertEquals(true, testWithAllowedDiscount);
    }    
}