/**
 * @description       : 
 * @author            : alysson.mota@gympass.com
 * @group             : 
 * @last modified on  : 06-29-2022
 * @last modified by  : alysson.mota@gympass.com
**/
public without sharing class GymActivityToBeBookedServiceImpl implements IGymActivityToBeBookedService {

    public void createNewToProdFromGymActivityRels(Id prodId, List<Gym_Activity_Relationship__c> gymActivityRels) {
        GymActivityToBeBookedSelector gymActivityToBeBookedSelector = (GymActivityToBeBookedSelector)Application.Selector.newInstance(Gym_Activity_To_Be_Booked__c.sobjectType); 
        
        List<Gym_Activity_To_Be_Booked__c> currentGymActivityToBeBooked = gymActivityToBeBookedSelector.byProdId(new Set<Id>{prodId});
        List<Gym_Activity_To_Be_Booked__c> gymActivitiesToBeBookedToDelete = getGymActivitiesToBeBookedToDelete(gymActivityRels, currentGymActivityToBeBooked);
        List<Gym_Activity_To_Be_Booked__c> gymActivitiesToBeBookedToInsert = getGymActivitiesToBeBookedToInsert(gymActivityRels, currentGymActivityToBeBooked, prodId);
    
        deleteAndInsertRecords(gymActivitiesToBeBookedToDelete, gymActivitiesToBeBookedToInsert);
    }


    private List<Gym_Activity_To_Be_Booked__c> getGymActivitiesToBeBookedToDelete( List<Gym_Activity_Relationship__c> gymActivityRels, List<Gym_Activity_To_Be_Booked__c> currentGymActivityToBeBooked){
        List<Gym_Activity_To_Be_Booked__c> gymActivitiesToBeBookedToDelete = new List<Gym_Activity_To_Be_Booked__c>();
        Map<Id, Gym_Activity_Relationship__c> idToGymActivityRel = new Map<Id, Gym_Activity_Relationship__c>(gymActivityRels);

        for (Gym_Activity_To_Be_Booked__c iGymBooked : currentGymActivityToBeBooked) {
            if (!idToGymActivityRel.containsKey(iGymBooked.GymActivityRelationshipId__c)) {
                gymActivitiesToBeBookedToDelete.add(iGymBooked);
            }
        }

        return gymActivitiesToBeBookedToDelete;
    }

    private List<Gym_Activity_To_Be_Booked__c> getGymActivitiesToBeBookedToInsert(List<Gym_Activity_Relationship__c> gymActivityRels, List<Gym_Activity_To_Be_Booked__c> currentGymActivityToBeBooked, Id prodId) {
        List<Gym_Activity_To_Be_Booked__c> gymActivitiesToBeBookedToInsert = new List<Gym_Activity_To_Be_Booked__c>();
        Map<Id, Gym_Activity_To_Be_Booked__c> gymRelIdToGymBookedId = new Map<Id, Gym_Activity_To_Be_Booked__c>();

        for (Gym_Activity_To_Be_Booked__c gymBooked : currentGymActivityToBeBooked) {
            gymRelIdToGymBookedId.put(gymBooked.GymActivityRelationshipId__c, gymBooked);    
        }

        for (Gym_Activity_Relationship__c gymRel : gymActivityRels) {
            if (!gymRelIdToGymBookedId.containsKey(gymRel.Id)) {
                Gym_Activity_To_Be_Booked__c newGymBooked = new Gym_Activity_To_Be_Booked__c();
                newGymBooked.GymActivityRelationshipId__c = gymRel.Id;
                newGymBooked.Name = gymRel.Name;
                newGymBooked.Product_Item__c = prodId;

                gymActivitiesToBeBookedToInsert.add(newGymBooked);
            }
        }

        return gymActivitiesToBeBookedToInsert;
    }

    private void deleteAndInsertRecords(List<Gym_Activity_To_Be_Booked__c> gymActivitiesToBeBookedToDelete, List<Gym_Activity_To_Be_Booked__c> gymActivitiesToBeBookedToInsert) {
        fflib_ISObjectUnitOfWork uow = Application.UnitOfWork.newInstance();

        uow.registerDeleted(gymActivitiesToBeBookedToDelete);
        uow.registerNew(gymActivitiesToBeBookedToInsert);

        uow.commitWork();
    }
}