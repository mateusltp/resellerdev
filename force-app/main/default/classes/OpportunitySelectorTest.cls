/**
 * @description       : 
 * @author            : gft.jorge.stevaux@ext.gympass.com
 * @group             : 
 * @last modified on  : 04-10-2022
 * @last modified by  : gepi@gft.com
**/

@isTest(seeAllData=false)

private class OpportunitySelectorTest {

    @TestSetup
    static void createData(){
            
        Account lAcc = PartnerDataFactory.newAccount();              
        Database.insert( lAcc );

        Contact lContact = PartnerDataFactory.newContact(lAcc.Id);
        Database.insert(lContact);

        Bank_Account__c lBankAcct = PartnerDataFactory.newBankAcct();
        Database.insert(lBankAcct);

        Acount_Bank_Account_Relationship__c lBankAcctRel = PartnerDataFactory.newBankAcctRel(lAcc.Id, lBankAcct);
        Database.insert(lBankAcctRel);
                                  
        Opportunity lOpp = PartnerDataFactory.newOpportunity( lAcc.Id, 'Partner_Wishlist_Renegotiation' ); 
        Database.insert( lOpp );

        Product_Item__c lProd = PartnerDataFactory.newProduct(lOpp);
        Database.insert(lProd);
    }

    @isTest
    static void getSObjectFieldList_Test(){

        List<Schema.SObjectField> expectedReturn = new List<Schema.SObjectField> {
			Opportunity.Id,
				Opportunity.Name,
				Opportunity.AccountId,
				Opportunity.StageName,
				Opportunity.Owner_Name__c
		};

        Test.startTest();
        System.assertEquals(expectedReturn, new OpportunitySelector().getSObjectFieldList());
        Test.stopTest();
    }

    @isTest
    static void getSObjectType_Test(){

        Test.startTest();
        System.assertEquals(Opportunity.sObjectType, new OpportunitySelector().getSObjectType());
        Test.stopTest();                           
    }

    @isTest
    static void selectById_Test(){

        List<Opportunity> oppLst = [SELECT Id, Name FROM Opportunity LIMIT 1];
        Set<Id> oppIds = (new Map<Id,Opportunity>(oppLst)).keySet();

        Test.startTest();
        System.assertEquals(oppLst.get(0).id, new OpportunitySelector().selectById(oppIds).get(0).id);
        System.assertEquals(oppLst.get(0).id, new OpportunitySelector().byId(oppIds).get(0).id);
        System.assertEquals(oppLst.get(0).id, new OpportunitySelector().selectOpportunityForPartnerProductFlow(oppIds).get(0).id);
        System.assertEquals(oppLst.get(0).id, new OpportunitySelector().selectOpportunityAndChildOppsById(oppIds).get(0).id);
        Test.stopTest();
    }

    @isTest
    static void selectLastWonToClone_Test(){

        List<Account> acctLst = [SELECT Id FROM Account LIMIT 1];
        Set<Id> acctIds = (new Map<Id, Account>(acctLst)).keySet();
        List<Opportunity> oppWon = new List<Opportunity>();
		Map<Id, Opportunity> mAccountOpp = new Map<Id, Opportunity>();

        List<Opportunity> oppLst = [SELECT Id, Name, AccountId, Payment__c, Standard_Payment__c, Justification_Payment__c  FROM Opportunity WHERE AccountId IN : acctIds LIMIT 1];
        system.debug('oppLst ' +oppLst);
        for(Opportunity oppItem : oppLst ){
            oppItem.StageName = 'Lançado/Ganho';
            oppItem.Proposal_Related__c = true;
            oppItem.Product_Related__c = true;
            oppWon.add(oppItem);
        }
        database.update(oppWon);

        List<Opportunity> oppLstWon = [SELECT Id, Name, AccountId FROM Opportunity LIMIT 1];

        For (Opportunity oppAcct : oppLstWon){
            mAccountOpp.put(oppAcct.AccountId, oppAcct);
        }

        Test.startTest();
        System.assertEquals(mAccountOpp.keySet(), new OpportunitySelector().selectLastWonToClone(acctIds).keySet());
        Test.stopTest();
    }

    @isTest
    static void selectLastWonToCloneRT_Test(){

        List<Account> acctLst = [SELECT Id FROM Account LIMIT 1];
        Set<Id> acctIds = (new Map<Id, Account>(acctLst)).keySet();
        List<Opportunity> oppWon = new List<Opportunity>();
		Map<Id, Opportunity> mAccountOpp = new Map<Id, Opportunity>();

        List<Opportunity> oppLst = [SELECT Id, Name, AccountId, Payment__c, Standard_Payment__c, Justification_Payment__c FROM Opportunity WHERE AccountId IN : acctIds LIMIT 1];
        for(Opportunity oppItem : oppLst ){
            oppItem.StageName = 'Lançado/Ganho';
            oppItem.Product_Related__c = true;
            oppItem.Proposal_Related__c = true;
            oppWon.add(oppItem);
        }
        database.update(oppWon);

        List<Opportunity> oppLstWon = [SELECT Id, Name, AccountId FROM Opportunity LIMIT 1];

        For (Opportunity oppAcct : oppLstWon){
            mAccountOpp.put(oppAcct.AccountId, oppAcct);
        }

        Test.startTest();
        System.assertEquals(mAccountOpp.keySet(), new OpportunitySelector().selectLastWonToClone(acctIds, new Set<String>{'Partner_Wishlist_Renegotiation'}).keySet());
        Test.stopTest();
    }

    @isTest

    static void selectPartnerToLostQueryLocator_Test(){

        Set<Id> partnerRenegotiationRT = 
            new Set<Id>{    Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Partner_Small_and_Medium_Renegotiation').getRecordTypeId(),
                            Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Partner_Wishlist_Renegotiation').getRecordTypeId()
                        };  
        
        String QLReturnedFromStart = 'SELECT Id, Name, AccountId, StageName, Owner_Name__c, CurrencyIsoCode, RecordTypeId, ' +
        'cancellation_date__c, Sub_Type__c, Cancellation_Reason__c, ' +
        'Cancellation_Reason_subcategory__c FROM Opportunity ' +
        'WHERE Cancellation_Date__c = TODAY AND ' +
        'StageName = \'Waiting Cancellation\' AND Sub_Type__c = \'Retention\' AND RecordTypeId IN : partnerRenegotiationRT ' +
        'ORDER BY Name ASC NULLS FIRST';

        Test.startTest();
        System.assert(new OpportunitySelector().selectPartnerToLostQueryLocator().getQuery().contains(QLReturnedFromStart));
        Test.stopTest();
    }

    @isTest

    static void selectPartnerToNotifyRenegotiationQueryLocator_Test(){

        Date firstNnotification = Date.today() + 30;
   		Date secondNotification = Date.today() + 60;
    	Date thirdNnotification = Date.today() + 90;

        Set<Id> partnerRenegotiationRT = 
            new Set<Id>{    Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Partner_Small_and_Medium_Renegotiation').getRecordTypeId(),
                            Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Partner_Wishlist_Renegotiation').getRecordTypeId()
                        };  
        
        String QLReturnedFromStart = 'SELECT Id, Name, AccountId, StageName, Owner_Name__c, CurrencyIsoCode, RecordTypeId, ' +
        'OwnerId, cancellation_date__c, Sub_Type__c FROM Opportunity ' +
        'WHERE RecordTypeId IN : partnerRenegotiationRT AND' +
        ' Sub_Type__c = \'Retention\' AND' +
        ' StageName = \'Waiting Cancellation\' AND' +
        ' (Cancellation_Date__c  =: firstNnotification OR' +
        ' Cancellation_Date__c  =: secondNotification OR' +
        ' Cancellation_Date__c  =: thirdNnotification)' +
        ' ORDER BY Name ASC NULLS FIRST ';

        Test.startTest();
        System.debug('query');
        System.debug(new OpportunitySelector().selectPartnerToNotifyRenegotiationQueryLocator().getQuery());
        System.assert(new OpportunitySelector().selectPartnerToNotifyRenegotiationQueryLocator().getQuery().contains(QLReturnedFromStart));
        Test.stopTest();
    }

    @isTest

    static void getDefaultRecordType_Test(){

        Schema.DescribeSObjectResult dsr = Opportunity.SObjectType.getDescribe();
		Schema.RecordTypeInfo defaultRecordType;
		for(Schema.RecordTypeInfo rti : dsr.getRecordTypeInfos()) {
			if(rti.isDefaultRecordTypeMapping()) {
				defaultRecordType = rti;
			}
		}

        Test.startTest();
        System.assertEquals(defaultRecordType, new OpportunitySelector().getDefaultRecordType());
        Test.stopTest();
    }
}