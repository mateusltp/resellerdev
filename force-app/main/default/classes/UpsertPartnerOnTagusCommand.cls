/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 09-14-2021
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/
public with sharing class UpsertPartnerOnTagusCommand extends AbstractUpdateableOutboundCommand {
    private Request request;
    private Object response;

    override public Object transformToSend() {
        if (
          !Schema.SObjectType.Account.isUpdateable() ||
          !Schema.SObjectType.Contact.isUpdateable()
        ) {
          throw new CustomException(
            'You should have update permission in contact and account sobjects to execute this event'
          );
        }
    
        TagusPartnerTransaction.PartnerTransactionType transactionType = event.getEventName()
            .contains('CREATE')
          ? TagusPartnerTransaction.PartnerTransactionType.CREATED
          : TagusPartnerTransaction.PartnerTransactionType.UPDATED;
    
        this.request = new Request();
    
        request.transaction_request = new RequestData();

        request.transaction_request.info = new TagusPartnerTransaction(event, transactionType);         
    
        Map<Id, Account> partnerMap = (Map<Id, Account>) event.getPayloadFromJson(
          Map<Id, Account>.class
        );    
   
        request.transaction_request.data = new TagusPartnerTransactionData(partnerMap);
        return request;


      }
    
      override public void processResult(Object response) {
        this.response = response;
      }
    
      override public void postUpdateExecute() {
        fflib_ISObjectUnitOfWork uow = Application.UnitOfWork.newInstance();
        List<Account> partners = new List<Account>();
        List<Contact> contacts = new List<Contact>();
    
        for (AccountPartnerTagusDTO partnerDTO : request.transaction_request.data.getPartners()) {
          Account partner = new Account(
            Id = partnerDTO.getSalesforceId(),
            UUID__c = partnerDTO.getId(),
            Send_To_Tagus__c = true
          );
    
          partners.add(partner);
          
    
          for (AccountPartnerTagusDTO.PartnerContactDTO partnerContactDTO : partnerDTO.getContacts()) {
            Contact contact = new Contact(
              Id = partnerContactDTO.getSalesforceId(),
              UUID__c = partnerContactDTO.getId()
            );
    
            contacts.add(contact);
          }
        }
    
        uow.registerDirty(partners);
        uow.registerDirty(contacts);
        uow.commitWork();
      }
    
    private class Request {
        private RequestData transaction_request;
    }

    private class RequestData{
        private TagusPartnerTransaction info;
        private TagusPartnerTransactionData data;
    }
}