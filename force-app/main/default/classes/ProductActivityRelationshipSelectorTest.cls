/**
 * @description       : 
 * @author            : gft.jorge.stevaux@ext.gympass.com
 * @group             : 
 * @last modified on  : 05-31-2022
 * @last modified by  : gft.jorge.stevaux@ext.gympass.com
**/
@isTest
public class ProductActivityRelationshipSelectorTest {

    @TestSetup
    static void makeData(){
        Product_Item__c parentProduct = getProductInstance(null, null);
        insert parentProduct;

        List<Gym_Activity_Relationship__c> garLst = new List<Gym_Activity_Relationship__c>();
        Gym_Activity_Relationship__c gar1 = getGymActivityRelInstance();
        Gym_Activity_Relationship__c gar2 = getGymActivityRelInstance();
        garLst.add(gar1);
        garLst.add(gar2);
        insert garLst;
        List<Product_Activity_Relationship__c> parLst = new List<Product_Activity_Relationship__c>();
        Product_Activity_Relationship__c par1 = getPrdActivityRelInstance(gar1, parentProduct);
        Product_Activity_Relationship__c par2 = getPrdActivityRelInstance(gar2, parentProduct);
        parLst.add(par1);
        parLst.add(par2);
        insert parLst;
        
    }

    private static Product_Item__c getProductInstance(Id parentProductId, Id oppId) {
        Id recordTypeId = Schema.SObjectType.Product_Item__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product').getRecordTypeId();
        Product_Item__c product = new Product_Item__c();
        product.Name = 'Boxing';
        product.RecordTypeId = recordTypeId;
        product.Parent_Product__c = parentProductId;
        product.Opportunity__c = oppId;
        product.Price_Visits_Month_Package_Selected__c = 100;
        return product;
    }

    private static Gym_Activity_Relationship__c getGymActivityRelInstance() {
        Gym_Activity_Relationship__c gar = new Gym_Activity_Relationship__c();
        gar.Name = 'Boxing';
        return gar;
    }

    private static Product_Activity_Relationship__c getPrdActivityRelInstance(Gym_Activity_Relationship__c gar, Product_Item__c product) {
        Product_Activity_Relationship__c par = new Product_Activity_Relationship__c();
        par.Name = 'PAR';
        par.Gym_Activity_Relationship__c = gar.Id;
        par.Product__c = product.Id;
        return par;
    }
   
    @isTest 
    public static void selectById_Test(){
        Map<Id, Product_Item__c> mapIdToProd = new Map<Id, Product_Item__c>([SELECT Id FROM Product_Item__c]);
        test.startTest();
        ProductActivityRelationshipSelector prodActSelector = new ProductActivityRelationshipSelector();
        prodActSelector.selectById(mapIdToProd.keySet());
        test.stopTest();
    }

    @isTest 
    public static void selectAllByProductIds_Test(){
        Map<Id, Product_Item__c> mapIdToProd = new Map<Id, Product_Item__c>([SELECT Id FROM Product_Item__c]);
        test.startTest();
        ProductActivityRelationshipSelector prodActSelector = new ProductActivityRelationshipSelector();
        prodActSelector.selectAllByProductIds(mapIdToProd.keySet());
        test.stopTest();
    }

    @isTest 
    public static void selectActivityByProductId_Test(){
        Map<Id, Product_Item__c> mapIdToProd = new Map<Id, Product_Item__c>([SELECT Id FROM Product_Item__c]);
        test.startTest();
        ProductActivityRelationshipSelector prodActSelector = new ProductActivityRelationshipSelector();
        prodActSelector.selectActivityByProductId(mapIdToProd.keySet());
        test.stopTest();
    }

}