/*
 * @author: Bruno Pinho
 * @description: Utils class with helper methods
 * @date: February/2019
 * @updated: Alysson - December/2019
 */
public class Utils {
    public enum CalloutExecutionStatus {EXECUTED, EXCEPTION_ERROR}

    public static Map<Id, Set<String>>  sObjMethodMap = new Map<Id, Set<String>>();

    public static String doHTTPCallout(CalloutWrapper calloutWrapper) {
        String strResponse = '';
        
        System.debug('Endpoint: ' + calloutWrapper.endpoint);
        System.debug('Method: ' + calloutWrapper.method);
        System.debug('Header Params: ' + calloutWrapper.headerParams);
        System.debug('Body: ' + calloutWrapper.body);
        
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(calloutWrapper.endpoint);
        request.setMethod(calloutWrapper.method);
        request.setBody(calloutWrapper.body);
        request.setTimeout(100000);
        System.debug('body :' + calloutWrapper.body);
        
        for (String key: calloutWrapper.headerParams.keySet()) {
            String value = calloutWrapper.headerParams.get(key);
            request.setHeader(key, value);
        }
        
        try {
            HttpResponse response = http.send(request);
            System.debug('Status Code: ' + response.getStatusCode());
            
            if (response.getStatusCode() == 200) {
                strResponse = response.getBody();
                System.debug('HTTP 200: ' + strResponse);
            } else {
                System.debug('The status code returned was not expected: ' + response.getStatusCode() + ' ' + response.getStatus());
            }
        } catch (Exception e) {
            System.debug('Exception e:' + e.getMessage());
            exceptionDebugger(e);
        }
        
        return strResponse;
    }
    
    public static String doHTTPCalloutWithoutBody(CalloutWrapper calloutWrapper) {
        String strResponse = '';
        
        System.debug('Endpoint: ' + calloutWrapper.endpoint);
        System.debug('Method: ' + calloutWrapper.method);
        System.debug('Header Params: ' + calloutWrapper.headerParams);
        
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(calloutWrapper.endpoint);
        request.setMethod(calloutWrapper.method);
        request.setTimeout(100000);
        
        for (String key: calloutWrapper.headerParams.keySet()) {
            String value = calloutWrapper.headerParams.get(key);
            request.setHeader(key, value);
        }
        
        try {
            HttpResponse response = http.send(request);
            
            System.debug('Status Code: ' + response.getStatusCode());
            
            if (response.getStatusCode() == 200) {
                strResponse = response.getBody();
                System.debug('HTTP 200: ' + strResponse);
            } else {
                System.debug('The status code returned was not expected: ' + response.getStatusCode() + ' ' + response.getStatus());
            }
        } catch (Exception e) {
            exceptionDebugger(e);
        }
        
        return strResponse;
    }
    
    public static CalloutStructResponse doHTTPCallout(Integration_Request__c integrationRequest) {
        System.debug('Util');
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(integrationRequest.Endpoint__c);
        request.setMethod(integrationRequest.HTTP_Request_Method__c);
        request.setBody(integrationRequest.Payload__c);
        request.setTimeout(100000);
        
        //TODO: Define headers paramenters and where they should be set
        String sToken = System.Label.HeimdallToken;
        request.setHeader('Content-Type', 'application/json');
        request.setHeader('Authorization', 'Bearer ' + sToken);

        
        CalloutStructResponse calloutStructResponse = new CalloutStructResponse();
        
        try {
            HttpResponse response = http.send(request);
            calloutStructResponse.calloutExecutionStatus = CalloutExecutionStatus.EXECUTED;
            calloutStructResponse.httpResponse = response;
            calloutStructResponse.integrationRequest = integrationRequest;
            System.debug(response.getBody());
            System.debug(response.getStatus());
            System.debug(response.getStatusCode());
        } catch (Exception e) {
            System.debug('Exception e:' + e.getMessage());
            calloutStructResponse.calloutExecutionStatus = CalloutExecutionStatus.EXCEPTION_ERROR;
            calloutStructResponse.exceptionErrorMessage = exceptionDebugger(e);
            calloutStructResponse.integrationRequest = integrationRequest;
        }
        
        return calloutStructResponse;
    }
    
    public static String exceptionDebugger(Exception e) {
        System.debug('An unhandled exception has occurred. Please find the details: ');
        System.debug('Type Name: ' + e.getTypeName());
        System.debug('Message: ' + e.getMessage());
        System.debug('Cause: ' + e.getCause());
        System.debug('Line Number: ' + e.getLineNumber());
        System.debug('Stack Trace: ' + e.getStackTraceString());
        
        String exMessage = 'An unhandled exception has occurred. Please find the details: \n';
        exMessage += 'Type Name: ' + e.getTypeName() + '\n';
        exMessage += 'Type Name: ' + e.getMessage() + '\n';
        exMessage += 'Type Name: ' + e.getCause() + '\n';
        exMessage += 'Type Name: ' + e.getLineNumber() + '\n';
        exMessage += 'Type Name: ' + e.getStackTraceString() + '\n';
        
        return exMessage; 
    }
    
    public static void exceptionDebuggerWithSlackNotification(Exception e) {
        System.debug('An unhandled exception has occurred. Please find the details: ');
        System.debug('Type Name: ' + e.getTypeName());
        System.debug('Message: ' + e.getMessage());
        System.debug('Cause: ' + e.getCause());
        System.debug('Line Number: ' + e.getLineNumber());
        System.debug('Stack Trace: ' + e.getStackTraceString());

        String message = '<!here> It seems like an *unhandled exception* has occurred.' + 'Type Name: ' + e.getTypeName() + '\n' + 'Message: ' + e.getMessage() + '\n' + 'Cause: ' + e.getCause() + '\n' + 'Line Number: ' + e.getLineNumber() + '\n' + 'Stack Trace: ' + e.getStackTraceString() + '\n`';
        
        SlackPublisher.postToSlack(message);
    }
    
    public static String getQueryWithAllFieldsFromObject(String objectName) {   
        String query = 'SELECT ';
        Map<String, Schema.SObjectField> objectFields = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();
        
        for (String s : objectFields.keySet()) {
            if (s != 'BillingAddress' && s != 'ShippingAddress' && s != 'Address' && s != 'iqscore' && s != 'completeddatetime' && s!= 'additionaladdress' && s!='QuoteToAddress' && s != 'OtherAddress' && s != 'MailingAddress')
                query += s + ',';
        }
        
        if (query.subString(query.Length()-2,query.Length()-1) == ',')
            query = query.subString(0,query.Length()-2);
        
        query += ' FROM ' + objectName;
        System.debug('query: '+query);
        
        return query;
    }
    
    public static String getQueryWithSomeFieldsFromObject(String objectName, List<String> customFields) {
        System.debug('objectName: ' + objectName);
        System.debug('customFields: ' + customFields);
        
        String query = 'SELECT ';
        String queryFields = '';
        
        Map<String, Schema.SObjectField> objectFields = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();
        Map<String, Boolean> mapValidFields = getMapWithValidFieldsForCSV(objectName);
        List<String> objectsFromActivity = new List<String>{'Event', 'Task'};
        System.debug('mapValidFields: '+mapValidFields);
        
        if (!objectsFromActivity.contains(objectName)) {
            for (String s : objectFields.keySet()) {
                if (queryFields == '') {
                    if (!s.contains('__c') && mapValidFields.get(s.toLowerCase()) == TRUE && s != 'iqscore' && s != 'completeddatetime')       
                        queryFields += s;
                } else {
                    if (!s.contains('__c') && mapValidFields.get(s.toLowerCase()) == TRUE && s != 'iqscore' && s != 'completeddatetime')       
                        queryFields += ','+s;
                }
            }
            
            for (String s: customFields) {
                if (s != 'iqscore' && s != 'completeddatetime' && mapValidFields.get(s.toLowerCase()) == TRUE)
                    queryFields += ','+s;
            }
            
            query += queryFields;
        } 
        else {
            query += getFieldsFromObjectsFromActivity(objectName, objectFields, mapValidFields); 
        }
        
        query += ' FROM ' + objectName;
        System.debug('query: '+query);
        
        return query;
    }
    
    public static Set<String> getSetWithAllFieldsFromObject(String objectName) {
        Map<String, Schema.SObjectField> objectFields = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();

        return objectFields.keySet();
    }

    public static String getSobjectFieldsDevNameForQuery( String sObjectDevName ){
        Set<String> lSetFieldsToIgnore = new Set<String>{'id','billingaddress','shippingaddress','address','iqscore',
            'completeddatetime','additionaladdress','quotetoaddress','otheraddress','mailingaddress'};

        Set< String > lSetFieldsDevName = getSetWithAllFieldsFromObject(sObjectDevName);

        lSetFieldsDevName.removeAll(lSetFieldsToIgnore);

        return String.join( new List<String>(lSetFieldsDevName) , ',' );
    }

    public static String getFieldsFromLayoutForQuery( String sObjectName , String layoutName ){
        Set< String > lFields = new Set< String >();
        List<Metadata.Metadata> lLayouts = 
            Metadata.Operations.retrieve(Metadata.MetadataType.Layout, 
                                        new List<String> {sObjectName + '-' + layoutName});

        Metadata.Layout lLayoutMd = (Metadata.Layout)lLayouts.get(0);
        for (Metadata.LayoutSection iSection : lLayoutMd.layoutSections) {
            for (Metadata.LayoutColumn iColumn : iSection.layoutColumns) {
                if (iColumn.layoutItems != null) {
                    for (Metadata.LayoutItem iItem : iColumn.layoutItems) {
                        lFields.add(iItem.field);
                    }
                }
            }
        }
        
        lFields.remove(null);

        return String.join( new List<String>(lFields) , ',' );
    }
    
    public static Map<String, Boolean> getMapWithValidFieldsForCSV(String objName) {
        Map<String, Boolean> mapValidFields = new Map<String, Boolean>();
        
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType leadSchema = schemaMap.get(objName);
        Map<String, Schema.SObjectField> fieldMap = leadSchema.getDescribe().fields.getMap();
        
        for (String fieldName: fieldMap.keySet())
        {
            String fielddataType = String.valueOf(fieldMap.get(fieldName).getDescribe().getType());
            
            if (fielddataType != 'TEXTAREA' && fielddataType != 'ADDRESS')
                mapValidFields.put(fieldName, true);
            else
                mapValidFields.put(fieldName, false);
        }
        
        return mapValidFields;
    }

    // Alysson - 12/06/2019
    public static String getFieldsFromObjectsFromActivity(String objectName, Map<String, Schema.SObjectField> objectFields, Map<String, Boolean> mapValidFields) {
        String queryFields = '';
        for (String s : objectFields.keySet()) {
            if (queryFields == '') {
                if (mapValidFields.get(s.toLowerCase()) == TRUE && s != 'iqscore' && s != 'completeddatetime')
                    queryFields += s;
            }
            else {
                if (mapValidFields.get(s.toLowerCase()) == TRUE && s != 'iqscore' && s != 'completeddatetime')
                    queryFields += ','+s;
            }
        }

        return queryFields;
    }
    
    public class CalloutStructResponse {
        public HttpResponse httpResponse {get; set;}
        public CalloutExecutionStatus calloutExecutionStatus {get; set;}
        public String exceptionErrorMessage {get; set;}
        public Integration_Request__c integrationRequest {get; set;}
    }
    
    public class CalloutWrapper {
        public String endpoint {get; set;}
        public String method {get; set;}
        public Map<String, String> headerParams {get; set;}
        public String body {get; set;}
    }

    public static String getRegionByCountry(String CountryName){
        try {
                RegionByCountry__mdt regionMtd = [SELECT Region__c FROM RegionByCountry__mdt 
                                            WHERE MasterLabel =: CountryName
                                            LIMIT 1];
                return regionMtd.Region__c;

        } catch (Exception e){
            return 'ALL'; //region not found on mtd
        }
    }
    
    public static Map< String , String > getMapContryCodeRegionFromMdt(){    
        Map< String , String > lMapCountryCodeRegion = new Map< String , String >();
        
        for( RegionByCountry__mdt iRegionMdt : [ SELECT MasterLabel, Region__c, Country_Code__c FROM RegionByCountry__mdt ] ){
            lMapCountryCodeRegion.put( iRegionMdt.Country_Code__c , iRegionMdt.Region__c );
        }
        
        return lMapCountryCodeRegion;
    }

    public static Map< String , String > getCountryCodeByCountry(){    
        Map<String, String> CountryToCountryCodeMap = new Map<String, String>();
        Schema.DescribeFieldResult fieldResult = User.Countrycode.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple){
            CountryToCountryCodeMap.put(f.getLabel(),  f.getValue());
        }
        return CountryToCountryCodeMap;
    }

    public static String capitalizeFully(String myString){
        if( String.isBlank( myString ) ){ return ''; }

        List<String> myStrings = myString.split(' ');
        for (Integer i = 0; i < myStrings.size(); i++){
            myStrings[i] = myStrings[i].ToLowerCase();
            myStrings[i] = myStrings[i].capitalize();    
        }            
        myString = String.join(myStrings, ' ');
        return myString;
    }

    //return set id not triggered/handled yet
    public static Boolean isSemaphored(String methodName, Id sObjId){       
        Set<String> methodLst = sObjMethodMap.get(sObjId);
        if(sObjMethodMap.get(sObjId)==null){
            sObjMethodMap.put(sObjId, new Set<String>{methodName});
            return false;
        } else {
            if(sObjMethodMap.get(sObjId).contains(methodName)){
                return true;
            } else {
                sObjMethodMap.get(sObjId).add(methodName);
                return false;
            }
        }
    }

     
    public static Map<String,List<String>> getPicklistValues( String objectName, String fieldName, String recordTypeName) {
        return getPicklistValues(objectName, fieldName, recordTypeName, false);
    }  
     
  	public static Map<String,List<String>> getPicklistValues( String objectName, String fieldName, String recordTypeName, Boolean isDependentPicklist) {
		
        System.debug(objectName + ' ' + fieldName + ' ' + recordTypeName);

        if(String.isBlank( objectName) || String.isBlank(fieldName)){
            return null;
        }
        String recordTypeId;
        if( !String.isBlank( recordTypeName ) ){
            recordTypeId = Schema.getGlobalDescribe().get( objectName ).getDescribe().getRecordTypeInfosByDeveloperName().get( recordTypeName ).getRecordTypeId();
        }
        if( recordTypeId == null || recordTypeId == '' ){
            recordTypeId = Schema.getGlobalDescribe().get( objectName ).getDescribe().getRecordTypeInfosByDeveloperName().get( 'Master' ).getRecordTypeId(); 
        }
      
        Map<String, List<String>> mapPicklistValues = new Map<String, List<String>>();        
        mapPicklistValues = isDependentPicklist ? getDependentPicklist( objectName, recordTypeId, fieldName ) : getValuesPicklist( objectName, recordTypeId, fieldName );
        return mapPicklistValues;
  	}

    private static Map<String, List<String>> getValuesPicklist(String objName, String recordTypeId, String fieldName){  
        List<String> vPckLst = new List<String>();            
        String urlObjName = '/'+objName+'/';
        String urlRecordTypeId = '/'+recordTypeId;
        String urlFieldName = '/'+fieldName;        
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        String url = 'callout:UIAPINamedCredential/services/data/v51.0/ui-api/object-info'+urlObjName+'picklist-values'+urlRecordTypeId+urlFieldName;
        request.setEndpoint(url);
        request.setMethod('GET');
        request.setHeader('Authorization', 'OAuth '+UserInfo.getSessionId());

        try{
            HttpResponse response = http.send(request);
            if( response.getStatus() == 'OK' && response.getStatusCode() == 200 ) {               
                Map<String,Object> root = (Map<String,Object>) JSON.deserializeUntyped( response.getBody() );
                if(root.get( 'values' ) != null){
                    List<Object> pValues = (List<Object>) root.get( 'values' );
                    for(Object pValue : pValues) {                
                        Map<String,Object> pValueMap = (Map<String,Object>)pValue;
                        vPckLst.add((String) pValueMap.get('label'));
                    }
                }
            } else {                
                System.debug('UI API Error ' + response.getStatus() + ' ' + response.getStatusCode());
                System.debug('Response ' + response);
                
            }
        } catch (Exception e){
            System.debug('Exception e:' + e.getMessage());
        }
       
        Map<String, List<String>> allPickLstValues = new Map<String, List<String>>{fieldName => vPckLst};
        return allPickLstValues;
    }

    public static Map<String, List<String>> getDependentPicklist(String objName, String recordTypeId, String fieldName) {
       
        List<String> vPckLst = new List<String>();            
        String urlObjName = '/'+objName+'/';
        String urlRecordTypeId = '/'+recordTypeId;
        String urlFieldName = '/'+fieldName;        
        Http http = new Http();
        
        HttpRequest request = new HttpRequest();
        String url = 'callout:UIAPINamedCredential/services/data/v51.0/ui-api/object-info'+urlObjName+'picklist-values'+urlRecordTypeId+urlFieldName;
        EncodingUtil.urlEncode(url,'UTF-8');
        request.setEndpoint(url);        
        request.setMethod('GET');
        request.setHeader('Authorization', 'OAuth '+UserInfo.getSessionId());
        Map<String, String> result = new Map<String,String>();
        Map<String, List<String>> mapControllingWithDependentList = new Map<String,List<String>>();
        Map<Object, String> mapControllingValueWithIndex = new Map<Object,String>();
        Map<String, List<String>> mapPicklistValues = new Map<String,List<String>>();

        try{
            HttpResponse response = http.send(request);               
            if( response.getStatus() == 'OK' && response.getStatusCode() == 200 ) { 
                Map<String,Object> root = (Map<String,Object>) JSON.deserializeUntyped( response.getBody() );
                if( root.containsKey('controllerValues') ) {                
                    Map<String, Object> controllingValues = (Map<String, Object>) root.get( 'controllerValues' );
                    for( String cValue: controllingValues.keySet() ) {                    
                        mapControllingValueWithIndex.put( controllingValues.get(cValue), cValue );
                    }
                }   
                if( !root.containsKey( 'values' ) ){                 
                    return mapControllingWithDependentList; 
                }
                
                List<Object> pValues = (List<Object>) root.get( 'values' );
                for(Object pValue : pValues) {                
                    Map<String,Object> pValueMap = (Map<String,Object>)pValue;
                    result.put( (String) pValueMap.get('value'), (String) pValueMap.get('label') );                
                    for(Object validfor : (List<Object>)pValueMap.get('validFor')) {
                        if( mapControllingValueWithIndex.containsKey( validfor ) ) {                            
                            if( !mapControllingWithDependentList.containsKey( mapControllingValueWithIndex.get( validfor ) ) ) {
                                
                                mapControllingWithDependentList.put( mapControllingValueWithIndex.get( validfor ), new List<String>() );
                            }                            
                            mapControllingWithDependentList.get( mapControllingValueWithIndex.get( validfor ) ).add( (String) pValueMap.get( 'label' ) );    
                        }
                    }                
                }   
                for( String controllingFields : mapControllingValueWithIndex.Values() ){
                    if( !mapPicklistValues.containsKey( controllingFields ) ) {                    
                        mapPicklistValues.put(controllingFields,new List<String>());
                    }
                    if(mapPicklistValues.containsKey( controllingFields ) && 
                    mapControllingWithDependentList.containsKey( controllingFields ) ) {
                        
                        mapPicklistValues.get( controllingFields ).addAll( mapControllingWithDependentList.get( controllingFields ) );
                    } 
                }            
            }else{
                System.debug('UI API Error ' + response.getStatus() + ' ' + response.getStatusCode());
                System.debug('Response ' + response);
                System.debug( 'mapPicklistValues : ' + JSON.serializePretty( mapPicklistValues ) );
            } 
        } catch (Exception e){
            System.debug('Exception e:' + e.getMessage());
        } 
        return mapPicklistValues;
    }
}