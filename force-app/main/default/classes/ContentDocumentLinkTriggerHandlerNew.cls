/**
 * @description       : 
 * @author            : ext.gft.marcus.silva@gympass.com
 * @group             : 
 * @last modified on  : 03-17-2022
 * @last modified by  : ext.gft.marcus.silva@gympass.com
**/
public without sharing class ContentDocumentLinkTriggerHandlerNew extends TriggerHandler {
    
    public override void afterInsert(){
        ContentDocumentLinkTriggerHandlerHelper.cloneContractAgreementFileFromContentDocumentLinkTrigger(trigger.new); 
        
    }
    
    public override void afterUpdate(){
        ContentDocumentLinkTriggerHandlerHelper.cloneContractAgreementFileFromContentDocumentLinkTrigger(trigger.new); 
        
    }
    
}