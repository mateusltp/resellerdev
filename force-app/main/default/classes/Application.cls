/**
 * @description This class is used to create a factory for our application we are working on. Basically
 * it allows you to send in an object type (or something similar) to have different classes returned to you.
 */
public with sharing class Application {


	// Configure and create the UnitOfWorkFactory for this Application
	public static final fflib_Application.UnitOfWorkFactory UnitOfWork = 
		new fflib_Application.UnitOfWorkFactory(
				new List<SObjectType> { 
                    Contact.SObjectType,
                    Account.SObjectType,                    
                    AccountContactRelation.SobjectType,
					Opportunity.SObjectType,
                    Account_Opportunity_Relationship__c.SObjectType,
                    Quote.sObjectType,
                    Gym_Activity__c.SObjectType,
                    Order.SObjectType,
                    Bank_Account__c.SObjectType,
                    Acount_Bank_Account_Relationship__c.SObjectType,
                    Product_Item__c.SObjectType,
                    Commercial_Condition__c.SObjectType,
                    Product_Assignment__c.SObjectType,
                    Product_Opportunity_Assignment__c.SObjectType,
                    Gym_Activity_Relationship__c.SObjectType,
                    Product_Activity_Relationship__c.SObjectType,
                    Threshold__c.SObjectType,
                    Product_Threshold_Member__c.SObjectType,
                    ContentVersion.SObjectType,
                    FileObject__c.SObjectType,
                    ContentDocument.SObjectType,
                    ContentDocumentLink.SObjectType,
                    DebugLog__c.SObjectType,
                    APXT_Redlining__Contract_Agreement__c.SObjectType,
                    Account_Contract_Agreement_Relationship__c.SObjectType,
                    Partner_Contract_Anexx_Info__c.SObjectType,
                    Ops_Setup_Validation_Form__c.SObjectType,
                    Step_Towards_Success_Partner__c.SObjectType,
                    Opening_Hours__c.SObjectType,
                    Gym_Activity_To_Be_Booked__c.SObjectType
                });
				  	
    // Configure and create the ServiceFactory for this Application
    public static final fflib_Application.ServiceFactory Service = 
        new Application.ServiceFactory( 
            new Map<Type, Type> {   

                    
            });

    // Configure and create the ServiceFactory for this Application by RecordTypeDevName
     public static final fflib_Application.ServiceFactory ServiceByRecordType = new fflib_Application.ServiceFactory( 
        new Map<String, Type> {
            'Account.Gym_Partner' => AccountPartnerServiceImpl.class, //ok
            'Opportunity.Gyms_Small_Medium_Partner' => OpportunityPartnerServiceImpl.class, //ok
            'Opportunity.Partner_Small_and_Medium_Renegotiation' => OpportunityPartnerServiceImpl.class,
            'Opportunity.Gyms_Wishlist_Partner' => OpportunityPartnerServiceImpl.class, //ok
            'Opportunity.Partner_Wishlist_Renegotiation' => OpportunityPartnerServiceImpl.class,
            'Account_Opportunity_Relationship__c.Partners_AOR' => AccountOpportunityRelPartnerServiceImpl.class,//ok
            'Contact.Gyms_Partner' => ContactPartnerServiceImpl.class, //ok
            'Opportunity.Partner_Flow_Opportunity' => OpportunityPartnerServiceImpl.class,
            'Opportunity.Partner_Flow_Child_Opportunity' => OpportunityPartnerServiceImpl.class,
            'Account.Partner_Flow_Account'=> AccountPartnerServiceImpl.class,
            'Contact.Partner_Flow_Contact' => ContactPartnerServiceImpl.class,
            'Account_Opportunity_Relationship__c.Partner_Flow_Opportunity_Member' => AccountOpportunityRelPartnerServiceImpl.class,
            'Product_Opportunity_Assignment__c.Partner_Flow_Product_Opportunity_Assignment' => ProdOppAssignmentServiceImpl.class,
            // 'ContentDocument.Content_Version' => ContentDocumentServiceImpl.class,
            'Product_Item__c.Partner_Flow_Product' => ProductItemPartnerServiceImpl.class,
            'Ops_Setup_Validation_Form__c.Partner_Flow_Ops_Form' => OpsSetupFormPartnerServiceImpl.class,
            'Step_Towards_Success_Partner__c.Partner_Flow_Opportunity_Step' => PartnerStepsTowardsSuccessServiceImpl.class,
            //'Lead.Partner_Owner_Referral_Lead' =>LeadPartnerServiceImpl.class,
            'Gym_Activity_To_Be_Booked__c' =>  GymActivityToBeBookedServiceImpl.class
        }
    );
                    
    // Configure and create the SelectorFactory for this Application
    public static final fflib_Application.SelectorFactory Selector = 
        new fflib_Application.SelectorFactory(
            new Map<SObjectType, Type> {
                    Account.SObjectType => AccountSelector.class,
                    Opportunity.SObjectType => OpportunitySelector.class,
                    Quote.SObjectType => ProposalSelector.class,
                    Account_Opportunity_Relationship__c.SObjectType => AccountOpportunitySelector.class,
                    Product_Item__c.SObjectType => ProductItemSelector.class,
                    OrgWideEmailAddress.SObjectType => OrgWideEmailAddressSelector.class,
                    Gym_Activity__c.SObjectType => GymActivitySelector.class,                    
                    EmailTemplate.sObjectType => EmailTemplateSelector.class,
                    CustomNotificationType.SObjectType => CustomNotificationTypeSelector.class,
                    Order.SObjectType => OrderSelector.class,
                    Contact.SObjectType => ContactSelector.class,
                    Product_Assignment__c.SObjectType => ProductAssignmentSelector.class,
                    Commercial_Condition__c.SObjectType =>  CommercialConditionSelector.class,
                    Product_Item__c.SObjectType => ProductItemSelector.class,
                    AccountContactRelation.sobjectType => AccountContactRelationSelector.class,
                    Gym_Activity_Relationship__c.SObjectType => GymActivityRelSelector.class,
                    Product_Opportunity_Assignment__c.SObjectType => ProductOpportunityAssignmentSelector.class,
                    Product_Activity_Relationship__c.SObjectType => ProductActivityRelationshipSelector.class,
                    Product_Threshold_Member__c.SObjectType =>  ProductThresholdMemberSelector.class,
                    AccountContactRelation.SObjectType => AccountContactRelationSelector.class,
                    Ops_Setup_Validation_Form__c.SObjectType => OpsSetupFormSelector.class,
                    APXT_Redlining__Contract_Agreement__c.SObjectType => ContractAgreementSelector.class,
                    Account_Contract_Agreement_Relationship__c.SobjectType => AccountContractAgreementRelSelector.class,
                    Acount_Bank_Account_Relationship__c.SObjectType => AccountBankAccountSelector.class,
                    FileObject__c.SobjectType => FileObjectSelector.class,
                    Step_Towards_Success_Partner__c.SobjectType => StepTowardsSuccessSelector.class,
                    Payment__c.SObjectType => PaymentSelector.class,
                    Opening_Hours__c.SObjectType => OpeningHourSelector.class,
                    ContentVersion.SObjectType => ContentVersionSelector.class,
                    Event.SObjectType => EventSelector.class,
                    EventWhoRelation.SObjectType => EventWhoRelationSelector.class,
                    ContentDocumentLink.SObjectType => ContentDocumentLinkSelector.class,
                    Gym_Activity_To_Be_Booked__c.SObjectType => GymActivityToBeBookedSelector.class
                });
           
    // Customised Service factory to support developer overrides via Custom Metadata (see Chapter 5)      
    private class ServiceFactory extends fflib_Application.ServiceFactory {
        
        private Map<String, String> servicesByClassName = new Map<String, String>();
        
        public ServiceFactory(Map<Type, Type> serviceInterfaceTypeByServiceImplType) {
            super(serviceInterfaceTypeByServiceImplType);
            
            // Map of overriden services defined by the developer in this org
            for(Service__mdt serviceOverride : 
                    [select DeveloperName, NamespacePrefix, ApexClass__c from Service__mdt]) {
                servicesByClassName.put(
                    serviceOverride.NamespacePrefix + '.' + serviceOverride.DeveloperName, 
                    serviceOverride.ApexClass__c);                
            }
        }
        
        public override Object newInstance(Type serviceInterfaceType) {
            
            // Has the developer overriden the Service implementaiton in their org?
            if(!Test.isRunningTest() && servicesByClassName.containsKey(serviceInterfaceType.getName())) {
                String overridenServiceImpl = servicesByClassName.get(serviceInterfaceType.getName());
                return Type.forName(overridenServiceImpl).newInstance();
            }
            
            // Default Service Factory returns mocked or registered default implementation
            return super.newInstance(serviceInterfaceType);    
        }        
    }

    /**
     * Utility method to throw Application exceptions to Lightning Components
     **/
    public static void throwAuraHandledException(Exception e) {
        String message = e.getMessage();
        AuraHandledException error = new AuraHandledException(message);
        error.setMessage(message); // Ensure Apex tests can assert the error message value
        throw error;
    }


}