@isTest(seeAllData=false)
public without sharing class DealDeskOperationalChangeBillingTest {
     
    @TestSetup
    static void createData(){      
        Account acc = getAccount();
        insert acc;
        
        Pricebook2 standardPricebook = new Pricebook2(
            Id = Test.getStandardPricebookId(),
            IsActive = true
        );
        update standardPricebook;
        Pricebook2 pb = standardPricebook;
        
        Product2 accessFeeProduct = getProduct('Enterprise Subscription');
        insert accessFeeProduct;
        
        Product2 setupFeeProduct = getProduct('Setup Fee');
        insert setupFeeProduct; 
        
        Opportunity opp = new Opportunity(CurrencyIsoCode='BRL');

        PricebookEntry accessFeeEntry = getPricebookEntry(pb, accessFeeProduct, opp);
        insert accessFeeEntry;
        
        PricebookEntry setupFeeEntry = getPricebookEntry(pb, setupFeeProduct, opp);
        insert setupFeeEntry;
        
        Product2 pProfServicesOneFee = getProfServicesOneFeeProduct();
        insert pProfServicesOneFee;
        
        PricebookEntry profServicesOneFeeEntry = getPricebookEntry(pb, pProfServicesOneFee, opp);
        insert profServicesOneFeeEntry;
        
        Product2 pProfServicesMainFee = getProfServicesMainFeeProduct();
        insert pProfServicesMainFee;
        
        PricebookEntry profServicesMainFeeEntry = getPricebookEntry(pb, pProfServicesMainFee, opp);
        insert profServicesMainFeeEntry;
        
        opp = getNewOpp(acc, pb);
        insert opp;

        Quote qt = getQuote(opp);
        insert qt;
        
        //access fee
        QuoteLineItem accessFee = getAccessFee(qt, accessFeeEntry);
        insert accessFee;        
        Payment__c payForAccessFee = getPaymentForFee(accessFee);
        insert payForAccessFee;        

        Eligibility__c eli = getEligibilityForPayment(payForAccessFee);
        insert eli;
                
        //setup fee
        QuoteLineItem setupFee = getSetupFee(qt, setupFeeEntry);
        insert setupFee;
        
        Payment__c payForSetupFee = getPaymentForFee(setupFee);
        insert payForSetupFee;
        
        //pro services one fee
        QuoteLineItem proServicesOneFee = getProServicesOneFee(qt, profServicesOneFeeEntry);
        insert proServicesOneFee;
        
        Payment__c payForProOneFee = getPaymentForFee(proServicesOneFee);
        insert payForProOneFee;
        
        // pro services main fee
        QuoteLineItem proServicesMainFee = getProServicesMaintenanceFee(qt, profServicesMainFeeEntry);
        insert proServicesMainFee;
        
        Payment__c payForProMainFee = getPaymentForFee(proServicesMainFee);
        insert payForProMainFee;
    }

    @isTest
    public static void changeBillingsSettingsOfOpp() {
        Opportunity lOpp = [ SELECT Id FROM Opportunity Limit 1];
        Id idQuote = [SELECT ID from Quote Limit 1].Id;
        Id lOperationalRtId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Deal_Desk_Operational').getRecordTypeId();
        List<QuoteLineItem> ltQuoteLine = QuoteLineItemRepository.feesPaymentsForQuote(new List<id>{idQuote});
        Case newCase = new Case();
        newCase.Description = 'isTest';
        newCase.OpportunityId__c = lOpp.Id;
        newCase.RecordTypeId = lOperationalRtId;
        newCase.QuoteId__c = idQuote;
        newCase.Justificate_No_Compliant_Topics__c = 'Justification Test';
        newCase.Subject = 'Deal Desk Operational Approval Request';
        
        populateBillingFields(newCase);
        
        Test.startTest();
            insert newCase;
        Test.stopTest();
    }

    private static void populateBillingFields(Case newCase){        
        newCase.ES_Billing_Day__c = '01';
        newCase.ES_Payment_Due_Days__c = '10 days';
        newCase.Custom_ES_Billing_Day__c =10;
        newCase.Custom_ES_Payment_Due_Days__c = 1;

        newCase.Setup_Fee_Billing_Day__c = '01';
        newCase.Setup_Fee_Payment_Due_Days__c = '10 days';
        newCase.Custom_Setup_Billing_Day__c = 1;
        newCase.Custom_Setup_Payment_Due_Day__c = 1;                          
        
        newCase.Prof_Services_One_Fee_Billing_Day__c = '01';
        newCase.Prof_Services_One_Fee_Payment_Due_Days__c = '10 days';
        newCase.Custom_Prof_Service_One_Fee_Billing_Day__c =01;
        newCase.Custom_Prof_Services_One_Fee_Payment__c = 1;        
        
        newCase.Prof_Services_Main_Fee_Billing_Day__c = '01';
        newCase.Prof_Services_Main_Fee_Payment_Due_Days__c = '10 Days';
        newCase.Custom_Prof_Service_Main_Fee_Billing_Day__c=1;
        newCase.Custom_Prof_Services_Main_Fee_Payment__c=1;

    }
    
    private static Account getAccount(){
        Account acc = new Account();
        acc.Name = 'Test Name';
        acc.Razao_Social__c = 'Test Legal Name';
        acc.BillingCity = 'Test Billig City';
        acc.BillingCountry = 'Brazil';
        acc.BillingCountryCode = 'BR';
        acc.BillingPostalCode = '00000-000';
        acc.BillingStreet = 'Test Street 999';
        acc.Id_Company__c = '26.556.823/0001-91';
        
        return acc;
    }
     
    private static Case getCancellationCase(Id idopp){
        Case cs = new Case();
        cs.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Contract_Cancellation').getRecordTypeId();
        cs.OpportunityId__c = idopp;
        return cs;
    }
    
    private static Product2 getProduct(String ProductName){
        Product2 product = new Product2();
        product.Name = ProductName;
        product.Minimum_Number_of_Employees__c = 0;
        product.Maximum_Number_of_Employees__c = 800;
        product.Family = ProductName;
        product.IsActive = true;
        return product;
    }
    
    private static PricebookEntry getPricebookEntry(Pricebook2 pb, Product2 prd, Opportunity opp){        
        PricebookEntry pbEntry = new PricebookEntry();
        pbEntry.Product2Id = prd.Id;
        pbEntry.Pricebook2Id = Test.getStandardPricebookId();
        pbEntry.UnitPrice = 200;
        pbEntry.IsActive = true;
        pbEntry.CurrencyIsoCode = opp.CurrencyIsoCode;
        return pbEntry;
    }
    
    private static Opportunity getClientSalesOpp(Account acc, Pricebook2 pb){
        Opportunity opp = new Opportunity();
        opp.AccountId = acc.Id;
        opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        opp.Name = 'Opp Test Name';
        opp.CloseDate = system.today().addDays(30);
        opp.StageName = 'Contract';
        opp.FastTrackStage__c = 'Setup';
        opp.Pricebook2Id = pb.Id;
        opp.Type = 'Expansion';  
        opp.Country_Manager_Approval__c = true;
        opp.Payment_approved__c = true;   
        opp.CurrencyIsoCode = 'BRL';
        opp.Gympass_Plus__c = 'Yes';
        opp.Standard_Payment__c = 'Yes';
        opp.Request_for_self_checkin__c = 'Yes';
        return opp;
    }
    
    private static Opportunity getNewOpp(Account acc, Pricebook2 pb){
        Opportunity opp = new Opportunity();
        opp.AccountId = acc.Id;
        opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        opp.Name = 'Opp Test Name New';
        opp.CloseDate = system.today().addDays(30);
        opp.FastTrackStage__c = 'Launched/Won';
        opp.StageName = 'Lançado/Ganho';
        opp.Pricebook2Id = pb.Id;
        opp.Type = 'Expansion';  
        opp.Country_Manager_Approval__c = true;
        opp.Payment_approved__c = true;   
        opp.CurrencyIsoCode = 'BRL';
        opp.Gympass_Plus__c = 'Yes';
        opp.Standard_Payment__c = 'Yes';
        opp.Request_for_self_checkin__c = 'Yes';
        return opp;
    }
    
      
    private static Case getDealDeskOperationalApproval(Opportunity opp){
        Case ddo = new Case();
        ddo.OpportunityId__c = opp.Id;
        ddo.Status = 'Approved';
        ddo.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Deal_Desk_Operational').getRecordTypeId(); 
        ddo.Deal_Desk_Evaluation__c = 'Approved';
        return ddo;
    }
    
    private static Quote getQuote(Opportunity opp){
        Quote qt = new Quote();
        qt.Name = 'Test';
        qt.OpportunityId = opp.Id;
        qt.License_Fee_Waiver__c = 'Yes';
        qt.Waiver_Termination_Date__c = system.today().addDays(10);
        return qt;
    }
    
    private static QuoteLineItem getAccessFee(Quote qt, PricebookEntry pbEntry){
        QuoteLineItem accessFee = new QuoteLineItem();
        accessFee.QuoteId = qt.Id;
        accessFee.PricebookEntryId = pbEntry.Id;
        accessFee.Description = '';
        accessFee.Quantity = 100;
        accessFee.UnitPrice = pbEntry.UnitPrice;  
        
        return accessFee;
    }
    
    private static QuoteLineItem getSetupFee(Quote qt, PricebookEntry pbEntry){
        QuoteLineItem SetupFee = new QuoteLineItem();
        SetupFee.QuoteId = qt.Id;
        SetupFee.PricebookEntryId = pbEntry.Id;
        SetupFee.Description = '';
        SetupFee.Quantity = 100;
        SetupFee.UnitPrice = pbEntry.UnitPrice; 
        
        return SetupFee;
    }
    
    private static Product2 getProfServicesOneFeeProduct(){
        Product2 product = new Product2();
        product.Name = 'Professional Services Setup Fee';
        product.Minimum_Number_of_Employees__c = 0;
        product.Maximum_Number_of_Employees__c = 900;
        product.Family = 'Professional Services';
        product.IsActive = true;
        product.Family_Member_Included__c = true;
        return product;
    }

    private static Product2 getProfServicesMainFeeProduct(){
        Product2 product = new Product2();
        product.Name = 'Professional Services Maintenance Fee';
        product.Minimum_Number_of_Employees__c = 0;
        product.Maximum_Number_of_Employees__c = 900;
        product.Family = 'Professional Services';
        product.IsActive = true;
        product.Family_Member_Included__c = true;
        return product;
    }
    
    private static QuoteLineItem getProServicesOneFee(Quote qt, PricebookEntry pbEntry){
        QuoteLineItem proServicesOneFee = new QuoteLineItem();
        proServicesOneFee.QuoteId = qt.Id;
        proServicesOneFee.PricebookEntryId = pbEntry.Id;
        proServicesOneFee.Description = '';
        proServicesOneFee.Quantity = 100;
        proServicesOneFee.UnitPrice = pbEntry.UnitPrice;
        
        return proServicesOneFee;
    }
    
    private static QuoteLineItem getProServicesMaintenanceFee(Quote qt, PricebookEntry pbEntry){
        QuoteLineItem proServicesMaintenanceFee = new QuoteLineItem();
        proServicesMaintenanceFee.QuoteId = qt.Id;
        proServicesMaintenanceFee.PricebookEntryId = pbEntry.Id;
        proServicesMaintenanceFee.Description = '';
        proServicesMaintenanceFee.Quantity = 100;
        proServicesMaintenanceFee.UnitPrice = pbEntry.UnitPrice;        
        return proServicesMaintenanceFee;
    }
   
    
    private static Payment__c getPaymentForFee(QuoteLineItem qli){
        Payment__c afp = new Payment__c();
        afp.RecordTypeId = Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Payment').getRecordTypeId();
        afp.Quote_Line_Item__c = qli.Id;
        afp.Percentage__c = 100;
        afp.Payment_Method__c = 'Wire Transfer';
        afp.PO_Required__c = 'Yes';
        afp.Frequency__c = 'Monthly';
        afp.Cutoff_Day__c = 1;   
        return afp;
    }
    
    private static Eligibility__c getEligibilityForPayment(Payment__c payment){
        Eligibility__c eli = new Eligibility__c();
        eli.Name = 'Headquarters';
        eli.Communication_Restriction__c = '3. Accept communications with employees after sign up';
        eli.Is_Default__c = true;
        eli.Group_Name__c = 'Main';
        eli.Launch_Date__c = system.today().addDays(10);
        return eli;
    }

}