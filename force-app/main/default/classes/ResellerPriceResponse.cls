public class ResellerPriceResponse {

    @AuraEnabled public Decimal totalPrice;
    @AuraEnabled public Decimal pricePerElegible;
    
    public ResellerPriceResponse (){}
    
    public ResellerPriceResponse (Decimal totalPrice, Decimal pricePerElegible){
        this.totalPrice = totalPrice;
        this.pricePerElegible = pricePerElegible;
    }
}