@IsTest
public class SelfCheckoutRouteTest {
  @TestSetup
  public static void makeData() {
    Account gympassEntity = AccountMock.getGympassEntity();
    gympassEntity.CurrencyIsoCode = 'BRL';
    gympassEntity.UUID__c = SelfCheckoutRequestMock.GYMPASS_ENTITY_UUID;
    insert gympassEntity;

    Pricebook2 pricebook = new Pricebook2(
      CurrencyIsoCode = 'BRL',
      Id = Test.getStandardPricebookId(),
      IsActive = true
    );

    update pricebook;

    List<Product2> products = new List<Product2>();

    Product2 setupFee = ProductMock.getSetupFee();
    setupFee.Minimum_Number_of_Employees__c = 0;
    setupFee.Maximum_Number_of_Employees__c = 10000;
    setupFee.IsActive = true;
    setupFee.Family = 'Enterprise Subscription';
    setupFee.Copay2__c = false;
    setupFee.Family_Member_Included__c = false;
    products.add(setupFee);

    Product2 accessFee = ProductMock.getAccessFee();
    accessFee.Minimum_Number_of_Employees__c = 0;
    accessFee.Maximum_Number_of_Employees__c = 10000;
    accessFee.IsActive = true;
    accessFee.Family = 'Enterprise Subscription';
    accessFee.Copay2__c = false;
    accessFee.Family_Member_Included__c = false;
    products.add(accessFee);

    insert products;

    List<PricebookEntry> pricebookEntries = new List<PricebookEntry>();

    PricebookEntry setupFeePricebookEntry = PricebookMock.getPricebookEntry(
      pricebook,
      setupFee
    );
    setupFeePricebookEntry.UnitPrice = 10;
    pricebookEntries.add(setupFeePricebookEntry);

    PricebookEntry accessFeePricebookEntry = PricebookMock.getPricebookEntry(
      pricebook,
      accessFee
    );
    accessFeePricebookEntry.UnitPrice = 10;
    pricebookEntries.add(accessFeePricebookEntry);

    insert pricebookEntries;
  }

  @IsTest
  public static void execute() {
    SelfCheckoutRequest selfCheckoutRequest = SelfCheckoutRequestMock.getMock();

    RestRequest request = new RestRequest();
    request.requestURI = '/services/apexrest/self-checkout';
    request.httpMethod = 'POST';
    request.requestBody = Blob.valueOf(JSON.serialize(selfCheckoutRequest));

    User integrationSMBJamorUser = [
      SELECT Id
      FROM User
      WHERE Name = 'Integration SMB Jamor'
    ];

    Test.startTest();

    RestContext.response = new RestResponse();

    RestContext.request = request;
    SelfCheckoutRoute.SelfCheckoutResponse response;

    System.runAs(integrationSMBJamorUser) {
      response = SelfCheckoutRoute.post();
    }

    Test.stopTest();

    System.assertEquals(
      204,
      response.getStatusCode(),
      'Error Message: ' + response.getErrorMessage()
    );
  }
}