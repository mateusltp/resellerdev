/**
* @author vinicius.ferraz
*/
public without sharing class QuoteLineItemTriggerHandler extends triggerhandler {
    
    public override void beforeUpdate() {
         //Temporary
         //QuoteLineItemTriggerEsFeeType updateEsFeeType = new QuoteLineItemTriggerEsFeeType((List<QuoteLineItem>) trigger.new, (Map<Id,QuoteLineItem>) trigger.oldMap);
         //updateEsFeeType.run();
         new OfferChangesForApprovalHandler().itemHandler( (List<QuoteLineItem>) trigger.new, (Map<Id,QuoteLineItem>) trigger.oldMap);
         new ResellerSKUDealDeskHandler().checkDealDeskFieldsApproved( (Map<Id,QuoteLineItem>) trigger.oldMap , (List<QuoteLineItem>) trigger.new ); 
 
     }
     
     public override void beforeDelete() {
         new SKUQuoteLineItemHandler().preventDefaultQuoteItemRemoval((List<QuoteLineItem>) trigger.old);
     }
     
     public override void afterUpdate() {
         new SmbSkuPlanBuilderQuoteDiscountUpdate().itemHandler( (List<QuoteLineItem>) trigger.new);
         new SkuPlanBuilderQuoteDiscountUpdate().itemHandler( (List<QuoteLineItem>) trigger.new);
         //new SMBRevenueMetricsUpdate().calculateRevenueMetrics((List<QuoteLineItem>) trigger.new);
     }
     
     public override void beforeInsert() {
         new OfferChangesForApprovalHandler().populateCustomTotalPrice( (List<QuoteLineItem>) trigger.new);
         //new SKUQuoteLineItemHandler().populateTranslationFields( (List<QuoteLineItem>) trigger.new);
     }
     
     public override void afterInsert() {
         new SmbSkuPlanBuilderQuoteDiscountUpdate().itemHandler( (List<QuoteLineItem>) trigger.new);
         new SkuPlanBuilderQuoteDiscountUpdate().itemHandler( (List<QuoteLineItem>) trigger.new);
         //new SMBRevenueMetricsUpdate().calculateRevenueMetrics((List<QuoteLineItem>) trigger.new);
     }
     
     public override void afterDelete() {
         //new SMBRevenueMetricsUpdate().calculateRevenueMetrics((List<QuoteLineItem>) trigger.old);
     }
     
 }