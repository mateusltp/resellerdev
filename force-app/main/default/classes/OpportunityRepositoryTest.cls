/**
* @author vinicius.ferraz
* @description OpportunityRepository unit test
*/
@isTest(seeAllData=false)
public class OpportunityRepositoryTest {
    
    public static final String OPPORTUNITY_NAME_TEST_1 = 'Test1';

    @TestSetup
    static void createData(){        
        Account acc = generateAccount();
        insert acc;
        insert generateOpportunity(acc);

        Opportunity smbOpp = getSmbOpp(acc);
        insert smbOpp;

        Quote qt = getQuote(smbOpp);
        insert qt;
    }

    @isTest
    static void testById(){
        Opportunity opportunity = [select Id,Name from Opportunity limit 1];
        OpportunityRepository opportunities = new OpportunityRepository();
        Opportunity opp = null;
        try{
            opp = opportunities.byId(opportunity.Id);
        }catch(System.QueryException e){
            System.assert(false);
        }
        System.assertEquals(opp.Id,opportunity.Id);
    }

    @isTest
    static void testByIdSmb(){
        Opportunity opportunity = [select Id,Name from Opportunity WHERE RecordType.DeveloperName = 'SMB_New_Business' limit 1];
        Account acc = [SELECT Id FROM Account LIMIT 1];
        Quote qt = [SELECT Id FROM Quote LIMIT 1];
        OpportunityRepository opportunities = new OpportunityRepository();
        Opportunity opp = null;
        try{
            opp = opportunities.byIdSMBForAccount((String)acc.Id, (String)opportunity.Id);
            opportunities.getValuesMdt(1);
            opportunities.sync(qt.Id, opportunity.Id);
            opportunities.LockOrUnlockOpportunityById(qt.Id);
        }catch(System.QueryException e){
            System.assert(false);
        }
        System.assertEquals(opp.Id,opportunity.Id);
    }

    @isTest
    static void testAdd(){
        OpportunityRepository opportunities = new OpportunityRepository();
        Account acc = [select Id from Account limit 1];
        Opportunity opp = generateOpportunity(acc);        
        System.assertEquals(opp.Id,null);
        try{
            opportunities.add(opp);
        }catch(System.DmlException e){
            System.assert(false);
        }
        System.assertNotEquals(opp.Id,null);
    }

    @isTest
    static void testEdit(){
        OpportunityRepository opportunities = new OpportunityRepository();
        Account acc = [select Id from Account limit 1];
        Opportunity opp = generateOpportunity(acc);        
        System.assertEquals(opp.Id,null);
        
        try{
            opportunities.edit(opp);
        }catch(System.DmlException e){
            System.assert(true);
        }
        System.assertEquals(opp.Id,null);
        
        opportunities.add(opp);
        
        try{
            opportunities.edit(opp);
        }catch(System.DmlException e){
            System.assert(false);
        }
        System.assertNotEquals(opp.Id,null);
    }

    @isTest
    static void testAddOrEdit(){
        OpportunityRepository opportunities = new OpportunityRepository();
        Account acc = [select Id from Account limit 1];
        Opportunity opp = generateOpportunity(acc);        
        System.assertEquals(opp.Id,null);
        
        try{
            opportunities.addOrEdit(opp);
        }catch(System.DmlException e){
            System.assert(false);
        }
        System.assertNotEquals(opp.Id,null);
        
        try{
            opportunities.addOrEdit(opp);
        }catch(System.DmlException e){
            System.assert(false);
        }
        System.assertNotEquals(opp.Id,null);
    }

    private static Account generateAccount(){
        Id rtId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gympass_Entity').getRecordTypeId();
        Account acc = new Account();
        acc.name='AcademiaBrasilCompanyPai';
        acc.RecordTypeId = rtId;
        acc.GP_Status__c = 'Active';
        acc.billingState = 'Minas Gerais';
        acc.CAP_Value__c = 120;
        acc.BillingCity = 'CityAcademiaBrasil';
        acc.billingStreet = 'Rua academiabrasilpai';
        acc.billingCountry = 'Brazil';
        acc.Gym_Type__c = 'Studios';
        acc.Gym_Classes__c = 'Cardio';
        acc.Types_of_ownership__c = Label.franchisePicklist;
        acc.Subscription_Type__c = 'Value per class';
        acc.Subscription_Period__c = 'Monthy value';
        acc.Subscription_Type_Estimated_Price__c    = 100;
        acc.Has_market_cannibalization__c = 'No';
        acc.Exclusivity__c = 'Yes';
        acc.Exclusivity_End_Date__c = Date.today().addYears(1);
        acc.Exclusivity_Partnership__c = 'Full Exclusivity';
        acc.Exclusivity_Restrictions__c= 'No';
        acc.Website = 'testing@tesapex.com';
        acc.Gym_Email__c = 'gymemail@apex.com';
        acc.Phone = '3222123123';
        acc.Can_use_logo__c = 'Yes';
        acc.Legal_Registration__c = 12123;
        acc.Legal_Title__c = 'Title LTDA';
        acc.Gyms_Identification_Document__c = 'CNPJ'; 
        return acc;
    }

    private static Opportunity generateOpportunity(Account acc){
        Id oppRtId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        Opportunity accOpp = new Opportunity();
        accOpp.recordTypeId = oppRtId;
        accOpp.AccountId = acc.id;
        accOpp.Name = OPPORTUNITY_NAME_TEST_1; 
        accOpp.CMS_Used__c = 'Yes';     
        accOpp.Gym_agreed_to_an_API_integration__c  = 'Yes';
        accOpp.Club_Management_System__c = 'Companhia Athletica';
        accOpp.Integration_Fee_Deduction__c = 'No';
        accOpp.CloseDate = Date.today();
        accOpp.Success_Look_Like__c = 'Yes';
        accOpp.Success_Look_Like_Description__c = 'Money money';
        accOpp.StageName = 'Qualificação';
        accOpp.Type = 'Expansion';  
        accOpp.Country_Manager_Approval__c = true;
        accOpp.Payment_approved__c = true;   
        accOpp.CurrencyIsoCode = 'BRL';
        accOpp.Gympass_Plus__c = 'Yes';
        accOpp.Standard_Payment__c = 'Yes';
        accOpp.Request_for_self_checkin__c = 'Yes';  
        return accOpp;
    }

    private static Opportunity getSmbOpp(Account acc){
        Opportunity opp = new Opportunity();
        opp.AccountId = acc.Id;
        opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SMB_New_Business').getRecordTypeId();
        opp.Name = 'Opp Test Name Renew';
        opp.CloseDate = system.today().addDays(30);
        opp.FastTrackStage__c = 'Launched/Won';
        opp.StageName = 'Lançado/Ganho';
        opp.Type = 'Expansion';  
        opp.Country_Manager_Approval__c = true;
        opp.Payment_approved__c = true;   
        opp.CurrencyIsoCode = 'BRL';
        opp.Gympass_Plus__c = 'Yes';
        opp.Standard_Payment__c = 'Yes';
        opp.Request_for_self_checkin__c = 'Yes';
        opp.Self_Checkout_New_Business__c = false;
        opp.Comments__c = 'Test';
        return opp;
    }

     private static Quote getQuote(Opportunity opp){
        Quote qt = new Quote();
        qt.Name = 'Test';
        qt.OpportunityId = opp.Id;
        qt.License_Fee_Waiver__c = 'Yes';
        qt.Waiver_Termination_Date__c = system.today().addDays(10);
        return qt;
    }
}