/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 04-06-2022
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/
public without sharing class ContentVersionTriggerHandler extends TriggerHandler {
    
    public override void afterInsert(){

        ContentVersionTriggerHandlerHelper.cloneContractAgreementFile(trigger.new);
       
    }

    public override void afterUpdate(){

        ContentVersionTriggerHandlerHelper.cloneContractAgreementFile(trigger.new);
       
    }

}