@IsTest
public class ResellerEngineGoNoGOTest { 
    
    @isTest public static void NoGOTest() {
        
        List<Account_Request__c> account_request_list = new List<Account_Request__c>();
        
        Account account = new Account();  
        account.Name = 'Test Account';
        account.Legal_Document_Type__c = 'CNPJ';
        account.Id_Company__c = '56947401000155';
        account.Razao_Social__c = 'TESTE';
        account.Industry = 'Airlines';             
        account.BillingCountry = 'Brazil';
        account.Type = 'Prospect';
        account.Industry = 'Government or Public Management';
        account.OwnerId = UserInfo.getUserId();
        insert account;
        
        Contact contact = new Contact();
        contact.FirstName = 'teste';
        contact.LastName = 'testeNovo';
        contact.AccountId = account.Id;
        contact.Email = 'teste@bemarius.example';
        contact.Role__c = 'FINANCE';
        insert contact;
        
        Opportunity opportunity = new Opportunity();
        Id oppRtId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        opportunity.recordTypeId = oppRtId;
        opportunity.AccountId = account.id;
        opportunity.Name = 'teste opp';      
        opportunity.CloseDate = Date.today();
        opportunity.StageName = 'Contract';
        opportunity.FastTrackStage__c = 'Contract';
        opportunity.Sales_Channel__c = 'Direct';
        opportunity.Type = 'New partnership';     
        opportunity.Probability = 20;
        //opportunity.Pricebook2Id = pricebook.Id;
        insert opportunity;
        
        Account_Request__c request = new Account_Request__c();  
        request.Name = 'teste';
        request.Unique_Identifier_Type__c = 'CNPJ';
        request.Unique_Identifier__c = '56947401000177';
        request.Website__c = 'testerequeste.com';
        request.Billing_Country__c = 'Brazil';
        request.Existing_In_SalesForce__c = true;
        request.Is_Gympass_Customer__c = true;
        request.ContactId__c = contact.Id;
        request.None_of_Above__c = false;
        request.AccountId__c = account.Id;
        
        Account_Request__c request2 = new Account_Request__c();  
        request2.Name = 'teste2';
        request2.Unique_Identifier_Type__c = 'CNPJ';
        request2.Unique_Identifier__c = '56947401000108';
        request2.Website__c = 'testerequeste.com';
        request2.Billing_Country__c = 'Brazil';
        request2.None_of_Above__c = false;
        request2.Existing_In_SalesForce__c = true;
        request2.Public_Sector__c = false;
        request2.ABM_Prospect__c = true;
        request2.ContactId__c = contact.Id;
        request2.AccountId__c = account.Id;
        
        Account_Request__c request3 = new Account_Request__c();  
        request3.Name = 'teste3';
        request3.Unique_Identifier_Type__c = 'CNPJ';
        request3.Unique_Identifier__c = '56947401000106';
        request3.Website__c = 'testerequeste3.com';
        request3.Billing_Country__c = 'Brazil';
        request3.Existing_In_SalesForce__c = true;
        request3.Direct_Channel_Open_Opp__c = true;
        request3.BID__c = 'No';
        request3.Partner_Model__c = 'Subsidy';
        request3.Current_Stage_In_Open_Opp__c = 'Qualification';
        request3.Last_Interaction_Account__c = '<=30';
        request3.ContactId__c = contact.Id;
        request3.AccountId__c = account.Id;
        
        Account_Request__c request4 = new Account_Request__c();  
        request4.Name = 'teste4';
        request4.Unique_Identifier_Type__c = 'CNPJ';
        request4.Unique_Identifier__c = '56947401000105';
        request4.Website__c = 'testerequeste4.com';
        request4.Billing_Country__c = 'Brazil';
        request4.Existing_In_SalesForce__c = true;
        request4.Queried_Opportunity__c = opportunity.Id;
        request4.Direct_Channel_Open_Opp__c = true;
        request4.BID__c = 'No';
        request4.Partner_Model__c = 'Intermediation';
        request4.Current_Stage_In_Open_Opp__c = 'Qualification';
        request4.Last_Modified_Opportunity__c = 29;
        request4.ContactId__c = contact.Id;
        request4.AccountId__c = account.Id;
        
        Account_Request__c request5 = new Account_Request__c();  
        request5.Name = 'teste5';
        request5.Unique_Identifier_Type__c = 'CNPJ';
        request5.Unique_Identifier__c = '56947401000104';
        request5.Website__c = 'testerequeste5.com';
        request5.Billing_Country__c = 'Brazil';
        request5.Engine_Status__c = 'Analyze';
        request5.ContactId__c = contact.Id;
        request5.AccountId__c = account.Id;
        
        Account_Request__c request7 = new Account_Request__c();  
        request7.Name = 'teste7';
        request7.Unique_Identifier_Type__c = 'CNPJ';
        request7.Unique_Identifier__c = '56947401000107';
        request7.Website__c = 'testerequeste2.com';
        request7.Billing_Country__c = 'Brazil';  
        request7.Existing_In_SalesForce__c = true;
        request7.Queried_Opportunity__c = opportunity.Id;
        request7.Direct_Channel_Open_Opp__c = true;
        request7.Current_Stage_In_Open_Opp__c = 'Contract';
        request7.Created_Date_Opportunity__c = 59;
        request7.Same_User__c = true;
        request7.Same_Unique_Identifier__c = true;
        request7.ContactId__c = contact.Id;
        request7.AccountId__c = account.Id;
        
        Account_Request__c request8 = new Account_Request__c();  
        request8.Name = 'teste8';
        request8.Unique_Identifier_Type__c = 'CNPJ';
        request8.Unique_Identifier__c = '56947401000107';
        request8.Website__c = 'testerequeste2.com';
        request8.Billing_Country__c = 'Brazil';  
        request8.Existing_In_SalesForce__c = true;
        request8.Queried_Opportunity__c = opportunity.Id;
        request8.Direct_Channel_Open_Opp__c = true;
        request8.Current_Stage_In_Open_Opp__c = 'Contract';
        request8.Created_Date_Opportunity__c = 59;
        request8.ContactId__c = contact.Id;
        request8.AccountId__c = account.Id;
        
        Account_Request__c request9 = new Account_Request__c();  
        request9.Name = 'teste9';
        request9.Unique_Identifier_Type__c = 'CNPJ';
        request9.Unique_Identifier__c = '56947401000100';
        request9.Website__c = 'testerequeste40.com';
        request9.Billing_Country__c = 'Brazil';
        request9.Existing_In_SalesForce__c = true;
        request9.Queried_Opportunity__c = opportunity.Id;
        request9.Direct_Channel_Open_Opp__c = true;
        request9.BID__c = 'No';
        request9.Partner_Model__c = 'Intermediation';
        request9.Current_Stage_In_Open_Opp__c = 'Validated';
        request9.Last_Modified_Opportunity__c = 19;
        request9.ContactId__c = contact.Id;
        request9.AccountId__c = account.Id;
                
        /*Account_Request__c request6 = new Account_Request__c();  
        request6.Name = 'teste6';
        request6.Unique_Identifier_Type__c = 'CNPJ';
        request6.Unique_Identifier__c = '56947401000105';
        request6.Website__c = 'testerequeste6.com';
        request6.Billing_Country__c = 'Brazil';
        request6.Engine_Status__c = 'Analyze';
        request6.Existing_In_SalesForce__c = true;
        request6.Is_Gympass_Customer__c = false;
        request6.ABM_Prospect__c = false;
        request6.Direct_Channel_Open_Opp__c = false;
        request6.Current_Stage_In_Open_Opp__c = 'Offer Sent';
        request6.BID__c = 'Yes';
        request6.Partner_Model__c = 'Subsidy';
        request6.Last_Interaction_Account__c = '>60';
        request6.ContactId__c = contact.Id;*/
        
        account_request_list.add(request);
        account_request_list.add(request2);
        account_request_list.add(request3);
        account_request_list.add(request4);
        account_request_list.add(request5);
        account_request_list.add(request7);
        account_request_list.add(request8);
        account_request_list.add(request9);
        
        insert account_request_list;
        Set<Id> requestIds = new Set<Id>{ request.Id, request2.Id, request3.Id, request4.Id, request5.Id, request7.Id, request8.Id, request9.Id};
            
            
        Test.startTest();
        ResellerEngineGoNoGO resellerEngine = new ResellerEngineGoNoGO(requestIds);
        resellerEngine.noGO();
        
        List<Account_Request__c> requests = [SELECT Id, Name, Engine_Status__c FROM Account_Request__c WHERE Id IN : requestIds];
        System.assertEquals(8, requests.size());
        Test.stopTest(); 
        
        

    }
    
    @isTest public static void SearchAccountsTest() {
        Account account = new Account();  
        account.Name = 'teste account';
        account.Legal_Document_Type__c = 'CNPJ';
        account.Id_Company__c = '56947401000144';
        account.Razao_Social__c = 'TESTE';
        account.Website = 'teste.com';				
        account.BillingCountry = 'Brazil';
        account.Type = 'Client';
        account.Industry = 'Government or Public Management';
        account.ABM_Prospect__c = true;
        account.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Reseller').getRecordTypeId();
        insert account;
        
        Account_Request__c request = new Account_Request__c();  
        request.Name = 'teste';
        request.Unique_Identifier_Type__c = 'CNPJ';
        request.Unique_Identifier__c = '56947401000144';
        request.Website__c = 'testerequeste.com';
        request.Billing_Country__c = 'Brazil';
        request.AccountId__c = account.Id;
        insert request;
        
        Set<Id> requestIds = new Set<Id>{request.Id}; 
            
        Test.startTest();
        ResellerEngineGoNoGO resellerEngine = new ResellerEngineGoNoGO(requestIds);
        resellerEngine.runSearchAccounts(false);
        Test.stopTest(); 
        
    }
    
    @isTest
    public static void SearchOpportunityTest() {
        Account account = new Account();  
        account.Name = 'teste account';
        account.Legal_Document_Type__c = 'CNPJ';
        account.Id_Company__c = '56947401000155';
        account.Razao_Social__c = 'TESTE';
        account.Website = 'teste.com';
        account.Industry = 'Airlines'; 						
        account.BillingCountry = 'Brazil';
        account.Type = 'Prospect';
        account.Industry = 'Government or Public Management';
        account.ABM_Prospect__c = true;
        account.OwnerId = UserInfo.getUserId();
        account.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Reseller').getRecordTypeId();
        insert account;
        
        //Pricebook2 pricebook = new Pricebook2(name = 'TESTE');
        
        Opportunity opportunity = new Opportunity();
        Id oppRtId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        opportunity.recordTypeId = oppRtId;
        opportunity.AccountId = account.id;
        opportunity.Name = 'teste opp';      
        opportunity.CloseDate = Date.today();
        opportunity.StageName = 'Contract';
        opportunity.FastTrackStage__c = 'Contract';
        opportunity.Sales_Channel__c = 'Direct';
        opportunity.Type = 'New partnership';     
        opportunity.Probability = 20;
        //opportunity.Pricebook2Id = pricebook.Id;
        insert opportunity;
        
        Account_Request__c request = new Account_Request__c();  
        request.Name = 'teste';
        request.Unique_Identifier_Type__c = 'CNPJ';
        request.Unique_Identifier__c = '56947401000155';
        request.Website__c = 'teste.com';
        request.Billing_Country__c = 'Brazil';
        request.AccountId__c = account.Id;
        insert request;
        
        Account_Request__c request2 = new Account_Request__c();  
        request2.Name = 'teste2';
        request2.Unique_Identifier_Type__c = 'CNPJ';
        request2.Unique_Identifier__c = '56947401000155';
        request2.Website__c = 'teste.com';
        request2.Billing_Country__c = 'Brazil';
        request2.AccountId__c = account.Id;
        request2.Queried_Opportunity__c = opportunity.Id;
        insert request2;
        
        
        Test.startTest();
        ResellerEngineGoNoGO resellerEngine = new ResellerEngineGoNoGO(new Set<Id> {request.Id, request2.Id});
        resellerEngine.runSearchOpportunity(false);
        Test.stopTest();
  
    }
    
    @isTest
    public static void setJointEffortTest() {
        
        List<Account> account_list = new List<Account>();
        
        Account account = new Account();  
        account.Name = 'teste account7';
        account.Legal_Document_Type__c = 'CNPJ';
        account.Id_Company__c = '56947401000117';
        account.Razao_Social__c = 'TESTE222';
        account.Website = 'teste222.com';				
        account.BillingCountry = 'Brazil';
        account.Type = 'Client';
        account.Industry = 'Government or Public Management';
        account.ABM_Prospect__c = true;
        account.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Reseller').getRecordTypeId();
        account_list.add(account);
        
        Account account2 = new Account();  
        account2.Name = 'teste account7';
        account2.Legal_Document_Type__c = 'CNPJ';
        account2.Id_Company__c = '56947401000117';
        account2.Razao_Social__c = 'TESTE222';
        account2.Website = 'teste222.com';				
        account2.BillingCountry = 'Brazil';
        account2.Type = 'Client';
        account2.ABM_Prospect__c = true;
        account2.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Reseller').getRecordTypeId();
        account_list.add(account2);
        
        insert account_list;
        
        List<Account_Request__c> account_request_list = new List<Account_Request__c>();
        
        
        Account_Request__c request7 = new Account_Request__c();  
        request7.Name = 'teste7';
        request7.Unique_Identifier_Type__c = 'CNPJ';
        request7.Unique_Identifier__c = '56947421000106';
        request7.Website__c = 'testerequeste7.com';
        request7.Billing_Country__c = 'Brazil';
        request7.AccountId__c = account.Id;
        account_request_list.add(request7);
        
        Account_Request__c request8 = new Account_Request__c();  
        request8.Name = 'teste8';
        request8.Unique_Identifier_Type__c = 'CNPJ';
        request8.Unique_Identifier__c = '56947421000108';
        request8.Website__c = 'testerequeste8.com';
        request8.Billing_Country__c = 'Brazil';
        request8.AccountId__c = account2.Id;
        request8.None_of_Above__c = true;
        account_request_list.add(request8); 
            
        insert account_request_list;
        
        Test.startTest();
        ResellerEngineGoNoGO resellerEngine = new ResellerEngineGoNoGO(new Set<Id> {request7.Id, request8.Id});
        resellerEngine.runSearchAccounts(true);
        resellerEngine.noGO();
        Test.stopTest(); 
    }
    
}