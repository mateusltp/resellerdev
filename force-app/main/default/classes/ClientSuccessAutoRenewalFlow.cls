/**
 * @description       : 
 * @author            : roei@gft.com
 * @group             : 
 * @last modified on  : 08-30-2021
 * @last modified by  : roei@gft.com
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   11-17-2020   roei@gft.com   Initial Version
**/
public without sharing class ClientSuccessAutoRenewalFlow {
    public class CustomException extends Exception {}
    private static String gOppRenegotiationRTDevName = 'Client_Success_Renegotiation';
    private static List< Id > gLstNoOppToCloneAccId = new List< Id >();
    private static List< Pricebook2 > gLstStandardPricebook = [select Id,Name from Pricebook2 where IsActive = true AND IsStandard = true limit 1];
    private static Id salesPoolId = [SELECT Id FROM User WHERE Username LIKE 'sales.pool@gympass.com%' Limit 1].Id;
    private static Boolean isUserRequest;
    public static String gSubtype;
    private static PricebookRepository gPbRepo;

    @InvocableMethod
    public static List< Opportunity > cloneLastClosedOpp( List< Id > aLstAccId ){
        isUserRequest = false;
        return cloneLastClosedOpp( aLstAccId, isUserRequest );
    }
    
    public static List< Opportunity > cloneLastClosedOpp( List< Id > aLstAccId, Boolean aUserRequest , String aRecordTypeDevName , String aSubtype ){
        gSubtype = aSubtype;
        gOppRenegotiationRTDevName = aRecordTypeDevName;
        return cloneLastClosedOpp( aLstAccId, aUserRequest );
    }
    
    public static List< Opportunity > cloneLastClosedOpp( List< Id > aLstAccId, Boolean userRequest ){
        isUserRequest = userRequest;
        List< Opportunity > lLstLastClosedOpp = getLastClosedOppByAcc( aLstAccId );
        List< Opportunity > lLstRenegotiationOpp = new List< Opportunity >();

        if( !gLstNoOppToCloneAccId.isEmpty() ) { lLstRenegotiationOpp = createNewRenegotiationOpp(); }

        if( lLstLastClosedOpp.isEmpty() ){ return lLstRenegotiationOpp; }

        Map< Id , Opportunity > lMapOppIdClonedOpp = cloneOfferSobjects( lLstLastClosedOpp );
        
        return !lLstRenegotiationOpp.isEmpty() ? lLstRenegotiationOpp : lMapOppIdClonedOpp.values();
    }

    private static List< Opportunity > createNewRenegotiationOpp(){
        List< Opportunity > lLstRenegotiationOpp = new List< Opportunity >();

        for( Id iAccId : gLstNoOppToCloneAccId ){
            lLstRenegotiationOpp.add(
                setDefaultValuesForRenegotiationOpp( new Opportunity( AccountId = iAccId , No_Won_Opportunity_in_SF__c = true ) )
            );
        }

        try{
            Database.insert( lLstRenegotiationOpp );
        } catch( Exception lExcep ){
            System.debug('Error Message: ' + lExcep.getMessage());
            System.debug('Error Stack Trace: ' + lExcep.getStackTraceString());
            throw new CustomException('Error creating new opportunity. Error message: ' + lExcep.getMessage());
        }

        return lLstRenegotiationOpp;
    }

    private static Map< Id , Opportunity > cloneOfferSobjects( List< Opportunity > aLstOppToClone ){
        Map< Id , Opportunity > lMapOppIdClonedOpp = cloneOpportunity( aLstOppToClone );

        cloneAccOppRelationship( lMapOppIdClonedOpp );

        Map< Id , Quote > lMapQuoteIdClonedQuote = cloneQuote( lMapOppIdClonedOpp );

        Map< Id , QuoteLineItem > lMapIdQuoteLineItemsCloned = cloneQuoteLineItems( lMapQuoteIdClonedQuote );

        Map< Id , Payment__c > lMapIdBillingSettingCloned = cloneQuoteBillingSettings( lMapIdQuoteLineItemsCloned );

        cloneEligibilities( lMapIdBillingSettingCloned );
        
        return lMapOppIdClonedOpp;
    }
    
    private static Map< Id , Opportunity > cloneOpportunity( List< Opportunity > aLstOppToClone ){
        Map< Id , Opportunity > lMapOppIdClonedOpp = new Map< Id , Opportunity >();
        Map< Id , Form__c > lMapOppIdM1Form = new Map< Id , Form__c >();               
        Id lOppId;

        for( Opportunity iOpp : aLstOppToClone ){
            lOppId = iOpp.Id;
            iOpp.Id = null;
            lMapOppIdClonedOpp.put( lOppId , iOpp.clone(false, true, false, false) );
        }

        try{
            setClientSuccessExecutiveFromLastClosedOpp(lMapOppIdClonedOpp);
            Database.insert( lMapOppIdClonedOpp.values() );
        } catch( Exception lExcep ){
            System.debug('Error Message: ' + lExcep.getMessage());
            System.debug('Error Stack Trace: ' + lExcep.getStackTraceString());
            throw new CustomException('Error while cloning Opportunity. Error message: ' + lExcep.getMessage());
        }

        return lMapOppIdClonedOpp;
    }

    private static void cloneAccOppRelationship( Map< Id , Opportunity > aMapOppIdClonedOpp ){
        Set< Id > aSetOppId = aMapOppIdClonedOpp.keySet();
        List< Account_Opportunity_Relationship__c > lLstAccOppRelationshipCloned = new List< Account_Opportunity_Relationship__c >();

        for( Account_Opportunity_Relationship__c iAccOppRelationship : 
             Database.query( getSobjectQuery( 'Account_Opportunity_Relationship__c' , 'WHERE Opportunity__c =: aSetOppId' ) ) ){ 
            iAccOppRelationship.Id = null;
            iAccOppRelationship.Opportunity__c = aMapOppIdClonedOpp.get( iAccOppRelationship.Opportunity__c ).Id;

            lLstAccOppRelationshipCloned.add( iAccOppRelationship.clone(false, true, false, false) );
        }

        try{
            Database.insert( lLstAccOppRelationshipCloned );
        } catch( Exception lExcep ){
            System.debug('Error Message: ' + lExcep.getMessage());
            System.debug('Error Stack Trace: ' + lExcep.getStackTraceString());
            throw new CustomException('Error while cloning Account Opportunity Relationship. Error message: ' + lExcep.getMessage());
        }
    }

    private static Map< Id , Quote > cloneQuote( Map< Id , Opportunity > aMapOppIdClonedOpp ){
        Map< Id , Quote > lMapQuoteIdClonedQuote = new Map< Id , Quote >();
        Set<Id> lLstOppId = aMapOppIdClonedOpp.keySet();
        Id lQuoteId;

        for( Quote iQuote : Database.query( getSobjectQuery( 'Quote' , 'WHERE OpportunityId =: lLstOppId' ) ) ){
            lQuoteId = iQuote.Id;
            iQuote.Id = null;
            iQuote.Start_Date__c = ( iQuote.End_Date__c == null ? System.today() : iQuote.End_Date__c ) + 1;
            iQuote.End_Date__c = null;
            iQuote.Free_Trial_Days__c = 0;
            iQuote.Payment_Terms_for_License_Fee__c = null;
            iQuote.Are_Setup_Fee_Included__c = null;
            iQuote.OpportunityId = aMapOppIdClonedOpp.get( iQuote.OpportunityId ).Id;
            lMapQuoteIdClonedQuote.put( lQuoteId , iQuote.clone(false, true, false, false) );
        }

        try{
            Database.insert( lMapQuoteIdClonedQuote.values() );
        } catch( Exception lExcep ){
            System.debug('Error Message: ' + lExcep.getMessage());
            System.debug('Error Stack Trace: ' + lExcep.getStackTraceString());
            throw new CustomException('Error while cloning Proposal. Error message: ' + lExcep.getMessage());
        }

        return lMapQuoteIdClonedQuote;
    }

    private static Map< Id , QuoteLineItem > cloneQuoteLineItems( Map< Id , Quote > aMapQuoteIdClonedQuote ){
        Map< Id , QuoteLineItem > lMapIdQuoteLineItemsCloned = new Map< Id , QuoteLineItem >();
        Set<Id> lLstQuoteId = aMapQuoteIdClonedQuote.keySet();
        Set<String> notAllowedFeesToClone = new Set<String>{'Setup Fee'};
        Id lQuoteLineItemId;

        for( QuoteLineItem iQuoteLineItem : Database.query( getSobjectQuery( 'QuoteLineItem' , 'WHERE QuoteId =: lLstQuoteId AND  Product2.Family NOT IN :notAllowedFeesToClone' ) ) ){
            lQuoteLineItemId = iQuoteLineItem.Id;
            iQuoteLineItem.Id = null;
            iQuoteLineItem.QuoteId = aMapQuoteIdClonedQuote.get( iQuoteLineItem.QuoteId ).Id;
            lMapIdQuoteLineItemsCloned.put( lQuoteLineItemId , iQuoteLineItem.clone(false, true, false, false) );
        }
        
        try{
            Database.insert( lMapIdQuoteLineItemsCloned.values() );
        } catch( Exception lExcep ){
            System.debug('Error Message: ' + lExcep.getMessage());
            System.debug('Error Stack Trace: ' + lExcep.getStackTraceString());
            throw new CustomException('Error while cloning Proposal Items. Error message: ' + lExcep.getMessage());
        }

        return lMapIdQuoteLineItemsCloned;
    }

    private static Map< Id , Payment__c > cloneQuoteBillingSettings( Map< Id , QuoteLineItem > aMapIdQuoteLineItemsCloned ){
        Map< Id , Payment__c > lMapIdBillingSettingCloned = new Map< Id , Payment__c >();
        Set<Id> lLstQuoteLineItemId = aMapIdQuoteLineItemsCloned.keySet();
        Id lBillingSettingId;

        for( Payment__c iBillingSetting : Database.query( getSobjectQuery( 'Payment__c' , 'WHERE Quote_Line_Item__c =: lLstQuoteLineItemId' ) ) ){
            lBillingSettingId = iBillingSetting.Id;
            
            iBillingSetting.Id = null;
            iBillingSetting.Quote_Line_Item__c = aMapIdQuoteLineItemsCloned.get( iBillingSetting.Quote_Line_Item__c ).Id;
            lMapIdBillingSettingCloned.put( lBillingSettingId , iBillingSetting.clone(false, true, false, false) );
        }

        try{
            Database.insert( lMapIdBillingSettingCloned.values() );
        } catch( Exception lExcep ){
            System.debug('Error Message: ' + lExcep.getMessage());
            System.debug('Error Stack Trace: ' + lExcep.getStackTraceString());
            throw new CustomException('Error while cloning Billing Setting from Proposal Items. Error message: ' + lExcep.getMessage());
        }

        return lMapIdBillingSettingCloned;
    }
    
    private static Map< Id , Eligibility__c > cloneEligibilities( Map< Id , Payment__c > aMapIdBillingSettingCloned ){
        Map< Id , Eligibility__c > lMapIdEligibilityCloned = new Map< Id , Eligibility__c >();
        Set<Id> lLstBillingSettingId = aMapIdBillingSettingCloned.keySet();
        Id lEligibilityId;

        for( Eligibility__c iEligibility : Database.query( getSobjectQuery( 'Eligibility__c' , 'WHERE Payment__c =: lLstBillingSettingId' ) ) ){
            lEligibilityId = iEligibility.Id;
            
            iEligibility.Id = null;
            iEligibility.Payment__c = aMapIdBillingSettingCloned.get( iEligibility.Payment__c ).Id;
            lMapIdEligibilityCloned.put( lEligibilityId , iEligibility.clone(false, true, false, false) );
        }
        
        try{
            Database.insert( lMapIdEligibilityCloned.values() );
        } catch( Exception lExcep ){
            System.debug('Error Message: ' + lExcep.getMessage());
            System.debug('Error Stack Trace: ' + lExcep.getStackTraceString());
            throw new CustomException('Error while cloning Eligibities. Error message: ' + lExcep.getMessage());
        }

        return lMapIdEligibilityCloned;
    }

    private static List< Opportunity > getLastClosedOppByAcc( List< Id > aLstAccId ){
        Set< String > lSetRTClosedOpp = new Set< String >{
            'Client_Success_Renegotiation','Client_Sales_New_Business',
            'SMB_New_Business','SMB_Success_Renegotiation'
        };
        List< Opportunity > lLstLastClosedOpp = new List< Opportunity >();
        
        for( Account iAcc : Database.query( getAccLastClosedOppQuery() ) ){
            if( iAcc.Opportunities.isEmpty() ) {
                gLstNoOppToCloneAccId.add( iAcc.Id );
                continue;
            }

            Opportunity lOpp = setDefaultValuesForRenegotiationOpp( iAcc.Opportunities[0] );
            lLstLastClosedOpp.add( lOpp );
        }

        return lLstLastClosedOpp;
    }

    private static Opportunity setDefaultValuesForRenegotiationOpp( Opportunity aOpp ){
        aOpp.Name = 'Renegotiation';
        aOpp.FastTrackStage__c = 'Offer Creation';
        aOpp.RecordTypeId = 
            Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(gOppRenegotiationRTDevName).getRecordTypeId();
        aOpp.StageName = 'Validated';
        aOpp.Sub_Type__c = isUserRequest != null && isUserRequest ?
            gSubtype : 'Contract Anniversary Renewal';
        aOpp.CloseDate = System.today();
        aOpp.OwnerId = isUserRequest ? UserInfo.getUserId() : salesPoolId;
        aOpp.Engagement_Journey_Completed__c = true;
        aOpp.Submission_Date__c = null;
        aOpp.Data_do_Lancamento__c = null;
        aOpp.Type = 'Renegotiation';
        aOpp.Specialist_Approved__c = false;
        aOpp.Pipefy_Card_ID__c	= '';


        /*if( !gLstStandardPricebook.isEmpty() ) {
            aOpp.Pricebook2Id = gLstStandardPricebook[0].Id;
        }*/
        
        return aOpp;
    }

    private static String getSobjectQuery( String sObjectName , String whereClause ){
        return ' SELECT ' + Utils.getSobjectFieldsDevNameForQuery( sObjectName ) + 
               ' FROM ' + sObjectName + ' ' +
               whereClause;
    }

    private static String getAccLastClosedOppQuery(){
        return 'SELECT Id,' + 
               ' ( SELECT ' + Utils.getSobjectFieldsDevNameForQuery('Opportunity') + 
               '   FROM Opportunities WHERE isClosed = true AND StageName = \'Lançado/Ganho\' AND Recordtype.DeveloperName =: lSetRTClosedOpp ORDER BY CreatedDate DESC LIMIT 1 ) ' +
               'FROM Account ' +
               'WHERE Id =: aLstAccId';
    }

    //M1 form contains the latest ClientExecutive assigned to opp
    private static void setClientSuccessExecutiveFromLastClosedOpp( Map< Id , Opportunity > lMapOppIdClonedOpp){
        Map<Id, Form__c> aMapOppWithM1Form = new FormRepository().getM1ForOppsAsMapOppIdToForm(new List<Id>(lMapOppIdClonedOpp.keySet()));
        for(Id iOpp : lMapOppIdClonedOpp.keySet()){
            Opportunity newOpp = lMapOppIdClonedOpp.get(iOpp);
            if(newOpp.Responsavel_Gympass_Relacionamento__c == null){
                if(!aMapOppWithM1Form.isEmpty() && aMapOppWithM1Form.get(iOpp) != null)
                    newOpp.Responsavel_Gympass_Relacionamento__c = aMapOppWithM1Form.get(iOpp).Client_Success_Executive__c;
                } 
            }
    }
}