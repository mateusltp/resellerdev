/**
 * @description       :
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             :
 * @last modified on  : 10-13-2021
 * @last modified by  : gft.samuel.silva@ext.gympass.com
 **/
public without sharing class ValidateOpportunityAccount {
  private List<Opportunity> opportunities;

  public void execute() {
    this.opportunities = getOpportunities();

    Map<Id, Account> accountMap = queryAccountMap();

    for (Opportunity opportunity : this.opportunities) {
      if (!accountMap.containsKey(opportunity.AccountId)) {
        continue;
      }

      Account account = accountMap.get(opportunity.AccountId);

      try {
        validateAccount(account);

        for (Contact contact : account.Contacts) {
          validateContact(contact);
        }
      } catch (CustomException error) {
        opportunity.addError(error.getMessage());
      }
    }
  }

  private List<Opportunity> getOpportunities() {
    List<Opportunity> opportunities = new List<Opportunity>();

    for (Opportunity opportunity : (List<Opportunity>) Trigger.new) {
      if (isToValidateOpportunity(opportunity)) {
        opportunities.add(opportunity);
      }
    }

    return opportunities;
  }

  private Boolean isToValidateOpportunity(Opportunity opportunity) {
    return opportunity.StageName.equals('Lançado/Ganho');
  }

  private Map<Id, Account> queryAccountMap() {
    Set<Id> accountsId = new Set<Id>();

    for (Opportunity opportunity : opportunities) {
      accountsId.add(opportunity.AccountId);
    }

    return new Map<Id, Account>(
      [
        SELECT
          Id,
          BillingCity,
          BillingCountry,
          BillingStreet,
          Attention__c,
          Email__c,
          Id_Company__c,
          Legal_Document_Type__c,
          (
            SELECT Id, Name, MailingCity, MailingCountry, MailingStreet, Role__c
            FROM Contacts
          )
        FROM Account
        WHERE
          Id IN :accountsId
          AND Send_To_Tagus__c = TRUE
          AND RecordType.DeveloperName = 'Empresas'
        WITH SECURITY_ENFORCED
      ]
    );
  }

  private void validateAccount(Account account) {
    if (String.isBlank(account.Email__c)) {
      throw new CustomException(
        'The account email is required to close an opportunity'
      );
    }

    if (String.isBlank(account.Id_Company__c)) {
      throw new CustomException(
        'The account legal number is required to close an opportunity'
      );
    }

    if (String.isBlank(account.Legal_Document_Type__c)) {
      throw new CustomException(
        'The account legal document type is required to close an opportunity'
      );
    }

    if (String.isBlank(account.Attention__c)) {
      throw new CustomException(
        'The account attention is required to close an opportunity'
      );
    }

    if (String.isBlank(account.BillingStreet)) {
      throw new CustomException(
        'The account street is required to close an opportunity'
      );
    }

    if (String.isBlank(account.BillingCity)) {
      throw new CustomException(
        'The account city is required to close an opportunity'
      );
    }
  }

  private void validateContact(Contact contact) {
    if (String.isBlank(contact.MailingStreet)) {
      throw new CustomException(
        'The contact ' +
        contact.Name +
        ' street is required to close an opportunity'
      );
    }

    if (String.isBlank(contact.MailingCity)) {
      throw new CustomException(
        'The contact ' +
        contact.Name +
        ' city is required to close an opportunity'
      );
    }

    if (String.isBlank(contact.Role__c)) {
      throw new CustomException(
        'The contact ' +
        contact.Name +
        ' role is required to close an opportunity'
      );
    }
  }
}