/**
 * @description       : 
 * @author            : alysson.mota@gympass.com
 * @group             : 
 * @last modified on  : 06-29-2022
 * @last modified by  : alysson.mota@gympass.com
**/
public with sharing class GymActivityToBeBookedService {

    public static void createNewToProdFromGymActivityRels(Id prodId, List<Gym_Activity_Relationship__c> gymActivityRels) {
        service().createNewToProdFromGymActivityRels(prodId, gymActivityRels);
    }

    private static IGymActivityToBeBookedService service() {
        return (IGymActivityToBeBookedService) Application.ServiceByRecordType.newInstanceByRecordType('Gym_Activity_To_Be_Booked__c');
    }

    public class GymActivityToBeBookedServiceException extends Exception {}
}