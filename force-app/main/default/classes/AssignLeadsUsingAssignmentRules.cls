/**
 * @description       : 
 * @author            : gft.jorge.stevaux@ext.gympass.com
 * @group             : 
 * @last modified on  : 05-11-2022
 * @last modified by  : gft.jorge.stevaux@ext.gympass.com
**/
public with sharing class AssignLeadsUsingAssignmentRules {
    @InvocableMethod
    public static void LeadAssign(List<Id> LeadIds)
    {
            Database.DMLOptions dmo = new Database.DMLOptions();
            dmo.assignmentRuleHeader.assignmentRuleId= label.LeadAssignmentRuleId;          
            List<Lead> LeadLst=[select id from lead where lead.id in :LeadIds];
            for (Lead iLead : LeadLst) {
                iLead.is_refer_a_new_gym_queue__c = false;
                iLead.setOptions(dmo);
            }
            update LeadLst;
   }
}