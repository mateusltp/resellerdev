/**
 * @File Name          : ProgressBarFieldsModelTest.cls
 * @Description        : 
 * @Author             : Samuel Silva - GFT (slml@gft.com)
 * @Group              : 
 * @Last Modified By   : gft.samuel.silva@ext.gympass.com
 * @Last Modified On   : 08-13-2021
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    30/04/2020   Samuel Silva - GFT (slml@gft.com)     Initial Version
**/
@isTest
public with sharing class ProgressBarFieldsModelTest {
  
    private static Id lOppRt = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Gyms_Small_Medium_Partner').getRecordTypeId();
    private static Id lOppWishRt = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Gyms_Wishlist_Partner').getRecordTypeId();
    private static Id lOppRenRt = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Partner_Small_and_Medium_Renegotiation').getRecordTypeId();
    private static Id lOppRenWishRt = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Partner_Wishlist_Renegotiation').getRecordTypeId();
    
    @TestSetup
    static void setupData(){
        Id rtId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gym_Partner').getRecordTypeId();
        Account acc = new Account();
        acc.name='AcademiaBrasilCompanyPai';
        acc.RecordTypeId = rtId;

        acc.ShippingState = 'Minas Gerais';
        acc.ShippingCountry = 'Brazil';
        acc.ShippingCity ='CITY CITY';
        acc.ShippingStreet = 'TESTING STREET NUX';
        
        acc.Gym_Type__c = 'Studios';
        acc.Types_of_ownership__c = Label.franchisePicklist;
        acc.Subscription_Type__c = 'Value per class';
        acc.Subscription_Period__c = 'Monthy value';
        acc.Subscription_Type_Estimated_Price__c = 100;
        acc.Has_market_cannibalization__c = 'No';
        acc.Exclusivity__c = 'Yes';
        acc.Exclusivity_End_Date__c = Date.today().addYears(1);
        acc.Exclusivity_Partnership__c = 'Full Exclusivity';
        acc.Exclusivity_Restrictions__c= 'No';
        acc.Website = 'testing@tesapex.com';
        acc.Gym_Email__c = 'gymemail@apex.com';
        acc.Phone = '3222123123';
        acc.Wishlist__c = false;
        acc.Tier__c = 'T4';
        INSERT acc; 


        
        Pricebook2 standardPricebook = new Pricebook2(
            Id = Test.getStandardPricebookId(),
            IsActive = true
        );
        update standardPricebook;
        Pricebook2 pb = standardPricebook;  

        Opportunity opp = generateOpportunity(acc,standardPricebook);
        insert opp;


    }

    
    @isTest static void checkFieldsInObjectAccTest() {
        Account acc = [SELECT ID,Name,Wishlist__c FROM Account WHERE Name ='AcademiaBrasilCompanyPai'];
        Test.startTest();        
            Double result = ProgressBarFieldsController.checkFieldsInObject(acc.Id);
            System.assert(result>0);
            
        Test.stopTest();               
    }


    
    @isTest static void checkFieldsInObjectOppSmallMediumTest() {    
        Opportunity opp = [SELECT ID  FROM Opportunity LIMIT 1];
        Test.startTest();                
            Double result = ProgressBarFieldsController.checkFieldsInObject(opp.Id);
            System.assert(result>0);            
        Test.stopTest();               
    }

    @isTest static void checkFieldsInObjectOppSmallMediumRenegotiationTest() {     
        Test.startTest();        
            Opportunity opp = [SELECT ID  FROM Opportunity LIMIT 1];
            opp.RecordTypeId = lOppRenRt;
            update opp;
            Double result = ProgressBarFieldsController.checkFieldsInObject(opp.Id);
            System.assert(result>0);            
        Test.stopTest();               
    }

    @isTest static void checkFieldsInObjectOppWishlistTest() {
      
        Test.startTest();        
            Account acc = [SELECT ID,Name,Wishlist__c FROM Account WHERE Name ='AcademiaBrasilCompanyPai'];
            acc.Wishlist__c = true;
            acc.Tier__c = 'T1';
            update acc;
            Opportunity opp = [SELECT ID  FROM Opportunity LIMIT 1];
            opp.RecordTypeId = lOppWishRt;
            update opp;
            Double result = ProgressBarFieldsController.checkFieldsInObject(opp.Id);
            System.assert(result>0);            
        Test.stopTest();               
    }

    @isTest static void checkFieldsInObjectOppWishlistRenegotiationTest() {        
        Test.startTest();   
            Account acc = [SELECT ID,Name,Wishlist__c FROM Account WHERE Name ='AcademiaBrasilCompanyPai'];
            acc.Wishlist__c = true;
            acc.Tier__c = 'T1';
            update acc;
            Opportunity opp = [SELECT ID  FROM Opportunity LIMIT 1];
            opp.RecordTypeId = lOppRenWishRt;
            update opp;     
            Double result = ProgressBarFieldsController.checkFieldsInObject(opp.Id);
            System.assert(result>0);            
        Test.stopTest();               
    }

    private static Opportunity generateOpportunity(Account lAcc, Pricebook2 lPricebook){
        Opportunity lAccOpp = new Opportunity();
         lAccOpp.CurrencyIsoCode='BRL';
         lAccOpp.recordTypeId = lOppRt;
         lAccOpp.AccountId = lAcc.id;
         lAccOpp.Sub_Type__c = 'Retention';
         lAccOpp.Name = lAcc.Id; 
         lAccOpp.CMS_Used__c = 'Yes';     
         lAccOpp.Gym_agreed_to_an_API_integration__c  = 'Yes';
         lAccOpp.Club_Management_System__c = 'Companhia Athletica';
         lAccOpp.Integration_Fee_Deduction__c = 'No';
         lAccOpp.CloseDate = Date.today();
         lAccOpp.Success_Look_Like__c = 'Yes';
         lAccOpp.Success_Look_Like_Description__c = 'Money money';
        //  lAccOpp.Cancellation_Reason__c = 'Fraud';
        //  lAccOpp.Cancellation_Reason_subcategory__c = 'Visit Manipulation';
         lAccOpp.StageName = 'Qualificação';
         lAccOpp.Type = 'Expansion';  
         lAccOpp.Country_Manager_Approval__c = true;
         lAccOpp.Payment_approved__c = true;   
         lAccOpp.CurrencyIsoCode = 'BRL';
         lAccOpp.Gympass_Plus__c = 'Yes';
         lAccOpp.Standard_Payment__c = 'Yes';
         lAccOpp.Request_for_self_checkin__c = 'Yes';  
         lAccOpp.Pricebook2Id = lPricebook.Id;
         lAccOpp.Training_Comments__c = 'teste';
         lAccOpp.Should_OPS_team_send_the_training__c = 'Standard Training';
         return lAccOpp;
     }
     
}