public without sharing class ParseEnterpriseDealsToClientDealsTA {
  public void execute() {
    List<Opportunity> opportunities = new List<Opportunity>();

    for (Opportunity newOpportunity : (List<Opportunity>) Trigger.new) {
      if (isToParseOpportunity(newOpportunity)) {
        opportunities.add(newOpportunity);
      }
    }

    if (opportunities.isEmpty()) {
      return;
    }

    List<EnterpriseDealBO> enterpriseDeals = EnterpriseDealDAO.getEnterpriseDealsByOpportunities(
      opportunities
    );

    List<ClientDealBO> clientDeals = new List<ClientDealBO>();

    for (EnterpriseDealBO enterpriseDeal : enterpriseDeals) {
      ClientDealBO clientDeal = new ParseEnterpriseDealToClientDealService(
          enterpriseDeal
        )
        .execute();

      clientDeals.add(clientDeal);
    }

    Savepoint savepoint = Database.setSavepoint();

    try {
      ClientDealDAO.upsertClientDeals(clientDeals);
    } catch (DmlException error) {
      for (Opportunity opportunity : opportunities) {
        opportunity.addError(error.getDmlMessage(0));
      }

      Database.rollback(savepoint);
    }
  }

  private Boolean isToParseOpportunity(Opportunity newOpportunity) {
    Opportunity oldOpportunity = (Opportunity) Trigger.oldMap
      .get(newOpportunity.Id);

    return Trigger.isUpdate &&
      newOpportunity.Send_To_Tagus__c &&
      oldOpportunity.StageName != newOpportunity.StageName &&
      ((newOpportunity.StageName.equals('Perdido') &&
      newOpportunity.Sub_Type__c == 'Retention') ||
      newOpportunity.StageName.equals('Lançado/Ganho')) &&
      (newOpportunity.RecordTypeId ==
      Schema.SObjectType.Opportunity
        .getRecordTypeInfosByDeveloperName()
        .get('Client_Success_Renegotiation')
        .getRecordTypeId() ||
      newOpportunity.RecordTypeId ==
      Schema.SObjectType.Opportunity
        .getRecordTypeInfosByDeveloperName()
        .get('SMB_New_Business')
        .getRecordTypeId() ||
      newOpportunity.RecordTypeId ==
      Schema.SObjectType.Opportunity
        .getRecordTypeInfosByDeveloperName()
        .get('SMB_Success_Renegotiation')
        .getRecordTypeId() ||
      newOpportunity.RecordTypeId ==
      Schema.SObjectType.Opportunity
        .getRecordTypeInfosByDeveloperName()
        .get('Client_Sales_New_Business')
        .getRecordTypeId());
  }
}