@isTest
public class SendCustomNotificationTest {
	 @isTest
    public static void massTest() {

        Massive_Account_Request__c massReq = new Massive_Account_Request__c(
            name='test_loadMassiveAccountRequest',
            Status__c='Draft',
            OwnerId = UserInfo.getUserId()
            
        );
        insert massReq;
        massReq.Status__c = 'Finished';
        
        update massReq;

        new SendCustomNotification().sendNotificationError(massReq);

        
    }

    @isTest
    public static void reqTest() {

        Account clientAccount = ResellerDataFactory.createClientAccountBrazil();
        insert clientAccount;

        Account_Request__c requestBrazil = ResellerDataFactory.createGenericAccountRequestBrazil(clientAccount);
        insert requestBrazil;

        new SendCustomNotification().sendNotificationAccountExecutive(requestBrazil, 'Account_Request_Go_Direct_Channel', UserInfo.getUserId());

        
    }
}