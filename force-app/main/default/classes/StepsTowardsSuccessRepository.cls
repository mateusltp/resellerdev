/**
 * @description       : 
 * @author            : AlyssonMota
 * @group             : 
 * @last modified on  : 11-12-2020
 * @last modified by  : Alysson Mota
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   11-11-2020   Alysson Mota   Initial Version
**/
public with sharing class StepsTowardsSuccessRepository {
    public StepsTowardsSuccessRepository() {

    }
    
    public Map<Id, List<Step_Towards_Success1__c>> getStepsTowardsSuccessFromOppsAsMap(List<Id> oppIdList) {
        Map<Id, List<Step_Towards_Success1__c>> oppIdToStepsList = new Map<Id, List<Step_Towards_Success1__c>>();
        
        List<Step_Towards_Success1__c> stepList = getStepsByOppIdList(oppIdList);
        System.debug(stepList);

        for (Step_Towards_Success1__c step : stepList) {
            if (oppIdToStepsList.containsKey(step.Related_Opportunity__c)) {
                oppIdToStepsList.get(step.Related_Opportunity__c).add(step);
            } else {
                oppIdToStepsList.put(step.Related_Opportunity__c, new List<Step_Towards_Success1__c>{step});
            }                                             
        }

        return oppIdToStepsList;
    }

    public List<Step_Towards_Success1__c> getStepsByOppIdList(List<Id> oppIdList) {
        Id m0recordTypeId = Schema.SObjectType.Step_Towards_Success1__c.getRecordTypeInfosByDeveloperName()
            .get('Opportunity_Step').getRecordTypeId();

        return [
            SELECT  Id, Achieved__c, Contact__c, Date_Achieved__c, Gympass_Event__c, Master_Account__c, Name,
                    RecordTypeId, Related_Account__c, Related_Form__c, Related_Opportunity__c, Step_Number__c
            FROM Step_Towards_Success1__c
            WHERE   Related_Opportunity__c IN :oppIdList
            AND     RecordTypeId = :m0recordTypeId
            ORDER BY Step_Number__c ASC
        ];        
    }
}