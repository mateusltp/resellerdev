/**
 * @author vncferraz
 * 
 * Provide unified builder for OfferWrapper on OfferQueueProcessor 
 */
public virtual class OfferWrapperBuilder extends BaseOfferWrapperBuilder{
    
    protected ContactRepository contacts;
    protected PricebookRepository pricebooks;    
        
    protected Id pricebookId;
    protected Id enterpriseSubscriptionPbEntryId;
    protected String currencyIsoCode;    
    protected Id primaryContactId;
    protected Id opportunityRecordTypeId;
    protected String opportunitySubType;
    protected String familyMemberPaymentMethod;
    protected String contactPermission;
    protected Decimal adjustedProbability;
    protected String fastTrackStage;
    protected String stage;

    protected Opportunity opportunity;
    protected Quote proposal;
    protected QuoteLineItem enterpriseSubscription;
    protected Payment__c billingSettings;
    protected Eligibility__c eligibility;
    protected List<Waiver__c> waivers;

    protected Account reseller;

    override
    public virtual void init(){
        super.init();
        this.wrapper = new OfferWrapper();         
        this.pricebooks = new SMBPricebookRepository();
        this.contacts = new ContactRepository();
        this.pricebookId = pricebooks.standard().Id;
    }   

    override
    public BaseOfferWrapper postLoad(Offer_Queue__c offer){        
        
        if (!wrapper.hasError())
            defineCurrencyIsoCode();
        
        if (!wrapper.hasError())
            defineOpportunityTypes(offer);
        
        if (!wrapper.hasError())
            defineContact();
        
        if (!wrapper.hasError())
            defineEnterpriseSubscriptionPbEntryId(offer);
        
        if (!wrapper.hasError())
            defineAdjustedProbability(offer);

        if (!wrapper.hasError()){
            defineFamilyMemberPaymentMethod();
            defineContactPermission();
        }

        if (!wrapper.hasError() && offer.Reseller__c != null)
            defineResellerAccount(offer);


        if (!wrapper.hasError())        
            wrapper.setOpportunity( toOpportunity(offer) );
        
        if (!wrapper.hasError())        
            wrapper.setProposal( toProposal(offer) );         

        if (!wrapper.hasError())                    
            wrapper.setEnterpriseSubscription( toEnterpriseSubscription(offer) );
        
        if (!wrapper.hasError())
            wrapper.setBillingSettings( toBillingSettings(offer) );
        
        if (!wrapper.hasError())
            wrapper.setEligibility( toEligibility(offer) );

        if (!wrapper.hasError() && hasWaiver(offer))
            wrapper.setWaivers( toWaivers(offer) );
        
        if (!wrapper.hasError())        
            wrapper.setAccountInOpportunity( toAccountInOpportunity(offer) );

        
        System.debug( ' wrapper = '+ JSON.serialize(wrapper) );

        return wrapper;
    }    

    private void defineCurrencyIsoCode(){
        if ( !this.currencyByCountry.containsKey(this.account.BillingCountry) ){
            addError('Please check Account Billing Country and Currency Configuration with your Salesforce Admin ('+this.account.BillingCountry+')');
            return;
        }
        this.currencyIsoCode = this.currencyByCountry.get( this.account.BillingCountry );
    }

    private void defineOpportunityTypes(Offer_Queue__c offer){
        switch on offer.Type_of_request__c {
            when 'SMB Sales' {
                this.opportunityRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SMB_New_Business').getRecordTypeId();
                this.opportunitySubType = '';
            }	
            when 'SMB Renegotiation' {
                this.opportunityRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SMB_Success_Renegotiation').getRecordTypeId();
                this.opportunitySubType = 'Renegotiation';
            }
            when 'SMB Renewal' {
                this.opportunityRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SMB_Success_Renegotiation').getRecordTypeId();
                this.opportunitySubType = 'Contract Anniversary';
            }
            when 'SMB Retention' {
                this.opportunityRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SMB_Success_Renegotiation').getRecordTypeId();
                this.opportunitySubType = 'Retention';
            }            
            when else {	
                addError('Please check Type of Request (SMB Sales, SMB Renegotiation, SMB Renewal or SMB Retention');
            }
        }
    }

    private void defineAdjustedProbability(Offer_Queue__c offer){
        this.adjustedProbability = 10; 

        switch on offer.Stage__c {
            when 'Offer Sent' {
                this.fastTrackStage = 'Offer Sent';
                this.stage = 'Proposta Enviada';
                this.adjustedProbability = 10;
            }	
            when 'Offer Approved' {
                this.fastTrackStage = 'Offer Approved';
                this.stage = 'Proposta Aprovada';
                this.adjustedProbability = 90;
            }  
            when 'Launched/Won' {
                this.fastTrackStage = 'Launched/Won';
                this.stage = 'Lançado/Ganho';
                this.adjustedProbability = 100;
            }  
            when else {	
                addError('Please check Stage (Offer Sent, Offer Approved, Launched/Won)');
            }           
        }
        
        if ( offer.Adjusted_Probability__c <> null ){
            this.adjustedProbability = offer.Adjusted_Probability__c;
            return;
        }     
    }

    private void defineFamilyMemberPaymentMethod(){
        this.familyMemberPaymentMethod = 'Credit Card';
    }

    private void defineContactPermission(){
        this.contactPermission = 'Allowlist';
    }

    private void defineResellerAccount(Offer_Queue__c offer){
        Account acc = null;
        try{
            this.reseller = this.accounts.byId(offer.Reseller__c);        
        }catch(System.QueryException e){
            addError('Reseller Account not found for ID '+offer.Reseller__c);
            return;
        }
    }

    private void defineContact(){
        List<Contact> contacts = this.contacts.fromAccount(this.account.Id);
        if ( contacts.isEmpty() ){
            addError('Please check Primary HR Contact on Account '+this.account.Name);
        }        
        for ( Contact contact : contacts ){
            if ( this.primaryContactId == null )
                this.primaryContactId = contact.Id;
            if ( contact.Primary_HR_Contact__c )
                this.primaryContactId = contact.Id;
        }
    }

    private void defineEnterpriseSubscriptionPbEntryId(Offer_Queue__c offer){
        PricebookEntry enterpriseSubscription = null;
        try{
            enterpriseSubscription = pricebooks.accessFee(new Pricebook2(Id=this.pricebookId), offer.Number_of_Employees__c, (offer.Family_Member__c=='Yes'), false, this.currencyIsoCode );
            this.enterpriseSubscriptionPbEntryId = enterpriseSubscription.Id;
        }catch(System.QueryException e){
            addError('Please check with your Salesforce Admin the Pricebook Enterprise Subscription for '+ this.pricebookId + ' '+ offer.Number_of_Employees__c+ ' ' + this.currencyIsoCode);
        }
    }

    private Opportunity toOpportunity(Offer_Queue__c offer){   
        Opportunity opportunity = new Opportunity();    
        opportunity.AccountId = offer.Account_ID__c; 
        opportunity.Name = (String.valueOf(System.today().month()) +'/' + String.valueOf(System.today().year()))+' '+this.account.Name;
        opportunity.Id = offer.Opportunity_ID__c;
        opportunity.Pricebook2Id = this.pricebookId;
        opportunity.Type = 'New Business';
        opportunity.RecordTypeId = this.opportunityRecordTypeId;
        opportunity.CurrencyIsoCode = this.currencyIsoCode;
        opportunity.Adjusted_Probability__c = this.adjustedProbability;
        opportunity.Adjusted_Probability_SMB__c = this.adjustedProbability!=null? String.valueOf(this.adjustedProbability) : '' ;
        opportunity.Gympass_Entity__c = this.gympassEntityId;
        opportunity.Data_do_Lancamento__c = offer.Start_Date__c;
        opportunity.Comments__c = offer.Comments__c;
        opportunity.CloseDate = offer.Close_Date__c;
        opportunity.autonomous_marketplace_contract__c = 'No';
        opportunity.Does_it_have_unique_technical_request__c = 'No';
        opportunity.Quantity_Offer_Number_of_Employees__c = offer.Number_Of_Employees__c;
        opportunity.Price_index__c = offer.Price_Index__c;
        opportunity.Price_index_description__c = offer.Price_Index_Description__c;
        opportunity.OwnerId = offer.OwnerEmail__c!=null? offer.OwnerEmail__c:this.account.OwnerId;
        opportunity.StageName = this.stage;
        opportunity.FastTrackStage__c = this.fastTrackStage;
        opportunity.Eligible_List_Registration_Method__c = offer.Eligible_List_Registration_Method__c;
        if ( this.reseller != null ){
            opportunity.Reseller_del__c = this.reseller.Id;
            opportunity.Direct_Sales_Executive__c = offer.Percentage_Reseller_Sales_Executive__c;
            opportunity.B_M__c = offer.Business_Model__c;
        }
        this.opportunity = opportunity;

        return opportunity;
    }

    private Quote toProposal(Offer_Queue__c offer){        
        Quote proposal = new Quote();

        proposal.Opportunity = this.opportunity;
        proposal.Name = this.account.Name;
        proposal.autonomous_marketplace_contract__c = 'No';
        proposal.Will_this_company_have_the_Free_Product__c = offer.Free_Plan__c;
        proposal.Pricebook2Id = this.pricebookId;
        proposal.Free_Trial_Days__c = offer.Free_Trial_End_User__c;                
        proposal.BillingStreet =   this.account.BillingStreet;
        proposal.BillingCity = this.account.BillingCity;
        proposal.BillingCountry =  this.account.BillingCountry;
        proposal.BillingPostalCode = this.account.BillingPostalCode;
        proposal.BillingState = this.account.BillingState;
        proposal.CurrencyIsoCode = this.currencyIsoCode;
        proposal.RecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();           
        proposal.License_Fee_Waiver__c = offer.Waiver_Start_Date__c<>null?'Yes':'No';
        proposal.Custom_Integrations_Required__c = 'No';
        proposal.Copay__c = 'No';
        proposal.Submission_Date__c = offer.Start_Date__c;
        proposal.Are_Family_Members_Included2__c = offer.Family_Member__c;
        proposal.Family_Member_Payment_Method__c = this.familyMemberPaymentMethod;
        proposal.Start_Date__c = offer.Start_Date__c;
        proposal.End_Date__c = offer.End_Date__c;        
        proposal.Contact_Permission__c = offer.Permission_Contact__c;
        proposal.Total_Number_of_Employees__c = offer.Number_Of_Employees__c;        
        proposal.ExpirationDate = System.today();
        proposal.ContactId = this.primaryContactId;
        proposal.Payroll_Debite_Memo_Contact__c = this.primaryContactId;
        proposal.Operations__c = this.primaryContactId;
        proposal.Finance__c = this.primaryContactId;           
        proposal.Employee_Corporate_Email__c = true;
        proposal.Employee_Company_ID__c = true;
        proposal.Employee_Name__c = true;
        proposal.Expansion_100_of_the_eligible__c = (offer.Full_Launch__c=='Yes');
        proposal.New_Hires__c = true;
        proposal.HR_Communication__c = true;
        proposal.Exclusivity_clause__c = (offer.Exclusivity_clause__c=='Yes');
        proposal.PR_in_contract__c = true;
        proposal.What_does_success_looks_like_Joint_BP__c = true;
        proposal.Employee_National_ID__c = ''; 
        proposal.Will_the_MF_be_subsidized__c = 'No';                
        proposal.Employee_Registration_Method__c = 'Eligible file';
        proposal.Unique_Identifier__c = offer.Unique_Identifier__c;
        proposal.Are_Setup_Fee_Included__c = 'No';
        proposal.Name_of_the_Benefit_Admin__c = offer.Name_of_the_Benefit_Admin__c;
        proposal.Corporate_E_mail__c = offer.Corporate_Email__c;
        proposal.identification_number__c = offer.Identification_Number__c;
        this.proposal = proposal;

        return proposal;
    }

    private QuoteLineItem toEnterpriseSubscription(Offer_Queue__c offer){
        QuoteLineItem enterpriseSubscription = new QuoteLineItem();

        enterpriseSubscription.Quote = this.proposal;
        enterpriseSubscription.PricebookEntryId = this.enterpriseSubscriptionPbEntryId;
        enterpriseSubscription.Quantity = offer.Number_of_Employees__c;
        enterpriseSubscription.UnitPrice = offer.Reference_Sales_Price_per_Eligible__c;

        this.enterpriseSubscription = enterpriseSubscription;

        return enterpriseSubscription;
    }

    private Payment__c toBillingSettings(Offer_Queue__c offer){
        Payment__c billingSettings = new Payment__c();

        billingSettings.Quote_Line_Item__r = this.enterpriseSubscription;
        billingSettings.recordTypeId = Schema.SObjectType.Payment__c.getRecordTypeInfosByDeveloperName().get('Payment').getRecordTypeId();
        billingSettings.Percentage__c = 100;
        billingSettings.PO_Required__c = 'Yes';
        billingSettings.Frequency__c = offer.ES_Payment_Frequency__c;
        billingSettings.Cutoff_Day__c = 1;   
        billingSettings.Payment_Due_Days__c = offer.ES_Due_Days__c;
        billingSettings.Billing_Day__c = offer.ES_Billing_Day__c;
        billingSettings.Account__c = this.account.Id;  
        billingSettings.Payment_Method__c = offer.ES_Payment_Method__c;
        this.billingSettings = billingSettings;
        
        return billingSettings;
    }

    private Eligibility__c toEligibility(Offer_Queue__c offer){
        Eligibility__c eligibility = new Eligibility__c();
        
        eligibility.Name = 'Headquarters';
        eligibility.Communication_Restriction__c = '3. Accept communications with employees after sign up';
        eligibility.Is_Default__c = true;
        eligibility.Group_Name__c = 'Main';
        eligibility.Launch_Date__c = offer.Start_Date__c;        
        eligibility.Billing_Day__c =offer.Membership_fee_Billing_Day__c<>null?offer.Membership_fee_Billing_Day__c:'01'; 
        eligibility.Payment_Due_Days__c = offer.Membership_fee_Due_Dates__c<>null?offer.Membership_fee_Due_Dates__c:'30 days';
        eligibility.Payment_Method__c = offer.Membership_fee_Payment_Method__c;
        this.eligibility = eligibility;

        return eligibility;
    }

    private void validateWaiver(Waiver__c w){
        if ( ( w.Start_Date__c <> null && w.End_Date__c == null ) ||  
             ( w.Start_Date__c == null && w.End_Date__c <> null )  )
             addError('Please adjust Waiver dates. StartDate and EndDate must be filled for Waiver definition on Account '+this.account.Id);
    }

    private boolean hasWaiver(Offer_Queue__c offer){
        return ( (offer.Waiver_Start_Date__c <> null && offer.Waiver_End_Date__c <> null) || 
                 (offer.Temporary_Discount_1_Start_Date__c <> null && offer.Temporary_Discount_1_End_Date__c <> null) );
    }

    private List<Waiver__c> toWaivers(Offer_Queue__c offer){
        List<Waiver__c> waivers = new List<Waiver__c>();
        
        if ( offer.Waiver_Start_Date__c!=null ){
            Waiver__c waiver = new Waiver__c();
            waiver.Start_Date__c = offer.Waiver_Start_Date__c;
            waiver.End_Date__c =  offer.Waiver_End_Date__c;
            waiver.Percentage__c = offer.Waiver_Percentage__c;
            waivers.add(waiver);
        }
        
        if ( offer.Temporary_Discount_1_Start_Date__c!=null ){
            Waiver__c temporaryDiscount1 = new Waiver__c();
            temporaryDiscount1.Start_Date__c = offer.Temporary_Discount_1_Start_Date__c;
            temporaryDiscount1.End_Date__c =  offer.Temporary_Discount_1_End_Date__c;
            temporaryDiscount1.Percentage__c = offer.Temporary_Discount_1_Percentage__c;
            waivers.add(temporaryDiscount1);
        }
        
        if ( offer.Temporary_Discount_2_Start_Date__c!=null ){
            Waiver__c temporaryDiscount2 = new Waiver__c();
            temporaryDiscount2.Start_Date__c = offer.Temporary_Discount_2_Start_Date__c;
            temporaryDiscount2.End_Date__c =  offer.Temporary_Discount_2_End_Date__c;
            temporaryDiscount2.Percentage__c = offer.Temporary_Discount_2_Percentage__c;
            waivers.add(temporaryDiscount2);
        }
        
        
        if ( offer.Temporary_Discount_3_Start_Date__c!=null ){
            Waiver__c temporaryDiscount3 = new Waiver__c();
            temporaryDiscount3.Start_Date__c = offer.Temporary_Discount_3_Start_Date__c;
            temporaryDiscount3.End_Date__c =  offer.Temporary_Discount_3_End_Date__c;
            temporaryDiscount3.Percentage__c = offer.Temporary_Discount_3_Percentage__c;
            waivers.add(temporaryDiscount3);
        }

        if ( offer.Temporary_Discount_4_Start_Date__c!=null ){
            Waiver__c temporaryDiscount4 = new Waiver__c();
            temporaryDiscount4.Start_Date__c = offer.Temporary_Discount_4_Start_Date__c;
            temporaryDiscount4.End_Date__c =  offer.Temporary_Discount_4_End_Date__c;
            temporaryDiscount4.Percentage__c = offer.Temporary_Discount_4_Percentage__c;
            waivers.add(temporaryDiscount4);
        }    

        this.waivers = waivers;

        for(Waiver__c w : this.waivers){
            validateWaiver(w);
        }

        return waivers;
    }

    protected virtual Account_Opportunity_Relationship__c toAccountInOpportunity(Offer_Queue__c offer){
        Account_Opportunity_Relationship__c accountInOpportunity = new Account_Opportunity_Relationship__c();
        accountInOpportunity.Account__c = offer.Account_ID__c;
        accountInOpportunity.Billing_Percentage__c = offer.ES_Billing_Percentage__c;
        accountInOpportunity.CurrencyIsoCode = this.opportunity.CurrencyIsoCode;
        accountInOpportunity.Gympass_Entity__c = this.gympassEntityId;
        accountInOpportunity.Main_Order__c = true;
        accountInOpportunity.Opportunity__c = this.opportunity.Id;
        accountInOpportunity.OwnerId = this.opportunity.OwnerId;    

        return accountInOpportunity;
    }
}