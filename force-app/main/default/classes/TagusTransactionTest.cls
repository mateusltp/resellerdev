@IsTest
public class TagusTransactionTest {
  @IsTest
  public static void execute() {
    Test.setMock(HttpCalloutMock.class, new HttpResponseMock());

    EventQueue event = new EventBuilder()
      .createEventFor('EVENT_MOCK')
      .withSender('SALESFORCE')
      .withReceiver('TAGUS')
      .buildEvent();

    Test.startTest();

    event.save();

    Test.stopTest();

    TagusTransaction transactionInfo = new TagusTransaction(
      event,
      TagusTransaction.TransactionType.GET
    );

    System.assert(transactionInfo.getId() != null, 'Id not assigned');

    System.assertEquals(
      'EVENT_MOCK',
      transactionInfo.getName(),
      'Wrong name assigned'
    );

    System.assertEquals(
      event.getEventId(),
      transactionInfo.getSalesforceId(),
      'Wrong Salesforce id assigned'
    );

    System.assertEquals(
      TagusTransaction.TransactionType.GET,
      transactionInfo.getType(),
      'Wrong type assigned'
    );

    System.assertEquals(
      'SALESFORCE',
      transactionInfo.getSource(),
      'Wrong source assigned'
    );

    System.assertEquals(
      Datetime.now(),
      transactionInfo.getTimestamp(),
      'Wrong timestamp assigned'
    );
  }

  private class HttpResponseMock implements HttpCalloutMock {
    public HTTPResponse respond(HTTPRequest req) {
      HttpResponse res = new HttpResponse();
      res.setHeader('Content-Type', 'application/json');
      res.setStatusCode(200);
      return res;
    }
  }
}