public without sharing class SelfCheckoutController {
  private RestRequest request;
  private RestResponse response;

  private List<BadRequest> badRequests;

  public SelfCheckoutController() {
    this.request = RestContext.request;
    this.response = RestContext.response;
    this.badRequests = new List<BadRequest>();
  }

  public RestResponse post() {
    Savepoint savepoint = Database.setSavepoint();

    try {
      SelfCheckoutRequest selfCheckoutRequest = getRequest();
      System.debug('selfCheckoutRequest: ' + selfCheckoutRequest);

      new SelfCheckoutRequestValidator(selfCheckoutRequest).validatePostRequest();

      new SelfCheckoutPublisher().publish(selfCheckoutRequest);
      
      response.statusCode = 204;
    } catch(JSONException error) {
      BadRequest badRequest = new BadRequest();
      badRequest.status_code = 'JSON_EXCEPTION';

      badRequest.messages.add('The payload is in the wrong format');

      badRequests.add(badRequest);
    } catch(DmlException error) {
      BadRequest badRequest = new BadRequest();
      badRequest.status_code = 'DML_EXCEPTION';

      for(Integer i=0; i < error.getNumDml(); i++) {
        badRequest.messages.add(error.getDmlMessage(i));
      }

      badRequests.add(badRequest);
    } catch(IntegrationException error) {
      BadRequest badRequest = new BadRequest();
      badRequest.status_code = 'INTEGRATION_EXCEPTION';
      badRequest.messages = IntegrationException.getMessages();

      badRequests.add(badRequest);
    }

    if (!badRequests.isEmpty()) {
      response.responseBody = Blob.valueOf(JSON.serialize(badRequests));
      response.statusCode = 400;

      Database.rollback(savepoint);
    }

    return response;
  }

  private SelfCheckoutRequest getRequest() {
    String requestBodyJson = this.request?.requestBody?.toString();
    System.debug('requestBodyJson: ' + requestBodyJson);

    if (requestBodyJson == null) {
      return null;
    }

    return (SelfCheckoutRequest) JSON.deserialize(requestBodyJson, SelfCheckoutRequest.class);
  }

  private class BadRequest {
    private List<String> messages;
    private String status_code;

    private BadRequest() {
      this.messages = new List<String>();
    }
  }
}