public with sharing class ResellerSKUEditController {
    
    @AuraEnabled
    public static void updateProductAndWaiversAndQuote(QuoteLineItem product, List<Waiver__c> waiversToUpsert, List<Waiver__c> waiversToDelete, Quote quote){
        try {     
            system.debug('product '+product.Quantity);
            FastTrackRegionalManagerApprovalHandler.fastTrackRegionalManagerApproval(product, quote);
            delete waiversToDelete;
            upsert waiversToUpsert;
            updateWaivers(product, quote);
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
    
    @AuraEnabled
    public static Quote getEditQuote(String opportunityId){
        try {
            system.debug('opportunityId '+opportunityId);
            QuoteRepository quoteRepository = new QuoteRepository();            
			Quote quote = quoteRepository.lastForOpportunity(opportunityId);
            return quote;
            
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
    
    public static void updateWaivers(QuoteLineItem product,Quote quote){
        try {     
            List<Waiver__c> lstWaiver = [SELECT Id FROM Waiver__c WHERE Quote_Line_Item__c =: product.id];
            if(lstWaiver.size() > 0){
                quote.Waiver_approved__c = false;
                if(quote.Enterprise_Discount_Approval_Needed__c){
                    quote.Commercial_Approved_Conditions__c = '[{"isApproved":false,"approvalCondition":"Regional Manager"},{"isApproved":false,"approvalCondition":"Waiver"}]';
                }else{
                    quote.Commercial_Approved_Conditions__c = '[{"isApproved":false,"approvalCondition":"Waiver"}]';
                }
            }else{
                quote.Waiver_approved__c = true;
                quote.Commercial_Approved_Conditions__c = quote.Enterprise_Discount_Approval_Needed__c ? '[{"isApproved":false,"approvalCondition":"Regional Manager"}]' : '' ;
            }
            update quote;     
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
}