/**
 * @File Name          : LeadWithParentAccountTest.cls
 * @Description        : 
 * @Author             : GEPI@GFT.com
 * @Group              : 
 * @Last Modified By   : GEPI@GFT.com
 * @Last Modified On   : 07-24-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    08/06/2020   Samuel Silva - GFT (slml@gft.com)     Initial Version
**/
@isTest
public with sharing class LeadWithParentAccountTest {
    

    @TestSetup
    static void makeData(){
        Id recordTypeIdAccount =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gym_Partner').getRecordTypeId();
        
        Account parentAccount = new Account(
            Name = 'ParentAccountforTest',
            RecordTypeId = recordTypeIdAccount,
            Types_of_Ownership__c = 'Franchise',
            Gym_Type__c = 'Full Service',
            Gym_Email__c = 'ParentAccountforTest@testando.com',
            ShippingStreet = 'Rua ParentAccountforTest 23',
            ShippingCity = 'City ParentAccountforTest',
            ShippingCountry = 'Brazil',
            website = 'ParentAccountforTest.com.br'
        );

        INSERT parentAccount;
    }
    
    @isTest static void findPotentialParentAccountTest() {
        Account acc = [SELECT Name FROM Account WHERE Name = 'ParentAccountforTest'];
        Test.startTest(); 
            List<Account> accLst = LeadWithParentAccountController.findPotentialParentAccount('ParentAccountfor', 'rua novotest');   
            System.assertEquals(acc.Name, accLst.get(0).Name);
            accLst.clear();
        Test.stopTest();               
    }

    @isTest static void findPotentialParentAccountWithWebsiteTest() {
        Account acc = [SELECT Name FROM Account WHERE Name = 'ParentAccountforTest'];
        Test.startTest(); 
            List<Account> accLst = LeadWithParentAccountController.findPotentialParentAccount('ParentAccountfor', 'rua novotest', 'ParentAccountforTest.com.br');   
            System.assertEquals(acc.Name, accLst.get(0).Name);
        Test.stopTest();               
    }
}