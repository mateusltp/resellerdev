/**
 * @author vinicius.ferraz
 */
public with sharing class EligibilityTriggerHandler extends triggerhandler {

    public override void beforeUpdate() {
        new OfferChangesForApprovalHandler().eligibilityHandler( (List<Eligibility__c>) trigger.new, (Map<Id,Eligibility__c>) trigger.oldMap);
        new ResellerSKUDealDeskHandler().checkDealDeskFieldsApproved( (Map<Id,Eligibility__c>) trigger.oldMap , (List<Eligibility__c>) trigger.new ); 

    }
    
    public override void beforeDelete() {
        //new EligibilityDeletionHandler().evaluateIfIsAllowedToDelete((List<Eligibility__c>) trigger.old);
    }
    
    public override void afterUpdate() {
        //new SKUDealDeskHandler().checkDealDeskFieldsApproved( (Map<Id,Eligibility__c>) trigger.oldMap , (List<Eligibility__c>) trigger.new );
    }

    public override void beforeInsert() {
        
    }

    public override void afterInsert() {
    }

    public override void afterDelete() {
        new EligibilityDeletionHandler().evaluateOppEnablersForDeletedRecords((List<Eligibility__c>) trigger.old);
    }
}