/**
 * @description       : 
 * @author            : gft.jorge.stevaux@ext.gympass.com
 * @group             : 
 * @last modified on  : 08-12-2021
 * @last modified by  : gft.jorge.stevaux@ext.gympass.com
**/
@isTest(seeAllData=false)
private class GymActivitySelectorTest {
    
    @TestSetup
    static void makeData(){

        Account lAcc = PartnerDataFactory.newAccount();              
        Database.insert( lAcc );

        Contact lContact = PartnerDataFactory.newContact(lAcc.Id);
        Database.insert(lContact);

        Bank_Account__c lBankAcct = PartnerDataFactory.newBankAcct();
        Database.insert(lBankAcct);

        Acount_Bank_Account_Relationship__c lBankAcctRel = PartnerDataFactory.newBankAcctRel(lAcc.Id, lBankAcct);
        Database.insert(lBankAcctRel);
                                  
        Opportunity lOpp = PartnerDataFactory.newOpportunity( lAcc.Id, 'Partner_Wishlist_Renegotiation' );     
        Database.insert( lOpp );

        Product_Item__c lProduct = PartnerDataFactory.newProduct(lOpp);
        Database.insert( lProduct );
        
        Gym_Activity__c lGymActivity = PartnerDataFactory.newGymActivity(lProduct.Id);
        Database.insert(lGymActivity);
        
    }

    @isTest 
    static void getSObjectFieldList_Test(){

        List<Schema.SObjectField> schSObjFieldLst = new List<Schema.SObjectField> {
            Gym_Activity__c.Id,
            Gym_Activity__c.Name
		};

        Test.startTest();
        System.assertEquals(schSObjFieldLst, new GymActivitySelector().getSObjectFieldList());
        Test.stopTest();
    }

    @isTest 
    static void getSObjectType_Test(){

        Test.startTest();
        System.assertEquals(Gym_Activity__c.sObjectType, new GymActivitySelector().getSObjectType());
        Test.stopTest();
    }

    @isTest 
    static void selectById_Test(){
        
        List<Gym_Activity__c> gymActLst = [SELECT Id, Name, CurrencyIsoCode
                                         FROM Gym_Activity__c LIMIT 1];

        Set<id> gymActIds = (new Map<Id, Gym_Activity__c>(gymActLst)).keySet();

        Test.startTest();
        system.assertEquals(gymActLst, new GymActivitySelector().selectById(gymActIds));
        Test.stopTest();
    }

    @isTest 
    static void selectByProductIdToClone_Test(){

        List<Product_Item__c> aProdLst = [SELECT Id, Name, CurrencyIsoCode FROM Product_Item__c LIMIT 1];
        Set<Id> aProdIds = (new Map<Id, Product_Item__c>(aProdLst)).keySet();

        List<Gym_Activity__c> gymActLst = [SELECT Id, Name, CurrencyIsoCode 
                                            FROM Gym_Activity__c 
                                            WHERE Product_Item__c IN : aProdIds LIMIT 1];

        Test.startTest();
        system.assertEquals(gymActLst.get(0).id, new GymActivitySelector().selectByProductIdToClone(aProdIds).get(0).id);
        Test.stopTest();
    }
}