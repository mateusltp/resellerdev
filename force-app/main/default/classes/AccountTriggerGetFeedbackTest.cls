/**
 * @description       : 
 * @author            : JRDL@GFT.com
 * @group             : 
 * @last modified on  : 
 * @last modified by  : 
 * Modifications Log 
 * Ver   Date         Author                              Modification
 * 1.0   09-06-2021   jrdl@gft.com   Initial Version
**/

@isTest(seeAllData=false)
public class AccountTriggerGetFeedbackTest {

    @TestSetup
    static void setup(){
        
        Account acc = generateAccount('Acc Test');
        INSERT acc;
        
        Contact ctt = generateContact(acc);
        INSERT ctt;

    }
    
    @isTest
    static void testClientSalesExperience(){
        Account account = [select Id,Name from Account limit 1];
        account.Type = 'Client';
        
        update account;
        
        //System.assertEquals();
    }
    
    private static Account generateAccount(String name){
        Id rtId =   Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
        Account acc = new Account();
        acc.name='Test ';
        acc.RecordTypeId = rtId;
        acc.GP_Status__c = 'Active';
        acc.Type = 'Prospect';
        acc.billingState = 'São Paulo';
        acc.BillingCity = 'São Paulo';
        acc.billingStreet = 'Rua Test';
        acc.billingCountry = 'Brazil';  
        acc.NumberOfEmployees = 520;
        return acc;
    }

    private static Contact generateContact(Account acc){
        Id rtCtt = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Direct_Channel').getRecordTypeId();
        Contact accCtt = new Contact();
        accCtt.recordTypeId = rtCtt;
        accCtt.AccountId = acc.id;
        accCtt.Email = 'test@test.com';
        accCtt.FirstName = 'Contato';
        accCtt.LastName = 'Teste';
        accCtt.DecisionMaker__c = 'Yes';
        accCtt.Primary_HR_Contact__c = TRUE;
        return accCtt;
    }
}