public with sharing class ClientDealBO {
  private Order order;
  private Opportunity opportunity;
  private Quote quote;
  private List<SalesItem> salesItems;
  private List<PaymentItem> paymentItems;

  public ClientDealBO() {
    this.salesItems = new List<SalesItem>();
    this.paymentItems = new List<PaymentItem>();
  }

  public ClientDealBO(Opportunity opportunity, Quote quote) {
    this.opportunity = opportunity;
    this.quote = quote;
    this.salesItems = new List<SalesItem>();
    this.paymentItems = new List<PaymentItem>();
  }

  public Order getOrder() {
    return this.order;
  }

  public void setOrder(Order order) {
    this.order = order;
  }

  public Opportunity getOpportunity() {
    return this.opportunity;
  }

  public void setOpportunity(Opportunity opportunity) {
    this.opportunity = opportunity;
  }

  public Quote getQuote() {
    return this.quote;
  }

  public void setQuote(Quote quote) {
    this.quote = quote;
  }

  public List<SalesItem> getSalesItems() {
    return this.salesItems;
  }

  public void addSaleItem(
    QuoteLineItem quoteLineItem,
    List<Waiver__c> waivers
  ) {
    this.salesItems.add(new SalesItem(quoteLineItem, waivers));
  }

  public List<PaymentItem> getPaymentItems() {
    return this.paymentItems;
  }

  public void addPaymentItem(
    Payment__c payment,
    List<Eligibility__c> eligibilities
  ) {
    this.paymentItems.add(new PaymentItem(payment, eligibilities));
  }

  public class SalesItem {
    private QuoteLineItem quoteLineItem;
    private List<Waiver__c> waivers;

    private SalesItem(QuoteLineItem quoteLineItem, List<Waiver__c> waivers) {
      this.quoteLineItem = quoteLineItem;
      this.waivers = waivers;
    }

    public QuoteLineItem getQuoteLineItem() {
      return this.quoteLineItem;
    }

    public List<Waiver__c> getWaivers() {
      return this.waivers;
    }
  }

  public class PaymentItem {
    private Payment__c payment;
    private List<Eligibility__c> eligibilities;

    public PaymentItem(Payment__c payment, List<Eligibility__c> eligibilities) {
      this.payment = payment;
      this.eligibilities = eligibilities;
    }

    public Payment__c getPayment() {
      return this.payment;
    }

    public List<Eligibility__c> getEligibilities() {
      return this.eligibilities;
    }
  }
}