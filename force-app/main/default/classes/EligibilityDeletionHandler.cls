/**
 * @description       : 
 * @author            : AlyssonMota
 * @group             : 
 * @last modified on  : 02-27-2021
 * @last modified by  : Alysson Mota
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   02-26-2021   Alysson Mota   Initial Version
**/
public inherited sharing class EligibilityDeletionHandler {    
    public void evaluateIfIsAllowedToDelete(List<Eligibility__c> oldEligibilities) {
        EligibilityRepository eligibilityRepository = new EligibilityRepository();
        Set<Id> paymentIdSet = new Set<Id>();
        Map<Id, List<Eligibility__c>> paymentIdToEligibilityList = new Map<Id,List<Eligibility__c>>();
        List<Eligibility__c> eligibilityList = new List<Eligibility__c>();

        for (Eligibility__c eligibility : oldEligibilities) {
            paymentIdSet.add(eligibility.Payment__c);
        }

        eligibilityList = eligibilityRepository.getEligibilitiesByPaymentId(new List<Id>(paymentIdSet));

        paymentIdToEligibilityList = getPaymentIdToEligibilityList(eligibilityList);

        for (Eligibility__c eligibility : oldEligibilities) {
            List<Eligibility__c> eligibilityListFromPayment = paymentIdToEligibilityList.get(eligibility.Payment__c);

            if (eligibilityListFromPayment != null && eligibilityListFromPayment.size() == 1) {
                eligibility.addError('Elegibility Deletion: It is not allowed to remove the group when the Billing Setting has only one record');
            }
        }
    }

    public void evaluateOppEnablersForDeletedRecords(List<Eligibility__c> oldEligibilities) {
        EligibilityRepository eligibilityRepository = new EligibilityRepository();
        List<Eligibility__c> remainigEligibilities = new List<Eligibility__c>();
        Set<Id> paymentIdSet = new Set<Id>();
        Map<Id, List<Eligibility__c>> paymentIdToEligibilityList = new Map<Id, List<Eligibility__c>>();
        List<Id> eligibilityIdList = new List<Id>();

        for (Eligibility__c eligibility : oldEligibilities) {
            paymentIdSet.add(eligibility.Payment__c);
        }

        remainigEligibilities = eligibilityRepository.getEligibilitiesByPaymentId(new List<Id>(paymentIdSet));

        for (Eligibility__c eligibility : remainigEligibilities) {
            eligibilityIdList.add(eligibility.Id);
        }

        EnablersHelper.updateStepsTowardsSuccess(eligibilityIdList);
    }

    private Map<Id, List<Eligibility__c>> getPaymentIdToEligibilityList(List<Eligibility__c> eligibilityList) {
        Map<Id, List<Eligibility__c>> paymentIdToEligibilityList = new Map<Id, List<Eligibility__c>>();

        for (Eligibility__c e : eligibilityList) {
            if (paymentIdToEligibilityList.containsKey(e.Payment__c)) {
                paymentIdToEligibilityList.get(e.Payment__c).add(e);
            } else {
                paymentIdToEligibilityList.put(e.Payment__c, new List<Eligibility__c>{e});
            }
        }

        return paymentIdToEligibilityList;
    }
    
}