/**
 * @description       : 
 * @author            : bruno.mendes@gympass.com
 * @group             : 
 * @last modified on  : 27/04/2022
 * @last modified by  : bruno.mendes@gympass.com
**/

@IsTest
private with sharing class ProdOppAssignmentServiceTest {

    @IsTest
    static void insertCurrencyProdOppAssignment_Test() {
        Id prodOppAssignRecordTypeId = Schema.SObjectType.Product_Opportunity_Assignment__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product_Opportunity_Assignment').getRecordTypeId();
        fflib_ApexMocks mocks = new fflib_ApexMocks();

        Id mockAccountId = fflib_IDGenerator.generate(Account.SObjectType);
        Account mockAccount = new Account(Id = mockAccountId, CurrencyIsoCode = 'EUR');

        Id mockOppId = fflib_IDGenerator.generate(Opportunity.SObjectType);
        Id mockOppMemberId = fflib_IDGenerator.generate(Account_Opportunity_Relationship__c.SObjectType);
        Set<Id> mockOppMemberIds = new Set<Id> {mockOppMemberId};
        Account_Opportunity_Relationship__c mockOppMember = new Account_Opportunity_Relationship__c(Id = mockOppMemberId, Account__c = mockAccountId, Account__r = mockAccount, Opportunity__c = mockOppId);
        Map<Id, Account_Opportunity_Relationship__c> mockOppMembers = new Map<Id, Account_Opportunity_Relationship__c> {mockOppMemberId => mockOppMember};

        Id mockProdAssignmentId = fflib_IDGenerator.generate(Product_Assignment__c.SObjectType);

        Id mockProdOppAssignmentId = fflib_IDGenerator.generate(Product_Opportunity_Assignment__c.SObjectType);
        Product_Opportunity_Assignment__c mockProdOppAssignment = new Product_Opportunity_Assignment__c(Id = mockProdOppAssignmentId, RecordTypeId = prodOppAssignRecordTypeId, OpportunityMemberId__c = mockOppMemberId, ProductAssignmentId__c = mockProdAssignmentId, CurrencyIsoCode = 'USD');
        List<Product_Opportunity_Assignment__c> mockProdOpportunityAssignments = new List<Product_Opportunity_Assignment__c> {mockProdOppAssignment};


        AccountOpportunitySelector mockAccountOpportunitySelector = (AccountOpportunitySelector)mocks.mock(AccountOpportunitySelector.class);

        mocks.startStubbing();

        mocks.when(mockAccountOpportunitySelector.sObjectType()).thenReturn(Account_Opportunity_Relationship__c.SObjectType);
        mocks.when(mockAccountOpportunitySelector.byIdAsMap(mockOppMemberIds)).thenReturn(mockOppMembers);

        mocks.stopStubbing();

        Application.Selector.setMock(mockAccountOpportunitySelector);

        Test.startTest();

        ProdOppAssignmentService service = new ProdOppAssignmentService();
        service.insertCurrencyProdOppAssignment(mockProdOpportunityAssignments);

        Test.stopTest();
        System.assertEquals('EUR', mockProdOpportunityAssignments[0].CurrencyIsoCode);
    }

    @IsTest
    static void updateCurrencyRelatedPartnerData_Test() {
        Id prodOppAssignRecordTypeId = Schema.SObjectType.Product_Opportunity_Assignment__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product_Opportunity_Assignment').getRecordTypeId();

        fflib_ApexMocks mocks = new fflib_ApexMocks();
        fflib_SObjectUnitOfWork mockUOW = (fflib_SObjectUnitOfWork)mocks.mock(fflib_SObjectUnitOfWork.class);

        Id mockAccountId = fflib_IDGenerator.generate(Account.SObjectType);
        Account mockAccount = new Account(Id = mockAccountId, CurrencyIsoCode = 'EUR');

        Id mockOppId = fflib_IDGenerator.generate(Opportunity.SObjectType);
        Id mockOppMemberId = fflib_IDGenerator.generate(Account_Opportunity_Relationship__c.SObjectType);
        Set<Id> mockOppMemberIds = new Set<Id> {mockOppMemberId};
        Account_Opportunity_Relationship__c mockOppMember = new Account_Opportunity_Relationship__c(Id = mockOppMemberId, Account__c = mockAccountId, Account__r = mockAccount, Opportunity__c = mockOppId);
        Map<Id, Account_Opportunity_Relationship__c> mockOppMembers = new Map<Id, Account_Opportunity_Relationship__c> {mockOppMemberId => mockOppMember};

        Id mockProdAssignmentId = fflib_IDGenerator.generate(Product_Assignment__c.SObjectType);
        Product_Assignment__c mockProdAssignment = new Product_Assignment__c(Id = mockProdAssignmentId, CurrencyIsoCode = 'EUR');
        Map<Id, Product_Assignment__c> mockProdAssignmentMap = new Map<Id, Product_Assignment__c>{mockProdAssignmentId => mockProdAssignment};

        Id mockProdOppAssignmentId = fflib_IDGenerator.generate(Product_Opportunity_Assignment__c.SObjectType);
        Product_Opportunity_Assignment__c mockProdOppAssignment = new Product_Opportunity_Assignment__c(Id = mockProdOppAssignmentId, RecordTypeId = prodOppAssignRecordTypeId, OpportunityMemberId__c = mockOppMemberId, ProductAssignmentId__c = mockProdAssignmentId, CurrencyIsoCode = 'USD');
        Map<Id, Product_Opportunity_Assignment__c> mockProdOpportunityAssignmentsMap = new Map<Id, Product_Opportunity_Assignment__c> {mockProdOppAssignmentId => mockProdOppAssignment};

        ProductAssignmentSelector mockProductAssignmentSelector = (ProductAssignmentSelector)mocks.mock(ProductAssignmentSelector.class);

        mocks.startStubbing();

        mocks.when(mockProductAssignmentSelector.sObjectType()).thenReturn(Product_Assignment__c.SObjectType);
        mocks.when(mockProductAssignmentSelector.selectProductAssignmentByIdAsMap(new Set<Id> {mockProdAssignmentId})).thenReturn(mockProdAssignmentMap);

        mocks.stopStubbing();

        Application.UnitOfWork.setMock(mockUOW);
        Application.Selector.setMock(mockProductAssignmentSelector);

        Test.startTest();

        ProdOppAssignmentService service = new ProdOppAssignmentService();
        service.updateCurrencyRelatedPartnerData(mockProdOpportunityAssignmentsMap);

        Test.stopTest();
        System.assertEquals('USD', mockProdAssignmentMap.values()[0].CurrencyIsoCode);
    }

    @IsTest
    static void updateCurrencyRelatedPartnerData2_Test() {
        Id prodOppAssignRecordTypeId = Schema.SObjectType.Product_Opportunity_Assignment__c.getRecordTypeInfosByDeveloperName().get('Partner_Flow_Product_Opportunity_Assignment').getRecordTypeId();

        fflib_ApexMocks mocks = new fflib_ApexMocks();
        fflib_SObjectUnitOfWork mockUOW = (fflib_SObjectUnitOfWork)mocks.mock(fflib_SObjectUnitOfWork.class);

        Id mockAccountId = fflib_IDGenerator.generate(Account.SObjectType);
        Account mockAccount = new Account(Id = mockAccountId, CurrencyIsoCode = 'EUR');

        Id mockOppId = fflib_IDGenerator.generate(Opportunity.SObjectType);
        Id mockOppMemberId = fflib_IDGenerator.generate(Account_Opportunity_Relationship__c.SObjectType);
        Set<Id> mockOppMemberIds = new Set<Id> {mockOppMemberId};
        Account_Opportunity_Relationship__c mockOppMember = new Account_Opportunity_Relationship__c(Id = mockOppMemberId, Account__c = mockAccountId, Account__r = mockAccount, Opportunity__c = mockOppId);
        Map<Id, Account_Opportunity_Relationship__c> mockOppMembers = new Map<Id, Account_Opportunity_Relationship__c> {mockOppMemberId => mockOppMember};

        Id mockProdAssignmentNewId = fflib_IDGenerator.generate(Product_Assignment__c.SObjectType);
        Product_Assignment__c mockProdAssignmentNew = new Product_Assignment__c(Id = mockProdAssignmentNewId, CurrencyIsoCode = 'EUR');
        Map<Id, Product_Assignment__c> mockProdAssignmentNewMap = new Map<Id, Product_Assignment__c>{mockProdAssignmentNewId => mockProdAssignmentNew};
        Id mockProdAssignmentOldId = fflib_IDGenerator.generate(Product_Assignment__c.SObjectType);
        Product_Assignment__c mockProdAssignmentOld = new Product_Assignment__c(Id = mockProdAssignmentOldId, CurrencyIsoCode = 'EUR');
        Map<Id, Product_Assignment__c> mockProdAssignmentOldMap = new Map<Id, Product_Assignment__c>{mockProdAssignmentOldId => mockProdAssignmentOld};


        Id mockProdOppAssignmentId = fflib_IDGenerator.generate(Product_Opportunity_Assignment__c.SObjectType);
        Product_Opportunity_Assignment__c mockProdOppAssignmentNew = new Product_Opportunity_Assignment__c(Id = mockProdOppAssignmentId, RecordTypeId = prodOppAssignRecordTypeId, OpportunityMemberId__c = mockOppMemberId, ProductAssignmentId__c = mockProdAssignmentNewId, CurrencyIsoCode = 'USD');
        Map<Id, Product_Opportunity_Assignment__c> mockProdOpportunityAssignmentsNewMap = new Map<Id, Product_Opportunity_Assignment__c> {mockProdOppAssignmentId => mockProdOppAssignmentNew};

        Product_Opportunity_Assignment__c mockProdOppAssignmentOld = new Product_Opportunity_Assignment__c(Id = mockProdOppAssignmentId, RecordTypeId = prodOppAssignRecordTypeId, OpportunityMemberId__c = mockOppMemberId, ProductAssignmentId__c = mockProdAssignmentOldId, CurrencyIsoCode = 'USD');
        Map<Id, Product_Opportunity_Assignment__c> mockProdOpportunityAssignmentsOldMap = new Map<Id, Product_Opportunity_Assignment__c> {mockProdOppAssignmentId => mockProdOppAssignmentOld};


        ProductAssignmentSelector mockProductAssignmentSelector = (ProductAssignmentSelector)mocks.mock(ProductAssignmentSelector.class);

        mocks.startStubbing();

        mocks.when(mockProductAssignmentSelector.sObjectType()).thenReturn(Product_Assignment__c.SObjectType);
        mocks.when(mockProductAssignmentSelector.selectProductAssignmentByIdAsMap(new Set<Id> {mockProdAssignmentNewId})).thenReturn(mockProdAssignmentNewMap);
        mocks.stopStubbing();

        Application.UnitOfWork.setMock(mockUOW);
        Application.Selector.setMock(mockProductAssignmentSelector);

        Test.startTest();

        ProdOppAssignmentService service = new ProdOppAssignmentService();
        service.updateCurrencyRelatedPartnerData(mockProdOpportunityAssignmentsNewMap, mockProdOpportunityAssignmentsOldMap);

        Test.stopTest();
    }

    @IsTest
    static void insertCurrencyProdOppAssignmentFailure_Test() {
        Test.startTest();

        try {
            Id rtAccId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gym_Partner').getRecordTypeId();
            ProdOppAssignmentService service = new ProdOppAssignmentService();
            service.insertCurrencyProdOppAssignment(new List<Product_Opportunity_Assignment__c> {new Product_Opportunity_Assignment__c(RecordTypeId = rtAccId)});
        } catch (Exception e) {
            system.debug('## msg: '+e.getMessage());
            system.assert(e instanceof ProdOppAssignmentService.ProdOppAssignmentServiceException);
        }

        Test.stopTest();

    }

    @IsTest
    static void updateCurrencyRelatedPartnerDataFailure_Test() {
        Test.startTest();

        try {
            Id rtAccId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gym_Partner').getRecordTypeId();
            ProdOppAssignmentService service = new ProdOppAssignmentService();
            service.updateCurrencyRelatedPartnerData(new Map<Id, Product_Opportunity_Assignment__c> {rtAccId => new Product_Opportunity_Assignment__c(RecordTypeId = rtAccId)});
        } catch (Exception e) {
            system.debug('## msg: '+e.getMessage());
            system.assert(e instanceof ProdOppAssignmentService.ProdOppAssignmentServiceException);
        }

        Test.stopTest();

    }

    @IsTest
    static void updateCurrencyRelatedPartnerDataFailure2_Test() {
        Test.startTest();

        try {
            Id rtAccId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gym_Partner').getRecordTypeId();
            ProdOppAssignmentService service = new ProdOppAssignmentService();
            service.updateCurrencyRelatedPartnerData(new Map<Id, Product_Opportunity_Assignment__c> {rtAccId => new Product_Opportunity_Assignment__c(RecordTypeId = rtAccId)}, new Map<Id, Product_Opportunity_Assignment__c> {rtAccId => new Product_Opportunity_Assignment__c(RecordTypeId = rtAccId)});
        } catch (Exception e) {
            system.debug('## msg: '+e.getMessage());
            system.assert(e instanceof ProdOppAssignmentService.ProdOppAssignmentServiceException);
        }

        Test.stopTest();

    }

}