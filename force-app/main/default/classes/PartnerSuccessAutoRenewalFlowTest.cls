/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 05-31-2021
 * @last modified by  : gft.samuel.silva@ext.gympass.com
 * Modifications Log 
 * Ver   Date         Author                             Modification
 * 1.0   05-25-2021   gft.samuel.silva@ext.gympass.com   Initial Version
**/
@isTest(seeAllData=false)
public with sharing class PartnerSuccessAutoRenewalFlowTest {
    
    @TestSetup
    static void createData(){  
        
        Id recordTypePartnerForm = Schema.SObjectType.Ops_Setup_Validation_Form__c.getRecordTypeInfosByDeveloperName().get('Partner_Ops_Setup_Validation').getRecordTypeId();
        Id recordTypeIdBank = Schema.SObjectType.Bank_Account__c.getRecordTypeInfosByDeveloperName().get('Gyms_Bank_Account').getRecordTypeId();
        Id recordTypeIdAccBank = Schema.SObjectType.Acount_Bank_Account_Relationship__c.getRecordTypeInfosByDeveloperName().get('Gyms_Account_Bank_Account_Partner').getRecordTypeId();    
        Id recordTypeIdProductItem = Schema.SObjectType.Product_Item__c.getRecordTypeInfosByDeveloperName().get('Gyms_Product_Item_Partner').getRecordTypeId();
        
        Account acc = generateGympassEntity();
        insert acc;
        
        Id ctcRtId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Gyms_Partner').getRecordTypeId();
        Contact contactAcc = new Contact(
            FirstName = 'Fernando',
            LastName = 'Souza',
            Email = 'fernandoTestSouze@mail.com',
            Phone = '111111111',
            recordTypeId = ctcRtId,
            MailingCountry = 'Brazil',
            Type_of_contact__c = 'Point of Contact;Legal Representative',
            Cargo__c = 'CEO',            
            accountId = acc.id
        );       
        INSERT contactAcc;       

        Pricebook2 standardPricebook = new Pricebook2(
            Id = Test.getStandardPricebookId(),
            IsActive = true
        );
        update standardPricebook;
        Pricebook2 pb = standardPricebook;                            
        Opportunity opp = new Opportunity(CurrencyIsoCode='BRL');
        opp = generateOpportunity('Gyms_Small_Medium_Partner',acc,pb);
        insert opp;	
        
        Account_Opportunity_Relationship__c lAccOppRelationship = new Account_Opportunity_Relationship__c();
        lAccOppRelationship.Opportunity__c = opp.Id;
        lAccOppRelationship.Account__c = acc.Id;
        insert lAccOppRelationship;

        
        List<Quote> propLst = new List<Quote>();
        Quote proposal = new Quote();
        proposal.RecordTypeId = Schema.SObjectType.Quote.getRecordTypeInfosByDeveloperName().get('Gyms_Quote_Partner').getRecordTypeId();
        proposal.Name = 'academiaBrasilCompanyQuote';
        proposal.Status = 'Ganho';
        proposal.Signed__c = Date.today();
        proposal.Start_Date__c  = Date.today();
        proposal.Final_Date__c =  Date.today().addYears(4);
        proposal.Priority_Tier__c = 'Tier 1';
        proposal.PR_Activity__c  = 'Yes';
        proposal.discounts_usage_volume_range__c = 'Yes';
        proposal.Threshold_Value_1__c = 12;
        proposal.Threshold_Value_2__c = 32;                         
        proposal.Threshold_Value_3__c = 42;                
        proposal.Threshold_Value_4__c = 48;                               
        proposal.First_Discount_Range__c = 20;
        proposal.Second_Discount_Range__c = 24; 
        proposal.Third_Discount_Range__c = 56;
        proposal.Fourth_Discount_Range__c = 70;
        proposal.Volume_Discount_Type__c = 'By single user volume';
        proposal.OpportunityId = opp.Id;        
        propLst.add(proposal);
        insert propLst;
        
        /**PRODUCT ITEM*/                
        Product_Item__c prodItem = new Product_Item__c();
        prodItem.recordTypeId = recordTypeIdProductItem;
        prodItem.Opportunity__c = opp.id;
        prodItem.Product_Type__c = 'In person';
        prodItem.Is_Network_CAP__c = 'No';
        prodItem.CAP_Value__c = 100;
        prodItem.CurrencyIsoCode = 'BRL';
        prodItem.Do_You_Have_A_No_Show_Fee__c = 'No';
        prodItem.Has_Late_Cancellation_Fee__c = 'No';
        prodItem.Late_Cancellation_Percent__c = '0';
        prodItem.Maximum_Live_Class_Per_Month__c = 0;
        prodItem.No_Show_Fee_Percent__c = '0';
        prodItem.Gym_Activity_Concat__c = 'Dance';
        prodItem.Product_Definition__c  ='Dança';
        prodItem.Product_Restriction__c = 'None';
        prodItem.Name = 'Dança'; 
        prodItem.Package_Type__c = 'Value per class';      
        prodItem.Max_Monthly_Visit_Per_User__c = 12;
        prodItem.Reference_Price_Value_Unlimited__c = 1;
        prodItem.Net_Transfer_Price__c  = 11;
        prodItem.Price_Visits_Month_Package_Selected__c = 400;
        prodItem.Selected_Plan__c = 'Gold';
        prodItem.Justification_Plan__c = 'Fit';
        INSERT prodItem;
        
        Ops_Setup_Validation_Form__c accOppForm = new Ops_Setup_Validation_Form__c (
        recordTypeId = recordTypePartnerForm,
        Name = opp.Name + ' - OPS Form',
        Status__c = 'Approved',
        Did_you_send_the_training_email__c = 'Yes',
        Opportunity__c = opp.Id ); 
        INSERT accOppForm;
        
        /**Bank Account*/
        Bank_Account__c bankAccount = new Bank_Account__c();
        bankAccount.recordTypeId = recordTypeIdBank;
        bankAccount.Bank_Account_Number_or_IBAN__c = 'abc';
        bankAccount.Bank_Account_Ownership__c = 'Contact text';
        bankAccount.Bank_Name__c = 'ItauFake';
        bankAccount.BIC_or_Routing_Number_or_Sort_Code__c = 'abc';
        bankAccount.Routing_Number__c = 1234;
        bankAccount.VAT_Number_or_UTR_number__c = 'tttt1231';
        INSERT bankAccount;
        
        Acount_Bank_Account_Relationship__c abr = new Acount_Bank_Account_Relationship__c();
        abr.Account__c = acc.Id;
        abr.Bank_Account__c = bankAccount.id;
        abr.RecordTypeId = recordTypeIdAccBank;
        INSERT abr;
    }
    
	@isTest
    private static void shouldCloneOpportunityAndRelatedObjects(){
        Id recordTypePartnerForm = Schema.SObjectType.Ops_Setup_Validation_Form__c.getRecordTypeInfosByDeveloperName().get('Partner_Ops_Setup_Validation').getRecordTypeId();
        id RtOpp = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Partner_Small_and_Medium_Renegotiation').getRecordTypeId();
        Opportunity lOpp = [SELECT id,AccountId, OwnerId, Name,Bank_Account_Related__c,RecordType.DeveloperName FROM Opportunity Limit 1 ];
        lOpp.StageName = 'Lançado/Ganho';
        lOpp.Sub_Type__c = 'Renegotiation';
        lOpp.Cancellation_Reason__c = 'Fraud';
        lOpp.Cancellation_Reason_subcategory__c = 'Visit manipulation';
        lOpp.RecordTypeId = RtOpp;
        update lOpp;
        String teste = 'Partner_Small_and_Medium_Renegotiation';
        Account lAcc = [ SELECT Id FROM Account LIMIT 1 ];
        List <Id> accIdInsert = new List<Id>();
        accIdInsert.add(lAcc.Id);
        
        Test.startTest();
        PartnerSuccessAutoRenewalFlow.cloneLastClosedOpp( accIdInsert, teste,lOpp.Sub_Type__c,lOpp.Cancellation_Reason__c, lOpp.Cancellation_Reason_subcategory__c, 'CX Team');
        Test.stopTest();
        
        List< Opportunity > lLstOpp = 
            [ SELECT Id,( SELECT Id FROM Quotes ) FROM Opportunity WHERE StageName = 'Qualificação' AND Recordtype.DeveloperName = 'Partner_Small_and_Medium_Renegotiation' ];
        
        System.assert( lLstOpp.size() == 1 );
        System.assert( lLstOpp[0].Quotes.size() == 1 );
    }
    
    @isTest
    private static void shouldCreateNewRenegotiationOpportunity(){
        Id lOppR = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Partner_Wishlist_Renegotiation').getRecordTypeId();
    	Opportunity lOpp = [ SELECT Id,AccountId FROM Opportunity LIMIT 1 ];
        lOpp.StageName = 'Perdido';
        lOpp.Loss_Reason__c = 'Lost to Competitor';
        lOpp.Sub_Type__c = 'Renegotiation';
        lOpp.Cancellation_Reason__c = 'Fraud';
        lOpp.Cancellation_Reason_subcategory__c = 'Visit manipulation';
        lOpp.Loss_Description__c = 'Perdido';
        lOpp.Loss_Reason__c = 'Fear of cannibalization';
        lOpp.RecordTypeId = lOppr;
        lOpp.Description = 'Descrição';
        update lOpp;
        String teste = 'Partner_Wishlist_Renegotiation';
        
        Account lAcc = [ SELECT Id FROM Account LIMIT 1 ];
        List <Id> accIdInsert = new List<Id>();
        accIdInsert.add(lAcc.Id);
        
        Test.startTest();
        PartnerSuccessAutoRenewalFlow.cloneLastClosedOpp( accIdInsert, 'Partner_Wishlist_Renegotiation',lOpp.Sub_Type__c,lOpp.Cancellation_Reason__c, lOpp.Cancellation_Reason_subcategory__c, 'CX Team' );
        Test.stopTest();
        
        List< Opportunity > lLstOpp = 
            [ SELECT Id, Pricebook2Id,( SELECT Id FROM Quotes ) 
              FROM Opportunity WHERE StageName = 'Qualificação' AND Recordtype.DeveloperName = 'Partner_Small_and_Medium_Renegotiation' ];
      
    } 

    private static Account generateGympassEntity(){
        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Gym_Partner').getRecordTypeId();
        Account lAcc = new Account();
        lAcc.name='AcademiaBrasilCompanyPai';
        lAcc.RecordTypeId = rtId;
        lAcc.GP_Status__c = 'Active';
        lAcc.billingState = 'Minas Gerais';
        lAcc.CAP_Value__c = 120;
        lAcc.BillingCity = 'CityAcademiaBrasil';
        lAcc.billingStreet = 'Rua academiabrasilpai';
        lAcc.billingCountry = 'Brazil';
        lAcc.ShippingCountry ='Brazil';
        lAcc.ShippingStreet ='Dona Maria';
        lAcc.ShippingCity ='Uberlandia';
        lAcc.Gym_Type__c = 'Studios';
        lAcc.Gym_Classes__c = 'Cardio';
        lAcc.Legal_Title__c = '11111';
        lAcc.Id_Company__c = '11111';
        lAcc.Types_of_ownership__c = Label.franchisePicklist;
        lAcc.Subscription_Type__c = 'Value per class';
        lAcc.Subscription_Period__c = 'Monthy value';
        lAcc.Subscription_Type_Estimated_Price__c    = 100;
        lAcc.Has_market_cannibalization__c = 'No';
        lAcc.Exclusivity__c = 'Yes';
        lAcc.Exclusivity_End_Date__c = Date.today().addYears(1);
        lAcc.Exclusivity_Partnership__c = 'Full Exclusivity';
        lAcc.Exclusivity_Restrictions__c= 'No';
        lAcc.Website = 'testing@tesapex.com';
        lAcc.Gym_Email__c = 'gymemail@apex.com';
        lAcc.Phone = '3222123123';
        lAcc.Can_use_logo__c = 'Yes';
        lAcc.Legal_Registration__c = 12123;
        lAcc.Legal_Title__c = 'Title LTDA';
        lAcc.Gyms_Identification_Document__c = 'CNPJ'; 
        return lAcc;
    }
    
    private static Opportunity generateOpportunity(String aRecordType, Account lAcc, Pricebook2 lPricebook){
        Id lOppRt = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(aRecordType).getRecordTypeId();
        Opportunity lAccOpp = new Opportunity();
        lAccOpp.recordTypeId = lOppRt;
        lAccOpp.AccountId = lAcc.id;
        lAccOpp.Name = lAcc.Id; 
        lAccOpp.CMS_Used__c = 'Yes';     
        lAccOpp.Gym_agreed_to_an_API_integration__c  = 'Yes';
        lAccOpp.Club_Management_System__c = 'Companhia Athletica';
        lAccOpp.Integration_Fee_Deduction__c = 'No';
        lAccOpp.CloseDate = Date.today();
        lAccOpp.Success_Look_Like__c = 'Yes';
        lAccOpp.Success_Look_Like_Description__c = 'Money money';
        lAccOpp.StageName = 'Proposta Enviada';
        lAccOpp.Type = 'Expansion';  
        lAccOpp.Country_Manager_Approval__c = true;
        lAccOpp.Payment_approved__c = true;   
        lAccOpp.CurrencyIsoCode = 'BRL';
        lAccOpp.Gympass_Plus__c = 'Yes';
        lAccOpp.Standard_Payment__c = 'Yes';
        lAccOpp.Request_for_self_checkin__c = 'Yes';  
        lAccOpp.Pricebook2Id = lPricebook.Id;
        lAccOpp.Training_Comments__c = 'teste';
        lAccOpp.Should_OPS_team_send_the_training__c = 'Standard Training';
        return lAccOpp;
    }
    
    private static Pricebook2 generatePricebook(){        
        Pricebook2 pb = new Pricebook2();
        pb.Name = 'Br';
        pb.Country__c = 'Brazil';
        pb.IsActive = true;
        return pb;
    }
    
    private static PricebookEntry generatePricebookEntry(Pricebook2 pb, Product2 product, Opportunity opp){        
        PricebookEntry pbEntry = new PricebookEntry();
        pbEntry.Product2Id = product.Id;
        pbEntry.Pricebook2Id = Test.getStandardPricebookId();
        pbEntry.UnitPrice = 100;
        pbEntry.IsActive = true;
        pbEntry.CurrencyIsoCode = opp.CurrencyIsoCode;
        return pbEntry;
    }
    
    private static Product2 generateProduct(String ProductName){
        Product2 product = new Product2();
        product.Name = ProductName;
        product.Minimum_Number_of_Employees__c = 0;
        product.Maximum_Number_of_Employees__c = 900;
        product.Family = ProductName;
        product.IsActive = true;
        return product;
    }
      
}