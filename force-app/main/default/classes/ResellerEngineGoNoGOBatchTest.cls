@isTest
public class ResellerEngineGoNoGOBatchTest {
    
    static testMethod void testMethod1() {
        
        Set<Id> AccountRequestIds = new Set<Id>();
        Account_Request__c acc = new Account_Request__c();
        acc.Name ='Name';
        AccountRequestIds.add(acc.Id);
        insert acc;
     
        list<Account_Request__c> actList = [SELECT Id FROM Account_Request__c WHERE Id =:AccountRequestIds];
        ResellerEngineGoNoGO engine = new ResellerEngineGoNoGO(actList);
        engine.noGO();
        
        Test.startTest();
        ResellerEngineGoNoGOBatch obj = new ResellerEngineGoNoGOBatch();
        DataBase.executeBatch(obj, 150);
        Test.stopTest();
        
        List<Account_Request__c> updateAr = [SELECT Id FROM Account_Request__c LIMIT 1];
        System.assertEquals(1,updateAr.size());
    }
}