/**
 * @author ercarval
 */
public class IntegrationException extends Exception {
  private static List<String> errorMessages;

  public IntegrationException(HttpResponse httpResponse) {
    this(
      httpResponse.getStatusCode() +
      ' : ' +
      httpResponse.getStatus() +
      ' [ ' +
      httpResponse.getBody() +
      ' ] '
    );

    if (errorMessages != null) {
      errorMessages.add(this.getMessage());
    } else {
      errorMessages = new List<String>{this.getMessage()};
    }
  }

  public static List<String> getMessages() {
    return errorMessages;
  }

  public static void setMessages(List<String> messages) {
    errorMessages = messages;
  }
}