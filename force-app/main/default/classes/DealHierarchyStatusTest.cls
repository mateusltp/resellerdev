/**
 * @description       : 
 * @author            : Samuel Silva - GFT (slml@gft.com)
 * @group             : 
 * @last modified on  : 12-29-2020
 * @last modified by  : Samuel Silva - GFT (slml@gft.com)
 * Modifications Log 
 * Ver   Date         Author                              Modification
 * 1.0   11-27-2020   Samuel Silva - GFT (slml@gft.com)   Initial Version
**/
@isTest
public with sharing class DealHierarchyStatusTest {
  
    @TestSetup
    static void createData(){        

        Pricebook2 standardPricebook = new Pricebook2(
            Id = Test.getStandardPricebookId(),
            IsActive = true
        );
        update standardPricebook;

        List<Account> childList1 = new List<Account>();
        Account parent1 = generateClientsAccount('Parent1');              
        INSERT parent1;
        Account child1 = generateClientsAccount('Child1'); 
        child1.parentId = parent1.id;
        childList1.add(child1);  
        Account child2 = generateClientsAccount('Child2'); 
        child2.parentId = parent1.id;
        childList1.add(child2);   
        Account child3 = generateClientsAccount('Child3');   
        child3.parentId = parent1.id;
        childList1.add(child3); 
        INSERT childList1;

        List<Account> childList2 = new List<Account>();
        Account parent2 = generateClientsAccount('Parent2');              
        INSERT parent2;
        Account rchild1 = generateClientsAccount('rChild1'); 
        rchild1.parentId = parent2.id;
        childList2.add(rchild1);  
        Account rchild2 = generateClientsAccount('rChild2'); 
        rchild2.parentId = parent2.id;
        childList2.add(rchild2);   
        Account rchild3 = generateClientsAccount('rChild3');   
        rchild3.parentId = parent2.id;
        childList2.add(rchild3); 
        INSERT childList2;

        Pricebook2 pb = standardPricebook;                      
        Product2 accessFee = generateProduct('Enterprise Subscription');
        insert accessFee;
        Product2 setupFee = generateProduct('Setup Fee');
        insert setupFee;        
        List<Opportunity> oppLst = new List<Opportunity>();
        Opportunity oppNb = generateNBOpp(parent1.Id,pb);     
        oppLst.add(oppNb);
        //INSERT oppNb;

        Opportunity oppRen = generateRenegotiationOpp(parent1.Id,pb);  
        oppLst.add(oppRen);

        Opportunity oppRet = generateRetentionOpp(parent2.Id,pb);
        oppLst.add(oppRet);
        INSERT oppLst;          
    
        
        List<Account_Opportunity_Relationship__c> aorList1 = createDealHierarchy(childList1, oppNb);
        INSERT aorList1; 
        childList1.remove(2);
        List<Account_Opportunity_Relationship__c> aorListRen = createDealHierarchy(childList1, oppRen);
        INSERT aorListRen;
        Case c = generateCaseCancellation(oppRet.Id);
        INSERT c;
        List<Account_Opportunity_Relationship__c> aorList2 = createDealHierarchy(childList2, oppRet);
        INSERT aorList2;
        for(Opportunity o : oppLst){
            o.StageName = 'Lançado/Ganho';
           // o.FastTrackStage__c = 'Launched/Won';
        }     
        //UPDATE oppLst;
    }

    @isTest
    public static void testCloseRetention() {
           
        Test.startTest();    
            Account acc = [SELECT Id FROM Account WHERE Name = 'AccountParent2'];
            Opportunity opp = [SELECT ID, StageName,FastTrackStage__c,Loss_Reason__c, Sub_Type__c FROM Opportunity WHERE AccountId =: acc.Id AND RecordType.DeveloperName = 'Client_Success_Renegotiation' AND Sub_Type__c = 'Retention'];  
            Case cancelCase = [SELECT Id, I_have_deactivated_the_current_contract__c, Status,Cancellation_Reason__c FROM Case WHERE OpportunityId__c =: opp.Id AND RecordType.DeveloperName = 'Contract_Cancellation'];          
            cancelCase.I_have_deactivated_the_current_contract__c = true;
            cancelCase.status = 'Confirmed';
            cancelCase.Cancellation_Reason__c = 'Test';
            UPDATE cancelCase;
            List<Account> childs = [SELECT ID, GP_Status__c, Type FROM Account WHERE ParentId =: acc.Id];
            System.assertEquals('Inactive', childs.get(0).GP_Status__c);
            System.assertEquals('Inactive', childs.get(1).GP_Status__c);
            System.assertEquals('Inactive', childs.get(2).GP_Status__c);
        Test.stopTest();        
    }

    @isTest
    public static void testCloseCaseRetention() {    
         Test.startTest();    
            Account acc = [SELECT Id FROM Account WHERE Name = 'AccountParent2'];
            Opportunity opp = [SELECT ID, StageName,FastTrackStage__c, Loss_Reason__c, Sub_Type__c FROM Opportunity WHERE AccountId =: acc.Id AND RecordType.DeveloperName = 'Client_Success_Renegotiation' AND Sub_Type__c = 'Retention'];  
            opp.StageName = 'Lançado/Ganho';   
            UPDATE opp;  
            List<Account> childs = [SELECT ID, GP_Status__c, Type FROM Account WHERE ParentId =: acc.Id];
            System.assertEquals('Active', childs.get(0).GP_Status__c);
            System.assertEquals('Active', childs.get(1).GP_Status__c);
            System.assertEquals('Active', childs.get(2).GP_Status__c);
        Test.stopTest();   
        
    }

    @isTest
    public static void testCloseNbOpp() {    
       Test.startTest();   
            Account acc = [SELECT Id FROM Account WHERE Name = 'AccountParent1'];
            Opportunity opp = [SELECT ID, StageName,FastTrackStage__c, Sub_Type__c FROM Opportunity WHERE AccountId =: acc.Id AND RecordType.DeveloperName = 'Client_Sales_New_Business'];   
            
            // opp.StageName = 'Lançado/Ganho';   
            // UPDATE opp;     
            List<Account> childs = [SELECT ID, GP_Status__c, Type FROM Account WHERE ParentId =: acc.Id];
            System.assertEquals('Active', childs.get(0).GP_Status__c);
            System.assertEquals('Active', childs.get(1).GP_Status__c);
            System.assertEquals('Active', childs.get(2).GP_Status__c);
        Test.stopTest();   
        
    }


    @isTest
    public static void testCloseRenegotiation() {     
         Test.startTest();   
            Account acc = [SELECT Id FROM Account WHERE Name = 'AccountParent1'];
            Opportunity oppRen = [SELECT ID, StageName,FastTrackStage__c, Loss_Reason__c, Sub_Type__c FROM Opportunity WHERE AccountId =: acc.Id AND RecordType.DeveloperName = 'Client_Success_Renegotiation' AND Sub_Type__c = 'Renegotiation'];   
            oppRen.StageName = 'Lançado/Ganho';
            Update oppRen;  
            List<Account> childs = [SELECT ID, GP_Status__c, Type FROM Account WHERE ParentId =: acc.Id AND GP_Status__c='Inactive'];
            //System.assertEquals('Inactive', childs.get(0).GP_Status__c);
        Test.stopTest();   
    }


    private static Account generateClientsAccount(String subname){
        ID accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Empresas').getRecordTypeId();
        Account acc = new Account();
        acc.RecordTypeId = accRecordTypeId;
        acc.Name = 'Account' + subname;
        acc.BillingCountry = 'Brazil';
        acc.BillingState = 'São Paulo';
        acc.GP_Status__c = 'Active';
        acc.Type = 'Prospecting';
        acc.NumberOfEmployees = 2000;
        return acc;
    }

    private static Case generateCaseCancellation(Id oppId){
        ID rt = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Contract_Cancellation').getRecordTypeId();
        Case c = new Case();
        c.RecordTypeId = rt;
        c.OpportunityId__c = oppId;
        return c;
    }

    
    private static Opportunity generateRetentionOpp(Id accId, Pricebook2 pb){
        Id oppRtId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Success_Renegotiation').getRecordTypeId();
        Opportunity opp = new Opportunity();
        opp.recordTypeId = oppRtId;
        opp.StageName='Qualification';
        opp.Loss_Reason__c = 'Price';
        opp.Sub_Type__c = 'Retention';
        opp.cancellation_date__c = Date.today();
        opp.AccountId = accId;
        opp.CloseDate = Date.today();
        opp.Engagement_Journey_Completed__c = true;
        opp.Name='Retention'; 
        opp.Pricebook2Id = pb.Id;
        return opp;
    }

    private static Opportunity generateRenegotiationOpp(Id accId, Pricebook2 pb){
        Id oppRtId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Success_Renegotiation').getRecordTypeId();
        Opportunity opp = new Opportunity();
        opp.CurrencyIsoCode='BRL';
        opp.recordTypeId = oppRtId;
        opp.StageName='Qualification';
        opp.Sub_Type__c = 'Renegotiation';
        opp.AccountId = accId;
        opp.CloseDate = Date.today();
        opp.Engagement_Journey_Completed__c = true;
        opp.Name='Renegotiation'; 
        opp.Pricebook2Id = pb.Id;
        return opp;
    }

    private static Opportunity generateNBOpp(Id accId, Pricebook2 pb){
        Id oppRtId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Client_Sales_New_Business').getRecordTypeId();
        Opportunity opp = new Opportunity();
        opp.CurrencyIsoCode='BRL';
        opp.recordTypeId = oppRtId;
        opp.StageName='Qualification';
        opp.AccountId = accId;
        opp.CloseDate = Date.today();
        opp.Engagement_Journey_Completed__c = true;
        opp.Name='Renegotiation'; 
        opp.Pricebook2Id = pb.Id;
        return opp;
    }


    private static Product2 generateProduct(String ProductName){
        Product2 product = new Product2();
        product.Name = ProductName;
        product.Minimum_Number_of_Employees__c = 0;
        product.Maximum_Number_of_Employees__c = 900;
        product.Family = ProductName;
        product.IsActive = true;
        return product;
    }

    private static List<Account_Opportunity_Relationship__c> createDealHierarchy(List<Account> childAccounts, Opportunity opp){
        List<Account_Opportunity_Relationship__c> aorLst = new List<Account_Opportunity_Relationship__c>();
        Id rt = Schema.SObjectType.Account_Opportunity_Relationship__c.getRecordTypeInfosByDeveloperName().get('Client_Sales').getRecordTypeId();
        for(Account c : childAccounts){
            Account_Opportunity_Relationship__c aor = new Account_Opportunity_Relationship__c();
            aor.Opportunity__c = opp.Id;
            aor.Account__c = c.Id;
            aor.RecordTypeId = rt;
            aorLst.add(aor);
        }
        return aorLst;
    }

}