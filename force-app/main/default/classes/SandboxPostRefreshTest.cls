/**
 * @description       : Test Class for SandboxPostRefresh
 * @author            : tiago.ribeiro@gympass.com
 * @group             : 
 * @last modified on  : 03-10-2022
 * @last modified by  : tiago.ribeiro@gympass.com
**/
@isTest
public with sharing class SandboxPostRefreshTest {
    @TestSetup
    static void makeData(){
        insert DataFactory.newEventConfigurations(10);
    }

    @isTest
    static void runPostRefreshTest() {
        Test.startTest();

        Test.testSandboxPostCopyScript(
            new SandboxPostRefresh(), 
            UserInfo.getOrganizationId(), 
            UserInfo.getOrganizationId(), 
            UserInfo.getOrganizationName()
            );

        Test.stopTest();

        List<EventConfiguration__c> listEventConfiguration = EventConfiguration__c.getAll().values();
        for(EventConfiguration__c eventConfig : listEventConfiguration){
            System.assertEquals(eventConfig.EndPointUrl__c, Label.Sandbox_Post_Refresh_URL, 'Endpoint URL not updated on Post Refresh');
        }
    }
}