/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 04-06-2022
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/
public without sharing class ContentVersionTriggerHandlerHelper {

    static boolean hasRun = false;
    
    public static void cloneContractAgreementFile(List<ContentVersion> listTriggerNew){
    
        System.debug(':: ContentVersionTriggerHandlerHelper entrou cloneContractAgreementFile');     
        
        try{
            Set<id> setContentDocumentId = new Set<Id>();
        
            for (ContentVersion cont: (List<ContentVersion>)listTriggerNew){
                setContentDocumentId.add(cont.ContentDocumentId);
            }

            List<ContentDocumentLink> listLink = [SELECT LinkedEntityId, ContentDocumentId , ContentDocument.Title , ContentDocument.ContentModifiedDate, ContentDocument.ContentSize  
                                                        FROM ContentDocumentLink WHERE ContentDocumentId = : setContentDocumentId ];

            Map<Id, ContentDocumentLink> mapContractAndContentDocLink = new Map<Id, ContentDocumentLink>();

            
            for (ContentDocumentLink contentDocLink : listLink ){
                Schema.SObjectType sobjectType = contentDocLink.LinkedEntityId.getSObjectType();
                if (sobjectType.getDescribe().getName() == 'APXT_Redlining__Contract_Agreement__c' ){                    
                    mapContractAndContentDocLink.put(contentDocLink.LinkedEntityId, contentDocLink);
                }
            }
            
            //Send notification when a new file has been attached to the contract agreement
            LegalSendEmailNewCaseFile.sendNotification(mapContractAndContentDocLink.keyset());          
            

            List<Document__c> listDocument = [SELECT Id, ContractAgreement__c FROM Document__c WHERE ContractAgreement__c IN : mapContractAndContentDocLink.keyset()];

            List<ContentDocumentLink> conClone = new List<ContentDocumentLink>();
            if(mapContractAndContentDocLink.values().size()> 0 ){
                for (Id linkedEntityId:  mapContractAndContentDocLink.keyset()){
                    for (Document__c doc : listDocument ){
                        if (doc.ContractAgreement__c == linkedEntityId){
                            ContentDocumentLink newclnk = mapContractAndContentDocLink.get(linkedEntityId).clone();
                            newclnk.LinkedEntityId = doc.Id;
                            newclnk.ShareType = 'V';
                            conClone.add(newclnk);
                        }
                    }
                }
            }
            
            if (!conClone.isEmpty()){
                upsert conClone;
            }
        }
        catch(Exception ex){
            system.debug('Error : ' + ex.getMessage());
        }
        
    }

}