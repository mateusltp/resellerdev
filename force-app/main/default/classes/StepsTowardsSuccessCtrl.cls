/**
 * @File Name          : StepsTowardsSuccessCtrl.cls
 * @Description        : 
 * @Author             : GEPI@GFT.com
 * @Group              : 
 * @Last Modified By   : alysson.mota@gympass.com
 * @Last Modified On   : 04-08-2022
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    08/06/2020   GEPI@GFT.com     Initial Version
**/
public without sharing class StepsTowardsSuccessCtrl {
    public static List<Profile> getProfilesList() {
        return [SELECT Id, Name FROM Profile];
    }
    @AuraEnabled
    public static Response getResponse(String recordId) {
        Response response = null;
        
        Set<String> relationshipProfilesNameSet = new Set<String>{};
            
        Set<String> accountRecords12StepsSet = new Set<String>{
            'Direct Channel','Indirect Channel'
        };
            
        Set<String> accountRecords10StepsSet = new Set<String>{
            'Gym_Partner'
        };
        
        Set<String> accRecsPartnerFlowSet = new Set<String>{
            'Partner_Flow_Account'
        };

        Set<String> oppRecsPartnerFlowSet = new Set<String>{
            'Partner_Flow_Opportunity'
        };

        Set<String> opportunityRecords10StepsSet = new Set<String>{
            'Gyms_Small_Medium_Partner', 'Gyms_Wishlist_Partner', 'Partner_Small_and_Medium_Renegotiation','Partner_Wishlist_Renegotiation'
        };

        Set<String> opportunityRecords12StepsSet = new Set<String>{
            'SMB', 'Indirect_Channel'
        };
        
        List<Profile> profilesList = getProfilesList();
        
        for (Profile profile : profilesList) {
            if (profile.Name.contains('Relationship') || profile.Name.contains('Success'))
                relationshipProfilesNameSet.add(profile.Name);
        }
        
        String sObjectType = ((Id)recordId).getSobjectType().getDescribe().getName();
        
        if (sObjectType == 'Opportunity') {
        	Opportunity opp = getOpportunity(recordId);
            
            if (opportunityRecords10StepsSet.contains(opp.RecordType.DeveloperName)) {
                // 10 steps call
                return StepsTowardsSuccessForGyms.getResponse(recordId);
            }

            if (oppRecsPartnerFlowSet.contains(opp.RecordType.DeveloperName)) {
                return PartnerEnablersController.getResponse(recordId);
            }
            
            if (opportunityRecords12StepsSet.contains(opp.RecordType.DeveloperName)) { 
                response = evalueteAndget12StepsForOpportunity(opp);
            } else {
                response = get12StepsForOpp(opp);
            }            
        }
        else if (sObjectType == 'Account') {
            Account acc = getAccount(recordId);
            
            if (accountRecords12StepsSet.contains(acc.RecordType.Name)) {
                response = getStepsForClientAccounts(acc);
            }
            else if (accountRecords10StepsSet.contains(acc.RecordType.DeveloperName)) {
           		// 10 steps call
                return StepsTowardsSuccessForGyms.getResponse(recordId);
            }
            else if (accRecsPartnerFlowSet.contains(acc.RecordType.DeveloperName)) {
                return StepsTowardsSuccessForGyms.getResponse(recordId);
            }
            else {
				response = generateEmptyResponse();
            }
        } else {
            response = generateEmptyResponse();
        }
        
        return response;
    }

    private static Response getStepsForClientAccounts(Account acc) {
        List<Step_Towards_Success1__c> accountSteps = getStepsFromAccount(acc);
        Map<Integer, Step_Towards_Success1__c> stepNumberToStep = new Map<Integer, Step_Towards_Success1__c>();
        
        for (Step_Towards_Success1__c step : accountSteps) {
            //step.Achieved__c = 'No';
			stepNumberToStep.put((Integer)step.Step_Number__c, step);
        }

        Response response = new Response();
        response.title = 'Steps Towards Success';
       	response.stepsTowardsSuccessWrapper = createWrapperList(accountSteps, stepNumberToStep);
        response.isVisible = true;
           
        return response;
    }

    private static Response get12StepsForOpp(Opportunity opp) {
        List<Step_Towards_Success1__c> oppSteps = getStepsFromOpportunity(opp);
        Map<Integer, Step_Towards_Success1__c> stepNumberToStep = new Map<Integer, Step_Towards_Success1__c>();
        
        for (Step_Towards_Success1__c step : oppSteps) {
            //step.Achieved__c = 'No';
			stepNumberToStep.put((Integer)step.Step_Number__c, step);
        }

        Response response = new Response();
        response.title = 'Steps Towards Success';
       	response.stepsTowardsSuccessWrapper = createWrapperList(oppSteps, stepNumberToStep);
        response.isVisible = true;
           
        return response;
    }
    
    public static Response evalueteAndget12StepsForOpportunity(Opportunity opp) {
		Response response = new Response();
        response.title = 'Steps Towards Success';
       	response.stepsTowardsSuccessWrapper = getStepsWrapperListForOpportunity(opp);
       	response.isVisible = true; 

        return response;
    }
    
    public static List<StepsTowardsSuccessWrapper> getStepsWrapperListForOpportunity(Opportunity opp) {        
        List<Step_Towards_Success1__c> stepsTowardsSuccess = getStepsFromOpportunity(opp);
        
        if (stepsTowardsSuccess.size() != 12) {
           	deleteSteps(stepsTowardsSuccess);
        	stepsTowardsSuccess = createStepsForOpportunity(opp);
        }
                
        List<StepsTowardsSuccessWrapper> stepsTowardsSuccessWrapperList = verifyStepsFor12Steps(stepsTowardsSuccess, opp.Id);
        
        assignAchievedStepsQuantityToOpportunity(opp, stepsTowardsSuccessWrapperList);
        
        return stepsTowardsSuccessWrapperList;
    }
    
    private static void deleteSteps(List<Step_Towards_Success1__c> stepsTowardsSuccess) {
        delete stepsTowardsSuccess;
    }
    
    private static List<Step_Towards_Success1__c> createStepsForOpportunity(Opportunity opp){
       	Id opportunityStepRecordTypeId = Schema.SObjectType.Step_Towards_Success1__c.getRecordTypeInfosByName().get('Opportunity Step').getRecordTypeId();
        List<Step_Towards_Success1__c> stepsToInsert = new List<Step_Towards_Success1__c>();
        
        Map<Decimal, Step_Towards_Success_Definition__mdt> stepNumbeToStepTowardsSuccessDefinition = getMap12StepsTowardsSucessDefinition();
        
        for (Integer i=1; i<=12; i++) {
			Step_Towards_Success1__c step = new Step_Towards_Success1__c();
            step.Step_Number__c = i;
            step.Related_Opportunity__c = opp.Id;
            step.Name = stepNumbeToStepTowardsSuccessDefinition.get(i).MasterLabel;
        	step.RecordTypeId = opportunityStepRecordTypeId;
            step.Achieved__c = 'No';
            step.Master_Account__c = opp.AccountId;
            
            stepsToInsert.add(step);
        }
        
        insert stepsToInsert;
        
        return stepsToInsert;
    }
    
    private static List<StepsTowardsSuccessWrapper> verifyStepsFor12Steps(List<Step_Towards_Success1__c> stepsTowardsSuccess, String recordId) {
        List<StepsTowardsSuccessWrapper> listStepsTowardsSuccessWrapper = new List<StepsTowardsSuccessWrapper>();
        
        List<Step_Towards_Success1__c> stepsToUpdate = new List<Step_Towards_Success1__c>();
        
        Map<Decimal, Step_Towards_Success1__c> mapStepsTowardsSucess = new Map<Decimal, Step_Towards_Success1__c>();
        
        Map<Decimal, Step_Towards_Success_Definition__mdt> stepNumbeToStepTowardsSuccessDefinition = getMap12StepsTowardsSucessDefinition();
        
        for (Step_Towards_Success1__c step : stepsTowardsSuccess) {
			mapStepsTowardsSucess.put(step.Step_Number__c, step);
        }        

		List<Quote> quotes = [
       		SELECT 	Id, Gym__r.Name, Contact_Permission__c, Payment_method__c, Employee_Corporate_Email__c,
           		  	Expansion_100_of_the_eligible__c, New_Hires__c, HR_Communication__c, PR_in_contract__c, What_does_success_looks_like_Joint_BP__c, Exclusivity_clause__c  
            FROM QUOTE
           	WHERE OpportunityId = :recordId
            AND IsSyncing = true
     	];
        
       	List<Event> events = [
        	SELECT Id, WhoId
            FROM Event
            WHERE WhatId =: recordId
            //AND StartDateTime = LAST_N_DAYS:120
        ];
        
        List<String> cLevelRoles = new List<String>{
            'Board Member', 'CEO', 'CFO', 'CHRO', 'Director', 'Vice President', 'Other C - Level'
  		};
        
        List<String> membershipFeesNames = new List<String>{
            'Argentina',
            'Brasil Padrão',
            'Chile',
            'Deutschland',
            'España',
            'France',
            'Ireland',
            'Italia',
            'México',
            'Nederland',
            'Portugal',
            'United Kingdom',
            'United States: 7 Tier'
        };
                        
        Set<Id> eventIdSet = new Set<Id>();
        Set<Id> contactIdSet = new Set<Id>();
        
        for (Event e : events) {
            eventIdSet.add(e.Id);
		}
        
        List<EventWhoRelation> EventWhoRelationList = [
       		SELECT Id, Relation.Name, RelationId, Type 
            FROM EventWhoRelation 
            WHERE EventId IN :eventIdSet
        ];
        
        for (EventWhoRelation relation : EventWhoRelationList) {
            if (relation.Type == 'Contact') {
          		contactIdSet.add(relation.RelationId);
            }
        }
        
   		List<Contact> contacts = [
          	SELECT Id
            FROM Contact
            WHERE Id IN :contactIdSet
            AND Cargo__c IN :cLevelRoles
        ];
        
        List<QuoteLineItem> quoteLineItems = [
            SELECT Id, ListPrice, UnitPrice 
            FROM QuoteLineItem 
            WHERE Quote.Opportunity.Id = :recordId
            Order By CreatedDate DESC
        ];
        
        
        for (Integer i = 1; i <= 12; i++) {
            if (mapStepsTowardsSucess.get(i) != null) {
                
            	Step_Towards_Success1__c stepTowardsSuccess = mapStepsTowardsSucess.get(i);
                stepTowardsSuccess.Achieved__c = 'No';
                
          		StepsTowardsSuccessWrapper stepsTowardsSuccessWrapper = new StepsTowardsSuccessWrapper();
                stepsTowardsSuccessWrapper.label = (i < 10 ? '0' + i : '' + i) + ' - ' + stepNumbeToStepTowardsSuccessDefinition.get(i).MasterLabel;
           		stepsTowardsSuccessWrapper.alternativeText = stepNumbeToStepTowardsSuccessDefinition.get(i).Description__c;
                stepsTowardsSuccessWrapper.option = 'negative';
                stepsTowardsSuccessWrapper.type12Steps = stepNumbeToStepTowardsSuccessDefinition.get(i).Category__c;
                
              	if (stepTowardsSuccess.Step_Number__c == 1) {
					if (quotes.size() > 0) {
                    	if (membershipFeesNames.contains(quotes.get(0).Gym__r.Name)) {
                        	stepsTowardsSuccessWrapper.option = 'positive';
                            stepTowardsSuccess.Achieved__c = 'Yes';
						}
                   	}                 
               	}
                    
               	if (stepTowardsSuccess.Step_Number__c == 2) {
                  	if (quotes.size() > 0) {
                    	if (quotes.get(0).Expansion_100_of_the_eligible__c == true) {
                        	stepsTowardsSuccessWrapper.option = 'positive';
                            stepTowardsSuccess.Achieved__c = 'Yes';
                        }
                   	}
               	}
                    
              	if (stepTowardsSuccess.Step_Number__c == 3) {
                    if (quoteLineItems.size() > 0) {
                        if (quoteLineItems.get(0).UnitPrice >= quoteLineItems.get(0).ListPrice) {
                            stepsTowardsSuccessWrapper.option = 'positive';
                            stepTowardsSuccess.Achieved__c = 'Yes';
                        }
                    }
               	}
                    
               	if (stepTowardsSuccess.Step_Number__c == 4) {
                    if (contacts.size() > 0) {
                    	stepsTowardsSuccessWrapper.option = 'positive';
                        stepTowardsSuccess.Achieved__c = 'Yes';
                    }
               	}
                    
               	if (stepTowardsSuccess.Step_Number__c == 5) {
                	if (quotes.size() > 0) {
                    	if (quotes.get(0).Payment_method__c == 'Payroll' || quotes.get(0).Payment_method__c == 'Payroll + Credit Card') {
                        	stepsTowardsSuccessWrapper.option = 'positive';
                            stepTowardsSuccess.Achieved__c = 'Yes';
                        }
                   	}
               	}
                    
               	if (stepTowardsSuccess.Step_Number__c == 6) {
                	if (quotes.size() > 0) {
                    	if (quotes.get(0).Employee_Corporate_Email__c == true) {
                        	stepsTowardsSuccessWrapper.option = 'positive';
                            stepTowardsSuccess.Achieved__c = 'Yes';
						}
                   	}
               	}
                    
               	if (stepTowardsSuccess.Step_Number__c == 7) {
              		if (quotes.size() > 0) {
                    	if (quotes.get(0).Contact_Permission__c == 'Whitelist') {
                        	stepsTowardsSuccessWrapper.option = 'positive';
                            stepTowardsSuccess.Achieved__c = 'Yes';
						}
                  	}
              	}
                    
               	if (stepTowardsSuccess.Step_Number__c == 8) {
                    if (quotes.size() > 0) {
                    	if (quotes.get(0).New_Hires__c == true) {
                        	stepsTowardsSuccessWrapper.option = 'positive';
                            stepTowardsSuccess.Achieved__c = 'Yes';
                        }
                   	}
               	}
                    
               	if (stepTowardsSuccess.Step_Number__c == 9) {
                    if (quotes.size() > 0) {
                    	if (quotes.get(0).HR_Communication__c == true) {
                        	stepsTowardsSuccessWrapper.option = 'positive';
                            stepTowardsSuccess.Achieved__c = 'Yes';
                        }
                   	}	
                }
                    
               	if (stepTowardsSuccess.Step_Number__c == 10) {
                    if (quotes.size() > 0) {
                    	if (quotes.get(0).Exclusivity_clause__c == true) {
                        	stepsTowardsSuccessWrapper.option = 'positive';
                            stepTowardsSuccess.Achieved__c = 'Yes';
                        }
                   	}	
               	}
                    
               	if (stepTowardsSuccess.Step_Number__c == 11) {
                	if (quotes.size() > 0) {
                    	if (quotes.get(0).PR_in_contract__c == true) {
                        	stepsTowardsSuccessWrapper.option = 'positive';
                            stepTowardsSuccess.Achieved__c = 'Yes';
                        }
                   	}        
               	}
                    
               	if (stepTowardsSuccess.Step_Number__c == 12) {
                	if (quotes.size() > 0) {
                    	if (quotes.get(0).What_does_success_looks_like_Joint_BP__c == true) {
                        	stepsTowardsSuccessWrapper.option = 'positive';
                            stepTowardsSuccess.Achieved__c = 'Yes';
                        }
                   	}    	
               	}
                
                stepsToUpdate.add(stepTowardsSuccess);
                
       		listStepsTowardsSuccessWrapper.add(stepsTowardsSuccessWrapper);
     		}
    	}
        
        update stepsToUpdate;
        
        return listStepsTowardsSuccessWrapper;
    }
    
    private static Opportunity getOpportunity(String recordId) {
		Opportunity opp = [
        	SELECT 	Id, RecordType.DeveloperName, RecordType.Name, Achieved_Steps_Towards_Success_Quantity__c, AccountId
           	FROM Opportunity
          	WHERE Id =: recordId
      	];
        
        return opp;
    }
    
    private static Account getAccount(String recordId) {
		Account acc = [
        	SELECT 	Id, RecordType.Name, 
            		RecordType.DeveloperName, RecordTypeId, Owner.Profile.Name, Business_Unit__c,
            		Achieved_Steps_Towards_Success_Quantity1__c
           	FROM Account
           	WHERE Id =: recordId
      	];
        
        return acc;
    }
    
    public static Map<Decimal, Step_Towards_Success_Definition__mdt> getMap12StepsTowardsSucessDefinition() {
		Map<Decimal, Step_Towards_Success_Definition__mdt> stepNumbeToStepTowardsSuccessDefinition = new Map<Decimal, Step_Towards_Success_Definition__mdt>(); 
         Set<String> stepsCategorySet = new Set<String>{
            'Business Enabler','Enrollment Enablers'
        };
        
        List<Step_Towards_Success_Definition__mdt> listStepsTowardsSuccessDefinitions = [
            SELECT DeveloperName, MasterLabel, Category__c, Description__c, Field_API_Name__c, Field_Expected_Value__c, How_are_we_going_to_track_it__c, Object_API_Name__c, Step__c
            FROM Step_Towards_Success_Definition__mdt
            WHERE Category__c IN :stepsCategorySet
            ORDER BY Step__c ASC
        ];
        
        for (Step_Towards_Success_Definition__mdt step : listStepsTowardsSuccessDefinitions) {
            stepNumbeToStepTowardsSuccessDefinition.put((Integer)step.Step__c, step);
        }
        
        return stepNumbeToStepTowardsSuccessDefinition;
    }
    
    private static Response generateEmptyResponse() {
		List<StepsTowardsSuccessWrapper> stepsTowardsSuccessWrapperList = new List<StepsTowardsSuccessWrapper>(); 
    	
        Map<Decimal, Step_Towards_Success_Definition__mdt> stepNumbeToStepTowardsSuccessDefinition = getMap12StepsTowardsSucessDefinition();
        
        for (Integer i = 1; i <= 12; i++) {
			StepsTowardsSuccessWrapper stepsTowardsSuccessWrapper = new StepsTowardsSuccessWrapper();
           	stepsTowardsSuccessWrapper.label = (i < 10 ? '0' + i : '' + i) + ' - ' + stepNumbeToStepTowardsSuccessDefinition.get(i).MasterLabel;
           	stepsTowardsSuccessWrapper.alternativeText = stepNumbeToStepTowardsSuccessDefinition.get(i).Description__c;
           	stepsTowardsSuccessWrapper.option = 'negative';
            stepsTowardsSuccessWrapper.type12Steps = stepNumbeToStepTowardsSuccessDefinition.get(i).Category__c;
            
			stepsTowardsSuccessWrapperList.add(stepsTowardsSuccessWrapper);            
        }
        
        Response response = new Response();
        response.title = 'Steps Towards Success';
       	response.stepsTowardsSuccessWrapper = stepsTowardsSuccessWrapperList;
       	response.isVisible = true;
        
        return response;
    }
    
    private static void assignAchievedStepsQuantityToOpportunity(Opportunity opp, List<StepsTowardsSuccessWrapper> stepsTowardsSuccessWrapperList) {
    	Integer achievedStepsQuantity = 0;
        
        for (StepsTowardsSuccessWrapper wrapper : stepsTowardsSuccessWrapperList) {
            if (wrapper.option == 'positive') {
            	achievedStepsQuantity++;
            }
        }
        
        if (opp.Achieved_Steps_Towards_Success_Quantity__c != achievedStepsQuantity) {
        	opp.Achieved_Steps_Towards_Success_Quantity__c = achievedStepsQuantity;
            update opp;
        }
        
    }
    
    private static List<Step_Towards_Success1__c> getStepsFromAccount(Account acc) {
    	List<Step_Towards_Success1__c> steps = [
        	SELECT Id, Achieved__c, Step_Number__c, Name
            FROM Step_Towards_Success1__c
            WHERE Related_Account__c =: acc.Id
            ORDER BY Step_Number__c
        ];
        
        return steps;
    }
    
    private static List<Step_Towards_Success1__c> createStepsForAccount(Account acc){
        System.debug('createStepsForAccount');
       	Id accountStepRecordTypeId = Schema.SObjectType.Step_Towards_Success1__c.getRecordTypeInfosByName().get('Account Step').getRecordTypeId();
        List<Step_Towards_Success1__c> stepsToInsert = new List<Step_Towards_Success1__c>();
        
        Map<Decimal, Step_Towards_Success_Definition__mdt> stepNumbeToStepTowardsSuccessDefinition = getMap12StepsTowardsSucessDefinition();
        
        for (Integer i=1; i<=12; i++) {
			Step_Towards_Success1__c step = new Step_Towards_Success1__c();
            step.Step_Number__c = i;
            step.Related_Account__c = acc.Id;
            step.Name = stepNumbeToStepTowardsSuccessDefinition.get(i).MasterLabel;
        	step.RecordTypeId = accountStepRecordTypeId;
            step.Achieved__c = 'No';
            step.Master_Account__c  = acc.Id;
            
            stepsToInsert.add(step);
        }
        
        insert stepsToInsert;
        
        return stepsToInsert;
    }
    
    private static void assignStepForCSOpps(Map<Integer, Step_Towards_Success1__c> stepNumberToStep, Integer stepNumber, Opportunity opp) {
        if (opp.StageName == 'Lançado/Ganho') {
       		stepNumberToStep.get(stepNumber).Achieved__c = 'Yes';
       	} else {
        	stepNumberToStep.get(stepNumber).Achieved__c = 'No';
       	}
    }
    
    private static List<StepsTowardsSuccessWrapper> createWrapperList(List<Step_Towards_Success1__c> accountSteps, Map<Integer, Step_Towards_Success1__c> stepNumberToStep) {
		List<StepsTowardsSuccessWrapper> wrapperList = new List<StepsTowardsSuccessWrapper>();
        Map<Decimal, Step_Towards_Success_Definition__mdt> stepNumbeToStepTowardsSuccessDefinition = getMap12StepsTowardsSucessDefinition();
        
      	for (Integer i = 0; i < accountSteps.size(); i++) {
        	Step_Towards_Success1__c stepTowardsSuccess = accountSteps.get(i);
                
           	StepsTowardsSuccessWrapper stepsTowardsSuccessWrapper = new StepsTowardsSuccessWrapper();
           	stepsTowardsSuccessWrapper.label = (i+1 < 10 ? '0' + (i+1) : '' + (i+1)) + ' - ' + stepNumbeToStepTowardsSuccessDefinition.get(i+1).MasterLabel;
           	stepsTowardsSuccessWrapper.alternativeText = stepNumbeToStepTowardsSuccessDefinition.get(i+1).Description__c;
           	stepsTowardsSuccessWrapper.option = (stepTowardsSuccess.Achieved__c == 'Yes' ? 'positive' : 'negative');
           	stepsTowardsSuccessWrapper.type12Steps = stepNumbeToStepTowardsSuccessDefinition.get(i+1).Category__c;
                    
          	wrapperList.add(stepsTowardsSuccessWrapper);    
        }
        
        
        return wrapperList;
    }
    
    private static void assignStepsFromNewBussinessOpp(List<Step_Towards_Success1__c> accountSteps, List<Step_Towards_Success1__c> oppSteps) {
        if (accountSteps.size() == 12 && oppSteps.size() == 12) {
            for (Integer i=0; i<12; i++) {
           		Step_Towards_Success1__c accountStep = accountSteps.get(i);
                accountStep.Achieved__c = oppSteps.get(i).Achieved__c;
            }
        }
    }
    
    private static List<Step_Towards_Success1__c> getStepsFromOpportunity(Opportunity opp) {
    	Id opportunityStepRecordTypeId = Schema.SObjectType.Step_Towards_Success1__c.getRecordTypeInfosByName().get('Opportunity Step').getRecordTypeId();
        
		List<Step_Towards_Success1__c> steps = [
        	SELECT Id, Step_Number__c, RecordTypeId, Achieved__c, Date_Achieved__c, Name, Master_Account__c
            FROM Step_Towards_Success1__c
            WHERE RecordTypeId =: opportunityStepRecordTypeId 
            AND Related_Opportunity__c =: opp.Id
            ORDER BY Step_Number__c
        ];
        
        return steps;
    }
    
    public class StepsTowardsSuccessWrapper {
        @AuraEnabled public String label {get; set;}
        @AuraEnabled public String option {get; set;}
        @AuraEnabled public String alternativeText {get; set;}
        @AuraEnabled public String type12Steps {get; set;}
    }
    
    public class Response {
		@AuraEnabled public String title {get; set;}
        @AuraEnabled public List<StepsTowardsSuccessWrapper> stepsTowardsSuccessWrapper {get; set;}
        @AuraEnabled public Boolean isVisible {get; set;}
    }
}