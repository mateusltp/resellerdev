@isTest(seeAllData=false)
public class ComissionPriceRepositoryTest {
    
    @TestSetup
    static void createData(){    
        
        Product2 productBRL = DataFactory.newProduct('teste', false, 'BRL');
        Database.insert(productBRL);
        
        Comission_Price__c comission = DataFactory.newComissionPrice(0, 30, productBRL.id, 'BRL');
        Database.insert(comission);        
        
    }
    
    
    @isTest
    static void byProductIdAndCurrencyIsoCodeTest(){
        Product2 product = [select Id,Name from Product2 where name = 'teste' limit 1];
        ComissionPriceRepository comissionPriceRepository = new ComissionPriceRepository();
        List<Comission_Price__c> listComission = null;
        try{
            listComission = comissionPriceRepository.byProductIdAndCurrencyIsoCode(product.id, 'BRL');
        }catch(System.QueryException e){
            System.assert(false);
        }
        System.assertEquals(listComission.size(),0);
    }
    
}