/**
 * @description       : 
 * @author            : gft.jorge.stevaux@ext.gympass.com
 * @group             : 
 * @last modified on  : 08-12-2021
 * @last modified by  : gft.jorge.stevaux@ext.gympass.com
**/

@isTest(seeAllData=false)
private class ProductItemSelectorTest {

    @TestSetup
    static void makeData(){

        Account lAcc = PartnerDataFactory.newAccount();              
        Database.insert( lAcc );

        Contact lContact = PartnerDataFactory.newContact(lAcc.Id);
        Database.insert(lContact);

        Bank_Account__c lBankAcct = PartnerDataFactory.newBankAcct();
        Database.insert(lBankAcct);

        Acount_Bank_Account_Relationship__c lBankAcctRel = PartnerDataFactory.newBankAcctRel(lAcc.Id, lBankAcct);
        Database.insert(lBankAcctRel);
                                  
        Opportunity lOpp = PartnerDataFactory.newOpportunity( lAcc.Id, 'Partner_Wishlist_Renegotiation' );     
        Database.insert( lOpp );

        Product_Item__c lParentProduct = PartnerDataFactory.newProduct(lOpp);
        Database.insert( lParentProduct );

        Product_Item__c lProduct = PartnerDataFactory.newProduct(lOpp);
        lProduct.Parent_Product__c = lParentProduct.Id;
        lProduct.Name = 'test product abcde';
        Database.insert( lProduct );
        
        Gym_Activity__c lGymActivity = PartnerDataFactory.newGymActivity(lProduct.Id);
        Database.insert(lGymActivity);
        
    }

    @isTest 
    static void getSObjectFieldList_Test(){

        List<Schema.SObjectField> schSObjLst = new List<Schema.SObjectField> {
			Product_Item__c.Id,
			Product_Item__c.Name
		};

        Test.startTest();
        System.assertEquals(schSObjLst, new ProductItemSelector().getSObjectFieldList());
        Test.stopTest();
    }

    @isTest 
    static void getSObjectType_Test(){

        Test.startTest();
        System.assertEquals(Product_Item__c.sObjectType, new ProductItemSelector().getSObjectType());
        Test.stopTest();
    }

    @isTest 
    static void selectById_Test(){

        List<Product_Item__c> prodLst = [SELECT Id, Name FROM Product_Item__c LIMIT 1];
        Set<Id> prodIds = (new Map<Id, Product_Item__c>(prodLst)).keySet();

        Test.startTest();
        system.assertEquals(prodLst.get(0).id, new ProductItemSelector().selectById(prodIds).get(0).id);
        Test.stopTest();
    }
    
    @isTest
    static void selectByOppIdToClone_Test(){

        List<Opportunity> oppLst = [SELECT Id, Name FROM Opportunity LIMIT 1];
        Set<Id> oppIds = (new Map<Id, Opportunity>(oppLst)).keySet();
        List<Product_Item__c> prodLst = [SELECT Id, Name FROM Product_Item__c WHERE Opportunity__c IN : oppIds LIMIT 1];

        Test.startTest();
        System.assertEquals(prodLst.get(0).id, new ProductItemSelector().selectByOppIdToClone(oppIds).get(0).id);
        Test.stopTest();
    }

    @isTest
    static void selectByIdToClone_Test(){
        Map<Id, Product_Item__c> productItemsByIds = new Map<Id, Product_Item__c>([SELECT Id FROM Product_Item__c]);

        Test.startTest();
        System.assertEquals(productItemsByIds.size(), new ProductItemSelector().selectByIdToClone(productItemsByIds.keySet()).size());
        Test.stopTest();
    }

    @isTest
    static void selectAllByParentId_Test(){
        Id parentProductId = [SELECT Id FROM Product_Item__c WHERE Parent_Product__c = null LIMIT 1].Id;
        List<Product_Item__c> childProducts = [SELECT Id FROM Product_Item__c WHERE Parent_Product__c = :parentProductId];

        Test.startTest();
        System.assertEquals(childProducts.size(), new ProductItemSelector().selectAllByParentId(new Set<Id> { parentProductId }).size());
        Test.stopTest();
    }

    @isTest
    static void selectProductByParentId_Test(){
        Id parentProductId = [SELECT Id FROM Product_Item__c WHERE Parent_Product__c = null LIMIT 1].Id;
        List<Product_Item__c> childProducts = [SELECT Id FROM Product_Item__c WHERE Parent_Product__c = :parentProductId];

        Test.startTest();
        System.assertEquals(childProducts.size(), new ProductItemSelector().selectProductByParentID(new Set<Id> { parentProductId }).size());
        Test.stopTest();
    }

    @isTest
    static void selectProductForSetup_Test(){

        Id oppId = [SELECT Id, Name FROM Opportunity LIMIT 1].Id;
        List<Product_Item__c> prodLst = [SELECT Id, Name FROM Product_Item__c WHERE Opportunity__c = :oppId LIMIT 1];

        Test.startTest();
        System.assertEquals(prodLst.get(0).id, new ProductItemSelector().selectProductForSetUp(oppId).get(0).id);
        Test.stopTest();
    }

    @isTest
    static void selectProductByIdAsMap_Test(){

        List<Product_Item__c> prodLst = [SELECT Id, Name FROM Product_Item__c LIMIT 1];
        Set<Id> prodIds = (new Map<Id, Product_Item__c>(prodLst)).keySet();

        Test.startTest();
        system.assertEquals(prodLst.get(0).id, new ProductItemSelector().selectProductByIdAsMap(prodIds).get(prodLst[0].Id).id);
        Test.stopTest();
    }

    @isTest
    static void selectProductByIdToClone_Test(){

        List<Product_Item__c> prodLst = [SELECT Id, Name FROM Product_Item__c LIMIT 1];
        Set<Id> prodIds = (new Map<Id, Product_Item__c>(prodLst)).keySet();

        Test.startTest();
        system.assertEquals(prodLst.get(0).id, new ProductItemSelector().selectProductByIdToClone(prodIds).get(prodLst[0].Id).id);
        Test.stopTest();
    }
}