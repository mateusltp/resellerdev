/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 09-21-2021
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/
public with sharing class OrderSelector extends ApplicationSelector {

    public List<Schema.SObjectField> getSObjectFieldList() {
		return new List<Schema.SObjectField> {
			Order.Id
		};
	}

	public Schema.SObjectType getSObjectType() {
		return Order.sObjectType;
	}

    public List<Order> selectById(Set<Id> ids) {
		return (List<Order>) super.selectSObjectsById(ids);
	}

    /** tags: TAGUS, Partners
	*/
	public List<Order> selectOrderFieldsToTagusParse ( List<Order> aOrderLst ){

		return ( (List<Order>) Database.query( 	newQueryFactory(false).
                                                        setEnforceFLS(true).
													    selectField(Order.Id).
														selectField(Order.UUID__c).			
														selectField(Order.OpportunityId).		
														selectField(Order.AccountId).	
														selectField(Order.QuoteId).		
														selectField(Order.Type).		
														selectField(Order.CurrencyIsoCode).	
														selectField(Order.EffectiveDate).	
														selectField(Order.EndDate).	
														selectField(Order.Status).	
														selectField('Quote.Signed__c').	
														selectField('Quote.UUID__c').	
														selectField('Quote.Order_Item_UUID__c').
														selectField('Opportunity.cancellation_date__c').
														selectField('Opportunity.UUID__c').																											
														selectField('Opportunity.cancellation_Reason__c').														
														selectField('Opportunity.Cancellation_Reason_subcategory__c').
														selectField('Account.UUID__c').
														selectField('Account.CurrencyIsoCode').		
														selectField('Account.BillingCountry').																
														selectField('Account.BillingCountryCode').																												
														selectField('Account.Legal_Title__c').													
													    setCondition('Id in : aOrderLst').
                                                        toSOQL()
												));
	}


}