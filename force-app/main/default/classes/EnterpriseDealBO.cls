public without sharing class EnterpriseDealBO {
  private Opportunity opportunity;
  private Quote quote;
  private List<Account_Opportunity_Relationship__c> splitBillings;
  private List<SalesItem> salesItems;

  public EnterpriseDealBO(Opportunity opportunity, Quote quote) {
    this.opportunity = opportunity;
    this.quote = quote;

    this.splitBillings = new List<Account_Opportunity_Relationship__c>();
    this.salesItems = new List<SalesItem>();
  }

  public Opportunity getOpportunity() {
    return this.opportunity;
  }

  public Quote getQuote() {
    return this.quote;
  }

  public List<Account_Opportunity_Relationship__c> getSplitBillings() {
    return this.splitBillings;
  }

  public void setSplitBilling(
    List<Account_Opportunity_Relationship__c> splitBillings
  ) {
    this.splitBillings = splitBillings;
  }

  public List<SalesItem> getSalesItems() {
    return this.salesItems;
  }

  public void addSalesItem(SalesItem salesItem) {
    this.salesItems.add(salesItem);
  }

  public class SalesItem {
    private QuoteLineItem quoteLineItem;
    private List<PaymentItem> paymentItems;

    public SalesItem(QuoteLineItem quoteLineItem) {
      this.quoteLineItem = quoteLineItem;
      this.paymentItems = new List<PaymentItem>();
    }

    public QuoteLineItem getQuoteLineItem() {
      return this.quoteLineItem;
    }

    public List<PaymentItem> getPaymentItems() {
      return this.paymentItems;
    }

    public void addPaymentItem(
      Payment__c payment,
      List<Eligibility__c> eligibilities,
      List<Waiver__c> waivers
    ) {
      paymentItems.add(new PaymentItem(payment, eligibilities, waivers));
    }
  }

  public class PaymentItem {
    private Payment__c payment;
    private List<Eligibility__c> eligibilities;
    private List<Waiver__c> waivers;

    private PaymentItem(
      Payment__c payment,
      List<Eligibility__c> eligibilities,
      List<Waiver__c> waivers
    ) {
      this.payment = payment;
      this.eligibilities = eligibilities;
      this.waivers = waivers;
    }

    public Payment__c getPayment() {
      return this.payment;
    }

    public List<Eligibility__c> getEligibilities() {
      return this.eligibilities;
    }

    public List<Waiver__c> getWaivers() {
      return this.waivers;
    }
  }
}