public class DoesNothingClass {
    // This class and function are only to be called from a Process Builder in order to make the node stop execution 
    // this workaround is needed because it is mandatory to have at least 1 immediate action on the process builder node.
    @InvocableMethod (label='Does Nothing Method') 
    public static void doesNothingMethod() {
        system.debug('##### doesNothingMethod #####');
    }
}