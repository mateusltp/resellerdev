public without sharing class TagusTransaction {
  private String id;
  private String name;
  private String salesforce_id;
  private TransactionType type;
  private String source;
  private Datetime timestamp;

  public TagusTransaction(EventQueue event, TransactionType type) {
    this.id = new Uuid().getValue();
    this.name = event.getEventName();
    this.salesforce_id = event.getEventId();
    this.type = type;
    this.source = event.get().sender__c;
    this.timestamp = Datetime.now();
  }

  public String getId() {
    return this.id;
  }

  public String getName() {
    return this.name;
  }

  public String getSalesforceId() {
    return this.salesforce_id;
  }

  public TransactionType getType() {
    return this.type;
  }

  public String getSource() {
    return this.source;
  }

  public Datetime getTimestamp() {
    return this.timestamp;
  }

  public enum TransactionType {
    GET,
    CREATED,
    UPDATED,
    DELETED
  }
}