/**
 * @author vinicius.ferraz
 */
trigger QuoteLineItemTrigger on QuoteLineItem (before insert, after insert, before update, after update, before delete, after delete) {
    new QuoteLineItemTriggerHandler().run();
}