/**
 * @description       : 
 * @author            : bruno.mendes@gympass.com
 * @group             : 
 * @last modified on  : 25/05/2022
 * @last modified by  : bruno.mendes@gympass.com
**/

trigger PartnerEventTrigger on Partner_Event__e (after insert) {
    new PartnerEventTriggerHandler().run();
}