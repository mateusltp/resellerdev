trigger ResellerLogErrorTrigger on Reseller_Log_Error__e (after insert) {


    List<Reseller_Log__c> logs = new List<Reseller_Log__c>(); 

    for(Reseller_Log_Error__e event : Trigger.new){
        logs.add(new Reseller_Log__c(   Action__c = event.Action__c, 
                                        Error_Message__c = event.Error_Message__c,
                                        Error_Tracking__c = event.Error_Tracking__c,
                                        Error_Type__c = event.Error_Type__c,
                                        Line_Number__c = event.Line_Number__c,
                                        Reference_Id__c = event.Reference_Id__c,
                                        Reference_SObject__c = event.Reference_SObject__c,
                                        User__c = event.User_Id__c));
    }

    if(!logs.isEmpty())insert logs;
}