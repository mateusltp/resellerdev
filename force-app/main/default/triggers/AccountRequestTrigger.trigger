trigger AccountRequestTrigger on Account_Request__c (before insert, after insert, before update, after update) {
    new AccountRequestHandler().run();
}