/**
 * @File Name          : LeadTrigger.trigger
 * @Description        : 
 * @Author             : MLNC@GFT.com
 * @Group              : 
 * @Last Modified By   : MLNC@GFT.com
 * @Last Modified On   : 27/04/2020 15:41:00
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    27/04/2020   MLNC@GFT.com     Initial Version
**/
trigger LeadTrigger on Lead (before insert, after insert, before update, after update, before delete, after delete) {
    new LeadTriggerHandler().run();
}