/**
 * @File Name          : AccountTrigger.trigger
 * @Description        : 
 * @Author             : GEPI@GFT.com
 * @Group              : 
 * @Last Modified By   : GEPI@GFT.com
 * @Last Modified On   : 02/04/2020 18:06:11
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    02/04/2020   GEPI@GFT.com     Initial Version
**/
trigger AccountTrigger on Account (before insert, after insert, before update, after update) {
    new AccountTriggerHandler().run();
}