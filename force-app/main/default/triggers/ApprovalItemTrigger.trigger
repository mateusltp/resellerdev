trigger ApprovalItemTrigger on Approval_Item__c (before insert, after insert, before update, after update) {
    new ApprovalItemTriggerHandler().run();
}