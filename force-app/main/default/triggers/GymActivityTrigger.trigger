trigger GymActivityTrigger on Gym_Activity__c (before insert, after insert, before update, after update, before delete, after delete) {
    new GymActivityTriggerHandler().run();
}