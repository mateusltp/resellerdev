trigger MassiveRequestTrigger on Massive_Account_Request__c (before insert, after insert, before update, after update) {
    new MassiveRequestHandler().run();
}