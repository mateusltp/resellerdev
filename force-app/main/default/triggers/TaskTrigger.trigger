/**
 * @description       : 
 * @author            : roei@gft.com
 * @group             : 
 * @last modified on  : 02-17-2021
 * @last modified by  : roei@gft.com
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   02-17-2021   roei@gft.com   Initial Version
**/
trigger TaskTrigger on Task (before insert, after insert, before update, after update, before delete, after delete, after undelete) {
    new TaskTriggerHandler().run();
}