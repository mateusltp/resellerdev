trigger ProductAssignmentTrigger on Product_Assignment__c (after insert, after update, before delete) {
    new ProductAssignmentTriggerHandler().run();
}