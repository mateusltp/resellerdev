trigger ContentDocumentLinkTriggerForDocument on ContentDocumentLink (after insert, after update) {
	new ContentDocumentLinkTriggerHandlerNew().run();
}