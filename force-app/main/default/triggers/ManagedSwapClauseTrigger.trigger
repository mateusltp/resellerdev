trigger ManagedSwapClauseTrigger on ManagedSwapClause__c (after insert, after update, after delete, before delete) {
    new ManagedSwapClauseTriggerHandler().run();
}