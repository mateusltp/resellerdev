/**
 * @description       : 
 * @author            : gft.samuel.silva@ext.gympass.com
 * @group             : 
 * @last modified on  : 04-06-2022
 * @last modified by  : gft.samuel.silva@ext.gympass.com
**/
trigger ContentDocumentLinkTriggerAfter on ContentDocumentLink (after insert, after update) {
  new ContentDocumentLinkTriggerHandler().run();
}