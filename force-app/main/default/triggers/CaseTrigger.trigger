/**
 * @description       : 
 * @author            : Samuel Silva - GFT (slml@gft.com)
 * @group             : 
 * @last modified on  : 12-07-2020
 * @last modified by  : Samuel Silva - GFT (slml@gft.com)
 * Modifications Log 
 * Ver   Date         Author                              Modification
 * 1.0   12-07-2020   Samuel Silva - GFT (slml@gft.com)   Initial Version
**/
trigger CaseTrigger on Case (before insert, after insert, before update, after update, before delete, after delete) {
    new CaseTriggerHandler().run();
}