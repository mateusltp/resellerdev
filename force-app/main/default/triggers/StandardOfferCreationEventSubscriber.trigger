trigger StandardOfferCreationEventSubscriber on Clients_Standard_Offer_Creation__e (after insert) {
    String oppId=null;
    for (Clients_Standard_Offer_Creation__e event : Trigger.New) {
        oppId = event.OpportunityId__c;
    }
    Account acc = null;
    try{
        acc = [ select Id,Account.NumberOfEmployees,Account.Id from Opportunity where Id = :oppId limit 1].Account; 
    }catch(System.QueryException e){
        return;
    }
    FastTrackProposalCreationTO to = new FastTrackProposalCreationBuilder()
        .withOpportunity(oppId)
        .withLastCreatedQuoteWhenExists(oppId)
        .withDealHierarchy(acc.NumberOfEmployees, new List<Id>{acc.Id})
        .build();
    
    FastTrackProposalCreation fastTrackProposalCreation = new FastTrackProposalCreation();
    try{
        fastTrackProposalCreation.save(to); 
    }catch(Exception e){
        //  do nothing, will be generated in fast track offer creation 
    }    
}