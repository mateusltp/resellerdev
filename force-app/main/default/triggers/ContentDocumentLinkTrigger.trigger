/**
 * @File Name          : ContentDocumentLinkTrigger.trigger
 * @Description        : 
 * @Author             : GEPI@GFT.com
 * @Group              : 
 * @Last Modified By   : gft.samuel.silva@ext.gympass.com
 * @Last Modified On   : 04-20-2022
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    06/07   DDMA@GFT.com     Initial Version
**/
trigger ContentDocumentLinkTrigger on ContentDocumentLink (before insert,after insert, before delete, after delete) {
    new ContentDocumentTriggerHandler().run();
}