trigger Sync_with_CC on APXT_Redlining__Contract_Agreement__c (after insert, after update, after delete, after undelete) {
	APXT_Redlining.PlatformDataService.sendData(Trigger.old, Trigger.new);
}