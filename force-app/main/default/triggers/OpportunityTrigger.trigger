/**
 * @File Name          : OpportunityTrigger.trigger
 * @Description        :
 * @Author             : GEPI@GFT.com
 * @Group              :
 * @Last Modified By   : GEPI@GFT.com
 * @Last Modified On   : 15/04/2020 12:21:01
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    15/04/2020   GEPI@GFT.com     Initial Version
 **/
trigger OpportunityTrigger on Opportunity(
  before insert,
  after insert,
  before update,
  after update,
  before delete,
  after delete
) {
  if (Limits.getQueries() < 50)
    new OpportunityTriggerHandler().run();
}