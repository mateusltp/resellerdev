trigger ManagedClauseTrigger on ManagedClause__c (before insert, before update) {
    new ManagedClauseTriggerHandler().run();
}