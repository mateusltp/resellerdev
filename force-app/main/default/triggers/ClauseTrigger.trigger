trigger ClauseTrigger on Clause__c (before insert, before update) {
    new ClauseTriggerHandler().run();
}