trigger SubManagedClausesTrigger on SubManagedClauses__c (before insert, before update) {
    new SubManagedClausesTriggerHandler().run();
}