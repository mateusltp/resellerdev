/**
 * @File Name          : ProductItemTrigger.trigger
 * @Description        : 
 * @Author             : GEPI@GFT.com
 * @Group              : 
 * @Last Modified By   : alysson.mota@gympass.com
 * @Last Modified On   : 03-08-2022
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    18/05/2020   GEPI@GFT.com     Initial Version
**/
trigger ProductItemTrigger on Product_Item__c (before insert, after insert, before update, after update, before delete) {
    new ProductItemTriggerHandler().run();
}