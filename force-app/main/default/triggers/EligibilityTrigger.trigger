/**
 * @author vinicius.ferraz
 */
trigger EligibilityTrigger on Eligibility__c (before insert, after insert, before update, after update, before delete, after delete) {
    new EligibilityTriggerHandler().run();
}