/**
 * @File Name          : ContentDocumentTrigger.trigger
 * @Description        : 
 * @Author             : GEPI@GFT.com
 * @Group              : 
 * @Last Modified By   : GEPI@GFT.com
 * @Last Modified On   : 07-09-2020
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    06/07   DDMA@GFT.com     Initial Version
**/
trigger ContentDocumentTrigger on ContentDocument (before delete, before update) {
    new ContentDocumentFileTriggerHandler().run();
}