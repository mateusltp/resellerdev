/**
 * @File Name          : BankAccountRelationTrigger.trigger
 * @Description        : 
 * @Author             : GEPI@GFT.com
 * @Group              : 
 * @Last Modified By   : GEPI@GFT.com
 * @Last Modified On   : 20/05/2020 17:20:49
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    20/05/2020   GEPI@GFT.com     Initial Version
**/
trigger BankAccountRelationTrigger on Acount_Bank_Account_Relationship__c (before insert, after insert, before update, after update) {
    new BankAccountRelationTriggerHandler().run();
}