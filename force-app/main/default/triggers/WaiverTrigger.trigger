/**
 * @author vinicius.ferraz
 */
trigger WaiverTrigger on Waiver__c (before insert, after insert, before update, after update, before delete, after delete) {
    new WaiverTriggerHandler().run();
}