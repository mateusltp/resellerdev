/*
* @Author Bruno Pinho
* @Date December/2018
* @Description Trigger to handle all Event needs
*/

trigger EventTrigger on Event (before insert, after insert, before update, after update, before delete, after delete, after undelete)
{
    TriggerFactory.createTriggerDispatcher(Event.sObjectType);

    new EventTriggerHandler().run();
}