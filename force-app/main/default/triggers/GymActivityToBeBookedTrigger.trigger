trigger GymActivityToBeBookedTrigger on Gym_Activity_To_Be_Booked__c (before insert, after insert, before update, after update, before delete, after delete) {
    new GymActivityToBeBookedTriggerHandler().run();
}