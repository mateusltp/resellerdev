/**
 * @author vinicius.ferraz
 */
trigger PaymentTrigger on Payment__c (before insert, after insert, before update, after update, before delete, after delete) {
    new PaymentTriggerHandler().run();
}