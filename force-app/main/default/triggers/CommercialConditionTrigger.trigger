trigger CommercialConditionTrigger on Commercial_Condition__c (after insert, after update, after delete) {
    new CommercialConditionTriggerHandler().run();
}