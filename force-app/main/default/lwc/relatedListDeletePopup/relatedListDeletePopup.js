/* eslint-disable no-console */
import { LightningElement , api} from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { deleteRecord } from 'lightning/uiRecordApi';
import deleteMessage from '@salesforce/label/c.deleteMessage';
import deleteTitle from '@salesforce/label/c.deleteTitle';
import removedMessage from '@salesforce/label/c.removedMessage';
import genericErrorMessage from '@salesforce/label/c.genericErrorMessage';
 
export default class RelatedListDeletePopup extends LightningElement {
    showModal = false;
    spinner = false;
    @api sobjectLabel;
    @api recordId;

    @api show() {
        this.showModal = true;
    }

    @api hide() {
        this.showModal = false;
    }

    handleClose() {
        this.showModal = false;     
    }
    handleDialogClose(){
        this.handleClose();
    }

    get body(){
        return deleteMessage;
    }

    get header(){
        return deleteTitle;
    }    

    handleDelete(){
        this.showSpinner();
        deleteRecord(this.recordId)
            .then(() => {
                this.hideSpinner();
                const evt = new ShowToastEvent({
                    title: removedMessage,
                    variant: "success"
                });
                this.dispatchEvent(evt);
                this.dispatchEvent(new CustomEvent("refreshdata"));
                this.hide();
            }).catch(error => {
                this.hideSpinner();
                const evt = new ShowToastEvent({
                    title: 'Error deleting record',
                    message: genericErrorMessage,
                    variant: 'error'
                })
                this.dispatchEvent(evt);
                this.hide();
            });
    }
    
    showSpinner(){
        this.spinner = true;
    }

    hideSpinner(){
        this.spinner = false;
    }
}