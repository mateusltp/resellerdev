import { LightningElement, track, api, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getAccountHierarchy from '@salesforce/apex/SplitBillingController.getAccountHierarchy';
import saveAccountWithOpportunity from '@salesforce/apex/SplitBillingController.saveAccountWithOpportunity';
import removeAccountFromOpportunity from '@salesforce/apex/SplitBillingController.removeAccountFromOpportunity';
import OneTimeFees from '@salesforce/apex/SplitBillingController.OneTimeFees';
import RecurringFees from '@salesforce/apex/SplitBillingController.RecurringFees';
import SystemModstamp from '@salesforce/schema/Account.SystemModstamp';

export default class ResellerFastTrackSplitBilling extends LightningElement {
    
    @api recordId;
    @track error;

    @track getOneTimeFees;
    @track OneTimeFeesBoolean;

    @track getRecurringFee;
    @track RecurringFeeBoolean;

    @track isEdited = false;

    @track data = [];
    @track initialData = [];
    @track selectedAccs = []; //Filter
    @track accountMap =[];
    @track hierarchy=[];
    @track payment=[];
    @track dataTemporarySplit = [];
    @track temporaryListHierarchy = [];
    @track temporaryListToRemove = [];

    connectedCallback() {
        this.handlerGetAccountHierarchy();
        this.OneTimeFees();
        this.RecurringFees();
    }

    handlerGetAccountHierarchy(){
        getAccountHierarchy({ oppId: this.recordId})
        .then(result => {
            for(let key in result){
                this.accountMap = [...this.accountMap, { key: key, keyValue: JSON.stringify(result[key]) }];
                this.hierarchy = JSON.parse(JSON.stringify(result['allPaymInfoInHierarchyList']));
                this.payment = JSON.parse(JSON.stringify(result['allPaymInfoWithPaymentList']));
            }
            for(var i=0; i<this.hierarchy.length; i++){
                this.temporaryListHierarchy.push({
                    AccountId: this.hierarchy[i].AccountId,
                    AccountName: this.hierarchy[i].AccountName,
                    AccountBillingCity: this.hierarchy[i].AccountBillingCity,
                    AccountBillingState: this.hierarchy[i].AccountBillingState,
                    AccountBillingCountry: this.hierarchy[i].AccountBillingCountry,
                    AccountLegalName: this.hierarchy[i].AccountLegalName, 
                    AccountLegalNumber: this.hierarchy[i]. AccountLegalNumber,
                    recurringFeePercentage: this.hierarchy[i].recurringFeePercentage,
                    nonRecurringFeePercentage: this.hierarchy[i].nonRecurringFeePercentage,
                    isSelected: false,
                })
                this.data.push({
                    Id: this.hierarchy[i].AccountId,
                    Name: this.hierarchy[i].AccountName,
                    BillingCity: this.hierarchy[i].AccountBillingCity,
                    BillingState: this.hierarchy[i].AccountBillingState,
                    BillingCountry: this.hierarchy[i].AccountBillingCountry,
                    LegalName: this.hierarchy[i].AccountLegalName, 
                    LegalNumber: this.hierarchy[i]. AccountLegalNumber,
                    RecurringFees: this.hierarchy[i].recurringFeePercentage,
                    OneTimeFees: this.hierarchy[i].nonRecurringFeePercentage,
                    isSelected: false,
                })
                this.initialData.push({
                    Id: this.hierarchy[i].AccountId,
                    Name: this.hierarchy[i].AccountName,
                    BillingCity: this.hierarchy[i].AccountBillingCity,
                    BillingState: this.hierarchy[i].AccountBillingState,
                    BillingCountry: this.hierarchy[i].AccountBillingCountry,
                    LegalName: this.hierarchy[i].AccountLegalName, 
                    LegalNumber: this.hierarchy[i]. AccountLegalNumber,
                    RecurringFees: this.hierarchy[i].recurringFeePercentage,
                    OneTimeFees: this.hierarchy[i].nonRecurringFeePercentage,
                    isSelected: false,
                })
            }

            for(var i=0; i<this.payment.length; i++){
                this.dataTemporarySplit.push({
                    AccountId: this.payment[i].AccountId,
                    AccountName: this.payment[i].AccountName,
                    AccountBillingCity: this.payment[i].AccountBillingCity,
                    AccountBillingState: this.payment[i].AccountBillingState,
                    AccountBillingCountry: this.payment[i].AccountBillingCountry,
                    AccountLegalName: this.payment[i].AccountLegalName, 
                    AccountLegalNumber: this.payment[i]. AccountLegalNumber,
                    recurringFeePercentage: this.payment[i].recurringFeePercentage,
                    nonRecurringFeePercentage: this.payment[i].nonRecurringFeePercentage,
                    isSelected: false,
                    
                })
            }
        })
        .catch(error => {
            this.showToast('Error', '' + error, 'error');
        });
    }

    allSelected(event) {
        let selectedRows = this.template.querySelectorAll('lightning-input');
        
        for(let i = 0; i < selectedRows.length; i++) {
            if(selectedRows[i].type === 'checkbox') {
                selectedRows[i].checked = event.target.checked;
            }
        }
    }

    OneTimeFees(){
        OneTimeFees({ oppId: this.recordId})
        .then(result =>{
            
            this.getOneTimeFees = result;
            console.log ('this.getOneTimeFees : ' + this.getOneTimeFees);
            
            if (this.getOneTimeFees === true){
                this.OneTimeFeesBoolean = true;
                console.log ('this.OneTimeFeesBoolean : ' + this.OneTimeFeesBoolean); 
            }else{
                this.OneTimeFeesBoolean = false;
                console.log ('this.OneTimeFeesBoolean : ' + this.OneTimeFeesBoolean); 
            }

        }).catch(error =>{
            console.log ('error getOneTimeFees ' + error);
        });
    }

    RecurringFees(){
        RecurringFees({ oppId: this.recordId})
        .then(result =>{
            
            this.getRecurringFee = result;
            console.log ('RecurringFeeBoolean : ' + this.getRecurringFee);
                
            if (this.getRecurringFee === true){
                this.RecurringFeeBoolean = true;
                console.log ('RecurringFeeBoolean : ' + this.RecurringFeeBoolean);
            }else{
                this.RecurringFeeBoolean = false; 
                console.log ('RecurringFeeBoolean : ' + this.RecurringFeeBoolean); 
            }

        }).catch(error =>{
            console.log ('error getRecurringFee ' + error);
        });
    }

    handlefilterChange( event ) {  
          
        const searchKey = event.target.value.toLowerCase();  
        this.selectedAccs = [];
 
        if (searchKey) {  
            this.data = this.initialData;
 
             if ( this.temporaryListHierarchy ) {

                let recs = [];

                for ( let rec of this.temporaryListHierarchy ) {
                    this.selectedAccs.push({
                       Name: rec.Name,
                       BillingCountry: rec.BillingCountry,
                       BillingCity: rec.BillingCity,
                       BillingState: rec.BillingState
                        })
                    
                }
                for ( let rec of this.selectedAccs ) {

                    let valuesArray = Object.values( rec );
 
                    for ( let val of valuesArray ) {
                    
                        if ( val ) {

                            if ( val.toLowerCase().includes( searchKey ) ) {

                                recs.push( rec );
                                break;
                        
                            }

                        }
                    }
                    
                }
               this.temporaryListHierarchy = recs;

             }
 
        }  else {

            this.temporaryListHierarchy = this.initialData;

        }
 
    }  

    onDoubleClickEdit(){
        this.isEdited = true;
    }

    handleCancel() {
        this.isEdited = false;
    }

    handleAllChange(event) {
        for (var i = 0; i < this.temporaryListHierarchy.length; i++) {
            this.temporaryListHierarchy[i].isSelected = event.target.checked;
        }
    }

    handleCheckChange(event) {
        var selectedRow = event.currentTarget;
        var key = selectedRow.dataset.id;
        this.temporaryListHierarchy[key].isSelected = event.target.checked;
    }

    getSelectedAccounts(event) {
        var delAccId;
        for (var i = this.temporaryListHierarchy.length-1; i >= 0; i--) {
            if (this.temporaryListHierarchy[i].isSelected) {
                this.addAccount(this.dataTemporarySplit, this.temporaryListHierarchy[i])
                delAccId = this.temporaryListHierarchy[i].Id;
                this.temporaryListHierarchy.splice(i,1);
            }
        }

        this.checkFees();
    }

    addAccount(list, info){
        list.push({
            AccountId: info.AccountId,
            AccountName: info.AccountName,
            AccountBillingCity: info.AccountBillingCity,
            AccountBillingState: info.AccountBillingState,
            AccountBillingCountry: info.AccountBillingCountry,
            AccountLegalName: info.AccountLegalName,
            AccountLegalNumber: info.AccountLegalNumber,
            recurringFeePercentage: 0,
            nonRecurringFeePercentage: 0,
            isSelected: false,
            
        })
    }

    remove(event) {
        var selectedRow = event.currentTarget;
        var key = selectedRow.dataset.id;

        this.addAccount(this.temporaryListHierarchy, this.dataTemporarySplit[key]);
        this.addAccount(this.temporaryListToRemove,this.dataTemporarySplit[key]);

        this.dataTemporarySplit.splice(key, 1);

        removeAccountFromOpportunity({oppId : this.recordId, PaymentInfoList : this.temporaryListToRemove})
        .then(() => {
            this.showToast('Success', 'Records saved succesfully!', 'success');
            this.isEdited = false;
            this.error = undefined;
        })
        .catch(error => {
            this.showToast('Error in Save', '' + error, 'error');
            this.isEdited = false;
            this.error = error;
        })

        this.checkFees();
    }

    handleFieldChange (event){
        let element = this.dataTemporarySplit.find(ele  => ele.AccountId === event.target.dataset.id);

        switch ( event.target.name ) { 
            case "RecurringFees":
                element.recurringFeePercentage = event.target.value;
                break;
            case "OneTimeFees":            
                element.nonRecurringFeePercentage = event.target.value;
                break;
            case "LegalName":            
                element.AccountLegalName = event.target.value;
                break;
            case "LegalNumber":            
                element.AccountLegalNumber = event.target.value;
                break;
        }
        this.dataTemporarySplit = [...this.dataTemporarySplit];
    }

    checkFees(){
        var recurring =0;
        var oneTime = 0;
    
        for (let rec of this.dataTemporarySplit){
            recurring += Number(rec.recurringFeePercentage);
            oneTime += Number(rec.nonRecurringFeePercentage);
        }

        if (this.RecurringFeeBoolean === true ){
            if(recurring!='100' /*|| oneTime!='100'*/  ){
                this.showToast('Error', 
                'Please fix deal billing percentages. The total must be 100%.', 
                'error');
            }else{
                this.handleSave();
            }
        }else if (this.OneTimeFeesBoolean === true) {
            if(/*recurring!='100'||*/ oneTime!='100'){
                this.showToast('Error', 
                'Please fix deal billing percentages. The total must be 100%.', 
                'error');
            }else{
                this.handleSave();
            }
        }
    }

    handleSave() {

        let toSaveList = this.myList;

        saveAccountWithOpportunity({oppId : this.recordId, PaymentInfoList : this.dataTemporarySplit})
        .then(() => {
            this.showToast('Success', 'Records saved succesfully!', 'success');
            this.isEdited = false;
            this.error = undefined;
        })
        .catch(error => {
            this.showToast('Error in Save', '' + error, 'error');
            this.isEdited = false;
            this.error = error;
        })

    }


    showToast(title, message, variant) {
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant,
        });
        this.dispatchEvent(evt);
    }

}