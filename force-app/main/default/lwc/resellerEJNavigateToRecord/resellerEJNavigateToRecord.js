import { LightningElement, api } from 'lwc';

export default class ResellerEJNavigateToRecord extends LightningElement {

    @api recordId;
    @api objectApiName;
    @api actionType;
    @api pageType;
}