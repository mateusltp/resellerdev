import { LightningElement, api } from "lwc";
import { updateRecord } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { FlowAttributeChangeEvent, FlowNavigationNextEvent} from 'lightning/flowSupport';
import FASTTRACK_STAGE from '@salesforce/schema/Opportunity.FastTrackStage__c';
import OPP_STAGE from '@salesforce/schema/Opportunity.StageName';
import OPP_ID from '@salesforce/schema/Opportunity.Id';
import sendToPipefy from '@salesforce/apex/PipefyIntegrationController.auraHandler';

export default class FastTrackFooter extends LightningElement {
    @api displayBackButton = false;
    @api displaySaveButton = false;
    @api displayContinueButton = false;
    @api backButtonLabel = 'Back';
    @api saveButtonLabel = 'Save';
    @api continueButtonLabel = 'Continue'; 
    @api backButtonVariant = 'neutral';
    @api saveButtonVariant = 'brand';
    @api continueButtonVariant = 'brand';
    @api actionClicked;
    @api backStage;
    @api oppStage;
    @api recordId;

    showSpinner = false;

    handleSave(){
        this.dispatchChangeAttributeEvent('save');
    }

    handleContinue(){
        console.log('teste botao ',this.recordId);
        //let sObjectName = 'Opportunity';
                
        sendToPipefy({ recordId: this.recordId, sObjectName: 'Opportunity'})
        .then((result) => {
                let data = result;
                console.log('retorno pipefy',data);

                if (data == 200){
                    const evt = new ShowToastEvent({
                        message: 'Your Pipefy has been created successfully',
                        variant: 'Success',
                        });
                        this.dispatchEvent(evt);
                }
        })
        
        .catch(error => { 
            //this.isLoading = false;
            if (error.body.message == 'This was already sent to Pipefy.'){
                console.log(error.body.message);
                
                const evt = new ShowToastEvent({
                message: error.body.message,
                variant: 'Error',
                });
                this.dispatchEvent(evt);
            }
        });
        //this.dispatchChangeAttributeEvent('continue');
    }

    dispatchChangeAttributeEvent(action){
        const attributeChangeEvent = new FlowAttributeChangeEvent('actionClicked', action);
        this.dispatchEvent(attributeChangeEvent);
        this.dispatchNextEvent();
    }

    dispatchNextEvent(){
        const navigateNextEvent = new FlowNavigationNextEvent();
        this.dispatchEvent(navigateNextEvent);
    }

    handleBack(){
        this.displaySpinner();
        this.updateOpportunityBackStage();
    }

    updateOpportunityBackStage(){
        const fields = this.assignFieldsTopUpdate();
        const recordInput = {fields};

        updateRecord(recordInput)
        .then(() => {
            this.hideSpinner();
        })
        .catch(error => {
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error updating the record',
                    message: error.body.message,
                    variant: 'error'
                })
            );
            this.hideSpinner();
        });
    }

    assignFieldsTopUpdate(){
        const fields = {}
        fields[OPP_ID.fieldApiName] = this.recordId;
        fields[FASTTRACK_STAGE.fieldApiName] = this.backStage;
        fields[OPP_STAGE.fieldApiName] = this.oppStage;
        return fields;
    }

    displaySpinner(){
        this.showSpinner = true;
    }

    hideSpinner(){
        this.showSpinner = false;
    }
}