import { LightningElement, api, track } from 'lwc';

import retrieveInitData from '@salesforce/apex/ResellerNewDeaLDeskOperationalCaseCtrl.retrieveInitData'; 

import CASE_OBJECT from '@salesforce/schema/Case';

import CASE_ELIGIBLE_REGISTRATION_METHOD from '@salesforce/schema/Case.Eligible_List_Registration_Method__c';
import CASE_ELIGIBLE_FILE_MANAGEMENT from '@salesforce/schema/Case.Eligible_File_Management__c';
import CASE_ELIGIBLE_FILE_UPDATE_FREQUENCY from '@salesforce/schema/Case.Eligible_File_Update_Frequency__c';
import CASE_ELIGIBLE_FILE_SENDING_DEADLINE from '@salesforce/schema/Case.Deadline_for_sending_Eligible_File__c';
//--
import CASE_ES_BILLING_DAY from '@salesforce/schema/Case.Recurring_Fees_Billing_Day__c';
import CASE_ES_PAYMENT_DUE_DAY from '@salesforce/schema/Case.Recurring_Fees_Payment_Due_Days__c';
//--
import CASE_SETUP_FEE_BILLING_DAY from '@salesforce/schema/Case.Non_Recurring_Billing_Day__c';
import CASE_SETUP_FEE_PAYMENT_DUE_DAY from '@salesforce/schema/Case.Non_Recurring_Payment_Due_Days__c';
//--
import CASE_PS_FEE_BILLING_DAY from '@salesforce/schema/Case.Prof_Services_One_Fee_Billing_Day__c';
import CASE_PS_FEE_PAYMENT_DUE_DAY from '@salesforce/schema/Case.Prof_Services_One_Fee_Payment_Due_Days__c';
//--
import CASE_PS_MAINTENANCE_BILLING_DAY from '@salesforce/schema/Case.Prof_Services_Main_Fee_Billing_Day__c';
import CASE_PS_MAINTENANCE_PAYMENT_DUE_DAY from '@salesforce/schema/Case.Prof_Services_Main_Fee_Payment_Due_Days__c';
//--
import CASE_MF_ELIGIBILITY_ES_BILLING_DAY from '@salesforce/schema/Case.MF_Eligibility_ES_Billing_Day__c';
import CASE_MF_ELIGIBILITY_ES_PAYMENT_DUE_DAY from '@salesforce/schema/Case.MF_Eligibility_ES_Payment_Due_Days__c';
//--
import CASE_COMPLIANT_TOPICS from '@salesforce/schema/Case.Justificate_No_Compliant_Topics__c';
import CASE_SUBJECT from '@salesforce/schema/Case.Subject';
import CASE_OWNER_ID from '@salesforce/schema/Case.OwnerId';
import CASE_OPPORTUNITY_ID from '@salesforce/schema/Case.OpportunityId__c';
import CASE_QUOTE_ID from '@salesforce/schema/Case.QuoteId__c';
//--
import CASE_RECORD_TYPE_ID from '@salesforce/schema/Case.RecordTypeId';

import { createRecord } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class ResellerNewDealDeskOperationalCaseSection extends LightningElement {
    
    @track activeSections = [
        'Eligible Section',
        'Enterprise Subscription Fee Section',
        'Setup Fee Section',
        'Professional Services Fee Section',
        'Professional Services Maintenance Section',
        'Subscription Fee Section', 
        'Complementary Information Section'];
    @api showLabels = false;

    @api initData;
    @track error;

    @api recordId;
    //@api objectApiName;

    isOpen = false;
    isLoading = false;

    //-- data storage vars
    @api eligibleRegistrationMethodVal;
    @api eligibleFileManagementVal;
    @track eligibleFileUpdateFrequencyVal;
    @api eligibleFileSendingDeadlineVal;

    @api enterpriseEsBillingDayVal;
    @api enterpriseEsPaymentDueDaysVal;

    @api setupFeeBillingDayVal;
    @api setupFeePaymentDueDayVal;

    @api professionalServicesFeeBillingDayVal;
    @api professionalServicesFeePaymentDueDayVal;

    @api professionalServMaintencanceBillingDayVal;
    @api professionalServMaintenancePaymentDueDayVal;

    @api mfEligibilityEsBillingDayVal;
    @api mfEligibilityEsPaymentDueDayVal;

    @api justificateNoCompliantTopicsVal;
    @api caseSubjectVal = "Deal Desk Operational Approval Request";
    @api defaultQuoteVal;
    @api queueIdVal;
    dealDeskOperatiqueueIdVal;

    @api recordTypeIdVal;
    
    //-- non-compliant cases per field value
    nonCompliantEligibleRegistrationMethod = 
        ["GDPR & API", "GDPR & SSO","API only", "SSO only", "SSO & SFTP", "SFTP & HR Portal"];
    nonCompliantDay = "Custom";
    
    hasProfessionalFee = false;
    hasProfessionalMaintenanceFee = false;
    hasSetupFee = false;

    @track displayDependentField;
    isCompliant = false;

    connectedCallback() {
        //console.log('######## INTO THE CONNECTED CALLBACK #######');
        this.fetchInitialData();
    }
    resul45iii;
    @track mapData= [];
    @track mapDataer= [];
    fetchInitialData() {
        this.showSpinner();
        retrieveInitData({ opportunityId : this.recordId })
            .then((result) => {
                // console.log(result);
                this.initData = result;

                //-- default data assignment
                if(result.previousDealDeskOperationalCase) {
                    console.log('entrou434');
                    this.eligibleRegistrationMethodVal = result.previousDealDeskOperationalCase.eligible_list_registration_method__c;
                    this.eligibleFileManagementVal = result.previousDealDeskOperationalCase.eligible_file_management__c;
                    if(this.eligibleRegistrationMethodVal == 'HR Portal'
                        || this.eligibleRegistrationMethodVal == 'SFTP & HR Portal') {
                        this.displayDependentField = true;
                        this.eligibleFileUpdateFrequencyVal = result.previousDealDeskOperationalCase.eligible_file_update_frequency__c;
                        this.eligibleFileSendingDeadlineVal = result.previousDealDeskOperationalCase.deadline_for_sending_eligible_file__c;
                    }

                    this.enterpriseEsBillingDayVal = result.previousDealDeskOperationalCase.recurring_Fees_Billing_Day__c;
                    this.enterpriseEsPaymentDueDaysVal = result.previousDealDeskOperationalCase.recurring_Fees_Payment_Due_Days__c;

                    this.setupFeeBillingDayVal = result.previousDealDeskOperationalCase.non_Recurring_Billing_Day__c;
                    this.setupFeePaymentDueDayVal = result.previousDealDeskOperationalCase.non_Recurring_Payment_Due_Days__c;

                    this.professionalServicesFeeBillingDayVal = result.previousDealDeskOperationalCase.prof_services_one_fee_billing_day__c;
                    this.professionalServicesFeePaymentDueDayVal = result.previousDealDeskOperationalCase.prof_services_one_fee_payment_due_days__c;

                    this.professionalServMaintencanceBillingDayVal = result.previousDealDeskOperationalCase.prof_services_main_fee_billing_day__c;
                    this.professionalServMaintenancePaymentDueDayVal = result.previousDealDeskOperationalCase.prof_services_main_fee_payment_due_days__c;

                    this.mfEligibilityEsBillingDayVal = result.previousDealDeskOperationalCase.mf_eligibility_es_billing_day__c;
                    this.mfEligibilityEsPaymentDueDayVal = result.previousDealDeskOperationalCase.mf_eligibility_es_payment_due_days__c;

                    this.justificateNoCompliantTopicsVal = (result.previousDealDeskOperationalCase.justificate_no_compliant_topics__c) ? 
                        result.previousDealDeskOperationalCase.justificate_no_compliant_topics__c : '';
                }
                
                if(result.defaultQuoteId) {
                    this.defaultQuoteVal = result.defaultQuoteId;
                }

                if(result.quoteLineItems) {
                    for (let i = 0; i < result.quoteLineItems; i++) {
                        if(result.quoteLineItems[i].Fee_Type__c == 'Professional Services Setup Fee') {
                            this.hasProfessionalFee = true;
                        } else if(result.quoteLineItems[i].Fee_Type__c == 'Professional Services Maintenance Fee') {
                            this.hasProfessionalMaintenanceFee = true;
                        } else if(result.quoteLineItems[i].Fee_Type__c == 'Setup Fee') {
                            this.hasSetupFee = true;
                        }
                    }
                }

                if(result.queueId) {
                    this.queueIdVal = result.queueId;
                }

                if(result.recordTypeId) {
                    this.recordTypeIdVal = result.recordTypeId;
                }


                if(this.enterpriseEsBillingDayVal == null || this.enterpriseEsBillingDayVal == '' ){
                    this.enterpriseEsBillingDayVal = '01';
                }

                if(this.enterpriseEsPaymentDueDaysVal == null || this.enterpriseEsPaymentDueDaysVal == ''){
                    this.enterpriseEsPaymentDueDaysVal = '30 days';
                }

                if( this.mfEligibilityEsBillingDayVal == null || this.mfEligibilityEsBillingDayVal == ''){
                    this.mfEligibilityEsBillingDayVal = '01';
                }

                if(this.mfEligibilityEsPaymentDueDayVal == null || this.mfEligibilityEsPaymentDueDayVal == ''){
                    this.mfEligibilityEsPaymentDueDayVal = '30 days';
                }
                
                this.hideSpinner();
            })
            .catch((error) => {
                this.error = error;
                this.hideSpinner();
            })
    }

    onSubmitHandler() {
        this.createNewDealDeskOperationalCase();
    }

    loadFields() {
        const fields = {};
        //-- record type definition
        fields[CASE_COMPLIANT_TOPICS.fieldApiName] = this.justificateNoCompliantTopicsVal;
        
        //-- fields filled in the form 
        fields[CASE_ELIGIBLE_REGISTRATION_METHOD.fieldApiName] = (this.eligibleRegistrationMethodVal) ? this.eligibleRegistrationMethodVal : null;
        fields[CASE_ELIGIBLE_FILE_MANAGEMENT.fieldApiName] = (this.eligibleFileManagementVal) ? this.eligibleFileManagementVal : null;
        fields[CASE_ELIGIBLE_FILE_UPDATE_FREQUENCY.fieldApiName] = (this.eligibleFileUpdateFrequencyVal) ? this.eligibleFileUpdateFrequencyVal : null;
        fields[CASE_ELIGIBLE_FILE_SENDING_DEADLINE.fieldApiName] = (this.eligibleFileSendingDeadlineVal) ? this.eligibleFileSendingDeadlineVal : null;

        fields[CASE_ES_BILLING_DAY.fieldApiName] = (this.enterpriseEsBillingDayVal) ? this.enterpriseEsBillingDayVal : null;
        fields[CASE_ES_PAYMENT_DUE_DAY.fieldApiName] = (this.enterpriseEsPaymentDueDaysVal) ? this.enterpriseEsPaymentDueDaysVal : null;

        fields[CASE_SETUP_FEE_BILLING_DAY.fieldApiName] = (this.setupFeeBillingDayVal) ? this.setupFeeBillingDayVal : null;
        fields[CASE_SETUP_FEE_PAYMENT_DUE_DAY.fieldApiName] = (this.setupFeePaymentDueDayVal) ? this.setupFeePaymentDueDayVal : null;

        fields[CASE_PS_FEE_BILLING_DAY.fieldApiName] = (this.professionalServicesFeeBillingDayVal) ? this.professionalServicesFeeBillingDayVal : null;
        fields[CASE_PS_FEE_PAYMENT_DUE_DAY.fieldApiName] = (this.professionalServicesFeePaymentDueDayVal) ? this.professionalServicesFeePaymentDueDayVal : null;

        fields[CASE_PS_MAINTENANCE_BILLING_DAY.fieldApiName] = (this.professionalServMaintencanceBillingDayVal) ? this.professionalServMaintencanceBillingDayVal : null;
        fields[CASE_PS_MAINTENANCE_PAYMENT_DUE_DAY.fieldApiName] = (this.professionalServMaintenancePaymentDueDayVal) ? this.professionalServMaintenancePaymentDueDayVal : null;

        fields[CASE_MF_ELIGIBILITY_ES_BILLING_DAY.fieldApiName] = (this.mfEligibilityEsBillingDayVal) ? this.mfEligibilityEsBillingDayVal : null;
        fields[CASE_MF_ELIGIBILITY_ES_PAYMENT_DUE_DAY.fieldApiName] = (this.mfEligibilityEsPaymentDueDayVal) ? this.mfEligibilityEsPaymentDueDayVal : null;

        fields[CASE_COMPLIANT_TOPICS.fieldApiName] = (this.justificateNoCompliantTopicsVal) ? this.justificateNoCompliantTopicsVal : null;
        fields[CASE_SUBJECT.fieldApiName] = (this.caseSubjectVal) ? this.caseSubjectVal : null;
        fields[CASE_OPPORTUNITY_ID.fieldApiName] = (this.recordId) ? this.recordId : null;
        fields[CASE_QUOTE_ID.fieldApiName] = (this.defaultQuoteVal) ? this.defaultQuoteVal : null;
        fields[CASE_OWNER_ID.fieldApiName] = (this.queueIdVal) ? this.queueIdVal : null;

        fields[CASE_RECORD_TYPE_ID.fieldApiName] = (this.recordTypeIdVal) ? this.recordTypeIdVal : null;
        
        return fields;
    }
    
    createNewDealDeskOperationalCase() {
        // console.log('### INTO THE createNewDealDeskOperationalCase ###')
        if(this.validateForm()) {
            this.isLoading = true;

            if(this.invalidCustomValue) {
                this.handleInvalidCustomValues();
                this.isLoading = false;
                return;

            } else if(!this.isCompliant) {
                this.handleNotCompliantCase();
                this.isLoading = false;
                return;

            } else {
                const fields = this.loadFields();
                const recordInput = { apiName: CASE_OBJECT.objectApiName, fields };
                
                // console.log("The case to insert: ");
                // console.log(recordInput);
                createRecord(recordInput)
                    .then(newCase => {
                        //console.log('### SUCCESS ###');
                        //console.log(newCase);
                        this.isOpen = false;
                        this.showToast('Deal Desk Operational Case created successfully', 'Success', 4000, 'success');
                        this.isLoading = false;
                        //-- TODO this.resetValues();
                        
                        //-- hard refresh to update related lists
                        window.location.reload();
                    })
                    .catch(error => {
                        this.showToast('Case creation failed', 'Please contact your administrator to check permission or data validation.', 6000, 'error');
                        this.isLoading = false;
                    })
            }
            
        } else {
            this.showToast('Form validation failed', 'The form validation failed. Please ensure that all the required fields have been filled in.', 6000, 'error');
            this.isLoading = false;
        }
    }

    showToast(toastMessage, toastTitle, toastDuration, toastVariant) {
        const event = new ShowToastEvent({
            title: toastMessage,
            message: toastTitle,
            duaration: toastDuration,
            variant: toastVariant,
        });
        this.dispatchEvent(event);
    }

    handleClick() {
        this.isOpen = true;
    }

    handleCancel() {
        this.isOpen = false;
    }

    handleNotCompliantCase() {
        this.scrollToDiv("complementaryInformationSectionDiv");
        this.showToast(
            'Non-compliant case',
            'This case is not compliant. Please fill in the field Justificate No Compliant Topics before saving.', 
            6000, 
            'error');
        return;
    }

    handleInvalidCustomValues() {
        this.showToast(
            'Invalid "Custom" value',
            'This type of opportunity does not allow to select the value "Custom" for the designated fields. Please review your form.', 
            6000, 
            'error');
        return;
    }

    handleEligibleRegistrationMethodUpdate (event) {
        this.eligibleRegistrationMethodVal = event.detail.value;

        if(this.eligibleRegistrationMethodVal == 'HR Portal' 
            || this.eligibleRegistrationMethodVal == 'SFTP & HR Portal') {
                this.displayDependentField = true; 
        } else {
            this.displayDependentField = false;
            this.eligibleFileUpdateFrequencyVal = null;
            this.eligibleFileSendingDeadlineVal = null;
        }
    }

    handleEligibleFileManagementUpdate(event) {
        this.eligibleFileManagementVal = event.detail.value;
    }

    handleEligibleFileUpdateFrequencyUpdate(event) {
        if(event.detail.value == this.nonCompliantDay) {
            this.showToast('Invalid value', 'This opportunity type does not allow the "Custom" value for this field.', 4000, 'error');
        }
        this.eligibleFileUpdateFrequencyVal = event.detail.value;
    }

    handleEligibleFileSendingDeadlineUpdate(event) {
        if(event.detail.value == this.nonCompliantDay) {
            this.showToast('Invalid value', 'This opportunity type does not allow the "Custom" value for this field.', 4000, 'error');
        }
        this.eligibleFileSendingDeadlineVal = event.detail.value;
    }

    handleEnterpriseEsBillingDayUpdate(event) {
        if(event.detail.value == this.nonCompliantDay) {
            this.showToast('Invalid value', 'This opportunity type does not allow the "Custom" value for this field.', 4000, 'error');
        }
        this.enterpriseEsBillingDayVal = event.detail.value;
    }

    handleEnterpriseEsPaymentDueDaysUpdate(event) {
        if(event.detail.value == this.nonCompliantDay) {
            this.showToast('Invalid value', 'This opportunity type does not allow the "Custom" value for this field.', 4000, 'error');
        }
        this.enterpriseEsPaymentDueDaysVal = event.detail.value;
    }

    handleSetupFeeBillingDayUpdate(event) {
        if(event.detail.value == this.nonCompliantDay) {
            this.showToast('Invalid value', 'This opportunity type does not allow the "Custom" value for this field.', 4000, 'error');
        }
        this.setupFeeBillingDayVal = event.detail.value;
    }

    handleSetupFeePaymentDueDayUpdate(event) {
        if(event.detail.value == this.nonCompliantDay) {
            this.showToast('Invalid value', 'This opportunity type does not allow the "Custom" value for this field.', 6000, 'error');
        }
        this.setupFeePaymentDueDayVal = event.detail.value;
    }

    handleProfessionalServicesFeeBillingDayUpdate(event) {
        if(event.detail.value == this.nonCompliantDay) {
            this.showToast('Invalid value', 'This opportunity type does not allow the "Custom" value for this field.', 6000, 'error');
        }
        this.professionalServicesFeeBillingDayVal = event.detail.value;
    }

    handleProfessionalServicesFeePaymentDueDayUpdate(event) {
        if(event.detail.value == this.nonCompliantDay) {
            this.showToast('Invalid value', 'This opportunity type does not allow the "Custom" value for this field.', 6000, 'error');
        }
        this.professionalServicesFeePaymentDueDayVal = event.detail.value;
    }

    handleProfessionalServMaintencanceBillingDayUpdate(event) {
        if(event.detail.value == this.nonCompliantDay) {
            this.showToast('Invalid value', 'This opportunity type does not allow the "Custom" value for this field.', 6000, 'error');
        }
        this.professionalServMaintencanceBillingDayVal = event.detail.value;
    }

    handleProfessionalServMaintenancePaymentDueDayUpdate(event) {
        if(event.detail.value == this.nonCompliantDay) {
            this.showToast('Invalid value', 'This opportunity type does not allow the "Custom" value for this field.', 6000, 'error');
        }
        this.professionalServMaintenancePaymentDueDayVal = event.detail.value;
    }

    handleMfEligibilityEsBillingDayUpdate(event) {
        if(event.detail.value == this.nonCompliantDay) {
            this.showToast('Invalid value', 'This opportunity type does not allow the "Custom" value for this field.', 6000, 'error');
        }
        this.mfEligibilityEsBillingDayVal = event.detail.value;
    }

    handleMfEligibilityEsPaymentDueDayUpdate(event) {
        if(event.detail.value == this.nonCompliantDay) {
            this.showToast('Invalid value', 'This opportunity type does not allow the "Custom" value for this field.', 6000, 'error');
        }
        this.mfEligibilityEsPaymentDueDayVal = event.detail.value;
    }

    handleJustificateNoCompliantTopicsUpdate(event) {
        this.justificateNoCompliantTopicsVal = event.detail.value;
    }

    handleCaseSubjectUpdate(event) {
        this.caseSubjectVal = event.detail.value;
    }

    scrollToDiv(divToNavigateTo) {
        const thisDiv = this.template.querySelector('[data-id="' + divToNavigateTo + '"]');
        thisDiv.scrollIntoView({behavior: "smooth", block: "center", inline: "nearest"});
    }

    validateForm() {
        //-- reset the validation flags
        this.isCompliant = true;
        this.invalidCustomValue = false;
        
        //-- validating compliance
        if((this.nonCompliantEligibleRegistrationMethod.includes(this.eligibleRegistrationMethodVal))
            && (!this.justificateNoCompliantTopicsVal)) {
            this.isCompliant = false;
        }

        //-- validating invalid 'Custom' picklist option
        if((((this.eligibleFileUpdateFrequencyVal) && (this.eligibleFileUpdateFrequencyVal == this.nonCompliantDay))
            || ((this.eligibleFileSendingDeadlineVal) && (this.eligibleFileSendingDeadlineVal == this.nonCompliantDay))
            || ((this.enterpriseEsBillingDayVal) && (this.enterpriseEsBillingDayVal == this.nonCompliantDay))
            || ((this.enterpriseEsPaymentDueDaysVal) && (this.enterpriseEsPaymentDueDaysVal == this.nonCompliantDay))
            || ((this.setupFeeBillingDayVal) && (this.setupFeeBillingDayVal == this.nonCompliantDay))
            || ((this.setupFeePaymentDueDayVal) && (this.setupFeePaymentDueDayVal == this.nonCompliantDay))
            || ((this.professionalServicesFeeBillingDayVal) && (this.professionalServicesFeeBillingDayVal == this.nonCompliantDay))
            || ((this.professionalServicesFeePaymentDueDayVal) && (this.professionalServicesFeePaymentDueDayVal == this.nonCompliantDay))
            || ((this.professionalServMaintencanceBillingDayVal) && (this.professionalServMaintencanceBillingDayVal == this.nonCompliantDay))
            || ((this.professionalServMaintenancePaymentDueDayVal) && (this.professionalServMaintenancePaymentDueDayVal == this.nonCompliantDay))
            || ((this.mfEligibilityEsBillingDayVal) && (this.mfEligibilityEsBillingDayVal == this.nonCompliantDay))
            || ((this.mfEligibilityEsPaymentDueDayVal) && (this.mfEligibilityEsPaymentDueDayVal == this.nonCompliantDay)))) {
                this.invalidCustomValue = true;
        }
        
        const allValid = [...this.template.querySelectorAll("lightning-input-field")]
            .reduce((validSoFar, inputCmp) => {
                inputCmp.reportValidity();
                if(!inputCmp.required) {
                    return validSoFar;
                }
                return validSoFar && inputCmp.value;
            }, true);
        return allValid;
    }

    showSpinner() {
        this.isLoading = true;
    }

    hideSpinner() {
        this.isLoading = false;
    }

}