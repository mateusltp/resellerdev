const validateCnpj = (cnpj) =>{
    if(cnpj.length != 18)return false;
    if(cnpj != null)cnpj = cnpj.replace(/[^\d]+/g,'');
    
    if(cnpj.length != 14)return false;
    if( cnpj == "00000000000000" || cnpj == "11111111111111" || 
        cnpj == "22222222222222" || cnpj == "33333333333333" || 
        cnpj == "44444444444444" || cnpj == "55555555555555" || 
        cnpj == "66666666666666" || cnpj == "77777777777777" || 
        cnpj == "88888888888888" || cnpj == "99999999999999") return false;
    
        
    let tamanho = cnpj.length - 2
    let numeros = cnpj.substring(0,tamanho);
    let digitos = cnpj.substring(tamanho);
    let soma = 0;
    let pos = tamanho - 7;

    for (let i = tamanho; i >= 1; i--) {
      soma += numeros.charAt(tamanho - i) * pos--;
      if (pos < 2)
            pos = 9;
    }

    let resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;

    if (resultado != digitos.charAt(0))
        return false;
     
    tamanho = tamanho + 1;
    numeros = cnpj.substring(0,tamanho);
    soma = 0;
    pos = tamanho - 7;

    for (let i = tamanho; i >= 1; i--) {
        soma += numeros.charAt(tamanho - i) * pos--;
        if (pos < 2)
            pos = 9;
    }

    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;

    if (resultado != digitos.charAt(1))
      return false;
       
    return true;
}

const validateNif = (nif) => {
    let added;
    let mod;
    let control;

    if(nif.length == 9){
        added = ((nif[7]*2)+(nif[6]*3)+(nif[5]*4)+(nif[4]*5)+(nif[3]*6)+(nif[2]*7)+(nif[1]*8)+(nif[0]*9));
        mod = added % 11;
        if(mod == 0 || mod == 1){
            control = 0;
        } else {
             control = 11 - mod;   
        }
        
        if(nif[8] == control){
            return true;
        } else {
             return false;  
        }
    } else {
        return false;
    }
}

export {validateCnpj, validateNif};