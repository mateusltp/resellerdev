/**
 * @author              JSilva
 * @date                01-08-2022
 * @description         Component cloned from Enterprise/SMB valueWithHelpTextFlow lightning web component.
 *                      Mimic the display of fields adding the tooltip that works similarily to Salesforce
 *                      standard help text.
**/
import { LightningElement,api, wire } from 'lwc';

export default class displayHelpText extends LightningElement {
    @api label;
    @api helpTextDetail;
    @api value;
    @api isBoldText;
}