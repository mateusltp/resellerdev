import { LightningElement, api } from 'lwc';

import { NavigationMixin } from 'lightning/navigation';

const COLUMNS = [
    { label: 'Name', fieldName: 'Name', hideDefaultActions: true },
    { label: 'Email', fieldName: 'Email', type: 'email', hideDefaultActions: true }
    // { label: 'Phone', fieldName: 'Phone', type: 'phone' }
];

export default class ResellerEJContactsConfirmationSection extends NavigationMixin(LightningElement)  {

    @api contactsList = [];
    @api account;

    @api linkLabel = 'List of Contacts';
    @api closeButtonLabel = 'Close';
    @api manageContactsButtonLabel = 'Manage Contacts';

    columns = COLUMNS;
    isOpen = false;
    accountPageRef;
    url;

    connectedCallback() {
        
        this.accountPageRef = {
            type: 'standard__recordPage',
            attributes: {
                recordId: this.account.Id,
                objectApiName: 'Account',
                actionName: 'view'
            }
        };
        this[NavigationMixin.GenerateUrl](this.accountPageRef)
            .then(url => this.url = url);
    }

    handleClick() {
        this.isOpen = true;
    }

    handleCloseAction() {
        this.isOpen = false;
    }

    handleManageAction(evt) {
        this.isOpen = false;

        evt.preventDefault();
        evt.stopPropagation();
        // Navigate to the Account Home page.
        window.open(this.url);

    }
}