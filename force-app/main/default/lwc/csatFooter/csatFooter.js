/**
 * @description       : 
 * @author            : tiago.ribeiro@gympass.com
 * @group             : 
 * @last modified on  : 06-30-2022
 * @last modified by  : tiago.ribeiro@gympass.com
**/
import { LightningElement, api } from 'lwc';
import {FlowNavigationNextEvent, FlowAttributeChangeEvent} from 'lightning/flowSupport';

export default class CsatFooter extends LightningElement {
    @api replyLaterLabel;
    @api nextButtonLabel;
    @api actionClicked;
    showSpinner = false;

    handleNext(){
        this.dispatchChangeAttributeEvent('next');
    }

    handleReplyLater(){
        this.dispatchChangeAttributeEvent('replyLater');
    }

    dispatchChangeAttributeEvent(action){
        const attributeChangeEvent = new FlowAttributeChangeEvent('actionClicked', action);
        this.dispatchEvent(attributeChangeEvent);
        this.dispatchNextEvent();
    }

    dispatchNextEvent(){
        const navigateNextEvent = new FlowNavigationNextEvent();
        this.dispatchEvent(navigateNextEvent);
    }
}