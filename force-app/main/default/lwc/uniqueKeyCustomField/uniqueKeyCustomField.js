import { api, LightningElement } from 'lwc';

const CNPJ = 'CNPJ';
const NIF = 'NIF';
const EIN = 'EIN';
const UNIQUE_KEYS = [CNPJ, NIF, EIN];

export default class UniqueKeyCustomField extends LightningElement {

    @api uniqueKey = 'CNPJ';
    @api label = 'Set a label';
    @api value;
    @api isRequired = false;

    handleManageKeysValidations(e){

        let uniqueKeyInput = this.template.querySelector('.uniqueKeyInput');
        let uniqueKeyValue = e.target.value;
        
        this.value = uniqueKeyValue;
        let isValid = false;

        if(this.uniqueKey == CNPJ){
            this.value = this.getCnpjMask(this.value);
            this.template.querySelector('.uniqueKeyInput').value = this.value;
            isValid = this.validateCnpj(this.value);
        }
        else if(this.uniqueKey == NIF)isValid = this.validateNif(uniqueKeyValue);
        else isValid = true;

        if(isValid == false) uniqueKeyInput.setCustomValidity('Unique key is not valid.');
        else uniqueKeyInput.setCustomValidity(''); 
        
        uniqueKeyInput.reportValidity();

        this.updateValue();
        
        this.validate(isValid);
    }

    updateValue(){
        this.dispatchEvent(new CustomEvent('updatekey', { detail: this.value }));
    }

    validate(isValid){
        this.dispatchEvent(new CustomEvent('invalid', { detail: isValid }));
    }

    /**
     * 
     * @param {*} input Cnpj Number
     * @returns Cnpj number
     * @description Creates a unique identifier mask according to the Brazilian CNPj.
     */
    getCnpjMask(input){
        let cnpj = input.replace(/\D/g, '').match(/(\d{0,2})(\d{0,3})(\d{0,3})(\d{0,4})(\d{0,2})/);
        return !cnpj[2] ? cnpj[1] : cnpj[1] + '.' + cnpj[2] + (cnpj[3] ? '.' + cnpj[3] : '')  + (cnpj[4] ? '/' + cnpj[4] : '') + (cnpj[5] ? '-' + cnpj[5] : '');
    }

    /**
     * 
     * @param {*} cnpj Cnpj Number
     * @returns boolean valid
     * @description Validates the unique identifier according to Brazilian CNPJ standards.
     */
    validateCnpj(cnpj){
        
        if(cnpj.length != 18)return false;
        if(cnpj != null)cnpj = cnpj.replace(/[^\d]+/g,'');
        
        if(cnpj.length != 14)return false;
        if( cnpj == "00000000000000" || cnpj == "11111111111111" || 
            cnpj == "22222222222222" || cnpj == "33333333333333" || 
            cnpj == "44444444444444" || cnpj == "55555555555555" || 
            cnpj == "66666666666666" || cnpj == "77777777777777" || 
            cnpj == "88888888888888" || cnpj == "99999999999999") return false;
        
            
        let tamanho = cnpj.length - 2
        let numeros = cnpj.substring(0,tamanho);
        let digitos = cnpj.substring(tamanho);
        let soma = 0;
        let pos = tamanho - 7;

        for (let i = tamanho; i >= 1; i--) {
          soma += numeros.charAt(tamanho - i) * pos--;
          if (pos < 2)
                pos = 9;
        }

        let resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;

        if (resultado != digitos.charAt(0))
            return false;
         
        tamanho = tamanho + 1;
        numeros = cnpj.substring(0,tamanho);
        soma = 0;
        pos = tamanho - 7;

        for (let i = tamanho; i >= 1; i--) {
            soma += numeros.charAt(tamanho - i) * pos--;
            if (pos < 2)
                pos = 9;
        }

        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;

        if (resultado != digitos.charAt(1))
          return false;
           
        return true;

    }

    validateNif(nif){
        let added;
        let mod;
        let control;
        
        if(nif.length == 9){
            added = ((nif[7]*2)+(nif[6]*3)+(nif[5]*4)+(nif[4]*5)+(nif[3]*6)+(nif[2]*7)+(nif[1]*8)+(nif[0]*9));
            mod = added % 11;
            if(mod == 0 || mod == 1){
                control = 0;
            } else {
                control = 11 - mod;   
            }
            
            if(nif[8] == control){
                return true;
            } else {
                return false;  
            }
        } else if(nif.length == 0){
            return true;
        } else {
            return false;
        } 
    }
}