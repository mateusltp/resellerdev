import { api, LightningElement, track, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { getRecord } from 'lightning/uiRecordApi';

export default class NetworkBuilderRemover extends LightningElement {
    //Boolean tracked variable to indicate if modal is open or not default value is false as modal is closed when page is loaded 
    @track isModalOpen = true;
    @api getIdFromParent;
    @track record;
    @track error;
    hideFields = false;

    @wire(getRecord, { recordId: '$getIdFromParent', fields: ['Account.Name'] })
    wiredAccount({ error, data }) {
        if (data) {
            this.record = data;
            this.error = undefined;
        } else if (error) {
            this.error = error;
            this.record = undefined;
        }
    }
    get name() {
        return this.record.fields.Name.value;
    }

    handleRemove(event) {
        event.preventDefault();       // stop the form from submitting
        const fields = event.detail.fields;
        console.log('>>>>>>>>>>>>>   + ' + JSON.stringify(fields));
        fields.ParentId = ''; 
        fields.Network_ID__c = '';
        this.template.querySelector('lightning-record-edit-form').submit(fields);
    }

    handleSuccess(event) {
        this.showPopUp('success', 'Account Removed!', 'The account was removed from this network.');
        this.closeModal();
    }

    handleError(event) {
        const details = event.detail;
        let message = details.message + ' Detail: ' + details.detail;
        this.showPopUp('error', 'Ops! An error has occurred while updating account', message);
    }

    showPopUp(aVariant, aTitle, aMessage) {
        const evt = new ShowToastEvent({
            title: aTitle,
            message: aMessage,
            variant: aVariant,
            mode: 'sticky'
        });
        this.dispatchEvent(evt);
    }

    closeModal() {
        const myEvent = new CustomEvent('close');
        this.dispatchEvent(myEvent);
    }

}