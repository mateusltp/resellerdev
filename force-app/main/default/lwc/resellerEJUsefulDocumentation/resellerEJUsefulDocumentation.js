import { LightningElement, api } from 'lwc';

import M1_TEMPLATE_LINK_LABEL from '@salesforce/label/c.RESELLER_Link_Label_M1_Template';
import M1_TEMPLATE_LINK_URL from '@salesforce/label/c.RESELLER_Link_URL_M1_Template';
import POST_LAUNCH_TEMPLATE_LINK_LABEL from '@salesforce/label/c.RESELLER_Link_Label_Post_Launch_Template';
import POST_LAUNCH_TEMPLATE_LINK_URL from '@salesforce/label/c.RESELLER_Link_URL_Post_Launch_Template';
import GOOD_PRACTICES_SUCCESS_LAUNCH_LINK_LABEL from '@salesforce/label/c.RESELLER_Link_Label_Good_Practices_Successful_Launch';
import GOOD_PRACTICES_SUCCESS_LAUNCH_LINK_URL from '@salesforce/label/c.RESELLER_Link_URL_Good_Practices_Successful_Launch';

export default class ResellerEJUsefulDocumentation extends LightningElement {

    @api m1TemplateLinkLabel = M1_TEMPLATE_LINK_LABEL;
    @api m1TemplateLinkURL = M1_TEMPLATE_LINK_URL;

    @api postLaunchTemplateLinkLabel = POST_LAUNCH_TEMPLATE_LINK_LABEL;
    @api postLaunchTemplateLinkURL = POST_LAUNCH_TEMPLATE_LINK_URL;

    @api goodPracticesSuccessLaunchLinkLabel = GOOD_PRACTICES_SUCCESS_LAUNCH_LINK_LABEL;
    @api goodPracticesSuccessLaunchLinkURL = GOOD_PRACTICES_SUCCESS_LAUNCH_LINK_URL;

}