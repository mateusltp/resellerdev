import { LightningElement, track, api, wire } from 'lwc';
import { CloseActionScreenEvent } from 'lightning/actions';
import { ContractTO } from './ContractTO';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import { refreshApex } from '@salesforce/apex';

import getTransferObjData from '@salesforce/apex/CreatePartnersContractMainController.getContractToInstance';
import createContract from '@salesforce/apex/CreatePartnersContractMainController.createContractRelatedRecordsAndUpdateLegalReps';
import evaluateTranferObj from '@salesforce/apex/CreatePartnersContractMainController.evaluateTransferObj';

import STAGE_NAME from '@salesforce/schema/Opportunity.StageName';

const fields = [STAGE_NAME];

export default class CreatePartnersContractMain extends NavigationMixin(LightningElement) {
    @api recordId;
    accountId;
    transferObjData = new ContractTO;
    currentStep = 's1';
    spinner = false;
    accountData;
    disableDoneButton = false;
    backResult; 
    isRedirect;
  
    connectedCallback() {    
        this.template.addEventListener('commercialvalidation', this.handleCommercialValidation.bind(this));
    }

    renderedCallback() {       
 

        if (this.currentStep == 's1') {
            this.applyStyleForStepS1();
        } 
        
        if (this.currentStep != 's1'){
            this.applyDefaultStyle();
        }
    }

    @api
    get isContract() { return this.currentStep == 's1' ? true : false; }

    @api
    get isAccounts() { return this.currentStep == 's2' ? true : false; }

    @api
    get isLegalReps() { return this.currentStep == 's3' ? true : false; }

    @api
    get toLoaded() { return this.transferObjData.masterOpportunity != null ? true : false}

    @wire(getRecord, { recordId: '$recordId', fields })
    opportunity;



    @wire(getTransferObjData, { oppId: '$recordId' }) 
    getToInstance(result){
        this.backResult = result;
        if(result.data) { 
            this.transferObjData = JSON.parse(JSON.stringify(result.data));
            this.accountData = this.transferObjData.masterAccount;
            if( this.accountData.Partner_Level__c == "Single Partner" ){    
                if (!this.ifOppStageOfferApproved()) {
                    this.showToastNotification('', 'Please, move the opportunity to the Offer Approved stage', 'warning');
                    this.hideSpinner();
                    this.closeAction();
                } else {
                    if( !this.contractAlreadyCreated() ){
                        if( this.accountData.Wishlist__c ) {
                            this.transferObjData.contract.type = 'Commercial Contract';
                            this.transferObjData.contract.country = this.transferObjData.masterAccount.BillingCountry; 
                            this.transferObjData.locationsAccountIdList.push(this.transferObjData.masterAccount.Id);                  
                            this.handleDone();
                        } else {
                            this.showToastNotification('', 'Contract for long tail is not available yet.', 'warning');
                            this.hideSpinner();
                            this.closeAction();
                        }
                    } else if(this.contractAlreadyCreated() && !this.isRedirect){
                        this.showToastNotification('', 'There is already a contract created of id ' + this.transferObjData.contract.contractSobjectRecord.Id, 'warning');
                        this.hideSpinner();
                        this.closeAction();
                    }  
                }
            } else {
                this.hideSpinner();
            }           
        } else if (result.error) {
            console.log(result.error.body.message);
            this.showToastNotification('Something unexpected happened. Please, contact a Salesforce admin.', result.error.body.message, 'error');
        }
    }
    

    applyStyleForStepS1() {
        const style = document.createElement('style');
        style.innerText = `.slds-modal__content{
            overflow: initial !important;
            overflow-y: visible !important; 
        }`;
        
        const style2 = document.createElement('style');
        style2.innerText = `.slds-modal__container{
            width: 450px !important;
        }`;

        this.template.querySelector('[data-id="panelBody"]').appendChild(style);
        this.template.querySelector("lightning-quick-action-panel").appendChild(style2);
    }

    applyDefaultStyle() {
        const style = document.createElement('style');
        style.innerText = `.slds-modal__content{
            overflow: initial !important;
            overflow-y: auto !important; 
        }`;

        const style2 = document.createElement('style');
        style2.innerText = `.slds-modal__container{
            width: 900px !important;
        }`;
        
        this.template.querySelector('[data-id="panelBody"]').appendChild(style);
        this.template.querySelector("lightning-quick-action-panel").appendChild(style2);
    }

    closeAction(){
        this.dispatchEvent(new CloseActionScreenEvent());
    }

    handleBackStep(){
        if (this.currentStep == 's2') {
            this.currentStep = 's1';
        }

        if (this.currentStep == 's3') {
            this.currentStep = 's2';
        }
    }

    handleNextStep(){
        if (this.currentStep == 's1') {
            if (this.areContractFieldsValid()) {
                if (!this.ifOppStageOfferApproved()) {
                    this.showToastNotification('', 'Please, move the opportunity to the Offer Approved stage', 'warning');
                }
                else if (this.contractAlreadyCreated()) {
                    this.showToastNotification('', 'There is already a contract created of id ' + this.transferObjData.contract.contractSobjectRecord.Id, 'warning');
                }
                /*
                else if (!this.contractTypeValueIsValidForOppType()) {
                    this.showToastNotification('', 'Selected contract type not valid for the Opportunity type: ' + this.transferObjData.masterOpportunity.RecordType.Name, 'error');
                }
                */
                else if (!this.contractTypeIsCommercialOrHibrid()) {
                    this.createContractNotCommercialOrHibrid();
                }
                else {
                    this.currentStep = 's2';
                }
            } else {
                this.showToastNotification('', 'Please fill all the contract fields', 'warning');
            }
        }
    }

    areContractFieldsValid() {
        let country = this.transferObjData.contract.country;
        let type    = this.transferObjData.contract.type;
        
        if (country != null && country != '' && type != null && type != '') {
            return true;
        } else {
            return false;
        }
    }

    /*
    contractTypeValueIsValidForOppType() {
        const mainOppValidOptions = ["Hybrid Contract", "Commercial Contract", "Intention Contract"];
        const childOppValidOptions = ["Franchisee Assent Agreement", "Commercial Contract"];
        let contractTypeValueIsValid = true;
        let seletedContractType = this.transferObjData.contract.type;
        
        if (!childOppValidOptions.includes(seletedContractType) && this.transferObjData.masterOpportunity.RecordType.DeveloperName == 'Partner_Flow_Child_Opportunity') {
            contractTypeValueIsValid = false;
        } 
        
        if (!mainOppValidOptions.includes(seletedContractType) && this.transferObjData.masterOpportunity.RecordType.DeveloperName == 'Partner_Flow_Opportunity') {
            contractTypeValueIsValid = false;
        }

        return contractTypeValueIsValid;
    }
    */
    
    contractAlreadyCreated() {
        let contractSObj = this.transferObjData.contract.contractSobjectRecord;
   
        return (contractSObj != undefined && contractSObj != null ? true : false);
    }

    createContractNotCommercialOrHibrid() {
        this.handleDone();
    }

    handleTransferObjUpdate(event) {
        console.log('handleTransferObjUpdate');
        this.transferObjData = JSON.parse(JSON.stringify(event.detail));
        
        if (this.currentStep != 's1') {
            this.evalAndRequestNewTranferObjFromBackend();
        }
    }

    evalAndRequestNewTranferObjFromBackend() {
        this.showSpinner();

        evaluateTranferObj({ data: this.transferObjData })
            .then((result) => {
                this.hideSpinner();
                this.transferObjData = JSON.parse(JSON.stringify(result));
                this.evaluateSelectedLocations();
            })
            .catch((error) => {
                this.hideSpinner();
                console.log(error);
            });
    }

    evaluateSelectedLocations() {
        if (this.transferObjData.locations.length > 0) {
            if (this.areAllSelectedAccountsLocations()) {
                this.disableDoneButton = false;
            } else {
                this.disableDoneButton = true;
                this.showToastNotification('', 'Please select only locations', 'warning');
            }
        }
    }

    areAllSelectedAccountsLocations() {
        let allLocations = false;

        this.transferObjData.locations.every(function(element, index){
            if (element.Partner_Level__c != 'Location') {
                allLocations = false;
                return;
            } else {
                allLocations = true;
            }
        });

        return allLocations;
    }

    ifOppStageOfferApproved() {
        return ( getFieldValue(this.opportunity.data, STAGE_NAME) == 'Proposta Aprovada' ? true : false);
    }

    contractTypeIsCommercialOrHibrid() {
        let contractTypesForLocations = ['Hybrid Contract', 'Commercial Contract', 'Franchisee Assent Agreement'];
        const oppRecTypes = ['Partner_Flow_Opportunity', 'Partner_Flow_Child_Opportunity'];
        
        return (
            contractTypesForLocations.includes(this.transferObjData.contract.type)
            && oppRecTypes.includes(this.transferObjData.masterOpportunity.RecordType.DeveloperName)
        );
    }

    handleDone() {       
        if (this.currentStep == 's2' && this.transferObjData.locationsWithDifLegalRep.length > 0) {
            this.currentStep = 's3';
            return;
        }
       
        if (this.contractTypeIsCommercialOrHibrid() && this.transferObjData.locationsAccountIdList.length == 0) {
            this.showToastNotification('', 'Please select at least one location', 'warning');
            return;
        }

        this.showSpinner();  

        createContract({ data: this.transferObjData })
            .then((result) => {
                this.hideSpinner();
                this.transferObjData = {...result};
                this.showToastNotification('', 'Contract Agreement created!', 'success'); 
                this.isRedirect = true;
                refreshApex(this.backResult).then(()=> {
                    this.redirectToContractPage();
                });
               

            })
            .catch((error) => {
                this.hideSpinner();
                this.showToastNotification('Error: Please, contact a Salesforce Admin', error.body.message, 'error');
                console.log(error);
            });
    }

    handleCommercialValidation(event) {
        console.log('handleCommercial');
        let isLocationSelectionValidForCommercial = event.detail;
        console.log(this.transferObjData);

        if (this.transferObjData.contract.type == 'Commercial Contract' && this.transferObjData.masterOpportunity.RecordType.DeveloperName != 'Partner_Flow_Child_Opportunity') {
            if (!isLocationSelectionValidForCommercial) {
                this.disableDoneButton = true;
                this.showToastNotification('', 'For a Commercial Contract all locations and only locations must be selected', 'warning')
            } else {
                this.disableDoneButton = false;
            }
        }
    }

    showToastNotification(title, message, variant) {
        const evt = new ShowToastEvent({title: title, message: message, variant: variant});
        this.dispatchEvent(evt);
    }

    redirectToContractPage() {       
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: this.transferObjData.contract.contractSobjectRecord.Id,
                objectApiName: 'APXT_Redlining__Contract_Agreement__c',
                actionName: 'view'
            }
        });
    }

    showSpinner(){ this.spinner = true }
    hideSpinner(){ this.spinner = false }
}