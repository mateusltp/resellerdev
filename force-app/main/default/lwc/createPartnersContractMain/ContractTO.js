/**
 * @description       : Class to use to deep clone the transfer object from apex controller
 * @author            : alysson.mota@gympass.com
 * @group             : 
 * @last modified on  : 03-15-2022
 * @last modified by  : alysson.mota@gympass.com
**/

export class ContractTO {
    
}