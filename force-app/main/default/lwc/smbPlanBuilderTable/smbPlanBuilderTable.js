import { LightningElement, api } from 'lwc';

export default class SmbPlanBuilderTable extends LightningElement {

    @api actionsDisabled = false;

    privateProducts;

    waiversPerLine = 2;
    marginTop = -8.0;
    sumPerWaiverLine = -0.9;

    @api
    get products() {
        return privateProducts;
    }
    set products(products) {
        this.privateProducts = [];
        if(products) {
            products.forEach(elem => {
                let product = {...elem};
                product.link = '/' + product.Id;
                product.Discount = product.Discount;
                this.privateProducts.push(product);
            });
        }
    }

    handleActionSelect(event) {
        const value = event.detail.value;
        const recordId = event.target.dataset.id;
        const recordName = event.target.dataset.name;
        let evt;
        switch(value) {
            case "edit":
                evt = new CustomEvent(
                    'editproduct',
                    { detail: recordId }
                );
                break;
            case "delete":
                evt = new CustomEvent(
                    'deleteproduct',
                    { detail: { recordId: recordId, recordName: recordName } }
                );
                break;
        }
        this.dispatchEvent(evt);
    }

    onHoverProduct(event){
        let index = event.currentTarget.dataset.index;
        const waivers = this.privateProducts[index].Waivers__r;
        if(waivers && waivers.length > 0){
            this.calculatePopupTopMargin(index, waivers.length);
        }
        else {
            this.calculatePopupTopMargin(index, 0);
        }
    }

    calculatePopupTopMargin(index, waiversLength){
        let popover = this.template.querySelectorAll('.spnPopover')[index];
        let numberWaiverLines = Math.ceil(waiversLength / this.waiversPerLine);
        popover.style.marginTop = (this.marginTop + (this.sumPerWaiverLine * (numberWaiverLines - 1))) + 'rem';
    }

    

}