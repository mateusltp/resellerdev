import { LightningElement, api} from 'lwc';

export default class CreatePartnersContractLegalReps extends LightningElement {
    @api transferObjData;
    newLegalRepresentative = {}

    connectedCallback() {
        if (this.transferObjData.masterOpportunity.RecordType.DeveloperName == 'Partner_Flow_Child_Opportunity') {
            this.newLegalRepresentative = this.transferObjData.masterOpportunity.Legal_representative__r;
        } else {
            this.newLegalRepresentative = this.transferObjData.masterAccount.Legal_Representative__r;
        }
    }
}