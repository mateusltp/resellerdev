import { LightningElement, api, track  } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getSkuProducts from '@salesforce/apex/SmbSkuAddProductsController.getSkuProducts';


export default class SmbSkuAddProducts extends LightningElement {

    @api recordId;

    @api currencyIsoCode;

    isLoading = false;

    @track products = [];

    selectedProducts = [];

    showModal = false;

    showSelectProducts = true;

    hasRendered = false;

    get modalClass() {
		return `slds-modal slds-modal_medium ${this.showModal ? "slds-fade-in-open" : ""}`;
	}

    get modalBackdropClass() {
		return `slds-backdrop ${this.showModal ? "slds-backdrop_open" : ""}`;
	}

    connectedCallback() {
        getSkuProducts({ opportunityId: this.recordId })
            .then(result => {
                result.forEach(element => {
                    let product = {...element};
                    product.CurrencyIsoCode = this.currencyIsoCode;
                    product.Subtotal = product.Total_Price__c;
                    this.products.push(product);
                });
            })
            .catch(error => {
                console.error(error);
                this.showToast('Error', error.body.message, 'error');
            })
    }

    renderedCallback() {
        if(!this.hasRendered) {
            this.hasRendered = true;
            // this timeout is for visual purposes only - this way the fade-in effect can be seen
            setTimeout(() => {
                this.showModal = true;
                }, "50");
        }
    }

    close() {
        this.showModal = false;
        this.fireCloseEvent();
    }

    handleClose() {
        this.close();
    }
    
    handleContinue(event) {
        this.selectedProducts = event.detail;
        this.showSelectProducts = false;
    }

    handleBack(event) {
        this.selectedProducts = event.detail;
        this.showSelectProducts = true;
    }

    handleLoading(event) {
        this.isLoading = event.detail;        
    }

    fireCloseEvent() {
        const evt = new CustomEvent('close');
        this.dispatchEvent(evt);
    }

    showToast(title, message, variant) {
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant,
        });
        this.dispatchEvent(evt);
    }

    
}