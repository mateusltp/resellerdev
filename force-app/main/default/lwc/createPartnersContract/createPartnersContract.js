import { LightningElement, wire, api } from 'lwc';
import { getObjectInfo, getPicklistValues } from 'lightning/uiObjectInfoApi';
import CONTRACT_OBJECT from '@salesforce/schema/APXT_Redlining__Contract_Agreement__c';
import COUNTRY from '@salesforce/schema/Account.BillingCountry';
import COTRACT_TYPE from '@salesforce/schema/APXT_Redlining__Contract_Agreement__c.Type_of_contract__c';
import ACCOUNT_OBJECT from '@salesforce/schema/Account';

export default class CreatePartnersContract extends LightningElement {
    @api recordId;
    @api transferObjData;
    
    transferObjDataClone;
    contractRectTypeId = '';
    typeOfContractOptions = [];
    countryOptions = this.getCountryOptions();
    contract = {};
    countryValue = '';
    typeValue = '';

    connectedCallback() {
        this.setContracTypeValues();
        this.setInputValues();
    }

    setInputValues() {
        if (this.transferObjData.contract.country != null && this.transferObjData.contract.type != null) {
            this.countryValue = this.transferObjData.contract.country;
            this.typeValue = this.transferObjData.contract.type;
        } else {
            this.instantiateCloneTO();

            if (this.transferObjData.masterOpportunity.RecordType.DeveloperName == 'Partner_Flow_Child_Opportunity') {
                let type = 'Franchisee Assent Agreement';
                this.typeValue = type;
                this.transferObjDataClone.contract.type = type;
            } 

            let country = this.transferObjData.masterAccount.BillingCountry;
            this.countryValue = country;
            this.transferObjDataClone.contract.country = country;

            this.updateMainTransferObj();
        }
    }

    updateTransferObj(event) {
        try {
            this.instantiateCloneTO();
    
            switch (event.target.name) {
                case 'country':
                    this.transferObjDataClone.contract.country = event.detail.value;
                    break;
                case 'type':
                    this.transferObjDataClone.contract.type = event.detail.value;
                    break;
                default:
                  console.log('Name not mapped');
            }
            console.log(JSON.stringify(this.transferObjDataClone.contract));
    
            this.updateMainTransferObj();
        } catch(error) {
            console.log(error);
        }
    }

    instantiateCloneTO() {
        if (this.transferObjDataClone == null || this.transferObjDataClone == undefined) {
            this.transferObjDataClone = JSON.parse(JSON.stringify(this.transferObjData));
        }
    }
    
    closeModal() {
        this.dispatchEvent(new CustomEvent("closemodal"));
    }

    updateMainTransferObj() {
        console.log('updateMainTransferObj');
        const transferobjupdate = new CustomEvent("updateto", {
            detail: this.transferObjDataClone
        });
    
        this.dispatchEvent(transferobjupdate);
    }

    setContracTypeValues() {
        let oppRecType = this.transferObjData.masterOpportunity.RecordType.DeveloperName;
        
        if (oppRecType == 'Partner_Flow_Child_Opportunity') {
            this.typeOfContractOptions = this.getContractTypesForChildOpp();
        } else {
            this.typeOfContractOptions = this.getContractTypesForParentOpp();
        }
    }

    getContractTypesForParentOpp() {
        return [
            {value: '',                     label: '--None--'},
            {value: 'Hybrid Contract',      label: 'Hybrid Contract'},
            {value: 'Commercial Contract',  label: 'Commercial Contract'},
            {value: 'Intention Contract',   label: 'Intention Contract'}
        ];
    }

    getContractTypesForChildOpp() {
        return [
            {value: '',                                 label: '--None--'},
            {value: 'Commercial Contract',              label: 'Commercial Contract'},
            {value: 'Franchisee Assent Agreement',      label: 'Franchisee Assent Agreement'}
        ];
    }

    getCountryOptions() {
        return [
            {value: '',                 label: '--None--'       },
            {value: 'Argentina',        label: 'Argentina'      },
            {value: 'Brazil',           label: 'Brazil'         },
            {value: 'Chile',            label: 'Chile'          },
            {value: 'Colombia',         label: 'Colombia'       },
            {value: 'France',           label: 'France'         },
            {value: 'Germany',          label: 'Germany'        },
            {value: 'Ireland',          label: 'Ireland'        },
            {value: 'Italy',            label: 'Italy'          },
            {value: 'Mexico',           label: 'Mexico'         },
            {value: 'Portugal',         label: 'Portugal'       },
            {value: 'Spain',            label: 'Spain'          },
            {value: 'United Kingdom',   label: 'United Kingdom' },
            {value: 'United States',    label: 'United States'  },
        ];
    }
}