import { LightningElement, api } from 'lwc';

export default class GenericInput extends LightningElement {

    @api label;
    @api type;
    @api disabled;
    @api value;
    @api valueString;
    @api valueDate;

    connectedCallback() {
        if(this.valueString != null)
            this.value = this.valueString;
        if(this.valueDate != null)
            this.value = this.valueDate;

    }

}