import { LightningElement, api} from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class PartnerSetupEditAllMenu extends LightningElement {
    @api selectedRecords;   
    @api isUs;
    @api receivedData;
    showEditScreen;
    selectedOption

    closeAction() {
        const closeEvt = new CustomEvent('closemenumodal');
        this.dispatchEvent(closeEvt);
    }

    handleScreen(){
        if(this.selectedOption != null){
            this.showEditScreen = true;
        } else {
            this.dispatchToast("Ops!", "warning", "Please, select one option");
        }
        
    }

    closeEditModal(){
        this.showEditScreen = false;
        this.closeAction();
    }


    handleOptionChoice(event){
        this.selectedOption = event.detail.value;
    }

    get options() {
        if( this.isUs ){
            return [
                { label: 'Bank account', value: 'isEditBankAccount' },
                { label: 'Description', value: 'isEditDescription' },
                { label: 'Email', value: 'isEditEmail' },
                { label: 'Logo', value: 'isEditLogo' },
                { label: 'Opening Hours', value: 'isEditOpenHours' },
                { label: 'Phone number', value: 'isEditPhone' },
                { label: 'Photos', value: 'isEditPhotos' },
                { label: 'Validation type', value: 'isEditValidationType' },
                { label: 'Website', value: 'isEditWebsite' },
                { label: 'W9 form', value: 'isEditW9' },
            ];
        } else {
            return [              
                { label: 'Bank account', value: 'isEditBankAccount' },
                { label: 'Description', value: 'isEditDescription' },
                { label: 'Email', value: 'isEditEmail' },
                { label: 'Logo', value: 'isEditLogo' },
                { label: 'Opening Hours', value: 'isEditOpenHours' },
                { label: 'Phone number', value: 'isEditPhone' },
                { label: 'Photos', value: 'isEditPhotos' },
                { label: 'Validation type', value: 'isEditValidationType' },
                { label: 'Website', value: 'isEditWebsite' }
            ];
        }
      
    }

    dispatchToast(title, variant, message) {
        let successtEvnt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant
        });
        this.dispatchEvent(successtEvnt);
    }

}