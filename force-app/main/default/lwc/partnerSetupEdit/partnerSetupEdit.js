import { LightningElement,track, api, wire } from 'lwc';
import apexSearch from '@salesforce/apex/SearchCustomLookupController.bankAccountSearch';
import { reduceErrors } from 'c/ldsUtils';
import W9Form from '@salesforce/resourceUrl/W9Form';
import { refreshApex } from '@salesforce/apex';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import deleteFiles from '@salesforce/apex/PartnerSetupController.deleteFiles';
import updatePartnerFromSetup from '@salesforce/apex/PartnerSetupController.updatePartnerFromSetup';
import FILE_TYPE from '@salesforce/schema/FIleObject__c.File_Type__c';
import ACCOUNT_DESCRIPTION from '@salesforce/schema/Account.Description';
import ACCOUNT_OBJ from '@salesforce/schema/Account';
import BANK_ACCOUNT_OBJ from '@salesforce/schema/Bank_Account__c';
import OPENING_HOURS_OBJ from '@salesforce/schema/Opening_Hours__c';
import { handleValidation } from 'c/customLayoutUtils';

export default class PartnerSetupEdit extends LightningElement {

    W9FormURL = W9Form;
    @api selectedRecords;
    @api selectedOption;
    @api receivedData;
    @api accountId;

    currentRecord;
    isEditWebsite;
    isEditPhone;
    isEditEmail;
    isEditValidationType;
    isEditDescription;
    isEditPhotos;
    isEditLogo;
    isEditW9;
    isEditOpenHours;
    isEditBankAccount;
    isClose = false;
    isEditAll;
    showSpinner = false;
    activeSection = 'general';
    accountSectionLabel = 'General'
    existingBankAccount;
    existingOpeningHours;
    @track logos = [];
    @track photos = [];
    @track w9s = [];
    counter = 0;
    @api handleDescription = false;
 
    get bankAccountId(){
        return this.currentRecord && this.currentRecord.partnerBankAccountId != null ? this.currentRecord.partnerBankAccountId : this.existingBankAccount;
    }

    get openHoursId(){
        return this.currentRecord && this.currentRecord.partnerOpeningHoursId != null ? this.currentRecord.partnerOpeningHoursId : '';
    }

    get editAccountSection(){
        return this.isEditSingleAccount || (!this.isEditSingleAccount && (this.isEditDescription || this.isEditEmail || this.isEditValidationType || this.isEditPhone || this.isEditWebsite ));
    }

    get modalContentClass(){        
        return this.isEditValidationType && !this.isEditSingleAccount ? 'slds-modal__content slds-p-around_medium slds-scrollable slds-relative c-modalcontent' : 'slds-modal__content slds-p-around_medium c-align-center slds-scrollable slds-relative';
    }

    get logoFilelenght(){
        return this.logos.length;
    }

    get photosFilelenght(){
        return this.photos.length > 4 ? 4 : this.photos.length;
    }

    get w9Filelenght(){
        return this.w9s.length;
    }

    get photosUploadedPercentage(){        
        return 'width: ' + Math.ceil((this.photos.length/4) * 100) + '%';;
    }

    get acceptedLogoFormats() {
        return ['.jpeg', '.jpg', '.png'];
    }

    get acceptedPhotosFormats() {
        return ['.jpeg','.jpg', '.png'];
    }

    get acceptedW9Formats() {
        return ['.pdf'];
    }

    get isBankAccountDisabled(){
        return this.existingBankAccount != null ? true : false;
    }

    get logoType(){
        return "Logo";
    }

    get photoType(){
        return "Photo";
    }

    get counterClass(){
        if (this.counter < 100) {
            return 'slds-p-left_x-small slds-text-body_small'
        } else if (this.counter >=100 && this.counter <=500) {
            return 'slds-text-color_success slds-p-left_x-small slds-text-body_small'
        } else {
            return 'slds-text-color_destructive slds-p-left_x-small slds-text-body_small'
        }

    }

    handleCharCount(event){
        this.handleDescription = true;
        this.counter = 0;
        const userCharInput = event.target.value.length;
        this.counter = this.counter + userCharInput;
    }

    connectedCallback(){
        if(this.receivedData != null){
            console.log('JSON.stringify(this.receivedData)');
            console.log(JSON.stringify(this.receivedData));
            let index =  this.receivedData.findIndex(o => o.accountId == this.accountId);
            if(index != -1) {
                this.currentRecord = this.receivedData[index];
                this.logos = [...this.currentRecord.partnerLogo];
                this.photos = [...this.currentRecord.partnerPhotos];
                if(Array.isArray(this.currentRecord.partnerW9)){
                    this.w9s = [...this.currentRecord.partnerW9];
                }
                
            }
        }
        this.setScreenLayout();
    }


    handleSearch(event) {
        const lookupElement = event.target;
        apexSearch(event.detail)
            .then(results => {
                lookupElement.setSearchResults(results);
            })
            .catch(error => {
                reduceErrors(error);
            });
    }

    setExistingBankAccount(event){
        this.existingBankAccount = event.detail[0];
    }

    closeAction() {
        this.isClose = true;
        this.handleFileDelete();    
    }

    setScreenLayout(){

        this.showFieldOrSectionInScreen(false);
        this.selectedOption = this.selectedOption != null ? this.selectedOption : 'isEditSingleAccount';

        switch (this.selectedOption) {
            case 'isEditWebsite':
                this.isEditWebsite = true;
                this.activeSection = 'general';
                this.accountSectionLabel = 'Website';
                break;
            case 'isEditPhone':
                this.isEditPhone = true;
                this.activeSection = 'general';
                this.accountSectionLabel = 'Phone';
                break;
            case 'isEditEmail':
                this.isEditEmail = true;                    
                this.activeSection = 'general';
                this.accountSectionLabel = 'Email';
                break;
            case 'isEditValidationType':
                this.isEditValidationType = true;                    
                this.activeSection = 'general';
                this.accountSectionLabel = 'Validation type';
                break;
            case 'isEditDescription':
                this.isEditDescription = true;                    
                this.activeSection = 'general';
                this.accountSectionLabel = 'Description';
                break
            case 'isEditPhotos':
                this.isEditPhotos = true;
                this.activeSection = 'photos';
                break
            case 'isEditLogo':
                this.isEditLogo = true;
                this.activeSection = 'logo';
                break
            case 'isEditW9':
                this.isEditW9 = true;
                this.activeSection = 'w9';
                break
            case 'isEditOpenHours':
                this.isEditOpenHours = true;
                this.activeSection = 'opening_hours';
                 break
            case 'isEditBankAccount':
                this.isEditBankAccount = true;
                this.activeSection = 'bank_account';                    
                break
            case 'isEditSingleAccount':
                this.isEditSingleAccount = true;
                this.showFieldOrSectionInScreen(true);
                break
            }
    }

    showFieldOrSectionInScreen(trueOrFalse){       
        this.isEditWebsite = trueOrFalse;
        this.isEditPhone = trueOrFalse;
        this.isEditEmail = trueOrFalse;
        this.isEditValidationType = trueOrFalse;
        this.isEditDescription = trueOrFalse;
        this.isEditPhotos = trueOrFalse;
        this.isEditLogo = trueOrFalse;
        this.isEditW9 = trueOrFalse;
        this.isEditOpenHours = trueOrFalse;
        this.isEditBankAccount = trueOrFalse;
    }

    handleLogoUploadFinished(event) {
        const uploadedFiles = event.detail.files;
        console.log('FIELDSSSS >> ' + JSON.stringify(uploadedFiles));
        for(let file of uploadedFiles){         
            file["fileType"] = 'Logo';
            file["entityId"] = this.accountId != null ? this.accountId  : '';
            file['isInsert'] = true;
            this.logos.push(file);
        }
    }

    handlePhotoUploadFinished(event) {
        const uploadedFiles = event.detail.files;
        for(let file of uploadedFiles){         
            file["fileType"] = 'Photo';
            file["entityId"] = this.accountId != null ? this.accountId  : '';
            file['isInsert'] = true;
            this.photos.push(file);
        }
    }

    handleW9UploadFinished(event) {
        const uploadedFiles = event.detail.files;
        for(let file of uploadedFiles){         
            file["fileType"] = 'W9';
            file["entityId"] = this.accountId != null ? this.accountId  : '';
            file['isInsert'] = true;
            this.w9s.push(file);
        }
    }

    handleFileDelete(event){
        this.showSpinner = true;
        let filesToDelete = [];
        let files = [];
        files.push(...this.logos);
        files.push(...this.w9s);
        files.push(...this.photos);

        let docId = event && event.target && event.target.dataset.documentid ? event.target.dataset.documentid : null;
        console.log('docId ' + docId);
        if(docId != null){ //it means we click in delete button, else it means we close window
            console.log('new file');
            console.log(JSON.stringify(files));
            let index =  files.findIndex(o => o.documentId == docId);
            if(index != -1) {
                let file = files[index];
                filesToDelete.push(file);
            }
        } else {
            if(this.isClose){
                console.log('isclose');                
                filesToDelete.push(...files.filter( file =>  file.documentId != null ));
            } else {
                console.log('isnotclose');
                let contVersionId = event && event.target && event.target.dataset.contentversionid ? event.target.dataset.contentversionid : null;
                filesToDelete.push(...files.filter( file => String(file.contentVersionId) == contVersionId ));
            }
        }
        console.log('JSON.stringify(filesToDelete)');
        console.log( JSON.stringify(filesToDelete) );
        if(filesToDelete.length > 0){
            deleteFiles({files: JSON.stringify(filesToDelete), deleteOldFiles: !this.isClose})
            .then((data) => {
                for(let file of filesToDelete){
                    console.log('FILE array');
                    console.log(JSON.stringify(file));
                    if(file.fileType === 'Logo'){
                        this.removeFromArrayOfFiles(this.logos, file.documentId);
                    } else if(file.fileType === 'Photo'){
                        this.removeFromArrayOfFiles(this.photos, file.documentId);
                    } else if(file.fileType === 'W9'){
                        this.removeFromArrayOfFiles(this.w9s, file.documentId);
                    }
                }                 
                this.showSpinner = false;
                if(this.isClose){
                    const closeEvt = new CustomEvent('closeeditmodal');
                    this.dispatchEvent(closeEvt);
                }
            })
            .catch((error) => {
                this.dispatchToast('Error', 'error', reduceErrors(error)[0]);
                this.showSpinner = false;
            });
        } else {
            this.showSpinner = false; 
            const closeEvt = new CustomEvent('closeeditmodal');
            this.dispatchEvent(closeEvt);
        }
    }

    removeFromArrayOfFiles(arrayOfFiles, key){
        let index =  arrayOfFiles.findIndex(o => o.documentId == key);                         
        if(index != -1) {
            arrayOfFiles.splice(index, 1);
        }
    }

    
    dispatchToast(title, variant, message) {
        let successtEvnt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant
        });
        this.dispatchEvent(successtEvnt);
    }

    handleSave(){

        let recordsInput = new Object();
        let idsToUpdate = [];
        //start of account object
        let accountFields = this.template.querySelectorAll('.account');        
        if(accountFields.length > 0 ){
            if(handleValidation(accountFields)){
                let accountRecordInput = this.populateRecordInput(ACCOUNT_OBJ.objectApiName,this.accountId, accountFields);
                if(accountRecordInput.fields[ACCOUNT_DESCRIPTION.fieldApiName] != undefined){
                    if(accountRecordInput.fields[ACCOUNT_DESCRIPTION.fieldApiName].length < 100 || accountRecordInput.fields[ACCOUNT_DESCRIPTION.fieldApiName].length > 500){
                        this.dispatchToast('Warning!','warning', 'Description should have between 100 and 500 characteres.' );
                        return;
                    }
                }           

                recordsInput.partner = accountRecordInput;
            } else {
                return;
            }
        }

        //start of bank_account object
        let bankAccountFields = this.template.querySelectorAll('.bank_account');
        if(bankAccountFields.length > 0){
            if(!this.isEditSingleAccount){
                if(handleValidation(bankAccountFields)){
                    let bankAccountRecordInput = this.populateRecordInput(BANK_ACCOUNT_OBJ.objectApiName, this.existingBankAccount, bankAccountFields);
                    bankAccountRecordInput['accountId'] = this.accountId;
                    recordsInput.bankAccount = bankAccountRecordInput;
                } else {
                    return;
                }
            } else {
                let bankAccountRecordInput = this.populateRecordInput(BANK_ACCOUNT_OBJ.objectApiName, this.existingBankAccount, bankAccountFields);
                bankAccountRecordInput['accountId'] = this.accountId;
                recordsInput.bankAccount = bankAccountRecordInput;
            }
          
        }

        //start of OPENING_HOURS object
        let openHoursFields = this.template.querySelectorAll('.opening_hours');
        if(openHoursFields.length > 0 ){
            if(handleValidation(openHoursFields)){
                let existingOpenHoursId = this.currentRecord && this.currentRecord.partnerOpeningHoursId ? this.currentRecord.partnerOpeningHoursId : null;
                let openHoursRecordInput = this.populateRecordInput(OPENING_HOURS_OBJ.objectApiName, existingOpenHoursId, openHoursFields);
                openHoursRecordInput.fields['Account_Related__c'] = this.accountId;
                if(!this.isEditSingleAccount && (openHoursRecordInput.fields['Days__c'] == null || openHoursRecordInput.fields['Days__c'] == '')){
                    this.dispatchToast('Ops', 'warning','Select, please fill in Days for Opening Hours.');
                    return;
                }
                recordsInput.openingHours = openHoursRecordInput;
            } else{
                return;
            }
        } 

        
        if(this.logos.length > 1){
            this.dispatchToast('Ops', 'warning','Select only 1 logo to upload. Please, delete one.');
            return;
        }

        // if(this.photos.length > 4){
        //     this.dispatchToast('Ops', 'warning','Select only 4 photos to upload. Please, delete one.');
        //     return;
        // }

        if(this.w9s.length > 1){
            this.dispatchToast('Ops', 'warning','Select only 1 W9 form to upload. Please, delete one.');
            return;
        }

        let files = [];
        files.push(...this.logos);
        files.push(...this.photos);
        files.push(...this.w9s);
        recordsInput.files = [...files];

       
        if(!this.isEditSingleAccount){
            for(let record of this.selectedRecords){
                idsToUpdate.push(record.accountId);
            }
        }
        
        console.log('this.recordsInput');
        console.log(JSON.stringify(recordsInput));
        this.commit(recordsInput, idsToUpdate);

    }

    populateRecordInput(objApiName, salesforceId, inputFields){      
        let recordInput = this.recordInputForCreate(objApiName);
        recordInput.fields["Id"] = salesforceId != null ? salesforceId : '';
        for(let inputField of inputFields){
            recordInput.fields[inputField.fieldName] = inputField.value;
        }       
        return recordInput
    }

    recordInputForCreate(objApiName) {
        let recordInput = {
            "apiName": objApiName,
            "fields": {
            }
        }
        return recordInput;
    }

    commit(recordInputs, idsToUpdate){
        this.showSpinner = true;
        updatePartnerFromSetup({partnersToUpdate: JSON.stringify(recordInputs), selectedRecords: idsToUpdate})
        .then((data) => {
            this.dispatchToast('Saved!', 'success', 'Partner(s) updated.');
            this.showSpinner = false;
            const closeEvt = new CustomEvent('closeeditmodal');
            this.dispatchEvent(closeEvt);
        })
        .catch((error) => {
            this.dispatchToast('Error', 'error', reduceErrors(error)[0]);
            this.showSpinner = false;
        });
    }

    get getOpenHourDays(){
        if(this.currentRecord == null){
            return 'Monday';
        }
    }
}