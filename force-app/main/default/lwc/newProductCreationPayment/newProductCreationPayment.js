import { LightningElement, track, wire, api } from 'lwc';
import { getRecordCreateDefaults } from 'lightning/uiRecordApi';
import { getNewLayoutTemplate, disableFieldsFromSection, enableFieldsFromSection, recordInputForCreate, handleValidation } from 'c/customLayoutUtils';
import COMMERCIAL_CONDITION_OBJECT from '@salesforce/schema/Commercial_Condition__c';

export default class NewProductCreationPayment extends LightningElement {

    @api partnerProductRecordTypes;
    @api partnerProductTo = [];
    @api requiresRadionButton;
    @api radioPaymentGroupOptions = {};
    @api isClone;
    @api isEdit;

    @track showSpinner = false;
    @track sectionWithFieldsAdvPayment = [];
    @track sectionWithFieldsMinGuaranteed = [];
    @track sectionWithFieldsCannibalization = [];
    @track advPaymentSelectedValue;
    @track minGuaranteedSelectedValue;
    @track cannibalizationSelectedValue;

    sectionByName = [];
    sectionsToExclude = ['Information'];
    fieldsToExclude = [];
    isAdvancedPaymentReady = false;
    isMinimumGuaranteed = false;
    isCannibalization = false;

    get options() {
        return [
            { label: 'Yes', value: 'Yes' },
            { label: 'No', value: 'No' },
        ];
    }


    @wire(getRecordCreateDefaults, { objectApiName: COMMERCIAL_CONDITION_OBJECT.objectApiName, recordTypeId: '$partnerProductRecordTypes.advancedPaymentRecordTypeId' })
    productTemplateAdvancedPayment({ error, data }) {
        if (data) {
            const template = getNewLayoutTemplate(data, this.sectionsToExclude, this.fieldsToExclude);
            this.sectionWithFieldsAdvPayment = template.sectionWithFields;
            this.sectionByName.push({ key: this.sectionWithFieldsAdvPayment[0].key, value: this.sectionWithFieldsAdvPayment });
            this.loadFieldsValueFromPartnerCommercialConditionTo(this.sectionWithFieldsAdvPayment, this.partnerProductRecordTypes.advancedPaymentRecordTypeName);
            this.isAdvancedPaymentReady = true;
        } else if (error) {
            console.log('error ' + JSON.stringify(error));
        }
    };


    @wire(getRecordCreateDefaults, { objectApiName: COMMERCIAL_CONDITION_OBJECT.objectApiName, recordTypeId: '$partnerProductRecordTypes.minimumGuaranteedRecordTypeId' })
    productTemplateMinimumGuaranteed({ error, data }) {
        if (data) {
            const template = getNewLayoutTemplate(data, this.sectionsToExclude, this.fieldsToExclude);
            this.sectionWithFieldsMinGuaranteed = template.sectionWithFields;
            this.sectionByName.push({ key: this.sectionWithFieldsMinGuaranteed[0].key, value: this.sectionWithFieldsMinGuaranteed });
            this.loadFieldsValueFromPartnerCommercialConditionTo(this.sectionWithFieldsMinGuaranteed, this.partnerProductRecordTypes.minimumGuaranteedRecordTypeName);
            this.isMinimumGuaranteed = true;
        } else if (error) {
            console.log('error ' + JSON.stringify(error));
        }
    };

    @wire(getRecordCreateDefaults, { objectApiName: COMMERCIAL_CONDITION_OBJECT.objectApiName, recordTypeId: '$partnerProductRecordTypes.cannibalizationRecordTypeId' })
    productTemplateCannibalization({ error, data }) {
        if (data) {
            const template = getNewLayoutTemplate(data, this.sectionsToExclude, this.fieldsToExclude);
            this.sectionWithFieldsCannibalization = template.sectionWithFields;
            this.sectionByName.push({ key: this.sectionWithFieldsCannibalization[0].key, value: this.sectionWithFieldsCannibalization });
            this.loadFieldsValueFromPartnerCommercialConditionTo(this.sectionWithFieldsCannibalization, this.partnerProductRecordTypes.cannibalizationRecordTypeName);
            this.isCannibalization = true;
        } else if (error) {
            console.log('error ' + JSON.stringify(error));
        }
    };

    isLayoutReady(){
        if(this.isAdvancedPaymentReady && this.isMinimumGuaranteed && this.isCannibalization){
            this.showSpinner = false;
        }
    }

    disconnectedCallback(){
        this.handlePaymentCommercialTOCreation();
        this.fireEventWithRadioButtonValue();
    }


    handleOptionChange(event) {
        let sectionName = event.currentTarget.name;
        let selectedOption = event.detail.value;
        let sectionToHandle = this.sectionByName.find(o => o.key == sectionName).value;

        if(sectionName.includes('Advanced')){
            this.advPaymentSelectedValue = selectedOption;
        } else if(sectionName.includes('Minimum')) {           
             this.minGuaranteedSelectedValue = selectedOption;            
        } else if(sectionName.includes('Cannibalization')) {
            this.cannibalizationSelectedValue = selectedOption;
        }

        if (selectedOption == 'Yes') {
            enableFieldsFromSection(sectionToHandle);
        } else {
            disableFieldsFromSection(sectionToHandle, true);
        }

    }

    handlePaymentCommercialTOCreation() {

        const inputAdvPayment = this.template.querySelectorAll('.c-input-comm-advpayment');
        const inputMinGuaranteed = this.template.querySelectorAll('.c-input-comm-minguaranteed');
        const inputCannibalization = this.template.querySelectorAll('.c-input-comm-cannibalization');

        if (this.validateEmptyFieldsForTO(inputAdvPayment)) {
            let CommercialAdvPaymentRecordInput = this.populateRecordInput(inputAdvPayment, COMMERCIAL_CONDITION_OBJECT.objectApiName, this.partnerProductRecordTypes.advancedPaymentRecordTypeId);
            this.populateProductTO(this.partnerProductRecordTypes.advancedPaymentRecordTypeName, CommercialAdvPaymentRecordInput);
        } else {
            this.partnerProductTo.removeCommercialCondition(this.partnerProductRecordTypes.advancedPaymentRecordTypeName);
        }
        if (this.validateEmptyFieldsForTO(inputMinGuaranteed)) {
            let CommercialMinGuaranteedRecordInput = this.populateRecordInput(inputMinGuaranteed, COMMERCIAL_CONDITION_OBJECT.objectApiName, this.partnerProductRecordTypes.minimumGuaranteedRecordTypeId);
            this.populateProductTO(this.partnerProductRecordTypes.minimumGuaranteedRecordTypeName, CommercialMinGuaranteedRecordInput);
        } else {
            this.partnerProductTo.removeCommercialCondition(this.partnerProductRecordTypes.minimumGuaranteedRecordTypeName);
        }
        if (this.validateEmptyFieldsForTO(inputCannibalization)) {
            let CommercialCannibalizationRecordInput = this.populateRecordInput(inputCannibalization, COMMERCIAL_CONDITION_OBJECT.objectApiName, this.partnerProductRecordTypes.cannibalizationRecordTypeId);
            this.populateProductTO(this.partnerProductRecordTypes.cannibalizationRecordTypeName, CommercialCannibalizationRecordInput);
        } else {
            this.partnerProductTo.removeCommercialCondition(this.partnerProductRecordTypes.cannibalizationRecordTypeName);
        }
    }

    populateRecordInput(inputFields, objectApiName, recordTypeId) {
        let recordInput = recordInputForCreate(objectApiName);
        inputFields.forEach(field => {
            recordInput.fields[String(field.name)] = field.value;
        });
        recordInput.fields['RecordTypeId'] = recordTypeId;
        return recordInput;
    }

    populateProductTO(aKey, aValue) {
        let commercialObject = { key: aKey, value: aValue };
        this.partnerProductTo.addNewCommercialConditionDetails(commercialObject);
    }

    @api
    handlePaymentValidation() {
        var inputFields = [];
        var templateFields = this.template.querySelectorAll('lightning-input-field');
        var templateRadioGroups = this.template.querySelectorAll('lightning-radio-group');
        inputFields.push(...templateFields);
        inputFields.push(...templateRadioGroups);
        const isCorrectInput = handleValidation(inputFields);
        if(isCorrectInput){
            this.handlePaymentCommercialTOCreation();
        }
        return isCorrectInput;
    }

    validateEmptyFieldsForTO(inputFields) {
        for (let i = 0; i < inputFields.length; i++) {
            let field = inputFields[i];
            if (field.disabled == false && field.required && field.value != null) {
                return true;
            }
        }
        return false;
    }

    loadFieldsValueFromPartnerCommercialConditionTo(sectionWithFields, recordTypeName) {
        if (this.partnerProductTo == null || this.partnerProductTo == undefined)
            return;
            
        console.log('___________________________________');
        console.log('loadFieldsValueFromPartnerCommercialConditionTo ' + recordTypeName );
        if (this.partnerProductTo.getCommercialDetails().length > 0 && this.partnerProductTo.getCommercialCondition(recordTypeName) != undefined) {
            sectionWithFields.forEach(section => {
                section.value.forEach(field => {
                    field.value = this.partnerProductTo.getCommercialCondition(recordTypeName).value.fields[String(field.apiName)];
                    console.log(field.apiName);
                    console.log(field.value);

                    if (field.value != null) {
                        this.enableSectionAndSetRadionButton(sectionWithFields, recordTypeName);
                    }
                })
            })
        } else {
            this.disableSectionAndSetRadionButton(sectionWithFields, recordTypeName);
        }
    }

    disableSectionAndSetRadionButton(sectionWithFields, radioGroupIdentifier) {

        console.log('disableSectionAndSetRadionButton');
        console.log(JSON.stringify(this.radioPaymentGroupOptions));

        if(this.requiresRadionButton && (this.isClone || this.isEdit)){
            this.setRadioButton(radioGroupIdentifier, 'No');
        }

        if (!this.requiresRadionButton) {       
            this.advPaymentSelectedValue = this.radioPaymentGroupOptions.advPaymentSelectedValue;
            this.minGuaranteedSelectedValue = this.radioPaymentGroupOptions.minGuaranteedSelectedValue;
            this.cannibalizationSelectedValue = this.radioPaymentGroupOptions.cannibalizationSelectedValue;
            
        }
        disableFieldsFromSection(sectionWithFields, true);
    }

    enableSectionAndSetRadionButton(sectionWithFields, radioGroupIdentifier) {
        this.setRadioButton(radioGroupIdentifier, 'Yes');
        enableFieldsFromSection(sectionWithFields);
    }

    setRadioButton(radioGroupIdentifier, option) {

        switch (radioGroupIdentifier) {
            case this.partnerProductRecordTypes.advancedPaymentRecordTypeName:
                this.advPaymentSelectedValue = option;
                break;
            case this.partnerProductRecordTypes.minimumGuaranteedRecordTypeName:
                this.minGuaranteedSelectedValue = option;
                break;
            case this.partnerProductRecordTypes.cannibalizationRecordTypeName:
                this.cannibalizationSelectedValue = option;
                break;
        }
    }
    
    fireEventWithRadioButtonValue(){
        console.log('fireEventWithRadioButtonValue');
        const selectedRadioEvt= new CustomEvent('selectedradio', { detail:
            {   name : 'Payment',
                advPaymentSelectedValue : this.advPaymentSelectedValue,
                minGuaranteedSelectedValue :  this.minGuaranteedSelectedValue, 
                cannibalizationSelectedValue : this.cannibalizationSelectedValue } }
            );

        this.dispatchEvent(selectedRadioEvt);
    }
}