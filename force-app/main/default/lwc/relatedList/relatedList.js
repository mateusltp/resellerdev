/**
 * @description       : 
 * @author            : tiago.ribeiro@gympass.com
 * @group             : 
 * @last modified on  : 06-21-2022
 * @last modified by  : tiago.ribeiro@gympass.com
**/
import { LightningElement, track, api, wire } from "lwc";
import {getRecord} from 'lightning/uiRecordApi';
import FASTTRACK_STAGE from '@salesforce/schema/Opportunity.FastTrackStage__c';
import { NavigationMixin } from "lightning/navigation";
import RelatedListHelper from "./relatedListHelper";
import {loadStyle} from 'lightning/platformResourceLoader';
import relatedListResource from '@salesforce/resourceUrl/relatedListResource';
import { subscribe } from 'lightning/empApi';
import {MessageContext} from 'lightning/messageService';

export default class RelatedList extends NavigationMixin(LightningElement) {
    @track state = {}
    @track buttonsDisabled = true;
    @api recordId;
    @api sobjectApiName;
    @api relatedFieldApiName;
    @api relatedFromChildObject = false;
    @api relatedChildObjectApiName;
    @api relatedChildObjectFieldApiName;
    @api numberOfRecords = 10;
    @api sortedBy;
    @api sortedDirection;
    @api fields;
    @api columns;
    @api iconDesignation;
    @api overrideRelatedListName;
    @api overrideActionsWithFlow;
    @api popoverTitleField;
    @api popoverFields;
    @api retrieveWaivers;
    @api showSpinner = false;
    @api activateNavigation;
    @api enabledButtonsStages;

    channelName = '/data/OpportunityChangeEvent';
    updateOfferChannelName = '/event/FastTrack_Component_Event__e';
    subscrition = {};
    
    isRefresh = false;
    fastTrackStage;
    popoverObject;
    mapActions;
    componentActions = [];
    listMenuActions = [];
    listColumns = [];

    helper = new RelatedListHelper();

    @wire(getRecord, {recordId:'$recordId', fields:[FASTTRACK_STAGE]})
    wiredRecord({data,error}){
        if(data){
            this.fastTrackStage = data.fields.FastTrackStage__c.value;
        }
        if(error){
            this.helper.handleError('Error Retrieving fields', this);
        }
    }

    @wire(MessageContext)
    messageContext;

    renderedCallback() {
        loadStyle(this, relatedListResource + '/relatedList.css');
    }

    get hasRecords() {
        return this.state.records != null && this.state.records.length;
    }

    connectedCallback(){
        this.handleSubscribeCDC();
        this.handleSubscribeOfferCreationUpdate();
        this.init();
    }

    //Use Change Data Capture to check for changes on FastTrackStage field
    handleSubscribeCDC(){
        const messageCallback = (response) => {
            // Response contains the payload of the new message received
            this.handleNotification(response);
        };

        // Invoke subscribe method of empApi. Pass reference to messageCallback
        subscribe(this.channelName, -1, messageCallback).then(response => {
            this.subscription = response;
            this.handleNotification(response);
        });
    }

    handleNotification(response){
        if(response.hasOwnProperty('data')){
            let jsonObj = response.data;
            if(jsonObj.hasOwnProperty('payload')){
                let payload = response.data.payload;
                if(this.helper.findCurrentIdAndFieldChange(payload, this.recordId)){
                    this.fastTrackStage = payload.FastTrackStage__c;
                    this.checkEnableButtons();
                }
            }
        }
    }

    handleSubscribeOfferCreationUpdate(){
        // Callback invoked whenever a new event message is received
        const self = this;
        const messageCallback = function (response) {
            var obj = JSON.parse(JSON.stringify(response));
            if(self.platformEventMatchesId(obj, self.recordId)){
                self.handleRefreshData();
            }
        };
 
        // Invoke subscribe method of empApi. Pass reference to messageCallback
        subscribe(this.updateOfferChannelName, -1, messageCallback).then(response => {
            // Response contains the subscription information on subscribe call
            console.log('Subscription request sent to: ', JSON.stringify(response.channel));
        });
    }

    platformEventMatchesId(message, recordId){
        return message.hasOwnProperty('data') && message.data.hasOwnProperty('payload') && 
                message.data.payload.hasOwnProperty('Record_Id__c') && recordId == message.data.payload.Record_Id__c;
    }

    async init() {
        this.displaySpinner();
        this.state.showRelatedList = this.recordId != null;
        if (! (this.recordId
            && this.sobjectApiName
            && this.relatedFieldApiName
            && this.fields
            && this.columns
            && this.iconDesignation)) {
            this.state.records = [];
            return;
        }

        this.popoverObject = this.relatedFromChildObject ? this.relatedChildObjectApiName : this.sobjectApiName;
        this.listColumns = this.helper.parseObject(this.columns);
        this.mapActions = this.helper.jsonToMap(this.overrideActionsWithFlow);
        
        this.state.fields = this.fields
        this.state.sortedBy = this.sortedBy
        this.state.sortedDirection = this.sortedDirection
        this.state.relatedFieldApiName= this.relatedFieldApiName
        this.state.recordId= this.recordId
        this.state.numberOfRecords= this.numberOfRecords
        this.state.sobjectApiName= this.sobjectApiName
        this.state.relatedFromChildObject = this.relatedFromChildObject
        this.state.relatedChildObjectApiName = this.relatedChildObjectApiName
        this.state.relatedChildObjectFieldApiName = this.relatedChildObjectFieldApiName
        this.state.overrideRelatedListName = this.overrideRelatedListName
        this.state.iconName = this.iconDesignation
        this.state.retrieveWaivers = this.retrieveWaivers

        const data = await this.helper.fetchData(this.state, this.isRefresh, this.messageContext);
        this.state.records = data.records;
        this.state.sobjectLabel = data.sobjectLabel;
        this.state.sobjectLabelPlural = data.sobjectLabelPlural;
        this.state.title = data.title;
        this.state.parentRelationshipApiName = data.parentRelationshipApiName;
        this.checkEnableButtons();
        this.initActions();
        this.hideSpinner();
    }

    checkEnableButtons(){
        let listEnabledStages = this.helper.splitToArray(this.enabledButtonsStages, ',');
        if(listEnabledStages && listEnabledStages.includes(this.fastTrackStage)){
            this.buttonsDisabled = false;
        }else{
            this.buttonsDisabled = true;
        }
    }

    initActions(){
        this.componentActions = [];
        this.listMenuActions = [];
        for (const key of this.mapActions.keys()){
            let action = this.mapActions.get(key);
            console.log('action ',action);
            if(this.helper.isComponentAction(action)){
                this.componentActions.push({
                    label: action.get('label'),
                    value: key
                });
            }else if(this.helper.isActionMenu(action)){
                this.listMenuActions.push({
                    label: action.get('label'),
                    value: key
                });
            }
        }
    }

    handleComponentActionSelected(event){
        let actionName = event.target.dataset.item;
        this.helper.handleAction(this, actionName);
    }

    handleActionSelected(event){
        let actionDetails = event.detail.actionDetails;
        let actionName = actionDetails.value;
        let recordId = actionDetails.recordId;
        let name = actionDetails.name;
        this.helper.handleAction(this, actionName, recordId, name);
    }

    handleEventFlowCall(flowDetails){
        const actionEvent = new CustomEvent('handleClickedAction', {detail: {flowDetails}});
        this.dispatchEvent(actionEvent);
    }

    handleCreateRecord() {
        const newEditPopup = this.template.querySelector("c-related-list-new-edit-popup");
        newEditPopup.recordId = null;
        newEditPopup.recordName = null;        
        newEditPopup.sobjectApiName = this.popoverObject;
        newEditPopup.sobjectLabel = this.state.sobjectLabel;
        newEditPopup.show(); 
    }

    handleEditRecord(row) {
        const newEditPopup = this.template.querySelector("c-related-list-new-edit-popup");
        newEditPopup.recordId = row.Id;
        newEditPopup.recordName = row.Name;
        newEditPopup.sobjectApiName = this.popoverObject;
        newEditPopup.sobjectLabel = this.state.sobjectLabel;
        newEditPopup.show();
    }

    handleDeleteRecord(row) {
        const newEditPopup = this.template.querySelector("c-related-list-delete-popup");
        newEditPopup.recordId = row.Id;
        newEditPopup.recordName = row.Name;
        newEditPopup.sobjectLabel = this.state.sobjectLabel;
        newEditPopup.show();
    }

    handleGotoRelatedList() {
        let recordId = this.recordId;
        let objectApiName = this.sobjectApiName;

        if(this.relatedFromChildObject && this.helper.hasRecords(this.state.records)){
            recordId = this.state.records[0][this.relatedChildObjectFieldApiName];
            objectApiName = this.relatedChildObjectApiName;
        }

        this.navigateToRelatedList(recordId, objectApiName);
    }

    navigateToRelatedList(recordId, objectApiName){
        this[NavigationMixin.Navigate]({
            type: "standard__recordRelationshipPage",
            attributes: {
                recordId: recordId,
                relationshipApiName: this.state.parentRelationshipApiName,
                actionName: "view",
                objectApiName: objectApiName
            }
        });
    }

    displaySpinner(){
        this.showSpinner = true;
    }

    hideSpinner(){
        this.showSpinner = false;
    }

    @api
    handleRefreshData(){
        this.isRefresh = true;
        this.init();
    }
}