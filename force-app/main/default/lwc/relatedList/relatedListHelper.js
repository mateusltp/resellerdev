/* eslint-disable guard-for-in */
/* eslint-disable no-console */
import initDataMethod from "@salesforce/apex/RelatedListController.initData";
import { publish} from 'lightning/messageService';
import refreshOfferSummary from '@salesforce/messageChannel/refreshRevenueMetricsChannel__c';

export default class RelatedListHelper {

    handleError(error, parent){
        const evt = new ShowToastEvent({
            message: error,
            variant: 'Error',
        });
        parent.dispatchEvent(evt);
    }

    findCurrentIdAndFieldChange(payload, recordId){
        let recordIds = payload.ChangeEventHeader.recordIds;
        let changedFields = payload.ChangeEventHeader.changedFields;
        //find the current recordId and FastTrackStage field was changed and if found then update buttons
        const recId = recordIds.find(element=> element == recordId);
        const fastTrackField = changedFields.find(element=> element == 'FastTrackStage__c');
        return recId && fastTrackField;
    }

    fetchData(state, isRefresh, messageContext) {
        let jsonData = Object.assign({}, state)
        jsonData.numberOfRecords = state.numberOfRecords + 1
        jsonData = JSON.stringify(jsonData)
        return initDataMethod({ jsonData })
            .then(response => {
                if(isRefresh){
                    publish(messageContext, refreshOfferSummary);
                }
                const data = JSON.parse(response)
                return this.processData(data, state)
            })
            .catch(error => {
                console.log(error);
            });
    }

    processData(data, state){
        const records = data.records;
        const title = state.overrideRelatedListName ? state.overrideRelatedListName : data.sobjectLabelPlural;
        this.generateLinks(records)
        if (records.length > state.numberOfRecords) {
            records.pop()
            data.title = `${title} (${state.numberOfRecords}+)`
        } else {
            data.title = `${title} (${Math.min(state.numberOfRecords, records.length)})`
        }     
        return data
    }

    generateLinks(records) {
        records.forEach(record => {
            record.LinkName = '/' + record.Id
            for (const propertyName in record) {
                const propertyValue = record[propertyName];
                if (typeof propertyValue === 'object') {
                    const newValue = propertyValue.Id ? ('/' + propertyValue.Id) : null;
                    this.flattenStructure(record, propertyName + '_', propertyValue);
                    if (newValue !== null) {
                        record[propertyName + '_LinkName'] = newValue;
                    }
                }
            }
        });

    }

    flattenStructure(topObject, prefix, toBeFlattened) {
        for (const propertyName in toBeFlattened) {
            const propertyValue = toBeFlattened[propertyName];
            if (typeof propertyValue === 'object') {
                this.flattenStructure(topObject, prefix + propertyName + '_', propertyValue);
            } else {
                topObject[prefix + propertyName] = propertyValue;
            }
        }
    }

    splitToArray(string, separator){
        return string.split(separator);
    }

    parseObject(toParseObject){
        let listObject = JSON.parse(toParseObject);
        let parsedList = [];
        for(var obj in listObject){
            parsedList.push(listObject[obj]);
        }

        return parsedList;
    }

    jsonToMap(jsonString){
        var resultMap = new Map();
        if(!jsonString){
            return resultMap;
        }
        var jsonObject = JSON.parse(jsonString);
        var dataObject = jsonObject.actions;
        var dataMap = new Map(Object.entries(dataObject));
        for (const key of dataMap.keys())  {
            var keyMap = new Map(Object.entries(dataMap.get(key)));
            resultMap.set(key, keyMap);
        }
    
        return resultMap;
    }

    isComponentAction(action){
        return action && action.get('actionMenu') && action.get('actionMenu') == 'false' && action.get('label');;
    }

    isActionMenu(action){
        return action && action.get('actionMenu') && action.get('actionMenu') == 'true' && action.get('label');
    }

    handleAction(self, actionName, recordId, name){
        if(this.isFlowMapped(self.mapActions, actionName)){
            let flowDetails = {
                flowName: self.mapActions.get(actionName).get('value'),
                recordId: recordId
            }
            console.log('testeee' + self.mapActions.get(actionName).get('value'));
            console.log('testeee22' + actionName);
            self.handleEventFlowCall(flowDetails);
        }else{
            let row = {
                Id: recordId, 
                Name: name
            };
            this.callStandardAction(self, actionName, row);
        }
    }

    isFlowMapped(mapActions, actionName){
        return mapActions && mapActions.get(actionName) && mapActions.get(actionName).get("value");
    }

    callStandardAction(self, actionName, row){
        switch (actionName) {
            case "new":
                self.handleCreateRecord();
                break;
            case "edit":
                self.handleEditRecord(row);
                break;
            case "delete":
                self.handleDeleteRecord(row);
                break;
            default:
        }
    }
    
    hasRecords(records){
        return records && records.length > 0;
    }
}