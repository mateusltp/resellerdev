import { LightningElement, track, wire, api } from 'lwc';
import { getRecordUi, createRecord, updateRecord, deleteRecord } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { getPicklistValues } from 'lightning/uiObjectInfoApi';

import getContactRecordTypeId from '@salesforce/apex/NetworkBuilderEditSingleAccountCtrl.getRecordTypeId';
import getContactRelations from '@salesforce/apex/NetworkBuilderEditSingleAccountCtrl.getContactRelations';
import getContactsFromParent from '@salesforce/apex/NetworkBuilderEditSingleAccountCtrl.getContactsFromParentThatAreNotRelatedToChild';
import ACCOUNT_CONTACT_RELATION_OBJECT from '@salesforce/schema/AccountContactRelation';
//import ACCOUNT_CONTACT_ROLES from '@salesforce/schema/AccountContactRelation.Roles';
import ACCOUNT_CONTACT_ROLES from '@salesforce/schema/AccountContactRelation.Type_of_Contact__c';

import { reduceErrors } from 'c/ldsUtils';

export default class NetworkBuilderEditSingleAccount extends LightningElement {
    @api selectedAccount;

    errorAdmin = 'Please give this error message to your admin.';

    parentAccountId;
    recordTypeId;
    @track showSpinner = true;
    @track sectionWithFields = [];
    @track activeSections = [];
    contactRelationsLoaded = false;
    contactRelations = [];
    contactRelationsDel = [];
    @track parentContactRelation = [];
    selectedParentContactRelation = [];
    @track contactsToCreate = [];
    @track contactRoleOptions = [];
    @track contactRecordTypeId = null;
    @track showCreateNewContactModal = false;
    noContactsAvailableMessage = '';

    @wire(getRecordUi, { recordIds: '$selectedAccount', layoutTypes: 'Full', modes: 'Edit' })
    accountRecordUi({ error, data }) {
        if (data && data.layouts && data.records) {
            this.recordTypeId = data.records[String(this.selectedAccount)].recordTypeId;
            var sectionLst = [...data.layouts.Account[this.recordTypeId].Full.Edit.sections];
            sectionLst.forEach(element => {
                this.activeSections.push(String(element.heading));
                var fieldLst = [];
                if (element.heading != 'System Information' && element.heading != 'Custom Links') {
                    element.layoutRows.forEach(row => {
                        row.layoutItems.forEach(field => {
                            field.layoutComponents.forEach(fieldItem => {
                                if (fieldItem.apiName != null) {
                                    let aField = {};
                                    aField.devName = fieldItem.apiName;
                                    aField.isRequired = fieldItem.required;
                                    fieldLst.push(aField);
                                }
                            });
                        });
                    });
                    this.sectionWithFields.push({ value: fieldLst, key: element.heading });
                }
            });
        }
    };

    @wire(getPicklistValues, { recordTypeId: '$recordTypeId', fieldApiName: ACCOUNT_CONTACT_ROLES })
    roleOptions({ error, data }) {
        if (data) {
            data.values.forEach(option => {
                let objOption = {};
                objOption.label = option.label;
                objOption.value = option.value;
                this.contactRoleOptions.push(objOption);
            })
        }
    }

    get hasContactInParentToImport() {
        return this.parentContactRelation.length > 0 ? true : false;
    }

    get parentContactsQuantity() {
        return this.parentContactRelation.length;
    }

    get hasContactsToCreate() {
        return this.contactsToCreate.length > 0 ? true : false;
    }

    get contactsToCreateQuantity() {
        return this.contactsToCreate.length;
    }

    get parentContactRelationData() {
        if (this.parentContactRelation) {
            let data = [];
            this.parentContactRelation.forEach(contact => {
                let newContact = {};
                newContact.Id = contact.Contact.Id;
                newContact.Name = contact.Contact.Name;
                newContact.Email = contact.Contact.Email;
                newContact.Roles = [];
                data.push(newContact);
            });
            return data;
        }
        return null;
    }

    get hasExistingContacts() {
        return this.contactRelations.length > 0 ? true : false;
    }

    get existingContactRelationsData() {
        if (this.contactRelations) {
            let data = [];
            this.contactRelations.forEach(rel => {
                let newContact = {};
                newContact.Id = rel.Id;
                newContact.Name = rel.Contact.Name;
                newContact.Email = rel.Contact.Email;
                //newContact.Type_of_contact__c = rel.Type_of_Contact__c;
                if(rel.Type_of_Contact__c) {
                    let types = rel.Type_of_Contact__c.split(';');
                    let roles = [];
                    for (const role of types) {
                        roles.push(role);
                    }
                    newContact.Roles = roles;
                }

                data.push(newContact);
            });
            return data;
        }
        return null;
    }

    handleOnLoad(event) {
        var record = event.detail.records;
        var fields = record[this.selectedAccount].fields;
        this.parentAccountId = fields.ParentId.value;
        //this.handleGetContactRelations();
        this.showSpinner = false;
    }

    handleSectionToggle(event) {
        let openSections = event.detail.openSections;
        console.log(openSections);
        let sections = openSections.join(', ');
        console.log(sections);
        if(sections.includes('Contacts') && !this.contactRelationsLoaded) {
            this.handleGetContactRelations();
        }
        //this.handleGetContactRelations();
    }

    handleSubmit(){
        this.enableSpinner();
        this.template.querySelector('lightning-record-edit-form').submit();
        //this.handleChanges();
    }

    
    handleSuccess(){
        // let successEvt = new ShowToastEvent({
        //     title: 'Success!',
        //     message: 'Account successfully updated.',
        //     variant: 'success'
        // });
        // this.dispatchEvent(successEvt);
        this.handleChanges();
        //this.closeModal();
    }

    handleImportContacts(event) {
        this.enableSpinner();
        if (event.target.checked) {
            this.handleGetContactsFromParent();
            this.disableSpinner();
        }
        else {
            this.parentContactRelation = [];
            this.noContactsAvailableMessage = '';
            this.disableSpinner();
        }
    }

    handleSelectedImportContact(event) {
        this.selectedParentContactRelation = [];
        let selectedRows = this.template.querySelectorAll('.contactRelationInput');
        for (let i = 0; i < selectedRows.length; i++) {
            if (selectedRows[i].checked && selectedRows[i].type === 'checkbox') {
                this.selectedParentContactRelation.push({
                    ContactId: selectedRows[i].value,
                    Type_of_Contact__c: ''
                })
                let queryTerm = "c-multi-select-picklist[data-id='" + selectedRows[i].value + "']";
                this.template.querySelector(queryTerm).disable(false);
            }
            else if (!selectedRows[i].checked && selectedRows[i].type === 'checkbox') {
                let parentImportIndex = this.selectedParentContactRelation.findIndex(rel => rel.ContactId === selectedRows[i].value);
                if (parentImportIndex > -1) {
                    this.selectedParentContactRelation.splice(parentImportIndex, 1);
                }
                let queryTerm = "c-multi-select-picklist[data-id='" + selectedRows[i].value + "']";
                this.template.querySelector(queryTerm).disable(true);
            }
        }

    }

    handleContactRoleChange() {
        this.handleSelectedImportContact();
    }

    handleSelectedExistingContact() {
        this.enableSpinner();
        this.contactRelationsDel = [];
        let selectedRows = this.template.querySelectorAll('.contactExistingRelationInput');
        for (let i = 0; i < selectedRows.length; i++) {
            if (selectedRows[i].checked && selectedRows[i].type === 'checkbox') {
                let contactRelDeleteIndex = this.contactRelationsDel.findIndex(rel => rel.Id === selectedRows[i].value);
                if (contactRelDeleteIndex > -1) {
                    this.contactRelationsDel.splice(contactRelDeleteIndex, 1);
                }
            }
            if (!selectedRows[i].checked && selectedRows[i].type === 'checkbox') {
                let contactRel = this.contactRelations.find(rel => rel.Id === selectedRows[i].value);
                this.contactRelationsDel.push({
                    contactRel
                })
            }
        }
        this.disableSpinner();
    }

    handleExistingContactRoleChange() {
        this.handleSelectedExistingContact();
    }

    handleSaveContact(event) {
        let newContact = event.detail;
        this.contactsToCreate.push(event.detail);
    }

    handleGetContactRelations() {
        this.enableSpinner();
        getContactRelations({ accountId: this.selectedAccount
                            })
            .then((result) => {
                this.contactRelations = result;
                this.contactRelationsLoaded = true;
                this.disableSpinner();
            })
            .catch((error) => {
                this.showToastNotification('An error occurred!', this.errorAdmin + ' ' + reduceErrors(error), 'warning');
            });
    }

    handleGetContactsFromParent() {
        getContactsFromParent({ childAccountId: this.selectedAccount,
                                parentAccountId: this.parentAccountId
                         })
         .then((result) => {
             this.parentContactRelation = result;
             if(this.parentContactRelation.length === 0) {
                 this.noContactsAvailableMessage = 'No contacts available for import.';
             } else {
                 this.noContactsAvailableMessage = '';
             }
         })
         .catch((error) => {
             this.showToastNotification('An error occurred!', this.errorAdmin + ' ' + reduceErrors(error), 'warning');
         });
    }

    enableSpinner() {
        this.showSpinner = true;
    }

    disableSpinner() {
        setTimeout(() => {
            this.showSpinner = false;
        }, 1000);
    }

    openModal() {
        this.isModalOpen = true;
    }

    closeModal( event ) {

        let buttonSource = event  != null && event != undefined ? true : false;
        console.log('buttonSource');
        console.log(buttonSource);
        if(!buttonSource){
            let successEvt = new ShowToastEvent({
                title: 'Success!',
                message: 'Account successfully updated.',
                variant: 'success'
            });
            this.dispatchEvent(successEvt);
        }
     
     
        const myEvent = new CustomEvent('close');
        this.dispatchEvent(myEvent);
        this.disableSpinner();
    }

    populateContactRelationInput(contactRelation, AccountId) {
            let contactRelationInput = this.recordInputForCreate(ACCOUNT_CONTACT_RELATION_OBJECT.objectApiName);
            contactRelationInput.fields['ContactId'] = contactRelation.ContactId;
            contactRelationInput.fields['AccountId'] = AccountId;
            contactRelationInput.fields['Type_of_Contact__c'] = contactRelation.Type_of_Contact__c;
            return contactRelationInput;
        }

    populateContactRelationUpdate(contactRelation) {
        let contactRelationInput = this.recordInputForUpdate();
        contactRelationInput.fields['Id'] = contactRelation.Id;
        contactRelationInput.fields['Type_of_Contact__c'] = contactRelation.Type_of_Contact__c;
        return contactRelationInput;
    }

    populateContactUpdate(contactRelation) {
        let contactInput = this.recordInputForUpdate();
        contactInput.fields['Id'] = contactRelation.ContactId;
        contactInput.fields['Type_of_contact__c'] = contactRelation.Type_of_Contact__c;
        return contactInput;
    }

    populateAccountUpdateLegalRep(contactRelation) {
        let accountInput = this.recordInputForUpdate();
        accountInput.fields['Id'] = this.selectedAccount;
        accountInput.fields['Legal_Representative__c'] = contactRelation.ContactId;
        return accountInput;
    }

    populateAccountRemoveLegalRep() {
        let accountInput = this.recordInputForUpdate();
        accountInput.fields['Id'] = this.selectedAccount;
        accountInput.fields['Legal_Representative__c'] = null;
        return accountInput;
    }

  
    async handleChanges() {
        try {
           // this.template.querySelector('lightning-record-edit-form').submit();
            if(this.contactRelationsLoaded || this.contactsToCreate.length > 0) {
                this.checkLegalRepresentatives();
                if (this.legalRepresentatives > 1) {
                    this.disableSpinner();
                    this.showToastNotification('Multiple Legal Representatives found.', 'You can only select one Legal Representative.', 'warning');
                    return;
                }
                this.enableSpinner();
                if (this.legalRepresentatives === 0) {
                    this.enableSpinner();
                    let accountResult = await updateRecord(this.populateAccountRemoveLegalRep());
                    console.log('account updated: '+accountResult.id)
                }
                for (let i = 0; i < this.selectedParentContactRelation.length; i++) {
                    if (this.legalRepresentatives === 1) {
                        let contactRel = this.selectedParentContactRelation[i];
                        if (contactRel.Type_of_Contact__c.includes('Legal Representative')) {
                            this.enableSpinner();
                            let accountResult = await updateRecord(this.populateAccountUpdateLegalRep(this.selectedParentContactRelation[i]));
                            console.log('account updated: '+accountResult.id)
                        }
                    }
                    this.enableSpinner();
                    let contactRelatResult = await createRecord(this.populateContactRelationInput(this.selectedParentContactRelation[i], this.selectedAccount));
                    console.log('contact relation created: '+contactRelatResult.id);
                }

                for (let i = 0; i < this.contactsToCreate.length; i++) {
                    this.enableSpinner();
                    let contactResult = await createRecord(this.contactsToCreate[i]);
                    console.log('contact created: '+contactResult.id);
                }

                for (let i = 0; i < this.contactRelations.length; i++) {
                    let contactRelDeleteIndex = this.contactRelationsDel.findIndex(rel => rel.contactRel.Id === this.contactRelations[i].Id);
                    if (contactRelDeleteIndex === -1) {
                        this.enableSpinner();
                        let contactRelatUpdateResult = await updateRecord(this.populateContactRelationUpdate(this.contactRelations[i]));
                        console.log('contact relation updated: '+contactRelatUpdateResult.id);
                        let contactRelation = this.contactRelations[i];
                        if(contactRelation.Contact.AccountId === this.selectedAccount && contactRelation.Type_of_Contact__c) {
                            // need to update the contact only if is directly related to the account. most likely need to grab the accountid related to the contacts when I get the data.
                            this.enableSpinner();
                            let contactUpdateResult = await updateRecord(this.populateContactUpdate(contactRelation));
                            console.log('contact updated: '+contactUpdateResult.id);
                        } else if (contactRelation.Contact.AccountId !== this.selectedAccount && contactRelation.Type_of_Contact__c && this.legalRepresentatives === 1) {
                            if (contactRelation.Type_of_Contact__c.includes('Legal Representative')) {
                                this.enableSpinner();
                                let accountResult = await updateRecord(this.populateAccountUpdateLegalRep(contactRelation));
                                console.log('account updated: '+accountResult.id)
                            }
                        }
                    }
                }

                for (let i = 0; i < this.contactRelationsDel.length; i++) {
                    try {
                        this.enableSpinner();
                        let contactRelatDeleteResult = await deleteRecord(this.contactRelationsDel[i].contactRel.Id);
                        console.log('contact relation deleted: '+JSON.stringify(contactRelatDeleteResult));
                    } catch(error) {
                        const errorMessage = reduceErrors(error);
                        if(errorMessage[0].includes('A direct relationship')) {
                            this.enableSpinner();
                            let contactDeleteResult = await deleteRecord(this.contactRelationsDel[i].contactRel.ContactId);
                            console.log('contact deleted:  '+JSON.stringify(contactDeleteResult));
                        }
                        else {
                            this.showToastNotification(this.errorAdmin + ' ' + errorMessage);

                        }
                    }

                }
            }
            //this.handleSuccess();
            this.closeModal();
        }  catch(error) {
            this.showToastNotification('An error occurred!', this.errorAdmin + ' ' + reduceErrors(error), 'warning');
        }

    }

    recordInputForCreate(objApiName) {
        let recordInput = {
            "apiName": objApiName,
            "fields": {
            }
        }
        return recordInput;
    }

    recordInputForUpdate() {
        let recordInput = {
            "fields": {
            }
        }
        return recordInput;
    }

    openCreateNewContactModal() {
        this.enableSpinner();
        getContactRecordTypeId( {recordTypeDevName : 'Gyms_Partner'}).then((result) => {
            this.contactRecordTypeId = result;
            this.showCreateNewContactModal = true;
        }).catch((error) => {
            this.showToastNotification('An error occurred!', this.errorAdmin + ' ' + reduceErrors(error), 'warning');
        });
        this.disableSpinner();
    }

    closeCreateNewContactModal() {
        this.showCreateNewContactModal = false;
    }

    showToastNotification(title, message, variant){
        const evt = new ShowToastEvent({ title: title, message: message, variant: variant });
        this.dispatchEvent(evt);
    }

    // multi select picklist
    handleSelectOptionList(event){
        let targetId = event.target.dataset.id;

        let evtDetail = event.detail;
        console.log(JSON.stringify(evtDetail));
        let evtDetailPayload = evtDetail.payload;
        console.log(JSON.stringify(evtDetailPayload));
        let selectedOptions = evtDetail.payload.values;
        console.log(JSON.stringify(selectedOptions));
        if(selectedOptions) {
            let optionsFormat = selectedOptions.join(';');
            let contactRelIndex = this.contactRelations.findIndex(rel => rel.Id == targetId);
            let contactParentRelIndex = this.selectedParentContactRelation.findIndex(rel => rel.ContactId == targetId);
            if(contactRelIndex > -1) {
                this.contactRelations[contactRelIndex].Type_of_Contact__c = optionsFormat;
                console.log(JSON.stringify(this.contactRelations));
            } else if (contactParentRelIndex > -1) {
                this.selectedParentContactRelation[contactParentRelIndex].Type_of_Contact__c = optionsFormat;
                console.log(JSON.stringify(this.selectedParentContactRelation));
            } else {
                let newContactsIndex = this.contactsToCreate.findIndex(contact => contact.fields.LastName == targetId);
                if (newContactsIndex > -1) {
                    this.contactsToCreate[newContactsIndex].fields.Type_of_contact__c = optionsFormat;
                    console.log(JSON.stringify(this.contactsToCreate));
                }
            }
        }
    }

    legalRepresentatives = 0;
    checkLegalRepresentatives() {
        const legalRep = 'Legal Representative';
        let qtLegalReps = 0;
        for (let rel of this.contactRelations) {
            if (rel.Type_of_Contact__c && rel.Type_of_Contact__c.includes(legalRep)) {
                qtLegalReps++;
            }
        }
        for (let parentRel of this.selectedParentContactRelation) {
            if (parentRel.Type_of_Contact__c && parentRel.Type_of_Contact__c.includes(legalRep)) {
                qtLegalReps++;
            }
        }
        for (let newContact of this.contactsToCreate) {
            if(newContact.fields.Type_of_contact__c && newContact.fields.Type_of_contact__c.includes(legalRep)) {
                qtLegalReps++;
            }
        }
        this.legalRepresentatives = qtLegalReps;
        console.log('qt legal reps: '+this.legalRepresentatives)
    }
}