import { LightningElement, api } from 'lwc';

import { FlowAttributeChangeEvent, FlowNavigationNextEvent} from 'lightning/flowSupport';

export default class ResellerEJFormSubmission extends LightningElement {

    @api modalHeader;
    @api clientSuccessExecutiveName;
    
    @api goBackButtonVariant;
    @api goBackButtonLabel = 'Go back';

    @api submitButtonVariant;
    @api submitButtonLabel = 'Submit';

    @api modalActionClicked;
    @api formType;
    
    // isM0Form = false;
    // isM1form = false;

    connectedCallback() {
        console.log(this.formType);
        this.isM0Form = (this.formType == 'M0') ? true : false;
        this.isM1Form = (this.formType == 'M1') ? true : false;
    }

    handleGoBack() {
        this.modalActionClicked = 'back';
        this.dispatchChangeAttributeEvent(this.modalActionClicked);
    }

    handleSubmit() {
        this.modalActionClicked = 'submit';
        this.dispatchChangeAttributeEvent(this.modalActionClicked);
    }

    dispatchChangeAttributeEvent(action) {
        const attributeChangeEvent = new FlowAttributeChangeEvent(
            'modalActionClicked',
            action
        );
        this.dispatchEvent(attributeChangeEvent);
        this.dispatchNextEvent();
    }

    dispatchNextEvent() {
        const navigateNextEvent = new FlowNavigationNextEvent();
        this.dispatchEvent(navigateNextEvent);
    }

}