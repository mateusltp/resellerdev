/**
 * @description       : Table for Related List
 * @author            : tiago.ribeiro@gympass.com
 * @group             : 
 * @last modified on  : 06-08-2022
 * @last modified by  : tiago.ribeiro@gympass.com
**/
import { LightningElement, api, track} from 'lwc';

export default class DynamicTable extends LightningElement {
    @track ranger;
    @track waiversList;
    @track records;
    @track disablebuttons;
    @api columns;
    @api icon;
    @api objectname;
    @api titlefield = 'Name';
    @api fields;
    @api actionsmenu;
    @api showSpinner = false;
    @api activatenavigation;
    
    recordsWithActions = [];
    waiversMap = new Map();
    runIterationOnRecordsChange = false; //Prevents iterateRecords() from running twice at the component start

    marginTop = -7.2;
    waiversPerLine = 3;
    sumPerWaiverLine = -0.9;
    
    @api
    get myrecords(){
        return this.records;
    }

    set myrecords(value) {
        this.records = value;
        if(this.runIterationOnRecordsChange){
            this.iterateRecords();
        }
    }

    @api
    get mydisablebuttons(){
        return this.disablebuttons;
    }   

    set mydisablebuttons(value) {
        this.disablebuttons = value;
        this.enableMenuButtons();
    }

    get displayMenuButton(){
        this.records.find(record => record.Is_Default__c !== true)
    }

    connectedCallback(){
        this.iterateRecords();
        this.runIterationOnRecordsChange = true;
    }

    iterateRecords(){
        this.recordsWithActions = [];
        let actionsWithoutDelete = this.filterActionsWithoutDelete();

        this.records.forEach(element => {
            this.processRecordsWithActions(element, actionsWithoutDelete);
            this.checkRecordsForWaivers(element);
        });
    }

    filterActionsWithoutDelete(){
        let actionsWithoutDelete = [];
        
        if(this.actionsmenu){
            actionsWithoutDelete = this.actionsmenu.filter(action => action.value != 'delete');
        }

        return actionsWithoutDelete;
    }

    processRecordsWithActions(element, actionsWithoutDelete){
        if(element.Is_Default__c){
            this.recordsWithActions.push({sku: element, actions: actionsWithoutDelete});
        }else{
            this.recordsWithActions.push({sku: element, actions: this.actionsmenu});
        }
    }

    checkRecordsForWaivers(element){
        if(element.Waivers__r){
            this.waiversMap.set(element.Id, this.iterateWaivers(element.Waivers__r.records));
        }else{
            this.waiversMap.set(element.Id, []);
        }
    }

    iterateWaivers(waivers){
        let listWaiversFormatted = [];
        waivers.forEach(waiver => {
            listWaiversFormatted.push(
                this.formatWaiversForPill(waiver)
            );
        });
        return listWaiversFormatted;
    }

    formatWaiversForPill(waiver){
        return {label: waiver.Percentage__c +'% / ' + waiver.Duration__c + this.formatDurationMonths(waiver.Duration__c)};
    }

    formatDurationMonths(duration){
        let concatenateMonths = ' months';
        if(duration == 1){
            concatenateMonths = ' month';
        }
        return concatenateMonths;
    }
    
    renderedCallback(){
        this.enableMenuButtons();
    }
    
    enableMenuButtons(){
        let menuButtons = this.template.querySelectorAll('lightning-button-menu');
            if(menuButtons){
                menuButtons.forEach(element => {
                    element.disabled = this.disablebuttons;
            });
        }
    }

    onHoverProduct(event){
        let ranger = event.currentTarget.dataset.rangerid;
        let index = event.currentTarget.dataset.rangerindex;
        this.waiversList = this.waiversMap.get(ranger);
        if(this.waiversList.length != 0){
            this.calculatePopupTopMargin(index);
        }
    }

    calculatePopupTopMargin(index){
        let popover = this.template.querySelectorAll('.spnPopover')[index];
        let numberWaiverLines = Math.ceil(this.waiversList.length / this.waiversPerLine);
        popover.style.marginTop = (this.marginTop + (this.sumPerWaiverLine * (numberWaiverLines - 1))) + 'rem';
    }

    actionSelected(event){
        let value = event.detail.value;
        let recordId = event.target.dataset.item;
        let name = event.target.dataset.label;
        let actionDetails = {value: value, recordId: recordId, name: name};
        this.fireEventToParent(actionDetails);
    }

    fireEventToParent(actionDetails){
        const actionEvent = new CustomEvent('handleaction', {detail: {actionDetails}});
        this.dispatchEvent(actionEvent);
    }
}