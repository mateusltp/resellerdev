import { LightningElement, api } from 'lwc';

import CLIENT_SUCCESS_APPROVAL_FIELD from '@salesforce/schema/Form__c.Client_Success_Approval__c';
import APPROVAL_STATUS_FIELD from '@salesforce/schema/Form__c.Approval_Status__c';

export default class ResellerEngagementJourneyFormApprovalStatus extends LightningElement {

    clientSuccessApproval = CLIENT_SUCCESS_APPROVAL_FIELD;
    approvalStatus = APPROVAL_STATUS_FIELD;

    //-- flexipage provided recordId and objectApiName
    @api recordId;
    @api objectApiName;

    @api showComponent;
    @api messageToShow = '';

    handleOnLoad() {
        let clientSuccessStatus = this.template.querySelector('.client_success_approval').value;
        let enablersStatus = this.template.querySelector('.approval_status').value;
        let msg = 'Pendind Approval: ';

        console.log("*** New Component ***");
        console.log('ClientSuccessStatus ' + clientSuccessStatus);
        console.log('enablersStatus ' + enablersStatus);

        if(!clientSuccessStatus || !enablersStatus.includes('Approved')) {
                if(!clientSuccessStatus) {
                    msg += 'Client Success has not been approved yet. '
                }            
                if(!enablersStatus.includes('Approved')) {
                    msg += 'Launch Scorecard (Enablers) has not been approved yet. '
                }      
                this.showComponent = true;
                this.messageToShow = msg;
            } else {
                this.showComponent = false;
            }

    }
    
}