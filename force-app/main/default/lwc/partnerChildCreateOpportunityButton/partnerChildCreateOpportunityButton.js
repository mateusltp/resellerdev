import { LightningElement,wire, api } from 'lwc';
import getLocationsByLegalGroup from '@salesforce/apex/PartnerChildOpportunityManagerController.getLocationsByLegalGroup';
import getMainOpportunityMember from '@salesforce/apex/PartnerChildOpportunityManagerController.getMainOpportunityMember';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { reduceErrors } from 'c/ldsUtils';
import { NavigationMixin } from 'lightning/navigation';
import OPPORTUNITY_ACCOUNT_FIELD from '@salesforce/schema/Opportunity.AccountId';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';

const fields = [OPPORTUNITY_ACCOUNT_FIELD];

export default class PartnerChildCreateOpportunityButton extends  NavigationMixin(LightningElement) {

    @api recordId;
    accountId;
    showSpinner=false;
    @api showButton=false;

    @wire(getRecord, { recordId: '$recordId', fields })
    oppMember({ error, data }) {
        if (data) {
            this.accountId = getFieldValue(data, OPPORTUNITY_ACCOUNT_FIELD);
        } else if (error) {
            this.dispatchToast('Error', 'error', reduceErrors(error)[0]);
            this.showSpinner = false;
        }
    };

    connectedCallback(){
        this.showSpinner = true;
        getLocationsByLegalGroup({ opportunityId : this.recordId })
        .then(result => {
            this.showSpinner = false;
            this.showButton = result.length > 0 ? true : false;
        })
        .catch(error => {           
            this.dispatchToast('Error', 'error', reduceErrors(error)[0]);
            this.showSpinner = false;
        });
        console.log('showButton ' + this.showButton);
    }

    handleRedirectToMainOppMember(event){     
        this.showSpinner = true;
        getMainOpportunityMember({ opportunityId : this.recordId,  accountId: this.accountId })
        .then(result => {
            this.showSpinner = false;
            this[NavigationMixin.Navigate]({
                type: 'standard__recordPage',
                attributes: {
                    recordId: result,
                    objectApiName: 'Account_Opportunity_Relationship__c',
                    actionName: 'view',
                }
            })
        })
        .catch(error => {           
            this.dispatchToast('Error', 'error', reduceErrors(error)[0]);
            this.showSpinner = false;
        });
    }

    
    dispatchToast(title, variant, message) {
        let successtEvnt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant
        });
        this.dispatchEvent(successtEvnt);
    }

}