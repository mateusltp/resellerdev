import { LightningElement, track, api } from 'lwc';

export default class SmbSkuInformation extends LightningElement {

    @track objectApiName = 'QuoteLineItem'

    @track sectionTitle = 'Product Information';

    @track product;

    @api
    set record(value) {
        this.product = {...value};
    }
    get record() {
        return this.product;
    }

    get productLink() {
        if(this.product)
            return '/' + this.product.Product2Id;
        else
            return '';
    }

    @api
    getProduct() {
        return this.product;
    }

    @api
    reportValidity() {
        let salesPriceField = this.template.querySelector('[data-label="Sales Price"]');
        return salesPriceField.reportValidity();
    }

    updateDiscount(salesPrice) {
        let discount = (1 - (salesPrice / this.product.List_Price__c)) * 100;
        if(discount >= 0 && discount !== Infinity) {
            this.product.Discount = discount;
        }
        else {
            this.product.Discount = 0.0;
        }
    }

    handleSalesPriceChange(event) {
        const salesPrice = event.detail.value;
        this.product.UnitPrice = salesPrice;
        this.product.Subtotal = salesPrice * this.product.Quantity;
        console.log(this.product.Subtotal);
        this.updateDiscount(salesPrice);
    }

}