import { LightningElement, api, wire, track } from 'lwc';
 

import TEM_BLANK from './resellerFlowMultiple.html'
import TEMP_SCREEN_SELECT_RESELLER from './selectResellerScreen.html';
import TEMP_SCREEN_UPLOAD_DOWNLOAD from './uploadDownloadScreen.html';
import TEMP_SCREEN_FILE_CONFIRMATION from './fileConfirmationScreen.html';
import TEMP_SCREEN_PROCESSING from './processingScreen.html';
import TEMP_SCREEN_ALL_DONE from './allDoneScreen.html';

import { getRecord, getFieldValue } from 'lightning/uiRecordApi';

import setStatusLoading from '@salesforce/apex/ResellerFormController.setStatusLoading'; 
import getResellerList from '@salesforce/apex/ResellerFormController.getResellerList';
import { loadStyle } from 'lightning/platformResourceLoader';
import resellerFileSelector from '@salesforce/resourceUrl/resellerFileSelector';
import createMassive from '@salesforce/apex/ResellerFormController.createMassiveAccountRequest';
import getResellerResource from '@salesforce/apex/ResellerFormController.getResellerResource';
import countAccountRequest from '@salesforce/apex/MassiveAccReqLoad.quantityMassiveAccountRequest';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

const SPREADSHEET_BRAZIL = 'Spreadsheet_Brazil';
const SPREADSHEET_INTERNATIONAL = 'Spreadsheet_International';

export default class ResellerFlowMultiple extends LightningElement {

    @api
    recordId;
    progress = 0;

    country;
    massivoUrl;
    resellerDefaultField = false;
    path = 0;
    accountsInFile = 0;
    buttonContinue = true;

    developerNameParam;
    staticResource;
   

    user_fields = {
        USER_ID,
        USER_COUNTRY_CODE,
        USER_NAME
    }

    
    label = {
        LABEL_COMMUNITY_COMPANY_QUERY,
        LABEL_COMMUNITY_SELECT_RESELLER,
        LABEL_COMMUNITY_CONTINUE,
        LABEL_COMMUNITY_BACK,
        LABEL_COMMUNITY_UPLOAD,
        LABEL_COMMUNITY_DROP_FILE,
        LABEL_COMMUNITY_LEAST_2_ACCOUNTS,
        LABEL_COMMUNITY_LEARN_HOW,
        LABEL_COMMUNITY_BEEN_UPLOADED,
        LABEL_COMMUNITY_PROCESSING_DATABASE,
        LABEL_COMMUNITY_ALL_DONE,
        LABEL_COMMUNITY_NOTIFICATION_CENTER,
        LABEL_NEED_HELP,
        LABEL_COMMUNITY_DOWNLOAD_TEMPLATE,
        LABEL_COMMUNITY_FINISH,
        LABEL_COMMUNITY_NONE
    }

    @api start(){
        if (hasAdminMassivePermission){
            this.path = 1;
        }
        else{
            console.log("Else star");
            this.createMassiveRegister();
            this.path = 2;
        }    
    }

    render(){
        if(this.path === 0)return TEM_BLANK;
        else if(this.path === 1)return TEMP_SCREEN_SELECT_RESELLER;
        else if(this.path === 2)return TEMP_SCREEN_UPLOAD_DOWNLOAD;
        else if(this.path === 3)return TEMP_SCREEN_FILE_CONFIRMATION;
        else if(this.path === 4)return TEMP_SCREEN_PROCESSING;
        else if(this.path === 5){
        this.setStatusLoading();
        return TEMP_SCREEN_ALL_DONE;
        }
  
    }

    renderedCallback() {
        Promise.all([
            loadStyle(this, resellerFileSelector)
        ]);        
    }

    setStatusLoading() {  
       
        setStatusLoading({  
            massiveRequestId: this.recordId 
            
        })  
            .then(result => {  
            if (result) {  
                console.log('result : ' + result); 
                
            }  
            }).catch(error => {  
            console.log('error ', error);  
            });  
    }  

    handleNext(e){ 
        this.path++;
        if(this.path == 4){
            this.toggleProgress();
        }
        if(this.path == 3 && this.uploadedFiles.length < 1){
            this.showToast('Toast Warning', 'It is mandatory to attach the file', 'warning');
            this.path = 2;
        }
    }

    handleBack(){
        this.path--;
        this.buttonContinue = true;
    }

    closeModal(){
        this.recordId = null;
        this.path = 0;
        this.progress = 0;
        this.buttonContinue = true;
    }

    handleSubmit(e){
        e.preventDefault();
        const fields = e.detail.files;
        this.template.querySelector('lightning-file-upload').submit(fields);
    }
    
    uploadedFiles = []; 
    file; 
    fileContents; 
    fileReader; 
    content; 
    fileName;

        
    onFileUpload(e) {   
        this.fileName = e.detail.files[0].name; 
        this.uploadedFiles = e.detail.files;
        this.countAccountRequestInSheet();
        console.log("File name" + e.detail.files[0].name); 
    }  

    get getFileName(){
        return this.fileName;
    }
    
    @wire(getRecord, {recordId : USER_ID, fields:[USER_COUNTRY_CODE, USER_NAME, USER_CURRENCY_ISO_CODE]})
    currentUser({error, data}){
        if(error){
            //SHOW ERROR USER
        }
        else if(data){
            this.country = data.fields.CountryCode.value;

            if (this.country == 'BR') this.developerNameParam = SPREADSHEET_BRAZIL;
            else this.developerNameParam = SPREADSHEET_INTERNATIONAL;
        }
    }

    handleStaticResource(){
        getResellerResource({ developerName : this.developerNameParam})
        .then(result =>{
            result = JSON.parse(result);
            this.staticResource = result.staticResource;

            window.open(this.staticResource);
        })
        .catch(error =>{
            this.showToast('Error static resource', error, 'error');
        })     
    }

    get isAdminUserMassivePermission(){
        return hasAdminMassivePermission;
    }

    progressBar() {
        clearInterval(this._interval);
        this.handleNext();
    }


    toggleProgress() {
        this._interval = setInterval(() => {
            this.progress = this.progress === 100 ?  this.progressBar() : this.progress + 5;
        }, 300);
    }
    
    @track items = [];
    @track valueResellers = '';
    @track chosenValue = '';

    @wire(getResellerList)
    wiredUserRoles({ error, data }) {
        if (data) {

            for(let i=0; i<data.length; i++)  {
                this.items = [...this.items ,{value: data[i].Id , label: data[i].Name} ];                                   
            }                
        } else if (error) {
            this.error = error;
        }
    }

    get resellerList() {
        return this.items;
    }

    handleChangeReseller(event) {
        const selectedOption = event.detail.value;
        this.chosenValue = selectedOption;
        this.buttonContinue = false;
    }

    get selectedReseller(){
        return this.chosenValue;
    }

    createMassiveRegister() {  
        createMassive({  
            resellerId: this.selectedReseller,
            userId: this.user_fields.USER_ID 
        })  
        .then(result => {  
        if (result) {  
            this.recordId = result; 
            if (hasAdminMassivePermission){
                this.handleNext();
                this.buttonContinue = true;
            } 
        }  
        }).catch(error => {  
        console.log('error ', error);  
        });  
    }
    
    countAccountRequestInSheet() {  
        countAccountRequest({  
            massId: this.recordId
        })  
        .then(result => {  
            if (result) {
                this.accountsInFile = result;
                console.log('Lines in the file' + this.accountsInFile);  
                if (this.accountsInFile <= 2){
                    this.showToast(LABEL_COMMUNITY_GENERAL_ERROR, LABEL_COMMUNITY_MULTIPLE_FILE_RULE, 'error');
                }else{
                    this.buttonContinue = false;
                }
            }  
        }).catch(error => {  
            console.log('error ', error);  
        });  
    } 

    showToast(title, mensage, variant) {
        const event = new ShowToastEvent({
            title: title,
            message: mensage,
            variant: variant,
            mode: 'dismissable'
        });
        this.dispatchEvent(event);
    }

    openVisualForcePage(event) {

        const urlWithParameters = '/resellerhub/apex/MassiveHelpPage';
        console.log('urlWithParameters...'+urlWithParameters);
        this[NavigationMixin.Navigate]({
        type: 'standard__webPage',
        attributes: {
        url: urlWithParameters
        }
        }, false);
    }

}

import LABEL_COMMUNITY_COMPANY_QUERY from '@salesforce/label/c.Community_Multiple_company_query';
import LABEL_COMMUNITY_SELECT_RESELLER from '@salesforce/label/c.Community_Select_Reseller';
import LABEL_COMMUNITY_CONTINUE from '@salesforce/label/c.Community_Next';
import LABEL_COMMUNITY_BACK from '@salesforce/label/c.Community_Back';
import LABEL_COMMUNITY_UPLOAD from '@salesforce/label/c.Community_Upload';
import LABEL_COMMUNITY_DROP_FILE from '@salesforce/label/c.Community_drop_file';
import LABEL_COMMUNITY_LEAST_2_ACCOUNTS from '@salesforce/label/c.Community_Least_2_accounts';
import LABEL_COMMUNITY_LEARN_HOW from '@salesforce/label/c.Community_Learn_how';
import LABEL_COMMUNITY_BEEN_UPLOADED from '@salesforce/label/c.Community_Been_uploaded';
import LABEL_COMMUNITY_PROCESSING_DATABASE from '@salesforce/label/c.Community_Processing_database';
import LABEL_COMMUNITY_ALL_DONE from '@salesforce/label/c.Community_All_done';
import LABEL_COMMUNITY_NOTIFICATION_CENTER from '@salesforce/label/c.Community_Notification_center';
import LABEL_NEED_HELP from '@salesforce/label/c.Community_Need_help';
import LABEL_COMMUNITY_DOWNLOAD_TEMPLATE from '@salesforce/label/c.Community_Download_template';
import LABEL_COMMUNITY_FINISH from '@salesforce/label/c.Community_Finish';
import LABEL_COMMUNITY_NONE from '@salesforce/label/c.Community_None';
import LABEL_COMMUNITY_GENERAL_ERROR from '@salesforce/label/c.Community_GeneralError';
import LABEL_COMMUNITY_MULTIPLE_FILE_RULE from '@salesforce/label/c.Community_MultipleFileRule';

import USER_ID from '@salesforce/user/Id';
import USER_COUNTRY_CODE from '@salesforce/schema/User.CountryCode';
import USER_NAME from '@salesforce/schema/User.Name';
import USER_CURRENCY_ISO_CODE from '@salesforce/schema/User.CurrencyIsoCode';

import hasAdminMassivePermission from '@salesforce/customPermission/Reseller_Admin';