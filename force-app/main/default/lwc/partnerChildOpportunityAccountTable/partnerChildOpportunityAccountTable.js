import { LightningElement, api, track } from 'lwc';

const columns = [
    { label: 'Account Name', fieldName: 'accountId', type: 'url',  typeAttributes: { label: { fieldName: 'accountName' }, target: '_self' }},
    { label: 'Partner Level', fieldName: 'partnerLevel' },
    { label: 'Legal Representative', fieldName: 'legalRepresentativeId', type: 'url',  typeAttributes: { label: { fieldName: 'legalRepresentativeName' }, target: '_blank' }},

];

export default class PartnerChildOpportunityAccountTable extends LightningElement {
    @api accountLst = [];
    columns = columns;


    get dataSize() {
        return this.accountLst.length;
    }
}