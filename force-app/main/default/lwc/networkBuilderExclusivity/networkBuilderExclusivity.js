import { LightningElement, api, wire } from 'lwc';
import updateAccountLst from '@salesforce/apex/NetworkBuilderEditAllController.updateAccountLst';
import getAccountRecordTypeId from '@salesforce/apex/NetworkBuilderEditAllController.getAccountRecordTypeId';
import getAccountData from '@salesforce/apex/NetworkBuilderEditAllController.getAccountData';


import { reduceErrors } from 'c/ldsUtils';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class NetworkBuilderExclusivity extends LightningElement {

    accountRecordTypeId;
    @api selectedAccountsId;
    @api hasExclusivityAccounts;

    selected; //for storing answers
    spinner = false;
    accounts;
    isLoaded = false;
    allAccountsApproved;
    requiresApproval;

    exclusiveOptions = [

        {
            id: "Question1",
            question: "Is exclusive?",
            answers: {
                yes: "Yes",
                no: "No",

            },
        }
    ]

    connectedCallback() {
        console.log('ids selecionados ' + this.selectedAccountsId);
        getAccountRecordTypeId({ recordTypeDevName: 'Partner_Flow_Account' }).then((result) => {
            this.accountRecordTypeId = result;
            console.log('recordtype ' + this.accountRecordTypeId);
        }).catch((error) => {
            let err = reduceErrors(error);
            console.log('erro: ' + JSON.stringify(err));
            this.showToastNotification(this.errorAdmin + ' ' + err);
        });
    }

    @wire(getAccountData, { accountIds: '$selectedAccountsId'})
    wiredAccounts(result) {
        if (result.data) {
            this.accounts = result.data;
            this.areAllAccountsApproved();
            this.requiresApproval = !this.allAccountsApproved && this.hasExclusivityAccounts;
        } else if (result.error) {
            this.showNotification('Ops! Error retrieving accounts data', 'Please contact your salesforce admin', 'error');
            this.accounts = null;
        }
        this.isLoaded = true;
    };

    get requiresApproval() {
        if(this.accounts !== undefined)
        return this.requiresApproval;
    }

    areAllAccountsApproved() {
        let accountsApproved = true;
        for (let account of this.accounts) {
            if(!account.isExclusivityApproved) {
                accountsApproved = false;
            }
        }
        accountsApproved ? this.selected = 'Yes' : this.selected = 'No';
        this.allAccountsApproved = accountsApproved;
    }

    dispatchToast(title, variant, message) {
        let successtEvnt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant
        });
        this.dispatchEvent(successtEvnt);
    }

    isInputValid() {
        let isValid = true;
        this.inputFields = this.template.querySelectorAll('.validate');
        this.inputFields.forEach(inputField => {
            if (inputField.value == null || inputField.value == '') {
                isValid = false;
            }
        });
        return isValid;
    }

    submitDetails(event) {
        this.isInputValid();

        if (this.isInputValid()) {
            this.showSpinner();
            let accountFields = {};
            this.template
                .querySelectorAll("lightning-input-field")
                .forEach((input) => {
                    accountFields[input.fieldName] = input.value;
                    console.log(accountFields);
                });
            updateAccountLst({ partnersToUpdate: JSON.stringify(accountFields), selectedRecords: this.selectedAccountsId })
                .then((data) => {
                    this.dispatchToast('Saved!', 'success', 'Partner(s) updated.');
                    this.hideSpinner();
                    this.closeModal();
                })
                .catch((error) => {
                    this.dispatchToast('Error', 'error', reduceErrors(error)[0]);
                    this.hideSpinner();

                });
        } else {
            this.dispatchToast('', 'error', 'Required fields missing!');
            this.hideSpinner();
        }

    }

    closeModal() {
        console.log('On close modal');
        const myEvent = new CustomEvent('close');
        this.dispatchEvent(myEvent);
    }

    handleGoBack() {
        console.log('On go back');
        const myEvent = new CustomEvent('goback');
        this.dispatchEvent(myEvent);
    }

    changeHandler(event) {
        // const { name, value } = event.target
        // const name = event.target.name
        const value = event.target.value
        this.selected = value;
        console.log('valor selecionado: ' + this.selected);
    }

    showSpinner() {
        this.spinner = true;
    }

    hideSpinner() {
        this.spinner = false;
    }

    showNotification(aTitle, aMessage, aVariant) {
        const evt = new ShowToastEvent({
            title: aTitle,
            message: aMessage,
            variant: aVariant,
        });
        this.dispatchEvent(evt);
    }

}