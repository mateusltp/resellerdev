import { LightningElement, api } from 'lwc';

export default class GenericCurrencyInput extends LightningElement {

    @api label = 'Input Label';

    @api variant = "standard";

    @api value; //required field

    @api required = false;

    @api messageWhenValueMissing = 'Complete this field.';

    @api messageWhenRangeOverflow = 'The number is too high.';

    @api messageWhenRangeUnderflow = 'The number is too low.';

    @api disabled = false;

    @api currencyCode; //required field

    @api currencyDisplayAs = 'symbol'; // it can be symbol or code

    @api locale = 'en-US';

    @api hideErrorMessage = false; // if true, the label with the error is not shown

    @api min;

    @api max;

    editing = false;

    errorMessage;

    markedAsError = false;

    get displayValue() {
        if((!this.value && this.value !== 0 || this.value.length === 0 ))
            return '';

        let numberFormat = new Intl.NumberFormat(this.locale, {
            style: 'currency',
            currency: this.currencyCode,
            currencyDisplay: this.currencyDisplayAs
          }).format(this.value);
          return numberFormat;
    }

    get showErrorMessage() {
        return this.markedAsError && !this.hideErrorMessage;
    }

    renderedCallback() {
        if(this.editing) {
            const numberInput = this.template.querySelector('[data-id="numberInput"]');
            numberInput.focus();
            if(this.markedAsError) {
                numberInput.classList.add('slds-has-error');
            }
        }      
        else {
            this.reportValidity();
        }
    }

    handleOnFocus() {
        this.editing = true;
    }

    handleFocusOut() {
        this.editing = false;
    }

    handleChange(event) {
        this.value = event.detail.value;

        this.dispatchEvent(new CustomEvent('change', {
            detail: {value: this.value}
          })
        );
    }

    @api
    reportValidity() {
        if(this.checkValidity()) {
            this.hideError();
            return true;
        }
        else {
            this.showError();
            return false;
        }
    }

    @api
    checkValidity() {
        if(this.required && !this.value) {
            this.errorMessage = this.messageWhenValueMissing;
            return false;
        }
        if(this.value) {
            let minValid = true;
            if(this.min) {
                minValid = parseFloat(this.value) >= parseFloat(this.min);
            }
            let maxValid = true;
            if(this.max) {
                maxValid = parseFloat(this.value) <= parseFloat(this.max);
            }
            if(!minValid) 
                this.errorMessage = this.messageWhenRangeUnderflow;
            if(!maxValid) 
                this.errorMessage = this.messageWhenRangeOverflow;
            return minValid && maxValid;
        }
        else
            return true;
    }

    hideError() {
        let input = this.template.querySelector('[data-id="input"]');
        input.classList.remove('slds-has-error');
        this.markedAsError = false;
    }

    showError() {
        let input = this.template.querySelector('[data-id="input"]');
        input.classList.add('slds-has-error');
        this.markedAsError = true;
    }


    

}