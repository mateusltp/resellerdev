import { LightningElement, api } from 'lwc';

export default class GenericLookupItem extends LightningElement {

    @api record;
    @api iconName;

    selectRecord() {
        this.dispatchEvent(new CustomEvent('select', { 
            detail: this.record
        }));
    }

    get rootClass() {
        return "slds-media slds-listbox__option slds-listbox__option_entity" + (this.record.secondary ? ' slds-listbox__option_has-meta' : '');
    }
}