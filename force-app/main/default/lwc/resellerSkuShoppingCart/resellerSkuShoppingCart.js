import { LightningElement, api, track  } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { FlowNavigationNextEvent, FlowNavigationBackEvent , FlowNavigationFinishEvent } from 'lightning/flowSupport';
import fillRequiredFields from '@salesforce/label/c.GenericFillRequiredFields';
import typeLabel from '@salesforce/label/c.profServicesTypes';

export default class ResellerSkuShoppingCart extends LightningElement {
    @api recordId;
    @api oppNumEmployees;
    @api newProductsScreen;
    @api lstSkuPriceData = [];

    @api oppCurrencyCode;

    @track columns;

    @track isModalOpen = true;

    @track isLoading = false;

    @track disableNextBtn = true;

    @track noProductsMessage1 = 'No products left';

    @track noProductsMessage2 = 'You can edit the ones you already added.';

    @api flowErrorMessage;

    selectedRecords = [];
    profServicesTypeLabel = typeLabel;

    get modalClass() {
		return `slds-modal slds-modal_medium ${this.isModalOpen ? "slds-fade-in-open" : ""}`;
	}

    get modalBackdropClass() {
		return `slds-backdrop ${this.isModalOpen ? "slds-backdrop_open" : ""}`;
	}

    get noProducts() {
        return this.lstSkuPriceData.length === 0;
    }

    @api
    get isToClose() {
        return !this.isModalOpen;
    }

    closeModal() {
        this.isModalOpen = false;
        this.dispatchEvent(new FlowNavigationNextEvent());
    }

    @api
    get lstSkuPrice() {
        return this.lstSkuPriceData;
    }

    set lstSkuPrice( alstSkuPrice ) {
        this.lstSkuPriceData = JSON.parse(JSON.stringify(alstSkuPrice));
        let i = 1;
        this.lstSkuPriceData.forEach( (element) => {
            element.rowIndex = i;
            element = this.handleProfessionalServicesTypes(element);
            i++;
        });
    }

    handleProfessionalServicesTypes(element){
        if(this.hasProfServicesTypes(element)){
            element.Product2.ListTypes = element.Product2.Professional_services_types__c.split(";");
            element = this.returnDisplayType(element);
        }
        return element;
    }

    hasProfServicesTypes(element){
        return element.hasOwnProperty('Product2') && element.Product2.Professional_services_types__c && element.Product2.Professional_Services_Type_Selection__c;
    }

    returnDisplayType(element){
        element.Product2.DisplayCheckbox = false;
        element.Product2.DisplayRadio = false;
        element.Product2.NoDisplay = false;
        switch (element.Product2.Professional_Services_Type_Selection__c) {
            case 'Checkbox':
                element.Product2.DisplayCheckbox = true;
                break;
            case 'Radio':
                element.Product2.DisplayRadio = true;
                break;
            case 'Select all':
                element.Product2.NoDisplay = true;
                element = this.automaticFillTypeFields(element);
                break;
            default:
                break;
        }
        
        return element;
    }

    automaticFillTypeFields(element){
        element.Product2.ListTypes.forEach(type => {
            element[type + '__c'] = true;
        });

        return element;
    }

    @api
    get lstSelectedRecords() {
        return this.selectedRecords;
    }

    @api
    get lstSelectedLineItemRecords() {
        return this.lstSkuPriceData;
    }

    handleClose() {
        this.closeModal();
    }

    handleUnitPriceChange(event) {
        const value = event.detail.value;
        const id = event.target.dataset.id;

        for(let i = 0; i < this.lstSkuPriceData.length; i++) {
            let product = this.lstSkuPriceData[i];
            if( product.Product2Id === id ){
                product.UnitPrice = value;
                product.Total_Price__c = (product.UnitPrice * product.Quantity).toFixed(2);
                product.Discount = ( ( 1 - (product.UnitPrice / product.List_Price__c) ) *100 ).toFixed(2);
                if(product.Discount < 0 || product.Discount > 100) {
                    product.Discount = 0;
                }
            }
        }

        this.lstSkuPriceData = [...this.lstSkuPriceData];

    }

    get selectedProductsScreen(){
        return !this.newProductsScreen;
    }

    onCheckboxChangeTypeValue(event){
        const checked = event.target.checked;
        const value = event.target.value;
        const id = event.target.dataset.id;
        const index = this.lstSkuPriceData.findIndex(product => product.Product2Id == id);
        if(index != -1){
            this.lstSkuPriceData[index][value + '__c'] = checked;
        }
    }

    onRadioChangeTypeValue(event){
        const value = event.target.value;
        const id = event.target.dataset.id;
        const index = this.lstSkuPriceData.findIndex(product => product.Product2Id == id);
        if(index != -1){
            this.lstSkuPriceData[index][value + '__c'] = true;
            this.updateValueOnRemainingTypes(value, index); 
        }
    }

    updateValueOnRemainingTypes(value, index){
        this.lstSkuPriceData[index].Product2.ListTypes.forEach(type => {
            if(type != value){
                this.lstSkuPriceData[index][type + '__c'] = false;
            }
        });
    }

    handleSelectAll(event) {
        if(event.target.checked) {
            this.lstSkuPriceData.forEach((element) => {
                this.addProductToSelectedList(element);
            });
        }
        else {
            this.lstSkuPriceData.forEach((element) => {
                this.removeProductFromSelectedList(element);
            });

        }
        this.updateNextBtnStatus();
    }

    addProductToSelectedList(product) {
        this.selectedRecords.push(product);
        let tr = this.template.querySelector('tr[data-id="' + product.Product2Id + '"]');
        tr.classList.add('slds-is-selected');
        let checkbox = this.template.querySelector('lightning-input[data-id="' + product.Product2Id + '"]');
        checkbox.checked = true;
    }

    removeProductFromSelectedList(product) {
        const index = this.selectedRecords.findIndex((element) => element.Product2Id === product.Product2Id);
        this.selectedRecords.splice(index, 1);
        let tr = this.template.querySelector('tr[data-id="' + product.Product2Id + '"]');
        tr.classList.remove('slds-is-selected');
        let checkbox = this.template.querySelector('lightning-input[data-id="' + product.Product2Id + '"]');
        checkbox.checked = false;
    }

    handleRecordSelection(event) {
        const id = event.target.dataset.id;
        const product = this.lstSkuPriceData.find(element => element.Product2Id === id);
        if(event.target.checked) {
            this.addProductToSelectedList(product);
        }
        else {
            this.removeProductFromSelectedList(product);
        }
        this.updateNextBtnStatus();
    }

    updateNextBtnStatus() {
        this.disableNextBtn = this.selectedRecords.length === 0;
    }

    handleDeleteProduct(event) {

        const id = event.target.dataset.id
        
        const rows = JSON.parse(JSON.stringify(this.lstSkuPriceData));
        const rowIndex = rows.findIndex((element) => element.Product2Id === id);
        rows.splice(rowIndex, 1);
        this.lstSkuPriceData = rows;
        if(this.lstSkuPriceData.length === 0) 
            this.dispatchEvent(new FlowNavigationBackEvent());

    }

    handleNext() {
        this.dispatchEvent(new FlowNavigationNextEvent());
    }

    handleBack() {
        this.dispatchEvent(new FlowNavigationBackEvent());
    }

    handleCancel() {
        this.closeModal();
    }

    handleSave() {
        let salesPriceInputs = this.template.querySelectorAll('c-generic-currency-input');
        let valid = true;
        salesPriceInputs.forEach(input => valid = valid && input.reportValidity());
        this.validateProfessionalServiceTypes();
        if(valid && this.validateProfessionalServiceTypes()) {
            this.removeProduct2Property();
            this.isLoading = true;
            this.dispatchEvent(new FlowNavigationNextEvent());
        }
        else {
            this.dispatchEvent(
                new ShowToastEvent({
                    message: fillRequiredFields,
                    variant: 'error'
                }),
            );
        }
    }

    validateProfessionalServiceTypes(){
        let productsWithProfServicesTypes = this.lstSkuPriceData.filter(product => product.hasOwnProperty('Product2') && !product.Product2.NoDisplay);
        let countValids = 0;
        if(productsWithProfServicesTypes && productsWithProfServicesTypes.length > 0){
            productsWithProfServicesTypes.forEach(product => {
                if(this.checkForAtLeastOneCheckedTypeOnProduct(product)){
                    countValids++;
                }
            });
        }else{
            return true;
        }

        return countValids == productsWithProfServicesTypes.length;
    }

    checkForAtLeastOneCheckedTypeOnProduct(product){
        let hasOneCheckedType = false;
        for(const type of product.Product2.ListTypes){
            if(product[type + '__c']){
                hasOneCheckedType = true;
                break;
            }
        }

        return hasOneCheckedType;
    }

    removeProduct2Property(){
        this.lstSkuPriceData.forEach(product => {
            if(product.hasOwnProperty('Product2')){
                delete product.Product2;
            }
        });
    }
    
}