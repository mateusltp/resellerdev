import { LightningElement, track, api, wire } from 'lwc';

import { FlowAttributeChangeEvent } from 'lightning/flowSupport';

import fetchDefaultValue from '@salesforce/apex/ResellerEJContactLookupController.fetchSingleRecordSelected';
import apexContactSearch from '@salesforce/apex/ResellerEJContactLookupController.searchContact';

export default class ResellerFastTrackOperationsContact extends LightningElement {
    //-- configurable parameters
    @api defaultRecordId;
    @api accountId;
    @api lookupLabel = '';
    @api lookupPlaceholder = '';
    @api isRequired = false;

    @api searchField = 'Name';

    @api operationsContactId;
    @api lstSelectedRecordIds = [];

    defaultResult = {};
    listOfDefaultResults = [];
    @track items;
    value;

    connectedCallback() {
        this.getDefaultValues();
        this.handleContactSearch();
    }

    getDefaultValues() {
        console.log('this.defaultRecordId ',this.defaultRecordId)
        fetchDefaultValue({defaultRecordId: this.defaultRecordId})
        .then((result) => {
            console.log('result getDefaultValues',result)
            
            this._searchedPreviousRecordMap = result.reduce((res, item) => {
                res[item.Id] = item;
                this.value = item.Id;
                return res;
            }, {});

            this.listOfDefaultResults = result.map(item => {
                return {
                    id: item.Id,
                    icon : 'standard:contact',
                    sObjectType : 'Contact',
                    title: this.getFieldValue(this.searchField, item),
                };
            });

            this._previousRecordItemMap = this.listOfDefaultResults.reduce((res, item) => {
                res[item.id] = item;
                return res;
            }, {});

            this.defaultResult = this.listOfDefaultResults.at(0);
            this.operationsContactId = this.listOfDefaultResults.at(0).id;
            this.lstSelectedRecordIds = this.lstSelectedRecordIds.concat(this.listOfDefaultResults.at(0).Id);
            
        })
        .catch((error) => {
            this.error = error;
            // this.hideSpinner();
        });
    }

    handleContactSearch() {
        //const lookupElement = event.target;
        console.log('this.accountId ',this.accountId)
        apexContactSearch({ 
                accountId: this.accountId
            })
            .then(results => {
                console.log('result ',results);
                let options = [];
                //lookupElement.setSearchResults(results);

                for(var key in results){                  
                    options.push({ label: results[key].title, value: results[key].id});
                }
                this.items = options;
                //console.log('options ',options);
                //console.log('items ',this.items);
            })
            .catch(error => {
                console.log(error);
            });
    }

    handleSelectedRecords(event) {
        //-- the custom lookup already updates the current selection
        this.lstSelectedRecordIds = event.detail;

        this.operationsContactId = event.detail[0];
        
        const attributeChangeEvent = new FlowAttributeChangeEvent(
            'operationsContactId',
            event.detail[0]
        );
        this.dispatchEvent(attributeChangeEvent);
    }

    getFieldValue(fieldName, obj) {
        if (fieldName) {
            return fieldName.split('.').reduce((result, item) => {
                return result[item];
            }, obj);
        }
        return '';
    }

    handleChange(event) {
        //console.log('teste ',event.detail.value)
        this.value = event.detail.value;
        this.operationsContactId = event.detail.value;

    }
}