import { LightningElement, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import apexSearch from '@salesforce/apex/SearchCustomLookupController.userSearch';
import createChildOpportunity from '@salesforce/apex/PartnerChildOpportunityManagerController.createChildOpportunity'
import { handleValidation } from 'c/customLayoutUtils';
import { reduceErrors } from 'c/ldsUtils';

export default class PartnerChildOpportunityCreator extends LightningElement {

    @api opportunityId;
    @api parentAccountId;
    @api parentAccountName;
    @api accountLst = [];
    showSpinner = false;
    legalRepresentativeId;
    ownerId;
    accountInOpp = [];
    noOwnerMessage;

    connectedCallback() {
        for (let i = 0; i < this.accountLst.length; i++) {
            this.accountInOpp.push(this.accountLst[i].accountId.split('/')[1]);
            if (this.accountLst[i].legalRepresentativeId != null && this.legalRepresentativeId == null) {
                this.legalRepresentativeId = this.accountLst[i].legalRepresentativeId.split('/')[1];
            }
        }
    }

    handleSearch(event) {
        const lookupElement = event.target;
        apexSearch(event.detail)
            .then(results => {
                lookupElement.setSearchResults(results);
            })
            .catch(error => {
                reduceErrors(error);
            });
    }

    setOwnerId(event){
        this.ownerId = event.detail[0];
    }

    handleOpportunityCreation(event) {
        this.noOwnerMessage = '';
        var inputFields = [];
        var oppNameField = this.template.querySelectorAll("[data-id='oppName']")[0];
        var oppCloseDateField = this.template.querySelectorAll("[data-id='oppCloseDate']")[0];
        inputFields.push(oppNameField);
        inputFields.push(oppCloseDateField);
        const isCorrectInput = handleValidation(inputFields);
        if (isCorrectInput && this.ownerId != null) {
            this.showSpinner = true;
            this.insertOpportunity(oppNameField.value, oppCloseDateField.value);
        } else if(this.ownerId == null ){
            this.noOwnerMessage = 'Please select an owner';
        }
    }

    // String name, Date closeDate, String ownerId, String accountId, String masterOpportunityId, List<Id> oppMemberIds 
    insertOpportunity(oppName, oppCloseDate) {
        createChildOpportunity({ name: oppName, closeDate: oppCloseDate, ownerId: this.ownerId, accountId: this.parentAccountId, masterOpportunityId: this.opportunityId, accountInOppIds: this.accountInOpp })
            .then(results => {
                this.dispatchToast('Success!', 'success', 'Opportunity ' + results + ' created.');
                this.showSpinner = false;
                this.refreshParentCmp();
            })
            .catch(error => {
                this.dispatchToast('Error', 'error', reduceErrors(error)[0]);
                this.showSpinner = false;
            });
    }

    dispatchToast(title, variant, message) {
        let successtEvnt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant
        });
        this.dispatchEvent(successtEvnt);
       
    }

    refreshParentCmp(){
        const refreshEvt = new CustomEvent('refreshdata', { detail: this.accountLst});
        this.dispatchEvent(refreshEvt);
    }
}