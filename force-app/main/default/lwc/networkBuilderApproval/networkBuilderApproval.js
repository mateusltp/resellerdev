/**
 * @description       : 
 * @author            : bruno.mendes@gympass.com
 * @group             : 
 * @last modified on  : 10/05/2022
 * @last modified by  : bruno.mendes@gympass.com
**/
import { LightningElement, api, wire } from 'lwc';
import sendAccountsForExclusivityApproval from '@salesforce/apex/NetworkBuilderApprovalController.sendAccountsForExclusivityApproval';

export default class NetworkBuilderApproval extends LightningElement {
    @api
    selectedAccountIds;

    exclusivityComments;
    spinner = false;

    accountsSentForApproval;
    accountsNotSentForApproval;

    hasApprovalResponse = false;

    handleExclusivityCommentsChange(event) {
        const commentsValue = event.target.value;
        this.exclusivityComments = commentsValue;
    }

    handleCloseModal() {
        const myEvent = new CustomEvent('close');
        this.dispatchEvent(myEvent);
    }

    async handleSendForApproval() {
        console.log(JSON.stringify(this.selectedValues));
        console.log(JSON.stringify(this.comments));
        console.log(JSON.stringify(this.selectedAccountIds));
        this.showSpinner();
        await sendAccountsForExclusivityApproval(
                {accountIds : this.selectedAccountIds, comments : this.exclusivityComments}
            ).then(
                (result) => {
                    console.log('result');
                    console.log(result);
                    for (let response of result) {
                        if (response.errorMessage) {
                            if(!this.accountsNotSentForApproval) this.accountsNotSentForApproval = [];
                            this.accountsNotSentForApproval.push(response);
                        } else {
                            if(!this.accountsSentForApproval) this.accountsSentForApproval = [];
                            this.accountsSentForApproval.push(response);
                        }
                    }
                    console.log(this.accountsNotSentForApproval);
                    console.log(this.accountsSentForApproval);
                    this.hasApprovalResponse = true;
                    this.hideSpinner();
                })
             .catch((error) => {
                   this.hideSpinner();
                   console.log('error');
                   console.log(error);
               });
    }

    showNotification(aTitle, aMessage, aVariant) {
        const evt = new ShowToastEvent({
            title: aTitle,
            message: aMessage,
            variant: aVariant,
        });
        this.dispatchEvent(evt);
    }

    showSpinner() {
        this.spinner = true;
    }

    hideSpinner() {
        this.spinner = false;
    }
}