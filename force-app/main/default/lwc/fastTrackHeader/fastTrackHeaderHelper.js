/**
 * @description       : 
 * @author            : tiago.ribeiro@gympass.com
 * @group             : 
 * @last modified on  : 04-07-2022
 * @last modified by  : tiago.ribeiro@gympass.com
**/
export default class fastTrackHeaderHelper{

    jsonToMap(jsonString){
        var resultMap = new Map();
        if(!jsonString){
            return resultMap;
        }
        var jsonObject = JSON.parse(jsonString);
        var dataObject = jsonObject.fields;
        var dataMap = new Map(Object.entries(dataObject));
        for (const key of dataMap.keys())  {
            var keyMap = new Map(Object.entries(dataMap.get(key)));
            resultMap.set(key, keyMap);
        }

        return resultMap;
    }

    processFields(mappedFields){
        let htmlContainer = '';
        for (const [key, value] of mappedFields.entries()) {
            let displayValueHtml = this.processValueHtml(value);
            htmlContainer += this.generateHtml(key, displayValueHtml);
        }
        
        return htmlContainer;
    }

    processValueHtml(value){
        let valueHtml;
        if(value.get("recordId")){
            valueHtml = this.generateUrlLink(value);
        }else{
            valueHtml = this.generateValue(value.get("value"));
        }

        return valueHtml;
    }

    generateUrlLink(urlValue){
        return '<p class="slds-form-element__label"><a href="/' + urlValue.get('recordId')+ '">' + urlValue.get('value') + '</a></p>'
    }

    generateValue(value){
        return '<p class="slds-form-element__label">' + value + '</p>';
    }

    generateHtml(value, displayValueHtml){
        return '<div class="slds-col">' +
                    '<p>' + value + '</p>' +
                    displayValueHtml +
                '</div>';
    }
}