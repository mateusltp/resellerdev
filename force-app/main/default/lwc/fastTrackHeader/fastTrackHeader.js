/**
 * @description       : 
 * @author            : tiago.ribeiro@gympass.com
 * @group             : 
 * @last modified on  : 06-07-2022
 * @last modified by  : tiago.ribeiro@gympass.com
**/
import { LightningElement, api } from "lwc";
import FastTrackHelper from "./fastTrackHeaderHelper";

export default class FastTrackHeader extends LightningElement {
    @api label = 'Mark as Lost';
    @api variant = 'neutral';
    @api title = 'Mark as Lost';
    @api cssClass;
    @api fieldsDisplayed;
    @api displayButton;
    @api headerPopup = 'Mark Opportunity as Lost';
    @api objectAPIName = 'Opportunity';
    @api recordId;

    helper = new FastTrackHelper();

    renderedCallback(){
        this.init();
    }

    init(){
        if(!this.fieldsDisplayed){
            return;
        }
        let mappedFields = this.helper.jsonToMap(this.fieldsDisplayed);
        let htmlContainer = this.helper.processFields(mappedFields);
        this.template.querySelector('.elementHTMLcontainer').innerHTML = htmlContainer;
    }

    handleClick(){
        const lostOppModal = this.template.querySelector("c-lost-opp-popup");
        lostOppModal.recordId = this.recordId;
        lostOppModal.objectAPIName = this.objectAPIName;
        lostOppModal.headerPopup = this.headerPopup;
        lostOppModal.show();
    }
}