import LightningDataTable from 'lightning/datatable';
import { api  } from 'lwc';
import { loadStyle } from 'lightning/platformResourceLoader';
import DatatablePicklistTemplate from './picklist-template.html';
//import LookupTemplate from './lookup-template.html';
import ContactTemplate from './contact-template.html';
import CustomDataTableResource from '@salesforce/resourceUrl/CustomDataTable';
//import CustomDataTableResource from './customDataTable.css';

export default class CustomDataTable extends LightningDataTable {

    static customTypes = {
        picklist: {
            template: DatatablePicklistTemplate,
            typeAttributes: ['label', 'placeholder', 'options', 'value', 'context', 'selectedRows'],
        },
        /*
        lookup: {
            template: LookupTemplate,
            typeAttributes: ['uniqueId', 'object', 'icon', 'label', 'displayFields', 'displayFormat', 'placeholder', 'filters']
        },
        */
        contact : {
            template: ContactTemplate,
            typeAttributes: ['contacts']
        }
    };

    constructor(){
        super();
        Promise.all([
            loadStyle(this, CustomDataTableResource),
        ]).then(() => {})
        
    }

}