import { LightningElement, track, wire, api } from 'lwc';
import { getRecordCreateDefaults, getRecord, getFieldValue } from 'lightning/uiRecordApi';
import { getPicklistValues } from 'lightning/uiObjectInfoApi';
import { getNewLayoutTemplate, disableFieldsFromSection, enableFieldsFromSection, recordInputForCreate, handleValidation } from 'c/customLayoutUtils';
import COMMERCIAL_CONDITION_OBJECT from '@salesforce/schema/Commercial_Condition__c';
import NAME_FIELD from '@salesforce/schema/Commercial_Condition__c.Name';

import THRESHOLD_OBJECT from '@salesforce/schema/Threshold__c';
import NAME_THRESHOLD_FIELD from '@salesforce/schema/Threshold__c.Name';
import VOLUME_DISCOUNT_DATE_FIELD from '@salesforce/schema/Threshold__c.Volume_discount_date__c';
import VOLUME_DISCOUNT_TYPE_FIELD from '@salesforce/schema/Threshold__c.Volume_Discount_Type__c';


import getStandardDiscounts from '@salesforce/apex/NewPartnerProductController.getStandardDiscount';
import { reduceErrors } from 'c/ldsUtils';
import PRODUCT_ACCOUNT_FIELD from '@salesforce/schema/Product_Item__c.Opportunity__r.AccountId';
import OPP_ACCOUNT_FIELD from '@salesforce/schema/Opportunity.AccountId';
const productFields = [PRODUCT_ACCOUNT_FIELD];
const oppFields = [OPP_ACCOUNT_FIELD];
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class NewProductCreationExclusivity extends LightningElement {

    @api partnerProductRecordTypes;
    @api partnerProductTo = {};
    @api requiresRadionButton;
    @api radioExclusivityGroupOptions = {};
    @api isClone;
    @api isEdit;
    @api recordId;
    @api objectApiName;
    @api isWishlistAccount;

    @track showSpinner = true;
    @track sectionExclusivityFields = [];
    @track sectionNoShowFields = [];
    @track sectionLateCancelationFields = [];
    @track sectionDiscountsFields = [];
    @track thresholdNumber = [1, 2, 3];
    @track isDiscountDisabled = true;

    @track exclusivitySelectedValue;
    @track noShowSelectedValue;
    @track lateCancelSelectedValue;
    @track volumeDiscountSelectedValue;
    @track isStandardVolDiscount;
    @track discountDate;

    sectionByName = [];
    sectionsToExclude = ['Information'];
    fieldsToExclude = [NAME_FIELD.fieldApiName, NAME_THRESHOLD_FIELD.fieldApiName, VOLUME_DISCOUNT_DATE_FIELD.fieldApiName, VOLUME_DISCOUNT_TYPE_FIELD.fieldApiName];
    isExclusivityReady = 'No';
    isNoShowFeeReady = 'No';
    isLateCancelation = 'No';
    isThresholdsReady = 'No';
    options = [
        { label: 'Yes', value: 'Yes' },
        { label: 'No', value: 'No' },
    ];

    isStandardVolDiscountLoaded = false;
    standardVolumeDiscounts;
    isStandardEnabled = false;

    VOLUME_DISCOUNT_TYPE_WISHLIST = 'By volume of money';
    VOLUME_DISCOUNT_TYPE_LONGTAIL = 'By single user volume';
    volumeDiscountTypeSelectedValue;

    @wire(getRecordCreateDefaults, { objectApiName: COMMERCIAL_CONDITION_OBJECT.objectApiName, recordTypeId: '$partnerProductRecordTypes.exclusivityRecordTypeId' })
    commercialConditionExclusivity({ error, data }) {
        if (data) {
            const template = getNewLayoutTemplate(data, this.sectionsToExclude, this.fieldsToExclude);
            this.sectionExclusivityFields = template.sectionWithFields;
            this.sectionByName.push({ key: this.sectionExclusivityFields[0].key, value: this.sectionExclusivityFields });
            this.loadFieldsValueFromPartnerCommercialConditionTo(this.sectionExclusivityFields, this.partnerProductRecordTypes.exclusivityRecordTypeName);
            this.isExclusivityReady = true;
            this.isLayoutReady();
        } else if (error) {
            console.log('error ' + JSON.stringify(error));
        }
    };

    @wire(getRecord, {recordId : '$recordId', fields: productFields})
    productItemRecord;

    @wire(getRecord, {recordId : '$recordId', fields: oppFields})
    opportunityRecord;

    @wire(getPicklistValues, {
        recordTypeId: '$partnerProductRecordTypes.tresholdPartnerRecordTypeId',
        fieldApiName: VOLUME_DISCOUNT_TYPE_FIELD
    })
    volumeDiscountPicklistValues;


    @wire(getRecordCreateDefaults, { objectApiName: COMMERCIAL_CONDITION_OBJECT.objectApiName, recordTypeId: '$partnerProductRecordTypes.noShowRecordTypeId' })
    commercialConditionNoShowFee({ error, data }) {
        if (data) {
            const template = getNewLayoutTemplate(data, this.sectionsToExclude, this.fieldsToExclude);
            this.sectionNoShowFields = template.sectionWithFields;
            this.sectionByName.push({ key: this.sectionNoShowFields[0].key, value: this.sectionNoShowFields });
            this.loadFieldsValueFromPartnerCommercialConditionTo(this.sectionNoShowFields, this.partnerProductRecordTypes.noShowRecordTypeName);
            this.isNoShowFeeReady = true;
            this.isLayoutReady();
        } else if (error) {
            console.log('error ' + JSON.stringify(error));
        }
    };

    @wire(getRecordCreateDefaults, { objectApiName: COMMERCIAL_CONDITION_OBJECT.objectApiName, recordTypeId: '$partnerProductRecordTypes.lateCancellationRecordTypeId' })
    commercialConditionLateCancelation({ error, data }) {
        if (data) {
            const template = getNewLayoutTemplate(data, this.sectionsToExclude, this.fieldsToExclude);
            this.sectionLateCancelationFields = template.sectionWithFields;
            this.sectionByName.push({ key: this.sectionLateCancelationFields[0].key, value: this.sectionLateCancelationFields });
            this.loadFieldsValueFromPartnerCommercialConditionTo(this.sectionLateCancelationFields, this.partnerProductRecordTypes.lateCancellationRecordTypeName);
            this.isLateCancelation = true;
            this.isLayoutReady();
        } else if (error) {
            console.log('error ' + JSON.stringify(error));
        }
    };


    @wire(getRecordCreateDefaults, { objectApiName: THRESHOLD_OBJECT.objectApiName, recordTypeId: '$partnerProductRecordTypes.tresholdPartnerRecordTypeId' })
    discounts({ error, data }) {
        if (data) {
            const template = getNewLayoutTemplate(data, this.sectionsToExclude, this.fieldsToExclude);
            this.sectionDiscountsFields = template.sectionWithFields;
            this.sectionByName.push({ key: this.sectionDiscountsFields[0].key, value: this.sectionDiscountsFields });
            this.isThresholdsReady = true;
            this.isLayoutReady();
        } else if (error) {
            console.log('error ' + JSON.stringify(error));
        }
    };

    isLayoutReady(){
        if(this.isExclusivityReady && this.isNoShowFeeReady && this.isLateCancelation && this.isThresholdsReady){
            this.showSpinner = false;
        }
    }

    disconnectedCallback(){
        this.handleExclusivityCommercialTOCreation();
        this.handleThresholdTOCreation();
        this.fireEventWithRadioButtonValue();
    }

    handleNumberOfThresholds(e) {

        this.populateThresholdNumberArray(e.detail.value);

    }

    handleStandardVolumeChange(event) {
        let selectedOption = event.detail.value;
        if (selectedOption == 'Yes') {
            this.enableSpinner();
            if (!this.isStandardVolDiscountLoaded) {
                getStandardDiscounts({ accountId: this.accountId }).then((result) => {
                    this.standardVolumeDiscounts = result;
                    this.isStandardVolDiscountLoaded = true;
                    this.populateThresholdsWithStandardValues();
                }).catch((error) => {
                    let err = reduceErrors(error);
                    console.log('erro: ' + JSON.stringify(err));
                    this.showToastNotification(this.errorAdmin + ' ' + err);
                });
            }
            if (this.isStandardVolDiscountLoaded) {
                this.populateThresholdsWithStandardValues();
            }
            this.disableSpinner();
        }
    }

    handleVolumeDiscountTypeChange(event) {
        this.volumeDiscountTypeSelectedValue = event.detail.value;
        console.log('volume discount selected value: '+this.volumeDiscountTypeSelectedValue);
    }


    populateThresholdNumberArray(thresholdLength) {
        this.thresholdNumber = [];
        for (var i = 1; i <= thresholdLength; i++) {
            this.thresholdNumber.push(i);
        }
    }

    populateThresholdsWithStandardValues() {
        for (let i = 1; i <= this.thresholdNumber.length; i++) {
            let queryTerm = "[data-threshold-id='" + i + "']";
            const inputValues = this.template.querySelectorAll(queryTerm);
            if (i == 1) {
                inputValues[0].value = this.standardVolumeDiscounts.thresholdStart1;
                inputValues[1].value = this.standardVolumeDiscounts.discount1;
            }
            if (i == 2) {
                inputValues[0].value = this.standardVolumeDiscounts.thresholdStart2;
                inputValues[1].value = this.standardVolumeDiscounts.discount2;
            }
            if (i == 3) {
                inputValues[0].value = this.standardVolumeDiscounts.thresholdStart3;
                inputValues[1].value = this.standardVolumeDiscounts.discount3;
            }
        }
    }

    get accountId() {
        if (this.objectApiName === 'Product_Item__c') {
            return this.productItemRecord.data.fields.Opportunity__r.value.fields.AccountId.value;
        } else if (this.objectApiName === 'Opportunity') {
            return this.opportunityRecord.data.fields.AccountId.value;
        }
    }

    get numberOfThresholds() {
        return this.thresholdNumber.length;
    }

    handleOptionChange(event) {
        let sectionName = event.currentTarget.name;
        console.log('sectionName: '+sectionName);
        let selectedOption = event.detail.value;

        let sectionToHandle = this.sectionByName.find(o => o.key == sectionName).value;

        if(sectionName.includes('Exclus')){
           this.exclusivitySelectedValue = selectedOption;
        } else if(sectionName.includes('No show')) {           
            this.noShowSelectedValue = selectedOption;            
        } else if(sectionName.includes('Late cancel')) {
           this.lateCancelSelectedValue = selectedOption;
        } else if(sectionName.includes('Discounts')) {
            this.volumeDiscountSelectedValue = selectedOption;
        }


        if (selectedOption == 'Yes') {
            enableFieldsFromSection(sectionToHandle);
            if (sectionToHandle[0].key == 'Discounts') {
                this.isDiscountDisabled = false;
            }
        } else {
            if (sectionToHandle[0].key == 'Discounts') {
                this.isDiscountDisabled = true;
            }
            disableFieldsFromSection(sectionToHandle, true);
        }
    }


    handleExclusivityCommercialTOCreation() {

        const inputExclusivityFields = this.template.querySelectorAll('.c-input-comm-exclus');
        const inputNoShowFields = this.template.querySelectorAll('.c-input-comm-noshow');
        const inputLateCancelFields = this.template.querySelectorAll('.c-input-comm-latecancel');
        
        if (this.validateEmptyFieldsForTO(inputExclusivityFields)) {
            let commercialExclusivityRecordInput = this.populateRecordInput(inputExclusivityFields, COMMERCIAL_CONDITION_OBJECT.objectApiName, this.partnerProductRecordTypes.exclusivityRecordTypeId);
            this.populateProductTO(this.partnerProductRecordTypes.exclusivityRecordTypeName, commercialExclusivityRecordInput);
        } else{
            this.partnerProductTo.removeCommercialCondition(this.partnerProductRecordTypes.exclusivityRecordTypeName);
        }

        if (this.validateEmptyFieldsForTO(inputNoShowFields)) {
            let commercialNoShowRecordInput = this.populateRecordInput(inputNoShowFields, COMMERCIAL_CONDITION_OBJECT.objectApiName, this.partnerProductRecordTypes.noShowRecordTypeId);
            this.populateProductTO(this.partnerProductRecordTypes.noShowRecordTypeName, commercialNoShowRecordInput);
        }else{
            this.partnerProductTo.removeCommercialCondition(this.partnerProductRecordTypes.noShowRecordTypeName);
        }

        if (this.validateEmptyFieldsForTO(inputLateCancelFields)) {
            let commercialLateCancelationRecordInput = this.populateRecordInput(inputLateCancelFields, COMMERCIAL_CONDITION_OBJECT.objectApiName, this.partnerProductRecordTypes.lateCancellationRecordTypeId);
            this.populateProductTO(this.partnerProductRecordTypes.lateCancellationRecordTypeName, commercialLateCancelationRecordInput);
        }else{
            this.partnerProductTo.removeCommercialCondition(this.partnerProductRecordTypes.lateCancellationRecordTypeName);
        }
    }

    handleThresholdTOCreation() {
        this.partnerProductTo.thresholds = [];
        for (let i = 1; i <= this.thresholdNumber.length; i++) {
            let queryTerm = "[data-threshold-id='" + i + "']";
            const inputValues = this.template.querySelectorAll(queryTerm);
            if (this.validateEmptyFieldsForTO(inputValues)) {
                let thresholdRecord = this.populateRecordInput(inputValues, THRESHOLD_OBJECT.objectApiName, this.partnerProductRecordTypes.tresholdPartnerRecordTypeId);
                thresholdRecord.fields[String(VOLUME_DISCOUNT_DATE_FIELD.fieldApiName)] = this.discountDate;
                thresholdRecord.fields[String(VOLUME_DISCOUNT_TYPE_FIELD.fieldApiName)] = this.volumeDiscountTypeSelectedValue;
                this.partnerProductTo.thresholds.push(thresholdRecord);
            }
        }

        console.log(JSON.stringify(this.partnerProductTo));
    }

    populateRecordInput(inputFields, objectApiName, recordTypeId) {
        let recordInput = recordInputForCreate(objectApiName);
        inputFields.forEach(field => {
            recordInput.fields[String(field.name)] = field.value;
        });
        recordInput.fields['RecordTypeId'] = recordTypeId;
        return recordInput;
    }

    populateProductTO(aKey, aValue) {
        let commercialObject = { key: aKey, value: aValue };
        this.partnerProductTo.addNewCommercialConditionDetails(commercialObject);
    }

    @api
    handleExclusivityValidation() {
        var inputFields = [];
        var templateFields = this.template.querySelectorAll('lightning-input-field');
        var templateRadioGroups = this.template.querySelectorAll('lightning-radio-group');
        var templateComboBoxes = this.template.querySelectorAll('lightning-combobox');
        var templateInputs = this.template.querySelectorAll('lightning-input');
        inputFields.push(...templateFields);
        inputFields.push(...templateRadioGroups);
        inputFields.push(...templateComboBoxes);
        inputFields.push(...templateInputs);

        const isCorrectInput = handleValidation(inputFields);
        if(isCorrectInput){
            this.handleExclusivityCommercialTOCreation();
            this.handleThresholdTOCreation();
        }
        return isCorrectInput;
    }

    validateEmptyFieldsForTO(inputFields) {
        for (let i = 0; i < inputFields.length; i++) {
            let field = inputFields[i];
            if (field.disabled == false && field.required && field.value != null) {
                return true;
            }
        }
        return false;
    }

    loadFieldsValueFromPartnerCommercialConditionTo(sectionWithFields, recordTypeName) {
        if (this.partnerProductTo == null || this.partnerProductTo == undefined)
            return;
        
        if ( this.partnerProductTo.getCommercialDetails().length > 0 && this.partnerProductTo.getCommercialCondition(recordTypeName) != undefined) {
     
            sectionWithFields.forEach(section => {
                section.value.forEach(field => {               
                    field.value = this.partnerProductTo.getCommercialCondition(recordTypeName).value.fields[String(field.apiName)];
                    console.log(field.value);
                    if (field.value != null) {
                        this.enableSectionAndSetRadionButton(sectionWithFields, recordTypeName);
                    }
                })
            })
        } else {
            this.disableSectionAndSetRadionButton(sectionWithFields, recordTypeName);
        }


    }

    loadThresholdValues() {

        if (this.partnerProductTo == null || this.partnerProductTo == undefined || this.partnerProductTo.thresholds == undefined)
            return;

        if (this.partnerProductTo.thresholds.length > 0) {
            this.populateThresholdNumberArray(this.partnerProductTo.thresholds.length);
        } else {
            this.populateThresholdNumberArray(3);
        }




        if (this.partnerProductTo.thresholds.length > 0) {   
        
            this.discountDate =  this.partnerProductTo.thresholds[0].fields[String(VOLUME_DISCOUNT_DATE_FIELD.fieldApiName)];
            this.volumeDiscountTypeSelectedValue = this.partnerProductTo.thresholds[0].fields[String(VOLUME_DISCOUNT_TYPE_FIELD.fieldApiName)];
            this.enableSectionAndSetRadionButton(this.sectionDiscountsFields, 'thresholds');
            this.isDiscountDisabled = false;
            for (let i = 1; i <= this.thresholdNumber.length; i++) {   

                let queryTerm = "[data-threshold-id='" + i + "']";
                var inputValues = this.template.querySelectorAll(queryTerm);
                
                if(inputValues.length > 0){
                    for ( let j = 0; j < inputValues.length; j++ ) {
                        console.log('this.partnerProductTo.thresholds[i-1].fields[String(inputValues[j].name)]');
                        console.log(this.partnerProductTo.thresholds[i-1].fields[String(inputValues[j].name)]);
                        inputValues[j].value = this.partnerProductTo.thresholds[i-1].fields[String(inputValues[j].name)];
                    }
                }
            }
        } 
        else {
            this.disableSectionAndSetRadionButton(this.sectionDiscountsFields, 'thresholds');
        }

    }

    disableSectionAndSetRadionButton(sectionWithFields, radioGroupIdentifier) {

        console.log('disableSectionAndSetRadionButton');
        console.log('radioGroupIdentifier: '+radioGroupIdentifier);
        console.log('sectionWithFields: '+JSON.stringify(sectionWithFields));
        console.log('this.isClone: '+this.isClone);
        console.log('this.isEdit: '+this.isEdit);
        console.log(this.requiresRadionButton);

        if(this.requiresRadionButton && (this.isClone || this.isEdit)){
            this.setRadioButton(radioGroupIdentifier, 'No');
        }

        if (!this.requiresRadionButton) {

            this.exclusivitySelectedValue = this.radioExclusivityGroupOptions.exclusivitySelectedValue;
            this.noShowSelectedValue = this.radioExclusivityGroupOptions.noShowSelectedValue;
            this.lateCancelSelectedValue = this.radioExclusivityGroupOptions.lateCancelSelectedValue;
            this.volumeDiscountSelectedValue = this.radioExclusivityGroupOptions.volumeDiscountSelectedValue;
        }

        disableFieldsFromSection(sectionWithFields, true);
    }


    enableSectionAndSetRadionButton(sectionWithFields, radioGroupIdentifier) {
        this.setRadioButton(radioGroupIdentifier, 'Yes');
        enableFieldsFromSection(sectionWithFields);
    }

    setRadioButton(radioGroupIdentifier, option) {
        console.log('## radioGroupIdentifier: '+radioGroupIdentifier);
        console.log('## option: '+option);
        console.log('## this.partnerProductRecordTypes.exclusivityRecordTypeName: '+this.partnerProductRecordTypes.exclusivityRecordTypeName);
        switch (radioGroupIdentifier) {
            case this.partnerProductRecordTypes.exclusivityRecordTypeName:
                this.exclusivitySelectedValue = option;
                break;
            case this.partnerProductRecordTypes.noShowRecordTypeName:
                this.noShowSelectedValue = option;
                break;
            case this.partnerProductRecordTypes.lateCancellationRecordTypeName:
                this.lateCancelSelectedValue = option;
                break;
            case 'thresholds':
                this.volumeDiscountSelectedValue = option;
                break;
        }
    }

    handleDiscountDate(event){
        this.discountDate = event.target.value;
    }

    fireEventWithRadioButtonValue(){
        console.log('fireEventWithRadioButtonValue');
        const selectedRadioEvt= new CustomEvent('selectedradio', { detail:
            {   name : 'Exclusivity',
                exclusivitySelectedValue : this.exclusivitySelectedValue,
                noShowSelectedValue :  this.noShowSelectedValue, 
                lateCancelSelectedValue : this.lateCancelSelectedValue,
                volumeDiscountSelectedValue :  this.volumeDiscountSelectedValue } }
            );

        this.dispatchEvent(selectedRadioEvt);
    }

    enableSpinner() {
        this.showSpinner = true;
    }

    disableSpinner() {
        this.showSpinner = false;
    }

    showToastNotification(title, message, variant) {
        const evt = new ShowToastEvent({ title: title, message: message, variant: variant });
        this.dispatchEvent(evt);
    }

}