export class PartnerProductItemTO {
    currentAccountId;
    currentOpportunityId;
    productDetails = {};
    currentProductId;
    selectedGymActivities = [];
    commercialDetails = [];
    thresholds = [];

    /**
     * @param { create custom object as objectName = { commercialConditionName (key) : value} } newCommercialConditionObject 
     */
    addNewCommercialConditionDetails(newCommercialConditionObject) {

        var index = this.commercialDetails.findIndex(o => o.key == newCommercialConditionObject.key);
        if (index != -1) {
            this.commercialDetails.splice(index, 1, newCommercialConditionObject);
        } else {
            this.commercialDetails.push(newCommercialConditionObject);
        }
    }
    
    removeCommercialCondition( keyName ){
        var index = this.commercialDetails.findIndex(o => o.key == keyName);
        if (index != -1) {
            this.commercialDetails.splice(index, 1);
        }
    }

    getCommercialDetails() {
        return this.commercialDetails;
    }

    getCommercialCondition(keyName) {
        return this.commercialDetails.find(o => o.key == keyName);
    }

}