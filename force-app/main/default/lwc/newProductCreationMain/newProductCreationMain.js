import { LightningElement, track, wire, api } from 'lwc';
import { PartnerProductItemTO } from './PartnerProductItemTO';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getRecordTypeIdsFormPartnerProductFlow from '@salesforce/apex/NewPartnerProductController.getRecordTypeIdsFormPartnerProductFlow';
import getOpportunityData from '@salesforce/apex/NewPartnerProductController.getOpportunityData';
import saveProduct from '@salesforce/apex/NewPartnerProductController.saveProduct';
import getProductData from '@salesforce/apex/NewPartnerProductController.getProductData';
import getRegexConfigs from '@salesforce/apex/NewPartnerProductController.getRegexCustomMetadata';
import { NavigationMixin } from 'lightning/navigation';
import PRODUCT_OBJECT from '@salesforce/schema/Product_Item__c';
import { getRecordNotifyChange } from 'lightning/uiRecordApi';

export default class NewProductCreationMain extends NavigationMixin(LightningElement) {

    // BM
    @api objectApiName;
    @api recordId;

    @api isClone = false;
    @api isEdit = false;

    partnerProductTo = new PartnerProductItemTO();
    requiresValidation = false;
    requiresRadionButtonsForExclusivityPage = true;
    requiresRadioButtonsForPaymentPage = true;

    radioExclusivityGroupOptions = {}
    radioPaymentGroupOptions = {}



    //@api opportunityId;
    @track progressButtonLable = 'Next';
    @track currentStep = 's1';
    @api partnerProductRecordTypes;
    @track productRecordType;
    @track isLoad = false;
    @track showBackButton = false;
    @track isWishlistAccount = false;
    newProdName = false;
    regexConfigs;

    connectedCallback() {
        this.handleRecordTypes();
    }

    async handleRecordTypes() {
        try {
            const result = await getRecordTypeIdsFormPartnerProductFlow();
            const resultRegex = await getRegexConfigs();
            console.log('## result: ' + JSON.stringify(result));
            this.partnerProductRecordTypes = result;
            this.regexConfigs = resultRegex;
            if (this.objectApiName === 'Product_Item__c') {
                this.getProduct();
            } else if (this.objectApiName === 'Opportunity') {
                this.getOpportunity();
            }
        } catch (error) {
            console.log('error', error);
        }
    }

    getOpportunity() {
        getOpportunityData({ recordId: this.recordId })
            .then((data) => {
                this.isWishlistAccount = data.isWishlistPartner;
                this.partnerProductTo.currentAccountId = data.accountId;
                this.partnerProductTo.currentOpportunityId = this.recordId;
                this.isLoad = true;
            })
            .catch((error) => {
                console.log('error ' + JSON.stringify(error));
            });
    }

    getProduct() {
        getProductData({ recordId: this.recordId })
            .then((data) => {
                this.isWishlistAccount = data.isWishlist;
                this.partnerProductTo.currentAccountId = data.currentAccountId;
                this.partnerProductTo.currentOpportunityId = data.currentOpportunityId;
                this.partnerProductTo.currentProductId = data.currentProductId;
                //let productDetailsCopy = this.fieldSetWrapper.fieldWrappers.map(element => ({...element}));
                this.partnerProductTo.productDetails = data.productDetails;
                this.partnerProductTo.selectedGymActivities = data.selectedGymActivities;
                this.partnerProductTo.commercialDetails = data.commercialDetails; // criar cópia e assignar - provavelmente resolve problema do disconnectedCallback
                this.partnerProductTo.thresholds = data.thresholds;
                this.isLoad = true;
                this.isEdit = true;
                console.log(JSON.stringify(this.partnerProductTo));
            })
            .catch((error) => {
                console.log('error ' + JSON.stringify(error));
            });
    }


    handleProgressPage() {
        this.isLoad = false;
        if (this.currentStep == 's1') {
            const isProductDetailsValid = this.template.querySelector('c-new-Product-Creation-Product-Details').handleProductDetailsValidation();
            if (isProductDetailsValid) {
                this.newProdName = true;
                this.currentStep = 's2';
            }

        } else if (this.currentStep == 's2') {
            const isActivitiesValid = this.template.querySelector('c-new-Product-Creation-Gym-Activities').handleActivitiesValidation();
            if (isActivitiesValid > 0) {
                this.currentStep = 's3';
            } else {
                let errorEvent = new ShowToastEvent({
                    message: 'Please select an activity',
                    variant: 'error'
                });
                this.dispatchEvent(errorEvent);
            }

        } else if (this.currentStep == 's3') {
            const isExclusivityValid = this.template.querySelector('c-new-Product-Creation-Exclusivity').handleExclusivityValidation();
            if (isExclusivityValid) {
                this.requiresRadionButtonsForExclusivityPage = false;
                this.insertProduct();
                /*if (this.isWishlistAccount) {
                    this.currentStep = 's4'
                } else {
                    this.insertProduct();
                }*/
            }

        }/* else if (this.currentStep == 's4') {
            const isPaymentValid = this.template.querySelector('c-new-Product-Creation-Payment').handlePaymentValidation();
            if (isPaymentValid) {
                this.requiresRadioButtonsForPaymentPage = false;
                this.insertProduct();
            }
        }*/

        this.handleButtonLabel();
        this.isLoad = true;
    }

    handleButtonLabel() {

        if ((this.currentStep == 's3'/* && !this.isWishlistAccount) || (this.currentStep == 's4' && this.isWishlistAccount*/)) {
            this.progressButtonLable = 'Save';
        } else {
            this.progressButtonLable = 'Next';
        }

        this.showBackButton = this.currentStep != 's1' ? true : false;
    }

    handleBackPage() {
        this.load = false;
        switch (this.currentStep) {
            case 's1':
                this.currentStep = 's1';
                break;
            case 's2':
                this.currentStep = 's1';
                break;
            case 's3':
                this.currentStep = 's2';
                this.requiresRadionButtonsForExclusivityPage = false;
                break;
            /*case 's4':
                this.currentStep = 's3';
                this.requiresRadioButtonsForPaymentPage = false;
                break;*/
            default:
                console.log('end of page');
        }

        this.handleButtonLabel();

        this.load = true;

    }

    handleCancel(event) {
        const closeEvt = new CustomEvent('closemodal');
        this.dispatchEvent(closeEvt);
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: this.recordId,
                objectApiName: 'Product_Item__c',
                actionName: 'view',
            }
        })
    }

    closeAction() {
        const closeEvt = new CustomEvent('closemodal');
        this.dispatchEvent(closeEvt);
        // if product or opp
        getRecordNotifyChange([{ recordId: this.recordId }]);
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: this.recordId,
                objectApiName: 'Product_Item__c',
                actionName: 'view',
            }
        })
    }

    async insertProduct() {
        this.isLoad = false;
        this.showSpinner();
        if (this.isClone) {
            this.partnerProductTo.currentProductId = '';
        }
        try {
            const record = await saveProduct({ partnerProductRecord: JSON.stringify(this.partnerProductTo) });
            let successtEvnt = new ShowToastEvent({
                title: 'Success',
                message: 'Product created',
                variant: 'success'
            });
            this.hideSpinner();
            this.isLoad = true;
            this.dispatchEvent(successtEvnt);
            this.navigateToNewProductPage(record);
        } catch (error) {
            this.isLoad = true;
            this.errorHandler(error);
        }

    }

    errorHandler(error) {
        console.log('ERROR');
        console.log(JSON.stringify(error));
        var errorMessage = 'Unknown error';
        if (error && error.body && Array.isArray(error.body)) {
            errorMessage = error.body.map(e => e.message).join(', ');
        } else if ((error && error.body && error.body.output && error.body.output.errors && Array.isArray(error.body.output.errors) && error.body.output.errors.length > 0)) {
            errorMessage = error.body.output.errors.map(e => e.message).join(', ');
        } else if ((error && error.body && error.body.output && error.body.output.fieldErrors)) {
            errorMessage = (JSON.stringify(error.body.output.fieldErrors).split('message')[1]).split(':')[2];
        }
        else if (error && error.body && error.body.message) {
            errorMessage = error.body.message;
        }

        let errorEvent = new ShowToastEvent({
            title: 'Error!',
            message: errorMessage,
            variant: 'error'
        });
        this.dispatchEvent(errorEvent);
    }

    saveRadioOptions(event) {

        console.log('saveRadioOptions');
        console.log(event.detail.name);
        if (event.detail.name == 'Exclusivity') {
            this.radioExclusivityGroupOptions['exclusivitySelectedValue'] = event.detail.exclusivitySelectedValue;
            this.radioExclusivityGroupOptions['noShowSelectedValue'] = event.detail.noShowSelectedValue;
            this.radioExclusivityGroupOptions['lateCancelSelectedValue'] = event.detail.lateCancelSelectedValue;
            this.radioExclusivityGroupOptions['volumeDiscountSelectedValue'] = event.detail.volumeDiscountSelectedValue;
        } /*else if (event.detail.name == 'Payment') {
            this.radioPaymentGroupOptions['advPaymentSelectedValue'] = event.detail.advPaymentSelectedValue;
            this.radioPaymentGroupOptions['minGuaranteedSelectedValue'] = event.detail.minGuaranteedSelectedValue;
            this.radioPaymentGroupOptions['cannibalizationSelectedValue'] = event.detail.cannibalizationSelectedValue;
        }*/

        console.log(JSON.stringify(this.radioExclusivityGroupOptions));
        /*console.log(JSON.stringify(this.radioPaymentGroupOptions));*/
    }


    navigateToNewProductPage(newRecordId) {
        getRecordNotifyChange([{ recordId: this.recordId }]);
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: newRecordId,
                objectApiName: 'Product_Item__c',
                actionName: 'view'
            }
        });
    }
    showSpinner() { this.spinner = true }
    hideSpinner() { this.spinner = false }

    get containerClass() {
        return this.isClone || this.isEdit ? 'slds-modal__container c-modal' : 'slds-modal__container';
    }

    get clone() {
        return this.isClone;
    }

    get exclusivityStep() {
        return true;
    }

    get isProductDetails() {
        return this.currentStep == 's1' ? true : false;
    }

    get isOverflow() {
        return this.currentStep == 's1' ? 'slds-modal__content  slds-p-around_medium customOverflow' : 'slds-modal__content  slds-p-around_medium';
    }

    get isGymActivities() {
        return this.currentStep == 's2' ? true : false;
    }

    get isExclusivity() {
        return this.currentStep == 's3' ? true : false;
    }

    /*get isPayment() {
        return this.currentStep == 's4' ? true : false;
    }*/
}