import { LightningElement, api } from 'lwc';

export default class ApprovalBadges extends LightningElement {
    @api itemsNeedApproval;
    @api quoteRecord;
    @api isDealDeskitems;
    @api listItemsNeedApproval;

    connectedCallback() {
        if( this.isDealDeskitems == null ){ return; }

        this.listItemsNeedApproval = this.isDealDeskitems ? this.getDealDeskBadges() :
            this.getCommercialApprovalBadges();
    }

    getDealDeskBadges(){
        let lstDealDeskConditions = this.itemsNeedApproval ? JSON.parse( this.itemsNeedApproval ) : [];

        if( !lstDealDeskConditions.length ){
            return [{
                conditionCode: 'NotRequiredCondition', 
                approvalCondition: 'Not required',
                class: 'slds-theme_success wrapped-content' }];
        }

        lstDealDeskConditions.forEach(condition => {
            condition.class = ( condition.isApproved ? 'slds-theme_success wrapped-content' : 'slds-theme_error wrapped-content' );
        });

        return lstDealDeskConditions;
    }

    getCommercialApprovalBadges(){
        let listItems = [];
        let item;

        if( this.quoteRecord.Discount_Approval_Level__c && this.quoteRecord.Discount_Approval_Level__c > 0 ){
            item = {};
            item.label = 'Discount out of range';
            item.class = 'slds-theme_' + (this.quoteRecord.Total_Discount_approved__c ? 'success' : 'error' );
            listItems.push( item );
        } 

        if( this.quoteRecord.Waiver_Approval_Level__c && this.quoteRecord.Waiver_Approval_Level__c > 0 ){
            item = {};
            item.label = 'Waiver too long';
            item.class = 'slds-theme_' + (this.quoteRecord.Waiver_approved__c ? 'success' : 'error' );
            listItems.push( item );
        }

        if( listItems.length == 0 ){
            item = {};
            item.label = 'Not required';
            item.class = 'slds-theme_success';
            listItems.push( item );
        }

        return listItems;
    }
}