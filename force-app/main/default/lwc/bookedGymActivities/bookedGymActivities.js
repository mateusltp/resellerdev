import getProdGymActivities from '@salesforce/apex/BookedGymActivitiesController.getProdGymActivities';
import createGymActivitiesToBeBooked from '@salesforce/apex/BookedGymActivitiesController.createGymActivitiesToBeBooked';
import getCurrentGymActivitiesBooked from '@salesforce/apex/BookedGymActivitiesController.getCurrentGymActivitiesBooked';
import { LightningElement, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

const columns = [
    { label: 'Gym Activity Name', fieldName: 'Name' }
];

export default class BookedGymActivities extends LightningElement {
    @api recordId
    
    columns = columns;
    data;
    selectedRows = [];
    spinnerMessage = '';
    spinner = false;

    connectedCallback() {
        this.getGymActivityData();
        this.getCurrentBookedActivities();
    }

    getGymActivityData() {
        getProdGymActivities({ prodId: this.recordId})
            .then((result) => {
                this.data = result;
            })
            .catch((error) => {
                this.showToastNotification('Error', error.body.message, 'error');
            });
    }

    getCurrentBookedActivities() {
        getCurrentGymActivitiesBooked({ prodId: this.recordId})
            .then((result) => {
                this.setSelectedRows(result);
            })
            .catch((error) => {
                this.showToastNotification('Error', error.body.message, 'error');
            });
    }

    handleRowSelection(event) {
        this.showSpinner('Updating Gym Activities');
        let currentSelectedRows = this.getCurrentSelectedRows();
        createGymActivitiesToBeBooked({ prodId: this.recordId, selectedGymActivities: currentSelectedRows })
            .then((result) => {
                this.showToastNotification('Success', 'Gym Activities updated!', 'success');
                this.hideSpinner();
            })
            .catch((error) => {
                this.showToastNotification('Error', error.body.message, 'error');
                this.hideSpinner();
            });
    }

    getCurrentSelectedRows() {
        return this.template.querySelector('lightning-datatable').getSelectedRows();
    }

    showToastNotification(title, message, variant) {
        const evt = new ShowToastEvent({title: title, message: message, variant: variant});
        this.dispatchEvent(evt);
    }

    setSelectedRows(gymActivityIds) {
        this.selectedRows = gymActivityIds;
    }

    showSpinner(message) {
        this.spinnerMessage = message;
        this.spinner = true;
    }

    hideSpinner() {
        this.spinner = false;
    }
}