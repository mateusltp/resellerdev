import { LightningElement, api, wire, track } from 'lwc';

import { FlowAttributeChangeEvent, FlowNavigationNextEvent} from 'lightning/flowSupport';

import fetchDefaultValue from '@salesforce/apex/ResellerEJContactLookupController.fetchSingleRecordSelected';
import apexContactSearch from '@salesforce/apex/ResellerEJContactLookupController.searchContact';

export default class ResellerEJSingleLookup extends LightningElement {

    //-- configurable parameters
    @api recordId;
    @api defaultRecordId;
    @api accountId;
    @api lookupLabel = '';
    @api lookupPlaceholder = '';
    @api isRequired = false;

    @api searchField = 'Name';

    @api selectedRecordId;
    @api lstSelectedRecordIds = [];

    defaultResult = {};
    listOfDefaultResults = [];

    @wire(fetchDefaultValue, {
        defaultRecordId: '$defaultRecordId'
    })
    handleHasDefaultValue({ data }) {
        if(data) {
            this._searchedPreviousRecordMap = data.reduce((res, item) => {
                res[item.Id] = item;
                return res;
            }, {});

            this.listOfDefaultResults = data.map(item => {
                return {
                    id: item.Id,
                    icon : 'standard:contact',
                    sObjectType : 'Contact',
                    title: this.getFieldValue(this.searchField, item),
                };
            });

            this._previousRecordItemMap = this.listOfDefaultResults.reduce((res, item) => {
                res[item.id] = item;
                return res;
            }, {});

            this.defaultResult = this.listOfDefaultResults.at(0);
            // this.hasDefaultRecords = true;
            this.selectedRecordId = this.listOfDefaultResults.at(0).Id;
            this.lstSelectedRecordIds = this.lstSelectedRecordIds.concat(this.listOfDefaultResults.at(0).Id);
        }
    }

    handleContactSearch(event) {
        const lookupElement = event.target;
        apexContactSearch({ 
                searchTerm: event.detail.searchTerm,
                selectedIds: event.detail.selectedIds,
                accountId: this.accountId
            })
            .then(results => {
                lookupElement.setSearchResults(results);
            })
            .catch(error => {
                reduceErrors(error);
            });
    }

    handleSelectedRecords(event) {
        //-- the custom lookup already updates the current selection
        this.lstSelectedRecordIds = event.detail;

        this.selectedRecordId = event.detail[0];
        
        const attributeChangeEvent = new FlowAttributeChangeEvent(
            'selectedRecordId',
            event.detail[0]
        );
        this.dispatchEvent(attributeChangeEvent);
    }

    getFieldValue(fieldName, obj) {
        if (fieldName) {
            return fieldName.split('.').reduce((result, item) => {
                return result[item];
            }, obj);
        }
        return '';
    }
}