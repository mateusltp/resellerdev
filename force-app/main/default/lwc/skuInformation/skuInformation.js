/**
 * @description       : 
 * @author            : tiago.ribeiro@gympass.com
 * @group             : 
 * @last modified on  : 07-18-2022
 * @last modified by  : tiago.ribeiro@gympass.com
**/
import { LightningElement, track, api, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { getRecord } from 'lightning/uiRecordApi';
import { refreshApex } from '@salesforce/apex';
import typeLabel from '@salesforce/label/c.profServicesTypes';

const FIELDS = [
    'QuoteLineItem.Product2Id',
    'QuoteLineItem.Product_Name__c',
    'QuoteLineItem.Quantity',
    'QuoteLineItem.List_Price__c',
    'QuoteLineItem.Total_List_Price__c',
    'QuoteLineItem.UnitPrice',
    'QuoteLineItem.Subtotal',
    'QuoteLineItem.Product_Fee_type__c',
    'QuoteLineItem.Discount',
    'QuoteLineItem.CurrencyIsoCode',
    'QuoteLineItem.EM__c',
    'QuoteLineItem.Copay__c',
    'QuoteLineItem.PGP__c',
    'QuoteLineItem.Product2.Professional_services_types__c',
    'QuoteLineItem.Product2.Professional_Services_Type_Selection__c'
];

export default class SkuInformation extends LightningElement {

    @track objectApiName = 'QuoteLineItem'

    @track sectionTitle = 'Product Information';

    @api recordId;

    quantity;

    @track product;
    
    getRecordResponse;
    typeOptions = [];
    displayProfServicesOptions = false;
    displayCheckbox = false;
    displayRadio = false;
    profServicesTypeLabel = typeLabel;

    @wire(getRecord, { recordId: '$recordId', fields: FIELDS })
    wiredRecord(response) {
        this.getRecordResponse = response;
        const { data, error } = response;
        if (error) {
            let message = 'Unknown error';
            if (Array.isArray(error.body)) {
                message = error.body.map(e => e.message).join(', ');
            } else if (typeof error.body.message === 'string') {
                message = error.body.message;
            }
            console.error(message);
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error loading product',
                    message: message,
                    variant: 'error',
                }),
            );
        } else if (data) {
            this.product = {};
            this.product.Product_Name__c = data.fields.Product_Name__c.value;
            this.product.Product2Id = data.fields.Product2Id.value;
            this.product.Quantity = data.fields.Quantity.value;
            this.product.List_Price__c = data.fields.List_Price__c.value;
            this.product.Total_List_Price__c = data.fields.Total_List_Price__c.value;
            this.product.UnitPrice = data.fields.UnitPrice.value;
            this.product.Subtotal = data.fields.Subtotal.value;
            this.product.Product_Fee_type__c = data.fields.Product_Fee_type__c.value;
            this.product.Discount = data.fields.Discount.value / 100;
            this.product.CurrencyIsoCode = data.fields.CurrencyIsoCode.value;
            this.product.EM__c = data.fields.EM__c.value;
            this.product.Copay__c = data.fields.Copay__c.value;
            this.product.PGP__c = data.fields.PGP__c.value;
            let stringTypes = data.fields.Product2.value.fields.Professional_services_types__c.value;
            let typeSelection = data.fields.Product2.value.fields.Professional_Services_Type_Selection__c.value;
            if(stringTypes && typeSelection && (typeSelection == 'Checkbox' || typeSelection == 'Radio')){
                this.displayProfServicesOptions = true;
                this.handleProfessionalServicesTypes(stringTypes);
                this.displayType(typeSelection);
            }
        }
    }

    handleProfessionalServicesTypes(stringTypes){
        if(stringTypes){
            let psTypes = stringTypes.split(";");
            this.displayCheckbox = this.displayAsCheckboxOptions(psTypes);
            this.filterAndAutoFillTypes(psTypes);
        }
    }

    displayType(typeSelection){
        this.displayCheckbox = false;
        this.displayRadio = false;
        switch (typeSelection) {
            case 'Checkbox':
                this.displayCheckbox = true;
                break;
            case 'Radio':
                this.displayRadio = true;
            default:
                break;
        }
    }

    displayAsCheckboxOptions(psTypes){
        let displayCheckbox = true;
        if(psTypes.includes('Select only one')){
            displayCheckbox = false;
        }

        return displayCheckbox;
    }

    filterAndAutoFillTypes(psTypes){
        let types = psTypes.filter(type => !type.includes('Select'));
        types.forEach(type => {
            let optionType = {
                label: type,
                value: type + '__c',
                checked: this.product[type + '__c']
            }
            
            this.typeOptions.push(optionType);
        });
    }

    get productLink() {
        if(this.product)
            return '/' + this.product.Product2Id;
        else
            return '';
    }

    @api
    getProduct() {
        const product = {
            Id: this.recordId,
            UnitPrice: this.product.UnitPrice,
            Discount: this.product.Discount * 100,
            EM__c: this.product.EM__c,
            Copay__c: this.product.Copay__c,
            PGP__c: this.product.PGP__c
        };
        return product;
    }

    @api
    reportValidity() {
        let salesPriceField = this.template.querySelector('[data-label="Sales Price"]');
        let profServicesValidity = this.checkProfServicesValidity();
        return salesPriceField.reportValidity() && profServicesValidity;
    }

    @api
    refreshProduct(){
        refreshApex(this.getRecordResponse);
    }

    checkProfServicesValidity(){
        let isValid = false;
        if(this.typeOptions && this.typeOptions.length > 0){
            for(const type of this.typeOptions){
                if(this.product[type.value]){
                    isValid = true;
                    break;
                }
            }
        }else{
            isValid = true;
        }

        return isValid;
    }

    updateDiscount(salesPrice) {
        let discount = (1 - (salesPrice / this.product.List_Price__c));
        if(discount >= 0 && discount !== Infinity) {
            this.product.Discount = discount;
        }
        else {
            this.product.Discount = 0.0;
        }
    }

    handleSalesPriceChange(event) {
        const salesPrice = event.detail.value;
        this.product.UnitPrice = salesPrice;
        this.product.Subtotal = salesPrice * this.product.Quantity;
        this.updateDiscount(salesPrice);
    }

    onCheckboxChangeTypeValue(event){
        const checked = event.target.checked;
        const value = event.target.value;
        this.product[value] = checked;
    }

    onRadioChangeTypeValue(event){
        const value = event.target.value;
        this.product[value] = true;
        this.updateValueOnRemainingTypes(value);
    }

    updateValueOnRemainingTypes(value){
        this.typeOptions.forEach(type => {
            if(type.value != value){
                this.product[type.value] = false;
            }
        });
    }
}