import { LightningElement, track, wire, api } from 'lwc';
import { getRecordCreateDefaults } from 'lightning/uiRecordApi';
import { getNewLayoutTemplate } from 'c/customLayoutUtils';
import CONTACT_OBJECT from '@salesforce/schema/Contact';
import PHONE_FIELD from '@salesforce/schema/Contact.Phone';
import CONTACT_ACCOUNT_ID_FIELD from '@salesforce/schema/Contact.AccountId';


export default class NetworkBuilderCreateNewContact extends LightningElement {
    @api partnerFlowId; // CURRENTLY USING GYMS_PARTNER RECORD TYPE - IT-2349
    @api accountId;
    @track showSpinner = false;
    @track sectionWithFields = [];
    sectionsToExclude = [];
    fieldsToExclude = [CONTACT_ACCOUNT_ID_FIELD.fieldApiName]; 

    @wire(getRecordCreateDefaults, { objectApiName: CONTACT_OBJECT.objectApiName, recordTypeId: '$partnerFlowId' })
    contactRecord({ error, data }) {
        if (data ) {
            const template = getNewLayoutTemplate(data, this.sectionsToExclude,this.fieldsToExclude);
            template.sectionWithFields.forEach(section => {
                section.value.forEach( field => {
                    if(field.apiName == PHONE_FIELD.fieldApiName){
                        field['isPhone'] = true;
                    }
                })
            })
            this.sectionWithFields = template.sectionWithFields;

        } else if( error ){
            console.log('error ' + JSON.stringify(error));
        }
    };

    handleContactCreation() {
        const inputFields = this.template.querySelectorAll('.c-input-contact');
        const isInputsCorrect = [...inputFields]
            .reduce((validSoFar, inputField) => {             
                return validSoFar && inputField.reportValidity();
            }, true);
        if (isInputsCorrect) {
            this.enableSpinner();
            let contactRecordInput = this.populateContactRecordInput(inputFields);
            if(this.accountId !== undefined && this.accountId !== null) {
                contactRecordInput.fields.AccountId = this.accountId;
            }
            this.sendContactRecordInput(contactRecordInput);
        }
    }

    populateContactRecordInput(inputFields) {
        let contactRecordInput = this.recordInputForCreate(CONTACT_OBJECT.objectApiName);
        inputFields.forEach(field => {
            contactRecordInput.fields[String(field.name)] = field.value;
        });
        contactRecordInput.fields['RecordTypeId'] = this.partnerFlowId;
        return contactRecordInput;
    }

    recordInputForCreate(objApiName) {
        let recordInput = {
            "apiName": objApiName,
            "fields": {
            }
        }
        return recordInput;
    }

    sendContactRecordInput(contactRecordInput) {
        const selectedEvent = new CustomEvent("savecontact", {
            detail: contactRecordInput
        });
        this.dispatchEvent(selectedEvent);
        this.disableSpinner();
        this.closeModal();
    }

    enableSpinner() {
        this.showSpinner = true;
    }

    disableSpinner() {
        this.showSpinner = false;
    }

    closeModal() {
        const myEvent = new CustomEvent('close');
        this.dispatchEvent(myEvent);
    }
}