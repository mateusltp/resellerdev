import getProdAssignments from '@salesforce/apex/ProductAssignmentListController.getProdAssignments';
import updateProdAssignments from '@salesforce/apex/ProductAssignmentListController.updateProdAssignments';
import getAccountData from '@salesforce/apex/ProductAssignmentListController.getAccountData';
import deleteProdAssignment from '@salesforce/apex/ProductAssignmentListController.deleteProdAssignment';
import createProdAssignmentsForChildProd from '@salesforce/apex/ProductAssignmentListController.createProdAssignmentsForChildProd';
import getHelperData from '@salesforce/apex/ProductAssignmentListController.getHelperData';
import { LightningElement,track, api} from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

const actions = [
    { label: 'Remove', name: 'delete' },
];

const columns = [
    { label: 'Account name'     ,fieldName: 'accountName'   ,type: 'text'     },
    { label: 'Market price'     ,fieldName: 'marketPrice'   ,type: 'currency', editable: 'true' },
    { label: 'CAP value'        ,fieldName: 'capValue'      ,type: 'currency' },
    { label: 'Net tr. price'    ,fieldName: 'netPrice'      ,type: 'currency' },
    { label: 'Late cancel.'     ,fieldName: 'lateCancel'    ,type: 'currency' },
    { label: 'No show fee'      ,fieldName: 'noShowFee'     ,type: 'currency' },
    {
        type: 'action',
        typeAttributes: { rowActions: actions },
    },
];

export default class ProductAssignmentList extends LightningElement {
    @api recordId;
    
    data            = [];
    dataBackUp      = [];
    draftValues     = [];
    accountIdLst    = [];
    deletedAccIdLst = [];
    
    columns             = columns;
    prodAccountData     = null;
    showProdAssigModal  = false;
    @track spinner             = false;
    @track showAddAccountButton = false;
    helperData          = null;

    connectedCallback() {
        this.showSpinner();
        //this.getProdAssignData();
        this.getProdAccountData();
        this.getHelperData();     
    }

    showAddAccountModal() {
        this.showProdAssigModal = true;
    }

    closeAddAccountModal() {
        this.showProdAssigModal = false;
    }

    showSpinner() {
        this.spinner = true;
    }

    hideSpinner() {
        this.spinner = false;
    }

    showToastNotification(title, message, variant) {
        const evt = new ShowToastEvent({title: title, message: message, variant: variant});
        this.dispatchEvent(evt);
    }

    handleSave( event ) {
        const updatedFields = event.detail.draftValues;

        this.showSpinner();

        updatedFields.forEach(element => {
            element.marketPrice = (element.marketPrice == "" ? 0 : element.marketPrice);
        });
        
        updateProdAssignments( { data: updatedFields } )
            .then( result => {
                this.draftValues = [];
                this.hideSpinner();
                this.getProdAssignData();
                this.getHelperData();
                this.showToastNotification('Success', 'Records updated', 'success');
            }).catch( error => {
                this.hideSpinner();
                if (error.status == 500) {
                    this.showToastNotification('Error updating records', error.body.message, 'error');
                }
        });
    }

    getProdAssignData() {
        console.log('getProdAssignData');
   
        getProdAssignments({ recordId: this.recordId}).then((result) => {
                console.log('Successssss');
                console.log(result);
                this.data = result;
                this.dataBackUp = this.data;
                if(Array.isArray(this.data)){
                    if(this.data[0] != null){
                        this.showAddAccountButton = this.data[0].partnerLevel != 'Single Partner' ? true :  false;
                    } else {
                        this.showAddAccountButton  = true;
                    }                   
                }
                this.hideSpinner();
            })
            .catch((error) => {
                this.showToastNotification('Error', error.body.message, 'error');
                this.hideSpinner();
            });
    }

    createProdAssign() {
        console.log('createProdAssign');
        createProdAssignmentsForChildProd({ recordId: this.recordId })
            .then((result) => {
                console.log('Successssss createProdAssign -> getProdAssignData ');
                console.log(result);
                this.getProdAssignData();
                this.hideSpinner();
                
            })
            .catch((error) => {
                this.showToastNotification('Error', error.body.message, 'error');
                this.hideSpinner();
            });
    }

    getProdAccountData() {
        getAccountData({ recordId: this.recordId})
            .then((result) => {
                console.log(result);
                this.prodAccountData = result;              
            })
            .catch((error) => {
                this.showToastNotification('Error', error.body.message, 'error');
            });
    }

    getHelperData() {
        console.log('::Entry helperData')
        console.log(':: recordId ' + this.recordId);
        getHelperData({ recordId: this.recordId})
        .then((result) => {
            console.log('::getHelperData ok');
            console.log(result);
            this.helperData = result;
            console.log(this.helperData);
            this.createProdAssign();
        })
        .catch((error) => {
            console.log('::getHelperData error');
            this.showToastNotification('Error', error.body.message, 'error');
        });
    }

    handleCreatedPas() {
        this.closeAddAccountModal();
        this.showSpinner();
        this.getProdAssignData();
        this.hideSpinner();
    }

    refreshComponent(){
        console.log('refresh data');
        eval("$A.get('e.force:refreshView').fire();");
    }

    handleSearchAction(evt) {
        var filteredRows;
        let rows = [... this.data],
            term = evt.target.value,
            results = rows, regex;

        if (term.length < 1) {
            this.data = this.dataBackUp;
            return;
        }

        try {
            regex = new RegExp(term, "i");
            results = rows.filter(row => regex.test(row.accountName));
            filteredRows = results;
            this.data = (filteredRows.length > 0) ? filteredRows : this.dataBackUp;
        } catch (e) {

        }
    }

    handleRowAction(event) {
        console.log('handleRowAction');
        const actionName = event.detail.action.name;
        const row = event.detail.row;
        let prodAsignmentId = row.id;
        let accountId = row.accountId;
        switch (actionName) {
            case 'delete':
                if (row.accountId != this.prodAccountData.Id) {
                    this.delProdAssignment(prodAsignmentId);
                } else {
                    this.showToastNotification('Warning', 'You are not allowed to delete this record', 'warning');
                }
                break;
        }
    }

    delProdAssignment(prodAsignmentId) {
        deleteProdAssignment({ prodAssignmentId: prodAsignmentId})
            .then((result) => {
                console.log(result);
                this.showToastNotification('Success', 'Record deleted', 'success');
                this.getProdAssignData();
                this.getHelperData();
            })
            .catch((error) => {
                this.showToastNotification('Error', error.body.message, 'error');
            });
    }
}