import { LightningElement, track, api, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import updateProductAndWaivers from '@salesforce/apex/SmbSkuEditController.updateProductAndWaivers';

export default class SmbSkuEdit extends LightningElement {

    @api recordId;

    @api record;

    @api showModal = false;

    @track isLoading = false;

    hasRendered = false;

    get modalClass() {
		return `slds-modal ${this.showModal ? "slds-fade-in-open" : ""}`;
	}

    get modalBackdropClass() {
		return `slds-backdrop ${this.showModal ? "slds-backdrop_open" : ""}`;
	}

    renderedCallback() {
        if(!this.hasRendered) {
            this.hasRendered = true;
            // this timeout is for visual purposes only - this way the fade-in effect can be seen
            setTimeout(() => {
                this.showModal = true;
                }, "50");
        }
    }

    close() {
        this.showModal = false;
        this.fireCloseEvent();
    }

    handleCancel() {
        this.close();
    }

    skuInformationIsValid() {
        return this.template.querySelector('c-smb-sku-information').reportValidity();
    }

    skuWaiversIsValid() {
        return this.template.querySelector('c-smb-sku-waivers').reportValidity();
    }

    formIsValid() {
        if(this.record && this.record.Allow_Waivers__c) {
            return this.skuInformationIsValid() && this.skuWaiversIsValid();     
        }
        else {
            return this.skuInformationIsValid();
        }
    }

    handleSave() {

        this.isLoading = true;

        if(this.formIsValid()) {
            let product = this.template.querySelector('c-smb-sku-information').getProduct();
            let waiversToUpsert = [];
            let waiversToDelete = [];
            if(this.record && this.record.Allow_Waivers__c) {
                waiversToUpsert = this.template.querySelector('c-smb-sku-waivers').getWaiversToUpsert();
                waiversToDelete = this.template.querySelector('c-smb-sku-waivers').getWaiversToDelete();
            }
    
            updateProductAndWaivers({ product: product, waiversToUpsert: waiversToUpsert, waiversToDelete: waiversToDelete})
                .then(() => {
                    this.isLoading = false;

                    product.Waivers__r = waiversToUpsert;

                    this.fireUpdateRecordEvent(product);

                    this.showToast('', 'The product was saved', 'Success');
    
                    this.close();
                })
                .catch(error => { 
                    this.isLoading = false;
    
                    console.error(error);

                    const evt = new ShowToastEvent({
                        message: error.body.message,
                        variant: 'Error',
                    });
                    this.dispatchEvent(evt);
                });
        }
        else {
            this.isLoading = false;            
            this.showToast('', 'Please complete all mandatory fields', 'Error');
        }

    }

    handleClose() {
        this.close();
    }

    showToast(title, message, variant) {
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant,
        });
        this.dispatchEvent(evt);
    }

    fireUpdateRecordEvent(product) {
        const evt = new CustomEvent('updaterecord', {detail: product});
        this.dispatchEvent(evt);
    }

    fireCloseEvent() {
        const evt = new CustomEvent('close');
        this.dispatchEvent(evt);
    }

}