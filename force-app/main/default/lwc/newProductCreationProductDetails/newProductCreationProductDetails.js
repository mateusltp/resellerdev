import { LightningElement, track, wire, api } from 'lwc';
import { getRecordCreateDefaults } from 'lightning/uiRecordApi';
import { getNewLayoutTemplate, recordInputForCreate, handleValidation} from 'c/customLayoutUtils';
import PRODUCT_OBJECT from '@salesforce/schema/Product_Item__c';
import PACKAGE_FIELD from '@salesforce/schema/Product_Item__c.Package_Type__c';
import NAME_FIELD from '@salesforce/schema/Product_Item__c.Name';
import COMMERCIAL_OBJECT from '@salesforce/schema/Commercial_Condition__c';
import VISITS_CAP_FIELD from '@salesforce/schema/Commercial_Condition__c.Visits_to_CAP__c';
import CAP_DISCOUNT_FIELD from '@salesforce/schema/Commercial_Condition__c.CAP_Discount__c';


export default class NewProductCreationProductDetails extends LightningElement {

    @api partnerProductRecordTypes;
    @api partnerProductTo = {};
    @api productId;
    @api isClone;
    @api newProdName;
    @api objectApiName;
    @api regexConfigs;
    @track sectionWithFields = [];
    @track showSpinner = true;
    @track capDiscountValue = '';
    @track visitsToCapValue = '';
    sectionsToExclude = [];
    fieldsToExclude = [];

    get visitsToCapMessage() {
        return 'Please specify a value between 1 and 31.';
    }

    get capDiscountMessage() {
        return 'Please specify a value between 0 and 100.';
    }

    get visitsToCap(){
        return VISITS_CAP_FIELD.fieldApiName;
    }

    get capDiscount(){
        return CAP_DISCOUNT_FIELD.fieldApiName;
    }

    @wire(getRecordCreateDefaults, { objectApiName: PRODUCT_OBJECT.objectApiName, recordTypeId: '$partnerProductRecordTypes.productFlowRecordTypeId' })
    productTemplate({ error, data }) {
        if (data) {
            const template = getNewLayoutTemplate(data, this.sectionsToExclude, this.fieldsToExclude);
            this.sectionWithFields = template.sectionWithFields;
            if (this.partnerProductTo != null && this.partnerProductTo != undefined) {             
                this.loadFieldsValueFromPartnerProductTo();
            }
            this.disableSpinner();
        } else if (error) {
            console.log('error ' + JSON.stringify(error));
        }
    };

    loadFieldsValueFromPartnerProductTo() {
       
        this.sectionWithFields.forEach(section => {
            section.value.forEach(field => {
                if(field.apiName == PACKAGE_FIELD.fieldApiName){
                    field['showCapFields'] = true;
                }
                if(this.partnerProductTo.productDetails != undefined && this.partnerProductTo.productDetails.fields != undefined){
                   field['value'] = this.partnerProductTo.productDetails.fields[String(field.apiName)];
                }
                if(this.partnerProductTo.getCommercialDetails() != undefined && this.partnerProductTo.getCommercialDetails().length > 0 ){                   
                    let cap = this.partnerProductRecordTypes.capRecordTypeName;         
                    if(this.partnerProductTo.getCommercialCondition(cap) != undefined){
                        this.capDiscountValue = this.partnerProductTo.getCommercialCondition(cap).value.fields.CAP_Discount__c;
                        this.visitsToCapValue = this.partnerProductTo.getCommercialCondition(cap).value.fields.Visits_to_CAP__c;
                    } 
                }
                if(this.isClone && field.apiName == NAME_FIELD.fieldApiName && !this.newProdName){
                    field['value'] = null;
                }
                // verify if field needs validation
                let regexConfig = this.regexConfigs.find(config => config.apiName === field.apiName);
                if (regexConfig) {
                    field['requiresValidation'] = true;
                } else {
                    field['requiresValidation'] = false;
                }
                console.log('field: ');
                console.log(JSON.parse(JSON.stringify(field)));
            })
        })
    }

    disconnectedCallback() {
        this.handleProductDetailsCreation();
    }


    handleProductDetailsCreation() {      
        const inputProdFields = this.template.querySelectorAll('.c-input-product');
        const inputCommFields = this.template.querySelectorAll('.c-input-commercial');
        let productDetailsRecordInput = this.populateRecordInput(inputProdFields, PRODUCT_OBJECT.objectApiName);
        let capDetailsRecordInput = this.populateRecordInput(inputCommFields, COMMERCIAL_OBJECT.objectApiName);
        let prod = {
            prodDetails: productDetailsRecordInput,
            cap: capDetailsRecordInput
        }; 
        this.populateProductTO( prod );
    }

    populateRecordInput( inputFields, objectApiName ) {
        let recordInput = recordInputForCreate( objectApiName );
        inputFields.forEach(field => {
            recordInput.fields[String(field.name)] = field.value;
        });
        recordInput.fields['RecordTypeId'] = objectApiName == PRODUCT_OBJECT.objectApiName ? this.partnerProductRecordTypes.productFlowRecordTypeId :  this.partnerProductRecordTypes.capRecordTypeId;
        return recordInput;
    } 

    populateProductTO( prod ) {
        this.partnerProductTo.productDetails = prod.prodDetails;
        var cap = this.partnerProductRecordTypes.capRecordTypeName;
        const aCap = { key: cap, value: prod.cap }
        this.partnerProductTo.addNewCommercialConditionDetails( aCap );

    }

    disableSpinner() {
        this.showSpinner = false;
    }

    handleInputChange(event) {
        /*let configs = getConfigs();
        let configs = JSON.parse(JSON.stringify(this.regexConfigs));*/

        let fieldValue = event.target.value;
        let dataApiName = event.currentTarget.dataset.apiname;
        let dataIsRequired = event.currentTarget.dataset.isrequired;
        let regexConfig = this.regexConfigs.find(config => config.apiName === dataApiName);
        if (!regexConfig) return;
        console.log(JSON.parse(JSON.stringify(regexConfig)));
        let regex = new RegExp(regexConfig.regex, 'gm');
        let result = String(fieldValue).match(regex);
        let queryTerm = "lightning-input[data-apiname='"+dataApiName+"']";
        let inputCmp = this.template.querySelector(queryTerm);
        if (result || (!fieldValue && dataIsRequired.toLowerCase() === 'false')) {
            inputCmp.setCustomValidity("");
        } else {
            inputCmp.setCustomValidity(""+regexConfig.message+"");
        }
        inputCmp.reportValidity();
    }

    @api
    handleProductDetailsValidation() {
        const inputProductFields = this.template.querySelectorAll('.c-input-product');
        const inputCommercialFields = this.template.querySelectorAll('.c-input-commercial');
        var inputFields = [];
        inputFields.push(... inputProductFields);
        inputFields.push(... inputCommercialFields);
        const isCorrectInput = handleValidation(inputFields);   
        return isCorrectInput;
    }
}