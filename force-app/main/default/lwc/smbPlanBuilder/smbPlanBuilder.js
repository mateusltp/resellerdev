import { LightningElement, api, track, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from "lightning/navigation";
import getData from '@salesforce/apex/SMBPlanBuilderController.getData';
import {getRecord} from 'lightning/uiRecordApi';
import FASTTRACK_STAGE from '@salesforce/schema/Opportunity.FastTrackStage__c';
import { publish, MessageContext } from 'lightning/messageService';
import smbRefreshRevenueMetricsChannel from '@salesforce/messageChannel/SMBRefreshRevenueMetricsChannel__c';
import {
    subscribe,
    unsubscribe,
    onError
} from 'lightning/empApi';

export default class SmbPlanBuilder extends NavigationMixin(LightningElement) {

    @api recordId;

    proposalId;

    @api title = 'Plan Builder';

    currencyIsoCode;

    fastTrackStage;

    actionsDisabled = true;

    @track products = [];

    isLoading = true;

    showAddProducts = false;

    showSkuEdit = false;

    @track recordToEdit;

    showSkuDelete = false;

    recordToDeleteId;

    recordToDeleteName;

    channelName = '/event/Opportunity_Quantity_Updated__e';

    get headerTitle() {
        return this.title + ' (' + this.products.length + ')';
    }

    get showTable() {
        return this.products.length > 0;
    }

    @wire(getRecord, {recordId:'$recordId', fields:[FASTTRACK_STAGE]})
    wiredRecord({ data, error }){
        if(data){
            this.fastTrackStage = data.fields.FastTrackStage__c.value;
            this.actionsDisabled = this.fastTrackStage !== 'Offer Creation';
        }
        if(error){
            console.error(error);
            this.showToast('Error', error.body.message, 'error');
        }
    }

    @wire(MessageContext)
    messageContext;

    connectedCallback() {

        this.registerErrorListener();
        this.subscribeChannel();

        this.loadData();

    }

    disconnectedCallback() {
        unsubscribe(this.subscription, response => {
            console.log('unsubscribe() response: ', JSON.stringify(response));
            // Response is true for successful unsubscribe
        });
    }

    loadData() {
        this.isLoading = true;
        getData({ opportunityId: this.recordId })
        .then(result => {
            this.proposalId = result.proposalId;
            this.products = result.products;
            this.currencyIsoCode = result.currencyIsoCode;
        })
        .catch(error => {
            console.error(error);
            this.showToast('Error', error.body.message, 'error');
        })
        .finally(() => {
            this.isLoading = false;
        });
    }

    subscribeChannel() {
        const messageCallback = function (response) {
            if(response.data.payload.Opportunity_Id__c === this.recordId) {
                this.loadData();
                this.sendRevenueMetricsRefreshMessage();
            }
        };

        subscribe(this.channelName, -1, messageCallback.bind(this));
    }

    registerErrorListener() {
        onError((error) => {
            this.showToast('Received error from server: ', JSON.stringify(error), 'error');
            console.error('Received error from server: ', JSON.stringify(error));
        });
    }

    handleAddProducts() {
        this.showAddProducts = true;         
    }

    handleEditProduct(event) {
        this.recordToEdit = this.products.find(product => event.detail === product.Id);
        console.log('this.recordToEdit ',this.recordToEdit)
        this.showSkuEdit = true;
    }

    handleDeleteProduct(event) {
        this.recordToDeleteId = event.detail.recordId; 
        this.recordToDeleteName = event.detail.recordName; 
        this.showSkuDelete = true;
    }

    handleUpdateRecord(event) {
        let product = event.detail;
        const index = this.products.findIndex(element => element.Id === product.Id);
        this.products[index] = product;
        this.products = [...this.products];
        this.sendRevenueMetricsRefreshMessage();
    }

    handleDeleteRecord(event) {
        const recordId = event.detail;
        const index = this.products.findIndex(element => element.Id === recordId);
        this.products.splice(index, 1);
        this.products = [...this.products];
        this.sendRevenueMetricsRefreshMessage();
    }

    handleSaveRecords(event) {
        const savedRecords = event.detail; 
        savedRecords.forEach(element => {
            this.products.push(element);
        });
        this.products = [...this.products];
        this.showAddProducts = false;
        this.sendRevenueMetricsRefreshMessage();
    }

    handleAddProductsClose() {
        this.showAddProducts = false;
    }

    handleEditProductClose() {
        this.showSkuEdit = false;
    }

    handleDeleteClose() {
        this.showSkuDelete = false;
    }

    sendRevenueMetricsRefreshMessage() {
        const payload = { message: 'refresh' };
        publish(this.messageContext, smbRefreshRevenueMetricsChannel, payload);
    }

    navigateToRelatedList(){
        this[NavigationMixin.Navigate]({
            type: "standard__recordRelationshipPage",
            attributes: {
                recordId: this.proposalId,
                relationshipApiName: 'QuoteLineItems',
                actionName: "view",
                objectApiName: 'QuoteLineItem'
            }
        });
    }

    showToast(title, message, variant) {
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant,
        });
        this.dispatchEvent(evt);
    }


}