import { LightningElement, track, api, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getWaivers from '@salesforce/apex/SKUWaiversController.getWaivers';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import waiversInfo from '@salesforce/label/c.waiversInfo';
import waiverDurationError from '@salesforce/label/c.waiverDurationError';

const ordinalMap = {
        1: '1st',
        2: '2nd',
        3: '3rd'
    };

const percentageOptions = [
        { label: '10%', value: 10 },
        { label: '20%', value: 20 },
        { label: '30%', value: 30 },
        { label: '40%', value: 40 },
        { label: '50%', value: 50 },
        { label: '60%', value: 60 },
        { label: '70%', value: 70 },
        { label: '80%', value: 80 },
        { label: '90%', value: 90 },
        { label: '100%', value: 100 }
    ];

const durationOptions = [
        { label: '1 month', value: 1 },
        { label: '2 months', value: 2 },
        { label: '3 months', value: 3 },
        { label: '4 months', value: 4 },
        { label: '5 months', value: 5 },
        { label: '6 months', value: 6 },
        { label: '7 months', value: 7 },
        { label: '8 months', value: 8 },
        { label: '9 months', value: 9 },
        { label: '10 months', value: 10 },
        { label: '11 months', value: 11 },
        { label: '12 months', value: 12 },
    ];

export default class ResellerSkuWaivers extends LightningElement {

    @api recordId;

    @track sectionTitle = 'Waivers or Temporary discounts';

    @track waivers = [];

    @track waiversToDelete = [];

    monthsAfterLaunchRTId;
    waiversInformation = waiversInfo;

    get percentageOptions() {
        return percentageOptions;
    }

    get durationOptions() {
        return durationOptions;
    }

    @wire(getObjectInfo, { objectApiName: 'Waiver__c' })
    handleResult({ error, data }) {
        if(data) { 
            const rtis = data.recordTypeInfos;
            this.monthsAfterLaunchRTId = Object.keys(rtis).find(rti => rtis[rti].name === 'Months after Launch');
            if(!this.monthsAfterLaunchRTId) {
                this.showToast('Error', 'Waiver record types were not retrieved successfully', 'error');
            }
        }
        else if(error) {
            console.error(error);
            this.showToast('Error', 'Error while getting record types: ' + error, 'error');
        }    
    }

    connectedCallback() {
        getWaivers({ productId: this.recordId })
            .then(result => {
                result.forEach(waiver => {
                    let clonedWaiver = {...waiver};
                    clonedWaiver.ordinal = this.calculateOrdinal(clonedWaiver.Position__c);
                    this.waivers.push(clonedWaiver);
                });
            })
            .catch(error => {
                console.error(error);
                this.showToast('Error', '' + error.body.message, 'error');
            });
    }

    @api getWaiversToUpsert() {
        return this.waivers;
    }

    @api getWaiversToDelete() {
        return this.waiversToDelete;
    }

    handleAddWaiver() {
        if(this.canAddMoreWaiver()) {
            let position = this.waivers.length + 1;
            let ordinal =  this.calculateOrdinal(position);
            let waiver = {Position__c: position, ordinal: ordinal, Percentage__c: 10, Duration__c: 1, Quote_Line_Item__c: this.recordId, RecordTypeId: this.monthsAfterLaunchRTId};
            this.waivers.push(waiver);
        }
        else {
            this.showToast('Error', waiverDurationError, 'error');
        } 
    }

    canAddMoreWaiver() {
        return this.calculateTotalMonths() < 12;
    }

    calculateTotalMonths() {
        let totalMonths = this.waivers.reduce((partialSum, waiver) => partialSum + waiver.Duration__c, 0);
        return totalMonths;
    }

    handleDeleteWaiver(event) {
        let i = event.target.dataset.id - 1;
        if(this.waivers[i].Id) {
            this.waiversToDelete.push(this.waivers[i]);
        }
        this.waivers.splice(i, 1);
        while(i < this.waivers.length) {
            let position = i+1;
            this.waivers[i].Position__c = position;
            this.waivers[i].ordinal =  this.calculateOrdinal(position);
            i++;
        }         
    }

    calculateOrdinal(position) {
        return position > 3 ? position + 'th' : ordinalMap[position];
    }

    handlePercentageChange(event) {
        this.waivers[event.target.dataset.id-1].Percentage__c = parseInt(event.detail.value);
    }

    handleDurationChange(event) {
        let oldValue = this.waivers[event.target.dataset.id-1].Duration__c;
        this.waivers[event.target.dataset.id-1].Duration__c = parseInt(event.detail.value);

        if(this.calculateTotalMonths() > 12) {
            let combobox = this.template.querySelector('[data-label="Duration"][data-id="' + event.target.dataset.id + '"]');
            combobox.value = oldValue;
            this.waivers[event.target.dataset.id-1].Duration__c = oldValue;
            this.showToast('Error', waiverDurationError, 'error');
        }
    }

    showToast(title, message, variant) {
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant,
        });
        this.dispatchEvent(evt);
    }



}