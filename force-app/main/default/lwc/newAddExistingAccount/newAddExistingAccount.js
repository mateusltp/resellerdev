import { LightningElement, track, api, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { updateRecord, getRecord, getFieldValue } from 'lightning/uiRecordApi';
import PARENT_ID from '@salesforce/schema/Account.ParentId';
import PARTNER_LEVEL from '@salesforce/schema/Account.Partner_Level__c';
import ID_FIELD from '@salesforce/schema/Account.Id';
import PARENT_NETWORKID from "@salesforce/schema/Account.Network_ID__c";
import ACCOUNT_NAME from "@salesforce/schema/Account.Name";
import ACCOUNT_PARENTNAME from "@salesforce/schema/Account.Parent.Name";
import ACCOUNT_PARENTNETWORKID from "@salesforce/schema/Account.Parent.Network_ID__c";
import apexSearch from '@salesforce/apex/NewAddExistingAccountController.search';


const fields = [PARENT_NETWORKID, PARTNER_LEVEL, ACCOUNT_NAME, ACCOUNT_PARENTNAME, ACCOUNT_PARENTNETWORKID];

export default class NewAddExistingAccount extends LightningElement {
    @track addExistingAccountPage = true;
    @track isParentModalOpen = false;
    @track isNetworkIdOpen = false;
    @api getNewParent; //id lst que vem do pai
    spinner = false;
    @track parentNetworkId;
    @track newparentNetworkId;
    @track newparentPartnerLevel;
    @track networkId;
    @track currentParent;
    newPartnerLevel;
    acctId;
    fieldsMap = new Map();
    accountName;
    newparentName;
    accountParentName;
    accountParentNetworkId;

    @wire(getRecord, {
        recordId: '$networkId',
        fields
    })
    account;

    @wire(getRecord, {
        recordId: '$acctId',
        fields
    })
    childaccount;

    renderedCallback() {
        this.networkId = this.getNewParent[0];
        this.newparentNetworkId = getFieldValue(this.account.data, PARENT_NETWORKID);
        this.newparentPartnerLevel = getFieldValue(this.account.data, PARTNER_LEVEL);
        this.newparentName = getFieldValue(this.account.data, ACCOUNT_NAME);
    }

    disconnectedCallback() {
        this.fieldsMap = new Map();
        this.addExistingAccountPage = true;
        this.isParentModalOpen = false;
    }

    handleSearchAccountLookup(event) {
        if (this.showMiniPage) this.showMiniPage = false;
        JSON.stringify(event.detail);
        this.acctId = event.detail[0];
    }

    handleSave() {
        this.handleRecordSave();
    }

    handleRecordSave() {
        const inputFields = this.template.querySelectorAll('lightning-input-field');
        if (inputFields && this.addExistingAccountPage) {
            inputFields.forEach(field => {
                this.fieldsMap.set(field.name, field.value);
            });
        }

        try {
            if (this.acctId != null) {
                this.handleValidation();
            }
        } catch (error) {
            this.hideSpinner();
            this.errors = JSON.stringify(error);
            this.dispatchEvent(new ShowToastEvent({
                title: 'Please select an account',
                message: this.errors,
                variant: 'error'
            }));
        }
    }

    handleValidation() {
        this.getNewParent = this.fieldsMap.get('getNewParent');
        this.currentParent = this.fieldsMap.get('CurrentParent');
        this.newPartnerLevel = this.fieldsMap.get('PartnerLevel');
        this.parentNetworkId = getFieldValue(this.childaccount.data, PARENT_NETWORKID);
        this.accountName = getFieldValue(this.childaccount.data, ACCOUNT_NAME);
        this.accountParentName = getFieldValue(this.childaccount.data, ACCOUNT_PARENTNAME);
        this.accountParentNetworkId = getFieldValue(this.childaccount.data, PARENT_NETWORKID);

        if (this.getNewParent != this.currentParent && this.currentParent !== null && this.addExistingAccountPage === true && this.getNewParent != this.acctId) {
            if (this.parentNetworkId != this.newparentNetworkId && this.newparentPartnerLevel == 'Brand') {
                this.isNetworkIdOpen = true;
            }
            this.addExistingAccountPage = false;
            this.isParentModalOpen = true;
            return;
        } else if (this.getNewParent == this.currentParent && this.addExistingAccountPage === true) {
            console.log('pais iguais');
            this.dispatchEvent(new ShowToastEvent({
                title: "This child account belongs to this parent account already.",
                message: "Please select another child account.",
                variant: "error",
            }),
            );
            return;
        } else if (this.getNewParent == this.acctId) {
            console.log('parent the same as lookup ERROR!!! ');
            this.dispatchEvent(new ShowToastEvent({
                title: "They are the same account!",
                message: "Please select a proper child account.",
                variant: "error",
            }),
            );
            return;
        }

        const fields = {};
        fields[ID_FIELD.fieldApiName] = this.acctId;
        fields[PARENT_ID.fieldApiName] = this.getNewParent[0];
        fields[PARTNER_LEVEL.fieldApiName] = this.newPartnerLevel;
        this.handleUpdate(fields);
    }

    handleUpdate(fields) {
        const recordInput = { fields };
        console.log('Campos a serem atualizados >>> ' + JSON.stringify(recordInput));
        this.showSpinner();
        updateRecord(recordInput)
            .then(() => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Success',
                        message: 'Parent Account updated',
                        variant: 'success'
                    })
                );
                return this.closeModal();
            })
            .catch(error => {
                this.hideSpinner();
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error updating record',
                        // message: error.body.message,
                        message: error.body.output.errors[0].message,
                        variant: 'error'
                    })
                );
            });
    }

    closeModal() {
        console.log('On close modal');
        const myEvent = new CustomEvent('close');
        this.dispatchEvent(myEvent);
    }

    showSpinner() {
        this.spinner = true;
    }

    hideSpinner() {
        this.spinner = false;
    }

    showMiniPage = false;

    mouseOver(evt) {
        console.log('onmouseover!!!');
        if (this.acctId != null) {
            this.showMiniPage = true;
        }
    }

    mouseOut(evt) {
        console.log('onmouseout!!!');
        this.showMiniPage = false;
    }

    handleMiniPageClosure(e) {
        this.closeMiniPage();
        // test
        this.closeMiniPage2();
    }

    closeMiniPage() {
        console.log('On close mini page');
        this.showMiniPage = false;
    }

    handleSearch(event) {
        const lookupElement = event.target;
        apexSearch(event.detail)
            .then(results => {
                lookupElement.setSearchResults(results);
            })
            .catch(error => {
                // TODO: handle error
            });
    }

    handleBack() {
        this.fieldsMap = new Map();
        this.addExistingAccountPage = true;
        this.isParentModalOpen = false;
        this.acctId = '';
        this.accountParentName = '';
        this.accountParentNetworkId = '';
    }
}