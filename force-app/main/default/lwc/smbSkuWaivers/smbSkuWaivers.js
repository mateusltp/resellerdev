import { LightningElement, track, api, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getData from '@salesforce/apex/SmbSkuWaiversController.getData';
import checkApprovalNeed from '@salesforce/apex/SmbSkuWaiversController.checkApprovalNeed';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';


const ordinalMap = {
        1: '1st',
        2: '2nd',
        3: '3rd'
    };

const percentageOptions = [
        { label: '10%', value: 10 },
        { label: '20%', value: 20 },
        { label: '30%', value: 30 },
        { label: '40%', value: 40 },
        { label: '50%', value: 50 },
        { label: '60%', value: 60 },
        { label: '70%', value: 70 },
        { label: '80%', value: 80 },
        { label: '90%', value: 90 },
        { label: '100%', value: 100 }
    ];

export default class SmbSkuWaivers extends LightningElement {

    @api recordId;

    @track sectionTitle = 'Waivers or Temporary discounts';

    @track needApproval = false;

    @track durationOptions = [];

    @track waivers = [];

    @track waiversToDelete = [];

    monthsAfterLaunchRTId;

    isCheckingApprovalNeed = true;

    quoteId;

    get approval() {
        return this.needApproval? "Need" : "No need";
    }

    get needApprovalClass() {
        return this.needApproval? "slds-badge slds-theme_error" : "slds-badge slds-theme_success";
    }

    get percentageOptions() {
        return percentageOptions;
    }

    @wire(getObjectInfo, { objectApiName: 'Waiver__c' })
    handleResult({ error, data }) {
        if(data) { 
            const rtis = data.recordTypeInfos;
            this.monthsAfterLaunchRTId = Object.keys(rtis).find(rti => rtis[rti].name === 'Months after Launch');
            if(!this.monthsAfterLaunchRTId) {
                this.showToast('Error', 'Waiver record types were not retrieved successfully', 'error');
            }
        }
        else if(error) {
            console.error(error);
            this.showToast('Error while getting record types', error.body.message, 'error');
        }    
    }

    connectedCallback() {
        getData({ productId: this.recordId })
            .then(result => {
                result.waivers.forEach(waiver => {
                    let clonedWaiver = {...waiver};
                    clonedWaiver.ordinal = this.calculateOrdinal(clonedWaiver.Position__c);
                    this.waivers.push(clonedWaiver);
                });
                this.durationOptions = result.durationOptions;
                this.quoteId = result.quoteId;
                if(this.waivers.length > 0) {
                    this.checkApprovalNeed();
                }
                else {
                    this.isCheckingApprovalNeed = false;
                }
                    
            })
            .catch(error => {
                console.error(error);
                this.showToast('Error', error.body.message, 'error');
            });
    }

    getDateFromDateString(dateString) {
        const date = dateString.split("/");
        return new Date(date[2], date[0], date[1]); // year month day
    }

    @api
    reportValidity() {
        return true;
    }

    @api getWaiversToUpsert() {
        return this.waivers;
    }

    @api getWaiversToDelete() {
        return this.waiversToDelete;
    }

    getNextOptionIndex() {
        let i = this.waivers.length - 1;
        if(i >= 0) {
            const waiver = this.waivers[i];
            const index = this.durationOptions.findIndex(option => option.value === waiver.End_Date_Text__c) + 1;
            return index;
        }
        else {
            return 0;
        }
    }

    handleAddWaiver() {
        const nextOptionIndex = this.getNextOptionIndex();
        if(nextOptionIndex <= this.durationOptions.length -1) {
            let dateString = this.durationOptions[nextOptionIndex].value;
            let position = this.waivers.length + 1;
            let ordinal =  this.calculateOrdinal(position);
            let waiver = { RecordTypeId: this.monthsAfterLaunchRTId, 
                            Position__c: position, 
                            ordinal: ordinal, 
                            Percentage__c: 10, 
                            End_Date_Text__c: dateString,
                            Quote_Line_Item__c: this.recordId,
                            QuoteId__c: this.quoteId
                        };
            this.waivers.push(waiver);
            this.checkApprovalNeed();
        }
        else {
            this.showToast('Error', 'You have no more periods available to add waiver', 'error');
        }
    }

    handleDeleteWaiver(event) {
        let i = event.target.dataset.id - 1;
        if(this.waivers[i].Id) {
            this.waiversToDelete.push(this.waivers[i]);
        }
        this.waivers.splice(i, 1);
        while(i < this.waivers.length) {
            let position = i+1;
            this.waivers[i].Position__c = position;
            this.waivers[i].ordinal =  this.calculateOrdinal(position);
            i++;
        }       
        this.checkApprovalNeed();
    }

    calculateOrdinal(position) {
        return position > 3 ? position + 'th' : ordinalMap[position];
    }

    handlePercentageChange(event) {
        this.waivers[event.target.dataset.id-1].Percentage__c = parseInt(event.detail.value);
    }

    handleDurationChange(event) {
        const waiverIndex = event.target.dataset.id-1;
        let oldDateString = this.waivers[waiverIndex].End_Date_Text__c;
        const newDateString = event.detail.value;
        const newDate = this.getDateFromDateString(newDateString);
        
        if(newDateString === oldDateString) {
            return;
        }
        
        let previousDate = new Date(1900, 1, 1);
        if(this.waivers[waiverIndex - 1]) {
            previousDate = this.getDateFromDateString(this.waivers[waiverIndex - 1].End_Date_Text__c);
        }

        let nextDate = new Date(3000, 1, 1);
        if(this.waivers[waiverIndex + 1]) {
            nextDate = this.getDateFromDateString(this.waivers[waiverIndex + 1].End_Date_Text__c);
        }
        if(newDate > previousDate && newDate < nextDate) {
            this.waivers[waiverIndex].End_Date_Text__c = newDateString;
            this.checkApprovalNeed();
        }
        else {
            let combobox = this.template.querySelector('[data-label="Duration"][data-id="' + event.target.dataset.id + '"]');
            combobox.value = oldDateString;
            this.showToast('Error', 'You cannot change duration for selected date', 'error');
        }

    }

    checkApprovalNeed() {
        this.isCheckingApprovalNeed = true;

        checkApprovalNeed({ productId: this.recordId, waivers: this.waivers, waiversToDelete: this.waiversToDelete })
            .then(result => {
                this.needApproval = result;
            })
            .catch(error => {
                console.error(error);
                this.showToast('Error', error.body.message, 'error');
            })
            .finally(() => {
                this.isCheckingApprovalNeed = false;
            });
    }

    showToast(title, message, variant) {
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant,
        });
        this.dispatchEvent(evt);
    }



}