import { LightningElement, track, api } from 'lwc';
import showActivities from '@salesforce/apex/NewPartnerProductController.showActivities';

const columns = [
    { label: 'Activity Name', fieldName: 'Name' }
];

export default class NewProductCreationGymActivities extends LightningElement {
    @api partnerProductTo = {};
    @track error;
    @track columns = columns;
    @track tableData = [];
    @track tableFilterData = [];
    @track setSelectedRows = [];
    @track showSpinner = false;
    tableDataBackUp = [];
    selectedActivities = [];
    isFilter = false;
    @track setSelectedRowsFilter = [];
    currentSelectedRows = [];
    unselectedRows = [];
    showOrHideStyle;

    connectedCallback() {
        this.showSpinner = true;
        showActivities()
            .then(result => {
                this.tableData = result;
                this.tableDataBackUp = this.tableData;
                this.tableFilterData = this.tableData;
                if (this.partnerProductTo.selectedGymActivities !== null && this.partnerProductTo.selectedGymActivities.length > 0) {
                    this.setPreviousActivities();
                }
                this.showSpinner = false;

            })
            .catch(error => {
                this.error = error;
                this.showSpinner = false;
            });
    }

    setPreviousActivities() {
        this.setSelectedRows = this.partnerProductTo.selectedGymActivities;
        this.setSelectedRows.forEach(id => {
            let gymActivity = this.tableData.find(element => element.Id == id);
            if (gymActivity != undefined) {
                this.selectedActivities.push(gymActivity);
            }
        });
    }

    disconnectedCallback() {
        if (this.setSelectedRows !== null) {
            this.partnerProductTo.selectedGymActivities = this.setSelectedRows;
        }
    }

    handleSearchAction(evt) {
        var filteredRows;

        var resultFiltered = this.tableFilterData.filter(x => {
            return !this.selectedActivities.some(t => t.Name === x.Name)
        })
        var noResultsFiltered = this.tableDataBackUp.filter(x => {
            return !this.selectedActivities.some(t => t.Name === x.Name)
        })

        let rows = [... this.tableFilterData],
            term = evt.target.value,
            results = rows, regex;

        if (term.length < 1) {
            this.isFilter = false;
            this.showOrHideStyle = '';
            this.tableFilterData = [...this.tableDataBackUp];
            return;
        }

        try {
            this.isFilter = true;
            this.showOrHideStyle = 'slds-hide';
            this.setSelectedRowsFilter = [...this.setSelectedRows];
            this.currentSelectedRows = [...this.setSelectedRows];
            regex = new RegExp(term, "i");
            results = resultFiltered.filter(row => regex.test(row.Name));
            filteredRows = results;
            this.tableFilterData = (filteredRows.length > 0) ? filteredRows : noResultsFiltered;
        } catch (e) {

        }
    }

    getSelectedName() {
        this.updatePillSelectedGymActivity();
        this.updateSelectGymActivityIds();
    }

    getSelectedNameFilter() {
        let currentRows = this.template.querySelector("lightning-datatable[data-my-id=tableFilter]").getSelectedRows();
        if (this.currentSelectedRows.length > 0) {
            let selectedIds = currentRows.map(row => row);
            this.unselectedRows = this.currentSelectedRows.filter(row => !selectedIds.includes(row));
        }
        this.currentSelectedRows = currentRows;

        if (this.unselectedRows.length > 0) {
            let arrItems6 = [];
            this.unselectedRows.forEach(Item => {
                arrItems6.push(Item.Name);
            })
            this.selectedActivities = this.selectedActivities.filter(item => item.Name != arrItems6);
        }
        this.updatePillSelectedGymActivityFilter();
        this.updateSelectGymActivityIdsFilter();
    }

    handleRemoveRecord(event) {
        if (this.isFilter) {
            const removeItem = event.target.dataset.item;
            this.selectedActivities = this.selectedActivities.filter(item => item.Name != removeItem);
            let arrItems3 = [];
            this.selectedActivities.forEach(activity => {
                arrItems3.push(activity.Id);
            })
            this.setSelectedRowsFilter = arrItems3;
        }
        const removeItem = event.target.dataset.item;
        this.selectedActivities = this.selectedActivities.filter(item => item.Name != removeItem);
        this.updateSelectGymActivityIds();
    }

    updatePillSelectedGymActivity() {
        this.selectedActivities = this.template.querySelector('lightning-datatable').getSelectedRows();
    }

    updatePillSelectedGymActivityFilter() {
        let uniq = [];
        let selectedRecords = this.template.querySelector("lightning-datatable[data-my-id=tableFilter]").getSelectedRows();
        selectedRecords.forEach(currentItem => {
            uniq.push(currentItem);
        });
        let returnedPills = [...this.selectedActivities, ...uniq];
        let uniqPills = [...returnedPills.reduce((map, obj) => map.set(obj.Name, obj), new Map()).values()];
        this.selectedActivities = uniqPills;
    }

    updateSelectGymActivityIds() {
        const arrItems = [];
        this.selectedActivities.forEach(activity => {
            arrItems.push(activity.Id);
        })
        //verificar o erro
        this.setSelectedRows = arrItems;
    }

    updateSelectGymActivityIdsFilter() {
        let arrItems2 = [];
        this.selectedActivities.forEach(activity => {
            arrItems2.push(activity.Id);
        })
        this.setSelectedRows = arrItems2;
    }

    get numberSelectedItems() {
        return this.selectedActivities.length;
    }

    @api
    handleActivitiesValidation() {
        return this.selectedActivities.length;
    }
}