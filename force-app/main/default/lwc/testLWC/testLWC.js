import { LightningElement, api } from 'lwc';

import apexSearch from '@salesforce/apex/SearchCustomLookupController.searchContact';

export default class TestLWC extends LightningElement {

    //@api contactRecord = ["00378000009QtUaAAK","00378000009zsAvAAI"];
    @api contactRecord = "00378000009QtUaAAK";

    

    // handleContactChange(event) {
    //     this.contactRecord = "00378000009QtUaAAK";
    // }

    get conditions() {
        return 'AccountId = \'0017800000AwT4kAAF\'';
    }

    get previouslySelectedConditions() {
        //-- return 'AccountId = ' + 'parentInputProperty';
        return 'AccountId = \'0017800000AwT4kAAF\'';
    }

    handleSearch(event) {
        const lookupElement = event.target;
        apexSearch(event.detail)
            .then(results => {
                lookupElement.setSearchResults(results);
            })
            .catch(error => {
                reduceErrors(error);
            });
    }

    handleCreateNewContact(event) {
        console.log("### Into the parent component ### handleCreateNewContact ###");
        console.log(event.target.value);
        
    }

    

}