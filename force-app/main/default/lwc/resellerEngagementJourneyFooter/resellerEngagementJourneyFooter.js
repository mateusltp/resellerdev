import { LightningElement, api } from 'lwc';

import { updateRecord } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { FlowAttributeChangeEvent, FlowNavigationNextEvent} from 'lightning/flowSupport';

export default class ResellerEngagementJourneyFooter extends LightningElement {

    @api displaySaveButton = false;
    @api displayContinueButton = false;
    @api isValidForm = false;
    @api formStatus = false;

    @api saveButtonVariant = 'brand';
    @api continueButtonVariant = 'brand';

    @api saveButtonLabel = 'Save';
    @api continueButtonLabel = 'Continue';

    @api actionClicked;
    showSpinner = false;

    handleSave() {
        this.actionClicked = 'save';
        this.dispatchChangeAttributeEvent(this.actionClicked);
    }

    handleContinue() {
        this.actionClicked = 'continue';
        this.dispatchChangeAttributeEvent(this.actionClicked);
    }

    dispatchChangeAttributeEvent(action) {
        const attributeChangeEvent = new FlowAttributeChangeEvent(
            'actionClicked',
            action
        );
        this.dispatchEvent(attributeChangeEvent);
        this.dispatchNextEvent();
    }

    dispatchNextEvent() {
        const navigateNextEvent = new FlowNavigationNextEvent();
        this.dispatchEvent(navigateNextEvent);
    }

}