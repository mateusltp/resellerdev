/**
 * 
 */
import { LightningElement, api } from 'lwc';

import { NavigationMixin } from 'lightning/navigation';

export default class NavigateToFlexipage extends LightningElement {

    @api recordId;
    @api objectApiName;
    @api actionType = 'view';
    @api pageType = 'standard__recordPage';

    connectedCallback() {
        //-- navigate to record page
        if(this.pageType == 'standard__recordPage') {
            this[NavigationMixin.Navigate]({
                type: 'standard__recordPage',
                attributes: {
                    recordId: this.recordId,
                    objectApiName: this.objectApiName,
                    actionName: this.action
                }
            });
        }
        
    }

}