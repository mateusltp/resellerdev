import { LightningElement, api } from 'lwc';


const columns = [
    { label: 'Name', fieldName: 'Name' },
    { label: 'Email', fieldName: 'Email', type: 'email' },
    { label: 'Phone', fieldName: 'Phone', type: 'phone' }
];

export default class ResellerEJContactsConfirmationModal extends LightningElement {

    @api contactsList = [];
    @api account;
    @api linkLabel = 'List of Contacts';
    @api modalHeader = 'Client\'s Contacts in Salesforce';
    @api confirmContactsModalAction;

    @api backButtonLabel = 'Cancel';
    @api backButtonVariant = 'brand';

    @api manageContactsButtonLabel = 'Manage Contacts';
    @api manageContactsButtonVariant = 'brand';

    columns = columns;
    isToDisplay = false;

    // connectedCallback() {}

    handleClick() {
        this.isToDisplay = true;
    }

    handleCancel() {

    }

    
}