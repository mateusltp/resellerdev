import { ShowToastEvent } from "lightning/platformShowToastEvent";

/**
* Open a toast
* @param {object} cmpReference - Pass "this" on this parameter
* @param {string} message Message to be showed in the toast
* @param {string} title toast title
* @param {string} duration duration of the toast in miliseconds
* @param {string} variant error, warning, success, info
* @returns {void}
*/
const showToast = (cmpReference, message = '', title = '', duration = 6000, variant = 'success') => {
    cmpReference.dispatchEvent(
      new ShowToastEvent({
        title,
        message,
        variant,
        duration
      })
    );
   }
    
   /**
   * Treat error message that can come from UI APi, Apex, DML or LDS
   * @param {object} value - error object or message
   * @returns {string} a treated message that can be showed to the user
   */
   const treatErrors = value => {
    //value can be array or singleton, normalize to array
    if (!Array.isArray(value)) {
      value = [value];
    }
    return (
      value
        //eliminate empty strings & null values
        .filter(msg => !!msg)
        //extract and format details from a variety of message types
        .map(msg => {
          if (typeof msg === 'string') {
            return msg;
          } else if (Array.isArray(msg.body)) {
            //extract details from UI API read error array
            let msgs = msg.body.map(b => "Error: " + b.message);
            //make a readable string from the array
            return Array.from(msgs.values()).join(", ");
          } else if (msg.body && typeof msg.body.message === "string") {
            //extract Apex, DML and other system errors
            if (msg.body.output && Array.isArray(msg.body.output.errors)) {
              return msg.body.output.errors.map(item => item.message);
            }
            return msg.body.message;
          } else if (msg.message && typeof msg.message === "string") {
            //extract JS errors and custom detail messages
            return msg.message;
          }
          //return HTTP status code if provided, else use failsafe string
          return msg.statusText ? msg.statusText : "Unknown error";
        })
        //flatten array
        .reduce((rtn, msg) => rtn.concat(msg) , [])
    );
   };
    
/**
* Standardize lightning data service error handling, with toast
* @param {object} cmpRefence - reference of the class of the component.
* @param {object} error - error object that wire function sends.
* @returns {boolean} - true if has no errors
*/
const ldsErrorHandling = (cmpRefence, error) => {
    let message = treatErrors(error);
        if (message.length > 0) {
            console.error(message);
            cmpRefence.dispatchEvent(
                new ShowToastEvent({
                    title: "Error",
                    message: message.join('\n'),
                    variant: "error",
                    duration: 6000
                })
            );
            return false;
        }
    return true;
};
    
   /**
   * Console log one object with json parse + json stringify
   * @param {object} obj - obj to log
   * @returns void
   */
   const logObj = obj => {
    console.log(JSON.parse(JSON.stringify(obj)));
   };
    
   /**
   * Transform data return by getRecord wire service into simple record
   * @param {object} data - data returned by getRecord wire service
   * @returns {object} - return a simple record
   */
   const ldsSimpleRecord = data => {
    return Object.keys(data.fields).reduce((result, item) => {
        result[item] = data.fields[item].value;
        return result;
    }, {});
   };
    
   export {
    ldsErrorHandling,
    logObj,
    treatErrors,
    showToast,
    ldsSimpleRecord
   };
    
   /**
   * Used on LWCChart Bundle
   *
   * @param {*} attribute
   */
   export function sanitize(attribute) {
    let value = attribute;
    if (value) {
      try {
        value = JSON.parse(value.replace(/\\/g, ''));
      } catch (e) {
        /* Silently */
      }
    }
    return value;
   }
    
   /**
   * Parse the received value into a boolean. This is required because
   * LWC does not allow boolean values directly in the markup
   * @param {string | boolean} value A true / false value, in string or boolean format
   * @returns The boolean parsed value
   */
   export function parseBoolean(value) {
    return typeof value == 'string' ? value === 'true' : value;
   }