import { LightningElement, api, track } from 'lwc';

export default class ChooseRequestType extends LightningElement {

    @track isModalOpen = false;
    typeConsult ='';

    @api start(){
        this.isModalOpen = true;
    }
    
    closeModal() {
        this.isModalOpen = false;
    }

    handleTypeConsult(event){
        this.typeConsult = event.target.value;
    }


}