import { LightningElement, track, wire, api } from 'lwc';
import { getRecordUi, createRecord, updateRecord, getRecord } from 'lightning/uiRecordApi';
import { getPicklistValues } from 'lightning/uiObjectInfoApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { getNewLayoutTemplateUsingParentRecord } from 'c/customLayoutUtils';
import ID_FIELD from '@salesforce/schema/Account.Id';
import PARENT_FIELD from '@salesforce/schema/Account.ParentId';
import getContactList from '@salesforce/apex/NetworkBuilderCreateNewAccountController.getContactsFromParent';
import getContactRecordTypeId from '@salesforce/apex/NetworkBuilderCreateNewAccountController.getRecordTypeId';
import ACCOUNT_OBJECT from '@salesforce/schema/Account';
import ACCOUNT_CONTACT_RELATION_OBJECT from '@salesforce/schema/AccountContactRelation';
//import ACCOUNT_CONTACT_ROLES from '@salesforce/schema/AccountContactRelation.Roles';
import ACCOUNT_CONTACT_ROLES from '@salesforce/schema/AccountContactRelation.Type_of_Contact__c';
import ACCOUNT_RECORDTYPE_FIELD from '@salesforce/schema/Account.RecordTypeId';


export default class NetworkBuilderCreateNewAccount extends LightningElement {
    @api selectedParentAcccount = [];
    newAccountId;
    recordTypeId;
    parentId;
    @track formParentId;
    @track showSpinner = true;
    @track sectionWithFields = [];
    @track parentContactRelation = [];
    selectedParentContactRelation = [];
    @track contactsToCreate = [];
    @track contactRoleOptions = [];
    @track showCreateNewContactModal = false;
    @track contactRecordTypeId = null;
    layoutType = 'Full';
    modeType = 'Edit';
    sectionsToExclude = [];
    fieldsToExclude = [];
    @track activeSections = [];
    
    connectedCallback() {
        if(this.selectedParentAcccount) {
            this.parentId = this.selectedParentAcccount[0];
        }
    }

    @wire(getRecord, { recordId: '$parentId', fields: [ACCOUNT_RECORDTYPE_FIELD] })
      acc;

    get recordTypeId() {
        return this.acc.data.fields.RecordTypeId.value;
    }

    @wire(getRecordUi, { recordIds: '$selectedParentAcccount', layoutTypes: '$layoutType', modes: '$modeType' })
    accountRecordUi({ error, data }) {     
        if(data){
            const template = getNewLayoutTemplateUsingParentRecord( data, this.selectedParentAcccount[0], ACCOUNT_OBJECT.objectApiName, this.layoutType, this.modeType, this.sectionsToExclude, this.fieldsToExclude);
            this.recordTypeId = template.recordTypeId;
            template.sectionWithFields.forEach(section => {
                this.activeSections.push(String(section.key));
                console.log('>>>>>>>>>>>>>>>>' + this.activeSections)
                section.value.forEach( field => {
                    if(field.apiName == PARENT_FIELD.fieldApiName){
                        field['isParentId'] = true;
                    }
                })
            })
            this.sectionWithFields = template.sectionWithFields;
        } else if(error){
            this.errorHandler(error);
        }
    };

    @wire(getPicklistValues, { recordTypeId: '$recordTypeId', fieldApiName: ACCOUNT_CONTACT_ROLES })
    roleOptions({ error, data }) {
        if (data) {
            data.values.forEach(option => {
                let objOption = {};
                objOption.label = option.label;
                objOption.value = option.value;
                this.contactRoleOptions.push(objOption);
            })
        }
    }

    async createAccount(accountRecordInput) {
        try {
            this.checkLegalRepresentatives();
            if (this.legalRepresentatives > 1) {
                this.showToastNotification('Multiple Legal Representatives found.', 'You can only select one Legal Representative.', 'warning');
                return;
            }
            this.enableSpinner();
            const accountRecord = await createRecord(accountRecordInput);
            this.newAccountId = accountRecord.id;
            let successAccountEvnt = new ShowToastEvent({
                title: 'Success',
                message: 'Your changes have been saved.',
                variant: 'success'
            });
            this.dispatchEvent(successAccountEvnt);
            for (let i = 0; i < this.selectedParentContactRelation.length; i++) {
                if (this.legalRepresentatives === 1) {
                    let contactRel = this.selectedParentContactRelation[i];
                    if (contactRel.Type_of_Contact__c.includes('Legal Representative')) {
                        let accountResult = await updateRecord(this.populateAccountUpdate(this.selectedParentContactRelation[i]));
                        console.log('account updated: '+accountResult.id)
                    }
                }
                let contactRelatResult = await createRecord(this.populateContactRelationInput(this.selectedParentContactRelation[i], accountRecord.id));
                let successContRelEvnt = new ShowToastEvent({
                    title: 'Success',
                    message: 'Contact imported with Id: ' + contactRelatResult.id,
                    variant: 'success'
                });
                this.dispatchEvent(successContRelEvnt);
            }

            for (let i = 0; i < this.contactsToCreate.length; i++) {
                this.contactsToCreate[i].fields["AccountId"] = accountRecord.id;
                let newContactResult = await createRecord(this.contactsToCreate[i]);
                let successContEvnt = new ShowToastEvent({
                    title: 'Success',
                    message: 'Contact created with Id: ' + newContactResult.id,
                    variant: 'success'
                });
                this.dispatchEvent(successContEvnt);
            }
            this.closeModal();
        } catch (error) {
            this.errorHandler(error);
        }
    }

    errorHandler(error) {
        console.log('ERROR');
        console.log(JSON.stringify(error));
        this.disableSpinner();
        var errorMessage = 'Unknown error';
        if (error && error.body && Array.isArray(error.body)) {
            errorMessage = error.body.map(e => e.message).join(', ');
        } else if ((error && error.body && error.body.output && error.body.output.errors && Array.isArray(error.body.output.errors) && error.body.output.errors.length > 0)) {
            errorMessage = error.body.output.errors.map(e => e.message).join(', ');
        } else if ((error && error.body && error.body.output && error.body.output.fieldErrors)) {
            errorMessage = (JSON.stringify(error.body.output.fieldErrors).split('message')[1]).split(':')[2];
        }
        else if (error && error.body && error.body.message) {
            errorMessage = error.body.message;
        }

        let errorEvent = new ShowToastEvent({
            title: 'Error!',
            message: errorMessage,
            variant: 'error'
        });
        this.dispatchEvent(errorEvent);
        //this.closeModal();
    }

    handleSaveAccount() {
        const inputFields = this.template.querySelectorAll('.c-input-account');
        const isInputsCorrect = [...inputFields]
            .reduce((validSoFar, inputField) => {
                return validSoFar && inputField.reportValidity();
            }, true);
        if (isInputsCorrect) {
            this.createAccount(this.populateAccountRecordInput(inputFields));
        }
    }


    handleImportContacts(event) {
        this.enableSpinner();
        if (event.target.checked) {
            getContactList({ recordId: this.selectedParentAcccount[0] })
                .then((result) => {
                    this.parentContactRelation = result;
                })
                .catch((error) => {
                    this.parentContactRelation = [];
                });
            this.disableSpinner();
        }
        else {
            this.parentContactRelation = [];
            this.disableSpinner();
        }

    }

    handleSelectedImportContact(event) {
        this.selectedParentContactRelation = [];
        let selectedRows = this.template.querySelectorAll('.contactRelationInput');
        for (let i = 0; i < selectedRows.length; i++) {
            if (selectedRows[i].checked && selectedRows[i].type === 'checkbox') {
                this.selectedParentContactRelation.push({
                    ContactId: selectedRows[i].value,
                    Type_of_Contact__c: ''
                })
                let queryTerm = "c-multi-select-picklist[data-id='" + selectedRows[i].value + "']";
                this.template.querySelector(queryTerm).disable(false);
            }
            else if (!selectedRows[i].checked && selectedRows[i].type === 'checkbox') {
                let parentImportIndex = this.selectedParentContactRelation.findIndex(rel => rel.ContactId === selectedRows[i].value);
                if (parentImportIndex > -1) {
                    this.selectedParentContactRelation.splice(parentImportIndex, 1);
                }
                let queryTerm = "c-multi-select-picklist[data-id='" + selectedRows[i].value + "']";
                this.template.querySelector(queryTerm).disable(true);
            }
        }

    }

    /*handleSelectedImportContact() {
        this.selectedParentContactRelation = [];
        let selectedRows = this.template.querySelectorAll('.contactRelationInput');
        for (let i = 0; i < selectedRows.length; i++) {
            if (selectedRows[i].checked && selectedRows[i].type === 'checkbox') {
                let queryTerm = "[data-id='" + selectedRows[i].value + "']";
                let roleOption = this.template.querySelectorAll(queryTerm)[0];
                this.selectedParentContactRelation.push({
                    ContactId: selectedRows[i].value,
                    Role: roleOption.value
                })
            }
        }
    }*/

    handleContactRoleChange() {
        this.handleSelectedImportContact();
    }

    get hasContactInParentToImport() {
        return this.parentContactRelation.length > 0 ? true : false;
    }

    get parentContactsQuantity() {
        return this.parentContactRelation.length;
    }

    get hasContactsToCreate() {
        return this.contactsToCreate.length > 0 ? true : false;
    }

    get contactsToCreateQuantity() {
        return this.contactsToCreate.length;
    }

    get parentContactRelationData() {
        if (this.parentContactRelation) {
            let data = [];
            this.parentContactRelation.forEach(contact => {
                let newContact = {};
                newContact.Id = contact.Contact.Id;
                newContact.Name = contact.Contact.Name;
                newContact.Email = contact.Contact.Email;
                newContact.Roles = [];
                data.push(newContact);
            });
            return data;
        }
        return null;
    }

    populateAccountRecordInput(inputFields) {
        let accountRecordInput = this.recordInputForCreate(ACCOUNT_OBJECT.objectApiName);
        inputFields.forEach(field => {
            if (field.name == 'BillingAddress') {
                accountRecordInput.fields['BillingCity'] = field.value.BillingCity;
                accountRecordInput.fields['BillingCountryCode'] = field.value.BillingCountryCode;
                accountRecordInput.fields['BillingPostalCode'] = field.value.BillingPostalCode;
                accountRecordInput.fields['BillingStateCode'] = field.value.BillingStateCode;
                accountRecordInput.fields['BillingStreet'] = field.value.BillingStreet;
            } else if (field.name != ID_FIELD.fieldApiName) {
                accountRecordInput.fields[String(field.name)] = field.value;
            }
        });
        accountRecordInput.fields['ParentId'] = this.selectedParentAcccount[0];
        accountRecordInput.fields['RecordTypeId'] = this.recordTypeId;
        return accountRecordInput;
    }

    populateContactRelationInput(contactRelation, AccountId) {
        let contactRelationInput = this.recordInputForCreate(ACCOUNT_CONTACT_RELATION_OBJECT.objectApiName);
        contactRelationInput.fields['ContactId'] = contactRelation.ContactId;
        contactRelationInput.fields['AccountId'] = AccountId;
        contactRelationInput.fields['Type_of_Contact__c'] = contactRelation.Type_of_Contact__c;
        return contactRelationInput;
    }

    populateAccountUpdate(contactRelation) {
        let accountInput = this.recordInputForUpdate();
        accountInput.fields['Id'] = this.newAccountId;
        accountInput.fields['Legal_Representative__c'] = contactRelation.ContactId;
        return accountInput;
    }

    recordInputForCreate(objApiName) {
        let recordInput = {
            "apiName": objApiName,
            "fields": {
            }
        }
        return recordInput;
    }

    recordInputForUpdate() {
        let recordInput = {
            "fields": {
            }
        }
        return recordInput;
    }

    enableSpinner() {
        this.showSpinner = true;
    }

    disableSpinner() {
        setTimeout(() => {
            this.showSpinner = false;
        }, 1000);
    }

    handleImportFromParent(event) {

        this.enableSpinner();
        if (event.target.checked) {
            this.formParentId = this.selectedParentAcccount[0];
            this.disableSpinner();
        }
        else {
            this.handleRefreshForm();
            this.disableSpinner();
        }
    }

    openModal() {
        this.isModalOpen = true;
    }


    openCreateNewContactModal() {
        this.enableSpinner();
        //getContactRecordTypeId( {recordTypeDevName : 'Partner_Flow_Contact'}).then((result) => {
        getContactRecordTypeId( {recordTypeDevName : 'Gyms_Partner'}).then((result) => {
            this.contactRecordTypeId = result;
            this.showCreateNewContactModal = true;
        }).catch((error) => {
            this.errorHandler(error, 'Record type');
        });
        this.disableSpinner();
    }

    closeCreateNewContactModal() {
        this.showCreateNewContactModal = false;
    }

    closeModal() {
        this.disableSpinner();
        this.handleRefreshForm();
        const myEvent = new CustomEvent('close');
        this.dispatchEvent(myEvent);
    }

    handleSaveContact(event) {
        this.contactsToCreate.push(event.detail);
    }

    handleRefreshForm() {
        this.formParentId = null;
        this.parentContactRelation = [];
        this.contactsToCreate = [];
    }

    showToastNotification(title, message, variant){
        const evt = new ShowToastEvent({ title: title, message: message, variant: variant });
        this.dispatchEvent(evt);
    }

    // multi select picklist
    handleSelectOptionList(event){
        let targetId = event.target.dataset.id;

        let evtDetail = event.detail;
        console.log(JSON.stringify(evtDetail));
        let evtDetailPayload = evtDetail.payload;
        console.log(JSON.stringify(evtDetailPayload));
        let selectedOptions = evtDetail.payload.values;
        console.log(JSON.stringify(selectedOptions));
        if(selectedOptions) {
            let optionsFormat = selectedOptions.join(';');
            let contactParentRelIndex = this.selectedParentContactRelation.findIndex(rel => rel.ContactId == targetId);
            if(contactParentRelIndex > -1) {
                this.selectedParentContactRelation[contactParentRelIndex].Type_of_Contact__c = optionsFormat;
                console.log(JSON.stringify(this.selectedParentContactRelation));
            } else {
                let newContactsIndex = this.contactsToCreate.findIndex(contact => contact.fields.LastName == targetId);
                if (newContactsIndex > -1) {
                    this.contactsToCreate[newContactsIndex].fields.Type_of_contact__c = optionsFormat;
                    console.log(JSON.stringify(this.contactsToCreate));
                }
            }
        }
    }

    legalRepresentatives = 0;
    checkLegalRepresentatives() {
        const legalRep = 'Legal Representative';
        let qtLegalReps = 0;
        for (let parentRel of this.selectedParentContactRelation) {
            if (parentRel.Type_of_Contact__c && parentRel.Type_of_Contact__c.includes(legalRep)) {
                qtLegalReps++;
            }
        }
        for (let newContact of this.contactsToCreate) {
            if(newContact.fields.Type_of_contact__c && newContact.fields.Type_of_contact__c.includes(legalRep)) {
                qtLegalReps++;
            }
        }
        this.legalRepresentatives = qtLegalReps;
        console.log('qt legal reps: '+this.legalRepresentatives)
    }
}