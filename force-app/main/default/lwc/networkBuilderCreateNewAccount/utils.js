/* 
    Implentation notes: 
    

*/

export class UtilClass {
    recordTypeIdFromObject;
    sectionWithFields = [];

    /*  Method description: 
        Param notes:  
    */
    getLayoutTemplate(error, data, recordId, sObjectName, layoutType, modeType, sectionsToIgnore ){
        console.log(JSON.stringify(data));
        if (data && data.layouts && data.records) {
            this.recordTypeIdFromObject = data.records[String(recordId)].recordTypeId;
            var sectionLst = [...data.layouts[sObjectName][this.recordTypeIdFromObject][layoutType][modeType].sections];
            sectionLst.forEach(element => {
                var fieldLst = [];
                if ( !sectionsToIgnore.includes(element.heading)) {
                    element.layoutRows.forEach(row => {
                        row.layoutItems.forEach(field => {
                            field.layoutComponents.forEach(fieldItem => {
                                if (field.editableForNew && fieldItem.apiName != null) {
                                    let aField = {};
                                    aField.isParentId = String(fieldItem.apiName).includes('ParentId');
                                    aField.devName = fieldItem.apiName;
                                    aField.isRequired = fieldItem.required;
                                    fieldLst.push(aField);
                                }
                            });
                        });
                    });
                    this.sectionWithFields.push({ value: fieldLst, key: element.heading });
                    console.log('UTILs');
                    console.log(this.sectionWithFields);
                }
            });
        }
    }

}