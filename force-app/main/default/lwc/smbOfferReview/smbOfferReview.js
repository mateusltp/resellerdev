import { LightningElement, api, wire, track } from 'lwc';
import { getRecord, getFieldValue, updateRecord } from 'lightning/uiRecordApi';
import sendToApproval from '@salesforce/apex/SmbOfferReviewController.sendToApproval';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

import TOTAL_DISCOUNT_APPROVED from '@salesforce/schema/Opportunity.SyncedQuote.Total_Discount_approved__c';
import TOTAL_DISCOUNT from '@salesforce/schema/Opportunity.SyncedQuote.Discount';
import WAIVER_APPROVED from '@salesforce/schema/Opportunity.SyncedQuote.Waiver_approved__c';
import DISCOUNT_APPROVAL_LEVEL from '@salesforce/schema/Opportunity.SyncedQuote.Discount_Approval_Level__c';
import WAIVER_APPROVAL_LEVEL from '@salesforce/schema/Opportunity.SyncedQuote.Waiver_Approval_Level__c';
import DD_APPROVED_DESCRIPTION from '@salesforce/schema/Opportunity.SyncedQuote.Deal_Desk_Approved_Conditions__c';
import QUOTE_NAME from '@salesforce/schema/Opportunity.SyncedQuote.Name';
import QUOTEID from '@salesforce/schema/Opportunity.SyncedQuoteId';
import FASTTRACK_STAGE from '@salesforce/schema/Opportunity.FastTrackStage__c';
import OPP_STAGE from '@salesforce/schema/Opportunity.StageName';
import ID_FIELD from '@salesforce/schema/Opportunity.Id';  

const fields = 
    [ TOTAL_DISCOUNT,
      TOTAL_DISCOUNT_APPROVED,
      WAIVER_APPROVED,
      DISCOUNT_APPROVAL_LEVEL,
      WAIVER_APPROVAL_LEVEL,
      QUOTE_NAME,
      QUOTEID,
      DD_APPROVED_DESCRIPTION ];

export default class SmbOfferReview extends LightningElement {
    @api recordId;
    @track opp;
    @track lstApprovedConditions;
    @track isLoading = true;

    @track rationaleCommercialConditions;
    @track rationaleDealDeskApproval;
    sfdcBaseURL;
    
    renderedCallback() {
        this.sfdcBaseURL = window.location.origin;
        
    }
    
    @wire( getRecord , { recordId: '$recordId', fields } )
    wiredOpp({ error, data }) {
        if( data ) {
            this.isLoading = false;
            this.opp = data;
            //this.lstApprovedConditions = this.getLstApprovedConditions();
            this.error = undefined;
        } else if( error ) {
            this.error = error;
            this.opp = undefined;
        }
    };    

    handlePreviousStage(){
        this.isLoading = true;
        console.log('1 ', this.isLoading)
        const fields = {};
        fields[ID_FIELD.fieldApiName] = this.recordId;
        fields[FASTTRACK_STAGE.fieldApiName] = 'Offer Creation';

        setTimeout(() => {
            console.log('2 ', this.isLoading)
            updateRecord( { fields } )
                .then(() => {
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: 'Success',
                            message: 'Changed stage',
                            variant: 'success'
                        })
                    );
                })
                .catch(error => {
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: 'Error creating record',
                            message: error.body.message,
                            variant: 'error',
                            mode: 'sticky'
                        })
                    );
                })
                .finally(() => {
                    console.log('3 ', this.isLoading)
                    this.isLoading = false;
                    console.log('4 ', this.isLoading)
                });
        }, 3000);
    }
    
    handleNextStage(){
        this.isLoading = true;
        const fields = {};
        fields[ID_FIELD.fieldApiName] = this.recordId;
        fields[OPP_STAGE.fieldApiName] = 'Proposta Enviada';
        fields[FASTTRACK_STAGE.fieldApiName] = 'Offer Sent';

        setTimeout(() => {
            updateRecord( { fields } )
                .then(() => {
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: 'Success',
                            message: 'Changed stage',
                            variant: 'success'
                        })
                    );
                })
                .catch( error => {
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: 'Error creating record',
                            message: error.body.message,
                            variant: 'error',
                            mode: 'sticky'
                        })
                    );
                })
                .finally(() => {
                    this.isLoading = false;
                });
        }, 3000);
    }

    handleSendToApproval(){
        sendToApproval({ 
                recordId : this.recordId, 
                aRationale : this.rationaleCommercialConditions, 
                aTotalDiscount : getFieldValue(this.opp, TOTAL_DISCOUNT), 
                aQuoteId : getFieldValue(this.opp, QUOTEID),
                aDealDeskRationale : this.rationaleDealDeskApproval,
                needQuoteApproval: this.checkJustifyCommercialConditions(),
                needDealDeskApproval:  false})
            .then(() => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Success',
                        message: 'Proposal sent to approval!',
                        variant: 'success'
                    })
                );
                this.rationaleDealDeskApproval = null;
                this.rationaleCommercialConditions = null;
            })
            .catch( error => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error creating record',
                        message: error.body.message,
                        variant: 'error',
                        mode: 'sticky'
                    })
                );
            });
    }
    
    getLstApprovedConditions(){
        let lstDealDeskConditions = JSON.parse( getFieldValue(this.opp, DD_APPROVED_DESCRIPTION) );

        if( !lstDealDeskConditions && lstDealDeskConditions.length ){ return []; }

        lstDealDeskConditions.forEach(condition => {
            condition.iconName = condition.isApproved ? 'utility:success' : 'utility:clear';
            condition.iconVariant = condition.isApproved ? 'success' : 'error';
        });

        return lstDealDeskConditions;
    }

    get lstApprovedConditionsIsEmpty(){
        return !this.lstApprovedConditions || !this.lstApprovedConditions.length;
    }

    get needDiscountApproval(){
        return getFieldValue(this.opp, TOTAL_DISCOUNT_APPROVED) || ( getFieldValue(this.opp, DISCOUNT_APPROVAL_LEVEL) == 0 ) ? 'utility:success' : 'utility:clear';
    }

    get getDiscountApprovalVariant(){
        return getFieldValue(this.opp, TOTAL_DISCOUNT_APPROVED) || ( getFieldValue(this.opp, DISCOUNT_APPROVAL_LEVEL) == 0 ) ? 'success' : 'error';
    }

    get needWaiverApproval(){
        return getFieldValue(this.opp, WAIVER_APPROVED) || ( getFieldValue(this.opp, WAIVER_APPROVAL_LEVEL) == 0 )  ? 'utility:success' : 'utility:clear';
    }

    get getWaiverApprovalVariant(){
        return getFieldValue(this.opp, WAIVER_APPROVED) || ( getFieldValue(this.opp, WAIVER_APPROVAL_LEVEL) == 0 ) ? 'success' : 'error';
    }

    get getQuoteName(){
        return getFieldValue(this.opp, QUOTE_NAME);
    }

    get approvalHistoryLink(){
        return this.sfdcBaseURL + '/lightning/r/Quote/' + getFieldValue(this.opp, QUOTEID) + '/related/ProcessSteps/view';
    }

    get enableJustifyCommercialConditions(){
        return ( !getFieldValue(this.opp, TOTAL_DISCOUNT_APPROVED) && ( getFieldValue(this.opp, DISCOUNT_APPROVAL_LEVEL) != null && getFieldValue(this.opp, DISCOUNT_APPROVAL_LEVEL) > 0 ) ) ||
            ( !getFieldValue(this.opp, WAIVER_APPROVED) && ( getFieldValue(this.opp, WAIVER_APPROVAL_LEVEL) != null && getFieldValue(this.opp, WAIVER_APPROVAL_LEVEL) > 0 ));
    }

    checkJustifyCommercialConditions(){
        return ( !getFieldValue(this.opp, TOTAL_DISCOUNT_APPROVED) && ( getFieldValue(this.opp, DISCOUNT_APPROVAL_LEVEL) != null && getFieldValue(this.opp, DISCOUNT_APPROVAL_LEVEL) > 0 ) ) ||
            ( !getFieldValue(this.opp, WAIVER_APPROVED) && ( getFieldValue(this.opp, WAIVER_APPROVAL_LEVEL) != null && getFieldValue(this.opp, WAIVER_APPROVAL_LEVEL) > 0 ));
    }

    get enableJustifyDealDesk(){
        let dealDeskDescription = getFieldValue(this.opp, DD_APPROVED_DESCRIPTION);

        if( !dealDeskDescription ){ return false; }

        let lstDealDeskConditions = JSON.parse( dealDeskDescription );
        
        let needApproval = false;

        lstDealDeskConditions.forEach(condition => {
            if( condition.isApproved == false ){
                needApproval = true;
            }
        });

        return needApproval;
    }

    checkJustifyDealDesk(){
        let dealDeskDescription = getFieldValue(this.opp, DD_APPROVED_DESCRIPTION);

        if( !dealDeskDescription ){ return false; }

        let lstDealDeskConditions = JSON.parse( dealDeskDescription );
        
        let needApproval = false;

        lstDealDeskConditions.forEach(condition => {
            if( condition.isApproved == false ){
                needApproval = true;
            }
        });

        return needApproval;
    }

    get disableSendToApproval(){
        return ( getFieldValue(this.opp, TOTAL_DISCOUNT_APPROVED) || ( getFieldValue(this.opp, DISCOUNT_APPROVAL_LEVEL) == 0 || !getFieldValue(this.opp, DISCOUNT_APPROVAL_LEVEL) ) ) &&
            ( getFieldValue(this.opp, WAIVER_APPROVED) || ( getFieldValue(this.opp, WAIVER_APPROVAL_LEVEL) == 0 || !getFieldValue(this.opp, WAIVER_APPROVAL_LEVEL) ) )
    }

    get disableContinue(){
        return ( !getFieldValue(this.opp, TOTAL_DISCOUNT_APPROVED) && ( getFieldValue(this.opp, DISCOUNT_APPROVAL_LEVEL) && getFieldValue(this.opp, DISCOUNT_APPROVAL_LEVEL) > 0 ) ) ||
            ( !getFieldValue(this.opp, WAIVER_APPROVED) && ( getFieldValue(this.opp, WAIVER_APPROVAL_LEVEL) && getFieldValue(this.opp, WAIVER_APPROVAL_LEVEL) > 0 ) )
    }
}