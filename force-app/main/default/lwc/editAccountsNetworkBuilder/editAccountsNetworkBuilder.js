import { LightningElement, api, wire, track } from 'lwc';
import getAccountList from '@salesforce/apex/EditAccountsNetworkBuilderController.getAccountList';
import updateAccounts from '@salesforce/apex/EditAccountsNetworkBuilderController.updateAccounts';
//import massUpdateAccounts from '@salesforce/apex/EditAccountsNetworkBuilderController.massUpdateAccounts';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import pubsub from 'c/pubsub';

const columns = [
    { label: 'Account Name', fieldName: 'Name', type: 'text', editable: true, sortable: true },
    {
        label: 'Partner Level', 
        fieldName: 'PartnerLevel', 
        type: 'picklist', 
        typeAttributes: {
            placeholder: 'Choose partner level', 
            options: [
                { label: 'Brand', value: 'Brand' },
                { label: 'Franchisee', value: 'Franchisee' },
                { label: 'Location', value: 'Location' },
                { label: 'Franchisor / Single owner', value: 'Franchisor / Single owner' }
            ], // list of all picklist options 
            value: { fieldName: 'PartnerLevel' }, // default value for picklist 
            context: { fieldName: 'Id' } // binding account Id with context variable to be returned back
        }
    },
    {
        label: 'Legal Representative', 
        fieldName: 'LegalRepresentative', 
        type: 'text',
        editable: true
    },
    /*
    {
        label: 'Legal Representative', 
        fieldName: 'Contacts', 
        type: 'contact', 
        typeAttributes: {
            contacts: {fieldName: 'Contacts'}
        }
    }
    */

];

export default class EditAccountsNetworkBuilder extends LightningElement {
    @api selectedAccountsId = [];
    @track error;
    @track data;
    
    columns = columns;
    spinner = false;
    partnerLevelValues = [];
    draftValues = [] ;
    dtSelectedRows = [];
    supressBottomBar = true;
    
    auxDraftValues = [];

    connectedCallback() {
        this.getAccounts();
        //this.callSubscriber();
    }

    handleSave( event ) {
        const updatedFields = event.detail.draftValues;
        console.log('on handle save');
        console.log(updatedFields);

        this.showSpinner();
        updateAccounts( { data: updatedFields } )
            .then( result => {
                this.draftValues = [];
                this.getAccounts();
                this.hideSpinner();
                this.showToastNotification('Success', 'Account(s) updated', 'success');
            }).catch( error => {
                this.hideSpinner();
                if (error.status == 500) {                     
                    this.showToastNotification('Error updating records', error.body.message, 'error');
                }
        });
        
    }

    closeModal() {
        const myEvent = new CustomEvent('close');
        this.dispatchEvent(myEvent);
    }

    showSpinner() {
        this.spinner = true;
    }

    hideSpinner() {
        this.spinner = false;
    }

    getAccounts() {
        this.showSpinner();        
        getAccountList({ accIds: this.selectedAccountsId})
            .then((result) => {
                this.data = result;
                this.hideSpinner();
            })
            .catch((error) => {
                this.error = error;
                this.hideSpinner();
            });
    }

    showToastNotification(title, message, variant){
        const evt = new ShowToastEvent({ title: title, message: message, variant: variant });
        this.dispatchEvent(evt);
    }

    //listener handler to get the context and data
    //updates datatable
    picklistChanged(event) {
        event.stopPropagation();
        let dataRecieved = event.detail.data;
        let updatedItem = { Id: dataRecieved.context, PartnerLevel: dataRecieved.value };
        this.updateDraftValues(updatedItem);
        //this.updateDataValues(updatedItem);
    }

     updateDraftValues(updateItem) {
        console.log();
        let draftValueChanged = false;
        let copyDraftValues = [...this.template.querySelector('c-custom-data-table').draftValues];
        //store changed value to do operations on save. This will enable inline editing & show standard cancel & save button
       
        copyDraftValues.forEach(item => {
            console.log(item);
            if (item.Id === updateItem.Id) {
                for (let field in updateItem) {
                    item[field] = updateItem[field];
                }
                draftValueChanged = true;
            }
        });

        let filtered = this.draftValues.filter(dValue => dValue.Id == updateItem.Id);
        if (filtered.length == 0) {
            this.draftValues = [...this.draftValues, updateItem];
        } else {
            let v = filtered[0];
            for (let field in updateItem) {
                v[field] = updateItem[field];
                this.draftValues = [...this.draftValues];
            }
        }
    }

    /*
    updateDataValues(updateItem) {
        let copyData = [... this.data];
        copyData.forEach(item => {
            if (item.Id === updateItem.Id) {
                for (let field in updateItem) {
                    item[field] = updateItem[field];
                }
            }
        });

        //write changes back to original data
        this.data = [...copyData];
    }
    */

    handleRowSelection(event) {
        console.log('handle row selection');
        let selectedRows = event.detail.selectedRows;
        this.dtSelectedRows = [...selectedRows];
        this.publishHandlerRowSelection();
    }

    publishHandlerRowSelection(){
        pubsub.publish('rowSelection', this.dtSelectedRows);
    }

    handleMassUpdate(event){
        const data = event.detail;
    }

    callSubscriber(){
       //this.callNetworkMassUpdate();
    }

    /*
    callNetworkMassUpdate() {
        pubsub.subscribe('networkMassUpdate', (data)=>{
            let selectedAccountIdLst = [];

            this.dtSelectedRows.forEach(row => {
                selectedAccountIdLst.push(row.Id);
            });

            this.showSpinner();
            
            massUpdateAccounts({ accountIdLst: selectedAccountIdLst, accountWrapper: data })
            .then( result => {
                this.getAccounts();

                this.hideSpinner();
                
                this.showToastNotification('Success', 'Account(s) updated', 'success');
            }).catch( error => {
                this.hideSpinner();
                
                if (error.status == 500) {                     
                    this.showToastNotification('Error updating records', error.body.message, 'error');
                }
            });
        })
    }
    */

    handleCancel() {
        this.getAccounts();
    }
}