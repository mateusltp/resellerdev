import { api, LightningElement } from 'lwc';
import ACCOUNT_ID from '@salesforce/schema/Account_Request__c.AccountId__c';
import lda from '@salesforce/resourceUrl/LDA';

export default class CustomSearch extends LightningElement {

    fields = [ACCOUNT_ID]

    handleSuccess(){

    }

    handleAccountSelection(event){
        console.log("the selected record id is"+event.detail);
    }

}