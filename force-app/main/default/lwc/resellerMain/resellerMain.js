import { LightningElement, track} from 'lwc';

export default class ResellerMain extends LightningElement {

    @track typeConsult ='';
        
    label = {
        LABEL_COMMUNITY_WELCOME,
        LABEL_COMMUNITY_WELCOME_PARTNERS,
        LABEL_COMMUNITY_WELCOME_SUBTITLE,
        LABEL_QUERY_COMPANY,
        LABEL_CHOOSE_QUERY,
        LABEL_SINGLE_COMPANY,
        LABEL_MULTIPLE_COMPANY,
        LABEL_GET_STARTED
    }

    handleShowModal(){
        const modal = this.template.querySelector("c-reseller-flow");
        modal.start();
    }
// voltar a versao do flow antigo apenas alterar o step e fazer a parte de pegar a account reseller
    handleTypeConsult(event){
        this.typeConsult = event.target.value;
        console.log("select type consult: " + this.typeConsult);
    }

    callFlow(){
        console.log("type consult: " + this.typeConsult);
        if(this.typeConsult == "singleCompany"){
            const modal = this.template.querySelector("c-reseller-flow");
            modal.start();
        }else if(this.typeConsult == "multipleCompany"){
            const modal = this.template.querySelector("c-reseller-flow-multiple");
            modal.start();
        }
    }

    get isMassivePermission(){
        console.log("Entrou metodo massivo " + hasMassivePermission);
        return hasMassivePermission;
    }

    

}

import hasMassivePermission from '@salesforce/customPermission/Account_Massive_Request';

import LABEL_COMMUNITY_WELCOME from '@salesforce/label/c.Community_welcome';
import LABEL_COMMUNITY_WELCOME_PARTNERS from '@salesforce/label/c.Community_Welcome_Partners';
import LABEL_COMMUNITY_WELCOME_SUBTITLE from '@salesforce/label/c.Community_Welcome_Subtitle';
import LABEL_QUERY_COMPANY from '@salesforce/label/c.Community_Query_Company';
import LABEL_CHOOSE_QUERY from '@salesforce/label/c.Community_Choose_query';
import LABEL_SINGLE_COMPANY from '@salesforce/label/c.Community_Single_company';
import LABEL_MULTIPLE_COMPANY from '@salesforce/label/c.Community_Multiple_company';
import LABEL_GET_STARTED from '@salesforce/label/c.Community_Get_started';