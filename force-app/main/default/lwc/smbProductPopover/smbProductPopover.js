import { LightningElement, api, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class SmbProductPopover extends LightningElement {
    @track privateRecordId;
    @track privateWaivers = [];

    @api fields;
    fieldsList = ['Quantity', 'Total_List_Price__c', 'UnitPrice', 'Subtotal'];   

    @api
    get recordId(){
        return this.privateRecordId;
    }

    set recordId(value) {
        this.privateRecordId = value;
    }

    @api
    get waivers() {
        return this.privateWaivers;
    }
    set waivers(value) {
        if(value) {
            this.privateWaivers = [];
            value.forEach(waiver => {
                this.privateWaivers.push({label: waiver.Percentage__c +'% to ' + waiver.End_Date_Text__c});
            });
        }
    }

    get hasWaivers() {
        return this.privateWaivers.length > 0;
    }

    showToast(title, message, variant) {
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant,
        });
        this.dispatchEvent(evt);
    }

}