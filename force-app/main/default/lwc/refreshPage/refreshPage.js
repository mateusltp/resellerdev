import { LightningElement } from 'lwc';

export default class RefreshPage extends LightningElement {


    connectedCallback(){
        window.location.reload();
    }
}