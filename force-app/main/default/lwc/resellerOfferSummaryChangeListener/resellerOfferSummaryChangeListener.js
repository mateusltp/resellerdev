import { LightningElement, wire } from 'lwc';

import { subscribe, unsubscribe, APPLICATION_SCOPE, MessageContext } from 'lightning/messageService';

import changeListener from '@salesforce/messageChannel/ResellerRefreshRevenueMetricsChannel__c';

import { FlowNavigationNextEvent } from 'lightning/flowSupport';

export default class ResellerOfferSummaryChangeListener extends LightningElement {

    subscription = null;

    @wire(MessageContext)
    messageContext;

    connectedCallback() {
        this.subscribeToMessageChannel();
    }

    subscribeToMessageChannel() {
        if (!this.subscription) {
            this.subscription = subscribe(
                this.messageContext,
                changeListener,
                (message) => this.handleMessage(message),
                { scope: APPLICATION_SCOPE }
            );
        }
    }

    handleMessage(message) {
        console.log('message: ' + JSON.stringify(message));
        this.dispatchEvent(new FlowNavigationNextEvent());
    }

    disconnectedCallback() {
        this.unsubscribeToMessageChannel();
    }

    unsubscribeToMessageChannel() {
        unsubscribe(this.subscription);
        this.subscription = null;
    }
}