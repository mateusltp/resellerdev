import { LightningElement ,wire,track} from 'lwc';
import getCases from '@salesforce/apex/GetCases.getCases';

export default class ListCaseRegion extends LightningElement {
    @track columns = [
        {
            label: 'Case Number',
            fieldName: 'caseUrl',
            // type: 'text',
            type: 'url',
            typeAttributes: {label: { fieldName: 'CaseNumber' }, 
            // target: '_blank'
        },
            sortable: true
        },
        {
            label: 'Company Name',
            fieldName: 'CompanyName__c',
            type: 'text',
            sortable: true
        },
        {
            label: 'Country',
            fieldName: 'Country__c',
            type: 'text',
            sortable: true
        },
        {
            label: 'Owner Name',
            fieldName: 'ownerUrl',
            type: 'url',
            typeAttributes: {label: { fieldName: 'ownerName' }, 
            },
            sortable: true
        },
        {
            label: 'Opportunity Record Type',
            fieldName: 'Opportunity_Record_Type__c',
            type: 'text',
            sortable: true
        },
        {
            label: 'Contract Agreement',
            fieldName: 'contractUrl',
            type: 'url',
            typeAttributes: {label: { fieldName: 'contractName' }, 
            },
            sortable: true
        },
        {
            label: 'Status',
            fieldName: 'Status',
            type: 'text',
            sortable: true
        },
        {
            label: 'CreatedBy',
            fieldName: 'CreatedBy__c',
            type: 'text',
            sortable: true
        },
        {
            label: 'Date/Time Opened',
            fieldName: 'CreatedDate',
            type: 'date', 
            typeAttributes: {
              month: 'numeric',
              day: 'numeric',
              year: 'numeric',
              hour: '2-digit',
              minute: '2-digit',
              hour12: true
            //   timeZoneName:'short'
            },
            sortable: true
        }

    ];

    @track error;
    @track cases = [];


    @wire(getCases)
    wiredCases(result) {
        const { data, error } = result;
        if(data) {
            let caseUrl;
            let ownerUrl;
            let ownerName
            let contractUrl;
            let contractName;
            console.log(data);
            this.cases = data.map(row => { 
                caseUrl = `/${row.Id}`;
                ownerUrl = `/${row.OwnerId}`;
                ownerName = row.Owner.Name;
                contractUrl = `/${row.ContractAgreement__c}`;
                contractName = row.ContractAgreement__r.Name;
                console.log(contractName);
                console.log({...row , caseUrl, ownerUrl, contractUrl});
                return {...row , caseUrl, ownerUrl, ownerName, contractUrl, contractName};
            })
            this.error = null;
        }
        if(error) {
            this.error = error;
            this.cases = [];
        }
    }
}