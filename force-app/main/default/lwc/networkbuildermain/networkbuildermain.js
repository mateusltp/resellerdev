import { LightningElement, track, wire, api } from 'lwc';
import { refreshApex } from '@salesforce/apex';
import fetchAccounts from '@salesforce/apex/NetworkBuilderMainController.buildNetwork';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { getRecord } from 'lightning/uiRecordApi';
import PARENT_ID_FIELD from '@salesforce/schema/Account.ParentId';
import ACCOUNT_ID_FIELD from '@salesforce/schema/Opportunity.AccountId';

const fields = [PARENT_ID_FIELD];

export default class Networkbuildermain extends LightningElement {
    @api recordId;
    @api objectApiName;
    @track gridColumns = this.getGridCol();
    @track gridData;
    @track networkTitle = '';
    @track showEditAccModal = false;
    @track selectedAccountAction;
    @track showRemoveModal = false;
    @track showAddExistingAccModal = false;
    @track showCreateNewAccountModal = false;
    @track showSpinner = false;
    @track showEditSingleAccountModal = false;
    @track showEditAllModal = false;
    @track showSendForApprovalModal = false;
    @track gridExpandedRows = [];

    dataResult;
    selectedRowIdLst        = [];
    searchFilteredRowsId    = [];
    lastSelectedAccountsIDs = [];

    accountLst;
    gridDataBackUp;
    recId;
    hasExclusivityAccounts;

    globalChildIdToParentIdMap = new Map();

    SEARCH_SELECT_CONTEXT   = Symbol('SEARCH');
    DEFAULT_SELECT_CONTEXT  = Symbol('DEFAULT');
    
    selectContext;
    rowContracted = false;
    lastToggleRow = null;
    masterAccountId = null;

    @wire(getRecord, { recordId: '$recordId', fields: ACCOUNT_ID_FIELD }) opportunity(result) {
        if (result.data) {
            this.recId = result.data.fields[ACCOUNT_ID_FIELD.fieldApiName].value;
        } else {
            this.recId = this.recordId;
        }
    }

    @wire(fetchAccounts, { recordId: '$recId', recordTypeDevName: 'Partner_Flow_Account' }) accounts(result) {
        this.dataResult = result;
        if (result.data) {
            let accountTree = [...result.data.tree];
            this.accountLst = [...result.data.accounts];
            this.hasExclusivityAccounts = result.data.hasExclusivityAccounts;
            let tempData = JSON.parse(JSON.stringify(accountTree).split('items').join('_children'));
            this.networkTitle = tempData[0].Name + "'s Network";
            this.gridData = tempData;
            this.gridDataBackUp = this.gridData;
            this.populateGlobalChildIdToParentIdMap(tempData);
            this.setTreeGridExpandedRow();
        } else if (result.error) {
            if (Array.isArray(result.error.body))
                console.log('Error is ' + result.error.body.map(e => e.message).join(', '));
            else if (typeof result.error.body.message === 'string')
                console.log('Error is ' + result.error.body.message);
        }
    }

    get accountsSelectNumber() {
        return this.selectedRowIdLst.length;
    }

    setTreeGridExpandedRow() {
        let expandedRows = [];

        this.accountLst.forEach(acc => {
            if (this.globalChildIdToParentIdMap.get(acc.id) == undefined) {
                this.setMasterAccountId(acc.id);
                expandedRows.push(acc.id);
            }
        });

        this.setGridExpandedRows(expandedRows);
    }

    handleSearchAction(evt) {
        var filteredRows;
        let rows = [... this.accountLst],
            term = evt.target.value,
            results = rows, regex;

        let filteredRowsIds = [];

        if (term.length < 1) {
            this.setTreeGridWithGlobalSelectedRowsId();
            this.gridData = this.gridDataBackUp;
            this.setSelectContext(this.DEFAULT_SELECT_CONTEXT);
            return;
        }

        try {
            this.setSelectContext(this.SEARCH_SELECT_CONTEXT);
            regex = new RegExp(term, "i");
            results = rows.filter(row => regex.test(row.Name) || regex.test(row.PartnerLevel) || regex.test(row.LegalRepresentativeName));
            filteredRows = results;

            filteredRows.forEach(row => filteredRowsIds.push(row.id));
            this.setSearchFilteredRowsId(filteredRowsIds);

            let auxSelectedFilteredRows = filteredRowsIds.filter(id => this.selectedRowIdLst.includes(id));

            if (filteredRows.length > 0) {
                this.setTreeGridSelectedRowsForSerachContext(auxSelectedFilteredRows);
                this.gridData = filteredRows;
            } else {
                this.gridData = [];
            }
        } catch (e) {

        }

    }

    setTreeGridSelectedRowsForSerachContext(selectedFilteredRows) {
        this.template.querySelector('lightning-tree-grid').selectedRows = selectedFilteredRows;
    }

    getGridCol() {
        return [
            {
                label: 'Account name',
                fieldName: 'NameUrl',
                type: 'url',
                typeAttributes: {
                    label: { fieldName: 'Name' },
                    target: '_self'
                }
            },
            { type: 'text', fieldName: 'PartnerLevel', label: 'Partner Level' },

            {
                label: 'Legal Representative',
                fieldName: 'LegalRepresentativeUrl',
                type: 'url',
                typeAttributes: {
                    label: { fieldName: 'LegalRepresentativeName' },
                    target: '_blank'
                }
            },
            {
                type: "action",
                typeAttributes: {
                    rowActions: [
                        { name: "Edit", label: "Edit" },
                        { name: "Remove from the network", label: "Remove from the network" }
                    ]
                }
            }
        ];
    }

    setSelectedRows() {
        if (this.getSelectContext() == this.SEARCH_SELECT_CONTEXT) {
            this.handleSearchSelectContext();
        } else {
            this.handleDefaultSelectContext();
        }

        this.setLastSelectedAccountsIDs();
    }

    setLastSelectedAccountsIDs() {
        let selectedRows = this.template.querySelector('lightning-tree-grid').getSelectedRows();
        
        if (selectedRows.length == 0) {
            this.lastSelectedAccountsIDs = [];
        } else {
            let selectedRowsIds = [];
            selectedRows.forEach(row => {
                selectedRowsIds.push(row.id);
            });

            this.lastSelectedAccountsIDs = selectedRowsIds;
        }
    }

    handleSearchSelectContext() {
        let selectRows = this.template.querySelector('lightning-tree-grid').getSelectedRows();
        
        let idMap = new Map();
        let currentSelectedRowsId   = [];
        let currentDeselectedRowsId = [];

        // Puts all selected rows in idMap
        this.selectedRowIdLst.forEach(id => idMap.set(id, id));

        selectRows.forEach(row => {
            currentSelectedRowsId.push(row.id);
            idMap.set(row.id, row.id)
        });

        // Removes rows from idMap
        this.selectedRowIdLst.forEach(id => {
            if (!currentSelectedRowsId.includes(id) && this.getSearchFilteredRowsId().includes(id)) {
                currentDeselectedRowsId.push(id);
            }
        });

        currentDeselectedRowsId.forEach(id => idMap.delete(id));

        this.selectedRowIdLst = Array.from(idMap.keys());
    }

    handleDefaultSelectContext() {
        let selectedRows = this.template.querySelector('lightning-tree-grid').getSelectedRows();
        let currentExpandedRows = Array.from(this.template.querySelector('lightning-tree-grid').getCurrentExpandedRows());
        
        this.showSpinner = true;

        if (this.accountLst.length > 1) {
            if (this.rowWasContracted()) {
                if (currentExpandedRows.length == 0) {
                    this.handleSelectionForContractedTree(selectedRows, currentExpandedRows);
                } else {
                    this.handleSelectionForNetwork(selectedRows, currentExpandedRows);
                }
                this.setRowContracted(false);
            } else {
                this.handleSelectionForNetwork(selectedRows, currentExpandedRows);                
            }
        } else {
            this.handleSelectionForSingleAccount(selectedRows);
        }

        this.showSpinner = false;
    }

    handleSelectionForContractedTree(selectedRows, currentExpandedRows) {
        this.handleSingleSelectionOrDeselection(selectedRows, currentExpandedRows);
    }

    handleSelectionForSingleAccount(selectedRows) {
        if (this.accountLst.length == 1) {
            let selectedRowsIds = [];

            if (selectedRows.length == 0) {
                this.selectedRowIdLst = [];
            } else {
                selectedRows.forEach(row => {
                    selectedRowsIds.push(row.id);
                });
    
                this.selectedRowIdLst = selectedRowsIds;
            }
        }
    }

    handleSelectionForNetwork(selectedRows, currentExpandedRows) {
        let newIdRowsSelected   = [];
        
        selectedRows.forEach(row => {
            if (!this.selectedRowIdLst.includes(row.id)) {
                newIdRowsSelected.push(row.id);
            }
        });

        // Select all if detect more than one account selected at once
        if (newIdRowsSelected.length > 1) {
            this.selectAllRows();
        }
        // Deselect all
        else if (this.selectedRowIdLst.length == this.accountLst.length && selectedRows.length == 0) {
            this.selectedRowIdLst = [];
        }
        // Single selection or deselection
        else {
            this.handleSingleSelectionOrDeselection(selectedRows, currentExpandedRows);
        }
    }
    
    handleSingleSelectionOrDeselection(selectedRows, currentExpandedRows) {
        let currentSelectedRowIds = [];
        let currentDeselectedAccountIds = [];
        let idMap = new Map();
    
        selectedRows.forEach(row => {
            idMap.set(row.id, row.id);
            currentSelectedRowIds.push(row.id);
        });
    
        this.selectedRowIdLst.forEach(id => {
            idMap.set(id, id);
            let parentAccountId = this.globalChildIdToParentIdMap.get(id);
            let isMasterAccountId = false;
    
            if (parentAccountId == undefined) {
                isMasterAccountId = true;
            }
    
            if (isMasterAccountId && !currentSelectedRowIds.includes(id)) {
                currentDeselectedAccountIds.push(id);
            }
            else if (!currentSelectedRowIds.includes(id) && currentExpandedRows.includes(parentAccountId)) {
                currentDeselectedAccountIds.push(id);
            }
        });         
    
        currentDeselectedAccountIds.forEach(id => idMap.delete(id));
    
        if (currentDeselectedAccountIds.length > 1) {
            this.selectedRowIdLst = [];
        } else {
            this.selectedRowIdLst = Array.from(idMap.keys());
        }
    }

    selectAllRows() {
        let selectedRowsIds = [];
            
        this.accountLst.forEach(acc => selectedRowsIds.push(acc.id) );
        this.selectedRowIdLst = selectedRowsIds;
    }

    onRowToggle(event) {
        this.setLastToggleRow(event.detail.row);

        if (this.getLastToggleRow().id == this.getMasterAccountId()) {
            console.log('Master account');
            this.onMasterAccountToggle();
            return;
        }

        if (event.detail.isExpanded) {
            this.setRowContracted(false);
            this.setTreeGridWithGlobalSelectedRowsId();
        } else {
            this.setRowContracted(true);
        }
    }

    onMasterAccountToggle() {
        console.log('onMasterAccountToggle');
        let currentExpandedRows = Array.from(this.template.querySelector('lightning-tree-grid').getCurrentExpandedRows());
        
        if (!currentExpandedRows.includes(this.getMasterAccountId())) {
            currentExpandedRows.push(this.getMasterAccountId());
        }

        this.setGridExpandedRows(currentExpandedRows);

        this.showToastNotification('This parent account can\'t be collapsed', '', 'info');
    }

    handleEdit() {
        if (this.selectedRowIdLst.length > 0) {
            this.openEditAccModal();
        } else {
            this.showToastNotification('No accounts selected', 'Please select at least one account', 'warning');
        }
    }


    handleCreateNewAccount() {
        if (this.selectedRowIdLst.length == 1) {
            this.openCreateNewAccountModal();
        } else {
            this.showToastNotification('Please select an account to continue', '', 'error');
        }
    }

    handleAddExistingAccount() {
        if (this.selectedRowIdLst.length == 1) {
            this.openAddExistingAccModal();
        } else {
            this.showToastNotification('Please select an account to continue', '', 'error');
        }
    }

    handleEditAll() {
        if (this.selectedRowIdLst.length > 0) {
            this.openEditAllModal();
        } else {
            this.showToastNotification('No accounts selected', 'Please select at least one account', 'warning');
        }
    }

    handleApproval() {
        if (this.selectedRowIdLst.length > 0) {
            this.openSendForApprovalModal();
        } else {
            this.showToastNotification('No accounts selected', 'Please select at least one account', 'warning');
        }
    }

    showToastNotification(title, message, variant) {
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant,
        });
        this.dispatchEvent(evt);
    }

    openCreateNewAccountModal() {
        this.showCreateNewAccountModal = true;
    }


    closeCreateNewAccountModal() {
        this.showCreateNewAccountModal = false;
        this.refreshTree();
    }

    openEditAccModal() {
        console.log('open modal');
        console.log(this.selectedRows);
        this.showEditAccModal = true;
    }

    closeEditAccModal() {
        this.showEditAccModal = false;
        this.showAddExistingAccModal = false;
        this.refreshTree();
    }

    openAddExistingAccModal() {
        console.log('open existing modal');
        this.showAddExistingAccModal = true;
    }

    openEditAllModal() {
        console.log('open modal');
        console.log(this.selectedRows);
        this.showEditAllModal = true;
    }

    openSendForApprovalModal() {
        console.log('open approval modal');
        this.showSendForApprovalModal = true;
    }

    closeSendForApprovalModal() {
        this.showSendForApprovalModal = false;
    }

    closeEditAllModal() {
        this.showEditAllModal = false;
        this.refreshTree();
    }


    closeRemoveModal() {
        this.showRemoveModal = false;
        this.refreshTree();
    }

    closeAddExistingAccModal() {
        this.showAddExistingAccModal = false;
        this.refreshTree();
    }

    refreshTree() {
        this.showSpinner = true;
        refreshApex(this.dataResult);
        this.disableSpinner();
    }

    handleRowAction(event) {
        const actionName = event.detail.action.name;
        const row = event.detail.row;
        this.selectedAccountAction = row.id;
        this.selectedRowIdLst = [];
        switch (actionName) {
            case 'Edit':
                this.showEditSingleAccountModal = true;
                break;
            case 'Remove from the network':
                this.showRemoveModal = true;
                break;
        }
    }

    closeEditSingleAccountModal() {
        this.showEditSingleAccountModal = false;
        this.refreshTree();
    }

    disableSpinner() {
        setTimeout(() => {
            this.showSpinner = false;
        }, 500);
    }

    populateGlobalChildIdToParentIdMap(data) {
        data.forEach(acc => this.getChildrenForAccount(acc));
    }
    
    getChildrenForAccount(acc) {
        if (acc.hasOwnProperty('_children')) {
            acc._children.forEach(child => {
                this.globalChildIdToParentIdMap.set(child.id, acc.id)
                this.getChildrenForAccount(child);
            });
        } else {
            return;
        }
    }

    setRowContracted(value) {
        console.log('setRowContracted as: ' + value);
        this.rowContracted = value;
    }

    rowWasContracted() {
        return this.rowContracted;
    }

    setTreeGridWithGlobalSelectedRowsId() {
        let tempSelectedRowIdLst = this.selectedRowIdLst;
        this.template.querySelector('lightning-tree-grid').selectedRows = tempSelectedRowIdLst;
    }

    setSelectContext(value) {
        this.selectContext = value;
    }

    getSelectContext() {
        return this.selectContext;
    }

    setSearchFilteredRowsId(rows) {
        this.searchFilteredRowsId = rows;
    }

    getSearchFilteredRowsId() {
        return this.searchFilteredRowsId;
    }

    setLastToggleRow(row) {
        this.lastToggleRow = row;
    }

    getLastToggleRow() {
        return this.lastToggleRow;
    }

    setMasterAccountId(id) {
        this.masterAccountId = id;
    }

    getMasterAccountId() {
        return this.masterAccountId;
    }

    setGridExpandedRows(expandedRows) {
        this.gridExpandedRows = expandedRows;
    }
}