import { LightningElement, track, api } from 'lwc';
import createOpsFormForContractAccounts from '@salesforce/apex/PartnerSetupController.createOpsFormForContractAccounts';
import { reduceErrors } from 'c/ldsUtils';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

const CREATING_SETUP_FORM = 'Creating Setup Forms';


export default class PartnerSetupButton extends LightningElement {

    @api recordId;
    @track showSpinner;
    spinnerMessage;

    get spinnerMessage(){
        return this.spinnerMessage;
    }

    
    connectedCallback(){
        this.showSpinner = true;
        this.spinnerMessage = CREATING_SETUP_FORM;     
        createOpsFormForContractAccounts({opportunityId : this.recordId}).then(result => {
            this.showSpinner = false;
        })
        .catch(error => {
            this.dispatchToast('Error', 'error', reduceErrors(error)[0]);
            this.showSpinner = false;
        });
    }

    dispatchToast(title, variant, message) {
        let successtEvnt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant
        });
        this.dispatchEvent(successtEvnt);
    }
}