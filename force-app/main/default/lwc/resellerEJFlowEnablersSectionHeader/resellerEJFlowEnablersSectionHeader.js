import { LightningElement, api } from 'lwc';

export default class ResellerEJFlowEnablersSectionHeader extends LightningElement {

    @api enablersIconName = '';
    @api enablersHeaderTitle = '';
    @api enablersSubtitleMessage = '';

}