import { LightningElement, api, track, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import createProdAssignments from '@salesforce/apex/ProductAssignmentListController.createProdAssignments';

export default class ProdAssignmentAddAccount extends LightningElement {
    @api prodRecordId;
    @api accountData;
    @api helperData = null;
    @track accountLst   = [];
    

    selectedAccounts    = [];
    disableButton       = true;
    spinner             = false;

    connectedCallback() {
        console.log('ProdAssignmentAddAccount connectedCallback');
        console.log(JSON.stringify(this.accountData));
    }

    closeModal() {
        const myEvent = new CustomEvent('close');
        this.dispatchEvent(myEvent);
    }

    handleSelectAccounts(event) {
        console.log('handleSelectAccounts');
        this.selectedAccounts = event.detail;
        console.log(this.selectedAccounts);
        this.validatePartnerLevelFromSelectedRows();
    }

    validatePartnerLevelFromSelectedRows() {
        if (this.selectedAccounts.length == 0) {
            this.disableButton = true;
            return;
        } 

        if (!this.partnerLevelIsValid()) {
            this.showToastNotification('Warning', 'Please, select only Locations', 'warning');
            this.disableButton = true;
        } else {
            this.disableButton = false;
        }
    }

    partnerLevelIsValid() {
        let isValid = false;

        this.selectedAccounts.every(function(element, index){
            if (element.partnerLevel != 'Location') {
                isValid = false;
                return;
            } else {
                isValid = true;
            }
        });

        return isValid;
    }

    showToastNotification(title, message, variant) {
        const evt = new ShowToastEvent({title: title, message: message, variant: variant});
        this.dispatchEvent(evt);
    }

    handleDoneBtn() {
        this.showSpinner();
        let accountIdLst = [];
        
        this.selectedAccounts.forEach(acc => accountIdLst.push(acc.id));
        
        createProdAssignments({ recordId: this.prodRecordId, accountIdLst: accountIdLst })
            .then((result) => {
                const createdPasEvent = new CustomEvent("createdpas");
                this.dispatchEvent(createdPasEvent)
                this.hideSpinner();
            })
            .catch((error) => {
                this.showToastNotification('Error', 'Error creating records', 'error');
                this.hideSpinner();
            });
    }

    showSpinner() {this.spinner = true}

    hideSpinner() {this.spinner = false}
}