import { LightningElement, api, track } from 'lwc';

export default class GenericRadioGroup extends LightningElement {

    @api label;

    radioGroupOptions;

    @api required = false;

    @api disabled = false;

    @api value;

    @api fieldRequiredMessage = 'This field is required';

    @track showError = false;

    renderedCallback() {
        if((this.value != undefined && this.value != null)) {
            this.selectRadioButton(this.value);
        }
    }

    @api
    get options() {
        return this.radioGroupOptions;
    }
    set options(options) {
        if(options) {
            this.radioGroupOptions = [];
            let radioGroupValues = new Set();
            options.forEach(option => {
                let clonedOption = {...option};
                if(radioGroupValues.has(clonedOption.value)) {
                    throw 'Duplicated values in the radio options are not allowed';
                }
                else {
                    radioGroupValues.add(clonedOption.value);
                }
                this.radioGroupOptions.push(clonedOption);
            });
        }
    }

    deselectAllRadioButtons() {
        let radioButtons = this.template.querySelectorAll('input');
        radioButtons.forEach(radioButton => {
            radioButton.checked = false;
        });
    }

    selectRadioButton(id) {
        if(this.showError) {
            this.hideErrorMessage();
        }
        let radioButton = this.template.querySelector('input[data-id="' + id + '"]'); 
        radioButton.checked = true;
        this.value = radioButton.value;
        this.dispatchChange(this.value);
    }

    handleRadioOptionClick(event) {
        this.deselectAllRadioButtons();
        this.selectRadioButton(event.currentTarget.dataset.id);
    }

    dispatchChange(value){
        const change = new CustomEvent('change', {
          detail: {value: value}
        });
        this.dispatchEvent(change);
    }

    @api
    reportValidity() {
        if(!this.checkValidity()) {
            this.showErrorMessage();
        }
    }

    @api
    checkValidity() {
        if(this.required && (this.value == undefined || this.value == null))
            return false;
        else
            return true;
    }

    hideErrorMessage() {
        this.showError = false;
        let fieldset = this.template.querySelector('fieldset');
        fieldset.classList.remove('slds-has-error');
    }

    showErrorMessage() {
        this.showError = true;
        let fieldset = this.template.querySelector('fieldset');
        fieldset.classList.add('slds-has-error');
    }

}