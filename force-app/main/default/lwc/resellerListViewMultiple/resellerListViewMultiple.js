/**
 * @description       : 
 * @author            : Mateus Augusto - GFT (moes@gft.com)
 * @group             : 
 * @last modified on  : 03-04-2022
 * @last modified by  : Mateus Augusto - GFT (moes@gft.com)
**/
import { LightningElement, wire, api, track } from 'lwc';
import { refreshApex } from '@salesforce/apex';
import getSearchValue from '@salesforce/apex/ResellerListViewMultipleController.getSearchValue';
import getPicklistValuesByApiName from '@salesforce/apex/ResellerListViewMultipleController.getPicklistValuesByApiName';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import updateAccountRequests from '@salesforce/apex/ResellerListViewMultipleController.updateAccountRequests';
import { getRecordNotifyChange } from 'lightning/uiRecordApi';
import downloadjs from '@salesforce/resourceUrl/downloadjs';
import { loadScript } from 'lightning/platformResourceLoader';
import runBatchesAgain from '@salesforce/apex/MassiveAccReqLoad.runBatchesAgain';
import { NavigationMixin } from 'lightning/navigation';
import { getRecord } from 'lightning/uiRecordApi';

let COLS = [
    { label: LABEL_COMMUNITY_COMPANY_NAME, fieldName: 'Name',  editable: true },
    { label: LABEL_COMMUNITY_WEBSITE, columnDefs:{className: "text-center"}, fieldName: 'Website__c', cellAttributes: { alignment: 'center'}, editable: true },
    //{ label: LABEL_COMMUNITY_UNIQUE_IDENTIFIER_TYPE, fieldName: 'Unique_Identifier_Type__c', cellAttributes: { alignment: 'center' }, editable: true },
    //{ label: LABEL_COMMUNITY_UNIQUE_IDENTIFIER, fieldName: 'Unique_Identifier__c', cellAttributes: { alignment: 'center' }, editable: true },
    { label: LABEL_COMMUNITY_PARTNER_MODEL, fieldName: 'Partner_Model__c', cellAttributes: { alignment: 'center' }, editable: true },
    { label: LABEL_COMMUNITY_EMPLOYEES_OFFER, fieldName: 'Total_number_of_employees__c', cellAttributes: { alignment: 'center' }, editable: true },
    { label: LABEL_COMMUNITY_COUNTRY, fieldName: 'Billing_Country__c', cellAttributes: { alignment: 'center' }, editable: true },
    //{ label: LABEL_COMMUNITY_STATUS, fieldName: 'Status__c', cellAttributes: { alignment: 'center' }, editable: true },
    { label: LABEL_COMMUNITY_ERROR_MESSAGE, fieldName: 'Error_message__c', cellAttributes: { alignment: 'center' }, editable: true }
];

let COLS_BR = [
    { label: LABEL_COMMUNITY_COMPANY_NAME, fieldName: 'Name',  editable: true },
    { label: LABEL_COMMUNITY_UNIQUE_IDENTIFIER_TYPE, fieldName: 'Unique_Identifier_Type__c', cellAttributes: { alignment: 'center' }, editable: true },
    { label: LABEL_COMMUNITY_UNIQUE_IDENTIFIER, fieldName: 'Unique_Identifier__c', cellAttributes: { alignment: 'center' }, editable: true },
    { label: LABEL_COMMUNITY_PARTNER_MODEL, fieldName: 'Partner_Model__c', cellAttributes: { alignment: 'center' }, editable: true },
    { label: LABEL_COMMUNITY_EMPLOYEES_OFFER, fieldName: 'Total_number_of_employees__c', cellAttributes: { alignment: 'center' }, editable: true },
    { label: LABEL_COMMUNITY_COUNTRY, fieldName: 'Billing_Country__c', cellAttributes: { alignment: 'center' }, editable: true },
    //{ label: LABEL_COMMUNITY_STATUS, fieldName: 'Status__c', cellAttributes: { alignment: 'center' }, editable: true },
    { label: LABEL_COMMUNITY_ERROR_MESSAGE, fieldName: 'Error_message__c', cellAttributes: { alignment: 'center' }, editable: true } 
  
];

export default class ResellerListViewMultiple extends LightningElement {

    @api recordId;
    @api columns = COLS;
    @api columnsBR = COLS_BR;
    @track isEditable = false;
    draftValues = [];
    selectedLabel = LABEL_COMMUNITY_AVAILABLE;
    valueCondition = 'GO';
    searchValue = '';
    error;

    lengthResult;
    accountRequestRecord;

    label = {
        LABEL_COMMUNITY_MULTIPLE_COMPANY,
        LABEL_COMMUNITY_CONDITION,
        LABEL_COMMUNITY_GENERATE_PDF,
        LABEL_COMMUNITY_QUICK_FIND,
        LABEL_COMMUNITY_ITEMS_FILTERED
    }

    options = [
            { label: LABEL_COMMUNITY_AVAILABLE, value: 'GO' },
            { label: LABEL_COMMUNITY_UNAVAILABLE, value: 'NO GO' },
            { label: LABEL_COMMUNITY_INVALID, value: 'invalid' },
        ];
    
    @track currentFilter;
    @track isExpanded = false;
    @track isLoaded = false;

    
    renderedCallback() {   
        this.isLoaded = true;

    }
 
    get dropdownTriggerClass() {
        if (this.isExpanded) {
            return 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click custom_list_view slds-is-open'
        } else {
            return 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click custom_list_view'
        }
    }
 
    handleFilterChangeButton(event) {
        this.isLoaded = false;
        this.currentFilter = event.target.dataset.filter;
        this.isExpanded = !this.isExpanded;


        if (this.valueCondition !== this.currentFilter) {
            this.valueCondition = this.currentFilter;

            if(this.valueCondition == 'GO'){ 
                this.selectedLabel = LABEL_COMMUNITY_AVAILABLE ; 
                this.isEditable = false;
            
            }else if(this.valueCondition == 'NO GO'){ 
                this.selectedLabel = LABEL_COMMUNITY_UNAVAILABLE ; 
                this.isEditable = false;
            
            }else if (this.valueCondition == 'invalid'){
                this.selectedLabel = LABEL_COMMUNITY_INVALID; 
                this.isEditable = true;
            }
        } 
    }

    runBatches(){
        runBatchesAgain({massId : this.recordId})
        .then(result =>{
            console.log('result : ' + result);
        })
        .catch(error =>{
            console.log('Error : ' + error);
        })

    }
 
    handleClickExtend() {
        this.isExpanded = !this.isExpanded;
    }

    handleKeyChange(event){
		this.searchValue = event.target.value;
		getSearchValue({ massAccReqId: this.recordId, searchKey: this.searchValue, engineStatus: this.valueCondition })
		.then(result => {
     
            //this.lengthResult2 = result.length;
		    this.error = undefined;

		})
		.catch(error => {
			this.error = error;
			
		})
	} 
    country;
    isCountryBrazil;

    @wire(getRecord, {recordId : USER_ID, fields:[USER_COUNTRY_CODE]})
    currentUser({error, data}){
        if(error){
            //SHOW ERROR USER
        }
        else if(data){
            this.country = data.fields.CountryCode.value;
    
            if (this.country == 'BR')this.isCountryBrazil = true; 
            else this.isCountryBrazil = false;
        }
    }
   
    @wire(getSearchValue, {
        massAccReqId: '$recordId', searchKey: '$searchValue', engineStatus: '$valueCondition'
    })
    loadRequests({ error, data }) {
       
        if (data) {

            this.accountRequestRecord = data;
            this.lengthResult = this.accountRequestRecord.length;
            this.error = undefined;

            let newcolumn = this.columns.map((item) => 
            Object.assign({}, item, {editable : this.isEditable}))
            this.columns = newcolumn;

            let newcolumnBR = this.columnsBR.map((item) => 
            Object.assign({}, item, {editable : this.isEditable}))
            this.columnsBR = newcolumnBR;
    
        } else if (error) {
            this.error = error;
            this.leads = undefined;
        }
    }

    async handleSave(event) {
        const updatedFields = event.detail.draftValues;
        const notifyChangeIds = updatedFields.map(row => { return { "recordId": row.Id } });
        try {

            const result = await updateAccountRequests({data: updatedFields});
            console.log(JSON.stringify("Apex update result: "+ result));
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Success',
                    message: 'Account Requests updated',
                    variant: 'success'
                })
            );
    
            getRecordNotifyChange(notifyChangeIds);

            const result2 = await runBatchesAgain({massId: this.recordId});
            console.log(JSON.stringify("Apex runBatchesAgain result: "+ result2));
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Success',
                    message: 'A new batch was created. We will send you the query report by email when it is ready',
                    variant: 'success'
                })
            );

            this.openMassive();

            this.draftValues = null;
            
           
       } catch(error) {
           console.log('Error : ' + JSON.stringify(error))
               this.dispatchEvent(
                   new ShowToastEvent({
                       title: 'Error updating or refreshing records',
                       message: error.body.message,
                       variant: 'error'
                   })
             );
        };
    }
    openMassive(){
        var url = '/resellerhub/s/detail/' + this.recordId;
        window.location.replace(url);
    }
    generatePdf(event){
        var url = '/resellerhub/apex/ResellerMultipleDetail?id=' + this.recordId;
        window.open(url);
    }
    renderedCallback() {
        loadScript(this, downloadjs)
        .then(() => console.log('Loaded download.js'))
        .catch(error => console.log(error));
    }
}

import LABEL_COMMUNITY_MULTIPLE_COMPANY from '@salesforce/label/c.Community_Multiple_company';
import LABEL_COMMUNITY_CONDITION from '@salesforce/label/c.Community_Condition';
import LABEL_COMMUNITY_GENERATE_PDF from '@salesforce/label/c.Community_Generate_PDF';
import USER_ID from '@salesforce/user/Id';
import USER_COUNTRY_CODE from '@salesforce/schema/User.CountryCode';

import LABEL_COMMUNITY_AVAILABLE from '@salesforce/label/c.Community_Condition_Available';
import LABEL_COMMUNITY_UNAVAILABLE from '@salesforce/label/c.Community_Condition_Unavailable';
import LABEL_COMMUNITY_INVALID from '@salesforce/label/c.Community_Condition_Invalid';

import LABEL_COMMUNITY_COMPANY_NAME from '@salesforce/label/c.Community_Company_name';
import LABEL_COMMUNITY_WEBSITE from '@salesforce/label/c.Community_Website';
import LABEL_COMMUNITY_UNIQUE_IDENTIFIER_TYPE from '@salesforce/label/c.Community_Ui_type';
import LABEL_COMMUNITY_UNIQUE_IDENTIFIER from '@salesforce/label/c.Community_Unique_Identifier';
import LABEL_COMMUNITY_PARTNER_MODEL from '@salesforce/label/c.Community_Partner_Model';
import LABEL_COMMUNITY_EMPLOYEES_OFFER from '@salesforce/label/c.Community_Employees_on_this_offer';
import LABEL_COMMUNITY_COUNTRY from '@salesforce/label/c.Community_Country';
import LABEL_COMMUNITY_STATUS from '@salesforce/label/c.Community_Status';

import LABEL_COMMUNITY_ITEMS_FILTERED from '@salesforce/label/c.Community_Items_Filtered';
import LABEL_COMMUNITY_QUICK_FIND from '@salesforce/label/c.Community_Quick_Find';
import LABEL_COMMUNITY_ERROR_MESSAGE from '@salesforce/label/c.Community_Error_message';