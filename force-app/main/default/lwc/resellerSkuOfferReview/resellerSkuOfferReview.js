import { LightningElement, api, wire, track } from 'lwc';
import { getRecord, getFieldValue, updateRecord } from 'lightning/uiRecordApi';
//import sendToApproval from '@salesforce/apex/SkuOfferReviewController.sendToApproval';
import sendToApproval from '@salesforce/apex/ResellerSkuOfferReviewController.sendToApproval';
import updateQuote from '@salesforce/apex/ResellerSkuOfferReviewController.updateQuote';
//import getQuote from '@salesforce/apex/ResellerSkuOfferReviewController.getQuote';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

import TOTAL_DISCOUNT_APPROVED from '@salesforce/schema/Opportunity.SyncedQuote.Total_Discount_approved__c';
import TOTAL_DISCOUNT from '@salesforce/schema/Opportunity.SyncedQuote.Discount';
import WAIVER_APPROVED from '@salesforce/schema/Opportunity.SyncedQuote.Waiver_approved__c';
import DISCOUNT_APPROVAL_LEVEL from '@salesforce/schema/Opportunity.SyncedQuote.Discount_Approval_Level__c';
import WAIVER_APPROVAL_LEVEL from '@salesforce/schema/Opportunity.SyncedQuote.Waiver_Approval_Level__c';
import DD_APPROVED_DESCRIPTION from '@salesforce/schema/Opportunity.SyncedQuote.Deal_Desk_Approved_Conditions__c';
import QUOTE_NAME from '@salesforce/schema/Opportunity.SyncedQuote.Name';
import QUOTEID from '@salesforce/schema/Opportunity.SyncedQuoteId';
import FASTTRACK_STAGE from '@salesforce/schema/Opportunity.FastTrackStage__c';
import OPP_STAGE from '@salesforce/schema/Opportunity.StageName';
import ID_FIELD from '@salesforce/schema/Opportunity.Id';
import COMMERCIAL_APPROVED_CONDITIONS from '@salesforce/schema/Opportunity.SyncedQuote.Commercial_Approved_Conditions__c';
import ENTERPRISE_DISCOUNT_APPROVAL_NEEDED from '@salesforce/schema/Opportunity.SyncedQuote.Enterprise_Discount_Approval_Needed__c'; 
import ENTERPRISE_SUBSCRIPTION_APPROVED from '@salesforce/schema/Opportunity.SyncedQuote.Enterprise_Subscription_Approved__c';
import ENABLERS_APPROVED_NEEDED from '@salesforce/schema/Opportunity.SyncedQuote.Enablers_Approval_Needed__c';
import ENABLERS_APPROVED from '@salesforce/schema/Opportunity.SyncedQuote.Enablers_Approved__c';

//import DEAL_DESK_COMMENTS from '@salesforce/schema/Opportunity.SyncedQuote.Deal_Desk_Comments__c';
//import RATIONALE from '@salesforce/schema/Opportunity.SyncedQuote.Rationale__c';

//import DEAL_DESCK_APPROVAL_NEEDED from '@salesforce/schema/Opportunity.SyncedQuote.Deal_Desk_Approval_Needed__c';
import SystemModstamp from '@salesforce/schema/Account.SystemModstamp';

const fields = 
    [ TOTAL_DISCOUNT,
      TOTAL_DISCOUNT_APPROVED,
      WAIVER_APPROVED,
      DISCOUNT_APPROVAL_LEVEL,
      WAIVER_APPROVAL_LEVEL,
      QUOTE_NAME,
      QUOTEID,
      DD_APPROVED_DESCRIPTION,
      ENABLERS_APPROVED,
      ENABLERS_APPROVED_NEEDED,
      COMMERCIAL_APPROVED_CONDITIONS,
      ENTERPRISE_DISCOUNT_APPROVAL_NEEDED,
      ENTERPRISE_SUBSCRIPTION_APPROVED,
      //DEAL_DESK_COMMENTS,
      //RATIONALE
       ];

export default class ResellerSkuOfferReview extends LightningElement {
    @api recordId;
    @track opp;
    @track lstApprovedConditions;
    @track isLoading = true;

    @api
    rationaleCommercialConditions;
    @api
    rationaleDealDeskApproval;
    sfdcBaseURL;
    disableJustifyCommercial;
    
    renderedCallback() {
        this.sfdcBaseURL = window.location.origin;
        this.isLoading = false;
        
    }
    
    @wire( getRecord , { recordId: '$recordId', fields } )
    wiredOpp({ error, data }) {
        if( data ) {
            console.log('opp ',data);
            this.opp = data;
            this.lstApprovedConditions = this.getLstApprovedConditions();

            this.error = undefined;
        } else if( error ) {
            console.log('error ',error);
            this.error = error;
            this.opp = undefined;
        }
    };    

    handlePreviousStage(){
        this.isLoading = true;
        const fields = {};
        fields[ID_FIELD.fieldApiName] = this.recordId;
        fields[FASTTRACK_STAGE.fieldApiName] = 'Offer Creation';

        setTimeout(() => {
            updateRecord( { fields } )
                .then(() => {
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: 'Success',
                            message: 'Changed stage',
                            variant: 'success'
                        })
                    );
                })
                .catch(error => {
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: 'Error creating record',
                            message: error.body.message,
                            variant: 'error'
                        })
                    );
                })
                .finally(() => {
                    this.isLoading = false;
                });
        }, 3000);
    }
    
    handleNextStage(){
        this.isLoading = true;
        const fields = {};
        fields[ID_FIELD.fieldApiName] = this.recordId;
        fields[OPP_STAGE.fieldApiName] = 'Proposta Enviada';
        fields[FASTTRACK_STAGE.fieldApiName] = 'Offer Sent';

        setTimeout(() => {
            updateRecord( { fields } )
                .then(() => {
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: 'Success',
                            message: 'Changed stage',
                            variant: 'success'
                        })
                    );
                })
                .catch( error => {
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: 'Error creating record',
                            message: error.body.message,
                            variant: 'error'
                        })
                    );
                })
                .finally(() => {
                    this.isLoading = false;
                });
        }, 3000);
    }

    dealDeskApproval;
    commercialConditions;

    handleJustifyDealDesk(event){
        let value = event.target.value;
        this.dealDeskApproval = value;
    }
    
    handleJustifyCommercial(event){
        let value = event.target.value;
        this.commercialConditions = value;
    }
    

     handleSendToApproval(){
        
        console.log( this.checkJustifyCommercialConditions() );
        sendToApproval({ 
                recordId : this.recordId, 
                aRationale : this.rationaleCommercialConditions, 
                aTotalDiscount : getFieldValue(this.opp, TOTAL_DISCOUNT), 
                aQuoteId : getFieldValue(this.opp, QUOTEID),
                aDealDeskRationale : this.rationaleDealDeskApproval,
                needQuoteApproval: this.checkJustifyCommercialConditions(),
                needDealDeskApproval:  this.checkJustifyDealDesk(),
                //needEnterpriseApproval: getFieldValue(this.opp, ENTERPRISE_DISCOUNT_APPROVAL_NEEDED),
                //needWaiverApproval: getFieldValue(this.opp, WAIVER_APPROVED)
                regionalManagerApproval: this.regionalManagerApproval})
            .then(() => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Success',
                        message: 'Proposal sent to approval!',
                        variant: 'success'
                    })
                ); 
            })
            .catch( error => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error creating record',
                        message: error.body.message,
                        variant: 'error'
                    })
                );
            });

            updateQuote({ 
            
                aQuoteId : getFieldValue(this.opp, QUOTEID), 
                rationaleDealDeskApproval : this.dealDeskApproval, 
                rationaleCommercialConditions : this.commercialConditions
               })
            .then(() => {
                console.log('quote updated');
            })
            .catch( error => {
                console.log('quote not updated' + error );
            });
           
          
     }
   
    getLstApprovedConditions(){
        let lstDealDeskConditions = JSON.parse( getFieldValue(this.opp, DD_APPROVED_DESCRIPTION) );

        if( !lstDealDeskConditions && lstDealDeskConditions.length ){ return []; }

        lstDealDeskConditions.forEach(condition => {
            condition.iconName = condition.isApproved ? 'utility:success' : 'utility:clear';
            condition.iconVariant = condition.isApproved ? 'success' : 'error';
        });

        return lstDealDeskConditions;
    }

    get lstApprovedConditionsIsEmpty(){
        return !this.lstApprovedConditions || !this.lstApprovedConditions.length;
    }

    get needDiscountApproval(){
        return getFieldValue(this.opp, TOTAL_DISCOUNT_APPROVED) || ( getFieldValue(this.opp, DISCOUNT_APPROVAL_LEVEL) == 0 ) ? 'utility:success' : 'utility:clear';
    }

    get getDiscountApprovalVariant(){
        return getFieldValue(this.opp, TOTAL_DISCOUNT_APPROVED) || ( getFieldValue(this.opp, DISCOUNT_APPROVAL_LEVEL) == 0 ) ? 'success' : 'error';
    }

    get needWaiverApproval(){
        return getFieldValue(this.opp, WAIVER_APPROVED) ? 'utility:success' : 'utility:clear';
    }

    get getWaiverApprovalVariant(){
        return getFieldValue(this.opp, WAIVER_APPROVED) ? 'success' : 'error';
    }

    get needEnterpriseSubscriptionApproval(){
        return getFieldValue(this.opp, ENTERPRISE_DISCOUNT_APPROVAL_NEEDED) ? 'utility:clear' : 'utility:success';
    }

    get getEnterpriseSubscriptionApprovalVariant(){
        return getFieldValue(this.opp, ENTERPRISE_DISCOUNT_APPROVAL_NEEDED) ? 'error' : 'success';
    }

    get needEnablersApproval(){
        return getFieldValue(this.opp, ENABLERS_APPROVED_NEEDED) ? 'utility:clear' : 'utility:success';
    }
    get getEnablersApprovalVariant(){
        return getFieldValue(this.opp, ENABLERS_APPROVED_NEEDED) ? 'error' : 'success';
    }

    get getQuoteName(){
        return getFieldValue(this.opp, QUOTE_NAME);
    }

    get approvalHistoryLink(){
        return this.sfdcBaseURL + '/lightning/r/Quote/' + getFieldValue(this.opp, QUOTEID) + '/related/ProcessSteps/view';
    }

    get enableJustifyCommercialConditions(){
        return ( !getFieldValue(this.opp, TOTAL_DISCOUNT_APPROVED) && ( getFieldValue(this.opp, DISCOUNT_APPROVAL_LEVEL) != null && getFieldValue(this.opp, DISCOUNT_APPROVAL_LEVEL) > 0 ) ) ||
            ( !getFieldValue(this.opp, WAIVER_APPROVED) && ( getFieldValue(this.opp, WAIVER_APPROVAL_LEVEL) != null && getFieldValue(this.opp, WAIVER_APPROVAL_LEVEL) > 0 ));
    }

    checkJustifyCommercialConditions(){
        return ( !getFieldValue(this.opp, TOTAL_DISCOUNT_APPROVED) && ( getFieldValue(this.opp, DISCOUNT_APPROVAL_LEVEL) != null && getFieldValue(this.opp, DISCOUNT_APPROVAL_LEVEL) > 0 ) ) ||
            ( !getFieldValue(this.opp, WAIVER_APPROVED) && ( getFieldValue(this.opp, WAIVER_APPROVAL_LEVEL) != null && getFieldValue(this.opp, WAIVER_APPROVAL_LEVEL) > 0 ));
    }

    get enableJustifyDealDesk(){
        let dealDeskDescription = getFieldValue(this.opp, DD_APPROVED_DESCRIPTION);

        if( !dealDeskDescription ){ return false; }

        let lstDealDeskConditions = JSON.parse( dealDeskDescription );
        
        let needApproval = false;

        lstDealDeskConditions.forEach(condition => {
            if( condition.isApproved == false ){
                needApproval = true;
            }
        });

        return needApproval;
    }

    checkJustifyDealDesk(){
        let dealDeskDescription = getFieldValue(this.opp, DD_APPROVED_DESCRIPTION);

        if( !dealDeskDescription ){ return false; }

        let lstDealDeskConditions = JSON.parse( dealDeskDescription );
        
        let needApproval = false;

        lstDealDeskConditions.forEach(condition => {
            if( condition.isApproved == false ){
                needApproval = true;
            }
        });
        
        return needApproval;
    }

    get disableSendToApproval(){

        // return ( getFieldValue(this.opp, TOTAL_DISCOUNT_APPROVED) || ( getFieldValue(this.opp, DISCOUNT_APPROVAL_LEVEL) == 0 || !getFieldValue(this.opp, DISCOUNT_APPROVAL_LEVEL) ) && !getFieldValue(this.opp, ENTERPRISE_DISCOUNT_APPROVAL_NEEDED) ) &&
        // ( getFieldValue(this.opp, WAIVER_APPROVED) || ( getFieldValue(this.opp, WAIVER_APPROVAL_LEVEL) == 0 || !getFieldValue(this.opp, WAIVER_APPROVAL_LEVEL) ) && !getFieldValue(this.opp, ENTERPRISE_DISCOUNT_APPROVAL_NEEDED) ) &&
        // (!this.checkJustifyDealDesk() && !getFieldValue(this.opp, ENTERPRISE_DISCOUNT_APPROVAL_NEEDED));

        return !this.checkJustifyDealDesk() && !this.regionalManagerApproval;
    }

    get disableJustifyDealDesk(){

        let dealDeskDescription = getFieldValue(this.opp, DD_APPROVED_DESCRIPTION);

        if( !dealDeskDescription ){ return true; }

        let isDisableJustify = true;

        if(dealDeskDescription.includes('false')){
            isDisableJustify = false;
       }
        return isDisableJustify;
    }

    // get disableJustifyCommercial2(){

    //     if(!getFieldValue(this.opp, WAIVER_APPROVED) || !getFieldValue(this.opp, ENTERPRISE_DISCOUNT_APPROVAL_NEEDED) || !getFieldValue(this.opp, ENABLERS_APPROVED_NEEDED)){ return false; }

    //     else{return true;} 
    // }

    get colorDealDeskApproval() { 

        let dealDeskDescription = getFieldValue(this.opp, DD_APPROVED_DESCRIPTION);

        if( !dealDeskDescription ){ return true; }

        let isDisableJustify = true;

        if(dealDeskDescription.includes('false')){
            isDisableJustify = false;
       }

        return isDisableJustify ? 'slds-box slds-box_xx-small colorbox_ok' : 'slds-box slds-box_xx-small colorbox_error';
    }

    get colorRegionalManagerApproval() { 

        let color = true;
        console.log(getFieldValue(this.opp, WAIVER_APPROVED))
        console.log(getFieldValue(this.opp, ENTERPRISE_DISCOUNT_APPROVAL_NEEDED))
        console.log(getFieldValue(this.opp, ENABLERS_APPROVED_NEEDED))
        if(!getFieldValue(this.opp, WAIVER_APPROVED) || 
            getFieldValue(this.opp, ENTERPRISE_DISCOUNT_APPROVAL_NEEDED) || 
            getFieldValue(this.opp, ENABLERS_APPROVED_NEEDED)){

                color = false;
                this.disableJustifyCommercial = false; 
                this.regionalManagerApproval = true;
            }else{
                color = true;
                this.disableJustifyCommercial = true;
                this.regionalManagerApproval = false; 
            } 

        return color ? 'slds-box slds-box_xx-small colorbox_ok' : 'slds-box slds-box_xx-small colorbox_error';
    }
    /*
        getQuote({ aQuoteId : getFieldValue(this.opp, QUOTEID) })
        .then(result => {
            this.rationaleDealDeskApproval = result.Deal_Desk_Comments__c;
            this.rationaleCommercialConditions = result.Rationale__c;
        
        })
        .catch(error => {
            console.error(error);

        });
    }

    @wire(getRecord, {recordId : QUOTEID, fields:[DEAL_DESK_COMMENTS, RATIONALE]})
    getQuoteTeste({ error, data }) {
        if( data ) {
            this.rationaleDealDeskApproval = result.Deal_Desk_Comments__c;
            this.rationaleCommercialConditions = result.Rationale__c;

        } else if( error ) {
            this.error = error;
        }
    }; 
    */
    get disableContinue(){
         
        return this.checkJustifyDealDesk() || this.regionalManagerApproval;
   }
}