import { LightningElement, api } from 'lwc';

import { FlowAttributeChangeEvent } from 'lightning/flowSupport';

import fetchDefaultValue from '@salesforce/apex/ResellerEJContactLookupController.fetchSingleRecordSelected';
import apexContactSearch from '@salesforce/apex/ResellerEJContactLookupController.searchContact';

export default class ResellerEJGymPALResponsibleLookup extends LightningElement {

    //-- configurable parameters
    @api recordId;
    @api defaultRecordId;
    @api accountId;
    @api lookupLabel = '';
    @api lookupPlaceholder = '';
    @api isRequired = false;

    @api searchField = 'Name';

    @api gymPALResponsibleContactId;
    @api lstSelectedRecordIds = [];

    defaultResult = {};
    listOfDefaultResults = [];

    connectedCallback() {
        this.getDefaultValues();
    }

    getDefaultValues() {
        fetchDefaultValue({
            defaultRecordId: this.defaultRecordId
        })
        .then((result) => {
            this._searchedPreviousRecordMap = result.reduce((res, item) => {
                res[item.Id] = item;
                return res;
            }, {});

            this.listOfDefaultResults = result.map(item => {
                return {
                    id: item.Id,
                    icon : 'standard:contact',
                    sObjectType : 'Contact',
                    title: this.getFieldValue(this.searchField, item),
                };
            });

            this._previousRecordItemMap = this.listOfDefaultResults.reduce((res, item) => {
                res[item.id] = item;
                return res;
            }, {});

            this.defaultResult = this.listOfDefaultResults.at(0);
            this.gymPALResponsibleContactId = this.listOfDefaultResults.at(0).id;
            this.lstSelectedRecordIds = this.lstSelectedRecordIds.concat(this.listOfDefaultResults.at(0).Id);
        })
        .catch((error) => {
            this.error = error;
        });
    }


    handleContactSearch(event) {
        const lookupElement = event.target;
        apexContactSearch({ 
                searchTerm: event.detail.searchTerm,
                selectedIds: event.detail.selectedIds,
                accountId: this.accountId
            })
            .then(results => {
                lookupElement.setSearchResults(results);
            })
            .catch(error => {
                reduceErrors(error);
            });
    }

    handleSelectedRecords(event) {
        //-- the custom lookup already updates the current selection
        this.lstSelectedRecordIds = event.detail;

        this.gymPALResponsibleContactId = event.detail[0];
        
        const attributeChangeEvent = new FlowAttributeChangeEvent(
            'gymPALResponsibleContactId',
            event.detail[0]
        );
        this.dispatchEvent(attributeChangeEvent);
    }

    getFieldValue(fieldName, obj) {
        if (fieldName) {
            return fieldName.split('.').reduce((result, item) => {
                return result[item];
            }, obj);
        }
        return '';
    }
}