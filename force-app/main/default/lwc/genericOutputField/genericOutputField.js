/**
 * 
 */
import { LightningElement, api } from 'lwc';

export default class GenericOutputField extends LightningElement {

    @api currentRecordId;
    @api objectApiName;
    @api fieldApiName;

    @api fieldLabel = "Custom label"
    @api helpTextDetail = "Custom help text."
    @api hasHelpText = false;

}