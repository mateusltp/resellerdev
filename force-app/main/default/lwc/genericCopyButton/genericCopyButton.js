import { LightningElement, api, track, wire } from 'lwc';
import { getRecord } from 'lightning/uiRecordApi';

export default class GenericCopyButton extends LightningElement {

    @api buttonLabel;
    @api iconName;
    @api mouseOverMessage;

    @track variant = 'brand';

    @api objectApiName;
    @api fieldApiName;
    fields;

    @api recordId;

    @wire(getRecord, {
        recordId: '$recordId',
        fields: '$fields'
    })
    record;

    connectedCallback() {
        this.fields = [this.objectApiName + '.' + this.fieldApiName];
    }

    handleCopyClick() {
        this.variant = 'brand-outline';
        this.iconName = 'utility:check';
        let tag = document.createElement('textarea');
        tag.setAttribute('id', 'contentToCopy');
        tag.value = this.record.data.fields[this.fieldApiName].value;
        document.getElementsByTagName('body')[0].appendChild(tag);
        document.getElementById('contentToCopy').select();
        document.execCommand('copy');
        document.getElementById('contentToCopy').remove();

        setTimeout(() => {  
                this.iconName = 'utility:copy';
                this.variant = 'brand';
            }, 
        500);  
    }
    

}