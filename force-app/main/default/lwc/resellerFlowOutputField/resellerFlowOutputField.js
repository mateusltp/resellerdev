import { LightningElement, api } from 'lwc';

export default class ResellerFlowOutputField extends LightningElement {

    @api label;
    @api helpTextDetail;
    @api value;
    @api hasHelpText;
    
    @api fieldType;

}