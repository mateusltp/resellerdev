import { LightningElement, api, track, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import buildNetwork from '@salesforce/apex/SelectPartnersAccountsController.buildNetwork';

export default class SelectPartnersAccounts extends LightningElement {
    @api objRecordId;
    @api accountData;
    
    // ** For contract creation context**
    @api context;
    @api transferObjData; 
    // ***************************

    // ** For product context **
    @api helperData = null;
    // ***************************

    @track gridColumns;
    @track gridData;
    @track accountLst = [];

    globalChildIdToParentIdMap = new Map();
    
    selectedAccounts        = [];
    gridDataBackUp          = [];
    expandedRows            = [];
    selectedRows            = [];
    globalSelectedRowsIdLst = [];
    locationsPreSelected    = [];
    searchFilteredRowsId    = [];

    spinner             = false;
    toggleActivated     = false;
    searchModeActivated = false;
    
    objectLabel = '';

    SEARCH_SELECT_CONTEXT   = Symbol('SEARCH');
    DEFAULT_SELECT_CONTEXT  = Symbol('DEFAULT');
    selectContext;
    
    @api
    get accountsSelectNumber() {
        return this.selectedAccounts.length;
    }
    
    @wire(buildNetwork, { recordId: '$objRecordId', recordTypeDevName: 'Partner_Flow_Account' }) accounts(result) {
        this.dataResult = result;
        if (result.data) {
            this.showSpinner();
            let accountTree = [...result.data.tree];
            let tempData = JSON.parse(JSON.stringify(accountTree).split('items').join('_children'));
            
            this.accountLst = [...result.data.accounts];
            this.gridData = tempData;
            this.gridDataBackUp = this.gridData;
            
            this.setSelectContext(this.DEFAULT_SELECT_CONTEXT);
            this.populateGlobalChildIdToParentIdMap(tempData);
            this.setExpandedRows();
            if (this.context == 'CONTRACT') {
                this.objectLabel = 'contract';
                this.setPreSelectedRowsAndAccountsForContract();
            } else {
                this.objectLabel = 'product';
                this.setPreSelectedRowsAndAccountsForProduct();
            }

            this.hideSpinner();
        } else if (result.error) {
            if (Array.isArray(result.error.body))
            this.showToastNotification('Error: Please contact Salesforce Admin', result.error.body.map(e => e.message).join(', '), 'error');
            else if (typeof result.error.body.message === 'string')
            this.showToastNotification('Error: ' + result.error.body.message, 'Please contact Salesforce Admin', 'error');
            
            this.hideSpinner();
        }
    }

    connectedCallback() {
        if (this.context == 'CONTRACT') {
            this.gridColumns = this.getGridColForContract();
        } else {
            this.gridColumns = this.getGridCol();
        }
    }
    
    getGridCol() {
        return [
            {   
                label: 'Name',
                fieldName: 'NameUrl',
                type: 'url',
                typeAttributes: {
                    label: { fieldName: 'Name' },
                    target: '_blank'   
                }
            },
            {   
                label: 'Partner Level',
                fieldName: 'PartnerLevel',
                type: 'text'
            }
        ];
    }
    
    getGridColForContract() {
        return [
            {   
                label: 'Name',
                fieldName: 'NameUrl',
                type: 'url',
                typeAttributes: {
                    label: { fieldName: 'Name' },
                    target: '_blank'   
                }
            },
            {   
                label: 'Partner Level',
                fieldName: 'PartnerLevel',
                type: 'text'
            },
            {   
                label: 'Legal Representative',
                fieldName: 'LegalRepresentativeUrl',
                type: 'url',
                typeAttributes: {
                    label: { fieldName: 'LegalRepresentativeName' },
                    target: '_blank'
                }
            }
        ];
    }

    handleSelectedRows(event) {
        console.log('handleSelectedRows');
        this.showSpinner();

        console.log(this.getSelectContext());
        if (this.getSelectContext() == this.SEARCH_SELECT_CONTEXT) {
            this.handleSearchSelectContext();
        } else {
            this.handleDefaultSelectContext();
        }

        this.hideSpinner();
    }

    handleSearchSelectContext() {
        console.log('handleSearchSelectContext');
        this.syncSelectedRowsSearchSelect();
    }
    
    syncSelectedRowsSearchSelect() {
        let selectRows = this.template.querySelector('lightning-tree-grid').getSelectedRows();
        let idMap = new Map();
        let currentSelectedRowsId   = [];
        let currentDeselectedRowsId = [];

        // Puts all selected rows in idMap
        this.getGlobalSelectedRowsIdLst().forEach(id => idMap.set(id, id));

        selectRows.forEach(row => {
            currentSelectedRowsId.push(row.id);
            idMap.set(row.id, row.id)
        });

        // Removes rows from idMap
        this.getGlobalSelectedRowsIdLst().forEach(id => {
            if (!currentSelectedRowsId.includes(id) && this.getSearchFilteredRowsId().includes(id)) {
                currentDeselectedRowsId.push(id);
            }
        });

        currentDeselectedRowsId.forEach(id => idMap.delete(id));

        this.setGlobalSelectedRowsIdLst(Array.from(idMap.keys()));
        this.setSelectedAccounts(this.getGlobalSelectedRowsIdLst());
        this.selectedRows = this.getGlobalSelectedRowsIdLst();
        this.dispatchSelectedAccounts();
    }

    handleDefaultSelectContext() {
        console.log('defaultSelectContext');
        if (!this.getToggleActivated()) {
            let selectRows = this.template.querySelector('lightning-tree-grid').getSelectedRows();

            this.syncSelectedRowsDefaultSelect(selectRows);

            if (this.context == 'CONTRACT') {
                if (this.transferObjData.contract.type == 'Commercial Contract') {
                    this.handleSelectRowsForCommercialContract(selectRows);
                } else {
                    this.handleDefaultSelectRows(selectRows);
                }
            } else {
                this.handleDefaultSelectRows(selectRows);
            }

        } else {
            this.setToggleActivated(false);
        }
    }

    syncSelectedRowsDefaultSelect(selectRows) {
        console.log('syncSelectedRows');
        let currentSelectedRowsId = [];
        let currentDeselectedRowsId = [];
        let currentExpandedRows = Array.from(this.template.querySelector('lightning-tree-grid').getCurrentExpandedRows());
        let idMap = new Map();

        // Puts all selected rows in idMap
        this.getGlobalSelectedRowsIdLst().forEach(id => idMap.set(id, id));
        
        selectRows.forEach(row => {
            currentSelectedRowsId.push(row.id);
            idMap.set(row.id, row.id)
        });

        // Removes rows from idMap
        this.getGlobalSelectedRowsIdLst().forEach(id => {
            if (!currentSelectedRowsId.includes(id) && this.areAllParentRowsExpanded(id, currentExpandedRows)) {
                currentDeselectedRowsId.push(id);
            }
        });

        currentDeselectedRowsId.forEach(id => idMap.delete(id));
        
        this.setGlobalSelectedRowsIdLst(Array.from(idMap.keys()));
        this.setSelectedAccounts(this.getGlobalSelectedRowsIdLst());
        this.selectedRows = this.getGlobalSelectedRowsIdLst();
    }

    areAllParentRowsExpanded(rowId, currentExpandedRows) {
        if (this.globalChildIdToParentIdMap.has(rowId)) {
            let parentId = this.globalChildIdToParentIdMap.get(rowId);
            
            // Recursive call
            if (currentExpandedRows.includes(parentId) && this.areAllParentRowsExpanded(parentId, currentExpandedRows)) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    handleToggle(event) {
        console.log('handleToggle');
        if (event.detail.isExpanded) {
            this.setToggleActivated(false);
            this.setTreeGridWithGlobalSelectedRowsId();
        } else {
            this.setToggleActivated(true);
        }
    }

    handleDefaultSelectRows(selectRows) {
        console.log('handleDefaultSelectRows');
        let arr = [];
        this.selectedAccounts = [];

        Array.from(selectRows).forEach(row => {
            arr.push(row.id);
            this.selectedAccounts.push({ id: row.id, name: row.Name, partnerLevel: row.PartnerLevel });
        });
        
        this.dispatchSelectedAccounts();
    }

    handleSelectRowsForCommercialContract(selectRows) {
        console.log('handleSelectRowsForCommercialContract');
        let validLocationSelection = true;
        let arr = [];
        this.selectedAccounts = [];

        Array.from(selectRows).forEach(row => {
            arr.push(row.id);
            this.selectedAccounts.push({ id: row.id, name: row.Name, partnerLevel: row.PartnerLevel });
        });

        this.selectedRows = this.getGlobalSelectedRowsIdLst();

        let firstNotLocation = selectRows.find(row => row.PartnerLevel != "Location");
        if (firstNotLocation != undefined) {
            validLocationSelection = false;
        }

        if (validLocationSelection) {
            this.locationsPreSelected.forEach(location => {
                let row = selectRows.find(element => element.id == location.id);
                if (row == undefined) {
                    validLocationSelection = false;
                }
            });
        }

        this.dispatchCommercialContractValidation(validLocationSelection);
    }

    dispatchSelectedAccounts() {
        const selectedAccountsEvent = new CustomEvent("selectaccounts", {
            detail: this.selectedAccounts
        });
    
        this.dispatchEvent(selectedAccountsEvent);
    }

    dispatchCommercialContractValidation(validation) {
        console.log('dispatchCommercialContractValidation');
        this.dispatchEvent(new CustomEvent('commercialvalidation', { bubbles: true, composed: true, detail: validation }));
    }

    handleRemoveRecord(event) {
        let targetId = event.target.dataset.id;
        let idArr = [];

        this.selectedAccounts = this.selectedAccounts.filter(account => account.id != targetId);
        this.selectedAccounts.forEach(acc => idArr.push(acc.id));
        this.setGlobalSelectedRowsIdLst(idArr);
        this.selectedRows = this.getGlobalSelectedRowsIdLst();

        this.dispatchSelectedAccounts();
    }

    showToastNotification(title, message, variant) {
        const evt = new ShowToastEvent({ title: title, message: message, variant: variant });
        this.dispatchEvent(evt);
    }

    showSpinner() { this.spinner = true }

    hideSpinner() { this.spinner = false }

    setExpandedRows() {
        let arr = []
        this.accountLst.forEach(acc => { arr.push(acc.id) });
        this.expandedRows = arr;
    }

    setPreSelectedRowsForMainOppCommercialContract() {
        if (this.accountData == undefined || this.accountData == null || this.accountLst.length == 0) {
            return;
        }

        let auxSelectedRows = [];
        let auxSelectedAccounts = [];

        try {
            let accsWithSameLegalReps = this.accountLst.filter(acc =>
                acc.PartnerLevel == 'Location' &&
                this.accountData.Legal_Representative__c != undefined &&
                acc.LegalRepresentativeUrl == ('/' + this.accountData.Legal_Representative__c)
            );


            accsWithSameLegalReps.forEach(acc => {
                if (this.context != 'CONTRACT') {
                    if (!this.deletedAccIdLst.includes(acc.id)) {
                        auxSelectedRows.push(acc.id);
                        auxSelectedAccounts.push({ id: acc.id, name: acc.Name, partnerLevel: acc.PartnerLevel });
                    }
                } else {
                    auxSelectedRows.push(acc.id);
                    auxSelectedAccounts.push({ id: acc.id, name: acc.Name, partnerLevel: acc.PartnerLevel });
                }
            });

            this.setGlobalSelectedRowsIdLst(auxSelectedRows);
            this.selectedRows = this.getGlobalSelectedRowsIdLst();
            this.selectedAccounts = auxSelectedAccounts;

            this.dispatchSelectedAccounts();

        } catch (error) {
            this.hideSpinner();
            console.log(error);
        }
    }

    setPreSelectedRowsAndAccountsForContract() {
        let oppRecType = this.transferObjData.masterOpportunity.RecordType.DeveloperName;

        if (this.accountData == undefined || this.accountData == null || this.accountLst.length == 0) {
            return;
        }

        if (oppRecType == 'Partner_Flow_Opportunity' && this.transferObjData.contract.type != 'Commercial Contract') {
            this.setPreSelectedRowsForMainOppCommercialContract();
            return;
        }

        let auxSelectedRows = [];
        let auxSelectedAccounts = [];

        try {
            let locations = [];

            if (oppRecType == 'Partner_Flow_Child_Opportunity') {
                locations = this.accountLst.filter(acc =>
                    acc.PartnerLevel == 'Location' && this.transferObjData.locationsIdInChildOpp.includes(acc.id)
                );
            } else {
                locations = this.accountLst.filter(acc =>
                    acc.PartnerLevel == 'Location'
                );
            }

            locations.forEach(acc => {
                auxSelectedRows.push(acc.id);
                auxSelectedAccounts.push({ id: acc.id, name: acc.Name, partnerLevel: acc.PartnerLevel });
            });

            this.setGlobalSelectedRowsIdLst(auxSelectedRows);
            this.selectedRows = this.getGlobalSelectedRowsIdLst();
            this.selectedAccounts = auxSelectedAccounts;
            this.locationsPreSelected = locations;
            
            this.dispatchSelectedAccounts();
        } catch (error) {
            this.hideSpinner();
            console.log(error);
        }
    }

    setPreSelectedRowsAndAccountsForProduct() {
        if (this.accountData == undefined || this.accountData == null || this.accountLst.length == 0) {
            return;
        }

        let auxSelectedRows = [];
        let auxSelectedAccounts = [];

        try {
            let filteredAccounts = [];

            if (this.helperData.prodOpp.RecordType.DeveloperName == 'Partner_Flow_Opportunity') {
                console.log('size main -> ' +this.helperData.idSetFromAccAddedToProduct.length );
                if (this.helperData.idSetFromAccAddedToProduct.length == 0) {
                    filteredAccounts = this.accountLst.filter( acc => acc.PartnerLevel == 'Location' );
                } else {
                    filteredAccounts = this.accountLst.filter( acc =>
                        this.helperData.idSetFromAccAddedToProduct.includes(acc.id)
                    );
                }
    
                filteredAccounts.forEach(acc => {
                    auxSelectedRows.push(acc.id);
                    auxSelectedAccounts.push({id: acc.id, name: acc.Name, partnerLevel: acc.PartnerLevel});
                });
            } 
            
            if (this.helperData.prodOpp.RecordType.DeveloperName == 'Partner_Flow_Child_Opportunity') {
                console.log('::size -> ' + this.helperData.idSetFromAccAddedToProduct.length)
                if (this.helperData.idSetFromAccAddedToProduct.length == 0) {
                    filteredAccounts = this.accountLst.filter(acc => 
                        this.helperData.accIdsFromProdOppMembers.includes(acc.id)
                    );
                } else {
                    filteredAccounts = this.accountLst.filter(acc => 
                        this.helperData.idSetFromAccAddedToProduct.includes(acc.id)
                    );
                }
    
                filteredAccounts.forEach(acc => {
                    auxSelectedRows.push(acc.id);
                    auxSelectedAccounts.push({ id: acc.id, name: acc.Name, partnerLevel: acc.PartnerLevel });
                });
            }

            this.setGlobalSelectedRowsIdLst(auxSelectedRows);
            this.selectedRows = this.getGlobalSelectedRowsIdLst();
            this.setSelectedAccounts(this.getGlobalSelectedRowsIdLst());
            this.dispatchSelectedAccounts();

        } catch (error) {
            this.hideSpinner();
            console.log(error);
        }
    }

    handleSearchAction(evt) {
        console.log('handleSearchAction');
        var filteredRows;
        let rows = [... this.accountLst],
            term = evt.target.value,
            results = rows, regex;

        let filteredRowsIds = [];
        
        console.log('term size: ' + term.length);

        if (term.length < 1) {
            this.gridData = this.gridDataBackUp;
            this.setTreeGridWithGlobalSelectedRowsId();
            this.setSelectContext(this.DEFAULT_SELECT_CONTEXT);
            return;
        }

        try {
            this.showSpinner();
            this.setSelectContext(this.SEARCH_SELECT_CONTEXT);

            regex = new RegExp(term, "i");
            results = rows.filter(row => regex.test(row.Name));
            
            filteredRows = results;
            filteredRows.forEach(row => filteredRowsIds.push(row.id));
            this.setSearchFilteredRowsId(filteredRowsIds);

            //this.gridData = (filteredRows.length > 0) ? filteredRows : this.gridDataBackUp;
            if (filteredRows.length > 0) {
                this.gridData = filteredRows;
            }
            
            this.hideSpinner();
        } catch (e) {

        }        
    }

    setTreeGridWithGlobalSelectedRowsId() {
        console.log('setTreeGridSelectedRowsWithCurrent');
        this.template.querySelector('lightning-tree-grid').selectedRows = this.globalSelectedRowsIdLst;
    }

    setGlobalSelectedRowsIdLst(rows) {
        this.globalSelectedRowsIdLst = rows;
    }

    getGlobalSelectedRowsIdLst() {
        return this.globalSelectedRowsIdLst;
    }

    setToggleActivated(value) {
        console.log('setToggleActivated as: ' + value);
        this.toggleActivated = value;
    }

    getToggleActivated() {
        return this.toggleActivated;
    }

    setSelectedAccounts(rows) {
        console.log('setSelectedAccounts'); 
        let accounts = [];

        this.accountLst.forEach(acc => {
            if (rows.includes(acc.id)) {
                accounts.push({ id: acc.id, name: acc.Name, partnerLevel: acc.PartnerLevel });
            }
        });

        this.selectedAccounts = accounts;
    }

    setSelectContext(value) {
        this.selectContext = value;
    }

    getSelectContext() {
        return this.selectContext;
    }

    setSearchFilteredRowsId(rows) {
        this.searchFilteredRowsId = rows;
    }

    getSearchFilteredRowsId() {
        return this.searchFilteredRowsId;
    }

    populateGlobalChildIdToParentIdMap(data) {
        console.log('populateGlobalChildIdToParentIdMap');    
        data.forEach(acc => this.getChildrenForAccount(acc));
    }

    getChildrenForAccount(acc) {
        if (acc.hasOwnProperty('_children')) {
            acc._children.forEach(child => {
                this.globalChildIdToParentIdMap.set(child.id, acc.id)
                this.getChildrenForAccount(child);
            });
        } else {
            return;
        }
    }
}