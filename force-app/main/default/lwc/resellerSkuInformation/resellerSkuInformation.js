import { LightningElement, track, api, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { getRecord } from 'lightning/uiRecordApi';
import { refreshApex } from '@salesforce/apex';
import typeLabel from '@salesforce/label/c.profServicesTypes';
import getPercentComissionList from '@salesforce/apex/PriceEngineService.getPercentComissionList';
import getEditQuote from '@salesforce/apex/ResellerSKUEditController.getEditQuote';
import { updateRecord } from 'lightning/uiRecordApi';


const FIELDS = [
    'QuoteLineItem.Product2Id',
    'QuoteLineItem.Product_Name__c',
    'QuoteLineItem.Quantity',
    'QuoteLineItem.List_Price__c',
    'QuoteLineItem.Total_List_Price__c',
    'QuoteLineItem.UnitPrice',
    'QuoteLineItem.Subtotal',
    'QuoteLineItem.Product_Fee_type__c',
    'QuoteLineItem.Discount',
    'QuoteLineItem.CurrencyIsoCode',
    'QuoteLineItem.EM__c',
    'QuoteLineItem.Copay__c',
    'QuoteLineItem.PGP__c',
    'QuoteLineItem.Product2.Professional_services_types__c',
    'QuoteLineItem.Product2.Professional_Services_Type_Selection__c',
    'QuoteLineItem.OpportunityLineItem.Opportunity.B_M__c'];

export default class ResellerSkuInformation extends LightningElement {

    @track objectApiName = 'QuoteLineItem'

    @track sectionTitle = 'Product Information';

    @api recordId;

    
    quantity;

    @track product;
    quote;
    @track items;
    newPrices;
    value;
    @track totalComission;
    getRecordResponse;
    typeOptions = [];
    displayProfServicesOptions = false;
    displayCheckbox = false;
    displayRadio = false;
    profServicesTypeLabel = typeLabel;
    displayRadio = false;
    isIntermediation = false;
    businessModel;
    quoteItemId;
    opportunityId;

    @wire(getRecord, { recordId: '$recordId', fields: FIELDS })
    wiredRecord(response) {
        this.getRecordResponse = response;
        const { data, error } = response;
        if (error) {
            let message = 'Unknown error';
            if (Array.isArray(error.body)) {
                message = error.body.map(e => e.message).join(', ');
            } else if (typeof error.body.message === 'string') {
                message = error.body.message;
            }
            console.error(message);
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error loading product',
                    message: message,
                    variant: 'error',
                }),
            );
        } else if (data) {
            console.log('data ', data);
            this.product = {};
            this.product.Product_Name__c = data.fields.Product_Name__c.value;
            this.product.Product2Id = data.fields.Product2Id.value;
            this.product.Quantity = data.fields.Quantity.value;
            this.product.List_Price__c = data.fields.List_Price__c.value;
            this.product.Total_List_Price__c = data.fields.Total_List_Price__c.value;
            this.product.UnitPrice = data.fields.UnitPrice.value;
            this.product.Subtotal = data.fields.Subtotal.value;
            this.product.Product_Fee_type__c = data.fields.Product_Fee_type__c.value;
            this.product.Discount = data.fields.Discount.value / 100;
            this.product.CurrencyIsoCode = data.fields.CurrencyIsoCode.value;
            this.product.EM__c = data.fields.EM__c.value;
            this.product.Copay__c = data.fields.Copay__c.value;
            this.product.PGP__c = data.fields.PGP__c.value;
            this.quoteItemId = data.id;
            this.opportunityId = data.fields.OpportunityLineItem.value.fields.Opportunity.value.id;
            let stringTypes = data.fields.Product2.value.fields.Professional_services_types__c.value;
            let typeSelection = data.fields.Product2.value.fields.Professional_Services_Type_Selection__c.value;
            this.businessModel = data.fields.OpportunityLineItem.value.fields.Opportunity.value.fields.B_M__c.value;

            if(stringTypes && typeSelection && (typeSelection == 'Checkbox' || typeSelection == 'Radio')){
                this.displayProfServicesOptions = true;
                this.handleProfessionalServicesTypes(stringTypes);
                this.displayType(typeSelection);
            }
            this.checkBusinessModel();
            this.fnAsync();
            //this.getEditQuote();
            //this.getPercentComissionList();
            //this.totalComissionCalc();

        }
    }

    async fnAsync() {
        try {
            let opportunityId = this.opportunityId;
            const data = await getEditQuote({opportunityId: opportunityId});
            this.quote = data;
            console.log('quote1 ',this.quote);
            if(this.quote.Comission_Percent__c == null){
                this.value = 30;
            }else{
                this.value = this.quote.Comission_Percent__c;
            }
            if(this.quote.Total_comission__c == null){
                this.totalComissionCalc();
            }else{
                this.totalComission = this.quote.Total_comission__c;
            }

            this.getPercentComissionList();
            this.totalComissionCalc();
        } catch (error) {
            this.error = error;
            console.log('error');
        }

      }
      
    handleProfessionalServicesTypes(stringTypes){
        if(stringTypes){
            let psTypes = stringTypes.split(";");
            this.displayCheckbox = this.displayAsCheckboxOptions(psTypes);
            this.filterAndAutoFillTypes(psTypes);
        }
    }

    displayType(typeSelection){
        this.displayCheckbox = false;
        this.displayRadio = false;
        switch (typeSelection) {
            case 'Checkbox':
                this.displayCheckbox = true;
                break;
            case 'Radio':
                this.displayRadio = true;
            default:
                break;
        }
    }

    displayAsCheckboxOptions(psTypes){
        let displayCheckbox = true;
        if(psTypes.includes('Select only one')){
            displayCheckbox = false;
        }

        return displayCheckbox;
    }

    filterAndAutoFillTypes(psTypes){
        let types = psTypes.filter(type => !type.includes('Select'));
        types.forEach(type => {
            let optionType = {
                label: type,
                value: type + '__c',
                checked: this.product[type + '__c']
            }
            
            this.typeOptions.push(optionType);
        });
    }

    checkBusinessModel(){
        if(this.businessModel == 'Intermediation' && this.product.Product_Name__c == 'Enterprise Subscription'){
            this.isIntermediation = true;
            //this.getPercentComissionList();
            //this.totalComissionCalc();
        }
    }

    getEditQuote(){
        console.log('getEditQuote ');
        let opportunityId = this.opportunityId;
        //console.log('opportunityId ',opportunityId);
        getEditQuote({ opportunityId: opportunityId})
            .then((result) => {
                this.quote = result;
                console.log('quote1 ',this.quote);
                if(this.quote.Comission_Percent__c == null){
                    this.value = 30;
                }else{
                    this.value = this.quote.Comission_Percent__c;
                }
                if(this.quote.Total_comission__c == null){
                    this.totalComissionCalc();
                }else{
                    this.totalComission = this.quote.Total_comission__c;
                }
            })
            .catch(error => { 
                console.error(error);

        });
    }
        
    getPercentComissionList(){
        console.log('getPercentComissionList ');
        let quoteItemId = this.quoteItemId;
        getPercentComissionList({ quoteItemId: quoteItemId})
            .then((result) => {
                let info = JSON.parse(result);
                let options = [];
                let newPrices = [];
                console.log('info ',info);

                if(this.quote.Comission_Percent__c == null){
                    this.value = 30;
                }else{
                    this.value = this.quote.Comission_Percent__c;
                }
                if(this.quote.Total_comission__c == null){
                    this.totalComissionCalc();
                }else{
                    this.totalComission = this.quote.Total_comission__c;
                }

                for(var key in info){                  
                    options.push({ label: info[key].commissionPercentage.toString() + "%", value: info[key].commissionPercentage });
                    newPrices.push({ label: info[key].commissionPercentage, value: info[key].price });
                    //if(info[key].commissionPercentage.toString() == '30'){
                    //    this.value = info[key].commissionPercentage;
                    //}
                }
            console.log('options ',options);
            console.log('newPrices ',newPrices);
            //console.log('this.quote.Comission_Percent__c ',this.quote);
            this.items = options;
            this.newPrices = newPrices;
            })
            .catch(error => { 
                console.error(error);

        });
    }

    totalComissionCalc(){
        //console.log('this.product.Quantity ',this.product.Quantity)
        //console.log('this.product.List_Price__c ',this.product.List_Price__c)
        //console.log('this.value ',this.value / 100)
        this.totalComission = (this.product.Quantity * this.product.UnitPrice ) * this.value / 100;  
        //this.totalComission = ((this.product.Quantity * this.product.List_Price__c ) * this.value / 100) / 100;  

    }

    get productLink() {
        if(this.product)
            return '/' + this.product.Product2Id;
        else
            return '';
    }

    @api
    getProduct() {
        const product = {
            Id: this.recordId,
            UnitPrice: this.product.UnitPrice,
            List_Price__c: this.product.List_Price__c,
            Discount: this.product.Discount * 100,
            EM__c: this.product.EM__c,
            Copay__c: this.product.Copay__c,
            PGP__c: this.product.PGP__c,
            OpportunityLineItemId: this.opportunityId,
            Quantity: this.product.Quantity,
            Plan_Name__c: this.product.Product_Name__c
        };
        return product;
    }

    @api
    getQuote() {
            this.quote.Percent_of_comission__c = this.value.toString();
            this.quote.Comission_Percent__c = this.value;
        //console.log('this.quote salve ',this.quote)
        return this.quote;
    }

    @api
    reportValidity() {
        let salesPriceField = this.template.querySelector('[data-label="Sales Price"]');
        let profServicesValidity = this.checkProfServicesValidity();
        return salesPriceField.reportValidity() && profServicesValidity;
    }

    @api
    refreshProduct(){
        refreshApex(this.getRecordResponse);
        //updateRecord({ fields: { Id: this.recordId }})
    }

    checkProfServicesValidity(){
        let isValid = false;
        if(this.typeOptions && this.typeOptions.length > 0){
            for(const type of this.typeOptions){
                if(this.product[type.value]){
                    isValid = true;
                    break;
                }
            }
        }else{
            isValid = true;
        }

        return isValid;
    }

    updateDiscount(salesPrice) {
        let discount = (1 - (salesPrice / this.product.List_Price__c));
        if(discount >= 0 && discount !== Infinity) {
            this.product.Discount = discount;
        }
        else {
            this.product.Discount = 0.0;
        }
    }

    handleSalesPriceChange(event) {
        const salesPrice = event.detail.value;
        this.product.UnitPrice = salesPrice;
        //this.product.List_Price__c = salesPrice;
        this.product.Subtotal = salesPrice * this.product.Quantity;
        this.updateDiscount(salesPrice);
        this.totalComissionCalc();
    }

    handleListPriceChange(event) {
        const listPrice = event.detail.value;
        //this.product.UnitPrice = salesPrice;
        //this.product.List_Price__c = salesPrice;
        this.product.Total_List_Price__c = listPrice * this.product.Quantity;
        this.updateDiscount(salesPrice);
        //this.totalComissionCalc();
    }
    onCheckboxChangeTypeValue(event){
        const checked = event.target.checked;
        const value = event.target.value;
        this.product[value] = checked;
    }

    onRadioChangeTypeValue(event){
        const value = event.target.value;
        this.product[value] = true;
        this.updateValueOnRemainingTypes(value);
    }

    updateValueOnRemainingTypes(value){
        this.typeOptions.forEach(type => {
            if(type.value != value){
                this.product[type.value] = false;
            }
        });
    }

    handleChange(event) {
        // Get the string of the "value" attribute on the selected option
        var UnitPrice;
        var List_Price__c;
        this.newPrices.forEach(function (item) {
            if(event.detail.value == item.label){
                UnitPrice = item.value;
                List_Price__c = item.value;
            }
         });
        this.product.List_Price__c = List_Price__c;
        this.product.UnitPrice = UnitPrice;
        this.product.Total_List_Price__c = List_Price__c * this.product.Quantity;
        this.product.Subtotal = UnitPrice * this.product.Quantity;
        //console.log('this.value ',this.value);
        //console.log('event.detail.value ',event.detail.value);
        this.value = parseInt(event.detail.value);
        //this.value = event.detail.value;
        this.totalComissionCalc();
        //this.handleListPriceChange();
        //this.handleSalesPriceChange();       
        //console.log('this.value ',this.value);
    }

      
}