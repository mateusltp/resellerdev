import { LightningElement, api } from 'lwc';

import { FlowAttributeChangeEvent } from 'lightning/flowSupport';

import fetchDefaultValue from '@salesforce/apex/ResellerEJContactLookupController.fetchDefaultValue';
import apexContactSearch from '@salesforce/apex/ResellerEJContactLookupController.searchContact';

export default class ResellerEJFlowContactLookup extends LightningElement { 
    
    //-- configurable parameters
    @api isMultiSelect = false;
    @api recordId;
    @api accountId = '';
    @api lookupLabel = '';
    @api lookupPlaceholder = '';

    @api searchField = 'Name';

    @api selectedRecordId;
    @api lstSelectedRecordIds = [];

    defaultResult = {};
    listOfDefaultResults = [];
    
    connectedCallback() {
        this.getDefaultValues();
    }

    getDefaultValues() {
        fetchDefaultValue({
            recordId: this.recordId,
        })
        .then((result) => {
            this._searchedPreviousRecordMap = result.reduce((res, item) => {
                res[item.Id] = item;
                return res;
            }, {});

            this.listOfDefaultResults = result.map(item => {
                return {
                    id: item.Id,
                    icon : 'standard:contact',
                    sObjectType : 'Contact',
                    title: this.getFieldValue(this.searchField, item),
                };
            });

            this._previousRecordItemMap = this.listOfDefaultResults.reduce((res, item) => {
                res[item.id] = item;
                return res;
            }, {});

            //-- adding the ids to the selectedIds list
            this.listOfDefaultResults.forEach(element => {
                const selectedRecord = this._searchedPreviousRecordMap[element.id];
                this.lstSelectedRecordIds = this.lstSelectedRecordIds.concat(element.id);
            });

        })
        .catch((error) => {
            this.error = error;
            // this.hideSpinner();
        });
    }

    handleContactSearch(event) {
        const lookupElement = event.target;
        
        apexContactSearch({ 
                searchTerm: event.detail.searchTerm,
                selectedIds: event.detail.selectedIds,
                accountId: this.accountId
            })
            .then(results => {
                lookupElement.setSearchResults(results);
            })
            .catch(error => {
                reduceErrors(error);
            });
    }

    handleSelectedRecords(event) {
        //-- the custom lookup already updates the current selection
        this.lstSelectedRecordIds = event.detail;

        const attributeChangeEvent = new FlowAttributeChangeEvent(
            'lstSelectedRecordIds',
            event.detail
        );
        this.dispatchEvent(attributeChangeEvent);
    }

    getFieldValue(fieldName, obj) {
        if (fieldName) {
            return fieldName.split('.').reduce((result, item) => {
                return result[item];
            }, obj);
        }
        return '';
    }
}