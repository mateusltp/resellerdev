import { LightningElement, api } from 'lwc';
import pubsub from 'c/pubsub';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class DatatablePicklist extends LightningElement {
    @api label;
    @api placeholder;
    @api options;
    @api value;
    @api context;
    @api selectedRows;

    hasRowsSelected = false;
    validationTypeValues = [];

    connectedCallback() {
        this.initializeValue();
        this.populatedPartnerLevelValues();
        this.callRowSelectionSubscriber();
        this.updateValue();
    }

    initialized = false;
 
    renderedCallback() {
        if (this.initialized) {
            return;
        }
        this.initialized = true;
        let listId = this.template.querySelector('datalist').id;
        this.template.querySelector("input").setAttribute("list", listId);
    }

    get rowsSelectedLength() {
        if (this.selectedRows != undefined && this.selectedRows.length > 0)
            return this.selectedRows.length
    }

    handleChange(event) {
        console.log('on pick handle change');
        //show the selected value on UI

        if (event.target.value == '') {
            this.showToastNotification('Error', 'The partner level cannot be empty, plese insert a valid value', 'error');
            return;
        }

        this.value = event.target.value;
        
        if (this.selectedRows != undefined && this.selectedRows.length > 0) {
            this.hasRowsSelected = true;
        }
        else {
            this.dispatchPicklistChanged();
        }
    }

    callRowSelectionSubscriber(){
        console.log('call callRowSelectionSubscriber');
        pubsub.subscribe('rowSelection', (message)=>{
            this.selectedRows = message;
            
            if (this.selectedRows.length < 1) {
                this.hasRowsSelected = false;
            }
        })
    }

    updateValue(){
        pubsub.subscribe('updateValue', (message)=>{            
            console.log('updateValue');
            console.log(message);
            console.log(this.selectedRows);
            this.selectedRows.forEach(row => {
                console.log(row.Id);
                console.log(this.context);
                if((row.Id == this.context) && (row.Id != message.Context)) {
                    this.value = message.Data.PartnerLevel;
                    this.dispatchPicklistChanged();
                }
            });
        });
    }

    handleCancel() {
        this.hasRowsSelected = false;
    }

    handleApply(event) {
        let myComboboxValue = this.template.querySelector('input').value;
        let myCheckboxValue = this.template.querySelector(["lightning-input[data-my-id=in1]"]).checked;

        let data = {
            validationType: myComboboxValue
        };

        let message = {
            Data: data,
            Context: this.context
        }
        
        if (myCheckboxValue == true) {
            this.publishUpdateValue(message);
            //this.publishHandler(data);
            this.dispatchEvent(new CustomEvent('picklistchanged', {
                composed: true,
                bubbles: true,
                cancelable: true,
                detail: {
                    data: {
                        context: this.context, value: myComboboxValue 
                    }
                }
            }));
        }
    }

    /*
    publishHandler(data){
        console.log('From publishHandler picklist');
        pubsub.publish('networkMassUpdate', data);
    }
    */

    publishUpdateValue(message){
        console.log('From publishUpdateValue picklist');
        this.hasRowsSelected = false;
        pubsub.publish('updateValue', message);
    }

    populatedPartnerLevelValues() {
        this.options.forEach(option => {
            this.validationTypeValues.push(option.value);
        });
    }

    handleCheck(event) {
        console.log('handleCheck');
        console.log(event.target.checked);
    }

    dispatchPicklistChanged() {
        //fire event to send context  and selected value to the data table
        this.dispatchEvent(new CustomEvent('picklistchanged', {
            composed: true,
            bubbles: true,
            cancelable: true,
            detail: {
                data: {
                    context: this.context, value: this.value 
                }
            }
        }));
    }

    handleKeyUp(event) {
        let value = event.target.value;

        if (this.validationTypeValues.includes(value) && (this.selectedRows == undefined || this.selectedRows.length == 0)) {
            this.value = value;
            this.dispatchPicklistChanged();
        }

        if (this.validationTypeValues.includes(value) && this.selectedRows != undefined && this.selectedRows.length > 0) {
            this.handleChange(event);
        }
    }

    showToastNotification(title, message, variant){
        const evt = new ShowToastEvent({ title: title, message: message, variant: variant });
        this.dispatchEvent(evt);
    }

    initializeValue() {
        if (this.value == undefined) {
            this.value = '';
        }
    }
}

// import { LightningElement, api } from 'lwc';

// export default class DatatablePicklist extends LightningElement {
//     @api label;
//     @api placeholder;
//     @api options;
//     @api value;
//     @api context;

//     handleChange(event) {
//         //show the selected value on UI
//         this.value = event.detail.value;

//         //fire event to send context and selected value to the data table
//         this.dispatchEvent(new CustomEvent('picklistchanged', {
//             composed: true,
//             bubbles: true,
//             cancelable: true,
//             detail: {
//                 data: { context: this.context, value: this.value }
//             }
//         }));
//     }

// }