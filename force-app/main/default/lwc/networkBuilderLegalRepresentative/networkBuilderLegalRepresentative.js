/**
 * Created by gympasser on 11/03/2022.
 */

import { LightningElement, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { createRecord, updateRecord } from 'lightning/uiRecordApi';
import apexSearch from '@salesforce/apex/NetworkBuilderEditAllController.search';
import getContactRecordTypeId from '@salesforce/apex/NetworkBuilderEditAllController.getRecordTypeId';
import saveDataExistingContact from '@salesforce/apex/NetworkBuilderEditAllController.saveDataExistingContact';
import saveDataNewContact from '@salesforce/apex/NetworkBuilderEditAllController.saveDataNewContact';
import ACCOUNT_CONTACT_RELATION_OBJECT from '@salesforce/schema/AccountContactRelation';
import { reduceErrors } from 'c/ldsUtils';

export default class NetworkBuilderLegalRepresentative extends LightningElement {
    @api selectedAccountsId = [];

    selectedOption;
    showSelector = true; // switch to true when more options are implemented
    showLegalRepSection = false; // switch to false when more options are implemented
    showCMSInfoSection = false;
    showExclusivitySection = false;
    showLegalInfoSection = false;
    showSpinner = false;
    showCreateNewContactModal = false;

    legalRepresentativeContactId;
    contactRecordTypeId;
    newContact;

    errorAdmin = 'Please give this error message to your admin.';

    connectedCallback() {
        console.log('ids selecionados ' + this.selectedAccountsId);
    }

    headersMap = {
        selection: 'Edit all',
        cmsInformation: 'CMS information',
        exclusivity: 'Exclusivity',
        legalInformation: 'Legal information',
        legalRepresentative: 'Legal representative',

    };

    get radioOptions() {
        return [
            { label: 'CMS information', value: 'cmsInformation' },
            { label: 'Exclusivity', value: 'exclusivity' },
            { label: 'Legal information', value: 'legalInformation' },
            { label: 'Legal representative', value: 'legalRepresentative' }
        ];
    }

    get header() {
        if (this.showLegalRepSection) {
            return this.headersMap.legalRepresentative;
        } else if (this.showCMSInfoSection) {
            return this.headersMap.cmsInformation;
        } else if (this.showExclusivitySection) {
            return this.headersMap.exclusivity;
        } else if (this.showLegalInfoSection) {
            return this.headersMap.legalInformation;
        } else {
            return this.headersMap.selection;
        }
    }

    get isFirstScreen() {
        if (!this.showLegalRepSection && !this.showCMSInfoSection && !this.showExclusivitySection && !this.showLegalInfoSection) {
            return true;
        } else {
            return false;
        }
    }

    get secondButtonLabel() {
        if (!this.showLegalRepSection && !this.showCMSInfoSection && !this.showExclusivitySection && !this.showLegalInfoSection) {
            return this.buttonsMap.next;
        } else {
            return this.buttonsMap.save;
        }
    }

    get selectedAccount() {
        return this.selectedAccountsId[0];
    }

    get hasNewContact() {
        return this.newContact !== undefined;
    }

    get getOpacity() {
        return 'slds-table c-width-inherit ' + (this.legalRepresentativeContactId ? 'c-opacity-half' : '')
    }

    handleRadioSelect(event) {
        const selectedOption = event.target.value;
        this.selectedOption = selectedOption;
    }

    handleSearch(event) {
        const lookupElement = event.target;
        apexSearch(event.detail)
            .then(results => {
                lookupElement.setSearchResults(results);
            })
            .catch(error => {
                // TODO: handle error
            });
    }

    handleSearchContactLookup(event) {
        this.legalRepresentativeContactId = event.detail[0];
    }

    handleContinue() {
        this.enableSpinner();
        if (this.selectedOption == 'cmsInformation') {
            this.showSelector = false;
            this.showCMSInfoSection = true;
            this.disableSpinner();
        } else if (this.selectedOption == 'exclusivity') {
            // this.showSelector = false;
            this.showExclusivitySection = true;
            this.disableSpinner();
        } else if (this.selectedOption == 'legalInformation') {
            this.showSelector = false;
            this.showLegalInfoSection = true;
            this.disableSpinner();
        } else if (this.selectedOption == 'legalRepresentative') {
            this.showSelector = false;
            this.showLegalRepSection = true;
            this.disableSpinner();
        }
    }

    handleSave() {
        if (this.legalRepresentativeContactId) {
            this.enableSpinner();
            saveDataExistingContact({
                accountIds: this.selectedAccountsId,
                selectedLegalRepContactId: this.legalRepresentativeContactId
            })
                .then((result) => {
                    this.disableSpinner();
                    this.showToastNotification('Legal Representative updated!', 'The legal representatives were updated successfully.', 'success');
                    this.closeModal();
                }).catch((error) => {
                    let err = reduceErrors(error);
                    console.log('erro: ' + JSON.stringify(err));
                    this.showToastNotification('An error occurred!', this.errorAdmin + ' ' + reduceErrors(error), 'warning');
                    this.disableSpinner();
                }
                );
        } else if (this.newContact) {
            this.enableSpinner();
            saveDataNewContact({
                accountIds: this.selectedAccountsId,
                newContactSerialized: JSON.stringify(this.newContact)
            })
                .then((result) => {
                    this.disableSpinner();
                    this.showToastNotification('Legal Representative updated!', 'The legal representatives were updated successfully.', 'success');
                    this.closeModal();
                }).catch((error) => {
                    let err = reduceErrors(error);
                    console.log('erro: ' + JSON.stringify(err));
                    this.showToastNotification('An error occurred!', this.errorAdmin + ' ' + reduceErrors(error), 'warning');
                    this.disableSpinner();
                }
                );
        } else {
            this.showToastNotification('No contact selected.', 'You must select a contact or create a new one in order to save.', 'warning');
        }
    }


    closeModal() {
        console.log('On close modal');
        const myEvent = new CustomEvent('close');
        this.dispatchEvent(myEvent);
    }

    handleGoBack() {
        console.log('On go back');
        const myEvent = new CustomEvent('goback');
        this.dispatchEvent(myEvent);
    }

    handleCloseCreateNewContactModal() {
        this.showCreateNewContactModal = false;
    }

    handleSaveContact(event) {
        this.enableSpinner();
        this.newContact = event.detail;
        console.log('new contact: ' + JSON.stringify(this.newContact));
        this.disableSpinner();
    }

    openCreateNewContactModal() {
        this.enableSpinner();
        getContactRecordTypeId({ recordTypeDevName: 'Gyms_Partner' }).then((result) => {
            this.contactRecordTypeId = result;
            this.showCreateNewContactModal = true;
        }).catch((error) => {
            let err = reduceErrors(error);
            console.log('erro: ' + JSON.stringify(err));
            this.showToastNotification(this.errorAdmin + ' ' + err);
        });
        this.disableSpinner();
    }


    populateContactRelationInput_Existing(accountId) {
        let contactRelationInput = this.recordInputForCreate(ACCOUNT_CONTACT_RELATION_OBJECT.objectApiName);
        contactRelationInput.fields['ContactId'] = this.legalRepresentativeContactId
        contactRelationInput.fields['AccountId'] = accountId;
        contactRelationInput.fields['Type_of_Contact__c'] = 'Legal Representative';
        return contactRelationInput;
    }

    populateAccountUpdateLegalRep_Existing(accountId) {
        let accountInput = this.recordInputForUpdate();
        accountInput.fields['Id'] = accountId;
        accountInput.fields['Legal_Representative__c'] = this.legalRepresentativeContactId;
        return accountInput;
    }

    recordInputForCreate(objApiName) {
        let recordInput = {
            "apiName": objApiName,
            "fields": {
            }
        }
        return recordInput;
    }

    recordInputForUpdate() {
        let recordInput = {
            "fields": {
            }
        }
        return recordInput;
    }

    enableSpinner() {
        this.showSpinner = true;
    }

    disableSpinner() {
        this.showSpinner = false;
    }

    showToastNotification(title, message, variant) {
        const evt = new ShowToastEvent({ title: title, message: message, variant: variant });
        this.dispatchEvent(evt);
    }

    closeModal() {
        console.log('On close modal');
        const myEvent = new CustomEvent('close');
        this.dispatchEvent(myEvent);
    }

}