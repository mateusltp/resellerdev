import { LightningElement, api, track } from 'lwc';

import RadioButtonHelper from "./genericRadioButtonInputLWCHelper";

export default class GenericRadioButtonInputLWC extends LightningElement {

    @api label = '';
    @api value = '';
    @api name = 'CustomRadioGroup';
    @api options = [];
    @api orientation;
    @track options_ = [];
    @track value_ = '';

    @api availableOptions;

    helper = new RadioButtonHelper();

    connectedCallback() {
        console.log("### Entered the connected callback");
        console.log(this.availableOptions);
        this.value_ = this.value;

        this.options = this.helper.jsonToMap(this.availableOptions);

        console.log(this.options);
        this.options.forEach( (option,index) => {
            console.log("### option");
            console.log(option);
            //console.log(option.get(key));

            console.log("### index");
            console.log(index);

            let option_ = JSON.parse(JSON.stringify(option));
            if (this.value == option_.value ) {
                option_.checked = true;
            }
            option_.key = index;
            option_.label = option.get(0).label;
            option_.value = option.value;
            option_.inputClass = `radio-input-${index}`;

            console.log("### option_");
            console.log(option_);
            this.options_.push(option_);

            console.log("### this.options_");
            console.log(this.options_);
        });
    }

    handleClick(event) {
        let key = event.currentTarget.dataset.key;
        if ( key == undefined || key == null){
        return;
        }
        let input = this.template.querySelector(`.radio-input-${key}`);
        if (input == undefined){
        return;
        }
        console.log(`key ${key} value ${input.value} `);
        //make the input checked
        input.checked = true;
        this.value_ = input.value;
        
        this.dispatchChange(input.value);
    }

    get radioClass() {
        return `slds-radio ${this.orientation == 'horizontal' ? 'horizontal' : ''}`;
    }

    dispatchChange(value) {
        const change = new CustomEvent('change', {
        detail: { value:value }
        });
        this.dispatchEvent(change);
    }

}