export default class GenericRadioButtonInputLWCHelper {

    jsonToMap(jsonString) {
        console.log("### Entered the helper's jsonToMap");
        console.log(jsonString);
        var resultMap = new Map();
        
        if(!jsonString) {
            return resultMap;
        }
        console.log("### CHECKED POINT 0 ###");
        var jsonObject = JSON.parse(jsonString);
        console.log("### CHECKED POINT 1 ###");
        
        //var dataObject = jsonObject.fields;
        var dataObject = jsonObject;
        console.log("### CHECKED POINT 2 ###");

        var dataMap = new Map(Object.entries(dataObject));
        console.log("### CHECKED POINT 3 ###");
        
        for (const key of dataMap.keys())  {
            var keyMap = new Map(Object.entries(dataMap.get(key)));
            resultMap.set(key, keyMap);
        }
        console.log(resultMap);
        return resultMap;
    }

    /*
    [{"label":"Yes","value":"Yes","byline":"we have an email address"},{"label":"No","value":"No","byline":"looks like a no go"}]
    */
}