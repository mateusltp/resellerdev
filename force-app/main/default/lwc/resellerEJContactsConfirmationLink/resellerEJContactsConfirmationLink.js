import { LightningElement, api } from 'lwc';

import { FlowAttributeChangeEvent, FlowNavigationNextEvent} from 'lightning/flowSupport';

export default class ResellerEJContactsConfirmationLink extends LightningElement {
    
    @api linkLabel = 'List of Contacts';

    @api confirmContactsLinkAction;

    handleClick() {
        this.confirmContactsLinkAction == 'confirm';
        
        const attributeChangeEvent = new FlowAttributeChangeEvent(
            'confirmContactsLinkAction',
            this.confirmContactsLinkAction
        );
        this.dispatchEvent(attributeChangeEvent);
        
        this.dispatchNextEvent();
    }

    dispatchNextEvent() {
        const navigateNextEvent = new FlowNavigationNextEvent();
        this.dispatchEvent(navigateNextEvent);
    }
}