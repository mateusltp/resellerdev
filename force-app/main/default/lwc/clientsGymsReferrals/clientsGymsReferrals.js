import { LightningElement, wire, api } from 'lwc';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import LEAD_OBJECT from '@salesforce/schema/Lead';
import { NavigationMixin } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class ClientsGymsReferrals extends NavigationMixin(LightningElement) {
    @api recordId;

    // get record type info
    @wire(getObjectInfo, { objectApiName: LEAD_OBJECT })
    objectInfo;

    get recordTypeId() {
        if (this.objectInfo) {
            if (this.objectInfo.data) {
                if (this.objectInfo.data.recordTypeInfos) {
                    const rtis = this.objectInfo.data.recordTypeInfos;
                    return Object.keys(rtis).find((rti) => rtis[rti].name === "Direct Channel");
                }
            }
        }
        return null;
    }

    handleSuccess(event) {
        console.log('onsuccess event recordEditForm', event.detail.id)
    }
    handleSubmit(event) {
        this.template.querySelector('lightning-record-edit-form').submit();
        console.log('onsubmit event recordEditForm' + event.detail.fields);
        //showtoast
        const evt = new ShowToastEvent({
            title: 'Success!',
            message: 'Gym referral was created!',
            variant: 'success',
            mode: 'dismissable'
        });
        this.dispatchEvent(evt);
        // Navigation to Lead List view(recent)
        this[NavigationMixin.Navigate]({
            type: 'standard__objectPage',
            attributes: {
                objectApiName: 'Lead',
                actionName: 'list'
            },
            state: {
                filterName: 'Recent'
            },
        });

    }

    closeModal() {
        console.log('On close modal');
        const myEvent = new CustomEvent('close');
        this.dispatchEvent(myEvent);
        // Navigation to Lead List view(recent)
        this[NavigationMixin.Navigate]({
            type: 'standard__objectPage',
            attributes: {
                objectApiName: 'Lead',
                actionName: 'list'
            },
            state: {
                filterName: 'Recent'
            },
        });
    }
}