import { LightningElement, api } from 'lwc';

export default class ApprovalBadges extends LightningElement {
    @api itemsNeedApproval;
    @api quoteRecord;
    @api isDealDeskitems;
    @api listItemsNeedApproval;

    connectedCallback() {
        if( this.itemsNeedApproval == null || this.isDealDeskitems == null ){ return; }

        this.listItemsNeedApproval = this.isDealDeskitems ? this.getDealDeskBadges() :
            this.getCommercialApprovalBadges();
    }

    getDealDeskBadges(){
        let lstDealDeskConditions = JSON.parse( this.itemsNeedApproval );

        if( !lstDealDeskConditions || !lstDealDeskConditions.length ){ 
            return [{
                conditionCode: 'NoNeedCondition', 
                approvalCondition: 'No Need',
                class: 'slds-theme_success slds-m-bottom_small' }];
        }

        lstDealDeskConditions.forEach(condition => {
            condition.class = ( ( condition.isApproved ? 'slds-theme_success' : 'slds-theme_error' ) + ' slds-m-bottom_small' );
        });

        return lstDealDeskConditions;
    }

    getCommercialApprovalBadges(){
        let listItems = [];
        let item;

        if( this.quoteRecord.Discount_Approval_Level__c && this.quoteRecord.Discount_Approval_Level__c > 0 ){
            item = {};
            item.label = 'Discount out of range';
            item.class = 'slds-theme_' + (this.quoteRecord.Total_Discount_approved__c ? 'success' : 'error' ) + ' slds-m-bottom_small';
            listItems.push( item );
        } 

        if( this.quoteRecord.Waiver_Approval_Level__c && this.quoteRecord.Waiver_Approval_Level__c > 0 ){
            item = {};
            item.label = 'Waiver or temporary discount';
            item.class = 'slds-theme_' + (this.quoteRecord.Waiver_approved__c ? 'success' : 'error' ) + ' slds-m-bottom_small';
            listItems.push( item );
        }

        if( listItems.length == 0 ){
            item = {};
            item.label = 'No Need';
            item.class = 'slds-theme_success slds-m-bottom_small';
            listItems.push( item );
        }

        return listItems;
    }
}