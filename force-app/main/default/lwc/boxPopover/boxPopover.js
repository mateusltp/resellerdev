/**
 * @description       : Display Popover with Lightning Record View Form
 * @author            : tiago.ribeiro@gympass.com
 * @group             : 
 * @last modified on  : 06-07-2022
 * @last modified by  : tiago.ribeiro@gympass.com
**/
import { LightningElement, api, track } from 'lwc';
import waiversLabel from '@salesforce/label/c.waiverDesignation';

export default class BoxPopover extends LightningElement {
    @track ranger;
    @track waiversList = [];
    @api iconname;
    @api objectname;
    @api titlefield;
    @api fields;
    fieldsList = [];
    waiversLength = 0;
    label = {
        waiversLabel
    }

    @api
    get myranger(){
        return this.ranger;
    }

    set myranger(value) {
        this.ranger = value;
    }

    @api
    get mywaivers(){
        return this.waiversList;
    }

    set mywaivers(value) {
        this.waiversList = value;
        this.waiversLength = this.assignWaiverListLength();
    }

    assignWaiverListLength(){
        return this.waiversList && this.waiversList.length > 0;
    }

    connectedCallback(){
        this.fieldsList = this.splitToString(this.fields);
    }

    splitToString(fieldsString){
        return fieldsString.split(',');
    }
}