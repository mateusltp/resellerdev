import { LightningElement, api } from 'lwc';

const CSS_CLASS = 'modal-hidden';

export default class GenericModal extends LightningElement {

    privateShowModal = false;

    @api
    get showModal() {
        return this.privateShowModal;
    }
    set showModal(value) {
        this.privateShowModal = value;
    }

    @api showSpinner = false;

    hasHeaderString = false;
    privateHeader;

    @api
    get header() {
        return this.privateHeader;
    }
    set header(value) {
        this.hasHeaderString = value !== '';
        this.privateHeader = value;
    }

    @api size;

    @api zIndex;

    @api height;


    get modalClass() {
        let result = 'slds-modal';
        result = this.size ? result + ' slds-modal_' + this.size : result;
        result = this.privateShowModal ? result + " slds-fade-in-open" : result;
		return result;
	}

    get modalBackdropClass() {
		return `slds-backdrop ${this.privateShowModal ? "slds-backdrop_open" : ""}`;
	}

    handleClose() {
        const evt = new CustomEvent('close');
        this.dispatchEvent(evt);
    }

    renderedCallback() {
        if(this.zIndex) {
            this.template.querySelector('section')
            .style.zIndex = this.zIndex;
        }
        if(this.height) {
            this.template.querySelector('.slds-modal__content')
            .style.height = this.height + '%';
        }
    }

}