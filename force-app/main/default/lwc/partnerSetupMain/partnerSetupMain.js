import { LightningElement, api, wire, track } from 'lwc';
import getAccounts from '@salesforce/apex/PartnerSetupController.getAccountsFromContract';
import updateRecords from '@salesforce/apex/PartnerSetupController.saveAccountRecords';
import { reduceErrors } from 'c/ldsUtils';
import { refreshApex } from '@salesforce/apex';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { getRecord, getFieldValue, updateRecord } from 'lightning/uiRecordApi';
import OPPORTUNITY_FIELD from '@salesforce/schema/Ops_Setup_Validation_Form__c.Opportunity__c';
import OPPORTUNITY_SETUP_STATUS_FIELD from '@salesforce/schema/Opportunity.Setup_Status__c';
import ID_FIELD from "@salesforce/schema/Opportunity.Id";

const VALIDATIONTYPEOPTIONS = [
    {value: 'Smart Check-in (SCI)', label: 'Smart Check-in (SCI)'},
    {value: 'Regular Check-in', label: 'Regular Check-in'}
];

const actions = [
    { label: 'Edit', name: 'edit' }
];

//action:approval
const COL_OTHER = [  
    { label: 'Partner Name', fieldName: 'partnerName', type: 'text', linkLabel: 'partnerName',  editable: true,  sortable: true, sortBy: 'partnerName', resizable: true },
    { label: 'Validation Type', fieldName: 'partnerValidationType', type: 'picklist', options: VALIDATIONTYPEOPTIONS, resizable: true, editable: true},
    { label: 'Website', fieldName: 'partnerWebsite', type: 'text', linkLabel: 'partnerWebsite', editable: true, resizable: true},
    { label: 'Email', fieldName: 'partnerEmail', type: 'text', editable: true, resizable: true},
    { label: 'Phone', fieldName: 'partnerPhone', type: 'text', editable: true, resizable: true},
    { label: 'Bank Account', fieldName: 'partnerHasBankAccount', type:'simple-icon', simpleIconName: 'action:approval', simpleIconAltText: 'Ready', simpleIconSize:'xx-small', simpleIconTitle: 'Bank Account is ready', resizable: true },
    { label: 'Opening hours', fieldName: 'partnerHasOpeningHours', type:'simple-icon', simpleIconName: 'action:approval', simpleIconAltText: 'Ready', simpleIconSize:'xx-small', simpleIconTitle: 'Opening hours is ready',  resizable: true},
    { label: 'Description',fieldName: 'partnerHasDescription', type:'simple-icon', simpleIconName: 'action:approval', simpleIconAltText: 'Ready', simpleIconSize:'xx-small', simpleIconTitle: 'Description is ready', resizable: true},
    { label: 'Photos',fieldName: 'partnerHasPhotos', type:'simple-icon', simpleIconName: 'action:approval', simpleIconAltText: 'Ready', simpleIconSize:'xx-small', simpleIconTitle: 'Photos is ready', resizable: true},
    { label: 'Logo', fieldName: 'partnerHasLogo', type:'simple-icon', simpleIconName: 'action:approval', simpleIconAltText: 'Ready', simpleIconSize:'xx-small', simpleIconTitle: 'Logo is ready', resizable: true},
    { label: 'Progress', fieldName: 'score', type: 'menu-button', actions: actions}
];
const COL_US = [  
    { label: 'Partner Name', fieldName: 'partnerName', type: 'text', linkLabel: 'partnerName',  editable: true,  sortable: true, sortBy: 'partnerName', resizable: true},
    { label: 'Validation Type', fieldName: 'partnerValidationType', type: 'picklist', options: VALIDATIONTYPEOPTIONS, resizable: true, editable: true},
    { label: 'Website', fieldName: 'partnerWebsite', type: 'text', linkLabel: 'partnerWebsite', editable: true, resizable: true},
    { label: 'Email', fieldName: 'partnerEmail', type: 'text', editable: true, resizable: true},
    { label: 'Phone', fieldName: 'partnerPhone', type: 'text', editable: true, resizable: true},
    { label: 'Bank Account', fieldName: 'partnerHasBankAccount', type:'simple-icon', simpleIconName: 'action:approval', simpleIconAltText: 'Ready', simpleIconSize:'xx-small', simpleIconTitle: 'Bank Account is ready', resizable: true },
    { label: 'Opening hours', fieldName: 'partnerHasOpeningHours', type:'simple-icon', simpleIconName: 'action:approval', simpleIconAltText: 'Ready', simpleIconSize:'xx-small', simpleIconTitle: 'Opening hours is ready',  resizable: true},
    { label: 'Description',fieldName: 'partnerHasDescription', type:'simple-icon', simpleIconName: 'action:approval', simpleIconAltText: 'Ready', simpleIconSize:'xx-small', simpleIconTitle: 'Description is ready', resizable: true},
    { label: 'Photos',fieldName: 'partnerHasPhotos', type:'simple-icon', simpleIconName: 'action:approval', simpleIconAltText: 'Ready', simpleIconSize:'xx-small', simpleIconTitle: 'Photos is ready', resizable: true},
    { label: 'Logo', fieldName: 'partnerHasLogo', type:'simple-icon', simpleIconName: 'action:approval', simpleIconAltText: 'Ready', simpleIconSize:'xx-small', simpleIconTitle: 'Logo is ready', resizable: true},
    { label: 'W9', fieldName: 'partnerHasW9' ,type:'simple-icon', simpleIconName: 'action:approval', simpleIconAltText: 'Ready', simpleIconSize:'xx-small', simpleIconTitle: 'W9 is ready', resizable: true},
    { label: 'Progress', fieldName: 'score', type: 'menu-button', actions: actions}
];

const PAGESIZEOPTIONS = [5,10,20,40];
const FIELDS = [OPPORTUNITY_FIELD];
const SETUP_FIELD = [OPPORTUNITY_SETUP_STATUS_FIELD];

export default class PartnerSetupMain extends LightningElement {

    @api opportunityId;
    
    @api recordId;
    @api accountIdFromRow;
    @track optionValue;
    error;
    columns;
    rawData;
    data;
    accs;   
    showTable = false; //Used to render table after we get the data from apex controller    
    pageSizeOptions = PAGESIZEOPTIONS;
    isLoading = true;
    loadMessage = 'Loading...';
    selectedRecords = [];
    totalRecords;
    totalRecordsReady = 0;
    showEditMenu;
    showEditSingleAccount;
    baseScore = 5;
    isUs;

    @wire(getRecord, { recordId: '$recordId', fields: FIELDS }) receiveFormData(result) {
        if (result.error) {
            this.dispatchToast('Error', 'error', reduceErrors(result.error)[0]);
            this.isLoading = false;
        } else if (result.data) {
            this.opportunityId = getFieldValue(result.data, OPPORTUNITY_FIELD);
        }
    }


    @wire(getRecord, { recordId: '$opportunityId', fields: SETUP_FIELD })
    opportunity;

    @wire( getAccounts, {opportunityId : '$opportunityId'} ) accountsInContract( result ) {
        this.showTable = false;
        this.totalRecordsReady  = 0;
        this.loadMessage = 'Loading...';
        this.isLoading = true;
        this.accs = [];        
        this.rawData = result;
        if (result.data) {           
            this.data = result.data;
            this.totalRecords = this.data.length;
            for(let i=0; i<this.data.length; i++){        
                let obj = {...this.data[i]};
                obj.accName = this.data[i].partnerName ? this.data[i].partnerName : "";             
                this.isUs = this.data[i].countryCode === 'US' ? true : false;
                if(this.isUs){
                    this.columns = COL_US;
                    this.baseScore = 10;
                } else {
                    this.columns = COL_OTHER;
                    this.baseScore = 9;
                }
                if(this.data[i].score == this.baseScore){
                    this.totalRecordsReady++;
                }
                obj.score = this.data[i].score + '/' + this.baseScore;
                this.accs.push(obj);

            }
            this.showTable = true;
            this.isLoading = false;
            this.checkOpportunityUnlock();     
        } else if (result.error) {
            this.dispatchToast('Error', 'error', reduceErrors(result.error)[0]);
            this.isLoading = false;
        }
    };

    handleRowSelection(event){
        this.selectedRecords = event.detail;
    }

    saveRecords(event){
        this.loadMessage = 'Saving...';
        this.isLoading = true;
        this.error = '';
        updateRecords({partnerRecords: JSON.stringify(event.detail)})
        .then(response=>{
            this.dispatchToast('All good', 'success', 'Your changes have been saved.');
            this.refreshData();
        })
        .catch(error=>{
            this.dispatchToast('Error', 'error', reduceErrors(error)[0]);
            this.isLoading = false;
        });
    }

    dispatchToast(title, variant, message) {
        let successtEvnt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant
        });
        this.dispatchEvent(successtEvnt);
    }

    refreshData(){
        this.selectedRecords = [];
        this.showLastOptionAfterRefresh();
        this.disableScreen();      
        setTimeout(() => {
            refreshApex(this.rawData);
            this.enableScreen();                            
        }, 1000);
    }

    showLastOptionAfterRefresh(){ 
        if( this.optionValue != undefined ) { 
            var uniq = this.pageSizeOptions.filter(row => !this.optionValue.includes(row));
            uniq = uniq.sort(this.sortNumberOptions);
            this.pageSizeOptions = [this.optionValue, ...uniq];
        }     
    }

    sortNumberOptions(a, b){
        return a - b;
    }

    disableScreen(){  
        this.showTable = false;
        this.isLoading = true;
    }

    checkOpportunityUnlock(){
  
        let oppStatus = getFieldValue(this.opportunity.data, OPPORTUNITY_SETUP_STATUS_FIELD);
        console.log('oppStatus ' + oppStatus);
        if(this.totalRecordsReady == this.totalRecords){
            if( oppStatus != 'Completed'){
                this.isLoading = true;
                this.loadMessage = 'Unlocking opportunity...';
                this.updateOpportunity('Completed');
            }
        } else {            
            if( oppStatus == 'Completed' ){
                this.updateOpportunity('In progress');
            }
        }

    }

    updateOpportunity(status){
        const fields = {};
        fields[ID_FIELD.fieldApiName] = this.opportunityId;
        fields[OPPORTUNITY_SETUP_STATUS_FIELD.fieldApiName] = status;
        const recordInput = {
            fields: fields
        };

        updateRecord(recordInput).then(() => {          
            if(status == 'Completed'){
                this.dispatchToast('All good', 'success', 'Setup completed - Your opportunity is good to go.');
            }           
            this.isLoading = false;
        }).catch(error => {
            this.dispatchToast('Error', 'error', 'Error unlocking opportunity. ' + reduceErrors(error)[0]);
            this.isLoading = false;
        });
    }

    enableScreen(){
        this.showTable = true;
        this.isLoading = false
    }

    editMenuClick(){
        this.showEditMenu = true;
    }

    closeEditMenu(){
        this.showEditMenu = false;
        this.refreshData();
    }

    closeEditSingleAccount(){
        this.showEditSingleAccount = false;
        this.refreshData();
    }

    handleRowAction(event){
        let obj = event.detail;
        if(obj.action.name === 'editSingleAccount'){
            this.accountIdFromRow = obj.row.accountId;
            this.showEditSingleAccount = true;
        }
    }

    @track optionValue;
    handleOptionValueChange(event) {
    this.optionValue = event.detail;    
  }

    get percentageReady(){
        console.log('this.totalRecords ' + this.totalRecords);
        console.log('this.totalRecordsReady ' + this.totalRecordsReady);
        return 'width: ' + Math.ceil((this.totalRecordsReady/this.totalRecords) * 100) + '%';
    }

    get isEditAllDisabled (){
        return this.selectedRecords.length > 1 ? false : true;
    }

    get totalRecordsReady(){
        return this.totalRecordsReady;
    }
   
}