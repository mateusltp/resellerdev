/**
 * @description       : 
 * @author            : tiago.ribeiro@gympass.com
 * @group             : 
 * @last modified on  : 05-17-2022
 * @last modified by  : tiago.ribeiro@gympass.com
**/
import { LightningElement, api } from "lwc";
import { updateRecord } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { FlowAttributeChangeEvent, FlowNavigationNextEvent} from 'lightning/flowSupport';
import FASTTRACK_STAGE from '@salesforce/schema/Opportunity.FastTrackStage__c';
import OPP_STAGE from '@salesforce/schema/Opportunity.StageName';
import OPP_ID from '@salesforce/schema/Opportunity.Id';

export default class FastTrackFooter extends LightningElement {
    @api displayBackButton = false;
    @api displaySaveButton = false;
    @api displayContinueButton = false;
    @api backButtonLabel = 'Back';
    @api saveButtonLabel = 'Save';
    @api continueButtonLabel = 'Continue'; 
    @api backButtonVariant = 'neutral';
    @api saveButtonVariant = 'brand';
    @api continueButtonVariant = 'brand';
    @api actionClicked;
    @api backStage;
    @api oppStage;
    @api recordId;

    showSpinner = false;

    handleSave(){
        this.dispatchChangeAttributeEvent('save');
    }

    handleContinue(){
        this.dispatchChangeAttributeEvent('continue');
    }

    dispatchChangeAttributeEvent(action){
        const attributeChangeEvent = new FlowAttributeChangeEvent('actionClicked', action);
        this.dispatchEvent(attributeChangeEvent);
        this.dispatchNextEvent();
    }

    dispatchNextEvent(){
        const navigateNextEvent = new FlowNavigationNextEvent();
        this.dispatchEvent(navigateNextEvent);
    }

    handleBack(){
        this.displaySpinner();
        this.updateOpportunityBackStage();
    }

    updateOpportunityBackStage(){
        const fields = this.assignFieldsTopUpdate();
        const recordInput = {fields};

        updateRecord(recordInput)
        .then(() => {
            this.hideSpinner();
        })
        .catch(error => {
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error updating the record',
                    message: error.body.message,
                    variant: 'error'
                })
            );
            this.hideSpinner();
        });
    }

    assignFieldsTopUpdate(){
        const fields = {}
        fields[OPP_ID.fieldApiName] = this.recordId;
        fields[FASTTRACK_STAGE.fieldApiName] = this.backStage;
        fields[OPP_STAGE.fieldApiName] = this.oppStage;
        return fields;
    }

    displaySpinner(){
        this.showSpinner = true;
    }

    hideSpinner(){
        this.showSpinner = false;
    }
}