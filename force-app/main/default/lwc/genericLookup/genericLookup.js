import { LightningElement, api, track, wire } from 'lwc';

import { ldsErrorHandling } from 'c/genericLookupUtils';

import fetchLookUpValues from '@salesforce/apex/GenericLookupController.fetchLookUpValues';
import fetchPreviouslySelectedValues from '@salesforce/apex/GenericLookupController.fetchPreviouslySelectedValues';
import getSelectedRecord from '@salesforce/apex/GenericLookupController.getSelectedRecord';
import getSelectedRecordsLst from '@salesforce/apex/GenericLookupController.getSelectedRecordsLst';


const DELAY = 400;

export default class GenericLookup extends LightningElement {

    @api label = '';
    @api required = false;
    @api objectApiName = '';
    @api searchField = 'Name';
    @api placeholder = '';
    @api iconName = '';
    @track _value = '';
    @api multipleSelect = false;
    @api selectedRecord;
    @api conditions = '';
    @api showNew = false;
    @api disabled = false;
    @api readonly = false;
    @api newLabel = '';
    @api aditionalFields = '';
    @api limit = '5';
    @api orderBy = 'Name';
    @api createOnEnter = false;

    //-- for multi selection but without the standard layout. Just send the event with the record without filling the lookup
    @api getSelectedOnly = false;
    @api searchOptions = [];
    @api noEntriesMessage = 'No entries found...';
    @api createOnEnterMessage = 'Click enter to create a new record...';
    @track _notInclude = [];
    @api searchBy = 'Name';
    @track searchString = '';
    @track isOpen = false;
    @track isLoading = false;
    @track listOfSearchRecords = [];
    @track lstSelectedRecords = [];

    //-- if there are records selected previously
    @api previouslySelectedConditions;
    @api isMultiSelectRemovable = false;
    @track listOfPreviousRecords = [];


    connectedCallback() {
        console.log('### CHECKPOINT connectedCallback ###');
        // console.log('### value ###');
        // console.log(this.value);
        // console.log('### hasLstSelectedRecords ###');
        // console.log(this.hasLstSelectedRecords);
        // console.log('### lstSelectedRecords ###');
        // console.log(this.lstSelectedRecords);
        console.log('### multipleSelect ###');
        // console.log(this.multipleSelect);
        

        this.loadSelectedRecord();
    }
    
    loadSelectedRecord() {
        console.log('### CHECKPOINT loadSelectedRecord ###');
        console.log(this._value);
        console.log(!this.selectedRecord);
        console.log(this._value && !this.selectedRecord);
        if(this._value && !this.selectedRecord) {

            if(this.multipleSelect == false) {
                console.log('### IS SINGLE SELECT ###');
                getSelectedRecord({ 
                    recordId: this._value, 
                    objectApiName: this.objectApiName, 
                    searchField: this.searchField
                })
                .then(record => {
                    console.log("--- IS SUCCESS CASE ---");
                    console.log(record);
                    this.selectedRecord = record;            
                })
                .catch(error => {
                    console.log("--- IS ERROR CASE ---");
                    ldsErrorHandling(this, error);
                });
            }

            //-- case of multipleSelect is getSelectedRecordsLstenabled 
            if(this.multipleSelect == true) {
                // console.log('### IS MULTIPLE SELECT ###');
                getSelectedRecordsLst({ 
                    recordIdLst: this._value, 
                    objectApiName: this.objectApiName, 
                    searchField: this.searchField
                })
                .then(records => {
                    // console.log("--- IS MULTIPLE SUCCESS CASE ---");
                    // console.log(records);
                    //this.selectedRecords(records);
                    this.lstSelectedRecords = records;
                })
                .catch(error => {
                    // console.log("--- IS MULTIPLE ERROR CASE ---");
                    ldsErrorHandling(this, error);
                });
                // console.log('### this.lstSelectedRecords ###');
                // console.log(this.lstSelectedRecords);
            }
            
        }
    }

    //-- returns the primary sort field (default to Name)
    get fieldToDisplay() {
        return this.selectedRecord[this.searchField];
    }
      
    @api
    get value() {
        console.log("### CHECKPOINT Value GET ###");
        return this._value;
    }
       
    set value(value) {
        console.log("### CHECKPOINT Value SET ###");
        this._value = value;
        this.loadSelectedRecord();
    }
    
    get showSelectedRecord() {
        return this.value && this.selectedRecord;
    }
    
    /**
     *  Calls apex and run the query with the right component's parameters to fetch the records
     *  and user's inserted data
     */
    @wire(fetchLookUpValues, {
            searchKeyWord: '$searchString',
            ObjectName: '$objectApiName',
            aditionalFields: '$aditionalFields',
            limitStr: '$limit',
            searchBy: '$searchBy',
            notInclude: '$_notInclude',
            conditions: '$conditions',
            orderBy: '$orderBy'
    })
    handleSearchResults({ data }) {
        console.log("### CHECKPOINT handleSearchResults ###");
        if(data) {
            this.isLoading = false;
            this._searchedRecordMap = data.reduce((res, item) => {
                res[item.Id] = item;
                return res;
            }, {});

            this.listOfSearchRecords = data.map(item => {
                return {
                    id: item.Id,
                    primary: this.getFieldValue(this.searchField, item),
                };
            });
            this._recordItemMap = this.listOfSearchRecords.reduce((res, item) => {
                res[item.id] = item;
                return res;
            }, {});
        }
    }
    //-- //

    /**
     * Call to apex to fetch the currently selected elements if they exist  
     */
    @wire(fetchPreviouslySelectedValues, {
        objectName: '$objectApiName',
        conditions: '$previouslySelectedConditions',
        orderBy: '$orderBy'
    })
    handleHasSelectedRecords({ data }) {
        console.log("### CHECKPOINT handleHasSelectedRecords ###");
        if(data) {
            this.isLoading = false;
            this._searchedPreviousRecordMap = data.reduce((res, item) => {
                res[item.Id] = item;
                return res;
            }, {});

            this.listOfPreviousRecords = data.map(item => {
                return {
                    id: item.Id,
                    primary: this.getFieldValue(this.searchField, item),
                };
            });

            this._previousRecordItemMap = this.listOfPreviousRecords.reduce((res, item) => {
                res[item.id] = item;
                return res;
            }, {});

            console.log("### ***** ###");
            //-- for each item in listOfPreviousRecords, populates the support data structures
            this.listOfPreviousRecords.forEach(element => {
                console.log(element);
                console.log(element.id);
                
                const selectedRecord = this._searchedPreviousRecordMap[element.id];
                this.searchString = '';
                this.isOpen = false;
      
                if (this.getSelectedOnly) return;
                console.log("### selectedRecord ###");
                console.log(selectedRecord);
                if (this.multipleSelect) {
                    this._notInclude = this._notInclude.concat(selectedRecord.Id);
                    this.lstSelectedRecords = this.lstSelectedRecords.concat(this._previousRecordItemMap[element.id]);
                } else {
                    this.selectedRecord = selectedRecord;
                    this.value = this.selectedRecord.Id;
                }   
            });
            console.log("### ***** ###");
            console.log("### this.lstSelectedRecords ### " );
            console.log(this.lstSelectedRecords);
            console.log(this._notInclude);
        }
    }
    

    get noEntriesMsg() {
        console.log("### CHECKPOINT noEntriesMsg ###");
        return this.createOnEnter ? this.createOnEnterMessage : this.noEntriesMessage;
    }
      
    get pillTabIndex() {
        return (this.readonly || this.disabled) ? -1 : 0;
    }
      
    get noEntriesFound() {
        return this.listOfSearchRecords.length === 0;
    }
      
    get showSearchOptions() {
        return this.searchOptions.length > 0 && !this.value;
    }
      
    get getContainerClass() {
        return 'slds-combobox_container' + (this.value ? ' slds-has-selection slds-truncate' : '');
    }
      
    get comboboxClass() {
        return 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click' + (this.isOpen ? ' slds-is-open' : '');
    }
      
    get getFormElementClass() {
        return 'slds-form-element' + (this.disabled || this.readonly ? '  disable-events' : this.error ? ' slds-has-error' : '');
    }

    get getComboboxGroupClass() {
        return 'slds-combobox-group' + (this.error ? ' has-custom-error' : '');
    }
      
    get hasLstSelectedRecords() {
        console.log("### CHECKPOINT hasSelectedRecords ###");
        return this.lstSelectedRecords.length > 0;
    }
    
    get canRemoveSelected() {
        return !(this.readonly || this.disabled);
    }
      
    get notInclude() {
        console.log("### CHECKPOINT notInclude GET ###");
        return this._notInclude;
    }
      
    @api
    set notInclude(value) {
        console.log("### CHECKPOINT notInclude SET ###");
        this._notInclude = this.lstSelectedRecords.map(e => e.id).concat(value);
    }
      
    get selectedRecords() {
        console.log("### CHECKPOINT selectedRecords GET ###");
        return this.lstSelectedRecords;
    }
      
    @api
    set selectedRecords(value) {
        console.log("### CHECKPOINT selectedRecords SET ###");
        this.lstSelectedRecords = this.lstSelectedRecords.concat(value);
    }   
      
    handleItemSelected(event) {
        console.log("### CHECKPOINT handleItemSelected ###");
        const selectedRecord = this._searchedRecordMap[event.detail.id];
        this.searchString = '';
        this.dispatchEvent(new CustomEvent('itemselected', {
            detail: selectedRecord
        }));
        this.isOpen = false;
      
        if (this.getSelectedOnly) return;
      
        if (this.multipleSelect) {
            this._notInclude = this._notInclude.concat(selectedRecord.Id);
            this.lstSelectedRecords = this.lstSelectedRecords.concat(this._recordItemMap[event.detail.id]);
        } else {
            this.selectedRecord = selectedRecord;
            this.value = this.selectedRecord.Id;
        }   
    }
      
      getFieldValue(fieldName, obj) {
        if (fieldName) {
          return fieldName.split('.').reduce((result, item) => {
            return result[item];
          }, obj);
        }
        return '';
      }
      
      //-- sets up the record removal event (multi entry selection)
      removePill(event) {
        let index = event.currentTarget.name;
        
        this.lstSelectedRecords.splice(index, 1);
        this._notInclude.splice(index, 1);
        
        this.dispatchEvent(new CustomEvent('removeselected'));
      }
      
      //-- opens the dropdown
      onClick() {
        this.isOpen = true;
      }
      
      //-- sets up the record removal event
      clear() {
        this.value = '';
        this.dispatchEvent(new CustomEvent('removeselected'));
      }
      
      //-- maneges events related with the user's typing
      onKeyUp(event) {
        //console.log("### INTO THE onKeyUp ");
        
        // const keycode = (event.keyCode ? event.keyCode : event.which),
        const searchKey = event.target.value;
        
        //     console.log(keycode);
        //     console.log(this.createOnEnter);
        // if (keycode == '13' && this.createOnEnter) {
        //     this.searchString = '';
        //     this.dispatchEvent(new CustomEvent('newrecord', {
        //         detail: { value: searchKey }
        //         })
        //     );
        
        // } else {
            window.clearTimeout(this.delayTimeout);
            // eslint-disable-next-line @lwc/lwc/no-async-operation
            this.delayTimeout = setTimeout(() => {
                this.searchString = searchKey;
                this.isLoading = true;
                this.isOpen = true;
            }, DELAY);
        // }
    }
    
    //-- allows to close the dropdown section if the user click out of it
    onBlur() {
        this.isOpen = false;
        this.isLoading = false;
    }
    
    //-- additionally to preventDefault standard, it allows the user to see which record they are hovering
    onMouseDown(event) {
        event.preventDefault();
    }
      
    searchOptionChanged(event) {
        this.searchBy = event.detail.value;
    }
      
    //-- if the showNew is set to true, a button appears in the end of the result list, this function handles the click
    onClickNew() {
        this.dispatchEvent(new CustomEvent('newrecord'));
        this.isOpen = false;
    }
    
    @api
    clearSearchString(){
        console.log('$$$ INTO THE CLEANER $$$');
        this.searchString = '';
    }
}