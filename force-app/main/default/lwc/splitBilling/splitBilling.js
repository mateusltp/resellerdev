import { LightningElement, track, api, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getAccountHierarchy from '@salesforce/apex/SplitBillingController.getAccountHierarchy';
import saveAccountWithOpportunity from '@salesforce/apex/SplitBillingController.saveAccountWithOpportunity';

export default class SplitBilling extends LightningElement {

        
    @api recordId;
    @track error;

    @track isEdited = false;

    @track data = [];
    @track initialData = [];
    @track selectedAccs = []; //Filter
    @track accountMap =[];
    @track hierarchy=[];
    @track payment=[];
    @track dataTemporarySplit = [];
    @track temporaryListHierarchy = [];

    connectedCallback() {
        getAccountHierarchy({ oppId: this.recordId})
            .then(result => {
                for(let key in result){
                    this.accountMap = [...this.accountMap, { key: key, keyValue: JSON.stringify(result[key]) }];
                    this.hierarchy = JSON.parse(JSON.stringify(result['allPaymInfoInHierarchyList']));
                    this.payment = JSON.parse(JSON.stringify(result['allPaymInfoWithPaymentList']));
                }
                for(var i=0; i<this.hierarchy.length; i++){
                    this.temporaryListHierarchy.push({
                        AccountId: this.hierarchy[i].AccountId,
                        AccountName: this.hierarchy[i].AccountName,
                        AccountBillingCity: this.hierarchy[i].AccountBillingCity,
                        AccountBillingState: this.hierarchy[i].AccountBillingState,
                        AccountBillingCountry: this.hierarchy[i].AccountBillingCountry,
                        AccountLegalName: this.hierarchy[i].AccountLegalName, 
                        AccountLegalNumber: this.hierarchy[i]. AccountLegalNumber,
                        recurringFeePercentage: this.hierarchy[i].recurringFeePercentage,
                        nonRecurringFeePercentage: this.hierarchy[i].nonRecurringFeePercentage,
                        isSelected: false,
                    })
                    this.data.push({
                        Id: this.hierarchy[i].AccountId,
                        Name: this.hierarchy[i].AccountName,
                        BillingCity: this.hierarchy[i].AccountBillingCity,
                        BillingState: this.hierarchy[i].AccountBillingState,
                        BillingCountry: this.hierarchy[i].AccountBillingCountry,
                        LegalName: this.hierarchy[i].AccountLegalName, 
                        LegalNumber: this.hierarchy[i]. AccountLegalNumber,
                        RecurringFees: this.hierarchy[i].recurringFeePercentage,
                        OneTimeFees: this.hierarchy[i].nonRecurringFeePercentage,
                        isSelected: false,
                    })
                    this.initialData.push({
                        Id: this.hierarchy[i].AccountId,
                        Name: this.hierarchy[i].AccountName,
                        BillingCity: this.hierarchy[i].AccountBillingCity,
                        BillingState: this.hierarchy[i].AccountBillingState,
                        BillingCountry: this.hierarchy[i].AccountBillingCountry,
                        LegalName: this.hierarchy[i].AccountLegalName, 
                        LegalNumber: this.hierarchy[i]. AccountLegalNumber,
                        RecurringFees: this.hierarchy[i].recurringFeePercentage,
                        OneTimeFees: this.hierarchy[i].nonRecurringFeePercentage,
                        isSelected: false,
                    })
                }

                for(var i=0; i<this.payment.length; i++){
                    console.log('Splt ', this.payment[i].AccountName);
                    this.dataTemporarySplit.push({
                        AccountId: this.payment[i].AccountId,
                        AccountName: this.payment[i].AccountName,
                        AccountBillingCity: this.payment[i].AccountBillingCity,
                        AccountBillingState: this.payment[i].AccountBillingState,
                        AccountBillingCountry: this.payment[i].AccountBillingCountry,
                        AccountLegalName: this.payment[i].AccountLegalName, 
                        AccountLegalNumber: this.payment[i]. AccountLegalNumber,
                        recurringFeePercentage: this.payment[i].recurringFeePercentage,
                        nonRecurringFeePercentage: this.payment[i].nonRecurringFeePercentage,
                        isSelected: false,
                    })
                }    
            })
            .catch(error => {
                console.error(error);
                this.showToast('Error', '' + error, 'error');
            });
    }

  
    allSelected(event) {
        let selectedRows = this.template.querySelectorAll('lightning-input');
        
        for(let i = 0; i < selectedRows.length; i++) {
            if(selectedRows[i].type === 'checkbox') {
                selectedRows[i].checked = event.target.checked;
            }
        }
    }


    handlefilterChange( event ) {  
          
        const searchKey = event.target.value.toLowerCase();  
        console.log( 'Search Key is ' + searchKey );
        this.selectedAccs = [];
 
        if (searchKey) {  
            this.data = this.initialData;
 
             if ( this.temporaryListHierarchy ) {

                let recs = [];

                for ( let rec of this.temporaryListHierarchy ) {
                    this.selectedAccs.push({
                       Name: rec.Name,
                       BillingCountry: rec.BillingCountry,
                       BillingCity: rec.BillingCity,
                       BillingState: rec.BillingState
                        })
                    
                }
                for ( let rec of this.selectedAccs ) {

                    console.log( 'Rec is ' + JSON.stringify( rec ) );
                    let valuesArray = Object.values( rec );
                    console.log( 'valuesArray is ' + valuesArray );
 
                    for ( let val of valuesArray ) {
                        console.log( 'val is ' + val );
                    
                        if ( val ) {

                            if ( val.toLowerCase().includes( searchKey ) ) {

                                recs.push( rec );
                                break;
                        
                            }

                        }
                    }
                    
                }

                console.log( 'Recs are ' + JSON.stringify( recs ) );
                this.temporaryListHierarchy = recs;

             }
 
        }  else {

            this.temporaryListHierarchy = this.initialData;

        }
 
    }  

    onDoubleClickEdit(){
        this.isEdited = true;
    }

    handleCancel() {
        this.isEdited = false;
    }

    handleAllChange(event) {
        for (var i = 0; i < this.temporaryListHierarchy.length; i++) {
            this.temporaryListHierarchy[i].isSelected = event.target.checked;
            console.log('Id: ', this.temporaryListHierarchy[i].AccountId, ' isSelected: ', this.temporaryListHierarchy[i].isSelected);
        }
    }

    handleCheckChange(event) {
        var selectedRow = event.currentTarget;
        var key = selectedRow.dataset.id;
        console.log('Key: ', key);
        this.temporaryListHierarchy[key].isSelected = event.target.checked;
        console.log('Id: ', this.temporaryListHierarchy[key].AccountId, ' isSelected: ', this.temporaryListHierarchy[key].isSelected);
    }

    getSelectedAccounts(event) {
        var delAccId;
        for (var i = this.temporaryListHierarchy.length-1; i >= 0; i--) {
            if (this.temporaryListHierarchy[i].isSelected) {
                this.addAccount(this.dataTemporarySplit, this.temporaryListHierarchy[i])
                delAccId = this.temporaryListHierarchy[i].Id;
                this.temporaryListHierarchy.splice(i,1);
            }
        }
    }

    addAccount(list, info){
        list.push({
            AccountId: info.AccountId,
            AccountName: info.AccountName,
            AccountBillingCity: info.AccountBillingCity,
            AccountBillingState: info.AccountBillingState,
            AccountBillingCountry: info.AccountBillingCountry,
            AccountLegalName: info.AccountLegalName,
            AccountLegalNumber: info.AccountLegalNumber,
            recurringFeePercentage: 0,
            nonRecurringFeePercentage: 0,
            isSelected: false,
        })
    }

    remove(event) {
        var selectedRow = event.currentTarget;
        var key = selectedRow.dataset.id;

        this.addAccount(this.temporaryListHierarchy, this.dataTemporarySplit[key]);
        this.dataTemporarySplit.splice(key, 1);
    }

    handleFieldChange (event){
        let element = this.dataTemporarySplit.find(ele  => ele.AccountId === event.target.dataset.id);

        switch ( event.target.name ) { 
            case "RecurringFees":
                element.recurringFeePercentage = event.target.value;
                break;
            case "OneTimeFees":            
                element.nonRecurringFeePercentage = event.target.value;
                break;
        }
        this.dataTemporarySplit = [...this.dataTemporarySplit];
    }

    checkFees(){
        var recurring =0;
        var oneTime = 0;
        
        for (let rec of this.dataTemporarySplit){
            recurring += Number(rec.recurringFeePercentage);
            oneTime += Number(rec.nonRecurringFeePercentage);
        }

        if(recurring!='100' || oneTime!='100'){
            this.showToast('Error', 
            'Please fix deal billing percentages. The total must be 100%.', 
            'error');
        }else{
            this.handleSave();
        }
    }

    handleSave() {
        let toSaveList = this.myList;

        saveAccountWithOpportunity({oppId : this.recordId, PaymentInfoList : this.dataTemporarySplit})
        .then(() => {
            this.showToast('Success', 'Records saved succesfully!', 'success');
            this.isEdited = false;
            this.error = undefined;
        })
        .catch(error => {
            this.showToast('Error in Save', '' + error, 'error');
            this.isEdited = false;
            this.error = error;
        })
    }

    showToast(title, message, variant) {
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant,
        });
        this.dispatchEvent(evt);
    }
 
}