import { LightningElement, api } from 'lwc';

export default class ResellerEJDisplayProducts extends LightningElement {

    @api records = [];

    get hasProducts() {
        return this.records.length > 0;
    }
}