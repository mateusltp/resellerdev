import { LightningElement, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import saveProducts from '@salesforce/apex/SmbSkuSaveProductsController.saveProducts';

export default class SmbSkuSaveProducts extends LightningElement {

    @api selectedProducts;

    @api currencyIsoCode;

    @api
    get products() {
        return this.selectedProducts;
    }

    set products( products ) {
        this.selectedProducts = JSON.parse(JSON.stringify(products));
        let i = 1;
        this.selectedProducts.forEach( (element) => {
            element.rowIndex = i;
            i++;
        });
    }

    handleUnitPriceChange(event) {
        const value = event.detail.value;
        const id = event.target.dataset.id;

        for(let i = 0; i < this.selectedProducts.length; i++) {
            let product = this.selectedProducts[i];

            if( product.Product2Id === id ){
                product.UnitPrice = value;
                product.Subtotal = (product.UnitPrice * product.Quantity).toFixed(2);
                product.Total_Price__c = product.Subtotal;
                product.Discount = ( ( 1 - (product.UnitPrice / product.List_Price__c) ) * 100 ).toFixed(2);
                if(product.Discount < 0 || product.Discount > 100) {
                    product.Discount = 0;
                }
            }
        }

        this.selectedProducts = [...this.selectedProducts];

    }

    handleDeleteProduct(event) {

        const id = event.target.dataset.id
        
        const rows = JSON.parse(JSON.stringify(this.selectedProducts));
        const rowIndex = rows.findIndex((element) => element.Product2Id === id);
        rows.splice(rowIndex, 1);
        this.selectedProducts = rows;
        if(this.selectedProducts.length === 0) 
            this.handleBack();

    }

    handleBack() {
        const evt = new CustomEvent('back', {detail: this.selectedProducts});
        this.dispatchEvent(evt);       
    }

    closeModal() {
        const evt = new CustomEvent('close');
        this.dispatchEvent(evt); 
    }

    handleCancel() {
        this.closeModal();
    }

    formIsValid() {
        let salesPriceInputs = this.template.querySelectorAll('c-generic-currency-input');
        let valid = true;
        salesPriceInputs.forEach(input => valid = valid && input.reportValidity());
        return valid;
    }

    handleSave() {
        if(this.formIsValid()) {
            this.fireLoadingEvent(true);
            saveProducts({ products: this.selectedProducts })
            .then(result => {
                this.fireSaveEvent(result);
                this.dispatchEvent(
                    new ShowToastEvent({
                        message: 'Products were added to the plan.',
                        variant: 'success'
                    }),
                );
            })
            .catch(error => {
                console.error(error);
                this.dispatchEvent(
                    new ShowToastEvent({
                        message: error.body.message,
                        variant: 'error'
                    }),
                );
                this.fireLoadingEvent(false);
            });
        }
        else {
            this.dispatchEvent(
                new ShowToastEvent({
                    message: 'Please complete all mandatory fields',
                    variant: 'error'
                }),
            );
        }
    }

    fireSaveEvent(products) {
        const evt = new CustomEvent('save', { detail: products, bubbles: true, composed: true });
        this.dispatchEvent(evt);
    }

    fireLoadingEvent(value) {
        const evt = new CustomEvent('loading', { detail: value });
        this.dispatchEvent(evt);
    }

}