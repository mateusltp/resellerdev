import { LightningElement, track, api, wire } from 'lwc';
import { getRecord } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import updateProductAndWaiversAndQuote from '@salesforce/apex/ResellerSKUEditController.updateProductAndWaiversAndQuote';
import requiredFieldsMessage from '@salesforce/label/c.GenericFillRequiredFields';
import errorMessage from '@salesforce/label/c.genericErrorMessage';
import productEditedMessage from '@salesforce/label/c.ProductEdited';
import { FlowNavigationFinishEvent } from 'lightning/flowSupport';
import { refreshApex } from '@salesforce/apex';

const QUOTE_LINE_ITEM_FIELDS = ['QuoteLineItem.Allow_Waivers__c'];

export default class ResellerSkuEdit extends LightningElement {

    @api record;
    
    @api recordId;

    @track isModalOpen = true;

    @track isLoading = false;

    @track allowWaivers = false;

    waiversStartFrom = 'Beginning of the contract';

    get modalClass() {
		return `slds-modal ${this.isModalOpen ? "slds-fade-in-open" : ""}`;
	}

    get modalBackdropClass() {
		return `slds-backdrop ${this.isModalOpen ? "slds-backdrop_open" : ""}`;
	}

    @wire(getRecord, { recordId: '$recordId', fields: QUOTE_LINE_ITEM_FIELDS })
    wiredRecord({ error, data }) {
        if(data) {
            this.allowWaivers = data.fields.Allow_Waivers__c.value;
        }
        else if (error) {
            console.error(error);
            this.showToast('Error', 'Error while getting record: ' + error.body.message, 'error');
        }
    }

    openModal() {
        this.isModalOpen = true;
    }

    closeModal() {
        this.isModalOpen = false;
        this.fireCloseEvent();
        this.dispatchEvent(new FlowNavigationFinishEvent());
    }

    handleCancel() {
        this.closeModal();
    }

    skuInformationIsValid() {
        return this.template.querySelector('c-reseller-sku-information').reportValidity();
    }

    formIsValid() {
        return this.skuInformationIsValid()
    }

    handleSave() {

        this.isLoading = true;

        if(this.formIsValid()) {
            let product = this.template.querySelector('c-reseller-sku-information').getProduct();
            let quote = this.template.querySelector('c-reseller-sku-information').getQuote();
            console.log('product ',JSON.stringify(product));
            console.log('quote ',JSON.stringify(quote));
            let waiversToUpsert = [];
            let waiversToDelete = [];
            if(this.allowWaivers) {
                product.Waivers_Start_From__c = this.waiversStartFrom;
                waiversToUpsert = this.template.querySelector('c-reseller-sku-waivers').getWaiversToUpsert();
                waiversToDelete = this.template.querySelector('c-reseller-sku-waivers').getWaiversToDelete();
            }
    
            updateProductAndWaiversAndQuote({ product: product, waiversToUpsert: waiversToUpsert, waiversToDelete: waiversToDelete, quote: quote})
                .then(() => {
                    this.isLoading = false;

                    //this.fireUpdateRecordEvent(product);

                    //this.showToast('', 'The product was saved', 'Success');
    
                    //this.closeModal();
                    this.showToast('', productEditedMessage, 'Success');
                    this.template.querySelector('c-reseller-sku-information').refreshProduct();
                    this.closeModal();
                })
                .catch(error => { 
                    this.isLoading = false;
    
                    console.error(error);

                    const evt = new ShowToastEvent({
                        message: errorMessage,
                        variant: 'Error',
                    });
                    this.dispatchEvent(evt);
                });
        }
        else {
            this.isLoading = false;
            
            this.showToast('', requiredFieldsMessage, 'Error');
        }

    }

    handleClose() {
        this.closeModal();
    }

    showToast(title, message, variant) {
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant,
        });
        this.dispatchEvent(evt);
    }

    fireCloseEvent() {
        const evt = new CustomEvent('close');
        this.dispatchEvent(evt);
    }

    fireUpdateRecordEvent(product) {
        const evt = new CustomEvent('updaterecord', {detail: product});
        this.dispatchEvent(evt);
    }

}