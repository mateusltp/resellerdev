import { LightningElement, api } from 'lwc';

export default class BadgesFlow extends LightningElement {


@api itemsNeedApproval;
@api listItemsNeedApproval;

    connectedCallback() {
        if(this.itemsNeedApproval != null)
            this.listItemsNeedApproval = this.itemsNeedApproval.split(",");

    }


}