import { LightningElement, track, wire, api  } from 'lwc';
import fetchDataHelper from './networkSetupDataHelper';
import pubsub from 'c/pubsub';

const columns = [
    { label: 'Partner Name', fieldName: 'partnerName', editable: true , wrapText: true},
    { label: 'Description', fieldName: 'description', type: 'text', editable: true , wrapText: true},
    { label: 'Website', fieldName: 'website', type: 'url', editable: true },
    { label: 'Email', fieldName: 'email', type: 'email', editable: true },
    { label: 'Phone', fieldName: 'phone', type: 'text', editable: true , wrapText: true},
    { label: 'Validation type', 
        fieldName: 'validationType', 
        type: 'picklist', 
        typeAttributes: {
            placeholder: 'Choose partner level', 
            options: [
                { label: 'Smart Check-in (SCI)', value: 'Brand' },
                { label: 'Regular Check-in2', value: 'Franchisee' },
            ], // list of all picklist options 
            value: { fieldName: 'validationType' }, // default value for picklist 
            context: { fieldName: 'validationType' } // binding account Id with context variable to be returned back
        }},
    { label: 'Bank Account', fieldName: 'bankAccount',cellAttributes: { iconName: 'action:approval', class: { fieldName: 'bankAccount'} }},
    { label: 'Opening hours', fieldName: 'openingHours', cellAttributes: { iconName: 'action:approval', class: { fieldName: 'openingHours'}  }},
    { label: 'Photos', fieldName: 'photos',cellAttributes: { iconName: 'action:approval', class: { fieldName: 'photos'} }},
    { label: 'Logo', fieldName: 'logo',cellAttributes: { iconName: 'action:approval', class: { fieldName: 'logo'} }},
    { label: 'W9', fieldName: 'w9', cellAttributes: {   iconName: 'action:approval',  
                                                        class: { fieldName: 'w9'},
                                                        alignment: 'center'
                                                    }
    }
];

export default class NetworkSetupMain extends LightningElement {
    
    @api selectedAccountsId = [];
    @track error;
    @track data;
    
    columns = columns;
    spinner = false;
    partnerLevelValues = [];
    draftValues = [] ;
    dtSelectedRows = [];
    supressBottomBar = true;


    async connectedCallback() {
        const data = await fetchDataHelper({ amountOfRecords: 2 });
        this.data = data;
    }

    handleMassUpdate(event){
        this.data = event.detail;
    }
    handleSave(event){
        this.data = event.detail;
    }

    picklistChanged(event) {
        event.stopPropagation();
        let dataRecieved = event.detail.data;
        let updatedItem = { Id: dataRecieved.context, validationType: dataRecieved.value };
        this.updateDraftValues(updatedItem);
        //this.updateDataValues(updatedItem);
    }

    updateDraftValues(updateItem) {
        console.log();
        let draftValueChanged = false;
        let copyDraftValues = [...this.template.querySelector('c-custom-data-table').draftValues];
        //store changed value to do operations on save. This will enable inline editing & show standard cancel & save button
       
        copyDraftValues.forEach(item => {
            console.log(item);
            if (item.Id === updateItem.Id) {
                for (let field in updateItem) {
                    item[field] = updateItem[field];
                }
                draftValueChanged = true;
            }
        });

        let filtered = this.draftValues.filter(dValue => dValue.Id == updateItem.Id);
        if (filtered.length == 0) {
            this.draftValues = [...this.draftValues, updateItem];
        } else {
            let v = filtered[0];
            for (let field in updateItem) {
                v[field] = updateItem[field];
                this.draftValues = [...this.draftValues];
            }
        }
    }

    handleRowSelection(event) {
        console.log('handle row selection');
        let selectedRows = event.detail.selectedRows;
        this.dtSelectedRows = [...selectedRows];
        this.publishHandlerRowSelection();
    }

    publishHandlerRowSelection(){
        pubsub.publish('rowSelection', this.dtSelectedRows);
    }
}

// import { LightningElement, track } from 'lwc';

// export default class CustomDatatableDemo extends LightningElement {

//     @track data = [];
//     //have this attribute to track data changed
//     //with custom picklist or custom lookup
//     @track draftValues = [];

//     lastSavedData = [];

//     connectedCallback() {
//         this.columns = [
//             { label: 'Name', fieldName: 'Name', editable: true },
//             { label: 'Account Number', fieldName: 'AccountNumber', editable: true },
//             { label: 'Phone', fieldName: 'phone', type: 'phone', editable: true },
//             {
//                 label: 'Rating', fieldName: 'Rating', type: 'picklist', typeAttributes: {
//                     placeholder: 'Choose rating', options: [
//                         { label: 'Hot', value: 'Hot' },
//                         { label: 'Warm', value: 'Warm' },
//                         { label: 'Cold', value: 'Cold' },
//                     ] // list of all picklist options
//                     , value: { fieldName: 'Rating' } // default value for picklist
//                     , context: { fieldName: 'Id' } // binding account Id with context variable to be returned back
//                 }
//             }
//         ];

//         //sample data
//         this.data = [{ 'Id': '12345', 'Name': 'Acme', 'AccountNumber': 'CD355119-A', 'Rating': 'Hot', phone: 12537 }, { 'Id': '34567', 'Name': 'Mace', 'AccountNumber': 'CD355120-A', 'Rating': 'Cold', phone: 1978234 }]
//         //save last saved copy
//         this.lastSavedData = JSON.parse(JSON.stringify(this.data));
//     }

//     updateDataValues(updateItem) {
//         let copyData = [... this.data];
//         copyData.forEach(item => {
//             if (item.Id === updateItem.Id) {
//                 for (let field in updateItem) {
//                     item[field] = updateItem[field];
//                 }
//             }
//         });

//         //write changes back to original data
//         this.data = [...copyData];
//     }

//     updateDraftValues(updateItem) {
//         let draftValueChanged = false;
//         let copyDraftValues = [...this.draftValues];
//         //store changed value to do operations
//         //on save. This will enable inline editing &
//         //show standard cancel & save button
//         copyDraftValues.forEach(item => {
//             if (item.Id === updateItem.Id) {
//                 for (let field in updateItem) {
//                     item[field] = updateItem[field];
//                 }
//                 draftValueChanged = true;
//             }
//         });

//         if (draftValueChanged) {
//             this.draftValues = [...copyDraftValues];
//         } else {
//             this.draftValues = [...copyDraftValues, updateItem];
//         }
//     }

//     //listener handler to get the context and data
//     //updates datatable
//     picklistChanged(event) {
//         event.stopPropagation();
//         let dataRecieved = event.detail.data;
//         let updatedItem = { Id: dataRecieved.context, Rating: dataRecieved.value };
//         this.updateDraftValues(updatedItem);
//         this.updateDataValues(updatedItem);
//     }

//     //handler to handle cell changes & update values in draft values
//     handleCellChange(event) {
//         this.updateDraftValues(event.detail.draftValues[0]);
//     }

//     handleSave(event) {
//         console.log('Updated items', this.draftValues);
//         //save last saved copy
//         this.lastSavedData = JSON.parse(JSON.stringify(this.data));
//     }

//     handleCancel(event) {
//         //remove draftValues & revert data changes
//         this.data = JSON.parse(JSON.stringify(this.lastSavedData));
//         this.draftValues = [];
//     }
// }