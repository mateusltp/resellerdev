import { api, LightningElement, track, wire } from 'lwc';


import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { getPicklistValues } from 'lightning/uiObjectInfoApi';
import { getObjectInfo }     from 'lightning/uiObjectInfoApi';
import { NavigationMixin } from 'lightning/navigation';

import createContact from '@salesforce/apex/ResellerFormController.createContact';

import getAccounts from '@salesforce/apex/ResellerFormController.getAccounts';
import setAccountInAccountRequest from '@salesforce/apex/ResellerFormController.setAccountInAccountRequest';
import createNewAccount from '@salesforce/apex/ResellerFormController.createNewAccount';
import runGoNoGO from '@salesforce/apex/ResellerFormController.runGoNoGO';
import setTotalPrice from '@salesforce/apex/ResellerFormController.setTotalPrice';
import createLogError from '@salesforce/apex/ResellerFormController.createLogError';
import getInfo from '@salesforce/apex/ResellerFormController.getInfo';
import getResellerResource from '@salesforce/apex/ResellerFormController.getResellerResource';

import TEM_BLANK from './resellerFlow.html'
import TEMP_SCREEN_COMPANY from './companyScreen.html';
import TEMP_SCREEN_CONTACT from './contactScreen.html';
import TEMP_SCREEN_COMPANY_LIST from './companyListScreen.html';
import TEMP_SCREEN_FINANCIAL from './financialScreen.html';
import TEMP_SCREEN_GO_NO_GO from './goNoGoScreen.html';

//@last modified on  : 09-28-2022 - New Selected single company query
//@last modified by  : (eurikesse.ferreira@gympass.com)
import TEMP_SCREEN_SELECT_RESELLER from './selectResellerScreen.html';
import getResellerList from '@salesforce/apex/ResellerFormController.getResellerList';
import { loadStyle } from 'lightning/platformResourceLoader';
import resellerFileSelector from '@salesforce/resourceUrl/resellerFileSelector';

import LABEL_COMMUNITY_COMPANY_QUERY from '@salesforce/label/c.Community_Single_company_query';
import LABEL_COMMUNITY_SELECT_RESELLER from '@salesforce/label/c.Community_Select_Reseller';
import LABEL_COMMUNITY_NONE from '@salesforce/label/c.Community_None';
import LABEL_COMMUNITY_CONTINUE_2 from '@salesforce/label/c.Community_Next';
import hasAdminSinglePermission from '@salesforce/customPermission/Reseller_Admin';

const CNPJ = 'CNPJ';
const NIF = 'NIF';
const PRESENTATION_ENGLISH = 'Presentation_English';
const PRESENTATION_SPANISH = 'Presentation_Spanish';

const fields = [ACCOUNT_REQUEST_NAME, ACCOUNT_REQUEST_ENGINE_STATUS, ACCOUNT_REQUEST_REASON_NOGO, ACCOUNT_REQUEST_OPPORTUNITY_ID];
  
export default class ResellerFlow extends NavigationMixin(LightningElement)  {

    @api
    recordId;

    //@last modified on  : 09-30-2022 - New Selected single company query
    buttonContinue = true;
    @track items = [];
    @track valueResellers = '';
    @track chosenValue = '';

    //@last modified on  : 09-30-2022 - New Selected single company query
    @api start(){
        if(hasAdminSinglePermission){
            this.path = 6;
        }else{
            console.log("Else star");
            this.path = 1; 
            
        }
    }


//@last modified on  : 09-30-2022 - New Selected single company query
get isAdminPermission(){
    console.log("get adminSinglePermission" + hasAdminSinglePermission);
    return hasAdminSinglePermission;
}


    path = 0;

    @track userName;

    user_fields = {
        USER_ID,
        USER_COUNTRY_CODE,
        USER_NAME
    }

    modal_labels = {
        LABEL_MODAL_REQUEST_WIZARD_HEADER
    }

    company_labels ={
        LABEL_WIZARD_TITLE,
        LABEL_COMMUNITY_CLEAR,
        LABEL_COMMUNITY_NEXT,
        LABEL_COMMUNITY_UNIQUE_IDENTIFIER
    }

    company_fields ={
        ACCOUNT_REQUEST_NAME,
        ACCOUNT_REQUEST_BUSINESS_NAME,
        ACCOUNT_REQUEST_WEBSITE,
        ACCOUNT_REQUEST_BILLING_COUNTRY,
        ACCOUNT_REQUEST_UNIQUE_IDENTIFIER_TYPE,
        ACCOUNT_REQUEST_UNIQUE_IDENTIFIER
    }
    
    @api
    uniqueIdentifierType;
    @api
    uniqueKeyValueInput;
    @api
    uniqueKeyValidate;
    country;
    billingCountry;

    changeBillingCountry;
    
    uniqueKeyRequired = false;
    websiteRequired = true;
    currencyUser;

    @api contactDisabledNext;

    contact_label = {
        LABEL_COMMUNITY_CONTACT_INFO,
        LABEL_COMMUNITY_BACK,        
        LABEL_COMMUNITY_NOT_INFORM,
        LABEL_COMMUNITY_INVALID_UNIQUE_IDENTIFIER,
        LABEL_COMMUNITY_INFORM_UNIQUE_IDENTIFIER,
        LABEL_COMMUNITY_ENABLED,
        LABEL_COMMUNITY_DISABLED

    }

    contact_fields = {
        ACCOUNT_REQUEST_CONTACT_NAME,
        ACCOUNT_REQUEST_EMAIL,
        ACCOUNT_REQUEST_ROLE,
        ACCOUNT_REQUEST_DEPARTMENT
    }

    toogleFieldValue = false;

    showLoading = false;
    @track showLoadingFin = false;

    company_list_label = {
        LABEL_COMMUNITY_CHOOSE_COMPANY,
        LABEL_COMMUNITY_NONE_ABOVE,
        LABEL_COMMUNITY_COMPANY,
        LABEL_COMMUNITY_BILLING_COUNTRY,
        LABEL_COMMUNITY_WEBSITE,
        LABEL_COMMUNITY_DUPLICATE_ACCOUNT,
        LABEL_COMMUNITY_DUPLICATING_RESTRICTION 
    }
    valueRadio = '';
    companyRecordsList;
    enabledCreatedAccount = false;
    @api accountId;
    noneAbove = false;
    disabledNext = true;

    financial_fields = {
        ACCOUNT_REQUEST_OBJECT,
        ACCOUNT_REQUEST_NAME,
        ACCOUNT_REQUEST_BILLING_COUNTRY,
        ACCOUNT_REQUEST_BID,
        ACCOUNT_REQUEST_TOTAL_NUMBER_OF_EMPLOYEES,
        ACCOUNT_REQUEST_EMPLOYEES_ON_THIS_OFFER,
        ACCOUNT_REQUEST_PARTNER_MODEL,
        ACCOUNT_REQUEST_WEBSITE,        
    }     
    
    
    
    financial_label = {
        
        LABEL_COMMUNITY_FILL,
        LABEL_COMMUNITY_PARTNER_MODEL,
        LABEL_COMMUNITY_MODEL,
        LABEL_COMMUNITY_BID,
        LABEL_COMMUNITY_SUBSIDY,
        LABEL_COMMUNITY_SUBSIDY_TEXT,
        LABEL_COMMUNITY_INTERMEDIATION,
        LABEL_COMMUNITY_INTERMEDIATION_TEXT,
        LABEL_COMMUNITY_BID_FILE_NECESSARY,
        LABEL_COMMUNITY_NUMBERS_ONLY_FIELD
    }

    

    @track selectedValue;
    @track options = [];
    @track selectedBID;
    @track optionsBID = [];

    @track isDisabledScreenCompany = false;
    @track isDisabled = true;
    isDisabledNext = true;
    partnerModel;
    bid;
    fileUpload = false;

    partnerModelIsIntermediation = false;
    isDisabledFieldNumberEmployees = false;
    numberemployeesteste;

    // reasonNoGO = ACCOUNT_REQUEST_REASON_NOGO;
    
    go_no_go_label = {
        LABEL_COMMUNITY_GOOD_NEWS,
        LABEL_COMMUNITY_UNITY_CONTACT_HELP,
        LABEL_COMMUNITY_NEW_QUERY,
        LABEL_COMMUNITY_DOWNLOAD_PRESENTATION,
        LABEL_COMMUNITY_CONTINUE,
        LABEL_COMMUNITY_ACCOUNT_IS,
        LABEL_COMMUNITY_UNDER_THE,
        LABEL_COMMUNITY_MODEL_IS,
        LABEL_COMMUNITY_TOTAL_PRICE,
        LABEL_COMMUNITY_PRICE_ELIGIBLE,
        LABEL_COMMUNITY_OFFER_VALID,
        LABEL_COMMUNITY_AVAILABLE,
        LABEL_COMMUNITY_WE_SORRY,
        LABEL_COMMUNITY_NOT_AVAILABLE,
        LABEL_COMMUNITY_FOR_PROPOSALS,
        LABEL_COMMUNITY_COMPANY_CLIENT,
        LABEL_COMMUNITY_COMPANY_NEGOTIATION,
        LABEL_COMMUNITY_90_days,
        LABEL_COMMUNITY_60_days
    }

    //@last modified on  : 09-30-2022 - New Selected single company query
    label = {
        LABEL_COMMUNITY_COMPANY_QUERY,
        LABEL_COMMUNITY_SELECT_RESELLER,
        LABEL_COMMUNITY_NONE,
        LABEL_COMMUNITY_CONTINUE_2,      

    }

    showGo = false;
    resultGo;
    textNoGo;



    developerNameParam;
    staticResource;
    
    render(){
        if(this.path === 0)return TEM_BLANK;
        else if(this.path === 6)return TEMP_SCREEN_SELECT_RESELLER;
        else if(this.path === 1)return TEMP_SCREEN_COMPANY;
        else if(this.path === 2)return TEMP_SCREEN_COMPANY_LIST;
        else if(this.path === 3)return TEMP_SCREEN_CONTACT;
        else if(this.path === 4)return TEMP_SCREEN_FINANCIAL;
        else if(this.path === 5){

            this.showLoadingFin = false;
            return TEMP_SCREEN_GO_NO_GO;
        }
        
    }

    handleNext(e){
        this.recordId = e.detail.id;
        this.path++;
        this.companyRecordsList = null;
        if(this.path === 2)this.handleGetAccount();
    }

    handleBack(){
        this.path--;
        this.companyRecordsList = null;
        this.isDisabledNext = true;
        this.disabledNext = true;
        this.bid = null;
        this.partnerModel = null;
        
        if(this.path === 2)this.handleGetAccount();
    }

    
 
    progressBar() {
        clearInterval(this._interval);
        this.handleNext();
    }
    renderedCallback() {
        Promise.all([
            loadStyle(this, resellerFileSelector)
        ]);        
    }

    //@last modified on  : 09-30-2022 - New Selected single company query
    @wire(getResellerList)
        wiredUserRoles({ error, data }) {
            if (data) {
    
                for(let i=0; i<data.length; i++)  {
                    this.items = [...this.items ,{value: data[i].Id , label: data[i].Name} ];                                   
                }                
            } else if (error) {
                this.error = error;
            }
        }
    
        get resellerList() {
            return this.items;
        }
    
    handleChangeReseller(event) {
            const selectedOption = event.detail.value;
            this.chosenValue = selectedOption;
            this.buttonContinue = false;
        }
        get selectedReseller(){
            console.log('choose ', +this.chosenValue);  
            return this.chosenValue;
        }
        singleAccountRequest() {  
            this.path = 1;          
        }



    handleBillingCountry(e) {

        this.changeBillingCountry = e.detail.value;

        //if(this.changeBillingCountry != this.country){
           this.billingCountry = this.changeBillingCountry;

            if(this.changeBillingCountry == 'Brazil'){       
                this.uniqueIdentifierType = 'CNPJ';
                this.websiteRequired = false;
                this.uniqueKeyRequired = true;
                this.uniqueKeyValidate = false;
        
            }else{
                this.uniqueIdentifierType = '';
                this.websiteRequired = true;
                this.uniqueKeyRequired = false;
                this.uniqueKeyValidate = null;
                   
            }
        //}
 
    }


    /**
     * 
     * Metodos do Template Company
     */
    @wire(getRecord, {recordId : USER_ID, fields:[USER_COUNTRY_CODE, USER_NAME, USER_CURRENCY_ISO_CODE]})
        currentUser({error, data}){
            if(error){
                //SHOW ERROR USER
            }
            else if(data){
              
            console.log('data.fields.CurrencyIsoCode.value' + data.fields.CurrencyIsoCode.value);
            this.currencyUser = data.fields.CurrencyIsoCode.value
            this.country = data.fields.CountryCode.value;
            this.userName = data.fields.Name.value;
            switch (this.country){
                case 'BR':
                    this.billingCountry = 'Brazil';
                    this.uniqueIdentifierType = 'CNPJ';
                    this.uniqueKeyRequired = true;
                    this.websiteRequired = false;
                    this.uniqueKeyValidate = false;
                    this.developerNameParam = PRESENTATION_ENGLISH;
                    break;
                case 'ES':
                    this.billingCountry = 'Spain';
                    this.uniqueIdentifierType = 'NIF';
                    this.developerNameParam = PRESENTATION_SPANISH;
                    //this.uniqueKeyValidate = false;
                    break;
                case 'US':
                    this.billingCountry = 'United States';
                    this.uniqueIdentifierType = 'EIN';
                    this.developerNameParam = PRESENTATION_ENGLISH;
                    break;
                case 'DE':
                    this.billingCountry = 'Germany';
                    this.uniqueIdentifierType = 'NIF';
                    this.developerNameParam = PRESENTATION_ENGLISH;
                    break;
                case 'GB':
                    this.billingCountry = 'United Kingdom';
                    this.uniqueIdentifierType = 'UTR';
                    this.developerNameParam = PRESENTATION_ENGLISH;
                    break;
            }       
        }
    }

    handleStaticResource(){
        getResellerResource({ developerName : this.developerNameParam})
        .then(result =>{
            result = JSON.parse(result);
            this.staticResource = result.staticResource;

            window.open(this.staticResource);
        })
        .catch(error =>{
            this.showToast('Error static resource', error, 'error');
        })
    }

    handleUniqueIdentifierType(e){
        this.uniqueIdentifierType = e.target.value;
        this.uniqueKeyValueInput = null;
    }

    handleUpDateKey(e){
        this.uniqueKeyValueInput = e.detail;
    }

    handleValidate(e){
        this.uniqueKeyValidate = e.detail;
    }

    handleSubmitCompany(e){
        e.preventDefault();
        const fields = e.detail.fields;
        fields.Unique_Identifier__c = this.uniqueKeyValueInput;

        if(this.uniqueKeyValidate == false || (this.uniqueKeyValidate == false && (this.uniqueKeyValueInput != '' || this.uniqueKeyValueInput  != undefined || this.uniqueKeyValueInput != null)))
            this.showToast(this.contact_label.LABEL_COMMUNITY_INVALID_UNIQUE_IDENTIFIER, this.contact_label.LABEL_COMMUNITY_INFORM_UNIQUE_IDENTIFIER, 'error');
        else this.template.querySelector('lightning-record-edit-form').submit(fields);
    }

    handleClear(){
        this.template.querySelectorAll('lightning-input-field').forEach(element => {
            element.value = null;
        });
    }

    /**
     * 
     * Metodos do Template Contact
     */

    contactDisabledNext = true;

    changeToggle(e){
        let value = e.target.checked;
        this.toogleFieldValue = value;
        this.contactDisabledNext = !value;
        
        this.template.querySelectorAll('lightning-input-field').forEach(element => {
            element.value = null;
            element.disabled = value;
        });
    }

    handleChangeContacField(e){
        let enabledField = false;
        this.template.querySelectorAll('lightning-input-field').forEach(element => {
            if(element.value == null || element.value == undefined || element.value == '' || element.value == ' ')
                enabledField = true;
            
            if(element.name == 'email')
                if(element.value)
                    if(!element.value.includes('@'))enabledField = true;
        });

        this.template.querySelectorAll('lightning-input').forEach(element => {
            if(element.name == 'toggle'){
                if(this.toogleFieldValue == true)enabledField = false;
            }
        });
        this.contactDisabledNext = enabledField;

    }

    handleSubmitContact(e){
        e.preventDefault();
        const fields = e.detail.fields;
        fields.Use_My_Own_Data__c = this.toogleFieldValue;
        this.template.querySelector('lightning-record-edit-form').submit(fields);
    }

    /**
     * 
     * Metodos do Template companyList
     */

    handleGetAccount(){
        getAccounts({ accountRequestId : this.recordId})
        .then( result =>{
            if(result != null){
                console.log('result value : ' + result);
                result = JSON.parse(result);

                result.forEach( item =>{
                    if(item.searchTrack == 'EXACT_LEGAL_NUMBER'){
                        this.enabledCreatedAccount = true;
                    }
                    else{
                        this.enabledCreatedAccount = false; 
                    }
                })
                
                this.companyRecordsList = result;
                console.log('result value : ' + result);
                //this.enabledCreatedAccount = result.uniqueKey;
            }
            else{ console.log('RESULT NULL : ');
                this.enabledCreatedAccount = false;
            } 
        })
        .catch( error =>{
            console.log( 'Error : ' + JSON.stringify(error));
        });

        
        
    }
    
    /*handleCreateContact(){
        if(this.toogleFieldValue == false){
            createContact({accountRequestId : this.recordId})
            .then(result =>{
            
            })
            .catch(error =>{
                console.log('Error : ' + error);
            });
        }
    }*/

    handleSetAccount(){
        setAccountInAccountRequest({accountId : this.accountId, accountRequestId : this.recordId, noneAbove : this.noneAbove, resellerId: this.selectedReseller, userId: this.user_fields.USER_ID})
        .then(result =>{
            this.path++;
            this.handleGetInfo();
        })
        .catch(error =>{
            console.log('Error : ' + JSON.stringify(error));
        });

       //this.handleCreateContact();
      
    }
   
    handleSelected(e){
        this.accountId = e.target.name;
        this.noneAbove = false;
        this.disabledNext = false;

        
        Array.from(this.template.querySelectorAll('lightning-input'))
        .forEach(element => {
            element.checked=false;
        });
        const checkbox = this.template.querySelector('lightning-input[data-value="'+e.target.dataset.value+'"]');
        checkbox.checked=true;
        
    }        

    /*handleNoneAbove(){
        this.showLoading = true;
        createNewAccount({accountRequestId : this.recordId})
        .then(result =>{     
            this.disabledNext = false;
            this.noneAbove = true;
            this.showLoading = false;
            this.enabledCreatedAccount = true;
            //this.path++;
        })
        .catch(error =>{
            this.showLoading = false;
            //alert(this.company_list_label.LABEL_COMMUNITY_DUPLICATE_ACCOUNT);
            this.showToast(this.company_list_label.LABEL_COMMUNITY_DUPLICATING_RESTRICTION, this.company_list_label.LABEL_COMMUNITY_DUPLICATE_ACCOUNT, 'error');
        })
    }*/

    handleNoneAbove(){
       
        this.disabledNext = false;
        this.noneAbove = true;
        this.showLoading = false;
        this.enabledCreatedAccount = true;
        //this.path++;
     
    }

    /**
     * 
     * Metodos do Template financial
     */
    @wire(getObjectInfo, { objectApiName: ACCOUNT_REQUEST_OBJECT })
    objectInfo;
    
    // Getting Partner Model Picklist values
    @wire(getPicklistValues, { recordTypeId: '$objectInfo.data.defaultRecordTypeId', fieldApiName: ACCOUNT_REQUEST_PARTNER_MODEL})
    ratingPicklistValues({error, data}) {
        if(data) {
            console.log('result ===> '+JSON.stringify(data.values));
            this.options = data.values;
        }
        else if(error) {
            console.log('error ===> '+JSON.stringify(error));
        }
    }

    // Getting BID Picklist values
    @wire(getPicklistValues, { recordTypeId: '$objectInfo.data.defaultRecordTypeId', fieldApiName: ACCOUNT_REQUEST_BID})
    ratingPicklistBID({error, data}) {
        if(data) {
            console.log('result ===> '+JSON.stringify(data.values));
            this.optionsBID = data.values;
        }
        else if(error) {
            console.log('error ===> '+JSON.stringify(error));
        }
    }

    handleSelectedModel(e){
        let value = e.target.value;
        this.partnerModel = value;

        this.enabledNext();
        
    }

    handleSelectedBID(e){
        let value = e.target.value;
        this.bid = value;

        if(value == 'Yes')this.isDisabled = false;
        else this.isDisabled = true;

        this.enabledNext();
    }

    handlerFinishedUpload(){
        this.fileUpload = true;
    }

    enabledNext(){
        let bid = this.bid;
        let partnerModel = this.partnerModel;

        if(partnerModel != null && bid != null)this.isDisabledNext = false;
    }

    isIntermediation(){
        
        let partnerModel = this.partnerModel;

        if(partnerModel == 'Intermediation'){

            this.partnerModelIsIntermediation = true;
        }
    }

    handleSubmitFinancial(e){
        console.log( 'Spinner SubmitFinancial inicio: ' + this.showLoadingFin );
        this.showLoadingFin = true;
        e.preventDefault();
        const fields = e.detail.fields;
        fields.BID__c = this.bid;
        fields.Partner_Model__c = this.partnerModel;
        
        if((this.fileUpload && this.bid == 'Yes') || this.bid == 'No') this.template.querySelector('lightning-record-edit-form').submit(fields);
        else {
            alert(this.financial_label.LABEL_COMMUNITY_BID_FILE_NECESSARY);
            this.showLoadingFin = false;
        }
        
       
        this.isIntermediation();

        
    }
   
    webSiteValidate = '';
    companyNameValidate = '';
    timeout;
    validaWebSite(e) {
        this.webSiteValidate = e.detail.value;
        clearTimeout(this.timeout);
        this.timeout = setTimeout(this.webSiteShowToast.bind(this), 3000);
        this.isDisabledScreenCompany = true;
    }

    webSiteShowToast(){
        const validate = ["www.", "WWW.", "Www.", "wWW.", "WWw.", "wwW.", "wWw.", "WwW."];
        const webSiteSubstring = this.webSiteValidate.substring(0,4);
        //if(this.websiteRequired == true){
            if( !validate.includes(webSiteSubstring)){
                this.showToast('Informe o website com o seguinte formato: www.', 'Informe o website com o seguinte formato: www.', 'warning');
                this.isDisabledScreenCompany = true;
            }
            else if(this.webSiteValidate.length < 8){
                this.showToast('O campo deve conter mais de 8 digitos.', 'O campo deve conter mais de 8 digitos.', 'warning');
                this.isDisabledScreenCompany = true;
            } 
            else{
                this.isDisabledScreenCompany = false;
        
            }    
        //}
    }

    validaCompanyName(e) {
        this.companyNameValidate = e.detail.value;
        clearTimeout(this.timeout);
        this.timeout = setTimeout(this.companyNameShowToast.bind(this), 3000);
       
    }

    companyNameShowToast(){
       
        if(this.companyNameValidate.length < 2){
            this.showToast('O campo deve conter mais de 2 digitos.', 'O campo deve conter mais de 2 digitos.', 'warning');
            this.isDisabledScreenCompany = true;
        }
        else{
            this.isDisabledScreenCompany = false;
        }
    }

    resetFieldNumberEmployees = false;
    validateFieldNumberEmployees(e){
        if(e.charCode >= 48 && e.charCode <= 57){  
        }else{
            this.resetFieldNumberEmployees = true;
        }
    }
    validateValue = null;
    resetNumberEmployees(e){
        this.validateValue = e.target.value;
        this.validateFieldEmployees();

        if(this.resetFieldNumberEmployees == true){
            this.template.querySelector('lightning-input[data-name="numberEmployeesText"]').value = null;
            this.showToast('Invalid value', LABEL_COMMUNITY_NUMBERS_ONLY, 'warning');
            this.resetFieldNumberEmployees = false;
        }
        else{
            this.template.querySelector('lightning-input-field[data-id="numberEmployees"]').value = e.target.value;
        }
        
    } 
    
    validateFieldEmployees(){
    
        if(this.validateValue.includes("."))this.resetFieldNumberEmployees = true;
        if(this.validateValue.includes(","))this.resetFieldNumberEmployees = true; 
    }

    handleRunEngine(e){
        this.showLoadingFin = true;
        console.log( 'Spinner handleRunEngine: ' + this.showLoadingFin );
        this.recordId = e.detail.id;
        this.companyRecordsList = null;
        this.handleRunGoNoGo(); 
        
        console.log( 'Spinner handleRunEngine Fim13: ' + this.showLoadingFin );
    }

    goNoGo;
    reasonNoGO;
    oppId;
    totalPrice;
    pricePerElegible;
    nameAccountRequest;
    website;
    billingCountry;

    handleRunGoNoGo(){
        console.log('0');
        runGoNoGO({requestId : this.recordId})
        .then(rs =>{
            console.log('1');
            this.goNoGo = rs;
            setTotalPrice({ requestId : this.recordId })
            .then( result => {
                console.log('2');
                this.totalPriceManageResponse(result);
            }).catch( error =>{
                this.showLoadingFin = false;
                console.log('3');
                console.log('ERROR : ' + JSON.stringify(error));
                this.showToast('Error', error.toString(), 'error');
            })
        })
        .catch(error =>{
            this.showLoadingFin = false;
            console.log('4');
            console.log('ERROR : ' + JSON.stringify(error));
            this.showToast('Error 1', error.toString(), 'error');
        })
    }

    totalPriceManageResponse(result){

        try{

            result = JSON.parse(result);
            this.goNoGo = result.go;

            if(result.reasonNoGo != null || result.reasonNoGo != ''){
                if(result.reasonNoGo =='The queried company is already a Gympass client.'){
                    this.reasonNoGO = LABEL_COMMUNITY_COMPANY_CLIENT;
                }else if(result.reasonNoGo =='The company has a negotiation already in process.'){
                    this.reasonNoGO = LABEL_COMMUNITY_COMPANY_NEGOTIATION;
                }else if(result.reasonNoGo =='You’ve already opened an opportunity for this account. Open opportunities are valid for 60 days. Check out on My opportunities.'){
                    this.reasonNoGO = LABEL_COMMUNITY_COMPANY_RESTRICTION;
                }
            }

            this.oppId = result.oppId;
            this.totalPrice = result.totalPrice;
            this.pricePerElegible = result.pricePerElegible;
            this.path++;

        }
        catch( error ){
            this.path++;
            this.handleCreateLogError(error);
            this.showLoadingFin = false;
        }
    }

    handleCreateLogError(error){
        createLogError({requestId : this.recordId, error : error.toString()})
        .then( result => {
            //this.showToast('Atenção', 'Error no calculo do preço', 'warning');
            console.log(' SUCESSO AO GERAR LOG JS');
        })
        .catch( error =>{
            this.showToast('Error', 'Reseller Hub Log Error', 'error');
            console.log(' ERROR AO GERAR LOG JS -- > ' + JSON.stringify(error));
        })

    }

    handleGetInfo(){

        getInfo({requestId : this.recordId})
        .then(result =>{
            result = JSON.parse(result);
            this.goNoGo = result.go;
            this.nameAccountRequest = result.nameAccountRequest;
            this.website = result.website;
            this.billingCountry = result.billingCountry;
           
        })
        .catch(error =>{
            this.showLoadingFin = false;
            this.showToast('Error', error, 'error');
        })
    }

    handleNewQuery(){
        this.recordId = null;
        this.path = 1;
        this.resultGo = null;
        this.textNoGo = null;
        this.uniqueKeyValueInput = null;
        this.toogleFieldValue = false;
        this.disabledNext = true;
        this.enabledCreatedAccount = false;
        this.contactDisabledNext = true;
    }
    
    openOpportunity(){
        
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: this.oppId,
                objectApiName: 'Opportunity',
                actionName: 'view'
            },
        });
    }

    closeModal(){
        this.recordId = null;
        this.path = 0;
        this.resultGo = null;
        this.uniqueKeyValueInput = null;
        this.toogleFieldValue = false;
        this.disabledNext = true;
        this.noneAbove = false;
        this.fileUpload = false;
        this.isDisabled = true;
        this.isDisabledNext = true;
        this.enabledCreatedAccount = false;
        this.contactDisabledNext = true;
        this.handleClear();
    }

    showToast(title, mensage, variant) {
        const event = new ShowToastEvent({
            title: title,
            message: mensage,
            variant: variant,
            mode: 'dismissable'
        });
        this.dispatchEvent(event);
    }
}

import ACCOUNT_REQUEST_OBJECT from '@salesforce/schema/Account_Request__c';
import ACCOUNT_REQUEST_NAME from '@salesforce/schema/Account_Request__c.Name';
import ACCOUNT_REQUEST_BUSINESS_NAME from '@salesforce/schema/Account_Request__c.Business_Name__c';
import ACCOUNT_REQUEST_WEBSITE from '@salesforce/schema/Account_Request__c.Website__c';
import ACCOUNT_REQUEST_BILLING_COUNTRY from '@salesforce/schema/Account_Request__c.Billing_Country__c';
import ACCOUNT_REQUEST_UNIQUE_IDENTIFIER_TYPE from '@salesforce/schema/Account_Request__c.Unique_Identifier_Type__c';
import ACCOUNT_REQUEST_UNIQUE_IDENTIFIER from '@salesforce/schema/Account_Request__c.Unique_Identifier__c';
import ACCOUNT_REQUEST_DEPARTMENT from '@salesforce/schema/Account_Request__c.Department__c';
import ACCOUNT_REQUEST_ROLE from '@salesforce/schema/Account_Request__c.Role__c';
import ACCOUNT_REQUEST_EMAIL from '@salesforce/schema/Account_Request__c.Email__c';
import ACCOUNT_REQUEST_CONTACT_NAME from '@salesforce/schema/Account_Request__c.Contact_Name__c';
import ACCOUNT_REQUEST_BID from '@salesforce/schema/Account_Request__c.BID__c';
import ACCOUNT_REQUEST_TOTAL_NUMBER_OF_EMPLOYEES from '@salesforce/schema/Account_Request__c.Total_number_of_employees__c';
import ACCOUNT_REQUEST_EMPLOYEES_ON_THIS_OFFER from '@salesforce/label/c.Community_Employees_on_this_offer';
import ACCOUNT_REQUEST_PARTNER_MODEL from '@salesforce/schema/Account_Request__c.Partner_Model__c';
import ACCOUNT_REQUEST_ENGINE_STATUS from '@salesforce/schema/Account_Request__c.Engine_Status__c';
import ACCOUNT_REQUEST_REASON_NOGO from '@salesforce/schema/Account_Request__c.Reason_NoGo__c';	
import ACCOUNT_REQUEST_OPPORTUNITY_ID from '@salesforce/schema/Account_Request__c.OpportunityId__c';

import USER_ID from '@salesforce/user/Id';
import USER_COUNTRY_CODE from '@salesforce/schema/User.CountryCode';
import USER_NAME from '@salesforce/schema/User.Name';
import USER_CURRENCY_ISO_CODE from '@salesforce/schema/User.CurrencyIsoCode';

/**
 * Declaração de Labels
 */
import LABEL_MODAL_REQUEST_WIZARD_HEADER from '@salesforce/label/c.Request_Wizard_Header';

import LABEL_WIZARD_TITLE from '@salesforce/label/c.Wizard_Title';
import LABEL_COMMUNITY_CLEAR from '@salesforce/label/c.Community_Clear';
import LABEL_COMMUNITY_NEXT from '@salesforce/label/c.Community_Next';
import LABEL_COMMUNITY_UNIQUE_IDENTIFIER from '@salesforce/label/c.Community_Unique_Identifier';

import LABEL_COMMUNITY_CONTACT_INFO from '@salesforce/label/c.Community_contact_info';
import LABEL_COMMUNITY_BACK from '@salesforce/label/c.Community_Back';
import LABEL_COMMUNITY_NOT_INFORM from '@salesforce/label/c.Community_not_Inform';

import LABEL_COMMUNITY_INVALID_UNIQUE_IDENTIFIER from '@salesforce/label/c.Community_Invalid_Unique_Identifier';
import LABEL_COMMUNITY_INFORM_UNIQUE_IDENTIFIER from '@salesforce/label/c.Community_Inform_valid_Unique_Identifier';

import LABEL_COMMUNITY_ENABLED from '@salesforce/label/c.Community_Enabled';   
import LABEL_COMMUNITY_DISABLED from '@salesforce/label/c.Community_Disabled';

import LABEL_COMMUNITY_CHOOSE_COMPANY from '@salesforce/label/c.Community_Choose_Company';
import LABEL_COMMUNITY_NONE_ABOVE from '@salesforce/label/c.Community_None_Above';
import LABEL_COMMUNITY_COMPANY from '@salesforce/label/c.Community_Company';
import LABEL_COMMUNITY_BILLING_COUNTRY from '@salesforce/label/c.Community_Billing_Country';
import LABEL_COMMUNITY_WEBSITE from '@salesforce/label/c.Community_Website';
import LABEL_COMMUNITY_DUPLICATE_ACCOUNT from '@salesforce/label/c.Community_Duplicate_Account';

import LABEL_COMMUNITY_FILL from '@salesforce/label/c.Community_Fill';
import LABEL_COMMUNITY_PARTNER_MODEL from '@salesforce/label/c.Community_Partner_Model';
import LABEL_COMMUNITY_SUBSIDY from '@salesforce/label/c.Community_Subsidy';
import LABEL_COMMUNITY_SUBSIDY_TEXT from '@salesforce/label/c.Community_Subsidy_Text';
import LABEL_COMMUNITY_INTERMEDIATION from '@salesforce/label/c.Community_Intermediation';
import LABEL_COMMUNITY_INTERMEDIATION_TEXT from '@salesforce/label/c.Community_Intermediation_Text';
import LABEL_COMMUNITY_BID_FILE_NECESSARY from '@salesforce/label/c.Community_Bid_file_necessary';

import LABEL_COMMUNITY_MODEL from '@salesforce/label/c.Community_Model';
import LABEL_COMMUNITY_BID from '@salesforce/label/c.Community_BID';

import LABEL_COMMUNITY_GOOD_NEWS from '@salesforce/label/c.Community_Good_News';
import LABEL_COMMUNITY_UNITY_CONTACT_HELP from '@salesforce/label/c.Community_Contact_Help';
import LABEL_COMMUNITY_NEW_QUERY from '@salesforce/label/c.Community_New_Query';
import LABEL_COMMUNITY_DOWNLOAD_PRESENTATION from '@salesforce/label/c.Community_Download_Presentation';
import LABEL_COMMUNITY_CONTINUE from '@salesforce/label/c.Community_Continue';
import LABEL_COMMUNITY_ACCOUNT_IS from '@salesforce/label/c.Community_Account_is';
import LABEL_COMMUNITY_UNDER_THE from '@salesforce/label/c.Community_under_the';
import LABEL_COMMUNITY_MODEL_IS from '@salesforce/label/c.Community_Model_is';
import LABEL_COMMUNITY_TOTAL_PRICE from '@salesforce/label/c.Community_Total_price';
import LABEL_COMMUNITY_PRICE_ELIGIBLE from '@salesforce/label/c.Community_Price_eligible';
import LABEL_COMMUNITY_OFFER_VALID from '@salesforce/label/c.Community_Offer_valid';
import LABEL_COMMUNITY_AVAILABLE from '@salesforce/label/c.Community_available';
import LABEL_COMMUNITY_WE_SORRY from '@salesforce/label/c.Community_We_sorry';
import LABEL_COMMUNITY_NOT_AVAILABLE from '@salesforce/label/c.Community_not_available';
import LABEL_COMMUNITY_FOR_PROPOSALS from '@salesforce/label/c.Community_for_proposals';
import LABEL_COMMUNITY_COMPANY_CLIENT from '@salesforce/label/c.Community_Company_client';
import LABEL_COMMUNITY_COMPANY_NEGOTIATION from '@salesforce/label/c.Community_Company_negotiation';
import LABEL_COMMUNITY_90_days from '@salesforce/label/c.Community_90_days';
import LABEL_COMMUNITY_60_days from '@salesforce/label/c.Community_60_days';
import LABEL_COMMUNITY_DUPLICATING_RESTRICTION from '@salesforce/label/c.Community_Duplicating_restriction';

import LABEL_COMMUNITY_COMPANY_RESTRICTION from '@salesforce/label/c.Community_Company_restriction';




import LABEL_COMMUNITY_NUMBERS_ONLY from '@salesforce/label/c.Community_NumbersOnly';
import LABEL_COMMUNITY_NUMBERS_ONLY_FIELD from '@salesforce/label/c.Community_NumbersOnly_field';