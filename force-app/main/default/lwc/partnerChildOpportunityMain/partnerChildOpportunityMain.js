import { LightningElement, api, wire } from 'lwc';
import { reduceErrors } from 'c/ldsUtils';
import { refreshApex } from '@salesforce/apex';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import OPPORTUNITY_FIELD from '@salesforce/schema/Account_Opportunity_Relationship__c.Opportunity__c';
import ACCOUNT_FIELD from '@salesforce/schema/Account_Opportunity_Relationship__c.Account__c';
import ACCOUNT_NAME_FIELD from '@salesforce/schema/Account_Opportunity_Relationship__c.Account__r.Name';
import OPPORTUNITY_CLOSEDATE_FIELD from '@salesforce/schema/Opportunity.CloseDate';
import OPPORTUNITY_OWNER_FIELD from '@salesforce/schema/Opportunity.OwnerId';
import OPPORTUNITY_NAME_FIELD from '@salesforce/schema/Opportunity.Name';
import OPPORTUNITY_ACCOUNT_FIELD from '@salesforce/schema/Opportunity.AccountId';
import getLocationsByLegalGroup from '@salesforce/apex/PartnerChildOpportunityManagerController.getLocationsByLegalGroup';


const fields = [OPPORTUNITY_FIELD, ACCOUNT_FIELD, ACCOUNT_NAME_FIELD];

export default class PartnerChildOpportunityMain extends LightningElement {
    @api recordId;
    opportunityId; 
    parentAccountId;
    parentAccountName;
    showSpinner = true;
    isAccountGroupReady = false;
    accountGroup = [];
    dataResult;
    currentOpportunitySection = 1;
    oppNameField = OPPORTUNITY_NAME_FIELD;
    oppCloseDateField = OPPORTUNITY_CLOSEDATE_FIELD;
    oppOwnerField = OPPORTUNITY_OWNER_FIELD;
    oppAccountField = OPPORTUNITY_ACCOUNT_FIELD;

    @wire(getRecord, { recordId: '$recordId', fields })
    oppMember({ error, data }) {
        if (data) {
            this.opportunityId = getFieldValue(data, OPPORTUNITY_FIELD);
            this.parentAccountId = getFieldValue(data, ACCOUNT_FIELD);
            this.parentAccountName = getFieldValue(data, ACCOUNT_NAME_FIELD);
        } else if (error) {
            this.dispatchToast('Error', 'error', reduceErrors(error)[0]);
            this.showSpinner = false;
        }
    };

    @wire( getLocationsByLegalGroup, {  opportunityId : '$opportunityId' }) accountsInOpp( result ) {
        this.showSpinner = true;
        this.dataResult = result;
        if (result.data) {
            this.accountGroup = [...result.data];
            this.showSpinner = false;
            this.isAccountGroupReady = true;
        } else if (result.error) {
            this.dispatchToast('Error', 'error', reduceErrors(result.error)[0]);
            this.showSpinner = false;
        }
    };


    handleRefreshData(event){
        this.isAccountGroupReady = false;        
        this.currentOpportunitySection = 1;
       // this.getLocationsFromOpportunityMember();
        refreshApex(this.dataResult);
    }
  
    dispatchToast(title, variant, message) {
        let successtEvnt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant
        });
        this.dispatchEvent(successtEvnt);
    }


    get opportunityNumber(){
        let oppSectionNumber = this.currentOpportunitySection;
        this.currentOpportunitySection = this.currentOpportunitySection < this.accountGroup.length ? this.currentOpportunitySection+1 : this.currentOpportunitySection;
        return 'Opportunity ' + oppSectionNumber;
    }

    get showAccountLst(){
        return this.isAccountGroupReady;
    }

}