import { LightningElement, api } from 'lwc';

export default class TestLwc2 extends LightningElement {

    @api recordId;

    @api sObjectType;
    @api fieldAPIName;
    @api fieldLabel;

}