/**
 * @description       : 
 * @author            : tania.fartaria@gympass.com
 * @group             : 
 * @last modified on  : 05-19-2022
 * @last modified by  : tiago.ribeiro@gympass.com
**/
import { LightningElement,api, wire } from 'lwc';

export default class displayHelpText extends LightningElement {
    @api label;
    @api helpTextDetail;
    @api value;
    @api isBoldText;
}