import { LightningElement, api } from 'lwc';
import { CloseActionScreenEvent } from 'lightning/actions';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import { reduceErrors } from 'c/ldsUtils';
import apexSearchAccount from '@salesforce/apex/SearchCustomLookupController.searchAccount';
import apexSearchContact from '@salesforce/apex/SearchCustomLookupController.searchContact';
// import apexSearchOpportunity from '@salesforce/apex/SearchCustomLookupController.searchOpportunityByAccountId';

export default class PartnerLeadsConvert extends LightningElement {
    @api recordId;

    isNewAccountSelected = true;
    isExistingAccountSelected = false;
    existingAccountId;

    isNewContactSelected = true;
    isExistingContactSelected = false;
    existingContactId;
    
    isNewOpportunitySelected = true;
    isExistingOpportunitySelected = false;
    existingOpportunityId;
    
    /*accounts controllers*/
    onNewAccountSelected(){
        this.isNewAccountSelected = true;
        this.isExistingAccountSelected = false;
    }

    onExistingAccountSelected(){
        this.isNewAccountSelected = false;
        this.isExistingAccountSelected = true;
    }

    onSelectExistingAccount(event){
        this.existingAccountId = event.detail[0];
    }


    handleSearchAccount(event) {
        const lookupElement = event.target;
        apexSearchAccount(event.detail)
            .then(results => {
                lookupElement.setSearchResults(results);
            })
            .catch(error => {
                this.dispatchToast('Error', 'error', reduceErrors(error)[0]);
            });
    }



    /*contact controllers*/
    onNewContactSelected(){
        this.isNewContactSelected = true;
        this.isExistingContactSelected = false;
    }

    onExistingContactSelected(){
        this.isNewContactSelected = false;
        this.isExistingContactSelected = true;
    }

    onSelectExistingContact(event){
        this.existingContactId = event.detail[0];
    }

    handleSearchContact(event) {
        const lookupElement = event.target;
        apexSearchContact(event.detail)
            .then(results => {
                lookupElement.setSearchResults(results);
            })
            .catch(error => {
                this.dispatchToast('Error', 'error', reduceErrors(error)[0]);
            });
    }


     /*opportunity controllers*/
     onNewOpportunitySelected(){
        this.isNewOpportunitySelected = true;
        this.isExistingOpportunitySelected = false;
    }

    onExistingOpportunitySelected(){
        this.isNewOpportunitySelected = false;
        this.isExistingOpportunitySelected = true;
    }

    onSelectExistingOpportunity(event){
        this.existingOpportunityId = event.detail[0];
    }


    closeAction(){
        this.dispatchEvent(new CloseActionScreenEvent());
    }

    dispatchToast(title, variant, message) {
        let successtEvnt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant
        });
        this.dispatchEvent(successtEvnt);
    }


}