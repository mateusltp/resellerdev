import { LightningElement, api, track } from 'lwc';
import updateAccountLst from '@salesforce/apex/NetworkBuilderEditAllController.updateAccountLst';
import { reduceErrors } from 'c/ldsUtils';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class NetworkBuilderLegalInformation extends LightningElement {
    @api selectedAccountsId;
    spinner = false;
    @track inputFields = [];

    connectedCallback() {
        console.log('ids selecionados ' + this.selectedAccountsId);
    }

    dispatchToast(title, variant, message) {
        let successtEvnt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant
        });
        this.dispatchEvent(successtEvnt);
    }

    isInputValid() {
        let isValid = true;
        this.inputFields = this.template.querySelectorAll('.validate');
        this.inputFields.forEach(inputField => {
            if (inputField.value == null || inputField.value == '') {
                isValid = false;
            }
        });
        return isValid;
    }


    submitDetails(event) {
        this.isInputValid();

        if (this.isInputValid()) {
            this.showSpinner();
            let accountFields = {};
            console.log(JSON.stringify(event.detail));
            this.template
                .querySelectorAll("lightning-input-field")
                .forEach((input) => {
                    accountFields[input.fieldName] = input.value;
                    console.log(accountFields);
                });

            updateAccountLst({ partnersToUpdate: JSON.stringify(accountFields), selectedRecords: this.selectedAccountsId })
                .then((data) => {
                    this.dispatchToast('Saved!', 'success', 'Partner(s) updated.');
                    this.hideSpinner();
                    this.closeModal();
                })
                .catch((error) => {
                    this.dispatchToast('Error', 'error', reduceErrors(error)[0]);
                    this.hideSpinner();
                });

        } else {
            this.dispatchToast('', 'error', 'Required fields missing!');
            this.hideSpinner();
        }

    }

    closeModal() {
        console.log('On close modal');
        const myEvent = new CustomEvent('close');
        this.dispatchEvent(myEvent);
    }

    handleGoBack() {
        console.log('On go back');
        const myEvent = new CustomEvent('goback');
        this.dispatchEvent(myEvent);
    }

    showSpinner() {
        this.spinner = true;
    }

    hideSpinner() {
        this.spinner = false;
    }
}