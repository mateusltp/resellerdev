import { LightningElement, api, wire, track } from 'lwc';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import { NavigationMixin } from 'lightning/navigation';

import NAME_FIELD from '@salesforce/schema/Account.Name';
import PARENTACCOUNT_FIELD from '@salesforce/schema/Account.Parent.Name';
import PARENTACCOUNTID_FIELD from '@salesforce/schema/Account.ParentId';
import ACCOUNTOWNER_FIELD from '@salesforce/schema/Account.Owner.Name';
import ACCOUNTOWNERID_FIELD from '@salesforce/schema/Account.OwnerId';
import TYPESOFOWNERSHIP_FIELD from '@salesforce/schema/Account.Types_of_ownership__c';
import ACCOUNTSITE_FIELD from '@salesforce/schema/Account.Site';
import GYMID_FIELD from '@salesforce/schema/Account.CRM_ID__c';
import LEGALREPRESENTATIVE_FIELD from '@salesforce/schema/Account.Legal_Representative__r.Name';
import LEGALREPRESENTATIVEID_FIELD from '@salesforce/schema/Account.Legal_Representative__c';
import WEBSITE_FIELD from '@salesforce/schema/Account.Website';
import ACHIVEDSCORE_FIELD from '@salesforce/schema/Account.Achieved_Score__c';

const fields = [PARENTACCOUNT_FIELD, PARENTACCOUNTID_FIELD, ACCOUNTOWNER_FIELD, ACCOUNTOWNERID_FIELD, TYPESOFOWNERSHIP_FIELD,
    ACCOUNTSITE_FIELD, GYMID_FIELD, LEGALREPRESENTATIVE_FIELD, LEGALREPRESENTATIVEID_FIELD, ACHIVEDSCORE_FIELD,
    WEBSITE_FIELD, NAME_FIELD];


export default class MiniPageLwc extends NavigationMixin(LightningElement) {
    @api getAccountId;

    @wire(getRecord, { recordId: '$getAccountId', fields })
    account;

    negativeImagePath = '/img/samples/flag_red.gif';
    positiveImagePath = '/img/samples/flag_green.gif'
    @track imageScoreUrl;

    get name() {
        return getFieldValue(this.account.data, NAME_FIELD);
    }
    get parentAccount() {
        return getFieldValue(this.account.data, PARENTACCOUNT_FIELD);
    }

    get owner() {
        return getFieldValue(this.account.data, ACCOUNTOWNER_FIELD);
    }
    get typesOfOwnership() {
        return getFieldValue(this.account.data, TYPESOFOWNERSHIP_FIELD);
    }
    get site() {
        return getFieldValue(this.account.data, ACCOUNTSITE_FIELD);
    }
    get gymId() {
        return getFieldValue(this.account.data, GYMID_FIELD);
    }
    get legalRepresentative() {
        return getFieldValue(this.account.data, LEGALREPRESENTATIVE_FIELD);
    }
    get enablesScore() {
        let enablersScore = getFieldValue(this.account.data, ACHIVEDSCORE_FIELD);
        let defaultEmpty = '0/10';
        console.log('## enablers: '+enablersScore);
        if (!enablersScore) {
            this.imageScoreUrl = 'https://'+window.location.hostname + this.negativeImagePath;
            return defaultEmpty;
        }
        if(enablersScore < 6) {
            this.imageScoreUrl = 'https://'+window.location.hostname + this.negativeImagePath;
        } else {
            this.imageScoreUrl = 'https://'+window.location.hostname + this.positiveImagePath;
        }
        return enablersScore+'/10';
    }
    get website() {
        return getFieldValue(this.account.data, WEBSITE_FIELD);
    }

    navigateTo(e) {
        let target = e.target.dataset.targetId;
        switch(target) {
            case 'parentaccount':
                this[NavigationMixin.GenerateUrl]({
                    type: "standard__recordPage",
                    attributes: {
                        recordId: getFieldValue(this.account.data, PARENTACCOUNTID_FIELD),
                        objectApiName: 'Account',
                        actionName: 'view'
                    }
                }).then(url => {
                    window.open(url, "_blank");
                });
                break;
            case 'owner':
                this[NavigationMixin.GenerateUrl]({
                    type: "standard__recordPage",
                    attributes: {
                        recordId: getFieldValue(this.account.data, ACCOUNTOWNERID_FIELD),
                        objectApiName: 'User',
                        actionName: 'view'
                    }
                }).then(url => {
                    window.open(url, "_blank");
                });
                break;
            case 'legalrepresentative':
                this[NavigationMixin.GenerateUrl]({
                    type: "standard__recordPage",
                    attributes: {
                        recordId: getFieldValue(this.account.data, LEGALREPRESENTATIVEID_FIELD),
                        objectApiName: 'Contact',
                        actionName: 'view'
                    }
                }).then(url => {
                    window.open(url, "_blank");
                });
                break;
            case 'website':
                let url = getFieldValue(this.account.data, WEBSITE_FIELD);
                window.open(url, "_blank");
                break;
            case 'childaccount':
                this[NavigationMixin.GenerateUrl]({
                    type: "standard__recordPage",
                    attributes: {
                        recordId: this.getAccountId,
                        objectApiName: 'Account',
                        actionName: 'view'
                    }
                }).then(url => {
                    window.open(url, "_blank");
                });
                break;
        }

    }







    isClosed = false;

     disconnectedCallback() {
         this.isClosed = false;
     }


}