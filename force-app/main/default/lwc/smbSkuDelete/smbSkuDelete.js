import { LightningElement, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { deleteRecord } from 'lightning/uiRecordApi';

export default class SmbSkuDelete extends LightningElement {

    @api recordId;

    @api recordName;

    showModal = false;

    isLoading = false;

    title = 'Remove Product';

    message = 'Are you sure you want to remove this product?';

    deleteSuccessMessage1 = "Product";

    deleteSuccessMessage2 = 'was deleted.'

    hasRendered = false;

    renderedCallback() {
        if(!this.hasRendered) {
            this.hasRendered = true;
            // this timeout is for visual purposes only - this way the fade-in effect can be seen
            setTimeout(() => {
                this.showModal = true;
              }, "50");
        }
    }

    close() {
        this.showModal = false;
        this.fireCloseEvent();
    }

    handleClose() {
        this.close();
    }

    getDeleteMessage() {
        return this.deleteSuccessMessage1 + ' "' + this.recordName + '" ' + this.deleteSuccessMessage2;
    }

    handleDelete() {
        this.isLoading = true;
        deleteRecord(this.recordId)
            .then(() => {
                const evt = new ShowToastEvent({
                    title: this.getDeleteMessage(),
                    variant: "success"
                });
                this.dispatchEvent(evt);
                this.fireDeleteRecordEvent();
            }).catch(error => {
                const evt = new ShowToastEvent({
                    title: 'Error deleting record',
                    message: error.body.message,
                    variant: 'error'
                })
                this.dispatchEvent(evt);
            }).finally(() => {
                this.isLoading = false;
                this.close();
            });
    }

    fireDeleteRecordEvent() {
        const evt = new CustomEvent('deleterecord', {detail: this.recordId});
        this.dispatchEvent(evt);
    }

    fireCloseEvent() {
        const evt = new CustomEvent('close');
        this.dispatchEvent(evt);
    }


}