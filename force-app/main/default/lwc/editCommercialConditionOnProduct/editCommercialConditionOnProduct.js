import { LightningElement, track, wire, api } from 'lwc';
import { getRecordCreateDefaults, getRecord, getFieldValue } from 'lightning/uiRecordApi';
import { ProductItemPartnerTO } from './ProductItemPartnerTO';
import { getNewLayoutTemplate, recordInputForCreate } from 'c/customLayoutUtils';
import { getPicklistValues } from 'lightning/uiObjectInfoApi';
import COMMERCIAL_CONDITION_OBJECT from '@salesforce/schema/Commercial_Condition__c';
import NAME_FIELD from '@salesforce/schema/Commercial_Condition__c.Name';
import THRESHOLD_OBJECT from '@salesforce/schema/Threshold__c';
import NAME_THRESHOLD_FIELD from '@salesforce/schema/Threshold__c.Name';
import VOLUME_DISCOUNT_DATE_FIELD from '@salesforce/schema/Threshold__c.Volume_discount_date__c';
import REPLICATE_DISCOUNT_FIELD from '@salesforce/schema/Threshold__c.Replicate_Discount__c';
import VOLUME_DISCOUNT_TYPE_FIELD from '@salesforce/schema/Threshold__c.Volume_Discount_Type__c';
import PRODUCT_ACCOUNT_FIELD from '@salesforce/schema/Product_Item__c.Opportunity__r.AccountId';
import OPP_ACCOUNT_FIELD from '@salesforce/schema/Opportunity.AccountId';
import getRecordTypeIdsFormPartnerProductFlow from '@salesforce/apex/NewPartnerProductController.getRecordTypeIdsFormPartnerProductFlow';
import saveProduct from '@salesforce/apex/NewPartnerProductController.saveProduct';
import getProductData from '@salesforce/apex/NewPartnerProductController.getProductData';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import SystemModstamp from '@salesforce/schema/Account.SystemModstamp';

const oppFields = [OPP_ACCOUNT_FIELD];
const productFields = [PRODUCT_ACCOUNT_FIELD];


export default class EditCommercialConditionOnProduct extends LightningElement {

  @api partnerProductRecordTypes = {};
  exclusivityRecordTypeId;
  noShowRecordTypeId;
  lateCancellationRecordTypeId;
  tresholdPartnerRecordTypeId;
  productFlowRecordTypeId;
  partnerProductTo = new ProductItemPartnerTO();
  productItem = {};
  @track spinner = false;
  @api requiresRadionButton;
  @api radioExclusivityGroupOptions = {};
  @api recordId;
  @api objectApiName;
  @api isWishlistAccount;
  activeSections = ['NoShowFee', 'lateCancelation', 'discounts'];
  @track noShowId;
  @track hasDiscount = 'No';
  @track showDiscount = false;
  @track showDateType = false;
  @track hasNoShow = 'No';
  @track lateCancelationId;
  @track hasLateCancelation = 'No';
  @track thresholdSize;
  //@track thresholdNumber = [1, 2, 3, 4];
  @track thresholdNumber = [];
  @track showSpinner = true;
  @track sectionExclusivityFields = [];
  @track sectionNoShowFields = [];
  @track sectionLateCancelationFields = [];
  @track sectionDiscountsFields = [];
  @track thresholdArrayOfQuantity = [];
  @track thresholdIds = [];
  @track thresholdDiscountDate;
  @track thresholdType;
  @track disabledFields = true;


  sectionByName = [];
  sectionsToExclude = ['Information'];
  fieldsToExclude = [NAME_FIELD.fieldApiName, NAME_THRESHOLD_FIELD.fieldApiName, VOLUME_DISCOUNT_DATE_FIELD.fieldApiName, REPLICATE_DISCOUNT_FIELD.fieldApiName, VOLUME_DISCOUNT_TYPE_FIELD.fieldApiName];

  options = [
    { label: 'Yes', value: 'Yes' },
    { label: 'No', value: 'No' },
  ];
  @wire(getPicklistValues, {
    recordTypeId: '$partnerProductRecordTypes.tresholdPartnerRecordTypeId',
    fieldApiName: VOLUME_DISCOUNT_TYPE_FIELD
  })
  volumeDiscountPicklistValues;
  connectedCallback() {
    this.handleRecordTypes();
    getProductData({ recordId: this.recordId })
      .then((data) => {
        this.partnerProductTo.currentAccountId = data.currentAccountId;
        this.partnerProductTo.currentOpportunityId = data.currentOpportunityId;
        this.partnerProductTo.currentProductId = data.currentProductId;
        //let productDetailsCopy = this.fieldSetWrapper.fieldWrappers.map(element => ({...element}));
        this.partnerProductTo.productDetails = data.productDetails;
        this.partnerProductTo.selectedGymActivities = data.selectedGymActivities;
        this.partnerProductTo.commercialDetails = data.commercialDetails; // criar cópia e assignar - provavelmente resolve problema do disconnectedCallback
        this.partnerProductTo.thresholds = data.thresholds;

        console.log('result partnerProductTo ' + JSON.stringify(this.partnerProductTo));

        console.log(JSON.stringify(this.partnerProductTo));

        data.commercialDetails.forEach((commercial) => {
          if (commercial.key == 'No_Show') {
            if (commercial.value.fields.Id != null) {
              this.noShowId = commercial.value.fields.Id;
              this.hasNoShow = 'Yes';
            }
          } else if (commercial.key == 'Late_Cancellation') {
            if (commercial.value.fields.Id != null) {
              this.lateCancelationId = commercial.value.fields.Id;
              this.hasLateCancelation = 'Yes';
            }
          }
        });
        this.thresholdSize = data.thresholds.length;
        if (this.thresholdSize > 0) {
          console.log('TEM TRESHOLD');
          this.thresholdDiscountDate = data.thresholds[0].fields.Volume_discount_date__c;
          this.thresholdType = data.thresholds[0].fields.Volume_Discount_Type__c;

          data.thresholds.forEach((threshold) => {
            this.thresholdIds.push(threshold.fields.Id);
          });
          this.populateThresholdNumberArray(this.thresholdSize);
          console.log('this.thresholdIds ' + this.thresholdIds);
        }
      }).catch((error) => {
        console.log('error getProductData ' + JSON.stringify(error));
      });
  }
  
  get hasDiscountChecker() {
    if ( this.thresholdIds.length > 0) {
      return true;
    }
    else return false;
  }

  async handleRecordTypes() {
    try {
      const result = await getRecordTypeIdsFormPartnerProductFlow();
      //  console.log('## result: ' + JSON.stringify(result));
      this.partnerProductRecordTypes = result;
    } catch (error) {
      //  console.log('error', error);
    }
  }


  @wire(getRecordCreateDefaults, { objectApiName: COMMERCIAL_CONDITION_OBJECT.objectApiName, recordTypeId: '$partnerProductRecordTypes.exclusivityRecordTypeId' })
  commercialConditionExclusivity({ error, data }) {
    if (data) {
      const template = getNewLayoutTemplate(data, this.sectionsToExclude, this.fieldsToExclude);
      this.sectionExclusivityFields = template.sectionWithFields;
      this.sectionByName.push({ key: this.sectionExclusivityFields[0].key, value: this.sectionExclusivityFields });
      //       this.loadFieldsValueFromPartnerCommercialConditionTo(this.sectionExclusivityFields, this.partnerProductRecordTypes.exclusivityRecordTypeName);
    } else if (error) {
      //   console.log('error ' + JSON.stringify(error));
    }
  };

  @wire(getRecordCreateDefaults, { objectApiName: COMMERCIAL_CONDITION_OBJECT.objectApiName, recordTypeId: '$partnerProductRecordTypes.noShowRecordTypeId' })
  commercialConditionNoShowFee({ error, data }) {
    if (data) {
      const template = getNewLayoutTemplate(data, this.sectionsToExclude, this.fieldsToExclude);
      this.sectionNoShowFields = template.sectionWithFields;
      this.sectionByName.push({ key: this.sectionNoShowFields[0].key, value: this.sectionNoShowFields });
      //   this.loadFieldsValueFromPartnerCommercialConditionTo(this.sectionNoShowFields, this.partnerProductRecordTypes.noShowRecordTypeName);
    } else if (error) {
      //   console.log('error ' + JSON.stringify(error));
    }
  };

  @wire(getRecordCreateDefaults, { objectApiName: COMMERCIAL_CONDITION_OBJECT.objectApiName, recordTypeId: '$partnerProductRecordTypes.lateCancellationRecordTypeId' })
  commercialConditionLateCancelation({ error, data }) {
    if (data) {
      const template = getNewLayoutTemplate(data, this.sectionsToExclude, this.fieldsToExclude);
      this.sectionLateCancelationFields = template.sectionWithFields;
      this.sectionByName.push({ key: this.sectionLateCancelationFields[0].key, value: this.sectionLateCancelationFields });
      //    this.loadFieldsValueFromPartnerCommercialConditionTo(this.sectionLateCancelationFields, this.partnerProductRecordTypes.lateCancellationRecordTypeName);
    } else if (error) {
      //  console.log('error ' + JSON.stringify(error));
    }
  };


  @wire(getRecordCreateDefaults, { objectApiName: THRESHOLD_OBJECT.objectApiName, recordTypeId: '$partnerProductRecordTypes.tresholdPartnerRecordTypeId' })
  discounts({ error, data }) {
    if (data) {
      const template = getNewLayoutTemplate(data, this.sectionsToExclude, this.fieldsToExclude);
      this.sectionDiscountsFields = template.sectionWithFields;
      this.sectionByName.push({ key: this.sectionDiscountsFields[0].key, value: this.sectionDiscountsFields });
    } else if (error) {
      // console.log('error ' + JSON.stringify(error));
    }
  };

  get thresholdQuantity() {
    for (let i = 1; i <= this.thresholdSize; i++) {
      this.thresholdArrayOfQuantity.push(i);
    }
    return this.thresholdArrayOfQuantity;
  }

  get thresholdQuantity() {

  }

  handleProgressPage() {
    this.isLoad = false;
    if (this.currentStep == 's1') {
      const aCommercialConditions = this.template.querySelector('c-edit-Commercial-Condition-On-Product').handleProductDetailsValidation();
      // if (isProductDetailsValid) {
      //     this.newProdName = true;
      //     this.currentStep = 's2';
      // }

    }
  }

  async saveConditions() {
    this.handleExclusivityCommercialTOCreation();
    this.handleThresholdTOCreation();
    this.spinner = true;
    try {
      this.partnerProductTo.commercialDetails.forEach((commercial) => {
        if (commercial.key == 'CAP') {
          commercial.value.fields['RecordTypeId'] = this.partnerProductRecordTypes.capRecordTypeId;
          console.log(JSON.stringify(this.partnerProductTo.commercialDetails));
        }
      });
      this.partnerProductTo.productDetails.fields['RecordTypeId'] = this.partnerProductRecordTypes.productFlowRecordTypeId;
      this.partnerProductTo.thresholds[0].fields['Volume_Discount_Type__c'] = this.thresholdType;
      this.partnerProductTo.thresholds[0].fields['Volume_discount_date__c'] = this.thresholdDiscountDate;
      const record = await saveProduct({ partnerProductRecord: JSON.stringify(this.partnerProductTo) });
      let successtEvnt = new ShowToastEvent({
        title: 'Success',
        message: 'Product created',
        variant: 'success'
      });
      this.spinner = false;
      this.dispatchEvent(successtEvnt);
    } catch (error) {
      this.errorHandler(error);
    }

  }

  handleExclusivityCommercialTOCreation() {

    // const inputExclusivityFields = this.template.querySelectorAll('.c-input-comm-exclus');    
    const inputNoShowFields = this.template.querySelectorAll('.c-save-no-show-fee');
    const inputLateCancelFields = this.template.querySelectorAll('.c-save-late-cancelation');


    let noShowRecordInput = this.populateRecordInput(inputNoShowFields, COMMERCIAL_CONDITION_OBJECT.objectApiName, this.partnerProductRecordTypes.noShowRecordTypeId, this.noShowId);
    this.populateProductTO(this.partnerProductRecordTypes.noShowRecordTypeName, noShowRecordInput);

    let commercialLateCancelInput = this.populateRecordInput(inputLateCancelFields, COMMERCIAL_CONDITION_OBJECT.objectApiName, this.partnerProductRecordTypes.lateCancellationRecordTypeId, this.lateCancelationId);
    this.populateProductTO(this.partnerProductRecordTypes.lateCancellationRecordTypeName, commercialLateCancelInput);
  }

  handleThresholdTOCreation() {
    this.partnerProductTo.thresholds = [];
    for (let i = 1; i <= this.thresholdNumber.length; i++) {
      let queryTerm = "[data-threshold-id='" + i + "']";
      const inputValues = this.template.querySelectorAll(queryTerm);
      console.log('inputValues tresholds');
      console.log(inputValues);

      let thresholdRecord = this.populateRecordInput(inputValues, THRESHOLD_OBJECT.objectApiName, this.partnerProductRecordTypes.tresholdPartnerRecordTypeId, null);
      console.log('thresholdRecord');
      console.log(thresholdRecord);
      thresholdRecord.fields[String(VOLUME_DISCOUNT_DATE_FIELD.fieldApiName)] = this.thresholdDiscountDate;
      thresholdRecord.fields[String(VOLUME_DISCOUNT_TYPE_FIELD.fieldApiName)] = this.thresholdType;

      this.partnerProductTo.thresholds.push(thresholdRecord);
    }

    console.log(JSON.stringify(this.partnerProductTo));
  }

  populateRecordInput(inputFields, objectApiName, recordTypeId, recordId) {
    let recordInput = recordInputForCreate(objectApiName);
    inputFields.forEach(field => {
      recordInput.fields[String(field.name)] = field.value;
    });
    recordInput.fields['RecordTypeId'] = recordTypeId;
    recordInput.fields['Id'] = recordId;
    return recordInput;
  }

  handleDiscountDate(event) {
    this.thresholdDiscountDate = event.target.value;
  }

  handleVolumeDiscountTypeChange(event) {
    this.thresholdType = event.detail.value;
  }

  populateProductTO(aKey, aValue) {
    let commercialObject = { key: aKey, value: aValue };
    this.partnerProductTo.addNewCommercialConditionDetails(commercialObject);
  }

  loadThresholdValues() {
    if (this.partnerProductTo == null || this.partnerProductTo == undefined || this.partnerProductTo.thresholds == undefined)
      return;
    
    if (this.partnerProductTo.thresholds.length > 0) {
      this.populateThresholdNumberArray(this.partnerProductTo.thresholds.length);
    }

    let context = this;

    this.partnerProductTo.thresholds.forEach(function (threshold, index){
      let queryTerm = "[data-threshold-id='" + ++index + "']";
      var inputValues = context.template.querySelectorAll(queryTerm);

      inputValues.forEach(function(inValue, index) {
        inValue.value = threshold.fields[String(inputValues[index].name)];
      });
    });
  }

  populateThresholdNumberArray(thresholdLength) {
    this.thresholdNumber = [];
    for (var i = 1; i <= thresholdLength; i++) {
      this.thresholdNumber.push(i);
    }
  }

  handleNumberOfThresholds(e) {

    this.populateThresholdNumberArray(e.detail.value);

  }

  errorHandler(error) {

    var errorMessage = 'Unknown error';
    if (error && error.body && Array.isArray(error.body)) {
      errorMessage = error.body.map(e => e.message).join(', ');
    } else if ((error && error.body && error.body.output && error.body.output.errors && Array.isArray(error.body.output.errors) && error.body.output.errors.length > 0)) {
      errorMessage = error.body.output.errors.map(e => e.message).join(', ');
    } else if ((error && error.body && error.body.output && error.body.output.fieldErrors)) {
      errorMessage = (JSON.stringify(error.body.output.fieldErrors).split('message')[1]).split(':')[2];
    }
    else if (error && error.body && error.body.message) {
      errorMessage = error.body.message;
    }

    let errorEvent = new ShowToastEvent({
      title: 'Error!',
      message: errorMessage,
      variant: 'error'
    });
    this.dispatchEvent(errorEvent);
  }

  validateEmptyFieldsForTO(inputFields) {
    for (let i = 0; i < inputFields.length; i++) {
      let field = inputFields[i];
      if (field.disabled == false && field.required && field.value != null) {
        return true;
      }
    }
    return false;
  }

  dispatchToast(title, variant, message) {
    let successtEvnt = new ShowToastEvent({
      title: title,
      message: message,
      variant: variant
    });
    this.dispatchEvent(successtEvnt);
  }
}