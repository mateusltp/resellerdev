import { LightningElement, api, wire } from 'lwc';

import { FlowAttributeChangeEvent, FlowNavigationNextEvent} from 'lightning/flowSupport';

import fetchDefaultValue from '@salesforce/apex/ResellerEJContactLookupController.fetchSingleRecordSelected';
import apexContactSearch from '@salesforce/apex/ResellerEJContactLookupController.searchContact';

export default class ResellerEJResellerContact extends LightningElement {
    //-- configurable parameters
    @api recordId;
    @api defaultRecordId;
    @api accountId;
    @api lookupLabel = '';
    @api lookupPlaceholder = '';
    @api isRequired = false;

    @api searchField = 'Name';

    @api resellerPointContactId;
    @api lstSelectedRecordIds = [];

    defaultResult = {};
    listOfDefaultResults = [];

    connectedCallback() {
        this.getDefaultValues();
    }

    getDefaultValues() {
        //console.log('*** Reseller LOOKUP *** CONNECTED CALLBACK');
        fetchDefaultValue({
            defaultRecordId: this.defaultRecordId
        })
        .then((result) => {
            // console.log('*** result ***');
            // console.log(result);
            this._searchedPreviousRecordMap = result.reduce((res, item) => {
                res[item.Id] = item;
                return res;
            }, {});

            this.listOfDefaultResults = result.map(item => {
                return {
                    id: item.Id,
                    icon : 'standard:contact',
                    sObjectType : 'Contact',
                    title: this.getFieldValue(this.searchField, item),
                };
            });

            this._previousRecordItemMap = this.listOfDefaultResults.reduce((res, item) => {
                res[item.id] = item;
                return res;
            }, {});

            // console.log('*** this.listOfDefaultResults.at(0) ***');
            // console.log(this.listOfDefaultResults.at(0));
            this.defaultResult = this.listOfDefaultResults.at(0);
            // this.hasDefaultRecords = true;
            this.resellerPointContactId = this.listOfDefaultResults.at(0).id;
            this.lstSelectedRecordIds = this.lstSelectedRecordIds.concat(this.listOfDefaultResults.at(0).id);
            // console.log('*** ***');
            // console.log(this.resellerPointContactId);
            // console.log('*** ***');
        })
        .catch((error) => {
            this.error = error;
            // this.hideSpinner();
        });
    }

    handleContactSearch(event) {
        // console.log('*** INTO RESELLER LOOKUP *** SEARCH');
        const lookupElement = event.target;
        apexContactSearch({ 
                searchTerm: event.detail.searchTerm,
                selectedIds: event.detail.selectedIds,
                accountId: this.accountId
            })
            .then(results => {
                lookupElement.setSearchResults(results);
            })
            .catch(error => {
                reduceErrors(error);
            });
    }

    handleSelectedRecords(event) {
        // console.log('*** INTO RESELLER LOOKUP *** SELECTED RECORDS');
        //-- the custom lookup already updates the current selection
        this.lstSelectedRecordIds = event.detail;

        this.resellerPointContactId = event.detail[0];
        
        const attributeChangeEvent = new FlowAttributeChangeEvent(
            'resellerPointContactId',
            event.detail[0]
        );
        this.dispatchEvent(attributeChangeEvent);
    }

    getFieldValue(fieldName, obj) {
        if (fieldName) {
            return fieldName.split('.').reduce((result, item) => {
                return result[item];
            }, obj);
        }
        return '';
    }
}