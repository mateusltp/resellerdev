/**
 * Implementation notes: To use a method you need to import it as import { methodName } from 'c/customLayoutUtils';
 */

/* systemSections: sections created by SF - no need to use in create/update templates*/
const systemSections = ['Custom Links', 'System Information'];

/**
 * 
 * @param { data result of LWC function @wire(getRecordUi ... ) } data 
 * @param { recordId used to copy layout template } parentRecordId 
 * @param { sObject Api name } sObjectName 
 * @param { Compact — Use this value to get a layout that contains a record’s key fields. 
 *          Full — Use this value to get a full layout.} layoutType 
 * @param { Create—Use this mode if you intend to build UI that lets a user create a record.
 *            Edit—Use this mode if you intend to build UI that lets a user edit a record.
 *           View—Use this mode if you intend to build UI that displays a record.} modeType 
 * @param { Array of layout sections that should not be copied } sectionsToExclude 
 * @param { Array of fields that should not be copied } fieldsToExclude 
 * @returns 
 */
export function getNewLayoutTemplateUsingParentRecord(data, parentRecordId, sObjectName, layoutType, modeType, sectionsToExclude, fieldsToExclude) {
    console.log('getNewLayoutTemplateUsingParentRecord');
    let sectionWithFields = [];
    let template = {};
    if (data && data.layouts && data.records) {
        template.recordTypeId = data.records[String(parentRecordId)].recordTypeId;
        var sectionLst = [...data.layouts[sObjectName][template.recordTypeId][layoutType][modeType].sections];
        sectionLst.forEach(element => {
            var fieldLst = [];
            if ( !systemSections.includes(element.heading) && !sectionsToExclude.includes(element.heading) ) {
                element.layoutRows.forEach(row => {
                    row.layoutItems.forEach(field => {
                        field.layoutComponents.forEach(fieldItem => {
                            console.log('fieldItem');
                            console.log(fieldItem);
                            if (field.editableForNew && fieldItem.apiName != null && !fieldsToExclude.includes(field.apiName)) {
                                let aField = {};
                                aField.apiName = fieldItem.apiName;
                                aField.isRequired = field.required;
                                if (aField.apiName == 'Name' || aField.apiName == 'CRM_ID__c' || aField.apiName == 'Partner_Level__c') {
                                    aField.isBlank = true;   
                                } else {
                                    aField.isBlank = false;
                                }
                                fieldLst.push(aField);
                            }
                        });
                    });
                });
                sectionWithFields.push({ value: fieldLst, key: element.heading });
            }
        });
        template.sectionWithFields = sectionWithFields;
        return template;
    }
}

/**
 * @param { data result of LWC function @wire(getRecordCreateDefaults ... ) } data 
 * @param { Array of layout sections that should not be copied } sectionsToExclude 
 * @param { Array of fields that should not be copied } fieldsToExclude 
 * @returns 
 */

export function getNewLayoutTemplate(data, sectionsToExclude, fieldsToExclude) {
    let template = {};
    if (data && data.layout && data.layout.sections) {
        let sectionWithFields = [];
        let sectionLst = [...data.layout.sections];
        sectionLst.forEach(element => {
            var fieldLst = [];
            if ( !systemSections.includes(element.heading) && !sectionsToExclude.includes(element.heading) ) {
                element.layoutRows.forEach(row => {
                    row.layoutItems.forEach(field => {
                        field.layoutComponents.forEach(fieldItem => {
                            if (field.editableForNew && fieldItem.apiName != null && !fieldsToExclude.includes(fieldItem.apiName)) {
                                let aField = {};
                                aField.apiName = fieldItem.apiName
                                aField.isRequired = field.required;
                                aField.disabled = false;
                                aField.label = field.label;
                                fieldLst.push(aField);
                            }
                        });
                    });
                });
                sectionWithFields.push({ value: fieldLst, key: element.heading });
            }
        });
        template.sectionWithFields = sectionWithFields;
        return template;
    }
};

export function recordInputForCreate( objApiName ) {
    let recordInput = {
        "apiName": objApiName,
        "fields": {
        }
    }
    return recordInput;
}


/**
 * @param { use  const inputFields = this.template.querySelectorAll('.CLASS NAME'); ) } inputFields 
 * @returns 
 */
export function handleValidation( inputFields ) {
    const isInputsCorrect = [...inputFields]
        .reduce((validSoFar, inputField) => {             
            return validSoFar && inputField.reportValidity();
        }, true);
    return isInputsCorrect;
}

/**
 * @param { sectionWithFields } sectionWithFields 
 * @returns 
 */
export function disableFieldsFromSection( sectionWithFields, eraseFieldValues) {

    sectionWithFields.forEach(section => {
        section.value.forEach( field => {              
                field.disabled = true;
                if(eraseFieldValues){
                    field.value = '';
                }
            })
        });
}

/**
 * @param { sectionWithFields } sectionWithFields 
 * @returns 
 */
 export function enableFieldsFromSection( sectionWithFields ) {

    sectionWithFields.forEach(section => {
        section.value.forEach( field => {
               field.disabled = false;
            })
        });
   
}