import { LightningElement, api, track, wire } from 'lwc';
import callReceitaAPI from '@salesforce/apex/ReceitaAPI.callReceitaAPI';
import { ShowToastEvent } from 'lightning/platformShowToastEvent'; //Exibe mensagens para o usuário
import NAME_FIELD from '@salesforce/schema/Account.Name';
import COUNTRY_FIELD from '@salesforce/schema/Account.BillingCountry';
import WEBSITE_FIELD from '@salesforce/schema/Account.Website';

import uniqueIdentifierSearch from "@salesforce/apex/AccountSearch.uniqueIdentifierSearch";

const type = "single";

const COLUMNS = [
    { label: 'Name', fieldName: NAME_FIELD.fieldApiName, type: 'text' },
    { label: 'BillingCountry', fieldName: COUNTRY_FIELD.fieldApiName, type: 'text'},
    { label: 'Website', fieldName: WEBSITE_FIELD.fieldApiName, type: 'text' }

   
];
export default class CompIntegracaoReceita extends LightningElement {
   
    columns = COLUMNS;

    @track error;
    @track receitaData;
    @track accounts; 
    
    site = '';
    name = '';
    uniqueIdentifierType = '';
    unique_Identifier = '';
 

    consultCNPJ() {
       
        let cnpj = this.unique_Identifier;
        if (cnpj) {
            cnpj = cnpj.replace(/[^\d]/g, '');
            window.console.log(cnpj);

            callReceitaAPI({CNPJ : cnpj}).then(result => {
                this.receitaData = result;
            }).catch(error => {
                window.console.log(error);

                const event = new ShowToastEvent({
                    title: 'Erro',
                    message : 'An error occurred when consulting the data of this CNPJ.',
                    variant : 'error'
                });

                this.dispatchEvent(event);
            });
        }
    }

   
    getAccounts() {

        //var Unique_Identifier__c = this.template.querySelector('Unique_Identifier__c');
        
        var uniqueIdentifierList = [];

        if(this.unique_Identifier != null && this.unique_Identifier != ''){

            uniqueIdentifierList.push(this.unique_Identifier);

            console.log('uniqueIdentifierList:' + uniqueIdentifierList);

        }

        uniqueIdentifierSearch({uniqueIdentifierList : uniqueIdentifierList, uniqueIdentifierType : this.uniqueIdentifierType, name : this.name,  site : this.site, type : type}).then(result => {
            this.accounts = result;

        })
        .catch(error => {
            console.log('error: ', error);
        });
              
    }


    handleChangeName(event) {
        this.name = event.detail.value;
        console.log('Name:' + this.name);
    }
   
    handleChangeSite(event) {
        this.site = event.detail.value;
        console.log('Site:' + this.site);
    } 

    handleChangeUniqueIdentifierType(event) {
        this.uniqueIdentifierType = event.detail.value;
        console.log('uniqueIdentifierType:' + this.uniqueIdentifierType);
    }

    handleChangeUniqueIdentifier(event) {
        this.unique_Identifier = event.detail.value;
        console.log('Unique_Identifier:' + this.unique_Identifier);
    }

    //Retorna o registro selecionado
    //var selectedAccount = this.template.querySelector("lightning-datatable").getSelectedRows();

    handleRowSelection (event) {

        var selectedRows = event.detail.selectedRows;
        if(selectedRows.length>1)
        {
            var record = this.template.querySelector('lightning-datatable');
            selectedRows = record.selectedRows = record.selectedRows.slice(1);
            this.showNotification();
            event.preventDefault();
            return;
        }
    }

    showNotification() {
        const event = new ShowToastEvent({
            title: 'Error',
            message: 'Only one row can be selected',
            variant: 'warning',
            mode: 'pester'
        });
        this.dispatchEvent(event);
    }
    
}