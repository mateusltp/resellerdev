/**
 * @description       : 
 * @author            : tiago.ribeiro@gympass.com
 * @group             : 
 * @last modified on  : 04-08-2022
 * @last modified by  : tiago.ribeiro@gympass.com
**/
import { LightningElement, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class LostOppPopup extends LightningElement {
    showModal = false;
    showSpinner = false;
    @api recordId;
    @api objectAPIName;
    @api headerPopup;

    @api show() {
        this.showModal = true;
    }

    handleDialogClose(){
        this.handleClose();
    }

    handleClose() {
        this.showModal = false;
    }

    handleSave(){
        this.openSpinner();
        this.template.querySelector('lightning-record-edit-form').submit();
    }

    openSpinner(){
        this.showSpinner = true;
    }

    hideSpinner(){
        this.showSpinner = false;
    }

    handleSuccess(){
        this.hideSpinner();
        this.handleClose();
        const evt = new ShowToastEvent({
            title: 'Success',
            message: 'The changes have been saved',
            variant: "success"
        });
        this.dispatchEvent(evt);
    }

    handleError(){
        this.hideSpinner();
    }
}