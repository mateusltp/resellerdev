/**
 * @description       : 
 * @author            : gepi@gft.com
 * @group             : 
 * @last modified on  : 04-15-2022
 * @last modified by  : gepi@gft.com
**/
import { LightningElement, api } from 'lwc';

export default class CreatePartnersContractSelectAccounts extends LightningElement {
    // Start old
    @api objRecordId;
    @api accountData;
    @api transferObjData;

    transferObjDataClone;

    connectedCallback() {
        console.log('on connec callback');
        console.log(this.objRecordId);
    }

    handleSelectAccounts(event) {
        console.log('-andleSelectAccounts');
        if (this.transferObjDataClone == null || this.transferObjDataClone == undefined) {
            this.transferObjDataClone = JSON.parse(JSON.stringify(this.transferObjData));
        }

        var selectedAccounts = event.detail;
        var selectedAccountsIds = [];
        console.log(selectedAccounts);
        console.log(this.accountData);
        selectedAccounts.forEach(account => {
            selectedAccountsIds.push(account.id);
        });
        console.log(selectedAccountsIds);
        this.transferObjDataClone.locationsAccountIdList = selectedAccountsIds;

        this.updateMainTransferObj();
    }

    updateMainTransferObj() {
        console.log('updateMainTransferObj');
        const transferobjupdate = new CustomEvent("updateto", {
            detail: this.transferObjDataClone
        });
    
        this.dispatchEvent(transferobjupdate);
    }

    closeModal() {
        this.dispatchEvent(new CustomEvent("closemodal"));
    }
}