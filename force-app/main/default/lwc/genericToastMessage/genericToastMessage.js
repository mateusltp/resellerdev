import { LightningElement, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

import { FlowNavigationFinishEvent, FlowNavigationNextEvent, FlowNavigationPauseEvent, FlowNavigationBackEvent } from 'lightning/flowSupport';

export default class GenericToastMessage extends LightningElement {
    /* Title of the toast */
    @api title;
    
    /*
    Message of the toast. If you wanted to display the Clickable url in the toast then the message format will be
    'Record {0} created! See it {1}!'
    where {0} is the record Name and {1} is the url to the record.
    You also need to provide the recordName variable in the template so that the template can be parsed.
    For Example the message is Record {0} created! See it {1}!
    And the recordName is John Doe & the url is https://www.google.com & actionLabel is Here
    then message in the toast will look like Record John Doe created! See it Here!
    */
    @api message;

    /* Variant of the toast. Should be one of the values, info, success, warning, error */
    @api variant;
    
    /* Delay of the toast in milliseconds */
    @api delay;
    
    /* Name of the record which will be displayed over the toast message */
    @api recordName;
    
    /* URL of the record where user will be redirected when user clicks on the url */
    @api url;
    
    /* Label of the clickable button. For Example Click Here, or Here */
    @api actionLabel;

    /* Flow Navigation after the toast is thrown. */
    @api flowNavigation;

    /* Property to define if the toast message is fired in the connectedCallback of the component */
    @api doNotShowOnConnect = false;

    connectedCallback(){
        if(!this.doNotShowOnConnect)
            this.showToastMessage();
    }

    @api
    showToastMessage() {
        let toastMessage = {
            title: this.title,
            message: this.message,
            variant: this.variant?this.variant:'info'
        };
        if(this.recordName && this.url){
            toastMessage.messageData = [
                this.recordName,
                {
                    url: this.url,
                    label: this.actionLabel,
                },
            ]
        }
        if(this.delay){
            setTimeout(() => {
                this.fireToastMessage(toastMessage);
            } , this.delay);
        }else{
            this.fireToastMessage(toastMessage);
        }

    }

    fireToastMessage = (toastMessage) => {
        console.error('Toast Message: ', toastMessage);
        this.dispatchEvent(new ShowToastEvent(toastMessage));
        switch(this.flowNavigation) {
            case 'Next':
                this.dispatchEvent(new FlowNavigationNextEvent());
                break;
            case 'Back':
                this.dispatchEvent(new FlowNavigationBackEvent());
                break;
            case 'Pause':
                this.dispatchEvent(new FlowNavigationPauseEvent());
                break;
            case 'Finish':
                this.dispatchEvent(new FlowNavigationFinishEvent());
                break;
        }
            
    }
}