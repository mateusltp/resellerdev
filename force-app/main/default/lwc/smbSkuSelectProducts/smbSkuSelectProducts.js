import { LightningElement, api } from 'lwc';


export default class SmbSkuSelectProducts extends LightningElement {

    @api products;

    selectedProducts = [];

    @api currencyIsoCode;

    noProductsMessage1 = 'No products left';

    noProductsMessage2 = 'You can edit the ones you already added.';

    disableNextBtn = true;

    get noProducts() {
        return this.products.length === 0;
    }

    handleSelectAll(event) {
        if(event.target.checked) {
            this.products.forEach((element) => {
                this.addProductToSelectedList(element);
            });
        }
        else {
            this.products.forEach((element) => {
                this.removeProductFromSelectedList(element);
            });

        }
        this.updateNextBtnStatus();
    }

    addProductToSelectedList(product) {
        this.selectedProducts.push(product);
        let tr = this.template.querySelector('tr[data-id="' + product.Product2Id + '"]');
        tr.classList.add('slds-is-selected');
        let checkbox = this.template.querySelector('lightning-input[data-id="' + product.Product2Id + '"]');
        checkbox.checked = true;
    }

    removeProductFromSelectedList(product) {
        const index = this.selectedProducts.findIndex((element) => element.Product2Id === product.Product2Id);
        this.selectedProducts.splice(index, 1);
        let tr = this.template.querySelector('tr[data-id="' + product.Product2Id + '"]');
        tr.classList.remove('slds-is-selected');
        let checkbox = this.template.querySelector('lightning-input[data-id="' + product.Product2Id + '"]');
        checkbox.checked = false;
    }

    updateNextBtnStatus() {
        this.disableNextBtn = this.selectedProducts.length === 0;
    }

    handleRecordSelection(event) {
        const id = event.target.dataset.id;
        const product = this.products.find(element => element.Product2Id === id);
        if(event.target.checked) {
            this.addProductToSelectedList(product);
        }
        else {
            this.removeProductFromSelectedList(product);
        }
        this.updateNextBtnStatus();
    }

    closeModal() {
        const evt = new CustomEvent('close');
        this.dispatchEvent(evt); 
    }

    handleCancel() {
        this.closeModal();
    }

    handleContinue() {
        const evt = new CustomEvent('continue', {detail: this.selectedProducts});
        this.dispatchEvent(evt);       
    }

}